<%-- 
    Document   : UploadReport
    Created on : Jun 25, 2011, 12:28:19 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.model.table.TblEvalTscDocs"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Upload Report</title>
    </head>
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

    <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
    <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
    <script type="text/javascript">

        $(document).ready(function() {
            $("#frmUploadDoc").validate({
                rules: {
                    uploadDocFile: {required: true},
                    documentBrief: {required: true,maxlength:100}
                },
                messages: {
                    uploadDocFile: { required: "<div class='reqF_1'>Please Select Document.</div>"},
                    documentBrief: { required: "<div class='reqF_1'>Please Enter Description.</div>",
                        maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                }
            });
        });
        $(function() {
            $('#frmUploadDoc').submit(function() {
                if($('#frmUploadDoc').valid()){
                    $('.err').remove();
                    var count = 0;
                    var browserName=""
                    var maxSize = parseInt($('#fileSize').val())*1024*1024;
                    var actSize = 0;
                    var fileName = "";
                    jQuery.each(jQuery.browser, function(i, val) {
                         browserName+=i;
                    });
                    $(":input[type='file']").each(function(){
                        if(browserName.indexOf("mozilla", 0)!=-1){
                            actSize = this.files[0].size;
                            fileName = this.files[0].name;
                        }else{
                            var file = this;
                            var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                            var filepath = file.value;
                            var thefile = myFSO.getFile(filepath);
                            actSize = thefile.size;
                            fileName = thefile.name;
                        }
                        if(parseInt(actSize)==0){
                            $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                            count++;
                        }
                        if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                            $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                            count++;
                        }
                        if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                            $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                            count++;
                        }
                    });
                    if(count==0){
                        $('#button').attr("disabled", "disabled");
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            });
        });
    </script>
    <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
                String st = "";
                if (request.getParameter("st") != null && !"".equals(request.getParameter("st"))) {
                    st = request.getParameter("st");
                }
                String tenderid = "";
                if (request.getParameter("tenderid") != null && !"".equals(request.getParameter("tenderid"))) {
                    tenderid = request.getParameter("tenderid");
                }
                String lotId = "0";
                if (request.getParameter("pckLotId") != null && !"".equals(request.getParameter("pckLotId"))) {
                    lotId = request.getParameter("pckLotId");
                }
    %>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            </div>
            <div class="contentArea_1">
                <!--Dashboard Header End-->
                <div class="pageHead_1">Upload Report<span style="float:right;"><a href="TSCUploadReport.jsp?tenderid=<%=tenderid%>&st=<%=st%>" class="action-button-goback">Go Back To Dashboard</a></span></div>

                <% pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                            TenderCommonService tenderCommonSer = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> tenderLotList = tenderCommonSer.returndata("GetLotPkgDetail", tenderid, lotId);
                            String lotPkgNo = null;
                            String lotPkgDesc = null;
                            if ("0".equals(lotId)) {
                                lotPkgNo = "Package No.";
                                lotPkgDesc = "Package Description";
                            } else {
                                lotPkgNo = "Lot No.";
                                lotPkgDesc = "Lot Description";
                            }
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <form method="POST" action="<%=request.getContextPath()%>/DocUploadForLotAndPkg" enctype="multipart/form-data" id="frmUploadDoc" enctype="multipart/form-data" name="frmUploadDoc" action="">
                    <input type="hidden" name="tenderId" value="<%=tenderid%>"/>
                     <%
                            if(request.getParameter("fq")!=null){
                            %>
                            <div class="responseMsg errorMsg" style="margin-top: 10px;"><%=request.getParameter("fq")%></div>
                            <%
                            }if(request.getParameter("fs")!=null){
                            %>
                            <div class="responseMsg errorMsg" style="margin-top: 10px;">
                                Max File Size <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                            </div>
                            <%
                            }
                            %>
                            <% if(request.getParameter("msg")!=null && "success".equalsIgnoreCase(request.getParameter("msg"))){ %>
                                <div class="responseMsg successMsg" style="margin-top: 10px;">Document Deleted Successfully</div>
                            <% } %>
                            <% if(request.getParameter("msg")!=null && "upload".equalsIgnoreCase(request.getParameter("msg"))){ %>
                                <div class="responseMsg successMsg" style="margin-top: 10px;">Document uploaded Successfully</div>
                            <% } %>
                            <% if(request.getParameter("msg")!=null && "0".equalsIgnoreCase(request.getParameter("msg"))){ %>
                                <div class="responseMsg errorMsg" style="margin-top: 10px;">System can not find the document</div>
                            <% } %>

                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <td width="15%" class="t-align-left ff"><%=lotPkgNo%> :</td>
                            <td width="85%" class="t-align-left"><%=tenderLotList.get(0).getFieldName1()%>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff"><%=lotPkgDesc%> :</td>
                            <td class="t-align-left"><%=tenderLotList.get(0).getFieldName2()%>
                                <input type="hidden" name="pkgLotId" value="<%=lotId%>"/>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>
                            <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                        </tr>
                        <tr>
                            <td width="10%" class="ff t-align-left">Document   : <span class="mandatory">*</span></td>
                            <td width="90%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="ff">Description : <span>*</span></td>
                            <td>
                                <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                <div id="dvDescpErMsg" class='reqF_1'></div>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Upload" /></label>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="100%"  class="t-align-left">Instructions</th>
                        </tr>
                        <tr>
                            <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("officer");%>
                            <td class="t-align-left">
                                Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th class="t-align-center" width="10%">S.No</th>
                            <th class="t-align-center" width="25%">File Name</th>
                            <th class="t-align-center" width="45%">File Description</th>
                            <th class="t-align-center" width="10%">File Size <br />
                                (in Kb)</th>
                            <th class="t-align-center" width="15%">Action</th>
                        </tr>
                        <%
                        EvaluationService e =(EvaluationService)AppContext.getSpringBean("EvaluationService");
                        List<TblEvalTscDocs> docs = e.getDocDetails(request.getParameter("tenderid"),request.getParameter("pckLotId"));
                        if(!docs.isEmpty() || docs!=null){
                            int size=0;
                            int i =1;
                            for(TblEvalTscDocs ttsd : docs){
                                
                                size = Integer.parseInt(ttsd.getDocSize())/1024;
%>
<tr>
                                                        <td class="t-align-center" style="text-align:center;"><%= i %></td>
                                                        <td class="t-align-center" style="text-align:left;"><%= ttsd.getDocumentName()  %></td>
                                                        <td class="t-align-center" style="text-align:left;"><%= ttsd.getDocumentDesc()  %></td>
                                                        <td class="t-align-center" style="text-align:left;"><%= size  %></td>
                                                        <td class="t-align-center" style="text-align:center;">
                                                            <a href="../DocUploadForLotAndPkg?Id=<%= ttsd.getTscDocId() %>&docName=<%= ttsd.getDocumentName() %>&action=removeDoc&tenderId=<%=tenderid %>&pkgId=<%=lotId%>"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a> |
                                                            <a href="../DocUploadForLotAndPkg?docName=<%= ttsd.getDocumentName() %>&tenderId=<%=tenderid%>&docSize=<%=size%>&action=downloadDoc&pkgId=<%=lotId%>" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                                        </td>
                                                   

<%
                            i++;
                            }

                            }else{%>
                            <td colspan="5" style="text-align:center;">no records found</td>
<%}%>
 </tr>
                    </table>
                </form>
            </div>
            <div>&nbsp;</div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
<%
            if (checkExtension != null) {
                checkExtension = null;
            }
%>