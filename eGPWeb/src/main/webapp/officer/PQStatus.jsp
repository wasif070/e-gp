<%--
    Document   : PQStatus
    Created on : Dec 12, 2010, 4:23:37 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TblPostQualificationService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar" %>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Post Qualification Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            function CompareToForToday(first)
            {
                var mdy = first.split('/')  //Date and month split
                var mdyhr= mdy[2].split(' ');  //Year and time split
                var mdyhrtime=mdyhr[1].split(':');
                if(mdyhrtime[1] == undefined){
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                }else
                {
                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                }

                var d = new Date();
                if(mdyhrtime[1] == undefined){
                    var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                }
                else
                {
                    var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(),d.getHours(),d.getMinutes());
                }
                return Date.parse(valuedate) >= Date.parse(todaydate);
            }
            function ValidateSiteVisitDate() {
               clear();
               var errFlag=true;
               //if ($("#cmbSiteInspection").val() == "Satisfactory") {
                    if ($("#txtSiteVisitDate").val() == ""){
                           $('#siteDateNospan').html('Please enter Site visit Date.');
                           errFlag=false;
                    }else if(CompareToForToday(document.getElementById('txtSiteVisitDate').value))
                    {
                        $('#siteDateNospan').html('Site visit Date should not be greater than the Current Date and Time');
                        errFlag=false;
                    }else{
                        $('#siteDateNospan').html('');
                    }
//                     else {
//                        var d =new Date();
//                        var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());
//                        var value=$("#txtSiteVisitDate").val();
//                        var mdy = value.split('/')  //Date and month split
//                        var mdyhr= mdy[2].split(' ');  //Year and time split
//                        var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
//
//                        if(Date.parse(valuedate) <= Date.parse(todaydate)){
//                           $('#siteDateNospan').html('Date must be greater than the current date');
//                            errFlag=false;
//                        }
//                    }
               //}
               $('#commentsErr').html('');
               if ($("#txtComments").val() == ""){
                       $('#commentsErr').html('Please enter Comments');
                       errFlag=false;
               }else if($("#txtComments").val().length > 500){
                   $('#commentsErr').html('Allows maximum 500 characters only');
                   errFlag=false;
               }

               if (errFlag==false){
                   return false;
               } else {
                   document.getElementById("btnSubmit").style.display='none';
               }
            }

            function clear(){
                $('#siteDateNospan').html('');
                $('#commentsErr').html('');
            }

            $(document).ready(function() {

                $("#frmPostQualification").validate({
                    rules: {
                        txtComments:{required:true}
                    },
                    messages: {
                        txtComments:{required:"<div class='reqF_1'>Please Enter Comment.</div>"}
                    }
                });

                $('#cmbSiteInspection').change(function() {
                    if ($('#cmbSiteInspection').val() == "Satisfactory")
                        $('#trSiteVisitDt').show();
                    else
                        $('#trSiteVisitDt').hide();
                    });
                });

        </script>
    </head>
    <body>
        <%
           String tenderId = request.getParameter("tenderId");
           HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
           String pkgId = request.getParameter("pkgId");
           String strComments, siteVisitDt = "";

           // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType="tenderId";
            int auditId=Integer.parseInt(tenderId);
            String auditAction="Complete PQ by TEC CP";
            String moduleName=EgpModule.Evaluation.getName();
            String remarks="";
            if(request.getParameter("txtComments") != null)
            {
                remarks=request.getParameter("txtComments");
            }
            
           if (request.getParameter("btnSubmit")!=null)
                {
                    String updateString, whereCondition, postQualId = "";
                    String userId = "";

                    if (request.getParameter("uId") != null) {
                        if (request.getParameter("uId") != "" && !request.getParameter("uId").equalsIgnoreCase("null")) {
                            userId = request.getParameter("uId");
                        }
                    }

                    if (request.getParameter("postQualId") != null) {
                        if (request.getParameter("postQualId") != "" && !request.getParameter("postQualId").equalsIgnoreCase("null")) {
                            postQualId = request.getParameter("postQualId");
                        }
                    }

                    if(request.getParameter("txtSiteVisitDate")!=null){
                        siteVisitDt=request.getParameter("txtSiteVisitDate");
                    }

                    /* START: CODE TO HANDLE SPECIAL CHARACTERS  */
                    strComments = handleSpecialChar.handleSpecialChar(request.getParameter("txtComments"));
                    /* END CODE TO HANDLE SPECIAL CHARACTERS  */

                   if (siteVisitDt!= null) {
                        if (!siteVisitDt.trim().equalsIgnoreCase("")) {
                            String[] temp = siteVisitDt.split(" ");
                            String[] dtArr = temp[0].split("/");
                             siteVisitDt = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0]+" "+temp[1];
                        }
                    }
                    String status = request.getParameter("status");
                    updateString="postQualStatus='"+status+"', siteVisitDate='" + siteVisitDt + "', siteVisitStatus='" + request.getParameter("cmbSiteInspection") + "', siteVisitComments='" + strComments + "'";
                    whereCondition = "postQaulId=" + postQualId + " And userId=" + userId;
                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");

                    CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_PostQualification", updateString, whereCondition).get(0);
                    String pageName = null;
                    if (commonMsgChk.getFlag().equals(true)) {
                       if(status.equals("Disqualify")){
                            //CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                            //dataMoreService.geteGPData("PostDisqualifyCRDelete",tenderId,pkgId,userId);
                            pageName = "ProcessPQ.jsp?tenderId=" + tenderId + "&msgId=closedcrdel&pkgLotId="+pkgId ;
                       }else{
                            pageName = "ProcessPQ.jsp?tenderId=" + tenderId + "&msgId=closed&pkgLotId="+pkgId ;
                       }
                    } else {
                        auditAction="Error in "+auditAction;
                       pageName = "PQStatus.jsp?postQualId=" + postQualId + "&uId=" + userId + "&tenderId=" + tenderId + "&msgId=error" ;
                    }
                    
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);

                    response.sendRedirect(pageName);
                }else{
        %>
        <!--Dashboard Header Start-->
        <div class="topHeader">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
        </div>
        <!--Dashboard Header End-->

        <form id="frmPostQualification" action="PQStatus.jsp?postQualId=<%=request.getParameter("postQualId")%>&uId=<%=request.getParameter("uId")%>&tenderId=<%=request.getParameter("tenderId")%>" name="frmPostQualification" method="POST">
            <input type="hidden" name="pkgId" value="<%=pkgId%>" />
            <div class="dashboard_div">
                <!--Dashboard Content Part Start-->
                <div class="contentArea_1">
                     <div class="pageHead_1">Post Qualification Status
                        <span style="float:right;"><a href="InitiatePQ.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go back to Dashboard</a></span>
                    </div>

                    <% pageContext.setAttribute("tenderId", tenderId);%>

                    <%@include file="../resources/common/TenderInfoBar.jsp" %>

                    <div>&nbsp;</div>
                    <%if (request.getParameter("msgId") != null) {
                        String msgId = "", msgTxt = "";
                        boolean isError = false;
                        msgId = request.getParameter("msgId");
                        if (!msgId.equalsIgnoreCase("")) {
                            if (msgId.equalsIgnoreCase("error")) {
                                isError = true;
                                msgTxt = "There was some error.";
                            } else {
                                msgTxt = "";
                            }
                    %>
                    <%if (isError) {%>
                    <div class="responseMsg errorMsg"><%=msgTxt%></div>
                    <%} else {%>
                    <div class="responseMsg successMsg"><%=msgTxt%></div>
                    <%}%>
                    <%}
                            }%>
                    <div>&nbsp;</div>
                    <%
                         pageContext.setAttribute("TSCtab", "6");
                    %>
                    <%@include  file="../resources/common/AfterLoginTSC.jsp"%>
                    <table width="100%" cellspacing="0" class="tableList_1">

                    <%
                        if (request.getParameter("uId") != null) {
                            TblPostQualificationService service = (TblPostQualificationService)AppContext.getSpringBean("PostQualificationService");
                             List<Object[]> list = service.getAll(request.getParameter("uId"),pkgId);
                            List<SPTenderCommonData> companyList = tenderCommonService.returndata("GetCompanyByUserId",
                                    request.getParameter("uId"),
                                    "0");
                            if (!companyList.isEmpty()) {
                    %>

                         <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td width="13%" class="t-align-left ff">Bidder/Consultant : </td>
                            <td width="87%" class="t-align-left"><%=companyList.get(0).getFieldName1()%></td>
                        </tr>
                        <% }%>
                        <%if("yes".equalsIgnoreCase(list.get(0)[2].toString())){%>
                        <tr id="trSiteVisitDt">
                            <td class="t-align-left ff">Actual Site Visit Date and Time : </td>
                            <td class="t-align-left">
                                <input name="txtSiteVisitDate" type="text" class="formTxtBox_1" id="txtSiteVisitDate"
                                       style="width:12%;" readonly="true" onfocus="GetCal('txtSiteVisitDate','txtSiteVisitDate');" />
                                <img id="imgDtOfIns" name="imgDtOfIns" onclick="GetCal('txtSiteVisitDate','imgDtOfIns');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                                <span id="siteDateNospan" class="reqF_1"></span>
                            </td>
                        </tr>
                        <%}%>
                        <tr>
                            <td class="t-align-left ff">Post Qualification Status : </td>
                            <td class="t-align-left">
                                <select name="status" class="formTxtBox_1">
                                    <option value="Qualify">Qualify</option>
                                    <option value="Disqualify">Disqualify</option>
                                </select>

                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Comments : </td>
                            <td class="t-align-left">
                                <textarea cols="100" rows="5" id="txtComments" name="txtComments" class="formTxtBox_1"></textarea>
                                <span id="commentsErr" class="reqF_1"></span>
                            </td>
                        </tr>
                    </table>
                    <div class="t-align-center t_space">
                        <label class="formBtn_1">
                            <input id="btnSubmit" name="btnSubmit" type="submit" onclick="return ValidateSiteVisitDate();" value="Submit" />
                        </label>
                    </div>
                    <% }%>
                    <div>&nbsp;</div>
                </div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End-->
            </div>
        </form>
                <%}%>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
