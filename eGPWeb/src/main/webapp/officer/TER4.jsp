<%-- 
    Document   : TER4
    Created on : Apr 14, 2011, 10:31:01 AM
    Author     : TaherT
--%>
<%--
Modified functions and add parameter for re-evaluation by dohatec
--%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    String tenderid = request.getParameter("tenderid");
                    String lotId = request.getParameter("lotId");
                    String roundId = request.getParameter("rId");
                    String stat = request.getParameter("stat");//stat value is 'eval'
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    String pNature = commonService.getProcNature(request.getParameter("tenderid")).toString();
                    String pMethod = commonService.getProcMethod(request.getParameter("tenderid")).toString();
                    String eventType = commonService.getEventType(tenderid).toString();
                    //String tenderPaidORFree = commonService.tenderPaidORFree(tenderid, lotId);

                    String repLabel = "Tender";
                    if(pNature.equalsIgnoreCase("Services")){
                        repLabel = "Proposal";
                    }
                     boolean bole_ter_flag = true;
                      if(request.getParameter("isPDF")!=null && "true".equalsIgnoreCase(request.getParameter("isPDF"))){
                        bole_ter_flag = false;
                        //System.out.println("isPDF"+bole_ter_flag);
                    }
                    int evalCount = 0;
                    if(request.getParameter("evalCount")!=null){
                        evalCount = Integer.parseInt(request.getParameter("evalCount"));
                     }
                    String userId = "";
                     
                    if(bole_ter_flag){
                        if(session.getAttribute("userId") != null){
                            userId = session.getAttribute("userId").toString();
                           
                        }
                    }else{
                        if(request.getParameter("tendrepUserId") != null && !"".equals(request.getParameter("tendrepUserId"))){
                            userId = request.getParameter("tendrepUserId");
                            //out.println("tendrepUserId="+request.getParameter("tendrepUserId"));
                        }
                    }

                    boolean showGoBack = true;
                    if("y".equals(request.getParameter("in"))){
                        showGoBack = false;
                    }
                    boolean isPQ = false;
                    boolean isNeg = false;
                    if (!"Services".equalsIgnoreCase(pNature) && (!(pMethod.equals("DPM"))) && (!(eventType.equalsIgnoreCase("PQ") || eventType.equalsIgnoreCase("REOI") || eventType.equalsIgnoreCase("1 stage-TSTM")))){
                        isPQ = true;
                    }
                    if("Services".equalsIgnoreCase(pNature) && (!(eventType.equalsIgnoreCase("PQ") || eventType.equalsIgnoreCase("REOI") || eventType.equalsIgnoreCase("1 stage-TSTM")))){
                        isNeg = true;
                    }
                    if(!"Services".equalsIgnoreCase(pNature) && (pMethod.equalsIgnoreCase("DPM"))){
                        isNeg = true;
                    }
                   
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=repLabel%> Evaluation Report 4</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            function signCheck(){
                /*jAlert("TER3 is not signed by all the Evaluation Committee members yet.","Member Signing", function(RetVal) {
                });*/
                alert("TER3 is not signed by all the Evaluation Committee members yet.");
                return false;
            }
        </script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <body>
         <%if(bole_ter_flag){%>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%}
                    ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                   CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");


                    List<SPCommonSearchDataMore> tendOpeningRpt = creationService.getOpeningReportData("getTenderOpeningRpt", tenderid, lotId,null);//lotid
                    List<SPCommonSearchDataMore> TECMembers = dataMoreService.getCommonSearchData("getTECMembers", tenderid, lotId,"TER4",roundId,String.valueOf(evalCount)); //lotid ,"TOR1"

                    List<SPCommonSearchDataMore> TendererAwarded = dataMoreService.getCommonSearchData("getQualifiedBidders", tenderid, lotId,pNature,String.valueOf(evalCount)); //lotid ,"TOR1"
// Edit By Palash, Dohatec
                    List<SPCommonSearchDataMore> TenderRecommendetion = dataMoreService.getCommonSearchData("reTenderRecommendetion", tenderid, String.valueOf(evalCount));
                    //List<SPCommonSearchDataMore> findAAUser = creationService.getreTenderRecommendetion("findAAUser", tenderid);
  // End
                    List<SPCommonSearchDataMore> ter3Signed = dataMoreService.getCommonSearchData("isTERSignedByAll", tenderid, lotId,"3","'TER1','TER2','TER3'",roundId,String.valueOf(evalCount));

                    boolean isTER3Signed = false;
                    if(ter3Signed!=null && (!ter3Signed.isEmpty()) && ter3Signed.get(0).getFieldName1().equals("1")){
                        isTER3Signed = true;
                    }

                    //List<Object[]> finalSubmissionUser = creationService.getFinalSubmissionUser(tenderid);

                    //List<Object[]> ter1Criteria = creationService.getTERCriteria("TER1", pNature);
                    List<SPCommonSearchDataMore> NoteofDissent = creationService.getOpeningReportData("getNoteofDissent", tenderid, lotId,"ter4");
                    List<SPCommonSearchDataMore> pMethodEstCost = dataMoreService.getCommonSearchData("GetPMethodEstCostTender", tenderid);

                    String estCost = pMethodEstCost.get(0).getFieldName2();
                    String estCostStatus = "-";
                    if (estCost != null) {
                        estCostStatus = (Double.parseDouble(estCost) > 0 ? "Approved" : "-");
                    }

                    boolean list1 = true;
                    boolean list5 = true;
                    boolean isView = false;
                    if (tendOpeningRpt.isEmpty()) {
                        list1 = false;
                    }
                    if (TECMembers.isEmpty()) {
                        list5 = false;
                    }
                     if("y".equals(request.getParameter("isview"))){
                        isView = true;
                    }
                    
                    if(isView)
                   {
                         // Coad added by Dipal for Audit Trail Log.
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        String idType="tenderId";
                        int auditId=Integer.parseInt(request.getParameter("tenderid"));
                        String auditAction="View Tender Evaluation Report 4";
                        String moduleName=EgpModule.Evaluation.getName();
                        String remarks="";
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    }
            %>
        <div class="tabPanelArea_1">
             <div  id="print_area">
            <div class="pageHead_1"><%=repLabel%> Evaluation Report 4 - Final Evaluation Report<span style="float: right;">
                    <%
                        dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        boolean isCommCP = false;                        
                        List<SPCommonSearchDataMore> commCP = dataMoreService.getCommonSearchData("evaluationMemberCheck", tenderid, userId, "1", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);                        
                        if (commCP != null && (!commCP.isEmpty()) && (!commCP.get(0).getFieldName1().equals("0"))) {
                            isCommCP = true;
                        }                        
                        commCP=null;
                         if(bole_ter_flag){
                            if(isView){
                        %>
                        <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                        <span id="svpdftor1">
                         &nbsp;&nbsp;
                         <a class="action-button-savepdf" id="saveASPDF" href="<%=request.getContextPath()%>/TorRptServlet?tenderId=<%=tenderid%>&lotId=<%=lotId%>&action=TER4">Save As PDF</a>
                         </span>
                        <%}
                        if(showGoBack){
                        if(isCommCP){
                    %>
                    <a id="goBack" class="action-button-goback" href="Evalclarify.jsp?tenderId=<%=tenderid%>&st=rp&comType=TEC">Go Back to Dashboard</a>
                    <%}else{%>
                    <a id="goBack" class="action-button-goback" href="MemberTERReport.jsp?tenderId=<%=tenderid%>&comType=TEC">Go Back to Dashboard</a>
                    <%}}}%>
                </span></div>
            <%
                    pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                    pageContext.setAttribute("tab", "6");
                        pageContext.setAttribute("isPDF", request.getParameter("isPDF"));
                        pageContext.setAttribute("userId", 0);
                        /*if(!bole_ter_flag){
                            pageContext.setAttribute("isPDF","true");
                        }*/
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <!--%@include  file="officerTabPanel.jsp"%-->
            <div class="tableHead_1 t_space t-align-center">Final Evaluation Report</div>
            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 b_space" width="100%">
                <tr>
                    <td width="24%" class="ff">Ministry Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName1());
                                }%></td>
                    <td width="16%" class="ff">Division Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName2());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff">Organization/Agency Name :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName3());
                                }%></td>
                    <td class="ff">Procuring Entity :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName4());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff"><%=repLabel%> Package No. and Description :</td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName5()+" & "+tendOpeningRpt.get(0).getFieldName6());
                                }%></td>
                </tr>
                <%if(!pNature.equals("Services")){%>
                <tr>
                    <td class="ff">Lot No. and Description : </td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName12());
                                    if(!tendOpeningRpt.get(0).getFieldName12().equals("")){
                                        out.print(" & ");
                                    }
                                    out.print(tendOpeningRpt.get(0).getFieldName13());
                                }%></td>
                </tr>
                <%}%>
            </table>
            <div class="inner-tableHead t_space">Procurement Data</div>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>

                        <th width="47%">Procurement Type</th>
                        <th width="53%">Procurement Method</th>
                    </tr>
                    <tr>
                        <td class="t-align-center" ><%if (list1) {
                                   if (tendOpeningRpt.get(0).getFieldName7().equalsIgnoreCase("NCT")) //Edit by aprojit
                                       out.print("NCB");
                                   else if (tendOpeningRpt.get(0).getFieldName7().equalsIgnoreCase("ICT"))
                                       out.print("ICB");
                                }%></td>
                       <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName9());
                                }%></td>
                    </tr>
                </tbody></table>
            <div class="inner-tableHead t_space">Procurement Plan</div>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>
                        <th width="17%">Approval<br>
                            Status</th>
                        <th width="31%">Budget Type</th>
                        <%if(!pNature.equalsIgnoreCase("Services")){%>
                        <th width="14%">Approval Status
                            of<br>
                            Official Estimates</th>
                        <%}%>
                    </tr>
                    <tr>
                        <td class="t-align-center">Approved</td>
                        <td class="t-align-center"><%if (list1) {
                                   if(tendOpeningRpt.get(0).getFieldName8().equalsIgnoreCase("Development")) //Edit by aprojit
                                       out.print("Capital");
                                   else if(tendOpeningRpt.get(0).getFieldName8().equalsIgnoreCase("Revenue"))
                                       out.print("Recurrent");
                                   else
                                       out.print(tendOpeningRpt.get(0).getFieldName8());
                                }%></td>
                        <%if(!pNature.equalsIgnoreCase("Services")){%>
                       <td class="t-align-center"><%=estCostStatus%></td>
                       <%}%>
                    </tr>
                </tbody></table>
            <table class="tableList_3 t_space" cellspacing="0" width="100%">
                <tbody><tr>
                        <th class="table-fileName">Name of Bidder/Consultant
                            Recommended for Award</th>

                    </tr>
                    <%for(SPCommonSearchDataMore bidder : TendererAwarded){%>
                    <tr>
                        <td style="text-align: left;"><%=bidder.getFieldName1()%></td>
                    </tr>
                    <%}%>
                </tbody></table>
            <table class="tableList_3 t_space" cellspacing="0" width="100%">
                <tbody><tr>
                      <%if(isNeg){%>
                        <th width="50%">Negotiation Report</th>
                      <%}if(isPQ){%>
                        <th width="50%">Post Qualification Report</th>
                      <%}%>
                        <th width="50%">Clarification on <%=repLabel%>s</th>
                    </tr>
                    <tr>
                       <%if(bole_ter_flag){%>
                      <%if(isNeg){%>
                      <td class="t-align-center"><a href="NegotiationReport.jsp?tenderid=<%=tenderid%>&lotId=<%=lotId%>&rId=<%=roundId%>&ReportFrom=TER4" target="_blank" <%if(!isTER3Signed){out.print(" onclick='return signCheck();'");}%>>View</a></td>
                      <%}if(isPQ){%>
                        <td class="t-align-center"><a href="ViewPQReport.jsp?tenderId=<%=tenderid%>&pkgLotId=<%=lotId%>&ter=y&evalCount=<%=evalCount%>" target="_blank" <%if(!isTER3Signed){out.print(" onclick='return signCheck();'");}%>>View</a></td>
                       <%}}else{out.print("<td class='t-align-center'>-</td>");}%>
                        <td class="t-align-center">
                            <%if(dataMoreService.geteGPData("getAllBidderForClariWithCP", tenderid, lotId,String.valueOf(evalCount)).isEmpty()){
                                out.print("Clarification on Tender has not taken place");
                            }else{%>
                            <a href="AllQeryResp.jsp?tenderid=<%=tenderid%>&ter=y&evalCount=<%=evalCount%>" target="_blank">View</a>
                            <%}%>
                        </td>

                    </tr>
                </tbody></table>

         <!-- Edit By Palash, Dohatec -->

    <%
       String MsgResult = "NO";
       for(SPCommonSearchDataMore Recommendetion : TenderRecommendetion){
           if (Recommendetion.getFieldName1() != null){
           MsgResult = Recommendetion.getFieldName1().toString();}
          
        }

       if(MsgResult.equalsIgnoreCase("") || MsgResult.equalsIgnoreCase("NULL")|| MsgResult.equalsIgnoreCase("NO")) {
          
          for (SPCommonSearchDataMore dataMore : TECMembers) {
                        // out.print("<td>");
                        if(dataMore.getFieldName5().equals(userId)){
                           if(dataMore.getFieldName6().equals("-")){
                                System.out.println("Test:  " + dataMore.getFieldName2());
                                if (dataMore.getFieldName2().equals("cp")) {
          %>

                                <table id="cpRecRe-Tender" class="tableList_3 t_space" cellspacing="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <th width="50%">Recommended Re-Tendering</th>
                                            <th width="50%">Select Yes or No</th>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left ff">Recommended Re-Tendering?: </td>
                                            <td class="t-align-left">
                                                <select name="cmbReqReTender" class="formTxtBox_1" id="cmbReqReTender" style="width:50px;">
                                                   <option value="No">No</option>
                                                   <option value="Yes">Yes</option>
                                                </select>
                                            </td>
                                        </tr>
                                     </tbody>
                                 </table>

                             <%
                             }

                           }
                         }                      
             }
        }
        else {  %>
                <table id="cpRecRe-TenderMsg" class="tableList_3 t_space" cellspacing="0" width="100%">
                                    <tbody>
                                        <tr>
                                            <th width="100%">Recommended Re-Tendering by Tender Evaluation Committee Chairperson</th>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left ff">Recommended Re-Tendering : <%=MsgResult%> </td>
                                        </tr>
                                     </tbody>
                                 </table>
          <% }
    %>

       <%
          
         // else {
       %>
      <!--  -->

      <!-- End Palash, Dohatec -->
                <%if(NoteofDissent!=null && (!NoteofDissent.isEmpty())){%>
                <div class="inner-tableHead t_space">Note of Dissent:</div>
                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <th><%=repLabel.charAt(0)%>EC Member</th>
                        <th>Note of Dissent</th>
                    </tr>
                    <%for(SPCommonSearchDataMore nod : NoteofDissent){%>
                    <tr>
                        <td><%=nod.getFieldName1()%></td>
                        <td><%=nod.getFieldName2()%></td>
                    </tr>
                    <%}%>
                </table>
                <%}%>
            <div class="inner-tableHead t_space"><%=repLabel.charAt(0)%>EC Members</div>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <td class="tableHead_1" colspan="5" ><%=repLabel.charAt(0)%>EC Members</td>
                </tr>
                <tr>
                    <th width="20%" >Name</th>
                    <%
                    for (SPCommonSearchDataMore dataMore : TECMembers) {
                        out.print("<td>");
                        if(dataMore.getFieldName5().equals(userId)){
                         if(dataMore.getFieldName6().equals("-")){
                             String valChk="";
                             if(!isCommCP){
                                valChk = "&nDis=y";
                             }
                   %>
                   <%
                    %>

                 <!-- Edit By Palash, Dohatec -->
                    <!-- <a href="TORSigningApp.jsp?id=<%=dataMore.getFieldName7()%>&uid=<%=dataMore.getFieldName5()%>&tid=<%=tenderid%>&lotId=<%=lotId%>&stat=<%=stat%>&rpt=ter4<%=valChk%>&rId=<%=roundId%>&rec="document.getElementById('#cmbReqReTender')<%if(!isTER3Signed){out.print(" onclick='return signCheck();'");} else{out.print(" onclick='return reqRetender();'");} %>>
                            <%=dataMore.getFieldName1()%>
                        </a> -->
                       <!-- <a href="TORSigningApp.jsp?id=<%=dataMore.getFieldName7()%>&uid=<%=dataMore.getFieldName5()%>&tid=<%=tenderid%>&lotId=<%=lotId%>&stat=<%=stat%>&rpt=ter4<%=valChk%>&rId=<%=roundId%>&rec='" $('#cmbReqReTender').val()+ <%if(!isTER3Signed){out.print(" onclick='return signCheck();'");}%>>
                            <%=dataMore.getFieldName1()%>
                        </a> -->

                            <div id="RecommendedReTendering">

                            </div>
                 <script type="text/javascript">                    


                     function RecommReTender() {
                          var recommendedValue = $('#cmbReqReTender').val();
                          $('#RecommendedReTendering').empty();                          
                          $('#RecommendedReTendering').append('<a href="TORSigningApp.jsp?id=<%=dataMore.getFieldName7()%>&uid=<%=dataMore.getFieldName5()%>&tid=<%=tenderid%>&lotId=<%=lotId%>&stat=<%=stat%>&rpt=ter4<%=valChk%>&rId=<%=roundId%>&rec='+recommendedValue+'" <% if(!isTER3Signed){ %> onclick="return signCheck();" <% } %>><%=dataMore.getFieldName1()%></a>');
                     }
                     
                     RecommReTender();

                     $( "#cmbReqReTender" ).change(function() {
                           RecommReTender();
                      });
                    
                 </script>
                  <!-- End Palash, Dohatec -->
                   <%
                        }else{
                             out.print(dataMore.getFieldName1());
                        }

                    }else{
                        out.print(dataMore.getFieldName1());
                    }
                        out.print("</td>");
                    }
                  %>
               </tr>
                <tr>
                    <th width="20%">Committee Role</th>
                    <%
                    int svpdfter4 = 0;
                    for (SPCommonSearchDataMore dataMore : TECMembers) {
                        out.print("<td>");
                        if (dataMore.getFieldName2().equals("cp")) {
                            out.print("Chairperson");
                        } else if (dataMore.getFieldName2().equals("ms")) {
                            out.print("Member Secretary");
                        } else if (dataMore.getFieldName2().equals("m")) {
                            out.print("Member");
                        }
                        out.print("</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Designation</th>
                    <%for (SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName3()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">PE Office</th>
                    <%for (SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName4()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Signed <%=repLabel%> Evaluation Report 4 On</th>
                    <%for(SPCommonSearchDataMore dataMore : TECMembers) {if(dataMore.getFieldName6().equals("-")){svpdfter4++;}out.print("<td>"+dataMore.getFieldName6()+"</td>");}%>
                </tr>
                 <tr>
                    <th width="20%">Comments</th>
                    <%for(SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName8()+"</td>");}%>
                </tr>
            </table>
             <%if(svpdfter4!=0){%>
                <script type="text/javascript">
                    $('#svpdftor1').hide();
                </script>
            <%}%>
        </div>
        </div>
        <%if(bole_ter_flag){%>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <%}%>
    </body>
    <%
    %>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <script type="text/javascript">
    $(document).ready(function() {

        $("#print").click(function() {
            //alert('sa');
            printElem({ leaveOpen: true, printMode: 'popup' });
        });

    });
    function printElem(options){
        //alert(options);
        $('#print').hide();
        $('#saveASPDF').hide();
        $('#goBack').hide();
        $('#print_area').printElement(options);
        $('#print').show();
        $('#saveASPDF').show();
        $('#goBack').show();
        //$('#trLast').hide();
    }
    </script>
</html>
