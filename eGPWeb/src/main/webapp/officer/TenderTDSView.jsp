<%--
    Document   : Add NewClause
    Created on : 24-Oct-2010, 3:13:11 PM
    Author     : yanki
--%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBeanTender" />
<jsp:useBean id="prepareTDSSrBean" class="com.cptu.egp.eps.web.servicebean.PrepareTenderTDSSrBean" />
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />

<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster" %>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderIttClause" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderIttSubClause" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderIttHeader" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <%
                int tenderId = 0;
                if (request.getParameter("tenderId") != null) {
                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                }
                String isPDF = "abc";
                if (request.getParameter("isPDF") != null) {
                    isPDF = request.getParameter("isPDF");
                }
                TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                String stdTemplateId = tenderDocumentSrBean.findStdTemplateId(String.valueOf(tenderId));
                String procType = "";

                List<TblTemplateMaster> templateMasterLst = defineSTDInDtlSrBean.getTemplateMaster(Short.parseShort(stdTemplateId));
                if (templateMasterLst != null) {
                    if (templateMasterLst.size() > 0) {
                        procType = templateMasterLst.get(0).getProcType();
                    }
                }

                int sectionId = Integer.parseInt(request.getParameter("sectionId"));

                List<TblTenderIttHeader> headerInfo = createSubSectionSrBean.getSubSectionDetail(sectionId);

                String contentType = createSubSectionSrBean.getContectType(sectionId);

                if ("ITT".equals(contentType)) {  contentType="ITB";}
                else if("TDS".equals(contentType)){contentType="BDS";}
                else if("PCC".equals(contentType)){contentType="SCC";}
                String contentType1 = "";
                String instruction = "";
                String clauseInstr = "";
                if (contentType.equalsIgnoreCase("ITT")) {
                    contentType1 = "BDS";
                    instruction = "Instructions for completing Tender/Proposal Data Sheet are provided in italics in parenthesis for the relevant ITB clauses";
                    clauseInstr = "Amendments of, and Supplements to, Clauses in the Instructions to Tenderers/Consultants";
                } else if (contentType.equalsIgnoreCase("ITC")) {
                    contentType1 = "PDS";
                    instruction = "Instructions for completing Proposal Data Sheet are provided in italics in parenthesis for the relevant ITC clauses";
                    clauseInstr = "Amendments of, and Supplements to, Clauses in the Instructions to Tenderers/Consultants";
                } else if (contentType.equalsIgnoreCase("GCC")) {
                    contentType1 = "SCC";
                    instruction = "Instructions for completing the Particular Conditions of Contract are provided in italics in parenthesis for the relevant GCC Clauses.";
                    clauseInstr = "Amendments of, and Supplements to, Clauses in the General Conditions of Contract";
                }

                int pkgOrLotId = -1;
                if (request.getParameter("porlId") != null) {
                    pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                }
                int tenderStdId = Integer.parseInt(request.getParameter("tenderStdId"));

    %>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View <%=contentType1%></title>

        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/common.js"></script>
        <script>            
            function printPage()
            {
                document.getElementById("TDSView").submit();
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <% }%>
            <div class="fixDiv">

                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <div class="t_space">
                                <div class="pageHead_1">View <%=contentType1%>
                                    <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
                                    <%if (pkgOrLotId == -1) {%>
                                    <span class="c-alignment-right"><a href="TenderDocPrep.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>" class="action-button-goback">Go Back to Tender Document</a></span>
                                    <%} else {%>
                                    <span class="c-alignment-right"><a href="TenderDocPrep.jsp?tenderId=<%= tenderId%>&porlId=<%= pkgOrLotId%>" title="Tender/Proposal Document" class="action-button-goback">Go Back to Tender Document</a></span>
                                    <%}%>
                                    <%}%>
                                </div>
                            </div>
                            <% pageContext.setAttribute("userId", "0");%>
                            <% pageContext.setAttribute("isPDF", isPDF);%>
                            <%   pageContext.setAttribute("tenderId", tenderId);%>
                            <%@include file="../resources/common/TenderInfoBar.jsp" %>
                            <form action="<%=request.getContextPath()%>/PDFServlet" id="TDSView" method="post">
                                <%--<input type="text" name="reportPage"  value="<%=request.getContextPath()%>/admin/TDSView.jsp?<%=request.getQueryString()%>" />--%>
                                <input type="hidden" name="reportName"  value="<%=contentType1%> View" />
                                <table width="100%" cellspacing="0" id="tdsInfo" class="tableList_1 t_space">
                                    <tr>
                                        <td colspan="2"><%=instruction%></td>
                                    </tr>
                                    <tr>
                                        <td><b><%=contentType%> Clause</b></td>
                                        <td><%=clauseInstr%></td>
                                    </tr>
                                    <%
                                                int k = 1, i = -1;
                                                for (int k1 = 0; k1 < headerInfo.size(); k1++) {
                                                    int ittHeaderId = headerInfo.get(k1).getTenderIttHeaderId();


                                                    List<SPTenderCommonData> tblTdsSubClause = prepareTDSSrBean.getTDSSubClause(ittHeaderId);

                                                    //tblTdsSubClause
                                                    String ittClauseId = "-1";

                                                    /*
                                                    ittHeaderId, ittClauseId, ittClauseName,
                                                    ittsubclauseid, ittSubClauseName, tdsSubClauseName,
                                                    tdsSubClauseId, ittHeaderName, tdsSubClauseId,
                                                    orderNo
                                                     */


                                                    if (tblTdsSubClause.size() > 0) {
                                    %>
                                    <tr>
                                        <td class="t-align-center" colspan="2"><b><%=tblTdsSubClause.get(0).getFieldName8()%></b></td>
                                    </tr>
                                    <%
                                                                                        }
                                                                                        for (i = 0; i < tblTdsSubClause.size(); i++) {
                                    %>
                                    <tr id="tr_<%=k++%>">
                                        <%
                                                                                                                            if (!ittClauseId.equals(tblTdsSubClause.get(i).getFieldName2())) {
                                                                                                                                ittClauseId = tblTdsSubClause.get(i).getFieldName2();
                                        %>
                                        <td style="line-height: 1.75"><%=contentType%> Clause</td>
                                        <td style="line-height: 1.75"><%=tblTdsSubClause.get(i).getFieldName3()%></td>

                                        <%
                                                                                                                            }
                                        %>
                                    </tr>
                                    <tr>
                                        <td style="line-height: 1.75">&nbsp;</td>
                                        <td style="line-height: 1.75"><%if (tblTdsSubClause.get(i).getFieldName5() != null) {
                                                                                                                                out.print(tblTdsSubClause.get(i).getFieldName5());
                                                                                                                            } else {
                                                                                                                                out.print("<i>Information Not Available</i>");
                                                                                                                            }%></td>
                                    </tr>
                                    <tr>
                                        <td style="line-height: 1.75">&nbsp;</td>
                                        <td style="line-height: 1.75"><%if (tblTdsSubClause.get(i).getFieldName6() != null) {
                                                                                                                                out.print(tblTdsSubClause.get(i).getFieldName6());
                                                                                                                            } else {
                                                                                                                                out.print("<i>Information Not Available</i>");
                                                                                                                            }%></td>
                                    </tr>
                                    <%
                                                    }
                                                }
                                    %>
                                    <!--                                        <tr><td class="t-align-center" colspan="2">
                                                                                    <a class="action-button-savepdf" onclick="printPage();">Save As PDF</a></td></tr>-->
                                </table>
                                <input type="hidden" id="total" name ="total" value="<%=i%>" />
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Page Content End-->
                <!--Middle Content Table End-->
                <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <% }%>
            </div>
        </div>
        <script>
            var headSel_Obj = document.getElementById("headTabSTD");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
