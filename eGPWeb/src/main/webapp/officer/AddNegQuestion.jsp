<%--
    Document   : NegotiationConfig
    Created on : Dec 23, 2010, 3:22:15 PM
    Author     : Rikin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<jsp:useBean id="negotiationdtbean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Negotiation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">

            // Validate Negotiation Questions 
            $(document).ready(function() {
                $("#idfrmnegQuestion").validate({
                    rules: {
                        txtQuestions: { required: true,maxlength: 1000}
                    },
                    messages: {
                        txtQuestions: { required: "<div class='reqF_1'>Please Enter Question</div>",
                            maxlength: "<div class='reqF_1'>Maximum 1000 characters are allowed</div>"}
                    }
                }
            );
            });


        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!-- include After Login Top Panel -->
                <%@include file="../resources/common/AfterLoginTop.jsp"%>
                <%
                            int userId = 0;
                            byte suserTypeIdAddNeg = 0;
                            int tenderId = 0;
                            int negId = 0;
                            if (session.getAttribute("userTypeId") != null) {
                                suserTypeIdAddNeg = Byte.parseByte(session.getAttribute("userTypeId").toString());
                            }
                            if (session.getAttribute("userId") != null) {
                                userId = Integer.parseInt(session.getAttribute("userId").toString());
                            }
                            if (request.getParameter("tenderId") != null) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }
                            if (request.getParameter("negId") != null) {
                                negId = Integer.parseInt(request.getParameter("negId"));
                            }
                            pageContext.setAttribute("tenderId", tenderId);
                %>
                <div class="dashboard_div">
                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->

                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1">
                            Negotiation
                            <span class="c-alignment-right">
                                <a href="ViewNegQuestions.jsp?tenderId=<%=tenderId%>&negId=<%=negId%>" class="action-button-goback" title="Go Back To Dashboard">Go Back To Dashboard</a>
                            </span>
                        </div>
                         <!-- include Tender information bar -->
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        
                         <%pageContext.setAttribute("tab", "7");%>
            <br/>
            <%@include file="/officer/officerTabPanel.jsp" %>
             <div class="tabPanelArea_1">
             <%-- Start: Common Evaluation Table --%>
                      <%@include file="/officer/EvalCommCommon.jsp" %>
                    <%-- End: Common Evaluation Table --%>
                <div class="t_space">
                    <%  pageContext.setAttribute("TSCtab", "7");%>
                        <%@include file="../resources/common/AfterLoginTSC.jsp" %>
                        </div>     
                        
                        <div class="tabPanelArea_1">
                            <!-- Display Negotiation add Question form -->
                            <form name="frmnegQuestion" id="idfrmnegQuestion" action="<%=request.getContextPath()%>/NegotiationSrBean" method="post">
                                <input type="hidden" name="action" id="idaction" value="createQuestion" />
                                <input type="hidden" name="tenderIdName" id="idtenderIdName" value="<%=tenderId%>" />
                                <input type="hidden" name="negIdName" id="idnegIdName" value="<%=negId%>" />
                                <div style="font-style: italic" class="ff" align="left">
                                    Fields marked with (<span class="mandatory">*</span>) are mandatory.
                                </div>
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <!-- Add field for Question field -->
                                    <tr>
                                        <td width="10%" class="t-align-left ff">Question : <span class="mandatory">*</span></td>
                                        <td width="90%" class="t-align-left">
                                            <label>
                                                <textarea name="txtQuestions" rows="7" class="formTxtBox_1" id="txtQuestions" style="width:675px;"></textarea>
                                                <span id="SPReply" class="reqF_1"></span>
                                            </label>
                                        </td>
                                    </tr>
                                    <!-- Add field for Uploading Document -->
                                    <tr>
                                        <td width="10%" class="t-align-left ff">Document :</td>
                                        <td width="90%" class="t-align-left">
                                            <a href="javascript:void(0)" onclick="window.open('NegQueryDocs.jsp?tenderId=<%=tenderId%>&negId=<%=negId%>','window','width=900,height=500');">Upload</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <!-- Display Document files fetch by ajax call -->
                                        <td>&nbsp;</td>
                                        <td>
                                            <div id="queryData"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="t-align-center t_space">
                                                <label class="formBtn_1">
                                                    <input name="btnSubmit" type="submit" value="Submit" />
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                        <!-- include Bottom Panel -->
                        <%@include file="../resources/common/Bottom.jsp" %>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<form id="form2" method="post">
</form>
<script language="javascript" type="text/javascript">
    // code to get Document list after uploading
    function getDocData(){
        $.post("<%=request.getContextPath()%>/NegotiationSrBean", {tenderId:<%=tenderId%>,negIdName:<%=negId%>,docAction:'NegQryDocs',action:'negDocs'}, function(j){
            document.getElementById("queryData").innerHTML = j;
        });
        document.getElementById("myquery").className ="" ;
        document.getElementById("allquery").className="sMenu";
    }
    // code to delete the uploaded Document file 
    function deleteFile(docName,docId,tender,negId,negQueryId){
        $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
            if(r == true){
                $.post("<%=request.getContextPath()%>/ServletNegQueryDocs", {docName:docName,docId:docId,tenderId:tender,negId:negId,negQueryId:negQueryId,funName:'remove'}, function(j){
                    jAlert("Document deleted successfully."," Delete Document ", function(RetVal) {
                        getDocData();
                    });
                });
            }else{
                getDocData();
            }
        });
    }
    // code to download the file.
    function downloadFile(docName,docSize,tender,negId,negQueryId){
        document.getElementById("form2").action= "<%=request.getContextPath()%>/ServletNegQueryDocs?docName="+docName+"&negId="+negId+"&docSize="+docSize+"&negQueryId="+negQueryId+"&tenderId="+tender+"&funName=download";
        document.getElementById("form2").submit();
    }
    getDocData();
</script>