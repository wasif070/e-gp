<%-- 
    Document   : ViewNOAWG
    Created on : Jan 10, 2011, 5:19:57 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.model.table.TblNoaAcceptance"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>

<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Letter of Acceptance (LOA)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>

        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>

        <%--<script type="text/javascript">
            $(document).ready(function() {
                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });
            });
            function printElem(options){
                $('#saveASPDFBtn').hide();
                $('#print').hide();
                $('#print_area').printElement(options);
                $('#saveASPDFBtn').show();
                $('#print').show();
            }
        </script>--%>

    </head>
    <body>
        <%
                    boolean bCommon = true;
                    if (request.getParameter("action") != null && "common".equals(request.getParameter("action"))) {
                        bCommon = false;
                    }
                    String userid = "";
                    //swati--to generate pdf
                    String tenderId = "", NOAID = "";
                    if (request.getParameter("tenderid") != null) {
                        tenderId = request.getParameter("tenderid");
                    }
                    if (request.getParameter("NOAID") != null) {
                        NOAID = request.getParameter("NOAID");
                    }

                    String isPDF = "abc";
                    if (request.getParameter("isPDF") != null) {
                        isPDF = request.getParameter("isPDF");
                    }
                    String folderName = folderName = pdfConstant.NOA;
                    String genId = tenderId + "_" + NOAID;

                    if (isPDF.equalsIgnoreCase("true")) {
                        userid = request.getParameter("userid");
                    }
                    if (!(isPDF.equalsIgnoreCase("true"))) {
                        try {
                            String reqURL = request.getRequestURL().toString();
                            //tenderid=1518&NOAID=46&app=no
                            String reqQuery = "tenderid=" + tenderId + "&NOAID=" + NOAID + "&userid=" + userid;
                            pdfCmd.genrateCmd(reqURL, reqQuery, folderName, genId);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    //String userNameNOA = null;
                    //end

        %>

        <% if (!(isPDF.equalsIgnoreCase("true"))) {
                        if (bCommon) {
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>

        <%
                            //userNameNOA = objUserName.toString();
                        }
                    }%>
        <input type="hidden" name="hdextReqBy" id="hdextReqBy" value="87"/>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->

            <%
                        String noaIssueId = request.getParameter("NOAID");
                        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        List<SPCommonSearchDataMore> pckLotId = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getLotId", noaIssueId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        if (bCommon) {
                            HttpSession hs = request.getSession();
                            if (hs.getAttribute("userId") != null) {
                                userid = hs.getAttribute("userId").toString();
                            }
                        }
                        String bidderId = "";
                        String bidderNm = "";
                        String hopaName = "";
                        String hopaOffice = "";
                        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        List<SPTenderCommonData> awarddetails = tenderCommonService.returndata("GetAwardDetails", request.getParameter("tenderid"), request.getParameter("NOAID"));
                        List<SPCommonSearchDataMore> detailsOfTender = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getPEOfficeName", tenderId);
                        List<SPCommonSearchDataMore> detailsOfTender1 = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "getPEName", tenderId,noaIssueId);
                        List<SPCommonSearchDataMore> procurenature = commonSearchDataMoreService.geteGPData("GetDetailsForNOA", request.getParameter("tenderid"), pckLotId.get(0).getFieldName2());
                        List<SPTenderCommonData> awarddetails2 = tenderCommonService.returndata("GetAwardDetails2", request.getParameter("NOAID"), request.getParameter("tenderid"));
                        List<SPCommonSearchDataMore> tenderdetails = commonSearchDataMoreService.geteGPData("getTendererDetails", noaIssueId);
                        List<SPTenderCommonData> companyList = tenderCommonService.returndata("GetEvaluatedBidders", tenderId, pckLotId.get(0).getFieldName1());
                        
                        /*Nitish Start06/04/2017
                        Get Hopa Name of the Agency */
                        
                        TenderCommonService objTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        List<SPTenderCommonData> tenderinfo = objTenderCommonService.returndata("getTenderInfoAfterTCApprove", tenderId, null);
                        List<SPTenderCommonData> hopaInfo = objTenderCommonService.returndata("getHopaName", tenderId, null);
                        hopaName = hopaInfo.get(0).getFieldName1();
                        
                        List<SPTenderCommonData> hopaInfo2 = objTenderCommonService.returndata("getHopaOffice", tenderId, null);
                        hopaOffice = hopaInfo2.get(0).getFieldName1();
                        
                        //Nitish End
                        
                        String Procurementnature = "";
                        String status ="";
                        if (!procurenature.isEmpty()) {
                                Procurementnature = procurenature.get(0).getFieldName1();
                           }
                        if (!awarddetails2.isEmpty()) {
                                status = awarddetails2.get(0).getFieldName6();
                           }

                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                        String eveType = commonService.getEventType(tenderId).toString();
            %>
            <!--Dashboard Header End-->
            <div class="contentArea_1">
                <div  id="print_area">
                <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
                <div class="pageHead_1"><%if (request.getParameter("app") != null && request.getParameter("app").equalsIgnoreCase("yes")) {%>Accept / Decline Letter of Acceptance<% } else {%>Letter of Acceptance (LOA) of Tender/Proposal ID : <%=tenderId%><% }%>
                    <% if (bCommon) {%>
                    <span id="hdnLink" class="c-alignment-right">
                        <a class="action-button-savepdf" id="saveASPDFBtn" href="<%=request.getContextPath()%>/GeneratePdf?folderName=<%=folderName%>&id=<%=genId%>&tId=<%=tenderId%>&NOAID=<%=NOAID%>" >Save As PDF </a>&nbsp;
                        <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
                        <a href="NOA.jsp?tenderId=<%=request.getParameter("tenderid")%>" class="action-button-goback">Go Back To Dashboard</a>
                    </span>
                    <% }%>

                </div>
                <% }%>
                
                    <%if (request.getParameter("flag") != null && request.getParameter("flag").equals("false")) {%>
                    <div id="errordiv" class="responseMsg errorMsg">System facing problem..Please try again later.</div>
                    <%}%>
                    <form id="frmnoa" name="frmnoa" action="" method="post">
                        <table width="100%" border="0" cellpadding="0" cellspacing="5" class="t_space">
                        <%if(isPDF.equalsIgnoreCase("true")){%>
                        <tr>
                            <td>
                                <div style="font-size: 15px;font-weight: bold;">Letter of Acceptance (LOA) of Tender/Proposal ID : <%=tenderId%></div>
                            </td>
                        </tr>
                        <%}%>
                            <%
                                        if (!awarddetails.isEmpty()) {
                            %>
                            <tr>
                                <td width="40%" class="t-align-left ff">
                                    <%--Contract No:  <%=awarddetails.get(0).getFieldName1()%><label></label>--%>
                                </td>
                                <td width="60%" class="t-align-right ff">Date: <%=awarddetails.get(0).getFieldName3()%><label></label></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">To :<%//awarddetails.get(0).getFieldName6()%>

                                </td>
                                <td class="t-align-right ff">&nbsp;</td>
                            </tr>
                            <%                          
                                bidderId = tenderdetails.get(0).getFieldName2();
                                bidderNm = tenderdetails.get(0).getFieldName3();
                            %>
                            <tr>
                                <td class="t-align-left ff">Name :
                                    <%=commonService.getConsultantName(pckLotId.get(0).getFieldName2())%>
                                </td>
                                <td class="t-align-right ff"></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Address:  <%=tenderdetails.get(0).getFieldName1() %>
                                </td>
                                <td class="t-align-right ff"></td>
                            </tr>
                            <tr>
                                <%
                                                                           // List<SPTenderCommonData> companyList = tenderCommonService.returndata("GetEvaluatedBidders", tenderId, pckLotId.get(0).getFieldName1());
                                %>
                                <!--                            <td colspan="2" class="t-align-left ff">Organization name: <-%=awarddetails.get(0).getFieldName6()%>-->
                                <%--<td colspan="2" class="t-align-left ff"><br/><%=tenderdetails.get(0).getFieldName3()%> --%>
                                </td>
                            </tr>
                            <!--                        <tr>
                                                        <td colspan="2" class="t-align-left ff">PE office name : <-%=awarddetails.get(0).getFieldName5()%>
                                                        </td>
                                                    </tr>-->
                            <tr>
                                <td class="t-align-left ff">&nbsp;<label></label></td>
                                <td class="t-align-right ff">&nbsp;</td>
                            </tr>
                            <%
                         String Amount ="";
                         String totalAmount="";
                         String tAmount="";
                         String AmountPer ="";
                        // int j=awarddetails.size();
                         for (int i = 0; i <awarddetails.size() ; i++)
                             {
                         Amount=Amount+"   "+awarddetails.get(i).getFieldName4();

                             }
                         totalAmount = Amount.replace("Tk.", "Nu.");
                         tAmount = totalAmount.replace("Taka", "Ngultrum");
                                               
                         for (int j = 0; j <awarddetails2.size() ; j++)
                             {
                          AmountPer=AmountPer+ "   " +awarddetails2.get(j).getFieldName2();
                             }
                                         if("Services".equalsIgnoreCase(Procurementnature)){
                                     %>
                                     <tr><td colspan="2">
                                     <jsp:include page="../resources/common/NOAServiceLetter.jsp" >
                                        <jsp:param name="noaId" value="<%=noaIssueId%>" />
                                        <jsp:param name="dop" value="<%=procurenature.get(0).getFieldName2()%>" />
                                        <jsp:param name="brief" value="<%=tenderdetails.get(0).getFieldName4()%>" />
                                        <jsp:param name="amt" value="<%=Amount%>" />
                                        <jsp:param name="contractSignDay" value="<%=awarddetails2.get(0).getFieldName1()%>" />
                                        <jsp:param name="perSecAmt" value="<%=AmountPer%>" />
                                        <jsp:param name="perSecDays" value="<%=awarddetails2.get(0).getFieldName3()%>" />
                                        <jsp:param name="perSecDt" value="<%=awarddetails2.get(0).getFieldName10()%>" />
                                    </jsp:include>
                                         </td></tr>
                                     <%}else{
                                        if("RFQ".equalsIgnoreCase(eveType) || "RFQL".equalsIgnoreCase(eveType) || "RFQU".equalsIgnoreCase(eveType)){
                                     %>
                                     <tr><td colspan="2">
                                     <jsp:include page="../resources/common/NOAGoodsWorkLetter.jsp" >
                                        <jsp:param name="dop" value="<%=procurenature.get(0).getFieldName2()%>" />
                                        <jsp:param name="brief" value="<%=tenderdetails.get(0).getFieldName4()%>" />
                                        <jsp:param name="amt" value="<%=awarddetails.get(0).getFieldName4()%>" />
                                        <jsp:param name="contractSignDay" value="<%=awarddetails2.get(0).getFieldName1()%>" />
                                        <jsp:param name="perSecAmt" value="<%=awarddetails2.get(0).getFieldName2()%>" />
                                        <jsp:param name="perSecDays" value="<%=awarddetails2.get(0).getFieldName3()%>" />
                                        <jsp:param name="perSecDt" value="<%=awarddetails2.get(0).getFieldName5()%>" />
                                        <jsp:param name="nature" value="<%=Procurementnature%>" />
                                    </jsp:include>
                                             </td></tr>
                                     <%}else{
                                    if (!procurenature.isEmpty()) {
                            %>
                            <tr>
                                <td colspan="2" class="t-align-left">
      	This is to notify you that your Bid dated  <em><strong><%=procurenature.get(0).getFieldName2()%></strong></em>
                                    <%
                                            if (Procurementnature.equalsIgnoreCase("Works")) {
                                    %>
                                for the execution of the <em><strong>Works</strong></em> for
                                <%     } else if (Procurementnature.equalsIgnoreCase("Goods")) {%>
                                <u>for the supply of <em><strong>Goods and related Services </em></strong> for</u>
                                <%     } else if (Procurementnature.equalsIgnoreCase("Services")) {%>
                                as per new format coming in e-LoI : <u>the following <em><strong> consulting services </em></strong> for</u>
                                <%     } %>
                                    <em><strong><%=awarddetails.get(0).getFieldName2()%></strong></em> for the Contract Price of the equivalent of <em><strong><%=tAmount%></strong></em> in BTN, as corrected and modified  in accordance with the Instructions to Bidders is hereby accepted by our Agency.<%-- <em><strong><%=awarddetails.get(0).getFieldName5()%></strong></em>.--%>
                                </td>
                            </tr>
                            <%}%>
                            <tr>
                                <td colspan="2" class="t-align-left">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="t-align-left">
      	The Contract in duplicate is attached hereto. You are hereby instructed to: </br> </br> </br>
                                </td>
                            </tr>
                            <%
                                        if (!awarddetails2.isEmpty()) {
                            %>
                            <tr>
                                <td colspan="2" class="t-align-left">
                                    <div class="listBox_emailFormatNOA">
                                        <ol>
                                            <%--<li> --%>
                	<%--accept in writing the Notification of Award within <input type="hidden" id="noaIssueDays" value="<%=awarddetails2.get(0).getFieldName1()%>"/><label id="noaIssueDaysWords"></label> <em><strong>(<%=awarddetails2.get(0).getFieldName1()%>)</strong></em> working days of its issuance pursuant to ITB Clause --%>
                        (a)	confirm your acceptance of this Letter of Acceptance by signing and dating both copies of it, and returning one copy to us no later than <em><strong>(<%=awarddetails2.get(0).getFieldName1()%>)</strong></em> days from the date hereof; and </br> </br>
                                            <%--</li>--%>
                                            <%
                                                                                        //List<Object[]> obj = tenderSrBean.getConfiForTender(Integer.parseInt(tenderId));
                                                                                        if (!pckLotId.isEmpty() && !"".equalsIgnoreCase(pckLotId.get(0).getFieldName3()) && !"ZERO".equalsIgnoreCase(pckLotId.get(0).getFieldName3())) {
                                            %>
                                            <br/>
                                            <%--<li>
                	furnish a Performance Security in the specified format  and in the amount of <em><strong><%=AmountPer%></strong></em>, within <input type="hidden" id="perSecDays" value="<%=awarddetails2.get(0).getFieldName3()%>"/><label id="perSecDaysWords"></label><em><strong> (<%=awarddetails2.get(0).getFieldName3()%>)</strong></em> days of acceptance of this Notification of Award but not later than <em><strong><u><%=awarddetails2.get(0).getFieldName4()%></u></strong></em>, in accordance with ITB Clause
                        --%>(b)	forward the Performance Security pursuant to ITB Sub-Clause 47.1, i.e., within <em><strong>(<%=awarddetails2.get(0).getFieldName1()%>)</strong></em> days after receipt of this Letter of Acceptance, and pursuant to GCC Sub-Clause 19.1 
                                            <%--</li>--%>
                                            <br/>
                                            <% }%>
                                            <%--<li>
                	sign the Contract within <input type="hidden" id="contractSignDays" value="<%=awarddetails2.get(0).getFieldName9()%>"/><label id="contractSignDaysWords"></label> <em><strong>(<%=awarddetails2.get(0).getFieldName9()%>) </strong></em>days of issuance of this Notification of Award but not later than <em><strong><%=awarddetails2.get(0).getFieldName5()%></strong></em>, in accordance with ITB Clause
                                            </li> --%>
                                        </ol>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="t-align-left">
      	<%--You may proceed with the execution
                                    <%
                                            if (!procurenature.isEmpty()) {
                                               if (Procurementnature.equalsIgnoreCase("Works")) {
                                    %>
                                   <u>for the execution of the Works for</u>
                                <%     } else if (Procurementnature.equalsIgnoreCase("Goods")) {%>
                                   <u>for the supply of Goods and related Services for</u>
                                <%     } else if (Procurementnature.equalsIgnoreCase("Services")) {%>
                                <u>as per new format coming in e-LoI : the following consulting services for</u>
                                <%     }
                                                                                }%>
                                    only upon completion of the above tasks. You may also please note that this Notification of Award shall constitute the formation of this Contract, which shall become binding upon you.
        --%>
                                </td>
                            </tr>
                            <tr>
                                <td height="5px" colspan="2"></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="t-align-left">
      	<%--We attach the draft Contract and all other documents for your perusal and signature.--%>
                                </td>
                            </tr>
                            <%}}}%>
                            <tr>
                                <%--<td style="width: 83%;"></td>--%>
                                <td class="t-align-left" style="float: left;">
                                    Authorized Signature: 
                                    <br/>
                                    <br/>
                                    <%--for and on behalf of
                                    <br/>--%>
                                    <%--It will print Hopa Name--%>
                                    Name: <em><strong>[<%=hopaName%>]</em></strong>
                                    <br/>
                                    <br/>
                                    Title of Signatory:  HOPA
                                    <%//userNameNOA%>
                                    <br/>
                                    <br/>
                                    <%--<%=detailsOfTender1.get(0).getFieldName1() %>--%>
                                   <!--br/>
                                    < %=commonService.getConsultantName(pckLotId.get(0).getFieldName2())%-->
                                    Name of Agency:<em><strong>[
                                    <%=hopaOffice%>
                                    ]</em></strong>
                                </td>
                            </tr>
                            <tr>
                                <td height="5px" colspan="2"></td>
                            </tr>
                                                        <%}%>
                        </table>
                    </form>
                    <%
                        TblNoaAcceptance tna = tenderSrBean.getTblNoaAcceptance(Integer.parseInt(NOAID));
                        if(tna!=null && "approved".equalsIgnoreCase(tna.getAcceptRejStatus())){
                    %>
                    <div class="t-align-left t_space" style="background-color: #f6f6f6; padding: 5px;">
                    <table width="100%" cellpadding="0" cellspacing="5">
                        <tr>
                            <th colspan="4" align="center" class="ff">Account Information</th>
                        </tr>
                        <tr>
                            <td width="15%" class="ff">Title of the Account :  </td>
                            <td width="35%"><%=tna.getAccountTitle()!=null?tna.getAccountTitle():"-"%></td>
                            <td width="15%" class="ff">Name of Bank :  </td>
                            <td width="35%"><%=tna.getBankName()!=null?tna.getBankName():"-"%></td>
                        </tr>
                        <tr>
                            <td class="ff">Name of Branch :  </td>
                            <td><%=tna.getBranchName()!=null?tna.getBranchName():"-"%></td>
                            <td class="ff">Account Number :  </td>
                            <td><%=tna.getAccountNo()!=null?tna.getAccountNo():"-"%></td>
                        </tr>
                        <tr>
                            <td class="ff">Telephone :  </td>
                            <td><%=tna.getTelephone()!=null?tna.getTelephone():"-"%></td>
                            <td class="ff">Fax No :  </td>
                            <td><%=tna.getFax()!=null?tna.getFax():"-"%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch e-mail ID :  </td>
                            <td><%=tna.getEmail()!=null?tna.getEmail():"-"%></td>
                            <td class="ff">SWIFT Code :  </td>
                            <td><%=tna.getSwiftCode()!=null?tna.getSwiftCode():"-"%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Address : </td>
                            <td><%=tna.getAddress()!=null?tna.getAddress():"-"%></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    </div>
                    <%}%>
                    
                
                <% if (!(isPDF.equalsIgnoreCase("true"))) {
                    %>
                    

                <div class="t_space" >&nbsp;</div>
                <%
                List<SPCommonSearchDataMore> sPCommonSearchDataMores = commonSearchDataMoreService.geteGPData("NoaDocInfo", tenderId, pckLotId.get(0).getFieldName1(), pckLotId.get(0).getFieldName2(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                if(!sPCommonSearchDataMores.isEmpty()){
                %>
                <table>
                    <tr>
                        <td class="ff">
                            Attached: <br/><br/><%for(SPCommonSearchDataMore listingForDoc : sPCommonSearchDataMores){
                                            out.print(listingForDoc.getFieldName1());
                                            out.print("&nbsp;&nbsp;&nbsp;&nbsp;");
                                            out.print(listingForDoc.getFieldName2());
                                            out.print("<br/>");
                                         } %>
                        </td>
                    </tr>
                </table>
                <% } %>
                <% }%>
                
                
                <jsp:include page="../resources/common/NOADocInclude.jsp" >
                            <jsp:param name="comtenderId" value="<%=tenderId%>" />
                            <jsp:param name="comlotId" value="<%=pckLotId.get(0).getFieldName1()%>" />
                            <jsp:param name="comuserId" value="<%=bidderId%>" />
                        </jsp:include>
                
            </div>
                </div>
            <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
                            <%if (bCommon) {
            %>
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
            <% }
                        }%>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <script type="text/javascript">
             $(document).ready(function() {
                                $("#print").click(function() {
                                    $("a[view='link']").each(function(){
                                        var temp = $(this).html().toString();
                                            $(this).parent().append("<span class='disp_link'>-</span>");
                                            $(this).hide();
                                            //$('.reqF_1').remove();
                                    })
                                    $('#hdnLink').hide();
                                    printElem({ leaveOpen: true, printMode: 'popup' });
                                    $("a[view='link']").each(function(){
                                        var temp = $(this).html().toString();
                                            $('.disp_link').remove();
                                            //$('.reqF_1').remove();
                                            $(this).show();
                                    })
                                    $('#hdnLink').show();
                                });

                            });
                            function printElem(options){
                                $('#print_area').printElement(options);
                            }       

        </script>
    <script type="text/javascript">
        
        document.getElementById('noaIssueDaysWords').innerHTML =WORD(document.getElementById('noaIssueDays').value);
        document.getElementById('perSecDaysWords').innerHTML =WORD(document.getElementById('perSecDays').value);
        document.getElementById('contractSignDaysWords').innerHTML =WORD(document.getElementById('contractSignDays').value);
    </script>
</html>
<%
            tenderSrBean = null;
            pdfCmd = null;
            pdfConstant = null;
%>
