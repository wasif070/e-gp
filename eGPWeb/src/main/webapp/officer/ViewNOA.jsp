<%-- 
    Document   : ViewNOA
    Created on : Feb 6, 2011, 2:34:56 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Letter of Acceptance Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function(){
                $("#frmViewNOA").validate({

                    rules:{
                        txtComment:{required:true,maxlength:1000}
                    },
                    messages:{
                        txtComment:{required:"<div class='reqF_1'>Please enter Comments.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 1000 characters are allowed.</div>"
                        }
                    }

                });

            });
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <table width="100%" cellspacing="0">
                    <tr valign="top">
                        <td><div id="ddtopmenubar" class="topMenu_1">
                                <!--<div class="dash_menu_1">-->
                                <ul>
                                    <li><a href="#"><img src="../resources/images/Dashboard/msgBoxIcn.png" />Message Box</a></li>
                                    <li><a href="#" rel="ddsubmenu1"><img src="../resources/images/Dashboard/configIcn.png" />Configuration</a></li>
                                    <li><a href="#" rel="ddsubmenu2"><img src="../resources/images/Dashboard/tenderIcn.png" />SBD</a></li>
                                    <li><a href="#"><img src="../resources/images/Dashboard/docLibIcn.png" />Content</a></li>
                                    <li><a href="#" rel="ddsubmenu3"><img src="../resources/images/Dashboard/committeeIcn.png" />Manage Users</a></li>
                                    <li><a href="#"><img src="../resources/images/Dashboard/reportIcn.png" />Report</a></li>
                                    <li><a href="#"><img src="../resources/images/Dashboard/myAccountIcn.png" />Profile</a></li>
                                </ul>
                            </div>
                            <script type="text/javascript">
                                ddlevelsmenu.setup("ddtopmenubar") //ddlevelsmenu.setup("mainmenuid", "topbar|sidebar")
                            </script>
                            <!--Multilevel Menu 1 (rel="ddsubmenu1")-->
                            <ul id="ddsubmenu1" class="ddsubmenustyle">
                                <li><a href="#">Financial Year Details </a>
                                    <ul>
                                        <li><a href="#">Sub Menu 1</a></li>
                                        <li><a href="#">Sub Menu 2</a></li>
                                        <li><a href="#">Sub Menu 3</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Configure calendar</a></li>
                                <li><a href="#">Item 3</a></li>
                            </ul>


                            <!--Multilevel Menu 2 (rel="ddsubmenu2")-->
                            <ul id="ddsubmenu2" class="ddsubmenustyle">
                                <li><a href="#">Item 1</a>
                                    <ul>
                                        <li><a href="#">Sub Menu 1</a></li>
                                        <li><a href="#">Sub Menu 2</a></li>
                                        <li><a href="#">Sub Menu 3</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Item 2</a></li>
                                <li><a href="#">Item 3</a></li>
                            </ul>

                            <!--Multilevel Menu 3 (rel="ddsubmenu3")-->
                            <ul id="ddsubmenu3" class="ddsubmenustyle">
                                <li><a href="#">Menu 1</a></li>
                                <li><a href="#">Menu 2</a></li>
                                <li><a href="#">Menu 3</a></li>
                                <li><a href="#">Menu 4</a></li>
                            </ul>

                            <table width="100%" cellspacing="6" class="loginInfoBar">
                                <tr>
                                    <td align="left"><b>Friday 27/08/2010 21:45</b></td>
                                    <td align="center"><b>Last Login :</b> Friday 27/08/2010 21:45</td>
                                    <td align="right"><img src="../resources/images/Dashboard/userIcn.png" class="linkIcon_1" /><b>Welcome,</b> User   &nbsp;|&nbsp; <img src="../resources/images/Dashboard/logoutIcn.png" class="linkIcon_1" alt="Logout" /><a href="#" title="Logout">Logout</a></td>
                                </tr>
                            </table></td>
                        <td width="141"><img src="../resources/images/Dashboard/e-GP.gif" width="141" height="64" alt="e-GP" /></td>
                    </tr>
                </table>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <form id="frmViewNOA" method="post" action="">
                <div class="pageHead_1">
                    View Letter of Acceptance Details
                    <span class="c-alignment-right">
                        <a href="#" class="action-button-goback">Go Back To Dashboard</a>
                    </span>
                </div>

                <table width="100%" border="0" cellpadding="0" cellspacing="5" class="t_space">
                    <tr>
                        <td width="40%" class="t-align-left ff">
                            Contract No:  &nbsp;<label></label>
                        </td>
                        <td width="60%" class="t-align-right ff">Date: &nbsp;<label></label></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">To

                        </td>
                        <td class="t-align-right ff">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">&nbsp;<label></label></td>
                        <td class="t-align-right ff">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="t-align-left">
      	This is to notify you that your Tender/Proposal dated <em><strong>Date</strong></em>  for the supply of Goods and related Services  for <em><strong>Name of contract</strong></em> for the Contract Price of Tk <em><strong>state amount in figures and in words</strong></em> as corrected and modified in accordance with the Instructions to Tenderers/Consultants, has been approved by <em><strong>name of Purchaser</strong></em>.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="t-align-left">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="t-align-left">
      	You are thus requested to take following actions :
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="t-align-left">
                            <div class="listBox_emailFormatNOA">
                                <ol>
                                    <li>
                	Accept in writing the Letter of Acceptance within seven <em><strong>7</strong></em> working days of its issuance pursuant to ITB Sub-Clause 62.3
                                    </li>
                                    <li>
                	Furnish a Performance Security in the specified format  and in the amount of Tk <em><strong>state amount in figures and words</strong></em> ,within Twenty-eight <em><strong>28</strong></em> days from issue of this Letter of Acceptance but not later than <em><strong>specify date</strong></em>, in accordance with ITB Clause 64.2
                                    </li>
                                    <li>
                	Sign the Contract within twenty eight (28 ) days of issuance of this Letter of Acceptance but not later than <em><strong>specify date</strong></em>, in accordance with ITB Clause 67.2
                                    </li>
                                </ol>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="t-align-left">
      	You may proceed with the execution of the supply of Goods and related Services only upon completion of the above tasks. You may also please note that this Letter of Acceptance shall constitute the formation of this Contract, which shall become binding upon you.

                        </td>
                    </tr>
                    <tr>
                        <td height="5px" colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="t-align-left">
      	We attach the draft Contract and all other documents for your perusal and signature.
                        </td>
                    </tr>
                    <tr>
                        <td height="5px" colspan="2"></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="t-align-left ff">
                            <table width="100%" cellpadding="0" cellspacing="5">
                                <tr>
                                    <td width="8%" align="left" valign="top">Comment :</td>
                                    <td width="92%" align="left" valign="top">

                                        <textarea rows="5" id="txtComment" name="txtComment" class="formTxtBox_1" style="width:100%;"></textarea>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="t-align-center">
                            <label class="formBtn_1 t_space">
                                <input name="" type="submit" value="Submit" />
                            </label>
                        </td>
                    </tr>
                </table>

                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                    <tr>
                        <th width="26%" class="ff">File Name</th>
                        <th width="51%" class="ff">File  Description</th>
                        <th width="12%" class="ff">File  size in KB</th>
                        <th width="11%" class="ff">Action</th>
                    </tr>
                    <tr>
                        <td class="t-align-left">&nbsp;</td>
                        <td class="t-align-left">&nbsp;</td>
                        <td class="t-align-left">&nbsp;</td>
                        <td class="t-align-left">&nbsp;</td>
                    </tr>
                </table>

                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </form>
            <!--Dashboard Footer Start-->
            <table width="100%" cellspacing="0" class="footerCss t_space">
                <tr>
                    <td align="left">e-GP &copy; All Rights Reserved
                        <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
                    <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a></td>
                </tr>
            </table>
            <!--Dashboard Footer End-->
        </div>

    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
