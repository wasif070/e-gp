<%-- 
    Document   : NOADocUpload
    Created on : Dec 24, 2010, 7:22:12 PM
    Author     : Administrator
--%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    if (session.getAttribute("userId") == null) {
                        response.sendRedirect("SessionTimedOut.jsp");
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Letter of Acceptance (LOA) Ref Document</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <script type="text/javascript">
            /*For text validation*/
            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:100}
                    },
                    messages: {
                        uploadDocFile: { required: "<div class='reqF_1'>Please select Document</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please enter Description</div>",
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed</div>"}
                    }
                });
            });
            $(function() {
                $('#frmUploadDoc').submit(function() {
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                            browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#btnUpld').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
            function doUnload()
            {
                if(window.opener.document.getElementById("dataTable")){
                    var cntObj = document.getElementById("noaDocCnt");
                    var count = 0;
                    if(cntObj!=null){
                     count = parseInt(cntObj.value);
                    }
                    for(var i=1;i<=count;i++){
                        document.getElementById("anchorDelImg_"+i).style.display = "none";
                    }
                    window.opener.document.getElementById("dataTable").innerHTML = document.getElementById('listingDoc').innerHTML;
                }
            }
        </script>
    </head>
    <body onunload="doUnload();">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->


            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <%
                        String tenderid = "";

                        if (request.getParameter("tenderid") != null) {
                            tenderid = request.getParameter("tenderid");
                        }

                        String userid = "";
                        if (request.getParameter("userid") != null) {
                            userid = request.getParameter("userid");
                        }
                        String pckLotid = "";
                        if (request.getParameter("pckLotid") != null) {
                            pckLotid = request.getParameter("pckLotid");
                        }
                        String roundId = "";
                        if (request.getParameter("roundId") != null) {
                            roundId = request.getParameter("roundId");
                        }
            %>

            <div class="contentArea_1">
                <div class="pageHead_1">Upload Letter of Acceptance (LOA) Ref Document</div>
                <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/ServletNOADoc" enctype="multipart/form-data" name="frmUploadDoc">
                    <input type="hidden" name="tenderid" value="<%=tenderid%>"/>
                    <input type="hidden" name="userid" value="<%=userid%>"/>
                    <input type="hidden" name="pckLotid" value="<%=pckLotid%>"/>
                    <input type="hidden" name="roundId" value="<%=roundId%>"/>
                    <%
                                if (request.getParameter("fq") != null) {
                    %>

                    <div class="responseMsg errorMsg" style="margin-top: 10px;"><%=request.getParameter("fq")%></div>
                    <%
                                }
                                if (request.getParameter("fs") != null) {
                    %>

                    <div class="responseMsg errorMsg"  style="margin-top: 10px;">

                        Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                    </div>
                    <%
                                }
                    %>

                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>

                            <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                        </tr>
                        <tr>
                            <td width="15%" class="ff t-align-left">Document   : <span class="mandatory">*</span></td>
                            <td width="85%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="ff">Description : <span>*</span></td>
                            <td>
                                <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                <div id="dvDescpErMsg" class='reqF_1'></div>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>

                                <label class="formBtn_1"><input type="submit" name="btnUpld" id="btnUpld" value="Upload" /></label>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="100%"  class="t-align-left">Instructions</th>
                        </tr>
                        <tr>
                            <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("tenderer");%>
                            <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                        </tr>
                    </table>
                </form>
                        
                        <div id="listingDoc" class="t_space">
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th width="4%" class="t-align-center">Sl. No.</th>
                            <th class="t-align-center" width="23%">File Name</th>
                            <th class="t-align-center" width="32%">File Description</th>
                            <th class="t-align-center" width="7%">File Size <br />
                                (in KB)</th>
                            <th class="t-align-center" width="18%">Action</th>
                        </tr>
                        <%

                                    int docCnt = 0;
                                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                    /*For data listing*/
                                    List<SPCommonSearchDataMore> sPCommonSearchDataMores = commonSearchDataMoreService.geteGPData("NoaDocInfo", tenderid, pckLotid, userid, roundId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    for (SPCommonSearchDataMore sptcd : sPCommonSearchDataMores) {
                                        docCnt++;
                        %>
                        <tr>
                            <td class="t-align-center"><%=docCnt%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                            <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                            <td class="t-align-center">
                                <a href="<%=request.getContextPath()%>/ServletNOADoc?docName=<%=sptcd.getFieldName1()%>&roundId=<%=roundId %>&tenderid=<%=tenderid%>&userid=<%=userid%>&pckLotid=<%=pckLotid%>&docSize=<%=sptcd.getFieldName3()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                &nbsp;
                                <a href="<%=request.getContextPath()%>/ServletNOADoc?docName=<%=sptcd.getFieldName1()%>&roundId=<%=roundId %>&tenderid=<%=tenderid%>&userid=<%=userid%>&pckLotid=<%=pckLotid%>&docId=<%=sptcd.getFieldName4()%>&funName=remove" title="Remove" id="anchorDelImg_<%= docCnt %>"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                            </td>
                        </tr>

                        <%   if (sptcd != null) {
                                        sptcd = null;
                                    }
                                }%>
                        <% if (docCnt == 0) {%>
                        <tr>
                            <td colspan="5" class="t-align-center">No records found.</td>
                        </tr>
                        <%}%>
                        <input type="hidden" name="noaDocCnt" id="noaDocCnt" value="<%=docCnt%>" />
                    </table>
                </div>

                <div>&nbsp;</div>
            </div>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
<%
            if (tblConfigurationMaster != null) {
                tblConfigurationMaster = null;
            }
            if (commonSearchDataMoreService != null) {
                commonSearchDataMoreService = null;
            }

%>
