<%--
Document   : ProgressReport
Created on : Jul 25, 2011, 12:46:45 PM
Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsNewBankGuarnatee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsNewBankGuarnateeService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsContractTermination"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsContractTerminationBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsContractTerminationBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsContractTermination"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPayment"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<jsp:useBean id="cmsVendorReqWccSrBean" class="com.cptu.egp.eps.web.servicebean.CmsVendorReqWccSrBean" />
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Progress Report</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
<script src="../resources/js/common.js" type="text/javascript"></script>
<script>
function View(wpId,tenderId,lotId,cntId){
    dynamicFromSubmit("PreparePRreport.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
}
function ViewR(wpId,tenderId,lotId,cntId){
    dynamicFromSubmit("PreparePRreport.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&cntId="+cntId);
}
function Edit(wpId,tenderId,lotId,cntId){
    dynamicFromSubmit("PreparePRreport.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&isedit=y");
}
function EditR(wpId,tenderId,lotId,cntId){
    dynamicFromSubmit("PreparePRreport.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&isedit=y&cntId="+cntId);
}
function ViewPR(wpId,tenderId,lotId,cntId){
    dynamicFromSubmit("ViewProgressReport.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&cntId="+cntId);
}
</script>
</head>
<%
    String userId = "";
    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
        userId = session.getAttribute("userId").toString();
        issueNOASrBean.setLogUserId(userId);
    }
    ResourceBundle bdl = null;
    bdl = ResourceBundle.getBundle("properties.cmsproperty");
    CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
    TenderCommonService wfTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    CmsNewBankGuarnateeService cmsNewBankGuarnateeService = (CmsNewBankGuarnateeService) AppContext.getSpringBean("CmsNewBankGuarnateeService");

    List<SPTenderCommonData> checkuserRights = wfTenderCommonService.returndata("CheckTenderUserRights",userId,request.getParameter("tenderId"));
    boolean allowReleaseForfeit = false;
    CommonService commonServiceee = (CommonService) AppContext.getSpringBean("CommonService");
    String procnatureee = commonServiceee.getProcNature(request.getParameter("tenderId")).toString();
    String procType = commonServiceee.getProcurementType(request.getParameter("tenderId")).toString();
%>
<div class="dashboard_div">
<!--Dashboard Header Start-->
<%@include  file="../resources/common/AfterLoginTop.jsp"%>
<!--Dashboard Header End-->
<!--Dashboard Content Part Start-->
<div class="contentArea_1">
<div class="pageHead_1">Progress Report</div>
<% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

<%@include file="../resources/common/TenderInfoBar.jsp" %>
<%
    List<Object[]> lotObj = commonServiceee.getLotDetails(request.getParameter("tenderId"));
    Object[] objj = null;
    if(lotObj!=null && !lotObj.isEmpty())
    {
        objj = lotObj.get(0);
        pageContext.setAttribute("lotId", objj[0].toString());
    }
    if("services".equalsIgnoreCase(procnatureee)||"works".equalsIgnoreCase(procnatureee)){
%>
<%@include file="../resources/common/ContractInfoBar.jsp" %>
<%}%>
<div>&nbsp;</div>
<%
            pageContext.setAttribute("tab", "14");

%>
<%@include  file="officerTabPanel.jsp"%>
<div class="tabPanelArea_1">

    <%
                pageContext.setAttribute("TSCtab", "2");

    %>
    <%@include  file="../resources/common/CMSTab.jsp"%>
    <div class="tabPanelArea_1">
        <div align="center">

            <%
                        String tenderId = request.getParameter("tenderId");
                        boolean flag = true;
                        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        commonSearchDataMoreService.setLogUserId(session.getAttribute("userId").toString());
                        List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithContractId", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                        service.setLogUserId(session.getAttribute("userId").toString());
                        int i = 0;
                        boolean flags = false;
                        final int ZERO = 0; 
                        int lotIds=0;

                        CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                       ccds.setLogUserId(session.getAttribute("userId").toString());
                        for (SPCommonSearchDataMore lotList : packageLotList) {
                            flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotList.getFieldName5()));
                           // List<Object> wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));
                            /* Dohatec Start */
                            List<Object> wpid;
                            if("ICT".equalsIgnoreCase(procType) || tenderCommonService.getSBDCurrency(Integer.parseInt(request.getParameter("tenderId"))).equalsIgnoreCase("Yes"))
                            {
                                wpid = service.getWpIdForTenderForms(Integer.parseInt(lotList.getFieldName5()),tenderId);
                            }
                            else
                            {
                                wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));
                            }
                            /* Dohatec End */
                            boolean isWCC = service.isWorkCompleteOrNot(lotList.getFieldName5());
                            boolean isConTer = service.isContractTerminatedOrNot(lotList.getFieldName5());
                            System.out.println("aaaaaaa " + isConTer);
            %>
            <script type="text/javascript">
                document.getElementById('pckNo').innerHTML = '<%=lotList.getFieldName1()%>';
                document.getElementById('pckDesc').innerHTML = '<%=lotList.getFieldName2()%>';
            </script>

            <%--<%    if (request.getParameter("msg") != null) {
            if ("wci".equals(request.getParameter("msg"))) {%>

            <div  class='responseMsg successMsg t-align-left'>
                Work Completion Certificate successfully issued
            </div>
                            <%
                                        }
                                    }
                            %>--%>

            <form name="frmcons" method="post">




                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                    <tr>
                        <td width="20%"><%=bdl.getString("CMS.lotno")%></td>
                        <td width="80%"><%=lotList.getFieldName3()%></td>
                    </tr>
                    <tr>
                        <td><%=bdl.getString("CMS.lotdes")%></td>
                        <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <%
                                    String strlotId = "";
                                    int lotId = 0;
                                    lotIds = Integer.parseInt(lotList.getFieldName5());
                                    if (request.getParameter("lotId") != null) {
                                        lotId = Integer.parseInt(request.getParameter("lotId"));
                                        strlotId = request.getParameter("lotId");
                                    }else{
                                        strlotId = lotList.getFieldName5();
                                    }

                                    if (request.getParameter("msg") != null) {
                                        if ("wci".equals(request.getParameter("msg"))) {


                                                    if (lotId == Integer.parseInt(lotList.getFieldName5())) {
                                    %>
                                    <div  class='responseMsg noticeMsg t-align-left'>
                                        <%=bdl.getString("CMS.notice.msg")%>
                                    </div>
                                    <%
                                         }
                                        }
                                       }
                                       if (request.getParameter("success") != null) {
                                       if ("issue".equals(request.getParameter("success"))) {
                                    %>
                                        Work Completion Certificate successfully issued
                                    <%}else{%>
                                        File Uploaded Successfully
                                    <%}}%>
                        </td>
                    </tr>
                    <%
                                                            int countt = 1;
                                                            if (!wpid.isEmpty() && wpid != null) {%>
                    <tr><td colspan="2">
                            <span style="float: left; text-align: left;font-weight:bold;">
                                <a href="ViewTotalInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>" class="">Financial Progress Report</a>
                            </span>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                    <th width="17%" class="t-align-center"><%=bdl.getString("CMS.PR.report")%></th>
                                    <th width="5%" class="t-align-center"><%=bdl.getString("CMS.action")%></th>
                                </tr>
                                <%for (Object obj : wpid) {
                                                 String toDisplay = service.getConsolidate(obj.toString());
                                                 boolean isallowvariation = service.allowVariationOrderOrNot(Integer.parseInt(obj.toString()));
                                                 boolean checkitemreceived = service.checkForItemFullyReceivedOrNotByWpId(Integer.parseInt(obj.toString()));
                                %>


                                <tr>
                                    <td style="text-align: center;"><%=countt%></td>
                                    <td><%=toDisplay.replace("Consolidate", "Progress Report")%></td>
                                    <td>

                                        <%
                                            boolean flaB = service.checkForPrEntry(Integer.parseInt(obj.toString()));
                                            boolean forWorksdateEdit = service.isDateEditedInWorksTenSide(obj.toString(), "sendtope");
                                            if(procnature.equalsIgnoreCase("works")){


                                            if(forWorksdateEdit){
                                            if (flaB) {
                                        %>

                                        <a href="#" onclick="Edit(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">Edit</a>&nbsp|&nbsp;
                                        <a href="#" onclick="ViewPR(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">View</a>
                                        <%} else {
                                                                                                    if (flags) {
                                        %>

                                        <%if(!checkitemreceived){%>
                                             <%if (isWCC) {%>
                                        <a href="#" onclick="jAlert('<%=bdl.getString("CMS.PR.workcomplete")%>','Progress Report', function(RetVal) {
                                        });">Prepare</a>&nbsp;|&nbsp;
                                        <%} else if (isConTer) {%>
                                        <a href="#" onclick="jAlert('<%=bdl.getString("CMS.PR.contractTerminate")%>','Progress Report', function(RetVal) {
                                    });">Prepare</a>&nbsp;|&nbsp;
                                        <%}else{%>
                                                                                          <a href="#" onclick="View(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">Prepare</a>&nbsp;|&nbsp;
                                        <%}}%>

                                        <%} else {%>
                                        <a href="#" onclick="jAlert('<%=bdl.getString("CMS.DateConfigPending")%>','Progress Report', function(RetVal) {
                                });">Prepare</a>&nbsp;|&nbsp;
                                        <%}%>
                                        <a href="#" onclick="ViewPR(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">View</a>
                                        <%}}else{%>
                                        <%=bdl.getString("CMS.works.edidatesByTenderer")%>
                                        <%}}else{%>
                                        <%if (flaB) {
                                        %>

                                        <a href="#" onclick="Edit(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">Edit</a>&nbsp|&nbsp;
                                        <a href="#" onclick="ViewPR(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">View</a>
                                        <%} else {
                                                                                                    if (flags) {
                                        %>
                                       <%if(!checkitemreceived){%>
                                             <%if (isWCC) {%>
                                        <a href="#" onclick="jAlert('<%=bdl.getString("CMS.PR.workcomplete")%>','Progress Report', function(RetVal) {
                                        });">Prepare</a>&nbsp;|&nbsp;
                                        <%} else if (isConTer) {%>
                                        <a href="#" onclick="jAlert('<%=bdl.getString("CMS.PR.contractTerminate")%>','Progress Report', function(RetVal) {
                                    });">Prepare</a>&nbsp;|&nbsp;
                                        <%}else{%>
                                       <a href="#" onclick="View(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">Prepare</a>&nbsp;|&nbsp;
                                        <%}}%>


                                        <%} else {%>
                                        <a href="#" onclick="jAlert('<%=bdl.getString("CMS.DateConfigPending")%>','Progress Report', function(RetVal) {
                                });">Prepare</a>&nbsp;|&nbsp;
                                        <%}%>
                                        <a href="#" onclick="ViewPR(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>);">View</a>
                                        <%}%>

                                    </td>
                                </tr>
                                <%

                                                                                                     }
countt++;
}%>

                            </table>
<%
                         //for (Object[] object : issueNOASrBean.getNOAListing(Integer.parseInt(tenderId))) {
                         List<Object[]> objectlist =    issueNOASrBean.getDetailsNOAforInvoice(Integer.parseInt(tenderId), Integer.parseInt(lotList.getFieldName5()));
                         if(!objectlist.isEmpty()){
                             Object[] object = objectlist.get(0);
                             if (issueNOASrBean.isAvlTCS((Integer) object[1])) {
                                     int contractSignId = 0;
                                     contractSignId = issueNOASrBean.getContractSignId();
                                     CmsWcCertificateServiceBean cmsWcCertificateServiceBean = new CmsWcCertificateServiceBean();
                                     cmsWcCertificateServiceBean.setLogUserId(userId);
                                     TblCmsWcCertificate tblCmsWcCertificate =
                                             cmsWcCertificateServiceBean.getCmsWcCertificateForContractSignId(contractSignId);
                                     out.print("<table cellspacing='0' class='tableList_1' width='100%'>");
                                     if (tblCmsWcCertificate == null) {
                                         out.print("<tr><td class='ff' width='20%' colspan='2' class='t-align-left'><div  class='responseMsg noticeMsg t-align-left'>"
                                                 + "On receiving the request of Issue Work Completion Certificate from");
                                                 if("Goods".equalsIgnoreCase(procnature))
                                                 {
                                                    out.print(" Supplier, ");
                                                 }else
                                                 {
                                                    out.print(" Contractor, ");
                                                 }
                                                 out.print("system will display a link 'Issue Work Completion Certificate' for further processing</div></td></tr>");
                                     }
                                     out.print("<tr>");
                                     out.print("<td class='ff' width='20%'  class='t-align-left'>  Work Completion Certificate  </td>");
                                     out.print("<td class='ff' width='80%'  class='t-align-left'>");
                                     CmsContractTerminationBean cmsContractTerminationBean = new CmsContractTerminationBean();
                                     cmsContractTerminationBean.setLogUserId(userId);
                                     //if(!isConTer)
                                     //{
                                        if(tblCmsWcCertificate == null){
                                            if(cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotList.getFieldName5()),0)){
                                                out.print("<a href ='WorkCompletionCertificate.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                     + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + "'>Issue Work Completion Certificate</a>&nbsp;&nbsp;&nbsp;&nbsp;");
                                            }
                                         }
                                         if(tblCmsWcCertificate != null){
                                            if(cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotList.getFieldName5()),0) && !"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                                 out.print("<a href ='WorkCompletionCertificate.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                 + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()+"'>Issue Work Completion Certificate</a>&nbsp;&nbsp;&nbsp;&nbsp;");
                                            }
                                            out.print("<a href ='ViewWCCertificate.jsp?wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                 + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>View Work Completion Certificate</a>");
                                            //out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                           // if(!"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                                    out.print("&nbsp|&nbsp<a href ='WCCUpload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                         + "&contractSignId=" + contractSignId + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                         + "'>Upload / Download Files</a>");
                                          //  }
                                        }
                                     //}else{
                                     //    out.print("<a href='#' onclick='CTerminationMsg()'>Issue Work Completion Certificate</a>");
                                     //}
                                     out.print(" </td>");
                                     out.print(" </tr>");
                                     /*out.print("<td class='ff' width='20%'  class='t-align-left'> Contract Termination </td>");
                                     out.print("<td class='ff' width='80%'  class='t-align-left'>");
                                     if (tblCmsContractTermination == null) {
                                         out.print("<a href ='ContractTermination.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                 + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + "'>Terminate</a> ");
                                     } else {
                                         out.print("<a href ='ViewContractTermination.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                 + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>ViewContractTermination</a>");
                                         out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                         out.print("<a href ='ContractTeminationUpload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                 + "&contractSignId=" + contractSignId + "&contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                 + "'>Upload/Download Files</a>");
                                     }*/
                                     out.print("</table>");
                                 }
                         }

                         //if
%>
                        </td></tr>
                        <%
                                                                }
                        %>

                   
                                  <%
                                    RepeatOrderService ros = (RepeatOrderService) AppContext.getSpringBean("RepeatOrderService");
                                        List<Object[]> mainForRO = ros.getMainLoopForROPESide(lotIds);
                                        if(!mainForRO.isEmpty()){
                                            int icount = 1;
                                                for(Object[] objs : mainForRO){
                                                     for (Object[] objd : issueNOASrBean.getNOAListingForRO(Integer.parseInt(tenderId),(Integer) objs[0])) {
                                                         flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotList.getFieldName5()),(Integer)objd[11]);
                                                         List<Object> wpids = ros.getWpIdForAfterRODone(Integer.parseInt(objs[1].toString()));
                                                          isWCC = service.isWorkCompleteOrNotForRO(lotId+"",(Integer)objd[11]);
                                                          isConTer = service.isContractTerminatedOrNotForRO(lotId+"",(Integer)objd[11]);
                                                            int countR = 1;
                                                            if (!wpids.isEmpty() && wpids != null) {%>

                                <tr>
                                    <td colspan="3">
                                <ul class="tabPanel_1 noprint t_space">
                                    <li class="sMenu button_padding">Repeat Order-<%=icount%></li>
                                </ul>

                                    </td>


                                                            <tr><td colspan="2">
                            <span style="float: left; text-align: left;font-weight:bold;">
                                <a href="ViewTotalInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotList.getFieldName5()%>&cntId=<%=objd[11]%>" class="">Financial Progress Report</a>
                            </span>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                    <th width="17%" class="t-align-center"><%=bdl.getString("CMS.PR.report")%></th>
                                    <th width="5%" class="t-align-center"><%=bdl.getString("CMS.action")%></th>
                                </tr>
                                <%
                                int showR=0;
                                for (Object obj : wpids) {
                                                 boolean checkitemreceived = service.checkForItemFullyReceivedOrNotByWpId(Integer.parseInt(obj.toString()));
                                                  List<Object[]> formsR = dDocSrBean.getAllBoQFormsForRO(lotIds);
                                %>


                                <tr>
                                    <td style="text-align: center;"><%=countR%></td>
                                    <td><%="Progress Report "+formsR.get(showR)[1] %></td>
                                    <td>

                                        <%
                                            boolean flaB = service.checkForPrEntry(Integer.parseInt(obj.toString()));
                                        if (flaB) {
                                        %>

                                        <a href="#" onclick="EditR(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>,<%=objd[11]%>);">Edit</a>&nbsp|&nbsp;
                                        <a href="#" onclick="ViewPR(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>,<%=objd[11]%>);">View</a>
                                        <%} else {
                                                if (flags) {
                                        %>
                                        <%if(!checkitemreceived){%>
                                             <%if (isWCC) {%>
                                        <a href="#" onclick="jAlert('<%=bdl.getString("CMS.PR.workcomplete")%>','Progress Report', function(RetVal) {
                                        });">Prepare</a>&nbsp;|&nbsp;
                                        <%} else if (isConTer) {%>
                                        <a href="#" onclick="jAlert('<%=bdl.getString("CMS.PR.contractTerminate")%>','Progress Report', function(RetVal) {
                                    });">Prepare</a>&nbsp;|&nbsp;
                                        <%}else{%>
                                              <a href="#" onclick="ViewR(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>,<%=objd[11]%>);">Prepare</a>&nbsp;|&nbsp;
                                        <%}}%>

                                        <%} else {%>
                                        <a href="#" onclick="jAlert('<%=bdl.getString("CMS.DateConfigPending")%>','Progress Report', function(RetVal) {
                                });">Prepare</a>&nbsp;|&nbsp;
                                        <%}%>
                                        <a href="#" onclick="ViewPR(<%=obj.toString()%>,<%=tenderId%>,<%=lotList.getFieldName5()%>,<%=objd[11]%>);">View</a>
                                        <%}%>

                                    </td>
                                </tr>
                                <%

                                                                                                     
countR++;
showR++;
}%>

                            </table>
<%
                         //for (Object[] object : issueNOASrBean.getNOAListing(Integer.parseInt(tenderId))) {
                         List<Object[]> objectlist =    issueNOASrBean.getDetailsForRO(Integer.parseInt(tenderId), Integer.parseInt(lotList.getFieldName5()),(Integer)objs[0]);
                         if(!objectlist.isEmpty()){
                             Object[] object = objectlist.get(0);
                             if (issueNOASrBean.isAvlTCS((Integer) object[1])) {
                                     int contractSignId = 0;
                                     contractSignId = issueNOASrBean.getContractSignId();
                                     CmsWcCertificateServiceBean cmsWcCertificateServiceBean = new CmsWcCertificateServiceBean();
                                     cmsWcCertificateServiceBean.setLogUserId(userId);
                                     TblCmsWcCertificate tblCmsWcCertificate =
                                             cmsWcCertificateServiceBean.getCmsWcCertificateForContractSignId((Integer)objd[11]);
                                     out.print("<table cellspacing='0' class='tableList_1' width='100%'>");
                                     if (tblCmsWcCertificate == null) {
                                         out.print("<tr><td class='ff' width='20%' colspan='2' class='t-align-left'><div  class='responseMsg noticeMsg t-align-left'>"
                                                 + "On receiving the request of Issue Work Completion Certificate from");
                                                 if("Goods".equalsIgnoreCase(procnature))
                                                 {
                                                    out.print(" Supplier, ");
                                                 }else
                                                 {
                                                    out.print(" Contractor, ");
                                                 }
                                                 out.print("system will display a link 'Issue Work Completion Certificate'</div></td></tr>");
                                     }
                                     out.print("<tr>");
                                     out.print("<td class='ff' width='20%'  class='t-align-left'>  Work Completion Certificate  </td>");
                                     out.print("<td class='ff' width='80%'  class='t-align-left'>");
                                     CmsContractTerminationBean cmsContractTerminationBean = new CmsContractTerminationBean();
                                     cmsContractTerminationBean.setLogUserId(userId);
                                     //if(!isConTer)
                                     //{
                                        if(tblCmsWcCertificate == null){
                                            if(cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotList.getFieldName5()),(Integer)objd[11])){
                                                out.print("<a href ='WorkCompletionCertificate.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                     + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + "'>Issue Work Completion Certificate</a>&nbsp;&nbsp;&nbsp;&nbsp;");
                                            }
                                         }
                                         if(tblCmsWcCertificate != null){
                                            if(cmsVendorReqWccSrBean.isRequestPending(Integer.parseInt(lotList.getFieldName5()),(Integer)objd[11]) && !"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                                 out.print("<a href ='WorkCompletionCertificate.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                 + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()+"'>Issue Work Completion Certificate</a>&nbsp;&nbsp;&nbsp;&nbsp;");
                                            }
                                            out.print("<a href ='ViewWCCertificate.jsp?wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                 + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>View Work Completion Certificate</a>");
                                            //out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                           // if(!"Yes".equalsIgnoreCase(tblCmsWcCertificate.getIsWorkComplete())){
                                                    out.print("&nbsp|&nbsp<a href ='WCCUpload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                         + "&contractSignId=" + contractSignId + "&wcCertId=" + tblCmsWcCertificate.getWcCertId()
                                                         + "'>Upload / Download Files</a>");
                                          //  }
                                        }
                                     //}else{
                                     //    out.print("<a href='#' onclick='CTerminationMsg()'>Issue Work Completion Certificate</a>");
                                     //}
                                     out.print(" </td>");
                                     out.print(" </tr>");
                                     /*out.print("<td class='ff' width='20%'  class='t-align-left'> Contract Termination </td>");
                                     out.print("<td class='ff' width='80%'  class='t-align-left'>");
                                     if (tblCmsContractTermination == null) {
                                         out.print("<a href ='ContractTermination.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                 + "&contractSignId=" + contractSignId + "&lotId=" + lotList.getFieldName5() + "'>Terminate</a> ");
                                     } else {
                                         out.print("<a href ='ViewContractTermination.jsp?contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                 + "&tenderId=" + tenderId + "&lotId=" + lotList.getFieldName5() + "'>ViewContractTermination</a>");
                                         out.print("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
                                         out.print("<a href ='ContractTeminationUpload.jsp?tenderId=" + tenderId + "&contractUserId=" + (Integer) object[8]
                                                 + "&contractSignId=" + contractSignId + "&contractTerminationId=" + tblCmsContractTermination.getContractTerminationId()
                                                 + "'>Upload/Download Files</a>");
                                     }*/
                                     out.print("</table>");
                                 }
                         }

                         //if
%>
                        </td></tr>
                        <%
                                                                }
                        %>

                        <%
                                                }icount++;}
                        }
                        %>
                         <%
                                }%>

                </table>
            </form>

        </div>
    </div></div>

</div>
<%@include file="../resources/common/Bottom.jsp" %>
</div>
<script>
var headSel_Obj = document.getElementById("headTabTender");
if(headSel_Obj != null){
headSel_Obj.setAttribute("class", "selected");    }
function CTerminationMsg(){
jAlert("Contract has been Terminated. You can not Issue Work Completion Certificate.","WCC", function(RetVal) {
});}
</script>
</html>
