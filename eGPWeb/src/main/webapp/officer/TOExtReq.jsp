<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Date" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.SHA1HashEncryption" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.lang.String" %>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page  import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page buffer="15kb"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Opening Date and Time Extension Request</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
<link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
<script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
<script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
</head>
<body>

<div class="dashboard_div">
  
    <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
  
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
  	<div class="pageHead_1">
            Opening Date and Time Extension Request
            &nbsp;<span style="float: right;" ><a href="OpenComm.jsp?tenderid=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a></span>
        </div>

        <%
            // Variable tenderId is defined by u on ur current page.
            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
         %>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>

            <%
            HandleSpecialChar handleSpecialChar = new HandleSpecialChar();

            String tenderId="", currUserId="", hopeUserId="", openingDt="", memberRole="", newOpeningDt="",
            strComments="", strXMLtxt="";
            boolean isTenderOpened = false, isDeclarationTime = false,
                    isDeclarationTimeOver = false, isDeclarationDone = false,
                    isMember = false, isMemberApproved = false,
                    isMemberDecrypter = false , isCommitteeApproved = false;
            int committeeMemberCnt=0, committeeApprovedMemberCnt=0;

        HttpSession hs = request.getSession();
        if (hs.getAttribute("userId") != null) {
            currUserId = hs.getAttribute("userId").toString();
        }

        if(request.getParameter("tenderId")!=null){
            tenderId=request.getParameter("tenderId");
        }

         // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType="tenderId";
            int auditId=Integer.parseInt(tenderId);
            String auditAction="Request for opening date extension";
            String moduleName=EgpModule.Tender_Opening.getName();
            String remarks="";
            
            
        
        /*START : CODE TO GET TENDER INFORMATIN FOR OPENING DATE EXTENSION PROCESS*/
        List<SPTenderCommonData> lstExtensionInfo = tenderCommonService.returndata("getTenderInfoForExtension", tenderId, currUserId);

            if (!lstExtensionInfo.isEmpty() && lstExtensionInfo.size()>0){

                 if ("yes".equalsIgnoreCase(lstExtensionInfo.get(0).getFieldName2())){
                                        isTenderOpened = true;
                                    }

                if ("yes".equalsIgnoreCase(lstExtensionInfo.get(0).getFieldName3())){
                    isDeclarationTime = true;
                }

                if ("yes".equalsIgnoreCase(lstExtensionInfo.get(0).getFieldName4())){
                    isDeclarationTimeOver = true;
                }

                if ("yes".equalsIgnoreCase(lstExtensionInfo.get(0).getFieldName5())){
                    isDeclarationDone = true;
                }

                if ("yes".equalsIgnoreCase(lstExtensionInfo.get(0).getFieldName6())){
                    isMember = true;
                }

                if ("yes".equalsIgnoreCase(lstExtensionInfo.get(0).getFieldName7())){
                    isMemberApproved = true;
                }

                  if ("yes".equalsIgnoreCase(lstExtensionInfo.get(0).getFieldName8())){
                        isCommitteeApproved = true;
                    }

                openingDt=lstExtensionInfo.get(0).getFieldName9();
                hopeUserId=lstExtensionInfo.get(0).getFieldName10();  
                TenderCommonService tenderService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            
                int approverId = tenderService.getTenderCommitteeChairperson(Integer.parseInt(request.getParameter("tenderId")));
                hopeUserId = Integer.toString(approverId);
            }

            
        /*END : CODE TO GET TENDER INFORMATIN FOR OPENING DATE EXTENSION PROCESS*/
            java.text.SimpleDateFormat sd= new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm");

            if(request.getParameter("btnExtend")!=null){
                
                //Date d =sd.parse();
                String temp[]=request.getParameter("txtOpeningDt").split(" ");
                String d[] = temp[0].split("/");                
                newOpeningDt = d[2]+"-"+d[1]+"-"+d[0]+" "+temp[1];
                
                strComments=request.getParameter("txtComments");

             /* START: CODE TO HANDLE SPECIAL CHARACTERS  */
                strComments = handleSpecialChar.handleSpecialChar(strComments);
             /* END: CODE TO HANDLE SPECIAL CHARACTERS  */

             if (newOpeningDt!="" && strComments!="")   {
                 CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                 CommonMsgChk commonMsgChk;

                 strXMLtxt = "<tbl_TenderOpenExt tenderId=\"" + tenderId + "\" userId=\"" + hopeUserId + "\" extReqDt=\"" + sd.format(new Date()) + "\" extReqBy=\"" + currUserId + "\" remarks=\"" + strComments + "\" newOpenDt=\"" + newOpeningDt + "\" envelopeId=\"1\" extStatus=\"pending\" finalOpenDt=\"\" hopeComments=\"\" approvalDt=\"\" />";
                 strXMLtxt = "<root>" + strXMLtxt + "</root>";
                 commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_TenderOpenExt", strXMLtxt, "").get(0);

                if (commonMsgChk.getFlag().equals(true)) 
                {
                    remarks=strComments;
                    auditAction="Request for opening date extension";
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    
                      String peOfficeNm="";

                      List<SPTenderCommonData> lstOfficerUserId =
                                            tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);
                                    if (!lstOfficerUserId.isEmpty()) {
                                       // peOfficerUserId = lstOfficerUserId.get(0).getFieldName1(); // Get PE UserId
                                        peOfficeNm = lstOfficerUserId.get(0).getFieldName3(); // Get PE Name
                            }
                                    lstOfficerUserId = null;

                    List<SPTenderCommonData> emails = tenderCommonService.returndata("getEmailIdfromUserId",hopeUserId,null);
                    UserRegisterService userRegisterService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                    String [] mail={emails.get(0).getFieldName1()};
                    SendMessageUtil sendMessageUtil = new SendMessageUtil();
                    MailContentUtility mailContentUtility = new MailContentUtility();
                    MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                    String mailText = mailContentUtility.contTOExtReq(tenderId,toextTenderRefNo, peOfficeNm);
                    sendMessageUtil.setEmailTo(mail);
                    sendMessageUtil.setEmailSub(HandleSpecialChar.handleSpecialChar("Opening Date and Time Extension Request"));
                    sendMessageUtil.setEmailMessage(mailText);
                    userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("msgboxfrom"), HandleSpecialChar.handleSpecialChar("Opening Date and Time Extension Request"), msgBoxContentUtility.contTOExtReq(tenderId,toextTenderRefNo, peOfficeNm));
                    sendMessageUtil.sendEmail();
                    sendMessageUtil=null;
                    mailContentUtility=null;
                    mail=null;
                    msgBoxContentUtility=null;
                    //response.sendRedirect("TOExtReq.jsp?tenderId=" + tenderId + "&msgId=success");
                    response.sendRedirect("OpenComm.jsp?tenderid=" + tenderId + "&msgId=extensionrequested");
                } else 
                {
                    auditAction="Error in Request for opening date extension";
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    
                    response.sendRedirect("TOExtReq.jsp?tenderId=" + tenderId + "&msgId=error");
                }
             }

           
        }
%>


     <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("success")){
                            msgTxt="Extension request submitted successfully.";
                        } else  if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError){%>
                        <div class="responseMsg errorMsg"><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg"><%=msgTxt%></div>
                   <%}%>
                <%}}%>

      <form id="frmTOExtReq" name="frmTOExtReq" action="TOExtReq.jsp?tenderId=<%=tenderId%>" method="post">
    <div style="font-style: italic" class="t-align-left t_space">
          Fields marked with (<span class="mandatory">*</span>) are mandatory
    </div>

    <table width="100%" cellspacing="0" class="tableList_1 t_space">
        <tr>
            <td width="21%" class="t-align-left ff">Current Opening Date & Time </td>
            <td width="79%" class="t-align-left"><%=openingDt%></td>
        </tr>
        <tr>
            <td class="t-align-left ff">Newly Proposed Opening Date & Time: <span class="mandatory">*</span></td>
            <td class="t-align-left">
                <input onfocus="GetCal('txtOpeningDt','txtOpeningDt');" name="txtOpeningDt" id="txtOpeningDt" type="text" class="formTxtBox_1" style="width:100px;" readonly="true" />
                <img id="imgOpeningDt" name="imgOpeningDt" onclick="GetCal('txtOpeningDt','imgOpeningDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
            </td>
        </tr>
        <tr>
            <td class="t-align-left ff">Reason for Delay: <span class="mandatory">*</span></td>
            <td class="t-align-left"><textarea cols="100" rows="5" id="txtComments" name="txtComments" class="formTxtBox_1"></textarea></td>
        </tr>
    </table>

  <div class="t-align-center t_space">
      <label class="formBtn_1">
          <input name="btnExtend" id="btnExtend" type="submit" value="Submit" />
      </label>
  </div>
        </form>
	<div>&nbsp;</div>
  </div>
  <!--Dashboard Content Part End-->
   <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
  <!--Dashboard Footer End-->
</div>
</body>
  <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
</script>
<script type="text/javascript">
    function GetCal(txtname,controlname)
    {
        new Calendar({
            inputField: txtname,
            trigger: controlname,
            showTime: true,
            dateFormat:"%d/%m/%Y %H:%M",
            onSelect: function() {
                var date = Calendar.intToDate(this.selection.get());
                LEFT_CAL.args.min = date;
                LEFT_CAL.redraw();
                this.hide();
            }
        });

        var LEFT_CAL = Calendar.setup({
            weekNumbers: false
        })
    }
  </script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#frmTOExtReq').validate({
            rules:{
                txtOpeningDt:{required:true,CompareToForToday:true},
                txtComments:{required:true,maxlength:2000}
            },
            messages:{
                txtOpeningDt:{required:"<div class='reqF_1'>Please enter Newly Proposed Opening Date & Time</div>",
                    CompareToForToday:"<div class='reqF_1'>Newly proposed opening date & time must be greater than the current date & time.</div>"
                },
                txtComments:{required:"<div class='reqF_1'>Please enter the Reason for Delay</div>",
                    maxlength:"<div class='reqF_1'>Maximum 2000 characters are allowed.</div>"
                }
            },
            errorPlacement:function(error,element){
                if(element.attr("name")=="txtOpeningDt")
                {
                    error.insertAfter("#imgOpeningDt");
                }
                else
                    error.insertAfter(element);
            }
        });
    });
</script>
</html>