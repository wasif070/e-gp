<%--
    Document   : TenderDocPrep
    Created on : 20-Nov-2010, 12:37:32 PM
    Author     : Yagnesh

    Modify by : Dipal Shah.
    Modify Date: 30/11/2011
    Decription : Added condition for Open edit link for grand summary if corrigendum is pending.
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateSectionDocs"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TendererComboService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CreateComboService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateGuildeLines"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderForms"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8" import="java.util.List, com.cptu.egp.eps.model.table.TblTenderSection"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<jsp:useBean id="grandSummary" class="com.cptu.egp.eps.web.servicebean.GrandSummarySrBean"  scope="page"/>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" scope="page"/>
 <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<jsp:useBean id="procurementNatureBean" class="com.cptu.egp.eps.web.servicebean.OfficerTabSrBean" scope="page"/>
<jsp:useBean id="evalSerCertiSrBean" class="com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean" scope="page" />
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />
<jsp:useBean id="tenderFrmSrBean" class="com.cptu.egp.eps.web.servicebean.TenderFormSrBean" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            String isPDF = "abc";

            if (request.getParameter("isPDF") != null) {
                isPDF = request.getParameter("isPDF");
            }
            int packageOrLotId = -1;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Document Preparation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/common.js"></script>
        <script>
        function delTenderFormConfirm(tenderid,formid,corriId)
        {
            var returnFlag=false;
              $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/CreateTenderFormSrvt",
                        data:"action=checkGrandSumExist&tenderId="+tenderid+"&formId="+formid+"&tableId=0&corriId="+corriId,
                        async: false,
                        success: function(j)
			{
				var alertMessage="Are You sure You want to delete this Form?";
				if(j.toString() != null && $.trim(j.toString()) != "0")
				{
				    alertMessage= "You have already prepared a Grand Summary Report. Deletion of this form will remove Grand Summary Report. Do you want to Continue?";
				}

				if(confirm(alertMessage))
				{
				    	returnFlag=true;
                                }
                                else
				    {
					returnFlag=false;
				    }

			}
		});

           return returnFlag;
         }

        </script>
<script type="text/javascript">
    function confirmation(formid,sectionId,pkgOrLotId){
        jConfirm('Do you really want to copy this Form? ', 'Copy Form confirmation', function (ans) {
            if (ans){
                dynamicFromSubmit('<%=request.getContextPath()%>/CreateTenderFormSrvt?action=dumpForm&tenderId=<%= request.getParameter("tenderId")%>&sectionId='+sectionId+'&formId='+formid+'&porlId='+pkgOrLotId+'&corriCrtNPending='+$('#corriCrtNPending').val());
            }
        });
    }
    // START Added By GSS for BOQ fixed or salvage items April 2015
     function askConfirmation(sectionId,boqType, corriCrtNPending){
        jConfirm('Do you really want to create  form with fixed rates </br> Please click <b>Yes</b> only if you are sure.<br/> To cancel click on <b>No</b>', 'Create New   Form confirmation', function (yes) {
            if (yes){
                if(corriCrtNPending!='true'){
                dynamicFromSubmit('<%=request.getContextPath()%>/CreateTenderFormSrvt?action=createFixedBOQForm&tenderId=<%= request.getParameter("tenderId")%>&sectionId='+sectionId+'&boqType='+boqType);
                }
                else{
                dynamicFromSubmit('<%=request.getContextPath()%>/CreateTenderFormSrvt?action=createFixedBOQForm&tenderId=<%= request.getParameter("tenderId")%>&sectionId='+sectionId+'&boqType='+boqType+'corriform');
                }
            }
        });
    }
    function askConfirmationForSalvage(sectionId,boqType,corriCrtNPending){
        jConfirm('Do you really want to create  form for salvage items </br> Please click <b>Yes</b> only if you are sure.<br/> To cancel click on <b>No</b>', 'Create New   Form confirmation', function (yes) {
            if (yes){
                if(corriCrtNPending!='true'){
                dynamicFromSubmit('<%=request.getContextPath()%>/CreateTenderFormSrvt?action=createFixedBOQForm&tenderId=<%= request.getParameter("tenderId")%>&sectionId='+sectionId+'&boqType='+boqType);
                }
                else{
                dynamicFromSubmit('<%=request.getContextPath()%>/CreateTenderFormSrvt?action=createFixedBOQForm&tenderId=<%= request.getParameter("tenderId")%>&sectionId='+sectionId+'&boqType='+boqType+'corriform');
                }
            }
        });
    }
    // END Added By GSS for BOQ fixed or salvage items
</script>
    </head>
    <body>
        <%
                    String logUserId = "0";
                    if (session.getAttribute("userId") != null) {
                    logUserId = session.getAttribute("userId").toString();
                    }
                    TendererComboService tenderercomboservice = (TendererComboService) AppContext.getSpringBean("TendererComboService");
                    tenderercomboservice.setLogUserId(logUserId);
                int tenderId = 0;
                boolean isSerivce = false;
            /*This variable is taken for cheking wether the tender is service or not*/
            String tenderidd="";
            if (request.getParameter("tenderId") != null) {
                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                tenderidd = request.getParameter("tenderId");
            }
            String strProcNature = "";

            strProcNature = procurementNatureBean.getProcurementNature(tenderId).get(0).getProcurementNature();

            TenderCommonService wfTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            List<SPTenderCommonData> checkuserRights = wfTenderCommonService.returndata("CheckTenderUserRights", logUserId,tenderidd);
            List<SPTenderCommonData> chekTenderCreator = wfTenderCommonService.returndata("CheckTenderCreator", logUserId ,tenderidd);

        %>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="contentArea_1">

            <!--Middle Content Table Start-->
            <!--Page Content Start-->
            <div class="pageHead_1">
                Tender Document Preparation
                <%--<span class="c-alignment-right">
                    <a class="action-button-goback" href="TenderDashboard.jsp?tenderid=<%= tenderId %>" title="Tender Dashboard">Tender Dashboard</a>
                </span>--%>
                <%--<span class="c-alignment-right">
                    <a class="action-button-goback" href="LotDetails.jsp?tenderid=<%= tenderId %>" title="Tender Lot Details">Lot Details</a>
                </span>--%>
            </div>
                <%if(request.getParameter("removeSucc")!=null){%>
                <div id="errMsg"  class="responseMsg successMsg" style="margin-top: 10px;">The document is removed successfully.</div>
                <%}%>
                <%if(request.getParameter("downForm")!=null){%>
                <div id="errMsg"  class="responseMsg errorMsg" style="margin-top: 10px;">The document was not uploaded properly.</div>
                <%}%>
                <%if(request.getParameter("isCancelled")!=null){%>
                <div id="succMsg"  class="responseMsg successMsg" style="margin-top: 10px;">Form canceled successfully.</div>
                <%}%>
                  <%if(request.getParameter("isFixedBOQCreated")!=null && "true".equals(request.getParameter("isFixedBOQCreated"))){%>
                <div id="succMsg"  class="responseMsg successMsg" style="margin-top: 10px;">Form created successfully.</div>
             <%}%>
             <%if(request.getParameter("isFixedBOQCreated")!=null && "false".equals(request.getParameter("isFixedBOQCreated"))){%>
                <div id="succMsg"  class="responseMsg errorMsg" style="margin-top: 10px;">Form creation failed.</div>
             <%}%>
            <%
                pageContext.setAttribute("tenderId", tenderId);
                pageContext.setAttribute("tab", "2");
            %>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%

            if (request.getParameter("porlId") != null) {
                packageOrLotId = Integer.parseInt(request.getParameter("porlId"));
            }

            int tenderStdId = 0;
            boolean isTenderPkgWise = false;
                            if (pageContext.getAttribute("isTenPackageWise") != null) {
                isTenderPkgWise = Boolean.parseBoolean(pageContext.getAttribute("isTenPackageWise").toString());
            }

                            if (isTenPackageWise) {
                tenderStdId = dDocSrBean.getTenderSTDId(tenderId);
                            } else {
                tenderStdId = dDocSrBean.getTenderSTDId(tenderId, packageOrLotId);
            }

            String stdName = "";
            TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
            int tendid = 0;
            String corriId=null;
            //Corrigendum Created and its status is Pending
            boolean corriCrtNPending = false;
                            List<SPTenderCommonData> lstCorri = tenderCommonService.returndata("getCorrigenduminfo", tenderId + "", null);
                            if (lstCorri != null) {
                                if (lstCorri.size() > 0) {
                    for (SPTenderCommonData data : lstCorri) {
                        if (data.getFieldName4().equalsIgnoreCase("pending")) {
                            corriCrtNPending = true;
                            corriId=data.getFieldName1();
                        }
                    }
                }
            }
           //START by GSS no edit for TDS once tender opening completed Ticket:11066
          List<SPTenderCommonData> lstTenderOpened = tenderCommonService.returndata("getTenderOpenStatus", tenderId + "", null);
           if (lstTenderOpened != null) {
                                if (lstTenderOpened.size() > 0) {
                    for (SPTenderCommonData data : lstTenderOpened) {
                        if (data.getFieldName1().equalsIgnoreCase("Yes")) {
                            corriCrtNPending = false;
                          }
                    }
                }
                                }
           //END by GSS no edit for TDS once tender opening completed

          /* if("Use STD".equalsIgnoreCase(request.getParameter("submit"))){
                tenderDocumentSrBean.dumpSTD(request.getParameter("tenderid"), request.getParameter("txtStdTemplate"), session.getAttribute("userId").toString());
                response.sendRedirect("TenderDocPrep.jsp?tenderId="+request.getParameter("tenderid"));
           }*/

            //geting lot description

            //boolean isPackage = false;
            //isPackage = true;


            // workflow Integration by Chalapathi

             int uid = 0;
                            if (session.getAttribute("userId") != null) {
                                Integer ob1 = (Integer) session.getAttribute("userId");
                       uid = ob1.intValue();
                   }
                        WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                        short eventid = 2;
                        int objectid = 0;
                        short activityid = 2;
                        int childid = 1;
                        short wflevel = 1;
                        int initiator = 0;
                        int approver = 0;
                        boolean isInitiater = false;
                        boolean evntexist = false;
                        String fileOnHand = "No";
                        String checkperole = "No";
                        String chTender = "No";
                        boolean ispublish = false;
                        boolean iswfLevelExist = false;
                        boolean isconApprove = false;
                        String tenderStatus = "";
                        String tenderworkflow = "";
                         String objtenderId = request.getParameter("tenderId");
                            if (objtenderId != null) {
                                objectid = Integer.parseInt(objtenderId);
                                childid = objectid;
                            }


                            evntexist = workFlowSrBean.isWorkFlowEventExist(eventid, objectid);
                            List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, activityid);
                            // workFlowSrBean.getWorkFlowConfig(objectid, activityid, childid, wflevel,uid);


                            if (tblWorkFlowLevelConfig.size() > 0) {
                                Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                iswfLevelExist = true;
                                while (twflc.hasNext()) {
                                    TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                    TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                    if (wflevel == workFlowlel.getWfLevel() && uid == lmaster.getUserId()) {
                                        fileOnHand = workFlowlel.getFileOnHand();
                                    }
                                    if (workFlowlel.getWfRoleId() == 1) {
                                        initiator = lmaster.getUserId();
                                    }
                                    if (workFlowlel.getWfRoleId() == 2) {
                                        approver = lmaster.getUserId();
                                    }
                                    if (fileOnHand.equalsIgnoreCase("yes") && uid == lmaster.getUserId()) {
                                        isInitiater = true;
                                        }

                                }

                            }


                             Object obj = session.getAttribute("userId");
                              String userid = "";
                            if (obj != null) {
                                userid = obj.toString();
                              }
                              String field1 = "CheckPE";
                            List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(field1, userid, "");

                            if (editdata.size() > 0) {
                                 checkperole = "Yes";
                            }

                    // for publish link
                            boolean b_publish_link = false;
                        TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        List<SPTenderCommonData> checkTender = tenderCommonService1.returndata("TenderWorkFlowStatus", request.getParameter("tenderId"),  "'Pending'");
                            if (checkTender.size() > 0) {
                            chTender = "Yes";
                            }
                            List<SPTenderCommonData> publist = tenderCommonService1.returndata("TenderWorkFlowStatus", tenderId + "", "'Approved','Conditional Approval'");
                        //out.println("tenderid " + tenderId);
                        //out.println("chTender ="+chTender);
                        //out.println("publist size ="+publist.size());
                            if (publist.size() > 0)
                            {
                            ispublish = true;
                            SPTenderCommonData  spTenderCommondata = publist.get(0);
                            tenderStatus = spTenderCommondata.getFieldName1();
                            tenderworkflow = spTenderCommondata.getFieldName2();

                                if (tenderworkflow.equalsIgnoreCase("Conditional Approval")) {
                                isconApprove = true;
                                      }
                            }
                            if("Approved".equals(tenderStatus)){
                                b_publish_link = true;
                            }


        %>
            <%
              String lotDesc = "";
                            String lotNo = null;
                            String st_forDownloadLink = "Package";
                            List<TblTenderLotSecurity> lots = null;
                            if (isTenPackageWise == false) {
                                lots = tenderDocumentSrBean.getLotDetailsByLotId(tenderId, packageOrLotId);
                                if (lots.size() > 0) {
                  TblTenderLotSecurity tenderLotSecurity =  lots.get(0);
                  lotDesc = tenderLotSecurity.getLotDesc();
                  lotNo = tenderLotSecurity.getLotNo();
                  st_forDownloadLink = "Lot_"+tenderLotSecurity.getLotNo();
                  }
                 }

                            if (request.getParameter("tenderId") != null) {
                    tendid = Integer.parseInt(request.getParameter("tenderId"));

                }
                            String formType = "";
             %>

             <%if(b_publish_link){%>
             <div class="t_space">
             <span class="c-alignment-right b_space">
                 <a class="action-button-download" href="<%=request.getContextPath()%>/TenderSecUploadServlet?tenderId=<%=tendid%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&funName=zipdownload" title="Download Tender/Proposal Dcoument">Download Tender Document</a>
                </span>
             </div>
             <%}%>

             <div class="t_space" >
              <%@include file="officerTabPanel.jsp"%>
              <input type="hidden" id="corriCrtNPending" value="<%=corriCrtNPending%>" />
             </div>
             <div class="tabPanelArea_1">
                <%
                        String templateId = tenderDocumentSrBean.findStdTemplateId(String.valueOf(tendid));
                                String stdName1 = tenderDocumentSrBean.checkForDump(String.valueOf(tendid));
                                if (stdName1 == null) {
                    String stdTemplateId = tenderDocumentSrBean.findStdTemplateId(String.valueOf(tendid));
                                    if (Integer.parseInt(stdTemplateId) != 0) {
              %>


              <%--<form method="post">
              <table width="100%" cellspacing="0" class="tableList_1 ">
                  <tr>
                      <td width="30%">Prepare Tender Document : </td>
                      <td width="70%">
                          <input type="hidden" value="<%=tendid%>" name="tenderid"/>
                          <input type="hidden" value="<%=stdTemplateId%>" name="txtStdTemplate"/>
                          <label class="formBtn_1">
                            <input name="submit" type="submit" value="Use STD" id="btnSubmit"/>
                        </label>
                      </td>
                  </tr>
              </table>
              </form>--%>
                    <%} else {
                                if(isTenderPkgWise){
                                  out.print("<table width='100%' cellspacing='0' class='tableList_1'><tr><td><div class=\"responseMsg noticeMsg\">SBD yet not mapped</div></td></tr></table>");
                                  }
                                else{
                                  out.print("<table width='100%' cellspacing='0' class='tableList_1'><tr><td colspan=4 width='85%'><div class=\"responseMsg noticeMsg\">SBD yet not mapped</div></td><td class='t-align-center'><br/><a href=LotDetails.jsp?tenderid="+tendid+">Lot Selection</a></td></tr></table>");
                                    }
                              }
                          } else {
                              if(isTenderPkgWise){
                              out.print("<table width='100%' cellspacing='0' class='tableList_1'><tr><td width='15%' class='t-align-left ff'>Standard Bidding Document (SBD) : </td><td width='85%'>" + stdName1 + "</td></tr></table>");
                              }
                              else
                                {
                                  out.print("<table width='100%' cellspacing='0' class='tableList_1'><tr><td width='15%' class='t-align-left ff'>Standard Bidding Document (SBD) : </td><td colspan=3 width='75%'>" + stdName1 + "</td><td class='t-align-center'><a href=LotDetails.jsp?tenderid="+tendid+">Lot Selection</a></td></tr></table>");
                                }
                          }%>

                          <table width='100%' cellspacing='0' class='tableList_1 t_space'><tr><td width='15%' class='t-align-left ff'>Guidance Notes : </td><td width='85%'>


            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                                                    <th width="4%">Sl. No.</th>
                                                    <th width="30%">File Name</th>
                                                    <th width="38%">File Description</th>
                                                    <th width="10%">File Size <br/> (in KB)</th>
                                                    <th width="18%">Action
                                                    </th>
                </tr>

                                  <%
                                                    java.util.List<com.cptu.egp.eps.model.table.TblTemplateGuildeLines> docs = defineSTDInDtlSrBean.getGuideLinesDocs(Integer.parseInt(templateId));
                                                    if(docs != null){
                                                        int size=0;
                                                        if(docs.size() > 0){
                                                            short j = 0;
                                                            for(TblTemplateGuildeLines ttsd : docs){
                                                                j++;
                                                                size = Integer.parseInt(ttsd.getDocSize())/1024;
                                                %>
                <tr>
                                                        <td style="text-align:center;"><%= j %></td>
                                                        <td style="text-align:left;"><%= ttsd.getDocName()  %></td>
                                                        <td style="text-align:left;"><%= ttsd.getDescription()  %></td>
                                                        <td style="text-align:center;"><%=size%></td>
                                                        <td style="text-align:center;">
                                                            <a href="<%=request.getContextPath()%>/GuideLinesDocUploadServlet?docName=<%= ttsd.getDocName() %>&templateId=<%= templateId %>&docSize=<%= ttsd.getDocSize() %>&action=downloadDoc&officerPage=true&tenderId=<%=tenderid%>" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                                        </td>
                                                    </tr>

                                                <%
                                                               ttsd = null;
                                                            }
                                                        }else{
                                                %>
                                                    <tr>
                                                        <td colspan="5" class="t-align-center">
                                                            No records found
                                                        </td>
                                                    </tr>
                                                <%
                                                        }
                                                        docs = null;
                                                    }else{
                                                %>
                                                <tr>
                                                    <td colspan="5" class="t-align-center">
                                                        No records found
                                                    </td>
                                                </tr>
                                                <%
                                                    }
                                                %>
                                            </table>
                                  </td>
                              </tr>
                </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <% if (!"".equals(stdName) && stdName != null) {%>
                        <tr>
                            <td colspan="3" class="t-align-left ff">Standard Bidding Document : <%= stdName%> </td>
                        </tr>
                        <%   }%>
                        <tr>
                    <td width="15%" class="t-align-left ff">
                                <%if ("services".equalsIgnoreCase(strProcNature) && isTenPackageWise) {%>
                            Package No. :
                                <%} else {%>
                            Lot No. :
                        <%}%>
                    </td>
                    <td width="85%" colspan="2" class="t-align-left">
                        <%
                                            if (isTenPackageWise) {
                                out.print(tenderDocumentSrBean.getPkgNo(tenderid));
                                            } else {
                                out.print(lotNo);
                            }
                        %>
                    </td>
                </tr>
                <tr>
                    <td width="15%" class="t-align-left ff">
                                <%if ("services".equalsIgnoreCase(strProcNature)  && isTenPackageWise) {%>
                            Package Description :
                                <%} else {%>
                            Lot Description :
                        <%}%>
                    </td>
                    <td width="85%" colspan="2" class="t-align-left">
                                <%if (isTenPackageWise) {
                                                if (packageDescription == null) {
                                out.print("--");
                                                } else {
                                                    out.print(packageDescription);
                                                }
                                            } else {
                            out.print(lotDesc);
                           }%>
                    </td>
                </tr>
               
                <%
                                            String subCriteria  = "Minimum Technical Score <br/>for Short Listing";
                                                /*This method is called for cheking wether the tender is service or not*/
                                                List<TblTenderDetails> tblTenderDetail = procurementNatureBean.getProcurementNature(tenderId);
                                                                            if (tblTenderDetail.get(0).getProcurementNature().equalsIgnoreCase("Services")) {
                                                        isSerivce = true;
                                                        if("reoi".equalsIgnoreCase(tblTenderDetail.get(0).getProcurementMethod())){
                                                            subCriteria  = "Configure Evaluation Criteria and Sub Criteria";
                                                        }
                                                    }
                                                                            if (isSerivce) {
                                                                                //if(!tts.getContentType().equalsIgnoreCase("TOR")){
                                            %>
                                           <!--  <tr>
                                                <td width="15%" class="ff" nowrap><%//=subCriteria%>  :</td> -->
                                                <%
                                                                                                    if (((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) || corriCrtNPending) { // if cond -1
                                                                                                                    if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){ // if cond - 2
                                                                                                                        String linkCap = "No";
                                                                                                                        if(corriCrtNPending){
                                                                                                                            linkCap = "Yes";
                                                                                                                        }
                                                                                                                        List<Object []> updateList=evalSerCertiSrBean.viewEvalSerMark(tenderId,linkCap);
                                                                                                                        linkCap = "Configure";
                                                                                                                        if(updateList!=null && !updateList.isEmpty()){
                                                                                                                            linkCap = "Edit";
                                                                                                                        }
                                                                                                                       
                                                %>
                                               <!--  <td width="85%" colspan="2"><a href="EvalCertiService.jsp?tenderId=<%//= tenderId%>&iscorri=<%//=corriCrtNPending%>"><%//=linkCap%></a> | <a href="EvalCertiServiceView.jsp?tenderId=<%//= tenderId%>&iscorri=<%//=corriCrtNPending%>">View</a></td> -->
                                        <%} else {%> <%/*if cond - 1 ends here*/%>
                                       <!-- <td width="85%" colspan="2"><a href="EvalCertiServiceView.jsp?tenderId=<%//= tenderId%>&iscorri=<%//=corriCrtNPending%>">View</a></td> -->
                                                <%}/*View criteria Condtion ends here*/%>
                        <%}/*Condtion  Insert and Edit Link*/ else {%> <%/*if cond - 2 ends here*/%>
                                      <!--<td width="85%" colspan="2"><a href="EvalCertiServiceView.jsp?tenderId=<%//= tenderId%>">View</a></td>-->
                                                <%}/*View criteria Condtion ends here*/%>
                                        <%/*}Innner Condition Ends here*/                                                                                     }/*Is service condtion ends here*/%>
<!--                    </tr> -->
                <%
                    int tenderittSectionId = 0;
                    List<TblTenderSection> tSections = dDocSrBean.getTenderSections(tenderStdId);
                                    if (tSections != null) {
                                if (tSections.size() > 0) {
                                    short j = 0;
                                    short disp = 0;
            %>
                <tr>
                    <th width="15%" class="t-align-left">Section No.</th>
                    <th width="75%" class="t-align-left">Section Name</th>
                    <th width="10%" class="t-align-left">Action</th>
                </tr>
                <%
                                                    for (TblTenderSection tts : tSections) {
                                                        disp++;
                                                        String str_FolderName = tts.getSectionName().replace("&", "^");
                                                        str_FolderName = "Section"+disp+"_"+str_FolderName;
                                                        if (tts.getContentType().equals("Form") || tts.getContentType().equals("TOR")) {
                                                    if (packageOrLotId == -1 || isTenPackageWise) {%>
                                    <%--<script type="text/javascript">
                                                function confirmation(formid){
                                                    jConfirm('Do you really want to Copy this form? ', 'Copy form confirmation', function (ans) {
                                                        if (ans)
                                                           window.location='<%=request.getContextPath()%>/CreateTenderFormSrvt?action=dumpForm&tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId='+formid+'&porlId=<%= packageOrLotId%>';


                                                    });
                                                }
                                            </script>--%>
                                                    <tr>
                            <td class="t-align-center"><%= disp%></td>
                                    <td colspan="2" class="t-align-left">
                                <%= tts.getSectionName()%>
                                        <%

                                            String folderName = pdfConstant.TENDERDOC;
                                 String pdfId = tenderId + "_" + tts.getTenderSectionId() + "_" + tenderStdId;
                                if (b_publish_link) {
                                        %>
                                            <a class="action-button-savepdf"
                                               href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=section<%=disp%>_<%=tts.getSectionName()%>.pdf&folderName=<%=str_FolderName%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&tenderId=<%=tenderId%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&officerPage=true">Save As PDF </a>
                                    &nbsp;<%--<a class="action-button-edit"
                                         href="GenerateTendDocFreshPDF.jsp?Id=<%=Integer.parseInt(id)%>&tenderid=<%=tenderId%>&folderName=<%=folderName%><%if(!isPackageWise){%>&porlId=<%=packageOrLotId%><%}%>">Generate fresh PDF</a>--%>

                                <% }%>
                                        <table width="100%" cellspacing="5" cellpadding="0" border="0">
                                            <tr>
                                                <td  width ="30%" class="ff">
                                                    Tender forms
                                                </td>
                                                <td class="txt_2" colspan="2"> Data and Sequence of data in Tech form and BOQ/Schedule of Price Bid forms should be same to avoid any confusion to Bidder/Consultant.
                                                </td>
                                            </tr>
                                    <%
                                                                                     if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                             || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false || (corriCrtNPending && isInitiater && ispublish)) {%>
<!--                                            <tr>
                                                <td width="20%" class="ff">New forms :</td>
                                                <td width="40%" ><%//if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){%><a href="CreateTenderForm.jsp?tenderId=<%//= tenderId%>&sectionId=<%//= tts.getTenderSectionId()%>&porlId=<%//= packageOrLotId%>">New Forms Preparation</a> <%//if(("works".equalsIgnoreCase(strProcNature))&&(("Bill of Quantities".equalsIgnoreCase(tts.getSectionName())))){%> | <a href="javascript:void(0);" Onclick="askConfirmation('<%//= tts.getTenderSectionId()%>','BOQfixed','<%//= corriCrtNPending %>');">Create Fixed Rate BOQ Form |</a><br/><a href="javascript:void(0);" Onclick="askConfirmationForSalvage('<%//= tts.getTenderSectionId()%>','BOQsalvage','<%//= corriCrtNPending %>');">Create BOQ for salvage works</a><%//}}%> </td>
                                                <td class="txt_2"><%//if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){%>To create any additional supplementary forms, click on this link.<%//}%></td>
                                            </tr>-->
                                    <% }%>
                                        </table>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                <th width="4%" class="t-align-center">Sl. <br> No.</th>
                    <th class="t-align-center" width="30%">File Name</th>
                    <th class="t-align-center" width="38%">File Description</th>
                    <th class="t-align-center" width="10%">File Size <br />
                        (in KB)</th>
                    <th class="t-align-center" width="18%">Action</th>
                                               <%

                    String tender = request.getParameter("tenderId");
                                                                        String sectionId = tts.getTenderSectionId() + "";

                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderSectionDocInfo", sectionId, "")) {
                                                        docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                         <td class="t-align-center">
                                           <%-- <%if (sptcd.getFieldName5().trim().equals("0")) {%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tender=<%=tender%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                                <%} else {%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?templateSectionDocId=<%=sptcd.getFieldName5()%>&sectionId=<%=sptcd.getFieldName5()%>&docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%}%>--%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&officerPage=true" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                         </td>
                    </tr>
 <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                                    <%   if (sptcd != null) {
                        sptcd = null;
                    }
                    }%>


                                            </table>
                                        <%
                                            java.util.List<com.cptu.egp.eps.model.table.TblTenderForms> forms = dDocSrBean.getTenderFormPackage(tts.getTenderSectionId());
                                        %>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th style="text-align:center; width:4%;">Sl. No.</th>
                                                <th width="33%">Form Name 1</th>
                                                <th width="63%">Actions</th>
                                            </tr>
                                            <%
                                                                            if (forms != null) {
                                                                                if (forms.size() != 0) {
                                                    boolean isFrmCanceled = false;
                                                    boolean isCorriForm = false;
                                                                                    for (int jj = 0; jj < forms.size(); jj++) {
                                                                                        boolean tableExists = true;
                                                                                        int checkFormId = Integer.parseInt(String.valueOf(forms.get(jj).getTenderFormId()));
                                                                                        TenderCommonService tcsFormCheck = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                                                        List<Object[]> tableIds = null;
                                                                                        tableIds = tcsFormCheck.checkForm(checkFormId);
                                                                                        if(!tableIds.isEmpty())
                                                                                        {
                                                                                           Object obj2 = null;
                                                                                           for(int i=0; i<tableIds.size(); i++)
                                                                                           {
                                                                                               obj2 = tableIds.get(i);
                                                                                               List<Object[]> countTable = null;
                                                                                               countTable = tcsFormCheck.checkTable(Integer.parseInt(obj2.toString()));
                                                                                               if(countTable == null || countTable.isEmpty())
                                                                                               {
                                                                                                   tableExists = false;
                                                                                               }
                                                                                               
                                                                                           }
                                                                                           tableIds.clear();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            tableExists = false;
                                                                                        }
                                                                                        if("services".equalsIgnoreCase(strProcNature)){
                                                                                            String tempFormId = String.valueOf(forms.get(jj).getTenderFormId());
                                                                                            List<Object> list =tenderFrmSrBean.getFormType(Integer.parseInt(tempFormId.replace("-","")));
                                                                                                formType = "";
                                                                                                if(list!=null && !list.isEmpty()){
                                                                                                    formType = (list.get(0)).toString();
                                                                                                }
                                                                                        }
                                                        isFrmCanceled = false;
                                                        isCorriForm = false;
                                            %>

                                                    <tr>
                                        <td style="text-align:center; width:4%;"><%=(jj + 1)%></td>
                                                        <td width="33%">
                                                            <%
                                                                                                    out.print(forms.get(jj).getFormName().replace("?s", "'s"));
                                                                                                    if (forms.get(jj).getFormStatus() != null) {
                                                                                                        if ("cp".equals(forms.get(jj).getFormStatus().toString()) || "c".equals(forms.get(jj).getFormStatus().toString())) {
                                                                        isFrmCanceled = true;
                                                                        out.print("  <font color='red'>(Form Canceled)</font>");
                                                                    }if("createp".equalsIgnoreCase(forms.get(jj).getFormStatus().toString())){
                                                                        isCorriForm = true;
                                                                    }
                                                                }
                                                            
                                                                if(tableExists)
                                                                {%>
                                                                    <img class="alertImg" id="DocumentUploadDone" src="../resources/images/done.ico" border="0" title="Form Ready" style="vertical-align:middle;" />
                                    
                                                                <%}
                                                                else{%>
                                                                    <img class="alertImg" id="DocumentUploadPending"  src="../resources/images/Pending.png" border="0" title="Form is not prepared yet." style="vertical-align:middle;" />

                                                                <%}%>
                                                        </td>
                                            <%--<script type="text/javascript">
                                                function confirmation(formid){
                                                    alert(formid);
                                                    jConfirm('Do you really want to Copy this form? ', 'Copy form confirmation', function (ans) {
                                                        if (ans)
                                                           window.location='<%=request.getContextPath()%>/CreateTenderFormSrvt?action=dumpForm&tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId='+formid+'&porlId=<%= packageOrLotId%>';


                                                    });
                                                }
                                            </script>--%>
                                            <td width="63%" style="line-height: 1.75;">
                                            <%  boolean bShowEditLink = true;
                                                if(forms.get(jj).getTemplateFormId() != 0 && (!"Works".equalsIgnoreCase(strProcNature) ||("Works".equalsIgnoreCase(strProcNature) && "No".equalsIgnoreCase(forms.get(jj).getIsPriceBid()))) && !"20".equalsIgnoreCase(formType)){
                                                    bShowEditLink = false;
                                                }

                                                if((!"17".equalsIgnoreCase(formType)) && "Services".equalsIgnoreCase(strProcNature) && corriCrtNPending && isCorriForm){
                                                    bShowEditLink = true;
                                                }
                                            if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                            if (((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                         || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false)|| (corriCrtNPending && isCorriForm)) {
                                            if(bShowEditLink) {%>
                                            <a href="CreateTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">Edit </a>&nbsp; | &nbsp;
                                            <%}%>
                                            <%if (forms.get(jj).getTemplateFormId() > 0) {
                                                if((!"17".equalsIgnoreCase(formType)) && "Services".equalsIgnoreCase(strProcNature) && corriCrtNPending && isCorriForm){
                                            %>
                                                    <a href="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=delForm&tenderId=<%= tenderId%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>&pn=<%=isSerivce%>&corriId=<%=corriId%>" onclick="return delTenderFormConfirm('<%= tenderId%>','<%= forms.get(jj).getTenderFormId()%>','<%= corriId%>');" >Delete</a>&nbsp; | &nbsp;
                                            <%
                                                }
                                            } else {%>

                                            <a href="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=delForm&tenderId=<%= tenderId%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>&pn=<%=isSerivce%>&corriId=<%=corriId%>" onclick="return delTenderFormConfirm('<%= tenderId%>','<%= forms.get(jj).getTenderFormId()%>','<%= corriId%>');" >Delete</a>&nbsp; | &nbsp;
                                                                <%}%>

                                            <a href="TenderTableDashboard.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">Form Dashboard</a> &nbsp; | &nbsp;
                                            <% }}%>
                                            <a href="ViewTenderCompleteForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">View Form</a>
                                            &nbsp; | &nbsp;
                                                            <%                              if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                                                                    if (((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false)|| (corriCrtNPending && isCorriForm)) {
                                                            %>

                                            <a href="DefineMandatoryDocs.jsp?tId=<%= tenderId%>&fId=<%= forms.get(jj).getTenderFormId()%><%if(!isPackageWise){%>&porlId=<%=packageOrLotId%><%}%>">Prepare Required Document List</a>
                                            &nbsp; | &nbsp;<br/>
                                            <%}}%>

                                            <a href="ViewMandatoryDocs.jsp?tId=<%= tenderId%>&fId=<%= forms.get(jj).getTenderFormId()%><%if(!isPackageWise){%>&porlId=<%=packageOrLotId%><%}%>&isPublished=<%=ispublish%>&isPublished=<%=ispublish%>">View Required Document List</a>
                                            &nbsp; | &nbsp;
                                            <%                                              if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                                                                    if (((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false)|| corriCrtNPending) {
                                                                                                        if(!("12".equalsIgnoreCase(formType) || "13".equalsIgnoreCase(formType) || "14".equalsIgnoreCase(formType) || "15".equalsIgnoreCase(formType) || "16".equalsIgnoreCase(formType) || "18".equalsIgnoreCase(formType) || "19".equalsIgnoreCase(formType))){
                                            %>
                                            <a href="#" onClick="confirmation('<%= forms.get(jj).getTenderFormId()%>' , '<%= tts.getTenderSectionId()%>' , '<%=packageOrLotId%>');">Copy this Form</a>
                                            &nbsp; | &nbsp;
                                            <% }/*Condition as per bug 5311*/}}%>

                                            <a href="TestTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">Test Form</a>

                                            <%if(!checkuserRights.isEmpty()){
                                            if (corriCrtNPending && (!isFrmCanceled) && !isCorriForm) {%>

                                            &nbsp; | &nbsp;
                                            <a href="javascript:void(0);" onclick="cancelForm('<%= tts.getTenderSectionId()%>','<%= forms.get(jj).getTenderFormId()%>','<%=corriId%>','<%= tenderId%>');">Cancel Form</a>
                                            <% }}%>
                                                            <%                              if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                                                                    if (((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false)|| (corriCrtNPending && isCorriForm)) {

                                                                                                        if (forms.get(jj).getTemplateFormId() == 0 || "13".equalsIgnoreCase(formType) || "14".equalsIgnoreCase(formType) || "15".equalsIgnoreCase(formType) || "16".equalsIgnoreCase(formType)){
                                                            %>
                                            &nbsp; | &nbsp;<a href="<%=request.getContextPath()%>/officer/CreateTenderCombo.jsp?formId=<%= forms.get(jj).getTenderFormId()%>&tenderId=<%= tenderId%>&porlId=<%= packageOrLotId%>">Create Combo</a>
                                            <% }}
                                            }%>
                                            <%
                                                long lng = tenderercomboservice.getComboCountFromTenderer(forms.get(jj).getTenderFormId());
                                                if(lng!=0)
                                                {
                                            %>
                                            &nbsp; | &nbsp;<a href="<%=request.getContextPath()%>/officer/ViewTenderCombo.jsp?templetformid=<%= forms.get(jj).getTenderFormId()%>&tenderId=<%= tenderId%>&mode=view<%if(!isPackageWise){%>&porlId=<%=packageOrLotId%><%}%>"TARGET = "_blank">View Combo</a>
                                                            <%}%>
                                                        </td>
                                                    </tr>
                                            <%
                                                    }
                                                }
                                                forms = null;
                                            }
                                            %>
                                        </table>
                                        <%

                                                                for (TblTenderLotSecurity ttls : tenderDocumentSrBean.getLotList(tenderId)) {
                                                                                if(tenderDocumentSrBean.getTenderFormLot(tts.getTenderSectionId(), ttls.getAppPkgLotId()).size() > 0) {
                                        %>
                                        <table width="100%" cellspacing="5" cellpadding="0" border="0" class="t_space">
                                            <% if(tts.getContentType().equalsIgnoreCase("form")){ %>
                                            <tr>
                                                <td width="15%" class="ff"><% if("services".equalsIgnoreCase(strProcNature) && packageOrLotId==-1){ out.print("Package No."); }else{ out.print("Lot No."); } %></td>
                                                <td width="85%"><%=ttls.getLotNo()%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff" nowrap><% if("services".equalsIgnoreCase(strProcNature) && packageOrLotId==-1){ out.print("Package Description"); }else{ out.print("Lot Description"); } %></td>
                                                <td><%=ttls.getLotDesc()%></td>
                                            </tr>
                                            <% } %>

                                        </table>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th style="text-align:center; width:4%;">Sl. No.</th>
                                                <th width="33%">Form Name 2</th>
                                                <th width="63%">Actions</th>
                                            </tr>
                                    <%
                                        /* Dohatec: ICT-Goods Start */
                                        //to check Currencies already added or not
                                        int iTenderId = Integer.parseInt(request.getParameter("tenderId"));
                                        TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
                                        long totalCurrencyAdded = tenderCurrencyService.getCurrencyCountForTender(iTenderId);

                                        //Procurement Type (i.e. NCT/ICT) retrieval
                                        List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                        boolean isIctTender = false;
                                        if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                                            isIctTender = true;
                                        }
                                        /* Dohatec: ICT-Goods End */

                                        int jj = 0;
                                        boolean isCorriForm = false;
                                        int tSecId = tts.getTenderSectionId();
                                        int tPId = ttls.getAppPkgLotId();
                                        List<TblTenderForms> tnForms = null;
                                        if (tPId != 0) {
                                            tnForms = grandSummary.getTenderPriceBidForm(tSecId, tPId);
                                        } else {
                                            tnForms = grandSummary.getTenderPriceBidForm(tSecId);
                                        }
                                        
                                        List<TblTenderForms> tblTenderFormsList = tenderDocumentSrBean.getTenderFormLot(tts.getTenderSectionId(), ttls.getAppPkgLotId());
                                        if("Goods".equalsIgnoreCase(strProcNature))
                                        {
                                            List<TblTenderForms> tblTenderMainFormsList = new ArrayList<TblTenderForms>();
                                            List<TblTenderForms> tblTenderDiscountFormsList = new ArrayList<TblTenderForms>();
                                            List<TblTenderForms> tblTenderCopiedFormsList = new ArrayList<TblTenderForms>();
                                            for(TblTenderForms tblTenderForms : tblTenderFormsList)
                                            {
                                                if(tblTenderForms.getTemplateFormId() == 0)
                                                {
                                                    tblTenderCopiedFormsList.add(tblTenderForms);
                                                }
                                                else
                                                {
                                                    if(tblTenderForms.getFormType().equalsIgnoreCase("BOQsalvage"))
                                                    {
                                                        tblTenderDiscountFormsList.add(tblTenderForms);
                                                    }
                                                    else
                                                    {
                                                        tblTenderMainFormsList.add(tblTenderForms);
                                                    }
                                                }
                                            }
                                            
                                            int listSize = tblTenderMainFormsList.size();
                                            int lv1=0,lv2=0;
                                            for(lv1=0; lv1<listSize; lv1++)
                                            {
                                                for(lv2=0; lv2<(listSize-(lv1+1)); lv2++)
                                                {
                                                    int compare = tblTenderMainFormsList.get(lv2).getFormName().compareTo(tblTenderMainFormsList.get(lv2+1).getFormName());
                                                    if(compare > 0)
                                                    {
                                                        TblTenderForms temp = tblTenderMainFormsList.get(lv2);
                                                        tblTenderMainFormsList.set(lv2, tblTenderMainFormsList.get(lv2+1));
                                                        tblTenderMainFormsList.set(lv2+1, temp);
                                                    }
                                                }
                                            }
                                            
                                            listSize = tblTenderDiscountFormsList.size();
                                            for(lv1=0; lv1<listSize; lv1++)
                                            {
                                                for(lv2=0; lv2<(listSize-(lv1+1)); lv2++)
                                                {
                                                    int compare = tblTenderDiscountFormsList.get(lv2).getFormName().compareTo(tblTenderDiscountFormsList.get(lv2+1).getFormName());
                                                    if(compare > 0)
                                                    {
                                                        TblTenderForms temp = tblTenderDiscountFormsList.get(lv2);
                                                        tblTenderDiscountFormsList.set(lv2, tblTenderDiscountFormsList.get(lv2+1));
                                                        tblTenderDiscountFormsList.set(lv2+1, temp);
                                                    }
                                                }
                                            }
                                            
                                            listSize = tblTenderCopiedFormsList.size();
                                            for(lv1=0; lv1<listSize; lv1++)
                                            {
                                                for(lv2=0; lv2<(listSize-(lv1+1)); lv2++)
                                                {
                                                    int compare = tblTenderCopiedFormsList.get(lv2).getFormName().compareTo(tblTenderCopiedFormsList.get(lv2+1).getFormName());
                                                    if(compare > 0)
                                                    {
                                                        TblTenderForms temp = tblTenderCopiedFormsList.get(lv2);
                                                        tblTenderCopiedFormsList.set(lv2, tblTenderCopiedFormsList.get(lv2+1));
                                                        tblTenderCopiedFormsList.set(lv2+1, temp);
                                                    }
                                                }
                                            }
                                            
                                            tblTenderFormsList.clear();
                                            tblTenderFormsList.addAll(tblTenderMainFormsList);
                                            tblTenderFormsList.addAll(tblTenderDiscountFormsList);
                                            tblTenderFormsList.addAll(tblTenderCopiedFormsList);
                                        }
                                        
                                        for (TblTenderForms tblTenderForms : tblTenderFormsList) {%>
                                        <%
                                            boolean tableExists2 = true;
                                            boolean tablePrepared = false;
                                            int checkFormId2 = tblTenderForms.getTenderFormId();
                                            TenderCommonService tcsFormCheck2 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                            List<Object[]> tableIds2 = null;
                                            tableIds2 = tcsFormCheck2.checkForm(checkFormId2);
                                            if(!tableIds2.isEmpty())
                                            {
                                               Object obj3 = null;
                                               for(int i=0; i<tableIds2.size(); i++)
                                               {
                                                   obj3 = tableIds2.get(i);
                                                   List<Object[]> countTable2 = null;
                                                   countTable2 = tcsFormCheck2.checkTable(Integer.parseInt(obj3.toString()));
                                                   if(countTable2 == null || countTable2.isEmpty())
                                                   {
                                                       tableExists2 = false;
                                                   }

                                               }
                                               tableIds2.clear();
                                            }
                                            else
                                            {
                                                tableExists2 = false;
                                            }
                                            if (tnForms != null) 
                                            {
                                                if (tnForms.size() > 0) 
                                                {
                                                    for (int fv = 0; fv < tnForms.size(); fv++) 
                                                    {
                                                        if(tnForms.get(fv).getTenderFormId() == checkFormId2)
                                                        {
                                                            tablePrepared = true;
                                                        }
                                                    }
                                                }
                                            }
                                            if("services".equalsIgnoreCase(strProcNature)){
                                                List<Object> list =tenderFrmSrBean.getFormType(tblTenderForms.getTenderFormId());
                                                formType = "";
                                                if(list!=null && !list.isEmpty()){
                                                    formType = (list.get(0)).toString();
                                                }
                                            }
                                            boolean isFrmCanceled = false;
                                            isFrmCanceled = false;
                                            isCorriForm = false;
                                        %>
                                            <tr>
                                                <td style="text-align:center; width:4%;"><%=(jj += 1)%></td>
                                                <td width="33%">
                                                    <%
                                                    out.print(tblTenderForms.getFormName().replace("?s", "'s"));
                                                    if (tblTenderForms.getFormStatus() != null) {
                                                        if ("cp".equals(tblTenderForms.getFormStatus().toString()) || "c".equals(tblTenderForms.getFormStatus().toString())) {
                                                            isFrmCanceled = true;
                                                            out.print("  <font color='red'>(Form Canceled)</font>");
                                                                    }if("createp".equalsIgnoreCase(tblTenderForms.getFormStatus().toString())){
                                                            isCorriForm = true;
                                                        }
                                                    }
                                                    %>
                                                    
                                                    <%if(tablePrepared)
                                                    {%>
                                                    <label style="color:red">*</label>
                                                    <%}%>
                                                    
                                                    <%if(tableExists2)
                                                    {%>
                                                        <img class="alertImg" id="filledUp" src="../resources/images/done.ico" border="0" title="Form Ready" style="vertical-align:middle;" />

                                                    <%}
                                                    else{%>
                                                        <img class="alertImg" id="notFilledUp"  src="../resources/images/Pending.png" border="0" title="Form is not prepared yet." style="vertical-align:middle;" />
                                   
                                                    <%}%>
                                                    
                                                    
                                                </td>
                                                <td width="63%" style="line-height: 1.75">
                                                <%
                                                    boolean bShowEditLink = true;
                                                    if(tblTenderForms.getTemplateFormId() != 0 && (!"Works".equalsIgnoreCase(strProcNature) ||("Works".equalsIgnoreCase(strProcNature) && "No".equalsIgnoreCase(tblTenderForms.getIsPriceBid()))) && !"20".equalsIgnoreCase(formType)){
                                                        bShowEditLink = false;
                                                    }

                                                    if((!"17".equalsIgnoreCase(formType)) && "Services".equalsIgnoreCase(strProcNature) && corriCrtNPending && isCorriForm){
                                                        bShowEditLink = true;
                                                    }

                                                    if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                        if (((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                                || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) || (corriCrtNPending && isCorriForm)) {
                                                            /* Dohatec: ICT-Goods Start */
                                                            if(isIctTender && "Goods".equalsIgnoreCase(strProcNature) && "Yes".equalsIgnoreCase(tblTenderForms.getIsPriceBid()) && totalCurrencyAdded <= 1){ //for PriceBid forms of goods - ICT, at least one foreign currency must be added with BTN to proceed
                                                                out.print("<span style='color:red'>Foreign Currency not yet added</span>");
                                                            }   /* Dohatec: ICT-Goods End */
                                                            else {
                                                                if(bShowEditLink){
                                                                %>
                                                                <a href="CreateTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= tblTenderForms.getTenderFormId()%>&porlId=<%= packageOrLotId%>">Edit</a>&nbsp; | &nbsp;
                                                                <%}%>
                                                                <% if (tblTenderForms.getTemplateFormId() != 0) {
                                                                    if((!"17".equalsIgnoreCase(formType)) && "Services".equalsIgnoreCase(strProcNature) && corriCrtNPending && isCorriForm){
                                                                %>
                                                                    <a href="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=delForm&tenderId=<%= tenderId%>&formId=<%= tblTenderForms.getTenderFormId()%>&porlId=<%= packageOrLotId%>&pn=<%=isSerivce%>&corriId=<%=corriId%>" onclick="return delTenderFormConfirm('<%= tenderId%>','<%= tblTenderForms.getTenderFormId()%>','<%= corriId%>');" >Delete</a>&nbsp; | &nbsp;
                                                                <%
                                                                    }
                                                                  } else {%>
                                                                    <a href="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=delForm&tenderId=<%= tenderId%>&formId=<%= tblTenderForms.getTenderFormId()%>&porlId=<%= packageOrLotId%>&pn=<%=isSerivce%>&corriId=<%=corriId%>" onclick="return delTenderFormConfirm('<%= tenderId%>','<%= tblTenderForms.getTenderFormId()%>','<%= corriId%>');" >Delete</a>&nbsp; | &nbsp;
                                                                    <%}%>

                                                                <a href="TenderTableDashboard.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= tblTenderForms.getTenderFormId()%>&porlId=<%= packageOrLotId%>">Form Dashboard</a> &nbsp; | &nbsp;
                                                            <%}
                                                        }
                                                    }
                                                %>
                                            <a href="ViewTenderCompleteForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%=tblTenderForms.getTenderFormId()%>&porlId=<%= packageOrLotId%>">View Form</a>
                                            &nbsp; | &nbsp;
                                                            <%                  if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                                                            if (((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                    || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false)|| (corriCrtNPending&& isCorriForm)) {
                                                            %>

                                            <a href="DefineMandatoryDocs.jsp?tId=<%= tenderId%>&fId=<%= tblTenderForms.getTenderFormId()%><%if(!isPackageWise){%>&porlId=<%=packageOrLotId%><%}%>">Prepare Required Document List</a>
                                            &nbsp; | &nbsp;<br/>
                                            <%}}%>
                                            <a href="ViewMandatoryDocs.jsp?tId=<%= tenderId%>&fId=<%= tblTenderForms.getTenderFormId()%><%if(!isPackageWise){%>&porlId=<%=packageOrLotId%><%}%>&isPublished=<%=ispublish%>">View Required Document List</a>
                                            &nbsp; | &nbsp;
                                                            <%                  if(!checkuserRights.isEmpty() && !"Discount Form".equalsIgnoreCase(tblTenderForms.getFormName()) && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                                                            if (((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                    || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) || corriCrtNPending) {
                                                                                                if(!("12".equalsIgnoreCase(formType) || "13".equalsIgnoreCase(formType) || "14".equalsIgnoreCase(formType) || "15".equalsIgnoreCase(formType) || "16".equalsIgnoreCase(formType) || "18".equalsIgnoreCase(formType) || "19".equalsIgnoreCase(formType))){
                                                            %>

                                            <a href="#" onclick="confirmation('<%=tblTenderForms.getTenderFormId()%>','<%= tts.getTenderSectionId()%>','<%=packageOrLotId%>');">Copy this Form</a>
                                            &nbsp; | &nbsp;
                                            <%}/*as per bug 5311*/ }}%>

                                            <a href="TestTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= tblTenderForms.getTenderFormId()%>&porlId=<%= packageOrLotId%>">Test Form</a>
                                            <%if(!checkuserRights.isEmpty()){
                                            if (corriCrtNPending && (!isFrmCanceled) && (!isCorriForm) && !"Discount Form".equalsIgnoreCase(tblTenderForms.getFormName())) {%>
                                            &nbsp; | &nbsp;
                                            <a href = "javascript:void(0);" onclick="cancelForm('<%= tts.getTenderSectionId()%>','<%= tblTenderForms.getTenderFormId()%>','<%=corriId%>','<%=tenderId%>');">Cancel Form</a>
                                            <% }}%>
                                                            <%                  if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                                                            if (((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                    || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false)|| (corriCrtNPending && isCorriForm)) {

                                                                                                if (tblTenderForms.getTemplateFormId() == 0 || "13".equalsIgnoreCase(formType) || "14".equalsIgnoreCase(formType) || "15".equalsIgnoreCase(formType) || "16".equalsIgnoreCase(formType)) {
                                                            %>
                                            &nbsp; | &nbsp;<a href="<%=request.getContextPath()%>/officer/CreateTenderCombo.jsp?formId=<%= tblTenderForms.getTenderFormId()%>&tenderId=<%= tenderId%>&porlId=<%= packageOrLotId%>">Create Combo</a>
                                            <% }}
                                            }%>
                                            <%
                                                long lng = tenderercomboservice.getComboCountFromTenderer(tblTenderForms.getTenderFormId());
                                                if(lng!=0)
                                                {
                                            %>
                                            &nbsp; | &nbsp;<a href="<%=request.getContextPath()%>/officer/ViewTenderCombo.jsp?templetformid=<%= tblTenderForms.getTenderFormId()%>&tenderId=<%= tenderId%>&mode=view<%if(!isPackageWise){%>&porlId=<%=packageOrLotId%><%}%>"TARGET = "_blank">View Combo</a>
                                                            <%}%>
                                                        </td>
                                                    </tr>
                                    <% }%>
                                    </table>
                                    <%
                                    /* Dohatec Start */
                                    //if(!isIctTender)
                                    {
                                    %>
                                    <table width="100%" cellspacing="5" cellpadding="0" class="t_space" border="0">
                                        <tr>
                                            <td class="ff" nowrap width="17%">Grand summary : </td>
                                            <td width="83%">
                        <%
                            int isGSCreatedId = -1;
                            isGSCreatedId = grandSummary.isGrandSummaryCreated(tenderId,ttls.getAppPkgLotId());
                            //out.println(" isGSCreatedId " + isGSCreatedId + " ispublish " + ispublish);
                            if (isGSCreatedId != -1)
                            {
                                                    %>
                                        <%if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString()))
                                        {
                                            if (
                                                (fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                ||
                                                (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish))
                                                || iswfLevelExist == false
                                                || corriCrtNPending==true //(Also Open edit link for grand summary if corrigendum is pending)
                                               )
                                               { %>
                                                    <a href="CreateGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&porlId=<%= ttls.getAppPkgLotId()%>&gsId=<%= isGSCreatedId%>&corriId=<%= corriId%>">Edit</a> &nbsp;|&nbsp;
                                            <% }
                                        }%>
                                        <a href="ViewGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&gsId=<%= isGSCreatedId%>&corriId=<%= corriId%>">View</a>
                            <%
                            }
                            else
                            {
                                if (!ispublish)
                                {
                                    if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                    if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                             || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                                        %>
                                            <a href="CreateGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&porlId=<%= ttls.getAppPkgLotId()%>&std=package">Create</a>
                                        <%
                                    }}
                                    if (isGSCreatedId != -1)
                                    {
                                    %>
                                        <a href="ViewGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&gsId=<%= isGSCreatedId%>">View</a>
                                    <%
                                    }
                                    %>

                <%

                                }
                                else
                                {
                                    //out.println("corriCrtNPending="+corriCrtNPending);
                                    if(corriCrtNPending)
                                    {
                                        if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){                                  
                %>
                                        <a href="CreateGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&porlId=<%= ttls.getAppPkgLotId()%>&std=package&corriId=<%= corriId%>">Create</a>
                <%                      }
                                    }
                                    else
                                    {
                                         out.print("Not Prepared");
                                    }

                                }
                        }
            %>
                                            </td>
                                        </tr>
                                    </table>
                                            <%}%>
                                <% }}%>
                                    </td>
                                    </tr>
                        <%
                              }
                                else {%>
                                    <tr>
                            <td class="t-align-center"><%= disp%></td>
                                    <td colspan="2" class="t-align-left">
                                <%= tts.getSectionName()%>
                                        <%
                                                                        if (b_publish_link) {
                                            String folderName = pdfConstant.TENDERDOC;
                                                                            String pdfId = tenderId + "_" + tts.getTenderSectionId() + "_" + tenderStdId;
                                        %>
                                            <a class="action-button-savepdf"
                                                href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=section<%=disp%>_<%=tts.getSectionName()%>.pdf&folderName=<%=str_FolderName%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&tenderId=<%=tenderId%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&officerPage=true">Save As PDF </a>

                                <% }%>
                                        <table width="100%" cellspacing="5" cellpadding="0" border="0">
                                            <tr>
                                                <td width="30%" class="ff">
                                                    Tender forms
                                                </td>
                                                <td class="txt_2" colspan="2"> Data and Sequence of data in Tech form and BOQ/Schedule of Price Bid forms should be same to avoid any confusion to Bidder/Consultant.
                                                </td>
                                            </tr>
                                            <%--
                                                /*This method is called for cheking wether the tender is service or not*/
                                            String subCriteria  = "Configure Marks for Technical Evaluation";
                                                List<TblTenderDetails> tblTenderDetail = procurementNatureBean.getProcurementNature(tenderId);
                                                                            if (tblTenderDetail.get(0).getProcurementNature().equalsIgnoreCase("service")) {
                                                        isSerivce = true;
                                                        if("reoi".equalsIgnoreCase(tblTenderDetail.get(0).getProcurementMethod())){
                                                            subCriteria  = "Configure Evaluation Criteria and Sub Criteria";
                                                        }
                                                    }
                                                        if (isSerivce) {
                                            %>
                                            <tr>
                                                <td width="30%" class="ff" nowrap><%=subCriteria%> :</td>
                                                <%
                                                                                                    if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                                                %>
                                                <td width="70%" colspan="2"><a href="EvalCertiService.jsp?tenderId=<%= tenderId%>">Configure</a> | <a href="EvalCertiServiceView.jsp?tenderId=<%= tenderId%>">View</a></td>
                                        <%}/*Condtion for View and Insert Link*/ else {%>
                                        <td width="70%" colspan="2"><a href="EvalCertiServiceView.jsp?tenderId=<%= tenderId%>">View</a></td>
                                                <%}%>
                                            </tr>
                                             <%}/*Is service codintion ends*/--%><%
                                                                                     if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                             || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false || (corriCrtNPending && isInitiater && ispublish)) {%>
<!--                                            <tr>
                                                <td width="30%" class="ff">New forms :</td>
                                                <td width="20%"><%if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){%><a href="CreateTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&porlId=<%= packageOrLotId%>">New Forms Preparation</a><%}%></td>
                                                <td class="txt_2"><%if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){%>To create any additional supplementary forms, click on this link.<%}%></td>
                                                </td>
                                            </tr>-->
                                    <% }%>
                                  <%--   <tr>
                                                <td width="15%" class="ff">Lot No.</td>
                                        <td width="85%"><%=lotNo%></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Lot Description</td>
                                        <td ><%=lotDesc%></td>
                                            </tr>
                                  --%>

                                  <%
                                  if(tenderDocumentSrBean.getTenderFormLot(tts.getTenderSectionId(), packageOrLotId).size() > 0){
                                      %>

                                    <tr>
                                                <td class="ff" nowrap>Grand summary : </td>
                                                <td colspan="2">
                                                    <%
                                                    int isGSCreatedId = -1;
                                                    isGSCreatedId = grandSummary.isGrandSummaryCreated(tenderId,packageOrLotId);
                                                    //out.println(" isGSCreatedId " + isGSCreatedId + " ispublish " + ispublish);
                                                                                if (isGSCreatedId != -1) {
                                                        %>
                                            <%if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                            if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                         || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {%>
                                            <a href="CreateGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&gsId=<%= isGSCreatedId%>&porlId=<%= packageOrLotId%>">Edit</a> &nbsp;|&nbsp;
                                            <% }}%>
                                            <a href="ViewGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&gsId=<%= isGSCreatedId%>&porlId=<%= packageOrLotId%>">View</a>
                                                        <%
                                                                                                } else {
                                                                                                    if (!ispublish) {
                                                                                                       if(!checkuserRights.isEmpty()){
                                                                                                           if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                        %>
                                            <a href="CreateGrandSummary.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&porlId=<%= packageOrLotId%>">Create</a>
                                                        <%
                                                                                      } } } else {
                                                            out.print("Not Prepared");
                                                        }
                                                    }
                                                    %>
                                                </td>
                                            </tr>
                                            <%}%>
                                            <%--<tr>
                                                <td class="ff">Mandatory forms : </td>
                                                <td><a href="#">Configure</a></td>
                                            </tr>--%>
                                        </table>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                <th width="4%" class="t-align-center">Sl. No.</th>
                    <th class="t-align-center" width="30%">File Name</th>
                    <th class="t-align-center" width="38%">File Description</th>
                    <th class="t-align-center" width="10%">File Size <br />
                        (in KB)</th>
                    <th class="t-align-center" width="18%">Action</th>
                                               <%

                    String tender = request.getParameter("tenderId");
                                                                        String sectionId = tts.getTenderSectionId() + "";

                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderSectionDocInfo", sectionId, "")) {
                                                        docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                         <td class="t-align-center">
                                           <%-- <%if (sptcd.getFieldName5().trim().equals("0")) {%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tender=<%=tender%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                                <%} else {%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?templateSectionDocId=<%=sptcd.getFieldName5()%>&sectionId=<%=sptcd.getFieldName5()%>&docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%}%>--%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&officerPage=true" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                         </td>
                    </tr>
 <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                                    <%   if (sptcd != null) {
                        sptcd = null;
                    }
                    }%>


                                            </table>
                                        <%
                                            java.util.List<com.cptu.egp.eps.model.table.TblTenderForms> forms = dDocSrBean.getTenderForm(tts.getTenderSectionId());
                                        %>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th align="center" style="text-align:center; width:4%;">Sl. No.</th>
                                                <th width="33%">Form Name 3</th>
                                                <th width="63%">Actions</th>
                                            </tr>
                                            <%
                                                                            if (forms != null) {
                                                                                if (forms.size() != 0) {
                                                    boolean isFrmCanceled = false;
                                                    boolean isCorriForm = false;
                                                                                    for (int jj = 0; jj < forms.size(); jj++) {
                                                                                        boolean tableExists3 = true;
                                                                                        int checkFormId3 = Integer.parseInt(String.valueOf(forms.get(jj).getTenderFormId()));
                                                                                        TenderCommonService tcsFormCheck3 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                                                                        List<Object[]> tableIds3 = null;
                                                                                        tableIds3 = tcsFormCheck3.checkForm(checkFormId3);
                                                                                        if(!tableIds3.isEmpty())
                                                                                        {
                                                                                           Object obj3 = null;
                                                                                           for(int i=0; i<tableIds3.size(); i++)
                                                                                           {
                                                                                               obj3 = tableIds3.get(i);
                                                                                               List<Object[]> countTable3 = null;
                                                                                               countTable3 = tcsFormCheck3.checkTable(Integer.parseInt(obj3.toString()));
                                                                                               if(countTable3 == null || countTable3.isEmpty())
                                                                                               {
                                                                                                   tableExists3 = false;
                                                                                               }
                                                                                               
                                                                                           }
                                                                                           tableIds3.clear();
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            tableExists3 = false;
                                                                                        }
                                                    if("services".equalsIgnoreCase(strProcNature)){
                                                        List<Object> list =tenderFrmSrBean.getFormType(forms.get(jj).getTemplateFormId());
                                                        formType = "";
                                                        if(list!=null && !list.isEmpty()){
                                                            formType = (list.get(0)).toString();
                                                        }
                                                    }
                                                        isFrmCanceled = false;
                                                        isCorriForm = false;
                                            %>
                                                    <tr>
                                        <td style="text-align:center; width:4%;"><%=(jj + 1)%></td>
                                                        <td width="33%">
                                                            <%
                                                                                                    out.print(forms.get(jj).getFormName().replace("?s", "'s"));
                                                                                                    if (forms.get(jj).getFormStatus() != null) {
                                                                                                        if ("cp".equals(forms.get(jj).getFormStatus().toString()) || "c".equals(forms.get(jj).getFormStatus())) {
                                                                        isFrmCanceled = true;
                                                                        out.print("  <font color='red'>(Form Canceled)</font>");
                                                                    }if("createp".equalsIgnoreCase(forms.get(jj).getFormStatus().toString())){
                                                                        isCorriForm = true;
                                                                    }
                                                                }
                                                            %>
                                                            <%if(tableExists3)
                                                            {%>
                                                                <img class="alertImg" id="filledUp2" src="../resources/images/done.ico" border="0" title="Form Ready" style="vertical-align:middle;" />

                                                            <%}
                                                            else{%>
                                                                <img class="alertImg" id="notFilledUp2"  src="../resources/images/Pending.png" border="0" title="Form is not prepared yet." style="vertical-align:middle;" />

                                                            <%}%>
                                                        </td>
                                                        <td width="63%">
                                            <%
                                                    boolean bShowEditLink = true;
                                                    if(forms.get(jj).getTemplateFormId() != 0 && (!"Works".equalsIgnoreCase(strProcNature) ||("Works".equalsIgnoreCase(strProcNature) && "No".equalsIgnoreCase(forms.get(jj).getIsPriceBid()))) && !"20".equalsIgnoreCase(formType)){
                                                        bShowEditLink = false;
                                                    }

                                                    if((!"17".equalsIgnoreCase(formType)) && "Services".equalsIgnoreCase(strProcNature) && corriCrtNPending && isCorriForm){
                                                        bShowEditLink = true;
                                                    }
                                            if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                            if (((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                                           || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) || (corriCrtNPending && isCorriForm)) {
                                                if(bShowEditLink){%>

                                            <a href="CreateTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">Edit</a>&nbsp; | &nbsp;
                                            <%}%>
                                            <%if (forms.get(jj).getTemplateFormId() > 0) {
                                                if((!"17".equalsIgnoreCase(formType)) && "Services".equalsIgnoreCase(strProcNature) && corriCrtNPending && isCorriForm){
                                            %>
                                                <a href="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=delForm&tenderId=<%= tenderId%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>&pn=<%=isSerivce%>&corriId=<%=corriId%>" onclick="return delTenderFormConfirm('<%= tenderId%>','<%= forms.get(jj).getTenderFormId()%>','<%= corriId%>');" >Delete</a>&nbsp; | &nbsp;
                                            <%
                                                }
                                                                      } else {%>

                                            <a href="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=delForm&tenderId=<%= tenderId%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>&pn=<%=isSerivce%>&corriId=<%=corriId%>" onclick="return delTenderFormConfirm('<%= tenderId%>','<%= forms.get(jj).getTenderFormId()%>','<%= corriId%>');" >Delete</a>&nbsp; | &nbsp;
                                                                <%}%>

                                            <a href="TenderTableDashboard.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">Form Dashboard</a> &nbsp; |
                                            <% }}%>
                                            <a href="ViewTenderCompleteForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">View Form</a>
                                            &nbsp; | &nbsp;
                                                            <%                          if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                                                                    if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                                                            %>

                                            <a href="DefineMandatoryDocs.jsp?tId=<%= tenderId%>&fId=<%= forms.get(jj).getTenderFormId()%><%if(!isPackageWise){%>&porlId=<%=packageOrLotId%><%}%>">Prepare Required Document List</a>
                                            &nbsp; | &nbsp;<br/>
                                            <%
                                            }}
                                            %>
                                            <a href="ViewMandatoryDocs.jsp?tId=<%= tenderId%>&fId=<%= forms.get(jj).getTenderFormId()%><%if(!isPackageWise){%>&porlId=<%=packageOrLotId%><%}%>&isPublished=<%=ispublish%>">View Required Document List</a>
                                            &nbsp; | &nbsp;
                                            <%                                  if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                                                                    if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                                                                                                        if(!("12".equalsIgnoreCase(formType) || "13".equalsIgnoreCase(formType) || "14".equalsIgnoreCase(formType) || "15".equalsIgnoreCase(formType) || "16".equalsIgnoreCase(formType) || "18".equalsIgnoreCase(formType) || "19".equalsIgnoreCase(formType))){

                                            %>

                                            <a href="#" onclick="confirmation('<%= forms.get(jj).getTenderFormId()%>','<%= tts.getTenderSectionId()%>','<%= packageOrLotId%>');">Copy this Form</a>
                                            &nbsp; | &nbsp;
                                                            <%}/*As per bug 5311*/
                                                            }}
                                                            %>

                                            <a href="TestTenderForm.jsp?tenderId=<%= tenderId%>&sectionId=<%= tts.getTenderSectionId()%>&formId=<%= forms.get(jj).getTenderFormId()%>&porlId=<%= packageOrLotId%>">Test Form</a>
                                                            <%
                                                                                                    if (corriCrtNPending && (!isFrmCanceled) && (!isCorriForm)) {
                                                            %>
                                                                &nbsp; | &nbsp;
                                             <a href = "javascript:void(0);" onclick="cancelForm('<%= tts.getTenderSectionId()%>','<%= forms.get(jj).getTenderFormId()%>','<%=corriId%>','<%= tenderId%>');">Cancel Form</a>
                                                            <%
                                                            }
                                                            %>
                                                            <%                          if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                                                                    if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                            || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {

                                                                                                        if (forms.get(jj).getTemplateFormId() == 0 || "13".equalsIgnoreCase(formType) || "14".equalsIgnoreCase(formType) || "15".equalsIgnoreCase(formType) || "16".equalsIgnoreCase(formType)) {
                                                            %>
                                                            &nbsp; | &nbsp;
                                            <a href="<%=request.getContextPath()%>/officer/CreateTenderCombo.jsp?formId=<%= forms.get(jj).getTenderFormId()%>&tenderId=<%= tenderId%>&porlId=<%= packageOrLotId%>">Create Combo</a>
                                            <% }}
                                            }%>
                                            <%
                                                long lng = tenderercomboservice.getComboCountFromTenderer(forms.get(jj).getTenderFormId());
                                                if(lng!=0)
                                                {
                                            %>
                                            &nbsp; | &nbsp;<a href="<%=request.getContextPath()%>/officer/ViewTenderCombo.jsp?templetformid=<%= forms.get(jj).getTenderFormId()%>&tenderId=<%= tenderId%>&mode=view<%if(!isPackageWise){%>&porlId=<%=packageOrLotId%><%}%>"TARGET = "_blank">View Combo</a>
                                                            <%}%>
                                                            <%--<a href="#">Edit</a> &nbsp;|&nbsp;
                                                            <a href="#">View</a> &nbsp;|&nbsp;
                                                            <a href="#">Test form</a> &nbsp;|&nbsp;
                                                            <a href="#">Formula </a> &nbsp;|&nbsp;
                                                            <a href="#">Cancel</a>&nbsp;|&nbsp;
                                                            <a href="#">Docs. Check list</a>--%>
                                                        </td>
                                                    </tr>
                                            <%
                                                    }
                                                }
                                                forms = null;
                                            }
                                            %>
                                        </table>
                                    </td>
                                    </tr>
                <%
                                                            }
                                                        } else if (tts.getContentType().equals("Document")) {
                %>
                                    <tr>
                            <td class="t-align-center"><%= disp%></td>
                                        <td colspan="2" class="t-align-left">

                                            <table width="100%" cellspacing="0" class="tableList_1">
                                                <tr>
                                        <td colspan="5" class="t-align-left ff"><%= tts.getSectionName()%></td>
                                                </tr>


                                                <th width="4%" class="t-align-center">Sl. No.</th>
                    <th class="t-align-center" width="30%">File Name</th>
                    <th class="t-align-center" width="38%">File Description</th>
                    <th class="t-align-center" width="10%">File Size <br />
                        (in KB)</th>
                    <th class="t-align-center" width="18%">Action</th>
                                               <%

                    String tender = request.getParameter("tenderId");
                                                                        String sectionId = tts.getTenderSectionId() + "";

                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderSectionDocInfo", sectionId, "")) {
                                                        docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                         <td class="t-align-center">
                                           <%-- <%if (sptcd.getFieldName5().trim().equals("0")) {%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tender=<%=tender%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                                <%} else {%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?templateSectionDocId=<%=sptcd.getFieldName5()%>&sectionId=<%=sptcd.getFieldName5()%>&docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%}%>--%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&officerPage=true" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                         </td>
                    </tr>
 <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                                    <%   if (sptcd != null) {
                        sptcd = null;
                    }
                    }%>


                                            </table>
                                        </td>
                                    </tr>
                <%
                                                        } else if (tts.getContentType().equals("Document By PE")) {
                %>
                                    <tr>
                            <td class="t-align-center"><%= disp%></td>
                                        <td colspan="2" class="t-align-left">


                                            <div class="t-align-left">
                                                <%
                                                                        if (((tenderStatus.equalsIgnoreCase("pending")
                                                        || tenderStatus.equals("")) && ispublish == false && isInitiater == true && fileOnHand.equalsIgnoreCase("Yes"))
                                                        || iswfLevelExist == false
                                                                                || (corriCrtNPending && isInitiater && ispublish == false)) {
                                                            String inCorrigendum = "no";
                                                                            if (corriCrtNPending) {
                                                                inCorrigendum = "yes";
                                                            }
                                                            if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                %>
                                    <a href="TenderDocUpload.jsp?tenderId=<%=tenderId%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&sectionId=<%=tts.getTenderSectionId()%>&folderName=<%=str_FolderName%>&porlId=<%= packageOrLotId%>&inCorri=<%= inCorrigendum%>">Upload</a></div>
                                    <%} }%>
                                            <table width="100%" cellspacing="0" class="tableList_1">
                                                <tr>
                                        <td colspan="5" class="t-align-left ff"><%= tts.getSectionName()%></td>
                                                </tr>
                                                <th width="4%" class="t-align-left">Sl. No.</th>
                    <th class="t-align-center" width="30%">File Name</th>
                    <th class="t-align-center" width="38%">File Description</th>
                    <th class="t-align-center" width="10%">File Size <br />
                        (in KB)</th>
                    <th class="t-align-center" width="18%">Action</th>
                                               <%


                    String tender = request.getParameter("tenderId");
                                                                        String sectionId = tts.getTenderSectionId() + "";

                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    boolean isDocCanceled = false;
                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderSectionDocInfo", sectionId, "")) {
                        docCnt++;
                        isDocCanceled = false;

                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left">
                            <%=sptcd.getFieldName1()%>
                            <%
                                                                    if (sptcd.getFieldName6() != null) {
                                                                        if ("cp".equals(sptcd.getFieldName6().trim().toString())) {
                                        isDocCanceled = true;
                                        out.print("  <br/><font color='red'>(Document Canceled)</font>");
                                    }
                                }
                            %>
                        </td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                         <td class="t-align-center">
                             <%
                                                                    if ("0".equals(sptcd.getFieldName5().trim())) {
                             %>
                                <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&officerPage=true" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>

                             <%                                          if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                                             if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                     || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                             %>
                                <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&docId=<%=sptcd.getFieldName4()%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&porlId=<%=packageOrLotId%>&from=1&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                             <%
                                    }}
                                                                             } else {
                             %>
                                 <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&folderName=<%=str_FolderName%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&docId=<%=sptcd.getFieldName4()%>&officerPage=true" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%                                          if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                                                                 if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                         || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && (!ispublish)) || iswfLevelExist == false) {
                             %>
                                <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&docId=<%=sptcd.getFieldName4()%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&from=1&porlId=<%=packageOrLotId%>&funName=remove&from=1"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                             <%
                                    }}
                             }


                            %>
                         </td>
                    </tr>

                                    <%   if (sptcd != null) {
                        sptcd = null;
                    }
                    }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                                            </table>      </td>
                                    </tr>
                <%
                                                        } else {
                %>
                                    <tr>
                            <td class="t-align-center"><%= disp%></td>
                            <td class="t-align-left"><%if(tts.getSectionName().equals("Particular Conditions of Contract (PCC)")){out.print("Special Conditions of Contract (SCC)");}else if(tts.getSectionName().equals("Proposal Data Sheet (PDS)")){out.print("Bid Data Sheet (BDS)");}else{out.print(tts.getSectionName());}%>
     <div align="right">
                                    <%        if (((tts.getContentType()).contains("ITT") || (tts.getContentType()).contains("GCC")) && ispublish) {
                                                                            String tempID = tenderDocumentSrBean.findStdTemplateId(String.valueOf(tendid));
                        String folderName = pdfConstant.STDPDF;
                                                                            String genId = tempID + "_" + tts.getTemplateSectionId();
                                                                            if(b_publish_link){
            %>

            <a class="action-button-savepdf"
                href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=section<%=disp%>_<%=tts.getSectionName()%>.pdf&folderName=<%=str_FolderName%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&tenderId=<%=tenderId%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&officerPage=true">Save As PDF </a>
                 &nbsp;<%--<a class="action-button-edit"
                                         href="GenerateTendDocFreshPDF.jsp?Id=<%=Integer.parseInt(id)%>&tenderid=<%=tenderId%>&folderName=<%=folderName%><%if(!isPackageWise){%>&porlId=<%=packageOrLotId%><%}%> ">Generate fresh PDF</a>--%>
                                    <% }/*If tender is published*/}%>
                                    <%        if (((tts.getContentType()).contains("TDS") || (tts.getContentType()).contains("PCC")) && ispublish) {
                        String folderName = pdfConstant.TENDERDOC;
                                                                            String pdfId = tenderId + "_" + tenderittSectionId + "_" + tenderStdId;
                                                                            if(b_publish_link){
            %>

            <a class="action-button-savepdf"
                href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=section<%=disp%>_<%=tts.getSectionName()%>.pdf&folderName=<%=str_FolderName%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&tenderId=<%=tenderId%>&sectionId=<%=tts.getTenderSectionId()%>&funName=download&officerPage=true">Save As PDF </a>
                 &nbsp;<%--<a class="action-button-edit"
                                         href="GenerateTendDocFreshPDF.jsp?Id=<%=Integer.parseInt(id)%>&tenderid=<%=tenderId%>&folderName=<%=folderName%><%if(!isPackageWise){%>&porlId=<%=packageOrLotId%><%}%> ">Generate fresh PDF</a>--%>
                                    <%}/*if ternder is published*/ }%>

     </div>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">

                                                <th width="4%" class="t-align-center">Sl. <br/> No.</th>
                    <th class="t-align-center" width="30%">File Name</th>
                    <th class="t-align-center" width="38%">File Description</th>
                    <th class="t-align-center" width="10%">File Size <br />
                        (in KB)</th>
                    <th class="t-align-center" width="18%">Action</th>
                                               <%

                    String tender = request.getParameter("tenderId");
                                                                        String sectionId = tts.getTenderSectionId() + "";


                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderSectionDocInfo", sectionId, "")) {
                                                        docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>

                         <td class="t-align-center">
                                            <%--<%if (sptcd.getFieldName5().trim().equals("0")) {%>
                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tender=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                                <%} else {%>

                             <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?templateSectionDocId=<%=sptcd.getFieldName5()%>&sectionId=<%=sptcd.getFieldName5()%>&docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             <%}%>--%>
                                            <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&sectionId=<%=tts.getTenderSectionId()%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&folderName=<%=str_FolderName%>&funName=download&officerPage=true" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                         </td>
                    </tr>
                                    <%   if (sptcd != null) {
                        sptcd = null;
                    }
                    }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                                            </table>
                                        </td>
                                        <%
                                                                if (tts.getContentType().equals("TDS") || tts.getContentType().equals("PCC")) {
                                        %>
                                            <td class="t-align-left">
                                <% if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                                                    || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false) || iswfLevelExist == false || corriCrtNPending) {
                                if(!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())){
                                %>
                                <a href="TenderTDSDashBoard.jsp?tenderStdId=<%=tenderStdId%>&tenderId=<%=tenderId%>&sectionId=<%=tenderittSectionId%>&porlId=<%= packageOrLotId%><% if(corriCrtNPending) { %>&corriCrtNPending=<%= corriCrtNPending %>&corriId=<%= corriId %><% } %><%if(disp==2){%>&parentLink=895<%}%>">Edit</a>
                                |<a href="TenderTDSView.jsp?tenderStdId=<%=tenderStdId%>&tenderId=<%=tenderId%>&sectionId=<%=tenderittSectionId%>&porlId=<%= packageOrLotId%>">View</a>

                                <% } }else {%>
                                <a href="TenderTDSView.jsp?tenderStdId=<%=tenderStdId%>&tenderId=<%=tenderId%>&sectionId=<%=tenderittSectionId%>&porlId=<%= packageOrLotId%>">View</a>
                                <% }%>
                                            </td>
                            <% } else {
                                            tenderittSectionId = tts.getTenderSectionId();
                                            %>
                            <td class="t-align-left"><a href="TenderITTDashboard.jsp?tenderStdId=<%=tenderStdId%>&tenderId=<%=tenderId%>&sectionId=<%=tts.getTenderSectionId()%>&porlId=<%= packageOrLotId%>">View</a></td>
                            <% }%>
                                    </tr>
                <%
                                }
                                                        j++;
                                tSections = null;
                            }
                                                } else {
                %>
                        <!--                            <tr>
                                <td colspan="3">
                                    No Records Found
                                </td>
                            </tr>-->
                        <%                                        }
                        tSections = null;
                                            } else {
                %>
                        <tr>
                            <td colspan="3" class="t-align-center">
                                No records found
                            </td>
                        </tr>
                        <%                            }
                %>
                <tr>
                    <td colspan="3">
                        <%if(b_publish_link){%>
                             <div>
                             <span class="c-alignment-right">
                                 <a class="action-button-download" href="<%=request.getContextPath()%>/TenderSecUploadServlet?tenderId=<%=tendid%>&lotNo=<%=st_forDownloadLink.replace("&", "^")%>&funName=zipdownload" title="Download Tender/Proposal Dcoument">Download Tender Document</a>
                                </span>
                             </div>
             <%}%>
                    </td>
                </tr>
            </table></div></div>
            <%--<div class="t-align-center t_space"><label class="formBtn_1"><input name="DocPreparation" type="button" value="Complete Document Preparation" /></label></div>--%>
            <%--<div>&nbsp;</div>--%>

            <!--Page Content End-->
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <script>

    function cancelForm(sectionId,formid,corriId,tenderid)
        {
              $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/CreateTenderFormSrvt",
                        data:"action=checkGrandSumExist&tenderId="+tenderid+"&formId="+formid+"&tableId=0&corriId="+corriId,
                        async: false,
                        success: function(j)
			{
				var alertMessage="Do you really want to cancel this form?";
				if(j.toString() != null && $.trim(j.toString()) != "0")
				{
				    alertMessage= "You have already prepared a Grand Summary Report. Cancelation of this form will remove Grand Summary Report. Do you want to Continue?";
				}
				if(confirm(alertMessage))
				{
				    	dynamicFromSubmit('<%=request.getContextPath()%>/CreateTenderFormSrvt?action=cancelForm&tenderId=<%= tenderId%>&sectionId='+sectionId+'&formId='+formid+'&porlId=<%= packageOrLotId%>&pn=<%=isSerivce%>&corriId='+corriId);
                                }
			}
		});

        }
         /*
        function cancelForm(sectionId,formid)
        {
            jConfirm('Do you really want to cancel this form?', 'Cancel form confirmation', function (anss)
            {
                if (anss)
                dynamicFromSubmit('<%=request.getContextPath()%>/CreateTenderFormSrvt?action=cancelForm&tenderId=<%= tenderId%>&sectionId='+sectionId+'&formId='+formid+'&porlId=<%= packageOrLotId%>&pn=<%=isSerivce%>');
            });
        }
        */
    </script>
</html>
<%
            if (grandSummary != null) {
        grandSummary = null;
    }
            if (dDocSrBean != null) {
        dDocSrBean = null;
    }
%>
