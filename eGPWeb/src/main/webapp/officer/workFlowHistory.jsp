<%-- 
    Document   : workFlowHistory
    Created on : Nov 12, 2010, 5:41:03 PM
    Author     : test
--%>

<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPWfFileHistory"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
<link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Workflow History</title>
          <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
       <!-- <script type="text/javascript" src="../resources/js/pngFix.js"></script> -->

    </head>
    <body>
        <%

       
        String objectid = request.getParameter("objectid");
        String eventid = request.getParameter("eventid");
        String childid = request.getParameter("childid");
        String loi = "";
        
        if(request.getParameter("loi") != null){
            loi = request.getParameter("loi");
        }
        
        String funName = "history";
        if(!childid.equals(objectid)){
            funName = "pkghistory";
        }
        
        String activityid = request.getParameter("activityid");
        String userid = request.getParameter("userid");
        String fraction = request.getParameter("fraction");
            String colName = null;
            String colHeader = null;
             String sortColumn = null;
            String caption =" ";
             String widths = "";
            colHeader= "Sl. <br/> No,Processed By,Processed Date and Time ,Comments,Action,To Be Processed By";
            colName = "S.No,fileSentFrom,processDate,View,action,fileSentTo";
            sortColumn = "false,false,true,false,false,false";
             widths = "5,35,15,10,10,25";
            //data= wfsr.getJSONArray(strList,para).toString();
           %>
        <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <div class="contentArea_1">
              <%
                            // Variable tenderId is defined by u on ur current page.
                            pageContext.setAttribute("tenderId", request.getParameter("objectid"));
                %>
                <div class="pageHead_1">Workflow History &nbsp;&nbsp;
                 <div align="right" style="width: 30%;float: right;">
                <% if(fraction.equals("app")){ %>
            <a class="action-button-goback" href="APPDashboard.jsp?appID=<%=objectid%>">Go back to Dashboard</a>
            <% }else if(fraction.equals("process")){ %>
             <a class="action-button-goback" href="PendingProcessing.jsp?viewtype=processed">Go back</a>
            <% }else if(fraction.equals("notice")){ %>
             <a class="action-button-goback" href="Notice.jsp?tenderid=<%=objectid%>">Go back to Tender Dashboard</a>
            <% }else if(fraction.equals("opening")){ %>
             <a class="action-button-goback" href="OpenComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
            <% }else if(fraction.equals("eval")){ 
                if("loi".equalsIgnoreCase(loi)){%>
                    <a class="action-button-goback" href="LOI.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
                <%}else{%>
                    <a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
            <% }}else if(fraction.equals("amendment")){ %>
             <a class="action-button-goback" href="Amendment.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
            <% }else if(fraction.equals("pretender")){ %>
             <a class="action-button-goback" href="PreTenderMeeting.jsp?tenderId=<%=objectid%>">Go back to Dashboard</a>
            <% }else if(fraction.equals("tsceval")){ %>
             <a class="action-button-goback" href="EvalComm.jsp?tenderid=<%=objectid%>">Go back to Dashboard</a>
            <%}else if(fraction.equalsIgnoreCase("Variation")){%>
            <a class="action-button-goback" href="DeliverySchedule.jsp?tenderId=<%=objectid%>">Go back to Dashboard</a>
            <% }else if(fraction.equals("Termination")){ %>
            <a class="action-button-goback" href="TabContractTermination.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
             <%}else if(fraction.equals("RepeatOrder")){ %>
            <a class="action-button-goback" href="repeatOrderMain.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
            <%}else if(fraction.equals("VariforServices")){ %>
            <a class="action-button-goback" href="WorkScheduleMain.jsp?tenderId=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a>
            <%}%>
                </div>
                </div>
                <div>&nbsp;</div>
            <div>
   <% 
        if(Integer.parseInt(activityid) == 1){
    %>
 <div> <jsp:include page="InformationBar.jsp">
         <jsp:param name="appId" value="<%=objectid%>" />
     </jsp:include></div>
  <%  }else if(Integer.parseInt(activityid) == 2 ||Integer.parseInt(activityid)== 5
                ||Integer.parseInt(activityid)== 6 ||Integer.parseInt(activityid)== 4
                                    ||Integer.parseInt(activityid)== 3 ||Integer.parseInt(activityid)== 8
                                    ||Integer.parseInt(activityid)== 13){
  pageContext.setAttribute("tenderId",objectid);%>

   <div> <%@include file="../resources/common/TenderInfoBar.jsp" %></div>
  <% } %>
  </div>
  <div class="borderDevider t_space">&nbsp;</div>
   <div class="tabPanelArea_2">

       <div style="width: 98.5%;" align="center">
            <jsp:include page="../resources/common/GridCommon.jsp" >
                <jsp:param name="caption" value="<%=caption%>" />
                <jsp:param name="url" value='<%=request.getContextPath()+"/WorkFlowHistoryServlet?objectid="+objectid+"&childid="+childid+"&eventid="+eventid+"&activityid="+activityid+"&userid="+userid+"&funName="+funName %>' />
                <jsp:param name="colHeader" value='<%=colHeader%>' />
                <jsp:param name="colName" value='<%=colName%>' />
                <jsp:param name="aling" value="center,left,center,center,center,center" />
                <jsp:param name="sortColumn" value="<%=sortColumn%>" />
                <jsp:param name="width" value="<%=widths%>" />
                <jsp:param name="searchOpt" value="false" />
            </jsp:include>
        </div>

   </div>

            </div>
  <div>&nbsp;</div>
       <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
    </body>
     <script type="text/javascript">
         <% if(!"app".equalsIgnoreCase(fraction) && !"process".equalsIgnoreCase(fraction)){ %>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        <% } %>
    </script>
</html>
