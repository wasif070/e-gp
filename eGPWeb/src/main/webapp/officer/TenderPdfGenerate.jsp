<%-- 
    Document   : TenderPdfGenerate
    Created on : May 27, 2011, 4:57:27 PM
    Author     : nishit
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.blockUI.js"></script>
       <script type="text/javascript">
            function pdfGenerated(){
                //$.blockUI({ message: null });
                $.post("<%=request.getContextPath()%>/PDFServlet", {tenderId:<%=request.getParameter("tenderid")%>,reqUrl:'<%=request.getRequestURL()%>',queryString:'<%=request.getQueryString()%>',funName:'generatePdf'},  function(j){
                    if(j.toString()=='ok'){
                        //window.document.referrer = "<%=request.getContextPath()%>/officer/PublishViewTender.jsp?id=<%=request.getParameter("tenderid")%>";
                        //window.location='<%=request.getContextPath()%>/officer/Notice.jsp?tenderid=<%=request.getParameter("tenderid")%>&msgPublish=msgPublish';
                       <% if(request.getParameter("isCorriPub")!=null){if("true".equals(request.getParameter("isCorriPub"))){ %>
                       document.getElementById('frm_amendment').submit();
                        <% }} else {%>
                        document.getElementById('frm_notice').submit();
                        <% } %>
                    }
                    });
               
            }
        </script>
        
       
</head>
<body onload="pdfGenerated();">
    <div>
           
           <table>
               <tr><td>&nbsp;</td></tr>
               <tr><td>&nbsp;</td></tr>
               <tr><td>&nbsp;</td></tr>
               <tr><td>&nbsp;</td></tr>
               <tr><td>&nbsp;</td></tr>
               <tr><td>&nbsp;</td></tr>
           </table>
       </div>
       <div class="contentArea_1">
           <%--<div style="display: none">
           <%@include file="../resources/common/AfterLoginTop.jsp" %>
           </div>--%>
         <table width="100%" cellpadding="0" cellspacing="10" class="tableList_1">
                <tr>
                    <td  align="left" valign="middle" style="font-size:16px; color:#6e6e6e; font-weight:bold;">
                        Dear User,<br /><br />
                        System is generating PDF and Zip files for Tender Documents<% if(request.getParameter("isCorriPub")!=null){if("true".equals(request.getParameter("isCorriPub"))){ %>  for the changes in <b>corrigendum</b> <% } }%>. This operation may take few minutes. You are requested to take care of below mentioned important points till the time this operation is not over:<br />
                        <br />
                        - Don't refresh this page<br />
                        - Don't click on 'Go back' button of your browser<br />
                        - Don't close this window.<br /><br />
                        Once the operation is completed, system will redirect you to the Tender Dashboard Page.
                    </td>
                </tr>
                </table>
                <table width="100%">
                <tr>
                    <td class="t-align-center"><img src="<%= request.getContextPath()%>/resources/images/Dashboard/ajax-loader.gif" width="150" height="50" style="margin-bottom:10px;" /> <br /></td>
                        
                </tr>
                </table>
        
       </div>
                    <form name="frm_notice" id="frm_notice" action="Notice.jsp?tenderid=<%=request.getParameter("tenderid")%>&msgPublish=msgPublish" method="post">
                    </form>
    <form name="frm_amendment" id="frm_amendment" action="Amendment.jsp?tenderid=<%=request.getParameter("tenderid")%>&msgId=published" method="post">
                    </form>
</body>
</html>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
                    
                    <%--if(flag){
                        response.sendRedirect("Notice.jsp?tenderid="+ id+"&msgPublish=msgPublish");
                    }--%>

   
