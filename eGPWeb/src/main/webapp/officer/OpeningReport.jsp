<%-- 
    Document   : OpeningReport
    Created on : Mar 15, 2011, 11:36:07 AM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    String repLabel=null;
                    if(commonService.getProcNature(request.getParameter("tenderid")).toString().equals("Services")){
                        repLabel = "Proposal";
                    }else{
                        repLabel = "Tender";
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=repLabel%> Opening Report 1</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>        
        <%
                    String tenderid = request.getParameter("tenderid");
                    String lotId = request.getParameter("lotId");
                    ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");

                    List<SPCommonSearchDataMore> tendOpeningRpt = creationService.getOpeningReportData("getTenderOpeningRpt", tenderid, lotId,"");//lotid

                    List<SPCommonSearchDataMore> advertDetail = creationService.getOpeningReportData("getAdvertisementDetail", tenderid, lotId,"");

                    List<SPCommonSearchDataMore> openingDateAndTime = creationService.getOpeningReportData("getOpeningDateAndTime", tenderid, lotId,"");

                    List<SPCommonSearchDataMore> tendDocument = creationService.getOpeningReportData("getTenderDocument", tenderid, lotId,"");//lotid

                    List<SPCommonSearchDataMore> TOCMembers = creationService.getOpeningReportData("getTOCMembers", tenderid, lotId,""); //lotid ,"TOR1"

                    boolean list1 = true;
                    boolean list2 = true;
                    boolean list3 = true;
                    boolean list4 = true;
                    boolean list5 = true;
                    if (tendOpeningRpt.isEmpty()) {
                        list1 = false;
                    }
                    if (advertDetail.isEmpty()) {
                        list2 = false;
                    }
                    if (openingDateAndTime.isEmpty()) {
                        list3 = false;
                    }
                    if (tendDocument.isEmpty()) {
                        list4 = false;
                    }
                    if (TOCMembers.isEmpty()) {
                        list5 = false;
                    }
                    int openDate = openingDateAndTime.size();
        %>
        <div class="tabPanelArea_1">
            <div class="pageHead_1"><%=repLabel%> Opening Report</div>
            <%
                    pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                    pageContext.setAttribute("tab", "6");
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>            
        <!--%@include  file="officerTabPanel.jsp"%-->
            <div class="tableHead_1 t_space"><%=repLabel%> Opening Report 1</div>
            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 b_space" width="100%">
                <tr>
                    <td width="24%" class="ff">Ministry Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName1());
                                }%></td>
                    <td width="16%" class="ff">Division Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName2());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff">Organization/Agency Name :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName3());
                                }%></td>
                    <td class="ff">Procuring Entity :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName4());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff"><%=repLabel%> Package No. and Description :</td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName5());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff">Lot No. and Description : </td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName12()+" & "+tendOpeningRpt.get(0).getFieldName6());
                                }%></td>
                </tr>
            </table>
            <table width="100%" cellspacing="0" class="tableList_3">
                <tr>
                    <td class="tableHead_1" colspan="4" >Procurement Data</td>
                </tr>
                <tr>
                    <th width="25%" >Procurement Type</th>
                    <th width="25%">Funding By</th>
                    <th width="25%">Budget Type</th>
                    <th width="25%">Method</th>
                </tr>
                <tr>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName7());
                                }%></td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName15());
                                }%></td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName8());
                                }%></td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName9());
                                }%></td>
                </tr>
            </table>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="4" >Procurement Plan</td>
                </tr>
                <tr>
                    <th width="50%" >Approving Authority</th>
                    <th width="50%">Approval Status</th>
                </tr>
                <tr>
                    <td >-<!--%if(list1){out.print(tendOpeningRpt.get(0).getFieldName1());}%--></td>
                    <td >Approved</td>                    
                </tr>
            </table>

            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="2" >Advertisement details</td>
                </tr>
                <tr>
                    <th width="73%" class="t-align-center">Newspaper Name</th>
                    <th width="27%" class="t-align-center">Newspaper Date</th>
                </tr>
                <%for (SPCommonSearchDataMore dataMore : advertDetail) {%>
                <tr>
                    <td ><%=dataMore.getFieldName1()%></td>
                    <td ><%=dataMore.getFieldName2()%></td>
                </tr>
                <%}%>
            </table>

            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="2" ><%=repLabel%> Date and Time</td>
                </tr>
                <tr>
                    <th width="73%" >Date and Time of Publishing </th>
                    <th width="27%">Date and Time of Closing</th>
                </tr>
                <tr>
                    <td ><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName10());
                                }%></td>
                    <td ><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName11());
                                }%></td>
                </tr>
            </table>

            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="4" >Opening Date and Time</td>
                </tr>
                <tr>
                    <%
                                for (int i = 0; i < openDate; i++) {
                                    if (i == 0) {
                                        out.print("<th>Original Date and Time</th>");
                                    } else {
                                        out.print("<th>" + (i + 1) + " Extension Date if any</th>");
                                    }
                                }
                    %>
                </tr>
                <tr>
                    <%
                                for (SPCommonSearchDataMore dataMore : openingDateAndTime) {
                                    out.print("<td >" + dataMore.getFieldName1() + "</td>");
                                }
                    %>
                </tr>
            </table>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="4" ><%=repLabel%> Document</td>
                </tr>
                <tr>
                    <th width="33%" >Nos. Downloaded</th>
                    <th width="34%">Nos. Received</th>
                    <th width="33%">Nos. Withdrawn</th>
                    <th width="33%">Nos. Substituted</th>
                </tr>
                <tr>
                    <td ><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName3());
                                }%></td>
                    <td ><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName1());
                                }%></td>
                    <td ><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName2());
                                }%></td>
                    <td ><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName4());
                                }%></td>
                </tr>
            </table>

            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="5" >TOC Members</td>
                </tr>
                <tr>
                    <th width="20%" >Name</th>
                    <%
                    for (SPCommonSearchDataMore dataMore : TOCMembers) {
                        out.print("<td>");
                        if(dataMore.getFieldName5().equals(session.getAttribute("userId").toString())){
                         if(dataMore.getFieldName6().equals("-")){
                   %>
                        <a href="TORSigningApp.jsp?id=<%=dataMore.getFieldName7()%>&uid=<%=dataMore.getFieldName5()%>&tid=<%=tenderid%>&lotId=<%=lotId%>&rpt=tor1">
                            <%=dataMore.getFieldName1()%>
                        </a>
                   <%
                        }else{
                             out.print(dataMore.getFieldName1());
                        }
                        
                    }else{
                        out.print(dataMore.getFieldName1());
                    }
                        out.print("</td>");
                    }
                  %>
                </tr>
                <tr>
                    <th width="20%">Committee Role</th>
                    <%for (SPCommonSearchDataMore dataMore : TOCMembers) {
                        out.print("<td>");
                            if (dataMore.getFieldName2().equals("cp")) {
                            out.print("Chairperson");
                        } else if (dataMore.getFieldName2().equals("ms")) {
                            out.print("Member Secretary");
                        } else if (dataMore.getFieldName2().equals("m")) {
                            out.print("Member");
                            }
                        out.print("</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Designation</th>
                    <%for (SPCommonSearchDataMore dataMore : TOCMembers) {out.print("<td>"+dataMore.getFieldName3()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">PE Office</th>
                    <%for (SPCommonSearchDataMore dataMore : TOCMembers) {out.print("<td>"+dataMore.getFieldName4()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Signed TOR On</th>
                    <%for(SPCommonSearchDataMore dataMore : TOCMembers) {out.print("<td>"+dataMore.getFieldName6()+"</td>");}%>
                </tr>               
            </table>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <%
                tendOpeningRpt = null;
                advertDetail = null;
                tendDocument = null;
                TOCMembers = null;
    %>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

