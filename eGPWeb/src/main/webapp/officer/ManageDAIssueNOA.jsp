<%-- 
    Document   : ManageDAIssueNOA
    Created on : Jan 5, 2011, 11:41:32 AM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Draft Agreement</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
        <jsp:useBean id="issueNOADtBean" scope="request" class="com.cptu.egp.eps.web.databean.IssueNOADtBean"/>
        <jsp:setProperty name="issueNOADtBean" property="*"/>
    </head>
    <%
                String noaIssueId = "";
                if (request.getParameter("noaIssueId") != null) {
                    noaIssueId = request.getParameter("noaIssueId");
                }
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    tenderId = request.getParameter("tenderId");
                }
                CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    //userId = Integer.parseInt(session.getAttribute("userId").toString());
                    userId = session.getAttribute("userId").toString();
                    issueNOASrBean.setLogUserId(userId);
                    commonSearchService.setLogUserId(userId);
                    tenderCommonService.setLogUserId(userId);
                }
    %>
    <script type="text/javascript">

        $(document).ready(function(){
            $("#frmManageIssueNOA").validate({

                rules:{
                    contractNo:{required:true,maxlength:100,spacevalidate:true},
                    contractAmtString:{required:true,decimal:true},
                    perfSecAmtString:{required:true,decimal:true},
                    contractName:{required:true,maxlength:100}

                },
                messages:{
                    contractNo:{required:"<div class='reqF_1'>Please enter contract no</div>",
                        maxlength:"<div class='reqF_1'>Maximum 100 characters are allowed</div>",
                        spacevalidate:"<div class='reqF_1'>Only space Is not allowed</div>"
                    },
                    contractName:{
                        required:"<div class='reqF_1'>Please enter contract name.</div>",
                        maxlength:"<div class='reqF_1'>Maximum 100 characters are allowed</div>"
                    },
                    contractAmtString:{
                        required:"<div class='reqF_1'>Please enter contract price in Figure (In Tk.)</div>",
                        decimal:"<div class='reqF_1'>Allows numbers and 2 digits after decimal.</div>"
                    },
                    perfSecAmtString:{
                        required:"<div class='reqF_1' id = 'perSecVal'>Please enter Performance security amount in Figure (In Tk.)</div>",
                        decimal:"<div class='reqF_1' id = 'perSecValDec'>Allows numbers and 2 digits after decimal.</div>"
                    }
                }
            });
        });
    </script>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <div class="contentArea_1">
                <form id="frmManageIssueNOA" action="" method="POST">
                    <div class="pageHead_1">Manage Draft Agreement
                        <span class="c-alignment-right">
                            <a href="#" class="action-button-goback">Go Back To Dashboard</a>
                        </span>
                    </div>
                    <%          if ("Update".equals(request.getParameter("Update"))) {
                                    if(issueNOASrBean.updateSerIssueNOA(issueNOADtBean)){
                                    response.sendRedirect("DANOA.jsp?tenderId="+tenderId);
                                }
                                }

                                //List<SPCommonSearchData> packageList = commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null);
                    %>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                        <% for(Object[] obj : issueNOASrBean.getListNOA(Integer.parseInt(noaIssueId))){ %>
                        <tr>
                            <td width="24%" class="ff">Contract No : <span class="mandatory">*</span></td>
                            <td width="76%"><input name="contractNo" value="<%=obj[8] %>" type="text" class="formTxtBox_1" id="txtContractNo" style="width:200px;" />
                                <input type="hidden" value="<%=obj[0] %>" id="noaAcceptId" name="noaAcceptId"/>
                                <input type="hidden" value="<%=obj[1] %>" id="comments" name="comments"/>
                                <input type="hidden" value="<%=obj[2] %>" id="acceptRejDtString" name="acceptRejDtString"/>
                                <input type="hidden" value="<%=obj[3] %>" id="acceptRejStatus" name="acceptRejStatus"/>
                                <input type="hidden" value="<%=obj[4] %>" id="acceptRejType" name="acceptRejType"/>
                                <input type="hidden" value="<%=obj[5] %>" id="noaIssueId" name="noaIssueId"/>
                                <input type="hidden" value="<%=obj[6] %>" id="tenderId" name="tenderId"/>
                                <input type="hidden" value="<%=obj[7] %>" id="pkgLotId" name="pkgLotId"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="24%" class="ff">Contract/Project name : <span class="mandatory">*</span></td>
                            <td width="76%"><input name="contractName" type="text" value="<%=obj[23] %>" class="formTxtBox_1" id="txtprojectName" style="width:200px;" />
                            </td>
                        </tr>
                        <tr>
                            <%
                                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
                            %>
                            <td class="ff">Date of Draft Agreement :</td>
                            <td class="formStyle_1"><%=dateFormat1.format(obj[9]) %>
                                <input type="hidden" value="<%=dateFormat1.format(obj[9]) %>" name="contractDtString"/></td>
                        </tr>
                        <tr>
                            <td class="ff">Name of Bidder/Consultant : </td>
                            <td>
                                <select id="hdUserId" style="width:205px;"class="formTxtBox_1" name="hdUserId">
                                <%for(SPTenderCommonData companyList : tenderCommonService.returndata("GetEvaluatedBidders",tenderId,"0")){ %>
                                <option <% if(obj[10].toString().equalsIgnoreCase(companyList.getFieldName2())){%>selected<%} %> value="<%=companyList.getFieldName2() %>"><%=companyList.getFieldName1() %></option>
                                <% } %>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Contract price In Figure (In Tk.) <span class="mandatory">*</span></td>
                            <td width="76%"><input name="contractAmtString" value="<%=new BigDecimal(obj[11].toString()).setScale(2, 0) %>" type="text" class="formTxtBox_1" id="txtContPriceFig" style="width:200px;" onblur="contractPrice(this.value);"/></td>
                        </tr>
                        <tr>
                            <td class="ff">Contract price In Words (In Tk.)</td>
                            <td><label id="conPrice"><%=obj[12] %></label>
                                <input id="contractAmtWords" value="<%=obj[12] %>" name="contractAmtWords" type="hidden"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">No. of days from the date of issuance of Draft Agreement</td>
                            <% dateFormat1 = new SimpleDateFormat("yyyy-MM-dd"); %>
                            <td><%=obj[13]%>
                                <input type="hidden" value="<%=obj[13]%>" name="noaIssueDays"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Draft Agreement acceptance last date &amp; time</td>
                            <% String noaADt=DateUtils.formatDate((Date)obj[14]);
                               String[] spaceSplitNOAADt = noaADt.split(" "); %>
                            <td><%=spaceSplitNOAADt[0] %>
                                <input type="hidden" value="<%=spaceSplitNOAADt[0] %>" name="noaAcceptDtString"/></td>
                        </tr>
                        <tr>
                            <td width="24%" class="ff">Performance security Required : </td>
                            <td width="76%">
                                <input checked name="perReq" type="radio" id="perReqYes" onclick="perVisible();" value="Yes"/>Yes
                                <input name="perReq" type="radio" id="perReqNo" onclick="perInvisible();" value="No"/>No
                            </td>
                        </tr>
                        <tr id="perSecAmtInFig">
                            <td class="ff">Performance security amount in Figure (In Tk.) <span class="mandatory"></span></td>
                            <td width="76%"><input onblur="performancePrice(this.value);" name="perfSecAmtString" type="text" value="<%=new BigDecimal(obj[15].toString()).setScale(2, 0)%>" class="formTxtBox_1" id="txtSecurityAmtFig" style="width:200px;" /></td>
                        </tr>
                        <tr id="perSecAmtInWord">
                            <td class="ff">Performance security amount in Words (In Tk.)</td>
                            <td><label id="perSecAmt"><%=obj[16]%></label>
                                <input type="hidden" name="perfSecAmtWords" value="<%=obj[16]%>" id="perfSecAmtWords"/>
                            </td>
                        </tr>
                        <tr id="noOfDaysForPerSecSub">
                            <td class="ff">No. of days for performance security submission</td>
                            <td><%=obj[17]%>
                                <input type="hidden" name="perSecSubDays" value="<%=obj[17]%>"/>
                            </td>
                        </tr>
                        <tr id="lastDTForPerSecSub">
                            <td class="ff" >Last date &amp; time for Performance security submission </td>
                            <% String lastDt=DateUtils.formatDate((Date)obj[18]);
                               String[] spaceSplitLastDt = lastDt.split(" "); %>
                            <td><%=spaceSplitLastDt[0] %>
                                <input type="hidden" value="<%=spaceSplitLastDt[0] %>" name="perSecSubDtString"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Signing of Contract in no. of days from the date of issuance</td>
                            <td><input type="hidden" name="contractSignDays"  value="<%=obj[19]%>"/>
                                <%=obj[19]%></td>

                        </tr>
                        <%
                                    dateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
                                    List<SPCommonSearchData> issueCon = commonSearchService.searchData("ProcureNOA", tenderId, "", "", null, null, null, null, null, null);
                        %>
                        <tr id="modeOfPayment">
                            <td class="ff">Mode of payment</td>
                            <td><label><%if (issueCon.get(0).getFieldName4().equalsIgnoreCase("Offline/Bank")) {%>Offline,Bank<% } else {%><%=issueCon.get(0).getFieldName4()%><% }%></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Last Date &amp; time of contract signing</td>
                            <% String lastContractSignDt=DateUtils.formatDate((Date)obj[20]);
                               String[] spaceSplitLastContractSignDt = lastContractSignDt.split(" "); %>
                            <td><%=spaceSplitLastContractSignDt[0] %>
                                <input type="hidden" value="<%=spaceSplitLastContractSignDt[0] %>" name="contractSignDtString"/>
                                <input type="hidden" value="<%=obj[21]%>" name="createdBy"/>
                                <input type="hidden" value="<%=obj[22]%>" name="createdDtString"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Clause Reference</td>
                            <td><textarea rows="4" cols="100" id="clauseRef" name="clauseRef"><%=obj[24] %></textarea>
                            </td>
                        </tr>
                        <% } %>
                    </table>
                    <div class="t-align-center t_space">
                        <label class="formBtn_1">
                            <input name="Update" type="submit" value="Update" />
                        </label>
                    </div>
                </form>
            </div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
    <script type="text/javascript">       
        function contractPrice(obj){
            document.getElementById('conPrice').innerHTML =WORD(obj);
            document.getElementById('contractAmtWords').value =WORD(obj);
        }
        function performancePrice(obj){
            document.getElementById('perSecAmt').innerHTML =WORD(obj);
            document.getElementById('perfSecAmtWords').value =WORD(obj);
        }
        function perInvisible(){
            document.getElementById('perSecAmtInWord').style.display = 'none';
            document.getElementById('perSecAmtInFig').style.display = 'none';
            document.getElementById('noOfDaysForPerSecSub').style.display = 'none';
            document.getElementById('lastDTForPerSecSub').style.display = 'none';
            document.getElementById('modeOfPayment').style.display = 'none';
            document.getElementById('txtSecurityAmtFig').setAttribute('name', "abc");
            if(document.getElementById('perSecVal') != null){
                if(document.getElementById('perSecVal').innerHTML != ''){
                    document.getElementById('perSecVal').innerHTML = '';

                }
            }

            if(document.getElementById('perSecValDec') != null){
                if(document.getElementById('perSecValDec').innerHTML != ''){
                    document.getElementById('perSecValDec').innerHTML = '';
                    //document.getElementById('txtSecurityAmtFig').setAttribute('name', "abc");
                }
            }
        }
        function perVisible(){
            document.getElementById('perSecAmtInFig').style.display = 'table-row';
            document.getElementById('perSecAmtInWord').style.display = 'table-row';
            document.getElementById('noOfDaysForPerSecSub').style.display = 'table-row';
            document.getElementById('lastDTForPerSecSub').style.display = 'table-row';
            document.getElementById('modeOfPayment').style.display = 'table-row';
            document.getElementById('txtSecurityAmtFig').setAttribute('name', 'perfSecAmtString');
        }
    </script>
    <%
                issueNOASrBean = null;
                issueNOADtBean = null;
    %>
</html>