<%-- 
    Document   : SrvViewPr
    Created on : Dec 13, 2011, 2:29:25 PM
    Author     : shreyansh Jogi
--%>

<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");

        %>
        <%
            String repId = request.getParameter("repId");
             ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Progress Report</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>
        <script type="text/javascript">


            /* check if pageNO is disable or not */
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            /*  load Grid for the User Registration Report */
            function loadTable()
            {
                $('#tohide').hide();
                if($("#keyWord").val() == undefined)
                    $("#keyWord").val('');
                $.post("<%=request.getContextPath()%>/ConsolidateServlet", {tenderId : '<%= request.getParameter("tenderId") %>',repId : '<%=repId%>',size: $("#size").val(),pageNo: $("#first").val(),action:'viewpr',wpId: $("#wpId").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if($('#noRecordFound').val() == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            /*  Handle First click event */
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo =$('#pageNo').val();
                    $('#first').val(0);
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Last Button click event */
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var size = (parseInt($('#size').val())*totalPages)-(parseInt($('#size').val()));
                    $('#first').val(size);
                    if(totalPages>0){
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Next Button click event */
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val());
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first+max);

                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo <= totalPages) {

                        loadTable();
                        $('#pageNo').val(Number(pageNo)+1);
                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            /*  Handle Previous click event */
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first-max);
                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    $('#pageNo').val(Number(pageNo)-1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)-1);
                });
            });
        </script>
        <script type="text/javascript">
            function Search(){
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            }
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var size = (parseInt($('#size').val())*pageNo)-$('#size').val();
                    $('#first').val(size);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }

                });
            });
            function changetable(){
                var pageNo=parseInt($('#pageNo').val(),10);
                var first = parseInt($('#first').val());
                var max = parseInt($('#size').val());
                var totalPages=parseInt($('#totalPages').val(),10);
                loadTable();

            }

        </script>

        <%
                    int userid = 0;
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userid = Integer.parseInt(hs.getAttribute("userId").toString());
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }

        %>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->


            <div class="contentArea_1">
                <div  id="print_area">
                <div class="DashboardContainer">
                    <div class="pageHead_1">View Progress Report
                        <%if(request.getParameter("flag")!=null){%>
                        <span class="c-alignment-right noprint">
                            <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                            <a href="Invoice.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a></span>
                        <%}else{
                        %>
                        <span class="c-alignment-right noprint">
                            <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                            <a href="SrvViewProgressReport.jsp?wpId=<%=request.getParameter("wpId")%>&lotId=<%=request.getParameter("lotId")%>&tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a></span>
                        <%}%>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <div>&nbsp;</div>
                    <%
                                pageContext.setAttribute("tab", "14");

                    %>
                    <%@include  file="officerTabPanel.jsp"%>
                    <div class="tabPanelArea_1">

                        <%
                        if(request.getParameter("flag")!=null){
                                    pageContext.setAttribute("TSCtab", "4");
                                    }else{
                                    pageContext.setAttribute("TSCtab", "2");
                                    }

                        %>
                        <%@include  file="../resources/common/CMSTab.jsp"%>
                        <div class="tabPanelArea_1">
                            <form name="prfrm" id="prfrm"  method="post" action="<%=request.getContextPath()%>/ConsolidateServlet">
                                <input type="hidden" name="repId" value="<%=request.getParameter("repId")%>" />
                    <%
                                String tenderId = request.getParameter("tenderId");
                                boolean flag = true;
                                //CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                //List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetails", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                //List<SPTenderCommonData> tenderValSec = tenderCommonService.returndata("getTenderDatesDiff", tenderId, null);
                                 CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                                List<Object[]> list = cs.getLotDetailsByPkgLotId(request.getParameter("lotId"),request.getParameter("tenderId"));
                                //ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                //List<TblCmsWpDetail> wpDetails = service.getAllDetail();
                                int i = 0;
                                for (Object [] lotList : list) {
                                    //boolean isDateEdited = service.checkForDateEditedOrNot(Integer.parseInt(lotList.getFieldName5()));
                                    //boolean chkContract = service.checkContractSigning(tenderId,Integer.parseInt(lotList.getFieldName5()));
                    %>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                            <tr>
                                <td width="20%">Package No.</td>
                                <td width="80%"><%=lotList[0]%></td>
                            </tr>
                            <tr>
                                <td>Package Description</td>
                                <td class="t-align-left"><%=lotList[1]%></td>
                            </tr>
                        </table>
                            <% }%>
                                <input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>" />
                                <input type="hidden" name="wpId" id="wpId" value="<%=request.getParameter("wpId")%>" />
                                <input type="hidden" name="action" value="preparepr" />
                                <div id="resultDiv" style="display: block;">
                                    <div  id="print_area">
                                        <%if("works".equalsIgnoreCase(procnature)){%>
                                                                                                                <%
    ConsolodateService service = (ConsolodateService)AppContext.getSpringBean("ConsolodateService");
%>
                                <table width="100%" cellspacing="0" class="t_space">
                                    <tr><td  style="margin-left:20% ;background-color: #FFFF99" width="5%"></td><td>&nbsp;&nbsp;New Items added in Original BSR</td></tr><tr><td>&nbsp;</td></tr><tr><td style="background-color: #CDC0B0" width="5%"></td><td>&nbsp;&nbsp;Items Updated in Original BSR</td>
                                        <td colspan="3" style="text-align: right;">&nbsp;</td>
                                    </tr>
                                </table>
                                        <%}%>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                            <tr>
                                                <th width="3%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                                <th width="20%" class="t-align-center"><%=bdl.getString("CMS.desc")%></th>
                                                <th width="15%" class="t-align-center"><%=bdl.getString("CMS.UOM")%>
                                                </th>
                                                <th width="9%" class="t-align-center"><%=bdl.getString("CMS.qty")%>
                                                </th>
                                                <th width="5%" class="t-align-center"><%=bdl.getString("CMS.PR.qtyacceptedtillpr")%>
                                                </th>
                                                <th width="18%" class="t-align-center"><%=bdl.getString("CMS.PR.qtyaccepted")%>
                                                </th>
                                                <th width="5%" class="t-align-center"><%=bdl.getString("CMS.PR.qtypending")%>
                                                </th>
                                                <th width="20%" class="t-align-center"><%=bdl.getString("CMS.PR.dod")%>
                                                </th>
                                                <th width="30%" class="t-align-center"><%=bdl.getString("CMS.PR.remarks")%>
                                                </th>
                                                
                                            </tr>
                                        </table>

                                    </div>
                                </div>
                                <div id="tohide">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                                        <tr>
                                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                Total Records per page : <select name="size" id="size" onchange="changetable();" style="width:40px;">
                                                    <option value="1000" selected>10</option>
                                                    <option value="20">20</option>
                                                    <option value="30">30</option>

                                                </select>
                                            </td>
                                            <td align="center"><input name="textfield3" type="text" id="dispPage" onKeydown="javascript: if (event.keyCode==13) Search();" value="1" class="formTxtBox_1" style="width:20px;" />
                                                &nbsp;
                                                <label class="formBtn_1">
                                                    <input type="submit" name="button"  id="btnGoto" value="Go To Page" />
                                                </label></td>
                                            <td  class="prevNext-container">
                                                <ul>
                                                    <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                                    <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <input type="hidden" id="pageNo" value="1"/>
                                <input type="hidden" id="first" value="0"/>
                            </form>
                        </div>
                    </div>
                    <!--Dashboard Content Part End-->
                </div></div>
                <!--Dashboard Footer Start-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End-->
            </div>
            </div>

    </body>
<script type="text/javascript">
    $(document).ready(function() {
        $("#print").click(function() {
            printElem({ leaveOpen: true, printMode: 'popup' });
        });

    });
    function printElem(options){
        $('#print_area').printElement(options);
    }
</script>

    <script>
        var headSel_Obj = document.getElementById("headTabReport");
        loadTable();
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

