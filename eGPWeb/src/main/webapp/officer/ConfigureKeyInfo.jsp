<%--
    Document   : ConfigureKeyInfo
    Created on : Nov 16, 2010, 5:50:42 PM
    Author     : Kinjal Shah,TaherT,Rishita
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderService"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderServiceImpl" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    boolean isEdit = false;
                    if(request.getParameter("isEdit") != null && request.getParameter("isEdit").equalsIgnoreCase("yes")){
                        isEdit = true;
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Configure Official Cost Estimate, Approving Authority, SBD Selection</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <!-- Dohatec Start -->
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <!-- Dohatec End -->

        <script type="text/javascript">
            function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            var vboolean;
            function validateSec(){
                if(document.getElementById('txttenderSecurityValidity')!=null){
                    if(document.getElementById('txttenderSecurityValidity').value == ''){
                        document.getElementById('sptendersecval').innerHTML ='Please enter Tender Security Validity In No. of Days';
                        vboolean = false;
                    }else if(!regForNumber(document.getElementById('txttenderSecurityValidity').value) || document.getElementById('txttenderSecurityValidity').value.indexOf('.') > 0){
                        document.getElementById('sptendersecval').innerHTML ='Please enter digits (0-9) only';
                        vboolean = false;
                    }else{
                        document.getElementById('sptendersecval').innerHTML ='';
                    }
                }
            }
            function validateDays(){
                vboolean = true;
                //alert(document.getElementById("hdnProcureNature").value);
                if(document.getElementById('txttenderValidity')!=null){
                    if(document.getElementById('txttenderValidity').value == ''){
                        document.getElementById('sptenderval').innerHTML ='Please enter Tender Validity in No. of Days';
                        vboolean = false;
                    }else if(!regForNumber(document.getElementById('txttenderValidity').value) || document.getElementById('txttenderValidity').value.indexOf('.') > 0){
                        document.getElementById('sptenderval').innerHTML ='Please enter digits (0-9) only';
                        vboolean = false;
                    }else if(!chkMinLength(document.getElementById('txttenderValidity').value))
                    {
                        document.getElementById('sptenderval').innerHTML='Tender Validity in No. of Days must not be less than 14 days';
                        vboolean = false;
                    }
                    else
                    {
                         if(document.getElementById("hdnProcureNature").value != null && document.getElementById("hdnProcureNature").value == 'Services' )

                             {
                                if((document.getElementById('txttenderValidity').value)>90 && (document.getElementById('txttenderValidity').value)<120){
                                    document.getElementById('SPMsg').innerHTML=''
                                }else{
                                    document.getElementById('SPMsg').innerHTML='Ideally it should be between 90-120 Days'
                                }
                             }
                         else
                           {
                                if((document.getElementById('txttenderValidity').value)>60 && (document.getElementById('txttenderValidity').value)<90)
                                {
                                    document.getElementById('SPMsg').innerHTML=''
                                }
                                else
                                {
                                    document.getElementById('SPMsg').innerHTML='Ideally it should be between 60-90 Days'
                                }
                           }

                        if(document.getElementById('txttenderSecurityValidity') != null){
                            document.getElementById('txttenderSecurityValidity').value = parseInt(document.getElementById('txttenderValidity').value) + 30;
                        }
                        document.getElementById('sptenderval').innerHTML = '';
                    }
                }
            }
            function addDomPreField(){
                if(document.getElementById('cmbDomesticPreference').value == 'Yes'){
                    document.getElementById('trDomPre').style.display = 'table-row';
                    document.getElementById('trDomPre').value="15"
                }else{
                    document.getElementById('trDomPre').style.display = 'none';
                     document.getElementById('trDomPre').value=""
                }
            }
            function Validation(){
                var vbool = true;

                //  Dohatec Start
                    if(document.getElementById('tenderDocument').options[document.getElementById('tenderDocument').selectedIndex].text == 'e-PW2(a) (For GOB Fund only)')
                    {
                        if(!confirm("Select e-PW3 in Case of DP's Fund"))
                        {
                            return false;
                        }
                    }
                    if(document.getElementById('tenderDocument').options[document.getElementById('tenderDocument').selectedIndex].text == 'e-PG2 (For GOB Fund only)')
                    {
                        if(!confirm("Select e-PG3 in Case of DP's Fund"))
                        {
                            return false;
                        }
                    }
                //  Dohatec End

                if(document.getElementById('tenderDocument').value == 0){
                     document.getElementById('valStdTemplate').innerHTML ='Please select Standard Bidding Document.';
                     return false;
                 }
                if(document.getElementById('txttenderValidity')!=null)
                {
                    if(document.getElementById('txttenderValidity').value == '')
                    {
                        document.getElementById('sptenderval').innerHTML ='Please enter Tender Validity in No. of Days';
                        vbool=false;
                    } else if (document.getElementById('txttenderValidity').value.split(".")[1] != undefined){
                        document.getElementById('sptenderval').innerHTML ='Please enter digits (0-9) only';
                        vbool=false;
                    }
                }
                if(document.getElementById('txttenderSecurityValidity')!=null)
                {
                    if(document.getElementById('txttenderSecurityValidity').value == '')
                    {
                        document.getElementById('sptendersecval').innerHTML ='Please enter Tender Security Validity In No. of Days';
                        vbool=false;
                    }
                }
                if(document.getElementById('cmbDomesticPreference') != null){
                    if(document.getElementById('cmbDomesticPreference').value == ""){
                        document.getElementById('valDomPre').innerHTML = 'Please select Domestic Preference Requires';
                        vbool = false;
                    }else{
                        document.getElementById('valDomPre').innerHTML = '';
                    }
                }

                if(document.getElementById('txtDomPrePer') != null){
                    if(document.getElementById('cmbDomesticPreference').value == "Yes"){
                        if(document.getElementById('txtDomPrePer').value == ""){
                            document.getElementById('valDomPrePer').innerHTML = 'Please enter Domestic Preference in %';
                            vbool = false;
                        }else if(!regForNumber(document.getElementById('txtDomPrePer').value)){
                            document.getElementById('valDomPrePer').innerHTML = 'Please enter Numerals (0-9) only and 3 Digits after Decimal';
                            vbool = false;
                        }else if(document.getElementById('txtDomPrePer').value.split(".")[1] != undefined){
                            if(!regForNumber(document.getElementById('txtDomPrePer').value) || document.getElementById('txtDomPrePer').value.split(".")[1].length > 3){
                                document.getElementById('valDomPrePer').innerHTML = 'Please enter Numerals (0-9) only and 3 Digits after Decimal';
                                vbool = false;
                            }else{
                                document.getElementById('valDomPrePer').innerHTML = '';
                            }
                        }else{
                            document.getElementById('valDomPrePer').innerHTML = '';
                        }
                    }else{
                        document.getElementById('valDomPrePer').innerHTML = '';
                    }
                }

                if(vbool == false || vboolean == false){
                    return false;
                }else{
                    var tenderValidity = document.getElementById('txttenderValidity').value;
                    var tenderSecurityValidity = document.getElementById('txttenderSecurityValidity').value;
                    var diff = tenderSecurityValidity - tenderValidity;
                    if(diff >= 28){
                        document.getElementById('SpErr').innerHTML ='';
                    }else{
                        document.getElementById('SPMsg').innerHTML=''
                        document.getElementById('SpErr').innerHTML ='Validity of Tender Security must be Tender Validity period plus 28 days';
                        return false;
                    }
                }

                if(vbool == true){
                    document.getElementById("btnSubmit").style.display = 'none';
                }
            }

            function chkMinLength(value)
            {
                var ValueChk=value;
                if(ValueChk<14)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            function numberZero(value,element)
            {
                var flag ;
                //alert(value.indexOf("."));
                if(isNaN(value)==false)
                {
                    if(value.indexOf(".")==-1)
                    {
                        if(value>0)
                        {
                            flag=true;
                        }
                        else
                        {
                            flag=false;
                        }
                    }
                    else
                    {
                        flag=false;
                    }
                }
                else
                {
                    flag=false;
                }
                //alert(flag);
                if(flag==false)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }


        </script>
    </head>
    <body>
        <%
            String tId = "";
            if (!request.getParameter("tenderId").equals("")) {
                tId = request.getParameter("tenderId");
            }
            TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        // IF SUBMIT THEN UPDATE FOLLOWING PARAMETERS IN DB ELSE DISPLAY PAGE.
                    if ("Submit".equals(request.getParameter("submit"))) {
                        if(request.getParameter("domesticPreference") != null){
                                TenderSrBean tSrBean = new TenderSrBean();
                                String domesticPreference = request.getParameter("domesticPreference");
                                String domPrePer = "0.00";
                                if(domesticPreference.equalsIgnoreCase("yes")){
                                    domPrePer = request.getParameter("domPrePer");
                                }
                                //Folloing setAudtiTrail commited because system will perform doble entry into system for configure key informaiton in case of tender createion.
                                //tSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                tSrBean.setAuditTrail(null);
                                tSrBean.insertDomestic(Integer.parseInt(tId),domesticPreference , domPrePer);

                            }
                            String tenderValidity = "0";
                            String tenderSecurity = "0";
                            tenderValidity = request.getParameter("tenderValidity")==null ? "0":request.getParameter("tenderValidity").toString();
                            tenderSecurity = request.getParameter("tenderSecurityValidity")==null ? "0":request.getParameter("tenderSecurityValidity").toString();
                            TenderService tsi = (TenderService) AppContext.getSpringBean("TenderService");
                            String actionPerformed = "Configure Key Information";
                            if(request.getParameter("actionPerformed") != null){
                                 actionPerformed = "Edit Configured Key Information";
                                 //Added by TaherT for Report Structure and Formula Deletion
                                 ReportCreationService rcs = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                                 for(Object reportIds : rcs.getReportID4Tender(tId)){
                                     rcs.deleteWholeReport(reportIds.toString());
                                 }
                                 tenderCommonService1.returndata("deleteServiceCriteria", tId, null);
                            }
                            tsi.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                            tsi.insertSPAddYpTEndernoticeBySP(
                                    "","","","","",
                                    Integer.parseInt(request.getParameter("appAuthority")),
                                    Integer.parseInt(request.getParameter("tenderDocument")),
                                    tenderValidity,
                                    tenderSecurity,
                                    null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, "updateconfig", Integer.parseInt(request.getParameter("hdnTenderIdName")), 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 0,"","",actionPerformed);

                            TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                            String stdName = tenderDocumentSrBean.checkForDump(String.valueOf(request.getParameter("hdnTenderIdName")));
                            if (stdName == null) {
                                String stdTemplateId = tenderDocumentSrBean.findStdTemplateId(String.valueOf(request.getParameter("hdnTenderIdName")));
                                if (Integer.parseInt(stdTemplateId) != 0) {
                                    tenderDocumentSrBean.dumpSTD(String.valueOf(request.getParameter("hdnTenderIdName")), stdTemplateId, session.getAttribute("userId").toString());
                                }
                            } else {
                                if (request.getParameter("hdStdTemplateId") != null && request.getParameter("hdStdTemplateChagne") != null) {
                                    //if (request.getParameter("hdStdTemplateId") != request.getParameter("hdStdTemplateChagne")) { // Old Version
                                    /*  Comment By Dohatec
                                    *   In Java, 2 String cannot be compared using '=' / '==' / '!='.
                                    *   In Java, 2 String must be compared using 'equals()' or 'equalsIgnoreCase()'. Otherwise condition'll return TRUE always.
                                    */
                                    if (!request.getParameter("hdStdTemplateId").equalsIgnoreCase(request.getParameter("hdStdTemplateChagne"))) { // Changed by Dohatec
                                        if(tenderDocumentSrBean.changeDumpSTD(Integer.parseInt(tId), Integer.parseInt(request.getParameter("tenderDocument")))){
                                            String stdTemplateId = tenderDocumentSrBean.findStdTemplateId(String.valueOf(request.getParameter("hdnTenderIdName")));
                                            if (Integer.parseInt(stdTemplateId) != 0) {
                                                tenderDocumentSrBean.dumpSTD(String.valueOf(request.getParameter("hdnTenderIdName")), stdTemplateId, session.getAttribute("userId").toString());
                                            }
                                        }
                                    }
                                }
                            }
                            String stdup = "";
                            if(!tenderDocumentSrBean.copySTDDocs4ZIP(request.getParameter("tenderDocument"),tId)){
                                stdup = "&stddocdump=error";
                            }
                            tenderDocumentSrBean = null;
                            String templateId = request.getParameter("tenderDocument");
                            
                            //Get Clarification days based on SBD. templateId of related SBD is passed.
                            List<SPTenderCommonData> getClarificationDays = tenderCommonService1.returndata("getClarificationDays", templateId, null);
                            
                            if (getClarificationDays.size() > 0) 
                            {
                                if (stdup.equalsIgnoreCase("")) {
                                    String submissionDate = tenderCommonService1.getTenderSubmissionDate(Integer.parseInt(tId));
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                    Date ldate = new Date();
                                    ldate = format.parse(submissionDate);
                                    
                                    // Number of days prior to tender submission/closing date for posting clarification query
                                    int ClarificationDays = Integer.parseInt(getClarificationDays.get(0).getFieldName1());
                                    
                                    // Setting last date of posting clarification query
                                    GregorianCalendar cal = new GregorianCalendar();
                                    cal.setTime(ldate);
                                    cal.add(Calendar.DATE, -ClarificationDays);
                                    ldate = cal.getTime();
                                    
                                    //ldate = new Date(ldate.getTime() - (long)(ClarificationDays * 24 * 3600 * 1000));
                                    
                                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                                    submissionDate = formatter.format(ldate);
                                    response.sendRedirect("TenderClariAfterSBDSubmit.jsp?tenderId="+tId+"&lastdate="+submissionDate);
                                    return;
                                }
                            }
                            response.sendRedirect("Notice.jsp?tenderid=" + request.getParameter("hdnTenderIdName")+stdup);


                    } else {

        %>


        <%

                                String estCost = "";
                                List<SPTenderCommonData> IsEOI = tenderCommonService1.returndata("getEventType", tId, null);
                                String EventType = "expressionofinterest"; //Expression of Interest
                                List<SPTenderCommonData> list = tenderCommonService1.returndata("getTenderConfigData", tId, null);

                                List<SPTenderCommonData> listDP = tenderCommonService1.returndata("chkDomesticPreference", tId, null);
                                List<SPTenderCommonData> listSuggestedStdAsPerRule=tenderCommonService1.returndata("getStdAsPerConfigRule", tId, null);
                                 //out.println("===================="+listSuggestedStdAsPerRule.size());
                                 String procNature = "";
                                if (!list.isEmpty()) {
                                    //for (SPTenderCommonData sptcd : list)
                                    //  {
                                    estCost = list.get(0).getFieldName1();
                                    BigDecimal estCostBigDecimal = new BigDecimal(estCost);
                                    estCostBigDecimal = estCostBigDecimal.divide(new BigDecimal("1000000"),8,BigDecimal.ROUND_HALF_UP);
                                    estCost = estCostBigDecimal.toString();
                                    procNature = list.get(0).getFieldName9();


                                }
                                 //out.println("---------------------------"+list.get(0).getFieldName6());
                                 if(list!=null && list.get(0).getFieldName6()!=null)
                                 {
                                        listSuggestedStdAsPerRule=null;
                                 }
                                TenderSrBean tenderSrBean = new TenderSrBean();
                                List<Object[]> tenConfig = tenderSrBean.getConfiForTender(Integer.parseInt(tId));
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            </div>
            <div class="contentArea_1">
                <!--Dashboard Header End-->
                <div class="pageHead_1">Configure Official Cost Estimate, Approving Authority, SBD Selection <span style="float:right;"><a href="Notice.jsp?tenderid=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back To Dashboard</a></span></div>

                <!--            <div id="action-tray" align="right">
		<ul>
        	<li><a href="Notice.jsp?tenderid=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back To Dashboard</a></li>
                                 </ul>
                            </div>-->
                <!--Dashboard Content Part Start-->
                <%
                                        // Variable tenderId is defined by u on ur current page.
                                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%

                %>

                <table border="0" cellspacing="10" cellpadding="0" width="100%" class="formStyle_1 t_space">
                    <tr>
                        <td style="font-style: italic" colspan="4" class="ff t-align-left" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                    </tr>
                </table>

                <form method="POST" id="frmConfigureKeyInfo" action="ConfigureKeyInfo.jsp?tenderId=<%=tId%>">
                    <input type="hidden" id="hdnTendererId" name="hdnTenderIdName" value="<%=tId%>" ></input>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <!--<tr>  <td width="25%" class="ff">To use Previous Tender Document </td><td width="75%" class="ff"><a href="MapOldDocuments.jsp?tenderid=<%=tId%>">Click here</a> </td> </tr>-->
                        <tr>
                            <td width="25%" class="ff">Estimated Cost (In million Nu.) : </td>
                            <td width="75%">
                                <%
                                if(isEdit){
                                %>
                                <input type="hidden" name="actionPerformed" value="yes" />
                                <% } %>
            <!--<input name="estimateCost" type="text" class="formTxtBox_1" id="txtestimateCost" style="width:200px;" maxlength="50" readonly="true" value="<%=estCost%>"/>-->
                                <label><%=estCost%></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Approving Authority : <span>*</span></td>
                            <td>
                                <select name="appAuthority" class="formTxtBox_1" id="cmbappAuthority" style="width:250px;">
                                    <%  List<SPTenderCommonData> cmbAAList = tenderCommonService.returndata("AAFillCombo", tId, estCost);
                                                            for (SPTenderCommonData sptcd1 : cmbAAList) {
                                                                if (sptcd1.getFieldName2().equalsIgnoreCase(list.get(0).getFieldName2())) {
                                    %>

                                    <option value="<%=sptcd1.getFieldName1()%>" selected="true"><%=sptcd1.getFieldName2()%></option>
                                    <%} else {%>
                                    <option value="<%=sptcd1.getFieldName1()%>"><%=sptcd1.getFieldName2()%></option>
                                    <% }
                                                            }%>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <%
                                if(IsEOI.get(0).getFieldName1().equalsIgnoreCase("REOI"))
                                {
                            %>
                                    <td class="ff">Standard REOI Document : <span>*</span></td>
                                    
                             <%
                                 }
                                 else if(IsEOI.get(0).getFieldName1().equalsIgnoreCase("RFP"))
                                 {
                             %>
                                <td class="ff">Standard Request for Proposal : <span>*</span></td>
                             <%
                                 }
                                 else
                                 {
                             %>
                                <td class="ff">Standard Bidding Document : <span>*</span></td>
                             
                             <%}%>
                            <td>
                                <select name="tenderDocument" id="tenderDocument" class="formTxtBox_1" onchange="fillHDStdTemplateId();"  style="width:250px;">
                                     
                                    <% List<SPTenderCommonData> cmbStdList = tenderCommonService.returndata("getStd", tId, procNature);
                                    if(IsEOI.get(0).getFieldName1().equalsIgnoreCase("REOI"))
                                    {
                                        %><option value="0">--Select Standard REOI Document--</option><%
                                        for (SPTenderCommonData sptcd : cmbStdList)
                                        {
                                            String ChkEOI = sptcd.getFieldName2();
                                            if(ChkEOI.contains("Individual Consultant"))
                                            {
                                                ChkEOI = "";
                                            }
                                            ChkEOI = ChkEOI.replaceAll(" ", "");
                                            boolean AddOption = false;
                                            if(ChkEOI.toLowerCase().contains(EventType))
                                            {
                                                AddOption = true;
                                            }
                                            if(AddOption)
                                            {
                                                if (sptcd.getFieldName2().equalsIgnoreCase(list.get(0).getFieldName6()))
                                                {
                                    %>
                                                    <option value="<%=sptcd.getFieldName1()%>" selected="true"><%=sptcd.getFieldName2()%></option>
                                    <%          }

                                                else if (listSuggestedStdAsPerRule != null && listSuggestedStdAsPerRule.size() == 1 && sptcd.getFieldName2().equalsIgnoreCase(listSuggestedStdAsPerRule.get(0).getFieldName2()))
                                                {
                                    %>
                                                    <option value="<%=sptcd.getFieldName1()%>" selected="true"><%=sptcd.getFieldName2()%></option>
                                    <%          }
                                                else
                                                {
                                    %>
                                                    <option value="<%=sptcd.getFieldName1()%>"><%=sptcd.getFieldName2()%></option>
                                    <%
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if(IsEOI.get(0).getFieldName1().equalsIgnoreCase("RFP"))
                                        {
                                            %><option value="0">--Select Standard Request for Proposal--</option><%
                                        }
                                        else
                                        {
                                             %><option value="0">--Select Standard Bidding Document--</option><%
                                        }
                                    
                                        for (SPTenderCommonData sptcd : cmbStdList)
                                        {
                                            String ChkEOI = sptcd.getFieldName2();
                                            if(ChkEOI.contains("Individual Consultant"))
                                            {
                                                ChkEOI = "";
                                            }
                                            ChkEOI = ChkEOI.replaceAll(" ", "");
                                            boolean AddOption = true;
                                            if(ChkEOI.toLowerCase().contains(EventType))
                                            {
                                                AddOption = false;
                                            }
                                            if(AddOption)
                                            {
                                                if (sptcd.getFieldName2().equalsIgnoreCase(list.get(0).getFieldName6()))
                                                {
                                    %>
                                                    <option value="<%=sptcd.getFieldName1()%>" selected="true"><%=sptcd.getFieldName2()%></option>
                                    <%          }

                                                else if (listSuggestedStdAsPerRule != null && listSuggestedStdAsPerRule.size() == 1 && sptcd.getFieldName2().equalsIgnoreCase(listSuggestedStdAsPerRule.get(0).getFieldName2()))
                                                {
                                    %>
                                                    <option value="<%=sptcd.getFieldName1()%>" selected="true"><%=sptcd.getFieldName2()%></option>
                                    <%          }
                                                else
                                                {
                                    %>
                                                    <option value="<%=sptcd.getFieldName1()%>"><%=sptcd.getFieldName2()%></option>
                                    <%
                                                }
                                            }
                                        }
                                    }

                                    %>


                                </select>
                                <div class="reqF_1" id="valStdTemplate"></div>
                                <input type="hidden" name="hdStdTemplateId" id="hdStdTemplateId"/>
                                <input type="hidden" name="hdStdTemplateChagne" id="hdStdTemplateChagne"/>

                                <!--    Dohatec Start   -->
                                <input type="hidden" name="isEdit" id="isEdit" value="<%=request.getParameter("isEdit")%>"/>
                                <input type="hidden" name="nameSTD" id="nameSTD"/>
                                <!--    Dohatec End     -->
                            </td>
                        </tr>
                            <%  if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                            %>
                        <tr>
                            <td class="ff">Domestic Preference Requires <span>*</span></td>
                            <td>
                                <select name="domesticPreference" class="formTxtBox_1" id="cmbDomesticPreference" style="width:220px;" onchange="addDomPreField();">
                                    <option value="">Select Domestic Preference</option>
                                    <option <% if(isEdit && listDP.get(0).getFieldName2().equalsIgnoreCase("Yes")){%>selected<%}%> value="Yes">Yes</option>
                                    <option <% if(isEdit && listDP.get(0).getFieldName2().equalsIgnoreCase("No")){%>selected<%}%> value="No">No</option>
                                </select>
                                <div class="reqF_1" id="valDomPre"></div>
                            </td>
                        </tr>
                        <tr style="display: none;" id="trDomPre">
                            <td class="ff">Domestic Preference in % <span>*</span></td>
                            <td><input 
                                    
                                    <% if(isEdit)
                                              { if(!listDP.get(0).getFieldName3().equalsIgnoreCase("0"))
                                                        {%>value="<%=listDP.get(0).getFieldName3()%>"<% }

                                               else {%>
                                                        value="5"
                                                     <% }
                                    }
                                   else { %>
                                    value="5"
                                        <% } %>
                                           
                                  type="text"  readonly="true" class="formTxtBox_1" name="domPrePer" id="txtDomPrePer" style="width:200px;"/>
                                <div class="reqF_1" id="valDomPrePer"></div>
                            </td>
                        </tr>
                        <script type="text/javascript">
                            if(document.getElementById('cmbDomesticPreference') != null){
                                if(document.getElementById('cmbDomesticPreference').value == 'Yes'){
                                    document.getElementById('trDomPre').style.display = 'table-row';
                                }
                            }
                        </script>
                        <% } if (!tenConfig.isEmpty() && tenConfig.get(0)[3].toString().equalsIgnoreCase("Yes")) {%>
                        <tr>
                            <td class="ff">Tender Validity in No. of Days : <span>*</span></td>
                            <td><input name="tenderValidity" onblur="validateDays();" type="text" class="formTxtBox_1" id="txttenderValidity" style="width:200px;"  <% if (!list.get(0).getFieldName4().equalsIgnoreCase("0")) {%> value="<%=list.get(0).getFieldName4()%>" <% }%> />
                                <div id="SPMsg"></div>
                                <div id="sptenderval" style="color: red;"></div></td>

                        </tr>
                        <% } if (!tenConfig.isEmpty() && tenConfig.get(0)[2].toString().equalsIgnoreCase("Yes")) {%>
                        <tr>
                            <td class="ff">Tender Security Validity In No. of Days : <span>*</span></td>
                            <td><input name="tenderSecurityValidity" onblur="validateSec();" type="text" class="formTxtBox_1" id="txttenderSecurityValidity" style="width:200px;" maxlength="50" <% if (!list.get(0).getFieldName4().equalsIgnoreCase("0")) {%> value="<%=list.get(0).getFieldName5()%>" <% }%>/>
                                <div id="sptendersecval" style="color: red;">&nbsp;</div></td>
                        </tr>
                        <% } %>
                        <tr align="left">
                            <td align="left" colspan="2">
                                <span id="SpErr" class="reqF_1">

                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td><label class="formBtn_1"><input type="submit" onclick="return Validation();" name="submit" id="btnSubmit" value="Submit"/></label><br/>
                                <!--                                <div class="mandatory" id="msgForDays"></div>-->
                            </td>
                        </tr>
                    </table>

                </form>
            </div>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
        <%}%>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        document.getElementById('hdStdTemplateId').value = document.getElementById('tenderDocument').options[document.getElementById('tenderDocument').selectedIndex].text;
        document.getElementById('hdStdTemplateChagne').value = document.getElementById('tenderDocument').options[document.getElementById('tenderDocument').selectedIndex].text;
        
        //  Dohatec Start
            var nameSTD = document.getElementById('tenderDocument').options[document.getElementById('tenderDocument').selectedIndex].text;
            document.getElementById('nameSTD').value = nameSTD;
        //  Dohatec End

        function fillHDStdTemplateId(){
             //document.getElementById('hdStdTemplateChagne').value = document.getElementById('tenderDocument').options[document.getElementById('tenderDocument').selectedIndex].text;
             // There was no condition.
             // 
             // Dohatec Start
                 if(document.getElementById('isEdit').value == 'yes') {
                     var newSTD = document.getElementById('tenderDocument').options[document.getElementById('tenderDocument').selectedIndex].text;
                     var oldSTD = document.getElementById('nameSTD').value;
                     if(newSTD !=  oldSTD && document.getElementById('tenderDocument').value != 0) {
                         jConfirm("If you select a new SBD, filled-in data in the previous document will be erased.\n\nDo you still want to select a new SBD?","SBD", function(RetVal){
                             if(!RetVal){
                                newSTD = oldSTD;
                                $('#tenderDocument').val($('#tenderDocument option:contains(' + newSTD + ')').val());
                                 document.getElementById('hdStdTemplateChagne').value = newSTD;
                             }
                         });
                     } else {
                         newSTD = oldSTD;
                     }
                     document.getElementById('hdStdTemplateChagne').value = newSTD;
                 } else {   //  Old Version
                     document.getElementById('hdStdTemplateChagne').value = document.getElementById('tenderDocument').options[document.getElementById('tenderDocument').selectedIndex].text;
                 }
             // Dohatec End
        }
    </script>
</html>

