<%-- 
    Document   : RequestBankGuarantee
    Created on : Sep 20, 2011, 4:54:07 PM
    Author     : rikin.p
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPerfSecurity"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TblTenderPerformanceSec"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Dohatec Start -->
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<!-- Dohatec End -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/form/CommonValidation.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language="javascript" type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d-%b-%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        var DateVar = document.getElementById("curDate").value;
                        var currentDate =Date.parse(DateVar);
                        if(date < currentDate){
                            this.hide();
                            alert("Payment end date must be greater than current date","Warning!");
                            document.getElementById(txtname).value = "";
                        }else{
                            LEFT_CAL.args.min = date;
                            LEFT_CAL.redraw();
                            this.hide();
                        }
//                        var date = Calendar.intToDate(this.selection.get());
//                        LEFT_CAL.args.min = date;
//                        LEFT_CAL.redraw();
//                        this.hide();
                        //checkDate(txtname);
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })

            }
            function getDate(value)
            {
               var splitedDate = value.split("-");
               return Date.parse(splitedDate[1]+" "+splitedDate[0]+", "+splitedDate[2]);
                
//               var mdy = value.split('/')  //Date and month split
//               var mdyhr= mdy[2].split(' ');  //Year and time split
//               
//               var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
//               return valuedate;
            }
            
            function calculateNewDays()
            {
                var firstDate = getDate(document.getElementById("dtEndDate").value);
                var validityDays  = document.getElementById("txtValidityDays").value;
                document.getElementById("validityDaysMsg").innerHTML = "";
                
                if(validityDays!="")
                {
                    if(!/^\d+$/.test(document.getElementById("txtValidityDays").value))
                    {
                            document.getElementById("validityDaysMsg").innerHTML = "Only numeric values are allowed.";
                            document.getElementById("dtValidity").value = "";
                            return false;
                    }else if(document.getElementById("dtEndDate").value==""){
                        document.getElementById("dateMsg").innerHTML = "Please enter last date of payment";
                        flag = false;
                    }
                    else
                    {
                        var enddate = new Date(firstDate).getTime()+(parseInt(validityDays)*24*60*60*1000);
                        var monthname = new Array("Jan","Feb","Mar","Apr","May","Jun", "Jul","Aug","Sep","Oct","Nov","Dec");
                        var enddate = new Date(enddate);
                        var finalenddate = enddate.getDate()+"-"+monthname[enddate.getMonth()]+"-"+enddate.getFullYear();
                        document.getElementById("dtValidity").value = finalenddate; 
                    }
                }
            }
            
            function validate()
            {
                var flag = true;
                //document.getElementById("dateMsg").innerHTML ="";
                document.getElementById("validityDaysMsg").innerHTML = "";
                document.getElementById("dateMsg").innerHTML = "";

                // Dohatec Start
                if($('#isICT').val() == 'ICT')
                {
                    var listSize = $('#listSize').val();
                    for(var i=0; i<listSize; i++)
                    {
                        document.getElementById("amountMsg_"+i).innerHTML = "";
                        if(trim(document.getElementById("txtAmount_"+i).value)==""){
                            document.getElementById("amountMsg_"+i).innerHTML = "Please enter amount";
                            flag =  false;
                        }else{

                            if(!/^(\d+(\.\d{1,3})?)$/.test(document.getElementById("txtAmount_"+i).value)){
                                document.getElementById("amountMsg_"+i).innerHTML = "Only numeric values are allowed. Maximum 3 Decimals are allowed.";
                                flag = false;
                            }
                            if(!checkperfValue(document.getElementById("txtAmount_"+i).value)){
                                flag =  false;
                            }
                        }
                    }
                }    // Dohatec End
                else    // Old Version
                {
                    document.getElementById("amountMsg").innerHTML = "";
                if(trim(document.getElementById("txtAmount").value)==""){
                    document.getElementById("amountMsg").innerHTML = "Please enter amount";
                    flag =  false;
                }else{
//                    if(!numericDE(document.getElementById("txtAmount").value)){
//                        document.getElementById("amountMsg").innerHTML = "Please enter valid amount";
//                        return false;
//                    }
                    
                    if(!/^(\d+(\.\d{1,3})?)$/.test(document.getElementById("txtAmount").value)){
                        document.getElementById("amountMsg").innerHTML = "Only numeric values are allowed. Maximum 3 Decimals are allowed.";
                        flag= false;
                    }
                    if(!checkperfValue(document.getElementById("txtAmount").value)){
                        flag=  false;
                    }
                }
                //alert(/^\d+$/.test(document.getElementById("txtValidityDays").value));
                }
                
                if(trim(document.getElementById("txtValidityDays").value)=="" || parseInt(document.getElementById("txtValidityDays").value)==0){
                    document.getElementById("validityDaysMsg").innerHTML = "Please enter validity in days";
                    flag= false;
                }else{
                    if(document.getElementById("txtValidityDays").value.length > 4){
                        document.getElementById("validityDaysMsg").innerHTML = "Maximum 4 digits are allowed";
                        document.getElementById("dtValidity").value = "";
                        flag= false;
                    }
                    if(!/^\d+$/.test(document.getElementById("txtValidityDays").value)){
                        //document.getElementById("allBudMsg").innerHTML = "<br/>Please enter numeric values only. Max 2 decimals allowed";
                        document.getElementById("validityDaysMsg").innerHTML = "Only numeric values are allowed.";
                        document.getElementById("dtValidity").value = "";
                        flag= false;
                    }
//                    if(!numericDE(document.getElementById("txtAmount").value)){
//                        document.getElementById("validityDaysMsg").innerHTML = "Please enter valid validity in days";
//                        flag = false;
//                    }
                }
                
                if(document.getElementById("dtEndDate").value==""){
                    document.getElementById("dateMsg").innerHTML = "Please enter last date of payment";
                    flag= false;
                }
//                else
//                {
//                    var d =new Date();
//                    var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());
//                    var value=document.getElementById("dtEndDate").value;
//                    
//                    var mdy = value.split('-')  //Date and month split                    
//                    var mdyhr= mdy[2].split(' ');  //Year and time split
//                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
//                    
//                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
//                    if(Date.parse(valuedate) > Date.parse(todaydate)){
//                        document.getElementById("dateMsg").innerHTML = "Payment end date must be greater than current date";
//                        flag= false;
//                    }
//                }
                
                if($.trim($('#txtComments').val()).length >=2000) {
                    $('#SpError7').html('Maximum 2000 characters are allowed.');
                    flag= false;
                }
                if(flag){
                    return true;
                }else{
                    return false;
                }

            }
            
            function checkperfValue(amount){
                
                //document.getElementById("amountMsg").innerHTML = "";
                var perfSecAmount = document.getElementById("perfAmount").value;
                if(amount==0)
                {   document.getElementById("amountMsg").innerHTML = "New Performance Security can not be 0 (in Nu.)";
                    return false;
                }    
                if(parseFloat(amount) > parseFloat(perfSecAmount)){
                    document.getElementById("amountMsg").innerHTML = "New Performance Security should not be greater than 50% of Performance Security";
                    document.getElementById("txtAmount").focus();
                    return false;
                }
                return true;
            }

            // Dohatec Start
            function checkperfValueICT(amount, id){

                var txtNo = id.split('_');
                var i = txtNo[1];
                document.getElementById("amountMsg_"+i).innerHTML = "";
                var perfSecAmount = document.getElementById("perfAmount_"+i).value;
                if(amount==0)
                {   document.getElementById("amountMsg_"+i).innerHTML = "New Performance Security can not be 0";
                    return false;
                }
                if(parseFloat(amount) > parseFloat(perfSecAmount)){
                    document.getElementById("amountMsg_"+i).innerHTML = "New Performance Security should not be greater than 50% of Performance Security";
                    document.getElementById("txtAmount_"+i).focus();
                    return false;
                }
                return true;
            }
            // Dohatec End
        function numericDE(value) {
            return  /^(\d+(\.\d{1,3})?)$/.test(value);
        }
        </script>
        <title>New Performance Security</title>
    </head>
    <%
                String tenderId = "";
                String userId = "";
                String lotId ="";
                String uId = "";
                
                if (request.getParameter("tenderId") != null) {
                    tenderId = request.getParameter("tenderId");
                    pageContext.setAttribute("tenderId", tenderId);
                }
                if (session.getAttribute("userId") == null) {
                    response.sendRedirect("SessionTimedOut.jsp");
                } else {
                    userId = session.getAttribute("userId").toString();
                }                
                if (request.getParameter("lotId") != null) {
                    lotId = request.getParameter("lotId");
                }
                if (request.getParameter("uId") != null) {
                    uId = request.getParameter("uId");
                }
                if (request.getParameter("lotId") != null) {
                    pageContext.setAttribute("lotId", lotId);
                }
                
                TblTenderPerformanceSec tblTenderPerformanceSec = (TblTenderPerformanceSec) AppContext.getSpringBean("TblTenderPerformanceSec");
                TblTenderPerfSecurity perfSec =  tblTenderPerformanceSec.getPerfSec(Integer.parseInt(userId),Integer.parseInt(tenderId),Integer.parseInt(lotId));
                CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
                String procnature = commonservice.getProcNature(request.getParameter("tenderId")).toString();

                // Dohatec Start
                TenderCommonService objTCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                List<SPTenderCommonData> listDP = objTCS.returndata("chkDomesticPreference", tenderId, null);
                boolean isIctTender = false;
                if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                    isIctTender = true;
                }
                // Dohatec End
    %>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                              <%@include file="../resources/common/TenderInfoBar.jsp" %>
                              <%@include  file="../resources/common/ContractInfoBar.jsp"%>
                            <br/>
                            <div class="pageHead_1">Bank Guarantee/Bank Draft for New Performance Security
                                <span style="float: right; text-align: right;">
                                    <%
                                        if("services".equalsIgnoreCase(procnature))
                                        {
                                    %>
                                        <a class="action-button-goback" href="InvoiceServiceCase.jsp?tenderId=<%=tenderId%>">Go back</a>
                                        <%}else{%>
                                        <a class="action-button-goback" href="Invoice.jsp?tenderId=<%=tenderId%>">Go back</a>
                                        <%}%>
                                </span>
                            </div>
                            <form name="frmBankGuarantee" id="frmBankGuarantee" method="post" action="<%=request.getContextPath()%>/BankGuaranteeServlet">
                                <input type="hidden" name="hidTenderId" id="hidTenderId" value="<%=tenderId%>" />
                                <input type="hidden" name="hidLotId" id="hidLotId" value="<%=lotId%>" />
                                <input type="hidden" name="hiduId" id="hiduId" value="<%=uId%>" />
                                <input type="hidden" name="action" id="action" value="requestBankGuarantee" />
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" >
                                <!-- Dohatec Start -->
                                <input type="hidden" name="isICT" id="isICT" <%if(isIctTender){%> value="ICT" <%}else{%> value="NCT" <%}%> />
                                <%
                                    if(isIctTender) {
                                        String txtCurrencySymbol = "";
                                        String currencyType = "";
                                        BigDecimal perSecAmount = BigDecimal.ZERO;
                                        CommonSearchDataMoreService objTSDetails = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                        List<SPCommonSearchDataMore> lstPerSecPayDetail = objTSDetails.geteGPData("getPerformanceSecurityPaymentDetail", tenderId);
                                        if(!lstPerSecPayDetail.isEmpty())
                                        {
                                            System.out.print("\n List Size : " + lstPerSecPayDetail.size());
                                            for(int i = 0; i<lstPerSecPayDetail.size(); i++)
                                            {
                                                txtCurrencySymbol = lstPerSecPayDetail.get(i).getFieldName13();
                                                currencyType = lstPerSecPayDetail.get(i).getFieldName12();
                                                perSecAmount = new BigDecimal(lstPerSecPayDetail.get(i).getFieldName4());
                                %>
                                    <tr>    <!-- Old version -->
                                        <td class='ff' class='t-align-left' width="20%">
                                            Amount (in <%=currencyType%>) : <span class="mandatory">*</span>
                                        </td>
                                        <td class='t-align-left'>
                                            <input type="text" name="txtAmount" id="txtAmount_<%=i%>" <%if(perSecAmount!=null){%>value="<%=perSecAmount.setScale(3,0).divide(new BigDecimal(2)).setScale(3,0)%>"<%}%> onblur="checkperfValueICT(this.value, this.id)" class="formTxtBox_1" maxlength="12"/>
                                            <input type="hidden" name="perfAmount" id="perfAmount_<%=i%>" <%if(perSecAmount!=null){%>value="<%=perSecAmount.setScale(3,0).divide(new BigDecimal(2)).setScale(3,0)%>"<%}%> />
                                            <br/><span class="reqF_2" id="amountMsg_<%=i%>"></span>
                                            <input type="hidden" name="listSize" id="listSize" value="<%=lstPerSecPayDetail.size()%>" />
                                            <input type="hidden" name="currencyName" id="currencyName_<%=i%>" value="<%=currencyType%>" />
                                        </td>
                                    </tr>
                                <%}}} else {%>
                                    <tr>    <!-- Old version -->
                                        <td class='ff' class='t-align-left' width="20%">
                                            Amount (In Nu.) : <span class="mandatory">*</span>
                                        </td>
                                        <td class='t-align-left'>
                                            <input type="text" name="txtAmount" id="txtAmount" <%if(perfSec.getPerSecurityAmt()!=null){%>value="<%=perfSec.getPerSecurityAmt().setScale(3,0).divide(new BigDecimal(2)).setScale(3,0)%>"<%}%> onblur="checkperfValue(this.value)" class="formTxtBox_1" maxlength="12"/>
                                            <input type="hidden" name="perfAmount" id="perfAmount" <%if(perfSec.getPerSecurityAmt()!=null){%>value="<%=perfSec.getPerSecurityAmt().setScale(3,0).divide(new BigDecimal(2)).setScale(3,0)%>"<%}%> />
                                            <br/><span class="reqF_2" id="amountMsg"></span>
                                        </td>
                                    </tr>
                                <%}%>
                                <!-- Dohatec End -->
                                <tr>
                                    <td class='ff' class='t-align-left'>
                                        Last Date of Submission : <span class="mandatory">*</span>
                                    </td>
                                    <td class='t-align-left'>
                                        <input name="dtEndDate" type="text" class="formTxtBox_1" id="dtEndDate" readonly="true"  onfocus="GetCal('dtEndDate','dtEndDate');" value="" onblur="calculateNewDays();" />
                                        <img id="dtEndDate1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('dtEndDate','dtEndDate1');" /><br /><span class="reqF_2" id="dateMsg"></span>
                                    </td> 
                                </tr>
                                <tr>
                                    <td class='ff' class='t-align-left' width="15%">
                                        Validity in No. of Days : <span class="mandatory">*</span>
                                    </td>      
                                    <td class='t-align-left'>
                                        <input type="text" name="txtValidityDays" id="txtValidityDays" value="" maxlength="4" class="formTxtBox_1" onblur="calculateNewDays()" /><br/><span class="reqF_2" id="validityDaysMsg" ></span>
                                    </td> 
                                </tr>
                                <tr>
                                    <td class='ff' class='t-align-left'>
                                        Last Date of Validity :
                                    </td>      
                                    <td class='t-align-left'>
                                        <input name="dtValidity" type="text" class="formTxtBox_1" id="dtValidity" readonly="true"  value=""/><br /><span class="reqF_2" id="dateMsg"></span>
                                    </td> 
                                </tr>
                                <tr>
                                    <td class="ff">Remarks : </td>
                                    <td><textarea class="formTxtBox_1" name="txtComments" id="txtComments" rows="5" cols="100"></textarea>
                                        <span class="reqF_1" id="SpError7"></span>

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="t-align-center">
                                        <label class="formBtn_1">
                                            <input type="submit" name="btnSave" id="btnSave" value="Save" onclick="return validate();" />
                                        </label>
                                    </td>
                                </tr>
                            </table>
                            </form>
                        </td><!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End-->
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
</html>