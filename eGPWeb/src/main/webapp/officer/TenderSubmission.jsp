<%-- 
    Document   : TenderSubmission
    Created on : Jan 7, 2011, 6:53:17 PM
    Author     : rajesh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page  import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page  import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService "%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page  import="com.cptu.egp.eps.service.serviceinterface.TenderBidService"%>
<%@page  import="com.cptu.egp.eps.web.utility.XMLReader"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Bidder/Consultant Submission Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
          <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    </head>
    <%
                CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");

                String userId = request.getParameter("Uid");
                boolean isSubDt = false;
                
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }

                 List<SPCommonSearchData> submitDateSts= commonSearchService.searchData("chkSubmissionDt", tenderId, null, null, null, null, null, null, null, null);
             if(submitDateSts.get(0).getFieldName1().equalsIgnoreCase("true"))
                 {isSubDt = true;}
             else{isSubDt=false;}
    %>
    <body>
<div class="contentArea_1">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            
                <div class="pageHead_1">Document Read Confirmation
                <span style="float:right;"><a href="TOS.jsp?tid=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a></span></div>
               
                <div class="mainDiv">
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <% boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");%>
                </div>
                <div>&nbsp;</div>
               <% pageContext.setAttribute("tab", 4);%><%--
                        <jsp:include page="TendererTabPanel.jsp" >
                            <jsp:param name="tab" value="4" />
                        </jsp:include>--%>

                <div class="tabPanelArea_1">
                    <%
                    HttpSession sn1 = request.getSession();
                        String userTypeId1 = sn1.getAttribute("userTypeId").toString();
                        
                                if (isTenPackageWis) {
                                    for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null))
                                    {
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td colspan="2" class="t-align-left ff">Tender Submission Details</td>
                        </tr>

                        <tr>
                            <td width="22%" class="t-align-left ff">Package No. :</td>
                            <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                        </tr>
                    </table>
                        
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <td width="22%" class="t-align-left ff">Tender Mega Hash :</td>
                            <td width="78%" class="t-align-left">
                                <%for (SPCommonSearchData megaHash : commonSearchService.searchData("getTenderMegaHash", tenderId, userId, "0", null, null, null, null, null, null)) {
                                                            out.println(megaHash.getFieldName1());
                                    }%>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Tender Mega Mega Hash :</td>
                            <td class="t-align-left">
                                   <%for (SPCommonSearchData megaHash : commonSearchService.searchData("getTenderMegaMegaHash", tenderId, userId, "0", null, null, null, null, null, null)) {
                                                                out.println(megaHash.getFieldName1());
                                    }%>

                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="35%" class="t-align-left ff">Form Name</th>

                            <th width="13%" class="t-align-left ff">Filled (Yes/No)</th>
                            <th width="13%" class="t-align-left ff">Mandatory (Yes/No)</th>
                            <th width="18%" class="t-align-left ff">Encrypted with Buyer Hash</th>
                            <th width="21%" class="t-align-left ff">Hash</th>
                        </tr>
                        <%
                                                          for (SPCommonSearchData formdetails : commonSearchService.searchData("getformDetails", tenderId, userId, "0", null, null, null, null, null, null)) {
                        %>
                        <tr>
                            <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>

                            <td class="t-align-center ff"><%=formdetails.getFieldName3()%></td>
                            <td class="t-align-center ff"><%=formdetails.getFieldName6()%></td>
                            <td class="t-align-center ff"><%=formdetails.getFieldName4()%></td>
                            <%if(formdetails.getFieldName7()!=null){%>
                            <td class="t-align-left ff"><%=formdetails.getFieldName7()%></td>
                            <%}else{%>
                            <td class="t-align-left ff"></td>
                            <%}%>
                        </tr>
                        <% }%>
                    </table>
                    <% }
                                               } else {

                                                   for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null))
                                                   {
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td colspan="2" class="t-align-left ff">Tender Submission Details</td>
                        </tr>
                        <tr>
                            <td width="22%" class="t-align-left ff">Package No. :</td>
                            <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <td width="22%" class="t-align-left ff">Tender Mega Hash :</td>
                            <td width="78%" class="t-align-left">
                                <%for (SPCommonSearchData megaHash : commonSearchService.searchData("getTenderMegaHash", tenderId, userId, "0", null, null, null, null, null, null)) {
                                                            out.println(megaHash.getFieldName1());
                                    }%>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Tender Mega Mega Hash :</td>
                            <td class="t-align-left">
                                <%for (SPCommonSearchData megaHash : commonSearchService.searchData("getTenderMegaMegaHash", tenderId, userId, "0", null, null, null, null, null, null)) {
                                                            out.println(megaHash.getFieldName1());
                                    }%>
                            </td>
                        </tr>
                    </table>
                    <%  }
                       for (SPCommonSearchData lotList : commonSearchService.searchData("gettenderlotbytenderid", tenderId, "", "", null, null, null, null, null, null))
                        {
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">

                        <tr>
                            <td width="22%" class="t-align-left ff">Lot No. :</td>
                            <td width="78%" class="t-align-left"><%=lotList.getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Lot Description :</td>
                            <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                        </tr>
                    </table>
                    
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="35%" class="t-align-left ff">Form Name</th>
                            <!--                                <th width="23%" class="t-align-left">Mapped Documents List</th>-->
                            <th width="13%" class="t-align-left ff">Filled (yes/No)</th>
                            <th width="13%" class="t-align-left ff">Mandatory (yes/No)</th>
                            <th width="18%" class="t-align-left ff">Encrypted with Buyer Hash</th>
                            <th width="21%" class="t-align-left ff">Hash</th>

                        </tr>
                        <%
                          for (SPCommonSearchData formdetails : commonSearchService.searchData("getformDetails", tenderId, userId, lotList.getFieldName3(), null, null, null, null, null, null))
                          {
                        %>
                        <tr>
                            <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                            <td class="t-align-center ff"><%=formdetails.getFieldName3()%></td>
                            <td class="t-align-center ff"><%=formdetails.getFieldName6()%></td>
                            <td class="t-align-center ff"><%=formdetails.getFieldName4()%></td>
                            <%if(formdetails.getFieldName7()!=null){%>
                            <td class="t-align-left ff"><%=formdetails.getFieldName7()%></td>
                            <%}else{%>
                            <td class="t-align-left ff"></td>
                            <%}%>
                        </tr>
                        <% }%>
                    </table>
                    <%     }
                                    }%>

                    <%
                                if (request.getParameter("btnFinalSub") != null) {
                                    //out.println("kinj");
                                    //out.println(tenderId);
                                    //out.println(userId);

                                     String ipAddress = request.getHeader("X-FORWARDED-FOR");
                                                                         
                                     if (ipAddress == null || "null".equals(ipAddress)) {
                                         ipAddress = request.getRemoteAddr(); }
                                    List<SPCommonSearchData> lotList = commonSearchService.searchData("doFinalSubmission", tenderId, userId, ipAddress, null, null, null, null, null, null);

                                    if (!lotList.isEmpty()) {


                                        if (lotList.get(0).getFieldName1().equalsIgnoreCase("1")) {


                                           //For mail, sms and msgBox(TaherT)
                                           TenderBidService tenderBidService = (TenderBidService)AppContext.getSpringBean("TenderBidService");
                                           SPTenderCommonData sptcd = tenderCommonService.returndata("tenderinfobar",id,null).get(0);
                                            UserRegisterService userRegisterService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                                            String data[]= tenderBidService.getTendererMobileNEmail(session.getAttribute("userId").toString());
                                            String mails[]={data[0]};
                                            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                                            userRegisterService.contentAdmMsgBox(data[0], XMLReader.getMessage("msgboxfrom"), "Final submission of a tender on e-GP", msgBoxContentUtility.bidTenderSubMsg(tenderId, sptcd.getFieldName2(),"final"));
                                            SendMessageUtil sendMessageUtil = new SendMessageUtil();
                                            MailContentUtility mailContentUtility = new MailContentUtility();
                                            String mailText = mailContentUtility.bidTenderSubMail(tenderId, sptcd.getFieldName2(),"final");
                                            sendMessageUtil.setEmailTo(mails);
                                            sendMessageUtil.setEmailSub("Final submission of a tender on e-GP");
                                            sendMessageUtil.setEmailMessage(mailText);
                                            sendMessageUtil.sendEmail();
                                            sendMessageUtil.setSmsNo(data[1]+data[2]);
                                            sendMessageUtil.setSmsBody("Dear User,%0AThis is to inform you that you have successfully completed final submission in a below mentioned tender Id:"+tenderId+" Reference No: "+sptcd.getFieldName2());
                                            sendMessageUtil.sendSMS();
                                            data=null;
                                            mails=null;
                                            sendMessageUtil=null;
                                            msgBoxContentUtility=null;
                                            //msg sending ended

                                            response.sendRedirect("SubmissionReceipt.jsp?tenderId=" + tenderId);
                                        } else if ("0".equalsIgnoreCase(lotList.get(0).getFieldName1())) {
                                            out.print("<div width='100%' align='center'><font style='color:red;'>" + lotList.get(0).getFieldName2() + "</font></div>");

                                        }
                                    }
                                }
                    %>

<form action="" id="frmDocReadConf" method="POST">
                    <%//if (paidCnt>0) {%>
                    <%if (!isSubDt){ /// IF SUBMISSION DATE GETS OVER%>
                    <%--<div class="t-align-center t_space">
                        <label class='reqF_1'>Submission Date is Over </label>
                    </div>--%>


                     <%} else { /// IF SUBMISSION DATE LIVE %>
                    <div class="t-align-center t_space">
                        <%
                            List<SPCommonSearchData> btnFinallst = commonSearchService.searchData("chkBidWithdrawal", tenderId, userId, null, null, null, null, null, null, null);
                              if (!btnFinallst.isEmpty()) {
                         if (btnFinallst.get(0).getFieldName1().equalsIgnoreCase("modify") || btnFinallst.get(0).getFieldName1().equalsIgnoreCase("Pending")  || btnFinallst.get(0).getFieldName1()== null)
                        {


                        %>
                                <label class="formBtn_1"><input name="btnFinalSub" type="Submit" value="Final Submission"/></label>
                        <%
                          }else if (btnFinallst.get(0).getFieldName1().equalsIgnoreCase("finalsubmission")) {
                              List<SPCommonSearchData> showhidebutton= commonSearchService.searchData("showhidesubmissionbutton", userTypeId1, null, null, null, null, null, null, null, null);
                            if(showhidebutton.get(0).getFieldName1().equalsIgnoreCase("Tenderer")){
                         %>
                                <label class="l_space"><a href="BidWithdrawal.jsp?bid=w&tenderid=<%=tenderId%>" class="anchorLink">Bid Withdrawl</a></label>
                                <label class="l_space"><a href="BidWithdrawal.jsp?tenderid=<%=tenderId%>" class="anchorLink">Modification</a></label>
                        <%}}} else {%>
                                <label class="formBtn_1">
                                    <input name="btnFinalSub" type="Submit" value="Final Submission"/></label>
                                <%}%>

                    </div>
                    <%}//}%>
                     </form>
                </div>  </div>
        </div>
 </div>
        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
