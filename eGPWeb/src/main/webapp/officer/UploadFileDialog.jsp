<%-- 
    Document   : UploadFileDialog
    Created on : Nov 9, 2010, 4:57:09 PM
    Author     : test
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowDocuments"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

                    if(session.getAttribute("userId") == null){
                        response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Uploade File Dialog</title>

        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>


        <script type="text/javascript">
            function validateFileDesc(value)
            {
                return /^([0-9a-zA-Z\@\*\(\)\-\+\.\s\/\,]*)[a-zA-Z]+([0-9a-zA-Z\@\*\(\)\-\+\.\s\/\,]*)$/.test(value);
            }
            function checkfile()
            {
                var check = false;
                value1=document.getElementById('uploadDocFile').value;
                param="document";
                var ext = value1.split(".");
                var fileType = ext[ext.length-1];
                fileType=fileType.toLowerCase();
                if (value1 != ''){
                    document.getElementById('filemsg').innerHTML='';
                    check = true;
                   
                }
                if (document.getElementById('description').value=='' && check == false)
                {
                    document.getElementById('filemsg').innerHTML="Please select file.";
                    document.getElementById('dvDescpErMsg').innerHTML="<div class='reqF_1'>Please enter Description.</div>";
                    return false;
                }
                if (document.getElementById('description').value=='' && check == true){
                    document.getElementById('filemsg').innerHTML='';
                    document.getElementById('dvDescpErMsg').innerHTML="<div class='reqF_1'>Please enter Description.</div>";
                    return false;
                }else if(!validateFileDesc(document.getElementById('description').value)){
                    document.getElementById("dvDescpErMsg").innerHTML="<div class='reqF_1'>Allows Alphanumeric and Special Characters (@,*, (,), -, +,/,., Space and ,)</div>";
                    return false;
                }
                else if (document.getElementById('description').value != '' && value1 != ''){
                    document.getElementById('filemsg').innerHTML='';
                    document.getElementById('dvDescpErMsg').innerHTML='';
                    return true;
                }else if (document.getElementById('description').value != '' && check == false){
                    document.getElementById('dvDescpErMsg').innerHTML='';
                    document.getElementById('filemsg').innerHTML="Please select Document";

                    return false;
                }

            }


            function closeWondow(){
                window.opener.getDocData();
                window.close();
            }

            $(function() {
                $('#frmUploadDoc').submit(function() {
                    $('#lbUpload').css("visibility", "collapse");
                    $('#lbClose').css("visibility", "collapse");
                });
            });
            $(function() {
                $('#frmUploadDoc').submit(function() {
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#Upload').attr("disabled", "disabled");
                            
                            return true;
                        }else{
                            $('#lbUpload').css("visibility", "visible");
                            $('#lbClose').css("visibility", "visible");
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body onunload="doUnload()">
        <%
                    String activityid = request.getParameter("activityid");
                    String userid = request.getParameter("userid");
                    String objectid = request.getParameter("objectid");
                    String childid = request.getParameter("childid");
                    String action = request.getParameter("action");
                    String errormsg = request.getParameter("msg");
                    String msg = null;
                    msg = "file size must be 2MBand extension must be withinjpeg, png, bmp, xls, xlsx, doc, docx, zip, rar";
        %>
        <div class="contentArea_1">
            <div class="tableHead_1 t_space">Upload Files :</div>
            <% if (errormsg != null) {
             if (errormsg.contains("not") || errormsg.contains("size")) {%>
            <div class="responseMsg errorMsg" style="margin-top: 10px;"><%=errormsg%></div>
            <% }else {%>
            <div class="responseMsg successMsg" style="margin-top: 10px;"><%=errormsg%></div>
            <% }
                }%>

            <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/WorkFlowFileUploadServlet" enctype="multipart/form-data" name="frmUploadDoc">
                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">

                    <tr>
                        <td width="15%" class="ff t-align-left">Document   : <span class="mandatory">*</span></td>
                        <td width="85%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                            <label id="filemsg" class='reqF_1' ></label>
                        </td>
                    <tr>
                        <td class="ff">Description : <span>*</span></td>
                        <td>

                            <input name="description" type="text" class="formTxtBox_1" maxlength="100" id="description" style="width:200px;" />
                            <div id="dvDescpErMsg" class='reqF_1'></div>
                        </td>
                    </tr>
                    <tr>
                        <td></td>

                        <td><label class="formBtn_1" style="visibility:visible;" id="lbUpload"><input type="submit"  value="Upload" id="Upload" name="upload"  onclick="return checkfile();"/></label>
                            <label class="formBtn_1" style="visibility:visible;" id="lbClose"><input type="button" name="btnClose" id="btnClose" value="Close" onclick="return closeWondow()" /></label>
                            <input type="hidden" value="<%=objectid%>" name="objectid" />
                            <input type="hidden" value="<%=activityid%>" name="activityid" />
                            <input type="hidden" value="<%=userid%>" name="userid" />
                            <input type="hidden" value="<%=childid%>" name="childid" />
                        </td>
                    </tr>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="100%"  class="t-align-left">Instructions</th>
                        </tr>
                        <tr>
                            <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("officer");%>
                            <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                        </tr>
                    </table>
                </table>
            </form>


            <div>
                <%

                            WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                            List<TblWorkFlowDocuments> tblWorkFlowDocuments =
                                    workFlowSrBean.getWorkFlowDocuments(Integer.parseInt(objectid),
                                    Integer.parseInt(activityid), Integer.parseInt(childid), Integer.parseInt(userid));
                            if (tblWorkFlowDocuments.size() > 0) {
                                request.setAttribute("tblWorkFlowDocuments", tblWorkFlowDocuments);
                            }

                %>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <form method="post">
                        <tr>
                            <th class="t-align-center">Sl. No</th>
                            <th>File Name</th>
                            <th class="t-align-center">Size in KB</th>
                            <th>Description</th>
                            <th class="t-align-center" >Action</th>
                        </tr>
                        <%
                        int c = 2;
                        String colorchange = "style='background-color:#E4FAD0;' ";
                                    if (tblWorkFlowDocuments.size() > 0) {
                                        Iterator docs = tblWorkFlowDocuments.iterator();
                                        int i = 1;
                                        while (docs.hasNext()) {

                                            TblWorkFlowDocuments twfd = (TblWorkFlowDocuments) docs.next();
                                          %>
                                   <tr <% if(c == i) out.print(colorchange); %> >
                                          <%
                                            out.write("<td class='t-align-center' >" + i + "</td>");
                                            out.write("<td>" + twfd.getDocumentName() + "</td>");
                                            out.write("<td class='t-align-center'>" + (Long.parseLong(twfd.getDocSize()) / 1024) + "</td>");
                                            out.write("<td>" + twfd.getDescription() + "</td>");
                                            out.write("<td class='t-align-center'><a href='" + request.getContextPath() + "/workflowdocservlet?docName=" + twfd.getDocumentName() + "&docSize=" + twfd.getDocSize() + "&action=download&docid=" + twfd.getWfDocumentId() + "&objectid="
                                                    + objectid + "&activityid=" + activityid + "&childid=" + childid + "&userid=" + userid + "'><img src='../resources/images/Dashboard/Download.png' alt='Download'></a>");
                                            out.write("&nbsp;&nbsp;<a href='" + request.getContextPath() + "/workflowdocservlet?docName=" + twfd.getDocumentName() + "&docId=" + twfd.getWfDocumentId() + "&action=remove&userid=" + userid + "&objectid="
                                                    + objectid + "&activityid=" + activityid + "&childid=" + childid + "' ><img alt='Delete' src='../resources/images/Dashboard/Delete.png'></a></td>");
                                            out.write("</tr>");
                                             if(c == i)
                                             c+=2;
                                            i++;
                                        }
                                    }
                        %>


                    </form>
                </table>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

<script type="text/javascript">
    function doUnload()
    {
        window.opener.document.getElementById('filelable').innerHTML= 'Uploaded Documents :';
        window.opener.getDocData();

    }
</script>