    <%--
        Document   : ReEvaluation
        Created on : Dec 31, 2014, 2:53:18 PM
        Author     : Ahsan
            Comment    : New file for showing previous evaluation report
    --%>

    <%@page import="java.util.ArrayList"%>
    <%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
    <%@page import="com.cptu.egp.eps.web.servicebean.TenderOpening"%>
    <%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
    <%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
    <%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
    <%@page import="java.util.List"%>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
    <%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk,com.cptu.egp.eps.web.utility.HandleSpecialChar,java.text.SimpleDateFormat,com.cptu.egp.eps.web.utility.MailContentUtility,com.cptu.egp.eps.web.utility.SendMessageUtil" %>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

    <html>
    <head>
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List of Tenderers/Consultants for seeking clarification View Query / Clarification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
    <div class="dashboard_div">

    <%
    boolean isQueSentToCP = false, isEvalNotificationGiven = false;
    String tenderId;

    String sentBy = "", role = "", committeetype = "";
    String strTeamSelUserId = "0";
    String lotId = "0";
    String roundId = "0";
    boolean aa = false;
    CommonSearchService commonSearchService1 = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
    TenderCommonService cmnService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    EvaluationService evalSrvice = (EvaluationService) AppContext.getSpringBean("EvaluationService");
    CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");


    tenderId = request.getParameter("tenderId");

               // String roundId = request.getParameter("roundId");

    if (session.getAttribute("userId") != null) {
    sentBy = session.getAttribute("userId").toString();
    }

    List<SPCommonSearchData> lstRole =
    commonSearchService1.searchData("EvalMemberRole", tenderId, sentBy, null, null, null, null, null, null, null);

    if (!lstRole.isEmpty()) {
        role = lstRole.get(0).getFieldName2();
        committeetype = lstRole.get(0).getFieldName3();
    }
    else
        aa = true;
    lstRole = null;

    // Get the UserId of the Evaluation Member selected for doing Evaluation
    List<SPTenderCommonData> lstTeamSelUserId =
    cmnService.returndata("getEvalTeamSelectedMember", tenderId, null);
    if (!lstTeamSelUserId.isEmpty()) {
        strTeamSelUserId = lstTeamSelUserId.get(0).getFieldName1();
    }
    lstTeamSelUserId = null;

    // Get info whether Questions sent to Chaieperson or not
    List<SPTenderCommonData> getQueSenttoCPStatus =
    cmnService.returndata("getQueSenttoCPStatus", tenderId, strTeamSelUserId);
    if (!getQueSenttoCPStatus.isEmpty()) {
        if ("Yes".equalsIgnoreCase(getQueSenttoCPStatus.get(0).getFieldName1())) {
            isQueSentToCP = true;
        }

        if ("Yes".equalsIgnoreCase(getQueSenttoCPStatus.get(0).getFieldName2())) {
            isEvalNotificationGiven = true;
        }
    }
    getQueSenttoCPStatus = null;

    // Get Tender Procurement Nature info
    String strProcurementNature = "";
    List<SPTenderCommonData> lstProcurementNature =
    cmnService.returndata("getTenderProcurementNature", tenderId, null);
    if (!lstProcurementNature.isEmpty()) {
        strProcurementNature = lstProcurementNature.get(0).getFieldName1();
    }
    lstProcurementNature = null;

    //if (request.getParameter("comType") != null && !"null".equalsIgnoreCase(request.getParameter("comType"))) {
    //    strComType = request.getParameter("comType");
    //}
    List<SPTenderCommonData> tenderLotList = null;
    String repLabel = "Tender";
    String repLabelShort = "TER";
    String eventType = "";
    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
    Object afterLoginTSCTabprocNature1 = commonService.getProcNature(tenderId);
    String procMethod = commonService.getProcMethod(tenderId).toString();

    if(afterLoginTSCTabprocNature1.toString().equals("Services")){
        repLabel = "Proposal";
        repLabelShort = "PER";
        tenderLotList = cmnService.returndata("getlotorpackagebytenderid",tenderId,"Package,1");
        eventType = commonService.getEventType(tenderId).toString();
    }else{
        tenderLotList = cmnService.returndata("getProcurementNatureForEval", tenderId, null);
    }
    for (SPTenderCommonData tenderLot : tenderLotList) {
        lotId = tenderLot.getFieldName3();
    }
    String reportID = null;
    List<SPCommonSearchDataMore> getreportID = null;
    getreportID = dataMore.geteGPData("getCRReportId", tenderId,"0","0");
    if(getreportID!=null && (!getreportID.isEmpty()))
        {
            reportID = getreportID.get(0).getFieldName1();
        }
    List<SPCommonSearchDataMore> bidderRounds = new ArrayList<SPCommonSearchDataMore>();

    %>
    <!--Dashboard Header Start-->
    <div class="topHeader">
        <%@include file="../resources/common/AfterLoginTop.jsp" %>
    </div>
    <form id="frmReEval" name="frmReEval">
    <!--Dashboard Header End-->
    <!--Dashboard Content Part Start-->
    <div class="contentArea_1">
        <div class="pageHead_1">Previous Evaluation Report</div>
        <%pageContext.setAttribute("tenderId", tenderId);%>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>
        <div>&nbsp;</div>
        <%if (aa == false) {%>
        <jsp:include page="officerTabPanel.jsp" >
            <jsp:param name="tab" value="7" />
        </jsp:include>
        <% }%>
        <div class="tabPanelArea_1">
             <%
             if(aa == false){
                         pageContext.setAttribute("TSCtab", "5");
              %>
            <%@include file="../resources/common/AfterLoginTSC.jsp"%>
            <%}%>
            <div class="tabPanelArea_1">
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                <th width="4%" class="t-align-center" height="21">Sl. No.</th>
                <th width="21%" class="t-align-center" height="21">List of Previous Evaluation</th>
                <th class="t-align-center" width="75%" height="21"><strong>Reports</strong></th>
                </tr>
                <%
                int evalcnt = evalSrvice.getEvaluationNo(Integer.parseInt(tenderId));
                for(int i = 0;i<evalcnt;i++)
                    {
                    String evalNo = "";
                    if(i==0)
                        evalNo = String.valueOf(i+1)+"st";
                    else if(i==1)
                        evalNo = String.valueOf(i+1)+"nd";
                    else if(i==2)
                        evalNo = String.valueOf(i+1)+"rd";
                    else
                        evalNo = String.valueOf(i+1)+"th";
                    bidderRounds = dataMore.geteGPData("getRoundsforEvaluation",tenderId,reportID,"L1",String.valueOf(i));
                    boolean isTER3TER4 = false;
                    for(SPCommonSearchDataMore round : bidderRounds){
                        roundId = round.getFieldName1();
                        isTER3TER4 = true;
                    }
                    if (Math.IEEEremainder(i, 2) == 0) {
                   
                     %>
                    <tr class="bgColor-Green"> <%  } else { %>
                    <tr> <% } %>
                        <td class="t-align-center"> <%=i+1%></td>
                        <td class="t-align-center"> <%=evalNo%> Evaluation </td>
                        <td class="t-align-center">

                            <a href="<%=request.getContextPath()%>/officer/TER1.jsp?tenderid=<%=tenderId%>&lotId=<%=lotId%>&isview=y&stat=eval&in=y&sign=y&evalCount=<%=i%>" target="_blank"><%=repLabel%> Evaluation Report 1 (<%=repLabelShort%>1)</a> &nbsp;&nbsp;&nbsp;&nbsp;
                            <% if(!eventType.equals("REOI")){%>
                            <%  if("Services".equalsIgnoreCase(strProcurementNature)){
                                    if(!procMethod.equals("QCBS")){
                            %>
                                    <a href="<%=request.getContextPath()%>/officer/TER2.jsp?tenderid=<%=tenderId%>&lotId=<%=lotId%>&rId=<%=roundId%>&isview=y&stat=eval&in=y&sign=y&evalCount=<%=i%>" target="_blank"><%=repLabel%> Evaluation Report (<%=repLabelShort%>2)</a> &nbsp;&nbsp;&nbsp;&nbsp;
                            <% }}
                               else {
                            %>
                                    <a href="<%=request.getContextPath()%>/officer/TER2.jsp?tenderid=<%=tenderId%>&lotId=<%=lotId%>&isview=y&stat=eval&in=y&sign=y&evalCount=<%=i%>" target="_blank"><%=repLabel%> Evaluation Report (<%=repLabelShort%>2)</a> &nbsp;&nbsp;&nbsp;&nbsp;
                            <% } %>
                            <% if(isTER3TER4==true){ %>
                            <a href="<%=request.getContextPath()%>/officer/TER3.jsp?tenderid=<%=tenderId%>&lotId=<%=lotId%>&rId=<%=roundId%>&in=y&evalCount=<%=i%>" target="_blank"><%=repLabel%> Evaluation Report (<%=repLabelShort%>3)</a> &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="<%=request.getContextPath()%>/officer/TER4.jsp?tenderid=<%=tenderId%>&lotId=<%=lotId%>&rId=<%=roundId%>&in=y&evalCount=<%=i%>" target="_blank"><%=repLabel%> Evaluation Report (<%=repLabelShort%>4)</a> &nbsp;&nbsp;&nbsp;&nbsp;
                            <%}}%>

                        </td>
                    </tr>
                   <%  } %>
                 </table>
            </div>
        </div>
        <div>&nbsp;</div>
    <!--Dashboard Content Part End-->
    </div>
    </form>
    <!--Dashboard Footer Start-->
    <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
    <!--Dashboard Footer End-->
    </div>
    </body>
    <script type="text/javascript" language="Javascript">
    var headSel_Obj = document.getElementById("headTabEval");
    if(headSel_Obj != null){
    headSel_Obj.setAttribute("class", "selected");
    }

    </script>
    </html>
