<%-- 
    Document   : TenderExtReq
    Created on : Nov 30, 2010, 5:54:43 PM
    Author     : rajesh
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Date" %>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="java.util.Calendar"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender Validity Extension Request</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <link type="text/css" rel="stylesheet" href="../resources/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/lang/en.js"></script>

        <script type="text/javascript">

            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });


                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            //Onclick Event
            function Validate(){
                var temp='false';
                
                if(document.getElementById("nameanddesig").value == '0')
                {                    
                    $('#spanauthority').html("<br/>Please select user to send the request");
                    return false;
                }
                if (document.getElementById('txtNewProposalDate').value=='')
                {
                    $('#spannewproposal').html('<br/>Please select New Proposal Date.');
                    temp='true';
                }else
                {
                    if(CompareToForToday(document.getElementById('txtNewProposalDate').value)){
                        if(!CompareToForGreater(document.getElementById('txtNewProposalDate').value,document.getElementById('proposaldate').value))
                        {
                            $('#spannewproposal').html('<br/>New Validity Date should be greater than Old Validity Date.');
                            temp='true';
                        }
                        else
                        {
                            $('#spannewproposal').html('');
                        }
                       
                    }
                    else
                    {
                        $('#spannewproposal').html('<br/>Please select Proposal Date greater than current date.');
                        temp='true';
                    }
                }
                
               if (document.getElementById('txtNewSecurityDate') != null){
                if (document.getElementById('txtNewSecurityDate').value=='')
                {
                    $('#spannewsecurity').html('<br/>Please select New Security Date.');
                    temp='true';
                }else{

                    if(CompareToForToday(document.getElementById('txtNewSecurityDate').value)){
                        if(!CompareToForGreater(document.getElementById('oldsecuritydate').value,document.getElementById('txtNewSecurityDate').value))
                        {
                            $('#spannewsecurity').html('');
                        }
                        else
                        {

                            $('#spannewsecurity').html('<br/>New Security Date should be greater than Old Security Date.');
                            temp='true';
                        }
                       
                    }
                    else
                    {
                        $('#spannewsecurity').html('<br/>Please select Security Date greater than current date.');
                        temp='true';
                    }
                }
               }
                
                if ($.trim(document.getElementById('txtExtension').value)=='')
                {
                    $('#spanextension').html('<br/>Please enter Extension reason');
                    temp='true';
                }
                if(document.getElementById('txtNewSecurityDate') != null){
                    if(document.getElementById('txtNewSecurityDate').value!=''){
                        if(!NewSecurityDate()){
                            temp='true';
                        }

                    }
                }
              
                
                if(temp=='true')
                {
                    return false;
                }
                
            }

            function CompareToForToday(first)
            {
                var mdy = first.split('/')  //Date and month split
                var mdyhr= mdy[2].split(' ');  //Year and time split
                            
                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                            
                var d = new Date();
                            
                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                            
                return Date.parse(valuedate) > Date.parse(todaydate);
            }

            //Function for CompareToForGreater
            function CompareToForGreater(value,params)
            {
                if(value!='' && params!=''){
                   
                    var mdy = value.split('/')  //Date and month split
                    var mdyhr=mdy[2].split(' ');  //Year and time split
                    var mdyp = params.split('/')
                    var mdyphr=mdyp[2].split(' ');


                    if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                    {
                        //alert('Both Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                    }
                    else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                    {
                        //alert('Both DateTime');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], parseInt(mdy[1])-1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var mdyphrsec=mdyphr[1].split(':');

                        var datep =  new Date(mdyphr[0], parseInt(mdyp[1])-1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('one Date and One DateTime');
                        var a = mdyhr[1];  //time
                        var b = mdyphr[1]; // time

                        if(a == undefined && b != undefined)
                        {
                            //alert('First Date');
                            var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                            var mdyphrsec=mdyphr[1].split(':');
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                        }
                        else
                        {
                            //alert('Second Date');
                            var mdyhrsec=mdyhr[1].split(':');
                            var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                        }
                    }
                    return Date.parse(date) > Date.parse(datep);
                }
                else
                {
                    return false;
                }
            }

            function calcDays(date1,date2){
                var currentTime = new Date()
                var month = currentTime.getMonth() + 1
                var day = currentTime.getDate()
                var year = currentTime.getFullYear()

                // alert(date2.value);
                var dat1 = (date2.value).split("/");
                //            var myDate = new Date(newdateformat);
                //            var month = parseInt(myDate.getMonth())+1;
                var sDate = new Date(dat1[1]+"/"+dat1[0]+"/"+dat1[2]);
                var eDate = new Date(month + "/" + day + "/" + year);
                var daysApart = Math.abs(Math.round((sDate-eDate)/86400000));

                return daysApart;
            }
            //OnBlur Event

            /* function NewProposalDate(){
             var temp='';

             if (document.getElementById('txtNewProposalDate').value=='')
                {
                    $('#spannewproposal').html('<br/>Please select New Proposal Date.');
                    temp='true';
                }else
                {
                    if(CompareToForToday(document.getElementById('txtNewProposalDate').value)){
                        if(!CompareToForGreater(document.getElementById('txtNewProposalDate').value,document.getElementById('proposaldate').value))
                        {
                            $('#spannewproposal').html('<br/>New Validity Date should be greater than Old Validity Date.');
                            temp='true';
                        }
                        else
                        {
                            $('#spannewproposal').html('');
                        }
                    }
                    else
                    {
                        $('#spannewproposal').html('<br/>Please select Proposal Date greater than current date.');
                        temp='true';
                    }
                }

                 if(temp=='true')
            {
                return false;
            }

        }*/

            function NewProposalDate(){
           
                if (document.getElementById('txtNewProposalDate').value!='')
                {
                    $('#spannewproposal').html('');
                    var mydate = document.getElementById('txtNewProposalDate').value;

                    var currdate = document.getElementById('txtNewSecurityDate').value;
                    var temp=mydate.split('/');
                    var newtemp=temp[1]+'/'+temp[0]+'/'+temp[2];
                    var today =new Date(newtemp);
                    var in_a_week =today.setDate(today.getDate()+30);

                    var firstdate=new Date(in_a_week);
                    var tempc=currdate.split('/');
                    var newtempc=tempc[1]+'/'+tempc[0]+'/'+tempc[2];
                    var todayc =new Date(newtempc);



                    if(CompareToForToday(document.getElementById('txtNewProposalDate').value)){
                        if(!CompareToForGreater(document.getElementById('txtNewProposalDate').value,document.getElementById('proposaldate').value))
                        {
                            $('#spannewproposal').html('<br/>New Validity Date should be greater than Old Validity Date.');
                            temp='true';
                        }
                        else
                        {
                            $('#spannewproposal').html('');
                        }
                    }
                    else
                    {
                        $('#spannewproposal').html('<br/>Please select Proposal Date greater than current date.');
                        temp='true';
                    }

               
                    var days = calcDays(new Date(),document.getElementById('proposaldate'));
                    var pecount = document.getElementById("pecount").value;
                    if(days<10 && pecount==0){
                        document.getElementById("disp").style.display = '';
                        document.getElementById("dispSecond").style.display = '';
                    }
                }
                else
                {
                    $('#spannewproposal').html('<br/>Please select New Proposal Date.');
                }
            }

            /*
        function NewSecurityDate(){
            if (document.getElementById('txtNewSecurityDate').value!='')
            {
                $('#spannewsecurity').html('');
            }
        }
             */

            function NewSecurityDate(){
                var temp='';


                var mydate = document.getElementById('txtNewProposalDate').value;

                var currdate = document.getElementById('txtNewSecurityDate').value;
                var temp=mydate.split('/');
                var newtemp=temp[1]+'/'+temp[0]+'/'+temp[2];
                var today =new Date(newtemp);
                var in_a_week =today.setDate(today.getDate()+30);

                var firstdate=new Date(in_a_week);
                var tempc=currdate.split('/');
                var newtempc=tempc[1]+'/'+tempc[0]+'/'+tempc[2];
                var todayc =new Date(newtempc);

                if(Date.parse(firstdate) > Date.parse(todayc)){
                    $('#spannewsecurity').html('<br/>Please select New Security Date 30 days greater than Proposal Date.');
                    temp='true';
                }
                else{
                    $('#spannewsecurity').html('');
                    temp='false';
                }
            
                if(temp=='true')
                {
                    return false;
                }
                else{
                    return true;
                }
            }

            function addDays(myDate,days) {
                return new Date(myDate.getTime() + days*24*60*60*1000);
            }



            function parseDate(str) {
                var mdy = str.split('/')
                return new Date(mdy[2], parseInt(mdy[1])-1, mdy[0]);
            }

            function daydiff(first, second) {
                return (second-first)/(1000*60*60*24)
            }


            function Extension(){
                if ($.trim(document.getElementById('txtExtension').value)!='')
                {
                    $('#spanextension').html('');
                }
                else
                {
                    $('#spanextension').html('<br/>Please enter Extension reason');
                }
            }
            function getUserListOnchange(){
           
                $('#spanextension').html('');
                $.post("<%=request.getContextPath()%>/CommMemServlet", {funName:'getuser',tenderId:$('#tendid').val(),Roles:$('#ApprAuth').val()}, function(j){
                    var i = j.toString();
                    $("#nameanddesig").html(i);
                });

            }
        
        </script>
    </head>
    <body onload="getUserListOnchange()">
        <%

                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    if (request.getParameter("btnExtReq") != null) {

                        String dtXml = "";
                        java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");


                        String dt = request.getParameter("txtNewProposalDate");
                        String webDt = "";
                        String dt1 = request.getParameter("proposaldate");
                        String webDt2 = "";

                        DateUtils dd = new DateUtils();

                        Date d1 = new Date(); //dd.convertStringtoDate(dt, "dd/MM/yyyy");
                        Date d2 = dd.convertStringtoDate(dt1, "dd/MM/yyyy");
                        int daysApart = Math.abs(Math.round((d1.getTime() - d2.getTime()) / 86400000));
                        String[] dtArr = dt.split("/");
                        dt = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0];
                        // Added By Dohatec to resolve Online Tracking ID 9113 Start
                        java.text.SimpleDateFormat format1 = new java.text.SimpleDateFormat("yyyy-MM-dd");
                        Date NewPropDate = format1.parse(dt);
                        String lastValAcceptDtStr = format1.format(new Date());
                        Date lastValAcceptDt = new Date();
                            Calendar c = Calendar.getInstance();
                            c.setTime(new Date()); // Now use today date.
                            c.add(Calendar.DATE, 10); // Adding 10 days
                         lastValAcceptDt =  c.getTime();
                         if (lastValAcceptDt.after(NewPropDate)) {
                             lastValAcceptDtStr = format1.format(NewPropDate);
                         }
                         else {
                             lastValAcceptDtStr = format1.format(lastValAcceptDt);
                         }
                         // Added By Dohatec to resolve Online Tracking ID 9113 End
                        if(request.getParameter("txtNewSecurityDate") != null){
                            webDt = request.getParameter("txtNewSecurityDate");
                        }

                        if (webDt != "") {
                            String[] webDtArr = webDt.split("/");
                            webDt = webDtArr[2] + "-" + webDtArr[1] + "-" + webDtArr[0];
                        }

                        if (dt1 != "") {
                            String[] webDtArr1 = dt1.split("/");
                            dt1 = webDtArr1[2] + "-" + webDtArr1[1] + "-" + webDtArr1[0];
                        }
                        if(request.getParameter("securitydate") != null){
                            webDt2 = request.getParameter("securitydate");
                        }

                        if (webDt2 != "") {
                            String[] webDtArr2 = webDt2.split("-");
                            webDt2 = webDtArr2[2] + "-" + webDtArr2[1] + "-" + webDtArr2[0];
                        }

                        String userid = "";
                        HttpSession hs = request.getSession();
                        if (hs.getAttribute("userId") != null) {
                            userid = hs.getAttribute("userId").toString();
                        }
                        
                         // Coad added by Dipal for Audit Trail Log.
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        String idType="tenderId";
                        int auditId=Integer.parseInt(request.getParameter("tenderId"));
                        String auditAction="Tender validity and security extention";
                        String moduleName=EgpModule.Evaluation.getName();
                        String remarks=handleSpecialChar.handleSpecialChar(request.getParameter("txtExtension").toString());
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        
                        
                        
                        String authority = request.getParameter("ApprAuth");
                        String user = request.getParameter("nameanddesig");
                        String[] temp = user.split("-");
                        CommonSearchDataMoreService moreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        List<SPCommonSearchDataMore> list = moreService.geteGPData("getApprovedExtId", request.getParameter("tenderId"), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        String uid = "";
                        for (SPCommonSearchDataMore more : list) {
                            uid = more.getFieldName1().toString();
                        }
                        int peCnt = 0;
                        for (SPTenderCommonData sptcd : tenderCommonService1.returndata("GetEmpRolesList", userid, null)) {
                            if (sptcd.getFieldName1().equalsIgnoreCase("HOPE")) {
                                peCnt++;
                            }
                        }
                        String tSecLastDt = "";
                        String tSecNewDt = "";
                        if(!webDt2.equals("")){
                            tSecLastDt = "tenderSecLastDt=\"" + webDt2 + "\" ";
                        }
                        if(!webDt.equals("")){
                            tSecNewDt = "tenderSecNewDt=\"" + webDt + "\" ";
                        }
                        if (daysApart > 10) {
                            dtXml = "<root><tbl_TenderValidityExtDate tenderId=\"" + request.getParameter("tenderId") + "\" "
                                    + "lastPropValDt=\"" + dt1 + "\" "
                                    + "newPropValDt=\"" + dt + "\" "
                                    + tSecLastDt
                                    + tSecNewDt
                                    + "govUserId=\"" + temp[1] + "\" "
                                    + "extReason=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("txtExtension").toString()) + "\" "
                                    + "extStatus=\"Approved\"  extActionBy=\"" + userid
                                    + "\" extActionDt=\"" + format.format(new Date()) + "\" valExtReqId=\" "
                                    + "\" extReasonSign=\" "
                                    + "\" extReplySign=\"\" lastValAcceptDt=\"" + lastValAcceptDtStr + "\" notifyComments=\"\" extSentToRole=\"0\" extReplyDt=\"\" extSentBy=\"0\"/></root>";// Modified By Dohatec to resolve Online Tracking ID 9113


                        } else if (peCnt != 0) {
                            dtXml = "<root><tbl_TenderValidityExtDate tenderId=\"" + request.getParameter("tenderId") + "\" "
                                    + "lastPropValDt=\"" + dt1 + "\" "
                                    + "newPropValDt=\"" + dt + "\" "
                                    + tSecLastDt
                                    + tSecNewDt
                                    + "govUserId=\"" + temp[1] + "\" "
                                    + "extReason=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("txtExtension").toString()) + "\" "
                                    + "extStatus=\"Approved\"  extActionBy=\"" + userid
                                    + "\" extActionDt=\"" + format.format(new Date()) + "\" valExtReqId=\" "
                                    + 0 + "\" extReasonSign=\" "
                                    + "\" extReplySign=\"\" lastValAcceptDt=\"" + lastValAcceptDtStr + "\" notifyComments=\"\" extSentToRole=\"" + authority + "\" extReplyDt=\"\" extSentBy=\"" + temp[0] + "\"/></root>";// Modified By Dohatec to resolve Online Tracking ID 9113
                        } else if (peCnt == 0) { //Not PE
                            dtXml = "<root><tbl_TenderValidityExtDate tenderId=\"" + request.getParameter("tenderId") + "\" "
                                    + "lastPropValDt=\"" + dt1 + "\" "
                                    + "newPropValDt=\"" + dt + "\" "
                                    + tSecLastDt
                                    + tSecNewDt
                                    + "govUserId=\"" + temp[1] + "\" "
                                    + "extReason=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("txtExtension").toString()) + "\" "
                                    + "extStatus=\"Pending\"  extActionBy=\"" + userid
                                    + "\" extActionDt=\"" + format.format(new Date()) + "\" valExtReqId=\" "
                                    + 0 + "\" extReasonSign=\" "
                                    + "\" extReplySign=\"\" lastValAcceptDt=\"" + format.format(new Date()) + "\" notifyComments=\"\" extSentToRole=\"" + authority + "\" extReplyDt=\"\" extSentBy=\"" + temp[0] + "\"/></root>";
                        } else {
                            dtXml = "<root><tbl_TenderValidityExtDate tenderId=\"" + request.getParameter("tenderId") + "\" "
                                    + "lastPropValDt=\"" + dt1 + "\" "
                                    + "newPropValDt=\"" + dt + "\" "
                                    + tSecLastDt
                                    + tSecNewDt
                                    + "govUserId=\"" + temp[1] + "\" "
                                    + "extReason=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("txtExtension").toString()) + "\" "
                                    + "extStatus=\"Approved\"  extActionBy=\"" + userid
                                    + "\" extActionDt=\"" + format.format(new Date()) + "\" valExtReqId=\" "
                                    + "\" extReasonSign=\" "
                                    + "\" extReplySign=\"\" lastValAcceptDt=\"" + lastValAcceptDtStr + "\" notifyComments=\"\" extSentToRole=\"" + authority + "\" extReplyDt=\"\" extSentBy=\"" + temp[0] + "\"/></root>"; // Modified By Dohatec to resolve Online Tracking ID 9113

                        }
                        Object objUser = null;
                        if (session.getAttribute("userName") != null) {
                            objUser = session.getAttribute("userName");
                        }
                        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                        CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_TenderValidityExtDate", dtXml, "").get(0);
                        if (commonMsgChk.getFlag() == true) {
                            for (SPTenderCommonData sptcd1 : tenderCommonService1.returndata("GetAllBidderForExt", request.getParameter("tenderId"), userid)) {
                                dtXml = "<root><tbl_TenderValAcceptance tenderId=\"" + request.getParameter("tenderId") + "\" "
                                        + "valExtDtId=\"" + commonMsgChk.getId() + "\" "
                                        + "userId=\"" + sptcd1.getFieldName1() + "\" "
                                        + "tenderValAcceptDt=\"\" "
                                        + "comments=\"\" "
                                        + "status=\"Pending\"/></root>";
                                CommonMsgChk commonMsgChk2 = commonXMLSPService.insertDataBySP("insert", "tbl_TenderValAcceptance", dtXml, "").get(0);

                                if (daysApart > 10 || peCnt != 0) {
                                    String[] tomailid = {sptcd1.getFieldName2()};
                                    String mailText = "";
                                    String frommailid = XMLReader.getMessage("emailIdNoReply");
                                    SendMessageUtil sendMessageUtil = new SendMessageUtil();
                                    MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                                    MailContentUtility mailContentUtility = new MailContentUtility();
                                    String peoffice = tenderCommonService1.getPEOfficeName(request.getParameter("tenderId"));
                                    for (SPTenderCommonData sptcd : tenderCommonService1.returndata("tenderinfobar", request.getParameter("tenderId"), null)) {
                                        mailText = mailContentUtility.ValidationAppRequestToBiddertoEmail(Integer.parseInt(sptcd.getFieldName1()), sptcd.getFieldName2(), objUser.toString(), peoffice);

                                        sendMessageUtil.setEmailTo(tomailid);
                                        sendMessageUtil.setSendBy(frommailid);
                                        sendMessageUtil.setEmailSub("Request for Validity/Security Extension");
                                        sendMessageUtil.setEmailMessage(mailText);
                                        userRegisterService.contentAdmMsgBox(tomailid[0], frommailid, HandleSpecialChar.handleSpecialChar("Request for Validity/Security Extension."), mailContentUtility.ValidationAppRequestToBidder(Integer.parseInt(sptcd.getFieldName1()), sptcd.getFieldName2(), objUser.toString(), peoffice));
                                        sendMessageUtil.sendEmail();
                                    }
                                }
                            }


                            SendMessageUtil sendMessageUtil = new SendMessageUtil();

                            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                            MailContentUtility mailContentUtility = new MailContentUtility();
                            String mailText = "";
                            for (SPTenderCommonData sptcd : tenderCommonService1.returndata("tenderinfobar", request.getParameter("tenderId"), null)) {
                                mailText = mailContentUtility.ValidationRequestToHope(Integer.parseInt(sptcd.getFieldName1()), sptcd.getFieldName2(), sptcd.getFieldName5());
                            }
                            String tomailid[] = {tenderCommonService1.getEmailID(temp[0])};
                            String frommailid = XMLReader.getMessage("emailIdNoReply");
                            sendMessageUtil.setEmailTo(tomailid);
                            sendMessageUtil.setSendBy(frommailid);
                            sendMessageUtil.setEmailSub("Tender Validity/Security Extension Request.");
                            sendMessageUtil.setEmailMessage(mailText);
                            userRegisterService.contentAdmMsgBox(tomailid[0], frommailid, HandleSpecialChar.handleSpecialChar("Tender Validity/Security Extension Request."), mailText);
                            sendMessageUtil.sendEmail();
                            
                            
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                            response.sendRedirect("TECProcess.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=true");
                        } else {
                            auditAction = "Error in "+auditAction;
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                            response.sendRedirect("TenderExtReq.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=false");
                        }
                    }
        %>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <form action="" method="post">
                <!--Dashboard Header End-->

                <div class="pageHead_1">Validity Extension Request
                    <span class="c-alignment-right"><a href="TECProcess.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back to Dashboard</a></span>
                </div>
                <%
                            String msg = "";
                            msg = request.getParameter("msg");
                            String userid = "";
                            HttpSession hs = request.getSession();
                            if (hs.getAttribute("userId") != null) {
                                userid = hs.getAttribute("userId").toString();
                            }

                            int peCnt = 0;
                            for (SPTenderCommonData sptcd : tenderCommonService1.returndata("GetEmpRolesList", userid, null)) {
                                if (sptcd.getFieldName1().equals("HOPE")) {
                                    peCnt++;
                                }
                            }
                %>

                <% if (msg != null && msg.contains("false")) {%>
                <div class="responseMsg errorMsg t_space">Validity Extension Request Failed, Try again later.</div>
                <%  }%>
                <input type="hidden" id="pecount" name="pecount" value="<%=peCnt%>" />
                <div class="mainDiv">
                    <%
                                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));

                    %>
                    <input type="hidden" name="tendid" id="tendid" value="<%=request.getParameter("tenderId")%>" />
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                </div>
                <div>&nbsp;</div>
                <hr />
                <div class="t-align-left t_space">
                    <span style="font-style: italic" class="t-align-left" style="float:left; font-weight:normal;">
			Fields marked with (<span class="mandatory">*</span>) are mandatory
                    </span>
                </div>
                <%
                            List<SPTenderCommonData> list = tenderCommonService.returndata("ValidityExtReq", request.getParameter("tenderId"), null);
                            if (!list.isEmpty()) {
                %>


                <table width="100%" cellspacing="0" class="tableList_1 t_space">

                    <tr>
                        <td width="21%" class="t-align-left ff">Tender/Proposal Validity in no. of Days : </td>
                        <td width="79%" class="t-align-left"><%=list.get(0).getFieldName1()%></td>
                    </tr>

                    <tr>
                        <td class="t-align-left ff">Last Date  of Tender/Proposal Validity : </td>
                        <td class="t-align-left"><%=list.get(0).getFieldName2()%>
                            <INPUT id="proposaldate" type="hidden" name="proposaldate" value="<%=list.get(0).getFieldName4()%>"/></td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">New Date of Tender/Proposal Validity : <span class="mandatory">*</span></td>
                        <td class="t-align-left"><input name="txtNewProposalDate" type="text" class="formTxtBox_1" id="txtNewProposalDate" style="width:70px;" readonly="true" onclick="GetCal('txtNewProposalDate','txtNewProposalDate');" onblur="NewProposalDate();"/>
                            <a href="javascript:void(0);"  title="Calender"><img id="txtNewProposalDateImg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('txtNewProposalDate','txtNewProposalDateImg');"/></a>
                            <span id="spannewproposal" class="mandatory"></span>
                        </td>
                    </tr>
                    <% TenderSrBean tenderSrBeanForValSec = new TenderSrBean();
                    List<Object[]> objForChkingValSec = tenderSrBeanForValSec.getConfiForTender(Integer.parseInt(request.getParameter("tenderId")));
                    if(!objForChkingValSec.isEmpty() && objForChkingValSec.get(0)[2].toString().equalsIgnoreCase("yes")){%>
                    <tr>
                        <td class="t-align-left ff">Last Date of Tender/Proposal Security Validity : </td>
                        <td class="t-align-left"><%=list.get(0).getFieldName3()%>
                            <INPUT id="oldsecuritydate" type="hidden" name="oldsecuritydate" value="<%=list.get(0).getFieldName5()%>"/>
                            <INPUT id="securitydate" type="hidden" name="securitydate" value="<%=list.get(0).getFieldName3()%>"/></td>
                    </tr>
                    <tr >
                        <td class="t-align-left ff">New Date of Tender/Proposal Security Validity : <span id="spanmandatory" name="spanmandatory" class="mandatory">*</span></td>
                        <td class="t-align-left"><input name="txtNewSecurityDate" type="text" class="formTxtBox_1" id="txtNewSecurityDate" style="width:70px;" readonly="true" onclick="GetCal('txtNewSecurityDate','txtNewSecurityDate');" onblur="NewSecurityDate();"/>
                            <a href="javascript:void(0);"  title="Calender"><img id="txtNewSecurityDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('txtNewSecurityDate','txtNewSecurityDateimg');"/></a>
                            <span id="spannewsecurity" class="mandatory"></span>
                        </td>
                    </tr>
                    <% } %>
                    <tr>
                        <td class="t-align-left ff">Extension Reason : <span class="mandatory">*</span> </td>
                        <td class="t-align-left">
                            <textarea cols="100" rows="5" id="txtExtension" name="txtExtension" class="formTxtBox_1" onblur="Extension();"></textarea>
                            <span id="spanextension" class="mandatory"></span>
                        </td>
                    </tr>
                    <%
                                                    boolean check = tenderCommonService.checkForFirstextends(Integer.parseInt(request.getParameter("tenderId")));
                    %>
                    <tr id="disp" style="display: none">
                        <td  class="t-align-left ff">Send To : <span class="mandatory">*</span> </td>
                        <td class="t-align-left">

                            <select name="ApprAuth" id="ApprAuth" class="formTxtBox_1" onchange="getUserListOnchange()">
                                <%if (!check) {%>
                                <option value="HOPE">HOPA</option>
                                <%} else {
                                    List<Object> role2 = tenderCommonService.getProcurementRoles();
                                    int i = 0;
                                    for (Object obj : role2) {
                                        if ("Secretary".equalsIgnoreCase(obj.toString()) || "CCGP".equalsIgnoreCase(obj.toString()) || "BOD".equalsIgnoreCase(obj.toString()) || "Minister".equalsIgnoreCase(obj.toString()) || "HOPE".equalsIgnoreCase(obj.toString())) {
                                %>
                                <option value="<%=obj.toString()%>" <%if ("HOPE".equalsIgnoreCase(obj.toString())) {%>selected<%}%> ><%=obj.toString()%></option>
                                <%}
                                        i++;
                                    }
                                %>

                                <%}%>
                            </select><span id="spanauthority" class="mandatory"></span>
                        </td>
                    </tr>
                    <tr id="dispSecond" style="display: none">
                        <td class="t-align-left ff">Name and Designation : <span class="mandatory">*</span> </td>
                        <td class="t-align-left">
                            <span id="spanextension" class="mandatory"></span>
                            <select name="nameanddesig" id="nameanddesig" class="formTxtBox_1" >
                                <option value="-1">default</option>
                            </select>
                        </td>
                    </tr>
                    <%

                                                    if (peCnt == 0) {
                    %>
                    <input type="hidden" name="hdnvalue" id="hdnvalue" value="HOPE"/>
                    <%} else {%>
                    <input type="hidden" name="hdnvalue" id="hdnvalue" value="NOHOPE"/>
                    <%}
                                }%>

                </table>
                <div class="t-align-center t_space">
                    <label class="formBtn_1">
                        <input name="btnExtReq" type="submit" value="Submit" onclick="return Validate();"/>
                    </label>
                </div>

                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End-->
            </form>
        </div>

    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
