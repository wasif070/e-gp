<%-- 
    Document   : ViewDomesticPreference
    Created on : Jun 16, 2011, 11:55:19 AM
    Author     : rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <title>View Domestic Preference</title>
    </head>
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    <body>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->

            <div class="contentArea_1">
                <!--Dashboard Header End-->
                <%
                            String userId = "";
                            HttpSession hs = request.getSession();
                            if (hs.getAttribute("userId") != null) {
                                userId = hs.getAttribute("userId").toString();
                            }
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                            String tenderId = "";
                            if (request.getParameter("tenderId") != null && !"".equals(request.getParameter("tenderId"))) {
                                tenderId = request.getParameter("tenderId");
                            }
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <table border="0" cellspacing="10" cellpadding="0" class="tableList_1 t_space" width="100%">
                    <tr>
                        <th>S.No</th>
                        <th>Tenderers/Consultants Name</th>
                    </tr>
                    <%
                                List<SPTenderCommonData> listDomesticPre =
                                        tenderCommonService.returndata("getDomesticTenderersList", tenderId, null);
                                int srno = 1;
                                for (SPTenderCommonData listing : listDomesticPre) {
                    %>
                    <tr>
                        <td class="t-align-center"><%=srno%></td>
                        <td><%=listing.getFieldName2()%></td>
                    </tr>
                    <% srno++;
                                    }%>
                </table>
            </div>
        </div>
    </body>
</html>
