<%--
    Document   : HopeDebarListing
    Created on : Jan 9, 2011, 3:29:04 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Debarment Request</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript">
                function fillGridOnEvent(type,email,cmpName,offId,isSearch){
                    var queryString = '<%=request.getContextPath()%>/InitDebarment?p=2&q=1&action=fetchData&status='+type;
                    if(isSearch){
                        queryString = queryString + '&mailId='+email+'&cmpName='+cmpName+'&offId='+offId;
                    }
                    $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                    jQuery("#list").jqGrid({
                        url:queryString,
                        datatype: "xml",
                        height: 250,
                        colNames:['Sl. No.','Company Name','Procuring Agency','Date and Time of Request',"Status","Action","Review Committee"],
                        colModel:[
                            {name:'srNo',index:'srNo', width:50,sortable:false,align:'center'},
                            {name:'procureEntity',index:'procureEntity', width:300,sortable:false},
                            {name:'dateRequest',index:'tdr.clarificationReqDt', width:120,align:'center'},
                            {name:'companyName',index:'(CASE WHEN tcm.companyId = 1 THEN concat(tm.firstName,\' \',tm.lastName)  ELSE tcm.companyName END)', width:200},
                            {name:'status',index:'status', width:170,sortable:false,align:'center'},
                            {name:'userid',index:'userid', width:50,sortable:false,align:'center'},
                            {name:'debarId',index:'debarId', width:110,sortable:false,align:'center'}
                        ],
                        multiselect: false,
                        paging: true,
                        rowNum:10,
                        rowList:[10,20,30],
                        pager: $("#page"),
                        sortable:false,
                        caption: " ",
                        gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                    }).navGrid('#page',{edit:false,add:false,del:false,search:false});
                }
                jQuery().ready(function (){
                    //fillGrid();
                });
        
                function test(id){
                    var isSearch = false;
                    var isValid = true;
                    if(id==0){
                        id = $("#tabvalue").val();
                        isSearch = true;
                    }else{
                        $('#emailId').val('');
                        $('#companyName').val('');                        
                        $('#cmboffice').val('0');
                    }
                    var email = $('#emailId').val();
                    var cmpName = $('#companyName').val();
                    var offId = $('#cmboffice').val();                    
                    if(isSearch){
                         $(".err").remove();
                             var cnt = 0;
                             if($.trim($("#emailId").val()) == "") {
                               /* $("#emailId").parent().append("<div class='err' style='color:red;'>Please enter e-mail ID</div>");
                                isValid = false;*/
                             }else{
                                 cnt++;
                             }
                             if($.trim($("#emailId").val()) != "") {
                                var mailTest = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;
                                if(!mailTest.test($("#emailId").val())) {
                                    $("#emailId").parent().append("<div class='err' style='color:red;'>Please enter valid e-mail ID</div>");
                                    isValid = false;
                                }
                             }

                             if($.trim($("#companyName").val()) == "") {
                                /*$("#companyName").parent().append("<div class='err' style='color:red;'>Please enter Company Name</div>");
                                isValid = false;*/
                             }else{
                                 cnt++;
                             }
                             
                             if($.trim($("#cmboffice").val()) == "0") {
                               /* $("#cmboffice").parent().append("<div class='err' style='color:red;'>Please select Office</div>");
                                isValid = false;*/
                             }else{
                                 cnt++;
                             }

                             if(cnt==0){
                                 isValid=false;
                                 jAlert("Please enter atleast one search Criteria.","Search Alert", function(RetVal) {
                                 });
                             }
                    }
                    if(isValid){
                        if(id==1){
                            document.getElementById("linkPending").className = "sMenu";
                            document.getElementById("linkProccessed").className = "";
                            $("#tabvalue").val('1');
                            fillGridOnEvent('in (\'sendtohope\') and tdr.hopeId=<%=session.getAttribute("userId")%>',email,cmpName,offId,isSearch);
                        }else if(id==2){
                            document.getElementById("linkPending").className = "";
                            document.getElementById("linkProccessed").className = "sMenu";
                            $("#tabvalue").val('2');
                            fillGridOnEvent('not in (\'sendtohope\',\'pending\',\'withpe\',\'pesatisfy\') and tdr.hopeId=<%=session.getAttribute("userId")%>',email,cmpName,offId,isSearch);
                        }
                    }
                }
      </script>
    </head>
    <body onload="test(<%if("y".equals(request.getParameter("isprocess"))){out.print("2");}else{out.print("1");}%>);">
        <div class="dashboard_div">
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <div class="pageHead_1">View Debarment Request
            <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('5');">Save as PDF</a></span>
            </div><br/>
            <%if("succ".equals(request.getParameter("pub"))){out.print("<div class=\"responseMsg successMsg\">Review Committee published successfully</div>");}%>
            <div class="formBg_1 t_space">
                <table width="100%" cellspacing="8" class="formStyle_1">
                    <tr>
                        <td class="ff t-align-left" width="20%">Email Id :</td>
                        <td class="ff t-align-left" width="80%">
                            <input type="text" name="emailId" id="emailId" style="width: 200px;" class="formTxtBox_1"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff t-align-left" width="20%">Company Name :</td>
                        <td class="ff t-align-left" width="80%">
                            <input type="text" name="companyName" id="companyName" style="width: 200px;" class="formTxtBox_1"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff t-align-left" width="20%">PA Office :</td>
                        <td width="80%" class="t-align-left">
                            <select id="cmboffice" style="width: 200px;" class="formTxtBox_1" name="offId">
                                <option value="0">--Please Select Office--</option>
                                <%
                                    CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                                    List<SPCommonSearchDataMore> offList = dataMoreService.geteGPDataMore("getHopeUserOffice",session.getAttribute("userId").toString());
                                    for(SPCommonSearchDataMore offdata : offList){
                                        out.print("<option value='"+offdata.getFieldName1()+"'>"+offdata.getFieldName2()+"</option>");
                                    }
                                %>
                                
                            </select>                            
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left" width="20%"></td>
                        <td class="t-align-left" width="80%">
                            <label class="formBtn_1">
                                <input type="button" value="Search" id="search" onclick="test(0);"/>
                            </label>
                            <label class="formBtn_1">
                                <input type="reset" value="Reset" onclick="window.location.reload();"/>
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
            <input type="hidden" value="1" id="tabvalue"/>
            <ul class="tabPanel_1 t_space">
                <li><a href="javascript:void(0);" id="linkPending" onclick="test(1);">Pending</a></li>
                <li><a href="javascript:void(0);" id="linkProccessed" onclick="test(2);">Processed</a></li>
            </ul>            
            <div class="tabPanelArea_1">
                    <div id="jQGrid" align="center">
                        <table id="list"></table>
                        <div id="page"></div>
                    </div>
                </div>
            <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                            String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="Debarment_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="Debarment" />
            </form>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabDebar");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
