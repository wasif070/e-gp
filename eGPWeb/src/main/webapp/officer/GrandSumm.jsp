<%-- 
    Document   : GrandSumm
    Created on : Dec 18, 2010, 6:20:42 PM
    Author     : Rikin
--%>
<%@page import="java.math.BigDecimal"%>
<%@page import="sun.awt.image.PixelConverter.Bgrx"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Grand Summary Report</title>
        <%String contextPath = request.getContextPath();%>
        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />        
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>       
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });
            function printElem(options){
                $('#print_area').printElement(options);
            }
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%
                        String isPDF = "abc";
                        if (request.getParameter("isPDF") != null) {
                            isPDF = request.getParameter("isPDF");
                        }
                        pageContext.setAttribute("isPDF", isPDF);
                        
                        String userId = "";
                        if("true".equalsIgnoreCase(isPDF)){
                            if (request.getParameter("userId") != null && !"".equalsIgnoreCase(request.getParameter("userId").toString())) {
                                userId = request.getParameter("userId").toString();
                            }
                        }else{
                            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                userId = session.getAttribute("userId").toString();
                            }
                        }
                        pageContext.setAttribute("userId", userId);
                        //if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                            //userId = Integer.parseInt(session.getAttribute("userId").toString());
                        //    userId = session.getAttribute("userId").toString();
                        //}
                        
                        String tenderId = "";
                        if (request.getParameter("tenderId") != null) {
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                            tenderId = request.getParameter("tenderId");
                        }
                        
                        String porlId = "";
                        if (request.getParameter("lotId") != null) {
                            porlId = request.getParameter("lotId").toString();
                        }
                        
                        String reportType = "";
                        if(request.getParameter("report")!=null){
                            reportType = request.getParameter("report").toString();
                        }
                        
                        String folderName = pdfConstant.GRANDSUMMARY;
                        String genId = tenderId+"_"+porlId+"/"+reportType;
                        
                        if (!(isPDF.equalsIgnoreCase("true"))) {
                            try{
                                 String reqURL = request.getRequestURL().toString();
                                 String reqQuery = "tenderId="+tenderId+"&lotId="+porlId+"&userId="+userId+"&report="+reportType;
                                 pdfCmd.genrateCmd(reqURL, reqQuery, folderName, genId);
                            }catch(Exception e){
                                 e.printStackTrace();
                            }
                        }
                        
                        int srNo = 0;
                        float total = 0;    

                        // Coad added by Dipal for Audit Trail Log.
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        String idType="tenderId";
                        int auditId=Integer.parseInt(tenderId);
                        String auditAction="View Grand Summary";
                        String moduleName=EgpModule.Tender_Document.getName();
                        String remarks="Officer (User Id): "+session.getAttribute("userId") +" has viewed Grand Summary for Tender id: "+tenderId;
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                        
                %>
                <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <% } %>
                <div class="contentArea_1">
                    <div class="pageHead_1">
                        <% if("Individual".equalsIgnoreCase(reportType)){ %>
                        Individual Report
                        <% } %>
                        <% if("Comparative".equalsIgnoreCase(reportType)){ %>
                        Comparative Report
                        <% } %>
                        
                        <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                        <% if(request.getHeader("referer")!=null){
                            String referrer = request.getHeader("referer").toString();
                            if(referrer.contains("OpenComm.jsp")){
                        %>
                            <span style="float:right;">
                                <a class="action-button-goback" href="OpenComm.jsp?tenderid=<%=tenderId%>">Go back to Dashboard</a>
                            </span>
                        <%  }else if(referrer.contains("OpeningReports.jsp")){ %>
                            <span style="float:right;">
                                <a class="action-button-goback" href="<%=request.getHeader("referer")%>">Go back to Dashboard</a>
                            </span>
                        <%  }else{ %>
                            <span style="float:right;">
                                <a class="action-button-goback" href="LotPackageDetail.jsp?tenderid=<%=tenderId%>&lotId=<%=porlId%>">Go back to Dashboard</a>
                            </span>
                        <% } %>
                        
                        <% } } %>
                    </div>
                    <div  id="print_area">
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                    <div class="t_space">&nbsp;</div>
                    <span style="float:right;" class="noprint">
                            <a class="action-button-savepdf" href="<%=request.getContextPath()%>/GeneratePdf?reqURL=&reqQuery=&folderName=<%=folderName%>&id=<%=genId%>" >Save As PDF </a>&nbsp;
                            <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
                            
                    </span>
                    <% } %>
                    <div class="t_space noprint">&nbsp;</div>
                    <div class="tabPanelArea_1">
                        <%
                            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                            List<SPCommonSearchData> userDetails = commonSearchService.searchData("getUserForFinalSubmission", tenderId, null, null, null, null, null, null, null, null);
                        %>
                        <% if("Individual".equalsIgnoreCase(reportType)){
                             for (SPCommonSearchData user : userDetails){
                                 List<SPCommonSearchData> grandSummeryDetails = commonSearchService.searchData("getGrandSumOpening", tenderId, user.getFieldName2(), porlId, null, null, null, null, null, null);
                                  BigDecimal totalBig= new BigDecimal(Double.parseDouble("0"));
                                %>
                                 <table width="100%" cellspacing="0" border="0" class="tableList_1 t_space">
                                     <tr>
                                         <th class="ff t-align-left" style="text-align: left;">Bidder/Consultant Name : &nbsp;<%=user.getFieldName1()%></th>
                                     </tr>
                                 </table>
                                 <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <th width="4%" class="t-align-center ff">Sl. No.</th>
                                        <th width="74%" class="t-align-center">Form Name</th>
                                        <th width="22%" class="t-align-center ff">Amount (in Nu.)</th>
                                    </tr>
                                 <%
                                 srNo = 1;
                                 for(SPCommonSearchData grandsummery : grandSummeryDetails){

                                     if("BOQsalvage".equalsIgnoreCase(grandsummery.getFieldName4())){  //negative form -salvage forms mube debited
                                         totalBig=totalBig.subtract(new BigDecimal(grandsummery.getFieldName2()).setScale(3,0));
                                         }
                                     else
                                         {
                                    totalBig=totalBig.add(new BigDecimal(grandsummery.getFieldName2()).setScale(3,0));
                                    }
                                 %>
                                    <tr>
                                        <td width="7%" class="t-align-center ff"><%=srNo%></td>
                                        <td width="71%" class="t-align-left"><%=grandsummery.getFieldName1()%> </td>
                                        <td width="22%" style="text-align: right;" class="ff"><%=new BigDecimal(grandsummery.getFieldName2()).setScale(3,0)%></td>
                                    </tr>
                            <%      srNo++;
                                 }
                                 %>
                                 <tr>
                                    <td width="7%" class="t-align-center ff">&nbsp;</td>
                                    <td width="71%" class="ff t-align-left">Grand Total</td>
                                    <td width="22%" style="text-align: right;" class="ff"><%=totalBig.setScale(3,0)%></td>
                                </tr>
                                 <%
                              }
                           }
                        %>
                        <% if("Comparative".equalsIgnoreCase(reportType)){ %>
                        <%
                            
                            BigDecimal totalArr[]=new BigDecimal[userDetails.size()];
                            for(int k=0;k<userDetails.size();k++)
                                {
                                totalArr[k]=new BigDecimal("0");
                                }
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th class="t-align-center ff">Form Name</th>
                                <% 
                                    for (SPCommonSearchData user : userDetails){
                                %>
                                <th class="t-align-center ff"><%=user.getFieldName1()%> <br />Amount in (Nu.)</th>
                                <% } %>
                            </tr>
                                <%
                                    List<SPCommonSearchData> formDetails = commonSearchService.searchData("getFormIdforCompReport", tenderId, null, porlId, null, null, null, null, null, null);
                                    int userindex = 0;
                                    for (SPCommonSearchData form : formDetails){
                                        userindex = 0;
                            %>
                                <tr>
                                    <td class="ff t-align-left"><%=form.getFieldName2()%></td>
                                    <%  for (SPCommonSearchData user : userDetails){
                                            List<SPCommonSearchData> grandSummeryDetails = commonSearchService.searchData("getComparativeGrandSumOpening", tenderId, user.getFieldName2(), porlId, form.getFieldName1(), null, null, null, null, null);
                                                if(!grandSummeryDetails.isEmpty()){
                                                     if("BOQsalvage".equalsIgnoreCase(grandSummeryDetails.get(0).getFieldName4())){  //negative form -salvage forms mube debited
                                                     totalArr[userindex]=totalArr[userindex].subtract(new BigDecimal(grandSummeryDetails.get(0).getFieldName2()).setScale(3,0));
                                                        }
                                                       else
                                                       {
                                                     totalArr[userindex]=totalArr[userindex].add(new BigDecimal(grandSummeryDetails.get(0).getFieldName2()).setScale(3,0));
                                                      }
                                                   
                                                }
                                                userindex++;
                                    %>
                                    
                                    <td style="text-align: right;"><% if(!grandSummeryDetails.isEmpty()){ out.print(new BigDecimal(grandSummeryDetails.get(0).getFieldName2()).setScale(3,0)); }else{ out.print("0"); } %></td>
                                    <% } %>
                                </tr>
                            <%     }  %>
                            <tr>
                                <td class="ff t-align-left">Grand Total </td>
                                <% for(BigDecimal userindtotal : totalArr){ %>
                                <td style="text-align: right;"><%=userindtotal.setScale(3,0)%></td>
                                <% } %>
                            </tr>
                        <% } %>
                        </table>
                        </div>
                    </div>         
                </div>
                <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <% } %>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
