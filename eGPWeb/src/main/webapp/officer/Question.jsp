<%-- 
    Document   : Question
    Created on : Jan 12, 2011, 10:50:26 AM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.model.table.TblAskProcurement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Question</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
    </head>
    <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
    %>
    <script type="text/javascript">
        function pending(){
            $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
            jQuery().ready(function (){
                jQuery("#list").jqGrid({
                    url:'<%=request.getContextPath()%>/AskProcurementServlet?q=1&action=fetchData',
                    datatype: "xml",
                    height: 250,
                    colNames:['S.No','Category','Query','Posted By','Posted On',"Action"],
                    colModel:[
                        {name:'srno',index:'srno', width:3,sortable:false,search:false,align:'center'},
                        {name:'category',index:'category', width:17,sortable:true,align:'center', searchoptions: { sopt: ['eq','cn']}},
                        {name:'question',index:'question', width:30,sortable:true, searchoptions: { sopt: ['eq','cn']}},
                        
                        {name:'postedBy',index:'postedBy', width:20,sortable:true,search:false},
                        {name:'postedOn',index:'postedDate', width:10,sortable:true,search:false},
                        {name:'Action',index:'Action', width:10,sortable:false,align:'center',search:false}
                    ],

                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    caption: "Question",
                    gridComplete: function(){
                        $("#list tr:nth-child(even)").css("background-color", "#fff");
                    }
                }).navGrid('#page',{edit:false,add:false,del:false});
            });
        }
    </script>
    <script type="text/javascript">
        function replied(){
            $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
            jQuery().ready(function (){
                jQuery("#list").jqGrid({
                    url:'<%=request.getContextPath()%>/AskProcurementServlet?q=1&action=fetchData&replied=yes',
                    datatype: "xml",
                    height: 250,
                    colNames:['S.No','Category','Query','Posted By','Posted On','Replied On',"Action"],
                    colModel:[
                        {name:'srno',index:'srno', width:3,sortable:false,search:false,align:'center'},
                        {name:'category',index:'category', width:14,sortable:false,align:'center', searchoptions: { sopt: ['eq','cn']}},
                        {name:'question',index:'question', width:30,sortable:false, searchoptions: { sopt: ['eq','cn']}},                        
                        {name:'postedBy',index:'postedBy', width:20,sortable:false,search:false},
                        {name:'postedOn',index:'postedOn', width:10,sortable:false,search:false},
                        {name:'repliedOn',index:'repliedOn', width:10,sortable:false,search:false},
                        {name:'Action',index:'Action', width:6,sortable:false,align:'center',search:false}
                    ],

                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    caption: "Question",
                    gridComplete: function(){
                        $("#list tr:nth-child(even)").css("background-color", "#fff");
                    }
                }).navGrid('#page',{edit:false,add:false,del:false});
            });
        }
    </script>
    <body onload="pending();">
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <div class="pageHead_1">View Queries</div>
                            <%if(request.getParameter("action") != null && request.getParameter("action").equals("reply")){%>
                            <br/><div align="left" id="sucMsg" class="responseMsg successMsg">Reply given successfully</div>
                            <% } %>
                            <%if(request.getParameter("action") != null && request.getParameter("action").equals("delsucc")){%>
                            <br/><div align="left" id="sucMsg" class="responseMsg successMsg">Question deleted successfully</div>
                            <% } %>
                            <%if(request.getParameter("action") != null && request.getParameter("action").equals("delfail")){%>
                            <br/><div align="left" id="sucMsg" class="responseMsg errorMsg">Problem in deleting Question</div>
                            <% } %>
                            <ul class="tabPanel_1 t_space">
                                <li><a href="javascript:void(0);" id="pendingTab" onclick="changeTab(1);" class="sMenu">Pending</a></li>
                                <li><a href="javascript:void(0);" id="repliedTab" onclick="changeTab(2);">Replied</a></li>
                            </ul>
                            <form id="frmProcurementQuestion" name="frmProcurementQuestion" method="post" action="">
                                <div class="tabPanelArea_1">
                                    <div id="jqGrid">
                                        <table id="list">
                                        </table>
                                    </div>
                                </div>
                                <div id="page" >
                                </div>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>

    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabQuestion");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <script type="text/javascript">
        function changeTab(tabNo){
            if(tabNo == 1){
                $("#repliedTab").removeClass("sMenu");
                $("#pendingTab").addClass("sMenu");
                pending();
            }
            else if(tabNo == 2){
                $("#pendingTab").removeClass("sMenu");
                $("#repliedTab").addClass("sMenu");
                replied();
            }
        }
    </script>
</html>
