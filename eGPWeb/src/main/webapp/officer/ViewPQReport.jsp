<%--
    Document   : ViewPQReport
    Created on : Jun 24, 2011, 4:30:27 PM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblPostQualification"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TblPostQualificationService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View  Post Qualification Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    </head>
    <body>
        <%  String tenderId;
                    tenderId = request.getParameter("tenderId");
                    String pkgLotId = request.getParameter("pkgLotId");
                     int evalCount = 0;
                     if(request.getParameter("evalCount")!=null){
                        evalCount = Integer.parseInt(request.getParameter("evalCount"));
                     }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Initiate Post Qualification
                    <%if(!"y".equals(request.getParameter("ter"))){%>
                    <span class="c-alignment-right"><a href="InitiatePQ.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Lot Selection</a></span>
                    <%}%>
                </div>
                <%
                            pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <div>&nbsp;</div>
                <div class="tabPanelArea_1">

                    <table width="100%" cellspacing="0" class="tableList_1">
                        <%          
                                    CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                                    List<Object[]> list = cs.getLotDetailsByTenderIdAndPkgIdforEval(tenderId, pkgLotId);
                                    List<Object[]> listcompany = cs.getCompanyNameAndEntryDate(tenderId,pkgLotId,evalCount);
                                    //TblPostQualificationService service = (TblPostQualificationService)AppContext.getSpringBean("PostQualificationService");
                                    //List<TblPostQualification> list1 = service.getAll(pkgLotId);


                        %>
                        <tr>
                            <td  class="ff" widtd="10%" valign="middle" class="t-align-center">Lot No.</td>
                            <td class="t-align-left"><%=list.get(0)[0]%></td>
                        </tr><tr>
                            <td class="ff" width="20%" valign="middle" class="t-align-center">Lot Description</td>
                            <td class="t-align-left"><%=list.get(0)[1]%></td>
                        </tr>
                    </table>
                    <br />
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th width="10%" valign="middle" class="t-align-center">Rank</th>
                            <th width="37%" valign="middle" class="t-align-center">Bidder/Consultant</th>
                            <th width="15%" valign="middle" class="t-align-center">PQ Status</th>
                            <th width="15%" valign="middle" class="t-align-center">Date and Time of PQ</th>
                            <th width="30%" valign="middle" class="t-align-center">Action</th>
                        </tr>
                        <%
                        String userId = request.getParameter("userId");
                        if(userId==null)
                            userId="";

                                   if(listcompany!=null && !listcompany.isEmpty()){                                       
                                       for(Object[] obj:listcompany){
                                           if("Qualify".equalsIgnoreCase(obj[4].toString())){

                        %>

                        <tr>
                            <td class="t-align-center"><%=obj[5].toString()%></td>
                            <td class="t-align-center"><%=obj[0].toString()%></td>
                            <td class="t-align-center"> <%out.print("initiated".equalsIgnoreCase(obj[4].toString()) ? "Pending" : "Qualify".equalsIgnoreCase(obj[4].toString()) ? "Qualified" : "Disqualified");%></td>
                            <td class="t-align-center"><%
                            String str[] = obj[1].toString().split("-");
                            if("1900".equalsIgnoreCase(str[0])){
                                out.print("-");
                                }
                            else{
                            out.print(DateUtils.gridDateToStr((Date) obj[1]));
                                    }%></td>
                            <td class="t-align-center">

                                <a href="ViewPostQualificationDtl.jsp?pkgLotId=<%=pkgLotId%>&uId=<%=obj[3].toString()%>&tenderId=<%=tenderId%>&postQualId=<%=obj[2].toString()%>">View</a>
                                <%
                                        }
                                  }
               }else{%>
                        <td class="t-align-center" colspan="5">No records found</td>
                        <%}

           %>
                    </table>

                    <div>&nbsp;</div>
                </div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
