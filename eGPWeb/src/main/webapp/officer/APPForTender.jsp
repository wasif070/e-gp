<%-- 
    Document   : APPForTender
    Created on : Dec 25, 2010, 10:58:26 AM
    Author     : rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPReturn"%>
<%@page import="java.util.List"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create Tender: Select APP for tender creation</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <jsp:useBean id="appServlet" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
    <jsp:useBean id="appSrBean" class="com.cptu.egp.eps.web.servicebean.APPSrBean"/>
    <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body >
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <%--<form id="frmMyApp" name="frmMyApp" method="post" action="">--%>
                <div class="contentArea_1">
                    <div class="pageHead_1">Select APP &amp; Packages</div>
                    <form id="frmAPPForTender" <%--action="<%=request.getContextPath()%>/APPFileUploadServlet"--%> name="frmAPPForTender" method="post">
                        <div class="formBg_1 t_space">
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <%
                                                String userid = "";
                                                String strReqParam = "";
                                                HttpSession hs = request.getSession();
                                                if (hs.getAttribute("userId") != null) {
                                                    userid = hs.getAttribute("userId").toString();
                                                }
                                                strReqParam =request.getParameter("createTender");
                                                appServlet.setLogUserId(userid);
                                                appSrBean.setLogUserId(userid);
                                                tenderSrBean.setLogUserId(userid);

                                                java.math.BigDecimal id = null;
                                                if ("CreateTender".equalsIgnoreCase(strReqParam)) {
                                                    int appid = Integer.parseInt(request.getParameter("app"));
                                                    int pckid = Integer.parseInt(request.getParameter("pckradio"));
                                                    List<CommonSPReturn> details = tenderSrBean.insertSPAddYpTEndernotice(appid, pckid, userid);
                                                    for (CommonSPReturn d : details) {
                                                        id = d.getId();
                                                    }
                                                    //response.sendRedirect("CreateTender.jsp?id=" + id);
                                                    out.write("<script type=\"text/javascript\"> window.location = \"CreateTender.jsp?id=" +id+"\"</script>");
                                                }
                                                String fYear = "";
                                                java.util.List<CommonAppData> appListDtBean = new java.util.ArrayList<CommonAppData>();
                                                appListDtBean = appServlet.getAPPDetails("FinancialYear", "", "");%>
                                    <td width="14%" class="ff">Financial Year :</td>
                                    <td width="32%">
                                        <select name="financialYear" class="formTxtBox_1" id="cmbFinancialYear" style="width:200px;">
                                            <%
                                                        for (CommonAppData commonApp : appListDtBean) {
                                                            if (commonApp.getFieldName3().equals("Yes")) {
                                                                fYear = commonApp.getFieldName2();
                                            %>
                                            <option selected="selected" value="<%=commonApp.getFieldName2()%>"><%=commonApp.getFieldName2()%></option>
                                            <%
                                                                                                        } else {
                                            %>
                                            <option value="<%=commonApp.getFieldName2()%>"><%=commonApp.getFieldName2()%></option>
                                            <% }
                                                        }%>
                                        </select>
                                    </td>
                                    <td width="14%" class="ff">Select APP :  <span class="mandatory">*</span></td>
                                    <td width="40%"><input type="hidden" name="AppPackage" id="AppPackage" value="AppPackageTenderNotCreated"/>
                                        <select name="app" class="formTxtBox_1" id="cmbapp" style="width:200px;" onchange="clertTable();">
                                            <% appListDtBean = appServlet.getAPPDetails("allapp", fYear, userid);%>
                                            <option value="">- Select APP -</option>
                                            <%
                                                        for (CommonAppData commonapp : appListDtBean) {

                                            %>
                                            <option value="<%=commonapp.getFieldName1()%>"><%=commonapp.getFieldName2()%></option>
                                            <%                                                                    }
                                            %>
                                        </select><br/>
                                        <div id="erroMsg" class="mandatory"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center"><label class="formBtn_1">
                                            <input type="button" name="btnSearchApp" id="btnSearchApp" value="Search APP" onclick="loadSearchPackageTable();" />
                                        </label>
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input type="reset" name="Reset" id="button" value="Reset" />
                                        </label>                                        
                                    </td>
                                </tr>
                            </table>

                        </div>
                        <%--<div class="t-align-right t_space b_space"><a href="<%=request.getContextPath()%>/help/tendernoticedocsprep/TenderNotice_Doc_PreparationSteps_25April2011.pdf" class="action-button-download" target="_blank">Steps for Tender Preparation</a></div>--%>
                        <table width="100%" cellspacing="0" id="resultTable" class="tableList_1 t_space" >
                            <tr>
                                <th >Select</th>
                                <th width="20%"> Package No. </th>
                                <th >Package Description</th>
                            </tr>
                        </table>
                        <div style="display: none" id="tdsubmit" class="t-align-center t_space">
                            <label class="formBtn_1">
                                <input value="Submit" type="button" name="btnsubmit" id="btnsubmit" onclick="return chkcounter();" />
                                <input type="hidden" name="createTender" id="createTender" value="CreateTender" />
                            </label>
                        </div>
                        <div id="chkradiobtn" class="mandatory t-align-center">
                        </div>
                    </form>                    
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

            </div>
        </div>
    </body>
    <%
                appServlet = null;
                appSrBean = null;
                tenderSrBean = null;
    %>    
</html>
<script type="text/javascript">
    function chkcounter(){

        var checkedValue = 0;
        var chkcounter='false';
        if(document.getElementById('counter').value == 1){
            //alert(document.frmAPPForTender.pckradio.checked);
            if(document.frmAPPForTender.pckradio.checked){
                chkcounter='true';
                checkedValue = document.frmAPPForTender.pckradio.value;
            }
        }else{
            for(var i = 0;i<document.getElementById('counter').value;i++){
                //alert(document.frmAPPForTender.pckradio[i].checked);
                if(document.frmAPPForTender.pckradio[i].checked){
                    chkcounter='true';
                    checkedValue = document.frmAPPForTender.pckradio[i].value;
                }
            }
        }
        if(chkcounter == 'false'){
            document.getElementById('chkradiobtn').innerHTML='Please select a package.';
            if(document.getElementById('counter').value == 0){
                document.getElementById('chkradiobtn').innerHTML='';
            }
            return false;
        }else{
            document.getElementById('chkradiobtn').innerHTML='';
        }
        $.ajax({
            url: "<%=request.getContextPath()%>/APPForTenderServlet?action=checkBusRule&packageId="+checkedValue,
            method: 'POST',
            async: false,
            success: function(j) {
                if(j.length!=0){
                    jAlert(j," Alert ", "Alert");
                    //alert(j);
                }else{
                    document.getElementById('frmAPPForTender').submit();
                }
            }
        });
    }
    function clertTable(){
        $('#resultTable').find("tr:gt(0)").remove();
        document.getElementById('tdsubmit').style.display='none';
        document.getElementById('chkradiobtn').innerHTML='';
    }
    function loadSearchPackageTable(){
        var bvalue='true';
        if(document.getElementById("cmbapp").value == ""){
            $('#resultTable').find("tr:gt(0)").remove();
            document.getElementById('tdsubmit').style.display='none';
            bvalue = 'false';
            //return false;
        }
        else{
            document.getElementById('tdsubmit').style.display='block';
            document.getElementById("erroMsg").innerHTML ='';
        }
        $.post("<%=request.getContextPath()%>/APPForTenderServlet", {appPck:$("#AppPackage").val(),packageId:$("#cmbapp").val()},  function(j){
            $('#resultTable').find("tr:gt(0)").remove();
            $('#resultTable tr:last').after(j);
        });
        if(bvalue == 'false'){
            document.getElementById("erroMsg").innerHTML ='Please select APP';
            return false;
        }else{
            document.getElementById("erroMsg").innerHTML ='';
        }
    }
</script>
<script type="text/javascript">
    $(function() {
        $('#cmbFinancialYear').change(function() {
            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbFinancialYear').val(),funName:'appCombo'},  function(j){
                $("select#cmbapp").html(j);
            });
        });
    });
</script>
<script type="text/javascript" language="Javascript">
    var headSel_Obj = document.getElementById("headTabTender");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
</script>