<%--
Document   : Search APP
Created on : Oct 23, 2010, 6:13:00 PM
Author     : Sanjay
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Search APP</title>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("hdnUserType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                            userType.append(request.getParameter("hdnUserType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }
                        %>
                         <jsp:include page="../resources/common/AfterLoginLeft.jsp">
                            <jsp:param name="hdnUserType" value="<%=userType%>"/>
                        </jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1">
                                        <div class="t_space">
                                            <div class="pageHead_1">APP Search Result</div>
                                            <div>&nbsp;</div>
                                            <form method="POST" id="frmSearchAPP" action="">
                                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                    <tr>
                                                        <th class="t-align-center">Sl. No.</th>
                                                        <th class="t-align-center">APP Id <br /> Letter Ref. No.</th>
                                                        <th class="t-align-center">Ministry / Division &amp; Organization <br /> Procuring Entity</th>
                                                        <th class="t-align-center">Dzongkhag / District</th>
                                                        <th class="t-align-center">Procurement Category Project Name</th>
                                                        <th class="t-align-center">Package No &amp; Description</th>
                                                        <th class="t-align-center">Est. Cost Procurement Method</th>
                                                    </tr>
                                                    <tr>
                                                        <td class="t-align-center"></td>
                                                        <td class="t-align-center"></td>
                                                        <td class="t-align-center"></td>
                                                        <td class="t-align-center"></td>
                                                        <td class="t-align-center"></td>
                                                        <td class="t-align-center"><a href="#" title="Link Name">Link</a></td>
                                                        <td class="t-align-center"></td>
                                                    </tr>
                                                </table>
                                                <div>&nbsp;</div>
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabApp");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
