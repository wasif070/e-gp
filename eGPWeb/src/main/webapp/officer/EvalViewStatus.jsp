<%-- 
    Document   : EvalViewStatus
    Created on : Jun 29, 2011, 5:08:39 PM
    Author     : nishit
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SpgetCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData,com.cptu.egp.eps.web.servicebean.EvalClariPostBidderSrBean"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Question and Answer</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="dashboard_div">

            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <%
                        boolean bol_isMark = false;
                        String tenderId = "0";
                            if(request.getParameter("tenderId")!=null){
                                tenderId = request.getParameter("tenderId");
                            }
                        String userId = "", uId = "", st = "";
                        String lotId = "";
                        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                            userId = session.getAttribute("userId").toString();
                        }
                        if (request.getParameter("uid") != null) {
                            uId = request.getParameter("uid");
                        }
                        if(!"".equalsIgnoreCase(request.getParameter("uId"))){
                            uId = request.getParameter("uId");
                        }
                        if (request.getParameter("st") != null && !"".equalsIgnoreCase(request.getParameter("st"))) {
                            st = request.getParameter("st");
                        }
                        String pageName = "Evalclarify.jsp";
                        TenderCommonService tenderCs = (TenderCommonService)AppContext.getSpringBean("TenderCommonService");
                         List<SPTenderCommonData> lstTeamSelUserId =
                                tenderCs.returndata("getEvalTeamSelectedMember", tenderId, null);
                                if (!lstTeamSelUserId.isEmpty()) {
                                    if(!userId.equals(lstTeamSelUserId.get(0).getFieldName1())){
                                     userId = lstTeamSelUserId.get(0).getFieldName1();
                                     pageName = "ViewClarify.jsp";
                                    }
                                }
                                lstTeamSelUserId = null;
                        
            %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">View Question and Answer<span style="float:right;"><a href="<%=pageName%>?tenderId=<%=tenderId%>&st=<%=st%>&lnk=cl" class="action-button-goback">Go Back</a></span></div>
                <div class="t_space">
                    <%
                                pageContext.setAttribute("tenderId", tenderId);
                    %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <%
                                boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
                                // Get the UserId of the Evaluation Member selected for doing Evaluation
                               
                    %>
                    
                   
                </div>

                <div class="t_space">
                    <%//if (!"TEC".equalsIgnoreCase(request.getParameter("comType"))) {%>
                    <jsp:include page="officerTabPanel.jsp" >
                        <jsp:param name="tab" value="7" />
                    </jsp:include>
                    <%// }%>
                </div>
                <div class="tabPanelArea_1">
                    <%
                                pageContext.setAttribute("TSCtab", "4");
                                int cnt = 0;
                    %>
                    <%@include  file="../resources/common/AfterLoginTSC.jsp"%>

                    <div class="tabPanelArea_1" >
                         
                        <% for (SPTenderCommonData companyList : tenderCommonService.returndata("GetCompanynameByUserid", uId, "0")) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th colspan="2" class="t_align_left ff">Company Details</th>
                            </tr>
                            <tr>
                                <td width="15%" class="t-align-left ff">Company Name :</td>
                                <td width="85%" class="t-align-left"><%=companyList.getFieldName1()%></td>
                            </tr>
                        </table>
                            <%@include  file="../officer/TSCComments.jsp" %>

                        <%
                                    } // END FOR LOOP OF COMPANY NAME
                                        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                        CommonSearchDataMoreService statusService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                                        List<SPCommonSearchDataMore> list_status = new ArrayList<SPCommonSearchDataMore>();
                                            for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th colspan="2" class="t_align_left ff">Tender Details</th>
                                </tr>

                                <tr>
                                    <td width="15%" class="t-align-left ff">Package No. :</td>
                                    <td width="85%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Package Description :</td>
                                    <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                                </tr>
                            </table>
                                <%}if (isTenPackageWis) {%>
                             <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                 <tr>
                                     <th>
                                         Sl. No.
                                     </th>
                                     <th>
                                         Form Name
                                     </th>
                                     <!-- For TEC member comments view Modified by Dohatec -->
                                     <th>
                                         Detail
                                     </th>
                                     <!-- For TEC member comments view Dohatec End -->
                                     <th>
                                         Status
                                     </th>
                                 </tr>
                                 <%
                                        int i_pkgCount = 1;
                                      list_status = statusService.geteGPData("getFormsForBidderEvalStatusView", tenderId, uId, userId, "evaluation", "0", null, null, null, null, null, null, null, null, null, null, null, null, null, null);                                     
                                      for(SPCommonSearchDataMore obj_Status : list_status){
                                  %>
                                  <tr>
                                      <td class="t-align-center"><%=i_pkgCount%></td>
                                      <td><%=obj_Status.getFieldName1()%></td>
                                      <!-- For TEC member comments view Modified by Dohatec -->
                                      <td class="t-align-center"><a href="ViewFormEvalution.jsp?tenderId=<%=tenderId%>&formId=<%=obj_Status.getFieldName4()%>&uId=<%=uId%>&pkgLotId=0">View</a></td>
                                      <!-- For TEC member comments view Dohatec End -->
                                      <td class="t-align-center"><%=obj_Status.getFieldName2()%></td>
                                  </tr>
                                  <%
                                       i_pkgCount++; 
                                       if(obj_Status!=null){
                                         obj_Status = null;  
                                       }
                                        }
                                      if(i_pkgCount==1){
                                    %>
                                    <td colspan="4" class="t-align-center" style="font-weight: bold;color: red">No Record Found</td>
                                    <%}%>
                             </table>

                            <%
                                                                    } // END packageList FOR LOOP
                                                                    else {
                                                                        int i_lotCnt = 0;
                                                                        for (SPCommonSearchDataMore lotList : statusService.geteGPData("getLotsForBidderEvalStatusView", tenderId, uId, userId, "evaluation", null, null, null, null,null,null, null, null, null, null,null,null,null,null,null)) {
                                                                            i_lotCnt++;
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">

                                <tr>
                                    <td width="15%" class="t-align-left ff">Lot No. :</td>
                                    <td width="85%" class="t-align-left"><%=lotList.getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Lot Description :</td>
                                    <td class="t-align-left"><%=lotList.getFieldName3()%></td>
                                </tr>
                            </table>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                 <tr>
                                     <th width="15%">
                                         Sl. No.
                                     </th>
                                     <th width="70%">
                                         Form Name
                                     </th>
                                      <!-- For TEC member comments view Modified by Dohatec -->
                                     <th>
                                         Detail
                                     </th>
                                      <!-- For TEC member comments view Dohatec End -->
                                     <th width="15%">
                                         Status
                                     </th>
                                 </tr>
                                 <%
                                        int i_pkgCount = 1;
                                      list_status = statusService.geteGPData("getFormsForBidderEvalStatusView", tenderId, uId, userId, "evaluation", lotList.getFieldName1(), null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                      for(SPCommonSearchDataMore obj_Status : list_status){
                                  %>
                                  <tr>
                                      <td class="t-align-center"><%=i_pkgCount%></td>
                                      <td><%=obj_Status.getFieldName1()%></td>
                                      <!-- For TEC member comments view Modified by Dohatec -->
                                      <td class="t-align-center"><a href="ViewFormEvalution.jsp?tenderId=<%=tenderId%>&formId=<%=obj_Status.getFieldName4()%>&uId=<%=uId%>&pkgLotId=<%=lotList.getFieldName1()%>">View</a></td>
                                       <!-- For TEC member comments view Dohatec End -->
                                      <td class="t-align-center"><%=obj_Status.getFieldName2()%></td>
                                  </tr>
                                  <%
                                       i_pkgCount++; 
                                       if(obj_Status!=null){
                                         obj_Status = null;  
                                       }
                                        }
                                      if(i_pkgCount==1){
                                    %>
                                    <td colspan="4" class="t-align-center" style="font-weight: bold;color: red">No Record Found</td>
                                    <%}%>
                             </table>    
                            <%
                                                    if(lotList!=null){
                                                        lotList=null;
                                                    }
                                                                        }//End Loop for LotWise
                                                                        if(i_lotCnt==0){
                             %>
                             <table class="t_space tableList_1" width="100%">
                                 <tr>
                                     <td class="t-align-center" colspan="2" style="font-weight: bold; color: red">No Record found</td>
                                 </tr>
                             </table>
                            <% 
                                                       }/*if condtion i_lotcnt ends here*/
                                        }/*Else Part Ends here*/%>
                                        
                          
                    </div>
                </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabEval");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
<%
    if(list_status!=null){
        list_status.clear();
        list_status = null;
    }
%>    
</html>

