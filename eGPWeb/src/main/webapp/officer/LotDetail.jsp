<%-- 
    Document   : LotDetail
    Created on : Dec 11, 2010, 7:49:22 PM
    Author     : Administrator
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
            <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Lot Detail</title>
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>

</head>
<body>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
  </div>
 
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
 

 	<div class="pageHead_1">Lot Details
        <span class="t-align-right" style="float: right;"><a class="action-button-goback" href="Amendment.jsp?tenderid=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a></span></div>
         <%
  pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
%>
 <%@include file="../resources/common/TenderInfoBar.jsp" %>
         <%@include  file="CommonLotDetail.jsp"%>
	<div>&nbsp;</div>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
  <table width="100%" cellspacing="0" class="footerCss">
    <tr>
      <td align="left">e-GP &copy; All Rights Reserved
        <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
      <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a></td>
    </tr>
  </table>
  <!--Dashboard Footer End-->
</div>
</body>
        <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>

