<%-- 
    Document   : ViewTendererHash
    Created on : Feb 7, 2011, 7:03:37 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Bidder's/Consultant's Hash</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
             <%@include file="../resources/common/AfterLoginTop.jsp" %>
             <div class="contentArea_1">
             <div class="pageHead_1">View Bidder's/Consultant's Hash<span style="float:right;"><a href="<%=request.getHeader("Referer")%>" class="action-button-goback">Go Back</a></span></div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <%
                        String tenderId = "";

                         if (request.getParameter("tenderId") != null){
                        tenderId = request.getParameter("tenderId");
                        

                        }
            %>

 
              <%   pageContext.setAttribute("tenderId", tenderId); %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                    <%
                        CommonSearchService searchService = (CommonSearchService)AppContext.getSpringBean("CommonSearchService");
                        List<SPCommonSearchData> megaHash = searchService.searchData("getTenderMegaMegaHash", tenderId, null, null, null, null, null, null, null, null);
                        if(megaHash!=null && (!megaHash.isEmpty())){
                            out.print("<div class='tableHead_1 t_space'>Tender Mega Mega Hash : "+megaHash.get(0).getFieldName1()+"</div>");
                        }
                    %>
                                 <table width="100%" cellspacing="0" class="tableList_1">
                                     <tr>
                                        <th width="4%" class="t-align-center">Sl. No.</th>
                                        <th class="t-align-center" width="23%">Bidders / Consultants</th>
                                        <th class="t-align-center" width="45%">Mega Hash</th>
                                        <th class="t-align-center" width="28%">Submitted Forms and <br/>Documents e-Signature</th>
                                    </tr>
                    <%
                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                     
                    for (SPTenderCommonData sptcd : tenderCS.returndata("GetTendererHash", tenderId, "")) {
                                                        docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName3()%></td>
                        <td class="t-align-center">
                            <a href="ViewFormDocESign.jsp?tenderId=<%=tenderId%>&userId=<%=sptcd.getFieldName1()%>" target="_blank">View</a>
                        </td>
                    </tr>
                         <%   if(sptcd!=null){
                        sptcd = null;
                    }
                    }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
            </table>
             </div>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
            </div>

       
    </body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>
</html>
