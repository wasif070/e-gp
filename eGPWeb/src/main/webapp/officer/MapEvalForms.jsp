<%-- 
    Document   : MapEvalForms
    Created on : Dec 29, 2010, 4:10:42 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.MapEvalFormsSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Map forms for Evaluation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
         <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>

        <%
        MapEvalFormsSrBean srBean = new MapEvalFormsSrBean();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String thisTenderId="";
        String thisLotId="0";        
        String thisUserId="";
        String referer = "";
        String orgReferer="";
        String strCcomType="";
        String strXML="";
        String actionType="add";
        
        boolean isCurTenLotWise=false;
        boolean isCurTenPackageWise = false;
        boolean isMemberDecrypter = false;
        boolean chkSuccess = false;
        
        if(request.getParameter("tenderid")!=null){
            thisTenderId = request.getParameter("tenderid");
        }
       
        if(request.getParameter("comType")!=null){
            strCcomType=request.getParameter("comType");
        }

        if (session.getAttribute("userId") != null) {
            thisUserId = session.getAttribute("userId").toString();
        }

        if ( request.getParameter("action") != null) {
            if ( !"".equalsIgnoreCase(request.getParameter("action")) && !"null".equalsIgnoreCase(request.getParameter("action"))){
                   actionType = request.getParameter("action");
               }            
        }

         List<SPTenderCommonData> listTenderTyp = srBean.mnethod(thisTenderId);

            if (!listTenderTyp.isEmpty()) {
                 if ("package".equalsIgnoreCase(listTenderTyp.get(0).getFieldName1())) {
                    isCurTenPackageWise = true;
                } else {
                    isCurTenLotWise = true;
                }
            }
         
         listTenderTyp=null;

            if (request.getParameter("lotId") != null) {
               if ( !"0".equalsIgnoreCase(request.getParameter("lotId")) && !"".equalsIgnoreCase(request.getParameter("lotId")) && !"null".equalsIgnoreCase(request.getParameter("lotId"))){
                   isCurTenLotWise = true;
                   thisLotId=  request.getParameter("lotId");
               } else {
                    isCurTenPackageWise = true;
               }
            } else {
                isCurTenPackageWise = true;
            }

            if (request.getParameter("btnSubmit") != null){               
                    int intCounter=0; 
                    int frmCount=0;
                    String ctrlHidden="hdnFormId_";
                    String ctrlRadio="rdCom_";
                    String frmId="";
                    String comType="";
                    String isTECCom="";
                    String isTSCCom="";
                    
                    frmCount= Integer.parseInt(request.getParameter("hdnFormCnt"));

                    for(intCounter=1;intCounter<=frmCount;intCounter++){
                        
                        frmId = request.getParameter(ctrlHidden+intCounter);
                        comType = request.getParameter(ctrlRadio+intCounter);

                        if ("TSC".equalsIgnoreCase(comType)){
                            if ("".equalsIgnoreCase(strXML)){
                                strXML = "<tbl_EvalMapForms tenderId=\"" + thisTenderId + "\" pkgLotId=\"" + thisLotId + "\" formId=\"" + frmId + "\" tecCom=\"no\" tscCom=\"yes\" configBy=\"" + thisUserId + "\" configDate=\"" + format.format(new Date()) + "\" />";
                            } else{
                                strXML += "<tbl_EvalMapForms tenderId=\"" + thisTenderId + "\" pkgLotId=\"" + thisLotId + "\" formId=\"" + frmId + "\" tecCom=\"no\" tscCom=\"yes\" configBy=\"" + thisUserId + "\" configDate=\"" + format.format(new Date()) + "\" />";
                            }
                        }           
                    }
                    
                    if (!"".equalsIgnoreCase(strXML)){
                        chkSuccess= srBean.mapEvaluationForms(strXML, thisTenderId, thisLotId);

                        if (chkSuccess) {                               
                               if("0".equalsIgnoreCase(thisLotId)){
                                thisLotId="";
                               }
                              
                        if(request.getParameter("hdnReferer")!=null){
                            orgReferer=request.getParameter("hdnReferer");
                        }
                               String path="";
                               if (isCurTenLotWise) {
                                   path="MapEvalTenderLots.jsp?tenderId="+ thisTenderId + "&msgId=mapped";
                               } else {
                                   path="EvalComm.jsp?tenderid="+ thisTenderId + "&msgId=mapped";
                               }
                               //response.sendRedirect(orgReferer + "&msgId=mapped");
                               response.sendRedirect(path);
                          } else {                            
                            if("0".equalsIgnoreCase(thisLotId)){
                                thisLotId="";
                               }
                            response.sendRedirect("MapEvalForms.jsp?action=" + actionType + "&tenderid=" + thisTenderId + "&lotId=" + thisLotId + "&msgId=error" + "&comType="+ strCcomType);
                         }
                    }

                    ctrlHidden=null;
                    ctrlRadio=null;
                    frmId=null;
                    comType=null;
                    isTECCom=null;
                    isTSCCom=null;

            }
%>


<script>
    function validateSubmit(obj){

        document.getElementById("errMsg").innerHTML="";

        var elm = obj.form.elements;
        var chkCount=0;
        var frmCnt=document.getElementById("hdnFormCnt").value;
        //alert("Total Forms : " + frmCnt);

        for (i = 0; i < elm.length; i++) {
            if (elm[i].type=="checkbox"){
                if (elm[i].checked==true){
                    chkCount++;
                }
            }
        }

        if (chkCount==0){
            document.getElementById("errMsg").innerHTML="<br/>Select at least one checkbox.";
            return false;
        }


    }

    function CheckAll(obj){
        //alert(obj.checked);
        var state=obj.checked;
        //alert(state);
        var elm = obj.form.elements;
        for (i = 0; i < elm.length; i++) {
            if (elm[i].type=="checkbox"){
                elm[i].checked=state;
            }
        }
    }
</script>

  
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

    </script>


      <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->

                <!--Dashboard Content Part Start-->
                <%                
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");

                    if(referer.indexOf("MakePaymentView")>0){
                        if(request.getParameter("hdnReferer")!=null){
                            orgReferer=request.getParameter("hdnReferer");
                        }
                    } else{
                        orgReferer = request.getHeader("referer");
                    }
                }
                
               %>
                <div class="contentArea_1">
                    <div class="pageHead_1">
                        &nbsp;Share forms for TSC
                        <span style="float: right;" >                            
                            <a href="<%=referer%>" class="action-button-goback">Go Back</a>
                        </span>                        
                    </div>
               <%
                pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
               %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <div>&nbsp;</div>
                <% pageContext.setAttribute("tab", "7");%>
                <%@include file="../officer/officerTabPanel.jsp" %>
                
                <div class="tabPanelArea_1">

                <%-- Start: CODE TO DISPLAY MESSAGES --%>
                     <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("mapped")){
                           msgTxt="Forms mapped successfully";
                       } else if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                       }  else {
                           msgTxt="";
                       }
                    %>
                   <%if (isError){%>
                        <div class="responseMsg errorMsg"><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg"><%=msgTxt%></div>
                   <%}%>
                <%}
                     msgId = null;
                     msgTxt = null;
                }%>

                <%-- End: CODE TO DISPLAY MESSAGES --%>

                     <%-- Start: Common Evaluation Table --%>
                <%@include file="/officer/EvalCommCommon.jsp" %>
               <%-- End: Common Evaluation Table --%>
                 
                    <form id="frmEvalForms" action="MapEvalForms.jsp?action=<%=actionType%>&tenderid=<%=request.getParameter("tenderid")%>&lotId=<%=request.getParameter("lotId")%>&comType=<%=strCcomType%>" method="POST">
                     <input type="hidden" name="hdnReferer" value="<%=orgReferer%>">
                        <% if (!"0".equalsIgnoreCase(thisLotId)) {%>

         <%
                    
                    List<SPTenderCommonData> PackageList = srBean.getLotOrPackageData(thisTenderId, "Package,1");

                    if (!"0".equalsIgnoreCase(thisLotId)) {
                    
                        List<SPTenderCommonData> lotList = srBean.getLotOrPackageData(thisTenderId, "Lot," + thisLotId);

                            if (!PackageList.isEmpty() && PackageList.size() > 0 && !lotList.isEmpty() && lotList.size() > 0) {%>
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <td width="15%" class="t-align-left ff">Package No. :</td>
                                    <td class="t-align-left"><%=PackageList.get(0).getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Package Description :</td>
                                    <td class="t-align-left"><%=PackageList.get(0).getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Lot No. :</td>
                                    <td><%=lotList.get(0).getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">Lot Description :</td>
                                    <td><%=lotList.get(0).getFieldName2()%></td>
                                </tr>
                            </table>
                        <%}
                        
                        lotList=null;
                     }

                     PackageList=null;
                    %>


        <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
            <tr>
                <th width="58%" class="ff">Form Name </th>
                <th width="24%" >Select TSC Committee</th>
            </tr>
        <%
    int frmCnt=0;
    
    List<SPTenderCommonData> formList = srBean.getLotOrPackageForms(thisTenderId, thisLotId);

    if (!formList.isEmpty()) {
        for (SPTenderCommonData formname : formList) {
            frmCnt++;
        %>
            <tr
                 <%if(Math.IEEEremainder(frmCnt,2)==0) {%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>
                >
                <td class="ff">
                    <p>
                        
                        <strong><%=formname.getFieldName1()%></strong>
                        <input type="hidden" name="hdnFormId_<%=frmCnt%>" value="<%=formname.getFieldName2()%>">
                    </p>
                </td>
                <td class="t-align-center">
                    <input type="radio" id="rdTSC" name="rdCom_<%=frmCnt%>" value="TSC" <%if ("tsccom".equalsIgnoreCase(formname.getFieldName3())) {%>checked="checked"<%}%> >
                </td>

            </tr>
        <%} // End For Loop
        }

        formList=null;

        if (frmCnt==0) {%>
         <tr>
            <td class="t-align-left" colspan="2">No forms found!</td>
        </tr>
        <%} else {%>
        <tr>
            <td class="t-align-center" colspan="2">
                <label class="formBtn_1">
                    <input name="btnSubmit" id="btnSubmit" type="submit" value="Submit" />
                    <input type="hidden" id="hdnFormCnt" name="hdnFormCnt" value="<%=frmCnt%>">
                </label>
                <span id="errMsg" class='reqF_1'></span>
            </td>
        </tr>
        <%}%>
        </table>
        <%} else if (isCurTenPackageWise){%>

         <%            
            List<SPTenderCommonData> PackageList = srBean.getLotOrPackageData(thisTenderId, "Package,1");
                    if (!PackageList.isEmpty() && PackageList.size() > 0) {%>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td width="15%" class="t-align-left ff">Package No. :</td>
                            <td class="t-align-left"><%=PackageList.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=PackageList.get(0).getFieldName2()%></td>
                        </tr>
                    </table>
        <%}
            PackageList=null;
        %>


        <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
            <tr>
                <th width="58%" class="ff">Form Name </th>
                <th width="24%">
                    <input type="checkbox" name="cbAll" id="cbAll" onclick="return CheckAll(this);">                    
                    Select TSC Committee
                </th>
            </tr>
        <%
        int frmCnt=0;
        
        List<SPTenderCommonData> formList = srBean.getLotOrPackageForms(thisTenderId, "0");

        if (!formList.isEmpty()) {
        for (SPTenderCommonData formname : formList) {
            frmCnt++;
    %>
    <tr
        <%if(Math.IEEEremainder(frmCnt,2)==0) {%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>
        >
        <td class="ff">
            <p>
                
                <strong><%=formname.getFieldName1()%></strong>
                <input type="hidden" name="hdnFormId_<%=frmCnt%>" value="<%=formname.getFieldName2()%>">
            </p>
        </td>
        <td class="t-align-center">
            <input type="checkbox" id="rdTSC" name="rdCom_<%=frmCnt%>" value="TSC" <%if ("tsccom".equalsIgnoreCase(formname.getFieldName3())) {%>checked="checked"<%}%> >
        </td>
    </tr>
    <%} // End For Loop
    }

    formList=null;
    if (frmCnt==0) {%>
     <tr>
        <td class="t-align-left" colspan="2">No forms found!</td>
    </tr>
    <%} else {%>
         <tr>
            <td class="t-align-center" colspan="2">
                <label class="formBtn_1">
                    <input name="btnSubmit" id="btnSubmit" type="submit" value="Submit" onclick="javascript: return validateSubmit(this)" />
                    <input type="hidden" id="hdnFormCnt" name="hdnFormCnt" value="<%=frmCnt%>">
                </label>
                <span id="errMsg" class='reqF_1'></span>
            </td>
        </tr>
    <%}%>
    </table>
  <%}%>

  <%
  // Set To Null
srBean=null;
format=null;

thisTenderId=null;
thisLotId=null;
thisUserId=null;
referer =null;
orgReferer=null;
strCcomType=null;
strXML=null;
actionType=null;

%>

</form>
                </div>               
                </div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->

      </div>
</body>

</html>
