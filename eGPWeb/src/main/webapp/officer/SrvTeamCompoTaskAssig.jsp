<%-- 
    Document   : SrvReExpense
    Created on : Dec 5, 2011, 11:52:47 AM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTeamComp"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Team Composition and Task Assignments</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Team Composition and Task Assignments
                    <span style="float: right; text-align: right;" class="noprint">
                    <a class="action-button-goback" href="WorkScheduleMain.jsp?tenderId=<%=request.getParameter("tenderId")%>" title="Go Back To WorkSchedule">Go Back</a>
                    </span>
            </div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%@include file="../resources/common/ContractInfoBar.jsp"%>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "14");
                        ResourceBundle bdl = null;
                        bdl = ResourceBundle.getBundle("properties.cmsproperty");
                        String tenderId = "";
                        if (request.getParameter("tenderId") != null) {
                            tenderId = request.getParameter("tenderId");
                        }
                        String lotId = "";
                        if (request.getParameter("lotId") != null) {
                            lotId = request.getParameter("lotId");
                        }
                        String formMapId = "";
                        if (request.getParameter("formMapId") != null) {
                            formMapId = request.getParameter("formMapId");
                        }
                        if (request.getParameter("srvFormMapId") != null) {
                            formMapId = request.getParameter("srvFormMapId").toString();
                        }
                        boolean flag = false;
                        CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                        String serviceType = commService.getServiceTypeForTender(Integer.parseInt(tenderId));
                        if ("Time based".equalsIgnoreCase(serviceType.toString())) {
                            flag = true;
                        }

            %>
            <%@include  file="officerTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "1");

                %>
                <%@include  file="../resources/common/CMSTab.jsp"%>
                <div class="tabPanelArea_1" >
                    <% if (request.getParameter("msg") != null) {
                                    if ("succ".equalsIgnoreCase(request.getParameter("msg"))) {
                    %>
                    <div class='responseMsg successMsg'>Team Composition and Task Assignments details updated Successfully</div>
                    <%} else if ("fail".equalsIgnoreCase(request.getParameter("msg"))) {%>
                    <div class='responseMsg errorMsg'>Team Composition and Task Assignments Data Submittion Failed</div>
                    <%}
                                }%>
                    <div align="center">                        
                        <form id="frmReExpenses" name="frmReExpenses" method="post" action='<%=request.getContextPath()%>/CMSSerCaseServlet?srvFormMapId=<%=formMapId%>&action=SrvTeamCompoTaskAssig&tenderId=<%=tenderId%>&lotId=<%=lotId%>'>
                            <table  border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <tr>
                                    <th>Sl. No.</th>
                                    <th>Position Assigned</th>
                                    <th>Name of  Staff</th>
                                    <th>Firm/Organization</th>
                                    <th>Staff Category (Key / Support)</th>
                                    <th>Position Defined by Consultant</th>
                                    <th>Category of Consultant proposed</th>
                                    <th>Area of Expertise</th>
                                    <th>Task Assigned</th>
                                </tr>
                                <%
                                            CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                                            List<TblCmsSrvTeamComp> list = cmss.getSrvTeamCompoTaskAssigData(Integer.parseInt(formMapId));
                                            if (!list.isEmpty()) {
                                                for (int i = 0; i < list.size(); i++) {
                                %>
                                <tr>
                                    <td class="t-align-right" style="text-align: right"><%=list.get(i).getSrno()%>
                                    <input type="hidden" name="Srno" value="<%=list.get(i).getSrno()%>">    
                                    </td>
                                    <td class="t-align-left"><%=list.get(i).getPositionAssigned()%>
                                    <input type="hidden" name="PositionAssigned" value="<%=list.get(i).getPositionAssigned()%>">        
                                    </td>                                    
                                    <td class="t-align-center"><input type="text" name="nameOfStafftxt"  id="nameOfStaffid<%=i%>" value="<%=list.get(i).getEmpName()%>" class="formTxtBox_1 w_textbox" onblur="return validate()"/><span id="nameOfStaffspan<%=i%>" class="reqF_1"></span></td>
                                    <td class="t-align-center"><input type="text" name="FirmOrganizationtxt"  id="FirmOrganizationid<%=i%>" value="<%=list.get(i).getOrganization()%>" class="formTxtBox_1 w_textbox" onblur="return validate()"/><span id="FirmOrganizationspan<%=i%>" class="reqF_1"></span></td>                                    
                                    <td class="t-align-left"><%=list.get(i).getStaffCat()%>
                                    <input type="hidden" name="StaffCat" value="<%=list.get(i).getStaffCat()%>">    
                                    </td>
                                    <td class="t-align-center"><input type="text" name="PositionConsulttxt"  id="PositionConsultid<%=i%>" value="<%=list.get(i).getPositionDefined()%>" class="formTxtBox_1 w_textbox" onblur="return validate()"/><span id="PositionConsultspan<%=i%>" class="reqF_1"></span></td>
                                    <td class="t-align-center"><input type="text" name="ConsultCategorytxt"  id="ConsultCategoryid<%=i%>" value="<%=list.get(i).getConsultPropCat()%>" class="formTxtBox_1 w_textbox" onblur="return validate()"/><span id="ConsultCategoryspan<%=i%>" class="reqF_1"></span></td>
                                    <td class="t-align-center"><input type="text" name="AreaOfExpertisetxt"  id="AreaOfExpertiseid<%=i%>" value="<%=list.get(i).getAreaOfExpertise()%>" class="formTxtBox_1 w_textbox" onblur="return validate()"/><span id="AreaOfExpertisespan<%=i%>" class="reqF_1"></span></td>
                                    <td class="t-align-center"><input type="text" name="TaskAssignedtxt"  id="TaskAssignedid<%=i%>" value="<%=list.get(i).getTaskAssigned()%>" class="formTxtBox_1 w_textbox" onblur="return validate()"/><span id="TaskAssignedspan<%=i%>" class="reqF_1"></span></td>                                    
                                <input type="hidden" name="srvTCId" value="<%=list.get(i).getSrvTcid()%>">
                                <input type="hidden" name="srvFormMapId" value="<%=formMapId%>">
                                <input type="hidden" name="forms" <%if (flag) {%>value="timebase"<%} else {%>value="lumpsum"<%}%>>
                                </tr>                                
                                <%}
                                            }%>
                            </table>
                            <table class="t_space">
                                <tr>
                                    <td>
                                        <label class="formBtn_1">
                                            <input type="submit" name="submitbtn" id="submit" value="Submit" onclick="return validate();"/>
                                        </label>
                                    </td>    
                                </tr>    
                            </table>
                           <br />
                        </form>
                    </div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
    <script>
        function regForNumber(value)
        {
            return /^(0|(\+)?[1-9]{1}[0-9]{0,8}|(\+)?[1-3]{1}[0-9]{1,9}|(\+)?[4]{1}([0-1]{1}[0-9]{8}|[2]{1}([0-8]{1}[0-9]{7}|[9]{1}([0-3]{1}[0-9]{6}|[4]{1}([0-8]{1}[0-9]{5}|[9]{1}([0-5]{1}[0-9]{4}|[6]{1}([0-6]{1}[0-9]{3}|[7]{1}([0-1]{1}[0-9]{2}|[2]{1}([0-8]{1}[0-9]{1}|[9]{1}[0-5]{1})))))))))$/.test(value);

        }
        function validate()
        {
            var vbool = true;
            for(var id = 0; id<<%=list.size()%>; id++)
            {
                if(document.getElementById('nameOfStaffid'+id).value!="")
                {
                    document.getElementById('nameOfStaffspan'+id).innerHTML="";
                }else{
                    document.getElementById('nameOfStaffspan'+id).innerHTML="Please enter Name of Staff";
                    vbool = false;
                }
                if(document.getElementById('FirmOrganizationid'+id).value!="")
                {
                    document.getElementById('FirmOrganizationspan'+id).innerHTML="";
                }else{
                    document.getElementById('FirmOrganizationspan'+id).innerHTML="Please enter Firm/Organization";
                    vbool = false;
                }
                if(document.getElementById('PositionConsultid'+id).value!="")
                {
                    document.getElementById('PositionConsultspan'+id).innerHTML="";
                }else{
                    document.getElementById('PositionConsultspan'+id).innerHTML="Please enter Position Defined by Consultant";
                    vbool = false;
                }
                if(document.getElementById('ConsultCategoryid'+id).value!="")
                {
                    document.getElementById('ConsultCategoryspan'+id).innerHTML="";
                }else{
                    document.getElementById('ConsultCategoryspan'+id).innerHTML="Please enter Category of Consultant proposed";
                    vbool = false;
                }
                if(document.getElementById('AreaOfExpertiseid'+id).value!="")
                {
                    document.getElementById('AreaOfExpertisespan'+id).innerHTML="";
                }else{
                    document.getElementById('AreaOfExpertisespan'+id).innerHTML="Please enter Area of Expertise";
                    vbool = false;
                }
                if(document.getElementById('TaskAssignedid'+id).value!="")
                {
                    document.getElementById('TaskAssignedspan'+id).innerHTML="";
                }else{
                    document.getElementById('TaskAssignedspan'+id).innerHTML="Please enter Task Assigned";
                    vbool = false;
                }
            }
            return vbool;
        }
    </script>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

