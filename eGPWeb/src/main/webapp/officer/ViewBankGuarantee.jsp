<%-- 
    Document   : ViewBankGuarantee
    Created on : Sep 20, 2011, 4:54:07 PM
    Author     : rikin.p
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsNewBankGuarnatee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsNewBankGuarnateeService"%>
<!-- Dohatec Start -->
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<!-- Dohatec End -->
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/form/CommonValidation.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.print.blocklink.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New Performance Security</title>
    </head>
    <%
                String tenderId = "";
                String userId = "";
                String lotId ="";
                String uId = "";
                int bgId = 0;
                
                CmsNewBankGuarnateeService cmsNewBankGuarnateeService = (CmsNewBankGuarnateeService) AppContext.getSpringBean("CmsNewBankGuarnateeService"); 
                
                if (request.getParameter("tenderId") != null) {
                    tenderId = request.getParameter("tenderId");
                    pageContext.setAttribute("tenderId", tenderId);
                }
                if (session.getAttribute("userId") == null) {
                    response.sendRedirect("SessionTimedOut.jsp");
                } else {
                    userId = session.getAttribute("userId").toString();
                }                
                if (request.getParameter("lotId") != null) {
                    lotId = request.getParameter("lotId");
                }
                if (request.getParameter("uId") != null) {
                    uId = request.getParameter("uId");
                }
                if (request.getParameter("lotId") != null) {
                    pageContext.setAttribute("lotId", lotId);
                }
                if (request.getParameter("bgId") != null) {
                    bgId = Integer.parseInt(request.getParameter("bgId"));
                }

                // Dohatec Start
                TenderCommonService tcs1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                List<SPTenderCommonData> listDP = tcs1.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                boolean isIctTender = false;
                if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                   isIctTender = true;
                }

                List<TblCmsNewBankGuarnatee> listBGDetails;
                List<Object[]> listBGDetailsICT = null;
                TblCmsNewBankGuarnatee tblCmsNewBankGuarnatee = null;
                if(isIctTender)
                {
                    listBGDetailsICT = cmsNewBankGuarnateeService.fetchBankGuaranteeDetailsICT(Integer.parseInt(tenderId), Integer.parseInt(lotId), Integer.parseInt(uId));
                }
                else    // Old Version
                {
                    //List<TblCmsNewBankGuarnatee> listBGDetails = cmsNewBankGuarnateeService.fetchBankGuaranteeDetails(bgId);
                    //TblCmsNewBankGuarnatee tblCmsNewBankGuarnatee = null;
                    listBGDetails = cmsNewBankGuarnateeService.fetchBankGuaranteeDetails(bgId);
                    if(!listBGDetails.isEmpty()){
                        tblCmsNewBankGuarnatee = listBGDetails.get(0);
                    }
                }
                // Dohatec End
                
                            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            ConsolodateService servicee = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                            int conId = servicee.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), conId, "contractId", EgpModule.Payment.getName(), "View New Performance Security", "PE Side");
    %>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <div  id="print_area">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                              <%@include file="../resources/common/TenderInfoBar.jsp" %>
                              <%@include  file="../resources/common/ContractInfoBar.jsp"%>
                            <br/>
                            <div class="pageHead_1">Bank Guarantee/Bank Draft for New Performance Security
                                <span style="float: right; text-align: right;" class="noprint">
                                        <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                                    <%
                                        CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
                                        String procnature = commonservice.getProcNature(request.getParameter("tenderId")).toString();
                                        if("services".equalsIgnoreCase(procnature)){
                                    %>
                                    <a class="action-button-goback" href="InvoiceServiceCase.jsp?tenderId=<%=tenderId%>">Go back</a>
                                    <%}else{%>
                                    <a class="action-button-goback" href="Invoice.jsp?tenderId=<%=tenderId%>">Go back</a>
                                    <%}%>
                                        
                                </span>
                            </div>
                            <!-- Dohatec Start -->
                            <%if(isIctTender){ %>
                            
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" >
                                    <%
                                        String lstDateSub = "";
                                        String validDay = "";
                                        String lstDateValid = "";
                                        String remark = "";
                                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                        int listSize = listBGDetailsICT.size();
                                        System.out.print("\n List : "+listSize);
                                        for(Object[] obj : listBGDetailsICT)
                                        {
                                            if(obj[1]!=null){
                                                lstDateSub = DateUtils.customDateFormate(df.parse(obj[1].toString()));
                                            }

                                            validDay = obj[2].toString();

                                            if(obj[3]!= null){
                                                lstDateValid = DateUtils.customDateFormate(df.parse(obj[3].toString()));
                                            }

                                           if(obj[4]!=null){
                                                remark = obj[4].toString();
                                           }

                                    %>
                                    <tr>
                                        <td class='ff' class='t-align-left' width="20%">
                                            Amount (In <%=obj[5].toString()%>) :
                                        </td>
                                        <td class='t-align-left'>
                                            <%=obj[0].toString()%>
                                        </td>
                                    </tr>
                                    <%}%>
                                    <tr>
                                        <td class='ff' class='t-align-left'>
                                            Last Date of Submission : <span class="mandatory">*</span>
                                        </td>
                                        <td class='t-align-left'>
                                            <%=lstDateSub%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class='ff' class='t-align-left' width="15%">
                                            Validity in No. of Days : <span class="mandatory">*</span>
                                        </td>
                                        <td class='t-align-left'>
                                            <%=validDay%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class='ff' class='t-align-left'>
                                            Last Date of Validity :
                                        </td>
                                        <td class='t-align-left'>
                                            <%=lstDateValid %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Remarks : </td>
                                        <td>
                                            <%=remark%>
                                        </td>
                                    </tr>
                                </table>

                            <%}else{%>

                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" >
                                    <tr>
                                        <td class='ff' class='t-align-left' width="20%">
                                            Amount (In Nu.) :
                                        </td>
                                        <td class='t-align-left'>
                                            <%=tblCmsNewBankGuarnatee.getBgAmt()%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class='ff' class='t-align-left'>
                                            Last Date of Submission : <span class="mandatory">*</span>
                                        </td>
                                        <td class='t-align-left'>
                                            <%
                                               if(tblCmsNewBankGuarnatee.getLastDtOfPayment()!=null){
                                                out.print(DateUtils.customDateFormate(tblCmsNewBankGuarnatee.getLastDtOfPayment()));
                                               }
                                            %>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class='ff' class='t-align-left' width="15%">
                                            Validity in No. of Days : <span class="mandatory">*</span>
                                        </td>
                                        <td class='t-align-left'>
                                            <%

                                                  out.print(tblCmsNewBankGuarnatee.getValidityDays());
                                            %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class='ff' class='t-align-left'>
                                            Last Date of Validity :
                                        </td>
                                        <td class='t-align-left'>
                                            <%
                                               if(tblCmsNewBankGuarnatee.getLastDtOfPayment()!= null){
                                                    out.print(DateUtils.customDateFormate(tblCmsNewBankGuarnatee.getLastDtOfValidity()));
                                               }
                                            %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Remarks : </td>
                                        <td>
                                            <%
                                               if(tblCmsNewBankGuarnatee.getLastDtOfPayment()!=null){
                                                    out.print(tblCmsNewBankGuarnatee.getRemarks());
                                               }else{
                                                    out.print("-");
                                               }
                                            %>
                                        </td>
                                    </tr>
                                </table>

                             <%}%>
                             <!-- Dohatec End -->
                             
                        </td>
                        <!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End-->
                </div>
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
    
<!-- </script> -->
</html>