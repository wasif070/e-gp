<%-- 
    Document   : TenderExtReqList
    Created on : Nov 30, 2010, 12:42:04 PM
    Author     : rajesh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender Validity Extension Request</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript">

            $(document).ready(function() {

                //Default Action
                $(".tabPanelArea_1").hide(); //Hide all content
                $("ul.tabPanel_1 li:first").addClass("sMenu").show(); //Activate first tab
                $(".tabPanelArea_1:first").show(); //Show first tab content

                $('tab1').show();

                //On Click Event
                $("ul.tabPanel_1 li").click(function() {
                    $("ul.tabPanel_1 li").removeClass("sMenu"); //Remove any "active" class
                    $(this).addClass("sMenu"); //Add "active" class to selected tab
                    $(".tabPanelArea_1").hide(); //Hide all tab content
                    var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content

                    if(activeTab=='#tab1')
                    {
                        $('tab1').hide();
                        $('tab2').show();
                        //document.getElementById(activeTab.replace("#","")).innerHTML='This is first Tab';
                    }
                    else if (activeTab=='#tab2')
                    {
                        //document.getElementById(activeTab.replace("#","")).innerHTML='This is second Tab';
                        $('tab2').hide();
                        $('tab1').show();
                    }

                    $(activeTab).fadeIn(); //Fade in the active content
                    return false;
                });

            });
        </script>

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Validity Extension Request</div>

            <ul class="tabPanel_1 t_space">
                <li class="sMenu"><a href="#tab1">Pending</a></li>
                <li class=""><a href="#tab2">Approved</a></li>
            </ul>
            <div class="tabPanelArea_1" id="tab1" style="display: block;">
                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <th width="4%" class="t-align-left">Sl. No. </th>
                        <th width="14%" class="t-align-left">ID</th>
                        <th width="13%" class="t-align-left">Ref. No.</th>
                        <th width="20%" class="t-align-left">Department</th>
                        <th width="20%" class="t-align-left">Office</th>
                        <th width="18%" class="t-align-left">Brief</th>
                        <th width="11%" class="t-align-left">Action</th>
                    </tr>
                    <%
                                String userId = "";
                                HttpSession hs = request.getSession();
                                if (hs.getAttribute("userId") != null) {
                                    userId = hs.getAttribute("userId").toString();
                                }
                                int i = 1;
                                TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                                List<SPTenderCommonData> listas = tenderCommonService.returndata("ValidityExtensionRequest", userId, "Pending");
                                if (!listas.isEmpty()) {
                                    for (SPTenderCommonData sptcd : listas) {
                    %>
                    <tr>
                        <td class="t-align-left"><%=i%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName3()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName4()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName5()%></td>
                        <td class="t-align-left"><a href="TenExtReqAPP.jsp?ExtId=<%=sptcd.getFieldName6()%>&tenderId=<%=sptcd.getFieldName1()%>">Process</a></td>
                    </tr>
                    <%

                                                            i++;
                                                        }
                                                    } else {%>
                    <tr>
                        <td class="t-align-left" colspan="7">No Record Found</td>
                    </tr>
                    <%}
                    %>
                </table>
            </div>
            <div class="tabPanelArea_1" id="tab2" style="display: none;">
                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <th width="4%" class="t-align-left">Sl. No. </th>
                        <th width="14%" class="t-align-left">ID</th>
                        <th width="13%" class="t-align-left">Ref. No.</th>
                        <th width="20%" class="t-align-left">Department</th>
                        <th width="20%" class="t-align-left">Office</th>
                        <th width="18%" class="t-align-left">Brief</th>
                        <th width="11%" class="t-align-left">Action</th>
                    </tr>
                    <%
                                int j = 1;
                                List<SPTenderCommonData> list = tenderCommonService.returndata("ValidityExtensionRequest", userId, "Approved");
                                if (!list.isEmpty()) {
                                    for (SPTenderCommonData sptcd : list) {
                    %>
                    <tr>
                        <td class="t-align-left"><%=j%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName3()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName4()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName5()%></td>
                        <td class="t-align-left"><a href="TenExtReqAPP.jsp?ExtId=<%=sptcd.getFieldName6()%>&tenderId=<%=sptcd.getFieldName1()%>">Process</a></td>
                    </tr>
                    <%

                                                            j++;
                                                        }
                                                    } else {%>
                    <tr>
                        <td class="t-align-left" colspan="7">No Record Found</td>
                    </tr>
                    <%}
                    %>
                </table>
            </div>

            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
