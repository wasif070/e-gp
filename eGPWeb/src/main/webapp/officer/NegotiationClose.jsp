<%--
    Document   : NegotiationConfig
    Created on : Dec 23, 2010, 3:22:15 PM
    Author     : parag
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<jsp:useBean id="negotiationdtbean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Negotiation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <%--<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>--%>
        <%--<script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script><script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
          <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
          <script type="text/javascript">

     $(document).ready(function() {
        $("#idfrmnegotiation").validate({
            rules: {
                txtRemarks: { required: true,maxlength: 1000}

            },
            messages: {
                txtRemarks: { required: "<div class='reqF_1'>Please Enter Comments</div>",
                maxlength: "<div class='reqF_1'>Maximum 1000 characters are allowed</div>"}
            }
        }
    );
    });


 </script>

    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp"%>
                <%
                            int userId = 0;
                            byte suserTypeId1 = 0;
                            int tenderId = 1;
                            int negId = 0;
                            String agreeStatus = "";
                            String agreeStatusTender = "";

                            if (session.getAttribute("userTypeId") != null) {
                                suserTypeId1 = Byte.parseByte(session.getAttribute("userTypeId").toString());
                            }
                            if (session.getAttribute("userId") != null) {
                                userId = Integer.parseInt(session.getAttribute("userId").toString());
                            }
                            if (request.getParameter("tenderId") != null) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }
                            if(request.getParameter("negId")!=null){
                                negId = Integer.parseInt(request.getParameter("negId"));
                                agreeStatus = negotiationdtbean.getOfficerAgreeStatus(negId);
                                agreeStatusTender = negotiationdtbean.getTenderAgreeStatus(negId);
                            }
                %>
                <div class="dashboard_div">
                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1">
                            Negotiation
                            <span style="float:right;">
                            <a href="<%=request.getContextPath()%>/officer/NegotiationProcess.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back to Dashboard</a>
                           </span>
                        </div>
                        <%
                            // Variable tenderId is defined by u on ur current page.
                            pageContext.setAttribute("tenderId", tenderId);
                        %>
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <div>&nbsp;</div>
                        <%
                            String tenderRefNo = pageContext.getAttribute("tenderRefNo").toString();
                        %>
                          <jsp:include page="officerTabPanel.jsp" >
                             <jsp:param name="tab" value="7" />
                        </jsp:include>
                        
                         <div class="tabPanelArea_1">
                              <%-- Start: Common Evaluation Table --%>
                          <%@include file="/officer/EvalCommCommon.jsp" %>
                        <%-- End: Common Evaluation Table --%>
                        <div>&nbsp;</div>
                       <%  pageContext.setAttribute("TSCtab", "7"); %>
                        <%@include file="../resources/common/AfterLoginTSC.jsp" %>
                        <div>&nbsp;</div>
                        <div class="tabPanelArea_1">
                            <form name="frmnegotiation" id="idfrmnegotiation" method="post" action="<%=request.getContextPath()%>/NegotiationSrBean">
                            <input type="hidden" name="action" id="idaction" value="closeNegotiation" />
                            <input type="hidden" name="tenderIdName" id="idtenderIdName" value="<%=tenderId%>" />
                            <input type="hidden" name="negIdName" id="idnegIdName" value="<%=negId%>" />
                            <input type="hidden" name="tenRefNo" value="<%=tenderRefNo%>" />
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <td width="16%" class="t-align-left ff">Status :</td>
                                    <td width="84%" class="t-align-left">
                                        <%if("".equalsIgnoreCase(agreeStatus) && !"Reject".equalsIgnoreCase(agreeStatusTender)){%>
                                            <label><input type="radio" name="nameStatus" id="idStatus1" value="Successful" checked="checked" />&nbsp;Successful&nbsp;&nbsp;</label>
                                            <label><input type="radio" name="nameStatus" id="idStatus2" value="Failed" />&nbsp;Failed</label>
                                            <span id="SPReply" class="reqF_1"></span>
                                    <%}else if("Accept".equalsIgnoreCase(agreeStatus)  && !"Reject".equalsIgnoreCase(agreeStatusTender)){%>
                                    <label>
                                        Successful
                                        <input type="hidden" name="nameStatus" value="Successful" />
                                    </label>
                                    <%}else{%>
                                    <label>
                                        Fail
                                        <input type="hidden" name="nameStatus" value="Failed" />
                                    </label>
                                    <%}%>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="16%" class="t-align-left ff">Comments : <span class="mandatory">*</span></td>
                                    <td width="84%" class="t-align-left">
                                        <label>
                                            <textarea name="txtRemarks" rows="3" class="formTxtBox_1" id="idRemarks" style="width:675px;"></textarea>
                                            <span id="SPReply" class="reqF_1"></span>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="t-align-center t_space">
                                              <label class="formBtn_1">
                                                    <input name="btnSubmit" type="submit" value="Close" />
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            </form>
                        </div>
                        <%@include file="../resources/common/Bottom.jsp" %>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

