<%--
    Document   : TestFormTable
    Created on : Jan 09, 2011, 12:00:21 PM
    Author     : yanki
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderForms"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<%@page import="java.util.ListIterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderCells"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderColumns"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTables"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="procurementNatureBean" class="com.cptu.egp.eps.web.servicebean.OfficerTabSrBean" scope="page"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Test Form</title>
        <%String contextPath = request.getContextPath();%>

        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />

        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/FormulaCalculation.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/Add.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/deployJava.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/GetHash.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="<%=contextPath%>/resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="<%=contextPath%>/resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="<%=contextPath%>/resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="<%=contextPath%>/resources/js/datepicker/js/lang/en.js"></script>


        <jsp:useBean id="frmView"  class="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean" />
        <jsp:useBean id="frmDtl"  class="com.cptu.egp.eps.web.servicebean.TenderFormSrBean" />
        <%--<jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />--%>
    <body>
        <div class="mainDiv">

            <div class="dashboard_div">
                <%
                            if (!"true".equals(request.getParameter("isPDF"))) {
                %>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%}%>
                <style>
                    .tableView_1 .ff {text-align: left;}
                </style>
                <div class="fixDiv">
                    <script type="text/javascript">
                        var verified = true;
                        var combo_val = [];
                        var combo_val_tableid = [];
                        var combo_val_tableIndex = [];
                        var SignerAPI;
                        $(function() {
                            SignerAPI = document.getElementById('SignerAPI'); // get access to the signer applet.
                        });
                        function GetCal(txtname,controlname,tableId,obj,tableIndex)
                        {
                            new Calendar({
                                inputField: txtname,
                                trigger: controlname,
                                showTime: 24,
                                dateFormat:"%d-%b-%Y",
                                onSelect: function() {
                                    var date = Calendar.intToDate(this.selection.get());
                                    LEFT_CAL.args.min = date;
                                    LEFT_CAL.redraw();
                                    this.hide();
                                    document.getElementById(txtname).focus();
                                    CheckDate(tableId,obj,txtname,txtname,tableIndex);
                                }
                            });

                            var LEFT_CAL = Calendar.setup({
                                weekNumbers: false
                            })

                        }
                    </script>
                    <!--Middle Content Table Start-->
                    <%
                                int userId = 0;
                                if (session.getAttribute("userId") != null) {
                                    if (!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                                    }
                                }

                                int formId = 0;
                                if (request.getParameter("formId") != null) {
                                    formId = Integer.parseInt(request.getParameter("formId"));
                                }
                                int tenderId = 0;
                                if (request.getParameter("tenderId") != null) {
                                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                                }
                                int sectionId = 0;
                                if (request.getParameter("sectionId") != null) {
                                    sectionId = Integer.parseInt(request.getParameter("sectionId"));
                                }
                                int pkgOrLotId = -1;
                                if (request.getParameter("porlId") != null) {
                                    pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                                }

                                String action = "";
                                int tableCount = 0;
                                tableCount = frmView.getNoOfTablesInForm(formId);
                                pageContext.setAttribute("tenderId", tenderId);
                                if ("".equalsIgnoreCase(action)) {
                    %>
                    <script>
                        var gblCnt =0;
                        var isMultiTable = false;
                        var arrCompType = new Array(<%=tableCount%>); //Stores the Array of the ComponentTypes of the Table Fields
                        var arrCellData = new Array(<%=tableCount%>);//Stores the Array of the CellData of the Table Fields
                        var arrRow = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using normal index
                        var arrCol = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using normal index
                        var arrTableAdded = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables
                        var arrTableFormula = new Array(<%=tableCount%>); //Stores the Array of the table formula
                        var arrFormulaFor = new Array(<%=tableCount%>); //Stores the Array of the fields where formula is applicable
                        var arrIds = new Array(<%=tableCount%>); //Stores the Array of the ids of the cols of the formula
                        var brokenFormulaIds = new Array();
                        var arrColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table
                        var arrStaticColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table which r only txt
                        var arrRowsKey = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using directly ths tableid as a key
                        var arrColsKey = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using directly ths tableid as a key
                        var arrTableAddedKey = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables using directly ths tableid as a key
                        var arrDataTypesforCell = new Array(<%=tableCount%>);
                        var arrColTotalIds = new Array(<%=tableCount%>);
                        var arrColTotalWordsIds = new Array(<%=tableCount%>);	// added for total in words.
                        var arrColOriValIds = new Array(<%=tableCount%>);	// added for original value to keep.
                        var isColTotalforTable = new Array(<%=tableCount%>);
                        for(var i=0;i<isColTotalforTable.length;i++)
                            isColTotalforTable[i]=0;
                        var arrForLabelDisp = new Array(<%=tableCount%>); //Stores the Array of the col ids of the which Fillby=3,Datatype=2 table
                        var arrColFillBy=new Array(<%=tableCount%>); // Stores the Array of Column having fill by Tenderer/PE/Auto
                    </script>
                    <%
                                }
                    %>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">
                            <td class="contentArea_1">
                                <%
                                            String frmName = "";
                                            StringBuffer frmHeader = new StringBuffer();
                                            StringBuffer frmFooter = new StringBuffer();
                                            String isMultipleFormFeeling = "";
                                            String isEncryption = "";
                                            String isPriceBid = "";
                                            int iPkgLotId = 0;

                                            java.util.List<com.cptu.egp.eps.model.table.TblTenderForms> frm = frmDtl.getFormDetail(formId);
                                            if (frm != null) {
                                                if (frm.size() > 0) {
                                                    frmName = frm.get(0).getFormName();
                                                    frmHeader.append(frm.get(0).getFormHeader());
                                                    frmFooter.append(frm.get(0).getFormFooter());
                                                    isMultipleFormFeeling = frm.get(0).getIsMultipleFilling();
                                                    isEncryption = frm.get(0).getIsEncryption();
                                                    isPriceBid = frm.get(0).getIsPriceBid();
                                                    iPkgLotId = frm.get(0).getPkgLotId();
                                                }
                                                frm = null;
                                            }
                                %>
                                <div class="t_space">
                                    <%if (!"true".equals(request.getParameter("isPDF"))) {%>
                                    <div class="pageHead_1" id="divFormName">
                                        <%=frmName%>
                                        <%}%>
                                        <%if (pkgOrLotId == -1) {%>
                                        <%
                                            if (!"true".equals(request.getParameter("isPDF"))) {
                                        %>
                                        <span style="float: right; text-align: right;">
                                            <a class="action-button-goback" href="TenderDocPrep.jsp?tenderId=<%= tenderId%>" title="Tender/Proposal Document">Tender Document</a>
                                        </span>
                                        <%}%>
                                        <%} else {%>
                                        <%
                                            if (!"true".equals(request.getParameter("isPDF"))) {
                                        %>
                                        <span style="float: right; text-align: right;">
                                            <a class="action-button-goback" href="TenderDocPrep.jsp?tenderId=<%= tenderId%>&porlId=<%= pkgOrLotId%>" title="Tender/Proposal Document">Tender Document</a>
                                        </span>
                                        <%}%>
                                        <%}%>
                                    </div>

                                    <div <%if ("true".equals(request.getParameter("isPDF"))) {%>style="display: none" <%}%>
                                                                                                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                                </div>

                                <%
                                            String s_lable = "Lot";
                                            List<TblTenderDetails> tblTenderDetail = procurementNatureBean.getProcurementNature(tenderId);
                                            if ("Goods".equalsIgnoreCase(tblTenderDetail.get(0).getProcurementNature()) || "Works".equalsIgnoreCase(tblTenderDetail.get(0).getProcurementNature())) {
                                                TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                                                String lotDesc = "";
                                                String lotNo = "";
                                                List<TblTenderLotSecurity> lots = new ArrayList<TblTenderLotSecurity>();
                                                if (isTenPackageWise == false) {
                                                    lots = tenderDocumentSrBean.getLotDetailsByLotId(tenderId, pkgOrLotId);
                                                    TblTenderLotSecurity tenderLotSecurity = lots.get(0);
                                                    lotDesc = tenderLotSecurity.getLotDesc();
                                                    lotNo = tenderLotSecurity.getLotNo();
                                                } else {
                                                    if ("yes".equalsIgnoreCase(isPriceBid)) {
                                                        if (pkgOrLotId == -1) {
                                                            pkgOrLotId = iPkgLotId;
                                                        }
                                                        if (pkgOrLotId > 0) {
                                                            lots = tenderDocumentSrBean.getLotDetailsByLotId(tenderId, pkgOrLotId);
                                                            TblTenderLotSecurity tenderLotSecurity = lots.get(0);
                                                            lotDesc = tenderLotSecurity.getLotDesc();
                                                            lotNo = tenderLotSecurity.getLotNo();
                                                        } else {
                                                            lotDesc = tblTenderDetail.get(0).getPackageDescription();
                                                            lotNo = tblTenderDetail.get(0).getPackageNo();
                                                        }
                                                    } else {
                                                        if (tblTenderDetail.get(0).getDocAvlMethod().equalsIgnoreCase("package")) {
                                                            s_lable = "Package";
                                                            lotDesc = tblTenderDetail.get(0).getPackageDescription();
                                                            lotNo = tblTenderDetail.get(0).getPackageNo();
                                                        } else {
                                                            lots = tenderDocumentSrBean.getLotDetailsByLotId(tenderId, pkgOrLotId);
                                                            TblTenderLotSecurity tenderLotSecurity = lots.get(0);
                                                            lotDesc = tenderLotSecurity.getLotDesc();
                                                            lotNo = tenderLotSecurity.getLotNo();
                                                        }
                                                    }
                                                }
                                                String lotPdf_cnt = "0";
                                                if (request.getParameter("lotDetail") != null) {
                                                    lotPdf_cnt = request.getParameter("lotDetail");
                                                }
                                                if (lotPdf_cnt.equalsIgnoreCase("0")) {

                                %>


                                <div class="tableHead_1 t_space"><%=s_lable%> Detail</div>
                                <table width="100%" cellspacing="5" cellpadding="0" border="0" class="tableView_1  loginBox">
                                    <tr>
                                        <td width="13%" class="ff t-align-left"><%=s_lable%> No. : </td>
                                        <td  class="t-align-left" ><%=lotNo%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff t-align-left"><%=s_lable%> Description : </td>
                                        <td class="t-align-left"><%=lotDesc%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff"></td>
                                        <td ></td>
                                    </tr>
                                </table>
                                <%}%>
                                <%
                                            }%>
                                <%if ("true".equals(request.getParameter("isPDF"))) {%>
                                <div class="pageHead_1 t_space" id="divFormName">
                                    <%=frmName%>
                                </div>
                                <%}%>
                                <% if (frmHeader != null && frmHeader.length() > 0 && !"".equals(frmHeader.toString().trim())) {%>
                                <table width="100%" cellspacing="10" class="tableView_1 t_space loginBox">
                                    <tr>
                                        <td class="ff" style="line-height: 1.75"><%=frmHeader%></td>
                                    </tr>
                                </table>
                                <%}%>
                                <table border="0" cellspacing="10" cellpadding="0" class="tableView_1 t_space loginBox t_space" width="100%">
                                    <tr>
                                        <td>
                                            <input type="hidden" name="hdnTenderId" id="hdnTenderId" value="<%= tenderId%>">
                                            <input type="hidden" name="sectionId" id="sectionId" value="<%= sectionId%>">
                                            <input type="hidden" name="formId" id="formId" value="<%= formId%>">
                                            <%
                                                        int tableId = 0;
                                                        int tblCnt1 = 0;
                                                        short cols = 0;
                                                        short rows = 0;
                                                        String tableName = "";
                                                        String tableHeader = "";
                                                        String tableFooter = "";
                                                        String isMultiTable = "";

                                                        List<TblTenderTables> tblTenderTables = frmView.getTenderTables(formId);
                                                        for (TblTenderTables tbl : tblTenderTables) {
                                                            tableId = tbl.getTenderTableId();

                                                            java.util.List<com.cptu.egp.eps.model.table.TblTenderTables> tblInfo = frmView.getTenderTablesDetail(tbl.getTenderTableId());
                                                            if (tblInfo != null) {
                                                                if (tblInfo.size() >= 0) {
                                                                    tableName = tblInfo.get(0).getTableName();
                                                                    tableHeader = tblInfo.get(0).getTableHeader();
                                                                    tableFooter = tblInfo.get(0).getTableFooter();
                                                                    cols = tblInfo.get(0).getNoOfCols();
                                                                    rows = tblInfo.get(0).getNoOfRows();
                                                                    isMultiTable = tblInfo.get(0).getIsMultipleFilling();
                                                                }
                                                                tblInfo = null;
                                                            }

                                                            cols = frmView.getNoOfColsInTable(tbl.getTenderTableId());
                                                            rows = frmView.getNoOfRowsInTable(tbl.getTenderTableId(), (short) 1);
                                            %>
                                            <script>
                                                chkBidTableId.push(<%=tableId%>);
                                                arrBidCount.push(1);
                                                // Setting TableId in Array
                                                arr[<%=tblCnt1%>]=<%=tableId%>;
                                                // Setting TableIdwise No of Rows in Array
                                                arrRow[<%=tblCnt1%>]=<%=rows%>;
                                                //alert('tablecnt : <%=tblCnt1%>')
                                                //alert('<%=rows%>');
                                                arrRowsKey[<%=tableId%>]=<%=rows%>;
                                                // Setting TableIdwise No of Cols in Array
                                                arrCol[<%=tblCnt1%>]=<%=cols%>;
                                                arrColsKey[<%=tableId%>]=<%=cols%>;
                                                // Setting TableIdwise No of Tables in Added
                                                arrTableAdded[<%=tblCnt1%>]=<%=1%>;
                                                arrTableAddedKey[<%=tableId%>]=<%=1%>;
                                                arrColTotalIds[<%=tblCnt1%>] = new Array(<%=cols%>);
                                                arrColTotalWordsIds[<%=tblCnt1%>] = new Array(<%=cols%>);
                                                arrColOriValIds[<%=tblCnt1%>] = new Array(<%=cols%>);
                                                for(var i=0;i<arrColTotalIds[<%=tblCnt1%>].length;i++){
                                                    arrColTotalIds[<%=tblCnt1%>][i] = 0;
                                                    arrColTotalWordsIds[<%=tblCnt1%>][i] = 0;
                                                    arrColOriValIds[<%=tblCnt1%>][i] = 0;
                                                }
                                            </script>

                                            <% // if (tableName != null && !"".equals(tableName.trim())) {%>
                                            <table width="100%" cellspacing="10" class="tableView_1 loginBox">
                                                <tr>
                                                    <td width="100" class="ff">Table Name : </td>
                                                    <td><%=tableName%></td>
                                                    <td class="t-align-right">
                                                        <%
                                                             if ("yes".equalsIgnoreCase(isMultiTable)) {
                                                        %>
                                                        <script>
                                                            isMultiTable = true;
                                                        </script>

                                                        <%
                                                                                                                         if (!"true".equals(request.getParameter("isPDF"))) {
                                                        %>
                                                        <label class="bidFormBtn_1" style="float: right" id="lblAddTable<%=tableId%>">
                                                            <input type="button" name="btn<%=tableId%>" id="bttn<%=tableId%>" value="Add Row" onClick="AddTable(this.form,<%=tableId%>,this)" />
                                                        </label>
                                                        <%}%>
                                                        <%
                                                             }
                                                        %>
                                                    </td>
                                                </tr>
                                            </table>
                                            <%//}%>
                                            <% if (tableHeader != null && !"".equals(tableHeader.trim())) {%>
                                            <table width="100%" cellspacing="10" class="tableView_1 loginBox">
                                                <tr>
                                                   <td class="ff" style="line-height: 1.75"><%=tableHeader%></td>
                                                </tr>
                                            </table>
                                            <%}%>
                                            <jsp:include page="TestTenderFormTable.jsp" flush="true">
                                                <jsp:param name="tableId" value="<%=tableId%>" />
                                                <jsp:param name="cols" value="<%=cols%>" />
                                                <jsp:param name="rows" value="<%=rows%>" />
                                                <jsp:param name="TableIndex" value="<%=tblCnt1%>" />
                                                <jsp:param name="isMultiTable" value="<%=isMultiTable%>" />
                                                <jsp:param name="tenderId" value="<%=tenderId%>" />
                                                <jsp:param name="userId" value="<%=userId%>" />
                                                <jsp:param name="formId" value="<%=formId%>" />
                                            </jsp:include>
                                            <script>
                                                var totalInWordAt = -1;
                                                try{
                                                    totalInWordAt = TotalWordColId
                                                    if(totalInWordAt < 0){
                                                        totalInWordAt = -1;
                                                    }
                                                }catch(err){
                                                    totalInWordAt = -1;
                                                }
                                            </script>
                                            <%if (tblCnt1 >= 0) {%>
                                            <script>
                                                if(arrStaticColIds[<%=tblCnt1%>].length > 0)
                                                {
                                                    for(var j=0;j<arr.length;j++)
                                                    {
                                                        LoopCounter++;
                                                    }
                                                }
                                                breakFormulas(arrDataTypesforCell);
                                                checkForFunctions();
                                            </script>
                                            <%}%>
                                            <% if (tableFooter != null && !"".equals(tableFooter.trim())) {%>
                                            <br/>
                                            <table width="100%" cellspacing="10" class="tableView_1 loginBox">
                                                <tr>
                                                    <td class="ff" style="line-height: 1.75"><%=tableFooter%></td>
                                                </tr>
                                            </table>
                                            <%}%>
                                            <div class="t_space">
                                        </div>
                                            <%                        tblCnt1++;
                                                        }
                                            %>
                                            <script>
                                                breakFormulas(arrDataTypesforCell);
                                                checkForFunctions();
                                            </script>
                                        </td>
                                    </tr>
                                </table>
                                <% if (frmFooter != null && frmFooter.length() > 0 && !"".equals(frmFooter.toString().trim())) {%>
                                <table width="100%" cellspacing="10" class="tableView_1 loginBox t_space">
                                    <tr>
                                        <td class="ff" style="line-height: 1.75"><%=frmFooter%></td>
                                    </tr>
                                </table>
                                <%}%>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <%
                        if (!"true".equals(request.getParameter("isPDF"))) {
            %>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <%}%>
        </div>
    </div>
</body>
<script type="text/javascript">
    var headSel_Obj = document.getElementById("headTabTender");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
    for(var cnt=0;cnt<combo_val.length;cnt++)
    {
        changeTextVal(document.getElementById(combo_val[cnt]),combo_val_tableid[cnt],0);
    }
</script>
</html>
<%
            if (tblTenderTables != null) {
                tblTenderTables = null;
            }
            if (frmView != null) {
                frmView = null;
            }
            if (frmDtl != null) {
                frmDtl = null;
            }
%>
