<%-- 
    Document   : UploadAmendDoc
    Created on : Jan 5, 2011, 6:29:32 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Amendment Reference Document</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script type="text/javascript">
            <%--$(function() {--%>
            $(document).ready(function() {
                /*Validation for text fields*/
                 $("#frmUploadDoc").validate({
                
                    rules: {
                        uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:50}
                    },
                    messages: {
                        uploadDocFile: { required: "<div class='reqF_1'>Please select Document</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please enter Description</div>",
                                        maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>"}
                    }
                });
            });<%--
            });--%>
                $(function() {
                $('#frmUploadDoc').submit(function() {
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#btnUpload').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="contentArea_1">
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <%
                        int tenderId=0;
                                int corrigendumId=0;
                        int lotId = 0;
                         if (request.getParameter("tenderId") != null){
                         tenderId = Integer.parseInt(request.getParameter("tenderId"));
                         }
                        if(request.getParameter("corrigendumId") != null){
                          corrigendumId = Integer.parseInt(request.getParameter("corrigendumId"));
                        }if (request.getParameter("lotId") != null) {
                        lotId = Integer.parseInt(request.getParameter("lotId"));
                        }
%>
<div class="pageHead_1">Amendment Reference Document
    <span class="t-align-right" style="float: right;"><a class="action-button-goback" href="Amendment.jsp?tenderid=<%=request.getParameter("tenderId")%>">Go back to Dashboard</a></span></div>

<div>&nbsp;</div>

         <form  id="frmUploadDoc"  method="post" action="<%=request.getContextPath()%>/ServletCorriDocUpload" enctype="multipart/form-data" name="frmUploadDoc">
                   <input type="hidden" name="tenderId" value="<%= tenderId%>" />
                <input type="hidden" name="corrigendumId" value="<%= corrigendumId%>" />
                 <input type="hidden" name="lotId" value="<%= lotId%>" />
            <%   pageContext.setAttribute("tenderId", tenderId); %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <div class="t_space">
                    <%
                                if (request.getParameter("fq") != null) {
                    %>

                    <div class="responseMsg errorMsg " ><%=request.getParameter("fq")%></div>
                    <%
                                }
                                if (request.getParameter("fs") != null) {
                    %>

                    <div class="responseMsg errorMsg t_space t_space">

                        Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                    </div>
                    <%
                                }
                    %>
                </div>
                    <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1 t_space">
                                    <tr>

                                        <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr>
                                        <td width="10%" class="ff">Document   : <span>*</span></td>
                                        <td width="90%"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:250px; background:none;"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Description   : <span>*</span></td>
                                        <td>
                                            <input name="documentBrief" id="documentBrief" type="text" class="formTxtBox_1" style="width:245px; background:none;"/>
                                        </td>
                                    </tr>
                                    <tr><td>&nbsp;</td>
                                        <td align="left"><label class="formBtn_1">
                                                <input type="submit" name="upload" id="btnUpload" value="Upload"/></label>
                                        </td>
                                    </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="100%"  class="t-align-left">Instructions</th>
                        </tr>
                        <tr>
                            <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("officer");%>
                            <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/></td>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                        </tr>
                    </table>
                </form>
           
                                 <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                     <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="23%">File Name</th>
                        <th class="t-align-center" width="32%">File Description</th>
                        <th class="t-align-center" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-center" width="18%">Action</th>
                    </tr>
                    <%
                              
                               String tender = request.getParameter("tenderId");
                               int docCnt = 0;
                               TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
/*Listing of data*/             
                               for (SPTenderCommonData sptcd : tenderCS.returndata("docTenderInfo", request.getParameter("corrigendumId"), lotId+"")) {
                                   docCnt++;
                    %>
                    <tr>
                        
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3())/1024)%></td>
                         <td class="t-align-center">
                             <a href="<%=request.getContextPath()%>/ServletCorriDocUpload?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tender%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             &nbsp;
                             <a href="<%=request.getContextPath()%>/ServletCorriDocUpload?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&tenderId=<%=tender%>&id=<%=request.getParameter("corrigendumId")%>&lotId=<%=request.getParameter("lotId")%>&funName=remove" ><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                         </td>
                    </tr>
                          <%   if(sptcd!=null){
                        sptcd = null;
                    }
                    }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
            </table>
            </div>

            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->


        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>

</html>
