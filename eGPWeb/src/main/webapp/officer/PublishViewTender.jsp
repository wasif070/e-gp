<%--
    Document   : ViewTender
    Created on : Nov 15, 2010, 3:18:02 PM
    Author     : rishita,Swati
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalServiceWeightage"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<%@page import="com.cptu.egp.eps.web.utility.SHA1HashEncryption"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");

                    int lotno = 0;
                    String id = "";
                    if (!request.getParameter("id").equals("")) {
                        id = request.getParameter("id");
                    }
                    //EvalSerCertiSrBean evalSerCertiSrBean = new EvalSerCertiSrBean();
                    Object userId = session.getAttribute("userId");
                    if(session.getAttribute("userId") != null){
                        userId = session.getAttribute("userId");
                        tenderSrBean.setLogUserId(userId.toString());
                        tenderSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                        //evalSerCertiSrBean.setLogUserId(userId.toString());
                    }
                    Object usrTypeId = session.getAttribute("userTypeId");
        %>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Publish Notice Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <!--jalert -->
<!--        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>-->
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script src="../resources/js/browser.scripts.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmPublishViewTComments").validate({
                    rules: {
                        comments: {required: true}
                    },
                    messages: {
                        comments: { required: "<div class='reqF_1'>Please enter Comments.</div>"}
                    }
                });

            });
             function printPage()
                {
                    $("#frmPublishViewForm").submit();
                }
        </script>
        <script  type="text/javascript">
            $(function() {
                $('#watchList').click(function() {
                    $.post("<%=request.getContextPath()%>/WatchListServlet", {type: "Tender",tenderId: <%=id%>,userId: <%=userId%>,funName: 'Add'},  function(j){
                        if(j.toString() != "0")
                        {
                            jAlert("Added to your WatchList","Success");
                            $("#watchId").val(j.toString());
                            $("#addCell").hide();
                            $("#removeCell").show();
                        }
                        else
                        {
                            jAlert("Can not be added to WatchList","Failure");
                        }

                    });
                });
            });
        </script>
        <script  type="text/javascript">
            $(function() {
                $('#watchListRem').click(function() {
                    $.post("<%=request.getContextPath()%>/WatchListServlet", {type: "Tender",watchId: $("#watchId").val(),funName: 'Remove'},  function(j){
                        if(j.toString() == "true")
                        {
                            jAlert("Removed from your WatchList","Success");
                            $("#removeCell").hide();
                            $("#addCell").show();
                        }
                        else
                        {
                            jAlert("Can not be removed from WatchList","Failure");
                        }
                    });
                });
            });
        </script>
    </head>
    <body>
        <%
                    Object objUsrId = session.getAttribute("userId");
                    int usId = 0;
                    if (objUsrId != null) {
                         usId = Integer.parseInt(objUsrId.toString());
                    }
                    TenderCommonService objTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
         
                      if ("Publish".equals(request.getParameter("hdnbutton"))) {
                        //lines added by Swati Patel for pdf reports
                        String reqURL = request.getRequestURL().toString() ;
                        String reqQuery = request.getQueryString() ;
                        String folderName = pdfConstant.TENDERNOTICE;

                        List<TblTenderDetails> details = tenderSrBean.getTenderDetails(Integer.parseInt(id));


                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        Calendar calendar = java.util.Calendar.getInstance();
                        String subDate = dateFormat.format(details.get(0).getSubmissionDt());
                        String subDate1 = dateFormat.format(details.get(0).getSubmissionDt());
                        calendar.setTime(dateFormat.parse(subDate));
                        System.out.println("details.get(0).getTenderValDays()-------->"+details.get(0).getTenderValDays());
                        calendar.add(Calendar.DATE, +details.get(0).getTenderValDays());
                        Date tenderValidityDt = calendar.getTime();

                        calendar.setTime(dateFormat.parse(subDate1));
                        calendar.add(Calendar.DATE, +details.get(0).getTenderSecurityDays());

                        Date tenderSecurityDt = calendar.getTime();
                        System.out.println("tenderValidityDt------------>"+tenderValidityDt);
                        List<SPTenderCommonData> listPeName;
                        WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                        List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData("CheckPE", userId.toString(), "");
                        if (editdata.size() > 0) 
                            tenderSrBean.updateStatus(Integer.parseInt(id),reqURL,reqQuery,folderName,tenderValidityDt,tenderSecurityDt,request.getSession().getAttribute("userName").toString());
                        else{
                            listPeName = objTenderCommonService.returndata("getSentToUserName", String.valueOf(id), "getPEName");
                            tenderSrBean.updateStatus(Integer.parseInt(id),reqURL,reqQuery,folderName,tenderValidityDt,tenderSecurityDt,listPeName.get(0).getFieldName1());
                        }
                        String remarks = "";
                        if(request.getParameter("comments") != null){
                            remarks = request.getParameter("comments");
                        }


                        String eSignature = SHA1HashEncryption.encodeStringSHA1(remarks);
                        tenderSrBean.insertInToTenderAuditTrail(Integer.parseInt(id),"Approved",usId,eSignature,remarks,request.getParameter("openingDate"));
                        response.sendRedirect("TenderPdfGenerate.jsp?tenderid="+ id);
                    }
                    String tenderStatus = "";
        %>
        <script type="text/javascript">
            $(function() {
                $('#frmPublishViewTComments').submit(function() {
                    if($('#frmPublishViewTComments').valid()){
                        if($('#Publish')!=null){
                            $('#Publish').attr("disabled","true");
                            $('#hdnbutton').val("Publish");
                        }
                    }
                });
            });


        </script>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%
                            boolean isLoggedIn = false;

                            if (objUsrId != null) {
                                isLoggedIn = true;
                            }

                             String isPDF= "abc";
                            if(request.getParameter("isPDF") != null)
                                    isPDF= request.getParameter("isPDF");

                          if(! (isPDF.equalsIgnoreCase("true")) ){
                            if (isLoggedIn) {
                %>

                <%} else {%>
                <%}
            }//end of isPDF
            %>
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr>
                        <td class="contentArea_1">
                            <div class="pageHead_1">Publish Notice Details
                                <%  if(! (isPDF.equalsIgnoreCase("true")) ){  %>
                                <span style="float:right;"><a href="Notice.jsp?tenderid=<%=id %>" class="action-button-goback">Go Back to Dashboard</a></span>
                                <%  } %>
                            </div>
                <%
                            for (CommonTenderDetails commonTenderDetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "tender")) {
                                List<Object[]> obj = tenderSrBean.getConfiForTender(Integer.parseInt(id));
                                tenderStatus = commonTenderDetails.getTenderStatus();
                %>
                <form method="POST" id="frmPublishViewForm" action="<%=request.getContextPath()%>/PDFServlet">
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 " width="100%">
                        <tr>
                            <td width="25%" class="ff">Ministry :
                                <input type="hidden" name="reportName" value="Tender_Detail"/>
                                <input type="hidden" name="reportPage" value="TenderDtlNoticeRpt.jsp?<%=request.getQueryString()%>"/></td>
                            <td width="25%"><%=commonTenderDetails.getMinistry()%></td>
                            <td width="25%" class="ff">Division :</td>
                            <td width="25%"><%=commonTenderDetails.getDivision()%></td>
                        </tr>
                        <tr><%
                                                        if (commonTenderDetails.getAgency() != null) {
                                                            if (!commonTenderDetails.getAgency().trim().equalsIgnoreCase("")) {
                            %>
                            <td class="ff">Organization :</td>
                            <td><%=commonTenderDetails.getAgency()%></td>
                            <% }
                                                            }%>
                            <td class="ff">Procuring Entity Name :</td>
                            <td><%=commonTenderDetails.getPeOfficeName()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Procuring Entity Code :</td>
                            <td><%=commonTenderDetails.getPeCode()%></td>
                            <td class="ff">Procuring Entity Dzongkhag / District :</td>
                            <td><%=commonTenderDetails.getPeDistrict()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Procurement Category :</td>
                            <td><%=commonTenderDetails.getProcurementNature()%></td>
                            <td class="ff">Procurement Type :</td>
                            <td><%=commonTenderDetails.getProcurementType()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Event Type  :</td>
                            <td><% if (commonTenderDetails.getEventType() != null) {%><%=commonTenderDetails.getEventType().toUpperCase() %><% }%></td>
                            <% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) {%> <td class="ff">Invitation for :</td>
                            <td><% if (commonTenderDetails.getInvitationFor() != null) {%><%=commonTenderDetails.getInvitationFor()%><% }%></td> <% }%>
                        </tr><% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {
                                                            String msg = "";
                                                            if (commonTenderDetails.getEventType() != null) {
                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {
                                                                    msg = "Request for expression of interest for selection of";
                                                                } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {
                                                                    msg = "RFP for selection of";
                                                                }
                                                            }
                        %>
                        <tr><% if (!msg.equals("")) {%>
                            <td class="ff"><%=msg%></td>
                            <td><label><%=commonTenderDetails.getReoiRfpFor()%></label><%--<input name="expInterestSel" readonly type="text" class="formTxtBox_1" id="txtexpInterestSel" style="width:200px;" value="<%=commonTenderDetails.getReoiRfpFor()%>" />--%></td><% }%>
                            <td class="ff"></td>
                            <td></td>
                        </tr><% }%>
                        <tr><%
                                                        if (commonTenderDetails.getContractType() != null) {
                                                            if (!commonTenderDetails.getContractType().equals("")) {
                            %>
                            <td class="ff">Contract Type : </td>
                            <td><%=commonTenderDetails.getContractType()%></td>
                            <% }
                                                            }
                                                            String msgTender = "";
                                                            if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ") || commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().contains("TSTM")) {
                                                                msgTender = "Invitation Reference No.";
                                                            } else if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {
                                                                msgTender = "REOI No.";
                                                            } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {
                                                                msgTender = "RFP No.";
                                                            }else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {
                                                                msgTender = "RFA No.";
                                                            }
                            %>
                            <td class="ff"><%=msgTender%><% if (!msgTender.equals("")) {%> :</td>
                            <td><%=commonTenderDetails.getReoiRfpRefNo()%></td><% }%>
                        </tr>
                    </table>
                    <div class="tableHead_22 ">Key Information and Funding Information :</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 " width="100%">
                        <tr>
                            <td width="25%" class="ff">Procurement Method : </td>
                            <td width="25%">
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("RFQ")){%><%="Limited Enquiry Method"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("RFQL")){%><%="Request For Quotation Lump Sum (RFQL)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("RFQU")){%><%="Request For Quotation Unit Rate (RFQU)"%><%}%> 
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OTM")){%><%="Open Tendering Method (OTM)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LTM")){%><%="Limited Tendering Method (LTM)"%><%}%>
                               <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("TSTM")){%><%="Two Stage Tendering Method (TSTM)"%><%}%> 
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("QCBS")){%><%="Quality Cost Based Selection (QCBS)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LCS")){%><%="Least Cost Selection (LCS)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SFB")){%><%="Selection under a Fixed Budget (SFB)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("DC")){%><%="Design Contest (DC)"%><%}%> 
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SBCQ")){%><%="Selection Based on Consultants Qualification (SBCQ)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SSS")){%><%="Single Source Selection (SSS)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("IC")){%><%="Selection of Independent Individual Consultant (IC)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("CSO")){%><%="Community Service Organisation (CSO)"%><%}%>
                                <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("DPM")){%><%="Direct Contracting Method (DCM)"%><%}%>
                               <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("OSTETM")){%><%="One stage Two Envelopes Tendering Method (OSTETM)"%><%}%>
                                 <% if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("FC")){%><%="Framework Contract (FC)"%><%}%>
                            </td>
                            <td width="25%" class="ff">Budget Type :</td>
                            <td width="25%"><%=commonTenderDetails.getBudgetType()%></td>
                        </tr>
                        <tr>
                            <td width="25%" class="ff">Source of Funds :</td>
                            <td width="25%"><%=commonTenderDetails.getSourceOfFund()%></td>
                            <% if (commonTenderDetails.getDevPartners() != null) {%>
                            <td width="25%" class="ff">Development Partner :</td>
                            <td width="25%"><%=commonTenderDetails.getDevPartners()%></td>
                            <% }%>
                        </tr>
                        <% List<Object> allocatedCost=tenderSrBean.getAllocatedCost(Integer.parseInt(id));
                             if (!allocatedCost.isEmpty() && !(allocatedCost.get(0).toString().startsWith("0.00"))) {
                            if(commonTenderDetails.getProcurementMethod().equalsIgnoreCase("LTM") || commonTenderDetails.getProcurementMethod().equalsIgnoreCase("SFB")){
                            %>
                        <tr>
                            <td width="25%" class="ff">Allocated Budget Amount in Tk. :</td>
                            <td width="25%"><%=new BigDecimal(allocatedCost.get(0).toString()).setScale(2,0) %></td>
                            <td width="25%" class="ff">&nbsp;</td>
                            <td width="25%">&nbsp;</td>
                        </tr>
                            <% } }%>
                    </table>

                    <div class="tableHead_22 ">Particular Information :</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 " width="100%">
                        <tr>
                            <td width="25%" class="ff">Project Code : </td>
                            <td width="25%"><% if (commonTenderDetails.getProjectCode() != null) {%><%=commonTenderDetails.getProjectCode()%><% } else {%>Not applicable<%}%></td>
                            <td width="25%" class="ff">Project Name : </td>
                            <td width="25%"><% if (commonTenderDetails.getProjectName() != null) {%><%=commonTenderDetails.getProjectName()%><% } else {%>Not applicable<%}%></td>
                        </tr>
                        <tr>
                            <td  width="25%" class="ff"><% if (!(commonTenderDetails.getEventType().equalsIgnoreCase("REOI") || commonTenderDetails.getEventType().equalsIgnoreCase("RFA") || commonTenderDetails.getEventType().equalsIgnoreCase("RFP"))) {%>Tender <% }%>Package No. and Description :</td>
                            <td colspan="3" width="75%"><%=commonTenderDetails.getPackageNo()%><br/>
                            <%=commonTenderDetails.getPackageDescription()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Category : </td>
                            <td colspan="3"><%=commonTenderDetails.getCpvCode()%></td>
                        </tr>
                        <tr>
                            <%
                                                            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("dd/MM/yyyy hh:mm");
                                                            String msgpublication = "";
                                                            if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {
                                                                msgpublication = "Scheduled Tender/Proposal";
                                                            } else if (!commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {
                                                                msgpublication = commonTenderDetails.getEventType().toUpperCase();
                                                            } else {
                                                                msgpublication = "Scheduled Pre-Qaulification";
                                                            }
                            %>
                            <td class="ff"><%=msgpublication%> Publication<br/>Date and Time :</td>
                            <td class="formStyle_1"><% if (commonTenderDetails.getTenderPubDt() != null) {%><%= DateUtils.gridDateToStrWithoutSec(commonTenderDetails.getTenderPubDt())%><% }%></td>
                            <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification<% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%>Tender/Proposal<% }%> Document last selling /<br/>downloading Date and Time : </td>
                            <td class="formStyle_1"><% if (commonTenderDetails.getDocEndDate() != null) {%><%=DateUtils.gridDateToStrWithoutSec(commonTenderDetails.getDocEndDate())%><% }%></td>
                        </tr>

                        <tr>
                            <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre - Qualification <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>Pre - REOI <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Pre - RFP <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {%>Pre - Application <%} else {%>Pre - Tender <%}%>meeting Start<br/>Date and Time :</td>
                            <td class="formStyle_1"><% if (commonTenderDetails.getPreBidStartDt() != null) {%><%=DateUtils.gridDateToStrWithoutSec(commonTenderDetails.getPreBidStartDt())%><% }%></td>
                            <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre - Qualification <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>Pre - REOI <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Pre - RFP <% } else if (commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {%>Pre - Application <%} else {%>Pre - Tender <%}%>meeting End<br/>Date and Time :</td>
                            <td class="formStyle_1"><% if (commonTenderDetails.getPreBidEndDt() != null) {%><%=DateUtils.gridDateToStrWithoutSec(commonTenderDetails.getPreBidEndDt())%><% }%></td>
                        </tr>
                        <tr>
                                <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification <% }
                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%>Tender/Proposal <% }

                                                                                       if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Proposal <% }
                                                                                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>EOI <% }%> Closing<br/>Date and Time :</td>
                            <td class="formStyle_1"><% if (commonTenderDetails.getSubmissionDt() != null) {%><%=DateUtils.gridDateToStrWithoutSec(commonTenderDetails.getSubmissionDt())%><% }%></td>
                                <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification <% }
                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%>Tender/Proposal <% }

                                                                                       if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%>Proposal <% }
                                                                                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%>EOI <% }%> Opening<br/>Date and Time :</td>

                            <td class="formStyle_1"><% if (commonTenderDetails.getOpeningDt() != null) {%><%=DateUtils.gridDateToStrWithoutSec(commonTenderDetails.getOpeningDt())%>

                                <% }%></td>
                        </tr>
                        <% if(!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")){
                        %>
                        <tr>
                            <td class="ff">Last Date and Time for Tender Security<br />Submission :</td>
                            <td><% if (commonTenderDetails.getSecurityLastDt() != null) {%><%=DateUtils.gridDateToStrWithoutSec(commonTenderDetails.getSecurityLastDt())%><% }%></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <% } %>
                    </table>

                    <div class="tableHead_22 ">Information for Bidder/Consultant :</div>

                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 " width="100%">
                        <%
                                                        if (commonTenderDetails.getEligibilityCriteria() != null) {
                                                            if (!commonTenderDetails.getEligibilityCriteria().equals("")) {
                        %>
                        <tr>
                            <td class="ff" width="25%"><% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) {%>Eligibility of Tenderer <% } else {%>Eligibility of Consultant <% }%> :</td>
                            <td width="75%"><%=commonTenderDetails.getEligibilityCriteria()%></td>
                        </tr>
                        <% }
                                                        }
                                                        if (commonTenderDetails.getTenderBrief() != null) {
                                                            if (!commonTenderDetails.getTenderBrief().equals("")) {
                        %>
                        <tr>
                            <td class="ff" >Brief Description of <% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods")) {%>Goods and Related Service<% } else if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) {%>Works<% } else if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {%>assignment<% }%> :</td>
                            <td><%=commonTenderDetails.getTenderBrief()%></td>
                        </tr>
                        <% }
                                                        }
                                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI") || commonTenderDetails.getEventType().equalsIgnoreCase("RFP") || commonTenderDetails.getEventType().equalsIgnoreCase("RFA")) {

                                                            if (commonTenderDetails.getDeliverables() != null) {
                                                                if (!commonTenderDetails.getDeliverables().equals("")) {
                        %>
                        <tr>
                            <td class="ff">Experience, Resources and<br />
                                delivery capacity required :</td>
                            <td><%=commonTenderDetails.getDeliverables()%></td>
                        </tr>
                        <% }
                                                                                    }
                                                                                    if (commonTenderDetails.getOtherDetails() != null) {
                                                                                        if (!commonTenderDetails.getOtherDetails().equals("")) {
                        %>
                        <tr>
                            <td class="ff">Other details (if  applicable) :</td>
                            <td><%=commonTenderDetails.getOtherDetails()%></td>
                        </tr>
                        <%} }
                                                                                    if (commonTenderDetails.getForeignFirm() != null) {
                                                                                        if (!commonTenderDetails.getForeignFirm().equals("")) {
                        %>
                        <tr>
                            <td class="ff"> Association with
                                foreign  firm :</td>
                            <td><%=commonTenderDetails.getForeignFirm()%></td>
                        </tr>
                        <% } } }
                        %>
                        <tr>
                            <td class="ff"> Evaluation Type :</td>
                            <td><%=commonTenderDetails.getEvalType()%></td>
                        </tr>
                        <%
                                                        if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods")) {%>

                        <tr>
                            <%for (CommonTenderDetails commonTdetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "lot")) {
                                                                                            lotno++;//rishita
                                                                                        }
                            %>
                            <td class="ff"> Document Available :</td>
<!--                            <td><%//commonTenderDetails.getDocAvlMethod()%></td>-->
                            <td><%
                                if(commonTenderDetails.getDocAvlMethod().equalsIgnoreCase("Lot")){
                            %>Lot wise<% }else if(commonTenderDetails.getDocAvlMethod().equalsIgnoreCase("Package")){ %>Package wise<% } %></td>
                        </tr>
                            <% //if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().equalsIgnoreCase("PQ") || commonTenderDetails.getEventType().contains("TSTM")) {
                             }                                                          // if (lotno > 1) {
                            if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")){
                        %>
                        <tr>
                            <td class="ff">Document Fees :</td>
                            <td><%=commonTenderDetails.getDocFeesMethod()%></td>
                        </tr>
                        <% } if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")){
                            if(commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Package wise")){
                                %>
                        <tr>
                            <td class="ff"><% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%>Pre-Qualification Document Price (In Nu.)<% }
                                                        if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER") || commonTenderDetails.getEventType().contains("TSTM")) {%>Tender/Proposal Document Price (In Nu.)<% }
                                                             if(commonTenderDetails.getEventType().equalsIgnoreCase("RFP")){%>RFP Document Price (In Nu.)<%}
                                                                if(commonTenderDetails.getEventType().equalsIgnoreCase("RFQ")){%>RFQ Document Price (In Nu.)<% }
                                                               if(commonTenderDetails.getEventType().equalsIgnoreCase("REOI")){%>REOI Document Price (In Nu.)<% }
                                                               if(commonTenderDetails.getEventType().equalsIgnoreCase("RFA")){%>RFA Document Price (In Nu.)<% }
                            %>
                            :</td>
                            <td><%=commonTenderDetails.getPkgDocFees().setScale(0, 0) %></td>
                        </tr>

                            <!-- Dohatec Start -->
                            <%if(commonTenderDetails.getProcurementType().equalsIgnoreCase("ICT")){%>
                                <tr>
                                    <td class="ff">Equivalent Tender Document Price (In USD) : </td>
                                    <td><%=commonTenderDetails.getPkgDocFeesUSD().setScale(0, 0)%></td>
                                </tr>
                            <%}%>
                            <!-- Dohatec End -->

                            <% } }
                                //if (commonTenderDetails.getEventType().equalsIgnoreCase("Tender/Proposal") || commonTenderDetails.getEventType().equalsIgnoreCase("PQ") || commonTenderDetails.getEventType().contains("TSTM")) {
                               if(!obj.isEmpty()){
                                if(obj.get(0)[0].toString().equalsIgnoreCase("Yes") || obj.get(0)[2].toString().equalsIgnoreCase("Yes")){
        %>
                        <tr>
                            <td class="ff">Mode of Payment :</td>
                            <td><label>Payment through Bank</label>
                                <%//commonTenderDetails.getDocFeesMode()%></td>
                        </tr>
                        <% }}%>

<!--                        <tr>
                            <td class="ff"> <p>Name and Address<br />
                                    of the Office(s) for tender security submission :</p>      </td>
                            <td><%=commonTenderDetails.getSecuritySubOff()%></td>
                        </tr>-->
                        <% //}
                        if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {
                                //if(evalSerCertiSrBean.evalCertiLink(Integer.parseInt(id)) != 0){
                            if(!commonTenderDetails.getEventType().equalsIgnoreCase("REOI")){
                            if(commonTenderDetails.getPassingMarks() != 0){
                        %>
                            <tr>
                                <td class="ff">Passing Points :</td>
                                <td><%=commonTenderDetails.getPassingMarks() %></td>
                                <td>&nbsp;</td>

                            </tr>
                        <% }}

                            boolean isT1L1 = false;
                                    CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                    List<SPCommonSearchDataMore> envDataMores = dataMore.geteGPData("GetTenderEnvCount", id, null, null);
                                    if(envDataMores!=null && (!envDataMores.isEmpty())){
                                        if(envDataMores.get(0).getFieldName2().equals("3")){
                                        //Evaluation Method 1. T1 2. L1 3. T1L1 4. T1L1A
                                        isT1L1 = true;
                                        }
                                    }
                                if(isT1L1){
                                    EvalSerCertiSrBean evalSerCertiSrBean = new EvalSerCertiSrBean();
                                    TblEvalServiceWeightage tblEvalServiceWeightage = new TblEvalServiceWeightage();
                                    tblEvalServiceWeightage = evalSerCertiSrBean.getTblEvalServiceByTenderId(Integer.parseInt(id));

                            %>
                            <tr>

                                <td class="ff">Weightage For Technical Evaluation : </td>
                                <td><%= tblEvalServiceWeightage.getTechWeight()%>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="ff">Weightage For Financial Evaluation : </td>
                                <td><%= tblEvalServiceWeightage.getFinancialWeight()%>
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                            <%}/*Condition for QCBS*/ } //} %>
                    </table>
                    <% if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Services")) {%>
                    <table width="100%" cellspacing="0" class="tableList_1 ">
                        <tr>
                            <th class="t-align-center">Ref. No. <br /></th>
                            <th class="t-align-left">Phasing of service <br /></th>
                            <th class="t-align-center">Location</th>
                            <th class="t-align-center" >Indicative Start Date <br /></th>
                            <th class="t-align-center">Indicative Completion Date <br /></th>
                        </tr>
                        <% int i = 0;
                             for (CommonTenderDetails commonTdetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "Phase")) {
                                 i++;
                                 java.text.SimpleDateFormat format1 = new java.text.SimpleDateFormat("dd-MMM-yyyy");//remove hh:mm
                        %>
                        <tr>
                            <td class="t-align-center"><%=commonTdetails.getPhasingRefNo() %></td>
                            <td class="t-align-left"><%=commonTdetails.getPhasingOfService()%></td>
                            <td class="t-align-center"><% if (commonTdetails.getLocation() != null) {%><%=commonTdetails.getLocation()%><% }%></td>
                            <td class="t-align-center"><%=format1.format(commonTdetails.getIndStartDt())%></td>
                            <td class="t-align-center"><%=format1.format(commonTdetails.getIndEndDt())%></td>
                        </tr>
                        <%}%>
                    </table>
                    <% } else if (commonTenderDetails.getProcurementNature().equalsIgnoreCase("Goods") || commonTenderDetails.getProcurementNature().equalsIgnoreCase("Works")) {
                    %>

                    <table width="100%" cellspacing="0" class="tableList_1 ">
                        <tr>
                            <th width="6%" class="t-align-left">Lot No.</th>
                            <% if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes") && commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Lot wise") && !obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")){
                            %>
                            <th width="44%" class="t-align-left">Identification of Lot</th>
                            <% }else if(!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")){%>
                            <th width="54%" class="t-align-left">Identification of Lot</th>
                            <%}else if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes") && commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Lot wise")){%>
                            <th width="54%" class="t-align-left">Identification of Lot</th>
                            <%}else{%>
                            <th width="64%" class="t-align-left">Identification of Lot</th>
                            <% } %>
                            <th width="10%" class="t-align-left">Location <br /></th>
                            <% if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")){
                            if (commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Lot wise")) {%>
                            <th width="10%" class="t-align-left">Document Fees<br />(Amount in Nu.)</th>
                            <% } }if(!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")){
                            %>
                            <th width="10%" class="t-align-left">Tender security<br />(Amount in Nu.)</th>

                                <!-- Dohatec Start -->
                                <%if(commonTenderDetails.getProcurementType().equalsIgnoreCase("ICT")){%>
                                    <th width="10%" class="t-align-center">Equivalent Tender Security (Amount in USD)</th>
                                <%}%>
                                <!-- Dohatec End -->

                            <% }%>
                            <th width="10%" class="t-align-left">Start Date</th>
                            <th width="10%" class="t-align-left">Completion Date</th>
                        </tr>
                        <% int j = 0;
                             for (CommonTenderDetails commonTdetails : tenderSrBean.getAPPDetails(Integer.parseInt(id), "lot")) {
                                 j++;
                        %>
                        <tr>
                            <td class="t-align-center"><%=commonTdetails.getLotNo()%></td>
                            <td class="t-align-left"><%=commonTdetails.getLotDesc()%></td>
                            <td class="t-align-center"><% if (commonTdetails.getLocationSec() != null) {%><%=commonTdetails.getLocationSec()%><% }%></td>
                            <% if(!obj.isEmpty() && obj.get(0)[0].toString().equalsIgnoreCase("Yes")){
                            if (commonTenderDetails.getDocFeesMethod().equalsIgnoreCase("Lot wise")) {%>
                            <td class="t-align-center"><%=commonTdetails.getDocFess().setScale(0,0)%></td>
                            <% } }if(!obj.isEmpty() && obj.get(0)[2].toString().equalsIgnoreCase("Yes")){
                            %>
                            <td class="t-align-center"><%=commonTdetails.getTenderSecurityAmt().setScale(0,0)%></td>

                                <!-- Dohatec Start -->
                                <%if(commonTenderDetails.getProcurementType().equalsIgnoreCase("ICT")){%>
                                    <td class="t-align-center"><%=commonTdetails.getTenderSecurityAmtUSD().setScale(0,0)%></td>
                                <%}%>
                                <!-- Dohatec End -->

                            <% }%>   
                            <%
                                 java.text.SimpleDateFormat format3 = new java.text.SimpleDateFormat("dd/MM/yyyy");
                                 java.text.SimpleDateFormat format4 = new java.text.SimpleDateFormat("dd-MMM-yyyy");
                                 %>
                            <td class="t-align-center"><%if(commonTdetails.getStartTime() != null && !"".equals(commonTdetails.getStartTime())){%><%=format4.format(format3.parse(commonTdetails.getStartTime())) %><% } %></td>
                            <td class="t-align-center"><%if(!"".equals(commonTdetails.getCompletionTime())){%><%=format4.format(format3.parse(commonTdetails.getCompletionTime()))%><% } %></td>
                        </tr>
                        <% }
                         }%>
                    </table>

                    <div class="tableHead_22 t_space">Procuring Entity Details:</div>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 " width="100%">
                        <tr>
                                <td class="ff" width="20%">Name of Official Inviting <% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%> Pre-Qualification<% }
                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%> Tender/Proposal<% }
                                                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%> REOI<% }
                                                                                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%> RFP<% }%> :</td>
                            <td width="30%"><%=commonTenderDetails.getPeName()%></td>
                                <td class="ff" width="20%"> Designation of Official Inviting <% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%> Pre-Qualification<% }
                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%> Tender/Proposal<% }
                                                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%> REOI<% }
                                                                                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%> RFP<% }%> :</td>
                            <td width="30%"><%=commonTenderDetails.getPeDesignation()%></td>
                        </tr>

                        <tr>
                                <td class="ff">Address of Official Inviting <% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%> Pre-Qualification<% }
                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%> Tender/Proposal<% }
                                                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%> REOI<% }
                                                                                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%> RFP<% }%> : </td>
                            <td><%=commonTenderDetails.getPeAddress()%></td>
                                <td class="ff">Contact details of Official Inviting <% if (commonTenderDetails.getEventType().equalsIgnoreCase("PQ")) {%> Pre-Qualification<% }
                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("TENDER")) {%> Tender/Proposal<% }
                                                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("REOI")) {%> REOI<% }
                                                                                                                                if (commonTenderDetails.getEventType().equalsIgnoreCase("RFP")) {%> RFP<% }%> :</td>
                            <td><%=commonTenderDetails.getPeContactDetails()%></td>
                        </tr>

                        <tr>
                            <td colspan="4" class="ff mandatory">The procuring entity reserves the right to accept or reject all Tenders / Pre-Qualifications / EOIs</td>
                        </tr>
                         <%
                if(! (isPDF.equalsIgnoreCase("true")) ){
                 %>
                        <tr><td>&nbsp;</td>
                            <td colspan="3" class="t-align-left">
                                <% if(tenderStatus.equalsIgnoreCase("Approved")){%>
                                <a class="action-button-savepdf" onclick="printPage()" href="javascript:void(0);">Save As PDF</a>
                                <% } %>
                            </td>
                            <%if (isLoggedIn && (usrTypeId.toString().equals("2"))) {
                                                                int watchId = tenderSrBean.checkWatchList(Integer.parseInt(userId.toString()), Integer.parseInt(id));
                                                                if (tenderSrBean.isInWatchList()) {
                            %>
                            <td align="center" id="addCell" style="display: none"><a id="watchList" class="action-button-add">Add to WatchList</a></td>
                            <td align="center" id="removeCell"><a id="watchListRem" class="action-button-delete">Remove from WatchList</a></td>
                            <%} else {%>
                            <td align="center" id="addCell" ><a id="watchList" class="action-button-add">Add to WatchList</a></td>
                            <td align="center" id="removeCell" style="display: none"><a id="watchListRem" class="action-button-delete">Remove from WatchList</a></td>
                            <%}%>
                            <td><input type="hidden" id="watchId" value="<%=watchId%>" /></td>
                            <%}%>
                        </tr>
                    </table>

<!--                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">

                    </table>-->
                </form>
                        <form action="" id="frmPublishViewTComments" method="post" >
                            <input type="hidden" value="<%=DateUtils.formatDate(commonTenderDetails.getOpeningDt())%>" name="openingDate"/>
                    <table border="0" cellspacing="7" cellpadding="0" class="formStyle_1 " width="100%">
                        <tr>
                            <td width="20%" class="ff">Comments<span>*</span></td>
                            <td width ="80%"><textarea rows="4" cols="100" id="comments" name="comments" class="formTxtBox_1"></textarea></td>
                        </tr>
                        <tr><td>&nbsp;</td>
                            <td class="t-align-left">
                                <label class="formBtn_1"><input type="submit" name="Publish" id="Publish" value="Publish" />
                                    <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                                </label>
                            </td>
                        </tr>

                    </table>
                </form>
                     <%  } //end if pdf loop
                     %>

                <% }
                            %>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                        </td>
                    </tr>
                </table>
                 <%
                if(! (isPDF.equalsIgnoreCase("true")) ){
                 %>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <% } %>
                <!--Dashboard Footer End-->
            </div>
        </div>
        <%
                    tenderSrBean = null;
        %>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
