<%-- 
    Document   : LOI
    Created on : Feb 27, 2017, 10:55:00 AM
    Author     : Nishith
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonSPWfFileHistory"%>
<%@page import="java.text.ParseException"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderEstCost"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>


<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    
                    String userId = "";
                    String tenderId = "";
                    String procNature = "";
                    String procType = "";
                    String procNatureId = "";
                    String procTypeId = "";
                    String procMethodId = "";
                    String procMethodName = "";
                    String daysToNOA = "";
                    String CurrentDate = "";
                    int uid=0;
                    int childid = 1;
                    boolean IsAllLotsApproved = true;
                    boolean isEvalRptSentForLOI = false;
                    
                    String msg = null;
                    
                    String aaQString = "";
                    aaQString="&rpt=1"; //Only for Financial
                    

                    if (request.getParameter("msg") != null) {
                        msg = request.getParameter("msg");
                    }
                    
                    
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userId = hs.getAttribute("userId").toString();
                        Integer ob1 = (Integer)hs.getAttribute("userId");
                        uid = ob1.intValue();
                    }
                    
                    if(request.getParameter("tenderid") != null){
                        tenderId = request.getParameter("tenderid");
                        childid = Integer.parseInt(tenderId);
                    }
                    
                    EvaluationService evalService1 = (EvaluationService)AppContext.getSpringBean("EvaluationService");
                    int eCount = 0;
                    eCount = evalService1.getEvaluationNo(Integer.parseInt(tenderId));
                    
                    List<CommonTenderDetails> list = tenderSrBean.getAPPDetails(Integer.parseInt(tenderId), "tender");
                    if (!list.isEmpty()){
                        procMethodId = list.get(0).getProcurementMethodId().toString();
                        procMethodName = list.get(0).getProcurementMethod().toString();
                        
                        procNature = list.get(0).getProcurementNature();
                        procType = list.get(0).getProcurementType();
                    }
                    procTypeId = procType.equalsIgnoreCase("NCT")? "1" : "2" ;
                    procNatureId = procNature.equalsIgnoreCase("Goods")?"1":procNature.equalsIgnoreCase("Works")?"2":"3";
                    
                    
                    
                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> getNOAConfig = commonSearchDataMoreService.geteGPData("GetNOAConfig", tenderId, procMethodId, procNatureId, procTypeId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    
                    
                    daysToNOA = getNOAConfig.isEmpty()?"0":getNOAConfig.get(0).getFieldName2();
                    CurrentDate = commonSearchDataMoreService.getCurrentDate();
                    
        
                    TenderEstCost estcost = (TenderEstCost) AppContext.getSpringBean("TenderEstCost");
                    boolean flagEstCost = false;
                    flagEstCost = estcost.checkForLink(Integer.parseInt(request.getParameter("tenderid")));
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    String eventType = commonService.getEventType(request.getParameter("tenderid")).toString();
                    
                    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> getTenderCommPubDate
                = tenderCommonService1.returndata("getTenderCommPubDate", tenderId, null);
                    

                boolean isPE = false;
              List<SPTenderCommonData> chkCurUserPE  = tenderCommonService1.returndata("getPEOfficerUserIdfromTenderId", tenderId, userId);

              if (!chkCurUserPE.isEmpty()) {
                  if(userId.equalsIgnoreCase(chkCurUserPE.get(0).getFieldName1())){
                    isPE = true;
                  }
              }
              
              

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Letter Of Intent (LOI)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>

        <!--jalert -->

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        
        <div class="contentArea_1">
        <div class="pageHead_1">Letter Of Intent (LOI) </div>

            <%
                        // Variable tenderId is defined by u on ur current page.
                        pageContext.setAttribute("tenderId", tenderId);
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>

            <div>&nbsp;</div>
            <% pageContext.setAttribute("isTenPackageWise",(Boolean)isTenPackageWise); %>
            <% pageContext.setAttribute("tab","24"); %>
            
            
            <div>
            
            <%@include  file="officerTabPanel.jsp"%>
            
            </div>
            
            <div class="tabPanelArea_1">
                
                
                <% if (msg != null && !msg.contains("not")) {%>
            <div id="successMsg" class="responseMsg successMsg t_space" style="margin-bottom: 12px;">File processed successfully</div>
            <%  } else if (msg != null && msg.contains("not")) {%>
            <div id="successMsg" class="responseMsg errorMsg" style="margin-bottom: 12px;">File has not processed successfully</div>
            <%  }%>
                
                
                
                <table width="100%" cellspacing="0" class="tableList_1">
                    <%
                            if(procNature.equalsIgnoreCase("Goods")){
                                List<SPTenderCommonData> getNumberOfApprovedLots = tenderCommonService1.returndata("getNumberOfApprovedLots", tenderid, null);
                                List<SPTenderCommonData> getNumberOfUnapprovedLots = tenderCommonService1.returndata("getNumberOfUnapprovedLots", tenderid, null);
                                if(Integer.parseInt(getNumberOfApprovedLots.get(0).getFieldName1()) < Integer.parseInt(getNumberOfUnapprovedLots.get(0).getFieldName1())) //Number of approved lots are less than total lots.
                                {
                                    IsAllLotsApproved = false;
                                }
                        %>
                    <tr>
                        <td colspan="3" class="t-align-left ff">Letter Of Intent(LOI)</td>
                    </tr>
                    <%
                            if(!IsAllLotsApproved)
                            {
                                %>
                                    <tr>
                                        <td colspan="3" style="color: red;"><b>All lots are not approved by Tender Committee Chairperson yet. Workflow can only be created after approving all lots.</b></td>
                                    </tr>
                                <%
                            }
                        }
                        List<SPTenderCommonData> lstLots = tenderCommonService1.returndata("getLoIdLotDesc_ForOpening_LOI", tenderid, null);
                        List<SPCommonSearchDataMore> bidderRounds = new ArrayList<SPCommonSearchDataMore>();//Round List Initialized
                        List<SPCommonSearchDataMore> listReportID = null;
                        
                        
                        String loiDate = "";
                        String tenderLot = "0";
                        String roundID = "0";
                        String reportID = "0";
                        int lotCounter = 1;
                        int NumberOfApprovedLots = lstLots.size();
                        boolean ShowWorkflowLink = true;
                        for (SPTenderCommonData objLot : lstLots) {
                        if(!tenderProcureNature.equalsIgnoreCase("Services"))
                        {
                            tenderLot =  objLot.getFieldName1();
                        } 
                        
                        //boolean ShowWorkflowLink = false;
                        
                        listReportID = dataMore.getEvalProcessInclude("getCRReportId", tenderid,tenderLot,"0");
                        if(listReportID!=null && (!listReportID.isEmpty())){
                            //if(listReportID.get(0).getFieldName2().equals("1"))
                                    reportID = listReportID.get(0).getFieldName1();
                        }

                        bidderRounds = dataMore.getEvalProcessInclude("getRoundsforEvaluation",tenderid,reportID,"L1",String.valueOf(eCount),tenderLot);
                        if(bidderRounds!=null && !bidderRounds.isEmpty())
                            roundID = bidderRounds.get(0).getFieldName1();
            
                        
                        loiDate = tcs.GetLOIIssueDate(tenderId,tenderLot,roundID);
                        isEvalRptSentForLOI = tcs.IsEvalRptSentForLOI(tenderId, tenderLot, roundID, String.valueOf(eCount));
                        
                        String convDate1;
                        Date date;
                        String noaDate="";
                        
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                        Calendar cal = Calendar.getInstance();
                        try{
                            date = formatter.parse(loiDate);
                            
                            cal.setTime(date);
                            cal.add(Calendar.DAY_OF_YEAR, Integer.parseInt(daysToNOA));
                            date = cal.getTime();
                            noaDate = formatter.format(date);
                            
                            //NOA date comparision with current date
                            Date NOADate = new SimpleDateFormat("dd/MM/yyyy").parse(noaDate);
                            Date CrntDate = new SimpleDateFormat("dd/MM/yyyy").parse(CurrentDate);
                            if(NOADate.after(CrntDate))
                            {
                                //NOADate is after CrntDate
                                ShowWorkflowLink = false;
                            }
//                            if(CrntDate.after(NOADate))
//                            {
//                                //CrntDate is after NOADate
//                                ShowWorkflowLink = true;
//                            }
//                            else if(CrntDate.equals(NOADate))
//                            {
//                                //CrntDate and NOADate are equal
//                                ShowWorkflowLink = true;
//                            }
                        }catch(Exception ex){
                            System.out.println(ex);
                        }
                    
                    %>
                    <tr>
                        <td colspan="3" class="t-align-left ff">&nbsp;</td>
                    </tr>
                    <tr>
                        <%
                            if(procNature.equalsIgnoreCase("Goods")){
                        %>
                            <td width="30%" class="t-align-left ff">Lot No: <%=objLot.getFieldName3()%></td>
                        <%}else{%>
                            <td width="30%" class="t-align-left ff">Letter Of Intent</td>
                        <%}%>
                        <td width="80%" class="t-align-left">
                            <%if("0".equalsIgnoreCase(roundID)){%>
                                <h5>Winner not yet selected.</h5>
                            <%}else{%>
                                <a href="ViewLOI.jsp?tLotId=<%=tenderLot%>&tenderid=<%=tenderId%>&lotNo=<%=lotCounter%>&roundId=<%=roundID%>">View</a>
                            <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td width="30%" class="t-align-left ff">LOA Issue Date</td>
                        <td width="80%" class="t-align-left">
                            <%if("".equalsIgnoreCase(noaDate)){%>
                                <h5>Letter Of Intent not yet issued.</h5>
                            <%}else{%>
                                <span><b><%=noaDate%></b></span>
                            <%}%>
                        </td>
                    </tr>
                    <% //if(isPE){%>
                    <% if(lotCounter == NumberOfApprovedLots && IsAllLotsApproved) { %>
                    <tr>
                            <td class="ff">Contract Approval Workflow</td>

                            <td class="t-align-left">
                                <%   
                                    
                                    WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                                    String checkperole = "No";
                                    boolean ispublish = false;
                                    boolean isTERSentToAA = false;
                                    
                                    String eventId = "1";
                                    List<SPCommonSearchDataMore> TERALL;
                                    
                                    
                                    
                                    
                                    envDataMores = dataMore.getEvalProcessInclude("GetTenderEnvCount", tenderId, null, null);
                                    
                                    if(envDataMores!=null && (!envDataMores.isEmpty())){
                                        
                                        eventId = envDataMores.get(0).getFieldName1();
                                    }
                                    
                                    
                                    if(procMethodName.equals("QCBS"))
                                        TERALL = dataMore.getEvalProcessInclude("isTERSentToAAService", tenderId, "0", "0","0",String.valueOf(eCount));
                                    else
                                        TERALL = dataMore.getEvalProcessInclude("isTERSentToAA", tenderId, tenderLot, eventId,roundID,String.valueOf(eCount));

                                    if(TERALL!=null && (!TERALL.isEmpty()) && (!TERALL.get(0).getFieldName1().equals("0"))){
                                        isTERSentToAA = true;
                                    }
                                    
                                    
                                    Boolean evaluationWorkflowCreated = false;
                                    List<SPCommonSearchDataMore> evaluationWorkflowList = dataMore.getEvalProcessInclude("isEvaluationWorkFlowCreated",tenderId,userId, "1");
                                    if(evaluationWorkflowList !=null && (!evaluationWorkflowList.isEmpty())){
                                        evaluationWorkflowCreated = true;
                                    }
                                    
                                    
                                    
                                    String field1 = "CheckCreator";
                                    List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(field1, tenderId, userId);

                                    if (editdata.size() > 0) {
                                        checkperole = "Yes";
                                    }
                                    
                                    short wfActivityid = 13;
                                    short wfEventid = 12;
                                    boolean wfEvntexist = false;
                                    wfEvntexist = workFlowSrBean.isWorkFlowEventExist(wfEventid, Integer.parseInt(tenderId));
                                    String wfUrl = "";

                                    boolean isCancelled = true;
                                    int pqTenderId = 0;
                                    List<TblTenderDetails> tblTenderDetailTS = tenderSrBean.getTenderStatus(String.valueOf(tenderId));
                                    if (!tblTenderDetailTS.isEmpty()) {
                                        pqTenderId = tblTenderDetailTS.get(0).getPqTenderId();
                                        if ("Cancelled".equalsIgnoreCase(tblTenderDetailTS.get(0).getTenderStatus())) {
                                            isCancelled = false;
                                        }
                                    }

                                    SPTenderCommonData isFormOk = tenderSrBean.isTenderFormOk(Integer.parseInt(tenderId));
                                    List<TblTenderDetails> tenderDetails = workFlowSrBean.checkSubmissionDate(Integer.parseInt(tenderId));

                                    boolean checksubmission = false;
                                    if (tenderDetails.size() > 0) {
                                        TblTenderDetails wfTenderDetails = tenderDetails.get(0);
                                        if (!"".equals(wfTenderDetails.getSubmissionDt()) && wfTenderDetails.getSubmissionDt() != null) {
                                            checksubmission = true;
                                        }
                                    }

                                    String isSigned = tenderCommonService1.returndata("getWorkFlowEditEnable", tenderId, "").get(0).getFieldName1();

                                    boolean istwfLevelExist = false;
                                    String wfFileOnHand = "No";
                                    int wfInitiator = 0;
                                    int wfApprover = 0;
                                    boolean isWfInitiater = false;
                                    String wfCheckperole = checkperole; //"No";
                                    String wfChTender = "No";
                                    boolean isWfpublish = false;
                                    boolean isWfConApprove = false;
                                    String from_action = "eval";

                                    boolean b_isPublish = false;
                                    String tenderStatus = "";
                                    String tenderworkflow = "";

                                    List<SPTenderCommonData> wfPublist = tenderCommonService1.returndata("TenderWorkFlowStatus", tenderId, "'Approved','Conditional Approval'");

                                    if (wfPublist.size() > 0) {
                                        ispublish = true;
                                        SPTenderCommonData wfSpTenderCommondata = wfPublist.get(0);
                                        tenderStatus = wfSpTenderCommondata.getFieldName1();
                                        tenderworkflow = wfSpTenderCommondata.getFieldName2();
                                        if ("Approved".equalsIgnoreCase(tenderStatus)) {
                                            b_isPublish = true;
                                        }
                                        if (tenderworkflow.equalsIgnoreCase("Conditional Approval")) {
                                            isWfConApprove = true;
                                        }
                                    }

                                    boolean approved = false;
                                    boolean forwarded = false;
                                    boolean editFlag = false;

                                    List<CommonSPWfFileHistory> commonSpWfFileHistory = workFlowSrBean.getWfHistory("History", (int)wfActivityid, Integer.parseInt(tenderId),(int)childid ,uid, 1, 10,"processDate","desc","","","",0,"","");
                                    if(!commonSpWfFileHistory.isEmpty())
                                    {
                                        for(int i = 0; i< commonSpWfFileHistory.size(); i++)
                                        {
                                            if(i == 0 && "Approve".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction()) && isSigned.equalsIgnoreCase("true"))
                                            {
                                                editFlag = true;
                                                break;
                                            }
                                            if("Approve".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction()))
                                            {
                                                approved = true;
                                                forwarded = true;
                                                break;
                                            }
                                            if("Forward".equalsIgnoreCase(commonSpWfFileHistory.get(i).getAction()) && !forwarded)
                                            {
                                                forwarded = true;
                                            }
                                        }
                                    }

                                    short wfTlevel = 1;
                                    List<TblWorkFlowLevelConfig> wfTblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(Integer.parseInt(tenderId), childid, wfActivityid);
                                    if (wfTblWorkFlowLevelConfig.size() > 0) {
                                        Iterator wfTwflc = wfTblWorkFlowLevelConfig.iterator();
                                        istwfLevelExist = true;
                                        while (wfTwflc.hasNext()) {
                                            TblWorkFlowLevelConfig workFlowlelT = (TblWorkFlowLevelConfig) wfTwflc.next();
                                            TblLoginMaster wflmaster = workFlowlelT.getTblLoginMaster();
                                            if (wfTlevel == workFlowlelT.getWfLevel() && uid == wflmaster.getUserId()) {
                                                wfFileOnHand = workFlowlelT.getFileOnHand();
                                            }
                                            if (workFlowlelT.getWfRoleId() == 1) {
                                                wfInitiator = wflmaster.getUserId();
                                            }
                                            if (workFlowlelT.getWfRoleId() == 2) {
                                                wfApprover = wflmaster.getUserId();
                                            }
                                            if (wfFileOnHand.equalsIgnoreCase("yes") && uid == wflmaster.getUserId()) {
                                                isWfInitiater = true;
                                            }
                                            if(wfTlevel == workFlowlelT.getWfLevel() && "yes".equalsIgnoreCase(workFlowlelT.getFileOnHand()) && !approved && !forwarded)
                                            {
                                                editFlag = true;
                                            }
                                        }
                                    }

                                    if(!editFlag)
                                    {
                                        int noOfReviewer = 0;
                                        List<CommonAppData> reviewer = workFlowSrBean.editWorkFlowData("workflowedit", "13", tenderId);
                                        Iterator it = reviewer.iterator();
                                        while (it.hasNext()) {
                                            CommonAppData commonAppData = (CommonAppData) it.next();
                                            noOfReviewer = Integer.valueOf(commonAppData.getFieldName3());
                                            if(noOfReviewer >= 0 && !editFlag && !approved && !forwarded)
                                                editFlag = true;
                                        }
                                    }

                                    String wfDonor = "";
                                    wfDonor = workFlowSrBean.isDonorReq(String.valueOf(wfEventid), Integer.valueOf(tenderId));
                                    String[] wfNorevs = wfDonor.split("_");
                                    int wfNorevrs = -1;

                                    String wfStrrev = wfNorevs[1];
                                    if (!wfStrrev.equals("null")) {
                                        wfNorevrs = Integer.parseInt(wfNorevs[1]);
                                    }
                                    
                                 
                                    if(wfEvntexist){
                                        
                                        if(!isTERSentToAA && isEvalRptSentForLOI && wfCheckperole.equals("Yes")){ %>
                                            <a href="FileProcessing.jsp?activityid=<%=wfActivityid%>&objectid=<%=tenderId%>&childid=<%=childid%>&eventid=<%=wfEventid%>&fromaction=<%=from_action%>&loi=loi">Process file in Workflow</a>
                                        
                                        <% }else{ %>
                                            <a  href="CreateWorkflow.jsp?activityid=<%=wfActivityid%>&eventid=<%=wfEventid%>&objectid=<%=tenderId%>&action=View&childid=<%=childid%>&parentLink=906&loi=Loi">View</a>
                                        <% }

                                           // if(wfCheckperole.equals("Yes") && !b_isPublish == false && editFlag){ 
                                                //if(isCancelled){
                                                  //  wfUrl = "CreateWorkflow.jsp?activityid=" + wfActivityid + "&eventid=" + wfEventid + "&objectid=" + tenderId + "&action=Edit&childid=" + childid + "&parentLink=916";
                                   %>
                                                
                                                    <!--<a style="cursor: pointer" onclick="wfAlert()">Edit</a>&nbsp;|&nbsp;-->
                                            
                                                    <!--//start-->
                                                    
                                                   
                                                    
                                  

<%//}%>
                                               <!--&nbsp;|&nbsp; <a  href="CreateWorkflow.jsp?activityid=wfActivityid&eventid=wfEventid&objectid=tenderId&action=View&childid=childid&parentLink=906&loi=Loi">View</a>-->

                                            <%//} else 
                                                if (istwfLevelExist == false && wfCheckperole.equals("Yes") && !b_isPublish == false && editFlag) {
                                                      if (isCancelled) {
                                                          wfUrl = "CreateWorkflow.jsp?activityid=" + wfActivityid + "&eventid=" + wfEventid + "&objectid=" + tenderId + "&action=Edit&childid=" + childid + "&parentLink=916";
                                            %>
                                                     
                                                        <!--<a style="cursor: pointer" onclick="wfAlert()">Edit</a>&nbsp;|&nbsp;-->
                                                      <%}%>
                                                      &nbsp;|&nbsp;<a  href="CreateWorkflow.jsp?activityid=<%=wfActivityid%>&eventid=<%=wfEventid%>&objectid=<%=tenderId%>&action=View&childid=<%=childid%>&parentLink=906&loi=Loi">View</a>

                                            <%} else if (isCancelled && istwfLevelExist == false && !wfCheckperole.equalsIgnoreCase("Yes")) {%>
                                                        <label>Workflow yet not configured</label>

                                            <%} else if (istwfLevelExist == true && isTERSentToAA) {%>
                                                    <!--view workflow-->
                                            <%}

                                    } else if (!b_isPublish == false && wfCheckperole.equals("Yes") && isSigned.equalsIgnoreCase("true")) {
                                        if (isCancelled) 
                                        {
                                            if(ShowWorkflowLink)
                                            {
                                                wfUrl = "CreateWorkflow.jsp?activityid=" + wfActivityid + "&eventid=" + wfEventid + "&objectid=" + tenderId + "&action=Create&childid=" + childid + "&parentLink=925";
                                                %>
                                                <a href="CreateWorkflow.jsp?activityid=<%=wfActivityid%>&eventid=<%=wfEventid%>&objectid=<%=tenderId%>&action=Create&childid=<%=childid%>&parentLink=925&submit=Submit&loi=Loi&tc=tc" onclick="return wfAlert()">Create</a>
                                                <!--<a style="cursor: pointer" onclick="wfAlert()">Create</a>-->
                                                <%
                                            }
                                            else
                                            {
                                                %>
                                                <span style="color:red;">LOA issue date has not reached.</span>
                                                <%
                                            }
                                        }

                                    } else if (isCancelled && istwfLevelExist == false && !wfCheckperole.equalsIgnoreCase("Yes")) {%>
                                            <label>Workflow yet not configured</label>
                                    <%}%>

                                    <%if (istwfLevelExist) {%>
                                        &nbsp;|&nbsp;<a href="workFlowHistory.jsp?activityid=<%=wfActivityid%>&objectid=<%=tenderId%>&childid=<%=childid%>&eventid=<%=wfEventid%>&userid=<%=uid%>&fraction=eval&parentLink=75&loi=loi" >View Workflow History</a>
                                    <% }%>

                            </td>

                        </tr>
                    <%} lotCounter++; } //}%>
                </table>
            </div>

            </div>
        
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>

    </body>
    
</html>

<script type="text/javascript">
        function wfAlert(){
        
            var msg = 'Once the WORKFLOW is created, it can not be undone.';

            return confirm(msg);
            
        }
    </script>