<%-- 
    Document   : AccPaymentDocUpload
    Created on : Aug 6, 2011, 3:52:41 PM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceDocument"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AccPaymentService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Payment Reference Document</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <%
        ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
                AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
        %>
<script type="text/javascript">

            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        //uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:50}
                    },
                    messages: {
                        //uploadDocFile: { required: "<div class='reqF_1'><%=bdl.getString("CMS.Doc.Validate.Document.field")%></div>"},
                        documentBrief: { required: "<div class='reqF_1'><%=bdl.getString("CMS.Doc.Validate.desc")%></div>",
                                        maxlength: "<div class='reqF_1'><%=bdl.getString("CMS.Doc.Validate.desc.max")%></div>"}
                    }
                });
            });
            $(function() {
                $('#frmUploadDoc').submit(function() {
                    if(document.getElementById("uploadDocFile").value=="")
                    {document.getElementById("docspan").innerHTML="please select Document";return false;}
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'><%=bdl.getString("CMS.Doc.validate.filesize")%></div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'><%=bdl.getString("CMS.Doc.validate.filename")%></div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#btnUpld').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
</script>
    </head>
    <body>            
<%@include file="../resources/common/AfterLoginTop.jsp" %>
            <%
                
                List<Object> objInvoiceId = null;
                String invoiceID = "";
                String tenderId = "";
                String InvoiceId = "";


                String InvoiceNo = "";
                TenderTablesSrBean beanCommon1 = new TenderTablesSrBean();
                String tenderType1 = beanCommon1.getTenderType(Integer.parseInt(request.getParameter("tenderId")));
                ConsolodateService cs = (ConsolodateService)AppContext.getSpringBean("ConsolodateService");

                if(session.getAttribute("userId")==null){
                        response.sendRedirect("SessionTimedOut.jsp");
                    }
                if (request.getParameter("tenderId") != null) {
                    tenderId = request.getParameter("tenderId");                    
                }
             //   if (request.getParameter("InvoiceId") != null) {
              //      InvoiceId = request.getParameter("InvoiceId");
             //   }
                
                String wpId = "";
                if(request.getParameter("wpId")!=null)
                {
                    wpId = request.getParameter("wpId");
                }

              /*  if (request.getParameter("invoiceId") != null) {
                    if(!tenderType1.equals("ICT"))
                        InvoiceId = request.getParameter("invoiceId");
                    else
                    {
                        ConsolodateService cs = (ConsolodateService)AppContext.getSpringBean("ConsolodateService");
                        objInvoiceId = cs.getInvoiceIdForICT(request.getParameter("invoiceId"), Integer.parseInt(wpId));
                        InvoiceId = objInvoiceId.get(0).toString();
                    }

                }*/

                if(!tenderType1.equals("ICT"))
                    {
                        if (request.getParameter("invoiceId") != null)
                        InvoiceId = request.getParameter("invoiceId");
                     else
                        InvoiceId = request.getParameter("InvoiceId");
                    }
                    else
                    {
                       // pageContext.setAttribute("invoiceNo", request.getParameter("invoiceNo"));
                        if( request.getParameter("invoiceNo")!=null)
                        {
                            InvoiceNo = request.getParameter("invoiceNo");
                            objInvoiceId = cs.getInvoiceIdForICT(request.getParameter("invoiceNo"), Integer.parseInt(wpId));
                            InvoiceId = objInvoiceId.get(0).toString();
                        }
                        else if(request.getParameter("invoiceId")!=null)
                        {
                            objInvoiceId = cs.getInvoiceNo(request.getParameter("invoiceId"));
                             InvoiceNo = objInvoiceId.get(0).toString();
                              InvoiceId = request.getParameter("invoiceId");
                        }
                        else if(request.getParameter("InvoiceId")!=null)
                        {
                            objInvoiceId = cs.getInvoiceNo(request.getParameter("InvoiceId"));
                             InvoiceNo = objInvoiceId.get(0).toString();
                             InvoiceId = request.getParameter("InvoiceId");
                        }


                       // pageContext.setAttribute("invoiceId", objInvoiceId.get(0).toString());
                    }
                
                String lotId = "";
                if(request.getParameter("lotId")!=null)
                {
                    lotId = request.getParameter("lotId");
                    pageContext.setAttribute("lotId", request.getParameter("lotId"));
                }
                 String logUserId = "0";
                 if (session.getAttribute("userId") != null) {
                 logUserId = session.getAttribute("userId").toString();
                 }
                CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();
                String strProcNature = "";
                if("Services".equalsIgnoreCase(procnature))
                {
                    strProcNature = "Consultant";
                }else if("goods".equalsIgnoreCase(procnature)){
                    strProcNature = "Supplier";
                }else{
                    strProcNature = "Contractor";
                }
            %>
            <div class="mainDiv">
            <div class="fixDiv">
                 <div class="dashboard_div">
            <div class="contentArea_1">
            <div class="pageHead_1">
                <%if("25".equalsIgnoreCase(accPaymentService.getProcurementRoleID(logUserId))){%>
                <%=bdl.getString("CMS.Doc.Upload.Title")%>
                <%}else{%>
                Payment Reference Documents
                <%}%>
                <span style="float: right; text-align: right;">
                <%if("25".equalsIgnoreCase(accPaymentService.getProcurementRoleID(logUserId))){%>
                     <%if(!tenderType1.equals("ICT")){%>
                    <a class="action-button-goback" href="AccPaymentView.jsp?tenderId=<%=tenderId%>&InvoiceId=<%=InvoiceId%>&wpId=<%=wpId%>&lotId=<%=lotId%>" title="STD Dashboard">Go Back</a>
                     <%}else{%>
                    <a class="action-button-goback" href="AccPaymentView.jsp?tenderId=<%=tenderId%>&InvoiceId=<%=InvoiceId%>&wpId=<%=wpId%>&lotId=<%=lotId%>&invoiceNo=<%=InvoiceNo%>" title="STD Dashboard">Go Back</a>
          
                  <%}%>
                <%}else{%>
                <%if(!tenderType1.equals("ICT")){%>
                    <a class="action-button-goback" href="ViewInvoice.jsp?tenderId=<%=tenderId%>&invoiceId=<%=InvoiceId%>&wpId=<%=wpId%>&lotId=<%=lotId%>" title="STD Dashboard">Go Back</a>
                <%}else{%>
                    <a class="action-button-goback" href="ViewInvoice.jsp?tenderId=<%=tenderId%>&invoiceNo=<%=InvoiceNo%>&wpId=<%=wpId%>&lotId=<%=lotId%>" title="STD Dashboard">Go Back</a>
                <%}%>
                <%}%>
            </span>
            </div>
            <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/AccPaymentDocServlet?funName=Upload" enctype="multipart/form-data" name="frmUploadDoc">
                <input type="hidden" name="tenderId" value="<%= tenderId%>" />
                <input type="hidden" name="InvoiceId" value="<%=InvoiceId%>" />
                <input type="hidden" name="wpId" value="<%=wpId%>" />
                <input type="hidden" name="lotId" value="<%=lotId%>" />
               <%pageContext.setAttribute("tenderId", tenderId);%>
               <%@include file="../resources/common/TenderInfoBar.jsp"%>                    
                    <%
                    if (request.getParameter("fq") != null) {
                        if (request.getParameter("fq").equals("Removed") || request.getParameter("fq").equals("Uploaded")) {
                    %>
                    <div class="responseMsg successMsg" style="margin-top: 10px;">File <%=request.getParameter("fq")%> Successfully</div>
                    <%} else {%>
                    <div> &nbsp;</div>
                    <div class="responseMsg errorMsg"><%=request.getParameter("fq")%></div>
                    <%
                            }
                        }
                        if (request.getParameter("fs") != null) {
                    %>
                    <div> &nbsp;</div>
                    <div class="responseMsg errorMsg">
                        Max. file size of a single file must not exceed <%=request.getParameter("fs")%>MB, Acceptable file types are : <%=request.getParameter("ft")%>.
                    </div>
                    <%}%>                    
                    <table width="90%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                    <tr>
                        <td style="font-style: italic" colspan="2" class="ff t-align-left" >Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                    </tr>
                    <tr>
                        <td width="10%" class="ff t-align-left"><%=bdl.getString("CMS.Doc.Document")%><span class="mandatory">*</span></td>
                        <td width="80%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                            <span id="docspan" class="reqF_1"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff"><%=bdl.getString("CMS.Doc.Desc")%><span>*</span></td>
                        <td>
                            <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                            <div id="dvDescpErMsg" class='reqF_1'></div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>
                            <label class="formBtn_1"><input type="submit" name="btnUpld" id="btnUpld" value="Upload" /></label>                            
                        </td>
                    </tr>
                   </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="100%"  class="t-align-left">Instructions</th>
                        </tr>
                        <tr>
                            <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("officer");%>
                            <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                        </tr>
                    </table>
            </form>
            
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <th width="8%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                    <th class="t-align-center" width="23%"><%=bdl.getString("CMS.Inv.FileName")%></th>
                    <th class="t-align-center" width="28%"><%=bdl.getString("CMS.Inv.FileDescription")%></th>
                    <th class="t-align-center" width="7%"><%=bdl.getString("CMS.Inv.FileSize")%><br />
                        <%=bdl.getString("CMS.Inv.inKB")%></th>
                    <th class="t-align-center" width="">Uploaded By</th>
                    <th class="t-align-center" width="18%"><%=bdl.getString("CMS.action")%></th>
                </tr>
                <%                        
                        String userId = "1";
                        if (session.getAttribute("userId") != null) {
                            userId = session.getAttribute("userId").toString();//Integer.parseInt(InvoiceId)
                            /*ConsolodateService cs = (ConsolodateService)AppContext.getSpringBean("ConsolodateService");
                            objInvoiceId = cs.getInvoiceIdForICT(request.getParameter("invoiceId"), Integer.parseInt(wpId));
                            invoiceID = objInvoiceId.get(0).toString();*/

                        }
                        List<TblCmsInvoiceDocument> getInvoiceDocData = accPaymentService.getInvoiceDocDetailsbyPassingUsertypeID(Integer.parseInt(InvoiceId), userTypeId);
                        //List<TblCmsInvoiceDocument> getInvoiceDocData = accPaymentService.getInvoiceDocDetailsbyPassingUsertypeID(Integer.parseInt(invoiceID), userTypeId);
                        if(!getInvoiceDocData.isEmpty())
                        {
                            for(int i =0; i<getInvoiceDocData.size(); i++)
                            {                                
                %>
                        <tr>
                            <td class="t-align-center"><%=(i+1)%></td>
                            <td class="t-align-left"><%=getInvoiceDocData.get(i).getDocumentName()%></td>
                            <td class="t-align-left"><%=getInvoiceDocData.get(i).getDocDescription()%></td>
                            <td class="t-align-center"><%=(Long.parseLong(getInvoiceDocData.get(i).getDocSize())/1024)%></td>
                            <td class="t-align-center">
                                <%
                                String str = "";
                                if (2 == getInvoiceDocData.get(i).getUserTypeId()) {
                                    str = "supplier";
                                    out.print(strProcNature);
                                } else {
                                    if("25".equalsIgnoreCase(accPaymentService.getProcurementRoleID(Integer.toString(getInvoiceDocData.get(i).getUploadedBy()))))
                                    {
                                        str = "accountant";
                                        out.print("Account Officer");
                                    }else{
                                        str = "PE";
                                        out.print("PE Officer");
                                    }
                                }
                                
                                boolean isAcc = false;
                                Object objIsAcc = session.getAttribute("procurementRole");
                                String strProRoleIsAcc = "";
                                if (objIsAcc != null) {
                                    strProRoleIsAcc = objIsAcc.toString();
                                }
                                String chkIsAcc = strProRoleIsAcc;
                                String chk1IsAcc[] = chkIsAcc.split(",");
                                for (int iIsAcc = 0; iIsAcc < chk1IsAcc.length; iIsAcc++) {
                                    if (chk1IsAcc[iIsAcc].equalsIgnoreCase("Account Officer")) {
                                        isAcc = true;
                                        break;
                                    }
                                }
                                %>
                            </td>
                            <td class="t-align-center">
                                <a href="<%=request.getContextPath()%>/AccPaymentDocServlet?docName=<%=getInvoiceDocData.get(i).getDocumentName()%>&docSize=<%=getInvoiceDocData.get(i).getDocSize()%>&tenderId=<%=tenderId%>&invoiceDocId=<%=getInvoiceDocData.get(i).getInvoiceDocId()%>&invoiceId=<%=getInvoiceDocData.get(i).getTblCmsInvoiceMaster().getInvoiceId()%>&wpId=<%=wpId%>&lotId=<%=lotId%>&str=<%=str%>&funName=download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                &nbsp;                                
                                <%
                                if(isAcc){
                                    if("accountant".equalsIgnoreCase(str)){
                                %>
                                <a href="<%=request.getContextPath()%>/AccPaymentDocServlet?&docName=<%=getInvoiceDocData.get(i).getDocumentName()%>&docSize=<%=getInvoiceDocData.get(i).getDocSize()%>&tenderId=<%=tenderId%>&invoiceDocId=<%=getInvoiceDocData.get(i).getInvoiceDocId()%>&invoiceId=<%=getInvoiceDocData.get(i).getTblCmsInvoiceMaster().getInvoiceId()%>&wpId=<%=wpId%>&lotId=<%=lotId%>&str=<%=str%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                <%
                                   }
                                }else{
                                    if("PE".equalsIgnoreCase(str)){
                                %>
                                <a href="<%=request.getContextPath()%>/AccPaymentDocServlet?&docName=<%=getInvoiceDocData.get(i).getDocumentName()%>&docSize=<%=getInvoiceDocData.get(i).getDocSize()%>&tenderId=<%=tenderId%>&invoiceDocId=<%=getInvoiceDocData.get(i).getInvoiceDocId()%>&invoiceId=<%=getInvoiceDocData.get(i).getTblCmsInvoiceMaster().getInvoiceId()%>&wpId=<%=wpId%>&lotId=<%=lotId%>&str=<%=str%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                <%
                                   }
                                }
                                %>
                            </td>
                        </tr>                        
                        <%}
                        }else{%>
                        <tr>
                            <td colspan="6" class="t-align-center"><%=bdl.getString("CMS.Inv.NoRecord")%></td>
                        </tr>
                        <%}%>
            </table>

            <div>&nbsp;</div>
            </div>
                 </div></div></div>            
           <%@include file="../resources/common/Bottom.jsp" %>            
    </body>
<script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

</script>
</html>

