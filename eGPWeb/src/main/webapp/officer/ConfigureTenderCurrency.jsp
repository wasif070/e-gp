<%--
    Document   : Currency Configuration
    Created on : Mar 21, 2011, 10:59:10 AM
    Author     : Ketan Prajapati
--%>
<%@page import="java.util.Collections"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.HashMap"%>
<%@page import= "com.cptu.egp.eps.service.serviceinterface.CurrencyMasterService" %>
<%@page import="com.cptu.egp.eps.model.table.TblCurrencyMaster"%>
<%@page import= "com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderCurrency"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.web.servicebean.Comparison" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    int iCurrencyCount = 0;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Configure Currency</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">

            //function to check/uncheck all checkboxs
            checked=false;
            function checkedAll (frmCurrencyConfig) {
                $(".err").remove();
                var aa= document.getElementById('frmCurrencyConfig');
                if (checked == false)
                {
                    checked = true;
                }
                else
                {
                    checked = false;
                }
                for (var i =0; i < aa.elements.length; i++)
                {
                    if(aa.elements[i].id == 'chkDefaultCurrency')
                    {
                        continue;
                    }
                    aa.elements[i].checked = checked;
                }
                if(checked)
                {
                    var vTotalCurrency = document.getElementById("hidTotalCurrency").value;
                    for(var i=2;i<=vTotalCurrency;i++)
                    {
                        checkExchageRate('txt_'+i,'chk_'+i);
                    }
                }
            }

            function validate()
            {
                //alert("In validate()");
                $(".err").remove();
                var valid = true;
                var vTotalCurrency = document.getElementById("hidTotalCurrency").value;
                for (var i =2; i <= vTotalCurrency; i++)
                {
                    var vCheckSelected = "chk_"+i;
                    var vExRateField = "txt_"+i;
                    if(document.getElementById(vCheckSelected).checked){
                        
                        var vDivErrId = "err"+vExRateField;
                        //alert("vExRateField : "+vExRateField+"value : "+document.getElementById(vExRateField).value+"vDivErrId :"+vDivErrId);
                        if(document.getElementById(vExRateField).value == '')
                        {
                            var vErrDiv = document.createElement('div');
                            vErrDiv.setAttribute('id',vDivErrId);
                            vErrDiv.innerHTML = 'Please enter exchange rate';
                            vErrDiv.className='err';
                            vErrDiv.style.color = 'red';
                            document.getElementById(vExRateField).parentNode.appendChild(vErrDiv);
                            valid = false;
                        }
                        else
                        {
                            var vExRate = document.getElementById(vExRateField).value;
                            document.getElementById(vExRateField).value = String(parseFloat(vExRate).toFixed(5));
                        }
                    }
                    else
                    {
                        document.getElementById(vExRateField).value="";
                    }
                }
                if(!valid)
                    return false;
                else{
                    return true;
                }
            }

            //function to validate Exchange Rate
            function checkExchageRate(txtField,chkField)
            {
                var vFlag = 0;
                var vExngRate = document.getElementById(txtField).value;
                var vIndex = vExngRate.indexOf('.', 0);
                var vDivErrId = "err"+txtField;
               
                //alert("vIndex : "+vIndex+"vExngRate : "+vExngRate+"chk : "+chkField+"vDivErrId : "+vDivErrId);
                if(document.getElementById(chkField).checked){

                    //remove any previous error div if present
                    if(document.getElementById(vDivErrId) != null)
                    {
                        //alert(document.getElementById(vDivErrId).innerHTML);
                        document.getElementById(txtField).parentNode.removeChild(document.getElementById(vDivErrId));
                    }
                    
                    if(vExngRate == '')//when Exchange Rate is entered by user
                    {
                        //alert("when exchange rate is empty");
                        var vErrDiv = document.createElement('div');
                        vErrDiv.setAttribute('id',vDivErrId);
                        vErrDiv.innerHTML = 'Please enter exchange rate';
                        vErrDiv.className='err';
                        vErrDiv.style.color='red';
                        document.getElementById(txtField).parentNode.appendChild(vErrDiv);
                        return false;
                    }
                    else if(!ValidateFloatFields(document.frmCurrencyConfig,txtField,'Desc',false,false) || document.getElementById(txtField).value == '.')
                    {//When Exchange Rate is not valid
                        //alert("when exchange rate is not valid");
                        var vErrDiv = document.createElement('div');
                        vErrDiv.setAttribute('id',vDivErrId);
                        vErrDiv.innerHTML = 'Please enter valid exchange rate';
                        vErrDiv.className='err';
                        vErrDiv.style.color='red';
                        document.getElementById(txtField).parentNode.appendChild(vErrDiv);
                        document.getElementById(txtField).value = "";
                        return false;
                    }
                    else if(vIndex != -1)
                    {
                        for(var i = 0 ;i<vExngRate.length ;i++)
                        {
                            if(vExngRate.charAt(i) == '.')
                            {
                                vFlag++;
                            }
                        }
                        var vScale = vExngRate.substring(vIndex+1, vExngRate.length).length;
                        if(vFlag >= 2)
                        {
                            var vErrDiv = document.createElement('div');
                            vErrDiv.setAttribute('id',vDivErrId);
                            vErrDiv.innerHTML = 'Please enter valid exchange rate';
                            vErrDiv.className='err';
                            vErrDiv.style.color='red';
                            document.getElementById(txtField).parentNode.appendChild(vErrDiv);
                            document.getElementById(txtField).value = "";
                            return false;
                        }
                        else if(vScale > 5)
                        {//When Exchange Rate is valid but scale digits are more than 5.
                            var vErrDiv = document.createElement('div');
                            vErrDiv.setAttribute('id',vDivErrId);
                            vErrDiv.innerHTML = 'Please enter exhange rate with only five scale digits';
                            vErrDiv.className='err';
                            vErrDiv.style.color='red';
                            document.getElementById(txtField).parentNode.appendChild(vErrDiv);
                            document.getElementById(txtField).value = "";
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    if(document.getElementById(vDivErrId) != null)
                    {
                        //alert(document.getElementById(vDivErrId).innerHTML);
                        document.getElementById(txtField).parentNode.removeChild(document.getElementById(vDivErrId));
                    }
                    var vErrDiv = document.createElement('div');
                    vErrDiv.setAttribute('id',vDivErrId);
                    vErrDiv.innerHTML = 'Please tick appropriate checkbox to enter exchange rate';
                    vErrDiv.className='err';
                    vErrDiv.style.color = 'red';
                    document.getElementById(txtField).parentNode.appendChild(vErrDiv);
                }
            }
            function ValidateFloatFields(thisform,Fields,Desc,AllowComma)
            {
                //alert("In ValidateFloatFields()");
                var tmpVal='';
                var FieldArray=new Array();
                var DescArray=new Array();
                var tmp=0;
                var tmpMsg='';
                // Create array for Field list
                tmpVal=Fields;
                do
                {
                    tmp=tmpVal.indexOf('|');
                    if (tmp == -1)
                    {
                        if (tmpVal != '')
                        {
                            FieldArray[FieldArray.length]=tmpVal;
                            tmpVal='';
                        }
                    } else {
                        FieldArray[FieldArray.length]=tmpVal.substring(0,tmp);
                        tmpVal=tmpVal.substring(tmp + 1);
                    }
                }
                while (tmpVal != '');
                // Create array for Desc list
                tmpVal=Desc;
                do
                {
                    tmp=tmpVal.indexOf('|');
                    if (tmp == -1)
                    {
                        if (tmpVal != '')
                        {
                            DescArray[DescArray.length]=tmpVal;
                            tmpVal='';
                        }
                    } else {
                        DescArray[DescArray.length]=tmpVal.substring(0,tmp);
                        tmpVal=tmpVal.substring(tmp + 1);
                    }
                }
                while (tmpVal != '');
                // Check to see if passed strings are of equal length
                if (FieldArray.length != DescArray.length)
                {
                    //alert('Fatal error: ValidateFloat - Passed lists do not have the same length');
                    return false;
                }
                // Validate fields
                for (i=0; i<FieldArray.length;i++)
                {
                    tmpMsg='';
                    eval('tmpVal=thisform.' + FieldArray[i] + '.value');
                    if (!ValFloat(tmpVal,AllowComma))
                    {
                        if (AllowComma != 1 && tmpVal.indexOf(',') != -1) tmpMsg=' without commas';
                        //alert(DescArray[i] + ' must be a numeric value' + tmpMsg + '.');
                        //eval('thisform.' + FieldArray[i] + '.focus()')
                        return false;
                    }
                }
                return true;
            }

            function ValFloat(ObjectValue,AllowComma)
            {
                // Used Internally by ValidateInt, ValidateFloat

                //Returns true if value is a number or is NULL
                //otherwise returns false

                if (ObjectValue.length == 0) return true;

                //Returns true if value is a number defined as
                //   having an optional leading + or -.
                //   having at most 1 decimal point.
                //   otherwise containing only the characters 0-9.
                var start_format = " .+-0123456789";
                var number_format = " .0123456789";
                if (AllowComma == 1) number_format=number_format + ',';
                var check_char;
                var decimal = false;
                var trailing_blank = false;
                var digits = false;

                //The first character can be + - .  blank or a digit.
                check_char = start_format.indexOf(ObjectValue.charAt(0))
                //Was it a decimal?
                if (check_char == 1)
                    decimal = true;
                else if (check_char < 1)
                    return false;

                //Remaining characters can be only . or a digit, but only one decimal.
                for (var i = 1; i < ObjectValue.length; i++)
                {
                    check_char = number_format.indexOf(ObjectValue.charAt(i))
                    if (check_char < 0)
                        return false;
                    else if (check_char == 1)
                    {
                        if (decimal)		// Second decimal.
                            return false;
                        else
                            decimal = true;
                    }
                    else if (check_char == 0)
                    {
                        if (decimal || digits) trailing_blank = true;
                        // ignore leading blanks
                    }
                    else if (trailing_blank)
                        return false;
                    else
                        digits = true;
                }
                //All tests passed, so...
                return true
            }

            function resetErrorFields(chkField,txtField)
            {
                var vDivErrId = "err"+txtField;
                if(!document.getElementById(chkField).checked){
                    document.getElementById(txtField).value="";
                    if(document.getElementById(vDivErrId) != null)
                    {
                        //alert(document.getElementById(vDivErrId).innerHTML);
                        document.getElementById(txtField).parentNode.removeChild(document.getElementById(vDivErrId));

                    }
                }
                else
                {
                    checkExchageRate(txtField,chkField);
                }
            }


        </script>

    <body>
        <div class="dashboard_div">
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <%
                        //Default currency details
                        int iDefaultCurrencyId = 0;
                        String strDefaultCurrencyName = "Taka";
                        BigDecimal bigDefCurrencyExRate = new BigDecimal(1.0000);

                        String tenderid = request.getParameter("tenderid");
                        pageContext.setAttribute("tenderId", tenderid);
                        

                        CurrencyMasterService currencyMasterService = (CurrencyMasterService) AppContext.getSpringBean("CurrencyMasterService");
                        List<TblCurrencyMaster> listCurrencyMaster = currencyMasterService.getAllCurrency();
                        
                        HashMap<String, BigDecimal> hashConfiguredCurrency = (HashMap<String, BigDecimal>) request.getSession().getAttribute("hashConfiguredCurrency");
                        if (hashConfiguredCurrency != null && hashConfiguredCurrency.size() > 0) {
                            hashConfiguredCurrency.remove(strDefaultCurrencyName);
                        }

            %><div class="contentArea_1">
                <div class="pageHead_1">Configure Currency<span style="float: right;"><a class="action-button-goback" href="Notice.jsp?tenderid=<%=tenderid%>">Go back to Dashboard</a></span></div>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <form action="<%=request.getContextPath()%>/TenderCurrencyConfigServlet?tenderid=<%=tenderid%>" method="post" id="frmCurrencyConfig" name="frmCurrencyConfig">
                    <div class="tabPanelArea_1">
                        <!--   <div class="tableList_1 t-align-right">

                               Field Marked (<span class="mandatory">*</span>) is Mandatory
                           </div> -->
                        <table class="tableList_1 t_space" width="100%" cellspacing="0">
                            <tbody>
                                <tr>

                                    <th colspan="4" class="t-align-left ff">Enter Currency Details</th>
                                </tr>
                                <tr>
                                    <td class="t-align-center ff" width="10%"><input type='checkbox' name='chkCurrecyConfig' onclick='checkedAll(frmCurrencyConfig);'></td>
                                    <td class="t-align-center ff" width="10%">Sr No</td>
                                    <td class="t-align-center ff" width="30%">Currency Name</td>
                                    <td class="t-align-center ff" width="50%">Exchage Rate</td>
                                </tr>
                                <tr class="loginInfoBar" >
                                    <td class="t-align-center"><input type="checkbox" name="chkDefaultCurrency" id="chkDefaultCurrency" value="" checked disabled /></td>
                                    <td class="t-align-center">1</td>
                                    <td class="t-align-center"><%=strDefaultCurrencyName%></td>
                                    <td class="t-align-center"><input type="text" class="formTxtBox_1" name="txt_1" id="txt_1" value="<%=bigDefCurrencyExRate.setScale(4)%>" style="width:50px;" readonly disabled/></td>
                                </tr>
                                <%

                                            iCurrencyCount = 1;
                                            for (TblCurrencyMaster currencyMaster : listCurrencyMaster) {
                                                String strCurrencyName = currencyMaster.getCurrencyName();
                                                int iCurrencyId = currencyMaster.getCurrencyId();
                                                if (strCurrencyName.equalsIgnoreCase(strDefaultCurrencyName)) {
                                                    iDefaultCurrencyId = iCurrencyId;
                                                    continue;
                                                } else {
                                                    iCurrencyCount++;

                                %>
                                <tr>
                                    <%
                                                                                        if (hashConfiguredCurrency != null && hashConfiguredCurrency.containsKey(strCurrencyName)) {
                                    %>
                                    <td class="t-align-center"><input type="checkbox" name="chk_<%=iCurrencyCount%>" id="chk_<%=iCurrencyCount%>" value="<%=iCurrencyId%>" checked  onclick="resetErrorFields('chk_<%=iCurrencyCount%>','txt_<%=iCurrencyCount%>')" /></td>
                                        <%} else {%>
                                    <td class="t-align-center"><input type="checkbox" name="chk_<%=iCurrencyCount%>" id="chk_<%=iCurrencyCount%>" value="<%=iCurrencyId%>" onclick="resetErrorFields('chk_<%=iCurrencyCount%>','txt_<%=iCurrencyCount%>')"  /></td>
                                        <%}%>
                                    <td class="t-align-center"><%=iCurrencyCount%></td>
                                    <td class="t-align-center"><%=strCurrencyName%></td>
                                    <%
                                                                                        if (hashConfiguredCurrency != null && hashConfiguredCurrency.containsKey(strCurrencyName)) {
                                    %>
                                    <td class="t-align-center"><input type="text" class="formTxtBox_1" name="txt_<%=iCurrencyCount%>" id="txt_<%=iCurrencyCount%>" value="<%=hashConfiguredCurrency.get(strCurrencyName)%>" style="width:50px;" onchange=" return checkExchageRate('txt_<%=iCurrencyCount%>','chk_<%=iCurrencyCount%>')"  /></td>
                                        <%} else {%>
                                    <td class="t-align-center"><input type="text" class="formTxtBox_1" name="txt_<%=iCurrencyCount%>" id="txt_<%=iCurrencyCount%>" value="" style="width:50px;"  onchange=" return checkExchageRate('txt_<%=iCurrencyCount%>','chk_<%=iCurrencyCount%>')"  /></td>
                                        <%}%>
                                </tr>
                                <%}
                                            }%>
                            </tbody></table>
                        <div class="t-align-center t_space">
                            <label class="formBtn_1"><input name="btnConfigureCurrency" id="btnConfigureCurrency" value="Configure" type="submit" onclick=" return validate();" ></label>
                        </div>
                    </div>
                    <div>&nbsp;</div>
                    <input type="hidden" id="hidTotalCurrency" name="hidTotalCurrency" value="<%=iCurrencyCount%>" />
                    <input type="hidden" id="hidDefaultCurrencyId" name="hidDefaultCurrencyId" value="<%=iDefaultCurrencyId%>" />
                    <input type="hidden" value="configureTenderCurrency" name="action" id="action" />
                </form>
            </div>
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
