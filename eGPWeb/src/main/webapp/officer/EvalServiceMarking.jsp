<%-- 
    Document   : EvalServiceMarking
    Created on : Mar 16, 2011, 12:00:21 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalSerFormDetail"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <title>Form Evaluation</title>
        <jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />
        <jsp:useBean id="evalSerCertiSrBean"  class="com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean" />       
    </head>
    <body>
        <%
                    int userId = 0;
                    if (!"".equalsIgnoreCase(request.getParameter("uId"))) {
                        userId = Integer.parseInt(request.getParameter("uId"));
                    }

                    int formId = 0;
                    if (request.getParameter("formId") != null) {
                        formId = Integer.parseInt(request.getParameter("formId"));
                    }

                    int tenderId = 0;
                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }

                    int flagMarks = 0;

                    int evalBy = 0;
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        evalBy = Integer.parseInt(hs.getAttribute("userId").toString());
                    }

                    boolean isEdit = false;
                    List<Object[]> markList = evalSerCertiSrBean.getDetailsEvalSerMark(tenderId, userId, formId,evalBy);
                    if (markList != null && !markList.isEmpty()) {
                        isEdit = true;
                    }
                    boolean isREOI = false;
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    if(commonService.getEventType(String.valueOf(tenderId)).toString().equals("REOI")){
                        isREOI = true;
                    }
                    String hideHTML = isREOI ? " style='display: none;' " : " ";
                    TenderCommonService tenderCommonService = (TenderCommonService)AppContext.getSpringBean("TenderCommonService");
					//Change by dohatec for re-evaluation
                    EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                    int evalCount = evalService.getEvaluationNo(tenderId);
        %>
        <div class="mainDiv">
            <div class="dashboard_div">                
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="fixDiv">                    
                    <form id="frmEvalSerMarking" name="frmEvalSerMarking" method="post" action="<%=request.getContextPath()%>/ServletEvalCertiService?funName=evalBidderMarkInsert&action=<%=isEdit %>" onsubmit="return validation();">
                        <input type="hidden" name="eventType" value="<%=isREOI%>"/>
                        <input type="hidden" name="evalCount" value="<%=evalCount%>"/>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr valign="top">
                                <td class="contentArea_1">
                                    <div class="pageHead_1" id="divFormName">Form Evaluation<span style="float:right;"><a href="SeekEvalClari.jsp?uId=<%=userId %>&tenderId=<%=tenderId %>&st=cl&lnk=et&comType=TEC" class="action-button-goback">Go Back To Dashboard</a></span></div>
                                    <% for (SPTenderCommonData companyList : tenderCommonService.returndata("GetCompanynameByUserid", request.getParameter("uId"), "0")) {
                                    %>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <th colspan="2" class="t_align_left ff">Company Details</th>
                                        </tr>
                                        <tr>
                                            <td width="22%" class="t-align-left ff">Company Name :</td>
                                            <td width="78%" class="t-align-left"><%=companyList.getFieldName1()%></td>
                                        </tr>
                                    </table>
                                        <%@include  file="../officer/TSCComments.jsp" %>

                                    <%
                                                } // END FOR LOOP OF COMPANY NAME
                                    %>
                                    <table width="100%" cellspacing="10" class="tableList_1 t_space">
                                        <tr>
                                            <td width="100" class="ff">Form Name : </td>
                                            <td><%=commonService.getFormName(String.valueOf(formId))%></td>
                                        </tr>
                                    </table>
                                    <%
                                                int tableId = 0;
                                                int tblCnt1 = 0;
                                                short cols = 0;
                                                short rows = 0;
                                                String tableName = "";
                                                String isMultiTable = "";
                                                List<Object> getBidId = tenderBidSrBean.getBidId(userId, formId);//fetch bid id
                                                List<CommonFormData> formTables = tenderBidSrBean.getFormTables(formId);
                                                int bidId = 0;
                                                int count = 0;
                                                for (Object obj : getBidId) {
                                                    bidId = (Integer) obj;
                                                    for (CommonFormData formData : formTables) {
                                                        tableId = formData.getTableId();

                                                        List<CommonFormData> tableInfo = tenderBidSrBean.getFormTablesDetails(tableId);
                                                        if (tableInfo != null) {
                                                            if (tableInfo.size() >= 0) {
                                                                tableName = tableInfo.get(0).getTableName();
                                                                cols = tableInfo.get(0).getNoOfCols();
                                                                rows = tableInfo.get(0).getNoOfRows();
                                                                isMultiTable = tableInfo.get(0).getIsMultipleFilling();
                                                            }
                                                            tableInfo = null;
                                                        }
                                                        


                                    %>
                                    <table width="100%" cellspacing="10" class="tableList_1 t_space">
                                        <tr>
                                            <td width="100" class="ff">Table Name : </td>
                                            <td><%=tableName%></td>
                                        </tr>
                                    </table>
                                    <jsp:include page="ViewEvalBidformTable.jsp" flush="true" >
                                        <jsp:param name="tableId" value="<%=tableId%>" />
                                        <jsp:param name="cols" value="<%=cols%>" />
                                        <jsp:param name="rows" value="<%=rows%>" />
                                        <jsp:param name="TableIndex" value="<%=tblCnt1%>" />
                                        <jsp:param name="isMultiTable" value="<%=isMultiTable%>" />
                                        <jsp:param name="bidId" value="<%=bidId%>" />
                                        <jsp:param name="action" value="Edit" />
                                        <jsp:param name="uId" value="<%=userId%>" />
                                        <jsp:param name="scriptShow" value="false" />
                                    </jsp:include>

                                    <% tblCnt1++;
                                                                                        }
                                                                                        if (isEdit) { %>
                                    <table width="100%" cellspacing="10" class="tableList_1 t_space">
                                        <tr>
                                                <th>Criteria</th>
                                                <th>Sub Criteria</th>
                                                <th <%=hideHTML%> >Points Allocated</th>
                                                <th>Rating</th>
                                                <th <%=hideHTML%> >Rating Weightage</th>
                                                <th <%=hideHTML%> >Rated Score</th>
                                                <th>Remarks if any</th>
                                        </tr>
                                        <% int chk=0;
                                        List<Object[]> details = evalSerCertiSrBean.getDetailsEvalSerMark(tenderId, userId, formId,evalBy,bidId);
                                        for(Object[] objects : details){%>
                                        <tr <%=(!isREOI && objects[3].toString().equals("0")) ? "style=\"display:none;\"" : ""%>>
                                            <td><%=objects[9]%></td>
                                            <td style="text-align: center;"><%=objects[0] %>
                                                <input type="hidden" name="subCriteriaId_<%=count%>" value="<%=objects[8] %>"/>
                                            </td>                                            
                                            <td <%=hideHTML%> class="t-align-center"><label id="marks_<%=flagMarks%>"><%=objects[3] %></label>
                                                <input type="hidden" name="maxMarks_<%=count%>" value="<%=objects[3] %>"/>
                                                <input type="hidden" name="esFormDetailId_<%=flagMarks%>" value="<%=objects[1] %>"/>
                                            </td>                                            
                                            <td style="text-align: center;">
                                                <select id="cmbRating_<%=flagMarks%>" name="rating_<%=count%>" onchange="calculate(this);" class="formTxtBox_1">
                                                    <option value="">--select--</option>

                                                    <!--<option < % if(objects[5].equals("Very Poor")){%>selected< %}%> value="0">Very Poor</option>
                                                    <option < % if(objects[5].equals("Poor")){%>selected< %}%> value="0.4">Poor</option>
                                                    <option < % if(objects[5].equals("Good")){%>selected< %}%> value="0.7">Good</option>
                                                    <option < % if(objects[5].equals("Very Good")){%>selected< %}%> value="0.9">Very Good</option>
                                                    <option < % if(objects[5].equals("Excellent")){%>selected< %}%> value="1">Excellent</option>-->

                                                    <option <% if(objects[5].equals("Poor")){%>selected<%}%> value="0.4">Poor</option>
                                                    <option <% if(objects[5].equals("Average")){%>selected<%}%> value="0.5">Average</option>
                                                    <option <% if(objects[5].equals("Satisfactory")){%>selected<%}%> value="0.7">Satisfactory</option>
                                                    <option <% if(objects[5].equals("Most Satisfactory")){%>selected<%}%> value="0.8">Most Satisfactory</option>
                                                    <option <% if(objects[5].equals("Good")){%>selected<%}%> value="0.9">Good</option>
                                                    <option <% if(objects[5].equals("Very Good")){%>selected<%}%> value="0.95">Very Good</option>
                                                    <option <% if(objects[5].equals("Excellent")){%>selected<%}%> value="1">Excellent</option>
                                                </select>
                                                    <input value="<%=objects[5]%>" name="ratingWeightage_<%=count%>" type="hidden" id="hdnRatingWeightage_<%=flagMarks%>" />
                                            </td>                                            
                                            <td <%=hideHTML%> class="t-align-center"><label id="lblWeightage_<%=flagMarks%>"><%=objects[6] %></label>
                                                <input name="ratedScore_<%=count%>" value="<%=objects[6] %>" id="hdnRatedScore_<%=flagMarks%>" type="hidden"/>
                                            </td>
                                            <td <%=hideHTML%> class="t-align-center"><label id="lblRatedScore_<%=flagMarks%>"><%=objects[7] %></label>
                                                <input name="actualMarks_<%=count%>" value="<%=objects[7] %>" id="hdnactualMarks_<%=flagMarks%>" type="hidden"/>
                                            </td>                                            
                                            <td><textarea class="formTxtBox_1" onblur="return validationMaxSize();" id="txtaRemarks_<%=flagMarks%>" name="remarks_<%=count%>" cols="50" rows="5"><%=objects[2] %></textarea></td>
                                        </tr>
                                        <% flagMarks++;
                                        } %>
                                        <tr>

                                        </tr>
                                    </table>
                                    <% } else {%>
                                    <table width="100%" cellspacing="10" class="tableList_1 t_space">
                                        <tr>
                                                <th>Criteria</th>
                                                <th>Sub Criteria</th>
                                                <th <%=hideHTML%> >Points Allocated</th>
                                                <th>Rating</th>
                                                <th <%=hideHTML%> >Rating Weightage</th>
                                                <th <%=hideHTML%> >Rated Score</th>
                                                <th>Remarks if any</th>
                                        </tr>
                                        <%
                                                                                for (Object[] listing : evalSerCertiSrBean.getEvalServiceMarks(formId)) {%>
                                                                                <tr <%=(!isREOI && listing[1].toString().equals("0")) ? "style=\"display:none;\"" : ""%>>
                                            <td><%=listing[3]%></td>
                                            <td style="text-align: center;"><%=listing[0]%>
                                                <input type="hidden" name="subCriteriaId_<%=count%>" value="<%=listing[2]%>"/>
                                            </td>                                            
                                            <td <%=hideHTML%> ><label id="marks_<%=flagMarks%>"><%=listing[1]%></label>
                                                <input type="hidden" name="maxMarks_<%=count%>" value="<%=listing[1]%>"/>
                                            </td>                                            
                                            <td style="text-align: center;">
                                                <select id="cmbRating_<%=flagMarks%>" name="rating_<%=count%>" onchange="calculate(this);" class="formTxtBox_1">
                                                    <option value="">--select--</option>
                                                    
                                                    <!--<option value="0">Very Poor</option>
                                                    <option value="0.4">Poor</option>
                                                    <option value="0.7">Good</option>
                                                    <option value="0.9">Very Good</option>
                                                    <option value="1">Excellent</option>-->
                                                    
                                                    <option value="0.4">Poor</option>
                                                    <option value="0.5">Average</option>
                                                    <option value="0.7">Satisfactory</option>
                                                    <option value="0.8">Most Satisfactory</option>
                                                    <option value="0.9">Good</option>
                                                    <option value="0.95">Very Good</option>
                                                    <option value="1" <%=(!isREOI && listing[1].toString().equals("0")) ? "selected" : ""%>>Excellent</option>
                                                </select>
                                                <input value="<%=(!isREOI && listing[1].toString().equals("0")) ? "Excellent" : ""%>" name="ratingWeightage_<%=count%>" type="hidden" id="hdnRatingWeightage_<%=flagMarks%>" />
                                            </td>                                            
                                            <td  <%=hideHTML%>  class="t-align-center"><label id="lblWeightage_<%=flagMarks%>"></label>
                                                <input name="ratedScore_<%=count%>" id="hdnRatedScore_<%=flagMarks%>" type="hidden" value="<%=(!isREOI && listing[1].toString().equals("0")) ? "1" : ""%>"/>
                                            </td>
                                            <td  <%=hideHTML%>  class="t-align-center"><label id="lblRatedScore_<%=flagMarks%>"></label>
                                                <input name="actualMarks_<%=count%>" id="hdnactualMarks_<%=flagMarks%>" type="hidden" value="<%=(!isREOI && listing[1].toString().equals("0")) ? "0" : ""%>"/>
                                            </td>                                            
                                            <td><textarea class="formTxtBox_1" onblur="return validationMaxSize();" id="txtaRemarks_<%=flagMarks%>" name="remarks_<%=count%>" cols="50" rows="5"></textarea></td>
                                        </tr>
                                        <% flagMarks++;
                                                                                }%>
                                    </table>
                                    <% }%>
                                    <input type="hidden" name="bidderId_<%=count%>" value="<%=bidId%>"/>
                                    <%count++;
                                                }%>
                                    <div class="t-align-center t_space">
                                        <label class="formBtn_1"><input  id="Submit" name="Submit" value="Submit" type="submit" /></label>
                                        <input type="hidden" name="tenderFormId" value="<%=formId%>"/>
                                        <input type="hidden" name="userId" value="<%=userId%>"/>
                                        <input type="hidden" name="tenderId" value="<%=tenderId%>"/>
                                        <input type="hidden" name="flagMarks" value="<%=flagMarks%>"/>
                                        <input type="hidden" name="count" value="<%=count%>"/>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
<script type="text/javascript">
    function calculate(obj){
        var temp = obj.id;
        var id = temp.split("_")[1];
        var per = obj.value;
        $('#lblWeightage_'+id).html(per);
        $('#hdnRatedScore_'+id).val(per);
        var points = Math.round(eval($('#marks_'+id).html()*per)*Math.pow(10,2))/Math.pow(10,2);        
        $('#lblRatedScore_'+id).html(points);
        $('#hdnactualMarks_'+id).val(points);

        $('#hdnRatingWeightage_'+id).val(obj.options[obj.selectedIndex].text);        

    }

    function validation(){
        $(".err").remove();
        var count = 0;
        $("select").each(function(){
            if($(this).val() == ""){
                $(this).parent().append("<div class='err' style='color:red;'>Please select Rating you wish to give.</div>");
                count++;
            }
        });
        //         var counter = < %=flagMarks%>;
        //        for (var i = 0; i < counter; i++) {
        //alert(document.getElementById("txtaRemarks_"+i).value);
        //            if(document.getElementById("txtaRemarks_"+i).value != ""){
        //                $(this).append("<div class='err' style='color:red;'>Maximum 500 characters are allowed.</div>");
        //            }
        $("textarea").each(function(){
            if($(this).val() != ""){
                if($(this).val().length > 500){
                    $(this).parent().append("<div class='err' style='color:red;'>Maximum 500 characters are allowed.</div>");
                    count++;
                }
            }
        });
        if(count > 0){
            return false;
        }else{
            $('#Submit').attr('disabled', true);
        }
    }

    function validationMaxSize(){
        
    }
</script>
<%
            tenderBidSrBean = null;
            evalSerCertiSrBean = null;
%>