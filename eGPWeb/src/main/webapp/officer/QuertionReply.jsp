<%-- 
    Document   : QuertionReply
    Created on : Dec 8, 2010, 8:40:28 PM
    Author     : parag
--%>

<%@page import="java.net.URLDecoder"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Post Reply</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <%--<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>--%>
        <%--<script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        

        <script type="text/javascript">
            $(document).ready(function(){

                $("#frmPreTendQuery").validate({
                    rules:{
                        txtReply:{required:true,maxlength:1000}
                    },
                    messages:
                        {
                        txtReply:{ required:"<div class='reqF_1'>Please enter Reply</div>",
                            maxlength:"<div class='reqF_1'>Maximum 1000 characters are allowed.</div>"
                        }

                    }
                });
            });

        </script>

    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp"%>
                <%
                            int userId = 0;
                            byte suserTypeId = 0;
                            int tenderId = 1;
                            int queryId = 1;
                            int cuserId = 0;

                            if (session.getAttribute("userTypeId") != null) {
                                suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                            }
                            if (session.getAttribute("userId") != null) {
                                userId = Integer.parseInt(session.getAttribute("userId").toString());
                            }
                            if (request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId"))) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }
                            if (request.getParameter("queryId") != null && !"null".equalsIgnoreCase(request.getParameter("queryId"))) {
                                queryId = Integer.parseInt(request.getParameter("queryId"));
                            }
                            if (request.getParameter("cuserId") != null && !"null".equalsIgnoreCase(request.getParameter("cuserId"))) {
                                cuserId = Integer.parseInt(request.getParameter("cuserId"));
                            }
                            
                            SPTenderCommonData getQueryText = preTendDtBean.getDataFromSP("GetQuestionText", queryId, 0).get(0);

                %>
                <div class="dashboard_div">
                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1">Post Reply</div>
                        <%
                                    // Variable tenderId is defined by u on ur current page.
                                    pageContext.setAttribute("tenderId", tenderId);
                        %>
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <div>&nbsp;</div>
                        <% pageContext.setAttribute("tab", 11);%>
                        <jsp:include page="officerTabPanel.jsp" >
                            <jsp:param name="tab" value="11" />
                        </jsp:include>

                        <div>&nbsp;</div>

                        <form method="post" id="frmPreTendQuery" action="<%=request.getContextPath()%>/PreTendQuerySrBean?action=replyQuestion">
                            <div class="tabPanelArea_1">
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <td width="16%" class="t-align-left ff">Query :</td>
                                        <td width="84%" class="t-align-left"><%=URLDecoder.decode(getQueryText.getFieldName2(),"UTF-8")%></td>
                                    </tr>
                                    <%
                                         TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                         java.util.List<SPTenderCommonData> docData=null;
                                         docData =  tenderCS.returndata("QuestionDocumentsBy", ""+tenderId,""+queryId);
                                         if(!docData.isEmpty()){
                                    %>
                                    <tr>
                                        <td class="t-align-left ff">Download Reference Document : </td>
                                        <td class="t-align-left"><a href="<%=request.getContextPath()%>/tenderer/PreTenderQueryDocUpload.jsp?tenderId=<%=tenderId%>&queryId=<%=queryId%>&userId=<%=cuserId%>&actionType=download&docType=Question" target="_blank">Download</a></td>
                                    </tr>
                                    <%}%>
                                </table>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <td width="16%" class="t-align-left ff">Reply :<span class="mandatory">*</span></td>
                                        <td width="84%" class="t-align-left">
                                            <label>
                                                <textarea name="txtReply" rows="3" class="formTxtBox_1" id="txtReply" style="width:675px;"></textarea>
                                            </label>          </td>
                                    </tr>
                                    <tr class="tableList_1">
                                        <td class="t-align-left ff">Reference Document :</td>
                                        <td class="t-align-left"><a href="#" onClick="window.open('PreTenderReplyUpload.jsp?queryId=<%=queryId%>&tenderId=<%=tenderId%>&docType=Question','_blank','width=900,height=500,toolbar=no,menubar=no,location=no,titlebar=no')">Upload</a></td>
                                    </tr>
                                </table>

                                <div id="docData">
                                </div>
                                <div>&nbsp;</div>
                                <div class="t-align-center">
                                    <label class="formBtn_1">
                                        <input type="submit" name="button3" id="button3" value="Submit" />
                                        <input type="hidden" name="hidQueryId" id="hidQueryId" value="<%=queryId%>" />
                                        <input type="hidden" name="hidTenderId" id="hidTenderId" value="<%=tenderId%>" />
                                    </label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div>&nbsp;</div>

                    <!--Dashboard Content Part End-->
                    <!--Dashboard Footer Start-->
                    <!--Dashboard Footer End-->
                    <%@include file="../resources/common/Bottom.jsp" %>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<form id="form2" method="post">
</form>
<script>
    function checkStatus(){
        if($('#cmbPostQuery').val() == "Reply"){
            $('#divReply').show();
        }else if($('#cmbPostQuery').val() == "Hold"){
            $('#divReply').hide();

        }else if($('#cmbPostQuery').val() == "Ignore"){
            $('#divReply').hide();
        }
    }

    function checkRepQuery(){
        if(document.getElementById("checkbox").checked){
            $('#lblRepQuery').show();
        }else{
            $('#lblRepQuery').hide();
        }
    }

    function getDocData(){
    <%--$.ajax({
     url: "<%=request.getContextPath()%>/PreTendQuerySrBean?tenderId=<%=tenderId%>&queryId=<%=queryId%>&action=docData&docAction=QuestionReplyDocs",
     method: 'POST',
     async: false,
     success: function(j) {
         document.getElementById("docData").innerHTML = j;
     }
 });--%>
         $.post("<%=request.getContextPath()%>/PreTendQuerySrBean", {tenderId:<%=tenderId%>,queryId:<%=queryId%>,action:'docData',docAction:'QuestionReplyDocs'}, function(j){
             document.getElementById("docData").innerHTML = j;
         });
     }
     getDocData();

     function downloadFile(docName,docSize,tender){
    <%--$.ajax({
             url: "<%=request.getContextPath()%>/PreTenderQueryDocServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=download",
             method: 'POST',
             async: false,
             success: function(j) {
             }
     });--%>
             document.getElementById("form2").action="<%=request.getContextPath()%>/PreTenderReplyUploadServlet?docName="+docName+"&docSize="+docSize+"&tender="+tender+"&funName=download";
             document.getElementById("form2").submit();
         }

         function deleteFile(docName,docId,tender){
    <%-- $.ajax({
            url: "<%=request.getContextPath()%>/PreTenderReplyUploadServlet?docName="+docName+"&docId="+docId+"&tender="+tender+"&funName=remove",
            method: 'POST',
            async: false,
            success: function(j) {
                jAlert("Document Deleted."," Delete Document ", function(RetVal) {
                });
            }
    });--%>
            $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
                if(r == true){
                    $.post("<%=request.getContextPath()%>/PreTenderReplyUploadServlet", {docName:docName,docId:docId,tender:tender,funName:'remove'}, function(j){
                        jAlert("Document deleted successfully."," Delete Document ", function(RetVal) {
                            getDocData();
                        });
                    });
                }else{
                    getDocData();
                }
            });
   
  }

</script>

