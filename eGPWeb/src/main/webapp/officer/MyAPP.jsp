<%-- 
    Document   : MyAPP
    Created on : Nov 2, 2010, 8:32:52 PM
    Author     : rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>My App</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.generatepdf.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
    </head>
    <%
        int userid = 0;
        HttpSession hs = request.getSession();
        if (hs.getAttribute("userId") != null) {
            userid = Integer.parseInt(hs.getAttribute("userId").toString());
        }
    %>
    <script type="text/javascript">
        function changeParam() {
            document.getElementById("status").value = 'Approved';
            fillGrid();
        }
        function fillGrid() {

        <%-- var tempvariable='false';
         if(document.getElementById('cmbbudgetType').value=='')
         {
             document.getElementById('geterror').innerHTML='Please select Budget Type';
             tempvariable='true';
         }
         if (tempvariable=='true')
         {
             return false;
         }--%>
            $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
            //alert("<%=request.getContextPath()%>/AppSearchServlet?q=1&action=fetchData&financialYear="+document.getElementById('cmbFinancialYear').value+"&projectName="+document.getElementById('cmbprojectName').value +"&budgetType="+document.getElementById('cmbbudgetType').value+"&status="+document.getElementById("status").value);
            jQuery("#list").jqGrid({
                
                url: '<%=request.getContextPath()%>/AppSearchServlet?q=1&action=fetchData&financialYear=' + document.getElementById("cmbFinancialYear").value + '&projectName=' + document.getElementById("cmbprojectName").value + '&budgetType=' + document.getElementById("cmbbudgetType").value + '&status=' + document.getElementById("cmbstatus").value + '&userId=' +<%=userid%> + '&appId=' + document.getElementById("txtAppId").value + '&appCode=' + document.getElementById("txtAppCode").value + '&myChkVal=' + document.getElementById("chkBoxmyApp").value,
                datatype: "xml",
                height: 250,
                colNames: ['Sl. <br/> No.', 'APP ID', 'Letter Ref. No.', "Budget Type", "Project/Activity Name", "Dashboard"],
                colModel: [
                    {name: 'srNo', index: 'srNo', width: '5%', sortable: false, align: 'center'},
                    {name: 'appId', index: 'appId', width: '10%', sortable: true, align: 'center'},
                    {name: 'appCode', index: 'appCode', width: '25%', sortable: true, align: 'center'},
                    {name: 'budType', index: 'budgetType', width: '25%', sortable: true, align: 'center'},
                    {name: 'prjName', index: 'projectName', width: '20%', sortable: true, align: 'center'},
                    {name: 'dashboard', index: 'dashboard', width: '15%', sortable: false, align: 'center'}
                ],
                multiselect: false,
                paging: true,
                rowNum: 10,
                rowList: [10, 20, 30],
                pager: $("#page"),
                caption: "Annual Procurement Plan",
                autowidth: true,
                gridComplete: function () {
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
            }).navGrid('#page', {edit: false, add: false, del: false, search: false});
        }
        jQuery().ready(function () {
            //fillGrid();
        });
        function filllstatus() {
            document.getElementById("status").value = document.getElementById("cmbstatus").value;
        }
        function cleartemp()
        {
            if (document.getElementById('cmbbudgetType').value != '')
            {
                document.getElementById('geterror').innerHTML = '';
                tempvarialbe = 'false';
            }

            var financeYear = document.getElementById("cmbFinancialYear").options[document.getElementById("cmbFinancialYear").selectedIndex].text;
            var arrfinance = financeYear.split("-");

            var Date1 = arrfinance[0];
            var Date2 = arrfinance[1];


            if (document.getElementById("cmbbudgetType").value == "2") {
                $.post("<%=request.getContextPath()%>/APPServlet", {param1: 'Government', param4: 'getAppProject', startDate: Date1, endDate: Date2, funName: 'getAppProject'}, function (j) {
                    $("select#cmbprojectName").html(j);
                });
            }
            if (document.getElementById("cmbbudgetType").value == "1") {
                $.post("<%=request.getContextPath()%>/APPServlet", {param1: 'Aid or Grant', param4: 'getAppProject', startDate: Date1, endDate: Date2, funName: 'getAppProject'}, function (j) {
                    $("select#cmbprojectName").html(j);
                });
            }
            if (document.getElementById("cmbbudgetType").value == "3") {
                $.post("<%=request.getContextPath()%>/APPServlet", {param1: 'Own fund', param4: 'getAppProject', startDate: Date1, endDate: Date2, funName: 'getAppProject'}, function (j) {
                    $("select#cmbprojectName").html(j);
                });
            }
        }
    </script>
    <script>


    </script>
    <script type="text/javascript">
        function defaltGrid() {
            fillGrid();
        }
        function resetVal() {
            document.getElementById("cmbstatus").options[0].selected = true;
            fillGrid();
        }
    </script>
    <jsp:useBean id="appServlet" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
    <body onload="defaltGrid(); hide();">
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <div class="contentArea_1">
                    <div class="pageHead_1">My Annual Procurement Plan
                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('5');">Save as PDF</a>
                        </span>
                    </div>
                    <form id="frmMyApp" name="frmMyApp" method="post">
                        <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                        <div class="formBg_1 t_space">
                            <table cellspacing="10" class="formStyle_1" width="100%" id="tblSearchBox">
                                <tr>
                                    <%--<td colspan="4" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>--%>
                                </tr>
                                <tr>
                                    <%                                        String logUserId = "0";
                                        if (session.getAttribute("userId") != null) {
                                            logUserId = session.getAttribute("userId").toString();
                                        }
                                        appServlet.setLogUserId(logUserId);

                                        java.util.List<CommonAppData> appListDtBean = new java.util.ArrayList<CommonAppData>();
                                        appListDtBean = appServlet.getAPPDetails("FinancialYear", "", "");%>
                                    <td width="10%" class="ff">Financial Year :</td>
                                    <td width="40%">
                                        <select name="financialYear" class="formTxtBox_1" id="cmbFinancialYear" style="width:205px;">
                                            <%
                                                for (CommonAppData commonApp : appListDtBean) {
                                                    if (commonApp.getFieldName3().equals("Yes")) {
                                            %>
                                            <option selected="selected" value="<%=commonApp.getFieldName2()%>"><%=commonApp.getFieldName2()%></option>
                                            <%
                                            } else {
                                            %>
                                            <option value="<%=commonApp.getFieldName2()%>"><%=commonApp.getFieldName2()%></option>
                                            <% }
                                                }%>
                                        </select>
                                    </td>
                                    <td width="10%" class="ff">Budget type : </td>
                                    <td width="40%">
                                        <!--Change Budget Type Development to Capital and Revenue to Recurrent by Proshanto Kumar Saha-->
                                        <select name="budgetType" class="formTxtBox_1" id="cmbbudgetType" style="width: 205px">
                                            <option value="">- Select Budget Type -</option>
                                            <option value="1">Capital</option>
                                            <option value="2">Recurrent</option>
                                            <option value="3">Own Fund</option>
                                        </select>
                                        <br/><span id="geterror" class=""></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Project Name : </td>
                                    <td><%
                                        appListDtBean = appServlet.getAPPDetails("MyProject", "" + userid, "");
                                        %>
                                        <select name="projectName" class="formTxtBox_1" id="cmbprojectName" style="width: 205px">
                                            <option value="">- Select Project -</option>
                                            <%
                                                for (CommonAppData commonApp : appListDtBean) {
                                            %>
                                            <option value="<%=commonApp.getFieldName2()%>"><%=commonApp.getFieldName2()%></option>
                                            <%}%>
                                        </select>
                                    </td>
                                    <td class="ff">Status : </td>
                                    <td>
                                        <select name="status" class="formTxtBox_1" id="cmbstatus" style="width: 205px" onchange="filllstatus();">
                                            <option value="Pending" selected="selected">- Pending -</option>
                                            <option value="Approved">- Approved -</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">APP ID : </td>
                                    <td><input class="formTxtBox_1" style="width: 191px" type="text" name="appId" id="txtAppId"/></td>
                                    <td class="ff">Letter Ref. No. : </td>
                                    <td><input class="formTxtBox_1" type="text" name="appCode" style="width: 191px" id="txtAppCode"/></td>
                                </tr>
                                <tr>
                                    <td class="ff">Created By Me : </td>
                                    <td><input class="form-checkboxes" name="chkBoxmyApp" type="checkbox" value="" id="chkBoxmyApp" /></td>
                                    <td class="ff">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input type="hidden" id="status" value="Pending" name="status"/></td>
                                    <td class="ff">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td class="ff">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center"><label class="formBtn_1">
                                            <input type="button" name="btnSearchApp" id="btnSearchApp" value="Search APP" onclick="fillGrid();" />
                                        </label>
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input type="button" name="btnreset" id="btnreset" value="Reset" onclick="resetForm();" />
                                        </label>
                                    </td>
                                </tr>
                            </table></div>
                    </form>
                    <form name="frmresetForm"  id="frmresetForm" method="post" action="<%=request.getContextPath()%>/officer/MyAPP.jsp"></form>
                    <div>&nbsp;</div>
                    <!--                    <script>
                                            function test(type){
                                                if(type==1){
                                                    document.getElementById("linkPending").className = "sMenu";
                                                    document.getElementById("linkApproved").className = "";
                                                    document.getElementById("status").value = 'Pending';
                                                    fillGrid();
                                                }else{
                                                    document.getElementById("linkApproved").className = "sMenu";
                                                    document.getElementById("linkPending").className = "";
                                                }
                                            }
                                        </script>-->
                    <!--                    <ul class="tabPanel_1">
                                            <li>
                                                <a href="javascript:void(0);" id="linkPending" onclick="test(1);" class="sMenu">Pending</a>
                                            </li>
                                            <li><a href="javascript:void(0);" id="linkApproved" onclick="test(2); changeParam();">Approved</a></li>
                                        </ul>-->
                    <div class="tabPanelArea_1">

                        <div id="jQGrid" style="width: 100%;" align="center">
                            <div id="list" style="width: 100%;"></div>
                            <div id="page" style="width: 100%;"></div>
                        </div>
                    </div>
                </div>
                <!--For Generate PDF  Starts-->
                <form id="formstyle" action="" method="post" name="formstyle">
                    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                    <%
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                        String appenddate = dateFormat1.format(new Date());
                    %>
                    <input type="hidden" name="fileName" id="fileName" value="MyAPP_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="MyAPP" />
                </form>
                <!--For Generate PDF  Ends-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

                </div>
            </div>
        </body>
        <script type="text/javascript" language="Javascript">
            $('#chkBoxmyApp').change(function () {
                if ($('#chkBoxmyApp').is(':checked')) {
                    $('#chkBoxmyApp').val("<%=userid%>");
                } else {
                    $('#chkBoxmyApp').val("");
                }
            });
            function resetForm() {
                document.getElementById("frmresetForm").submit();
            }
        </script>
        <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabApp");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }
            
        function showHide()
        {
            if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
                document.getElementById('tblSearchBox').style.display = 'table';
                document.getElementById('collExp').innerHTML = '- Advanced Search';
            }else{
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Advanced Search';
            }
        }
        function hide()
        {
            document.getElementById('tblSearchBox').style.display = 'none';
            document.getElementById('collExp').innerHTML = '+ Advanced Search';
        }

        </script>
    <%
        appServlet = null;
    %>
</html>
