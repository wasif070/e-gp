<%-- 
    Document   : ViewEvalRpt
    Created on : Dec 15, 2010, 4:31:53 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="java.text.SimpleDateFormat" %>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Eval Report Documents</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->


            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <%
                        String evalRptId = "1";
                        String tenderId = "0";

                        if (request.getParameter("evalRptId") != null) {
                            evalRptId = request.getParameter("evalRptId");
                        }
                        if (request.getParameter("tenderId") != null) {
                            tenderId = request.getParameter("tenderId");
                        }


            %>
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="contentArea_1">
                <div class="pageHead_1">Evaluation Report Documents<span style="float:right;"><a href="EvalCommTSC.jsp?tenderid=<%=tenderId %>" class="action-button-goback">Go Back to Dashboard</a></span></div>
                <%-- <div class="t_space" align="right"><a href="Notice.jsp?tenderid=<%=postQualId%>" class="action-button-goback">Go Back Dashboard</a></div>--%>
                <% pageContext.setAttribute("tenderId", tenderId);%>

                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div class="dividerBorder t_space">&nbsp;</div>
                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <th width="4%" class="t-align-left">Sl. No.</th>
                        <th class="t-align-left" width="31%">File Name</th>
                        <th class="t-align-left" width="55%">File Description</th>
                        <th class="t-align-left" width="5%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-left" width="5%">Action</th>
                    </tr>
                    <%


                                if (request.getParameter("evalRptId") != null) {
                                    evalRptId = request.getParameter("evalRptId");
                                }


                                int docCnt = 0;
                                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
String status = "";
                                for (SPTenderCommonData sptcd : tenderCS.returndata("EvalRptDocInfo", evalRptId, "")) {
                                    docCnt++;
                                    for (SPTenderCommonData commonTenderDetails : tenderCS.returndata("getTscMemberStatus", tenderId, evalRptId)) {
                                     status = commonTenderDetails.getFieldName7();
                                    if (!status.equals("Pending")) {
                                        status = "Signed";
                                    }
                                    }
                    %>
                    <tr>
                        <td class="t-align-left"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <%if(status=="Signed"){%>
                        <td class="t-align-left">
                            <a href="<%=request.getContextPath()%>/ClarificationRefDocServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&evalRptId=<%=evalRptId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a></td>
                        <%}else{%>
                        <td class="t-align-left">
                            <a href="<%=request.getContextPath()%>/ClarificationRefDocServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&evalRptId=<%=evalRptId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                            &nbsp;
                            <a href="<%=request.getContextPath()%>/ClarificationRefDocServlet?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&evalRptId=<%=evalRptId%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                        </td>
                        <%}%>
                    </tr>

                    <%   if (sptcd != null) {
                                        sptcd = null;
                                    }
                                }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                </table>
                <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1 t_space">
                    <tr>
                        <th width="53%" class="ff">Committee Members </th>
                        <th width="24%">Signed Status </th>
                        <th width="18%">Signed Date &amp; Time </th>

                    </tr>
                    <%
                                tenderId = request.getParameter("tenderId");
                                

                                evalRptId = request.getParameter("evalRptId");
                                
                                boolean showVerify = false;
                                int memberCnt = 0;
                                int ApprovedMemCnt = 0;
                                for (SPTenderCommonData commonTenderDetails : tenderCS.returndata("getTscMemberStatus", tenderId, evalRptId)) {
                                    memberCnt++;
                                    // ApprovedMemCnt=Integer.parseInt(commonTenderDetails.getFieldName8());
                                    //out.println(ApprovedMemCnt);

                                     status = commonTenderDetails.getFieldName7();
                                    if (!status.equals("Pending")) {
                                        status = "Signed";
                                    }
                                    String currUserId = "";
                                    currUserId = commonTenderDetails.getFieldName1();
                                    if (currUserId != "") {
                                        if (Integer.parseInt(currUserId) == Integer.parseInt(session.getAttribute("userId").toString())) {
                                            showVerify = true;
                                        }
                                    }
                                    String date = commonTenderDetails.getFieldName8();
                                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a");

                                    SimpleDateFormat sd1 = new SimpleDateFormat("MM/dd/yyyy hh:mm a");


                                    

                    %>
                    <tr>
                        <td class="ff">

                            <% if (showVerify) {%>
                            <a href="EvalRptApp.jsp?tenderId=<%=tenderId%>&evalRptId=<%=evalRptId%>"><%=commonTenderDetails.getFieldName2()%></a>
                            <%} else {%><%=commonTenderDetails.getFieldName2()%><%}%>
                        </td><% %>
                        <td><%=status%></td>
                        <% if (date == null) {
                                                            date = "-";
                        %><td><%=date%></td>
                        <% } else {

                             format.format(sd1.parse(date));
                             
                        %><td><%=format.format(sd1.parse(date))%></td><%}%>

                    </tr>
                    <%}%>
                    <% if (memberCnt == 0) {%>
                    <tr>
                        <td colspan="3" class="ff">No members found!</td>
                    </tr>
                    <%}%>
                </table>
            </div>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <%-- <table width="100%" cellspacing="0" class="footerCss">
                 <tr>
                     <td align="left">e-GP &copy; All Rights Reserved
                         <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
                     <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a>


                    </td>
                </tr>
            </table>--%>
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->


        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

