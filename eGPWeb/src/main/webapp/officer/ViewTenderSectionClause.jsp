<%--
    Document   : CreateSectionClause
    Created on : 24-Oct-2010, 1:03:35 AM
    Author     : yanki
--%>

<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBeanTender" />
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderIttClause" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderIttSubClause" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
         <%
            if(request.getParameter("ittHeaderId") == null || request.getParameter("sectionId") == null){
                return;
            }
            int ittHeaderId = Integer.parseInt(request.getParameter("ittHeaderId"));
            int sectionId = Integer.parseInt(request.getParameter("sectionId"));
            int tenderId = Integer.parseInt(request.getParameter("tenderId"));
            int tenderStdId = Integer.parseInt(request.getParameter("tenderStdId"));
            int pkgOrLotId = -1;
            if(request.getParameter("porlId")!=null){
                pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
            }

            String subSectionName = createSubSectionSrBean.getSubSectionName(ittHeaderId);

            String contentType = createSubSectionSrBean.getContectType(sectionId);
            String contentType1 = "";
            if(contentType.equalsIgnoreCase("ITT")){
                contentType1 = "BDS";
                contentType="ITB"; //set by Nitish_Oritro
            }else if(contentType.equalsIgnoreCase("GCC")){
                contentType1 = "SCC";
            }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View <%=contentType%></title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <%--<style type="text/css">
            ul li {margin-left: 20px;}
        </style>--%>
    </head>
    <body>

        <div class="mainDiv">
            <%if(!"yes".equalsIgnoreCase(request.getParameter("noa"))){%>
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <%}%>
            <div class="fixDiv">
                <!--Middle Content Table Start-->

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1">View <%=contentType%></div>
                            </div>
                            <%if(!"yes".equalsIgnoreCase(request.getParameter("noa"))){%>
                            <%if(pkgOrLotId == -1){%>
                            <table width="100%" cellspacing="0" cellpadding="0" class="t_space b_space">
                                <tr>
                                    <td  align="left">
                                    <a href="TenderDocPrep.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>" class="action-button-goback">Go Back to Tender Document Preparation</a>
                                    </td>
                                    <td align="right">
                                    <a href="TenderITTDashboard.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>&sectionId=<%=sectionId%>" class="action-button-goback">Go Back to Tender <%=contentType%> Dashboard</a>
                                    </td>
                                </tr>
                            </table>
                            <%}else{%>
                            <table width="100%" cellspacing="0" cellpadding="0" class="t_space b_space">
                                <tr>
                                    <td  align="left">
                                    <a href="TenderDocPrep.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>&porlId=<%= pkgOrLotId %>" class="action-button-goback">Go Back to Tender Document Preparation</a>
                                    </td>
                                    <td align="right">
                                    <a href="TenderITTDashboard.jsp?tenderId=<%=tenderId%>&tenderStdId=<%=tenderStdId%>&sectionId=<%=sectionId%>&porlId=<%= pkgOrLotId %>" class="action-button-goback">Go Back to Tender <%=contentType%> Dashboard</a>
                                    </td>
                                </tr>
                            </table>
                            <%}}%>
                            
                            <form action="<%=request.getContextPath()%>/SectionClauseSrBean" method="post"  onsubmit="return validate();">
                                <%--/PrepareTDS.jsp--%>
                                <input type="hidden" name="clauseCount" id="clauseCount" value="1" />
                                <input type="hidden" name="totalClauseCount" id="totalClauseCount" value="1" />
                                <input type="hidden" name="ittHeaderId" id="ittHeaderId" value="<%=ittHeaderId%>" />
                                <input type="hidden" name="sectionId" id="sectionId" value="<%=sectionId%>" />
                                <input type="hidden" name="templateId" id="templateId" value="<%=request.getParameter("templateId")%>" />


                                <table id="clauseData" width="100%" cellspacing="0" class="tableList_1">
                                    <tr id="tr_0">
                                        <th colspan="2" align="center"><%=subSectionName%></th>
                                    </tr>
                                    <%
                                    List<TblTenderIttClause> tblIttClause = createSubSectionSrBean.getTenderClauseDetail(ittHeaderId);
                                    
                                    int ittClauseId = -1;
                                    for(int i=0; i<tblIttClause.size(); i++){
                                        
                                        //int clauseId = tblIttClause.get(i).getTempplateIttClauseId();
                                    %>
                                    <tr id="tr_<%=(i+1)%>_1">
                                        <td style="line-height: 1.75">
                                            <%
                                            if(ittClauseId != tblIttClause.get(i).getTempplateIttClauseId()){
                                                ittClauseId = tblIttClause.get(i).getTempplateIttClauseId();
                                                out.print(tblIttClause.get(i).getIttClauseName());
                                            } else {
                                                out.print("&nbsp;");
                                            }
                                            %>
                                        </td>
                                    <%
                                        List<TblTenderIttSubClause> tblIttSubClause = createSubSectionSrBean.getTenderSubClauseDetail(tblIttClause.get(i).getTenderIttClauseId());
                                        
                                        int j = 0;
                                        for(j=0;j<tblIttSubClause.size();j++){
                                            tblIttSubClause.get(j).getTenderIttSubClauseId();
                                            tblIttSubClause.get(j).getIttSubClauseName();
                                            if(j != 0){
                                        %>
                                            <tr id="tr_<%=(i+1)%>_<%=(j+1)*2%>">
                                                <td>&nbsp;</td>
                                        <%
                                            }
                                        %>
                                                <td style="line-height: 1.75">
                                                    <%=tblIttSubClause.get(j).getIttSubClauseName()%>
                                                </td>
                                            </tr>

                                        <%
                                        }
                                    }
                                    %>
                                </table>

                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <%if(!"yes".equalsIgnoreCase(request.getParameter("noa"))){%>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <%}%>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
<%
createSubSectionSrBean = null;
%>