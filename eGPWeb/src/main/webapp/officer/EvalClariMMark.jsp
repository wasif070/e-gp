<%-- 
    Document   : EvalClariMMark
    Created on : Jan 11, 2011, 7:08:21 PM
    Author     : Rajesh Singh
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk,com.cptu.egp.eps.web.utility.HandleSpecialChar,java.text.SimpleDateFormat,com.cptu.egp.eps.web.utility.MailContentUtility,com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="texxt/html; charset=UTF-8">
        <title>Evaluation Clarification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            function Validate(){
                var i=0;j=0;total=0;
                for(i=0;i<document.getElementById("countlot").value;i++){
                    var formcount = document.getElementById("countform"+i).value;
                    var check=true;

                    for (j = 0; j < formcount; j++) {
                        if($.trim(document.getElementById("lotfromlist" + i + j).value)==''){
                            jAlert("Please fill all the textbox.","Evaluation Clarification Alert", function(RetVal) {
                            });
                            check=false;
                        }
                        else
                        {
                            if(digits($.trim(document.getElementById("lotfromlist" + i + j).value)))
                            {
                                total=parseInt(total)+parseInt($.trim(document.getElementById("lotfromlist" + i + j).value));
                                
                            }else
                            {
                                jAlert("Please enter only Digits.","Evaluation Clarification Alert", function(RetVal) {
                                });
                                check=false;
                            }
                        }
                    }

                }

                    if(total >100){
                        jAlert("Total sum should not be greater than 100.","Evaluation Clarification Alert", function(RetVal) {
                                    });
                    return false;
                    }else if(total <100){
                        jAlert("Total sum should not be lesser than 100.","Evaluation Clarification Alert", function(RetVal) {
                                    });
                    return false;
                    }

                    if(check==false){
                    return false;
                    }
            }

            function Validatefrom(){
                var j=0;total=0;
                
                    var formcount = document.getElementById("countforms").value;
                    var check=true;
                    
                    for (j = 0; j < formcount; j++) {
                        if($.trim(document.getElementById("fromlist" + j).value)==''){
                            jAlert("Please fill all the textbox.","Evaluation Clarification Alert", function(RetVal) {
                            });
                            check=false;
                        }
                        else
                        {
                            if(digits($.trim(document.getElementById("fromlist" + j).value)))
                            {
                                total=parseInt(total)+parseInt($.trim(document.getElementById("fromlist" + j).value));
                                if(total >100){
                                    jAlert("Total Max Marks should be less than 100","Evaluation Clarification Alert", function(RetVal) {
                                    });
                                    check=false;
                                }
                            }else
                            {
                                jAlert("Please enter only Digits.","Evaluation Clarification Alert", function(RetVal) {
                                });
                                check=false;
                            }
                        }
                    }

                    if(total >100){
                        jAlert("Total sum should not be greater than 100.","Evaluation Clarification Alert", function(RetVal) {
                                    });
                    return false;
                    }else if(total <100){
                        jAlert("Total sum should not be lesser than 100.","Evaluation Clarification Alert", function(RetVal) {
                                    });
                    return false;
                    }

                    if(check==false){
                    return false;
                    }
            }

            function digits(value) {
                //return /^\d+$/.test(value);
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
            }

            function Reset(){
                $(":input:text").attr('value','');
                return false;
            }
        </script>
    </head>
    <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <%
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    //userId = Integer.parseInt(session.getAttribute("userId").toString());
                    userId = session.getAttribute("userId").toString();
                }
                boolean isSubDt = false;
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }

    %>
    <body>
        <div class="dashboard_div">

            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <form id="frmEvalClari" name="frmEvalClari" action="" method="post">
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->

                <%
                            if (request.getParameter("btnSendLot") != null) {

                                String userid = "";
                                HttpSession hs = request.getSession();
                                if (hs.getAttribute("userId") != null) {
                                    userid = hs.getAttribute("userId").toString();
                                }

                                String dtXml = "";
                                java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                                int lotcount = Integer.parseInt(request.getParameter("countlot"));
                                int errorcount = 0;
                                //Lot wise for lop
                                for (int z = 0; z < lotcount; z++) {
                                    int formcount = Integer.parseInt(request.getParameter("countform" + String.valueOf(z)));
                                    for (int y = 0; y < formcount; y++) {
                                        
                                        if (request.getParameter("lotfromlist" + String.valueOf(z) + String.valueOf(y)) != null) {

                                            String table = "", updateString = "", whereCondition = "";
                                            table = "tbl_EvalServiceForms";
                                            updateString = " maxMarks='" + request.getParameter("lotfromlist" + String.valueOf(z) + String.valueOf(y)) + "' ";
                                            whereCondition = "esFormId=" + request.getParameter("hdnlfromlist" + String.valueOf(z) + String.valueOf(y));

                                            //out.println("update "+table+ " set "+updateString +" where "+ whereCondition );

                                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                            CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalServiceForms", updateString, whereCondition).get(0);
                                            //out.println(commonMsgChk.getMsg());
                                            if (commonMsgChk.getFlag() == false) {
                                                errorcount = 1;
                                            }
                                        }
                                    }
                                }

                                if (errorcount == 1) {
                                    response.sendRedirect("EvalClariMMark.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=false");
                                } else {
                                    response.sendRedirect("EvalComm.jsp?tenderid=" + request.getParameter("tenderId") + "&msgs=trues");
                                }
                            }


                            if (request.getParameter("btnSendfrom") != null) {

                                String userid = "";
                                HttpSession hs = request.getSession();
                                if (hs.getAttribute("userId") != null) {
                                    userid = hs.getAttribute("userId").toString();
                                }

                                String dtXml = "";
                                java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                                int errorcount = 0;
                                //Lot wise for lop

                                int formcount = Integer.parseInt(request.getParameter("countforms"));
                                for (int y = 0; y < formcount; y++) {
                                    
                                    if (request.getParameter("fromlist" + String.valueOf(y)) != null) {
                                        String table = "", updateString = "", whereCondition = "";
                                        table = "tbl_EvalServiceForms";
                                        updateString = " maxMarks='" + request.getParameter("fromlist" + String.valueOf(y)) + "' ";
                                        whereCondition = "esFormId=" + request.getParameter("hdnfromlist" + String.valueOf(y));

                                        //out.println("update "+table+ " set "+updateString +" where "+ whereCondition );

                                        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                        CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalServiceForms", updateString, whereCondition).get(0);

                                        if (commonMsgChk.getFlag() == false) {
                                            errorcount = 1;
                                        }
                                    }
                                }

                                if (errorcount == 1) {
                                    response.sendRedirect("EvalClariMMark.jsp?tenderId=" + request.getParameter("tenderId") + "&msg=false");
                                } else {
                                    response.sendRedirect("EvalComm.jsp?tenderid=" + request.getParameter("tenderId") + "&msgs=trues");
                                }
                            }
                %>
                <%
                            // Variable tenderId is defined by u on ur current page.
                            String msg = "";
                            msg = request.getParameter("msg");
                %>
                <div class="contentArea_1">
                    <div class="pageHead_1">Evaluation Clarification</div>
                    <% if (msg != null && msg.contains("false")) {%>
                    <div class="responseMsg errorMsg t_space">Operation Failed, Please try again later.</div>
                    <%  }%>
                    <%
                                pageContext.setAttribute("tenderId", tenderId);
                    %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <%
                                boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");
                    %>
                    <div>&nbsp;</div>
                    <%if (!"TEC".equalsIgnoreCase(request.getParameter("comType"))) {%>
                    <jsp:include page="officerTabPanel.jsp" >
                        <jsp:param name="tab" value="7" />
                    </jsp:include>
                    <% }%>
                    <div class="tabPanelArea_1">
                        <%
                                    if ("rp".equalsIgnoreCase(request.getParameter("st"))) {
                                        pageContext.setAttribute("TSCtab", "3");
                                    } else {
                                        pageContext.setAttribute("TSCtab", "4");
                                    }
                        %>
                        <%@include file="../resources/common/AfterLoginTSC.jsp"%>
                        <%
                                    CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                    List<SPCommonSearchData> submitDateSts = commonSearchService.searchData("chkSubmissionDt", tenderId, null, null, null, null, null, null, null, null);
                                    if (submitDateSts.get(0).getFieldName1().equalsIgnoreCase("true")) {
                                        isSubDt = true;
                                    } else {
                                        isSubDt = false;
                                    }
                                    if (isTenPackageWis) {
                                        //List<SPCommonSearchData> packageList = ;
                                        for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td colspan="2" class="t-align-left ff">Tender Submission Details</td>
                            </tr>

                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="22%" class="t-align-left ff">Form Name</th>
                                <th width="23%" class="t-align-left">Max Marks</th>
                            </tr>
                            <%
                                                                        int l = 0;
                                                                        for (SPCommonSearchData formdetails : commonSearchService.searchData("FormsList", tenderId, userId, "0", null, null, null, null, null, null)) {
                            %>
                            <tr>
                                <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                                <td class="t-align-center">
                                    <input type="text" name="fromlist<%=l%>" id="fromlist<%=l%>" style="width: 25px"/>
                                    <input type="hidden" name="hdnfromlist<%=l%>" id="lotfromlist<%=l%>" value="<%=formdetails.getFieldName4()%>" />
                                </td>
                            </tr>
                            <% l++;
                                                                        }%>
                            <input type="hidden" name="countforms" id="countforms" value="<%=l%>"/>
                        </table>
                        <div class="t-align-center t_space">
                            <label class="formBtn_1">
                                <input name="btnSendfrom" id="btnSendfrom" type="submit" value="Submit" onclick="return Validatefrom();"/>
                            </label>
                            <label class="formBtn_1">
                                <input name="btnSendreset" type="submit" value="Reset" onclick="return Reset();"/>
                            </label>
                        </div>
                        <% }
                                                            } else {

                                                                for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td colspan="2" class="t-align-left ff">Tender Submission Details</td>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <%  }
                                                                int i = 0;
                                                                for (SPCommonSearchData lotList : commonSearchService.searchData("gettenderlotbytenderid", tenderId, "", "", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">

                            <tr>
                                <td width="22%" class="t-align-left ff">Lot No. :</td>
                                <td width="78%" class="t-align-left"><%=lotList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Lot Description :</td>
                                <td class="t-align-left"><%=lotList.getFieldName2()%></td>
                            </tr>
                        </table><table width="100%" cellspacing="0" class="tableList_1" id="tb2">
                            <tr>
                                <th width="22%" class="t-align-left ff">Form Name</th>
                                <th width="23%" class="t-align-left">Max Marks</th>
                            </tr>
                            <%
                                                                                                int j = 0;
                                                                                                for (SPCommonSearchData formdetails : commonSearchService.searchData("FormsList", tenderId, userId, lotList.getFieldName3(), null, null, null, null, null, null)) {
                            %>
                            <tr>
                                <td class="t-align-left ff"><%=formdetails.getFieldName2()%></td>
                                <td class="t-align-left">
                                    <input type="text" name="lotfromlist<%=i%><%=j%>" id="lotfromlist<%=i%><%=j%>" size="50px"/>
                                    <input type="hidden" name="hdnlfromlist<%=i%><%=j%>" id="lotfromlist<%=i%><%=j%>" value="<%=formdetails.getFieldName4()%>" />
                                </td>
                            </tr>
                            <% j++;
                                                                                                }%>
                            <input type="hidden" name="countform<%=i%>" id="countform<%=i%>" value="<%=j%>"/>
                        </table>
                        <input type="hidden" name="lotNumber<%=i%>" id="lotNumber<%=i%>" value="<%=lotList.getFieldName1()%>"/>
                        <%     i++;
                                                                }%>
                        <input type="hidden" name="countlot" id="countlot" value="<%=i%>"/>
                        <div class="t-align-center t_space">
                            <label class="formBtn_1">
                                <input name="btnSendLot" id="btnSendLot" type="submit" value="Submit" onclick="return Validate();"/>
                            </label>
                            <label class="formBtn_1">
                                <input name="btnSendreset" type="submit" value="Reset" onclick="return Reset();"/>
                            </label>
                        </div>
                        <% }%>


                    </div>
                    <div>&nbsp;</div>
                    <!--Dashboard Content Part End-->
                </div>
            </form>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
     <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
