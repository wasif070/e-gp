<%-- 
    Document   : getdate
    Created on : Nov 3, 2010, 6:48:35 PM
    Author     : Prashant
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.daointerface.HibernateQueryDao"%>
<%@page import="com.cptu.egp.eps.model.table.TblAppPackages"%>
<%@page import="com.cptu.egp.eps.dao.daointerface.TblAppPackagesDao"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/Html" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.sql.*" %>
<%@page import="java.io.*" %>
<%@page import="java.util.Date" %>
<%@page import="java.text.DateFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="com.cptu.egp.eps.web.utility.*" %>
<jsp:useBean id="appServlet" scope="request" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
<%
    int appId = 0;
    if(request.getParameter("hdnAppId")!=null){
        appId = Integer.parseInt(request.getParameter("hdnAppId"));
    }
    
    int pkgId = 0;
    if(request.getParameter("hdnPkgId")!=null){
        pkgId = Integer.parseInt(request.getParameter("hdnPkgId"));
    }

    String dateType = "PQ";
    if(request.getParameter("hdnDateType") != null && !"".equalsIgnoreCase(request.getParameter("hdnDateType"))){
        dateType = request.getParameter("hdnDateType");
    }

    SimpleDateFormat sd2 = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat sd = new SimpleDateFormat("dd-MMM-yyyy");
    SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd");

    String advtDate = "";
    
    if(request.getParameter("Pqdtadvtinvt")!=null && !"".equals(request.getParameter("Pqdtadvtinvt"))){
        advtDate = sd1.format(sd2.parse(request.getParameter("Pqdtadvtinvt")));
    }
    
    String advtDateNoDays = "";
    if(request.getParameter("PqdtadvtinvtNo")!=null && !"".equals(request.getParameter("PqdtadvtinvtNo"))){
        advtDateNoDays = request.getParameter("PqdtadvtinvtNo");
    }

    String appSubDate = "";
    if(request.getParameter("Pqdtappsub")!=null && !"".equals(request.getParameter("Pqdtappsub"))){
        appSubDate = sd1.format(sd.parse(request.getParameter("Pqdtappsub")));
    }

    String appSubDateNoDays = "";
    if(request.getParameter("PqdtappsubNo") != null && !"".equals(request.getParameter("PqdtappsubNo"))){
        appSubDateNoDays = request.getParameter("PqdtappsubNo");
    }

    String appSubDateExp = "";
    if(request.getParameter("Pqdtsubevarpt") != null && !"".equals(request.getParameter("Pqdtsubevarpt"))){
        appSubDateExp = sd1.format(sd.parse(request.getParameter("Pqdtsubevarpt")));
    }
        

    String appSubDateNoDaysExp = "";
    if(request.getParameter("PqdtsubevarptNo") != null && !"".equals(request.getParameter("PqdtsubevarptNo"))){
        appSubDateNoDaysExp = request.getParameter("PqdtsubevarptNo");
    }   

    String appList = "";
    if(request.getParameter("Pqdtapplst") != null && !"".equals(request.getParameter("Pqdtapplst"))){
        appList = sd1.format(sd.parse(request.getParameter("Pqdtapplst")));
    }

    String RfqDtAdvtift = "";
    if(request.getParameter("Rfqdtadvtift") != null && !"".equals(request.getParameter("Rfqdtadvtift"))){
        RfqDtAdvtift = sd1.format(sd2.parse(request.getParameter("Rfqdtadvtift")));
    }
    
    String RfqDtAdvtiftNo = "";
    if(request.getParameter("RfqdtadvtiftNo") != null && !"".equals(request.getParameter("RfqdtadvtiftNo"))){
        RfqDtAdvtiftNo = request.getParameter("RfqdtadvtiftNo");
    }

    String RfqDtSub = "";
    if(request.getParameter("Rfqdtsub")!=null && !"".equals(request.getParameter("Rfqdtsub"))){
        RfqDtSub = sd1.format(sd.parse(request.getParameter("Rfqdtsub")));
    }
    
    
    String RfqDtSubNo = "";
    if(request.getParameter("RfqdtsubNo") != null && !"".equals(request.getParameter("RfqdtsubNo"))){
        RfqDtSubNo = request.getParameter("RfqdtsubNo");
    }

    String RfqExpDtopen = "";
    if(request.getParameter("Rfqexpdtopen") != null && !"".equals(request.getParameter("Rfqexpdtopen"))){
        RfqExpDtopen = sd1.format(sd.parse(request.getParameter("Rfqexpdtopen")));
    }
    
    String RfqExpDtopenNo = "";
    if(request.getParameter("RfqexpdtopenNo")!=null && !"".equals(request.getParameter("RfqexpdtopenNo"))){
        RfqExpDtopenNo = request.getParameter("RfqexpdtopenNo");
    }

    String RfqDtSubEvaRpt = "";
    if(request.getParameter("RfqdtsubevaRpt") != null && !"".equals(request.getParameter("RfqdtsubevaRpt"))){
        RfqDtSubEvaRpt = sd1.format(sd.parse(request.getParameter("RfqdtsubevaRpt")));
    }
    
    String RfqdtSubEvaRptNo = "";   
    if(request.getParameter("RfqdtsubevaRptNo")!=null && !"".equals(request.getParameter("RfqdtsubevaRptNo"))){
        RfqdtSubEvaRptNo = request.getParameter("RfqdtsubevaRptNo");
    }
    
    String RfqExpDtAppawd = "";
    if(request.getParameter("RfqexpdtAppawd")!=null && !"".equals(request.getParameter("RfqexpdtAppawd"))){
        RfqExpDtAppawd = sd1.format(sd.parse(request.getParameter("RfqexpdtAppawd")));
    }
    
    String RfqExpDtAppawdNo = "";
    if(request.getParameter("RfqexpdtAppawdNo")!=null && !"".equals(request.getParameter("RfqexpdtAppawdNo"))){
        RfqExpDtAppawdNo = request.getParameter("RfqexpdtAppawdNo");
    }
    //Code by Proshanto Kumar Saha
    String RfqexpdtLtrIntAwd = "";
    if(request.getParameter("RfqexpdtLtrIntAwd")!=null && !"".equals(request.getParameter("RfqexpdtLtrIntAwd"))){
        RfqexpdtLtrIntAwd = request.getParameter("RfqexpdtLtrIntAwd");
    }
    String RfqexpdtLtrIntAwdNo = "";
    if(request.getParameter("RfqexpdtLtrIntAwdNo")!=null && !"".equals(request.getParameter("RfqexpdtLtrIntAwdNo"))){
        RfqexpdtLtrIntAwdNo = request.getParameter("RfqexpdtLtrIntAwdNo");
    }
    //Code End by Proshanto Kumar Saha
    String RfqDtIssNOA = "";
    if(request.getParameter("RfqdtIssNOA")!=null && !"".equals(request.getParameter("RfqdtIssNOA"))){
        RfqDtIssNOA = sd1.format(sd.parse(request.getParameter("RfqdtIssNOA")));
    }
    
    String RfqDtIssNOANo = "";
    if(request.getParameter("RfqdtIssNOANo")!=null && !"".equals(request.getParameter("RfqdtIssNOANo"))){
        RfqDtIssNOANo = request.getParameter("RfqdtIssNOANo");
    }
    
    String RfqExpDtSign = "";
    if(request.getParameter("RfqexpdtSign")!=null && !"".equals(request.getParameter("RfqexpdtSign"))){
        RfqExpDtSign = sd1.format(sd.parse(request.getParameter("RfqexpdtSign")));
    }
    
    String RfqExpDtSignNo = "";
    if(request.getParameter("RfqexpdtSignNo")!=null && !"".equals(request.getParameter("RfqexpdtSignNo"))){
        RfqExpDtSignNo = request.getParameter("RfqexpdtSignNo");
    }
    
    String RfqExpDtCompContract = "";
    if(request.getParameter("RfqexpdtCompContract")!=null && !"".equals(request.getParameter("RfqexpdtCompContract"))){
        if(request.getParameter("RfqexpdtCompContract").indexOf("/") >= 0){
            RfqExpDtCompContract = sd1.format(sd2.parse(request.getParameter("RfqexpdtCompContract")));
        }
        else{
        RfqExpDtCompContract = sd1.format(sd.parse(request.getParameter("RfqexpdtCompContract")));
        }
    }

    String TSTMexPdtAdvtIFT = "";
    if(request.getParameter("TSTMexpdtadvtIFT")!=null && !"".equals(request.getParameter("TSTMexpdtadvtIFT"))){
        TSTMexPdtAdvtIFT = sd1.format(sd2.parse(request.getParameter("TSTMexpdtadvtIFT")));
    }

    String TSTMexPdtadvtIFTNo = "";
    if(request.getParameter("TSTMexpdtadvtIFTNo")!=null && !"".equals(request.getParameter("TSTMexpdtadvtIFTNo")))
        TSTMexPdtadvtIFTNo = request.getParameter("TSTMexpdtadvtIFTNo");

    String TSTMexPdtSub = "";
    if(request.getParameter("TSTMexpdtSub")!=null && !"".equals(request.getParameter("TSTMexpdtSub")))
        TSTMexPdtSub = sd1.format(sd.parse(request.getParameter("TSTMexpdtSub")));
    
    String TSTMexPdtSubNo = "";
    if(request.getParameter("TSTMexpdtSubNo")!=null && !"".equals(request.getParameter("TSTMexpdtSubNo")))
        TSTMexPdtSubNo = request.getParameter("TSTMexpdtSubNo");
    
    String TSTMexPdtOpen = "";
    if(request.getParameter("TSTMexpdtopen")!=null && !"".equals(request.getParameter("TSTMexpdtopen")))
        TSTMexPdtOpen = sd1.format(sd.parse(request.getParameter("TSTMexpdtopen")));
    
    String TSTMexPdtOpenNo = "";
    if(request.getParameter("TSTMexpdtopenNo")!=null && !"".equals(request.getParameter("TSTMexpdtopenNo")))
        TSTMexPdtOpenNo = request.getParameter("TSTMexpdtopenNo");
    
    String TSTMexPdtSubEvaRpt = "";
    if(request.getParameter("TSTMexpdtsubEvaRpt")!=null && !"".equals(request.getParameter("TSTMexpdtsubEvaRpt")))
        TSTMexPdtSubEvaRpt = sd1.format(sd.parse(request.getParameter("TSTMexpdtsubEvaRpt")));
    
    String TSTMexPdtSubEvaRptNo = "";
    if(request.getParameter("TSTMexpdtsubEvaRptNo")!=null && !"".equals(request.getParameter("TSTMexpdtsubEvaRptNo")))
        TSTMexPdtSubEvaRptNo = request.getParameter("TSTMexpdtsubEvaRptNo");

    String TSTMexPdtAppEvaRpt = "";
    if(request.getParameter("TSTMexpdtappEvaRpt")!=null && !"".equals(request.getParameter("TSTMexpdtappEvaRpt")))
        TSTMexPdtAppEvaRpt = sd1.format(sd.parse(request.getParameter("TSTMexpdtappEvaRpt")));
    
    String TSTM2expDtIssuefinalDoc = "";
    if(request.getParameter("TSTM2expdtIssuefinalDoc")!=null && !"".equals(request.getParameter("TSTM2expdtIssuefinalDoc")))
        TSTM2expDtIssuefinalDoc = sd1.format(sd2.parse(request.getParameter("TSTM2expdtIssuefinalDoc")));

    String TSTM2expDtIssuefinalDocNo = "";
    if(request.getParameter("TSTM2expdtIssuefinalDocNo")!=null && !"".equals(request.getParameter("TSTM2expdtIssuefinalDocNo")))
        TSTM2expDtIssuefinalDocNo = request.getParameter("TSTM2expdtIssuefinalDocNo");

    String TSTM2expDtSub = "";
    if(request.getParameter("TSTM2expdtSub")!=null && !"".equals(request.getParameter("TSTM2expdtSub")))
        TSTM2expDtSub = sd1.format(sd.parse(request.getParameter("TSTM2expdtSub")));
    
    String TSTM2expDtSubNo = "";
    if(request.getParameter("TSTM2expdtSubNo")!=null && !"".equals(request.getParameter("TSTM2expdtSubNo")))
        TSTM2expDtSubNo = request.getParameter("TSTM2expdtSubNo");

    String TSTM2expDtOpen = "";
    if(request.getParameter("TSTM2expdtOpen")!=null && !"".equals(request.getParameter("TSTM2expdtOpen")))
        TSTM2expDtOpen = sd1.format(sd.parse(request.getParameter("TSTM2expdtOpen")));

    String TSTM2expDtOpenNo = "";
    if(request.getParameter("TSTM2expdtOpenNo")!=null && !"".equals(request.getParameter("TSTM2expdtOpenNo")))
        TSTM2expDtOpenNo = request.getParameter("TSTM2expdtOpenNo");

    String TSTM2expDtSubevaRpt = "";
    if(request.getParameter("TSTM2expdtsubevaRpt")!=null && !"".equals(request.getParameter("TSTM2expdtsubevaRpt")))
       TSTM2expDtSubevaRpt = sd1.format(sd.parse(request.getParameter("TSTM2expdtsubevaRpt")));
    
    String TSTM2expDtSubevaRptNo = "";
    if(request.getParameter("TSTM2expdtsubevaRptNo")!=null && !"".equals(request.getParameter("TSTM2expdtsubevaRptNo")))
       TSTM2expDtSubevaRptNo = request.getParameter("TSTM2expdtsubevaRptNo");

    String TSTM2expDtAppEvaRpt = "";
    if(request.getParameter("TSTM2expdtAppEvaRpt")!=null && !"".equals(request.getParameter("TSTM2expdtAppEvaRpt")))
        TSTM2expDtAppEvaRpt = sd1.format(sd.parse(request.getParameter("TSTM2expdtAppEvaRpt")));
    
    String TSTM2expDtAppEvaRptNo = "";
    if(request.getParameter("TSTM2expdtAppEvaRptNo")!=null && !"".equals(request.getParameter("TSTM2expdtAppEvaRptNo")))
        TSTM2expDtAppEvaRptNo = request.getParameter("TSTM2expdtAppEvaRptNo");
     //Code by Proshanto Kumar Saha
    String TSTM2expdtLetterOfIntent = "";
    if(request.getParameter("TSTM2expdtLetterOfIntent")!=null && !"".equals(request.getParameter("TSTM2expdtLetterOfIntent"))){
        RfqexpdtLtrIntAwd = request.getParameter("TSTM2expdtLetterOfIntent");
    }
    String TSTM2expdtLetterOfIntentNo = "";
    if(request.getParameter("TSTM2expdtLetterOfIntentNo")!=null && !"".equals(request.getParameter("TSTM2expdtLetterOfIntentNo"))){
        RfqexpdtLtrIntAwdNo = request.getParameter("TSTM2expdtLetterOfIntentNo");
    }
    //Code End by Proshanto Kumar Saha
    String TSTM2expDtIssueNOA = "";
    if(request.getParameter("TSTM2expdtIssueNOA")!=null && !"".equals(request.getParameter("TSTM2expdtIssueNOA")))
        TSTM2expDtIssueNOA = sd1.format(sd.parse(request.getParameter("TSTM2expdtIssueNOA")));

    String TSTM2expDtIssueNOANo = "";
    if(request.getParameter("TSTM2expdtIssueNOANo")!=null && !"".equals(request.getParameter("TSTM2expdtIssueNOANo")))
        TSTM2expDtIssueNOANo = request.getParameter("TSTM2expdtIssueNOANo");

    String TSTM2expDtSignContract = "";
    if(request.getParameter("TSTM2expdtSignContract")!=null && !"".equals(request.getParameter("TSTM2expdtSignContract")))
        TSTM2expDtSignContract = sd1.format(sd.parse(request.getParameter("TSTM2expdtSignContract")));
    
    String TSTM2expDtSignContractNo = "";
    if(request.getParameter("TSTM2expdtSignContractNo")!=null && !"".equals(request.getParameter("TSTM2expdtSignContractNo")))
        TSTM2expDtSignContractNo = request.getParameter("TSTM2expdtSignContractNo");

    String TSTM2expdtcomplcontract = "";
    if(request.getParameter("TSTM2expdtcomplcontract")!=null && !"".equals(request.getParameter("TSTM2expdtcomplcontract")))
        TSTM2expdtcomplcontract = sd1.format(sd.parse(request.getParameter("TSTM2expdtcomplcontract")));

    String REOQexpDtAdvtREOI = "";
    if(request.getParameter("REOQexpdtadvtREOI")!=null && !"".equals(request.getParameter("REOQexpdtadvtREOI")))
        REOQexpDtAdvtREOI = sd1.format(sd2.parse(request.getParameter("REOQexpdtadvtREOI")));

    String REOQexpDtAdvtREOINo = "";
    if(request.getParameter("REOQexpdtadvtREOINo")!=null && !"".equals(request.getParameter("REOQexpdtadvtREOINo")))
        REOQexpDtAdvtREOINo = request.getParameter("REOQexpdtadvtREOINo");
    
    String REOQexpDtlstDtRcptEOI = "";
    if(request.getParameter("REOQexpdtlstdtRcptEOI")!=null && !"".equals(request.getParameter("REOQexpdtlstdtRcptEOI")))
        REOQexpDtlstDtRcptEOI = sd1.format(sd.parse(request.getParameter("REOQexpdtlstdtRcptEOI")));

    String REOQexpDtlstDtRcptEOINo = "";
    if(request.getParameter("REOQexpdtlstdtRcptEOINo")!=null && !"".equals(request.getParameter("REOQexpdtlstdtRcptEOINo")))
        REOQexpDtlstDtRcptEOINo = request.getParameter("REOQexpdtlstdtRcptEOINo");

    String REOQexpDtSubsrtlstFrm = "";
    if(request.getParameter("REOQexpdtsubsrtlstFrm")!=null && !"".equals(request.getParameter("REOQexpdtsubsrtlstFrm")))
        REOQexpDtSubsrtlstFrm = sd1.format(sd.parse(request.getParameter("REOQexpdtsubsrtlstFrm")));
    
    String REOQexpDtSubsrtlstFrmNo = "";
    if(request.getParameter("REOQexpdtsubsrtlstFrmNo")!=null && !"".equals(request.getParameter("REOQexpdtsubsrtlstFrmNo")))
        REOQexpDtSubsrtlstFrmNo = request.getParameter("REOQexpdtsubsrtlstFrmNo");

    String REOQextDtAppsrtlstFrm = "";
    if(request.getParameter("REOQextdtAppsrtlstFrm")!=null && !"".equals(request.getParameter("REOQextdtAppsrtlstFrm")))
        REOQextDtAppsrtlstFrm = sd1.format(sd.parse(request.getParameter("REOQextdtAppsrtlstFrm")));

    String REQexpDtcompContract = "";
    if(request.getParameter("REQexpdtcompContract")!=null && !"".equals(request.getParameter("REQexpdtcompContract")))
        REQexpDtcompContract = request.getParameter("REQexpdtcompContract");
    
    String RFPexpDtIssueRFP = "";
    if(request.getParameter("RFPexpdtissueRFP")!=null && !"".equals(request.getParameter("RFPexpdtissueRFP")))
        RFPexpDtIssueRFP = sd1.format(sd2.parse(request.getParameter("RFPexpdtissueRFP")));
    
    String RFPexpDtIssueRFPNo = "";
    if(request.getParameter("RFPexpdtissueRFPNo")!=null && !"".equals(request.getParameter("RFPexpdtissueRFPNo")))
        RFPexpDtIssueRFPNo = request.getParameter("RFPexpdtissueRFPNo");

    String RFPexpDtSubProposal = "";
    if(request.getParameter("RFPexpdtSubProposal")!=null && !"".equals(request.getParameter("RFPexpdtSubProposal")))
        RFPexpDtSubProposal = sd1.format(sd.parse(request.getParameter("RFPexpdtSubProposal")));
    
    String RFPexpDtSubProposalNo = "";
    if(request.getParameter("RFPexpdtSubProposalNo")!=null && !"".equals(request.getParameter("RFPexpdtSubProposalNo")))
        RFPexpDtSubProposalNo = request.getParameter("RFPexpdtSubProposalNo");
    
    String RFPexpDtTechOpen = "";
    if(request.getParameter("RFPexpdttechOpen")!=null && !"".equals(request.getParameter("RFPexpdttechOpen")))
       RFPexpDtTechOpen = sd1.format(sd.parse(request.getParameter("RFPexpdttechOpen")));
    
    String RFPexpDtTechOpenNo = "";
    if(request.getParameter("RFPexpdttechOpenNo")!=null && !"".equals(request.getParameter("RFPexpdttechOpenNo")))
        RFPexpDtTechOpenNo = request.getParameter("RFPexpdttechOpenNo");

    String RFPexpDtTechEva = "";
    if(request.getParameter("RFPexpdttechEva")!=null && !"".equals(request.getParameter("RFPexpdttechEva")))
        RFPexpDtTechEva = sd1.format(sd.parse(request.getParameter("RFPexpdttechEva")));

    String RFPexpDtTechEvaNo = "";
    if(request.getParameter("RFPexpdttechEvaNo")!=null && !"".equals(request.getParameter("RFPexpdttechEvaNo")));
        RFPexpDtTechEvaNo = request.getParameter("RFPexpdttechEvaNo");
        
    String RFPextDtFinOpen = "";
    if(request.getParameter("RFPextdtFinOpen")!=null && !"".equals(request.getParameter("RFPextdtFinOpen")))
        RFPextDtFinOpen = sd1.format(sd.parse(request.getParameter("RFPextdtFinOpen")));

    String RFPextDtFinOpenNo = "";
    if(request.getParameter("RFPextdtFinOpenNo")!=null && !"".equals(request.getParameter("RFPextdtFinOpenNo")))
        RFPextDtFinOpenNo = request.getParameter("RFPextdtFinOpenNo");

    String RFPexpDtSubCOmEvaRpt = "";
    if(request.getParameter("RFPexpdtsubCOmEvaRpt")!=null && !"".equals(request.getParameter("RFPexpdtsubCOmEvaRpt")))
        RFPexpDtSubCOmEvaRpt = sd1.format(sd.parse(request.getParameter("RFPexpdtsubCOmEvaRpt")));
   
    String RFPexpDtSubCOmEvaRptNo = "";
    if(request.getParameter("RFPexpdtsubCOmEvaRptNo")!=null && !"".equals(request.getParameter("RFPexpdtsubCOmEvaRptNo")))
        RFPexpDtSubCOmEvaRptNo = request.getParameter("RFPexpdtsubCOmEvaRptNo");
    
    String RFPexpDtAppcomEvaRpt = "";
    if(request.getParameter("RFPexpdtappcomEvaRpt")!=null && !"".equals(request.getParameter("RFPexpdtappcomEvaRpt")))
        RFPexpDtAppcomEvaRpt = sd1.format(sd.parse(request.getParameter("RFPexpdtappcomEvaRpt")));
    
    String RFPexpDtAppcomEvaRptNo = "";
    if(request.getParameter("RFPexpdtappcomEvaRptNo")!=null && !"".equals(request.getParameter("RFPexpdtappcomEvaRptNo")))
        RFPexpDtAppcomEvaRptNo = request.getParameter("RFPexpdtappcomEvaRptNo");

    String RFPexpDtCompNego = "";
    if(request.getParameter("RFPexpdtcompNego")!=null && !"".equals(request.getParameter("RFPexpdtcompNego")))
        RFPexpDtCompNego = sd1.format(sd.parse(request.getParameter("RFPexpdtcompNego")));

    String RFPexpDtCompNegoNo = "";
    if(request.getParameter("RFPexpdtcompNegoNo")!=null && !"".equals(request.getParameter("RFPexpdtcompNegoNo")))
        RFPexpDtCompNegoNo = request.getParameter("RFPexpdtcompNegoNo");
    
    String RFPexpDtAppawdContract = "";
    if(request.getParameter("RFPexpdtappawdContract")!=null && !"".equals(request.getParameter("RFPexpdtappawdContract")))
        RFPexpDtAppawdContract = sd1.format(sd.parse(request.getParameter("RFPexpdtappawdContract")));

    String RFPexpDtAppawdContractNo = "";
    if(request.getParameter("RFPexpdtappawdContractNo")!=null && !"".equals(request.getParameter("RFPexpdtappawdContractNo")))
       RFPexpDtAppawdContractNo = request.getParameter("RFPexpdtappawdContractNo");
    
    String RFPexpDtSigncontract = "";
    if(request.getParameter("RFPexpdtsigncontract")!=null && !"".equals(request.getParameter("RFPexpdtsigncontract")))
        RFPexpDtSigncontract = sd1.format(sd.parse(request.getParameter("RFPexpdtsigncontract")));
    
    String RFPexpDtSigncontractNo = "";
    if(request.getParameter("RFPexpdtsigncontractNo")!=null && !"".equals(request.getParameter("RFPexpdtsigncontractNo")))
        RFPexpDtSigncontractNo = request.getParameter("RFPexpdtsigncontractNo");
    
    String RFPexpDtComplcontract = "";
    if(request.getParameter("RFPexpdtcomplcontract")!=null && !"".equals(request.getParameter("RFPexpdtcomplcontract"))){
        if(request.getParameter("RFPexpdtcomplcontract").indexOf("/") >= 0){
            RFPexpDtComplcontract = sd1.format(sd2.parse(request.getParameter("RFPexpdtcomplcontract")));
        }
        else{
        RFPexpDtComplcontract = sd1.format(sd.parse(request.getParameter("RFPexpdtcomplcontract")));
        }
    }
    
    String RFAexpdtAdvtRFA = "";
    if(request.getParameter("RFAexpdtadvtRFA")!=null && !"".equals(request.getParameter("RFAexpdtadvtRFA")))
        RFAexpdtAdvtRFA = sd1.format(sd2.parse(request.getParameter("RFAexpdtadvtRFA")));
    
    String RFAexpdtAdvtRFANo = "";
    if(request.getParameter("RFAexpdtadvtRFANo")!=null && !"".equals(request.getParameter("RFAexpdtadvtRFANo")))
        RFAexpdtAdvtRFANo = request.getParameter("RFAexpdtadvtRFANo");

    String RFAdtrcptApp = "";
    if(request.getParameter("RFAdtrcptApp")!=null && !"".equals(request.getParameter("RFAdtrcptApp")))
        RFAdtrcptApp = sd1.format(sd.parse(request.getParameter("RFAdtrcptApp")));

    String RFAdtrcptAppNo = "";
    if(request.getParameter("RFAdtrcptAppNo")!=null && !"".equals(request.getParameter("RFAdtrcptAppNo")))
       RFAdtrcptAppNo = request.getParameter("RFAdtrcptAppNo");
    
    String RFAdtEveapp = "";
    if(request.getParameter("RFAdteveapp")!=null && !"".equals(request.getParameter("RFAdteveapp")))
        RFAdtEveapp = sd1.format(sd.parse(request.getParameter("RFAdteveapp")));
    
    String RFAdtEveappNo = "";
    if(request.getParameter("RFAdteveappNo")!=null && !"".equals(request.getParameter("RFAdteveappNo")))
        RFAdtEveappNo = request.getParameter("RFAdteveappNo");

    String RFAdtIntrvwselInd = "";
    if(request.getParameter("RFAdtintrvwselInd")!=null && !"".equals(request.getParameter("RFAdtintrvwselInd")))
        RFAdtIntrvwselInd = sd1.format(sd.parse(request.getParameter("RFAdtintrvwselInd")));

    String RFAdtIntrvwselIndNo = "";
    if(request.getParameter("RFAdtintrvwselIndNo")!=null && !"".equals(request.getParameter("RFAdtintrvwselIndNo")))
        RFAdtIntrvwselIndNo = request.getParameter("RFAdtintrvwselIndNo");

    String RFAdtEvaFnlselLst = "";
    if(request.getParameter("RFAdtevaFnlselLst")!=null && !"".equals(request.getParameter("RFAdtevaFnlselLst")))
        RFAdtEvaFnlselLst = sd1.format(sd.parse(request.getParameter("RFAdtevaFnlselLst")));
    
    String RFAdtEvaFnlselLstNo = "";
    if(request.getParameter("RFAdtevaFnlselLstNo")!=null && !"".equals(request.getParameter("RFAdtevaFnlselLstNo")))
        RFAdtEvaFnlselLstNo = request.getParameter("RFAdtevaFnlselLstNo");

    String RFAdtEubevaRpt = "";
        if(request.getParameter("RFAdtsubevaRpt")!=null && !"".equals(request.getParameter("RFAdtsubevaRpt")))
        RFAdtEubevaRpt = sd1.format(sd.parse(request.getParameter("RFAdtsubevaRpt")));
    
    String RFAdtSubevaRptNo = "";
    if(request.getParameter("RFAdtsubevaRptNo")!=null && !"".equals(request.getParameter("RFAdtsubevaRptNo")))
        RFAdtSubevaRptNo = request.getParameter("RFAdtsubevaRptNo");

    String RFAdtAppCons = "";
    if(request.getParameter("RFAdtappCons")!=null && !"".equals(request.getParameter("RFAdtappCons")))
        RFAdtAppCons = sd1.format(sd.parse(request.getParameter("RFAdtappCons")));
    boolean isReviseApp=false;
    if(request.getParameter("isRevision")!=null&&"true".equalsIgnoreCase(request.getParameter("isRevision")))
        isReviseApp=true;
    if(isReviseApp){
        HibernateQueryDao HQDao=(HibernateQueryDao)AppContext.getSpringBean("HibernateQueryDao");

        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    tenderCommonService.createTempAppPackages(String.valueOf(pkgId));
         HQDao.updateDeleteNewQuery("update TblAppPackages ap set ap.appStatus='BeingRevised',workflowStatus='pending' where ap.tblAppMaster.appId="+appId+" and ap.packageId="+pkgId);

        }


    
   String pqDtId=request.getParameter("pqDtId");

    String dtXml = "";

    if ("PQ".equals(dateType)) {
        String topLine="<root><tbl_AppPqTenderDates appid=\""+appId+"\" packageId=\""+pkgId+"\" ";
        if("Update".equalsIgnoreCase(request.getParameter("update"))||"Revise".equalsIgnoreCase(request.getParameter("Revise")))
        topLine="<root><tbl_AppPqTenderDates pqDtId=\""+pqDtId+"\" appid=\""+appId+"\" packageId=\""+pkgId+"\" ";

        dtXml = topLine
        + "advtDt=\"" + advtDate + "\" advtDays=\"" + advtDateNoDays + "\" openDt=\"\" openDays=\"\"  "
        + "subDt=\"" + appSubDate + "\" subDays=\"" + appSubDateNoDays + "\" "
        + "evalRptDt=\"" + appSubDateExp + "\" evalRptDays=\"" + appSubDateNoDaysExp + "\" "
        + "appLstDt=\"" + appList + "\" "
        + "tenderAdvertDt=\"" + RfqDtAdvtift + "\" tenderAdvertDays=\"" + RfqDtAdvtiftNo + "\" "
        + "tenderSubDt=\"" + RfqDtSub + "\" tenderSubDays=\"" + RfqDtSubNo + "\" "
        + "tenderOpenDt=\"" + RfqExpDtopen + "\" tenderOpenDays=\"" + RfqExpDtopenNo + "\""
        + " techSubCmtRptDt=\"\" techSubCmtRptDays=\"\" "
        + "tenderEvalRptDt=\"" + RfqDtSubEvaRpt + "\" tenderEvalRptdays=\"" + RfqdtSubEvaRptNo + "\" "
        + "tenderEvalRptAppDt=\"\" tenderEvalRptAppDays=\"\" "
        + "tenderContractAppDt=\"" + RfqExpDtAppawd + "\" tenderContractAppDays=\"" + RfqExpDtAppawdNo + "\" "
        //Code by Proshanto Kumar Saha
        +  "tenderLetterIntentDt=\"" + RfqexpdtLtrIntAwd + "\" tnderLetterIntentDays=\"" + RfqexpdtLtrIntAwdNo + "\" "
        //Code end by Proshanto Kumar Saha
        + "tenderNoaIssueDt=\"" + RfqDtIssNOA + "\" tenderNoaIssueDays=\"" + RfqDtIssNOANo + "\" "
        + "tenderContractSignDt=\"" + RfqExpDtSign + "\" tenderContractSignDays=\"" + RfqExpDtSignNo + "\" "
        + "tenderContractCompDt=\"" + RfqExpDtCompContract + "\" "
        + "reoiReceiptDt=\"\" reoiReceiptDays=\"\" rfpTechEvalDt=\"\" rfpTechEvalDays=\"\" rfpFinancialOpenDt=\"\" rfpFinancialOpenDays=\"\" rfpNegCompDt=\"\" rfpNegCompDays=\"\" rfpContractAppDt=\"\" rfpContractAppDays=\"\" rfaAdvertDt=\"\" rfaAdvertDays=\"\" rfaReceiptDt=\"\" rfaReceiptDays=\"\" rfaEvalDt=\"\" rfaEvalDays=\"\" rfaInterviewDt=\"\" rfaInterviewDays=\"\" rfaFinalSelDt=\"\" rfaFinalSelDays=\"\" rfaEvalRptSubDt=\"\" rfaEvalRptSubDays=\"\" rfaAppConsultantDt=\"\" actAdvtDt=\"\" actSubDt=\"\" actEvalRptDt=\"\" actAppLstDt=\"\" actTenderAdvertDt=\"\" actTenderSubDt=\"\" actTenderOpenDt=\"\" actTechSubCmtRptDt=\"\" actTenderEvalRptDt=\"\" acttenderEvalRptAppDt=\"\" actTenderContractAppDt=\"\" actTenderNoaIssueDt=\"\" actTenderContractSignDt=\"\" actTenderContractCompDt=\"\" actReoiReceiptDt=\"\" actRfpTechEvalDt=\"\" actRfpFinancialOpenDt=\"\" actRfpNegComDt=\"\" actRfpContractAppDt=\"\" actRfaAdvertDt=\"\" actRfaReceiptDt=\"\" actRfaEvalDt=\"\" actRfaInterviewDt=\"\" actRfaFinalSelDt=\"\" actRfaEvalRptSubDt=\"\" actRfaAppConsultantDt=\"\" /></root>";
    } else if ("TSTM".equals(dateType)) {
        String topLine="<root><tbl_AppPqTenderDates appid=\""+appId+"\" packageId=\""+pkgId+"\" ";
        if("Update".equalsIgnoreCase(request.getParameter("update"))||"Revise".equalsIgnoreCase(request.getParameter("Revise")))
        topLine="<root><tbl_AppPqTenderDates pqDtId=\""+pqDtId+"\" appid=\""+appId+"\" packageId=\""+pkgId+"\" ";

        dtXml = topLine
        + "advtDt=\"" + TSTMexPdtAdvtIFT + "\" advtDays=\"" + TSTMexPdtadvtIFTNo + "\" openDt=\"" + TSTMexPdtOpen + "\" openDays=\"" + TSTMexPdtOpenNo + "\""
        + " subDt=\"" + TSTMexPdtSub + "\" subDays=\"" + TSTMexPdtSubNo + "\" "
        + "evalRptDt=\"" + TSTMexPdtSubEvaRpt + "\" evalRptDays=\"" + TSTMexPdtSubEvaRptNo + "\" "
        + "appLstDt=\"" + TSTMexPdtAppEvaRpt + "\" "
        + "tenderAdvertDt=\"" + TSTM2expDtIssuefinalDoc + "\" tenderAdvertDays=\"" + TSTM2expDtIssuefinalDocNo + "\" "
        + "tenderSubDt=\"" + TSTM2expDtSub + "\" tenderSubDays=\"" + TSTM2expDtSubNo + "\" "
        + "tenderOpenDt=\"" + TSTM2expDtOpen + "\" tenderOpenDays=\"" + TSTM2expDtOpenNo + "\""
        + " techSubCmtRptDt=\"\" techSubCmtRptDays=\"\" "
        + "tenderEvalRptDt=\"" + TSTM2expDtSubevaRpt + "\" tenderEvalRptdays=\"" + TSTM2expDtSubevaRptNo + "\" "
        + "tenderEvalRptAppDt=\"" + TSTM2expDtAppEvaRpt + "\" tenderEvalRptAppDays=\"" + TSTM2expDtAppEvaRptNo + "\" "
        + "tenderContractAppDt=\"\" tenderContractAppDays=\"\" "
        //Code by Proshanto
        + "tenderLetterIntentDt=\"" + TSTM2expdtLetterOfIntent + "\" tnderLetterIntentDays=\"" + TSTM2expdtLetterOfIntentNo + "\" "
        //Code end by Proshanto
        + "tenderNoaIssueDt=\"" + TSTM2expDtIssueNOA + "\" tenderNoaIssueDays=\"" + TSTM2expDtIssueNOANo + "\" "
        + "tenderContractSignDt=\"" + TSTM2expDtSignContract + "\" tenderContractSignDays=\"" + TSTM2expDtSignContractNo + "\" "
        + "tenderContractCompDt=\""  + TSTM2expdtcomplcontract + "\" "
        + "reoiReceiptDt=\"\" reoiReceiptDays=\"\" rfpTechEvalDt=\"\" rfpTechEvalDays=\"\" rfpFinancialOpenDt=\"\" rfpFinancialOpenDays=\"\" rfpNegCompDt=\"\" rfpNegCompDays=\"\" rfpContractAppDt=\"\" rfpContractAppDays=\"\" rfaAdvertDt=\"\" rfaAdvertDays=\"\" rfaReceiptDt=\"\" rfaReceiptDays=\"\" rfaEvalDt=\"\" rfaEvalDays=\"\" rfaInterviewDt=\"\" rfaInterviewDays=\"\" rfaFinalSelDt=\"\" rfaFinalSelDays=\"\" rfaEvalRptSubDt=\"\" rfaEvalRptSubDays=\"\" rfaAppConsultantDt=\"\" actAdvtDt=\"\" actSubDt=\"\" actEvalRptDt=\"\" actAppLstDt=\"\" actTenderAdvertDt=\"\" actTenderSubDt=\"\" actTenderOpenDt=\"\" actTechSubCmtRptDt=\"\" actTenderEvalRptDt=\"\" acttenderEvalRptAppDt=\"\" actTenderContractAppDt=\"\" actTenderNoaIssueDt=\"\" actTenderContractSignDt=\"\" actTenderContractCompDt=\"\" actReoiReceiptDt=\"\" actRfpTechEvalDt=\"\" actRfpFinancialOpenDt=\"\" actRfpNegComDt=\"\" actRfpContractAppDt=\"\" actRfaAdvertDt=\"\" actRfaReceiptDt=\"\" actRfaEvalDt=\"\" actRfaInterviewDt=\"\" actRfaFinalSelDt=\"\" actRfaEvalRptSubDt=\"\" actRfaAppConsultantDt=\"\" /></root>";
    } else if ("REOI".equals(dateType)) {
         String topLine="<root><tbl_AppPqTenderDates appid=\""+appId+"\" packageId=\""+pkgId+"\" advtDt=\""+REOQexpDtAdvtREOI +"\" advtDays=\""+REOQexpDtAdvtREOINo +"\" openDt=\"\" openDays=\"\" subDt=\""+REOQexpDtSubsrtlstFrm+"\" subDays=\""+REOQexpDtSubsrtlstFrmNo+"\" evalRptDt=\"\" evalRptDays=\"\" appLstDt=\""+REOQextDtAppsrtlstFrm+"\" ";
        if("Update".equalsIgnoreCase(request.getParameter("update"))||"Revise".equalsIgnoreCase(request.getParameter("Revise")))
            topLine="<root><tbl_AppPqTenderDates pqDtId=\""+pqDtId+"\" appid=\""+appId+"\" packageId=\""+pkgId+"\" advtDt=\""+REOQexpDtAdvtREOI +"\" advtDays=\""+REOQexpDtAdvtREOINo +"\" openDt=\"\" openDays=\"\" subDt=\""+REOQexpDtSubsrtlstFrm+"\" subDays=\""+REOQexpDtSubsrtlstFrmNo+"\" evalRptDt=\"\" evalRptDays=\"\" appLstDt=\""+REOQextDtAppsrtlstFrm+"\" ";
 
        dtXml = topLine
        + "tenderAdvertDt=\""+RFPexpDtIssueRFP  +"\" tenderAdvertDays=\"" + RFPexpDtIssueRFPNo  + "\" "
        + "tenderSubDt=\"" +  RFPexpDtSubProposal  + "\" tenderSubDays=\"" +  RFPexpDtSubProposalNo  + "\" "
        + "tenderOpenDt=\"" + RFPexpDtTechOpen  + "\" tenderOpenDays=\"" + RFPexpDtTechOpenNo  + "\" "
        + "techSubCmtRptDt=\"\" techSubCmtRptDays=\"\" "
        + "tenderEvalRptDt=\"" + RFPexpDtSubCOmEvaRpt + "\" tenderEvalRptdays=\"" + RFPexpDtSubCOmEvaRptNo + "\" "
        + "tenderEvalRptAppDt=\"" + RFPexpDtAppcomEvaRpt + "\" tenderEvalRptAppDays=\"" + RFPexpDtAppcomEvaRptNo + "\" tenderContractAppDt=\"\" tenderContractAppDays=\"\" tenderNoaIssueDt=\"\" tenderNoaIssueDays=\"\" "
        + "tenderContractSignDt=\"" + RFPexpDtSigncontract + "\" tenderContractSignDays=\"" + RFPexpDtSigncontractNo + "\""
        + " tenderContractCompDt=\"" + RFPexpDtComplcontract + "\" reoiReceiptDt=\""+REOQexpDtlstDtRcptEOI+"\" reoiReceiptDays=\""+REOQexpDtlstDtRcptEOINo+"\" "
        + "rfpTechEvalDt=\"" + RFPexpDtTechEva  + "\" rfpTechEvalDays=\"" + RFPexpDtTechEvaNo  + "\" "
        + "rfpFinancialOpenDt=\"" + RFPextDtFinOpen  + "\" rfpFinancialOpenDays=\"" + RFPextDtFinOpenNo   + "\" "
        + "rfpNegCompDt=\"" + RFPexpDtCompNego + "\" rfpNegCompDays=\"" + RFPexpDtCompNegoNo + "\" "
        + "rfpContractAppDt=\"" + RFPexpDtAppawdContract + "\" rfpContractAppDays=\"" + RFPexpDtAppawdContractNo + "\" "
        + "rfaAdvertDt=\"\" rfaAdvertDays=\"\" rfaReceiptDt=\"\" rfaReceiptDays=\"\" rfaEvalDt=\"\" rfaEvalDays=\"\" rfaInterviewDt=\"\" rfaInterviewDays=\"\" rfaFinalSelDt=\"\" rfaFinalSelDays=\"\" rfaEvalRptSubDt=\"\" rfaEvalRptSubDays=\"\" rfaAppConsultantDt=\"\" actAdvtDt=\"\" actSubDt=\"\" actEvalRptDt=\"\" actAppLstDt=\"\" actTenderAdvertDt=\"\" actTenderSubDt=\"\" actTenderOpenDt=\"\" actTechSubCmtRptDt=\"\" actTenderEvalRptDt=\"\" acttenderEvalRptAppDt=\"\" actTenderContractAppDt=\"\" actTenderNoaIssueDt=\"\" actTenderContractSignDt=\"\" actTenderContractCompDt=\"\" actReoiReceiptDt=\"\" actRfpTechEvalDt=\"\" actRfpFinancialOpenDt=\"\" actRfpNegComDt=\"\" actRfpContractAppDt=\"\" actRfaAdvertDt=\"\" actRfaReceiptDt=\"\" actRfaEvalDt=\"\" actRfaInterviewDt=\"\" actRfaFinalSelDt=\"\" actRfaEvalRptSubDt=\"\" actRfaAppConsultantDt=\"\" /></root>";

    }else if ("RFA".equals(dateType) || "RFP".equals(dateType)) {
        String topLine="<root><tbl_AppPqTenderDates appid=\""+appId+"\" packageId=\""+pkgId+"\" advtDt=\"\" advtDays=\"\" openDt=\"\" openDays=\"\" subDt=\"\" subDays=\"\" evalRptDt=\"\" evalRptDays=\"\" appLstDt=\"\" tenderAdvertDt=\"\" tenderAdvertDays=\"\" tenderSubDt=\"\" tenderSubDays=\"\" tenderOpenDt=\"\" tenderOpenDays=\"\" techSubCmtRptDt=\"\" techSubCmtRptDays=\"\" tenderEvalRptDt=\"\" tenderEvalRptdays=\"\" tenderEvalRptAppDt=\"\" tenderEvalRptAppDays=\"\" tenderContractAppDt=\"\" tenderContractAppDays=\"\" tenderNoaIssueDt=\"\" tenderNoaIssueDays=\"\" tenderContractSignDt=\"\" tenderContractSignDays=\"\" tenderContractCompDt=\"\" reoiReceiptDt=\"\" reoiReceiptDays=\"\" rfpTechEvalDt=\"\" rfpTechEvalDays=\"\" rfpFinancialOpenDt=\"\" rfpFinancialOpenDays=\"\" rfpNegCompDt=\"\" rfpNegCompDays=\"\" rfpContractAppDt=\"\" rfpContractAppDays=\"\"";
        if("Update".equalsIgnoreCase(request.getParameter("update"))||"Revise".equalsIgnoreCase(request.getParameter("Revise")))
            topLine="<root><tbl_AppPqTenderDates pqDtId=\""+pqDtId+"\" appid=\""+appId+"\" packageId=\""+pkgId+"\" advtDt=\"\" advtDays=\"\" openDt=\"\" openDays=\"\" subDt=\"\" subDays=\"\" evalRptDt=\"\" evalRptDays=\"\" appLstDt=\"\" tenderAdvertDt=\"\" tenderAdvertDays=\"\" tenderSubDt=\"\" tenderSubDays=\"\" tenderOpenDt=\"\" tenderOpenDays=\"\" techSubCmtRptDt=\"\" techSubCmtRptDays=\"\" tenderEvalRptDt=\"\" tenderEvalRptdays=\"\" tenderEvalRptAppDt=\"\" tenderEvalRptAppDays=\"\" tenderContractAppDt=\"\" tenderContractAppDays=\"\" tenderNoaIssueDt=\"\" tenderNoaIssueDays=\"\" tenderContractSignDt=\"\" tenderContractSignDays=\"\" tenderContractCompDt=\"\" reoiReceiptDt=\"\" reoiReceiptDays=\"\" rfpTechEvalDt=\"\" rfpTechEvalDays=\"\" rfpFinancialOpenDt=\"\" rfpFinancialOpenDays=\"\" rfpNegCompDt=\"\" rfpNegCompDays=\"\" rfpContractAppDt=\"\" rfpContractAppDays=\"\"";

        dtXml = topLine
        + " rfaAdvertDt=\"" + RFAexpdtAdvtRFA + "\" rfaAdvertDays=\"" + RFAexpdtAdvtRFANo + "\" "
        + "rfaReceiptDt=\"" + RFAdtrcptApp + "\" rfaReceiptDays=\"" + RFAdtrcptAppNo + "\" "
        + "rfaEvalDt=\"" + RFAdtEveapp + "\" rfaEvalDays=\"" + RFAdtEveappNo + "\" "
        + "rfaInterviewDt=\"" + RFAdtIntrvwselInd + "\" rfaInterviewDays=\"" + RFAdtIntrvwselIndNo + "\" "
        + "rfaFinalSelDt=\"" + RFAdtEvaFnlselLst + "\" rfaFinalSelDays=\"" + RFAdtEvaFnlselLstNo + "\""
        + " rfaEvalRptSubDt=\"" + RFAdtEubevaRpt + "\" rfaEvalRptSubDays=\"" + RFAdtSubevaRptNo + "\" "
        + "rfaAppConsultantDt=\"" + RFAdtAppCons + "\""
        + " actAdvtDt=\"\" actSubDt=\"\" actEvalRptDt=\"\" actAppLstDt=\"\" actTenderAdvertDt=\"\" actTenderSubDt=\"\" actTenderOpenDt=\"\" actTechSubCmtRptDt=\"\" actTenderEvalRptDt=\"\" acttenderEvalRptAppDt=\"\" actTenderContractAppDt=\"\" actTenderNoaIssueDt=\"\" actTenderContractSignDt=\"\" actTenderContractCompDt=\"\" actReoiReceiptDt=\"\" actRfpTechEvalDt=\"\" actRfpFinancialOpenDt=\"\" actRfpNegComDt=\"\" actRfpContractAppDt=\"\" actRfaAdvertDt=\"\" actRfaReceiptDt=\"\" actRfaEvalDt=\"\" actRfaInterviewDt=\"\" actRfaFinalSelDt=\"\" actRfaEvalRptSubDt=\"\" actRfaAppConsultantDt=\"\" /></root>";
    }


   /*
    out.println("<Br>Expected Date of Advertisement of Invitation on e-GP website: " + advtDate);
    out.println("<Br>No. of days:" + advtDateNoDays);
    out.println("<Br>Expected Date of Applications Submission " + appSubDate);
    out.println("<Br>No. of days:" + appSubDateNoDays);
    out.println("<br>Expected Date Of Submission Of Evaluation Report With Recommended List : " + appSubDateExp);
    out.println("<Br>No. of days:" + appSubDateNoDaysExp);
    out.println("<Br>Expected Date of Approval Of List :" + appList);

    out.println("<br>Tender / RFQ Dates:");
    out.println("<Br>Expected Date Of Advertisement Of IFT on e-GP website : " + RfqDtAdvtift);
    out.println("<Br>No. of days:" + RfqDtAdvtiftNo);
    out.println("<Br>Expected Date of Submission of Tenders : " + RfqDtSub);
    out.println("<Br>No. of days:" + RfqDtSubNo);
    out.println("<Br>Expected Date of Opening of Tenders : " + RfqExpDtopen);
    out.println("<Br>No. of days:" + RfqExpDtopenNo);
    out.println("<Br>Expected Date of Submission of Evaluation Report :" + RfqDtSubEvaRpt);
    out.println("<Br>No. of days:" + RfqdtSubEvaRptNo);
    out.println("<Br>Expected Date of Approval for Award of Contract : " + RfqExpDtAppawd);
    out.println("<Br>No. of days:" + RfqExpDtAppawdNo);
    out.println("<Br>Expected Date of Issuance of the LOA :" + RfqDtIssNOA);
    out.println("<Br>No. of days:" + RfqDtIssNOANo);
    out.println("<Br>Expected Date Of Signing of Contract :" + RfqExpDtSign);
    out.println("<Br>No. of days:" + RfqExpDtSignNo);
    out.println("<Br>Expected Date of Completion of Contract  : " + RfqExpDtCompContract);
    out.println("<br><br>");

    out.println("<Br> TSTM 1st Stage:");
    out.println("<br>Expected Date Of Advertisement Of IFT on e-GP website :" + TSTMexPdtAdvtIFT);
    out.println("<br>No. of days:" + TSTMexPdtadvtIFTNo);
    out.println("<br>Expected Date of Submission of Tenders:" + TSTMexPdtSub);
    out.println("<br>No. of days:" + TSTMexPdtSubNo);
    out.println("<br>Expected Date of Opening of Tenders :" + TSTMexPdtOpen);
    out.println("<br>No. of days:" + TSTMexPdtOpenNo);
    out.println("<br>Expected Date of Submission of Evaluation Report :" + TSTMexPdtSubEvaRpt);
    out.println("<br>No. of days:" + TSTMexPdtSubEvaRptNo);
    out.println("<br>Expected Date of Approval of the Evaluation Report : :" + TSTMexPdtAppEvaRpt);
    out.println("<br><br>");

    out.println("<Br> TSTM 2nd Stage:");
    out.println("<br>Expected Date of issue of Final Tender Document to qualified tenderers : " + TSTM2expDtIssuefinalDoc);
    out.println("<br>No. of days:" + TSTM2expDtIssuefinalDocNo);
    out.println("<br>Expected Date of Submission of Tenders :" + TSTM2expDtSub);
    out.println("<br>No. of days:" + TSTM2expDtSubNo);
    out.println("<br>Expected Date of Opening of Tenders :" + TSTM2expDtOpen);
    out.println("<br>No. of days:" + TSTM2expDtOpenNo);
    out.println("<br>Expected Date of Submission of Evaluation Report : " + TSTM2expDtSubevaRpt);
    out.println("<br>No. of days:" + TSTM2expDtSubevaRptNo);
    out.println("<br>Expected Date of Approval of the Evaluation Report :" + TSTM2expDtAppEvaRpt);
    out.println("<br>No. of days:" + TSTM2expDtAppEvaRptNo);
    out.println("<br>Expected Date of Issue of LOA : " + TSTM2expDtIssueNOA);
    out.println("<br>No. of days:" + TSTM2expDtIssueNOANo);
    out.println("<br>Expected Date Of Signing of Contract :" + TSTM2expDtSignContract);
    out.println("<br>No. of days:" + TSTM2expDtSignContractNo);
*/ 
    if(("Save".equalsIgnoreCase(request.getParameter("save"))) || ("Save and Add More Package".equalsIgnoreCase(request.getParameter("saveadd"))) || ("Update".equalsIgnoreCase(request.getParameter("update"))) || "Revise".equalsIgnoreCase(request.getParameter("Revise")))
    {
            StringBuilder action=new StringBuilder();
            String auditAction = "";
            ///action.append(request.getParameter("save"));
            if("Save".equalsIgnoreCase(request.getParameter("save")) || ("Save and Add More Package".equalsIgnoreCase(request.getParameter("saveadd")))){
                //action.delete(0, action.length());
                action.append("insert");
                auditAction = "Add Package Dates";
            }else{
                action.append("update");
                auditAction = "Edit Package Dates";
            }
            
            appServlet.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
            boolean checkDateExist=false;
            if(!("Update".equalsIgnoreCase(request.getParameter("update"))||"Revise".equalsIgnoreCase(request.getParameter("Revise"))))
            {
               checkDateExist=appServlet.chkPkgDatesExist(pkgId+""); 
            }
            
            if(!checkDateExist)
           {
                if(appServlet.addAPPPkgDates(dtXml,action.toString(),"appId",String.valueOf(appId),auditAction,"")){
                    //String fwdToPage = "";
                    if(("Save".equalsIgnoreCase(request.getParameter("save"))) || ("Update".equalsIgnoreCase(request.getParameter("update"))) ||"Revise".equalsIgnoreCase(request.getParameter("Revise"))){
                        //fwdToPage = "APPDashboard.jsp";
                        response.sendRedirect("APPDashboard.jsp?appID="+appId);
                    }else if(("Save and Add More Package".equalsIgnoreCase(request.getParameter("saveadd")))){
                        //fwdToPage = "AddPackageDetail.jsp";
                        response.sendRedirect("AddPackageDetail.jsp?appId="+appId);
                    }
                }else{
                    response.sendRedirect("AddPackageDates.jsp?msg=Error&appId"+appId);
                }
           }
           else
           {
                if(("Save".equalsIgnoreCase(request.getParameter("save"))) || ("Update".equalsIgnoreCase(request.getParameter("update")))||"Revise".equalsIgnoreCase(request.getParameter("Revise")))
                {
                    response.sendRedirect("APPDashboard.jsp?appID="+appId);
                }
                else if(("Save and Add More Package".equalsIgnoreCase(request.getParameter("saveadd"))))
                {
                        //fwdToPage = "AddPackageDetail.jsp";
                        response.sendRedirect("AddPackageDetail.jsp?appId="+appId);
                }
                else
                {
                    response.sendRedirect("AddPackageDates.jsp?msg=Error&appId"+appId);
                }
           }
    }
%>