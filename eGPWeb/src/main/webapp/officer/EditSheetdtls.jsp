<%-- 
    Document   : EditSheetdtls
    Created on : Dec 12, 2011, 4:35:09 PM
    Author     : shreyansh Jogi
--%>

 <%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.web.utility.MonthName"%>
<%
        CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
        List<Object> formmapId = cmss.getSrvFormMapId(Integer.parseInt(request.getParameter("tenderId")), 15);
        List<Object[]> list = null;
        int j=0;
        if(formmapId!=null && !formmapId.isEmpty()){
           list = cmss.getListForPRForTB((Integer)formmapId.get(0),30);
            }

%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Progress Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        <script src="../resources/js/monthDaysCalculation.js" type="text/javascript"></script>
        <script type="text/javascript">
             var daysArray1 = new Array();
             var daysArray2 = new Array();
             var daysArray3 = new Array();
        </script>
          <%
                                            if(list!=null && !list.isEmpty()){
                                                int i = 0;
                                                for(Object obj[] : list){%>
                                                <script type="text/javascript">
                                                        daysArray1[<%=i%>] = '<%=obj[5]%>';
                                                        daysArray2[<%=i%>] = '<%=obj[6]%>';
                                                        daysArray3[<%=i%>] = '<%=obj[3]%>';
                                                </script>
                                                <% i++;}
                                                }%>
        <script>
            function ViewPRR(wpId,tenderId,lotId,cntId){
                dynamicFromSubmit("viewProgressReport.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&cntId="+cntId);
            }
            function checkForAttSheet1(){
                    document.getElementById("action").value = "editsheet";
                    document.getElementById("status").value = "accept";
                    document.forms["prfrm"].submit();
            }
            function checkForAttSheet2(){
                    document.getElementById("action").value = "editsheet";
                     document.getElementById("status").value = "reject";
                    document.forms["prfrm"].submit();
            }
            
       
        </script>
    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Edit Attendance Sheet
            <span class="c-alignment-right">
                <%
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");            
                    String serviceType = commonService.getServiceTypeForTender(Integer.parseInt(request.getParameter("tenderId")));
                    if("Time based".equalsIgnoreCase(serviceType.toString()))
                    {    
                %>
                        <a href="ProgressReportMain.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a>
                <%}else{%>
                        <a href="SrvLumpSumPr.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a>
                <%}%>
            </span>
            </div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "10");

            %>
                <div class="tabPanelArea_1">
                    <div align="center">

                        <%
                                    ResourceBundle bdl = null;
                                    bdl = ResourceBundle.getBundle("properties.cmsproperty");

                                %>
                                <form name="prfrm" id="prfrm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post">
                                 <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                            <tr>
                                 <th width="3%" class="t-align-center">Sr.No.</th>
                                                <th width="20%" class="t-align-center">Name of Employees</th>
                                                <th width="15%" class="t-align-center">Position Assigned
                                                </th>
                                                <th width="9%" class="t-align-center">Home / Field
                                                </th>
                                                <th width="5%" class="t-align-center">No. of Days
                                                </th>
                                                <th width="18%" class="t-align-center">Month
                                                </th>
                                                <th width="18%" class="t-align-center">Year
                                                </th>
                                                 <th width="10%" class="t-align-center">Remarks
                                                </th>
                                            </tr>

                                                <%
                                                List<Object[]> listt = cmss.ViewAttSheetDtls(Integer.parseInt(request.getParameter("sheetId")));
                                                 MonthName monthName = new MonthName();
                                                 int jj = 0;
                                            if(listt!=null && !listt.isEmpty()){

                                                for(Object obj[] : listt){%>
                                                <tr>
                                                <td width="3%" class="t-align-left"><%=obj[0]%></td>
                                                <td width="20%" class="t-align-left"><%=obj[1]%></td>
                                                <td width="15%" class="t-align-left"><%=obj[2]%>
                                                </td>
                                                
                                                <td width="5%" class="t-align-left"><%=obj[7]%>
                                                </td>
                                                <td width="9%" style="text-align: right" ><%=obj[3]%>
                                                </td>
                                                <td width="5%" class="t-align-left"><%=monthName.getMonth((Integer) obj[5]) %>
                                                </td>
                                                <td width="5%" style="text-align: right"><%=obj[6]%>
                                                </td>
                                                <td width="5%" class="t-align-left"><input type="text" class="formTxtBox_1" value="" name="remarks_<%=jj%>" id="remarks_<%=jj%>"  />
                                                </td>
                                                </tr>
                                                <input type="hidden" name="attdtlsheetId_<%=jj%>" value="<%=obj[8]%>" />
                                                

                                                   <% jj++;}}%>
                                                   <input type="hidden" name="attsheetId" value="<%=listt.get(0)[9]%>" />
                                                   <input type="hidden" name="listcount" value=<%=jj%> />
                                                    <input type="hidden" name="action" id="action" value="" />
                                                    <input type="hidden" name="tenderId" value="<%=request.getParameter("tenderId")%>" />
                                                    <input type="hidden" name="lotId" value="<%=request.getParameter("lotId")%>" />
                                                    <input type="hidden" name="status" id="status" value="" />
<tr><td colspan="8"><center>
                <label class="formBtn_1">
                    <input type="button" name="button" onclick="checkForAttSheet1();" id="statusbtnaccpt" value="Accept" />
                </label>&nbsp;
                <label class="formBtn_1">
                    <input type="button" name="button" onclick="checkForAttSheet2();" id="statusbtnrjct"  value="Reject" />
                </label>
        </center> </td></tr>
                                 </table>
                                </form>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="../resources/common/Bottom.jsp" %>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>


