<%-- 
    Document   : SrvReExpense
    Created on : Dec 5, 2011, 11:52:47 AM
    Author     : dixit
--%>

<%@page import="java.math.RoundingMode"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsNewBankGuarnatee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsNewBankGuarnateeService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.MonthName"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvAttSheetMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payment</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>

<%
String tenderId = "";
                        if(request.getParameter("tenderId")!=null)
                        {
                            tenderId = request.getParameter("tenderId");
                            pageContext.setAttribute("tenderId", tenderId);
                        }   %>
    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Payment</div>            
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
                <div class="tabPanelArea_1">
            <%
                        pageContext.setAttribute("tab", "14");
                        ResourceBundle bdl = null;
                        bdl = ResourceBundle.getBundle("properties.cmsproperty");
                                             
                        String lotId = "";
                        String wpId = "";
                        ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                        CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                        CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                        CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                        List<Object[]> lotObj = cs.getLotDetails(tenderId);
                        Object[] obj = null;
                        if(lotObj!=null && !lotObj.isEmpty())
                        {
                            obj = lotObj.get(0);
                            lotId = obj[0].toString();
                        }
                        List<Object[]> WpIdObj = cmss.getWpId(Integer.parseInt(tenderId));
                        Object[] objwp = null;
                        if(WpIdObj!=null && !WpIdObj.isEmpty())
                        {
                            objwp = WpIdObj.get(0);
                            wpId = objwp[3].toString();
                        }
                        List<Object[]> listSrvFormMapId = cmss.getSrvFormMapId(Integer.parseInt(tenderId));
                        Object[] srvFormMapobj = null;
                        if(!listSrvFormMapId.isEmpty())
                        {
                            srvFormMapobj = listSrvFormMapId.get(0);
                            //lotId = srvFormMapobj[1].toString();
                            //wpId = srvFormMapobj[3].toString();
                        }                                                                                                    
                        pageContext.setAttribute("lotId", lotId); 
                        boolean flagg = true;
                        boolean flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotId));
                        CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                        String serviceType = commService.getServiceTypeForTender(Integer.parseInt(tenderId));
                        if("Time based".equalsIgnoreCase(serviceType.toString()))
                        {
                            flagg = false;
                        }%>
                        <%@include file="../resources/common/ContractInfoBar.jsp"%>
            <div>&nbsp;</div>
            <%@include  file="officerTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "4");

                %>
                <%@include  file="../resources/common/CMSTab.jsp"%>
                <div class="tabPanelArea_1">
                        <%if(flags){
            %>

                    <% if (request.getParameter("msg") != null) {
                         if ("accept".equalsIgnoreCase(request.getParameter("msg"))) {
                    %>
                    <div class='responseMsg successMsg'>Invoice Generated Successfully</div>
                    <%}if("reject".equalsIgnoreCase(request.getParameter("msg"))){
                    %>
                        <div class='responseMsg successMsg'><%=bdl.getString("CMS.Inv.reject")%></div>
                    <%    
                    }}%>
                    <div align="center">
                    <%   out.print("<table width='100%' border='0' cellpadding='0' cellspacing='0' class='tableList_1 t_space'");
                         String userId = "";
                         if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                             userId = session.getAttribute("userId").toString();
                         }
                         CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                         TenderCommonService wfTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                         CmsNewBankGuarnateeService cmsNewBankGuarnateeService = (CmsNewBankGuarnateeService) AppContext.getSpringBean("CmsNewBankGuarnateeService");
                         List<SPTenderCommonData> checkuserRights = wfTenderCommonService.returndata("CheckTenderUserRights",userId,request.getParameter("tenderId"));
                         boolean allowReleaseForfeit = false;
                         int j = 0;
                         String paymentToId = "";
                         List<SPCommonSearchData> lstPayments= commonSearchService.searchData("getPaidTendererListForPE", tenderId, lotId, "Performance Security", userId, null, null, null, null, null);
                         List<SPCommonSearchData> lstBankGuarantee = commonSearchService.searchData("getPaidTendererListForPE", tenderId, lotId, "Bank Guarantee", userId, null, null, null, null, null);
                         CommonSearchDataMoreService objPayment = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                         int icountcon=0;
                         if(lstPayments!=null && !lstPayments.isEmpty()){
                         out.print("<tr>");
                         out.print("<td class='ff' width='20%'  class='t-align-left'>Performance Security</td>");
                         out.print("<td><table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                         out.print("<tr><th width='4%' class='t-align-left'>Sl. No.</th>");
                         out.print("<th width='15%' class='t-align-left'>Contract No.</th>");
                         out.print("<th width='38%' class='t-align-left'>Company Name</th>");
                         out.print("<th class='t-align-left' width='12%'>Payment Status</th>");
                         out.print("<th class='t-align-left' width='12%'>Payment Amount</th>");
                         out.print("<th class='t-align-left' width='12%'>Payment Date</th>");
                         out.print("<th class='t-align-left' width='30%'>Action</th></tr>");    
                         for (SPCommonSearchData sptcd : lstPayments)
                         {
                             List<Object[]> cntDetails = service.getContractNo(Integer.parseInt(sptcd.getFieldName9()));
                             j++;
                             out.print("<tr");
                             if(Math.IEEEremainder(j,2)==0){
                             out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                             out.print("<td class='t-align-left'>"+j+"</td>");
                             out.print("<td class='t-align-left'>");
                             if(cntDetails!=null && !cntDetails.isEmpty() ){
                             if("yes".equalsIgnoreCase(cntDetails.get(0)[1].toString())){
                                 out.print(""+cntDetails.get(0)[0]+" (For Repeat Order)");
                                 }else{
                                 out.print(""+cntDetails.get(0)[0]+"");
                                 }

                             }
                             out.print("</td>");
                             out.print("<td class='t-align-left'>"+sptcd.getFieldName2()+"</td>");
                             if("Forfeited".equalsIgnoreCase(sptcd.getFieldName8())){
                                 out.print("<td class='t-align-left'>Compensated</td>");
                             }else{
                                 out.print("<td class='t-align-left'>"+sptcd.getFieldName8()+"</td>");
                             }
                             List<SPCommonSearchDataMore> lstPaymentDetail = objPayment.geteGPData("getTenderPaymentDetail", sptcd.getFieldName1(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);


                              out.print("<td class='t-align-left'>");
                              if("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){ %>
                                <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                            <% }else if("USD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                <label>$</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                            <% }else if("Nu.".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                            <% }
                             out.print("</td>");
                             out.print("<td class='t-align-left'>"+lstPaymentDetail.get(0).getFieldName6()+"</td>");
                             out.print("<td class='t-align-left'>");
                             if(!checkuserRights.isEmpty()){
                                 paymentToId = sptcd.getFieldName3();
                             out.print("<a href='"+request.getContextPath()+"/partner/ViewTenderPaymentDetails.jsp?payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Payment Details</a>");
                             if("Paid".equalsIgnoreCase(sptcd.getFieldName8()) || "Extended".equalsIgnoreCase(sptcd.getFieldName8())){
                                 if("ps".equalsIgnoreCase(sptcd.getFieldName6())) {
                                    String strActionRequest = "";
                                    List<SPTenderCommonData> lstPaymentActionRequest =
                                                                  tenderCommonService.returndata("getRequestActionFromPaymentId", sptcd.getFieldName1(), null);
                                    if(lstPaymentActionRequest.isEmpty()){
                                        allowReleaseForfeit = true;
                                    } else {
                                        allowReleaseForfeit = false;
                                        strActionRequest = lstPaymentActionRequest.get(0).getFieldName2();
                                    }
                                    if("Release".equalsIgnoreCase(strActionRequest)){
                                        strActionRequest = "Released";
                                    }
                                    if(allowReleaseForfeit){
                                        if(!lstBankGuarantee.isEmpty())
                                            {
                                                out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestrelease&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Release Request</a>");
                                            }
                                                out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestforfeit&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Compensate Request</a>");

                                    } else {
                                        if("Forfeit".equalsIgnoreCase(strActionRequest))
                                        {
                                            out.print("&nbsp;&nbsp;(Compensate)");
                                        }else{
                                        out.print("&nbsp;&nbsp;("+strActionRequest+")");
                                        }
                                    }
                                 }
                              }
                              }
                              out.print("</td></tr>");
                              icountcon++;
                             }
                             if(j==0){
                                out.print("<tr><td colspan='7'>No records available</td></tr>");
                             }
                             out.print(" </table></td></tr>");
                             }
                         out.print(" <tr>");
                         out.print("<td class='ff' width='20%'  class='t-align-left'>New Performance Security</td>");

                         List<Object[]> listBg = cmsNewBankGuarnateeService.fetchBankGuaranteeList(Integer.parseInt(tenderId),Integer.parseInt(userId));
                         out.print("<td>");

                         if(!lstPayments.isEmpty() || true)
                         {
                             TblCmsNewBankGuarnatee tblCmsNewBankGuarnatee = cmsNewBankGuarnateeService.fetchBankGuarantee(Integer.parseInt(tenderId));
                             //if(tblCmsNewBankGuarnatee==null){
                             out.print("<a href='"+request.getContextPath()+"/officer/RequestBankGuarantee.jsp?uId="+service.getL1bidderuserID(tenderId)+"&tenderId="+tenderId+"&lotId="+lotId+"'>Request Bank Guarantee for Additional Performance Security</a>");
                             //out.print("<a href='"+request.getContextPath()+"/officer/RequestBankGuarantee.jsp?uId="+paymentToId+"&tenderId="+tenderId+"&lotId="+lotId+"'>Request Bank Guarantee for Additional Performance Security</a>");
                            // }else{
                             //out.print("Bank Guarantee Requested");
                            // }
                             if((listBg!=null && !listBg.isEmpty()) || (lstBankGuarantee!=null && !lstBankGuarantee.isEmpty())){
                             out.print("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                             out.print("<tr><th width='4%' class='t-align-center'>Sl. No.</th>");
                             out.print("<th width='7%' class='t-align-left'>Contract  No.</th>");
                             out.print("<th width='38%' class='t-align-left'>Company Name</th>");
                             out.print("<th class='t-align-center' width='12%'>Payment Status</th>");
                             out.print("<th class='t-align-left' width='30%'>Action</th></tr>");
                             j = 0;
                             int icountforBG=0;
                             for(Object[] obj1: listBg){
                                 j++;

                                 out.print("<tr");
                                 if(Math.IEEEremainder(j,2)==0){
                                     out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                                     out.print("<td class='t-align-center'>"+j+"</td>");
                                     out.print("<td class='t-align-left'>");
                                     if("yes".equalsIgnoreCase(obj1[4].toString())){
                                        out.print(""+obj1[3]+" (For Repeat Order)");
                                     }else{
                                        out.print(""+obj1[3]+"");
                                     }
                                     out.print("</td>");
                                     String strCompname = "";
                                     if("1".equals(obj1[6].toString())){
                                         strCompname = obj1[0].toString()+" "+obj1[1].toString();
                                     }else{
                                         strCompname = obj1[7].toString();
                                     }
                                     out.print("<td class='t-align-left'>"+strCompname+"</td>");
                                     out.print("<td class='t-align-center'>Pending</td>");
                                     out.print("<td class='t-align-left'>");
                                     out.print("<a href='"+request.getContextPath()+"/officer/ViewBankGuarantee.jsp?uId="+paymentToId+"&tenderId="+tenderId+"&lotId="+lotId+"&bgId="+obj1[2]+"'>View</a>");
                                     out.print("</td>");
                                     out.print("</tr>");
                                      icountforBG++;

                             }
                             for (SPCommonSearchData sptcd : lstBankGuarantee)
                             {
                                  List<Object[]> cntDetails = service.getContractNo(Integer.parseInt(sptcd.getFieldName9()));
                                 j++;
                                 out.print("<tr");
                                 if(Math.IEEEremainder(j,2)==0){
                                 out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                                 out.print("<td class='t-align-center'>"+j+"</td>");
                                 out.print("<td class='t-align-left'>");
                                 if(cntDetails!=null && !cntDetails.isEmpty() ){
                                 if("yes".equalsIgnoreCase(cntDetails.get(0)[1].toString())){
                                 out.print(""+cntDetails.get(0)[0]+" (For Repeat Order)");
                                 }else{
                                 out.print(""+cntDetails.get(0)[0]+"");
                                 }
                                 }
                                 out.print("</td>");
                                 out.print("<td class='t-align-left'>"+sptcd.getFieldName2()+"</td>");
                                 if("Forfeited".equalsIgnoreCase(sptcd.getFieldName8())){
                                     out.print("<td class='t-align-center'>Compensated</td>");
                                 }else{
                                     out.print("<td class='t-align-center'>"+sptcd.getFieldName8()+"</td>");
                                 }
                                 out.print("<td class='t-align-left'>");
                                 if(!checkuserRights.isEmpty()){
                                 out.print("<a href='"+request.getContextPath()+"/partner/ViewTenderPaymentDetails.jsp?payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Payment Details</a>");
                                 if("Paid".equalsIgnoreCase(sptcd.getFieldName8()) || "Extended".equalsIgnoreCase(sptcd.getFieldName8())){
                                     if("bg".equalsIgnoreCase(sptcd.getFieldName6())) {
                                        String strActionRequest = "";
                                        List<SPTenderCommonData> lstPaymentActionRequest =
                                                                      tenderCommonService.returndata("getRequestActionFromPaymentId", sptcd.getFieldName1(), null);
                                        if(lstPaymentActionRequest.isEmpty()){
                                            allowReleaseForfeit = true;
                                        } else {
                                            allowReleaseForfeit = false;
                                            strActionRequest = lstPaymentActionRequest.get(0).getFieldName2();
                                        }
                                        if("Release".equalsIgnoreCase(strActionRequest)){
                                            strActionRequest = "Released";
                                        }
                                        if(allowReleaseForfeit){
                                                out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestrelease&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"'>Release Request</a>");
                                                out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestforfeit&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"'>Compensate Request</a>");

                                        } else {
                                            if("Forfeit".equalsIgnoreCase(strActionRequest))
                                            {
                                                out.print("&nbsp;&nbsp;(Compensate)");
                                            }else{
                                            out.print("&nbsp;&nbsp;("+strActionRequest+")");
                                            }
                                        }
                                     }
                                  }
                                  }
                                  out.print("</td></tr>");

                                 }
                                 if(j==0){
                                    out.print("<tr><td colspan='5'>No records available</td></tr>");
                                 }
                                 out.print("</table>");
                         }}
                         out.print("</td></tr>");
                        %>    
                          <%                                
                                NOAServiceImpl noaServiceImpl = (NOAServiceImpl) AppContext.getSpringBean("NOAServiceImpl");
                                List<Object[]> noalist = noaServiceImpl.getDetailsNOAforInvoice(Integer.parseInt(tenderId),Integer.parseInt(lotId));
                                Object[] noaObj  = null;
                                List<Object[]> invIdData = null;
                                boolean isAdvInvFlag = false;
                                if(!noalist.isEmpty())
                                {
                                    noaObj = noalist.get(0);
                                    if(noaObj[12]!=null && !"0.000".equalsIgnoreCase(noaObj[12].toString()))
                                    {BigDecimal big = new BigDecimal(c_obj[2].toString()).multiply(new BigDecimal(noaObj[12].toString()));
                            %>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                
                                    </table>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                    <tr>
                                        <td width="34%">Advance Amount</td>
                                        <td width="1%">
                                            <%
                                                if(noaObj[12]!=null && !"0.000".equalsIgnoreCase(noaObj[12].toString()))
                                                {out.print(noaObj[12].toString()+"<br/>(In %)");}
                                            %>
                                        </td>
                                        <td width="1%"><%=big.divide(new BigDecimal(100),3,RoundingMode.HALF_UP).setScale(3,0)%><br/><%=bdl.getString("CMS.Inv.InBDT")%></td>
                                        <%
                                            isAdvInvFlag = service.getAcceptedFlag(Integer.parseInt(wpId));
                                            if(isAdvInvFlag)
                                            {
                                        %>
                                                <td width="9%">Invoice has not been generated yet.</td>
                                        <%  }
                                            invIdData = service.getInvoiceMasterDatawithInvAmt(wpId);
                                            if (!invIdData.isEmpty() && invIdData != null) {
                                                for(int k=0;k<invIdData.size(); k++)
                                                {
                                                  Object[] objInvdata = invIdData.get(k);
                                           %>
                                        <td width="9%" class="t_align_left" style="text-align: left">
                                        <table class="tableList_1 t_space">
                                        <tr>
                                            <th width="1%" class="t-align-left">Invoices</th>
                                            <th width="1%" class="t-align-left">Status</th>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&invoiceId=<%=objInvdata[0]%>&wpId=<%=wpId%>"><%=objInvdata[2].toString()%></a><%if("sendtope".equalsIgnoreCase(objInvdata[1].toString())) {%><span class="reqF_1">*</span><%}%></td>
                                            <td class="t-align-left">
                                                <%
                                                    String status = "";
                                                    if (objInvdata[1] == null) {
                                                        status = "-";
                                                    } else if ("createdbyten".equalsIgnoreCase(objInvdata[1].toString())) {
                                                        status = "Invoice Generated by Consultant";
                                                    } else if ("acceptedbype".equalsIgnoreCase(objInvdata[1].toString())) {
                                                        status = "Invoice Accepted by PE";
                                                    } else if ("sendtope".equalsIgnoreCase(objInvdata[1].toString())) {
                                                        status = "In Process";
                                                    } else if ("sendtotenderer".equalsIgnoreCase(objInvdata[1].toString())) {
                                                        status = "Processed by Accounts Officer";
                                                    } else if ("remarksbype".equalsIgnoreCase(objInvdata[1].toString())) {
                                                        status = "Processed by PE";
                                                    } else if ("rejected".equalsIgnoreCase(objInvdata[1].toString())) {
                                                        status = "Invoice Rejected by PE";
                                                    }
                                                    out.print(status);
                                                %>
                                            </td>
                                        </tr>
                                        </table></td>
                                        <%}}%>                                                                                                                                                                                                               
                                    </tr>
                                    </table>
                            <%            
                                    }
                                }    
                            %>
                            <%if(flagg){%>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <th>Sl. No.</th>
                                <th>Milestone Name </th>                                
                                <th>Description</th>                                
                                <th>Payment as % of Contract Value</th>                                
                                <th>Mile Stone Date proposed by PE </th>
                                <th>Mile Stone Date proposed by Consultant</th>
                                <th>Invoice Amount <br/><%=bdl.getString("CMS.Inv.InBDT")%></th>
                                <th>Remarks<br/>By Consultant</th>
                                <th>Action</th>
                                <%                                                                        
                                    List<TblCmsSrvPaymentSch> list = cmss.getPaymentScheduleData(Integer.parseInt(srvFormMapobj[0].toString()));
                                    if(!list.isEmpty()){
                                    for(int i=0; i<list.size(); i++)
                                    {    
                                %>
                                <tr>
                                    <td class="t-align-left"><%=list.get(i).getSrNo()%></td>
                                    <td class="t-align-left"><%=list.get(i).getMilestone()%></td>
                                    <td class="t-align-left"><%=list.get(i).getDescription()%></td>
                                    <td class="t-align-left"><%=list.get(i).getPercentOfCtrVal()%></td>
                                    <td class="t-align-left"><%=DateUtils.customDateFormate(list.get(i).getPeenddate())%></td>
                                    <td class="t-align-left"><%=DateUtils.customDateFormate(list.get(i).getEndDate())%></td>
                                    <td style="text-align: right"><%=new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*list.get(i).getPercentOfCtrVal().doubleValue())/100).setScale(3,0)%>
                                    <%BigDecimal totalAmount =  new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*list.get(i).getPercentOfCtrVal().doubleValue())/100).setScale(3,0);%>
                                    </td>                 
                                    <form id="frmid<%=list.get(i).getSrvPsid()%>" name="frmname<%=list.get(i).getSrvPsid()%>" method="post" action="<%=request.getContextPath()%>/CMSSerCaseServlet?action=GenerateInvoiceTenSide"> 
                                        <td class="t-align-left">
                                            <%
                                                List<Object[]> invData = cmss.getInvoiceData(list.get(i).getSrvPsid(),Integer.parseInt(tenderId));                                                
                                                Object[] InvObj = null;
                                                if(!invData.isEmpty()){
                                                    isAdvInvFlag = cmss.getAcceptedFlaginLumpsum(list.get(i).getSrvPsid(),Integer.parseInt(tenderId));
                                                    if(isAdvInvFlag)
                                                    {                                                        
                                                        out.print("-");
                                                    }else{
                                                        List<Object[]> listlumpsum = cmss.getInvoiceDatainlumpsum(list.get(i).getSrvPsid(),Integer.parseInt(tenderId));
                                                        if(!listlumpsum.isEmpty() && listlumpsum!=null)
                                                        {
                                                            Object[] InvlumpObj = listlumpsum.get(0);
                                                            if(InvlumpObj[6]!=null){
                                                                out.print(InvlumpObj[6].toString());
                                                            }
                                                        }
                                                    }                                                    
                                            %>
                                                
                                                <%}else{%>-<%}%>  
                                        </td>                                    
                                    <td class="t-align-left">
                                    <%if("Completed".equalsIgnoreCase(list.get(i).getStatus())){%>                                    
                                    <% 
                                        
                                        if(!invData.isEmpty() && invData!=null){
                                            for(int k=0;k<invData.size(); k++)
                                            {
                                            InvObj = invData.get(k);
                                            if(InvObj!=null)
                                            {
                                    %>
                                                <table class="tableList_1 t_space">
                                                <tr>
                                                    <th width="3%" class="t-align-left">Invoices</th>
                                                    <th width="3%" class="t-align-left">Status</th>
                                                </tr>
                                                <tr>                                                                
                                                    <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&invoiceId=<%=InvObj[0]%>&wpId=<%=wpId%>&srvPsId=<%=list.get(i).getSrvPsid()%>"><%=InvObj[4].toString()%></a><%if("sendtope".equalsIgnoreCase(InvObj[2].toString())) {%><span class="reqF_1">*</span><%}%></td>
                                                    <td class="t-align-left">
                                                        <%
                                                            String status = "";
                                                            if (InvObj[2] == null) {
                                                                status = "-";
                                                            } else if ("createdbyten".equalsIgnoreCase(InvObj[2].toString())) {
                                                                status = "Invoice Generated by Consultant";
                                                            } else if ("acceptedbype".equalsIgnoreCase(InvObj[2].toString())) {
                                                                status = "Invoice Accepted by PE";
                                                            } else if ("sendtope".equalsIgnoreCase(InvObj[2].toString())) {
                                                                status = "In Process";
                                                            } else if ("sendtotenderer".equalsIgnoreCase(InvObj[2].toString())) {
                                                                status = "Processed by Accounts Officer";
                                                            } else if ("remarksbype".equalsIgnoreCase(InvObj[2].toString())) {
                                                                status = "Processed by PE";
                                                            } else if ("rejected".equalsIgnoreCase(InvObj[2].toString())) {
                                                                status = "Invoice Rejected by PE";
                                                            }
                                                            out.print(status);
                                                        %>
                                                    </td>
                                                </tr>
                                                </table>
                                    <%            
                                            }    
                                        }}else{
                                    %>
                                    Invoice has Not been generated yet.
                                    <%}}else{%>
                                    Milestone has Not been Completed Yet.
                                    <%}%>
                                    </td>
                                    <input type="hidden" value="<%=list.get(i).getSrvPsid()%>" name="SrvPsid"/>
                                    <input type="hidden" value="<%=tenderId%>" name="tenderId"/>
                                    <input type="hidden" value="<%=wpId%>" name="wpId"/>
                                    <input type="hidden" value="<%=lotId%>" name="lotId"/>
                                    <input type="hidden" value="<%=totalAmount%>" name="totalamount"/>
                                    </form>
                                </tr>                                
                                <%}}%>                                 
                            </table>
                            <%}else{%>
                            <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                <th width="1%" class="t-align-left">Sr.No.</th>
                                <th width="15%" class="t-align-left">Attendance Sheet</th>
                                <th width="10%" class="t-align-left">Month-Year</th>
                                <th width="20%" class="t-align-left">Created Date</th>
                                <th width="45%" class="t-align-left">Action</th>
                                <%
                                    List<TblCmsSrvAttSheetMaster> os = cmss.getAllAttSheetDtls(Integer.parseInt(tenderId));
                                    MonthName monthName = new MonthName();                                    
                                    if(os!=null && !os.isEmpty()){
                                        int cc=1;
                                        int ccc=os.size();
                                        for(int i=ccc-1;i>=0;i--){
                                %>
                                        <tr>
                                        <td width="12%" class="t-align-left"><%=cc %></td>
                                        <td width="15%" class="t-align-left">Attendance Sheet - <%=i+1%></td>
                                        <td class="t-align-left"><%=monthName.getMonth(os.get(i).getMonthId())+" - "+os.get(i).getYear() %></td>
                                        <td class="t-align-left"><%=DateUtils.gridDateToStrWithoutSec(os.get(i).getCreatedDt()) %></td>
                                        <td class="t-align-left">
                                        <%
                                            List<Object[]> invDataforAttaSheet = cmss.getInvoiceData(os.get(i).getAttSheetId(),Integer.parseInt(tenderId));
                                            if(invDataforAttaSheet.isEmpty()){
                                        %>
                                        Invoice has not been generated yet
                                        <%}else{
                                            Object[] InvObjforAttSheets = null;
                                            InvObjforAttSheets = invDataforAttaSheet.get(0);
                                            if(InvObjforAttSheets!=null)
                                            {    
                                        %>
                                        <table class="tableList_1 t_space">
                                        <tr>
                                            <th width="1%" class="t-align-left">Invoices</th>
                                            <th width="1%" class="t-align-left">Status</th>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&lotId=<%=lotId%>&invoiceId=<%=InvObjforAttSheets[0]%>&wpId=<%=wpId%>&Sheetflag=true"><%=InvObjforAttSheets[4].toString()%></a><%if("sendtope".equalsIgnoreCase(InvObjforAttSheets[2].toString())) {%><span class="reqF_1">*</span><%}%></td>
                                            <td class="t-align-left">
                                                <%
                                                    String status = "";
                                                    if (InvObjforAttSheets[2] == null) {
                                                        status = "-";
                                                    } else if ("createdbyten".equalsIgnoreCase(InvObjforAttSheets[2].toString())) {
                                                        status = "Invoice Generated by Consultant";
                                                    } else if ("acceptedbype".equalsIgnoreCase(InvObjforAttSheets[2].toString())) {
                                                        status = "Invoice Accepted by PE";
                                                    } else if ("sendtope".equalsIgnoreCase(InvObjforAttSheets[2].toString())) {
                                                        status = "In Process";
                                                    } else if ("sendtotenderer".equalsIgnoreCase(InvObjforAttSheets[2].toString())) {
                                                        status = "Processed by Accounts Officer";
                                                    } else if ("remarksbype".equalsIgnoreCase(InvObjforAttSheets[2].toString())) {
                                                        status = "Processed by PE";
                                                    } else if ("rejected".equalsIgnoreCase(InvObjforAttSheets[2].toString())) {
                                                        status = "Invoice Rejected by PE";
                                                    }
                                                    out.print(status);
                                                %>
                                            </td>
                                        </tr>
                                        </table>
                                        <%}}%>
                                        </td>
                                        </tr>
                                    <%cc++;}}else{%>
                                    <tr>
                                        <td colspan="5">No records found</td>
                                    </tr>
                                    <%}%>

                            </table>

                     <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="2%" class="t-align-left"><%=bdl.getString("CMS.Srno")%></th>
                                                <th width="26%" class="t-align-left">
                                                    Forms
                                                </th>
                                                <th width="60%" class="t-align-left"><%=bdl.getString("CMS.PR.listofpr")%></th>
                                                <th width="20%" class="t-align-left"><%=bdl.getString("CMS.status")%></th>
                                            </tr>
                                          <%
                                          //List<Object> invoicId = service.getInvoiceId(obj.toString());
                                          String wpIdd = service.getWpIdForService(Integer.parseInt(lotId));
                                          List<Object[]> invoicId = service.getInvoiceIdList(wpIdd);
                                          List<TblCmsPrMaster> Prlist = service.getPRHistory(Integer.parseInt(wpIdd));

                                            %>
                                            <tr>
                                                <td style="text-align: center;">1</td>
                                                <td>
                                                    Reimbursable Expenses
                                                </td>
                                                <td>
                                                    <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                                    <tr>
                                                        <th width="20%" class="t-align-left"><%=bdl.getString("CMS.PR.prno")%></th>
                                                        <th width="50%" class="t-align-left"><%=bdl.getString("CMS.PR.listofpr")%></th>
                                                        <th width="10%" class="t-align-left"><%=bdl.getString("CMS.action")%>
                                                         </th>
                                                     </tr>
                                                    <%
                                             int prId=0;
                                             if(!Prlist.isEmpty() && Prlist!=null){
                                             int k=1;
                                             for(int ii=0;ii<Prlist.size();ii++){
                                                    String temp[] = Prlist.get(ii).getProgressRepNo().split(" ");
                                                    String temp1[] = temp[4].split("-");
                                                    String finalNo = temp[0]+" "+temp[1]+" "+temp[2]+" "+temp[3]+" "+DateUtils.customDateFormate(DateUtils.convertStringtoDate(temp[4].toString(),"yyyy-MM-dd"));
    %>
                                             <tr>
                                               <td class="t-align-left"><%=k+ii%></td>
                                             <td class="t-align-left"><%=finalNo%></td>
                                             <td class="t-align-left"><a href="ViewPr.jsp?repId=<%=Prlist.get(ii).getProgressRepId()%>&wpId=<%=wpIdd %>&tenderId=<%=tenderId%>&flag=inv&lotId=<%=lotId %>">View</a></td>
                                             <%
                                                prId=Prlist.get(ii).getProgressRepId();
                                            }}%>
                                                    </table>
                                                </td>
                                                <td class="t-align-left">
                                                     <%if(!invoicId.isEmpty() && invoicId!=null){%>
                                                    <%}else{%>
                                                    Not Generated
                                                    <%}%>
                                                <%if(!invoicId.isEmpty() && invoicId!=null){
                                                %>
                                                <div>
                                                    <table class="tableList_1 t_space">
                                                        <tr>
                                                     <th width="3%" class="t-align-left">Invoices</th>
                                                     <th width="3%" class="t-align-left">Status</th>
                                                        </tr>
                                                <%
                                                int ii=1;
                                                for(int invoicIndex=0; invoicIndex<invoicId.size(); invoicIndex++){
                                                Object[] invobj = invoicId.get(invoicIndex);
                                                %>
                                                <tr>
                                                    <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&invoiceId=<%=invobj[0]%>&lotId=<%=lotId%>&wpId=<%=wpId%>"><%=invobj[2].toString()%></a><%if("sendtope".equalsIgnoreCase(invobj[1].toString())) {%><span class="reqF_1">*</span><%}%></td>
                                                    <td class="t-align-left">
                                                                    <%
                                                        String status = "";
                                                        if (invobj[1] == null) {
                                                            status = "-";
                                                        } else if ("createdbyten".equalsIgnoreCase(invobj[1].toString())) {
                                                            status = "Invoice Generated by Consultant";
                                                        } else if ("acceptedbype".equalsIgnoreCase(invobj[1].toString())) {
                                                            status = "Invoice Accepted by PE";
                                                        } else if ("sendtope".equalsIgnoreCase(invobj[1].toString())) {
                                                            status = "In Process";
                                                        } else if ("sendtotenderer".equalsIgnoreCase(invobj[1].toString())) {
                                                            status = "Processed by Accounts Officer";
                                                        } else if ("remarksbype".equalsIgnoreCase(invobj[1].toString())) {
                                                            status = "Processed by PE";
                                                        } else if ("rejected".equalsIgnoreCase(invobj[1].toString())) {
                                                            status = "Invoice Rejected by PE";
                                                        }
                                                        out.print(status);
    %>
                                                                </td>
                                                </tr>
   <%
   ii++;
                                                } %>

                                                </table>
                                                </div>
                                                </td>
                                            </tr>


                                            <%

                                                }
                                    }
                                                                                                                            %>
                                        </table>
                            <%}else{%>
                            <div class="responseMsg noticeMsg">date Configuration is pending</div>
                            <%
                            
                            }%>
<!--                        </form>-->
                    </div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
<script>
var headSel_Obj = document.getElementById("headTabTender");
if(headSel_Obj != null){
headSel_Obj.setAttribute("class", "selected");
}
</script>
</html>

