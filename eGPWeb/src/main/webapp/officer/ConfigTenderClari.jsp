<%-- 
    Document   : ConfigTenderClari
    Created on : Jun 3, 2011, 5:47:56 PM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.model.table.TblTenderPostQueConfig"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderPostQueConfigService"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="preTendDtBean" class="com.cptu.egp.eps.web.databean.PreTendQueryDtBean" />
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Configure Clarification on Tender</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <!--jalert -->
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>        
        <title>Configure Clarification on Tender</title>
        <script type="text/javascript">
            /*For jquery calander*/


            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
    

            function clarivalidate()
            {
                if(document.getElementById("claricomboid").value=="No")
                {
                    document.getElementById("lastdatetrid").style.display = "none";
                }
                if(document.getElementById("claricomboid").value=="Yes")
                {
                    document.getElementById("lastdatetrid").style.display = "table-row";
                }                
            }
            function validateLastDate()
            {
              var flag = true;
              if(document.getElementById("claricomboid").value=="Yes")
              {
                if(document.getElementById("lastDate").value=="") {
                    document.getElementById("lastdatespan").innerHTML="please select Last Date and Time for posting of query";
                    flag = false;
                } else {
                   try{
                    $.ajax({
                        url: "<%=request.getContextPath()%>/TenderPostQueConfigServlet?param1="+$('#lastDate').val()+"&param2="+$('#hiddentenderid').val()+"&action=validatelastdate",
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            if(j.toString().length>5)
                            {
                                document.getElementById("lastdatespan").innerHTML=j;
                                flag = false;
                            }if(j.toString()=='Eror'){
                                jAlert("Please Configure Dates in Tender Notice page"," Alert ", "Alert");
                                flag = false;
                            }
                        }
                    });
                   }catch(e){
                   }
                }
            } else {
                if(document.getElementById("lastDate").value!=""){
                    document.getElementById("lastdatespan").innerHTML="";
                }
              }
              if(flag == true) {
                document.getElementById("sublastdate").style.display='none';
              }
              return flag;
            }
        </script>
    </head>  
    <body>
        <%                    
                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                String tenderid = request.getParameter("tenderId");
        %>
        <%
                TenderPostQueConfigService tenderPostQueConfigService = (TenderPostQueConfigService) AppContext.getSpringBean("TenderPostQueConfigService");
                if (session.getAttribute("userId") != null) {
                        tenderPostQueConfigService.setLogUserId(session.getAttribute("userId").toString());
                }
                int tenderID =0;
                String queryId = "0";
                if(request.getParameter("tenderId")!=null)
                {
                    tenderID = Integer.parseInt(request.getParameter("tenderId"));
                }
                if(request.getParameter("queryId")!=null)
                {
                    queryId = request.getParameter("queryId");
                }
                List<TblTenderPostQueConfig> listt = tenderPostQueConfigService.getTenderPostQueConfig(tenderID);
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>        
        <div class="contentArea_1">
           
            <div class="pageHead_1">Configure Clarification on Tender
            <span style="float:right;"><a href="Notice.jsp?tenderid=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back To Dashboard</a></span>
            </div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
           
            
     
            <form id="configtenderclari" name="configtenderclarification" method="post" action='<%=request.getContextPath()%>/TenderPostQueConfigServlet?action=saveclarification&tenderId=<%=tenderid%>'>
                &nbsp;                
                <div class="tableHead_1 t_space">Configure Clarification on Tender
                </div>
                
                
                <table  cellspacing="10" cellpadding="0" width="100%" class="tableView_1 infoBarBorder ">
                    <tr><td align="left">
                    <div  align="left" class="" style="padding-left:0px;font-style: italic;">Fields marked with (<span class="mandatory">*</span>) are mandatory.</div>
                </td>
                </tr>
<!--                <tr>
                    <td class="ff" width="25%">Clarification on Tender/Proposal is to be allowed <span class="mandatory">*</span></td>
                    <td class="t-align-left">
                        <select name="claricombo" id="claricomboid" class="formTxtBox_1" onchange="clarivalidate()">
                            <option value="Yes"<%if(!listt.isEmpty()){if("Yes".equalsIgnoreCase(listt.get(0).getIsQueAnsConfig())){%>selected="true"<%}}%>>Yes</option>
                          <option value="No"<%if(!listt.isEmpty()){if("No".equalsIgnoreCase(listt.get(0).getIsQueAnsConfig())){%>selected="true"<%}}%>>No</option>
                        </select>
                        
                    </td>
                </tr>-->
                <input type="hidden" id ="hiddentenderid" value="<%= tenderid %>">
                <input type="hidden" name="queryId" value="<%=queryId%>">
                <input type="hidden" name="claricombo" id="claricomboid" value="Yes">
                <%      String  date = "";
                            boolean b_flag = false;
                            if(!listt.isEmpty()){
                                if(listt.get(0).getPostQueLastDt()!=null){
                                    b_flag = true;
                                listt.get(0).getPostQueLastDt();
                                date = DateUtils.convertStrToDate(listt.get(0).getPostQueLastDt());
                                }
                            }
                    %>
                 <tr id="lastdatetrid" <%if(!b_flag && "edit".equalsIgnoreCase(request.getParameter("msg"))){%>style="display: none;"<%}%>>
                    <td class="ff" width="25%">Last Date and Time for posting of query: <span class="mandatory">*</span></td>
                    <td colspan="1" align="left">
                        <%if(b_flag){%>
                        <input name="lastdate" type="text" class="formTxtBox_1" value="<%=date%>"readonly="true" id="lastDate" style="width:100px;" onblur="return validatelastDate()"onfocus="GetCal('lastDate','lastDate');"/>
                        <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtPqdtsubevarptimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onmouseover="GetCal('lastDate','txtPqdtsubevarptimg');"/></a>
                        <%}else{%>
                        <input name="lastdate" type="text" class="formTxtBox_1" value="<%=date%>"readonly="true" id="lastDate" style="width:100px;" onblur="return validatelastDate()"onfocus="GetCal('lastDate','lastDate');"/>
                        <a href="javascript:void(0);" onclick="" title="Calender"><img id="txtPqdtsubevarptimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onmouseover="GetCal('lastDate','txtPqdtsubevarptimg');"/></a>
                        <%}%>
                    <span id="lastdatespan" class="reqF_1" ></span>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td align="left">
                        <%
                            if(!listt.isEmpty())
                            {
                        %>
                        <label class="formBtn_1">
                            <input type="submit" name="submitlastdate" id="sublastdate" value="Update" onclick="return validateLastDate()"/>
                        </label>
                        <%}else{%>
                        <label class="formBtn_1">
                            <input type="submit" name="submitlastdate" id="sublastdate" value="Submit" onclick="return validateLastDate()"/>
                        </label>
                        <%}%>
                    </td>
                </tr>
                </table></form>
                </div>                     
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

