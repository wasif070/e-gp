<%-- 
    Document   : BidForm
    Created on : Nov 16, 2010, 5:18:59 PM
    Author     : Sanjay,rishita
--%>

<%@page import="java.util.ListIterator"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateCells"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateColumns"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateTables"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Preparation</title>
        <%String contextPath = request.getContextPath();%>
        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="<%=contextPath%>/resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="<%=contextPath%>/resources/js/datepicker/css/border-radius.css" />
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/FormulaCalculation.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/Add.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/deployJava.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/GetHash.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/CommonValidation.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=contextPath%>/resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="<%=contextPath%>/resources/js/datepicker/js/lang/en.js"></script>
        <jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />
        <body>
          <script type="text/javascript">
            $(function() {
                $( "#encrypt-dialog-message" ).dialog({
                    autoOpen: false,
                    resizable: false,
                    modal: true,
                    width: 500,
                    buttons: {
                        Ok: function() {
                            $(this).dialog("close");
                            var btnencrypt = document.getElementById("encrypt");
                            encryptBData(btnencrypt);
                            document.getElementById("action").value = "Encrypt And Save";
                            document.getElementById("frmBidSubmit").submit();
                        },
                        Cancel: function() {
                            $(this).dialog("close");
                        }
                    }
                });
            });
        </script>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="fixDiv">
                <script type="text/javascript">
                    var verified = true;
                    var SignerAPI;
                    $(function() {
                        SignerAPI = document.getElementById('SignerAPI'); // get access to the signer applet.
                    });
                    function GetCal(txtname,controlname,tableId,obj,tableIndex)
                    {
                        new Calendar({
                            inputField: txtname,
                            trigger: controlname,
                            showTime: 24,
                            dateFormat:"%d-%b-%Y",
                            onSelect: function() {
                                var date = Calendar.intToDate(this.selection.get());
                                LEFT_CAL.args.min = date;
                                LEFT_CAL.redraw();
                                this.hide();
                                document.getElementById(txtname).focus();
                                CheckDate(tableId,obj,txtname,txtname,tableIndex);
                            }
                        });

                        var LEFT_CAL = Calendar.setup({
                            weekNumbers: false
                        })

                    }
                    //alert(SignerAPI);
                </script>
                    <!--Middle Content Table Start-->
<%
                int userId = 0;
                if(session.getAttribute("userId") != null) {
                    if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                    }
                }        
                int formId = 0;
                if (request.getParameter("formId") != null) {
                    formId = Integer.parseInt(request.getParameter("formId"));
                }

                int tenderId = 0;
                if (request.getParameter("tenderId") != null) {
                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                }

                int negId = 0;
                if (request.getParameter("negId") != null) {
                    negId = Integer.parseInt(request.getParameter("negId"));
                }
                
                int bidId = 0;
                if (request.getParameter("bidId") != null) {
                    bidId = Integer.parseInt(request.getParameter("bidId"));
                }

                int lotId = 0;
                if (request.getParameter("lotId") != null) {
                    lotId = Integer.parseInt(request.getParameter("lotId"));
                }

                String action = "";
                if(request.getParameter("action") != null && !"".equalsIgnoreCase(request.getParameter("action"))){
                     action = request.getParameter("action");
                }

                boolean isEdit = false;
                if("Edit".equalsIgnoreCase(action)){
                    isEdit = true;
                }

                boolean isView = false;
                if("View".equalsIgnoreCase(action)){
                    isView = true;
                }

                boolean isEncrypt = false;
                if("Encrypt".equalsIgnoreCase(action)){
                    isEncrypt = true;
                }
                
                int tableCount = 0;
                tableCount = tenderBidSrBean.getNoOfTable(formId);

                if("".equalsIgnoreCase(action)){
%>
<script>
        var gblCnt =0;
        var isMultiTable = false;
        var arrCompType = new Array(<%=tableCount%>); //Stores the Array of the ComponentTypes of the Table Fields
        var arrCellData = new Array(<%=tableCount%>);//Stores the Array of the CellData of the Table Fields
        var arrRow = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using normal index
        var arrCol = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using normal index
        var arrTableAdded = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables
        var arrTableFormula = new Array(<%=tableCount%>); //Stores the Array of the table formula
        var arrFormulaFor = new Array(<%=tableCount%>); //Stores the Array of the fields where formula is applicable
        var arrIds = new Array(<%=tableCount%>); //Stores the Array of the ids of the cols of the formula
        var brokenFormulaIds = new Array();
        var arrColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table
        var arrStaticColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table which r only txt
        var arrRowsKey = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using directly ths tableid as a key
        var arrColsKey = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using directly ths tableid as a key
        var arrTableAddedKey = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables using directly ths tableid as a key
        var arrDataTypesforCell = new Array(<%=tableCount%>);
        var arrColTotalIds = new Array(<%=tableCount%>);
        var arrColTotalWordsIds = new Array(<%=tableCount%>);	// added for total in words.
        var arrColOriValIds = new Array(<%=tableCount%>);	// added for original value to keep.
        var isColTotalforTable = new Array(<%=tableCount%>);
        for(var i=0;i<isColTotalforTable.length;i++)
                isColTotalforTable[i]=0;
        var arrForLabelDisp = new Array(<%=tableCount%>); //Stores the Array of the col ids of the which Fillby=3,Datatype=2 table
</script>
<%
                    }
                    if(isEdit || isView || isEncrypt){
                        
                        tableCount = tenderBidSrBean.getNoOfTable(formId);
%>
<script>
		verified = false;
                var gblCnt =0;
                chkEdit = true;
                var isMultiTable = false;
                var arrCompType = new Array(<%=tableCount%>); //Stores the Array of the ComponentTypes of the Table Fields
                var arrCellData = new Array(<%=tableCount%>);//Stores the Array of the CellData of the Table Fields
                var arrRow = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using normal index
                var arrCol = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using normal index
                var arrTableAdded = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables
                var arrTableFormula = new Array(<%=tableCount%>); //Stores the Array of the table formula
                var arrFormulaFor = new Array(<%=tableCount%>); //Stores the Array of the fields where formula is applicable
                var arrIds = new Array(<%=tableCount%>); //Stores the Array of the ids of the cols of the formula
                var brokenFormulaIds = new Array();
                var arrColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table
                var arrStaticColIds =  new Array(<%=tableCount%>); //Stores the Array of the col ids of tha table which r only txt
                var arrRowsKey = new Array(<%=tableCount%>); //Stores the Array of the No of rows of the Table using directly ths tableid as a key
                var arrColsKey = new Array(<%=tableCount%>); //Stores the Array of the No of cols of the Table using directly ths tableid as a key
                var arrTableAddedKey = new Array(<%=tableCount%>); //Stores the Array of the   No of table added for the Tables using directly ths tableid as a key
                var arrDataTypesforCell = new Array(<%=tableCount%>);
                var arrColTotalIds = new Array(<%=tableCount%>);
                var arrColTotalWordsIds = new Array(<%=tableCount%>);	// added for total in words.
                var arrColOriValIds = new Array(<%=tableCount%>);	// added for original value to keep.
                var isColTotalforTable = new Array(<%=tableCount%>);
                var totalBidTable = new Array(<%=tableCount%>);
                for(var i=0;i<isColTotalforTable.length;i++)
                        isColTotalforTable[i]=0;
                var arrForLabelDisp = new Array(<%=tableCount%>); //Stores the Array of the col ids of the which Fillby=3,Datatype=2 table

</script>              
<%
                    }
%>
                <form id="frmBidSubmit" name="frmBidSubmit" method="post" action="NegGetBidData.jsp">
                    <%if(isEdit || isView || isEncrypt){%>
                    <input type="hidden" name="hdnBidId" id="hdnBidId" value="<%=bidId%>">
                    <%}%>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">
                            <td class="contentArea_1">
<%
        String formName = "";
        String formHeader = "";
        String formFooter = "";
        String isMultipleFormFeeling = "";
        String isEncryption = "";
        String isPriceBid = "";
        String isMandatory = "";

        List<CommonFormData> formDetails = tenderBidSrBean.getFormData(formId);
        for(CommonFormData formData : formDetails){
            formName = formData.getFormName();
            formHeader = formData.getFormHeader();
            formFooter = formData.getFormFooter();
            isMultipleFormFeeling = formData.getIsMultipleFormFeeling();
            isEncryption = formData.getIsEncryption();
            isPriceBid = formData.getIsPriceBid();
            isMandatory = formData.getIsMandatory();
        }
%>
                                <div class="t_space">
                                <div class="pageHead_1" id="divFormName">
                                    <%=formName%>
                                    <span style="float: right; text-align: right;">
                                        <a class="action-button-goback" href="NegForms.jsp?tenderId=<%=tenderId%>&uId=2281&negId=221" title="Bid Dashboard">Go Back To Dashboard</a>
                                    </span>
                                </div>
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td>
<%
        if(isEncrypt)
        {
%>
                                            <input type="hidden" name="hdnBuyerPwd" id="hdnBuyerPwd" value="<%=tenderBidSrBean.getBuyerPwdHash(tenderId)%>">
<%
        }
%>
                                            <input type="hidden" name="hdnTenderId" id="hdnTenderId" value="<%=tenderId%>"/>
                                            <input type="hidden" name="hdnFormId" id="hdnFormId" value="<%=formId%>"/>
                                            <input type="hidden" name="hdnLotId" id="hdnLotId" value="<%=lotId%>"/>
                                            <input type="hidden" name="hdnNegId" id="hdnNegId" value="<%=negId%>"/>
<%
        int tableId = 0;
        int tblCnt1 = 0;
        short cols = 0;
        short rows = 0;
        String tableName = "";
        String tableHeader = "";
        String tableFooter = "";
        
        String isMultiTable = "";

        List<CommonFormData> formTables = tenderBidSrBean.getFormTables(formId);
        for(CommonFormData formData : formTables){
            tableId = formData.getTableId();
            tableHeader = formData.getTableHeader();
            tableFooter = formData.getTableFooter();
            List<CommonFormData> tableInfo = tenderBidSrBean.getFormTablesDetails(tableId);
            if (tableInfo != null) {
                if (tableInfo.size() >= 0) {
                    tableName = tableInfo.get(0).getTableName();
                    cols = tableInfo.get(0).getNoOfCols();
                    rows = tableInfo.get(0).getNoOfRows();
                    isMultiTable = tableInfo.get(0).getIsMultipleFilling();
                }
                tableInfo = null;
            }
            
            cols = (tenderBidSrBean.getNoOfColsInTable(tableId)).shortValue();
            rows = (tenderBidSrBean.getNoOfRowsInTable(tableId, (short) 1)).shortValue();
%>
<script>
        chkBidTableId.push(<%=tableId%>);
        arrBidCount.push(1);
        // Setting TableId in Array
        arr[<%=tblCnt1%>]=<%=tableId%>;
        // Setting TableIdwise No of Rows in Array
        arrRow[<%=tblCnt1%>]=<%=rows%>;
        //alert('tablecnt : <%=tblCnt1%>')
        //alert('<%=rows%>');
        arrRowsKey[<%=tableId%>]=<%=rows%>;
        // Setting TableIdwise No of Cols in Array
        arrCol[<%=tblCnt1%>]=<%=cols%>;
        arrColsKey[<%=tableId%>]=<%=cols%>;
        // Setting TableIdwise No of Tables in Added
        arrTableAdded[<%=tblCnt1%>]=<%=1%>;
        arrTableAddedKey[<%=tableId%>]=<%=1%>;
        arrColTotalIds[<%=tblCnt1%>] = new Array(<%=cols%>);
        arrColTotalWordsIds[<%=tblCnt1%>] = new Array(<%=cols%>);
        arrColOriValIds[<%=tblCnt1%>] = new Array(<%=cols%>);
        for(var i=0;i<arrColTotalIds[<%=tblCnt1%>].length;i++){
                arrColTotalIds[<%=tblCnt1%>][i] = 0;
                arrColTotalWordsIds[<%=tblCnt1%>][i] = 0;
                arrColOriValIds[<%=tblCnt1%>][i] = 0;
        }
</script>
                                                <div id="divMsg" class="responseMsg successMsg" style="display:none">&nbsp;</div>
                                                <table width="100%" cellspacing="10" class="tableList_1 t_space">
                                                    <tr>
                                                        <td width="100" class="ff" colspan="2"><%=tableName%></td>
                                                        <td class="t-align-right">
                                            <%
                                                    if("yes".equalsIgnoreCase(isMultiTable)){
                                            %>
                                                            <script>
                                                                isMultiTable = true;
                                                            </script>
                                                            <label <%if(isEdit){%>class="formBtn_disabled"<%}else if(!(isEdit || isView || isEncrypt)){%>class="bidFormBtn_1"<%}%> style="float: right" id="lblDelTable<%=tableId%>"
                                                            <%if(isView){%>style="display:none" <%}%>>
                                                                <input type="button" name="btnDel<%=tableId%>" id="bttnDel<%=tableId%>" value="Delete Record" onClick="DelTable(this.form,<%=tableId%>,this)"
                                                                       <%if(isEdit){%>
                                                                       disabled="true"
                                                                       <%}%>
                                                                       <%if(isView || isEncrypt){%>style="display:none" <%}%>
                                                                       />
                                                            </label>
                                                            <label <%if(isEdit){%>class="formBtn_disabled"<%}else if(!(isEdit || isView || isEncrypt)){%>class="bidFormBtn_1"<%}%> style="float: right" id="lblAddTable<%=tableId%>"
                                                            <%if(isView){%>style="display:none" <%}%>>
                                                                <input type="button" name="btn<%=tableId%>" id="bttn<%=tableId%>" value="Add Record" onClick="AddBidTable(this.form,<%=tableId%>,this)"
                                                                       <%if(isEdit){%>
                                                                       disabled="true"
                                                                       <%}%>
                                                                       <%if(isView || isEncrypt){%>style="display:none" <%}%>
                                                                       />
                                                            </label>
                                                            <input type="hidden" name="delTableIds<%=tableId%>" id="delTableIds<%=tableId%>" value="" />
                                                        <%
                                                                }
                                                        %>
                                                        </td>
                                                    </tr>
                                                    <% if(!"".equals(tableHeader)){ %>
                                                    <tr>
                                                        <td width="100" class="ff" colspan="3"><%=tableHeader%></td>
                                                    </tr>
                                                    <% }  %>
                                                </table>
                                                <jsp:include page="NegBidFormTable.jsp" flush="true">
                                                    <jsp:param name="tableId" value="<%=tableId%>" />
                                                    <jsp:param name="formId" value="<%=formId %>" />
                                                    <jsp:param name="cols" value="<%=cols%>" />
                                                    <jsp:param name="rows" value="<%=rows%>" />
                                                    <jsp:param name="TableIndex" value="<%=tblCnt1%>" />
                                                    <jsp:param name="isMultiTable" value="<%=isMultiTable%>" />
                                                </jsp:include>
                                                <% if(!"".equals(tableFooter)){ %>
                                                 <table width="100%" cellspacing="10" class="tableList_1">
                                                         <tr>
                                                             <td width="100%" colspan="3" class="ff"><%=tableFooter%></td>
                                                        </tr>
                                                </table>
                                                   <% }  %>
                                                   <% if(!"".equals(formFooter)){ %>
                                                 <table width="100%" cellspacing="10" class="tableList_1">        
                                                         <tr>
                                                             <td width="100%" class="ff" colspan="3"><%=formFooter%></td>
                                                        </tr>
                                                </table>
                                                   <% }  %>
                                                 <table width="100%" cellspacing="0" cellpadding="0" class="formStyle_1 t_space">
                                                    <tr>
                                                        <td align="center">
                                                            <div  class="btnContainer t_space">
                                                                <label class="formBtn_1" id="lblSave">
                                                                    <input type="submit" name="save" id="save" value="Update" onclick="return chkValidate(this);">
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                 </table>
<script>
        var totalInWordAt = -1;
        try{
            totalInWordAt = TotalWordColId
            if(totalInWordAt < 0){
                totalInWordAt = -1;
            }
        }catch(err){
            totalInWordAt = -1;
        }
</script>
<%if(tblCnt1 >= 0){%>
<script>
        if(arrStaticColIds[<%=tblCnt1%>].length > 0)
	{
            for(var j=0;j<arr.length;j++)
            {
                LoopCounter++;
            }
	}
        breakFormulas(arrDataTypesforCell);
        checkForFunctions();
</script>
<%}%>
<%
            tblCnt1++;
            formData = null;
        }
%>
<script>
    breakFormulas(arrDataTypesforCell);
    checkForFunctions();
</script>
                                        </td>
                                    </tr>
                                </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
                </div>
                <div>
                    <input type="hidden" name="hdnPwd" id="hdnPwd"/>
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
