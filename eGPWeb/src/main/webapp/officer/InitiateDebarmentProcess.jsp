<%--
    Document   : InitiateDebarment
    Created on : Jan 09, 2011, 3:33:25 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentTypes"%>
<%@page import="com.cptu.egp.eps.web.servicebean.InitDebarmentSrBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.dao.storedprocedure.UserApprovalBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.Calendar,com.cptu.egp.eps.service.serviceimpl.ContentAdminService" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Initiate Debarment Process</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript">
            $(document).ready(function()
            {
                $('#search1').keyup(function()
                {
                    searchTable($(this).val(),1);
                });
                $('#search2').keyup(function()
                {
                    searchTable($(this).val(),2);
                });
            });
            function searchTable(inputVal,onsearch)
            {
                var table = $('#tblData');
                table.find('tr').each(function(index, row)
                {
                    var allCells = $(row).find('td[mysearch="'+onsearch+'"]');
                    if(allCells.length > 0)
                    {
                        var found = false;
                        allCells.each(function(index, td)
                        {
                            var regExp = new RegExp(inputVal, 'i');
                            if(regExp.test($(td).text()))
                            {
                                found = true;
                                return false;
                            }
                        });
                        if(found == true)$(row).show();else $(row).hide();
                    }
                });
            }
            function getDisplayData(rdBtn){
                var rdValue=rdBtn.value;
                $('#debarType').val(rdValue);
                $('#debarIds').val(null);
                $("#tbodyVal").html(null);
                $("#trVal").hide();
                if(rdValue=="6"){
                    $('#addDet').hide();
                }else{
                    $('#addDet').show();
                    if(rdValue=="1"){
                        $('#thTender1').show();
                        $('#thTender2').show();
                    }else{
                        $('#thTender1').hide();
                        $('#thTender2').hide();
                    }
                    if(rdValue=="1"){                        
                        $('#lbHead1').html("Ref No.");
                        $('#lbHead2').html("Tender/Proposal Brief");
                        $('#dlbHead1').html("Ref No.");
                        $('#dlbHead2').html("Tender/Proposal Brief");
                    }if(rdValue=="2"){
                        $('#lbHead1').html("Letter Ref. No.");
                        $('#lbHead2').html("Package No.");
                        $('#dlbHead1').html("Letter Ref. No.");
                        $('#dlbHead2').html("Package No.");
                    }if(rdValue=="3"){
                        $('#lbHead1').html("Project Name");
                        $('#lbHead2').html("Project Code");
                        $('#dlbHead1').html("Project Name");
                        $('#dlbHead2').html("Project Code");
                    }if(rdValue=="4"){
                        $('#lbHead1').html("PA Office Name");
                        $('#lbHead2').html("PA Code");
                        $('#dlbHead1').html("PA Office Name");
                        $('#dlbHead2').html("PA Code");
                    }if(rdValue=="5"){
                        $('#lbHead1').html("Department Name");
                        $('#lbHead2').html("Department Type");
                        $('#dlbHead1').html("Department Name");
                        $('#dlbHead2').html("Department Type");
                    }
                }                
            }
            $(function() {
                $('#btnSearch').click(function() {

                    $('#dis').css("display", "none");
                    if($('#cmbSearchBy').val()=='emailId'){

                        txt = $('#txtSearchVal').val();
                        if($('#txtSearchVal').val()==""){
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('Please enter e-mail ID.')
                            return false;
                        }

                        var SearchByval=$('#cmbSearchBy').val();
                        var val = EmailCheck(txt)

                        if (val == true) {
                            $('#SearchValError').html('')
                            $('#dis').css("display", "none");
                            //return true;
                        }
                        else {
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('<div class="reqF_1"> Please enter valid e-mail ID.</div>')
                            return false;
                        }
                    }

                    else if($('#cmbSearchBy').val()=='companyName'){

                        if($('#txtSearchVal').val()==""){
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('Please enter Company Name.')
                            return false;
                        }
                    }

                    /*else if($('#cmbSearchBy').val()=='companyRegNumber'){
                        if($('#txtSearchVal').val()==""){
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('Please enter Registration No.')
                            return false;
                        }
                    }*/
                });
            });

            function EmailCheck(value) {
                //alert('in');
                //alert(value);
                var c = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);
                //alert(c);
                return c;

            }
            function GetCal(txtname,controlname){
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            function validate(){

                $(".err").remove();

                var valid = true;
                if($("#companyName").val() == "") {
                    $("#companyName").parent().append("<div class='err' style='color:red;'>Please select Company Name</div>");
                    valid = false;
                }

                if($("#debarmentType").val() == "") {
                    $("#debarmentType").parent().append("<div class='err' style='color:red;'>Please select Debarment Reason</div>");
                    valid = false;
                }

                if($("#clarification").val() == "") {
                    $("#clarification").parent().append("<div class='err' style='color:red;'>Please enter Clarification details</div>");
                    valid = false;
                }else{
                    if($("#clarification").val().length>2000){
                        $("#clarification").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters allowed</div>");
                        valid = false;
                    }
                }

                if($("#lastResponseDate").val() == "") {
                    $("#lastResponseDate").parent().append("<div class='err' style='color:red;'>Please select Last Date for Response</div>");
                    valid = false;
                }else{
                    if(!CompareToForGreater($('#lastResponseDate').val(),$('#hdnCurrDate').val())){
                        $("#lastResponseDate").parent().append("<div class='err' style='color:red;'>Response date must be greater than current date.</div>");
                        return false;
                    }
                }
                if($("#debarType").val() == "") {
                    $("#debarType").parent().append("<div class='err' style='color:red;'>Please select Debarment Type</div>");
                    valid = false;
                }else{
                    if($("#debarType").val() != "6") {
                        if($("#debarIds").val() == "") {
                            $("#debarIds").parent().append("<div class='err' style='color:red;'>Please Add Details of "+$("#debarType"+eval($("#debarType").val()-1)).attr('title')+".</div>");
                            valid = false;
                        }
                    }
                }

                if(!valid){
                    return false;
                }
            }
       
            $(function() {                
                $( "#dialog:ui-dialog" ).dialog( "destroy" );
                $( "#dialog-form" ).dialog({
                    autoOpen: false,
                    resizable:false,
                    draggable:false,
                    height: 500,
                    width: 600,
                    modal: true,
                    buttons: {
                        "Add": function() {
                            var cnt=0;
                            $(function() {                                
                                var data="";                                
                                $("#tbodyVal").html(null);
                                $(":checkbox[checked='true']").each(function(){                                    
                                    if($("#debarType").val()=="1"){
                                        $("#tbodyVal" ).append("<tr><td>"+$(this).parent('td').parent('tr').children()[1].innerHTML+"</td><td>"+$(this).parent('td').parent('tr').children()[2].innerHTML+"</td><td>"+$(this).parent('td').parent('tr').children()[3].innerHTML+"</td><td><a href='javascript:void(0)' class='action-button-delete t-align-center' onclick='delRow(this,"+$(this).val()+")'>Remove</a></td></tr>");
                                    }else{
                                        $("#tbodyVal" ).append("<tr><td>"+$(this).parent('td').parent('tr').children()[1].innerHTML+"</td><td>"+$(this).parent('td').parent('tr').children()[2].innerHTML+"</td><td><a href='javascript:void(0)' class='action-button-delete t-align-center' onclick='delRow(this,"+$(this).val()+")'>Remove</a></td></tr>");
                                    }                                    
                                    data+=$(this).val()+",";
                                    cnt++;
                                });
                                $('#debarIds').val(data);
                                $('#trVal').show();                                
                            });
                            if(cnt!=0){
                                $( this ).dialog( "close" );
                            }else{
                                jAlert("Please select one to add","Debarment alert", function(RetVal) {
                                    
                                });
                            }
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    },
                    close: function() {
                    }
                });

                $("#addDet" ).click(function(){
                    if($('#lbHead1').html()!=""){
                        $('#debarIds').val(null);                                            
                        $.post("<%=request.getContextPath()%>/InitDebarment", {debarType:$('#debarType').val(),action:'getDebarData'}, function(j){
                            $("#tbodyData").html(j.toString());
                        });
                        if($('#debarType').val()=='1'){
                            $('#search1').val('');
                            $('#search2').val('');
                        }else{
                           $('#search2').val('');
                        }
                        $("#dialog-form").dialog("open");                        
                    }else{
                        jAlert("Please Select Debarment Type.","Debarment alert", function(RetVal) {
                        });
                    }
                });
            });
            function CompareToForGreater(value,params)
            {

                var mdy = value.split('/')  //Date and month split
                var mdyhr=mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr=mdyp[2].split(' ');


                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                {
                    //alert('Both Date');
                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                }
                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                {
                    //alert('Both DateTime');
                    var mdyhrsec=mdyhr[1].split(':');
                    var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                    var mdyphrsec=mdyphr[1].split(':');
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                }
                else
                {
                    //alert('one Date and One DateTime');
                    var a = mdyhr[1];  //time
                    var b = mdyphr[1]; // time

                    if(a == undefined && b != undefined)
                    {
                        //alert('First Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('Second Date');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                    }
                }

                return Date.parse(date) >= Date.parse(datep);
            }

            function delRow(row,val){
                var curRow = $(row).closest('tr');
                var ids = $('#debarIds').val();
                $('#debarIds').val(ids.replace(val+",",''));
                curRow.remove();
            }
        </script>
    </head>
    <body>
        <input type="hidden" value="<%=DateUtils.formatStdDate(new java.util.Date())%>" name="currDate" id="hdnCurrDate"/>
        <div class="mainDiv">
                <div class="fixDiv">
        <div class="contentArea_1">

            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->

            <%

                        InitDebarmentSrBean debarmentSrBean = new InitDebarmentSrBean();
                        java.util.List<SPTenderCommonData> list1 = new java.util.ArrayList<SPTenderCommonData>();
                        if ("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {
                            list1 = debarmentSrBean.searchCompanyForDebar(request.getParameter("searchBy"), request.getParameter("searchVal"));                            
                        }                       
                        List<TblDebarmentTypes> debarTypes = debarmentSrBean.getAllDebars();
            %>
            <div class="pageHead_1">Initiate Debarment Process<span style="float:right;"> <a class="action-button-goback" href="DebarmentListing.jsp">Go back</a> </span></div><br/>
            <% if (request.getParameter("flag") != null) {
                            String flag = request.getParameter("flag");
                            if (flag.equalsIgnoreCase("fail")) {
                                out.println("<div class='responseMsg errorMsg'>Error Initiating Debarment Process</div><br/>");
                            }
            }%>
            <div class="formBg_1">
                <form  method="post" id="search" name="search">
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                        <tr>
                            <td class="ff" width="10%">Search by   : <span>*</span></td>
                            <td width="90%"><select name="searchBy" class="formTxtBox_1" id="cmbSearchBy" style="width: 200px;">
                                    <!--<option value="companyRegNumber">Registration No</option>-->
                                    <option value="companyName">Company Name</option>
                                    <option value="emailId" selected>e-mail ID</option>
                                </select>&nbsp;
                                <input name="searchVal" type="text" class="formTxtBox_1" id="txtSearchVal" style="width:200px;"/>&nbsp;
                                <label class="formBtn_1">
                                    <input type="submit" name="btnSearch" id="btnSearch" value="Search" />
                                </label>
                                <%if("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {if(list1.isEmpty()){out.print("<span style='color : red;font-weight:bold;'>&nbsp;No matching records found.</span>");}}%>
                            </td>
                        </tr>
                        <tr id="dis" style="display: none;"><td width="10%">&nbsp;</td><td class="t-align-left"><span id="SearchValError" class="reqF_1"></span></td></tr>
                    </table>
                </form>
            </div>
            <form action="<%=request.getContextPath()%>/InitDebarment" method="post">
                <input type="hidden" value="initiateDebarment" name="action" id="action" />
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <%if("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {if(!list1.isEmpty()){%>
                    <tr>
                        <td width="16%" class="t-align-left ff">Company Name :<span class="mandatory">*</span></td>
                        <td width="84%" class="t-align-left">
                            <select name="companyName" class="formTxtBox_1" id="companyName" style="width:200px;">
                                <option selected="selected" value="">- Select Company -</option>
                                <%for (SPTenderCommonData data : list1) {%>
                                <option value="<%=data.getFieldName2()%>"><%=data.getFieldName1()%></option>
                                <%}%>
                            </select>
                            <%if("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {if(list1.isEmpty()){out.print("<span style='color : red;font-weight:bold;'>No matching records found.</span>");}else{out.print("<span style='color : green;font-weight:bold;'>Details found please select the Company Name.</span>");}}%>
                        </td>
                    </tr>                    
                    <tr>
                        <td class="t-align-left ff">Debarment Type :<span class="mandatory">*</span></td>
                        <td class="t-align-left">
                            <%int i = 0;
                                        for (TblDebarmentTypes types : debarTypes) {%>
                            <label><input type="radio" value="<%=types.getDebarTypeId()%>" name="debarType" id="debarType<%=i%>" onclick="getDisplayData(this)" title="<%=types.getDebarType()%>"/>&nbsp;<%=types.getDebarType()%></label>&nbsp;&nbsp;&nbsp;
                            <%i++;
                                        }%>                            
                            <input type="hidden" value="" id="debarType"/>
                            <input type="hidden" value="" name="debarIds" id="debarIds"/>
                            <span class="c-alignment-right"><a id="addDet" class="action-button-add">Add Details</a></span>
                        </td>
                    </tr>
                    <tr id="trVal" style="display: none;">
                        <td class="t-align-left ff">&nbsp;</td>
                        <td class="t-align-left">
                            <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                                <tr>
                                    <th class='t-align-center' id="thTender1">Tender ID</th>
                                    <th class='t-align-center' width='30%'><label id='dlbHead1'></label></th>
                                    <th class='t-align-center' width='50%'><label id='dlbHead2'></label></th>
                                    <th class='t-align-center' width='50%'>Action</th>
                                </tr>
                                <tbody id="tbodyVal"></tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Debarment Reason :<span class="mandatory">*</span></td>
                        <td class="t-align-left">
                            <select name="debarmentType" id="debarmentType" class="formTxtBox_1">
                                <option value="">-Debarment Reason-</option>
                                <option value="Corrupt Practice">Corrupt Practice</option>
                                <option value="Fraudulent Practice">Fraudulent Practice</option>
                                <option value="Collusive practice">Collusive practice</option>
                                <option value="Coercive practice">Coercive practice</option>
                                <option value="Obstructive Practice">Obstructive Practice</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="t-align-left ff">Clarification :<span class="mandatory">*</span></td>
                        <td width="84%" class="t-align-left">
                            <textarea rows="5" id="clarification" name="clarification" class="formTxtBox_1" style="width:99%;"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Last  Date for Response :<span class="mandatory">*</span></td>
                        <td class="t-align-left">
                            <input class="formTxtBox_1" name="lastResponseDate" type="text" class="formTxtBox_1" id="lastResponseDate" style='width:70px;' readonly='readonly' value='' />
                            <a href="javascript:void(0);"  title="Calender" onclick="GetCal('lastResponseDate','lastResponseDate');" >
                                <img src="../resources/images/Dashboard/calendarIcn.png" id="imgfield_1" style="vertical-align:middle;" border="0" onclick="GetCal('lastResponseDate','imgfield_1');" /></a>
                        </td>
                    </tr>
                    <%}}%>
                    <!--tr>
                        <td class="t-align-left ff">Supporting Document :</td>
                        <td class="t-align-left">
                            <input type="file" disabled/>
                        </td>
                    </tr-->
                </table>
                <%if("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {if(!list1.isEmpty()){%>
                <div class="t-align-center t_space">
                    <label class="formBtn_1"><input type="submit" name="button3" id="button3" value="Submit" onclick="return validate();"  /></label>
                </div>
                <%}}%>
            </form>
            </div>
            <%@include file="../resources/common/Bottom.jsp" %>
            </div>
            <!--Dashboard Footer End-->
        </div>
        <div id="dialog-form" title="Debarment">
            <fieldset>
                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tblData">
                    <tr>
                        <th class="t-align-center">Select</th>
                        <th class="t-align-center" id="thTender2">Tender ID<br/><input type="text" value="" id="search1" class="formTxtBox_1" style="width: 100px;"/></th>
                        <th class="t-align-center"><label id="lbHead1"></label><br/><input type="text" value="" id="search2" class="formTxtBox_1" style="width: 100px;"/></th>
                        <th class="t-align-center"><label id="lbHead2"></label></th>
                    </tr>
                    <tbody id="tbodyData">

                    </tbody>
                </table>
            </fieldset>            
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabDebar");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
