<%-- 
    Document   : ReportDisplay
    Created on : Dec 22, 2010, 11:15:06 AM
    Author     : TaherT
--%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblReportLots"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.web.servicebean.ReportCreationSrBean" %>
<%@page import="com.cptu.egp.eps.model.table.TblReportColumnMaster" %>
<html>    
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>       
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>        
    </head>
    <body>
        <%
                    String reportType="TOR/TER";
                    if(request.getParameter("reportType")!=null)
                    {
                        reportType=request.getParameter("reportType");
                    }
                    
                    String tenderid = request.getParameter("tenderid");
                    String repId = request.getParameter("repId");
                    ReportCreationSrBean rcsb = new ReportCreationSrBean();
                    Object data[] = rcsb.getReportTabIdNoOfCols(repId);
                    int reportTableId = Integer.parseInt(data[0].toString());
                    int noOfCols = Integer.parseInt(data[1].toString());
                    List<TblReportColumnMaster> list = rcsb.getReportColumns(reportTableId);
                    List<Object[]> lotList = rcsb.findLotPkgForReport(tenderid);
                    List<Object[]> formList = rcsb.getReportFormula(String.valueOf(reportTableId));
                    if(request.getParameter("isdisp")!= null && request.getParameter("isdisp").equalsIgnoreCase("y"))
                   {
                         // Coad added by Dipal for Audit Trail Log.
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        String idType="tenderId";
                        int auditId=Integer.parseInt(request.getParameter("tenderid"));
                        String auditAction="PCR for "+reportType+" viewed by PE";
                        String moduleName=EgpModule.Tender_Notice.getName();
                        String remarks="";
                         makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                   }
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <%
                    pageContext.setAttribute("tenderId", tenderid);
        %>
        <div class="pageHead_1">View Report<span style="float: right;"><a class="action-button-goback" href="Notice.jsp?tenderid=<%=tenderid%>">Go back to Dashboard</a></span></div>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>        
        <div class="tabPanelArea_1">
            <div class="tableHead_1"><%=data[2]%></div>
            <table class="tableList_1" width="100%" cellspacing="0">
                <tbody><tr>

                        <th colspan="<%=noOfCols + noOfCols%>" class="t-align-left ff"><%=data[3]%></th>
                    </tr>
                    <tr>
                        <%for (TblReportColumnMaster master : list) {%>
                        <th class="t-align-left ff"><div class="break-word"><%=master.getColHeader()%>
                            </div></th>
                        <%}%>
                    </tr>

                    <tr>
                        <%for (TblReportColumnMaster master : list) {%>
                        <td class="t-align-left"><%if (master.getFilledBy() == 1) {
                                    out.print("Company");
                                } /*else if (master.getFilledBy() == 2) {
                                    out.print("Government");
                                } */else if (master.getFilledBy() == 2) {
                                    out.print("Auto");
                                } else if (master.getFilledBy() == 3) {
                                    out.print("Auto Number");
                                }else if (master.getFilledBy() == 4) {
                                    out.print("Official Cost Estimate");
                                }%></td>
                        <%}%>
                    </tr>
                    <tr>
                        <td colspan="<%=noOfCols + noOfCols%>" class="t-align-left formSubHead_1"><%=data[4]%></td>
                    </tr>
                </tbody></table>
                 <%if(formList!=null && !formList.isEmpty()){%>
                 <div class="tableHead_1 t_space">Formula(s)</div>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th style="text-align:left; width:4%;">Sl. No.</th>
                            <th style="text-align:left;">Apply To Column</th>
                            <th style="text-align:left;">Formula</th>
                        </tr>
                        <%
                            int q=1;
                            for(Object[] formulaList : formList){
                         %>
                         <tr class="<%if(q%2==0){out.print("bgColor-Green");}else{out.print("bgColor-white");}%>">
                             <td><%=q%></td>
                             <td><%=formulaList[2]%></td>
                             <td><%=formulaList[1]%></td>                             
                         </tr>
                         <%q++;}%>                         
                    </table>
                    <%}%>
             <%
                if(!"y".equals(request.getParameter("isdisp"))){
             %>
           <div class="t-align-center t_space">
                <a href="ReportColumns.jsp?tenderid=<%=tenderid%>&isedit=y&repId=<%=repId%>&reportType=<%=reportType%>"  class="anchorLink">Go Back and Edit</a>
                &nbsp;
                <%
                    String pageNameT=null;
                    if(data[5].toString().equalsIgnoreCase("item wise")){//evalType from tbl_TenderDetails
                        if(!lotList.isEmpty()){
                           TblReportLots rptlots = new TblReportLots();
                           rptlots.setReportId(Integer.parseInt(repId));
                           rptlots.setPkgLotId(Integer.parseInt(lotList.get(0)[0].toString()));
                           if(!rcsb.insertReportLots(rptlots,false)){
                             out.println("Error in inserting Lots");
                           }
                        }                        
                        pageNameT="ReportFormSelection.jsp?tenderid="+tenderid+"&repId="+repId+"&reportType="+reportType;
                    }else{                        
                        if(lotList.size()!=0){
                            pageNameT="ReportLotPkgSelection.jsp?tenderid="+tenderid+"&repId="+repId+"&reportType="+reportType;
                            //+"eval="+data[5].toString().toLowerCase().charAt(0)
                        }                        
                    }
     
    %>
                
                <a href="<%=pageNameT%>" class="anchorLink">Ok</a>
            </div>
            <%}%>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
