<%--
    Document   : EvalRptApp
    Created on : Dec 7, 2010, 4:16:49 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="java.util.Date" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.SHA1HashEncryption" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
                <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Eval Report Approval</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

       <%-- <script type="text/javascript" language="javascript">
            $(document).ready(function(){
                $('#CommApp').validate({
                    rules:{
                        txtpassword: {required: true},
                        txtcomments: {required: true, maxlength: 500}
                    },
                    messages: {
                        txtpassword: {required:"<div class='reqF_1'>Please enter password.</div>"},
                        txtcomments: {
                            required: "<div class='reqF_1'>Please Enter Comments.</div>",
                            maxlength: "<div class='reqF_1'>Only 500 characters are allowed.</div>"}
                    }
                });
            });
        </script>--%>
       <script type="text/javascript">

            $(document).ready(function() {
                $("#CommApp").validate({
                    rules: {
                        txtpassword: {required: true},
                        txtcomments: {required: true,maxlength:100}
                    },
                    messages: {
                        txtpassword: { required: "<div class='reqF_1'>Please Enter Password.</div>"},
                        txtcomments: { required: "<div class='reqF_1'>Please Enter Comments.</div>",
                                        maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                    }
                });
            });
        </script>

    </head>
    <body>
        <%
                    String tenderId = "51";
                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();

                    //Fetch tenderid from querysting and get corrigendumid
                    if (!request.getParameter("tenderId").equals("")) {
                        tenderId = request.getParameter("tenderId");
                    }
                    
                    String evalRptId="5";
                     if (!request.getParameter("evalRptId").equals("")) {
                        evalRptId = request.getParameter("evalRptId");
                    }



                    //This condition is used to submit data in database, this is button click event.
                if (request.getParameter("btnsubmit") != null) {

                        //Get password and check in database for existence, if found perform insert operation
                        //else show message of invalid password.

                        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        List<SPTenderCommonData> list = tenderCommonService.returndata("checkLogin", request.getParameter("hiddenemail"),  SHA1HashEncryption.encodeStringSHA1(request.getParameter("txtpassword")));

                        if (!list.isEmpty()) {
                             if (!request.getParameter("tenderId").equals("") && !session.getAttribute("userId").equals("") ) {
                            String table="", updateString = "", whereCondition="";

                            table="tbl_Committeemembers";
                            updateString="appStatus='approved', appDate=getdate(), remarks='" + handleSpecialChar.handleSpecialChar(request.getParameter("txtcomments")) + "'";
                            whereCondition="committeeId=" + request.getParameter("tenderId") + " And userID="+ session.getAttribute("userId");

                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_Committeemembers", updateString, whereCondition).get(0);
                            out.println(commonMsgChk.getMsg());
                        }

                        }else
                            {
                            out.println("This is wrong password");
                        }
                    }

        %>


            <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <div class="topHeader">
             <%@include file="../resources/common/AfterLoginTop.jsp" %>
                </div>
                 <div class="contentArea_1">

                    <form id="CommApp" action="<%=request.getContextPath()%>/ClarificationRefDocServlet?funName=approval" method="post">
                        <input type="hidden" name="tenderId" value="<%=tenderId%>"/>
                        <input type="hidden" name="evalRptId" value="<%=evalRptId%>"/>
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <div>&nbsp;</div>
                <div class="pageHead_1">Evaluation Report App<span style="float:right;"><a href="ClarificationRefDoc.jsp?tenderId=<%=tenderId %>&evalRptId=<%=evalRptId%>" class="action-button-goback">Go Back</a></span></div>

                <div style="font-style: italic" class="t-align-left t_space">

                    <span class="t-align-left" style="float:left; font-weight:normal;">
				Fields marked with (<span class="mandatory">*</span>) are mandatory
                    </span>
                </div>
                <div>&nbsp;</div>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <%
                                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    String userId = session.getAttribute("userId").toString();
                                    List<SPTenderCommonData> list = tenderCommonService.returndata("GetEmployeeDetail",userId,null);
                                    if (!list.isEmpty()) {
                        %>
                        <tr>
                        <td width="10%" class="ff">e-mail ID :  </td>

                      <td width="90%"><%=list.get(0).getFieldName1()%><input type="hidden" name="hiddenid" id="hiddenid" value="<%=list.get(0).getFieldName4()%>"/>

                        </td>
                        </tr>
                    <tr>
                        <td width="10%" class="ff">User Name :  </td>

                      <td width="90%"><%=list.get(0).getFieldName3()%><input type="hidden" name="hiddenid" id="hiddenid" value="<%=list.get(0).getFieldName4()%>"/>

                        </td>

                        <%}%>
                    </tr>
                    <tr>
                        <td class="ff" valign="top">Password : <span class="mandatory">*</span> </td>
                        <td>
                            <input name="txtpassword" type="password" class="formTxtBox_1" id="txtpassword" style="width:200px;" autocomplete="off" />
                        </td>
                    </tr>
                    <tr>
                        <td class="ff" valign="top">Comments : <span class="mandatory">*</span></td>
                        <td>
                            <textarea name="txtcomments" id="txtcomments" class="formTxtBox_1" rows="5" style="width: 40%;"></textarea>
                        </td>
                    </tr>
                </table>
                <div class="t-align-center t_space"><label class="formBtn_1">
                        <input name="btnsubmit" type="submit" value="Submit" /></label></div>
                     </form>
                 </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->

        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->


            </div>

    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>

