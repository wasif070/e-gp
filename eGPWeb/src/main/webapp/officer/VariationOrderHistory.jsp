<%-- 
Document   : VariationOrderHistory
Created on : Sep 16, 2011, 11:49:41 AM
Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsTrackVariation"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsVariationOrder"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Variation Order history</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
<script src="../resources/js/common.js" type="text/javascript"></script>


</head>
<div class="dashboard_div">
<!--Dashboard Header Start-->
<%@include  file="../resources/common/AfterLoginTop.jsp"%>
<!--Dashboard Header End-->
<!--Dashboard Content Part Start-->
<div class="contentArea_1">
<div class="pageHead_1">Variation Order history
<span class="c-alignment-right"><a href="DeliverySchedule.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a></span>
</div>
<% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

<%@include file="../resources/common/TenderInfoBar.jsp" %>
<div>&nbsp;</div>
<%
        pageContext.setAttribute("tab", "14");
        ResourceBundle bdl = null;
        bdl = ResourceBundle.getBundle("properties.cmsproperty");

%>
<%@include  file="officerTabPanel.jsp"%>
<div class="tabPanelArea_1">

<%
            pageContext.setAttribute("TSCtab", "1");

%>
<%@include  file="../resources/common/CMSTab.jsp"%>
<div class="tabPanelArea_1">

    <div align="center">

        <%
                    String tenderId = request.getParameter("tenderId");
                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithContractId", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    int i = 0;
                    boolean flags = false;
                    CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                    for (SPCommonSearchDataMore lotList : packageLotList) {

                        flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotList.getFieldName5()));
                        List<Object> wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));

        %>

        <form name="frmcons" method="post">

            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                <tr>
                    <td width="20%"><%=bdl.getString("CMS.lotno")%></td>
                    <td width="80%"><%=lotList.getFieldName3()%></td>
                </tr>
                <tr>
                    <td><%=bdl.getString("CMS.lotdes")%></td>
                    <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                </tr>

                <%
                                                        int count = 1;
                                                        if (!wpid.isEmpty() && wpid != null) {%>
                <tr><td colspan="2">
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                <th width="17%" class="t-align-center">Variation Orders
                                <th width="17%" class="t-align-center">Creation Date and time
                                <th width="17%" class="t-align-center">Created By
                                <th width="6%" class="t-align-center"><%=bdl.getString("CMS.action")%></th>
                            </tr>
                            <%
                                CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                                String pename = "";
                                List<TblCmsVariationOrder> orders = service.getListOfVariationOrder(Integer.parseInt(request.getParameter("wpId")));
                                if(!orders.isEmpty()){
                                    for(int s=0;s<orders.size();s++){
                                        pename = cs.getUserName(orders.get(s).getCreatedBy(), 3);
                            %>

                            <tr>
                                <td style="text-align: center;"><%=s+1%></td>
                                <td><% out.print((orders.get(s).getVariationNo()==null) ? "Variation Order-"+s+1 : orders.get(s).getVariationNo()); %>
                                </td>
                                <td><%=DateUtils.gridDateToStrWithoutSec(orders.get(s).getVariOrdCreationDt())%>
                                </td>
                                <td><%=pename.substring(pename.indexOf("%$")+2,pename.length()) %>
                                </td>
                                <td>
                                    <a href="ViewVariationOrderHistory.jsp?lotID=<%=lotList.getFieldName5()%>&varId=<%=orders.get(s).getVariOrdId()%>&tenderId=<%=tenderId%>">View</a>
                                </td>



                            </tr>


                            <% }}else{%>
                            <tr class="t-align-center">
                                <td class="t-align-center" colspan="5">
                                No records found
                                </td>



                            </tr>
                                    <%}
                                count++;
                                
                                    
                                }%>

                        </table>
                    </td></tr>
                    <%
                                                                                                }
                    %>


            </table>
        </form>

    </div>
</div></div></div>

<%@include file="../resources/common/Bottom.jsp" %>
</div>
<script>
var headSel_Obj = document.getElementById("headTabTender");
if(headSel_Obj != null){
    headSel_Obj.setAttribute("class", "selected");
}
</script>

</html>
