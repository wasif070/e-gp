<%--
    Document   : ServiceWorkPlan
    Created on : Dec 5, 2011, 11:32:20 AM
    Author     : shreyansh.shah
--%>
<%@page import="java.io.PrintWriter"%>
<%@page import="com.cptu.egp.eps.web.utility.GenerateChart"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSalaryRe"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvStaffSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTeamComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvCnsltComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <% ResourceBundle srbd = null;
                    srbd = ResourceBundle.getBundle("properties.cmsproperty");
                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Work Schedule</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

    </head>
    <body>
        <%
                    String referpage = request.getHeader("referer");
                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String type = "";
                    int tenderId = 0;
                    int lotId = 0;
                    int formMapId = 0;
                    int srvBoqId = 0;
                    String styleClass = "";

                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }
                    if (request.getParameter("lotId") != null) {
                        pageContext.setAttribute("lotId", request.getParameter("lotId"));
                        lotId = Integer.parseInt(request.getParameter("lotId"));
                    }
                    if (request.getParameter("formMapId") != null) {
                        formMapId = Integer.parseInt(request.getParameter("formMapId"));
                    }
                    if (request.getParameter("srvBoqId") != null) {
                        srvBoqId = Integer.parseInt(request.getParameter("srvBoqId"));
                    }
                    int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
                    boolean flag = false;
                    CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                    String serviceType = commService.getServiceTypeForTender(tenderId);
                    if ("Time based".equalsIgnoreCase(serviceType.toString())) {
                        flag = true;
                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->


            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">
                        <% if (srvBoqId == 19) {
                                        out.print(srbd.getString("CMS.service.workPlan.title"));
                                    } else if (srvBoqId == 16) {
                                        out.print("Reimbursable Expensses");
                                    } else if (srvBoqId == 18) {
                                        out.print("Consultant Composition");
                                    } else if (srvBoqId == 12) {
                                        out.print("Payment Schedule");
                                    } else if (srvBoqId == 13) {
                                        out.print("Team Composition and Task Assign");
                                    } else if (srvBoqId == 14) {
                                        out.print("Staffing Schedule");
                                    } else if (srvBoqId == 15) {
                                        out.print("Salary Reimbursable");
                                    }
                                    if (request.getParameter("tenderId") != null) {
                                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                                    }
                    GenerateChart gc = new GenerateChart();


                        %>
                        <span style="float: right; text-align: right;" class="noprint">
                            <a class="action-button-goback" href="<%=referpage%>" title="Go Back">Go Back</a>
                        </span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <%if (request.getParameter("lotId") != null) {%>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <%}%>
                    <form name="frm" action="<%=request.getContextPath()%>/CMSSerCaseServlet" method="post" >
                        <input type="hidden" name="addcount" id="addcount" value="" />
                        <div id="resultDiv" style="display: block;">
                            <div  id="print_area">

                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                    <% if (srvBoqId == 19) {%>
                                    <tr>
                                        <th width="3%" class="t-align-center"><%=srbd.getString("CMS.service.workSchedule.srNo")%></th>
                                        <th width="40%" class="t-align-center"><%=srbd.getString("CMS.service.workSchedule.activity")%></th>
                                        <th width="10%" class="t-align-center"><%=srbd.getString("CMS.sdate")%></th>
                                        <th width="5%" class="t-align-center"><%=srbd.getString("CMS.service.workSchedule.noOfDays")%>
                                        </th>
                                        <th width="10%" class="t-align-center"><%=srbd.getString("CMS.edate")%>
                                        </th>
                                        <th width="15%" class="t-align-center"><%=srbd.getString("CMS.PR.remarks")%>
                                        </th>
                                    </tr>
                                    <%

                                         List<Object[]> listt = cmss.getAllServiceWorkPlan(formMapId);

                                         try {

                                             if (listt != null && !listt.isEmpty()) {
                                                 int i = 0;
                                                 for (i = 0; i < listt.size(); i++) {
                                                     if (i % 2 == 0) {
                                                         styleClass = "bgColor-white";
                                                     } else {
                                                         styleClass = "bgColor-Green";
                                                     }

                                                     out.print("<tr class='" + styleClass + "'>");
                                                     out.print("<td  class=\"t-align-left\">" + listt.get(i)[0] + "</td>");
                                                     out.print("<td  class=\"t-align-left\">" + listt.get(i)[1] + "</td>");
                                                     out.print("<td  class=\"t-align-left\">" + DateUtils.gridDateToStrWithoutSec((Date) listt.get(i)[2]).split(" ")[0] + "</td>");
                                                     out.print("<td style=\"text-align :right;\">" + listt.get(i)[3] + "</td>");
                                                     out.print("<td  class=\"t-align-left\">" + DateUtils.gridDateToStrWithoutSec((Date) listt.get(i)[4]).split(" ")[0] + "</td>");
                                                     out.print("<td  class=\"t-align-left\">" + listt.get(i)[5] + "</td>");
                                                     out.print("<input type=hidden name=srvwplanid_" + i + " id=srvwplanid_" + i + " value=" + listt.get(i)[7] + " />");
                                                     out.print("</tr>");
                                                 }
                                                 out.print("<input type=hidden name=listcount id=listcount value=" + i + " />");

                                             } else {
                                                 out.print("<tr>");
                                                 out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                                                 out.print("</tr>");
                                             }

                                         } catch (Exception e) {
                                             e.printStackTrace();
                                         }

                                         if (request.getParameter("lotId") != null) {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Work_Schedule.getName(), "View Service WorkPlan", "");
                                         } else {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Noa.getName(), "View Service WorkPlan", "");
                                         }
                                          
                                         String ganttfilename = gc.GanttChart(session, new PrintWriter(out),formMapId);
                                         String ganttchart = request.getContextPath()+ "/servlet/DisplayChart?filename=" + ganttfilename;
                                         %>
                                         <tr><td colspan="6">
                                                 <center>
                                                     <img src="<%=ganttchart%>" border="0" usemap="#<%= ganttfilename %>" /></center>
    </td></tr>


                                     <%} else if (srvBoqId == 16) {
                                    %>
                                    <tr>
                                        <th>Sl. No.</th>
                                        <th>Description Category</th>
                                        <th>Description</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                        <th>Unit Cost</th>
                                        <th>Total Amount (In Nu.)</th>
                                    </tr>
                                    <%
                                         List<TblCmsSrvReExpense> list = cmss.getSrvReExpensesData(formMapId);
                                         if (!list.isEmpty()) {
                                             BigDecimal totAmt = new BigDecimal(0);
                                             for (int i = 0; i < list.size(); i++) {
                                                  totAmt = totAmt.add(list.get(i).getTotalAmt());
                                                 if (i % 2 == 0) {
                                                     styleClass = "bgColor-white";
                                                 } else {
                                                     styleClass = "bgColor-Green";
                                                 }
                                    %>

                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"><%=list.get(i).getSrNo()%></td>
                                        <td class="t-align-left"><%=list.get(i).getCatagoryDesc()%></td>
                                        <td class="t-align-left"><%=list.get(i).getDescription()%></td>
                                        <td class="t-align-left"><%=list.get(i).getUnit()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getQty()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getUnitCost()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getTotalAmt()%></td>
                                    <input type="hidden" name="muliplytxt" id="muliplytxt<%=i%>"/>
                                    <input type="hidden" value="<%=list.get(i).getSrvReid()%>" name="srvReId"/>
                                    </tr>
                                    <%}%>
                                     <tr>
                                    <td colspan="5"></td>
                                    <td class="t-align-right ff" style="text-align: right;">Grand Total</td>
                                    <td class="t-align-right" style="text-align: right;"><%=totAmt%></td>
                                </tr>
                                         <%}%>
                                    <%
                                         if (request.getParameter("lotId") != null) {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Work_Schedule.getName(), "View Reimbursable Expense", "");
                                         } else {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Noa.getName(), "View Reimbursable Expense", "");
                                         }
                                     } else if (srvBoqId == 18) {%>
                                    <tr>
                                        <th>Sl. No.</th>
                                        <th>Consultant Category</th>
                                        <%if (flag) {%>
                                        <th>Indicated No Of Key Professional Staff</th>
                                        <th>Input Staff Months</th>
                                        <%}%>
                                        <th>Indicated No. of Professional Staff</th>
                                        <th>Indicated No. of Support Staff</th>
                                        <th>Consultant Name</th>
                                        <th>No. of Key Professional Staff</th>
                                        <th>No. of Support Professional Staff</th>
                                    </tr>
                                    <%
                                         List<TblCmsSrvCnsltComp> list = cmss.getSrvConsltCompoData(formMapId);
                                         if (!list.isEmpty()) {
                                             for (int i = 0; i < list.size(); i++) {
                                                 if (i % 2 == 0) {
                                                     styleClass = "bgColor-white";
                                                 } else {
                                                     styleClass = "bgColor-Green";
                                                 }
                                    %>

                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"><%=list.get(i).getSrNo()%></td>
                                        <td class="t-align-left"><%=list.get(i).getConsultantCtg()%></td>
                                        <%if (flag) {%>
                                        <td style="text-align :right;"><%=list.get(i).getTotalNosPe()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getMonths()%></td>
                                        <% }%>
                                        <td style="text-align :right;"><%=list.get(i).getKeyNosPe()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getSupportNosPe()%></td>
                                        <td class="t-align-left"><%=list.get(i).getConsultantName()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getKeyNos()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getSupportNos()%>
                                            <input type="hidden" name="srvCCId" value="<%=list.get(i).getSrvCcid()%>">
                                        </td>
                                    </tr>
                                    <%}
                                         }%>
                                    <%
                                         if (request.getParameter("lotId") != null) {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Work_Schedule.getName(), "View Consultant Composition", "");
                                         } else {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Noa.getName(), "View Consultant Composition", "");
                                         }
                                     } else if (srvBoqId == 12) {%>
                                    <tr>
                                        <th>Sl. No.</th>
                                        <%if (!flag) {%>
                                        <th>Milestone Name </th>
                                        <%}%>
                                        <th>Description</th>
                                        <%if (!flag) {%>
                                        <th>Payment as % of Contract Value</th>
                                        <%}%>
                                        <th>Mile Stone Date proposed by PE </th>
                                        <th>Mile Stone Date proposed by Consultant</th>
                                    </tr>
                                    <%
                                         List<TblCmsSrvPaymentSch> list = cmss.getPaymentScheduleData(formMapId);
                                         if (!list.isEmpty()) {
                                             for (int i = 0; i < list.size(); i++) {
                                                 if (i % 2 == 0) {
                                                     styleClass = "bgColor-white";
                                                 } else {
                                                     styleClass = "bgColor-Green";
                                                 }
                                    %>

                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"><%=list.get(i).getSrNo()%></td>
                                        <%if (!flag) {%>
                                        <td class="t-align-left"><%=list.get(i).getMilestone()%></td>
                                        <% } %>
                                        <td class="t-align-left"><%=list.get(i).getDescription()%></td>
                                        <%if (!flag) {%>
                                        <td style="text-align :right;"><%=list.get(i).getPercentOfCtrVal()%></td>
                                        <% } %>
                                        <td class="t-align-left"><%=DateUtils.customDateFormate(list.get(i).getPeenddate())%></td>
                                        <td class="t-align-left"><%=DateUtils.customDateFormate(list.get(i).getEndDate())%></td>
                                    </tr>
                                    <%}
                                         }%>
                                    <%
                                         if (request.getParameter("lotId") != null) {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Work_Schedule.getName(), "View Payment Schedule", "");
                                         } else {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Noa.getName(), "View Payment Schedule", "");
                                         }
                                         String linefilename = gc.LineChart(session, new PrintWriter(out),formMapId,serviceType);
                                         String linechart = request.getContextPath()+ "/servlet/DisplayChart?filename=" + linefilename;
                                         int colSpan = 5;
                                         if(flag){
                                             colSpan = 3;
                                         }
                                         %>
                                         <tr><td colspan="<%= colSpan %>">
                                                 <center>
                                                     <img src="<%=linechart%>" border="0" usemap="#<%= linefilename %>" /></center>
                                             </td>
                                                     <td>
                                                     <table width="100%" cellspacing="0" class="tableList_1 t_space" >
                                                     <tr>
                                                         <th>
                                                             MileStone No.
                                                         </th>
                                                         <%if (!flag) {%>
                                                         <th>
                                                             MileStone Name
                                                         </th>
                                                         <%}%>
                                                         <th>
                                                             Description
                                                         </th>
                                                     </tr>
                                                     <%if (!list.isEmpty()) {
                                                         int i = 1;
                                                     for(TblCmsSrvPaymentSch sch : list){
    %>
    <tr><td width="10%">
                                                             <%=i%>
                                                         </td>
                                                         <%if (!flag) {%>
                                                         <td width="45%">
                                                             <%=sch.getMilestone() %>
                                                         </td>
                                                         <%}%>
                                                         <td width="45%">
                                                             <%=sch.getDescription() %>
                                                         </td>
                                                     </tr>
                                                     <%i++;}}%>
                                                 </table>
                                             </td></tr>


                                    <% } else if (srvBoqId == 13) {%>
                                    <tr>
                                        <th>Sl. No.</th>
                                        <th>Name of  Staff </th>
                                        <th>Firm/Organization</th>
                                        <th>Staff Category    (Key / Support)</th>
                                        <th>Position Defined by Consultant </th>
                                        <th>Category of Consultant proposed Consultant Category</th>
                                        <th>Area of Expertise</th>
                                        <th>Task Assigned</th>
                                    </tr>
                                    <%
                                         List<TblCmsSrvTeamComp> list = cmss.getTeamCompositionData(formMapId);
                                         if (!list.isEmpty()) {
                                             for (int i = 0; i < list.size(); i++) {
                                                 if (i % 2 == 0) {
                                                     styleClass = "bgColor-white";
                                                 } else {
                                                     styleClass = "bgColor-Green";
                                                 }
                                    %>

                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"><%=list.get(i).getSrno()%></td>
                                        <td class="t-align-left"><%=list.get(i).getEmpName()%></td>
                                        <td class="t-align-left"><%=list.get(i).getOrganization()%></td>
                                        <td class="t-align-left"><%=list.get(i).getStaffCat()%></td>
                                        <td class="t-align-left"><%=list.get(i).getPositionDefined()%></td>
                                        <td class="t-align-left"><%=list.get(i).getConsultPropCat()%></td>
                                        <td class="t-align-left"><%=list.get(i).getAreaOfExpertise()%></td>
                                        <td class="t-align-left"><%=list.get(i).getTaskAssigned()%></td>
                                    </tr>
                                    <%}
                                         }%>

                                    <%
                                         if (request.getParameter("lotId") != null) {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Work_Schedule.getName(), "View Team Composition and Task Assign", "");
                                         } else {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Noa.getName(), "View Team Composition and Task Assign", "");
                                         }
                                     } else if (srvBoqId == 14) {%>
                                    <tr>
                                        <th>Sl. No.</th>
                                        <th>Name of Employees </th>
                                        <th>Home / Field</th>
                                        <th>Start Date </th>
                                        <th>No. of Days</th>
                                        <th>End Date</th>
                                    </tr>
                                    <%
                                         List<TblCmsSrvStaffSch> list = cmss.getStaffScheduleData(formMapId);
                                         if (!list.isEmpty()) {
                                             for (int i = 0; i < list.size(); i++) {
                                                 if (i % 2 == 0) {
                                                     styleClass = "bgColor-white";
                                                 } else {
                                                     styleClass = "bgColor-Green";
                                                 }
                                    %>

                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"><%=list.get(i).getSrno()%></td>
                                        <td class="t-align-left"><%=list.get(i).getEmpName()%></td>
                                        <td class="t-align-left"><%=list.get(i).getWorkFrom()%></td>
                                        <td class="t-align-left"><%=DateUtils.gridDateToStrWithoutSec((Date) list.get(i).getStartDt()).split(" ")[0]%></td>
                                        <td style="text-align :right;"><%=list.get(i).getNoOfDays()%></td>
                                        <td class="t-align-left"><%=DateUtils.gridDateToStrWithoutSec((Date) list.get(i).getEndDt()).split(" ")[0]%></td>

                                    </tr>
                                    <%}
                                         }%>

                                    <%
                                         if (request.getParameter("lotId") != null) {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Work_Schedule.getName(), "View Staffing Schedule", "");
                                         } else {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Noa.getName(), "View Staffing Schedule", "");
                                         }
                                     } else if (srvBoqId == 15) {%>
                                    <tr>
                                      <th width="4%">Sl. No.</th>
                                        <th width="40%">Name of Employees </th>
                                        <th width="16%">Home / Field</th>
                                        <th width="10%">Staff Months</th>
                                        <th width="15%">Staff Month Rate</th>
                                        <th width="25%">Total Rate</th>
                                    </tr>
                                    <%
                                         List<TblCmsSrvSalaryRe> list = cmss.getSalaryReimbursData(formMapId);
                                         BigDecimal totMonth = new BigDecimal(0);
                                         BigDecimal totSal = new BigDecimal(0);
                                          BigDecimal totSall = new BigDecimal(0);
                                         if (!list.isEmpty()) {
                                             for (int i = 0; i < list.size(); i++) {
                                                  totMonth = totMonth.add(list.get(i).getWorkMonths());
                                                  totSal = totSal.add(list.get(i).getEmpMonthSalary());
                                                  totSall = totSall.add(list.get(i).getEmpMonthSalary().multiply(list.get(i).getWorkMonths()));
                                                 if (i % 2 == 0) {
                                                     styleClass = "bgColor-white";
                                                 } else {
                                                     styleClass = "bgColor-Green";
                                                 }
                                    %>

                                    <tr class=<%=styleClass%> >
                                        <td class="t-align-left"><%=list.get(i).getSrNo()%></td>
                                        <td class="t-align-left"><%=list.get(i).getEmpName()%></td>
                                        <td class="t-align-left"><%=list.get(i).getWorkFrom()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getWorkMonths()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getEmpMonthSalary()%></td>
                                        <td style="text-align :right;"><%=list.get(i).getEmpMonthSalary().multiply(list.get(i).getWorkMonths()).setScale(3, 0) %></td>

                                    </tr>

                                    <%}
                                         }%>
                                          <tr><td style="text-align: right" class="ff" colspan="3">Grand Total</td>
                                         <td class="ff" style="text-align: right"><%=totMonth%></td>
                                         <td class="ff" style="text-align: right"><%=totSal%></td>
                                         <td class="ff" style="text-align: right"><%=totSall.setScale(3, 0) %></td>
                                     </tr>
                                    <%
                                         if (request.getParameter("lotId") != null) {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Work_Schedule.getName(), "View Break-down of Staff Remuneration", "");
                                         } else {
                                             makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Noa.getName(), "View Break-down of Staff Remuneration", "");
                                         }
                                     } else if (srvBoqId == 17) {%>
                                    <%makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Work_Schedule.getName(), "View Cost Component", "");
                                     }%>

                                </table>
                            </div>
                        </div>


                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" id="first" value="0"/>
                        <br />
                        <center>



                        </center>
                    </form>
                </div>

                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
