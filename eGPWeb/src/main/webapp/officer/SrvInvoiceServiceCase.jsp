<%-- 
    Document   : SrvInvoiceServiceCase
    Created on : Dec 14, 2011, 11:35:43 AM
    Author     : shreyansh Jogi
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.NOAServiceImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsNewBankGuarnatee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsNewBankGuarnateeService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsPrMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Invoice</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>


    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Invoice</div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                    dDocSrBean.setLogUserId(userId);
                }

                CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                TenderCommonService wfTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                CmsNewBankGuarnateeService cmsNewBankGuarnateeService = (CmsNewBankGuarnateeService) AppContext.getSpringBean("CmsNewBankGuarnateeService");

                List<SPTenderCommonData> checkuserRights = wfTenderCommonService.returndata("CheckTenderUserRights",userId,request.getParameter("tenderId"));
                boolean allowReleaseForfeit = false;
                String strlotId = "";
            %>
            <%
                        pageContext.setAttribute("tab", "14");
                        ResourceBundle bdl = null;
                    bdl = ResourceBundle.getBundle("properties.cmsproperty");

            %>
            <%@include  file="officerTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "4");

                %>
                <%@include  file="../resources/common/CMSTab.jsp"%>
                <div class="tabPanelArea_1">
                     <% if(request.getParameter("msg") != null){
                     if("accept".equalsIgnoreCase(request.getParameter("msg"))){

    %>
    <div class='responseMsg successMsg'><%=bdl.getString("CMS.Inv.accept")%></div>
                   <%}
                     if("reject".equalsIgnoreCase(request.getParameter("msg"))){

    %>
    <div class='responseMsg successMsg'><%=bdl.getString("CMS.Inv.reject")%></div>
                   <%}

                     }

 %>
                    <div align="center">

                         <%
                                    String tenderId = request.getParameter("tenderId");
                                    boolean flag = true;
                                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                    List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetails", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                    int i = 0;
                                    boolean flags = false;
                                    CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                    for (SPCommonSearchDataMore lotList : packageLotList) {
                                        String wpId = service.getWpIdForService(Integer.parseInt(lotList.getFieldName5()));
                                        flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotList.getFieldName5()));
                                        strlotId = lotList.getFieldName5();
                        %>

                        <form name="frmcons" method="post">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <tr>
                                    <td width="20%"><%=bdl.getString("CMS.lotno")%></td>
                                    <td width="80%"><%=lotList.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td><%=bdl.getString("CMS.lotdes")%></td>
                                    <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                </tr>

                                <%
                                 out.print(" <tr>");
                                 out.print("<td class='ff' width='20%'  class='t-align-left'>Performance Security</td>");
                                 out.print("<td><table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                                 out.print("<tr><th width='7%' class='t-align-center'>Sl. No.</th>");
                                 out.print("<th width='15%' class='t-align-center'>Contract No.</th>");
                                 out.print("<th width='35%' class='t-align-center'>Company Name</th>");
                                 out.print("<th class='t-align-center' width='12%'>Payment Status</th>");
                                 out.print("<th class='t-align-center' width='12%'>Payment Amount</th>");
                                 out.print("<th class='t-align-center' width='12%'>Payment Date</th>");
                                 out.print("<th class='t-align-center' width='30%'>Action</th></tr>");
                                 int j = 0;

                                 String paymentToId = "";
                                 List<SPCommonSearchData> lstPayments= commonSearchService.searchData("getPaidTendererListForPE", tenderId, lotList.getFieldName5(), "Performance Security", userId, null, null, null, null, null);
                                 List<SPCommonSearchData> lstBankGuarantee = commonSearchService.searchData("getPaidTendererListForPE", tenderId, lotList.getFieldName5(), "Bank Guarantee", userId, null, null, null, null, null);
                                 CommonSearchDataMoreService objPayment = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                 int icountcon=0;
                                 for (SPCommonSearchData sptcd : lstPayments)
                                 {
                                     List<Object[]> cntDetails = service.getContractNo(Integer.parseInt(sptcd.getFieldName9()));
                                     j++;
                                     out.print("<tr");
                                     if(Math.IEEEremainder(j,2)==0){
                                     out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                                     out.print("<td class='t-align-center'>"+j+"</td>");
                                     if(cntDetails!=null && !cntDetails.isEmpty() ){
                                     if("yes".equalsIgnoreCase(cntDetails.get(0)[1].toString())){
                                         out.print("<td class='t-align-center'>"+cntDetails.get(0)[0]+" (For Repeat Order)</td>");
                                         }else{
                                         out.print("<td class='t-align-center'>"+cntDetails.get(0)[0]+"</td>");
                                         }

                                     }

                                     out.print("<td class='t-align-left'>"+sptcd.getFieldName2()+"</td>");
                                     if("Forfeited".equalsIgnoreCase(sptcd.getFieldName8())){
                                         out.print("<td class='t-align-center'>Compensated</td>");
                                     }else{
                                         out.print("<td class='t-align-center'>"+sptcd.getFieldName8()+"</td>");
                                     }
                                     List<SPCommonSearchDataMore> lstPaymentDetail = objPayment.geteGPData("getTenderPaymentDetail", sptcd.getFieldName1(), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);


                                      out.print("<td class='t-align-center'>");
                                      if("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){ %>
                                        <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                    <% }else if("USD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                        <label>$</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                    <% }else if("Nu.".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                        <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                    <% }
                                     out.print("</td>");
                                     out.print("<td class='t-align-center'>"+lstPaymentDetail.get(0).getFieldName6()+"</td>");
                                     out.print("<td class='t-align-center'>");
                                     if(!checkuserRights.isEmpty()){
                                         paymentToId = sptcd.getFieldName3();
                                     out.print("<a href='"+request.getContextPath()+"/partner/ViewTenderPaymentDetails.jsp?payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Payment Details</a>");
                                     if("Paid".equalsIgnoreCase(sptcd.getFieldName8()) || "Extended".equalsIgnoreCase(sptcd.getFieldName8())){
                                         if("ps".equalsIgnoreCase(sptcd.getFieldName6())) {
                                            String strActionRequest = "";
                                            List<SPTenderCommonData> lstPaymentActionRequest =
                                                                          tenderCommonService.returndata("getRequestActionFromPaymentId", sptcd.getFieldName1(), null);
                                            if(lstPaymentActionRequest.isEmpty()){
                                                allowReleaseForfeit = true;
                                            } else {
                                                allowReleaseForfeit = false;
                                                strActionRequest = lstPaymentActionRequest.get(0).getFieldName2();
                                            }
                                            if("Release".equalsIgnoreCase(strActionRequest)){
                                                strActionRequest = "Released";
                                            }
                                            if(allowReleaseForfeit){
                                                if(!lstBankGuarantee.isEmpty())
                                                    {
                                                        out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestrelease&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"'>Release Request</a>");
                                                    }
                                                        out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestforfeit&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"'>Compensate Request</a>");

                                            } else {
                                                if("Forfeit".equalsIgnoreCase(strActionRequest))
                                                {
                                                    out.print("&nbsp;&nbsp;(Compensate)");
                                                }else{
                                                out.print("&nbsp;&nbsp;("+strActionRequest+")");
                                                }
                                            }
                                         }
                                      }
                                      }
                                      out.print("</td></tr>");
                                      icountcon++;
                                     }
                                     if(j==0){
                                        out.print("<tr><td colspan='6'>No records available</td></tr>");
                                     }
                                     out.print(" </table></td></tr>");
                                     out.print(" <tr>");
                                 out.print("<td class='ff' width='20%'  class='t-align-left'>New Performance Security</td>");

                                 List<Object[]> listBg = cmsNewBankGuarnateeService.fetchBankGuaranteeList(Integer.parseInt(tenderId),Integer.parseInt(userId));
                                 out.print("<td>");

                                 if(!lstPayments.isEmpty())
                                 {
                                     TblCmsNewBankGuarnatee tblCmsNewBankGuarnatee = cmsNewBankGuarnateeService.fetchBankGuarantee(Integer.parseInt(tenderId));
                                     //if(tblCmsNewBankGuarnatee==null){
                                     out.print("<a href='"+request.getContextPath()+"/officer/RequestBankGuarantee.jsp?uId="+paymentToId+"&tenderId="+tenderId+"&lotId="+strlotId+"'>Request Bank Guarantee for Additional Performance Security</a>");
                                    // }else{
                                     //out.print("Bank Guarantee Requested");
                                    // }

                                     out.print("<table width='100%' cellspacing='0' class='tableList_1 t_space'>");
                                     out.print("<tr><th width='7%' class='t-align-center'>Sl. No.</th>");
                                     out.print("<th width='7%' class='t-align-center'>Contract  No.</th>");
                                     out.print("<th width='35%' class='t-align-center'>Company Name</th>");
                                     out.print("<th class='t-align-center' width='12%'>Payment Status</th>");
                                     out.print("<th class='t-align-center' width='30%'>Action</th></tr>");
                                     j = 0;
                                     int icountforBG=0;
                                     for(Object[] obj1: listBg){
                                         j++;

                                         out.print("<tr");
                                         if(Math.IEEEremainder(j,2)==0){
                                             out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                                             out.print("<td class='t-align-center'>"+j+"</td>");
                                             if("yes".equalsIgnoreCase(obj1[4].toString())){
                                                out.print("<td class='t-align-center'>"+obj1[3]+" (For Repeat Order)</td>");
                                             }else{
                                                out.print("<td class='t-align-center'>"+obj1[3]+"</td>");
                                             }
                                             out.print("<td class='t-align-left'>"+obj1[0]+" "+obj1[1]+"</td>");
                                             out.print("<td class='t-align-center'>Pending</td>");
                                             out.print("<td class='t-align-center'>");
                                             out.print("<a href='"+request.getContextPath()+"/officer/ViewBankGuarantee.jsp?uId="+paymentToId+"&tenderId="+tenderId+"&lotId="+strlotId+"&bgId="+obj1[2]+"'>View</a>");
                                             out.print("</td>");
                                             out.print("</tr>");
                                              icountforBG++;

                                     }
                                     for (SPCommonSearchData sptcd : lstBankGuarantee)
                                     {
                                          List<Object[]> cntDetails = service.getContractNo(Integer.parseInt(sptcd.getFieldName9()));
                                         j++;
                                         out.print("<tr");
                                         if(Math.IEEEremainder(j,2)==0){
                                         out.print("class='bgColor-Green' >");} else {out.print("class='bgColor-white' >");};
                                         out.print("<td class='t-align-center'>"+j+"</td>");
                                          if(cntDetails!=null && !cntDetails.isEmpty() ){
                                         if("yes".equalsIgnoreCase(cntDetails.get(0)[1].toString())){
                                         out.print("<td class='t-align-center'>"+cntDetails.get(0)[0]+" (For Repeat Order)</td>");
                                         }else{
                                         out.print("<td class='t-align-center'>"+cntDetails.get(0)[0]+"</td>");
                                         }
                                         }
                                         out.print("<td class='t-align-left'>"+sptcd.getFieldName2()+"</td>");
                                         if("Forfeited".equalsIgnoreCase(sptcd.getFieldName8())){
                                             out.print("<td class='t-align-center'>Compensated</td>");
                                         }else{
                                             out.print("<td class='t-align-center'>"+sptcd.getFieldName8()+"</td>");
                                         }
                                         out.print("<td class='t-align-center'>");
                                         if(!checkuserRights.isEmpty()){
                                         out.print("<a href='"+request.getContextPath()+"/partner/ViewTenderPaymentDetails.jsp?payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"&view=CMS'>Payment Details</a>");
                                         if("Paid".equalsIgnoreCase(sptcd.getFieldName8()) || "Extended".equalsIgnoreCase(sptcd.getFieldName8())){
                                             if("bg".equalsIgnoreCase(sptcd.getFieldName6())) {
                                                String strActionRequest = "";
                                                List<SPTenderCommonData> lstPaymentActionRequest =
                                                                              tenderCommonService.returndata("getRequestActionFromPaymentId", sptcd.getFieldName1(), null);
                                                if(lstPaymentActionRequest.isEmpty()){
                                                    allowReleaseForfeit = true;
                                                } else {
                                                    allowReleaseForfeit = false;
                                                    strActionRequest = lstPaymentActionRequest.get(0).getFieldName2();
                                                }
                                                if("Release".equalsIgnoreCase(strActionRequest)){
                                                    strActionRequest = "Released";
                                                }
                                                if(allowReleaseForfeit){
                                                        out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestrelease&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"'>Release Request</a>");
                                                        out.print("&nbsp;|&nbsp;<a href='"+request.getContextPath()+"/partner/TenderPaymentReleaseForfeit.jsp?action=requestforfeit&payId="+sptcd.getFieldName1()+"&uId="+sptcd.getFieldName3()+"&tenderId="+sptcd.getFieldName4()+"&lotId="+sptcd.getFieldName5()+"&payTyp="+sptcd.getFieldName6()+"'>Compensate Request</a>");

                                                } else {
                                                    if("Forfeit".equalsIgnoreCase(strActionRequest))
                                                    {
                                                        out.print("&nbsp;&nbsp;(Compensate)");
                                                    }else{
                                                    out.print("&nbsp;&nbsp;("+strActionRequest+")");
                                                    }
                                                }
                                             }
                                          }
                                          }
                                          out.print("</td></tr>");

                                         }
                                         if(j==0){
                                            out.print("<tr><td colspan='5'>No records available</td></tr>");
                                         }
                                         out.print("</table>");
                                 }
                                 out.print("</td></tr>");
                                %>
                                <%
                                                                int count = 1;
                                                               %>
                                <tr><td colspan="2">
                                        <%
                                            NOAServiceImpl noaServiceImpl = (NOAServiceImpl) AppContext.getSpringBean("NOAServiceImpl");
                                            List<Object[]> noalist = noaServiceImpl.getDetailsNOAforInvoice(Integer.parseInt(tenderId),Integer.parseInt(lotList.getFieldName5()));
                                            Object[] noaObj  = null;
                                            if(!noalist.isEmpty())
                                            {
                                                noaObj = noalist.get(0);
                                                if(noaObj[12]!=null && !"0.000".equalsIgnoreCase(noaObj[12].toString()))
                                                {
                                        %>

                                                            <%}}else{%>Not Generated
                                                            <%}%>
                                                        </tr>
                                                    </table>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                            <tr>
                                                <th width="2%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                                <th width="26%" class="t-align-center">
                                                    Forms
                                                </th>
                                                <th width="60%" class="t-align-center"><%=bdl.getString("CMS.PR.listofpr")%></th>
                                                <th width="20%" class="t-align-center"><%=bdl.getString("CMS.status")%></th>
                                            </tr>
                                          <%
                                          //List<Object> invoicId = service.getInvoiceId(obj.toString());
                                          List<Object[]> invoicId = service.getInvoiceIdList(wpId);
                                          List<TblCmsPrMaster> Prlist = service.getPRHistory(Integer.parseInt(wpId));

                                            %>
                                            <tr>
                                                <td style="text-align: center;">1</td>
                                                <td>
                                                    Reimbursable Expenses
                                                </td>
                                                <td>
                                                    <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                                    <tr>
                                                        <th width="20%" class="t-align-center"><%=bdl.getString("CMS.PR.prno")%></th>
                                                        <th width="50%" class="t-align-center"><%=bdl.getString("CMS.PR.listofpr")%></th>
                                                        <th width="10%" class="t-align-center"><%=bdl.getString("CMS.action")%>
                                                         </th>
                                                     </tr>
                                                    <%
                                             int prId=0;
                                             if(!Prlist.isEmpty() && Prlist!=null){
                                             int k=1;
                                             for(int ii=0;ii<Prlist.size();ii++){
                                                    String temp[] = Prlist.get(ii).getProgressRepNo().split(" ");
                                                    String temp1[] = temp[4].split("-");
                                                    System.out.println(temp[4]);
                                                    String finalNo = temp[0]+" "+temp[1]+" "+temp[2]+" "+temp[3]+" "+DateUtils.customDateFormate(DateUtils.convertStringtoDate(temp[4].toString(),"yyyy-MM-dd"));
    %>
                                             <tr>
                                               <td class="t-align-center"><%=k+ii%></td>
                                             <td class="t-align-left"><%=finalNo%></td>
                                             <td class="t-align-left"><a href="ViewPr.jsp?repId=<%=Prlist.get(ii).getProgressRepId()%>&wpId=<%=wpId %>&tenderId=<%=tenderId%>&flag=inv&lotId=<%= lotList.getFieldName5() %>">View</a></td>
                                             <%
                                                prId=Prlist.get(ii).getProgressRepId();
                                            }}%>
                                                    </table>
                                                </td>
                                                <td class="t-align-left">
                                                     <%if(!invoicId.isEmpty() && invoicId!=null){%>
                                                    <%}else{%>
                                                    Not Generated
                                                    <%}%>
                                                <%if(!invoicId.isEmpty() && invoicId!=null){
                                                %>
                                                <div>
                                                    <table class="tableList_1 t_space">
                                                        <tr>
                                                     <th width="3%" class="t-align-center">Invoices</th>
                                                     <th width="3%" class="t-align-center">Status</th>
                                                        </tr>
                                                <%
                                                int ii=1;
                                                for(int invoicIndex=0; invoicIndex<invoicId.size(); invoicIndex++){
                                                Object[] invobj = invoicId.get(invoicIndex);
                                                %>
                                                <tr>
                                                    <td class="t-align-left"><a href="ViewInvoice.jsp?tenderId=<%=tenderId%>&invoiceId=<%=invobj[0]%>&lotId=<%=lotList.getFieldName5()%>&wpId=<%=wpId%>"><%=invobj[2].toString()%></a></td>
                                                    <td class="t-align-left">
                                                                    <%
                                                        String status = "";
                                                        if (invobj[1] == null) {
                                                            status = "-";
                                                        } else if ("createdbyten".equalsIgnoreCase(invobj[1].toString())) {
                                                            if (procnature.equalsIgnoreCase("works")) {
                                                                status = "Invoice Generated by Contractor";
                                                            } else {
                                                                status = "Invoice Generated by Supplier";
                                                            }
                                                        } else if ("acceptedbype".equalsIgnoreCase(invobj[1].toString())) {
                                                            status = "Invoice Accepted by PE";
                                                        } else if ("sendtope".equalsIgnoreCase(invobj[1].toString())) {
                                                            status = "In Process";
                                                        } else if ("sendtotenderer".equalsIgnoreCase(invobj[1].toString())) {
                                                            status = "Processed by Accounts Officer";
                                                        } else if ("remarksbype".equalsIgnoreCase(invobj[1].toString())) {
                                                            status = "Processed by PE";
                                                        } else if ("rejected".equalsIgnoreCase(invobj[1].toString())) {
                                                            status = "Invoice Rejected by PE";
                                                        }
                                                        out.print(status);
    %>
                                                                </td>
                                                </tr>
   <%
   ii++;
                                                } %>

                                                </table>
                                                </div>
                                                </td>
                                            </tr>


                                            <%

                                                }
                                    }
                                                                                                                            %>
                                        </table>


                        </form>

                    </div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
       <script>
            function Edit(wpId,tenderId,lotId){


                dynamicFromSubmit("EditDatesForBoq.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&flag=cms");

            }
            function view(wpId,tenderId,lotId){
                dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
            }
        </script>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>

</html>
