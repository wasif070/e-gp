<%-- 
    Document   : WCCUpload
    Created on : Jul 29, 2011, 6:00:46 PM
    Author     : Sreenu.Durga
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertificateServiceBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertificate"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWcCertDoc"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsWcCertDocServiceBean"%>
<%@page import="com.cptu.egp.eps.web.utility.CheckExtension"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Upload Documents to certificate</title>
        <%
                    //int wcCertId = 0;
                    //if (request.getParameter("wcCertId") != null) {
                      //  wcCertId = Integer.parseInt(request.getParameter("wcCertId"));
                    //}
                    int tenderId = 0;
                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }
                    int contractUserId = 0;
                    if (request.getParameter("contractUserId") != null) {
                        contractUserId = Integer.parseInt(request.getParameter("contractUserId"));
                    }
                    int contractSignId = 0;
                    if (request.getParameter("contractSignId") != null) {
                        contractSignId = Integer.parseInt(request.getParameter("contractSignId"));
                    }
                    String functionName = "";
                    String message = "";
                    if (request.getParameter("functionName") != null) {
                        functionName = request.getParameter("functionName");
                    }
                    boolean check = false;
                    if (functionName.length() > 0) {
                        if (request.getParameter("check") != null) {
                            check = Boolean.parseBoolean(request.getParameter("check"));
                        }
                        if ("upload".equalsIgnoreCase(functionName)) {
                            if (!check) {
                                message = "File Uploaded successfully!";
                            } else {
                                message = "Error encountered while Uploading the file. </br> Please Check the <b>Name</b> or <b>Size</b> or <b>Extension</b> of the file!";
                            }
                        } else if ("download".equalsIgnoreCase(functionName)) {
                            if (!check) {
                                message = "File Downloaded Succesfully!";
                            } else {
                                message = "Error encountered while downloading the file. Please try again!";
                            }
                        } else if ("remove".equalsIgnoreCase(functionName)) {
                            if (!check) {
                                message = "File Removed Succesfully!";
                            } else {
                                message = "Error encountered while removing the file. Please try again!";
                            }
                        }
                    }
                    String logUserId = "0";
                    if (session.getAttribute("userId") != null) {
                        logUserId = session.getAttribute("userId").toString();
                    }
                    CmsWcCertificateServiceBean cmsWcCertificateServiceBean = new CmsWcCertificateServiceBean();
                    TblCmsWcCertificate tblCmsWcCertificate =
                    cmsWcCertificateServiceBean.getCmsWcCertificateForContractSignId(contractSignId);
        %>
    </head>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#frmWCCUpload").validate({
                rules: {
                    //uploadDocFile: {required: true},
                    documentBrief: {required: true,maxlength:100}
                },
                messages: {
                    //uploadDocFile: { required: "<div class='reqF_1'>Please select Document.</div>"},
                    documentBrief: { required: "<div class='reqF_1'>Please enter Description.</div>",
                        maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                }
            });
            $(function() {
                $('#frmWCCUpload').submit(function() {
                    if(document.getElementById("uploadDocFile").value=="")
                    {document.getElementById("docspan").innerHTML="please select Document";return false;}
                    if($('#frmWCCUpload').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                            browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;                               
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                alert(fileName);
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#button').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        });
    </script>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="pageHead_1"> Upload Document
                                <span style="float: right; text-align: right;">
                                    <%
                                        CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
                                        String procnature = commonservice.getProcNature(request.getParameter("tenderId")).toString();
                                        String serviceType = commonservice.getServiceTypeForTender(Integer.parseInt(request.getParameter("tenderId")));
                                        if("services".equalsIgnoreCase(procnature))
                                        {
                                            if("Time based".equalsIgnoreCase(serviceType.toString()))
                                            {
                                    %>
                                                <a class="action-button-goback" href="ProgressReportMain.jsp?tenderId=<%=tenderId%>">Go back</a>
                                            <%}else{%>        
                                                <a class="action-button-goback" href="SrvLumpSumPr.jsp?tenderId=<%=tenderId%>">Go back</a>
                                    <%}}else{%>
                                    <a class="action-button-goback" href="ProgressReport.jsp?tenderId=<%=tenderId%>">Go back</a>
                                    <%}%>
                                </span>
                            </div>
                            <%
                                        if (message.length() != 0) {
                                            if(!check){
                                                out.print("<div id='successMsg' class='responseMsg successMsg' style='display:block; margin-top: 10px;'>");

                                            }else{
                                                out.print("<div id='errorMsg' class='responseMsg errorMsg' style='display:block; margin-top: 10px;'>");
                                            }                                            
                                            out.print(message);
                                            out.print("</div>");
                                        }
                            %>

                            <form method="POST" id="frmWCCUpload"  enctype="multipart/form-data" action="<%=request.getContextPath()%>/WCCUploadServlet?functionName=upload">
                                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1 t_space ">
                                    <%    if (request.getParameter("msg") != null) {
                                    if ("wci".equals(request.getParameter("msg"))) {%>

                                    <div  class='responseMsg successMsg t-align-left'>
                                        Data Inserted Successfully
                                    </div>
                                                    <%
                                                                }
                                                            }
                                                    %>

                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="ff" align="left">
                                            Fields marked with (<span class="mandatory">*</span>) are mandatory.
                                            <input type="hidden" name="wcCertId" value="<%=tblCmsWcCertificate.getWcCertId()%>" id="wcCertId">
                                            <input type="hidden" name="tenderId" value="<%=tenderId%>" id="tenderId">
                                            <input type="hidden" name="contractUserId" value="<%=contractUserId%>" id="contractUserId">
                                            <input type="hidden" name="contractSignId" value="<%=contractSignId%>" id="contractSignId">
                                        </td>                                   
                                    </tr>
                                    <tr>
                                        <td width="15%" class="ff t-align-left">Document   : <span class="mandatory">*</span></td>
                                        <td width="85%" class="t-align-left">
                                            <input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                                            <span id="docspan" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Description : <span>*</span></td>
                                        <td>
                                            <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                            <div id="dvDescpErMsg" class='reqF_1'></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <%
                                                CmsWcCertDocServiceBean cmsWcCertDocServiceBean = new CmsWcCertDocServiceBean();
                                                cmsWcCertDocServiceBean.setLogUserId(logUserId);
                                                int docCnt = 0;
                                                List<TblCmsWcCertDoc> tblCmsWcCertDocList = cmsWcCertDocServiceBean.getAllCmsWcCertDocForWccId(tblCmsWcCertificate.getWcCertId());
                                                if(tblCmsWcCertDocList != null && !tblCmsWcCertDocList.isEmpty()) {
                                            %>
                                            <label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Upload" /></label>
                                            <input type="hidden" name="success" value="issue">
                                            <%}else{%>
                                            <label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Issue" /></label>
                                            <input type="hidden" name="success" value="upload">
                                            <%}%>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" cellspacing="0" cellpadding="0" class="tableList_1 t_space">
                                    <tr>
                                        <th width="100%"  class="t-align-left">Instructions</th>
                                    </tr>
                                    <tr>
                                        <%
                                                    CheckExtension checkExtension = new CheckExtension();
                                                    TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("officer");
                                        %>
                                        <td class="t-align-left">
                                            Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                            <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left">Acceptable File Types
                                            <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left">A file path may contain any below given special characters:
                                            <span class="mandatory">(Space, -, _, \)</span>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="4%" class="t-align-center">Sl. No.</th>
                                    <th class="t-align-center" width="23%">File Name</th>
                                    <th class="t-align-center" width="32%">File Description</th>
                                    <th class="t-align-center" width="7%">File Size <br />(in KB)</th>
                                    <th class="t-align-center" width="18%">Action</th>
                                </tr>
                                <%                                            
                                            if (tblCmsWcCertDocList != null && !tblCmsWcCertDocList.isEmpty()) {
                                                for (TblCmsWcCertDoc tblCmsWcCertDoc : tblCmsWcCertDocList) {
                                                    docCnt++;
                                                    out.print("<tr>");
                                                    out.print("<td class='t-align-center'> " + docCnt + "</td>");
                                                    out.print("<td class='t-align-left'> " + tblCmsWcCertDoc.getDocumentName() + "</td>");
                                                    out.print("<td class='t-align-left'> " + tblCmsWcCertDoc.getDocDescription() + "</td>");
                                                    out.print("<td class='t-align-center'> " + (Long.parseLong(tblCmsWcCertDoc.getDocSize()) / 1024) + "</td>");
                                                    out.print("<td class='t-align-center'> "
                                                            + "<a href =" + request.getContextPath() + "/WCCUploadServlet?functionName=download&wcCertDocId="
                                                            + tblCmsWcCertDoc.getWcCertDocId() + "&wcCertId=" + tblCmsWcCertificate.getWcCertId() + "&tenderId=" + tenderId + "&contractSignId=" + contractSignId
                                                            + "><img src='../resources/images/Dashboard/Download.png' alt='Download' /></a>"
                                                            + "&nbsp;" + "&nbsp;" + "&nbsp;"+ "&nbsp;" + "&nbsp;" + "&nbsp;"
                                                            + "<a href =" + request.getContextPath() + "/WCCUploadServlet?functionName=remove&wcCertDocId="
                                                            + tblCmsWcCertDoc.getWcCertDocId() + "&wcCertId=" + tblCmsWcCertificate.getWcCertId() + "&tenderId=" + tenderId + "&contractSignId=" + contractSignId
                                                            + "><img src='../resources/images/Dashboard/Delete.png' alt='Remove' /></a>");
                                                    out.print("</td>");
                                                    out.print("</tr>");
                                                }
                                            } else {
                                                out.print("<tr>");
                                                out.print(" <td colspan='5' class='t-align-center'>No records found.</td>");
                                                out.print("</tr>");
                                            }
                                %>
                            </table>
                        </td><!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End-->
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
</html>