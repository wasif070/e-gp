<%-- 
    Document   : EvalPostQue
    Created on : Dec 30, 2010, 7:04:30 PM
    Author     : Administrator
--%>


<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.BidderClarificationSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData,com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Post Query</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){

                $("#frmPostQuestion").validate({
                    rules:{
                        txtQuestion:{required:true,maxlength:1000}
                    },

                    messages:{
                        txtQuestion:{required:"<div class='reqF_1'>Please enter Query</div>",
                            maxlength:"<div class='reqF_1'>Maximum 1000 characters are allowed</div>"}

                    }
                });
            });

        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <%
                AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");
                EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");

                String tenderId = "0", uId = "0", poBy = "0", pkgLotId = "0", frmId = "0", bidId = "", st = "";
                String strGovUserId="0";
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                st = request.getParameter("st");

                if (session.getAttribute("govUserId") != null) {
                     strGovUserId = session.getAttribute("govUserId").toString();
                }
                if (request.getParameter("tenderId") != null) {
                    tenderId = request.getParameter("tenderId");
                }
                if (request.getParameter("uId") != null) {
                    uId = request.getParameter("uId");
                }
                if (request.getParameter("frmId") != null) {
                    frmId = request.getParameter("frmId");
                }

                if (session.getAttribute("userId") != null) {
                    poBy = session.getAttribute("userId").toString();
                }

                if (request.getParameter("pkgLotId") != null) {
                    pkgLotId = request.getParameter("pkgLotId");
                }
               // if (request.getParameter("bidId") != null) {
               //     bidId = request.getParameter("bidId");
               // }
                // Submit the question

                if ("Submit".equals(request.getParameter("btnSubmit"))) {
                    String action = "Seek clarification to tenderer by TEC/PEC Members";
                    try{
                       String postXml = "";
                        String answer = "";
                         postXml = "<root><tbl_EvalFormQues tenderId=\"" + tenderId
                                + "\" pkgLotId=\"" + pkgLotId + "\" tenderFormId=\"" + frmId
                                + "\" userId=\"" + uId + "\" quePostedBy=\"" + poBy
                                + "\" quePostDt=\"" + format.format(new Date()) + "\" answer=\"" + answer
                                + "\" queSentByTec=\"0\" evalStatus=\"" + answer
                                + "\" question=\"" + URLEncoder.encode(request.getParameter("txtQuestion"),"UTF-8")
                                + "\" postedByGovUserId=\"" + strGovUserId + "\" /></root>";
                       // CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                        
                     //   CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert","tbl_EvalFormQues",postXml,"").get(0);
                        //Insert by dohatec
                        //String evalCount = evalService.getEvaluationNo(Integer.parseInt(tenderId));
                        CommonMsgChk commonMsgChk =addUpdate.addUpdOpeningEvaluation("SeekClarification",tenderId,pkgLotId,frmId,uId,poBy,format.format(new Date()),answer,"0",answer,URLEncoder.encode(request.getParameter("txtQuestion"),"UTF-8"),strGovUserId,"","","","","","","","").get(0);

                    }catch(Exception e){
                        action = "Error in "+action+" "+e.getMessage();
                    }finally{
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(tenderId), "TenderId", EgpModule.Evaluation.getName(), action, "");
                        action = null;
                    }                  
                }

                // Delete the question
                if (request.getParameter("qId") != null && request.getParameter("std") != null) {

                    String strCond = "userId=" + uId + " And tenderId=" + tenderId + 
                            " And tenderFormId=" + frmId + " And evalQueId=" + request.getParameter("qId");

                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                     // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    commonXMLSPService.setAuditTrail(objAuditTrail);
                    commonXMLSPService.setModuleName(EgpModule.Evaluation.getName());
                    String auditValues[]=new String[4];
                    auditValues[0]="tenderId";
                    auditValues[1]= tenderId;                            
                    auditValues[2]="Remove clarification to tenderer by TEC/PEC Members";
                    auditValues[3]= "";
                    CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("delete","tbl_EvalFormQues","",strCond,auditValues).get(0);
                    
                    
                }
            %>
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">View Forms / Seek Clarification
                 <span style="float:right;">
                <a href="SeekEvalClari.jsp?uId=<%=request.getParameter("uId")%>&st=<%=st%>&tenderId=<%=tenderId%>" class="action-button-goback">Go back to Dashboard</a>
                </span>
                </div>
                 <%
                            pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <div class="t_space t-align-center formStyle_1 ff">
                    <%
                        ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                        CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        List<SPCommonSearchDataMore> tendererData = dataMore.geteGPData("getUNameTendererIdCmpId", uId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        String[] jvSubData = creationService.jvSubContractChk(tenderId, uId);
                        StringBuilder name_link = new StringBuilder();
                        name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewRegistrationDetail.jsp?uId="+uId+"&tId="+tendererData.get(0).getFieldName3()+"&cId="+tendererData.get(0).getFieldName2()+"&top=no' target='_blank'>"+tendererData.get(0).getFieldName1()+"</a>");
                        if(jvSubData[0]!=null && jvSubData[0].equals("jvyes")){
                            name_link.delete(0, name_link.length());
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewJVCADetails.jsp?uId="+uId+"' target='_blank'>"+tendererData.get(0).getFieldName1()+"</a>");
                            name_link.append("<br/>");
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewJVCADetails.jsp?uId="+uId+"' target='_blank' style='color:red'>(JVCA - View Details)</a>");
                        }
                        if(jvSubData[1]!=null && jvSubData[1].equals("subyes")){
                            name_link.append("<br/>");
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewTenderSubContractor.jsp?uId="+uId+"&tenderId="+tenderId+"&tId="+tendererData.get(0).getFieldName3()+"&cId="+tendererData.get(0).getFieldName2()+"' target='_blank' style='color:red'>(Sub Contractor/Consultant - View Details)</a>");
                        }
                        out.print(name_link);
                    %>
                </div>
                <div class='responseMsg noticeMsg' style='margin-top: 12px;vertical-align: top;'><br>Queries to be asked to Tenderers/Consultants for clarification of their Tenders in order to assist the examination and evaluation of the Tender should not lead to a change in substance of the Tender or in any of the key elements of the Tender, such as price and delivery schedule.</div>

                <%@include  file="../officer/TSCComments.jsp" %>

                
                <form id="frmPostQuestion" name="frmPostQuestion" method="POST" action="EvalPostQue.jsp?tenderId=<%=tenderId%>&st=<%=st%>&uId=<%=uId%>&frmId=<%=frmId%>&pkgLotId=<%=pkgLotId%>">                    
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th colspan="2" class="t-align-left">
                                Post Query
                            </th>
                        </tr>
                        <tr>
                            <td width="15%" valign="middle" class="t-align-left ff">Query : <span style="color: red;"> *</span></td>
                            <td width="90%" class="t-align-left">
                                <textarea rows="5" id="txtQuestion" name="txtQuestion" class="formTxtBox_1" style="width: 99%;"></textarea>
                            </td>
                        </tr>
                       
                    </table>
                    <div class="t_space t-align-center">
                        <label class="formBtn_1">
                                    <input name="btnSubmit" type="submit" value="Submit" />
                        </label>
                    </div>
                    <%if (request.getParameter("msgId") != null) {
                    String msgId = "", msgTxt = "";
                    boolean isError = false;
                    msgId = request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")) {
                        if (msgId.equalsIgnoreCase("error")) {
                            isError = true;
                            msgTxt = "There was some error.";
                        } else {
                            msgTxt = "Question removed successfully.";
                        }
                    %>
                    <%if (isError) {%>
                    <div class="responseMsg errorMsg" style="margin-top: 10px;"><%=msgTxt%></div>
                    <%} else {%>
                    <div class="responseMsg successMsg" style="margin-top: 10px;"><%=msgTxt%></div>
                    <%}%>
                    <%}
                         }
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th class="t-align-center">Sl. No.</th>
                            <th class="t-align-center">Member Name</th>
                            <th class="t-align-center">Query</th>
                            <th class="t-align-center">Action</th>
                        </tr>
                        <%
							//modified by dohatec for re-evaluation
                             int evalCount = evalService.getEvaluationNo(Integer.parseInt(tenderId));
                            String strCondition = "userId=" + uId + " And tenderId=" + tenderId + " And tenderFormId=" + frmId + "and evalCount=" + String.valueOf(evalCount);
                            //String strConditionForAllUser = "tenderId=" + tenderId + " And tenderFormId=" + frmId;
                            BidderClarificationSrBean srBean = new BidderClarificationSrBean();
                            
                            int QusCnt = 0;
                            //TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> QuestionList = srBean.getFormQuestions(strCondition);
                            List<SPTenderCommonData> QuestionListForAllUser = srBean.getFormQuestions(strCondition);
                            
                            if (!QuestionList.isEmpty() && QuestionList.size() > 0) {

                                for (SPTenderCommonData QuestionAll : QuestionListForAllUser) {
                                    QusCnt++;
                                    // HIGHLIGHT THE ALTERNATE ROW
                                    if(Math.IEEEremainder(QusCnt,2)==0) {
                        %>
                        <tr class="bgColor-Green">
                         <% } else { %>
                        <tr>
                        <% } %>
                            <td width="5%" class="t-align-center"><%=QusCnt%></td>
                            <td width="10%" class="t-align-left"><%=QuestionAll.getFieldName8()%></td>
                            <td width="78%" class="t-align-left"><%=URLDecoder.decode(QuestionAll.getFieldName6(),"UTF-8")%></td>
                            <%for (SPTenderCommonData Question : QuestionList) {
                                if(QuestionAll.getFieldName9().equalsIgnoreCase(poBy)){
                            %>
                            <td width="15%" class="t-align-center"><a href="EvalPostQue.jsp?qId=<%=QuestionAll.getFieldName1()%>&st=<%=st%>&std=d&tenderId=<%=tenderId%>&uId=<%=uId%>&frmId=<%=frmId%>&pkgLotId=<%=pkgLotId%>&msgId=sc">Remove</a></td>
                            <%
                                }else{  %>
                                <td width="15%" class="t-align-center"></td>
                      <%  }
                                break;
                               } %>
                        </tr>
                        <%}                            
                               } else {%>
                            <tr>
                                <td colspan="4">No Queries Found</td>
                            </tr>
                        <%}%>
                    </table>
                </form>
            </div>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
