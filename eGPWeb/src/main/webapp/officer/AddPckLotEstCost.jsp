<%--
    Document   : AddPckLotEstCost
    Created on : Dec 6, 2010, 7:32:40 PM
    Author     : shreyansh
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderEstCost"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotSecurity"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="appSrBean" class="com.cptu.egp.eps.web.servicebean.APPSrBean" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
     <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
    <script type="text/javascript">
        function regForNumber(value)
        {
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

        }
        function validate()
        {
            $('.err').remove();
            var count = document.getElementById('count').value;
//            alert(count);
//
//            alert(document.getElementById('taka_0').value);
//            alert(document.getElementById('taka_1').value);
//            return false;
            var vbool=true;
            for(var i=0;i<count;i++){
                if(!regForNumber(document.getElementById('taka_'+i).value)){
                    $("#taka_"+i).parent().append("<div class='err' style='color:red;'>Please enter Numerals (0-9) only and 9 maximum Digits after Decimal</div>");
                    vbool = false;
                }else if(document.getElementById('taka_'+i).value.indexOf("-")!=-1){
                    $("#taka_"+i).parent().append("<div class='err' style='color:red;'>Please enter Numerals (0-9) only and 9 maximum Digits after Decimal</div>");
                    vbool = false;
                } else if(document.getElementById('taka_'+i).value.split(".")[1] != undefined){
                    if(document.getElementById('taka_'+i).value.split(".")[1].length > 9){
                        $("#taka_"+i).parent().append("<div class='err' style='color:red;'>Please enter Numerals (0-9) only and maximum 9 Digits after Decimal</div>");
                        vbool = false;
                    }
                } else if(parseFloat(document.getElementById('taka_'+i).value) <= 0){
                        $("#taka_"+i).parent().append("<div class='err' style='color:red;'>Input should be greater than zero.</div>");
                        vbool = false;                    
                }
            }
            if(!vbool){
                return false;
            } else {
                 document.getElementById("submit").style.display = 'none';
            }
        }
         //added by ahsan for showning amount in words
        function estimateCost(obj,Location){
           document.getElementById("estimateCostInWords_"+Location).innerHTML = '';
           obj.value = ($.trim(obj.value) * 1);
           document.getElementById("estimateCostInWords_"+Location).innerHTML =WORD((obj.value)*1000000);
        }
        function estimateCostNu(obj,Location){
           document.getElementById("estimateCostInWordsNu_"+Location).innerHTML = '';
           obj.value = ($.trim(obj.value) * 1);
           document.getElementById("estimateCostInWordsNu_"+Location).innerHTML =WORD((obj.value)*1000000);
           max = document.getElementById('maxVal').value;
           min = document.getElementById('minVal').value;
           area2 = document.getElementById('area').value;
           pCategory2 = document.getElementById('pCategory').value;
           value = document.getElementById('taka_'+Location).value*1000000;
           max2 = max/1000000;
           min2 = min/1000000;
           if(value>max)
           {
               jAlert("Official Cost Estimate is greater than threshold value. When Area: "+area2+", Procurement Category: "+pCategory2+", "+max2+" million Nu. is the maximum value for Official Cost Estimate according to Procurement Method Rules."," Alert ", "Alert");
           }
           else if(value<min)
           {
               jAlert("Official Cost Estimate is less than threshold value. When Area: "+area2+", Procurement Category: "+pCategory2+", "+min2+" million Nu. is the minimum value for Official Cost Estimate according to Procurement Method Rules."," Alert ", "Alert");
           } 
        }
        function estimateCost2(obj,Location){
           document.getElementById("estimateCostInWords_"+Location).innerHTML = '';
           obj.value = ($.trim(obj.value) * 1);
           document.getElementById("estimateCostInWords_"+Location).innerHTML =WORD((obj.value)*1000000);
           max = document.getElementById('maxVal').value;
           min = document.getElementById('minVal').value;
           area2 = document.getElementById('area').value;
           pCategory2 = document.getElementById('pCategory').value;
           value = document.getElementById('taka_'+Location).value*1000000;
           max2 = max/1000000;
           min2 = min/1000000;
           if(value>max)
           {
               jAlert("Official Cost Estimate is greater than threshold value. When Area: "+area2+", Procurement Category: "+pCategory2+", "+max2+" million Nu. is the maximum value for Official Cost Estimate according to Procurement Method Rules."," Alert ", "Alert");
           }
           else if(value<min)
           {
               jAlert("Official Cost Estimate is less than threshold value. When Area: "+area2+", Procurement Category: "+pCategory2+", "+min2+" million Nu. is the minimum value for Official Cost Estimate according to Procurement Method Rules."," Alert ", "Alert");
           }
        }
    </script>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <%
                    // Variable tenderId is defined by u on ur current page.
                    pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                    pageContext.setAttribute("tab", "2");
                    TenderDocumentSrBean tenderDocumentSrBean = new TenderDocumentSrBean();
                    List<TblTenderLotSecurity> lots = null;
                    int tendid = 0;
                    if (request.getParameter("tenderid") != null) {
                        tendid = Integer.parseInt(request.getParameter("tenderid"));
                        lots = tenderDocumentSrBean.getLotDetails(tendid);
                    }
                    /*if("Use STD".equalsIgnoreCase(request.getParameter("submit"))){
                    tenderDocumentSrBean.dumpSTD(String.valueOf(tendid), request.getParameter("txtStdTemplate"), session.getAttribute("userId").toString());
                    response.sendRedirect("LotDetails.jsp?tenderid="+tendid);
                    }*/

        %>
        
        
        
           
            
<!-- To check if the given official cost estimate is in the range, Dohatec, 6th September, 2016-->
            <%
                String[] maxMin = new String[7];
                String changedArea = "";
                Object list2 = null;
                Object list3 = null;
                Object list4 = null;
                if(request.getParameter("tenderid")!=null)
                {
                    list2 = tenderDocumentSrBean.getPkgNo(request.getParameter("tenderid").toString());
                    list3 = tenderDocumentSrBean.getDptId(request.getParameter("tenderid").toString());
                    if(list3!=null)
                    {
                        list4 = tenderDocumentSrBean.getDptType(list3.toString());
                    }
                    if(list4!=null)
                    {
                        maxMin = appSrBean.getMinMax(list2.toString(),list4.toString());
                        if(maxMin[2].contains("SubDistrict"))
                        {
                            changedArea = "Dungkhag";
                        }
                        else if(maxMin[2].contains("District"))
                        {
                            changedArea = "Dzongkhag";
                        }
                        else if(maxMin[2].contains("Division"))
                        {
                            changedArea = "Department";
                        }
                        else if(maxMin[2].contains("Organization"))
                        {
                            changedArea = "Division";
                        }
                        else if(maxMin[2].contains("Autonomus"))
                        {
                            changedArea = "Autonomous";
                        }
                        else
                        {
                            changedArea = maxMin[2];
                        }
                    }
               }
            %>
            <input type="hidden" id="maxVal" value="<%=maxMin[0]%>">
            <input type="hidden" id="minVal" value="<%=maxMin[1]%>">
            <input type="hidden" id="area" value="<%=changedArea%>">
            <input type="hidden" id="pCategory" value="<%=maxMin[3]%>">
<!-- To check if the given official cost estimate is in the range, Dohatec, 6th September, 2016-->
            
             
             
        

        
        
        
        
        
        
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Official Cost Estimate</title>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>

        <div class="contentArea_1">
            <div class="pageHead_1">Official Cost Estimate
                <span class="c-alignment-right"><a href="TenderDashboard.jsp?tenderid=<%=tendid%>" class="action-button-goback">Go back to Tender Dashboard</a></span>
            </div>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                        String procnature = commonService.getProcNature(tendid + "").toString();

            %>


            <div class="tabPanelArea_1 ">
                <form name="Tenderestcost" method="post" action="<%= request.getContextPath()  %>/TenderEstCostServlet">
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <%
                                        if (procnature.equalsIgnoreCase("services")) {
                            %>

                            <th width="10%" class="t-align-center">Package No.</th>
                            <th width="70%" class="t-align-left">Package Description</th>

                            <%} else {%>                <th width="10%" class="t-align-center">Lot No.</th>
                            <th width="70%" class="t-align-left">Lot Description</th>
                            <%}%>

                            <th width="20%" class="t-align-left">Official Cost Estimate
                                In million (Nu.)
                                <input type="hidden" name="proNature" value="<%=procnature%>"/>
                            </th>
                        </tr>



                        <%

                                    int i = 0;
                                    Iterator it = lots.iterator();
                                    if (!procnature.equalsIgnoreCase("services")) {
                                        while (it.hasNext()) {
                                            TblTenderLotSecurity tblTenderLotSecurity = (TblTenderLotSecurity) it.next();
                        %>

                        <tr>
                            <td class="t-align-center"><%= tblTenderLotSecurity.getLotNo()%></td>
                            <td><%=tblTenderLotSecurity.getLotDesc()%></td>
                            <input type="hidden" name="packId" value="<%= tblTenderLotSecurity.getAppPkgLotId()%>">
                            <td class="t-align-center"><input type="text" class="formTxtBox_1"name="taka_<%= i%>" id="taka_<%= i%>" onblur="estimateCostNu(this,<%= i%>);" />
                            
                                             <div id="estimateCostInWordsNu_<%= i%>"></div>
                            </td>
                        </tr>
                        <%
                                                                    i++;
                                                                   
                                                                }
                                         out.println(" <input type=hidden id=count name=count value="+i+ "></input>");
                                                            } else {
                                                                int j = 0;
                                                                List<Object[]> list = commonService.getPkgDetialByTenderId(tendid);
                                                                for (Object[] obj : list) {
                        %>
                        <tr>
                            <td class="t-align-center"><%= obj[0].toString()%></td>
                            <td><%=obj[1].toString()%></td>
                            <td class="t-align-center"><input type="text" class="formTxtBox_1"name="taka_<%= j%>" id="taka_<%= j%>" onblur="estimateCost2(this,'<%= j%>');" />
                            <div id="estimateCostInWords_<%= j%>"></div>
                            </td>
                        </tr>
                        <%
                                            j++;
                                            
                                        }
                                                                out.print(" <input type=hidden name=count id=count value=" + j + "></input>");
                                    }
                        %>
                        <input type="hidden" name="tenderid" value="<%=tendid%>" />
                        <tr>
                            <td colspan="3" class="t-align-center ff">
                                <span class="formBtn_1">
                                    <input type="submit" name="submit" id="submit" value="Submit" onclick="return validate();" /></span>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <div>&nbsp;</div>

        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

    </body>

</html>
