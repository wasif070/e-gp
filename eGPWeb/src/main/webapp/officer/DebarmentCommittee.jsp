<%-- 
    Document   : DebarmentCommittee
    Created on : Jan 18, 2011, 4:01:53 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.servicebean.InitDebarmentSrBean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%if("y".equals(request.getParameter("isview"))){out.print("View ");}if("y".equals(request.getParameter("isedit"))){out.print("Edit ");}%>Debarment Committee</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
             function validate(){

                $(".err").remove();

                var valid = true;
                if($.trim($("#txtcommitteeName").val()) == "") {
                    $("#txtcommitteeName").parent().append("<div class='err' style='color:red;'>Please enter Committee Name.</div>");
                    valid = false;
                }else{
                    if($("#txtcommitteeName").val().length>25) {
                        $("#txtcommitteeName").parent().append("<div class='err' style='color:red;'>Max 25 characters allowed.</div>");
                        valid = false;
                    }
                }
                var len=$('#members').children()[1].children.length;
                var cp=0;
                var mems = new Array();
                for(var i=0;i<len;i++){                        
                    if($('#cmbMemRole'+i).val()=="cp"){
                        cp++;
                    }
                    mems.push($('#memfrm'+i).val());
                }
                var cbol=false;
                for(var i=0;i<len;i++){
                    if($('#cmbMemRole'+i).val()=="cp"){
                        if(mems[i].indexOf('AO',0)!=-1){
                            cbol=true;
                        }
                    }
                }                
                if(cp>1){
                    jAlert("There can be only 1 Chairperson.","Chairperson alert", function(RetVal) {
                    });
                   return false;
                }
                if(cp==0){
                    jAlert("There must be 1 Chairperson.","Chairperson alert", function(RetVal) {
                    });
                    return false;
                }
                if(!cbol){
                    jAlert("Chairperson must be AO.","Procurement Role alert", function(RetVal) {
                    });
                    return false;
                }
                if(!valid){
                    return false;
                }
             }
            $(function() {
                $('#btnSearch').click(function() {
                    if($('#cmboffice').val()!=null){
                        $.post("<%=request.getContextPath()%>/InitDebarment", {officeId:$('#cmboffice').val(),pe1count:$('#pe1count').val(),action:'memberSearch'}, function(j){                            
                            $("#tbodype").html(j.toString());
                        });
                    }else{
                        $("#tbodype").html(null);
                    }
                });
            });
            function getOfficesForDept(){
                $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId:$('#cmborg').val(),funName:'Office'}, function(j){
                    $("select#cmboffice").html(j);
                });
            }

            function delRow(row){
                var curRow = $(row).parents('tr');
                var len=$('#members').children()[1].children.length;
                var id=$(row).attr("id").substring(6,$(row).attr("id").length);
                for(var i=id;i<len;i++){
                    $('#memid'+eval(eval(i)+eval(1))).attr("id","memid"+(i));
                    $('#cmbMemRole'+eval(eval(i)+eval(1))).attr("id","cmbMemRole"+(i));
                    $('#memfrm'+eval(eval(i)+eval(1))).attr("id","memfrm"+(i));
                    $('#delRow'+eval(eval(i)+eval(1))).attr("id","delRow"+(i));
                }
                curRow.remove();
            }

            $(function() {
                // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
                $( "#dialog:ui-dialog" ).dialog( "destroy" );
                $( "#dialog-form" ).dialog({
                    autoOpen: false,
                    resizable:false,
                    draggable:false,
                    height: 500,
                    width: 600,
                    modal: true,
                    buttons: {
                        "Add": function() {
                            $(function() {
                                //if(eval(($(":checkbox[checked='true']").length)+($('#members').children()[1].children.length))<=$('#maxmem').val()){
                                var len=$('#members').children()[1].children.length;
                                var i =len;
                                $(":checkbox[checked='true']").each(function(){
                                    var a = $(this).attr("id");
                                    var b = a.substring(3, a.length);
                                    var addUser=true;
                                    var sameHope=false;
                                    var isHope=false;
                                    if($("#chk"+b).val()==$("#hopeId").val()){
                                        sameHope=true;
                                    }
                                    if(sameHope){
                                        jAlert("Hopa cannot be a member of Review Committee.","Hopa alert", function(RetVal) {
                                        });
                                    }else{
                                        for(var j=0;j<len;j++){
                                            if($('#memid'+j).val()==$("#chk"+b).val()){
                                                addUser=false;
                                                break;
                                            }
                                            if($("#memstat"+b).val().indexOf('HOPE',0)!=-1){
                                                isHope=true;
                                                break;
                                            }
                                        }
                                        if(isHope){
                                            jAlert("Hopa cannot be a member of Review Committee.","Hopa alert", function(RetVal) {
                                            });
                                        }else{
                                            if(addUser){
                                                $( "#members tbody" ).append("<tr>"+
                                                    "<td class='t-align-left'><input id='memid"+i+"' type='hidden' name='memUserIds' value='"+$("#chk"+b).val()+"'/>"+$("#spn"+b).val()+"</td>"+
                                                    "<td class='t-align-left'>"+
                                                    "<select name='memberRoles' class='formTxtBox_1' id='cmbMemRole"+i+"' style='width:200px;'>"+
                                                    "<option selected='selected' value='cp'>Chairperson</option>"+
                                                    "<option value='m'>Member</option>"+
                                                    "</select>"+
                                                    "</td>"+
                                                    "<td class='t-align-center'><input id='memfrm"+i+"' type='hidden' name='memberFroms' value='"+$("#memstat"+b).val()+"'/>"+$("#memstat"+b).val()+"</td>"+
                                                    "<td class='t-align-left'><a id='delRow"+i+"' class='action-button-delete' onclick='delRow(this)'>Remove</a></td>"+
                                                    "</tr>");
                                                i++;
                                            }else{
                                                jAlert("Member already added in the committee.","Same member alert", function(RetVal) {
                                                });
                                            }
                                      }
                                    }
                                });            
                            });
                            $( this ).dialog( "close" );
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                        }
                    },
                    close: function() {
                    }
                });

                $("#addmem" ).click(function(){
                    $("#dialog-form").dialog("open");
                    $("input[type='checkbox']").removeAttr("checked");
                });
            });
        </script>
    </head>
</head>
<body>
    <%
        boolean isEdit=false;
        String debarComId="";
        String comName="";
        if ("y".equals(request.getParameter("isedit")) || "y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub"))) {
             isEdit = true;
             debarComId = request.getParameter("comId");
             comName = request.getParameter("cName");
        }
        if("y".equals(request.getParameter("isview"))){
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(session.getAttribute("userId").toString()), "userId", EgpModule.Debarment.getName(), "View Debarment Committee by Hopa", "");
        }
        InitDebarmentSrBean srBean = new InitDebarmentSrBean();
        String debarId = request.getParameter("debId");
        if("Submit".equalsIgnoreCase(request.getParameter("submit"))){
            srBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()));
            srBean.createDebarCommittee(request.getParameter("committeeName"), session.getAttribute("userId").toString(), debarId, request.getParameterValues("memUserIds"), request.getParameterValues("memberRoles"));
            response.sendRedirect("HopeDebarListing.jsp?&isprocess=y");
        }else if("Update".equalsIgnoreCase(request.getParameter("submit"))){
            srBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()));
            srBean.updateDebarCommittee(request.getParameter("committeeName"),request.getParameter("comId"), request.getParameterValues("memUserIds"), request.getParameterValues("memberRoles"));
            response.sendRedirect("HopeDebarListing.jsp?isprocess=y");
        }else if("Publish".equalsIgnoreCase(request.getParameter("submit"))){
            srBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()));
            srBean.publishDebarCommitte(request.getParameter("comId"),request.getParameter("hdnComName"),debarId,request.getParameter("uname"),request.getParameter("compname"),session.getAttribute("userId").toString());
            response.sendRedirect("HopeDebarListing.jsp?pub=succ&isprocess=y");
        }else{
    %>
    <%@include  file="../resources/common/AfterLoginTop.jsp"%>
    <div class="contentArea_1">
        <div class="pageHead_1"><%if("y".equals(request.getParameter("isview"))){out.print("View ");}if("y".equals(request.getParameter("isedit"))){out.print("Edit ");}%>Debarment Committee <span style="float:right;"> <a class="action-button-goback" href="HopeDebarListing.jsp?isprocess=y">Go back</a> </span></div>
        <input type="hidden" id="hopeId" value="<%=session.getAttribute("userId").toString()%>">        
    <form action="DebarmentCommittee.jsp?debId=<%=debarId%>" method="post">
        <input type="hidden" value="<%=objUserName%>" name="uname">
        <input type="hidden" value="<%=request.getParameter("compname")%>" name="compname">
        <table width="100%" cellspacing="0" class="tableList_1 t_space">
            <tr>
                <td width="26%" class="t-align-left ff">Committee Name : <%if (!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))) {%><span class="mandatory">*</span><%}%></td>
                <td width="74%" class="t-align-left">
                    <%if (!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))) {%>
                    <input name="committeeName" type="text" class="formTxtBox_1" id="txtcommitteeName" style="width:200px;" value="<%=comName%>"/>
                    <%}else{out.print(comName);}%>
                    <input name="comId" type="hidden" class="formTxtBox_1" value="<%=debarComId%>"/>
                    <input name="hdnComName" type="hidden" class="formTxtBox_1" value="<%=comName%>"/>
                </td>
            </tr>
        </table>
        <br/>
        <%if (!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))) {%>
        <div class="t-align-center"><a id="addmem" class="action-button-add">Add Members</a></div>
        <%}%>
        <table width="100%" cellspacing="0" class="tableList_1 t_space" id="members">
            <thead>
                <tr>
                    <th width="17%" class="t-align-left ff">Members Name</th>
                    <th width="53%" class="t-align-left ff">Committee Role <%/*if(!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))){*/%><%--<span class="mandatory">*</span><%}%>--%></th>
                    <th width="17%" class="t-align-left ff">Procurement Role</th>
                    <%if (!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))) {%><th width="13%" class="t-align-left ff">Action</th><%}%>
                </tr>
            </thead>
            <tbody>
            <%
                if(isEdit){
                int j=0;
                for(SPTenderCommonData cmdb : srBean.savedDebarmentComUser(debarComId)){
            %>
            <tr>
                <td class="t-align-left"><input id="memid<%=j%>" type="hidden" name="memUserIds" value="<%=cmdb.getFieldName1()%>"/><%=cmdb.getFieldName2()%></td>
                <td class="t-align-left">
                     <%if("y".equals(request.getParameter("isview"))||"y".equals(request.getParameter("ispub"))){
                        if (cmdb.getFieldName4().equals("cp")) {
                            out.print("Chairperson");
                        }else if (cmdb.getFieldName4().equals("m")) {
                            out.print("Member");
                        }
                    }else{%>
                    <select name="memberRoles" class="formTxtBox_1" id="cmbMemRole<%=j%>" style="width:200px;">
                        <option <%if(cmdb.getFieldName4().equals("cp")){%>selected<%}%> value="cp">Chairperson</option>                        
                        <option <%if(cmdb.getFieldName4().equals("m")){%>selected<%}%> value="m">Member</option>
                        </select>
                    </td>
                    <%}%>
                <td class="t-align-center"><input id="memfrm<%=j%>" type="hidden" name="memberFroms" value="<%=cmdb.getFieldName3()%>"/><%=cmdb.getFieldName3()%></td>
                <%if(!("y".equals(request.getParameter("isview")) || "y".equals(request.getParameter("ispub")))){%><td class="t-align-center"><a id="delRow<%=j%>" class="action-button-delete" onclick="delRow(this)">Remove</a></td><%}%>
                </tr>
                <%j++;}}%>
            </tbody>
        </table>
        <br/>
        <%if("y".equals(request.getParameter("isedit")) || request.getParameter("comId")==null){%>
        <div class="t-align-center">
            <label class="formBtn_1">
                <input name="submit" type="submit" value="<%if("y".equals(request.getParameter("isedit"))){out.print("Update");}if(request.getParameter("comId")==null){out.print("Submit");}%>" id="btnSubmit" onclick="return validate();"/>
            </label>
        </div>
        <%}%>
        <%if("y".equals(request.getParameter("ispub"))){%>
        <div class="t-align-center">
            <label class="formBtn_1">
                <input name="submit" type="submit" value="Publish" id="btnSubmit"/>
            </label>
        </div>
        <%}%>
    </form>
    <div id="dialog-form" title="Debarment Committee Member">
        <fieldset>
            <div class="formBg_1"  id="pe2">
                <input type="hidden" id="pe1count" value="0">
                <table width="100%" border="0" cellpadding="0" cellspacing="10" class="formStyle_1">
                    <tr>
                        <td width="14%" class="ff">Organisation Name :</td>
                        <td width="86%">
                            <input type="text" id="txtDeptName" style="width: 200px;" class="formTxtBox_1">
                            <input type="hidden" id="cmborg">
                            <a id="imgTree" href="javascript:void(0);" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=createCommitee', '', 'width=350px,height=400px,scrollbars=1','');">
                                <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff" width="14%">PE Office :</td>
                        <td width="86%">
                            <select id="cmboffice" style="width: 200px;" class="formTxtBox_1">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="14%">&nbsp;</td>
                        <td width="86%" align="center">
                            <label class="formBtn_1">
                                <input type="button" id="btnSearch" value="Search"/>
                            </label>
                        </td>
                    </tr>
                </table>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="10%" class="t-align-center">Select</th>
                        <th class="t-align-center" width="30%">Member Name</th>
                        <th class="t-align-center" width="30%">Member Designation</th>
                        <th class="t-align-center" width="30%">Procurement Role</th>
                    </tr>
                    <tbody id="tbodype">

                    </tbody>
                </table>
            </div>
        </fieldset>
    </div>
    </div>
    <%@include file="../resources/common/Bottom.jsp" %>
    <%}%>
</body>
</html>
