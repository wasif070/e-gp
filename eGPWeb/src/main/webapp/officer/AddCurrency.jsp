<%--
    Document   : AddCurrency
    Created on : Aug 30, 2012, 12:39:07 PM
    Author     : Md. khaled Ben Islam
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderCurrency"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigureCurrencySrBean"%>
<%@page import="com.cptu.egp.eps.web.servlet.TenderCurrencyConfigServlet"%>
<%@page import="javax.servlet.http.HttpSession"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            
System.out.print("test");
        %>
        <%
            // Variable tenderId is defined by u on ur current page.
            pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Add Currency</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>

        <!--jAlert -->
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <script type="text/javascript">

            jQuery(document).ready(function($){

                $("#btnAddCurrency").click(function(){
                    if($("#ddlCurrency")[0].selectedIndex > 0 && parseInt($("#totalCurrencyAdded").val()) <= 3 && !isAlreadyAdded($("#ddlCurrency").val())){

                        $("#totalCurrencyAdded").val(parseInt($("#totalCurrencyAdded").val()) + 1);
                        $("#addedCurrencies").val($("#addedCurrencies").val() + "," + $("#ddlCurrency").val());

                        $(".ajax-feedback").css("visibility", "visible");
                        $.post("<%=request.getContextPath()%>/TenderCurrencyConfigServlet", {action: "AddTenderCurrency",tenderId: "<%=request.getParameter("tenderid")%>",currencyId: $("#ddlCurrency").val()},  function(currencyId){
                            if(currencyId != undefined && parseInt(currencyId) > 0 ){
                                $("#ddlCurrency").val(currencyId);
                                $("#tblCurrency").append("<tr><td width='15%' class='t-align-center'>" + $("#tblCurrency tr").length + "</td><td width='70%' class='t-align-left'>" + $("#ddlCurrency option:selected").text() + "</td><td width='15%' class='t-align-center'><input class='checkboxDelete' type='checkbox' value='"+ $("#ddlCurrency").val() + "'></td></tr>");

                            }
                        });
                        $(".ajax-feedback").css("visibility", "hidden");
                    }
                    else {
                        if(parseInt($("#totalCurrencyAdded").val()) > 3){
                            jAlert("System will allow only 4 four currencies.", "Alert");
                            return false;
                        }

                        if(isAlreadyAdded($("#ddlCurrency").val())){
                            jAlert("Selected currecy already added.", "Alert");
                            return false;
                        }
                    }
                });

                $("#btnDeleteCurrency").click(function(){

                    var length = 0;
                    var checkedID = "";

                    $('.checkboxDelete').each(function () {
                        if ($(this).attr("checked")) {
                            checkedID = checkedID + "" + $(this).val() + ",";
                            length++;
                        }
                    });
                    checkedID = checkedID.substring(0, checkedID.length - 1);

                    if (checkedID == "") {
                        jAlert("No Currency Selected.");
                        return false;
                    }
                    else {
                        $(".ajax-feedback").css("visibility", "visible");
                        $.post("<%=request.getContextPath()%>/TenderCurrencyConfigServlet", {action: "DeleteTenderCurrency",tenderId: "<%=request.getParameter("tenderid")%>",currencyIds: checkedID},  function(currencyList){
                        if(currencyList != undefined && currencyList != "false"){
                            $("#tblCurrency tbody").remove();
                            $("#tblCurrency").append(currencyList);
                        }
                    });
                    $(".ajax-feedback").css("visibility", "hidden");
                    }
                });

            }); //ready()

            /**
             * Array.prototype.[method name] allows you to define/overwrite an objects method
             * needle is the item you are searching for
             * this is a special variable that refers to "this" instance of an Array.
             * returns true if needle is in the array, and false otherwise
             */
            Array.prototype.contains = function ( needle ) {
               for (i in this) {
                   if (this[i] == needle) return true;
               }
               return false;
            }

            //if currency
            function isAlreadyAdded(currencyID){
                if($("#addedCurrencies").val() != undefined && $("#addedCurrencies").val() != ""){
                    var IDs = $("#addedCurrencies").val().split(",");
                    if(IDs.contains(currencyID)){
                        return true;
                    }
                }
                else {
                    return false;
                }
            }

        </script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1">
            <div class="pageHead_1">Add Currency <span style="float:right;"><a href="Notice.jsp?tenderid=<%=request.getParameter("tenderid")%>" class="action-button-goback">Go Back To Dashboard</a></span>
            </div>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <div class="formBg_1">
                    <table id="tblSearchBox" cellspacing="10" class="formStyle_1" width="100%">

                        <tr>
                            <td width="17%" class="ff">Choose Currency :</td>
                            <td width="33%">
                                <jsp:useBean id="configureCurrencySrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ConfigureCurrencySrBean"/>
                                <select name="ddlCurrency" class="formTxtBox_1" id="ddlCurrency" style="width:208px;">
                                    <option value="0" selected="selected">-- Select Currency --</option>
                                    <c:forEach var="currency" items="${configureCurrencySrBean.currencyList}">
                                        <option value="${currency.objectId}">${currency.objectValue}</option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td width="50%"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <label class="formBtn_1"><input type="button" name="addCurrency" id="btnAddCurrency" value="Add" /></label>&nbsp;
                                <img class="ajax-feedback" alt="" title="" src="/resources/images/loading.gif" style="visibility: hidden; height: 18px;">
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-center" colspan="4">
                                <%
                                    //Default Currency (Bangladeshi Taka - BTN) added automatically
                                    int iCurrencyId = 0;
                                    iCurrencyId = configureCurrencySrBean.getCurrencyId("BTN");

                                    BigDecimal bigExchangeRate = new BigDecimal(0.00);
                                    int iTenderId = Integer.parseInt(request.getParameter("tenderid"));
                                    HttpSession sessionObj = request.getSession();
                                    int  iUserid = Integer.parseInt(sessionObj.getAttribute("userId").toString());

                                    TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
                                    long totalCurrencyAdded = tenderCurrencyService.getCurrencyCountForTender(iTenderId);

                                  /*  if(totalCurrencyAdded == 0){
                                        TblTenderCurrency tblTenderCurrency = new TblTenderCurrency();
                                        tblTenderCurrency.setCurrencyId(iCurrencyId);
                                        tblTenderCurrency.setTenderId(iTenderId);
                                        tblTenderCurrency.setExchangeRate(bigExchangeRate);
                                        tblTenderCurrency.setCreatedBy(iUserid);
                                        tblTenderCurrency.setCreatedDate(new Date());
                                        tenderCurrencyService.addTblTenderCurrency(tblTenderCurrency);
                                    }*/
                                %>
                            </td>
                        </tr>
                    </table>
            </div>
            <div>&nbsp;</div>
            <div class="tabPanelArea_1 ">
                <form name="currencyInfo" method="post" action="">
                    <table id="tblCurrency" width="100%" cellspacing="0" class="tableList_1">
                        <thead>
                            <tr>
                                <th width="15%" class="t-align-center">No.</th>
                                <th width="70%" class="t-align-left">Currency Name</th>
                                <th width="15%" class="t-align-left">Select</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                String currencyRows = ""; //will hold the result
                                String addedCurrencies = ""; //comma separated IDs of Currencies already added to the tender

                                List<Object[]> listCurrencyObj = new ArrayList<Object[]>();
                                listCurrencyObj = tenderCurrencyService.getCurrencyTenderwise(iTenderId);

                                if(listCurrencyObj.size() > 0){
                                    int rowNo = 0;
                                    for(Object[] obj :listCurrencyObj){
                                        rowNo +=1;
                                        currencyRows+= "<tr>";
                                        currencyRows+= "<td width='15%' class='t-align-center'>" + rowNo + "</td>";
                                        //currencyRows+= "<td width='70%' class='t-align-left'>" + obj[3] + " - " + obj[0] + "</td>";  //Obj[0] holds Currency Name, Obj[1] holds Currency Rate, Obj[2] holds Currency ID, obj[3] holds Cyrrency Short Name (e.g. BTN)
                                        if(obj[3].equals("BTN") || obj[3].equals("btn")){//obj[3] holds the CurrencyShortName (e.g. BTN), BTN is the default currency and BTN must not allowed to be deleted
                                            currencyRows+= "<td width='70%' class='t-align-left'>BTN - Bhutanese Ngultrum</td>";  //Obj[0] holds Currency Name, Obj[1] holds Currency Rate, Obj[2] holds Currency ID, obj[3] holds Cyrrency Short Name (e.g. BTN)
                                            currencyRows+= "<td width='15%' class='t-align-center'><input class='checkboxDelete' type='checkbox' value='"+ obj[2] + "' disabled='disabled'></td>";
                                        }
                                        else {
                                            currencyRows+= "<td width='70%' class='t-align-left'>" + obj[3] + " - " + obj[0] + "</td>";  //Obj[0] holds Currency Name, Obj[1] holds Currency Rate, Obj[2] holds Currency ID, obj[3] holds Cyrrency Short Name (e.g. BTN)
                                            currencyRows+= "<td width='15%' class='t-align-center'><input class='checkboxDelete' type='checkbox' value='"+ obj[2] + "'></td>";
                                        }

                                        currencyRows+= "</tr>";
                                        addedCurrencies += obj[2] + ",";
                                    }
                                }
                                out.print(currencyRows);
                            %>
                        <input type="hidden" name="addedCurrencies" value="<%= addedCurrencies %>" id="addedCurrencies">
                        <input type="hidden" id="totalCurrencyAdded" name="currencyCounter" value="<%= listCurrencyObj.size()%>"/>
                        </tbody>
                    </table>
                </form>
                <table id="tblSearchBox" cellspacing="10" class="formStyle_1" width="100%">
                        <tr>
                            <td></td>
                            <td class="t-align-right">
                                <label class="formBtn_1"><input type="button" name="deleteCurrency" id="btnDeleteCurrency" value="Delete" /></label> &nbsp;
                                <img class="ajax-feedback" alt="" title="" src="/resources/images/loading.gif" style="visibility: hidden; height: 18px;">
                            </td>
                        </tr>
                    </table>
            </div>
        </div>
        <div>&nbsp;</div>

        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

    </body>
</html>