<%-- 
    Document   : ConfigureDomesticPreference
    Created on : Jun 15, 2011, 2:21:59 PM
    Author     : rishita
--%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDomesticPref"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <title>Configure Domestic Preference</title>
    </head>
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    <body>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            </div>
            <div class="contentArea_1">
                <!--Dashboard Header End-->
                <div class="pageHead_1">Configure Domestic Preference<span style="float:right;"><a href="EvalComm.jsp?tenderid=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back To Dashboard</a></span></div>

                <%
                            String tenderId = "";
                            if (request.getParameter("tenderId") != null && !"".equals(request.getParameter("tenderId"))) {
                                tenderId = request.getParameter("tenderId");
                            }
                            CommitteMemberService commMemService = (CommitteMemberService) AppContext.getSpringBean("CommitteMemberService");
                            boolean isEdit = false;
                            if (commMemService.getTblTenderDomesticPref(Integer.parseInt(tenderId)) != 0) {
                                isEdit = true;
                            }
                            List<TblTenderDomesticPref> ttdp = new ArrayList<TblTenderDomesticPref>();
                            ttdp = commMemService.getDetailsTblTenderDomesticPref(Integer.parseInt(tenderId));
                            String userId = "";
                            HttpSession hs = request.getSession();
                            if (hs.getAttribute("userId") != null) {
                                userId = hs.getAttribute("userId").toString();
                            }
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <form method="POST" id="frmConfigureDomPre" name="frmConfigureDomPre" action="<%=request.getContextPath()%>/TenderDomesticPrefServlet?action=<%if (isEdit) {%>edit<% } else {%>insert<% }%>">
                    <table border="0" cellspacing="10" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <th>S.No</th>
                            <th>Tenderers/Consultants Name</th>
                            <th>Select Bidder/Consultant for Domestic Preference</th>
                        </tr>
                        <%

                                    List<SPTenderCommonData> listDomesticPre =
                                            tenderCommonService.returndata("getDomesticTenderersList", tenderId, null);
                                    int srno = 1;
                                    for (SPTenderCommonData listing : listDomesticPre) {
                                        commMemService.setLogUserId(userId);
                        %>
                        <tr>
                            <td width="10%" class="t-align-center"><%=srno%></td>
                            <td width="80%"><%=listing.getFieldName2()%></td>
                            <td width="10%" class="t-align-center"><input class="formTxtBox_1" <% if (isEdit) {
                                                                        for (TblTenderDomesticPref tdp : ttdp) {
                                                                            if (tdp.getUserId() == Integer.parseInt(listing.getFieldName1())) {
                                                                                if (tdp.getDomesticPref().equalsIgnoreCase("Yes")) {%>checked <% }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }%> type="checkbox" id="chkdomesticPref<%=srno %>" name="domesticPref" value="<%=listing.getFieldName1()%>"/>
                                <input type="hidden" name="userId" value="<%=listing.getFieldName1()%>"/>
                            </td>
                        </tr>
                        <% srno++;
                                    }%>
                                    <input type="hidden" id="cnt" value="<%=srno%>">
                        <%
                                    if (isEdit) {
                                        for (TblTenderDomesticPref tdp : ttdp) {
                        %><input type="hidden" name="domPriceId" value="<%=tdp.getDomPriceId()%>"/>
                        <input type="hidden" name="createdDt" value="<%=tdp.getCreatedDt() %>"/>
                        <% } } %>
                        <tr>
                            <td colspan="3" class="t-align-center">
                                <input type="hidden" name="tenderid" value="<%=tenderId%>"/>
                                <label class="formBtn_1">
                                    <input  id="Submit" name="Submit" onclick="return validation();" value="Submit" type="submit" />
                                </label>
                            </td>
                        </tr>
                    </table>
                    <div id="msgVal"></div>
                </form>
            </div>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
            $(function() {
                    $('#frmConfigureDomPre').submit(function() {
                        var vbool=true;
                         var cnt=$('#cnt').val();
                         var chk=0;
                         for(var i=1;i<cnt;i++){
                            if(!$('#chkdomesticPref'+i).attr("checked")){
                                chk++;
                            }
                         }
                         if(chk>0){
                              if(confirm("Selected Bidder/Consultant will be considered as Domestic Preferences.Are you sure?"))
                                    vbool = true;
                                  else vbool = false;
                             // });
                             //vbool = false;
                         }
                         return vbool;
                    });
                });
    </script>
</html>
