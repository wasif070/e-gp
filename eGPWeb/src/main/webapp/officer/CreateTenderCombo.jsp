<%-- 
    Document   : CreateTenderCombo
    Created on : Mar 31, 2011, 11:21:58 AM
    Author     : dixit
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create Bidder/Consultant ComboBox Form Wise</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/temp/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/Add.js"type="text/javascript"></script>
        <script language="JavaScript">
        /* javascript function for making a row cell */
        var count=0; var index=0;
        function AddInputWithEvent1(theform,compType,compName,compId,spanId,row,eventName,eventFunction,tdId)
        {
            var Cell;
            var comp ;
            Cell = document.createElement("TD");
            Cell.setAttribute("id",tdId);
            Cell.innerHTML="<input type='"+compType+"'  name='"+compName+"'  id='"+compId+"' "+eventName+"='"+eventFunction+"' class='formTxtBox_1'><span id='txtspan"+spanId+"'class='reqF_1'></span>";
            row.appendChild(Cell);
        }
        function AddInputWithEvent2(theform,compType,compName,compId,spanId,row,eventName,eventFunction,tdId)
        {
            var Cell;
            var comp ;
            Cell = document.createElement("TD");
            Cell.setAttribute("id","value"+(spanId+1));
            Cell.innerHTML="<input type='"+compType+"'  name='"+compName+"'  id='"+compId+"' "+eventName+"='"+eventFunction+"' class='formTxtBox_1'><span id='vtxtspan"+spanId+"'class='reqF_1'></span>";
            row.appendChild(Cell);
        }
        function AddInputWithEventChk(theform,compType,compName,compId,value,row,eventName,eventFunction,tdId)
        {
            var Cell;
            var comp ;
            Cell = document.createElement("TD");
            Cell.setAttribute("id",tdId);
            Cell.innerHTML="<input type='"+compType+"'  name='"+compName+"'  id='"+compId+"'  value='"+value+"' "+eventName+"='"+eventFunction+"' class='formTxtBox_1'>";
            row.appendChild(Cell);
        }
 /* javascript function for add new row in the table */
function addtoCombo(theform)
{
                var cnt = count; index++;
		var newCnt = parseInt(cnt);
                newCnt++;

			var tbody = document.getElementById("AddCombo").getElementsByTagName("tbody")[0];
			var Row = document.createElement("TR");
			Row.setAttribute("id","row"+index);
			Row.setAttribute("name","row"+index);
                        AddInputWithEventChk(theform,"checkbox","chkkField","chkkField"+index,index,Row,"TD");
			AddInputWithEvent1(theform,"text","lblField","lblField"+index,index,Row,"onblur","BlockMessages();","TD");
			AddInputWithEvent2(theform,"text","valueField","valueField"+index,index,Row,"onblur","vBlockMessages();","TD");
			AddInputWithEventChk(theform,"checkbox","chkField","chkField"+index,index,Row,"onClick","checkDefault(this.form,this)","TD");
			tbody.appendChild(Row);
			//document.getElementById('valueField'+index).value=index+1;
                        if(document.getElementById("radbtn2").checked==true)
                        {
                            document.getElementById("value"+(index+1)).style.display="none";
                        }
			count = index;
}
/* javascript function for validate item label and value field */
function ValidateListBoxName()
{
    var vbool=true;
    if(document.getElementById("ListName").value == "")
    {
        document.getElementById("listspan").innerHTML="Please enter combobox name";
        vbool= false;
    }var k=0;var m=0;var n=0;
    var j=0;for(var i=0; i<=index; i++){
    if(document.getElementById("lblField"+i).value == "")
    {   j=j+1;
        document.getElementById("txtspan"+i).innerHTML = "Please enter item label";
    }else{k=k+1;}
    if(document.getElementById("valueField"+i).value == "")
    {   j=j+1;
        document.getElementById("vtxtspan"+i).innerHTML = "Please enter item value";
    }else{k=k+1;}
    }
    if(k!=0)
    {
        for(var i=0; i<=index; i++)
        {  for(var l=0; l<=index; l++)
            {
                var itemtext1 = document.getElementById("lblField"+i).value;
                var itemtext2 = document.getElementById("lblField"+(l)).value;
                var itemtext3 = document.getElementById("valueField"+i).value;
                var itemtext4 = document.getElementById("valueField"+l).value;
                if(i!=l)
                {
                  if(itemtext1!="" && itemtext2!=""){    
                    if(itemtext1==itemtext2)
                    {
                        j=j+1;m=m+1;
                    }
                  }
                  if(itemtext3!="" && itemtext4!=""){  
                    if(itemtext3==itemtext4)
                    {
                        j=j+1;n=n+1;
                    }
                  }  
                }
            }
        }
        if(m!=0)
        {
            jAlert("find duplicates in item label","Bidder/Consultant Create Combo", function(RetVal) {});
        }
        if(n!=0)
        {
            jAlert("find duplicates in item value","Std Create Combo", function(RetVal) {});
        }
    }
    if(document.getElementById("radbtn1").checked==true)
    {
        for(var i=0; i<=index; i++)
        {
            if(document.getElementById("valueField"+i).value!="")
            {
                var evt = document.getElementById("valueField"+i).value;
                if(!/^[-+]?\d*\.?\d*$/.test(evt))//if(!/^[-+]?[0-9]\d{0,2}(\.\d{1,2})?%?$/.test(evt))
                {
                    document.getElementById("vtxtspan"+i).innerHTML = "Please Enter Numeric values";
                    j=j+1;
                }
            }
            else
            {
                document.getElementById("vtxtspan"+i).innerHTML = "Please enter item value";
                j=j+1;
            }
        }
    }
    if(j!=0){return false;}
    return vbool;
}
/* javascript function for validate List box name field */
function BlockListBoxMessage()
{
    if(document.getElementById("ListName").value!="")
    {
        document.getElementById("listspan").style.display = 'none';
    }
}
/* javascript function for validate item label & assigning  item label value to item value in the case of calc
 is not required */
function BlockMessages()
{
    for(var i =0; i<=count; i++){
    if(document.getElementById("lblField"+i).value!="")
    {
        document.getElementById("txtspan"+i).style.display = 'none';
        if(document.getElementById("radbtn2").checked==true)
        {
            var getvalue = document.getElementById("lblField"+i).value;
            document.getElementById("valueField"+i).value= getvalue;
        }
    }}
}
/* javascript function for validate item value field in the case of when calc is required */
function vBlockMessages()
{
    if(document.getElementById("radbtn1").checked==true)
    {
        for(var i =0; i<=count; i++)
        {
            if(document.getElementById("valueField"+i).value!="")
            {
                if(!/^[-+]?\d*\.?\d*$/.test(document.getElementById("valueField"+i).value))
                {
                        document.getElementById("vtxtspan"+i).innerHTML = "Please Enter Numeric values";
                }
                else
                {
                    document.getElementById("vtxtspan"+i).innerHTML = "";
                }
            }
        }
    }
}
/* javascript function for selected combo item is displayed when combo is viewed */
function checkDefault(theform,chk)
{
    chk.checked=true;
    for(var i=0;i<theform.chkField.length;i++)
    {
        if(document.getElementById("chkField"+i) != null && document.getElementById("chkField"+i) != chk)
            document.getElementById("chkField"+i).checked=false;
            
    }
}
/* javascript function for delete row from the table */
function deleteRow(tableID) {
            try {
            var bool=true;
            var table = document.getElementById(tableID);
            var rowCount = table.rows.length;
            var j=0;var c=0;
            for(var k=0; k<rowCount; k++) {
                var row = table.rows[k];
                var chkbox = row.cells[0].childNodes[0];
                    if(chkbox.checked==true)
                    {
                        c++;
                        j=j+1;
                    }
                }
                if(c>1)
                {
                    jAlert("please select only one checkbox","Bidder/Consultant Create Combo", function(RetVal) {});
                    bool= false;
                }else{for(var i=0; i<rowCount; i++){
                var row = table.rows[i];
                var chkbox = row.cells[0].childNodes[0];
                if(null != chkbox && true == chkbox.checked && bool)
                {
                     if(i!=2)
                     {
                            table.deleteRow(i);
                            for(var id=i; id<rowCount; id++)
                            {
                                $('#chkkField'+eval(eval(id)-eval(2))).attr("id","chkkField"+(id-3));
                                $('#lblField'+eval(eval(id)-eval(2))).attr("id","lblField"+(id-3));
                                $('#txtspan'+eval(eval(id)-eval(2))).attr("id","txtspan"+(id-3));
                                $('#valueField'+eval(eval(id)-eval(2))).attr("id","valueField"+(id-3));
                                $('#valueField'+eval(eval(id)-eval(1))).val(eval(id-1));
                                $('#chkField'+eval(eval(id)-eval(2))).attr("id","chkField"+(id-3));
                            }
                            rowCount--;index--;
                            i--;
                     }else{jAlert("At least 1 option is required for Combo box","Tender Create Combo", function(RetVal) {});}
                }}}
            if(j==0)
            {
                jAlert("please select the checkbox first","Bidder/Consultant Create Combo", function(RetVal) {});
            }
            }catch(e) {
                //alert(e);
            }return bool;
        }
        /* javascript function for the hide item value field column in the case of when calc is not required */
        function SelectedRadioButton()
        {
            var val = 0;
            for( i = 0; i < document.comboform.rad.length; i++ )
            {
                if( document.comboform.rad[i].checked == true )
                val = document.comboform.rad[i].value;
                if(val=="No")
                {
                    for(var x =0; x<=(index+1); x++)
                    {
                        document.getElementById("value"+x).style.display = 'none';
                    }
                }else if(val=="Yes")
                {
                    for(var x =0; x<=(index+1); x++)
                    {
                        document.getElementById("value"+x).style.display = 'table-cell';
                    }
                }
            }
        }
        </script>
    </head>
        <body onload="SelectedRadioButton()">
        <%
                String formid="";
                if(!"".equalsIgnoreCase(request.getParameter("formId"))){
                formid = request.getParameter("formId");}
                String tenderid="";
                if(!"".equalsIgnoreCase(request.getParameter("tenderId"))){
                tenderid = request.getParameter("tenderId");}
                int pkgOrLotId = -1;
                if(!"".equalsIgnoreCase(request.getParameter("porlId"))){
                pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));}
        %>
            <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp"%>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>

                    <td class="contentArea">

                        <div class="pageHead_1">Create Bidder/Consultant ComboBox Form Wise
                        
                        <span class="c-alignment-right"><a href="TenderDocPrep.jsp?tenderId=<%= tenderid %>&porlId=<%=pkgOrLotId%>" class="action-button-goback" title="Tender/Proposal Document">Tender Document</a></span>
                        
                        </div><div class="t_space b_space"> <%if(request.getParameter("msg")!=null){ %><div class="responseMsg errorMsg t_space"><span>Combo Creation Failed</span></div><%}%></div>

                        <%--<div class="pageHead_1" align="center"> <%=msg%></div>--%>
                        <form action='<%=request.getContextPath()%>/CreateTenderComboSrBean?formId=<%= formid %>&tenderId=<%= tenderid %>&porlId=<%=pkgOrLotId%>&message=tenderer&action=addcomboData' method="post" name="comboform">
                            <div style="font-style: italic" align="right" class="">Fields marked with (<span class="mandatory">*</span>) are mandatory.</div>&nbsp;&nbsp;
                            <table  cellspacing="0" class="tableList_1" width="100%" id="tblrule" name="tblrule" border="0">

                                <tr>
                                    <th colspan="2" style="text-align: left">Create Combo box</th>
                                </tr>

                                <tr>
                                    <td>
                                        <strong colspan="2">Combo box Name : <span class="mandatory">*</span></strong>
                                        <input type="text" name="ListName" id="ListName" size="25"  class="formTxtBox_1"  onblur="BlockListBoxMessage()" /><span style="padding-left:112px;" id="listspan" class="reqF_1"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong colspan="2">Is To Be Filled For Calculation :</strong>
                                        <input type="radio" name="rad" id="radbtn1" value="Yes"  onclick="SelectedRadioButton()"/> Yes
                                        <input type="radio" name="rad" id="radbtn2" value="No" CHECKED onclick="SelectedRadioButton()"/> No
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" class="tableView_1" align="center"  width="100%" cellpadding="0" cellspacing="0" id="AddCombo" >
                                        <tbody id="cmbValueTbody">
                                        <tr>
                                            <td colspan="2" align="center">
                                                <span class="formBtn_1"><input type="button" name="ADD" value="  Add  " onClick="addtoCombo(this.form);" /></span>
                                            </td>
                                            <td colspan="2" align="center">
                                                <span class="formBtn_1"><input type="button" name="Remove" value="  Remove  " onclick="return deleteRow('AddCombo')" /></span>
                                            </td>
                                        </tr>
                                         <tr>
                                                <td><strong>Select</strong></td>
                                                <td><strong>Item Label : <span class="mandatory">*</span></strong></td>
                                                <td id="value0"><strong>Item Value : <span class="mandatory">*</span></strong></td>
                                                <td><strong>Default<br/>Selected</strong></td>
                                        </tr>

                                         <tr id="row0">
                                                <td><input type="checkbox" name="chkkField" id="chkkField0" style="border: 1px solid #7799BC" value="0" /></td>
                                                <td><input type="text" name="lblField" id="lblField0" class="formTxtBox_1" onblur="BlockMessages()"/><span id="txtspan0" class="reqF_1" ></span></td>
                                                <td id="value1"><input type="text" name="valueField" id="valueField0" class="formTxtBox_1"  value="" onblur="vBlockMessages()"/><span id="vtxtspan0" class="reqF_1" ></span></td>
                                                <td><input type="checkbox" name="chkField" id="chkField0" style="border: 1px solid #7799BC" checked value="0" onClick="checkDefault(this.form,this)"/></td>
                                        </tr>

                                        </tbody>
			               </table>
		                    </td>

                                </tr>
                            	<tr>
                                    <td class="t-align-center">
                                        <span class="formBtn_1"><input type="submit" id="SaveCombo"name="SaveCombo" value="Save Combo"onclick=" return ValidateListBoxName()"/></span>&nbsp;
                                        <!--<span class="formBtn_1"><input type="button" name="Close" value="Go Back" onClick="javascript: history.go(-1)"/></span>-->
                                    </td>
                                </tr>

                            </table>&nbsp;

                            <input type="hidden" name="TotRule" id="TotRule" value="2"/>
                            <input type="hidden" name="itemCount"/>
                            <div>&nbsp;</div>
                            <div align="center"></div>
                        </form>
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

    </script>
</html>

