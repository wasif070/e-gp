<%-- 
    Document   : TER3
    Created on : Apr 14, 2011, 10:31:01 AM
    Author     : TaherT
--%>

<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<jsp:useBean id="negotiationProcessSrBean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<jsp:useBean id ="DomesticPreference" class="com.cptu.egp.eps.web.servicebean.DomesticPreferenceSrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    String tenderid = request.getParameter("tenderid");
                    String lotId = request.getParameter("lotId");
                    String roundId = request.getParameter("rId");
                    String stat = request.getParameter("stat");
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    String pNature = commonService.getProcNature(request.getParameter("tenderid")).toString();
                    String pMethod = commonService.getProcMethod(request.getParameter("tenderid")).toString();
                    String eventType = commonService.getEventType(tenderid).toString();
                    //String tenderPaidORFree = commonService.tenderPaidORFree(tenderid, lotId);
                    boolean isREOI = commonService.getEventType(tenderid).toString().equalsIgnoreCase("REOI");

                    String repLabel = "Tender";
                    if(pNature.equalsIgnoreCase("Services")){
                        repLabel = "Proposal";
                    }
                     boolean bole_ter_flag = true;
                      if(request.getParameter("isPDF")!=null && "true".equalsIgnoreCase(request.getParameter("isPDF"))){
                        bole_ter_flag = false;
                        //System.out.println("isPDF"+bole_ter_flag);
                    }
                    String userId = "";
                    if(bole_ter_flag){
                        if(session.getAttribute("userId") != null){
                            userId = session.getAttribute("userId").toString();
                        }
                    }else{
                        if(request.getParameter("tendrepUserId") != null && !"".equals(request.getParameter("tendrepUserId"))){
                            userId = request.getParameter("tendrepUserId");
                        }
                    }
                    
                    boolean showGoBack = true;
                    if("y".equals(request.getParameter("in"))){
                        showGoBack = false;
                    }

                     int evalCount = 0;
                     if(request.getParameter("evalCount")!=null){
                        evalCount = Integer.parseInt(request.getParameter("evalCount"));
                     }
                                       
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=repLabel%> Evaluation Report 3</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            function signPQCheck(){
                /*jAlert("Post Qualification of Tenderers/Consultants is pending.","Member Signing", function(RetVal) {
                });*/
                alert("Post Qualification of Tenderers/Consultants is pending.");
                return false;
            }
            function signNegCheck(){
                /*jAlert("Negotiation of Tenderers/Consultants is pending.","Member Signing", function(RetVal) {
                });*/
                alert("Negotiation of Tenderers/Consultants is pending.");
                return false;
            }
            $(function() {
                    $( "#dialog:ui-dialog" ).dialog( "destroy" );
                    $( "#dialog-form" ).dialog({
                        autoOpen: false,
                        resizable:false,
                        draggable:false,
                        height: 250,
                        width: 500,
                        modal: true,
                        buttons: {
                         "OK": function() {
                                 $(this).dialog("close");
                         }},
                        close: function() {
                            $(this).dialog("close");
                        }
                    });
                    $("#sfblock").click(function(){
                        $("#dialog-form").dialog("open");
                    });
            });
       
        </script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <body>
        <%if(bole_ter_flag){%>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%}
                ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");

                    List<SPCommonSearchDataMore> tendOpeningRpt = creationService.getOpeningReportData("getTenderOpeningRpt", tenderid, lotId,null);//lotid


                    CommonSearchDataMoreService searchDataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> TECMembers = searchDataMoreService.getCommonSearchData("getTECMembers", tenderid, lotId,"TER3",roundId,String.valueOf(evalCount)); //lotid ,"TOR1"

                    //List<Object[]> finalSubmissionUser = creationService.getFinalSubmissionUser(tenderid);

                    //List<Object[]> ter2Criteria = creationService.getTERCriteria("TER2", pNature);

                    List<SPCommonSearchDataMore> TendererAwarded = searchDataMoreService.getCommonSearchData("getPostQualifiedBidders", tenderid, lotId,roundId,String.valueOf(evalCount)); //lotid ,"TOR1"
                    

                    List<SPCommonSearchDataMore> NoteofDissent = creationService.getOpeningReportData("getNoteofDissent", tenderid, lotId,"ter3");
                    List<SPCommonSearchDataMore> pMethodEstCost = searchDataMoreService.getCommonSearchData("GetPMethodEstCostTender", tenderid);
                    String estCost = pMethodEstCost.get(0).getFieldName2();
                    String estCostStatus = "-";
                    if (estCost != null) {
                        estCostStatus = (Double.parseDouble(estCost) > 0 ? "Approved" : "-");
                    }

                    boolean list1 = true;
                    boolean list5 = true;
                    boolean isView = false;
                    if (tendOpeningRpt.isEmpty()) {
                        list1 = false;
                    }
                    if (TECMembers.isEmpty()) {
                        list5 = false;
                    }
                     if("y".equals(request.getParameter("isview"))){
                        isView = true;
                    }
                    
                    if(isView)
                   {
                         // Coad added by Dipal for Audit Trail Log.
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        String idType="tenderId";
                        int auditId=Integer.parseInt(request.getParameter("tenderid"));
                        String auditAction="View Tender Evaluation Report 3";
                        String moduleName=EgpModule.Evaluation.getName();
                        String remarks="";
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    }
                    boolean isPQ = false;
                    boolean isNeg = false;
                    if (!"Services".equalsIgnoreCase(pNature) && (!(pMethod.equals("DPM"))) && (!(eventType.equalsIgnoreCase("PQ") || eventType.equalsIgnoreCase("REOI") || eventType.equalsIgnoreCase("1 stage-TSTM")))){
                        isPQ = true;
                    }
                    if("Services".equalsIgnoreCase(pNature) && (!(eventType.equalsIgnoreCase("PQ") || eventType.equalsIgnoreCase("REOI") || eventType.equalsIgnoreCase("1 stage-TSTM")))){
                        isNeg = true;
                    }
                    if(!"Services".equalsIgnoreCase(pNature) && (pMethod.equalsIgnoreCase("DPM"))){
                        isNeg = true;
                    }
                    int postQCount=0;
        %>
        <div class="tabPanelArea_1">
            <div  id="print_area">

            <div class="pageHead_1"><%=repLabel%> Evaluation Report 3 - Financial Evaluation and Price Comparison<span style="float: right;">
                    <%
                        CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        boolean isCommCP = false;                        
                        List<SPCommonSearchDataMore> commCP = dataMoreService.getCommonSearchData("evaluationMemberCheck", tenderid, userId, "1", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);                        
                        if (commCP != null && (!commCP.isEmpty()) && (!commCP.get(0).getFieldName1().equals("0"))) {
                            isCommCP = true;
                        }                        
                        commCP=null;
                        if(bole_ter_flag){
                            if(isView){
                        %>
                        <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                        <span id="svpdftor1">
                         &nbsp;&nbsp;
                         <a class="action-button-savepdf" id="saveASPDF" href="<%=request.getContextPath()%>/TorRptServlet?tenderId=<%=tenderid%>&lotId=<%=lotId%>&action=TER3">Save As PDF</a>
                         </span>
                        <%}
                        if(showGoBack){
                        if(isCommCP){
                    %>
                    <a id="goBack" class="action-button-goback" href="Evalclarify.jsp?tenderId=<%=tenderid%>&st=rp&comType=TEC">Go Back to Dashboard</a>
                    <%}else{%>
                    <a id="goBack" class="action-button-goback" href="MemberTERReport.jsp?tenderId=<%=tenderid%>&comType=TEC">Go Back to Dashboard</a>
                    <%}}}%>
                </span></div>
            <%
                        pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                        pageContext.setAttribute("tab", "6");
                        pageContext.setAttribute("isPDF", request.getParameter("isPDF"));
                        pageContext.setAttribute("userId", 0);
                        /*if(!bole_ter_flag){
                            pageContext.setAttribute("isPDF","true");
                        }*/
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div class="tableHead_1 t_space t-align-center">Financial Evaluation and Price Comparison</div>
            <!--<form action="< %=request.getContextPath()%>/ServletEvalCertiService?funName=evalTERReportIns" method="post">-->
            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 b_space" width="100%">
                <tr>
                    <td width="24%" class="ff">Ministry Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName1());
                                }%></td>
                    <td width="16%" class="ff">Division Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName2());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff">Organization/Agency Name :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName3());
                                }%></td>
                    <td class="ff">Procuring Entity :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName4());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff"><%=repLabel%> Package No. and Description :</td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName5()+" & "+tendOpeningRpt.get(0).getFieldName6());
                                }%></td>
                </tr>
                 <%if(!pNature.equals("Services")){%>
                <tr>
                    <td class="ff">Lot No. and Description : </td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName12());
                                    if(!tendOpeningRpt.get(0).getFieldName12().equals("")){
                                        out.print(" & ");
                                    }
                                    out.print(tendOpeningRpt.get(0).getFieldName13());
                                }%></td>
                </tr>
                <%}%>
            </table>
            <div class="inner-tableHead t_space">Procurement Data</div>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>

                        <th width="47%">Procurement Type</th>
                        <th width="53%">Procurement Method</th>
                    </tr>
                    <tr>
                        <td class="t-align-center"><%if (list1) {
                                    if (tendOpeningRpt.get(0).getFieldName7().equalsIgnoreCase("NCT")) //Edit by aprojit
                                       out.print("NCB");
                                   else if (tendOpeningRpt.get(0).getFieldName7().equalsIgnoreCase("ICT"))
                                       out.print("ICB");
                                }%></td>
                       <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName9());
                                }%></td>
                    </tr>
                </tbody></table>

            <div class="inner-tableHead t_space">Procurement Plan</div>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>
                        <th width="17%">Approval<br>
                            Status</th>
                        <th width="31%">Budget Type</th>
                        <%if(!pNature.equalsIgnoreCase("Services")){%>
                        <th width="14%">Approval Status
                            of<br>
                            Official Estimates</th>
                        <%}%>
                    </tr>
                    <tr>
                        <td class="t-align-center">Approved</td>
                        <td class="t-align-center"><%if (list1) {
                                   if(tendOpeningRpt.get(0).getFieldName8().equalsIgnoreCase("Development")) //Edit by aprojit
                                       out.print("Capital");
                                   else if(tendOpeningRpt.get(0).getFieldName8().equalsIgnoreCase("Revenue"))
                                       out.print("Recurrent");
                                   else
                                       out.print(tendOpeningRpt.get(0).getFieldName8());

                                }%></td>
                        <%if(!pNature.equalsIgnoreCase("Services")){%>
                        <td class="t-align-center"><%=estCostStatus%></td>
                        <%}%>
                    </tr>
                </tbody></table>            
            <%
                if(pNature.equalsIgnoreCase("Services")){
                    EvalServiceCriteriaService criteriaService = (EvalServiceCriteriaService) AppContext.getSpringBean("EvalServiceCriteriaService");
                    List<Object[]> biddersName = criteriaService.getFinalSubBidders(tenderid);
                    Object minScore = criteriaService.getPassingMarksofTender(tenderid);
                    StringBuilder tfootData1 = new StringBuilder();
                    StringBuilder tfootData2 = new StringBuilder();
                    StringBuilder tfootData3 = new StringBuilder();
                    String cpuserId = null;
                    String cppuserId = null;
                    List<SPCommonSearchDataMore> memusersId = dataMoreService.geteGPDataMore("getMemUserIdMadeEval",tenderid);
                    if(memusersId!=null && !memusersId.isEmpty()){
                        cpuserId = memusersId.get(0).getFieldName1();
                        cppuserId = memusersId.get(0).getFieldName2();
                    }else{
                        cpuserId = session.getAttribute("userId").toString();
                        cppuserId = session.getAttribute("userId").toString();
                    }
            %>
            <div class="inner-tableHead t_space">Criteria</div>
            <table class="tableList_1" cellspacing="0" width="100%" id="marksTab">
                <tr>
                    <th class="t-align-center">Form Name</th>
                    <th class="t-align-center">Criteria</th>
                    <th class="t-align-center">Sub Criteria</th>
                    <th class="t-align-center">Points</th>
                    <%
                                for (Object[] bidder : biddersName) {
                                    String name = bidder[1].toString();
                                    if ("-".equals(name)) {
                                        name = bidder[2].toString() + " " + bidder[3].toString();
                                    }
                                    out.print("<th class='t-align-center'>" + name + "</th>");
                                }
                    %>
                    <!--                    -->
                </tr>
                <%
                             double mainTotal=0;
                            //0 esd.maxMarks ,1 esd.actualMarks,2 esd.ratingWeightage,3 esd.ratedScore,
                            //4 tf.tenderFormId,5 tf.formName,6 esc.subCriteria,7 esd.bidId
                            for (Object formId : criteriaService.getAllFormId(tenderid)) {
                                List<Object[]> list = criteriaService.evalServiceRepData(formId.toString(), cpuserId, tenderid,evalCount); //change by dohatec for re-evaluation
                               // double secCount=0;
                                for (Object[] data : list) {
                %>
                <tr>
                    <td><%
                               if(true || !("Work plan".equals(data[2].toString())||
                                        "Organization and Staffing".equals(data[2].toString()))){
                                   out.print(data[1]);
                               } else {
                                   out.print("");
                               }
                        %></td>
                    <td><%
                               if(true || !("Work plan".equals(data[2].toString())||
                                        "Organization and Staffing".equals(data[2].toString()))){
                                   out.print(data[5]);
                               } else {
                                   out.print("");
                               }
                        %></td>
                    <td><%out.print(data[2]);%></td>   
                    <td opt="hide"><%out.print(data[3]);mainTotal+=Double.parseDouble(data[3].toString());%></td>
                    <%
                        for (Object[] bidder : biddersName) {
                           out.print("<td>"+new BigDecimal(criteriaService.getEvalBidderMarks(tenderid, data[0].toString(), cppuserId, bidder[0].toString(),data[4].toString(),evalCount).toString()).setScale(2, 0)+"</td>"); //change by dohatec for re-evaluation
                        }
                    %>
                </tr>
                <%}
                            }
                            for (Object[] bidder : biddersName) {
                                  double marks = 0.0;
                                  for(Object data : criteriaService.getEvalBidderTotal(tenderid, cppuserId, bidder[0].toString(),evalCount)){ //change by dohatec for re-evaluation
                                     marks+=Double.parseDouble(data.toString());
                                  }
                                   tfootData1.append("<td class='formSubHead_1'>"+new BigDecimal(String.valueOf(marks)).setScale(2, 0)+"</td>");
                                   tfootData2.append("<td class='formSubHead_1'>"+minScore+"</td>");
                                   if(marks>=Double.parseDouble(minScore.toString())){
                                        tfootData3.append("<td class='formSubHead_1'>Pass</td>");
                                   }else{
                                       tfootData3.append("<td class='formSubHead_1 mandatory'>Fail</td>");
                                   }
                            }
                            out.print("<tr><td class='formSubHead_1'><td class='formSubHead_1'></td><td align='right' class='formSubHead_1'>Total : </td><td class='formSubHead_1'>"+mainTotal+"</td>"+tfootData1.toString()+"</tr>");
                            out.print("<tr><td class='formSubHead_1'></td><td class='formSubHead_1'></td><td align='right' class='formSubHead_1'>Minimum Passing Points : </td><td class='formSubHead_1'></td>"+tfootData2.toString()+"</tr>");
                            out.print("<tr><td class='formSubHead_1'></td><td class='formSubHead_1'></td><td align='right' class='formSubHead_1'>Status(Pass/Fail) : </td><td class='formSubHead_1'></td>"+tfootData3.toString()+"</tr>");
                %>
            </table>                        
            <%}else{
               Integer preference = 0;
               preference = Integer.valueOf(DomesticPreference.IsDomesticPreference(Integer.valueOf(tenderid)).toString());     
               if(preference >= 1)
                {
                 
                   List<SPTenderCommonData> listDomesticPre = tenderCommonService.returndata("getLowestBidderUsingDomesticPref", tenderid, null);
                    for (SPTenderCommonData listing : listDomesticPre) {
                        System.out.println("local>>>>"+listing.getFieldName1());
                         System.out.println("international>>>>"+listing.getFieldName2());
                         float international = Float.parseFloat(listing.getFieldName2().toString());
                       // Double international  = new Double(listing.getFieldName2().toString()).setScale(2,0);
                       //    BigDecimal percent  = new BigDecimal(listing.getFieldName3().toString()).setScale(2,0);
                         float percent = Float.parseFloat(listing.getFieldName3().toString());
                        
                           System.out.println("percent>>>>"+percent);



               %>
                
                <div class="inner-tableHead t_space">Domestic Preference</div> <a id="sfblock" href="javascript:void(0);" style="color: red;text-decoration: underline;">Domestic Preference Calculation</a><br>
                <table width="100%" cellspacing="0" class="tableList_1">
                        <thead>
                            <tr>
                                <th width="34%" class="t-align-center">Evaluation Price</th>
                                <th width="33%" class="t-align-left">Group A (Domestic)</th>
                                <th width="33%" class="t-align-left">Group B or C Tenders</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Quoted Amount</td>
                                <td><%=listing.getFieldName1().toString()%></td>
                                <td><%=new BigDecimal(international).setScale(2,0)%></td>
                            </tr>
                            <tr>
                                <td>Comparison</td>
                                <td><%=listing.getFieldName1().toString()%></td>
                                <td><%=new BigDecimal(international*percent).setScale(2,0)%></td>
                            </tr>
                            <tr>
                                <td>Final EVP</td>
                                <td><%=listing.getFieldName1().toString()%></td>
                                <td><%=new BigDecimal(international+(international*percent)).setScale(2,0)%></td>
                            </tr>
                        </tbody>
                    </table>

            <% }
               List<SPTenderCommonData> listDomesticPre1 = tenderCommonService.returndata("updateAfterDomesticPref", tenderid, null);
                    for (SPTenderCommonData listing : listDomesticPre1) {
                        if(listing.getFieldName1().equals("false"))
                            {%>
                            <script type="text/javascript">
                               alert("Please click Go Back and then click View and Sign again");
                            </script>
                           <%}}
               }
                        //else{


                    Object repId = creationService.findRepIdofTOR(tenderid, lotId,"TER");
                    if(repId!=null){
                    out.print("<div class='inner-tableHead t_space'>Price Evaluation</div>");
            %>
            <jsp:include page="../officer/TenderRepInclude.jsp">
                <jsp:param name="tenderid"  value="<%=tenderid%>"/>
                <jsp:param name="repId"  value="<%=repId%>"/>
                <jsp:param name="isEval"  value="true"/>
                <jsp:param name="istos"  value="true"/>
                <jsp:param name="rId"  value="<%=roundId%>"/>
            </jsp:include>
            <%}}%>
            <%if(isPQ){%>
            <div class="inner-tableHead t_space">Post Qualification</div>
            <table width="100%" cellspacing="0" class="tableList_3">
              <tr>
                <th width="84%">Name of Bidder/Consultant</th>
                <th width="16%">Status</th>
              </tr>              
              <%
                 for(SPCommonSearchDataMore bidder : TendererAwarded){
                     postQCount++;
              %>
              <tr>
                <td style="text-align: left;"><%=bidder.getFieldName1()%></td>
                <td class="t-align-center">
                  <% 
                    if("Qualify".equalsIgnoreCase(bidder.getFieldName2())){out.print("Qualified");}
                    else if("Disqualify".equalsIgnoreCase(bidder.getFieldName2())){out.print("Disqualified");}
                  %>
                </td>
              </tr>
              <%}if(TendererAwarded.isEmpty()){out.print("<tr><td colspan='2' style='color: red;' class='t-align-center'>No Records Found</td></tr>");}%>
            </table>
            <%}%>
            <%if(isNeg){%>
            <div class="inner-tableHead t_space">Negotiated Report</div>
            <table class="tableList_1" cellspacing="0" width="100%">
                <tr>
                    <th class="t-align-center" width="10%">Rank</th>
                    <th class="t-align-center" width="50%">Name of the Consultant/Applicant</th>
                    <th class="t-align-center" width="20%">Proposal Price</th>
                    <th class="t-align-center" width="20%">Negotiated Proposal Price</th>
                </tr>
                <%
                    //CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                    List<SPCommonSearchDataMore> negPriceList = dataMoreService.geteGPDataMore("getNegPriceRpt",tenderid,roundId,String.valueOf(evalCount)); //change by dohatec for re-evaluation

                    String negUserid=null;
                    if(tenderid != null)
                    {
                        Object[] objects = negotiationProcessSrBean.getNegoTenderInfoFromTenderId(Integer.parseInt(tenderid));
                        if(objects != null && objects.length >= 1){
                            negUserid = objects[1].toString();
                        }
                    }
                    /*for(SPCommonSearchDataMore negPrice : negPriceList)
                    {
                        out.print("<tr>");
                            out.print("<td class='t-align-center'>"+negPrice.getFieldName1()+"</td>");
                            out.print("<td>"+negPrice.getFieldName2()+"</td>");
                            out.print("<td>"+negPrice.getFieldName3()+"</td>");
                            long sum = 0;
                            if(negUserid!= null && negPrice.getFieldName5() != null && negUserid.equalsIgnoreCase(negPrice.getFieldName5()))
                            {
                                for (SPCommonSearchData details : commonSearchService.searchData("getGrandSumNego", tenderid, negPrice.getFieldName5(), "0", null, null, null, null, null, null))
                                {
                                    if(details.getFieldName2() != null && !details.getFieldName2().equalsIgnoreCase("") && !details.getFieldName2().equalsIgnoreCase("null"))
                                    {
                                         sum = sum+Long.parseLong(details.getFieldName2());
                                    }
                                }
                                out.print("<td>"+sum+"</td>");
                            }
                            else
                                out.print("<td>-</td>");
                        out.print("</tr>");
                    }
                    */
                    for(SPCommonSearchDataMore negPrice : negPriceList){
                        out.print("<tr>");
                            out.print("<td class='t-align-center'>"+negPrice.getFieldName1()+"</td>");
                            out.print("<td>"+negPrice.getFieldName2()+"</td>");
                            out.print("<td>"+new BigDecimal(negPrice.getFieldName3()).setScale(2,0)+"</td>");
                            out.print("<td>"+new BigDecimal(negPrice.getFieldName4()).setScale(2,0)+"</td>");
                        out.print("</tr>");
                    }
                %>
            </table>
            <div class="inner-tableHead t_space">Negotiation</div>
            <table class="tableList_1" cellspacing="0" width="100%">
                <tr>
                    <th class="t-align-center" width="10%">Rank</th>
                    <th class="t-align-center" width="50%">Name of the Consultant/Applicant</th>
                    <th class="t-align-center" width="20%">Status</th>
                    <th class="t-align-center" width="20%">Report</th>
                </tr>
                <%
                    List<SPCommonSearchDataMore> negBidderList = dataMoreService.geteGPDataMore("BiddersNegList",tenderid,String.valueOf(evalCount)); //change by dohatec for re-evaluation
                    int negBidCnt = 1;
                    for(SPCommonSearchDataMore negBidder : negBidderList){
                        out.print("<tr>");
                            out.print("<td class='t-align-center'>"+negBidCnt+"</td>");
                            out.print("<td>"+negBidder.getFieldName2()+"</td>");
                            out.print("<td class='t-align-center'>"+negBidder.getFieldName3()+"</td>");
                               if(bole_ter_flag){
                                    out.print("<td class='t-align-center'><a target='_blank' href='NegotiationReport.jsp?tenderid="+tenderid+"&ReportFrom=TER3&lotId="+lotId+"&rId="+roundId+"&negId="+negBidder.getFieldName1()+"&uId="+negBidder.getFieldName4()+"'>View</a></td>");
                                }else{
                                    out.print("<td>-</td>");
                                }

                        out.print("</tr>");
                        negBidCnt++;
                    }
                    if(negBidderList.isEmpty()){
                        out.print("<tr><td colspan='4' class='ff mandatory t-align-center'>No Records found</td></tr>");
                    }
                %>
            </table>
            <%}%>
            <%if(NoteofDissent!=null && (!NoteofDissent.isEmpty())){%>
            <div class="inner-tableHead t_space">Note of Dissent:</div>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <th><%=repLabel.charAt(0)%>EC Member</th>
                    <th>Note of Dissent</th>
                </tr>
                <%for(SPCommonSearchDataMore nod : NoteofDissent){%>
                <tr>
                    <td><%=nod.getFieldName1()%></td>
                    <td><%=nod.getFieldName2()%></td>
                </tr>
                <%}%>
            </table>
            <%}%>
            <div class="c_t_space atxt_1 mandatory">
<!--                I do hereby declare and confirm that I have no business or other links to any of the competing Tenderers.-->
                <br/>

                The Evaluation Committee certifies that the examination and evaluation has followed the requirements of the Act, the Rules made there under and the terms and conditions of the prescribed Application, Tender or Proposal Document and that all facts and information have been correctly reflected in the Evaluation Report and, that no substantial or important information has been omitted. </div>

            <div class="inner-tableHead t_space"><%=repLabel.charAt(0)%>EC Members</div>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <td class="tableHead_1" colspan="5" ><%=repLabel.charAt(0)%>EC Members</td>
                </tr>
                <tr>
                    <th width="20%" >Name</th>
                    <%
                    for (SPCommonSearchDataMore dataMore : TECMembers) {
                        out.print("<td>");
                        if(dataMore.getFieldName5().equals(userId)){
                         if(dataMore.getFieldName6().equals("-")){
                             String valChk="";
                             if(!isCommCP){
                                valChk = "&nDis=y";
                             }
                   %>
                <a href="TORSigningApp.jsp?id=<%=dataMore.getFieldName7()%>&uid=<%=dataMore.getFieldName5()%>&tid=<%=tenderid%>&lotId=<%=lotId%>&stat=<%=stat%>&rpt=ter3<%=valChk%>&rId=<%=roundId%>"                   
                   <%
                       if(isPQ && postQCount==0){
                           out.print(" onclick='return signPQCheck();'");
                       }
                       if(isNeg){
                           List<SPCommonSearchDataMore> negList =  dataMoreService.geteGPDataMore("isNegCompleted",roundId);
                           if(!(negList!=null && (!negList.isEmpty()) && negList.get(0).getFieldName1().equals("1"))){
                               out.print(" onclick='return signNegCheck();'");
                           }
                       }
                   %>>
                        <%=dataMore.getFieldName1()%>
                    </a>
                   <%
                        }else{
                             out.print(dataMore.getFieldName1());
                        }

                    }else{
                        out.print(dataMore.getFieldName1());
                    }
                        out.print("</td>");
                    }
                  %>
               </tr>
                <tr>
                    <th width="20%">Committee Role</th>
                    <%
                    int svpdfter3 = 0;
                    for (SPCommonSearchDataMore dataMore : TECMembers) {
                        out.print("<td>");
                            if (dataMore.getFieldName2().equals("cp")) {
                            out.print("Chairperson");
                        } else if (dataMore.getFieldName2().equals("ms")) {
                            out.print("Member Secretary");
                        } else if (dataMore.getFieldName2().equals("m")) {
                            out.print("Member");
                            }
                        out.print("</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Designation</th>
                    <%for (SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName3()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">PE Office</th>
                    <%for (SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName4()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Signed <%=repLabel%> Evaluation Report 3 On</th>
                    <%for(SPCommonSearchDataMore dataMore : TECMembers) {if(dataMore.getFieldName6().equals("-")){svpdfter3++;}out.print("<td>"+dataMore.getFieldName6()+"</td>");}%>
                </tr>
                 <tr>
                    <th width="20%">Comments</th>
                    <%for(SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName8()+"</td>");}%>
                </tr>
            </table>
                <%if(svpdfter3!=0){%>
                    <script type="text/javascript">
                        $('#svpdftor1').hide();
                    </script>
                <%}%>
                <input type="hidden" value="<%=tenderid%>" name="tId">
                <input type="hidden" value="<%=lotId%>" name="lId">
                <input type="hidden" value="<%=stat%>" name="stat">
                <input type="hidden" value="TER2" name="rType">                
            <!--</form>-->
            </div>
            <%if(pNature.equals("Services") && !isREOI){%>
            <script type="text/javascript">
                $('#marksTab td[opt="hide"]').each(function() {
                    var ctd = this.innerHTML;
                    if(ctd=='0'){
                        $(this).parent().hide();
                    }
                });
            </script>
            <%}%>
        </div>
         <%
       // }}
        %>
        <div id ="dialog-form" title="View Domestic Preferecne Formula">
            <fieldset>
                        <table width="100%" cellspacing="0" border="1" class="tableList_1">
                             <tr><td style="color:Red;"> Domestic preference calculated on the basis following formula :-
                                <ol style="padding-left:25px;">
                                        <li style="margin:2px;">EXW price per Line Item= Quantity*(Unit price EXW- VAT for Goods manufactured in Bhutan
                                                                                                 [Nu.])
                                        </li>
                                        <li style="margin:2px;">Quoted Price (Without Domestic Preference) = (EXW price per Line Item +

                                 Inland transportation, Insurance and other local costs for the delivery of the Goods to their final destination [Nu.])
                                        </li>
                                </ol>
                               </td>
                            </tr>
                        </table>
            </fieldset>


        </div>
        <%if(bole_ter_flag){%>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <%}%>
    </body>
   
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <script type="text/javascript">
    $(document).ready(function() {

        $("#print").click(function() {
            //alert('sa');
            printElem({ leaveOpen: true, printMode: 'popup' });
        });

    });
    function printElem(options){
        //alert(options);
        $('#print').hide();
        $('#saveASPDF').hide();
        $('#goBack').hide();
        $('#print_area').printElement(options);
        $('#print').show();
        $('#saveASPDF').show();
        $('#goBack').show();
        //$('#trLast').hide();
    }
    </script>
</html>
