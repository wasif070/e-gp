<%-- 
    Document   : AccPaymentView
    Created on : Aug 11, 2011, 11:57:27 AM
    Author     : dixit
--%>

<%@page import="java.util.Collections"%>
<%@page import="com.cptu.egp.eps.web.utility.MonthName"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceMaster"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.math.BigInteger"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceDocument"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsInvoiceAccDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AccPaymentService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Accounts Officer Payments View Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/print/jquery.txt"></script>
        <script src="../resources/js/jQuery/jquery.print.blocklink.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
        </head>
        <script>
            function SendToTen(wpId,tenderId,lotId,invId){
                 jConfirm("Are you sure you have been authorized to send directly to the Bidder/Consultant directly?","Invoice", function(RetVal){
                    if(RetVal)
                    {
                      dynamicFromSubmit("<%=request.getContextPath()%>/AccPaymentServlet?tenderId="+tenderId+"&InvoiceId="+invId+"&wpId="+wpId+"&lotId="+lotId+"&action=sendtotenderer");
                    }
                });
           }
            function SendToTenForICT(wpId,tenderId,lotId,invId,invoiceNo){
            //var invoiceNo = "Invoice1";
                 jConfirm("Are you sure you have been authorized to send directly to the Bidder/Consultant directly?","Invoice", function(RetVal){
                    if(RetVal)
                    {
                      dynamicFromSubmit("<%=request.getContextPath()%>/AccPaymentServlet?tenderId="+tenderId+"&InvoiceId="+invId+"&wpId="+wpId+"&lotId="+lotId+"&invoiceNo="+invoiceNo+"&action=sendtotenderer");
                    }
                });
           }
        </script>

    <%
        ResourceBundle bdl = null;
                bdl = ResourceBundle.getBundle("properties.cmsproperty");
    %>
    <body onload="AmtInWords();">
        <%

            String tenderId = "";
            int totalCur = 0;
           
            BigDecimal bigadvadjAmt = BigDecimal.ZERO;
            double dbtotalRDeducted = 0.0;
           

            List<Object[]> totalRDeducted = null;
            
            if (request.getParameter("tenderId") != null) {
                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                tenderId = request.getParameter("tenderId");
            }
            String userId = "";
            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                userId = session.getAttribute("userId").toString();
            }
            String InvoiceId = "";
             String InvoiceNo = "";
            //if(request.getParameter("invoiceId")!=null)
           // {
           //     InvoiceId = request.getParameter("invoiceId");
          //  }
            String wpId = "";
            if(request.getParameter("wpId")!=null)
            {
                wpId = request.getParameter("wpId");
                pageContext.setAttribute("wpId", request.getParameter("wpId"));
            }
            String lotId = "";
            if(request.getParameter("lotId")!=null)
            {
                lotId = request.getParameter("lotId");
                pageContext.setAttribute("lotId", request.getParameter("lotId"));
            }
            pageContext.setAttribute("tab", "14");
            ConsolodateService cs = (ConsolodateService)AppContext.getSpringBean("ConsolodateService");
            TenderTablesSrBean beanCommon1 = new TenderTablesSrBean();
            String tenderType1 = beanCommon1.getTenderType(Integer.parseInt(request.getParameter("tenderId")));
            List<Object> objInvoiceId = null;
           
            if(!tenderType1.equals("ICT"))
            {
                if(request.getParameter("InvoiceId") == null)
                    InvoiceId = request.getParameter("invoiceId");
                else
                    InvoiceId = request.getParameter("InvoiceId");
                pageContext.setAttribute("invoiceId", InvoiceId);
            }
            else
            {
                pageContext.setAttribute("invoiceNo", request.getParameter("invoiceNo"));
                objInvoiceId = cs.getInvoiceIdForICT(request.getParameter("invoiceNo"), Integer.parseInt(wpId));
                InvoiceId = objInvoiceId.get(0).toString();
                pageContext.setAttribute("invoiceId", objInvoiceId.get(0).toString());
                InvoiceNo  = request.getParameter("invoiceNo");
            }

            System.out.println(InvoiceId);
          //  pageContext.setAttribute("invoiceId", InvoiceId);

            CommonService commonSer = (CommonService) AppContext.getSpringBean("CommonService");
            String procnature = commonSer.getProcNature(request.getParameter("tenderId")).toString();
            String strProcNature = "";
            if("Services".equalsIgnoreCase(procnature))
            {
                    strProcNature = "Consultant";
            }else if("goods".equalsIgnoreCase(procnature)){
                    strProcNature = "Supplier";
            }else{
                    strProcNature = "Contractor";
            }

            AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
            List<TblCmsInvoiceAccDetails> invoicedetails = accPaymentService.getInvoiceAccDetails(Integer.parseInt(InvoiceId));
            boolean isEdit = false;
            if(!invoicedetails.isEmpty())
            {
                isEdit = true;
            }            
            boolean HideActionScenario = true;
            List<Object> invstatusObject = accPaymentService.getInvoiceStatus(Integer.parseInt(InvoiceId));
            if(!invstatusObject.isEmpty())
            {                
                if("sendtope".equalsIgnoreCase(invstatusObject.get(0).toString()) || "sendtotenderer".equalsIgnoreCase(invstatusObject.get(0).toString()))
                {
                    HideActionScenario = false;
                }
            }
       %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div  id="print_area">
            <div class="contentArea_1">
                <div class="pageHead_1"><%=bdl.getString("CMS.Inv.ViewPage.Title")%>
                <span style="float: right; text-align: right;" class="noprint">
                <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                <a class="action-button-goback" href="MakePayment.jsp" title="STD Dashboard">Go Back</a>
                </span>
                </div>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <%@include file="../resources/common/ContractInfoBar.jsp"%>
                <%
                String message="";
                double strVatPlusAit[] = new double[4];
                double strAdvVatPlusAdvAit[] = new double[4];
                double netVatandAitStr[] = new double[4];

                if(request.getParameter("msg")!=null)
                {
                    message = request.getParameter("msg");
                }
                if("succ".equalsIgnoreCase(message))
                {
                %>
                <div class="t_space"><div class="responseMsg successMsg t_space"><span><%=bdl.getString("CMS.Inv.invSave")%></span></div></div>
                <%}else if("updated".equalsIgnoreCase(message)){%>
                <div class="t_space"><div class="responseMsg successMsg t_space"><span><%=bdl.getString("CMS.Inv.invUpdate")%></span></div></div>
                <%}else if("sendtope".equalsIgnoreCase(message)){%>
                <div class="t_space"><div class="responseMsg errorMsg t_space"><span><%=bdl.getString("CMS.Inv.Sendtope.fail")%></span></div></div>
                <%}else if("sendtotenderer".equalsIgnoreCase(message)){%>
                <div class="t_space"><div class="responseMsg errorMsg t_space"><span><%=bdl.getString("CMS.Inv.SendtoSupplier.fail")%></span></div></div>
                <%}%>
                <div>&nbsp;</div>

                <%--<%@include  file="officerTabPanel.jsp"%>--%>
                    <div class="tabPanelArea_1">
                    <%
                        pageContext.setAttribute("TSCtab", "3");
                    %>
                    <%--<%@include  file="../resources/common/CMSTab.jsp"%>--%>
                    <div class="tabPanelArea_1">
                    
                    <div align="center">                        
                      <form action='' method="post" name="AccPaymentfrm">
                           <%
                               CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");         
                               CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                               String conType = commService.getServiceTypeForTender(Integer.parseInt(request.getParameter("tenderId")));
                               String procCase = commService.getProcNature(request.getParameter("tenderId")).toString();  
                              // ConsolodateService cs = (ConsolodateService)AppContext.getSpringBean("ConsolodateService");
                               List<TblCmsInvoiceMaster> InvMasdata = cs.getTblCmsInvoiceMaster(Integer.parseInt(InvoiceId));
                               if(!"services".equalsIgnoreCase(procCase)){
                               if(!InvMasdata.isEmpty())
                               {
                                   if("yes".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                                   {
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                               <%
                               List<Object[]> invIdData = null;
                                if(tenderType1.equals("ICT")){
                                invIdData = cs.getInvoiceTotalAmtForICT(Integer.parseInt(InvoiceId),Integer.parseInt(request.getParameter("wpId")));
                                if (!invIdData.isEmpty() && invIdData != null) {

                                for(int k=0;k<invIdData.size(); k++)
                                {

                                %>
                                <tr>
                                    <td class="ff" width="88%">Advance Amount (In <%=invIdData.get(k)[1]%>)</td>
                                    <td width="12%"><%=invIdData.get(k)[0]%></td>
                                </tr>
                                <%}}}else{%>
                                <tr>
                                    <td class="ff" width="88%">Advance Amount <%=bdl.getString("CMS.Inv.InBDT")%></td>
                                    <td width="12%"><%=InvMasdata.get(0).getTotalInvAmt()%></td>
                                </tr>
                                <% } %>
                            </table>
                            <%}else{%>
                            <%@include  file="../resources/common/CommonViewInvoice.jsp"%>
                            <%}}}else{
                                if("yes".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                                   {
                                %>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <td class="ff" width="88%">Advance Amount <%=bdl.getString("CMS.Inv.InBDT")%></td>
                                            <td width="12%"><%=InvMasdata.get(0).getTotalInvAmt()%></td>
                                        </tr>
                                    </table>
                                 <%}else{
                                   if("time based".equalsIgnoreCase(conType)){
                             %>
                                            <%
                                                List<Object[]> AttSheetslist = cmss.ViewAttSheetDtls(InvMasdata.get(0).getTillPrid());
                                                String weeks_days[] = new String[7];
                                                MonthName monthName = new MonthName();
                                                BigDecimal TotalAmount = new BigDecimal(0);
                                                if(AttSheetslist!=null && !AttSheetslist.isEmpty()){
                                            %>
                                            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                            <tr>
                                                <th width="3%" class="t-align-center">Sr.No</th>
                                                <th width="20%" class="t-align-center">Name of Employees</th>
                                                <th width="15%" class="t-align-center">Position Assigned</th>
                                                <th width="9%" class="t-align-center">Home / Field</th>
                                                <th width="5%" class="t-align-center">No of Days</th>
                                                <th width="5%" class="t-align-center">Rate</th>
                                                <th width="18%" class="t-align-center">Month</th>
                                                <th width="18%" class="t-align-center">Year</th>
                                                <th width="18%" class="t-align-center">Invoice Amount</th>
                                            </tr>
                                            <%
                                                for(Object obj[] : AttSheetslist){
                                                TotalAmount = TotalAmount.add((BigDecimal)obj[10]) ;
                                            %>
                                            <tr>
                                                <td width="3%" class="t-align-center"><%=obj[0]%></td>
                                                <td width="20%" class="t-align-center"><%=obj[1]%></td>
                                                <td width="15%" class="t-align-center"><%=obj[2]%></td>
                                                <td width="9%" class="t-align-center"><%=obj[7]%></td>
                                                <td width="5%" class="t-align-center"><%=obj[3]%></td>
                                                <td width="5%" class="t-align-center"><%=obj[4]%></td>
                                                <td width="5%" class="t-align-center"><%=monthName.getMonth((Integer) obj[5]) %>
                                                <td width="5%" class="t-align-center"><%=obj[6]%></td>
                                                <td width="5%" class="t-align-center"><%=obj[10]%></td>
                                           </tr>
                                               <% }}%>
                                           <tr><td class="t-align-center" colspan=7></td>
                                               <td class="t-align-center ff">Total Amount (In Nu.)</td>
                                               <td class="t-align-right ff" style="text-align: right;"><%=TotalAmount%></td>
                                           </tr>
                                            </table>
                              <%@include  file="../resources/common/CommonViewInvoice.jsp"%>
                              <%}else{%>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                    <th>S No</th>                                    
                                    <th>Milestone Name </th>                                
                                    <th>Description</th>                                
                                    <th>Payment as % of Contract Value</th>                                
                                    <th>Mile Stone Date proposed by PE </th>
                                    <th>Mile Stone Date proposed by Consultant</th>
                                    <th>Invoice Amount <br/><%=bdl.getString("CMS.Inv.InBDT")%></th>                                                              
                                    <%                                                                                      
                                        List<TblCmsSrvPaymentSch> listPaymentData = cmss.getPaymentScheduleDatabySrvPSId(InvMasdata.get(0).getTillPrid());
                                        if(!listPaymentData.isEmpty()){                                        
                                    %>
                                    <tr>
                                        <td class="t-align-center"><%=listPaymentData.get(0).getSrNo()%></td>                                    
                                        <td class="t-align-center"><%=listPaymentData.get(0).getMilestone()%></td>                                    
                                        <td class="t-align-center"><%=listPaymentData.get(0).getDescription()%></td>                                    
                                        <td class="t-align-center"><%=listPaymentData.get(0).getPercentOfCtrVal()%></td>                                    
                                        <td class="t-align-center"><%=DateUtils.customDateFormate(listPaymentData.get(0).getPeenddate())%></td>
                                        <td class="t-align-center"><%=DateUtils.customDateFormate(listPaymentData.get(0).getEndDate())%></td> 
                                        <td><%=new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*listPaymentData.get(0).getPercentOfCtrVal().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)%>
                                    </tr> 
                                </table>
                                <%}}}}
                                %>    
                          <table cellspacing="0" class="tableList_1 t_space" width="100%">

                          <%
                            List<Object[]> totalAmt = null;
                                        ConsolodateService getTotalAmt = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                      //  TenderTablesSrBean beanCommon1 = new TenderTablesSrBean();
                                       // String tenderType1 = beanCommon1.getTenderType(Integer.parseInt(request.getParameter("tenderId")));

                                        if(tenderType1.equals("ICT"))
                                            //totalAmt =  getTotalAmt.getInvoiceTotalAmtForICT(Integer.parseInt(InvoiceId),Integer.parseInt(request.getParameter("wpId")));
                                            totalAmt = getTotalAmt.returndata("getInvoiceTotalAmtForICT",InvoiceId, request.getParameter("wpId"));
                                        else
                                            if("no".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()))
                                                totalAmt =  getTotalAmt.getInvoiceTotalAmt(InvoiceId);
                                            else
                                                totalAmt =  getTotalAmt. getInvoiceTotalAmtAdv(InvoiceId);
                                        String totalAmount = "";
                                        String[] curArr = new String[totalAmt.size()];
                                        String[] totalArr = new String[totalAmt.size()];
                                        String[] totalInvoiceId = new String[totalAmt.size()];
                                        BigDecimal[] invoiceAmount = new BigDecimal[totalAmt.size()];
                                        BigDecimal[] grossAmount = new BigDecimal[totalAmt.size()];
                                        String[] cntrAmnt = new String[totalAmt.size()];
                                      
                                        int incr = 0;
                                        totalCur = totalAmt.size();
                                        boolean bdtflag = false;

                                         if("yes".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv()) && tenderType1.equals("NCT"))
                                         {
                                          curArr[incr] = "BTN";
                                          totalInvoiceId[incr] = InvoiceId.toString();
                                          cntrAmnt[incr] =  InvMasdata.get(0).getTotalInvAmt().toString();
                                          incr = incr +1;
                                         }
                                        else{
                                        for(Object[] oob:totalAmt)
                                            {
                                            if(tenderType1.equals("ICT"))
                                            {
                                                 if(oob[1].toString().equals("BTN")  && bdtflag == false)
                                                {
                                                curArr[incr] = oob[1].toString();
                                                totalArr[incr] = oob[0].toString();
                                                totalInvoiceId[incr] = oob[2].toString();
                                                cntrAmnt[incr] =  oob[3].toString();
                                                 incr = incr +1;
                                                 bdtflag = true;
                                                }
                                                else if (!oob[1].toString().equals("BTN")){
                                                curArr[incr] = oob[1].toString();
                                                totalArr[incr] = oob[0].toString();
                                                totalInvoiceId[incr] = oob[2].toString();
                                                cntrAmnt[incr] =  oob[3].toString();
                                                    incr = incr +1;
                                                }
                                                else
                                                    {continue;}
                                            }
                                            else
                                                {
                                                curArr[incr] = "BTN";
                                                totalInvoiceId[incr] = oob[1].toString();
                                                cntrAmnt[incr] =  c_obj[2].toString();
                                                incr = incr +1;
                                                }
                                            }
                                        }
                                          totalCur = incr;
                                         
                                          for(int incr2=0;incr2<incr;incr2++){      
                                             %>
                                             <tr>

                                              <td class="ff" width="25%" class="t-align-left">Invoice amount (In <%=curArr[incr2].toString()%>)</td>
                                              <td class="t-align-left">
                                                                                 
                                    <% if(isEdit){
                                        invoicedetails = accPaymentService.getInvoiceAccDetails(Integer.parseInt(totalInvoiceId[incr2].toString()));
                                        out.print(invoicedetails.get(0).getInvoiceAmt());
                                        invoiceAmount[incr2] = invoicedetails.get(0).getInvoiceAmt();
                                        grossAmount[incr2] = invoicedetails.get(0).getGrossAmt();
                                     //   invoiceDetailArr[0][incr] = invoicedetails.get(0).getAdvAdjAmt().toString();//Advance Amount of Contract Value :
                                     //   invoiceDetailArr[1][incr] =
                                     //   invoiceDetailArr[2][incr] =
                                    //    invoiceDetailArr[3][incr] =
                                    //    invoiceDetailArr[4][incr] =
                                      }
                                      //else{
                                      //          out.print(oob[0].toString());
                                     //   }

                                    %>
                                <input type="hidden" id="invoiceAmtid<%=incr2%>" <%if(isEdit){%>value="<%=invoicedetails.get(0).getInvoiceAmt()%>"<%}%>>
                                 &nbsp;&nbsp;&nbsp;<span name ="amtWords" id="amtinWords<%=incr2%>" class="ff"></span>
                                </td> <% /*incr ++ ;*/}  %>
                               <input type="hidden" name="totalCurrency" id="totalCurrency" value="<%=totalCur%>">
                            </tr>
                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.deduction")%></div>
                        <table cellspacing="0" class="tableList_1" width="100%">
                            <%
                           // for(int incrDe = 0;incrDe<totalCur;incrDe++){
                           // invoicedetails = accPaymentService.getInvoiceAccDetails(Integer.parseInt(totalInvoiceId[incrDe]));

                            %>
                            <tr><td class="ff" width="25%">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="ff" width="25%">Total Advance Adjustment till Date :</td>
                                <td>
                                    <%
                                        int ti = 0;
                                        List<Object[]> objCurBefore = Collections.EMPTY_LIST;
                                        List<Object[]> objAdjustmentBefore = null;
                                       Object[] obj = null;
                                       BigDecimal salvageAdjustmentTillDate=BigDecimal.ZERO.setScale(3,0);
                                       // String curForAdjustment[] =
                                        String[] curForAdjustment = new String[4];
                                         String[] amountForAdjustment = new String[4];
                                        totalRDeducted = accPaymentService.getAllTotalInvoiceAmtForWp(Integer.parseInt(wpId));
                                        double inttotalRDeducted = 0.0;
                                        if(!totalRDeducted.isEmpty())
                                        {
                                            obj = totalRDeducted.get(0);
                                            if(obj[1]==null)
                                            {
                                                inttotalRDeducted = new BigDecimal(0).setScale(3,BigDecimal.ROUND_HALF_UP).doubleValue();
                                            }
                                            else{
                                                inttotalRDeducted = new BigDecimal(obj[1].toString()).setScale(3,BigDecimal.ROUND_HALF_UP).doubleValue();
                                                salvageAdjustmentTillDate=new BigDecimal(obj[14].toString()).setScale(3,0);
                                            }
                                        }
                                        double strAdvanceAmount = 0;
                                        String AdvanceAmount = accPaymentService.getAdvanceContractAmount(Integer.parseInt(tenderId),Integer.parseInt(lotId));
                                         String salvageAmountStr = accPaymentService.getSalavageContractAmount(Integer.parseInt(tenderId), Integer.parseInt(lotId));

                                        BigDecimal salvageAmount=new BigDecimal(salvageAmountStr).setScale(3,0);
                                        if(!"".equalsIgnoreCase(AdvanceAmount))
                                        {   strAdvanceAmount = new BigDecimal(AdvanceAmount.toString()).doubleValue();
                                        }else{strAdvanceAmount = new BigDecimal(0).doubleValue();}
                                        if(tenderType1.equals("ICT"))
                                            {
                                              objCurBefore = accPaymentService.getCurrencyInAccGenerateBefore(Integer.parseInt(wpId),Integer.parseInt(tenderId));
                                              System.out.print("objCurBefore.size()"+objCurBefore.size());
                                             if(!objCurBefore.isEmpty() && obj[1]!=null)
                                                        {

                                                        for(int i=0;i<objCurBefore.size();i++){
                                                            for(int j=0;j<totalCur;j++)
                                                                {
                                                                if(curArr[j].equals(objCurBefore.get(i)[1].toString()))
                                                                    continue;
                                                                else if (j+1 == totalCur){
                                                                    curForAdjustment[ti] = objCurBefore.get(i)[1].toString();
                                                                    amountForAdjustment[ti] = objCurBefore.get(i)[0].toString();
                                                                    ti++;
                                                                    }

                                                                }
                                                            }
                                                         }
                                            }
                                    %>
                                    <table width="100%" style="border:none;">
                                        <tr><% if(!tenderType1.equals("ICT") && "no".equalsIgnoreCase(InvMasdata.get(0).getIsAdvInv())){ %>
                                            <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(inttotalRDeducted).setScale(3,BigDecimal.ROUND_HALF_UP)%></td>
                                            <td style="text-align: right;border:none;padding:0px;width:110px;">
                                            <%
                                            for(int j=0;j<totalCur;j++){
                                            bigadvadjAmt= new BigDecimal((new BigDecimal(cntrAmnt[j].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,BigDecimal.ROUND_HALF_UP);
                                            %>
                                                <%=new BigDecimal((bigadvadjAmt.doubleValue()*inttotalRDeducted)/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+")"%>
                                            <%}%> </td></tr>
                                            
                                             <%}else if(tenderType1.equals("ICT")){
                                            for(int j=0;j<totalCur;j++){
                                                String AdjustmentBefore = "0";
                                                double advAdj = 0;
                                                 if(!objCurBefore.isEmpty() && obj[1]!=null){
                                                objAdjustmentBefore = getTotalAmt.returndata("getAdjustAmountGenerateBefore",wpId, curArr[j]);
                                                AdjustmentBefore = objAdjustmentBefore.get(0)[0].toString();

                                                //   objAdjustmentBefore = accPaymentService.getAdjustAmountGenerateBefore(Integer.parseInt(wpId),curArr[j]);
                                                advAdj = (Double.valueOf(cntrAmnt[j])*Double.valueOf(AdvanceAmount))/100;
                                                }
                                             %>
                                              <tr><td style="text-align: left;border:none;padding:0px;"><%=AdjustmentBefore%></td>
                                            <td style="text-align: right;border:none;padding:0px;width:110px;"><%=new BigDecimal((advAdj * Double.valueOf(AdjustmentBefore))/100).setScale(3,BigDecimal.ROUND_HALF_UP)%> &nbsp;(In  <%=curArr[j]%>)</td>
                                             <%}%> </tr>
                                          <%   if(!objCurBefore.isEmpty() && obj[1]!=null)
                                             {
                                              for(int i=0;i<ti;i++)
                                                  {
                                                    objAdjustmentBefore = getTotalAmt.returndata("getAdjustAmountGenerateBefore",wpId, curForAdjustment[i]);
                                                    double advAdj = (Double.valueOf(amountForAdjustment[i])*Double.valueOf(AdvanceAmount))/100;
                                                   %>
                                        <tr><td style="text-align: left;border:none;padding:0px;"><%=objAdjustmentBefore.get(0)[0].toString()%></td>
                                            <td style="text-align: right;border:none;padding:0px;width:110px;"><%=new BigDecimal((advAdj * Double.valueOf(objAdjustmentBefore.get(0)[0].toString()))/100).setScale(3,BigDecimal.ROUND_HALF_UP)%> &nbsp;(In  <%=curForAdjustment[i]%>)</td>

                                             <% }%> </tr> <% }}  %>
                                            
                                       
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advAmount")%></td>
                                <td   class="t-align-left">
                                <%
                                    //double strAdvanceAmount = 0;
                                    //String AdvanceAmount = accPaymentService.getAdvanceContractAmount(Integer.parseInt(tenderId),Integer.parseInt(lotId));
                                    //if(!"".equalsIgnoreCase(AdvanceAmount))
                                    //{   strAdvanceAmount = new BigDecimal(AdvanceAmount.toString()).doubleValue();
                                    //}else{strAdvanceAmount = new BigDecimal(0).doubleValue();}
                                    //out.print(new BigDecimal(strAdvanceAmount).setScale(3,0));
                                    //out.print("<span style='padding-left:107px;'>");
                                    //out.print(new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,0)+ " (In BTN)");
                                    //out.print("</span>");
                                    
                                %>
                                    <table width="100%" style="border:none;">
                                        <tr>
                                             <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(strAdvanceAmount).setScale(3,BigDecimal.ROUND_HALF_UP)%></td>
                                             <td style="text-align: right;border:none;padding:0px;width:110px;">
                                             <%if(!tenderType1.equals("ICT")){ %>
                                             <%=new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" "+bdl.getString("CMS.Inv.InBDT")%>
                                             <%
                                            }else{  
                                                for(int j=0;j<totalCur;j++){%>
                                              <%=new BigDecimal((new BigDecimal(cntrAmnt[j].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+")"%>
                                            <%}}%>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjAmount")%></td>
                                <td class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                        <tr>
                                            <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getAdvAdjAmt()%><%}%></td>
                                            <td style="text-align: right;border:none;padding:0px;width:110px;">
                                                
                                                
                                                <%if(isEdit){
                                                     if(!tenderType1.equals("ICT")){ %>
                                                     <%=new BigDecimal((bigadvadjAmt.doubleValue()* invoicedetails.get(0).getAdvAdjAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In BTN)"%>
                                                    <%}else{ for(int j=0;j<totalCur;j++)
                                                    {
                                                    BigDecimal bigadvadjAmtt = BigDecimal.ZERO;
                                                    bigadvadjAmtt= new BigDecimal((new BigDecimal(cntrAmnt[j].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,BigDecimal.ROUND_HALF_UP);
                                       
                                                    %><%=new BigDecimal((bigadvadjAmtt.doubleValue()* invoicedetails.get(0).getAdvAdjAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+")"%>
                                                <%}}}%>
                                                          
                                            </td>
                                        </tr>
                                    </table>
                                    <%--<%if(isEdit){%> <%=invoicedetails.get(0).getAdvAdjAmt()%><%}%>
                                    <span style="padding-left:95px;"><%if(isEdit){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getAdvAdjAmt().doubleValue())/100).setScale(3,0));}%> (In BTN)</span>--%>
                                </td>                                
                            </tr>
                               <% if(salvageAmount.compareTo(BigDecimal.ZERO)>0 && !tenderType1.equals("ICT") ){ %>
                             <tr>
                                 <td class="ff" width="25%">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td class="ff" width="25%">Total Salvage Adjustment till Date :</td>
                                <td>
                                     <table width="100%" style="border:none;">
                                     <tr>
                                   <td style="text-align: left;border:none;padding:0px;"> <%=salvageAdjustmentTillDate %> </td>
                                   <td style="text-align: right;border:none;padding:0px;"><span id="SalvageAdjpercentage0"><%= salvageAdjustmentTillDate.multiply(salvageAmount).divide(new BigDecimal(100).setScale(3,0)) %></span><span id="SalvageAdjinbdt0"><%=bdl.getString("CMS.Inv.InBDT")%></span></td>
                                     </tr>
                                     </table>
                                </td>
                            </tr>
                            <tr>
                                 <td class="ff" width="25%">Total Salvage Amount:</td>
                                <td>

                                <table width="100%" style="border:none;">
                                     <tr>
                                   <td style="text-align: left;border:none;padding:0px;"></td>
                                    <td style="text-align: right;border:none;padding:0px;"> <%= salvageAmount %> <%=bdl.getString("CMS.Inv.InBDT")%></td>
                                    </tr>
                                 </table>
                                </td>
                                <td class="ff" width="25%"> Salvage Adjustment :</td>
                                <td class="t-align-left">
                                <table width="100%" style="border:none;">
                                     <tr>
                                   <td style="text-align: left;border:none;padding:0px;"><span id="SalvageAdjAmtPercentage0"><%= invoicedetails.get(0).getSalvageAdjAmt() %></span></td>
                                    <td style="text-align: right;border:none;padding:0px;"> <span id="SalvageAdjAmtinbdt0"><%= invoicedetails.get(0).getSalvageAdjAmt().multiply(salvageAmount).divide(new BigDecimal(100).setScale(3,0)) %><%=bdl.getString("CMS.Inv.InBDT")%></span></td>
                                    </tr>
                                 </table>


                                </td>
                            </tr>
                            <% } %>
                            <tr>
                            <tr>                                
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.VAT")%>
                                <td class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <%if(!tenderType1.equals("ICT")){ %>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getVatAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;width:110px;"><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getVatAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                    
                                        <%
                                            }else{ %>
                                            <td style="text-align: left;border:none;padding:0px;display: none"></td>
                                            <td style="text-align: right;border:none;padding:0px;width:110px;"><%if(isEdit){%><%=invoicedetails.get(0).getVatAmt()+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                               
                                           <% }%>
                                   
                                    </tr>
                                    </table>
                                    <%--<%if(isEdit){%><%=invoicedetails.get(0).getVatAmt()%><%}%>
                                    <span style="padding-left:95px;"><%if(isEdit){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getVatAmt().doubleValue())/100).setScale(3,0));}%> (In BTN)</span>--%>
                                </td>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjVAT")%></td>
                                <td   class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getAdvVatAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;width:110px;">
                                            <%if(isEdit){
                                            for(int j=0;j<totalCur;j++)
                                                    {
                                            %><%=new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getAdvVatAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+")"%>
                                            <%}}%>
                                        </td>
                                    </tr>
                                    </table>
                                    <%--<%if(isEdit){%> <%=invoicedetails.get(0).getAdvVatAmt()%><%}%>
                                    <span style="padding-left:95px;"><%if(isEdit){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getAdvVatAmt().doubleValue())/100).setScale(3,0));}%> (In BTN)</span>--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.AIT")%>
                                <td   class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getAitAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;width:110px;">
                                            <%if(isEdit){
                                                for(int j=0;j<totalCur;j++){
                                                    if(tenderType1.equals("ICT") && curArr[j].equals("BTN"))
                                                        {
                                                     %><%=new BigDecimal(((invoiceAmount[j].doubleValue() - invoicedetails.get(0).getVatAmt().doubleValue())*invoicedetails.get(0).getAitAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+")"%>
                                                   <% } else {
                                                     %><%=new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getAitAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+")"%>
                                            <%}}}%></td>
                                    </tr>
                                    </table>
                                    <%--<%if(isEdit){%> <%=invoicedetails.get(0).getAitAmt()%><%}%>
                                    <span style="padding-left:104px;"><%if(isEdit){out.prfint(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getAitAmt().doubleValue())/100).setScale(3,0));}%> (In BTN)</span>--%>
                                </td>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.advadjAIT")%></td>
                                <td   class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getAdvAitAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;width:110px;">
										<%if(isEdit){
										 for(int j=0;j<totalCur;j++){
										%><%=new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getAdvAitAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+")"%>
										<%}}%></td>
                                    </tr>
                                    </table>
                                    <%--<%if(isEdit){%><%=invoicedetails.get(0).getAdvAitAmt()%><%}%>
                                    <span style="padding-left:95px;"><%if(isEdit){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getAdvAitAmt().doubleValue())/100).setScale(3,0));}%> (In BTN)</span>--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.VATAIT")%></td>
                                <td   class="t-align-left" width="25%">
                                   <table width="100%" style="border:none;">
                                    <tr>
                                <%
                                    if(!tenderType1.equals("ICT")){
                                        if(isEdit){
                                       %>
                                
                                <%
                                             for(int j=0;j<totalCur;j++)
                                            {
                                            if(invoicedetails.get(0).getVatAmt()!=null && invoicedetails.get(0).getAitAmt()!=null)
                                            {
                                                strVatPlusAit[j] = (invoicedetails.get(0).getVatAmt().doubleValue()+invoicedetails.get(0).getAitAmt().doubleValue());
                                            }
                                            else if(invoicedetails.get(0).getVatAmt()!=null && invoicedetails.get(0).getAitAmt()==null)
                                            {
                                                strVatPlusAit[j] = invoicedetails.get(0).getVatAmt().doubleValue();
                                            }
                                            else if(invoicedetails.get(0).getVatAmt()==null && invoicedetails.get(0).getAitAmt()!=null)
                                            {
                                                strVatPlusAit[j] = invoicedetails.get(0).getAitAmt().doubleValue();
                                            }
                                       

                                %>
                                
                                        <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(strVatPlusAit[j]).setScale(3,BigDecimal.ROUND_HALF_UP)%></td>
                                        <td style="text-align: right;border:none;padding:0px;width:110px;"><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*strVatPlusAit[j])/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                    
                                    <% }} %>
                                  

                                  <% }  else
                                    {
                                    if(isEdit){
                                        %>
                                 <!--  <td   class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr> -->
                                        <td style="text-align: left;border:none;padding:0px;display: none;width:110px;"></td>
                                        
                                    <%
                                        for(int j=0;j<totalCur;j++)
                                        {
                                        if(curArr[j].equals("BTN") || curArr[j].equals("Taka"))
                                            {
                                            //new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getAdvAitAmt().doubleValue())/100).setScale(3,0);
                                            strVatPlusAit[j] = invoicedetails.get(0).getVatAmt().doubleValue() + (((invoiceAmount[j].doubleValue()- invoicedetails.get(0).getVatAmt().doubleValue())*invoicedetails.get(0).getAitAmt().doubleValue())/100);
                                            }
                                        else
                                            {
                                              strVatPlusAit[j] = (invoiceAmount[j].doubleValue()*invoicedetails.get(0).getAitAmt().doubleValue())/100;
                                            }
                                       // }
                                    //  }
                                    
                                   %>
                                    <tr>
                                        <td style="text-align: right;border:none;padding:0px;width:110px;"><%=new BigDecimal(strVatPlusAit[j]).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+") <br/>"%></td>
                                    </tr>
                                        <% }}} %>
                                    </tr>
                                    </table>
                                    <%--<%=new BigDecimal(strVatPlusAit).setScale(3,0)%>
                                    <span style="padding-left:95px;"><%if(isEdit){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*strVatPlusAit)/100).setScale(3,0));}%> (In BTN)</span>--%>
                                </td>                                
                                <td   class="ff"><%=bdl.getString("CMS.Inv.advVATAIT")%>
                                </td>
                                <td   class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                    
                                        <%
                                        if(isEdit){
                                         for(int j=0;j<totalCur;j++)
                                        {
                                        if(invoicedetails.get(0).getAdvVatAmt()!=null && invoicedetails.get(0).getAdvAitAmt()!=null)
                                        {
                                            strAdvVatPlusAdvAit[j] = (invoicedetails.get(0).getAdvVatAmt().doubleValue()+invoicedetails.get(0).getAdvAitAmt().doubleValue());
                                        }
                                        else if(invoicedetails.get(0).getAdvVatAmt()!=null && invoicedetails.get(0).getAdvAitAmt()==null)
                                        {
                                            strAdvVatPlusAdvAit[j] = invoicedetails.get(0).getAdvVatAmt().doubleValue();
                                        }
                                        else if(invoicedetails.get(0).getAdvVatAmt()==null && invoicedetails.get(0).getAitAmt()!=null)
                                        {
                                            strAdvVatPlusAdvAit[j] = invoicedetails.get(0).getAdvAitAmt().doubleValue();
                                        }
                                        }} %>
                                        <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(strAdvVatPlusAdvAit[0]).setScale(3,BigDecimal.ROUND_HALF_UP)%></td>
                                        <td style="text-align: right;border:none;padding:0px;width:110px;">

                                       <% for(int j=0;j<totalCur;j++){
                                        if(isEdit){%><%=new BigDecimal((invoiceAmount[j].doubleValue()*strAdvVatPlusAdvAit[j])/100).setScale(3,0)+" (In "+curArr[j]+")"%><br><%}%>
                                        <%}%>

                                    </td>
                                    
                                    
                                    </tr>
                                    </table>
                                    <%--<%=new BigDecimal(strAdvVatPlusAdvAit).setScale(3,0)%>
                                    <span style="padding-left:95px;"><%if(isEdit){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*strAdvVatPlusAdvAit)/100).setScale(3,0));}%> (In BTN)</span>--%>
                                </td>
                            </tr>
                            <tr style="display:none;">
                                <td class="ff"><%=bdl.getString("CMS.Inv.netVATAIT")%></td>
                                
                                <td class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <%
                                    if(isEdit){
                                        if(tenderType1.equals("ICT"))
                                        {
                                        for(int j=0;j<totalCur;j++)
                                            {
                                              double bigadvadjAmtt = 0.0;
                                              bigadvadjAmtt= (Double.parseDouble(cntrAmnt[j])*strAdvanceAmount)/100;
                                              bigadvadjAmtt = (bigadvadjAmtt*invoicedetails.get(0).getAdvAdjAmt().doubleValue())/100;
                                           
                                             netVatandAitStr[j] = bigadvadjAmtt + strVatPlusAit[j] -((invoiceAmount[j].doubleValue()*strAdvVatPlusAdvAit[j])/100);
                                            }
                                        }
                                        else{
                                        for(int j=0;j<totalCur;j++)
                                            {
                                            double bigadvadjAmtt = 0.0;
                                            bigadvadjAmtt = (bigadvadjAmt.doubleValue()* invoicedetails.get(0).getAdvAdjAmt().doubleValue())/100;
                                            netVatandAitStr[j] =  strVatPlusAit[j] - strAdvVatPlusAdvAit[j];
                                             %>


                                        <td style="text-align: left;border:none;padding:0px;"><%=0%></td>
                                        <td style="text-align: right;border:none;padding:0px;width:110px;"><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*netVatandAitStr[j])/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></td>
                                       </tr> <%  }}}  %>
                                    </table>
                                    <%--<%=new BigDecimal(netVatandAitStr).setScale(3,0)%>
                                    <span style="padding-left:91px;"><%if(isEdit){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*netVatandAitStr)/100).setScale(3,0));}%> (In BTN)</span>--%>
                                </td>
                                <td class="ff"></td>
                                <td class="ff"></td>
                            </tr>
                            <tr>
                                <td class="ff" width="12%"  class="t-align-left"><%=bdl.getString("CMS.Inv.liquditydamage")%></td>
                                <td   class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getldAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;width:110px;">
                                         <%if(isEdit){
										 for(int j=0;j<totalCur;j++){
                                             System.out.println("invoiceAmount"+(invoiceAmount[j].doubleValue()*invoicedetails.get(0).getldAmt().doubleValue())/100);

										%><%=new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getldAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+")"%>
										<%}}%></td>
                                    </tr>
                                    </table>
                                    <%--<%if(isEdit){%><%=invoicedetails.get(0).getldAmt()%><%}%>
                                    <span style="padding-left:95px;"><%if(isEdit){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getldAmt().doubleValue())/100).setScale(3,0));}%> (In BTN)</span>--%>
                                </td>
                                <td   class="ff"><%=bdl.getString("CMS.Inv.penalty")%>
                                </td>
                                <td   class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getPenaltyAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;width:110px;">
                                        <% if(!tenderType1.equals("ICT")){ %>
                                            <%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getPenaltyAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%>
                                        <% }else{  for(int j=0;j<totalCur;j++){%>
                                              <%if(isEdit){%><%=new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getPenaltyAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+")"%><%}%>

                                        <% }}%>
                                        </td>
                                    </tr>
                                    </table>
                                    <%--<%if(isEdit){%> <%=invoicedetails.get(0).getPenaltyAmt()%><%}%>
                                    <span style="padding-left:95px;"><%if(isEdit){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getPenaltyAmt().doubleValue())/100).setScale(3,0));}%> (In BTN)</span>--%>
                                </td>
                            </tr>  
                        <% //} %>
                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.addition")%></div>
                        <table cellspacing="0" class="tableList_1" width="100%">
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.Bonus")%></td>
                                <td   class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){%><%=invoicedetails.get(0).getBonusAmt()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;width: 110px;">
                                         <%if(isEdit){
										 for(int j=0;j<totalCur;j++){
										%><%=new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getBonusAmt().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+")"%>
										<%}}%></td>
                                    </tr>
                                    </table>
                                    <%--<%if(isEdit){%><%=invoicedetails.get(0).getBonusAmt()%><%}%>
                                    <span style="padding-left:95px;"><%if(isEdit){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getBonusAmt().doubleValue())/100).setScale(3,0));}%> (In BTN)</span>--%>
                                </td>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.InterestOnDP")%></td>
                                <td   class="t-align-left" width="25%">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit && invoicedetails.get(0).getInterestonDP()!=null){%><%=invoicedetails.get(0).getInterestonDP()%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;width: 110px;">
                                          <% if(!tenderType1.equals("ICT")){ %>
                                            <%if(isEdit && invoicedetails.get(0).getInterestonDP()!=null){%><%if(isEdit){%><%=new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getInterestonDP().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" "+bdl.getString("CMS.Inv.InBDT")%><%}}%>
                                          <% }else{  for(int j=0;j<totalCur;j++){%>
                                           <%if(isEdit && invoicedetails.get(0).getInterestonDP()!=null){%><%if(isEdit){%><%=new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getInterestonDP().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+")"%><%}}%>
                                          <%}}%>
                                        </td>
                                    </tr>
                                    </table>
                                    <%--<%if(isEdit && invoicedetails.get(0).getInterestonDP()!=null){%><%=invoicedetails.get(0).getInterestonDP()%><%}%>
                                    <span style="padding-left:96px;"><%if(isEdit && invoicedetails.get(0).getInterestonDP()!=null){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getInterestonDP().doubleValue())/100).setScale(3,0)+" (In BTN)");}%></span>--%>
                                </td>
                            </tr>
                        </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.retention")%></div>
                        <table cellspacing="0" class="tableList_1" width="100%">
                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.retentionmoneytilldate")%></td>
                            <td   class="t-align-left">
                                <%
                                   
                                    if(!totalRDeducted.isEmpty())
                                    {
                                        Object[] objj = totalRDeducted.get(0);
                                        if(objj[2]==null)
                                        {
                                            dbtotalRDeducted = new BigDecimal(0).setScale(3,BigDecimal.ROUND_HALF_UP).doubleValue();
                                        }
                                        else{
                                            dbtotalRDeducted =  new BigDecimal(objj[2].toString()).setScale(3,BigDecimal.ROUND_HALF_UP).doubleValue();
                                        }
                                        //out.print(new BigDecimal(dbtotalRDeducted).setScale(3, 0));
                                        //out.print("<span style='padding-left:104px;'>");
                                        //out.print(new BigDecimal((new BigDecimal(c_obj[2].toString()).doubleValue()*dbtotalRDeducted)/100).setScale(3,0)+ " (In BTN)");
                                        //out.print("</span>");
                                    }
                                %>
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%=new BigDecimal(dbtotalRDeducted).setScale(3, BigDecimal.ROUND_HALF_UP)%></td>
                                        <td style="text-align: right;border:none;padding:0px;">
                                          <% for(int j=0;j<totalCur;j++){%>    
                                            <%=new BigDecimal((new BigDecimal(cntrAmnt[j].toString()).doubleValue()*dbtotalRDeducted)/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+")"%>
                                          <% } %>
                                        </td>
                                    </tr>
                                    </table>
                            </td>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.retentionmoney")%></td>
                                <td class="t-align-left">
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: left;border:none;padding:0px;"><%if(isEdit){if(invoicedetails.get(0).getRetentionMadd()!=null){%><%=invoicedetails.get(0).getRetentionMadd()%>&nbsp;<span style="color: #f00;">(Added)</span><%}else if(invoicedetails.get(0).getRetentionMdeduct()!=null){%><%=invoicedetails.get(0).getRetentionMdeduct()%>&nbsp;<span style="color: #f00;">(Deducted)</span><%}}%></td>
                                        <td style="text-align: right;border:none;padding:0px;">
                                        <%if(isEdit){
										 for(int j=0;j<totalCur;j++){
                                             if(invoicedetails.get(0).getRetentionMadd()!=null)
                                                 {
                                                 out.print(new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getRetentionMadd().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+") ");
                                                 }
                                             else if(invoicedetails.get(0).getRetentionMdeduct()!=null)
                                                 {
                                                 out.print(new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getRetentionMdeduct().doubleValue())/100).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+") ");
                                                 }
										
										}}%></td>
                                    </tr>
                                    </table>
                                    <%--<%if(isEdit){if(invoicedetails.get(0).getRetentionMadd()!=null){%><%=invoicedetails.get(0).getRetentionMadd()%>&nbsp;<span style="color: #f00;">(Added)</span><%}else if(invoicedetails.get(0).getRetentionMdeduct()!=null){%><%=invoicedetails.get(0).getRetentionMdeduct()%>&nbsp;<span style="color: #f00;">(Deducted)</span><%}}%>
                                     <span style="padding-left:50px;"><%if(isEdit){if(invoicedetails.get(0).getRetentionMadd()!=null){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getRetentionMadd().doubleValue())/100).setScale(3,0));}else if(invoicedetails.get(0).getRetentionMdeduct()!=null){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getRetentionMdeduct().doubleValue())/100).setScale(3,0));}}%> (In BTN)</span>--%>
                                </td>
                            </tr>
                            <tr> <td colspan="2"><ol style="padding-right: 5px;margin-left: 20px;"><li>Retention money at a percentage as specified in Schedule II will be
                                         deductable from each bill due to a Contractor until completion of the whole Works or delivery.</li>
                                     <li>On completion of the whole Works, half the total amount retained shall be repaid to
                                         the Contractor and the remaining amount may also be paid to the Contractor if an unconditional
                                         Bank guarantee is furnished for that remaining amount.</li>
                                     <li >The remaining amount or the Bank guarantee, under Sub-Rule (2), shall be returned,
                                         within the period specified in schedule II , after issuance of all Defects Correction Certificate
                                         under Rule 39(29) by the Project Manager or any other appropriate Authority ..</li>
                                     <li >Deduction of retention money shall not be applied to small Works Contracts if no
                                         advance payment has been made to the Contractor and in such case the provisions of Sub-Rule
                                         (7) and (8) of Rule 27 shall be applied.</li></ol>
                                 </td>
                            </tr>
                        </table>
                                <br />
                                <table class="tableList_1" cellspacing="0" width="100%">
                                    <tr>
                                        <td width="25%" class="ff">
                                            <%=strProcNature%> Bank Details
                                        </td>
                                        <td class="ff">
                                           <a onclick="javascript:window.open('ViewAccInfo.jsp?id=<%=id%>', '', 'width=800px,height=300px,scrollbars=1','');" href="javascript:void(0);">View</a>
                                        </td>
                                    </tr>
                                </table>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails")%><%if(!tenderType1.equals("ICT")){%>&nbsp;&nbsp;<%=invoicedetails.get(0).getGrossAmt()+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></div>
                        <table class="tableList_1" cellspacing="0" width="100%">
                            <tr>
                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%><span class="mandatory">*</span></td>
                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getModeOfPayment()%><%}%>
                                <span id="modeofPaymentspan" class="reqF_1"></span>
                            </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=DateUtils.customDateFormate(invoicedetails.get(0).getDateOfPayment())%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getBankName()%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getBranchName()%><%}%>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getInstrumentNo()%><%}%>
                                </td>
                            </tr>
                        </table>
                        <%
                            double NetVat = 0;
                            if(invoicedetails.get(0).getVatAmt()!=null)
                            {
                                if(invoicedetails.get(0).getAdvVatAmt()!=null)
                                {
                                    NetVat = (((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getVatAmt().doubleValue())/100) - ((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getAdvVatAmt().doubleValue())/100)) ;
                                }
                            }                            
                        %>                        
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails.vat")%><%if(!tenderType1.equals("ICT")){%>&nbsp;&nbsp;<%=new BigDecimal(NetVat).setScale(3,BigDecimal.ROUND_HALF_UP)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></div>
                        <%if(invoicedetails.get(0).getVatdateOfPayment()!=null){%>
                        <table class="tableList_1" cellspacing="0" width="100%">
                            <tr>
                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%><span class="mandatory">*</span></td>
                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getVatmodeOfPayment()%><%}%>
                                <span id="vatmodeofPaymentspan" class="reqF_1"></span>
                            </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%><span class="mandatory">*</span></td>
                                <td class="t-align-left"><%if(isEdit){%><%=DateUtils.customDateFormate(invoicedetails.get(0).getVatdateOfPayment())%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getVatbankName()%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getVatbranchName()%><%}%>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getVatinstrumentNo()%><%}%>
                                </td>
                            </tr>
                        </table>
                        <%}%>
                        <%
                            double NetAit = 0;
                            if(invoicedetails.get(0).getVatAmt()!=null)
                            {
                                if(invoicedetails.get(0).getAdvVatAmt()!=null)
                                {
                                    NetAit = (((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getAitAmt().doubleValue())/100) - ((invoicedetails.get(0).getInvoiceAmt().doubleValue()*invoicedetails.get(0).getAdvAitAmt().doubleValue())/100)) ;
                                }
                            }                            
                        %>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.paydetails.ait")%><%if(!tenderType1.equals("ICT")){%>&nbsp;&nbsp;<%=new BigDecimal(NetAit).setScale(3,BigDecimal.ROUND_HALF_UP)+" "+bdl.getString("CMS.Inv.InBDT")%><%}%></div>
                        <%if(invoicedetails.get(0).getAitdateOfPayment()!=null){%>
                        <table class="tableList_1" cellspacing="0" width="100%">
                            <tr>
                            <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.modeofpayment")%><span class="mandatory">*</span></td>
                            <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getAitmodeOfPayment()%><%}%>
                                <span id="aitmodeofPaymentspan" class="reqF_1"></span>
                            </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.dateofpayment")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=DateUtils.customDateFormate(invoicedetails.get(0).getAitdateOfPayment())%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.bankname")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getAitbankName()%><%}%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.branchname")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getAitbranchName()%><%}%>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff" width="25%"  class="t-align-left"><%=bdl.getString("CMS.Inv.instumentnumber")%><span class="mandatory">*</span></td>
                                <td   class="t-align-left"><%if(isEdit){%><%=invoicedetails.get(0).getAitinstrumentNo()%><%}%>
                                </td>
                            </tr>
                        </table>
                        <%}%>
                        <div class="tableHead_1 t_space" align="left"><%=bdl.getString("CMS.Inv.gross")%></div>
                        <table class="tableList_1" cellspacing="0" width="100%">
                            <%
                            double totalDeductedAmount[] = new double[4];
                            double totalDeductedAmountt[] = new double[4];
                            double totalAddedAmount[] = new double[4];
                            BigDecimal bigdeductedAmt = BigDecimal.ZERO.setScale(3,BigDecimal.ROUND_HALF_UP);

                                        if(isEdit)
                                        {
                                          for(int j=0;j<totalCur;j++){
                                          bigdeductedAmt = BigDecimal.ZERO.setScale(3,BigDecimal.ROUND_HALF_UP);
                            %>
                        <tr>
                                <td class="ff" bgcolor="#dddddd">Total Amount Deducted (In <%=curArr[j]%>) :</td>
                                <td>
                                    <%

                                         if(invoicedetails.get(0).getAdvAdjAmt()!=null)
                                          {
                                                totalDeductedAmount[j] = totalDeductedAmount[j] + new BigDecimal((new BigDecimal((new BigDecimal(cntrAmnt[j].toString()).doubleValue()*strAdvanceAmount)/100).setScale(3,BigDecimal.ROUND_HALF_UP).doubleValue()*invoicedetails.get(0).getAdvAdjAmt().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                               
                                          }
                                           if(invoicedetails.get(0).getSalvageAdjAmt()!=null)
                                          {
                                                totalDeductedAmount[j] = totalDeductedAmount[j] + invoicedetails.get(0).getSalvageAdjAmt().multiply(salvageAmount).divide(new BigDecimal(100).setScale(3,0)).doubleValue();
                                               
                                          }
                                         
                                          if(invoicedetails.get(0).getldAmt()!=null)
                                          {
                                              totalDeductedAmount[j] = totalDeductedAmount[j] + new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getldAmt().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                              totalDeductedAmountt[j] = totalDeductedAmountt[j] +new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getldAmt().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                             
                                          }
                                        
                                          if(invoicedetails.get(0).getPenaltyAmt()!=null)
                                          {
                                              totalDeductedAmount[j] = totalDeductedAmount[j] + new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getPenaltyAmt().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                              totalDeductedAmountt[j] = totalDeductedAmountt[j] + new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getPenaltyAmt().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                              
                                          }
                                         
                                          if(invoicedetails.get(0).getRetentionMdeduct()!=null)
                                          {
                                              totalDeductedAmount[j] = totalDeductedAmount[j] + new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getRetentionMdeduct().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                              totalDeductedAmountt[j] = totalDeductedAmountt[j] + new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getRetentionMdeduct().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                             
                                          }
                                            
                                          if(invoicedetails.get(0).getRetentionMadd()!=null)
                                          {
                                              totalAddedAmount[j] = totalAddedAmount[j] + new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getRetentionMadd().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                             
                                          }
                                            
                                          if(invoicedetails.get(0).getBonusAmt()!=null)
                                          {
                                              totalAddedAmount[j] = totalAddedAmount[j] + new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getBonusAmt().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                              
                                          }
                                          if(invoicedetails.get(0).getInterestonDP()!=null)
                                          {
                                              totalAddedAmount[j] = totalAddedAmount[j] + new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getInterestonDP().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
                                          }
                                          if(netVatandAitStr[j]>0)
                                          {
                                             
                                              if(!tenderType1.equals("ICT"))
                                                {
                                                   // totalDeductedAmount[j] = totalDeductedAmount[j]+ new BigDecimal((invoiceAmount[j].doubleValue()*invoicedetails.get(0).getInterestonDP().doubleValue())/100).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue()[j];
                                                  //   System.out.print("dft"+totalDeductedAmount[j]);
                                                 //   totalDeductedAmountt[j] = totalDeductedAmountt[j]+netVatandAitStr[j];
                                                //     System.out.print("dftt"+totalDeductedAmountt[j]);
                                                 //     System.out.print("dfdd"+bigadvadjAmt);
                                                //    bigdeductedAmt =  new BigDecimal(totalDeductedAmountt[j]).setScale(3,BigDecimal.ROUND_HALF_UP);
                                                //     System.out.print("dfddddd"+bigdeductedAmt);
                                                  
                                                     bigdeductedAmt = new BigDecimal((invoiceAmount[j].doubleValue()*netVatandAitStr[j])/100).setScale(3,BigDecimal.ROUND_HALF_UP).add(new BigDecimal(totalDeductedAmount[j]).setScale(3, BigDecimal.ROUND_HALF_UP));
                                                    
                                                }
                                              else
                                                {
                                            // BigDecimal bigdeductedAmt =  new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*totalDeductedAmountt[j])/100).setScale(3,0).add(new BigDecimal((bigadvadjAmt.doubleValue()*invoicedetails.get(0).getAdvAdjAmt().doubleValue())/100).setScale(3,0));

                                                         
                                                  bigdeductedAmt = new BigDecimal(netVatandAitStr[j]).setScale(3,BigDecimal.ROUND_HALF_UP).add(new BigDecimal(totalDeductedAmountt[j]).setScale(3, BigDecimal.ROUND_HALF_UP));
                                                 // System.out.print("bigdeductedAmt"+bigdeductedAmt);
                                              }
                                             
                                          }
                                          if(netVatandAitStr[j]<0)
                                          {
                                              totalAddedAmount[j] = totalAddedAmount[j]-netVatandAitStr[j];
                                          }
                                          if(netVatandAitStr[j]==0.0){
                                          System.out.println("final netvat:"+netVatandAitStr[j]);
                                          bigdeductedAmt=new BigDecimal(totalDeductedAmount[j]).setScale(3, BigDecimal.ROUND_HALF_UP);
                                          }
                                         
                                    %>
                                    <%--<span style="padding-left:95px;"><%if(isEdit){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*totalDeductedAmount)/100).setScale(3,0));}%> (In BTN)</span>--%>
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <%
                                           // BigDecimal bigdeductedAmt =  new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*totalDeductedAmountt[j])/100).setScale(3,0).add(new BigDecimal((bigadvadjAmt.doubleValue()*invoicedetails.get(0).getAdvAdjAmt().doubleValue())/100).setScale(3,0));
                                        %>
                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=bigdeductedAmt+" (In "+curArr[j]+")"%><%}%></td>
                                       
                                        <td style="text-align: right;border:none;padding:0px;"><%--<%=new BigDecimal(totalDeductedAmount).setScale(3,0)%>&nbsp;(in "%")--%></td>
                                    </tr>
                                    </table>
                                </td>
                        </tr>
                        <tr>
                                <td class="ff" bgcolor="#dddddd">Total Amount Added (In <%=curArr[j]%>):</td>
                                <td>
                                    <table width="100%" style="border:none;">
                                    <tr>
                                        <td style="text-align: right;border:none;padding:0px;"><%if(isEdit){%><%=new BigDecimal(totalAddedAmount[j]).setScale(3,BigDecimal.ROUND_HALF_UP)+" (In "+curArr[j]+")"%><%}%></td>
                                        <td style="text-align: right;border:none;padding:0px;"><%--<%=new BigDecimal(totalAddedAmount).setScale(3,0)%>&nbsp;(in "%")--%></td>
                                    </tr>
                                    </table>
                                <%--<%out.print(new BigDecimal(totalAddedAmount).setScale(3,0));%>
                                <span style="padding-left:88px;"><%if(isEdit){out.print(new BigDecimal((invoicedetails.get(0).getInvoiceAmt().doubleValue()*totalAddedAmount)/100).setScale(3,0));}%> (In BTN)</span>--%>
                                </td>
                        </tr>
                        <tr>
                            <td class="ff" width="25%"  class="t-align-left">Net Amount (In <%=curArr[j]%>)</td>
                            <td style="text-align: right"><%if(isEdit){%><%=grossAmount[j]%><%}%>
                                
                            <input type="hidden" id="GrossAmtid<%=j%>" <%if(isEdit){%>value="<%=grossAmount[j]%>"<%}%>>
                            &nbsp;&nbsp;&nbsp;<span id="GrossinWords<%=j%>" class="ff"></span>
                            </td>
                        </tr>
                         <% }} %>
                        </table>                        
                      </form>
                    </div>
                </div>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <td class="t-align-left ff" width="15%">
                           <%if(HideActionScenario){%>
                            <%=bdl.getString("CMS.Inv.Upload")%>
                           <%}else{%>
                            <%=bdl.getString("CMS.Inv.Download")%>
                           <%}%>
                        </td>
                        <td>
                            <%if(HideActionScenario){%>
                            <a href="AccPaymentDocUpload.jsp?tenderId=<%=tenderId%>&InvoiceId=<%=InvoiceId%>&wpId=<%=wpId%>&lotId=<%=lotId%>" class="noprint">Upload</a>
                            <%}%>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                <th width="8%" class="t-align-center"><%=bdl.getString("CMS.Srno")%></th>
                                <th class="t-align-center" width="23%"><%=bdl.getString("CMS.Inv.FileName")%></th>
                                <th class="t-align-center" width="28%"><%=bdl.getString("CMS.Inv.FileDescription")%></th>
                                <th class="t-align-center" width="9%"><%=bdl.getString("CMS.Inv.FileSize")%><br /><%=bdl.getString("CMS.Inv.inKB")%></th>
                                <th class="t-align-center" width=""><%=bdl.getString("CMS.Inv.Uploadedby")%></th>
                                <th class="t-align-center" width="18%"><%=bdl.getString("CMS.action")%></th>
                                </tr>
                                <%                                    
                                    List<TblCmsInvoiceDocument> getInvoiceDocData = accPaymentService.getInvoiceDocDetails(Integer.parseInt(InvoiceId));
                                    if(!getInvoiceDocData.isEmpty())
                                    {
                                        for(int i =0; i<getInvoiceDocData.size(); i++)
                                        {
                                %>
                                <tr>
                                    <td class="t-align-center"><%=(i+1)%></td>
                                    <td class="t-align-left"><%=getInvoiceDocData.get(i).getDocumentName()%></td>
                                    <td class="t-align-left"><%=getInvoiceDocData.get(i).getDocDescription()%></td>
                                    <td class="t-align-center"><%=(Long.parseLong(getInvoiceDocData.get(i).getDocSize())/1024)%></td>
                                    <td class="t-align-center">
                                        <%String str = "";
                                        if (2 == getInvoiceDocData.get(i).getUserTypeId()) {
                                            str = "supplier";
                                            out.print("Supplier");
                                        } else {
                                            if("25".equalsIgnoreCase(accPaymentService.getProcurementRoleID(Integer.toString(getInvoiceDocData.get(i).getUploadedBy()))))
                                            {
                                                str = "accountant";
                                                out.print("Account Officer");
                                            }else{
                                                str = "PE";
                                                out.print("PE Officer");
                                            }

                                        }%>
                                    </td>
                                    <td class="t-align-center">
                                        <a href="<%=request.getContextPath()%>/AccPaymentDocServlet?docName=<%=getInvoiceDocData.get(i).getDocumentName()%>&docSize=<%=getInvoiceDocData.get(i).getDocSize()%>&tenderId=<%=tenderId%>&invoiceDocId=<%=getInvoiceDocData.get(i).getInvoiceDocId()%>&invoiceId=<%=getInvoiceDocData.get(i).getTblCmsInvoiceMaster().getInvoiceId()%>&supplier=<%if(2==getInvoiceDocData.get(i).getUserTypeId()){%>yes<%}%>&str=<%=str%>&funName=download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                        &nbsp;
                                        <%if("accountant".equalsIgnoreCase(str)){%>
                                        <%if(HideActionScenario){%>
                                        <a href="<%=request.getContextPath()%>/AccPaymentDocServlet?&docName=<%=getInvoiceDocData.get(i).getDocumentName()%>&docSize=<%=getInvoiceDocData.get(i).getDocSize()%>&tenderId=<%=tenderId%>&invoiceDocId=<%=getInvoiceDocData.get(i).getInvoiceDocId()%>&invoiceId=<%=getInvoiceDocData.get(i).getTblCmsInvoiceMaster().getInvoiceId()%>&supplier=<%if(2==getInvoiceDocData.get(i).getUserTypeId()){%>yes<%}%>&str=<%=str%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                        <%}}%>
                                    </td>
                                </tr>
                                 <%}
                                }else{%>
                                <tr>
                                    <td colspan="6" class="t-align-center"><%=bdl.getString("CMS.Inv.NoRecord")%></td>
                                </tr>
                                <%}%>
                        </table>
                        </td>
                    </tr>
                </table>
             </div>
            </div>
            </div>
            <%if(HideActionScenario){%>
            <table class="t_space" cellspacing="0" width="100%">
                <tr>
                    <td class="t-align-center">
                        <% if(!tenderType1.equals("ICT")){ %>
                        <a href="<%=request.getContextPath()%>/AccPaymentServlet?tenderId=<%=tenderId%>&InvoiceId=<%=InvoiceId%>&wpId=<%=wpId%>&lotId=<%=lotId%>&action=sendtope" class="anchorLink" style="text-decoration: none; color: #fff;">Send To PE</a>                       
                        <a onclick="SendToTen(<%=wpId%>,<%=tenderId%>,<%=lotId%>,<%=InvoiceId%>)"  class="anchorLink" style="text-decoration: none; color: #fff;">Send To <%=strProcNature%></a>
                        <a href="AccPayment.jsp?tenderId=<%=tenderId%>&InvoiceId=<%=InvoiceId%>&wpId=<%=wpId%>&lotId=<%=lotId%>" class="anchorLink" style="text-decoration: none; color: #fff;">Edit Payment Details</a>
                        <% } else { %>
                        <a href="<%=request.getContextPath()%>/AccPaymentServlet?tenderId=<%=tenderId%>&InvoiceId=<%=InvoiceId%>&wpId=<%=wpId%>&lotId=<%=lotId%>&invoiceNo=<%=request.getParameter("invoiceNo")%>&action=sendtope" class="anchorLink" style="text-decoration: none; color: #fff;">Send To PE</a>
                        <a onclick="SendToTenForICT(<%=wpId%>,<%=tenderId%>,<%=lotId%>,<%=InvoiceId%>,'<%=InvoiceNo%>')"  class="anchorLink" style="text-decoration: none; color: #fff;">Send To <%=strProcNature%></a>
                        <a href="AccPayment.jsp?tenderId=<%=tenderId%>&InvoiceId=<%=InvoiceId%>&wpId=<%=wpId%>&lotId=<%=lotId%>&invoiceNo=<%=request.getParameter("invoiceNo")%>" class="anchorLink" style="text-decoration: none; color: #fff;">Edit Payment Details</a>
                        <% } %>
                    </td>
                </tr>
            </table>
            <%}%>
            <div>&nbsp;</div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabMakePay");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        function AmtInWords()
        {
           // var a =document.getElementsByName("amtWords");
             var sDomAttr =  document.getElementById("totalCurrency").value;
            for(var i = 0;i<sDomAttr;i++)
                {
                    //Numeric to word currency conversion by Emtaz on 20/April/2016
                //document.getElementById("amtinWords"+i).innerHTML = '('+(DoIt(document.getElementById("invoiceAmtid"+i).value))+')';
                //document.getElementById("GrossinWords"+i).innerHTML = '('+(DoIt(document.getElementById("GrossAmtid"+i).value))+')';
                    document.getElementById("amtinWords"+i).innerHTML = '('+(CurrencyConverter(document.getElementById("invoiceAmtid"+i).value))+')';
                    document.getElementById("GrossinWords"+i).innerHTML = '('+(CurrencyConverter(document.getElementById("GrossAmtid"+i).value))+')';
                }
         //   document.getElementById("amtinWords").innerHTML = DoIt(document.getElementById("invoiceAmtid").value);
         //   document.getElementById("GrossinWords").innerHTML = DoIt(document.getElementById("GrossAmtid").value)
        }
    </script>       
</html>
