<%-- 
    Document   : CommonLotDetail
    Created on : Dec 11, 2010, 6:25:06 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>


        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>


<%
            // get tenderid and set in string
            String tid = "";
            if(request.getParameter("tenderId")!=null){
            tid = request.getParameter("tenderId");
            }
            String corrigendumId="";
            if(request.getParameter("corrigendumId")!=null){
                corrigendumId = request.getParameter("corrigendumId");
                }


%>

<!--Dashboard Header Start--><!--Dashboard Header End-->
<!--Dashboard Content Part Start-->
<div class="tableHead_1 t_space">Lot Detail</div>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<table width="100%" cellspacing="0" class="tableList_1 t_space">

    <tr>
        <th width="4%" class="t-align-center">Sl. No.</th>
        <th class="t-align-center" width="31%">Lot No.</th>
        <th class="t-align-center" width="60%">Lot Description</th>
        <th class="t-align-center" width="5%">Action</th>
    </tr>
<%

            // create instance of Service layer. Call proc with required parameters
            TenderCommonService tenderCService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            int count = 0;
            for (CommonTenderDetails commonTdetails : tenderSrBean.getAPPDetails(Integer.parseInt(tid), "lot")) {
                count++;

%>
    <tr>
                        <td class="t-align-center"><%=count%></td>
                        <td class="t-align-center"><%=commonTdetails.getLotNo()%></td>
                        <td class="t-align-left"><%=commonTdetails.getLotDesc() %></td>
                        <td class="t-align-left"><a href="UploadAmendDoc.jsp?tenderId=<%=tid%>&corrigendumId=<%=corrigendumId%>&lotId=<%=commonTdetails.getAppPkgLotId()%>">Action</a></td>
                    </tr>
<%}if(count == 0){%>
 <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
</tr>
<%}%>
</table>



