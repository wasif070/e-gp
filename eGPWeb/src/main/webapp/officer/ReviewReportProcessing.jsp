<%--
    Document   : ReviewReportProcessing
    Created on : Oct 30, 2011, 10:42:16 AM
    Author     : darshan
--%>

<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalRptForwardToAa"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvaluationMatrix"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalRptSentToAa"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalReportDocs"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
            int intEnvelopcnt = 0;
            String s_tenderid = "0";
            String s_lotId = "0";
            String s_reviewId = "0";
            String s_rId = "0";
            String evalType = "eval";

            TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            TblEvalRptForwardToAa tblEvalRptForwardToAa = new TblEvalRptForwardToAa();
            EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");

            if (request.getParameter("tenderid") != null) {
                s_tenderid = request.getParameter("tenderid");
            }
            if (request.getParameter("lotId") != null) {
                s_lotId = request.getParameter("lotId");
            }
            if (request.getParameter("evalRptFFid") != null) {
                s_reviewId = request.getParameter("evalRptFFid");
            }
            if (request.getParameter("rId") != null) {
                s_rId = request.getParameter("rId");
            }
            if (request.getParameter("stat") != null) {
                evalType = request.getParameter("stat");
            }
            List<SPTenderCommonData> lstEnvelops = tenderCommonService1.returndata("getTenderEnvelopeCount", s_tenderid, "0");
            if (lstEnvelops != null && !lstEnvelops.isEmpty() && Integer.parseInt(lstEnvelops.get(0).getFieldName1()) > 0) {
                intEnvelopcnt = Integer.parseInt(lstEnvelops.get(0).getFieldName1());
            }

            tblEvalRptForwardToAa = evalService.getTblEvalRptForwardToAa(Integer.parseInt(s_reviewId), Integer.parseInt(s_rId));
            boolean isForward = !tblEvalRptForwardToAa.getRptStatus().equalsIgnoreCase("Forward");
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Report Reviewing by Reviewer</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
                <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            function setOfficer(){
                $.post("<%=request.getContextPath()%>/EvaluationServlet", {type:'',tenderid:$('#tenderid').val(),designation:'AA',action:'empCombo'}, function(j){
                    $("#nameTd").html(j);
                    $("#nameTr").show();
                });
            }
            function validate(){
                $(".err").remove();
                var valid = true;
                if(CKEDITOR.instances.txtRemark.getData() == 0){
                    $("#txtRemark").parent().append("<div class='err' style='color:red;'>Please enter Comments</div>");
                    valid=false;
                }
                if(CKEDITOR.instances.txtRemark.getData().length > 2000)
                {
                    $("#txtRemark").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters are allowed</div>");
                    valid = false;
                }
                if(!valid){
                    return false;
                }
            }
            function getDocData(){
                <% if (!tblEvalRptForwardToAa.getRptStatus().equalsIgnoreCase("Forward")) {%>
                $.post("<%=request.getContextPath()%>/EvalRptDocsServlet", {tenderid:<%=s_tenderid%>,lotId:<%=s_lotId%>,rId:<%=s_rId%>,funName:'docReviewTable',isForward:'<%=isForward%>'}, function(j){
                    document.getElementById("queryData").innerHTML = j;
                });
                <%}%>
            }
            function deleteFile(docId,docName,tenderId,lotId,rId){
               // alert(docId+docName+tenderId+lotId+rId);
                $.alerts._show("Delete Document", 'Do you really want to delete this file?', null, 'confirm', function(r) {
                    if(r == true){
                        $.post("<%=request.getContextPath()%>/EvalRptDocsServlet", {docName:docName,docId:docId,tenderid:tenderId,lotId:lotId,rId:rId,funName:'docDelete'}, function(j){
                            jAlert("Document deleted successfully."," Delete Document ", function(RetVal) {
                                getDocData();
                            });
                        });
                    }else{
                        getDocData();
                    }
                });
            }
        </script>
    </head>
    <body onload="setOfficer()">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="DashboardContainer">
                <div class="contentArea_1">
                    <div class="pageHead_1">Review Evaluation Report <span class="c-alignment-right"><a href="ReviewReportListing.jsp?tenderid=<%=s_tenderid%>&lotId=<%=s_lotId%>" title="Go Back" class="action-button-goback">Go Back</a></span></div>
                    <%
                                pageContext.setAttribute("tenderId", s_tenderid);
                    %>
                    <div class="t_space">
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    </div>
                    <form id="frmReprotProcessing" action="<%=request.getContextPath()%>/EvaluationServlet" name="frmReprotProcessing"  method="POST">
                        <input type="hidden" value="updateReviewRpt" name="action" />
                        <input type="hidden" name="tenderid" id="tenderid" value="<%=s_tenderid%>" />
                        <input type="hidden" name="lotId" value="<%=s_lotId%>" />
                        <input type="hidden" name="sentRole" value="AA" />
                        <input type="hidden" name="sUserId" value="<%=tblEvalRptForwardToAa.getSentByUserId()%>" />
                        <input type="hidden" name="rId" value="<%=s_rId%>" />
                        <input type="hidden" name="txtRptStatus" value="f" />
                        <input type="hidden" name="rptMethod" value="<%=intEnvelopcnt%>" />

                        <table width="100%" cellspacing="10" class="tableList_3 t_space">
                            <tr>
                                <td class="ff">Tender Opening Report 1</td>
                                <td>
                                    <a href="<%=contextPath%>/officer/TOR1.jsp?tenderid=<%=s_tenderid%>&lotId=<%=s_lotId%>&goback=hide" target="_blank">
                                    <%if ("Services".equalsIgnoreCase(tenderProcureNature)) {%>
                                    POR1
                                    <%} else {%>
                                    TOR1
                                    <%}%>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Tender Opening Report 2</td>
                                <td>
                                    <a href="<%=contextPath%>/officer/TOR2.jsp?tenderid=<%=s_tenderid%>&lotId=<%=s_lotId%>&goback=hide" target="_blank">
                                    <%if ("Services".equalsIgnoreCase(tenderProcureNature)) {%>
                                    POR2
                                    <%} else {%>
                                    TOR2
                                    <%}%>
                                </a>
                                </td>
                            </tr>
                            <jsp:include page="TERInclude.jsp">
                                <jsp:param name="tenderId" value="<%=s_tenderid%>"/>
                                <jsp:param name="lotId" value="<%=s_lotId%>"/>
                                <jsp:param name="roundId" value="<%=s_rId%>"/>
                                <jsp:param name="evalType" value="<%=evalType%>"/>
                            </jsp:include>
                            <tr>
                                <td class="ff">Comments of TEC Chairperson :</td>
                                <td><%=tblEvalRptForwardToAa.getSentUserRemarks()%></td>
                            </tr>
                            <tr id="nameTr" >
                                <td class="ff">Forward Officer's Name :</td>
                                <td id="nameTd">

                                </td>
                            </tr>
                            <% if (tblEvalRptForwardToAa.getRptStatus().equalsIgnoreCase("Forward")) {%>
                            <tr>
                                <td class="ff">Status :</td>
                                <td><%=tblEvalRptForwardToAa.getRptStatus()%></td>
                            </tr>
                            <tr>
                                <td class="ff">Comments :</td>
                                <td><%=tblEvalRptForwardToAa.getActionUserRemarks()%></td>
                            </tr>
                            <tr>
                                <td width="30%" class="t-align-left ff">Evaluation Report</td>
                                <td width="80%" class="t-align-left">
                                    <a href="<%=request.getContextPath() %>/officer/ViewEvalReportListing.jsp?tenderid=<%=s_tenderid%>&isSU=yes&lotId=<%=s_lotId%>&evalRptFFid=<%=tblEvalRptForwardToAa.getEvalFfrptId()%>&rId=<%=s_rId%>">View</a>
                                </td>
                            </tr>
                            <%}%>
                        </table>
                        <% if (!tblEvalRptForwardToAa.getRptStatus().equalsIgnoreCase("Forward")) {%>
                        <div style="font-style: italic" class="formStyle_1 t_space b_space ff">Fields marked with (<span class="mandatory">*</span>) are mandatory</div>
                        <table width="100%" cellspacing="10" class="tableView_1 t_space">
                            <tr>
                                <td class="ff" width="10%">Action :  </td>
                                <td width="90%"><strong>Forward</strong></td>
                            </tr>
                            <tr id="trRemark">
                                <td valign="top" class="ff">Comments : &nbsp;<span class="mandatory">*</span></td>
                                <td>
                                    <textarea cols="100" name="textComments" rows="5" id="txtRemark" class="formTxtBox_1"></textarea>
                                    <script type="text/javascript">
                                        CKEDITOR.replace( 'textComments',
                                        {
                                            toolbar : "egpToolbar"

                                        });
                                    </script>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Reference Document :</td>
                                <td>
                                    <a href="javascript:void(0)" onclick="window.open('EvalRptDocUpload.jsp?tenderid=<%=s_tenderid%>&lotId=<%=s_lotId%>&rId=<%=s_rId%>','window','resizable=yes,scrollbars=1');">Upload</a>
                                    <%--<a href="EvalRptDocUpload.jsp?tenderid=<%=s_tenderid%>&lotId=<%=s_lotId%>&rId=<%=s_rId%>">Upload</a>--%>
                                </td>
                            </tr>
                            <tr id="trSubmit">
                                <td>&nbsp;</td>
                                <td class="t-align-left">
                                    <input type="hidden" name="rptId" value="<%=tblEvalRptForwardToAa.getEvalFfrptId()%>" />
                                    <label class="formBtn_1 l_space "><input type="submit" name="submit" value="Submit" onclick="return validate();"/></label>
                                </td>
                            </tr>
                        </table>
                    </form>

                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <div id="queryData"></div>
                    </table>
                    <%}%>
                </div>

            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabEval");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        getDocData();
    </script>
</html>
<%
            if (tblEvalRptForwardToAa != null) {
                tblEvalRptForwardToAa = null;
            }
%>
