<%-- 
    Document   : GiveClarificationAA
    Created on : Jun 18, 2011, 11:56:11 AM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.EvaluationMatrix"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblEvalReportClarification"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Give Clarification</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>

        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
    </head>
    <body>
        <div class="dashboard_div">
            <%
                String tenderId = "";
                if (request.getParameter("tenderid") != null && !"".equals(request.getParameter("tenderid"))) {
                    tenderId = request.getParameter("tenderid");
                }
                String lotId = "0";
                if (request.getParameter("lotId") != null && !"".equals(request.getParameter("lotId"))) {
                    lotId = request.getParameter("lotId");
                }
                String rId = "";
                if (request.getParameter("rId") != null && !"".equals(request.getParameter("rId"))) {
                    rId = request.getParameter("rId");
                }
            %>
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            </div>
            <div class="contentArea_1">
                <div class="pageHead_1">Give Clarification<span style="float:right;"><a href="Evalclarify.jsp?tenderId=<%=request.getParameter("tenderid")%>&st=rp&comType=TEC" class="action-button-goback">Go Back To Dashboard</a></span></div>
                <!--Dashboard Header End-->
                <%
                            String userId = "";
                            HttpSession hs = request.getSession();
                            if (hs.getAttribute("userId") != null) {
                                userId = hs.getAttribute("userId").toString();
                            }
                            pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                            int evalRptClariId = 0;
                            /*if (request.getParameter("evalRptClariId") != null && !"".equals(request.getParameter("evalRptClariId"))) {
                            evalRptClariId = Integer.parseInt(request.getParameter("evalRptClariId"));
                            }*/
                            int evalRptToAaid = 0;
                            if (request.getParameter("evalRptToAaid") != null && !"".equals(request.getParameter("evalRptToAaid"))) {
                                evalRptToAaid = Integer.parseInt(request.getParameter("evalRptToAaid"));
                            }

                            EvaluationService evaluationService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                            List<TblEvalReportClarification> tblEvalReportClarifications = evaluationService.fetchDataFromEvalReportClari(evalRptToAaid);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <form id="frmGiveClari" name="frmGiveClari" action="<%=request.getContextPath()%>/EvaluationServlet?action=giveClari" method="post">
                    <table width="100%" cellspacing="0" cellpadding="5" class="tableList_1">
                        <%
                                String evalType = "eval";
                                if(request.getParameter("stat")!=null){
                                    evalType = request.getParameter("stat");
                                }
                                List<SPTenderCommonData> tenderLotList = tenderCommonService.returndata("GetLotPkgDetail", tenderId, lotId);
                                String lotPkgNo = null;
                                String lotPkgDesc = null;
                                if ("0".equals(lotId)) {
                                    lotPkgNo = "Package No.";
                                    lotPkgDesc = "Package Description";
                                } else {
                                    lotPkgNo = "Lot No.";
                                    lotPkgDesc = "Lot Description";
                                }
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <jsp:include page="TERInclude.jsp">
                                <jsp:param name="tenderId" value="<%=tenderId%>"/>
                                <jsp:param name="lotId" value="<%=lotId%>"/>
                                <jsp:param name="roundId" value="<%=rId%>"/>
                                <jsp:param name="evalType" value="<%=evalType%>"/>
                            </jsp:include>
                            <%                                              for (TblEvalReportClarification clarification : tblEvalReportClarifications) {
                                            if (clarification.getAnswerDt() == null) {
                                                evalRptClariId = clarification.getEvalRptClariId();
                            %>
                            <input type="hidden" id="evalRptClariId" name="evalRptClariId" value="<%=clarification.getEvalRptClariId()%>"/>
                            <%
                                            }
                                        }
                                        List<TblEvalReportClarification> listing = evaluationService.fetchDataFromERC(evalRptClariId);
                            %>
                            <tr style="display: none;" id="trHideQuery">
                                <td class="ff">Query :</td>
                                <td><input type="hidden" name="rfpNo" value="<%=tenderLotList.get(0).getFieldName3()%>"/>
                                    <input type="hidden" name="lotPkgNo" value="<%=lotPkgNo%>"/>
                                    <input type="hidden" name="lotPkgDesc" value="<%=lotPkgDesc%>"/>
                                    <input type="hidden" name="rId" value="<%=rId%>"/>
                                    <%if (!listing.isEmpty() && listing.get(0).getQuery() != null) {%><%=listing.get(0).getQuery()%><%}%></td>
                            </tr>
                            <tr style="display: none;" id="trHideDate">
                                <td class="ff">Last Date and Time for Response :</td>
                                <td><%if (!listing.isEmpty() && listing.get(0).getLastResponseDate() != null) {%><%=DateUtils.gridDateToStr(listing.get(0).getLastResponseDate())%>
                                    <input type="hidden" name="lastDateTime" value="<%=DateUtils.gridDateToStr(listing.get(0).getLastResponseDate()) %>"/>
                                    <%}%></td>
                            </tr>

                            <tr style="display: none;" id="trHideResponse">
                                <td valign="top" class="ff">Response : <span class="mandatory" >*</span></td>
                                <td><textarea cols="100" rows="5" id="txtaResponse" name="response" class="formTxtBox_1"></textarea>
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        CKEDITOR.replace( 'response',
                                        {
                                            toolbar : "egpToolbar"

                                        });
                                        //]]>
                                    </script>
                                </td>
                            </tr>
                        </table>
                        <div id="btnHide" style="display: none;" class="t_space t-align-center">
                            <input type="hidden" name="evalRptToAaid" value="<%=evalRptToAaid%>"/>
                            <input type="hidden" name="tenderId" value="<%=tenderId%>"/>
                            <label class="formBtn_1">
                                <input  id="Submit" name="Submit" value="Submit" type="submit" onclick="return validation();"/>
                            </label>
                        </div>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_3 t_space">
                        <tr>
                            <th width="4%" class="ff">Sl. No.</th>
                            <th width="40%">Query</th>
                            <th width="13%">Date and Time<br />of Query </th>
                            <th width="31%">Response</th>
                            <th width="12%">Date and Time<br />of Response </th>
                        </tr>
                        <%                            int srno = 1;
                                    //EvaluationService evaluationService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                                    for (TblEvalReportClarification listingdata : tblEvalReportClarifications) {
                        %>
                        <tr>
                            <td class="t-align-center"><%=srno%></td>
                            <td><%=listingdata.getQuery()%></td>
                            <td><%=DateUtils.gridDateToStr(listingdata.getQueryDt())%></td>
                            <td><%if (!listingdata.getAnswer().equals("")) {%><%=listingdata.getAnswer()%><% } else {%> - <% }%></td>
                            <td><%if (listingdata.getAnswerDt() != null) {%><%=DateUtils.gridDateToStr(listingdata.getAnswerDt())%><% } else {%> -
                                <script type="text/javascript">
                                    $('#btnHide').show();
                                    $('#trHideResponse').show();
                                    $('#trHideQuery').show();
                                    $('#trHideDate').show();
                                </script>
                                <% }%></td>
                        </tr>
                        <% srno++;
                                    }%>
                    </table>
                </form>

            </div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        function validation(){
            var bVal = true;
            $(".err").remove();
            if(CKEDITOR.instances.txtaResponse != null){
                //if(document.getElementById('txtaexpRequired')!=null){
                if(!required($.trim(CKEDITOR.instances.txtaResponse.getData())) || isCKEditorFieldBlank($.trim(CKEDITOR.instances.txtaResponse.getData().replace(/<[^>]*>|\s/g, ''))))
                {
                    $("#txtaResponse").parent().append("<div class='err' style='color:red;'>Please enter Response</div>");
                    bVal=false;
                }
                else
                {
                    if(!Maxlenght(CKEDITOR.instances.txtaResponse.getData(),'2000')){
                        $("#txtaResponse").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters are allowed</div>");
                        bVal=false;
                    }
                }
            }
            if(!bVal){
                return false;
            } else {
                document.getElementById("Submit").style.display = 'none';
            }
        }

        function required(controlid)
        {
            var temp=controlid.length;
            if(temp <= 0 ){
                return false;
            }else{
                return true;
            }
        }
        function Maxlenght(controlid,maxlenght)
        {
            var temp=controlid.length;
            if(temp>=maxlenght){
                return false;
            }else
                return true;
        }
    </script>
</html>
