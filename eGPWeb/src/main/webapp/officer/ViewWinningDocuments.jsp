<%-- 
    Document   : ViewWinningDocuments.jsp
    Created on : Apr 20, 2015, 11:43:10 AM
    Author     : Istiak (Dohatec)
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalCommonSearchService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderForms"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderSection"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="comDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Previous Documents</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <%
                    String tenderId = request.getParameter("tenderId");
                    String pkgLotId = request.getParameter("pkgLotId");
                    String currentUserId = request.getParameter("cuId");
                    String oldTenderId = request.getParameter("oldTenderId");
                    String userId = request.getParameter("puId");
                    String docId = request.getParameter("docId");
        %>
        <script type="text/javascript">
            
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Header End-->

            <div class="contentArea_1">
                <div class="pageHead_1">Previous Documents
                    <span class="c-alignment-right">
                        <%if("s".equalsIgnoreCase(request.getParameter("flag"))){%>
                            <a href="ViewWinningMappedDocuments.jsp?tenderId=<%=tenderId%>&pkgLotId=<%=pkgLotId%>&uId=<%=currentUserId%>&docId=<%=docId%>&flag=s" class="action-button-goback">Go back</a>
                        <%}else{%>
                            <a href="ViewWinningMappedDocuments.jsp?tenderId=<%=tenderId%>&pkgLotId=<%=pkgLotId%>&uId=<%=currentUserId%>&docId=<%=docId%>" class="action-button-goback">Go back</a>
                        <%}%>
                    </span>
                </div>
                <%
                            pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <div>&nbsp;</div>

                <div class="tabPanelArea_1">

                    <!--*****************************************************************************************************************-->

                    <%
                    int comTenderStdId = comDocSrBean.getTenderSTDId(Integer.parseInt(oldTenderId));
                    List<TblTenderSection> tSections = comDocSrBean.getTenderSections(comTenderStdId);
                    
                    CommonService comcommonService = (CommonService)AppContext.getSpringBean("CommonService");
                    String comEventType = comcommonService.getEventType(oldTenderId).toString();
                    String comProcNature = comcommonService.getProcNature(oldTenderId).toString();
                    String comProcMethod = comcommonService.getProcMethod(oldTenderId).toString();
                    boolean isTenderPkgWise = false;
                    
                    int comTemplateId = 0;
                    int comDisp = 0;
                    int sectionId = 0;
                    String str_FolderName = "";
                    short disp = 0;
                    for (TblTenderSection tts : tSections){
                        disp++;

                        if("Form".equalsIgnoreCase(tts.getContentType())){
                    %>
                    
                    <table class="tableList_1" width="100%" cellspacing="0">
                        <tbody>
                            <tr>
                                <td class="ff" colspan="5"> <%=tts.getSectionName()%> </td>
                            </tr>
                        </tbody>
                    </table>                    
                    <br/>
                    
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th style="text-align:center; width:4%;">Sl. No.</th>
                            <th width="33%">Form Name </th>
                            <th width="63%">Map Document List</th>

                        </tr>
                    <%
                       List<TblTenderForms> forms = comDocSrBean.getTenderForm(tts.getTenderSectionId());
                       int count=0;
                       List<Object[]> comDoc = comDocSrBean.commonDocMapList(oldTenderId, userId);
                        List<Object[]> winDoc = comDocSrBean.mappedWinningDocMapList(tenderId, userId);
                       for(int i=0;i<forms.size();i++){
                           if(!("c".equalsIgnoreCase(forms.get(i).getFormStatus())|| "createp".equalsIgnoreCase(forms.get(i).getFormStatus()))){
                           count++;
                    %>
                        <tr>
                            <td><%=count%></td>
                            <td><%out.print(forms.get(i).getFormName().replace("?s", "'s"));%></td>
                        <%
                            boolean isMapped = false;
                                if(comDoc!=null && !comDoc.isEmpty()){
                                    for(int j=0;j<comDoc.size();j++){
                                        if(forms.get(i).getTenderFormId()==Integer.parseInt(comDoc.get(j)[0].toString())){
                                            isMapped = true;
                                        }
                                    }
                                }if(isMapped){
                        %>

                            <td>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                            <th width="85%">Document Name </th>
                            <th width="10%" class="t-align-center">Action</th>
                            </tr>
                            <%for(int j=0;j<comDoc.size();j++){
                                for(int k=0;k<winDoc.size();k++){

                                        if(forms.get(i).getTenderFormId()==Integer.parseInt(comDoc.get(j)[0].toString())){
                                            
                                                    if(Integer.parseInt(docId)==Integer.parseInt(comDoc.get(j)[3].toString())){
                            %>
                                            <tr>
                                                <td><%=comDoc.get(j)[1]%></td>
                                                <td class="t-align-center"><a view='link' href="<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=download&fileName=<%=comDoc.get(j)[1]%>&fileLen=<%=comDoc.get(j)[2]%>&uIdDoc=<%=userId%>&docUid=<%=userId%>" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a></td>
                                            </tr>
                                        <% break;}
                                        }
                                        }
                            }%>

                        </table>
                            </td></tr>
                        <%}/*Mapped Doc List*/else{%>
                        <td>-</td></tr>
                        <%}%>
                    <%}/*Cancled form is not displayed*/}/*Form loop Ends here*/%>
                    </table><br/>
                    <%}/*Form Condition Ends here*/%>

                    <%}/*For loop ends of tenderSection*/%>
                    
                   <!--*****************************************************************************************************************-->
                </div>
            </div>
            <!--Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Footer End-->
        </div>
    </body>
</html>
