<%-- 
    Document   : TSCSeekClarificaiton
    Created on : Dec 7, 2010, 6:23:01 PM
    Author     : Rajesh Singh
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Officer - Evaluation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <title>Committe Evalution</title>
        <!--jalert -->

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <link type="text/css" rel="stylesheet" href="../resources/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/lang/en.js"></script>

        <script type="text/javascript" >
            function checkTender(){
                jAlert("there is no Tender Notice"," Alert ", "Alert");
            }


            function checkdefaultconf(){
                jAlert("Default workflow configuration hasn't crated"," Alert ", "Alert");
            }
            function checkDates(){
                jAlert(" please configure dates"," Alert ", "Alert");
            }

            //START -- Function calling Calender to display.
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });


                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            //END -- Function calling Calender to display.

            //START -- Jquery Validation for this Page.
            $(document).ready(function() {
                $("#frmClarification").validate({
                    rules: {
                        txtTSCreport: { required: true},
                        txtexpDate:{required:true}
                    },
                    messages: {
                        txtTSCreport: { required: "<div class='reqF_1'>Please enter TSC Report Clarification.</div>"},
                        txtexpDate:{required:"<div class='reqF_1'>Please Select Expected Date of Clarification.</div>"}
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "txtexpDate")
                            error.insertAfter("#txtexpDateImg");
                        else
                            error.insertAfter(element);
                    }

                });
            });
            //END -- Jquery Validation for this Page.

        </script>
    </head>
    <body>
        <%--START -- Onclick Event.--%>
        <%
        HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
        java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    if (request.getParameter("btnExpDate") != null) {
                        String dtXml = "";

                        String dt = request.getParameter("txtexpDate");

                        String[] dtArr = dt.split("/");
                        dt = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0];

                        String userId = "";
                                HttpSession hs = request.getSession();
                                if (hs.getAttribute("userId") != null) {
                                    userId = hs.getAttribute("userId").toString();
                                }

                                

                        dtXml = "<root><tbl_EvalReportQry evalRptReqId=\"" + request.getParameter("rrid") + "\" "
                                + "evalRptId=\"" + request.getParameter("req") + "\" "
                                + "reportQuery=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("txtTSCreport")) + "\" "
                                + "expRspDt=\"" + dt + "\" "
                                + "sentBy=\"" + userId + "\" "
                                + "sentTo=\"" + request.getParameter("Uid") + "\" "
                                + "reportResponse=\"\" "
                                + "reportResponseDt=\"\" "
                                + "reportQryDt=\""+ format.format(new Date()) +"\"/></root>";

                        
                        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                        CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalReportQry", dtXml, "").get(0);
                        //out.println(commonMsgChk.getMsg());
                        if(commonMsgChk.getFlag()==true)
                        {
                            response.sendRedirect("EvalCommTSC.jsp?tenderid="+request.getParameter("tenderid"));
                        }
                        else
                            {out.println(commonMsgChk.getMsg());}
                    }
        %>
        <%--END -- Onclick Event.--%>

        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="pageHead_1">Tender Dashboard</div>
        <%
                    // Variable tenderId is defined by u on ur current page.
                    pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                    pageContext.setAttribute("tab", "7");
                    //pageContext.setAttribute("TSCtab", "1");
%>
        <%@include file="../resources/common/TenderInfoBar.jsp" %>

        <div class="tabPanelArea_1">
            <%
                        CommitteMemberService committeMemberService = (CommitteMemberService) AppContext.getSpringBean("CommitteMemberService");
                        int uid = 0;
                        if (session.getAttribute("userId") != null) {
                            Integer ob1 = (Integer) session.getAttribute("userId");
                            uid = ob1.intValue();
                        }
                        WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                        short eventid = 6;
                        int objectid = 0;
                        short activityid = 6;
                        int childid = 1;
                        short wflevel = 1;
                        int initiator = 0;
                        int approver = 0;
                        boolean isInitiater = false;
                        boolean evntexist = false;
                        String fileOnHand = "No";
                        String checkperole = "No";
                        String chTender = "No";
                        boolean ispublish = false;
                        boolean iswfLevelExist = false;
                        boolean isconApprove = false;
                        String evalCommit = "";
                        String objtenderId = request.getParameter("tenderid");
                        if (objtenderId != null) {
                            objectid = Integer.parseInt(objtenderId);
                            childid = objectid;
                        }


                        evntexist = workFlowSrBean.isWorkFlowEventExist(eventid, objectid);
                        List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(objectid, childid, activityid);
                        // workFlowSrBean.getWorkFlowConfig(objectid, activityid, childid, wflevel,uid);

                        
                        if (tblWorkFlowLevelConfig.size() > 0) {
                            Iterator twflc = tblWorkFlowLevelConfig.iterator();
                            iswfLevelExist = true;
                            while (twflc.hasNext()) {
                                TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                if (wflevel == workFlowlel.getWfLevel() && uid == lmaster.getUserId()) {
                                    fileOnHand = workFlowlel.getFileOnHand();
                                }
                                if (workFlowlel.getWfRoleId() == 1) {
                                    initiator = lmaster.getUserId();
                                }
                                if (workFlowlel.getWfRoleId() == 2) {
                                    approver = lmaster.getUserId();
                                }
                                if (fileOnHand.equalsIgnoreCase("yes") && uid == lmaster.getUserId()) {
                                    isInitiater = true;
                                }

                            }

                        }


                        String userid = "";
                        if (session.getAttribute("userId") != null) {
                            Object obj = session.getAttribute("userId");
                            userid = obj.toString();
                        }
                        String field1 = "CheckPE";
                        List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(field1, userid, "");

                        if (editdata.size() > 0) {
                            checkperole = "Yes";
                        }
                        
                        // for publish link

                        TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        List<SPTenderCommonData> checkEval = tenderCommonService1.returndata("EvalComWorkFlowStatus", request.getParameter("tenderid"), "'Pending'");
                        
                        if (checkEval.size() > 0) {
                            chTender = "Yes";
                        }
                        List<SPTenderCommonData> publist = tenderCommonService1.returndata("EvalComWorkFlowStatus", request.getParameter("tenderid"), "'Approved','Conditional Approval'");
                        
                        if (publist.size() > 0) {
                            ispublish = true;
                            SPTenderCommonData spTenderCommondata = publist.get(0);
                            evalCommit = spTenderCommondata.getFieldName1();
                            String conap = spTenderCommondata.getFieldName2();
                            if (conap.equalsIgnoreCase("Conditional Approval")) {
                                isconApprove = true;
                            }
                        }
                        
            %>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <td width="10%" class="t-align-center">1</td>
                    <td width="30%" class="t-align-center">Evaluation Committee</td>
                    <td width="80%" class="t-align-center">
                        <%if ((committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + request.getParameter("tenderid") + " and tc.committeeType in ('TEC','PEC')") == 0)) {%><a href="CommFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>">Create</a>&nbsp;|&nbsp;
                        <%} else {
                            if ((committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + request.getParameter("tenderid") + " and tc.committeeType in ('TEC','PEC') and tc.committeStatus='pending'") == 0)) {%><a href="CommFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>&isview=y">View</a>&nbsp; | &nbsp;<%} else {%>

                        <%  if ((fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true && ispublish == false && chTender.equalsIgnoreCase("Yes"))
                                                                || (isconApprove == true && fileOnHand.equalsIgnoreCase("Yes") && isInitiater == true) || iswfLevelExist == false) {%>
                        <a href="CommFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>&isedit=y">Edit</a>&nbsp;| &nbsp;
                        <% }%>
                        <a href="CommFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>&isview=y">View</a>&nbsp; |&nbsp;
                        <% if ((ispublish == true && !evalCommit.equalsIgnoreCase("Approved")) || (initiator == approver && initiator != 0 && approver != 0)) {%>
                        <a href="CommFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>&ispub=y">Publish</a>
                        &nbsp;| &nbsp;
                        <% }%>
                        <%}
                                    }%>

                        <%if ((committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + request.getParameter("tenderid") + " and tc.committeeType='TSC'") == 0)) {%><a href="TSCFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>">Create TSC</a>&nbsp;|&nbsp;
                        <%} else {
                            if ((committeMemberService.checkCountByHql("TblCommittee tc", "tc.tblTenderMaster.tenderId=" + request.getParameter("tenderid") + " and tc.committeeType='TSC' and tc.committeStatus='pending'") == 0)) {%><a href="TSCFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>&isview=y">View TSC</a> &nbsp;| &nbsp;<%} else {%><a href="TSCFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>&isedit=y">Edit TSC</a> &nbsp; | &nbsp;<a href="TSCFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>&isview=y">View TSC</a> &nbsp;| &nbsp;<a href="TSCFormation.jsp?tenderid=<%=request.getParameter("tenderid")%>&ispub=y">Publish TSC</a><%}
                                        }%>
                        <%
                                    if (isInitiater == true && initiator != approver && ispublish == false && fileOnHand.equalsIgnoreCase("Yes")) {
                                        %>
                        <% if (chTender.equalsIgnoreCase("Yes")) {%>
                        <a href="FileProcessing.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&fromaction=eval" >Process in Workflow</a>
                        <% }
                                    }%>
                        &nbsp;| <br />&nbsp;| &nbsp;<a href="#">Use Existing Committee</a>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td width="10%" class="t-align-center">2</td>
                    <td width="30%" class="t-align-center">WorkFlow</td>
                    <td width="80%" class="t-align-center">
                        <% if (evntexist) {

                                        if (fileOnHand.equalsIgnoreCase("yes") && checkperole.equals("Yes") && ispublish == false && isInitiater == true) {

                        %>
                        <a class="action-button-edit" href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>'>Edit</a>

                        &nbsp;&nbsp;
                        <a class="action-button-view" href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>">View</a>

                        <% } else if (iswfLevelExist == false && checkperole.equals("Yes") && ispublish == false) {
                        %>

                        <a class="action-button-edit" href='CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Edit&childid=<%=childid%>'>Edit</a>
                        <a class="action-button-view" href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>">View</a>
                        <%  } else if (iswfLevelExist == true) {%>
                        <a class="action-button-view" href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=View&childid=<%=childid%>">View</a>

                        <%
                             }
                         } else if (checkperole.equals("Yes")) {
                             List<CommonAppData> defaultconf = workFlowSrBean.editWorkFlowData("WorkFlowRuleEngine", Integer.toString(eventid), "");
                             if (defaultconf.size() > 0) {
                        %>
                        <a class="action-button-add" href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=<%=eventid%>&objectid=<%=objectid%>&action=Create&childid=<%=childid%>">Create</a>
                        <% } else {%>
                        <a class="action-button-add" href="#" onclick="checkdefaultconf()">Create</a>
                        <%                         }
                         }
                        %>


                        <% if (iswfLevelExist) {%>
                        <a href="workFlowHistory.jsp?activityid=<%=activityid%>&objectid=<%=objectid%>&childid=<%=childid%>&eventid=<%=eventid%>&userid=<%=uid%>&fraction=eval" >View Workflow History</a>

                        <% }%>

                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <form id="frmClarification" action="" method="post">
                <div class="tabPanelArea_1">
                    <%-- Sub Panal --%>
                    <%
                                pageContext.setAttribute("TSCtab", "2");
                    %>
                    <%@include  file="../resources/common/AfterLoginTSC.jsp"%>

                    <table class="tableList_1" width="100%" border="0" cellpadding="5" cellspacing="0">
                        <tbody><tr>

                                <td class="t-align-left ff" width="16%">TSC Report :</td>
                                <td class="t-align-left" width="84%">
                                    <textarea rows="5" id="txtTSCreport" name="txtTSCreport" class="formTxtBox_1" style="width: 95%;"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Expected Date of Clarification :</td>

                                <td class="t-align-left"><input name="txtexpDate" class="formTxtBox_1" id="txtexpDate" style="width: 70px;" readonly="readonly" type="text" onclick="GetCal('txtexpDate','txtexpDate');">
                                    <a id="txtexpDateImg" href="javascript:void(0);" onclick="GetCal('txtexpDate','txtexpDateImg');" title="Calender"><img src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" border="0"></a></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="t-align-center">
                                    <label class="formBtn_1">
                                        <input id="btnExpDate" name="btnExpDate" value="Submit" type="submit">
                                    </label>
                                </td>

                            </tr>
                        </tbody></table>

                    <div>&nbsp;</div>

                </div>
            </form>
        </div>
        <br/>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
