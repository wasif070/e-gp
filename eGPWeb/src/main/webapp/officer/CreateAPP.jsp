
<%--
Document   : Create APP
Created on : Oct 23, 2010, 6:13:00 PM
Author     : Sanjay
--%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.APPServiceImpl"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.AppMessage"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.web.databean.APPDtBean"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.APPService"%>
<%@page buffer="15kb"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create Annual Procurement Plan (APP)</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />        
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script>
            $(document).ready(function() {               
                
                $('#cmbBudgetType').attr("disabled", false);
                $('#txtEntrustingAgency').blur(function(){ 
                    
                    var EntrustingAgency = $("#txtEntrustingAgency").val();
                    if($.trim(EntrustingAgency)!=""){
                       $('#msgEntrustAgency').html(''); 
                    }
                    else{
                       $('#msgEntrustAgency').html('<br/>Please enter Reference Letter Ref. No.');  
                    }
                    
                    $('#msgEntrustingAgency').html('');
                    $('#isValidEntrustingAgency').val('f');
                    
                    if($.trim($(this).val()).length > 0)
                    {                    
                        $.post("<%=request.getContextPath()%>/APPServlet", 
                        {
                            param1:$(this).val(),
                            funName:'getOfficeInfoFromAPPDepositWork'},
                            function(j){
                                if(j.toString().length > 0)
                                {
                                    if(j.toString() == "The reference APP is not Approved.")
                                    {
                                        //$('#isValidEntrustingAgency').val('f');
                                        $('#msgEntrustingAgency').html('<b style="color:red">The reference APP is not Approved.</b>');
                                    }
                                    else if(j.toString() == "Tender is created from the reference APP.")
                                    {
                                        //$('#isValidEntrustingAgency').val('f');
                                        $('#msgEntrustingAgency').html('<b style="color:red">Tender is created from the reference APP.</b>');
                                    }
                                    else if(j.toString() == "The reference APP is already Deposited.")
                                    {
                                        //$('#isValidEntrustingAgency').val('f');
                                        $('#msgEntrustingAgency').html('<b style="color:red">The reference APP is already Deposited.</b>');
                                    }
                                    else if(j.toString() == "Reference APP is Deposit Work type.")
                                    {
                                        //$('#isValidEntrustingAgency').val('f');
                                        $('#msgEntrustingAgency').html('<b style="color:red">Reference APP is Deposit Work type.</b>');
                                    }
                                    else
                                    {
                                        $('#isValidEntrustingAgency').val('t');
                                        $('#msgEntrustingAgency').html('Agency: '+'<b style="color:green">'+j.toString()+' </b>');
                                    }
                                }
                                else
                                {
                                    $('#msgEntrustingAgency').html('<b style="color:red">No Agency found</b>');
                                }
                            });
                    }
                });
                
                //Nitish Start
                
                $('#cmbBudgetType').blur(function()
                {  
                    var budgetType = $("#cmbBudgetType").val();
                    
                    /*if(document.getElementById("cmbBudgetType").value =="1" || document.getElementById("cmbBudgetType").value =="2" || document.getElementById("cmbBudgetType").value =="3")
                    {
                        $('#msgBudgetType').html('');
                    }*/
                    if(budgetType !="")
                    {
                        $('#msgBudgetType').html('');
                    }
                    else
                    {
                        $('#msgBudgetType').html('Please select Budget Type.'); 
                    }
                });
        
                //Nitish END
                
                $('#txtAppCode').blur(function(){  
                    var appCode = $("#txtAppCode").val();
                    if($.trim(appCode)!=""){
                       $('#msgappCode').html(''); 
                    }
                    else{
                       $('#msgappCode').html('Please enter Letter Ref. No.');  
                    }
                    $('#msgTxtAppCode').html('');
                    $('#isValidtxtAppCode').val('f');
                     //alert($(this).val());
                    if($.trim($(this).val()).length > 0)
                    {  
                        if(/\s/.test($(this).val())) {
                            $('#msgappCode').html('White space is not allowed in Letter Ref. No.');
                        }  
                        else
                        {              
                            $.post("<%=request.getContextPath()%>/APPServlet", 
                            {
                                param1:$(this).val(),
                                funName:'getOfficeInfoFromAPP'},
                                function(j){
                                    if(j.toString().length > 0)
                                    {
                                        //alert(j.tostring());
                                        $('#isValidtxtAppCode').val('f');
                                        $('#msgTxtAppCode').html('Letter Ref. No. Already Exists');
                                        $('#msgTxtAppCode').css("color","Red");
                                    }
                                    else
                                    {
                                        $('#isValidtxtAppCode').val('t');
                                        $('#msgTxtAppCode').html('OK');
                                        $('#msgTxtAppCode').css("color","green");
                                    }
                                });
                        }    
                    }
                });
             
             });

        </script>
        <%
                StringBuilder userType = new StringBuilder();
                int userId = 0;

                userType.append("org");
                if (request.getParameter("hdnUserType") != null && !"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                    userType.append(request.getParameter("hdnUserType"));
                }

                if (session.getAttribute("userId") != null&& !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = Integer.parseInt(session.getAttribute("userId").toString());
                }
        %>
    </head>
    <jsp:useBean id="appServlet" scope="request" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
    <jsp:useBean id="appDtBean" scope="request" class="com.cptu.egp.eps.web.databean.APPDtBean"/>    
    <jsp:useBean id="appViewDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AppViewPkgDtBean"/>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">

                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <!--Page Content Start-->
                <%
                            
                            String uId="0";
                            if(session.getAttribute("userId")!=null){
                                uId=session.getAttribute("userId").toString();
                            }
                            appServlet.setLogUserId(uId);
                            java.util.List<CommonAppData> appListDtBean = new java.util.ArrayList<CommonAppData>();
                            if ("createapp".equalsIgnoreCase(request.getParameter("action"))) {
                                try {
                                    int budgetType = 0;
                                    int prjId = 0;
                                    String msg = "";
                                    String prjName = "";
                                    int peOfficeId = 0;
                                    int peORauId = 0;
                                    int peEmpId = 0;
                                    int officeId = 0;
                                    String ActivityName = "";
                                    //added by rased                                    
                                    String appType = ""; 
                                    String entrustingAgency = ""; 
                                    String refAppCode = "";
                                    Integer refAppId = null; 
                                    int revisionCount = 0;
                                    Date lastRevisionDate = null;
                                    
                                    if (request.getParameter("entrustagency") != null && !"".equalsIgnoreCase(request.getParameter("entrustagency"))) {
                                        refAppCode = request.getParameter("entrustagency");                                   
                                        APPService appService = (APPService) AppContext.getSpringBean("APPService");
                                        CommonAppData app1 = appService.getAPPFromCode(refAppCode);
                                        refAppId = Integer.parseInt(app1.getFieldName1());                                    
                                        entrustingAgency = appService.getOfficeInfoFromAPP("AppCode", refAppCode).getFieldName1();
                                    }       

                                    if (request.getParameter("budgetType") != null && !"".equalsIgnoreCase(request.getParameter("budgetType"))) {
                                        budgetType = Integer.parseInt(request.getParameter("budgetType"));
                                    }
                                    
                                    if (request.getParameter("depoplanWork") != null && !"".equalsIgnoreCase(request.getParameter("depoplanWork"))) {
                                        appType = request.getParameter("depoplanWork");
                                    }

                                    if (request.getParameter("project") != null && !"".equalsIgnoreCase(request.getParameter("project"))) {
                                        prjId = Integer.parseInt(request.getParameter("project"));
                                    }

                                    if (request.getParameter("hdnPrjName") != null && !"".equalsIgnoreCase(request.getParameter("hdnPrjName"))) {
                                        if(request.getParameter("hdnPrjName").equalsIgnoreCase("- Select Project -"))
                                        {
                                            prjName = "";
                                        }
                                        else
                                        {
                                            prjName = request.getParameter("hdnPrjName");
                                        }
                                    }

                                    if (request.getParameter("hdnPO") != null && !"".equalsIgnoreCase(request.getParameter("hdnPO"))) {
                                        peOfficeId = Integer.parseInt(request.getParameter("hdnPO"));
                                    }

                                    if (request.getParameter("hdnPE") != null && !"".equalsIgnoreCase(request.getParameter("hdnPE"))) {
                                        peORauId = Integer.parseInt(request.getParameter("hdnPE"));
                                    }
                                    
                                    if(peORauId ==0 && request.getParameter("hdnAU") != null && !"".equalsIgnoreCase(request.getParameter("hdnAU")))
                                    {
                                        peORauId = Integer.parseInt(request.getParameter("hdnAU"));
                                    }

                                    if (request.getParameter("projectoffice") != null && !"".equalsIgnoreCase(request.getParameter("projectoffice"))) {
                                        officeId = Integer.parseInt(request.getParameter("projectoffice").substring(0, request.getParameter("projectoffice").indexOf("_")));
                                    }
                                    
                                    if (request.getParameter("ActivityName") != null && !"".equalsIgnoreCase(request.getParameter("ActivityName"))) {
                                        ActivityName = request.getParameter("ActivityName");
                                    }

                                    int deptId = officeId;
                                    
                                    appServlet.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                    //if(budgetType==1){
                                    //    msg  = appServlet.addAPPDetail(request.getParameter("hdnFinYear"), budgetType, prjId, prjName, officeId, peEmpId, peEmpId, request.getParameter("appcode"), userId, deptId, 1, "create", 0);
                                    //}else{
                                 
                                    msg = appServlet.addAPPDetail(request.getParameter("hdnFinYear"), budgetType, prjId, prjName, peOfficeId, peORauId, peORauId, request.getParameter("appcode"), userId, deptId, 1, "create", 0, appType, entrustingAgency, refAppId, revisionCount, lastRevisionDate,ActivityName);
                                    //}
                                    if (msg.contains("Success")) {
                                        response.sendRedirect("AddPackageDetail.jsp?appId="+msg.substring(msg.indexOf("_")+1, msg.length()));
%>
<!--                <form id="frmInsUpdate" name="frmInsUpdate">
                    <input type="hidden" name="appId" id="appId" value="<%=msg.substring(msg.indexOf("_") + 1, msg.length())%>"/>
                </form>
                <script>
                    document.getElementById("frmInsUpdate").action = "AddPackageDetail.jsp";
                    document.getElementById("frmInsUpdate").submit();
                </script>-->
                <%
                                    }
                                } catch (Exception e) {

                                    response.sendRedirect("CreateAPP.jsp?msg=Error");
                                }
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <div class="pageHead_1">Create Annual Procurement Plan</div>
                            <% if (request.getParameter("msg") != null && "Error".equalsIgnoreCase(request.getParameter("msg"))) { %>
                                <div class="responseMsg errorMsg"><%=appMessage.createAPPErrMsg%></div>
                            <% } %>
                            <div class="stepWiz_1 t_space">
                                <ul>
                                    <li class="sMenu">Create Annual Procurement Plan</li>
                                    <li>&gt;&gt;&nbsp;&nbsp;Add Package Details</li>
                                    <li>&gt;&gt;&nbsp;&nbsp;Add Package Dates</li>
                                </ul>
                            </div>

                            <form method="POST" id="frmCreateAPP" action="CreateAPP.jsp">
                                <input type="hidden" name="action" id="idaction" value="createapp" />
                                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1 t_space">
                                    <tr>
                                                <td style="font-style: italic" class="ff t-align-left" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Budget Type : <span>*</span></td>
                                        <td>
                                            <select name="budgetType" class="formTxtBox_1" id="cmbBudgetType" style="width:200px;" disabled>
                                                <option value="" >-- Select Budget Type --</option>
                                                <option value="1">Capital Budget</option>
                                                <option value="2">Recurrent Budget</option>
                                                <option value="3">Own Fund</option>
                                            </select>
                                            <br/>
                                            <span style="color: red;" id="msgBudgetType"></span>
                                        </td>
                                    </tr>
                                    <%appListDtBean = appServlet.getAPPDetails("FinancialYear", "", "");%>
                                    <tr>
                                        <td class="ff">Financial Year : <span>*</span></td>
                                        <td>
                                            <select name="financialyear" class="formTxtBox_1" id="cmbFinancialYear" style="width:200px;" onChange="setFinYear(this);">
                                                <%String defaultValue = appListDtBean.get(0).getFieldName2();
                                                            if (appListDtBean != null && !appListDtBean.isEmpty()) {
                                                                for (CommonAppData commonApp : appListDtBean) {
                                                %>
                                                <option <%if ("Yes".equalsIgnoreCase(commonApp.getFieldName3())) {
                                                                    out.print("selected");
                                                                    defaultValue = commonApp.getFieldName2();
                                                                }
                                                        else{
                                                            out.print("disabled");
                                                        }     
                                                %> value="<%=commonApp.getFieldName1()%>"><%=commonApp.getFieldName2()%></option>
                                                <%
                                                                }
                                                            }
                                                %>
                                            </select>
                                            <input type="hidden" name="hdnFinYear" id="hdnFinYear" value="<%=defaultValue%>"/>
                                        </td>
                                    </tr>
                                        
                                    <%//appListDtBean = appServlet.getAPPDetails("AppProject", String.valueOf(userId), "");%>
                                    <tr>
                                        <td class="ff">Select Project : <span id="spanPrj" name="spanPrj" style="visibility:hidden">*</span></td>
                                        <td>
                                            <select class="formTxtBox_1" name="project" id="cmbProject" style="width:200px;" onChange="setPrjName(this);">
                                                <option value="">- Select Project -</option>
                                            </select>
                                            <span id="errPrjMsg" style="color: red;"></span>
                                            <input type="hidden" name="ProjectSourceOfFund" id="ProjectSourceOfFund"/>
                                            <input type="hidden" name="hdnPrjName" id="hdnPrjName"/>
                                            <input type="hidden" name="hdnPrjEndDate" id="hdnPrjEndDate"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            
                                        </td>
                                        <td>
                                            <b>Or</b>
                                        </td>
                                    </tr>
                                    <tr id="ActivityNameRow">
                                        <td class="ff">Activity Name : <span style="visibility:hidden">*</span> </td>
                                        <td>
                                            <input name="ActivityName" type="text" class="formTxtBox_1" id="ActivityName" style="width:186px;" />
                                        </td>
                                    </tr>
                                    <%appListDtBean = appServlet.getAPPDetails("PeOffice", String.valueOf(userId), "");%>
                                    <tr>
                                        <td class="ff">PA / Project Office : <span id="spanPO" style="visibility: hidden">*</span></td>
                                        <td>
                                            <select name="projectoffice" class="formTxtBox_1" id="cmbProjectOffice" style="width:200px;display:none">
                                                <option value="">- Select PA / Project Office -</option>
                                                <%
                                                            String strPO = "";
                                                            String strPOId = "";
                                                            if (appListDtBean != null && !appListDtBean.isEmpty()) {
                                                                for (CommonAppData commonApp : appListDtBean) {
                                                %>
                                                <option value="<%=commonApp.getFieldName1()%>_<%=commonApp.getFieldName4()%>"><%=commonApp.getFieldName2()%></option>
                                                <%
                                                                    strPO = commonApp.getFieldName2();
                                                                    strPOId = commonApp.getFieldName1();
                                                                }
                                                            }
                                                %>
                                            </select>
                                            <span id="errPOMsg" style="color: red;"></span>
                                            <label id="lblPO"><%=strPO%></label>
                                            <input type="hidden" name="hdnPO" id="hdnPO" value="<%=strPOId%>"/>
                                        </td>
                                    </tr>
                                    <%  appListDtBean = appServlet.getAPPDetails("Pe", strPOId, ""); %>
                                    <tr>
                                        <td class="ff">PA : <span id="spanPE" style="visibility: hidden">*</span></td>
                                        <td>
                                            <%
                                                        if (appListDtBean != null && !appListDtBean.isEmpty()) {
                                                            for (CommonAppData commonApp : appListDtBean) {
                                            %>
                                            <span id="errPEMsg" style="color: red;"></span>
                                            <label id="lblPE"><%=commonApp.getFieldName2()%></label>
                                            <input type="hidden" name="hdnPE" id="hdnPE" value="<%=commonApp.getFieldName1()%>"/>
                                            <%
                                                            }
                                                        }
                                                        else
                                                        {
                                                            out.print("No PA Available");
                                                            appListDtBean = appServlet.getAPPDetails("IsAuAvailable", strPOId, "");
                                                            if (appListDtBean != null && !appListDtBean.isEmpty()) 
                                                            {
                                                                for (CommonAppData commonApp : appListDtBean) 
                                                                {
                                                                    if(commonApp.getFieldName3().equalsIgnoreCase(session.getAttribute("userId").toString()))
                                                                    {
                                                        %>
                                                                        <input type="hidden" name="hdnAU" id="hdnAU" value="<%=commonApp.getFieldName1()%>"/>
                                                        <%
                                                                    }
                                                                }
                                                            }
                                                           else
                                                            {
                                                        %>
                                                                <input type="hidden" name="hdnAU" id="hdnAU" value="0"/>
                                                        <%
                                                            }
                                                        %>
                                                            <input type="hidden" name="hdnPE" id="hdnPE" value="0"/>
                                                        <%
                                                        }
                                                        %>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff">Letter Ref. No. : <span>*</span></td>
                                        <td>
                                            <input name="appcode" type="text" class="formTxtBox_1" id="txtAppCode" style="width:186px;" />
                                            <br />
                                            <span style="color: red;" id="msgappCode"></span>
                                           <b> <span style="color: red;" id="msgTxtAppCode"></span></b>
                                            <input type="hidden" value="f" id="isValidtxtAppCode"/>                                                                                
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">APP Type : <span>*</span></td>
                                        <td>
                                            <select name="depoplanWork" class="formTxtBox_1" id="cmbdepoplanWork" style="width:200px;" onChange="setDPType(this);" onblur="setMessage(this)">
                                                <option value="">-- Select Category --</option>
                                                <option value="1" >Planned Work</option>
                                                <option value="2">Deposit Work</option>               
                                                <option value="3" >Adhoc Work</option>
                                            </select>
                                            <br/>
                                                <span id="msgdepoplanWork" class="reqF_2"></span>
                                        </td>
                                    </tr>
                                        <tr id="trEntrustingAgency" style="display: none">
                                        <td class="ff">Reference of Letter Ref. No. : <span>*</span></td>
                                        <td>
                                            <input name="entrustagency" type="text" class="formTxtBox_1" id="txtEntrustingAgency" style="width:200px;"/>
                                            <span id="msgEntrustAgency" class="reqF_2"></span>
                                            <br/>
                                            <span id="msgEntrustingAgency"></span>
                                            <input type="hidden" value="f" id="isValidEntrustingAgency"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;<input type="hidden" name="btnNext" id="btnNext" value="Next"/></td>
                                        <td><label class="formBtn_1">
                                                
                                                <input type="submit" name="buttonNext" id="buttonNext" value="Next" onClick="javascirpt: this.disabled=true;if(!validate()){this.disabled=false;return false;}"/>
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                                <div>&nbsp;</div>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Page Content End-->

                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        $(function() {
            $("#frmCreateAPP").validate({
                rules: {
                    budgetType: {required: true},
                    appcode: {required: true,maxlength: 50},
                    entrustagency: {required: $('#cmbdepoplanWork').val()==="2"},
                    depoplanWork: {required:true, minlength: 1}
                },
                messages: {
                    budgetType: { required: "<div class='reqF_1'>Please select Budget Type.</div>"},
                    appcode: { required: "<div class='reqF_1'>Please enter Letter Ref. No.</div>",
                        maxlength:"<div class='reqF_1'>Maximum 50 Characters are allowed</div>"},
                    entrustagency: {required: "Please enter Reference Letter Ref. No."},
                    depoplanWork: {required: "Please select APP Type", minlength: "Please select APP Type"}
                }
            });

            $('#cmbProject').blur(function() {
                if(document.getElementById("cmbBudgetType").value!="" && document.getElementById("cmbBudgetType").value=="1")
                {
                    document.getElementById("ActivityName").disabled = true; 
                    document.getElementById("errPrjMsg").innerHTML ="";
                    var flag = true;

                    if(document.getElementById("cmbProject").value=="" && document.getElementById("ActivityName").value=="")
                    {
                        document.getElementById("ActivityName").disabled = false;
                        document.getElementById("errPrjMsg").innerHTML ="<br/>Please select Project or enter Activity name";
                        flag = false;
                    }
                }
            });
            $('#cmbProject').change(function() {
                if(document.getElementById("cmbBudgetType").value!="" && document.getElementById("cmbBudgetType").value=="1")
                {
                    
                    document.getElementById("errPrjMsg").innerHTML ="";
                    var flag = true;

                    if(document.getElementById("cmbProject").value=="" && document.getElementById("ActivityName").value=="")
                    {
                        
                        document.getElementById("errPrjMsg").innerHTML ="<br/>Please select Project or enter Activity name";
                        flag = false; 
                    }
                    if(document.getElementById("cmbProject").value !="")
                    {
                        document.getElementById("ActivityName").disabled = false;
                    }
                    
                    
                    
                }
                
                if(document.getElementById("cmbBudgetType").value!="" && ((document.getElementById("cmbBudgetType").value=="2") || ( document.getElementById("cmbBudgetType").value=="3")))
                {
                    if(document.getElementById("cmbProject").value!="")
                    {
                        document.getElementById("ActivityName").disabled = true;
                    }
                    else
                    {
                        document.getElementById("ActivityName").disabled = false;
                    }
                }
                
                
            });
            
            $('#ActivityName').blur(function() {
                if(document.getElementById("cmbBudgetType").value!="" && document.getElementById("cmbBudgetType").value=="1")
                {
                    document.getElementById("errPrjMsg").innerHTML ="";
                    var flag = true;

                    if(document.getElementById("cmbProject").value=="" && document.getElementById("ActivityName").value=="")
                    {
                        document.getElementById("errPrjMsg").innerHTML ="<br/>Please select Project or enter Activity name";
                        flag = false;
                    }
                }
            });
            $('#ActivityName').change(function() {
                if(document.getElementById("cmbBudgetType").value!="" && document.getElementById("cmbBudgetType").value=="1")
                {
                    document.getElementById("errPrjMsg").innerHTML ="";
                    var flag = true;
                    
                    if(document.getElementById("ActivityName").value!="")
                    {
                        document.getElementById("cmbProject").disabled = true;
                    }
                    
                    
                    if(document.getElementById("cmbProject").value=="" && document.getElementById("ActivityName").value=="")
                    {
                        
                        document.getElementById("errPrjMsg").innerHTML ="<br/>Please select Project or enter Activity name";
                        flag = false;
                    }
                    
                }
                if(document.getElementById("cmbBudgetType").value!="" && ((document.getElementById("cmbBudgetType").value=="2") || ( document.getElementById("cmbBudgetType").value=="3")))
                {
                    if(document.getElementById("ActivityName").value!="")
                    {
                        document.getElementById("cmbProject").disabled = true;
                    }
                    else
                    {
                        document.getElementById("cmbProject").disabled = false;
                    }
                }
                
                if(document.getElementById("cmbBudgetType").value!="" && document.getElementById("cmbBudgetType").value=="1")
                {
                    if(document.getElementById("ActivityName").value!="")
                    {
                        document.getElementById("cmbProject").disabled = true;
                    }
                    else
                    {
                        document.getElementById("cmbProject").disabled = false;
                    }
                }
                
                
            });

            $('#cmbBudgetType').change(function() {
                document.getElementById("cmbProject").options[0].selected = "selected";
                var financeYear =  document.getElementById("cmbFinancialYear").options[document.getElementById("cmbFinancialYear").selectedIndex].text;
                var arrfinance = financeYear.split("-");

                
                //var Date1 = arrfinance[0] + "-" +arrfinance[1] + "-" + arrfinance[2];
                //var Date2 = arrfinance[3] + "-" +arrfinance[4] + "-" + arrfinance[5];
                
                var Date1 = arrfinance[0];
                var Date2 = arrfinance[1];
                
                document.getElementById("errPrjMsg").innerHTML ="";
                
                
                if(document.getElementById("cmbBudgetType").value !="")
                {
                    if(document.getElementById("ActivityName").value !="")
                    {
                        document.getElementById("cmbProject").disabled = true;
                    }
                    else
                    {
                        document.getElementById("cmbProject").disabled = false;
                    }
                }
                
                
                if(document.getElementById("cmbBudgetType").value=="")
                {
                    document.getElementById("cmbProject").disabled = false;
                    document.getElementById("ActivityName").disabled = false;
                }
                
                if(document.getElementById("cmbBudgetType").value=="1"){
                    document.getElementById("spanPrj").style.visibility = "visible";
                }else{
                    document.getElementById("spanPrj").style.visibility = "hidden";
                }

                if(document.getElementById("cmbBudgetType").value=="2"){
                    $.post("<%=request.getContextPath()%>/APPServlet", {param1:'Government',param4:'getAppProject',startDate:Date1, endDate:Date2, funName:'getAppProject'},  function(j){
                        $("select#cmbProject").html(j);
                    });
                }
                if(document.getElementById("cmbBudgetType").value=="1"){
                    $.post("<%=request.getContextPath()%>/APPServlet", {param1:'Aid or Grant',param4:'getAppProject',startDate:Date1, endDate:Date2,funName:'getAppProject'},  function(j){
                        $("select#cmbProject").html(j);
                    });
                }
                if(document.getElementById("cmbBudgetType").value=="3"){
                    $.post("<%=request.getContextPath()%>/APPServlet", {param1:'Own fund',param4:'getAppProject',startDate:Date1, endDate:Date2,funName:'getAppProject'},  function(j){
                        $("select#cmbProject").html(j);
                    });
                }
                //document.getElementById("ActivityNameRow").style.display = "none";
            });

            $('#cmbFinancialYear').change(function() {
                document.getElementById("cmbProject").options[0].selected = "selected";
                var financeYear =  document.getElementById("cmbFinancialYear").options[document.getElementById("cmbFinancialYear").selectedIndex].text;
                var arrfinance = financeYear.split("-");

                //var Date1 = arrfinance[0] + "-" +arrfinance[1] + "-" + arrfinance[2];
                //var Date2 = arrfinance[3] + "-" +arrfinance[4] + "-" + arrfinance[5];
                var Date1 = arrfinance[0];
                var Date2 = arrfinance[1];

                if(document.getElementById("cmbBudgetType").value=="1"){
                    document.getElementById("spanPrj").style.visibility = "visible";
                }else{
                    document.getElementById("spanPrj").style.visibility = "hidden";
                }

                if(document.getElementById("cmbBudgetType").value=="2"){
                    $.post("<%=request.getContextPath()%>/APPServlet", {param1:'Government',param4:'getAppProject',startDate:Date1, endDate:Date2, funName:'getAppProject'},  function(j){
                        $("select#cmbProject").html(j);
        });
                }
                if(document.getElementById("cmbBudgetType").value=="1"){
                    $.post("<%=request.getContextPath()%>/APPServlet", {param1:'Aid or Grant',param4:'getAppProject',startDate:Date1, endDate:Date2,funName:'getAppProject'},  function(j){
                        $("select#cmbProject").html(j);
                    });
                }
                if(document.getElementById("cmbBudgetType").value=="3"){
                    $.post("<%=request.getContextPath()%>/APPServlet", {param1:'Own fund',param4:'getAppProject',startDate:Date1, endDate:Date2,funName:'getAppProject'},  function(j){
                        $("select#cmbProject").html(j);
                    });
                }
                //document.getElementById("ActivityNameRow").style.display = "none";
            });
        });

        /*$(function() {
            $('#cmbProjectOffice').change(function() {
                if(document.getElementById("cmbProjectOffice").value!="")
                {
                    document.getElementById("errPOMsg").innerHTML ="";
                    document.getElementById("errPEMsg").innerHTML ="";
                    $.post("<%//=request.getContextPath()%>/APPServlet", {projectId:$('#cmbProjectOffice').val(),funName:'getPE'},  function(j){
                        $("select#cmbPE").html(j);
                    });
                }
                else
                {
                    document.getElementById("errPOMsg").innerHTML ="Please Select Project Office";
                    document.getElementById("errPEMsg").innerHTML ="Please Select PE";
                }
            });
      });

      $(function() {
            $('#cmbPE').change(function() {
                alert(document.getElementById("cmbPE").value);
                if(document.getElementById("cmbPE").value!="")
                {
                    document.getElementById("errPEMsg").innerHTML ="";
                }
                else{
                    document.getElementById("errPEMsg").innerHTML ="Please Select PE";
                }
            });
      });*/

        function setPrjName(obj){
            document.getElementById("hdnPrjName").value = obj.options[obj.selectedIndex].text;
        }

        function setFinYear(obj){
            document.getElementById("hdnFinYear").value = obj.options[obj.selectedIndex].text;
        }
        //Nitish Start
        function setMessage(obj)
        {
            var appType = obj.value;
            if(appType != "")
            {
                $('#msgdepoplanWork').html('');
            }
            else
            {
                $('#msgdepoplanWork').html('Please select APP Type.'); 
            }
        }
        //Nitish END
        
        function setDPType(obj) {
                                                if ("2" == obj.options[obj.selectedIndex].value) {
                                                    $('#trEntrustingAgency').show();
                                                    $('#trTimeFrame').hide();
                                                    if (document.getElementById("txtEntrustingAgency").value == null || document.getElementById("txtEntrustingAgency").value == "null")
                                                    {
                                                        document.getElementById("txtEntrustingAgency").value = "";
                                                    }

                                                } else if ("" == obj.options[obj.selectedIndex].value) {
                                                    $('#trEntrustingAgency').hide();
                                                    $('#trTimeFrame').hide();

                                                } else {
                                                    $('#trEntrustingAgency').hide();
                                                    $('#trTimeFrame').show();
                                                }
                                            }

        function validate(){

            var flag1 = true;
            $(".reqF_1").remove();
            var budType = document.getElementById("cmbBudgetType").value;
            var depoplanWork = document.getElementById("cmbdepoplanWork").value;
                 
            var appCode = document.getElementById("txtAppCode").value;
            document.getElementById("msgBudgetType").innerHTML  = "";
            document.getElementById("msgdepoplanWork").innerHTML  = "";

            document.getElementById("msgappCode").innerHTML = "";
            document.getElementById("errPrjMsg").innerHTML ="";
           
            if ("2" == document.getElementById("cmbdepoplanWork").options[document.getElementById("cmbdepoplanWork").selectedIndex].value) {
                if (document.getElementById("txtEntrustingAgency") != null) {
                    if ($('#txtEntrustingAgency').val() == '') {
                        $('#msgEntrustAgency').html('<br/>Please enter Reference Letter Ref. No.');
                        flag1 = false;
                    } else {
                        $('#msgEntrustAgency').html('');
                    }
                }
            }
            
            if( $('#isValidtxtAppCode').val() == 'f')
            {
                flag1 = false;
            }
            
            if(budType==""){
                document.getElementById("msgBudgetType").innerHTML = "Please select Budget Type.";
                flag1 = false;
            }
            
            if(depoplanWork==""){
                document.getElementById("msgdepoplanWork").innerHTML = "Please select APP Type.";
                flag1 = false;
            }
            
            if($( $('#cmbdepoplanWork').val()==="2" && '#isValidEntrustingAgency').val() == 'f')
            {
                flag1 = false;
            }
            
            if(document.getElementById("cmbBudgetType")!=null){
                if(document.getElementById("cmbBudgetType").value!=""){
                    if(document.getElementById("cmbBudgetType").value=="1")
                    {
                        if(document.getElementById("cmbProject").value=="" && document.getElementById("ActivityName").value=="")
                        {
                            document.getElementById("errPrjMsg").innerHTML ="<br/>Please select Project or enter Activity name.";
                            flag1 = false;
                        }
                        else
                        {
                            document.getElementById("errPrjMsg").innerHTML ="";   
                            //flag1 = true;
                        }

                    }
                    else{
                        document.getElementById("errPrjMsg").innerHTML ="";
                        //flag1 = true;
                    }
                }
            }
            
            if(document.getElementById("cmbProject").value!="" && document.getElementById("ActivityName").value!="")
            {
                document.getElementById("errPrjMsg").innerHTML ="<br/>Either select Project or enter Activity name. Only one should be given.";
                flag1 = false;
            }

            if($.trim(appCode)==""){
                document.getElementById("msgappCode").innerHTML = "Please enter Letter Ref. No.";
                flag1 = false;
            }else if(/\s/.test(appCode)){
                document.getElementById("msgappCode").innerHTML = "White space is not allowed in Letter Ref. No.";
                flag1 = false;
            }else{
                if(appCode.length > 50){
                    document.getElementById("msgappCode").innerHTML = "Maximum 50 Characters are allowed";
                    flag1 = false;
                }
            }
            
            if(document.getElementById("hdnPE").value!=0 && flag1)
            {
                document.getElementById("frmCreateAPP").submit();
            }
            else if(document.getElementById("hdnPE").value==0)
            {
                if(document.getElementById("hdnAU").value!=0 && document.getElementById("hdnAU").value!=null)
                {
                    if(flag1)
                    {
                        document.getElementById("frmCreateAPP").submit();
                    }
                }
                else
                {
                    jAlert("No PA/AU available"," Alert ", "Alert");
                    return false;
                }
            }
            else
            {
                return false;
            }
            
            //return flag1;
            return false;
        }
       </script>
       <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabApp");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
       </script>
</html>
