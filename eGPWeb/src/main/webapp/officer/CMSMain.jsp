<%-- 
    Document   : CMSMain
    Created on : Jul 30, 2011, 11:18:11 AM
    Author     : dixit
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.CommonSearchServiceSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsDateConfig"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderPayment"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    ResourceBundle bdl = null;
                    bdl = ResourceBundle.getBundle("properties.cmsproperty");
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    boolean isPerformanceSecurityPaid = false;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>CMS</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
        <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
        
        <!--Code Start by Proshanto Kumar Saha,Dohatec-->
        <style type="text/css">
            .newCMSTab {
               overflow: auto;
              }
        </style>
        <!--Code End by Proshanto Kumar Saha,Dohatec-->

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">CMS</div>
                <%
                    String tenderId = "";
                    if (request.getParameter("tenderId") != null) {
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                        tenderId = request.getParameter("tenderId");
                    }
                    String userId = "";
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = session.getAttribute("userId").toString();
                        issueNOASrBean.setLogUserId(userId);
                    }
                    pageContext.setAttribute("tab", "14");
                    List<Object[]> objBusinessRule = tenderSrBean.getConfiForTender(Integer.parseInt(tenderId));
                    //String procnature = commonService.getProcNature(request.getParameter("tenderId")).toString();

                    int lotId = 0;
                    List<Object []> chkObj = null;
                    boolean isNoaAndCSDone = false;
                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    chkObj = issueNOASrBean.getNOAListing(Integer.parseInt(tenderId));
                    if(!chkObj.isEmpty()){
                        if(chkObj.size() > 0){
                            for (Object[] obj : chkObj) {
                                isNoaAndCSDone = service.checkContractSigning(tenderId, Integer.parseInt(obj[0].toString()));
                                //
                                 List<TblTenderPayment> detailsPayment = issueNOASrBean.getPaymentDetails(Integer.parseInt(tenderId), (Integer) obj[0], (Integer) obj[8]);  
                            }
                        }
                    }
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>

                <%@include  file="officerTabPanel.jsp"%>
                <div class="tabPanelArea_1 newCMSTab">
                    <%

                    //Code Start by Proshanto Kumar Saha,Dohatec
                    //This portion is used to show CMSTab.jsp if contract sign is not done and performance security is given and tender validity is over
                      boolean checkStatus = false;
                      CommonSearchServiceSrBean objCommonSearchServiceSrBean=new CommonSearchServiceSrBean();
                      checkStatus=objCommonSearchServiceSrBean.getCSignTValidityAndPSecurityStatus(tenderId, userId);
                      if(checkStatus)
                       {
                       %>
                               <%@include  file="../resources/common/CMSTab.jsp"%>
                       <%
                       }
                       else if(!isNoaAndCSDone)
                       {
                        %>
                                <div id="successMsg" class="responseMsg noticeMsg t_space" ><span>Contract Signing is pending.</span></div>
                        <%
                       }
                      else if(isNoaAndCSDone){
                      //Code End by Proshanto Kumar Saha,Dohatec

                          //else{
                                pageContext.setAttribute("TSCtab", "3");
                    %>
                    <%@include  file="../resources/common/CMSTab.jsp"%>
                    <div class="tabPanelArea_1">
                        <div align="center">
                            <%
                                        String capNameOfVendor = "";
                                        String strLotNo = "Lot No.";
                                        String strLotDes = "Lot Description";
                                        if ("goods".equalsIgnoreCase(procnature)) {
                                            capNameOfVendor = "Name of Supplier";
                                        } else if ("works".equalsIgnoreCase(procnature)) {
                                            capNameOfVendor = "Name of Contractor";
                                        }else if("services".equalsIgnoreCase(procnature))
                                        {
                                            strLotNo = "Package No.";
                                            strLotDes = "Package Description";
                                            capNameOfVendor = "Name of Consultant";
                                        }
                                        for (Object[] obj : chkObj) {
                                            lotId = (Integer) obj[0];
                                            List<TblTenderPayment> detailsPayment = issueNOASrBean.getPaymentDetails(Integer.parseInt(tenderId), (Integer) obj[0], (Integer) obj[8]);
                                            SimpleDateFormat simpl = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
                                            if (issueNOASrBean.isAvlTCS((Integer) obj[1])) {
                                                //ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                                CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                                List<TblCmsDateConfig> configdatalist = cmsConfigDateService.getConfigDatesdata(Integer.parseInt(tenderId), Integer.parseInt(obj[0].toString()),issueNOASrBean.getContractSignId());
                                                boolean editFlag = false;
                                                if (!configdatalist.isEmpty()) {
                                                    editFlag = true;
                                                }
                                                boolean flag = isNoaAndCSDone;
                            %>

                            <table cellspacing="0" class="tableList_1 t_space" width="100%">
                                <tr>
                                    <td class="ff" width="12%" class="t-align-left"><%=strLotNo%></td>
                                    <td   class="t-align-left"><%=obj[6]%></td>
                                </tr>
                                <tr>
                                    <td class="ff" width="12%"  class="t-align-left"><%=strLotDes%></td>
                                    <td  class="t-align-left"><%=obj[7]%></td>
                                </tr>
                            </table>
                            <br />
                            <%if (request.getParameter("msg")!=null) {
                            %>
                            <script>jAlert('You can not configure Payment if Date Configuration is pending','Commencement Date', function(RetVal) {
                                });</script>
                            <%} %>
                            <%if (!editFlag) {
                                                                            if (procnature.equalsIgnoreCase("goods") || procnature.equalsIgnoreCase("services")) {
                            %>
                            <div  class='responseMsg noticeMsg t-align-left'><%=bdl.getString("CMS.goods.configdates")%></div>
                            <%} else {%>
                            <div  class='responseMsg noticeMsg t-align-left'><%=bdl.getString("CMS.works.configdates")%></div>
                                        <%                                }
                                                                    }%>
                            <br />
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th width="15%" class="t-align-left"><%= capNameOfVendor%></th>
                                    <th width="10%" class="t-align-left">Contract No.</th>
                                    <th width="10%" class="t-align-left">Contract Name</th>
                                    <!--                                <th width="15%" class="t-align-left">Date of Issue</th>
                                                                    <th width="15%" class="t-align-left">Last Acceptance Date and Time</th>
                                                                    <th width="15%" class="t-align-left">NOA Acceptance Status</th>-->
                                    <%
                                         if (!objBusinessRule.isEmpty() && objBusinessRule.get(0)[1].toString().equalsIgnoreCase("yes")) {
                                    %>
                                    <th width="15%" class="t-align-left">Performance Security </th>
                                    <%}%>
                                    <th width="10%" class="t-align-left">Action</th>
                                </tr>
                                <tr>
                                    <td style="text-align: left;"><%=obj[9]%></td>
                                    <td class="t-align-center" > <%=obj[2]%></td>
                                    <td class="t-align-center" > <%=obj[7]%></td>
                                    <%
                                        if (!objBusinessRule.isEmpty() && objBusinessRule.get(0)[1].toString().equalsIgnoreCase("yes")) {
                                            out.print("<td class=\"t-align-center\">");
                                            if (detailsPayment.isEmpty()) {
                                                out.print("Pending");
                                            } else {
                                                if (detailsPayment.get(0).getIsVerified().equalsIgnoreCase("yes")) {
                                                    out.print("Received");
                                                } else {
                                                    out.print("Pending");
                                                }
                                            }
                                        }
                                    %>
                                    <td class="t-align-center">
                                        <%
                                                                                    if (flag) {
                                                                                        if (!editFlag) {
                                        %>
                                        <a href="ConfigureDates.jsp?lotId=<%=obj[0]%>&contractSignId=<%=issueNOASrBean.getContractSignId()%>&tenderId=<%=tenderId%>">Configure Date</a>

                                        <%} else {%>                                        
                                        <%
                                                                                    boolean check = false;
                                                                                    check = service.isWorkCompleteOrNot(obj[0].toString());
                                                                                    if (!check) {
                                                                                        check = service.isContractTerminatedOrNot(obj[0].toString());
                                                                                        if (!check) {
                                        %>
                                        <a href="ConfigureDates.jsp?lotId=<%=obj[0]%>&contractSignId=<%=issueNOASrBean.getContractSignId()%>&tenderId=<%=tenderId%>&msg=edit">Edit Date</a>&nbsp;|&nbsp;
                                        <% List<TblCmsDateConfig> tblcmsdatedata = cmsConfigDateService.getConfigDatesdata(Integer.parseInt(tenderId),Integer.parseInt(obj[0].toString()),issueNOASrBean.getContractSignId());%>
                                        <a href="ConfigureDatesUploadDoc.jsp?lotId=<%=obj[0]%>&contractSignId=<%=issueNOASrBean.getContractSignId()%>&tenderId=<%=tenderId%>&keyId=<%=tblcmsdatedata.get(0).getActContractDtId()%>&docx=cd">Upload / Download Files</a>&nbsp;|&nbsp;
                                        <%
                                                                                                } else {%>
                                        <a href="#" onclick="isContractTerminatedOrNot()">Edit Date</a>&nbsp;|&nbsp;
                                        <%}
                                                                                                    } else {%>
                                        <a href="#" onclick="isWorkCompleteOrNot()">Edit Date</a>&nbsp;|&nbsp;
                                        <%}
                                        %>
                                        <a href="ViewConfigureDates.jsp?lotId=<%=obj[0]%>&contractSignId=<%=issueNOASrBean.getContractSignId()%>&tenderId=<%=tenderId%>&msg=view">View Date History</a>
                                        <%  }
                                                                    } else {%>
                                        Contract Signing is Pending
                                        <%}
                                                            }%>
                                    </td>
                                </tr>
                           
                            <%}%>

                            </table>

<br />
                            <%


                                    //ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                    RepeatOrderService ros = (RepeatOrderService) AppContext.getSpringBean("RepeatOrderService");
                                    int i = 0;
                                   
                                        List<Object[]> mainForRO = ros.getMainLoopForROPESide(lotId);
                                        if(!mainForRO.isEmpty()){
                                            int icount = 1;
                                                for(Object[] objs : mainForRO){
                                                       

                        %>
                         
                            
                              <% for (Object[] obj : issueNOASrBean.getNOAListingForRO(Integer.parseInt(tenderId),(Integer) objs[0])) {%>
                               <%              List<TblTenderPayment> detailsPayment = issueNOASrBean.getPaymentDetails(Integer.parseInt(tenderId), (Integer) obj[0], (Integer) obj[8]);
                                            SimpleDateFormat simpl = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
                                            if (issueNOASrBean.isAvlTCS((Integer) obj[1])) {
                                                CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                                List<TblCmsDateConfig> configdatalist = cmsConfigDateService.getConfigDatesdata(Integer.parseInt(tenderId), Integer.parseInt(obj[0].toString()),issueNOASrBean.getContractSignId());
                                                boolean editFlag = false;
                                                if (!configdatalist.isEmpty()) {
                                                    editFlag = true;
                                                }
                                                boolean flag = isNoaAndCSDone;
                            if (!editFlag) {
                                                                            if (procnature.equalsIgnoreCase("goods")) {
                            %>
                            <div  class='responseMsg noticeMsg t-align-left'><%=bdl.getString("CMS.goods.configdates")%></div>
                            <%} else {%>
                            <div  class='responseMsg noticeMsg t-align-left'><%=bdl.getString("CMS.works.configdates")%></div>
                                        <%                                }
                                                                    }%>
                            <tr>
                                    <td colspan="5">
                                <ul class="tabPanel_1 noprint t_space">
                                    <li class="sMenu button_padding">Repeat Order-<%=icount%></li>
                                </ul>

                                    </td>
                             
                           
                            <br />
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tr>
                                    <th width="15%" class="t-align-left"><%= capNameOfVendor%></th>
                                    <th width="10%" class="t-align-left">Contract No.</th>
                                    <th width="10%" class="t-align-left">Contract Name</th>
                                    <!--                                <th width="15%" class="t-align-left">Date of Issue</th>
                                                                    <th width="15%" class="t-align-left">Last Acceptance Date and Time</th>
                                                                    <th width="15%" class="t-align-left">NOA Acceptance Status</th>-->
                                    <%
                                         if (!objBusinessRule.isEmpty() && objBusinessRule.get(0)[1].toString().equalsIgnoreCase("yes")) {
                                    %>
                                    <th width="15%" class="t-align-left">Performance Security </th>
                                    <%}%>
                                    <th width="10%" class="t-align-left">Action</th>
                                </tr>
                                <tr>
                                    <td style="text-align: left;"><%=obj[9]%></td>
                                    <td class="t-align-center" > <%=obj[2]%></td>
                                    <td class="t-align-center" > <%=obj[7]%></td>
                                    <%
                                        if (!objBusinessRule.isEmpty() && objBusinessRule.get(0)[1].toString().equalsIgnoreCase("yes")) {
                                            out.print("<td class=\"t-align-center\">");
                                            if (detailsPayment.isEmpty()) {
                                                out.print("Pending");
                                            } else {
                                                if (detailsPayment.get(0).getIsVerified().equalsIgnoreCase("yes")) {
                                                    out.print("Received");
                                                } else {
                                                    out.print("Pending");
                                                }
                                            }
                                        }
                                    %>
                                    <td class="t-align-center">
                                        <%
                                                                                    if (flag) {
                                                                                        if (!editFlag) {
                                        %>
                                        <a href="ConfigureDatesForRO.jsp?lotId=<%=obj[0]%>&contractSignId=<%=issueNOASrBean.getContractSignId()%>&tenderId=<%=tenderId%>&roundId=<%=objs[0].toString()%>">Configure Date</a>

                                        <%} else {%>
                                        <%
                                                                                    boolean check = false;
                                                                                    check = service.isWorkCompleteOrNot(obj[0].toString());
                                                                                    if (!check) {
                                                                                        check = service.isContractTerminatedOrNot(obj[0].toString());
                                                                                        if (!check) {
                                        %>
                                        <a href="ConfigureDatesForRO.jsp?lotId=<%=obj[0]%>&contractSignId=<%=issueNOASrBean.getContractSignId()%>&tenderId=<%=tenderId%>&msg=edit&roundId=<%=objs[0].toString()%>">Edit Date</a>&nbsp;|&nbsp;
                                        <% List<TblCmsDateConfig> tblcmsdatedata = cmsConfigDateService.getConfigDatesdata(Integer.parseInt(tenderId),Integer.parseInt(obj[0].toString()),issueNOASrBean.getContractSignId());%>
                                        <a href="ConfigureDatesUploadDoc.jsp?lotId=<%=obj[0]%>&contractSignId=<%=issueNOASrBean.getContractSignId()%>&tenderId=<%=tenderId%>&keyId=<%=tblcmsdatedata.get(0).getActContractDtId()%>&docx=cd">Upload / Download Files</a>&nbsp;|&nbsp;
                                        <%
                                                                                                } else {%>
                                        <a href="#" onclick="isContractTerminatedOrNot()">Edit Date</a>&nbsp;|&nbsp;
                                        <%}
                                                                                                    } else {%>
                                        <a href="#" onclick="isWorkCompleteOrNot()">Edit Date</a>&nbsp;|&nbsp;
                                        <%}
                                        %>
                                        <a href="ViewConfigureDates.jsp?lotId=<%=obj[0]%>&contractSignId=<%=issueNOASrBean.getContractSignId()%>&tenderId=<%=tenderId%>&msg=view">View Date History</a>
                                        <%  }
                                                                    } else {%>
                                        Contract Signing is Pending
                                        <%}
                                                            }%>
                                    </td>
                                </tr>
                            </table>
                            <%
                                    }
                                    icount++;
                                }
                            }
                            %>
                        </div>
                    </div>
                </div>
                <%
                        }
                %>
            </div>
            <div>&nbsp;</div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        function isContractTerminatedOrNot(){
            jAlert("Commencement Dates cannot be edited as Contract has been Terminated by Procuring Entity"," Alert ", "Alert");
        }
        function isWorkCompleteOrNot(){
            jAlert("Commencement Dates cannot be edited once Work Completion Certificate is issued by Procuring Entity"," Alert ", "Alert");
        }
    </script>
</html>
<%
            issueNOASrBean = null;
%>