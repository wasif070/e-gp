<%-- 
    Document   : UploadCSV
    Created on : Oct 14, 2011, 2:32:24 PM
    Author     : rikin.p
--%>
<%@page import="com.cptu.egp.eps.web.utility.FilePathUtility"%>
<%@page import="org.apache.commons.fileupload.FileUploadException"%>
<%@page import="java.io.File"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="org.apache.commons.fileupload.FileItemFactory"%>
<%

    // create file upload factory and upload servlet
    FileItemFactory factory = new DiskFileItemFactory();
    ServletFileUpload upload = new ServletFileUpload(factory);

    List uploadedItems = null;
    FileItem fileItem = null;
    String myFileName = "";
    String filePath = FilePathUtility.getFilePath().get("CSVFiles");
    
    String tenderId = "";
    String sectionId = "";
    String formId = "";
    String tableId = "";
    String porlotId = "";
    
    if(request.getParameter("hidtenderId")!=null && !"".equals(request.getParameter("hidtenderId"))){
        tenderId = request.getParameter("hidtenderId");
    }
    if(request.getParameter("hidsectionId")!=null && !"".equals(request.getParameter("hidsectionId"))){
        sectionId = request.getParameter("hidsectionId");
    }
    if(request.getParameter("hidformId")!=null && !"".equals(request.getParameter("hidformId"))){
        formId = request.getParameter("hidformId");
    }
    if(request.getParameter("hidtableId")!=null && !"".equals(request.getParameter("hidtenderId"))){
        tableId = request.getParameter("hidtableId");
    }
    if(request.getParameter("hidporlotId")!=null && !"".equals(request.getParameter("hidporlotId"))){
        porlotId = request.getParameter("hidporlotId");
    }
    
    try 
    {
        // iterate over all uploaded files
        uploadedItems = upload.parseRequest(request);
        Iterator i = uploadedItems.iterator();
        while (i.hasNext()) 
        {
            fileItem = (FileItem) i.next();
            if (fileItem.isFormField() == false) 
            {
                if (fileItem.getSize() > 0) 
                {
                    File uploadedFile = null;
                    String myFullFileName = fileItem.getName(), slashType = (myFullFileName.lastIndexOf("\\") > 0) ? "\\" : "/"; // Windows or UNIX
                    int startIndex = myFullFileName.lastIndexOf(slashType);

                    // Ignore the path and get the filename
                    myFileName = myFullFileName.substring(startIndex + 1, myFullFileName.length());
                    // Create new File object
                    uploadedFile = new File(filePath, myFileName);
                    // Write the uploaded file to the system
                    fileItem.write(uploadedFile);
                }
            }
        }
    } 
    catch (FileUploadException e) 
    {
            e.printStackTrace();
    } 
    catch (Exception e) 
    {
            e.printStackTrace();
    }
    
%>
<html>
    <head>
        <title>Upload CSV</title>
    </head>
    <body>
        <form name="frmsubmit" id="idfrmsubmit" method="post" action="TenderTableMatrix.jsp?tenderId=<%=tenderId%>&sectionId=<%=sectionId%>&formId=<%=formId%>&tableId=<%=tableId%>&porlId=<%=porlotId%>">
        <input type="hidden" name="pageName" id="pageName" value="<%=myFileName%>" />
        <input type="hidden" name="action" id="action" value="readFile" />
        </form>
    </body>
    <script type="text/javascript">
        document.getElementById("idfrmsubmit").submit();
    </script>
</html>
