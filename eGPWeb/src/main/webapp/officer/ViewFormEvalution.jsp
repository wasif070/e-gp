<%--
    Document   : TenderPaymentDocsUpload
    Created on : Nov 29, 2010, 3:29:18 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Forms Evaluation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
              
    </head>
    <body>
        <div class="dashboard_div">
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <div class="contentArea_1">
                <%
                            String tenderId = "";
                            String formId = "";
                            String userId = "", uId = "", pkgLotId = "";
                            boolean isCommCP = false;
                            if (request.getParameter("tenderId") != null) {
                                tenderId = request.getParameter("tenderId");
                            }

                            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                userId = session.getAttribute("userId").toString();
                            }
                           
                            if (request.getParameter("formId") != null && !"".equalsIgnoreCase(request.getParameter("formId"))) {
                                formId = request.getParameter("formId");
                            }

                            if (request.getParameter("pkgLotId") != null && !"".equalsIgnoreCase(request.getParameter("pkgLotId"))) {
                                pkgLotId = request.getParameter("pkgLotId");
                            }
                            CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                            List<SPCommonSearchDataMore> commCP = dataMore.getEvalProcessInclude("evaluationMemberCheck", tenderId, session.getAttribute("userId").toString(), "1");
                            if (commCP != null && (!commCP.isEmpty()) && (!commCP.get(0).getFieldName1().equals("0"))) { isCommCP = true; }
                            TenderCommonService tenderCs = (TenderCommonService)AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> lstTeamSelUserId =
                                tenderCs.returndata("getEvalTeamSelectedMember", tenderId, null);
                                if (!lstTeamSelUserId.isEmpty()) {
                                    if(!userId.equals(lstTeamSelUserId.get(0).getFieldName1())){
                                     userId = lstTeamSelUserId.get(0).getFieldName1();
                                    }
                                }

                            if (request.getParameter("uId") != null) {
                                uId = request.getParameter("uId");
                            }
                           
                            if (session.getAttribute("userId") == null) {
                                response.sendRedirect("SessionTimedOut.jsp");
                            }
                            
                            String queryString = "";
                            if (request.getQueryString() != null) {
                                queryString = request.getQueryString();
                            }
                            String pageName = "";
                            String uri = "";
                            if (request.getRequestURI() != null) {
                                uri = request.getRequestURI();
                            }
                            String url = "";
                            if (request.getRequestURL() != null) {
                                url = request.getRequestURL().toString();
                            }
                            
                            if (request.getParameter("page") == null) {
                                String temp = url.substring(0, url.indexOf(uri));
                                pageName = request.getHeader("referer");
                                pageName = (pageName.replace(temp, ""));                                
                            }

                            if (request.getParameter("page") != null) {
                                pageName = (request.getParameter("page"));
                            }

                %>                   
                    <input type="hidden" name="pageName" value="<%=pageName%>"/>                   
                    <input type="hidden" name="formId" value="<%=formId%>"/>
                    <div class="pageHead_1">View Forms Evaluation
                        <%if (request.getParameter("page") != null) { %>
                        <span style="float:right;"><a href="<%=pageName%>" class="action-button-goback">Go Back</a></span>
                        <%} else {
                              String PageRef = "";
                              PageRef = request.getHeader("referer");
                         %>                       
                         <span style="float:right;"><a href="<%=PageRef%>" class="action-button-goback">Go Back</a></span>
                        <%}%>
                    </div>
                    <%   pageContext.setAttribute("tenderId", tenderId);%>
                    <div class="t_space">
                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    </div>
                                       
                    <% for (SPTenderCommonData companyList : tenderCommonService.returndata("GetCompanynameByUserid", uId, "0")) {
                    %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th colspan="2" class="t_align_left ff">Company Details</th>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Company Name :</td>
                                <td width="78%" class="t-align-left"><%=companyList.getFieldName1()%></td>
                            </tr>
                        </table>
                            <%@include file="TSCComments.jsp" %>

                        <%
                                    } // END FOR LOOP OF COMPANY NAME
                        %>


                        <%

                       CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");

                        for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                                    %>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <th colspan="2" class="t_align_left ff">Tender/Proposal Details</th>
                                        </tr>

                                        <tr>
                                            <td width="22%" class="t-align-left ff">Package No. :</td>
                                            <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-left ff">Package Description :</td>
                                            <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                                        </tr>
                                    </table>

                                    <% } %>

                                     <%
                            int cntQuest = 0;
                            int memnameCnt = 0;
                            // START FOR LOOP OF TEC / TSC MEMBER NAME BY FORM-ID AND TENDER-ID.                            
                            for (SPCommonSearchData sptcd : commonSearchService.searchData("GetEvalComMemForBidderClari", tenderId, formId, userId, null, null, null, null, null, null)) {
                                memnameCnt++;
                            %>
                            <table width="100%" cellspacing="0" class="tableList_k t_space">
                                <tr>
                                    <th colspan="3" class="t-align-left">TEC / TSC Member Name : &nbsp;<%=sptcd.getFieldName3()%>
                                    </th>
                                </tr>
                                <tr>
                                    <th width="4%" class="t-align-center">Sl. No.</th>
                                    <th width="48%" class="t-align-center">Questions</th>
                                    <th width="48%" class="t-align-center">Answer</th>

                                </tr>
                                <%

                                    // START FOR LOOP OF QUESTION POSTED BY TEC / TSC MEMBERS.
                                    for (SPCommonSearchData question : commonSearchService.searchData("GetEvalComMemQuestionByMem", tenderId,
                                                formId,
                                                sptcd.getFieldName2(), uId, null, null, null, null, null)) {
                                        cntQuest++;
                                        // HIGHLIGHT THE ALTERNATE ROW
                                        if(Math.IEEEremainder(cntQuest,2)==0) {
                                %>
                                <tr class="bgColor-Green">
                                <% } else { %>
                                <tr>
                                <% }
                                    // For fixing #1210 on 13/Nov/2013
                                    String strQuestion = question.getFieldName3();
                                    try{
                                        strQuestion = URLDecoder.decode(question.getFieldName3(),"UTF-8");
                                    }catch(Exception e) {
                                        System.out.print("Error while decoading ");
                                        e.printStackTrace();
                                        strQuestion = question.getFieldName3();
                                    }
                                    %>
                                    <td class="t-align-left"><%=cntQuest%></td>

                                    <td  class="t-align-left"><%=strQuestion%></td>

                                    <%if (!question.getFieldName4().equals("")) {%>
                                    <td  class="t-align-left">
                                        <%//=URLDecoder.decode(question.getFieldName4(),"UTF-8")%>
                                        <%=question.getFieldName4()%></td>
                                    <%} else {%>
                                    <td  class="t-align-left">
                                        <%//=URLDecoder.decode(question.getFieldName4(),"UTF-8")%>
                                        <%=question.getFieldName4()%>
                                    </td>
                                    <%}%>
                                </tr>
                                <%

                                    } // FOR Loop Question Ends Here

                                    if (cntQuest == 0) {
                                %>
                                <tr>
                                    <td colspan="3"  class="t-align-center">No queries posted yet</td>
                                </tr>
                                <%}%>
                            </table>
                            <%      }//Member Name Loop Ends Here..
                                                                        if (memnameCnt == 0) {%>
                          <table class="t_space tableList_k" width="100%">
                             <tr>
                                <td width="85%" class="t-align-left">No member has posted any queries yet</td>
                             </tr>
                          </table>
                            <%
                                }
                                CommonSearchDataMoreService statusService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                                List<SPCommonSearchDataMore> list_status = new ArrayList<SPCommonSearchDataMore>();
                                if (!lstTeamSelUserId.isEmpty() || !isCommCP) {
                                list_status = statusService.geteGPData("getFormsForBidderEvalStatusView", tenderId, uId, userId, "evaluation", pkgLotId, null, null, null, null, null, null, null, null, null, null, null, null, null, null);                                
                             %>
                             <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                 <tr>
                                    <th colspan="3" class="t-align-left">Evaluation Report</th>
                                </tr>
                                <tr>
                                    <td width="10%" valign="top" class="t-align-left ff">Evaluation Status :</td>
                                    <td width="86%" class="t-align-left">
                                     <%
                                         for(SPCommonSearchDataMore obj_Status : list_status) {                                             
                                                  if((obj_Status.getFieldName4()).equals(formId)) {                                                       
                                                       out.print(obj_Status.getFieldName2());
                                                     }
                                              }
                                     %>                                         
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td width="10%" valign="top" class="t-align-left ff">Reason :</td>
                                    <td width="86%" class="t-align-left">
                                     <%
                                         for(SPCommonSearchDataMore obj_Status : list_status) {                                             
                                                  if((obj_Status.getFieldName4()).equals(formId)) {                                                       
                                                        out.print(URLDecoder.decode(obj_Status.getFieldName3(),"UTF-8"));
                                                     }
                                              }
                                     %>
                                    </td>
                                </tr>
                             </table>
                                    <%     }
                                else {%>
                                 <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                  <tr> <th colspan=2 class="t-align-left">Evaluation Status Report</th> </tr>
                                 <%  for (SPCommonSearchData memList1 : commonSearchService.searchData("evalComMemberForReport", tenderId, uId, null, null, null, null, null, null, null)) {
                                     %>
                             <tr>
                                <th width="20%" class="t-align-left ff"> Member Name </th>
                                <th width="80%" class="t-align-center ff"><%=memList1.getFieldName1()%></th>
                            </tr>
                            <% List<SPCommonSearchData> evalList = commonSearchService.searchData("EvalMemberStatus", tenderId, uId, formId, memList1.getFieldName2(), null, null, null, null, null);
                                 if (!evalList.isEmpty()) {%>
                                 <tr>
                                    <th width="20%" class="t-align-left">Evaluation Status</th>
                                    <th width="80%"  class="t-align-left">Reason</th>
                                 </tr>
                                 <tr>
                                    <td width="22%" class="t-align-center"><%=evalList.get(0).getFieldName2()%></td>
                                    <td class="t-align-center"><%=URLDecoder.decode(evalList.get(0).getFieldName3(),"UTF-8")%></td>
                                 </tr>
                                 <%} }%>
                             </table>
                                <%}%>
               <!--  </form> -->
                 
            </div>
            <div>&nbsp;</div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
       <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>

</html>
