<%-- 
    Document   : TOSSigningApp
    Created on : Nov 22, 2010, 3:16:31 PM
    Author     : rajesh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Date" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.SHA1HashEncryption" %>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Committee Listing</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function(){
                $('#frmTOS').validate({
                    rules:{
                        txtpassword:{required:true},
                        txtcomments:{
                            required:true,
                            maxlength:500}
                    },
                    messages:{
                        txtpassword:{required:"<div class='reqF_1'>Please enter Password.</div>"},
                        txtcomments:{
                            required:"<div class='reqF_1'>Please enter Comments.</div>",
                            maxlength:"<div class='reqF_1'>Only 500 characters are allowed.</div>"}
                    }
                });
            })
        </script>

    </head>
    <body>
        <%
                    String i_tenderId = "";
                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                    //Fetch tenderid from querysting and get corrigendumid
                    if (!request.getParameter("id").equals("")) {
                        i_tenderId = request.getParameter("id");
                    }

                    //This condition is used to submit data in database, this is button click event.
                    if (request.getParameter("btnsubmit") != null) {
                        
                        //Get password and check in database for existence, if found perform insert operation
                        //else show message of invalid password.
                        //System.out.print(SHA1HashEncryption.encodeStringSHA1(request.getParameter("txtpassword")));
                        TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        List<SPTenderCommonData> list = tenderCommonService1.returndata("checkLogin", request.getParameter("hiddenemail"),  SHA1HashEncryption.encodeStringSHA1(request.getParameter("txtpassword")));

                        if (!list.isEmpty()) {
                             if (!request.getParameter("id").equals("") && !request.getParameter("uid").equals("") && !request.getParameter("tid").equals("")) {
                            String dtXml = "";
                            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            //dtXml = "<root><tbl_TosSheetSign tosId=\"" + request.getParameter("hiddenid") + "\" tenderId=\"" + request.getParameter("id") + "\" "
                            dtXml = "<root><tbl_TosSheetSign tenderId=\"" + request.getParameter("tid") + "\" "
                                    + "userId=\"" + request.getParameter("uid") + "\" comments=\"" + handleSpecialChar.handleSpecialChar(request.getParameter("txtcomments").toString()) + "\" signedDate=\"" + format.format(new Date()) + "\" committeeId=\"" + request.getParameter("id") + "\" tosStatus=\"Approved\"/></root>";

                            
                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_TosSheetSign", dtXml, "").get(0);
                            //out.println(commonMsgChk.getMsg());

                                if (commonMsgChk.getFlag().equals(true)) {
                                    response.sendRedirect("TOS.jsp?tid="+request.getParameter("tid")+"&msgId=approved");
                                 } else {
                                    response.sendRedirect("TOSSigningApp.jsp?id="+request.getParameter("id")+"&uid="+request.getParameter("uid")+ "&tid="+request.getParameter("tid")+"&msgId=invalid");
                                 }

                            }

                        }else
                            {
                             //out.println("This is wrong password");
                             response.sendRedirect("TOSSigningApp.jsp?id="+request.getParameter("id")+"&uid="+request.getParameter("uid")+ "&tid="+request.getParameter("tid")+"&msgId=invalid");
                        }
                    }

        %>
       

            <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <div class="topHeader">
                    <%@include file="../resources/common/AfterLoginTop.jsp" %>
                </div>
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                 <form id="frmTOS" action="TOSSigningApp.jsp?id=<%=request.getParameter("id")%>&uid=<%=request.getParameter("uid")%>&tid=<%=request.getParameter("tid")%>" method="post">
                <div class="contentArea_1">
                <div>&nbsp;</div>
                <div class="pageHead_1">Committee Member’s Approval
                &nbsp;<span style="float: right;" ><a href="TOS.jsp?tid=<%=request.getParameter("tid") %>" class="action-button-goback">Go Back to Dashboard</a></span>
                </div>
<%
                   
                        // Variable tenderId is defined by u on ur current page.
                        pageContext.setAttribute("tenderId", request.getParameter("tid"));
            %>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
                <div style="font-style: italic" class="t-align-left t_space">
          Fields marked with (<span class="mandatory">*</span>) are mandatory
    </div>
                <div>&nbsp;</div>
                <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("invalid")){
                           isError=true; msgTxt="This is wrong password.";
                        } else if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError && msgTxt!=""){%>
                   <div>&nbsp;</div>
                   <div class="responseMsg errorMsg"><%=msgTxt%></div>
                   <%} else if (msgTxt!="") {%>
                   <div>&nbsp;</div>
                   <div class="responseMsg successMsg"><%=msgTxt%></div>
                   <%}%>
                <%}}%>
                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <td width="10%" class="ff">e-mail ID :  </td>
                        <%
                                    //TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    List<SPTenderCommonData> list = tenderCommonService.returndata("getCommitteeInfo", i_tenderId, request.getParameter("uid"));
                                    if (!list.isEmpty()) {
                        %>
                        <td width="90%"><%=list.get(0).getFieldName1()%><input type="hidden" name="hiddenid" id="hiddenid" value="<%=list.get(0).getFieldName4()%>"/>
                        <input type="hidden" name="hiddenemail" id="hiddenemail" value="<%=list.get(0).getFieldName1()%>"/>
                        </td>

                        <%}%>
                    </tr>
                    <tr>
                        <td class="ff" valign="top">Password : <span class="mandatory">*</span> </td>
                        <td>
                            <input name="txtpassword" type="password" class="formTxtBox_1" id="textfield" style="width:200px;" autocomplete="off" />
                        </td>
                    </tr>
                    <tr>
                        <td class="ff" valign="top">Comments : <span class="mandatory">*</span></td>
                        <td>
                            <textarea name="txtcomments" id="txtcomments" class="formTxtBox_1" cols="60" rows="5"></textarea>
                        </td>
                    </tr>
                </table>
                <div class="t-align-center t_space">
                    <label class="formBtn_1">
                        <input name="btnsubmit" type="submit" value="Submit" />
                    </label>
                </div>
                <div>&nbsp;</div>
                </div>
                      </form>

                    <!--Dashboard Content Part End-->
                 <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
            </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
