<%-- 
    Document   : TORSigningApp
    Created on : Apr 8, 2011, 4:45:50 PM
    Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List" %>
<%@page import="java.util.Date" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.SHA1HashEncryption" %>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Committee Listing</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <script type="text/javascript">

            $(document).ready(function(){
                $('#frmTOS').validate({
                    rules:{
                        txtpassword:{required:true},
                        txtcomments:{
                            required:true,
                            maxlength:500}
                    },
                    messages:{
                        txtpassword:{required:"<div class='reqF_1'>Please enter Password.</div>"},
                        txtcomments:{
                            required:"<div class='reqF_1'>Please enter Comments.</div>",
                            maxlength:"<div class='reqF_1'>Only 500 characters are allowed.</div>"}
                    }
                });
            })
            function validate(){
                var vbool = false;
                if($('#frmTOS').valid()){
                    $('#hdnsubmit').val("Submit")
                    $('#btnsubmit').attr("disabled", true);
                    vbool = true;
                }
                return vbool;
            }
            function changeLabel(val){
                if(val == "I Agree"){
                    $('#com').show();
                    $('#nod').hide();
                }
                if(val == "I Disagree"){
                    $('#com').hide();
                    $('#nod').show();
                }
            }
        </script>

    </head>
    <body>
        <%
                    String i_tenderId = "";
                    String strTenderId = "";
                    String strLotId = "0";
                    String strUserId = "";
                    String strGovUserId = "";
                    String strReportType = "";
                    String strReferer = "";
                    String stat = "";
                    String roundId = "0";
                    String nroundId = "0";
                    boolean isDissent = false;
                    String dissentStr = "";
                    if("y".equals(request.getParameter("nDis"))){
                       isDissent = true;
                       dissentStr = "&nDis=y";
                    }

                    HttpSession hs = request.getSession();
                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                    //Fetch tenderid from querysting and get corrigendumid
                    if (!request.getParameter("id").equals("")) {
                        i_tenderId = request.getParameter("id");
                    }

                     if (hs.getAttribute("govUserId") != null) {
                     strGovUserId = hs.getAttribute("govUserId").toString();
              }

                     if (!request.getParameter("tid").equals("")) {
                        strTenderId = request.getParameter("tid");
                    }

                     if (!request.getParameter("lotId").equals("")) {
                        strLotId = request.getParameter("lotId");
                    }

                     if (!request.getParameter("rpt").equals("")) {
                        strReportType = request.getParameter("rpt");
                    }

                    if (!request.getParameter("uid").equals("")) {
                        strUserId = request.getParameter("uid");
                    }
                    if (request.getParameter("stat")!=null) {
                        stat = request.getParameter("stat");
                    }

                    if (request.getParameter("rId")!=null) {
                        roundId = request.getParameter("rId");
                    }
                    if (request.getParameter("roundId")!=null) {
                        nroundId = request.getParameter("roundId");
                    }

                    if (request.getHeader("referer") != null) {
                    strReferer = request.getHeader("referer");
                }
// Edit By Palash, Dohatec
                    String rect = "";
                    if(strReportType.equalsIgnoreCase("ter4")){

                     // if(request.getParameter("rec").equalsIgnoreCase("Yes")){
                       //rect = request.getParameter("rec").toString();

                    //}
                    rect = request.getParameter("rec").toString();
                   // System.out.println("Palash:  " + rect);
                    }
                    // System.out.println("Palash1:  " + rect);
                    
                    // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                   // Edit By Palash, Dohatec
                    AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");
                   //End
                    String idType="tenderId";
                    int auditId=Integer.parseInt(strTenderId);
                    String auditAction=strReportType.toUpperCase()+" signed by Opening Committee Member as "+request.getParameter("dissent");
                    String moduleName=EgpModule.Tender_Opening.getName();
                    if(strReportType.toUpperCase().startsWith("TER") || strReportType.toUpperCase().startsWith("PER"))
                    {
                        auditAction="TEC Members sign "+strReportType.toUpperCase() +" as "+request.getParameter("dissent");
                        moduleName=EgpModule.Evaluation.getName();
                        
                    }
                    String remarks=request.getParameter("txtcomments");
                    //Edit by Palash, Dohatec
                    String strPath = "";
                    if(strReportType.equalsIgnoreCase("ter4")){
                         strPath = "TORSigningApp.jsp?id="+i_tenderId+"&uid="+strUserId+"&tid="+strTenderId+"&lotId="+strLotId+"&rpt="+strReportType+"&stat="+stat+dissentStr+"&rId="+roundId+"&roundId="+nroundId+"&rec="+rect;
                    }
                    else {
                         strPath = "TORSigningApp.jsp?id="+i_tenderId+"&uid="+strUserId+"&tid="+strTenderId+"&lotId="+strLotId+"&rpt="+strReportType+"&stat="+stat+dissentStr+"&rId="+roundId+"&roundId="+nroundId;
                    }
                  // End, Palash

                    //This condition is used to submit data in database, this is button click event.
                    if (request.getParameter("hdnsubmit") != null) {
                       
                        //Get password and check in database for existence, if found perform insert operation
                        //else show message of invalid password.
                        //System.out.print(SHA1HashEncryption.encodeStringSHA1(request.getParameter("txtpassword")));
                        TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        
                        List<SPTenderCommonData> list =
                                tenderCommonService1.returndata("checkLogin", request.getParameter("hiddenemail"),  SHA1HashEncryption.encodeStringSHA1(request.getParameter("txtpassword")));

                        EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                        int evalCount = evalService.getEvaluationNo(Integer.parseInt(strTenderId));

                        if (!list.isEmpty()) {
                            
                          if (!request.getParameter("id").equals("") && !request.getParameter("uid").equals("") && !request.getParameter("tid").equals("")) {
                            CommonSearchDataMoreService  dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                            List<SPCommonSearchDataMore> rptEntryAvail = dataMoreService.geteGPDataMore("EvaluationRptSignChk",strTenderId,strLotId,strUserId,roundId,strReportType,String.valueOf(evalCount));
                            if(rptEntryAvail!=null && (!rptEntryAvail.isEmpty()) && rptEntryAvail.get(0).getFieldName1().equals("0")){
                            String dtXml = "";
                            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                            //Edit by Palash, Dohatec
                            CommonMsgChk commonMsgChk;
                            if ("torCommon".equalsIgnoreCase(strReportType)) {
                                    commonMsgChk =addUpdate.addUpdOpeningEvaluation("SignTER4","tor1",strTenderId,strLotId,strUserId,handleSpecialChar.handleSpecialChar(request.getParameter("txtcomments").toString()),format.format(new Date()),strGovUserId,request.getParameter("dissent").toString(),roundId,rect,String.valueOf(evalCount),"","","","","","","","").get(0);
                                    commonMsgChk =addUpdate.addUpdOpeningEvaluation("SignTER4","tor2",strTenderId,strLotId,strUserId,handleSpecialChar.handleSpecialChar(request.getParameter("txtcomments").toString()),format.format(new Date()),strGovUserId,request.getParameter("dissent").toString(),roundId,rect,String.valueOf(evalCount),"","","","","","","","").get(0);
                                }
                            else{
                                commonMsgChk =addUpdate.addUpdOpeningEvaluation("SignTER4",strReportType,strTenderId,strLotId,strUserId,handleSpecialChar.handleSpecialChar(request.getParameter("txtcomments").toString()),format.format(new Date()),strGovUserId,request.getParameter("dissent").toString(),roundId,rect,String.valueOf(evalCount),"","","","","","","","").get(0);
                            }//End, Palash

                                if (commonMsgChk.getFlag()) {
                                    //response.sendRedirect("TOS.jsp?tid="+request.getParameter("tid")+"&msgId=approved");
                                     if("tor1".equalsIgnoreCase(strReportType)){
                                        //response.sendRedirectFilter strPath=request.getContextPath()+"/officer/TOR1.jsp?tenderid="+strTenderId+"&lotId="+strLotId+"&stat="+stat+"&msgId=approved";
                                        strPath="TOR1.jsp?tenderid="+strTenderId+"&lotId="+strLotId+"&stat="+stat+"&msgId=approved&rId="+roundId;
                                     }
                                     if("tor2".equalsIgnoreCase(strReportType)){
                                        //response.sendRedirectFilter strPath=request.getContextPath()+"/officer/TOR2.jsp?tenderid="+strTenderId+"&lotId="+strLotId+"&stat="+stat+"&msgId=approved";
                                        strPath="TOR2.jsp?tenderid="+strTenderId+"&lotId="+strLotId+"&stat="+stat+"&msgId=approved&rId="+roundId;
                                     }    
                                     if("torCommon".equalsIgnoreCase(strReportType)){
                                        //response.sendRedirectFilter strPath=request.getContextPath()+"/officer/TOR2.jsp?tenderid="+strTenderId+"&lotId="+strLotId+"&stat="+stat+"&msgId=approved";
                                        strPath="TORCommon.jsp?tenderid="+strTenderId+"&lotId="+strLotId+"&stat="+stat+"&msgId=approved&rId="+roundId;
                                     } 
                                     if("ter1".equalsIgnoreCase(strReportType)){
                                        //response.sendRedirectFilter strPath=request.getContextPath()+"/officer/TER1.jsp?tenderid="+strTenderId+"&lotId="+strLotId+"&stat="+stat+"&msgId=approved&isview=y";
                                        strPath="TER1.jsp?tenderid="+strTenderId+"&lotId="+strLotId+"&stat="+stat+"&msgId=approved&isview=y&rId="+roundId+"&sign=y&evalCount="+evalCount;
                                     }
                                     if("ter2".equalsIgnoreCase(strReportType)){
                                        //response.sendRedirectFilter strPath=request.getContextPath()+"/officer/TER2.jsp?tenderid="+strTenderId+"&lotId="+strLotId+"&stat="+stat+"&msgId=approved&isview=y";
                                        strPath="TER2.jsp?tenderid="+strTenderId+"&lotId="+strLotId+"&stat="+stat+"&msgId=approved&isview=y&rId="+roundId+"&roundId="+nroundId+"&sign=y&evalCount="+evalCount;
                                     }
                                     if("ter3".equalsIgnoreCase(strReportType)){
                                        //response.sendRedirectFilter strPath=request.getContextPath()+"/officer/TER3.jsp?tenderid="+strTenderId+"&lotId="+strLotId+"&stat="+stat+"&msgId=approved";
                                        strPath="TER3.jsp?tenderid="+strTenderId+"&lotId="+strLotId+"&stat="+stat+"&msgId=approved&rId="+roundId+"&isview=y&evalCount="+evalCount;
                                     }
                                     if("ter4".equalsIgnoreCase(strReportType)){
                                        //response.sendRedirectFilter strPath=request.getContextPath()+"/officer/TER4.jsp?tenderid="+strTenderId+"&lotId="+strLotId+"&stat="+stat+"&msgId=approved";
                                        strPath="TER4.jsp?tenderid="+strTenderId+"&lotId="+strLotId+"&stat="+stat+"&msgId=approved&rId="+roundId+"&isview=y&evalCount="+evalCount;
                                     }
                                      
                                 } else 
                                 {
                                        
                                        auditAction="Error in "+auditAction;
                                        
                                    //response.sendRedirect("TORSigningApp.jsp?id="+request.getParameter("id")+"&uid="+request.getParameter("uid")+ "&tid="+request.getParameter("tid")+"&msgId=invalid");
                                    strPath+="&msgId=invalid";
                                 }
                              }else{
                                   
                                   auditAction="Error in "+auditAction;
                                   
                                   strPath+="&msgId=error";
                                   //To stop repetetive entry
                              }
                            }

                        }else{
                            
                             auditAction="Error in "+auditAction;
                             
                             //out.println("This is wrong password");
                             strPath+="&msgId=invalid";
                             //strPath="TORSigningApp.jsp?id="+request.getParameter("id")+"&uid="+strUserId+ "&tid="+strTenderId+"&lotId="+strLotId+"&rpt="+strReportType+"&stat="+stat+"&msgId=invalid";
                        }
                        
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                        response.sendRedirect(strPath);
                    }
        %>


            <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <div class="topHeader">
                    <%@include file="../resources/common/AfterLoginTop.jsp" %>
                </div>
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <form id="frmTOS" action="<%=strPath%>" method="post" onsubmit="return validate();">
                <div class="contentArea_1">
                <div>&nbsp;</div>
                <div class="pageHead_1">Committee Member’s Approval
                &nbsp;<span style="float: right;" ><a href="<%=strReferer%>" class="action-button-goback">Go Back to Dashboard</a></span>
                </div>
                <%
                            // Variable tenderId is defined by u on ur current page.
                            pageContext.setAttribute("tenderId", request.getParameter("tid"));
                %>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
                <div style="font-style: italic" class="t-align-left t_space">
          Fields marked with (<span class="mandatory">*</span>) are mandatory
    </div>
                <div>&nbsp;</div>
                <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("invalid")){
                           isError=true; msgTxt="This is wrong password.";
                        } else if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError && msgTxt!=""){%>
                   <div>&nbsp;</div>
                   <div class="responseMsg errorMsg"><%=msgTxt%></div>
                   <%} else if (msgTxt!="") {%>
                   <div>&nbsp;</div>
                   <div class="responseMsg successMsg"><%=msgTxt%></div>
                   <%}%>
                <%}}%>


                <%
                            List<SPTenderCommonData> list = tenderCommonService.returndata("getCommitteeInfo", i_tenderId, strUserId);
                            
                            if (!list.isEmpty()) {
                %>

                <table width="100%" cellspacing="0" class="tableList_1">
                     <tr>
                        <td width="15%" class="ff">Committee Member's Name : </td>
                        <td width="85%"><%=list.get(0).getFieldName6()%>
                            <input type="hidden" id="hdnName" name="hdnName" value="<%=list.get(0).getFieldName6()%>"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" class="ff">e-mail ID : </td>
                       
                        <td width="85%"><%=list.get(0).getFieldName1()%><input type="hidden" name="hiddenid" id="hiddenid" value="<%=list.get(0).getFieldName4()%>"/>
                        <input type="hidden" name="hiddenemail" id="hiddenemail" value="<%=list.get(0).getFieldName1()%>"/>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff" valign="top">Password : <span class="mandatory">*</span> </td>
                        <td>
                            <input name="txtpassword" type="password" class="formTxtBox_1" id="textfield" style="width:200px;" autocomplete="off" />
                        </td>
                    </tr>
                    <%if(isDissent){%>
                    <tr>
                        <td class="ff" valign="top">Action : <span class="mandatory">*</span> </td>
                        <td>
                            <select style="width:200px;" class="formTxtBox_1" name="dissent" onchange="changeLabel(this.value);">
                                <option value="I Agree">I Agree</option>
                                <option value="I Disagree">I Disagree</option>
                            </select>
                        </td>
                    </tr>
                    <%}else{out.print("<input type='hidden' value='I Agree' name='dissent'/>");}%>
                    <tr>
                        <td class="ff" valign="top"><span id="com">Comments</span><span id="nod" style="display: none;">Note of Dissent</span> : <span class="mandatory">*</span></td>
                        <td>
                            <textarea name="txtcomments" id="txtcomments" class="formTxtBox_1" cols="60" rows="5"></textarea>
                        </td>
                    </tr>
                </table>
                <table width="100%" cellspacing="0" class="t_space">
                    <tr><td width="15%"></td><td width="85%" class="t-align-left">
                    <label class="formBtn_1">
                        <input name="btnsubmit" type="submit" value="Sign" id="btnsubmit"/>
                        <input name="hdnsubmit" type="hidden" id="hdnsubmit"/>
                    </label>
                </td></tr></table>
                     <%}%>


                <div>&nbsp;</div>
                </div>
                      </form>

                    <!--Dashboard Content Part End-->
                 <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
            </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
