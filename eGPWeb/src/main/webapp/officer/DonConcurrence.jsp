<%-- 
    Document   : DonConcurrence
    Created on : Oct 28, 2010, 7:26:02 PM
    Author     : dhruti
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
            <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>e-GP Administration - Default Workflow Configuration</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
<script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
     <script type="text/javascript">
            $(document).ready(function() {
                $("#frmDonConcurrence").validate({
                    rules: {
                        donor: {required: true}
                    },
                    messages: {
                    donor: { required: "<div class='reqF_1'>Please select donor concurrence requires.</div>"}
                           }

                });
            });
        </script>
</head>
<body>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <div class="topHeader">
    <table width="100%" cellspacing="0">
      <tr valign="top">
        <td><div class="dash_menu_1">
            <ul>
              <li><a href="#"><img src="../resources/images/Dashboard/msgBoxIcn.png" />Message Box</a></li>
              <li><a href="#"><img src="../resources/images/Dashboard/appIcn.png" />APP</a></li>
              <li class="mSel"><a href="#"><img src="../resources/images/Dashboard/tenderIcn.png" />Tender</a></li>
              <li><a href="#"><img src="../resources/images/Dashboard/committeeIcn.png" />Evaluation</a></li>
              <li><a href="#"><img src="../resources/images/Dashboard/reportIcn.png" />Report</a></li>
              <li><a href="#"><img src="../resources/images/Dashboard/myAccountIcn.png" />My Account</a></li>
              <li><a href="#"><img src="../resources/images/Dashboard/helpIcn.png" />Help</a></li>
            </ul>
          </div>
          <table width="100%" cellspacing="6" class="loginInfoBar">
            <tr>
              <td align="left"><b>Friday 27/08/2010 21:45</b></td>
              <td align="center"><b>Last Login :</b> Friday 27/08/2010 21:45</td>
              <td align="right"><img src="../resources/images/Dashboard/userIcn.png" class="linkIcon_1" /><b>Welcome,</b> User   &nbsp;|&nbsp; <img src="../resources/images/Dashboard/logoutIcn.png" class="linkIcon_1" alt="Logout" /><a href="#" title="Logout">Logout</a></td>
            </tr>
          </table></td>
        <td width="141"><img src="../resources/images/Dashboard/e-GP.gif" width="141" height="64" alt="e-GP" /></td>
      </tr>
    </table>
  </div>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="pageHead_1">Tender Dashboard</div>
  <div class="tableHead_1 t_space">Tender Detail</div>
  <form method="POST" id="frmDonConcurrence" action="">
  <table width="100%" cellspacing="10" class="tableView_1">
    <tr>
      <td width="100" class="ff">Tender Id :</td>
      <td>11</td>
      <td width="124" class="ff">Tender No : </td>
      <td>Test - UVM-01</td>
    </tr>

    <tr>
      <td class="ff">Due date &amp; time :</td>
      <td>02/06/2010 17:00</td>
      <td class="ff">Opening date &amp; time : </td>
      <td>02/06/2010 17:00</td>
    </tr>
    <tr>
      <td class="ff">Department :</td>
      <td colspan="3">NPCIL Group</td>
    </tr>
    <tr>
      <td class="ff">Brief :</td>
      <td colspan="3">Chittagong</td>
    </tr>
    <tr>
      <td class="ff">&nbsp;</td>
      <td colspan="3"><img src="../resources/images/Dashboard/viewIcn.png" alt="Tender/Proposal Notice" class="linkIcon_1" /><a href="#" title="Tender/Proposal Notice">Tender Notice</a> &nbsp;|&nbsp; <img src="../resources/images/Dashboard/downloadIcn.png" alt="Download Documents" class="linkIcon_1" /><a href="#" title="Download Documents">Download Documents</a></td>
    </tr>
  </table>
  <div>&nbsp;</div>
  <ul class="tabPanel_1">
    <li><a href="#">Notice &amp; Document</a></li>
    <li><a href="javascript:void(0);" class="sMenu">Workflow</a></li>
    <li><a href="#">Pre – Tender Meeting</a></li>
    <li><a href="#">Amendment</a></li>
    <li><a href="#">Opening</a></li>
    <li><a href="#">Evaluation</a></li>
    <li><a href="#">Payment</a></li>
    <li><a href="#">Letter of Acceptance (LOA)</a></li>
    <li><a href="#">Contract Signing</a></li>
  </ul>
  <div class="tabPanelArea_1">
    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
    <tr>
      <td class="ff">Donor Concurrence Requires : <span>*</span></td>
      <td>
        <select name="donor" class="formTxtBox_1" id="cmbDonor" style="width:200px;">
            <option selected="selected">No</option>
            <option>Yes</option>
        </select>

        </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><label class="formBtn_1"><input type="submit" name="submit" id="btnsubmit" value="Submit" /></label></td>
    </tr>
    </table>
  </div>
  <div>&nbsp;</div>
</form>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
  <table width="100%" cellspacing="0" class="footerCss">
    <tr>
      <td align="left">e-GP &copy; All Rights Reserved
        <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
      <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a></td>
    </tr>
  </table>
  <!--Dashboard Footer End-->
</div>
</body>
 <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabWorkFlow");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
