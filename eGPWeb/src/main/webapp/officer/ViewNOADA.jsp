<%-- 
    Document   : ViewNOADA
    Created on : Jan 10, 2011, 5:45:56 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Draft Agreement</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
    </head>
    <%
                String noaIssueId = "";
                if (request.getParameter("noaIssueId") != null) {
                    noaIssueId = request.getParameter("noaIssueId");
                }
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    tenderId = request.getParameter("tenderId");
                }
                CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    //userId = Integer.parseInt(session.getAttribute("userId").toString());
                    userId = session.getAttribute("userId").toString();
                    issueNOASrBean.setLogUserId(userId);
                    commonSearchService.setLogUserId(userId);
                    tenderCommonService.setLogUserId(userId);
                }
    %>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <div class="contentArea_1">
                <form id="frmManageIssueNOA" action="" method="POST">
                    <div class="pageHead_1">Manage Draft Agreement
                        <span class="c-alignment-right">
                            <a href="#" class="action-button-goback">Go Back To Dashboard</a>
                        </span>
                    </div>
                    <%          
                                //List<SPCommonSearchData> packageList = commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null);
                    %>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                        <% for(Object[] obj : issueNOASrBean.getListNOA(Integer.parseInt(noaIssueId))){ %>
                        <tr>
                            <td width="24%" class="ff">Contract No : <span class="mandatory">*</span></td>
                            <td width="76%"><%=obj[8] %>
                            </td>
                        </tr>
                        <tr>
                            <td width="24%" class="ff">Contract/Project name : <span class="mandatory">*</span></td>
                            <td width="76%"><%=obj[23] %>
                            </td>
                        </tr>
                        <tr>
                            <%
                                        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                            %>
                            <td class="ff">Date of Draft Agreement :</td>
                            <td class="formStyle_1"><%=dateFormat.format(obj[9]) %>
                                
                        </tr>
                        <tr>
                            <td class="ff">Name of Bidder/Consultant : </td>
                            <td>
                                <%for(SPTenderCommonData companyList : tenderCommonService.returndata("GetEvaluatedBidders",tenderId,"0")){
                                    if(obj[10].toString().equalsIgnoreCase(companyList.getFieldName2())){ %><%=companyList.getFieldName1() %> <% } } %>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Contract price In Figure (In Tk.) <span class="mandatory">*</span></td>
                            <td width="76%"><%=new BigDecimal(obj[11].toString()).setScale(2, 0) %></td>
                        </tr>
                        <tr>
                            <td class="ff">Contract price In Words (In Tk.)</td>
                            <td><label id="conPrice"><%=obj[12] %></label></td>
                        </tr>
                        <tr>
                            <td class="ff">No. of days from the date of issuance of Draft Agreement</td>
                            <% dateFormat = new SimpleDateFormat("yyyy-MM-dd"); %>
                            <td><%=obj[13]%></td>
                        </tr>
                        <tr>
                            <td class="ff">Draft Agreement acceptance last date &amp; time</td>
                            <% String noaADt=DateUtils.formatDate((Date)obj[14]);
                               String[] spaceSplitNOAADt = noaADt.split(" "); %>
                            <td><%=spaceSplitNOAADt[0] %></td>
                        </tr>
                        <tr id="perSecAmtInFig">
                            <td class="ff">Performance security amount in Figure (In Tk.) <span class="mandatory"></span></td>
                            <td width="76%"><%=new BigDecimal(obj[15].toString()).setScale(2, 0)%></td>
                        </tr>
                        <tr id="perSecAmtInWord">
                            <td class="ff">Performance security amount in Words (In Tk.)</td>
                            <td><label id="perSecAmt"><%=obj[16]%></label>
                            </td>
                        </tr>
                        <tr id="noOfDaysForPerSecSub">
                            <td class="ff">No. of days for performance security submission</td>
                            <td><%=obj[17]%>
                            </td>
                        </tr>
                        <tr id="lastDTForPerSecSub">
                            <td class="ff" >Last date &amp; time for Performance security submission </td>
                            <% String lastDt=DateUtils.formatDate((Date)obj[18]);
                               String[] spaceSplitLastDt = lastDt.split(" "); %>
                            <td><%=spaceSplitLastDt[0] %>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Signing of Contract in no. of days from the date of issuance</td>
                            <td><input type="hidden" name="contractSignDays"  value="<%=obj[19]%>"/>
                                <%=obj[19]%></td>

                        </tr>
                        <%
                                    dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                    List<SPCommonSearchData> issueCon = commonSearchService.searchData("ProcureNOA", tenderId, "", "", null, null, null, null, null, null);
                        %>
                        <tr id="modeOfPayment">
                            <td class="ff">Mode of payment</td>
                            <td><label><%if (issueCon.get(0).getFieldName4().equalsIgnoreCase("Offline/Bank")) {%>Offline,Bank<% } else {%><%=issueCon.get(0).getFieldName4()%><% }%></label>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Last Date &amp; time of contract signing</td>
                            <% String lastContractSignDt=DateUtils.formatDate((Date)obj[20]);
                               String[] spaceSplitLastContractSignDt = lastContractSignDt.split(" "); %>
                            <td><%=spaceSplitLastContractSignDt[0] %>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Clause Reference</td>
                            <td><%=obj[24] %>
                            </td>
                        </tr>
                        <% } %>
                    </table>
                    <div class="t-align-center t_space">
                        <label class="formBtn_1">
                            <input name="Update" type="submit" value="Update" />
                        </label>
                    </div>
                </form>
            </div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    
    <%
                issueNOASrBean = null;
    %>
</html>
