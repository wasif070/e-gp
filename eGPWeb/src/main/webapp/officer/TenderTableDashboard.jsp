<%--
    Document   : CreateForm
    Created on : 24-Oct-2010, 4:49:09 PM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<jsp:useBean id="tableDashboard"  class="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean" />
<jsp:useBean id="tenderFrmSrBean" class="com.cptu.egp.eps.web.servicebean.TenderFormSrBean" />
<%@page import="java.util.List" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Form Dashboard</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
    </head>
        <script>
             function delTenderTableConfirm(tenderid,formid,tableId,corriId)
        {
            var returnFlag=true;
              $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/CreateTenderFormSrvt",
                        data:"action=checkGrandSumExist&tenderId="+tenderid+"&formId="+formid+"&tableId="+tableId+"&corriId="+corriId,
                        async: false,
                        success: function(j)
			{
				var alertMessage="Are You sure You want to delete this Table?";
				if(j.toString() != null && $.trim(j.toString()) != "0")
				{
				    alertMessage= "You have already prepared a Grand Summary Report. Deletion of this table will remove Grand Summary Report. Do you want to Continue?";
                                    if(confirm(alertMessage))
                                    {
                                            returnFlag=true;
                                    }
                                    else
				    {
					returnFlag=false;
				    }
				}
			}
		});

           return returnFlag;
         }
        </script>
    <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
    %>
    <%
        int tenderId = 0;
        String tenderid = "";
        int sectionId = 0;
        int formId = 0;
        int pkgOrLotId = -1;
        
        if (request.getParameter("tenderId") != null) {
            tenderId = Integer.parseInt(request.getParameter("tenderId"));
            tenderid = request.getParameter("tenderId");
        }
        if (request.getParameter("sectionId") != null) {
            sectionId = Integer.parseInt(request.getParameter("sectionId"));
        }
        if (request.getParameter("formId") != null) {
            formId = Integer.parseInt(request.getParameter("formId"));
        }
        if (request.getParameter("templateId") != null) {
            tenderId = Integer.parseInt(request.getParameter("templateId"));
            tenderid = request.getParameter("templateId");
        }
        if(request.getParameter("porlId")!=null){
            pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
        }

        String frmName = "";
        StringBuffer frmHeader = new StringBuffer();
        StringBuffer frmFooter = new StringBuffer();
        String isBOQ = "";
        int templateFormId = 0;
        List<com.cptu.egp.eps.model.table.TblTenderForms> frm = tenderFrmSrBean.getFormDetail(formId);
        if (frm != null) {
            if (frm.size() > 0) {
                frmName = frm.get(0).getFormName();
                frmHeader.append(frm.get(0).getFormHeader());
                frmFooter.append(frm.get(0).getFormFooter());
                isBOQ = frm.get(0).getIsPriceBid();
                templateFormId = frm.get(0).getTemplateFormId();
            }
            frm = null;
        }

    %>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="contentArea_1">
            <div class="pageHead_1">
                Form Dashboard
                <%if(pkgOrLotId == -1){%>
                    <span class="c-alignment-right"><a href="TenderDocPrep.jsp?tenderId=<%= tenderId %>" title="Tender/Proposal Document" class="action-button-goback">Tender Document</a></span>
                <%}else{%>
                    <span class="c-alignment-right"><a href="TenderDocPrep.jsp?tenderId=<%= tenderId %>&porlId=<%= pkgOrLotId %>" title="Tender/Proposal Document" class="action-button-goback">Tender Document</a></span>
                <%}%>
            </div>
            
            <!--Middle Content Table Start-->
            <%
                java.util.List<com.cptu.egp.eps.model.table.TblTenderTables> tblTenderTables = tableDashboard.getTenderTables(formId);
                pageContext.setAttribute("tenderId", tenderId);
            %>
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <%
            //aprojit Start
            String userid = "";
                            if (session.getAttribute("userId") != null) {
                                Object obj = session.getAttribute("userId");
                                userid = obj.toString();
                            }
            TenderCommonService wfTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            
            List<SPTenderCommonData> checkuserRights = wfTenderCommonService.returndata("CheckTenderUserRights", userid,tenderid);
            List<SPTenderCommonData> chekTenderCreator = wfTenderCommonService.returndata("CheckTenderCreator", userid ,tenderid);
            
            String corriId=null;
            //Corrigendum Created and its status is Pending
            boolean corriCrtNPending = false;
                            List<SPTenderCommonData> lstCorri = tenderCommonService.returndata("getCorrigenduminfo", tenderId + "", null);
                            if (lstCorri != null) {
                                if (lstCorri.size() > 0) {
                    for (SPTenderCommonData data : lstCorri) {
                        if (data.getFieldName4().equalsIgnoreCase("pending")) {
                            corriCrtNPending = true;
                            corriId=data.getFieldName1();
                        }
                    }
                }
            }

                if(templateFormId == 0){
            %>
            <table width="100%" cellspacing="0" class="tableView_1 t_space b_space">
                <tr>
                    <td align="right">
                        <a href="CreateTenderFormTable.jsp?tenderId=<%= tenderId %>&sectionId=<%= sectionId %>&formId=<%= formId %>&porlId=<%=pkgOrLotId%>" onclick="addRow(document.getElementById('frmTableCreation'));" title="Add Table" class="action-button-add">Add Table</a>
                    </td>
                </tr>
            </table>
            <%
                }
            %>
            <% if(frmName != null && !"".equals(frmName.trim())){%>
            <table width="100%" cellspacing="0" class="tableHead_1 <%if(templateFormId != 0){%>t_space<%}%>">
                <tr>
                    <td width="100" class="ff">Form Name : </td>
                    <td><%=frmName%></td>
                </tr>
            </table>
                <%}%>
            <% if(frmHeader != null && frmHeader.length() > 0 && !"".equals(frmHeader.toString().trim())){%>
                <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                <tr>
                    <td width="100" class="ff">Form Header : </td>
                    <td style="line-height: 1.75"><%= frmHeader.toString() %></td>
                </tr>
            </table>
                 <%}%>

                 <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <th style="text-align:left;">Table Name</th>
                    <th style="text-align:left;">Action</th>
                </tr>
                <%
                        boolean isEntryInColumns = false;
                        if(tblTenderTables !=null){
                            for (int i = 0; i < tblTenderTables.size(); i++) {
                                isEntryInColumns = tableDashboard.isEntryPresentForCols(tblTenderTables.get(i).getTenderTableId());
                %>
                <% //Code to identify discount form
                String isDisForm = "false";
                if(tblTenderTables.get(i).getTableName().contains("Discount Form")){
                    isDisForm = "true";
                }
                %>
                <tr>
                    <%
                        boolean tableExists2 = true;
                        TenderCommonService tcsFormCheck2 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        List<Object[]> countTable2 = null;
                        countTable2 = tcsFormCheck2.checkTable(tblTenderTables.get(i).getTenderTableId());
                        if(countTable2 == null || countTable2.isEmpty())
                        {
                            tableExists2 = false;
                        }
                    %>
                    <td>
                        <%= tblTenderTables.get(i).getTableName().replace("?s","'s") %>
                        <%
                            if(tableExists2)
                            {
                                %><img id="filledUp" src="../resources/images/done.ico" border="0" title="Table Ready" style="vertical-align:middle;" /><%
                            }
                            else
                            {
                                %><img id="notFilledUp"  src="../resources/images/Pending.png" border="0" title="Table is not filled up yet." style="vertical-align:middle;" /><%
                            }
                        %>
                    </td>
                    <td>
                        <%
                            if(tblTenderTables.get(i).getTemplatetableId() == 0){
                        %>
                        <a href="CreateTenderFormTable.jsp?tenderId=<%= tenderId %>&sectionId=<%= sectionId %>&formId=<%= formId %>&tableId=<%= tblTenderTables.get(i).getTenderTableId() %>&porlId=<%= pkgOrLotId %>">Edit Table Details</a> |
                        <%
                            }
                        %>
                        <%
                            if(tblTenderTables.get(i).getTemplatetableId() == 0){
                        %>
                         <%--<img src="../resources/images/Dashboard/removeIcn.png" alt="Delete Row" class="linkIcon_1" />--%>
                         <a href="<%=request.getContextPath()%>/CreateTenderFormSrvt?action=formTableDel&tenderId=<%=tenderId%>&sectionId=<%=sectionId%>&formId=<%=formId%>&tableId=<%=tblTenderTables.get(i).getTenderTableId()%>&porlId=<%= pkgOrLotId %>&corriId=<%=corriId%>" title="Delete Table" onclick="return delTenderTableConfirm('<%=tenderId%>','<%=formId%>','<%=tblTenderTables.get(i).getTenderTableId()%>','<%=corriId%>');">Delete Table</a> |
                        <%
                            }
                        %>
                        
                        <%if(isEntryInColumns){ 
                            if (!checkuserRights.isEmpty() && userid.equalsIgnoreCase(chekTenderCreator.get(0).getFieldName1().toString())) {%>
                            <a href="TenderTableMatrix.jsp?tenderId=<%=tenderId%>&sectionId=<%=sectionId%>&formId=<%=formId%>&tableId=<%=tblTenderTables.get(i).getTenderTableId()%>&Edit=true&porlId=<%= pkgOrLotId %>&isDis=<%= isDisForm%>">Fill up the Tables</a> |
                            <% } %>
                            <a href="ViewTenderTableMatrix.jsp?tenderId=<%=tenderId%>&sectionId=<%=sectionId%>&formId=<%=formId%>&tableId=<%=tblTenderTables.get(i).getTenderTableId()%>&porlId=<%= pkgOrLotId %>">View Table Matrix</a>
                            <%
                            if(tblTenderTables.get(i).getTemplatetableId() == 0){
                                if(tableDashboard.isAutoColumnPresent(tblTenderTables.get(i).getTenderTableId())){
                                    if(tableDashboard.atLeastOneFormulaCreated(tblTenderTables.get(i).getTenderTableId())){
                            %>
                                | <a href="CreateTenderFormula.jsp?tenderId=<%=tenderId%>&sectionId=<%=sectionId%>&formId=<%=formId%>&tableId=<%=tblTenderTables.get(i).getTenderTableId()%>&porlId=<%= pkgOrLotId %>">Edit Formula</a>
                            <%
                                    }else{
                            %>
                                | <a href="CreateTenderFormula.jsp?tenderId=<%=tenderId%>&sectionId=<%=sectionId%>&formId=<%=formId%>&tableId=<%=tblTenderTables.get(i).getTenderTableId()%>&porlId=<%= pkgOrLotId %>">Create Formula</a>
                            <%
                                    }
                                }
                            }
                            %>

                        <%}else{%>
                            <img src="../resources/images/Dashboard/addIcn.png" alt="Create Table Matrix" class="linkIcon_1" />
                            <a href="TenderTableMatrix.jsp?tenderId=<%=tenderId%>&sectionId=<%=sectionId%>&formId=<%=formId%>&tableId=<%=tblTenderTables.get(i).getTenderTableId()%>&porlId=<%= pkgOrLotId %>" title="Create Table Matrix">Create Table Matrix</a>
                        <%}%>
                        
                    </td>
                </tr>
                <%
                            }
                            if(tblTenderTables.size() == 0){
                %>
                <tr>
                    <td colspan="2" align="center">
                        No Record Found
                    </td>
                </tr>
                <%
                            }
                            tblTenderTables = null;
                        }else{
                %>
                <tr>
                    <td colspan="2" align="center">
                        No Record Found
                    </td>
                </tr>
                <%
                        }
                %>
            </table>
            <% if(frmFooter != null && frmFooter.length() > 0 && !"".equals(frmFooter.toString().trim())){%>
            <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                <tr>
                    <td width="100" class="ff">Form Footer : </td>
                    <td style="line-height: 1.75"><%= frmFooter.toString() %></td>
                </tr>
            </table>
              <%}%>
            </div>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <%
        if(frmHeader != null){
            frmHeader = null;
        }
        if(frmFooter != null){
            frmFooter = null;
        }
        if (tblTenderTables != null) {
            tblTenderTables = null;
        }
        if (tableDashboard != null) {
            tableDashboard = null;
        }
        if (tenderFrmSrBean != null) {
            tenderFrmSrBean = null;
        }
    %>
</html>