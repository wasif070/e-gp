<%-- 
    Document   : PublishCT
    Created on : Aug 24, 2011, 3:13:10 PM
    Author     : Sreenu.Durga
--%>

<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cptu.egp.eps.model.table.TblCmsCTReasonType"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsCTReasonTypeBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsContractTermination"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsContractTerminationBean"%>

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Contact Termination View Page</title>
    </head>
    <%
                String tenderId = "";
                if (request.getParameter("tenderId") != null) {
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    tenderId = request.getParameter("tenderId");
                }
                int contractTerminationId = 0;
                if (request.getParameter("contractTerminationId") != null) {
                    contractTerminationId = Integer.parseInt(request.getParameter("contractTerminationId"));
                }
                String userId = "";
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    userId = session.getAttribute("userId").toString();
                }
                String lotId = "";
                if (request.getParameter("lotId") != null) {
                    lotId = request.getParameter("lotId");
                    pageContext.setAttribute("lotId", lotId);
                }
                CmsCTReasonTypeBean cmsCTReasonTypeBean = new CmsCTReasonTypeBean();
                cmsCTReasonTypeBean.setLogUserId(userId);
                List<TblCmsCTReasonType> cmsCTReasonTypeList = cmsCTReasonTypeBean.getAllCmsCTReasonType();

                CmsContractTerminationBean cmsContractTerminationBean = new CmsContractTerminationBean();
                cmsContractTerminationBean.setLogUserId(userId);
                TblCmsContractTermination tblCmsContractTermination = cmsContractTerminationBean.getCmsContractTermination(contractTerminationId);
    %>

    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <%@include  file="../resources/common/ContractInfoBar.jsp"%>
                            <br/>
                            <!--Page Content Start-->
                            <div class="pageHead_1"> Contract Termination Page
                                <span style="float: right; text-align: right;">
                                    <a class="action-button-goback" href="TabContractTermination.jsp?tenderId=<%=tenderId%>">Go back</a>
                                </span>
                            </div>
                            <form method="POST" id="formContractTermination" action="<%= request.getContextPath() %>/ContractTerminationServlet?action=approve">
                                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1" >
                                    <tr id="tblRow_dateOfTermination">
                                        <td class="ff" width="15%">Date of Termination : </td>
                                        <td>
                                            <%= tblCmsContractTermination.getDateOfTermination()%>
                                            <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>">
                                            <input type="hidden" name="contractTerminationId" id="contractTerminationId" value="<%=contractTerminationId%>">
                                            <input type="hidden" name="lotId" id="lotId" value="<%=lotId%>">
                                        </td>
                                    </tr>
                                    <tr id="tblRow_typeOfReason">
                                        <td class="ff">Type of Reason : </td>
                                        <td>
                                            <%
                                                        String[] reasonTypes = tblCmsContractTermination.getReasonType().split("\\^");
                                                        for (String reasonTypeId : reasonTypes) {
                                                            int reasonId = Integer.parseInt(reasonTypeId);
                                                            for (TblCmsCTReasonType cmsCTReasonType : cmsCTReasonTypeList) {
                                                                if (cmsCTReasonType.getCtReasonTypeId() == reasonId) {
                                                                    out.print(cmsCTReasonType.getCtReason() + "<br>");
                                                                    break;
                                                                }
                                                            }//inner for
                                                        }// outer for
%>
                                        </td>
                                    </tr>
                                    <tr id="tblRow_workFlowStatus">
                                        <td class="ff">Status : </td>
                                        <td>
                                            <%= tblCmsContractTermination.getStatus()%>
                                        </td>
                                    </tr>
<!--                                    <tr id="tblRow_releasePerformanceGuarantee" >
                                        <td class="ff">Release Performance Security : </td>
                                        <td>
                                            <a%= tblCmsContractTermination.getReleasePg()%>
                                        </td>
                                    </tr>-->
                                    <tr id="tblRow_reason">
                                        <td  class="ff">Reason For Termination : </td>
                                        <td>
                                            <%= tblCmsContractTermination.getReason()%>
                                        </td>
                                    </tr>
                                    <tr id="tblRow_formButton">                                        
                                                <%
                                                    short activityid = 10;
                                                    WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                                                    List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(tblCmsContractTermination.getContractTerminationId(), tblCmsContractTermination.getContractTerminationId(),activityid);
                                                    int initiator = 0;
                                                    int approver = 0;
                                                    boolean iswfLevelExist = false;
                                                    if (tblWorkFlowLevelConfig.size() > 0) {
                                                    Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                                    iswfLevelExist = true;
                                                    while (twflc.hasNext()) {
                                                         TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                                         TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                                         if (workFlowlel.getWfRoleId() == 1) {
                                                         initiator = lmaster.getUserId();
                                                         }
                                                         if (workFlowlel.getWfRoleId() == 2) {
                                                             approver = lmaster.getUserId();
                                                         }
                                                         }
                                                    }
                                                    String donor = "";
                                                    donor = workFlowSrBean.isDonorReq("9",tblCmsContractTermination.getContractTerminationId());
                                                    String[] norevs = donor.split("_");
                                                    int norevrs = -1;
                                                    String strrev = norevs[1];
                                                    if(!strrev.equals("null")){
                                                    norevrs = Integer.parseInt(norevs[1]);
                                                    }
                                                    if(initiator == approver && norevrs == 0)
                                                    {
                                                        tblCmsContractTermination.setWorkflowStatus("approved");
                                                    }
                                                %>
                                                <input type="hidden" name="norevrs" value="<%=norevrs%>">
                                                <td class="t-align-left">
                                                <%if("approved".equalsIgnoreCase(tblCmsContractTermination.getWorkflowStatus())){%>                                                
                                                <label class="formBtn_1">
                                                <input type="submit" align="middle" name="submit" id="submit" value="Approve"/>
                                                </label>&nbsp;
                                                
                                                <%}
                                                  if(initiator == approver && norevrs == 0)
                                                  {
                                                      tblCmsContractTermination.setWorkflowStatus("rejected");
                                                  }
                                                  if("rejected".equalsIgnoreCase(tblCmsContractTermination.getWorkflowStatus())){
                                                %>
                                                
                                                <label class="formBtn_1">
                                                <input type="submit" align="middle" name="submit" id="submit" value="Reject"/>
                                                </label>                                                
                                                <%}%>
                                                </td>
                                    </tr>
                                </table>
                            </form>
                        </td><!--Page Content End-->
                    </tr>
                </table><!--Middle Content Table End-->
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
</html>

