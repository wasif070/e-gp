<%-- 
    Document   : WorkScheduleMain
    Created on : Sep 13, 2011, 4:31:58 PM
    Author     : shreyansh
--%>

<%@page import="org.springframework.security.authentication.DisabledException"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SpGeteGPCmsDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.WorkFlowServiceImpl"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvRevari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvCcvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPsvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPsvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvWpvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSrvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSsvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTcvari"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsVariationOrder"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsWpDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<jsp:useBean id="appSrBean" class="com.cptu.egp.eps.web.servicebean.APPSrBean" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Work Schedule</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>


    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Work Schedule</div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <%
            CommonService commonServiceee = (CommonService) AppContext.getSpringBean("CommonService");
            String procnatureee = commonServiceee.getProcNature(request.getParameter("tenderId")).toString();
            List<Object[]> lotObj = commonServiceee.getLotDetails(request.getParameter("tenderId"));
            Object[] objj = null;
            if(lotObj!=null && !lotObj.isEmpty())
            {
                objj = lotObj.get(0);
                pageContext.setAttribute("lotId", objj[0].toString());
            }%>
            <%@include file="../resources/common/ContractInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
                        pageContext.setAttribute("tab", "14");
                        ResourceBundle bdl = null;
                        bdl = ResourceBundle.getBundle("properties.cmsproperty");
                        String userId = "";
                        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                            userId = session.getAttribute("userId").toString();
                            appSrBean.setLogUserId(userId);
                        }
                        WorkFlowServiceImpl workFlowServiceImpl = (WorkFlowServiceImpl) AppContext.getSpringBean("WorkFlowService");
            %>
            <%@include  file="officerTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "1");

                %>
                <%@include  file="../resources/common/CMSTab.jsp"%>
                <div class="tabPanelArea_1">
                    <% if (request.getParameter("msg") != null) {
                                    if ("edit".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.dates.edit")%> </div>
                    <%}
                         if ("sent".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.works.sentToTen")%></div>
                    <%}

                         if ("noedit".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg successMsg'><%=bdl.getString("CMS.var.success")%></div>
                    <%}
                         if ("changed".equalsIgnoreCase(request.getParameter("msg"))) {

                    %>
                    <div class='responseMsg errorMsg'><%=bdl.getString("CMS.var.Nochange")%></div>
                    <%}
                         if ("approved".equalsIgnoreCase(request.getParameter("msg"))) {               
                    %>
                    <div class='responseMsg successMsg'>Send to Consultant Successfully</div>
                    <%         
                    }if ("done".equalsIgnoreCase(request.getParameter("msg"))) {               
                    %>
                    <div class='responseMsg successMsg'>Form detail(s) updated successfully</div>
                    <%
                    }if ("finlise".equalsIgnoreCase(request.getParameter("msg"))) {
                    %>
                    <div class='responseMsg successMsg'>Variation Order Finalized Successfully</div>
                    <%
                    }}
                    %>
                    <div>

                        <%
                                    String tenderId = request.getParameter("tenderId");
                                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                    List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetails", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
                                    int i = 0;
                                    int lotid = 0;

                                    boolean isTimeBased = false;
                                    CommonService commService = (CommonService) AppContext.getSpringBean("CommonService");
                                    String serviceType = commService.getServiceTypeForTender(Integer.parseInt(tenderId));
                                    boolean disbleEditlink = false;
                                    if("Time based".equalsIgnoreCase(serviceType.toString())){
                                        isTimeBased = true;
                                    }else{
                                        disbleEditlink = service.checkForItemFullyReceivedOrNotForServices(Integer.parseInt(tenderId));
                                        }
                                    List<TblCmsVariationOrder> ifVari =  cmss.getListOfVariationOrderForSrv(Integer.parseInt(tenderId));
                                    CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                    boolean allew = true;

                                    for (SPCommonSearchDataMore lotList : packageLotList) {
                                        List<Object> wpid = service.getWpId(Integer.parseInt(lotList.getFieldName5()));
                                        boolean flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotList.getFieldName5()));
                                        if(flags){
                        %>

                        <form name="frmcons" method="post">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <tr>
                                    <td width="20%">Package No.</td>
                                    <td width="80%"><%=lotList.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td>Package Description</td>
                                    <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                </tr>
                            </table>
                                    <%if(ifVari==null || ifVari.isEmpty() || ifVari.get(ifVari.size()-1).getVariOrdStatus().equalsIgnoreCase("accepted") || ifVari.get(ifVari.size()-1).getVariOrdWFStatus().equalsIgnoreCase("rejected")){%>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                    <tr>
                                        <td class="ff t_align_left">Variation Order</td>
                                    <td width="80%"><a href="<%=request.getContextPath()%>/CMSSerCaseServlet?tenderId=<%=tenderId%>&lotId=<%=lotid%>&action=placeOrder">Place Variation Order</a>
                                    </td>
                                </tr>
                                </table>
                                <%}%>

                                <%
                                                lotid = Integer.parseInt(lotList.getFieldName5().toString());
                                            %>
                                
                                
                                <%
                                            
                                            List<Object[]> serviceForms = cmss.getAllFormsForService(Integer.parseInt(tenderId));
                                %>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                    <tr>
                                        <td colspan="4">
                                            <span style="float: left; text-align: left;font-weight:bold;"><b><a href="StaffInputTotal.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>">Total Staff Input Report</a></b></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th width="3%" class="t_align_left">Sr.No</th>
                                        <th  width="55%" class="t_align_left">Forms</th>
                                        <th  width="20%" class="t_align_left">Action</th>

                                        <% if(!ifVari.isEmpty()) {
                                            if(ifVari.get(ifVari.size() - 1).getVariOrdStatus().equalsIgnoreCase("pending")) {
                                            %>
                                            <th width="10%" class="t_align_left">Variation Order</th>
                                        <%  } } %>
                                        
                                    </tr>

                                    <% int serivceSrNO = 1;
                                                List<Object> data = null;
                                                for (Object[] obj : serviceForms) {%>
                                    <tr>
                                        <td class="t_align_left"><%=serivceSrNO%></td>
                                        <td class="t_align_left"><%=obj[0]%></td>
                                        <%
                                                                                        data = cmss.dataSucssDump(Integer.parseInt(tenderId), (Integer) obj[1]);
                                                                                        if (data != null && !data.isEmpty()) {
                                                                                            int srvBoqId = Integer.parseInt(obj[2].toString());
                                                                                            if (srvBoqId == 19) {
                                         %>
                                        <td style="">
                                            <%if(!disbleEditlink){%>
                                            <a href="ServiceWorkPlan.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&lotId=<%=lotid%>&srvBoqId=<%=obj[2]%>&Type=Edit">Edit</a> |
                                            <%}%>
                                            <a href="ViewAllForms.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View</a> | <a href="ViewWorkServiceHistoryLink.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&lotId=<%=lotid%>&srvBoqId=<%=obj[2]%>&Type=Edit">View History</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        </td>
                                         <% if(!ifVari.isEmpty()) {
                                            if(ifVari.get(ifVari.size() - 1).getVariOrdStatus().equalsIgnoreCase("pending")) {
                                                List<TblCmsSrvWpvari> srvWpvaris = cmss.getSrvWpVariationOrder(ifVari.get(ifVari.size() - 1).getVariOrdId());
                                                if(srvWpvaris.isEmpty()){
                                            %>
                                        <td class="t_align_left"><a href="ServiceWpVariationOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>">Variation Order</a></td>                                        
                                         <%  }else{%>
                                        <td class="t_align_left"><a href="ServiceWpVariationOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>&isEdit=y">Edit</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>&docx=VariOrder&module=WS">Upload / Download Document</a>
                                        </td>
                                        <%  } } } %>
                                        <% } else if (srvBoqId == 12) {%>
                                        <td style="">
                                            <% if(!isTimeBased){ %>
                                            <%if(!disbleEditlink){%>
                                            <a href="SrvPaymentSchedule.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&formMapId=<%=data.get(0)%>">Edit</a> |
                                            <% } %>
                                            <% } %>
                                            <a href="ViewAllForms.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View</a>
                                            <% if(!isTimeBased){ %>
                                            | <a href="ViewPSHistoryLink.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View History</a>
                                        <% } %>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        </td>
                                         <% if(!isTimeBased){ %>
                                        
                                                     <% if(!ifVari.isEmpty()) {
                                            if(ifVari.get(ifVari.size() - 1).getVariOrdStatus().equalsIgnoreCase("pending")) {
                                                List<TblCmsSrvPsvari> srvPsvaris = cmss.getSrvPsVariationOrder(ifVari.get(ifVari.size() - 1).getVariOrdId());
                                                if(srvPsvaris.isEmpty()){
                                            %>

                                            <td class="t_align_left"><a href="ServicePsVariationOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>">Variation Order</a></td>
                                        <%  }else{%>
                                               <td class="t_align_left"><a href="ServicePsVariationOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>&isEdit=y">Edit</a>
                                               &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>&docx=VariOrder&module=WS">Upload / Download Document</a>
                                               </td>
                                        <%  } } }%>
                                        
                                        <% } else { %>
                                        <% } %>
                                        <% } else if (srvBoqId == 18) {%>

                                        <td style="">
                                            <%if(!disbleEditlink){%>
                                            <a href="SrvConsltCompo.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&formMapId=<%=data.get(0)%>">Edit</a> |
                                            <%}%>
                                            <a href="ViewAllForms.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View</a> | <a href="ViewHistoryForCC.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View History</a>
                                        &nbsp;|&nbsp;
                                        <a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        </td>
                                       <% if(!ifVari.isEmpty()) {
                                            if(ifVari.get(ifVari.size() - 1).getVariOrdStatus().equalsIgnoreCase("pending")) {
                                                List<TblCmsSrvCcvari> srvCcvaris = cmss.getSrvCcVariationOrder(ifVari.get(ifVari.size() - 1).getVariOrdId());
                                                if(srvCcvaris.isEmpty()){
                                            %>
                                        <td class="t_align_left"><a href="ServiceCcVariationOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>">Variation Order</a>
                                        
                                        </td>
                                        <%  }else{%>
                                                <td class="t_align_left"><a href="ServiceCcVariationOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>&isEdit=y">Edit</a>
                                                &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>&docx=VariOrder&module=WS">Upload / Download Document</a>
                                                </td>
                                        <%  } } }%>
                                        <% } else if (srvBoqId == 13) {%>
                                        <td style="">
                                            <%if(!disbleEditlink){%>
                                            <a href="SrvTeamCompoTaskAssig.jsp?tenderId=<%=tenderId%>&lotId=<%= lotid%>&formMapId=<%=data.get(0)%>">Edit</a> |
                                            <%}%>
                                            <a href="ViewAllForms.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View</a>
                                        | <a href="ViewTCHistory.jsp?tenderId=<%=tenderId%>&lotId=<%= lotid%>&formMapId=<%=data.get(0)%>">View History</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        </td>
                                        <% if(!ifVari.isEmpty()) {
                                            if(ifVari.get(ifVari.size() - 1).getVariOrdStatus().equalsIgnoreCase("pending")) {
                                                List<TblCmsSrvTcvari> srvTcvaris = cmss.getDetailOfTCForVari(ifVari.get(ifVari.size() - 1).getVariOrdId());
                                                if(srvTcvaris.isEmpty()){
                                            %>
                                            <td class="t_align_left"><a href="TCVariOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>&varId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>">Variation Order</a></td>
                                        <%  }else{%>
                                        <td class="t_align_left"><a href="TCVariOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>&varId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>&isEdit=y">Edit</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>&docx=VariOrder&module=WS">Upload / Download Document</a>
                                        </td>
                                            <%} } }%>
                                        <% } else if (srvBoqId == 14) {%>
                                        <td style="">
                                            <%if(!disbleEditlink){%>
                                            <a href="SrvStaffingSchedule.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&formMapId=<%=data.get(0)%>">Edit</a> |
                                            <%}%>
                                            <a href="ViewAllForms.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View</a> | <a href="ViewSsHistoryLink.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&lotId=<%=lotid%>&srvBoqId=<%=obj[2]%>">View History</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        </td>
                                        <% if(!ifVari.isEmpty()) {
                                            if(ifVari.get(ifVari.size() - 1).getVariOrdStatus().equalsIgnoreCase("pending")) {
                                                List<TblCmsSrvSsvari> srvSsvaris = cmss.getDetailOfSSForVari(ifVari.get(ifVari.size() - 1).getVariOrdId());
                                                if(srvSsvaris.isEmpty()){
                                            %>
                                            <td class="t_align_left"><a href="SSVariOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>&varId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>">Variation Order</a></td>
                                        <%  }else{%>
                                        <td class="t_align_left"><a href="SSVariOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>&varId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>&isEdit=y">Edit</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>&docx=VariOrder&module=WS">Upload / Download Document</a>
                                        </td>
                                            <%} } }%>
                                        <% } else if (srvBoqId == 15) {%>
                                        <td style=""> <a href="ViewAllForms.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        </td>
                                       <% if(!ifVari.isEmpty()) {
                                            if(ifVari.get(ifVari.size() - 1).getVariOrdStatus().equalsIgnoreCase("pending")) {
                                                List<TblCmsSrvSrvari> srvTcvaris = cmss.getDetailOfSRForVari(ifVari.get(ifVari.size() - 1).getVariOrdId());
                                                if(srvTcvaris.isEmpty()){
                                            %>
                                            <td class="t_align_left"><a href="SrVariOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>&varId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>">Variation Order</a></td>
                                        <%  }else{%>
                                        <td class="t_align_left"><a href="SrVariOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>&varId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>&isEdit=y">Edit</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>&docx=VariOrder&module=WS">Upload / Download Document</a>
                                        </td>
                                            <%} } }%>
                                        <% } else if (srvBoqId == 16) {%>
                                        <td style="">
                                            <%if(!disbleEditlink){%>
                                            <a href="SrvReExpense.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&formMapId=<%=data.get(0)%>">Edit</a> |
                                            <%}%>
                                            <a href="ViewAllForms.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View</a> | <a href="ViewHistoryForRE.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">View History</a>
                                        &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS">Upload / Download Document</a>
                                        </td>
                                       <% if(!ifVari.isEmpty()) {
                                            if(ifVari.get(ifVari.size() - 1).getVariOrdStatus().equalsIgnoreCase("pending")) {
                                                List<TblCmsSrvRevari> srvRevaris = cmss.getSrvReVariationOrder(ifVari.get(ifVari.size() - 1).getVariOrdId());
                                                if(srvRevaris.isEmpty()){
                                            %>
                                        <td class="t_align_left"><a href="ServiceReVariationOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>">Variation Order</a></td>
                                         <%  }else{%>
                                                <td class="t_align_left"><a href="ServiceReVariationOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>&isEdit=y">Edit</a>
                                                &nbsp;|&nbsp;<a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&varOrdId=<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>&docx=VariOrder&module=WS">Upload / Download Document</a>
                                                </td>
                                        <%  } } }%>
                                        <% } else if (srvBoqId == 20) {
                                            SpGeteGPCmsDataMore spcsdm = (SpGeteGPCmsDataMore)AppContext.getSpringBean("SpGeteGPCmsDataMore");
                                            List<SPCommonSearchDataMore> listBidderId = spcsdm.executeProcedure("getNOADetailWithTenderId", tenderId,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);
                                            String bidderId = "";
                                            if(listBidderId!=null && !listBidderId.isEmpty()){
                                                bidderId = listBidderId.get(0).getFieldName1();
                                            }
                                        %>
                                        <td><a href="WorkScheduleUploadDoc.jsp?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&keyId=<%=data.get(0).toString()%>&docx=Worksch&module=WS&param=hide">Download Document</a>
                                            | 
                                            <a view='link' onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/ViewBOQForms.jsp?formId=<%=obj[1] %>&userId=<%=bidderId%>&nture=Services&tenId=<%=tenderId%>&ws=t', '', 'width=1200px,height=600px,scrollbars=1','');" href="javascript:void(0);">View</a>
                                        </td>
                                        <% if(!ifVari.isEmpty()) {
                                            if(ifVari.get(ifVari.size() - 1).getVariOrdStatus().equalsIgnoreCase("pending")) {
                                                List<TblCmsSrvRevari> srvRevaris = cmss.getSrvReVariationOrder(ifVari.get(ifVari.size() - 1).getVariOrdId());
                                                if(srvRevaris.isEmpty()){
                                            %>
                                        <td class="t_align_left">
                                        -
                                        </td>
                                         <%  }else{%>
                                                <td class="t_align_left">
                                                -
                                                </td>
                                        <%  } } }%>
                                        <% } else {%>
                                        <td style="">-</td>
                                        <% if(!ifVari.isEmpty()) {
                                            if(ifVari.get(ifVari.size() - 1).getVariOrdStatus().equalsIgnoreCase("pending")) {
                                            %>
                                        <td class="t_align_left"><a href="ServiceWpVariationOrder.jsp?formMapId=<%=data.get(0)%>&tenderId=<%=tenderId%>&srvBoqId=<%=obj[2]%>&lotId=<%=lotid%>">Variation Order</a></td>
                                        <%  } } %>
                                        <%}%>
                                    </tr>
                                    <%serivceSrNO++;

                                                    }
                                                }
                                    %>
                                    <%
if(!ifVari.isEmpty()){
boolean mores = cmss.checkIfItemAddedInVariOrder(ifVari.get(ifVari.size()-1).getVariOrdId());
if(mores){
if(ifVari.get(ifVari.size()-1).getVariOrdStatus().equalsIgnoreCase("pending")){%>
                                    <tr><td colspan="4">
                                            <center>
                                                <%if(cmss.allowFinaliseVari(ifVari.get(ifVari.size()-1).getVariOrdId())==0){%>
                                    <label class="formBtn_1">
                                        <input type="button" name="Boqbutton" id="Boqbutton" value="Finalize Variation Order" onclick="finalise(<%=ifVari.get(ifVari.size()-1).getVariOrdId()%>,<%=tenderId%>,<%=lotid%>);" />
                                    </label>
                                    <%}else{%>
                                     <label class="formBtn_1">
                                        <input type="button" name="Boqbutton" id="Boqbutton" value="Finalize Variation Order" onclick="jAlert('It is mandatory to add all empolyee name in staffing schedule and salary reimbursable','Variation Order', function(RetVal) {});" />
                                    </label>
                                    <%}%>
                                                </center>
                                    </td></tr>
                                <%}}}%>                                </table>


                                <%

                                int ii =0;
                                int ij =1;
                                if(!ifVari.isEmpty()){

    %>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                    <th class="t_align_center">Sl. No.</th>
                    <th class="t_align_center">Variation Order</th>
                    <th class="t_align_center">Creation Date</th>
                    <th class="t_align_center">Status</th>
                    <th class="t_align_center">Action</th>

                <%for(TblCmsVariationOrder order : ifVari){
                    if(!order.getVariOrdStatus().equalsIgnoreCase("pending")){
                %>
                    <tr>
                    <td class="t_align_left"><%=ij%></td>
                    <td class="t_align_left"><%=order.getVariationNo()%></td>
                    <td class="t_align_left"><%=DateUtils.gridDateToStrWithoutSec(order.getVariOrdCreationDt())%></td>
                    <td class="t_align_left"><%if(order.getVariOrdStatus().equalsIgnoreCase("sendtoten")){out.print("Sent to Consultant");}else if(order.getVariOrdStatus().equalsIgnoreCase("finalise")){out.print("Finalized");}else{out.print((String.valueOf(Character.toUpperCase(order.getVariOrdStatus().charAt(0))).concat(order.getVariOrdStatus().substring(1))).replace('s', 'z')) ;} %></td>
                    <td class="t_align_left">

                        <%--<%if(ifVari.get(ii).getVariOrdStatus().equalsIgnoreCase("finalise") || ifVari.get(ii).getVariOrdStatus().equalsIgnoreCase("accepted")){%>
                        <a href="ViewAllVariOrderForms.jsp?tenderId=<%=tenderId%>&varId=<%=ifVari.get(ii).getVariOrdId()%>">View</a>
                        <%}else{%>
                        <%}%>--%>
                        <%if(ifVari.get(ii).getVariOrdStatus().equalsIgnoreCase("finalise") || ifVari.get(ii).getVariOrdStatus().equalsIgnoreCase("sendtoten") || ifVari.get(ii).getVariOrdStatus().equalsIgnoreCase("accepted")){%>
                            <%
                                List<Object> getFileOnSt = new ArrayList<Object>();
                                String donor = "";
                                short activityid = 11;
                                int initiator = 0;
                                int approver = 0;
                                String WorkFlowStatus ="";
                                String VariOrderStatus ="";
                                int VariOId =0;
                                boolean iswfLevelExist = false;
                                WorkFlowStatus = ifVari.get(ii).getVariOrdWFStatus();
                                VariOrderStatus = ifVari.get(ii).getVariOrdStatus();
                                VariOId = ifVari.get(ii).getVariOrdId();
                                    WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                                    List<TblWorkFlowLevelConfig> tblWorkFlowLevelConfig = workFlowSrBean.editWorkFlowLevel(VariOId, VariOId, activityid);
                                    if (tblWorkFlowLevelConfig.size() > 0) {
                                    Iterator twflc = tblWorkFlowLevelConfig.iterator();
                                    iswfLevelExist = true;
                                    while (twflc.hasNext()) {
                                         TblWorkFlowLevelConfig workFlowlel = (TblWorkFlowLevelConfig) twflc.next();
                                         TblLoginMaster lmaster = workFlowlel.getTblLoginMaster();
                                         if (workFlowlel.getWfRoleId() == 1) {
                                         initiator = lmaster.getUserId();
                                         }
                                         if (workFlowlel.getWfRoleId() == 2) {
                                             approver = lmaster.getUserId();
                                         }
                                         }}
                                         donor = workFlowSrBean.isDonorReq("10",VariOId);
                                         String[] norevs = donor.split("_");
                                         String noOfReviewers = norevs[1];
                                         getFileOnSt = appSrBean.getFileOnHandStatus(VariOId,VariOId,Integer.parseInt(userId),10);
                            %>
                                <%
                                    if("pending".equalsIgnoreCase(WorkFlowStatus) || "".equalsIgnoreCase(WorkFlowStatus))
                                    {

                                        //donor = workFlowSrBean.isDonorReq("10",VariOId);
                                        //String[] norevs = donor.split("_");
                                        //String noOfReviewers = norevs[1];
                                        //if(!"0".equalsIgnoreCase(noOfReviewers))
                                        //{
                                %>
                                            <a href="ViewAllVariOrderForms.jsp?tenderId=<%=tenderId%>&varId=<%=ifVari.get(ii).getVariOrdId()%>&wpId=0">View Variation Order</a> &nbsp;|&nbsp;
                                <%
                                        //}
                                        if("0".equalsIgnoreCase(noOfReviewers))
                                        {
                                            if(initiator!=approver)
                                            {
                                                if(tblWorkFlowLevelConfig.size()>0)
                                                {
                                                    if(!"Yes".equalsIgnoreCase(getFileOnSt.get(0).toString())){
                                       %>
                                                            &nbsp;|&nbsp;<a href="workFlowHistory.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&userid=<%=userId%>&childid=<%=VariOId%>&tenderId=<%=tenderId%>&lotId=<%=lotid%>&fraction=VariforServices&wpId=0">View Workflow History</a>
                                                            &nbsp;|&nbsp;<a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&action=View&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0">View Workflow</a>
                                       <%                         
                                                    }else{
                                       %>
                                                            <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=tenderId%>&action=Edit&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0">Edit Workflow</a>
                                                            &nbsp;|&nbsp;<a href="FileProcessing.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&userid=<%=userId%>&childid=<%=VariOId%>&&tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&fromaction=VariforServices">Process file in Workflow</a>
                                       <%                         
                                                    }    
                                                }else{
                                       %>
                                                    <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=tenderId%>&action=Edit&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0">Edit Workflow</a>
                                       <%                    
                                                }
                                                
                                            }else{
                                                  if(tblWorkFlowLevelConfig.size()>0)
                                                  {
                                                    if("finalise".equalsIgnoreCase(VariOrderStatus))
                                                    {
                                        %>
                                                            <a href="<%=request.getContextPath()%>/CMSSerCaseServlet?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&action=sendtoten&VariOId=<%=VariOId%>" >Send To Consultant</a>
                                        <%
                                                    }
                                                    else
                                                    {
                                        %>
        <!--                                                <a href="ViewAllVariOrderForms.jsp?tenderId=<%=tenderId%>&varId=<%=ifVari.get(ii).getVariOrdId()%>&wpId=0">View Variation Order</a> &nbsp;|&nbsp;-->
                                        <%                
                                                        if(initiator!=approver)
                                                        {
                                        %>
                                                            &nbsp;|&nbsp;<a href="workFlowHistory.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&userid=<%=userId%>&childid=<%=VariOId%>&tenderId=<%=tenderId%>&lotId=<%=lotid%>&fraction=VariforServices&wpId=0">View Workflow History</a>&nbsp;|&nbsp;
                                        <%
                                                        }
                                        %>
                                                        <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&action=View&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0">View Workflow</a>
                                        <%
                                                    }
                                                  }else{
                                        %>
                                                   <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=tenderId%>&action=Edit&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0">Edit Workflow</a>
                                        <%
                                                  }
                                             }     
                                        }else
                                        {
                                            if(tblWorkFlowLevelConfig.size()>0)
                                            {
                                                if(!"finalise".equalsIgnoreCase(VariOrderStatus))
                                                {
                                %>
                                                    &nbsp;|&nbsp;<a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=tenderId%>&action=Edit&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0">Edit Workflow</a>
                                <%
                                                }                                                
                                                if(!"Yes".equalsIgnoreCase(getFileOnSt.get(0).toString()))
                                                {
                                %>
<!--                                                    &nbsp;|&nbsp;<a href="ViewAllVariOrderForms.jsp?tenderId=<%=tenderId%>&varId=<%=ifVari.get(ii).getVariOrdId()%>">View Variation Order</a>-->
                                                    &nbsp;|&nbsp;<a href="workFlowHistory.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&userid=<%=userId%>&childid=<%=VariOId%>&tenderId=<%=tenderId%>&lotId=<%=lotid%>&fraction=VariforServices&wpId=0">View Workflow History</a>
                                <%
                                                }
                                                else
                                                {
                                %>
                                                    <a href="FileProcessing.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&userid=<%=userId%>&childid=<%=VariOId%>&&tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&fromaction=VariforServices">Process file in Workflow</a>
                                <%
                                                }
                                            }
                                            else
                                            {
                                                if(noOfReviewers==null || noOfReviewers=="" || "".equalsIgnoreCase(noOfReviewers) || "null".equalsIgnoreCase(noOfReviewers))
                                                {
                                                    if(workFlowServiceImpl.getWorkFlowRuleEngineData(10)){
                                %>
                                                        <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=tenderId%>&action=Create&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0">Create Workflow</a>
                                <%
                                                    }else{
                                %>                    
                                                        &nbsp;|&nbsp;<a href="#" onclick="jAlert('Workflow Configuration is pending in BusinessRule','Repeat Order', function(RetVal) {});">Create Workflow</a>
                                <%              }}else{
                                %>                    
                                                    <a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=tenderId%>&action=Edit&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0">Edit Workflow</a>
                                <%              }    

                                            }
                                        }
                                    }
                                    else if("approved".equalsIgnoreCase(WorkFlowStatus))
                                    {
                                        if("finalise".equalsIgnoreCase(VariOrderStatus))
                                        {
                                %>
                                            <a href="ViewAllVariOrderForms.jsp?tenderId=<%=tenderId%>&varId=<%=ifVari.get(ii).getVariOrdId()%>&wpId=0">View Variation Order</a>&nbsp;|&nbsp;
                                            <a href="<%=request.getContextPath()%>/CMSSerCaseServlet?tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0&action=sendtoten&VariOId=<%=VariOId%>" >Send To Consultant</a>
                                <%
                                        }
                                        else
                                        {
                                %>
                                            &nbsp;|&nbsp;<a href="ViewAllVariOrderForms.jsp?tenderId=<%=tenderId%>&varId=<%=ifVari.get(ii).getVariOrdId()%>&wpId=0">View Variation Order</a>
                                            &nbsp;|&nbsp;<a href="workFlowHistory.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&userid=<%=userId%>&childid=<%=VariOId%>&tenderId=<%=tenderId%>&lotId=<%=lotid%>&fraction=VariforServices&wpId=0">View Workflow History</a>
                                            &nbsp;|&nbsp;<a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&action=View&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0">View Workflow</a>
                                <%
                                        }
                                    }else if("rejected".equalsIgnoreCase(WorkFlowStatus)){
                                        
                                        if("0".equalsIgnoreCase(noOfReviewers)){
                                %>
                                            <a href="ViewAllVariOrderForms.jsp?tenderId=<%=tenderId%>&varId=<%=ifVari.get(ii).getVariOrdId()%>&wpId=0">View Variation Order</a>
                                <%            
                                            if(initiator!=approver){
                                %>               
                                                &nbsp;|&nbsp;<a href="workFlowHistory.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&userid=<%=userId%>&childid=<%=VariOId%>&tenderId=<%=tenderId%>&lotId=<%=lotid%>&fraction=VariforServices&wpId=0">View Workflow History</a>
                                <%                
                                            }
                                %>
                                                &nbsp;|&nbsp;<a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&action=View&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0">View Workflow</a>
                                <%              
                                        }else{
                                %>
                                                <a href="ViewAllVariOrderForms.jsp?tenderId=<%=tenderId%>&varId=<%=ifVari.get(ii).getVariOrdId()%>&wpId=0">View Variation Order</a>
                                                &nbsp;|&nbsp;<a href="workFlowHistory.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&userid=<%=userId%>&childid=<%=VariOId%>&tenderId=<%=tenderId%>&lotId=<%=lotid%>&fraction=VariforServices&wpId=0">View Workflow History</a>
                                                &nbsp;|&nbsp;<a href="CreateWorkflow.jsp?activityid=<%=activityid%>&eventid=10&objectid=<%=VariOId%>&action=View&childid=<%=VariOId%>&isFileProcessed=No&tenderId=<%=tenderId%>&lotId=<%=lotid%>&wpId=0">View Workflow</a>
                                <%            
                                            
                                        }
                                    }

                                %>
                        <%}%>

                    </td>
                    </tr>
                <%ii++;ij++;
               }}%></table><%}}else{
                                            allew=false;
                                             
                                            %>

                                      <%}}%>
                                      <%if(!allew){
    //response.sendRedirect("CMSMain.jsp?tenderId="+tenderId+"&msg=cant");
    %>
<div class="responseMsg noticeMsg">Date configuration is pending</div>
                                      <%}%>

                        </form>
                    </div>
                </div></div></div>
        <%@include file="../resources/common/Bottom.jsp" %>
    </div>
    <script>
        function Edit(wpId,tenderId,lotId){

       
            dynamicFromSubmit("ViewDatesForServices.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&flag=cms");
      
        }
        function finalise(varId,tenderId,lotId){
                dynamicFromSubmit("<%=request.getContextPath()%>/CMSSerCaseServlet?varId="+varId+"&tenderId="+tenderId+"&lotId="+lotId+"&action=finalise");
        }
        function view(wpId,tenderId,lotId){
        
            dynamicFromSubmit("ViewDatesForServices.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
       
        }
        function vari(wpId,tenderId,lotId){
            dynamicFromSubmit("VariationOrder.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
        }
    </script>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>

</html>
