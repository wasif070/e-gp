<%--
    Document   : CreateForm
    Created on : 24-Oct-2010, 4:49:09 PM
    Author     : yanki
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.TenderCurrencyService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListBox"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderComboSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderListCellDetail"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="tableMatrix"  class="com.cptu.egp.eps.web.servicebean.TenderTablesSrBean" />
<jsp:useBean id="frmView"  class="com.cptu.egp.eps.web.servicebean.TenderFormSrBean" />
<%@page import="java.util.ListIterator" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderColumns" %>
<%@page import="com.cptu.egp.eps.model.table.TblTenderCells" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Table</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <%
                int tenderId = 0;
                int sectionId = 0;
                int formId = 0;
                int tableId = 0;
                int pkgOrLotId = -1;
                int userId = 0;

                if(session.getAttribute("userId") != null) {
                    if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                    }
                }
                
                if (request.getParameter("tenderId") != null) {
                    tenderId = Integer.parseInt(request.getParameter("tenderId"));
                }
                if (request.getParameter("sectionId") != null) {
                    sectionId = Integer.parseInt(request.getParameter("sectionId"));
                }
                if (request.getParameter("formId") != null) {
                    formId = Integer.parseInt(request.getParameter("formId"));
                }
                if (request.getParameter("tableId") != null) {
                    tableId = Integer.parseInt(request.getParameter("tableId"));
                }
                if(request.getParameter("porlId")!=null){
                    pkgOrLotId = Integer.parseInt(request.getParameter("porlId"));
                }

                String frmName = "";
                StringBuffer frmHeader = new StringBuffer();
                StringBuffer frmFooter = new StringBuffer();
                java.util.List<com.cptu.egp.eps.model.table.TblTenderForms> frm = frmView.getFormDetail(formId);
                if (frm != null) {
                    if (frm.size() > 0) {
                        frmName = frm.get(0).getFormName();
                        frmHeader.append(frm.get(0).getFormHeader());
                        frmFooter.append(frm.get(0).getFormFooter());
                        if(frmName!=null){
                            frmName = frmName.replace("?s", "'s");
                        }
                    }
                    frm = null;
                    }

                java.util.List<com.cptu.egp.eps.model.table.TblTenderTables> tblInfo = tableMatrix.getTenderTablesDetail(tableId);
                short cols = 0;
                short rows = 0;
                String tableName = "";
                String tableHeader = "";
                String tableFooter = "";
                boolean isBOQForm = false;
                //tableMatrix.isPriceBidForm(formId);
                if (tblInfo != null) {
                    if (tblInfo.size() >= 0) {
                        tableName = tblInfo.get(0).getTableName();
                        if(tableName!=null){
                            tableName = tableName.replace("?s", "'s");
                        }
                        tableHeader = tblInfo.get(0).getTableHeader();
                        tableFooter = tblInfo.get(0).getTableFooter();
                        cols = tblInfo.get(0).getNoOfCols();
                        rows = tblInfo.get(0).getNoOfRows();
                    }
                }

                if(!isBOQForm && rows == 0){
                    rows = 1;
                }

                //boolean isInEditMode = tableMatrix.isEntryPresentForCols(tableId);
                
                java.util.ListIterator<TblTenderColumns> tblColumnsDtl = tableMatrix.getColumnsDtls(tableId, true).listIterator();
                java.util.ListIterator<TblTenderCells> tblCellsDtl = tableMatrix.getCellsDtls(tableId).listIterator();

                String colHeader = "";
                byte filledBy = 0;
                byte dataType = 0;
                String colType = "";
                int colId = 0;
                byte showOrHide = 1;
                
                short fillBy[] = new short[cols];
                int arrColId[] = new int[cols];
                String arrColType[] = new String[cols];

                List<Object[]> listCurrencyObj = new ArrayList<Object[]>();
                TenderCurrencyService tenderCurrencyService = (TenderCurrencyService) AppContext.getSpringBean("TenderCurrencyService");
                listCurrencyObj = tenderCurrencyService.getCurrencyTenderwise(tenderId, userId);
                pageContext.setAttribute("tenderId", tenderId);
    %>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Middle Content Table Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">
                    View Table Matrix
                    <%if(pkgOrLotId == -1){%>
                        <span class="c-alignment-right">
                            <a class="action-button-goback" href="TenderTableDashboard.jsp?tenderId=<%= tenderId %>&sectionId=<%= sectionId %>&formId=<%= formId %>" title="Form Dashboard">Form Dashboard</a>
                        </span>
                    <%}else{%>
                        <span class="c-alignment-right">
                            <a class="action-button-goback" href="TenderTableDashboard.jsp?tenderId=<%= tenderId %>&sectionId=<%= sectionId %>&formId=<%= formId %>&porlId=<%= pkgOrLotId %>" title="Form Dashboard">Form Dashboard</a>
                        </span>
                    <%}%>
                </div>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <% if(frmName != null && !"".equals(frmName.trim())){%>
                <table width="100%" class="tableHead_1 t_space">
                    <tr>
                        <td width="100" class="ff">Form Name : </td>
                        <td style="font-weight: normal;"><%= frmName %></td>
                    </tr>
                </table>
                    <%}%>
                    <% if(frmHeader != null && frmHeader.length() > 0 && !"".equals(frmHeader.toString().trim())){%>
                    <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                        <tr>
                            <td width="100" class="ff">Form Header : </td>
                            <td style="font-weight: normal;"><%= frmHeader.toString()%></td>
                        </tr>
                    </table>
                        <%}%>
                <% if(tableName != null && !"".equals(tableName.trim())){%>
                        <table width="100%" class="tableHead_1 t_space">
                    <tr>
                        <td width="100" class="ff">Table Name : </td>
                        <td style="font-weight: normal;"><%= tableName %></td>
                    </tr>
                </table>
                    <%}%>
                <% if(tableHeader != null && !"".equals(tableHeader.trim())){%>
                    <table width="100%" class="tableHead_1 t_space">
                    <tr>
                        <td class="ff">Table Header : </td>
                        <td style="font-weight: normal;"><%= tableHeader %></td>
                    </tr>
                </table>
                    <%}%>
                <table width="100%" cellspacing="0" class="tableList_1" id="FormMatrix">
                    <tbody>
                        <%
                            boolean isTotFormulaCre = false;
                            isTotFormulaCre = tableMatrix.isTotalFormulaCreated(tableId);
                            String strDtType = "";
                            for (short i = -1; i <= rows; i++) {
                                if(i == 0){
                        %>
                         <tr id="ColumnRow">
                        <%
                                    for(short j=0;j<cols;j++){
                                        if(tblColumnsDtl.hasNext()){
                                            TblTenderColumns ttc = tblColumnsDtl.next();
                                            colHeader = ttc.getColumnHeader();
                                            if(colHeader!=null)
                                            {
                                                colHeader = colHeader.replace("?s", "'s");
                                            }
                                            if(colHeader.contains("Unit Cost in Figure (BTN)") )
                                            {
                                                colHeader = colHeader.replace("BTN", "Nu.");
                                            }
                                            colType = ttc.getColumnType();
                                            filledBy = ttc.getFilledBy();
                                            dataType = ttc.getDataType();
                                            colId = ttc.getColumnId();
                                            showOrHide = Byte.parseByte(ttc.getShoworhide());
                                            ttc = null;
                                        }
                                        arrColType[j] = colType;
                                        fillBy[j] = filledBy;
                                        arrColId[j] = colId;

                        %>
                                    <th id="addTD<%= j + 1 %>" colid="<%= j + 1 %>" <%if(false && showOrHide == 0){out.print(" style='display:none' ");}%> >
                                        <%= colHeader %>
                                    </th>
                        <%
                                    }
                        %>
                         </tr>
                        <%
                                }
                                if(i > 0){
                                    if(i == rows && isTotFormulaCre){
                                        java.util.HashMap<Integer, Integer> hmGTCols = tableMatrix.getGTColumns(tableId);
                        %>
                                        <tr id="TR<%=i%>">

                                            <%
                                                for(int j=0; j<cols; j++){
                                            %>
                                            <td id="TD<%= i %>_<%= j + 1 %>" align="center">
                                            <%
                                            if(hmGTCols != null){
                                                if(hmGTCols.containsValue(arrColId[j])){
                                                    out.print("<b>Total Formula</b>");
                                                }else{
                                                    out.print("");
                                                }
                                            }
                                            %>
                                            </td>
                                            <%
                                                }
                                            %>
                                        </tr>
                        <%
                                        if(hmGTCols != null){
                                            hmGTCols = null;
                                        }
                                    }else{
                        %>
                                        <tr id="TR<%=i%>">

                                            <%
                                                int cnt = 0;
                                                short columnId;
                                                int cellId = 0;
                                                List<TblTenderListCellDetail> listCellDetail = new ArrayList<TblTenderListCellDetail>();
                                                
                                                for(int j=0; j<cols; j++){
                                                        String cellValue = "";
                                                        strDtType = "";
                                                        if(tblCellsDtl.hasNext()){
                                                            cnt++;
                                                            TblTenderCells cells = tblCellsDtl.next();
                                            %>
                                                            <td id="TD<%= i %>_<%= j + 1 %>" align="center" <%if(false && cells.getShowOrHide() == 0){out.print(" style='display:none' ");}%> >
                                            <%
                                                            dataType = cells.getCellDatatype();
                                                            filledBy = cells.getCellDatatype();
                                                            cellValue = cells.getCellvalue();
                                                            if(cellValue!=null){
                                                                cellValue = cellValue.replace("?s", "'s");
                                                            }

                                                            columnId =  cells.getColumnId();
                                                            cellId  = cells.getCellId();
                                                            listCellDetail = tableMatrix.getTenderListCellDetail(tableId,columnId,cellId);
                                                            
                                                            if(dataType == 1){strDtType = "Small Text";}
                                                            if(dataType == 2){strDtType = "Long Text";}
                                                            if(dataType == 3){strDtType = "Money Positive";}
                                                            if(dataType == 8){strDtType = "Money All";}
                                                            if(dataType == 6){strDtType = "";}
                                                            if(dataType == 4){strDtType = "Numeric";}
                                                            if(dataType == 9){strDtType = "Combo Box with Calculation";}
                                                            if(dataType == 10){strDtType = "Combo Box w/o Calculation";}
                                                            if(dataType == 12){strDtType = "Date";}
                                                            if(dataType == 13){strDtType = "Money Positive(3 digits after decimal)";}
                                                            
                                                            if(fillBy[j] == 2){
                                                                out.print("Fill By Bidder/Consultant - " + strDtType);
                                                            }else if(fillBy[j] == 1){
                                                                out.print(cellValue);
                                                            }else{
                                                                out.print("Auto - " + strDtType);
                                                            }
                                                            if(fillBy[j]==2 && dataType==3 && "8".equals(arrColType[j]))
                                                            {
                                                                if(listCurrencyObj.size() > 0){
                                                            %>
                                                            <select id="Currency<%=tableId%>_<%=i%>_<%=columnId%>" name="Currency<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1" onchange="changeCurrencyValue('row<%=tableId%>_<%=i%>_<%=columnId%>',this.value,'<%=tableId%>');">
                                                                <% for(Object[] obj :listCurrencyObj){ %>
                                                                <option value="<%=obj[1]%>"><%=obj[0]%></option>
                                                                <% } %>
                                                            </select>
                                                            <%
                                                                }
                                                            }
                                                            
                                                            if(dataType==9 || dataType==10){
                                                                 TenderComboSrBean cmbSrBean = new TenderComboSrBean();
                                                                 if(listCellDetail.size() > 0){
                                                                    TblTenderListBox tblListBoxMaster = listCellDetail.get(0).getTblTenderListBox();
                                                                    List<TblTenderListDetail> listBoxDetail = cmbSrBean.getTenderListBoxDetail(tblListBoxMaster.getTenderListId());
                                                                    %>
                                                                     <select id="idcombodetail" name="namecombodetail<%=tblListBoxMaster.getTenderListId()%>" class="formTxtBox_1">
                                                                        <option value="">--Select--</option>
                                                                        <% for(TblTenderListDetail tblListBoxDetail : listBoxDetail){ %>
                                                                            <option value="<%=tblListBoxDetail.getItemValue()%>"><%=tblListBoxDetail.getItemText()%></option>
                                                                        <% } %>
                                                                    </select>
                                                <%
                                                                 }
                                                            }
                                            %>
                                                            </td>
                                            <%
                                                            cells = null;
                                                        }
                                                }
                                            %>
                                        </tr>
                        <%
                                    }
                                }
                            }
                        %>
                    </tbody>
                </table>
                <% if(tableFooter != null && !"".equals(tableFooter.trim())){%>
                    <table width="100%" class="tableHead_1">
                    <tr>
                        <td width="100" class="ff">Table Footer : </td>
                        <td style="font-weight: normal;"><%= tableFooter %></td>
                    </tr>
                </table>
                    <%}%>
                    <% if(frmFooter != null && frmFooter.length() > 0 && !"".equals(frmFooter.toString().trim())){%>
                    <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                            <tr>
                                <td width="100" class="ff">Form Footer : </td>
                                <td style="font-weight: normal;"><%=frmFooter.toString()%></td>
                            </tr>
                        </table>
                            <%}%>
            </div>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <%
    if (tblCellsDtl != null) {
        tblCellsDtl = null;
    }
    if (tblColumnsDtl != null) {
        tblColumnsDtl = null;
    }
    if (tableMatrix != null) {
        tableMatrix = null;
    }
    if (frmView != null) {
        frmView = null;
    }
    %>
</html>