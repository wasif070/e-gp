<%--
    Document   : EvalTenderer
    Created on : Jan 2, 2011, 2:11:23 PM
    Author     : Administrator,swati,rishita
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalServiceCriteriaService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext,com.cptu.egp.eps.web.servicebean.EvalClariPostBidderSrBean" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService,com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Evaluate Bidder/Consultant</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <script type="text/javascript">
            function validateSubmit(obj)
            {
                $(".reqF_1").remove();
                //alert(obj.elements);
                //alert(document.getElementById("sptechQualFail").innterHTML);
                   //alert(elm);
                   //alert(elm.length);
                     //  alert(elm[i].type);
                     //$('#bankChecker').attr("checked")==true
                     if($('#techQualify_0').attr("checked") ==false && $('#techFail_0').attr("checked")==false){
                        $("#techQualify_0").parent().append("<div class='reqF_1'>Please select atleast one Evaluation Option.</div>"); 
                            return false;
                        }
            }
        </script>

    </head>
    <body>
        <%
        String cuserId = "";
        String userId = "";
        String s_lotId = "0";
        String tenderId = "";
            if (request.getParameter("tenderId") != null) {
                                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                                tenderId = request.getParameter("tenderId");
                                userId = request.getParameter("userId");
                            }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->

            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Evaluate Bidder/Consultant
                    <span style="float: right;"><a href="Evalclarify.jsp?tenderId=<%=tenderId%>&st=rp&comType=TEC" class="action-button-goback">Go Back</a></span>
                </div>
                    <%         CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                    // CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                      AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");
                        
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString()))
                    {
                        //userId = Integer.parseInt(session.getAttribute("userId").toString());
                        cuserId = session.getAttribute("userId").toString();
                    }
                    if (request.getParameter("lotId") != null)
                    {
                        s_lotId = request.getParameter("lotId");
                    }
                    /*boolean isService = false;
                    if (commonService.getProcNature(tenderId).toString().equalsIgnoreCase("Services")) {
                    isService = true;
                    } */
                    EvalClariPostBidderSrBean evalClariPostBidderSrBean = new EvalClariPostBidderSrBean();
                    boolean isCheck = false;
                    EvalServiceCriteriaService criteriaServiceFinalResponse = (EvalServiceCriteriaService) AppContext.getSpringBean("EvalServiceCriteriaService");
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    String Status = criteriaServiceFinalResponse.getBidderExtensionStatus(tenderId, userId).toString();
                    System.out.println("---------------"+Status+"--------------------------------------------");
                    if ("Submit".equals(request.getParameter("btnSubmit")))
                    {
                        String action = "";
                        try
                        {
                            //code added by swati for check Duplicate entry
                            String techQua = "", methodType = "";
                            methodType = request.getParameter("methodType");
                                
                                
                            /* if (methodType.equalsIgnoreCase("OTM")) {
                            if (request.getParameter("techQualify").equalsIgnoreCase("techQualify")) {
                            techQua = "Accepted";
                            }
                            if (request.getParameter("techQualify").equalsIgnoreCase("techFail")) {
                            techQua = "Rejected";
                            }
                            } else if (methodType.equalsIgnoreCase("LTM")) {
                            if (request.getParameter("techQualify").equalsIgnoreCase("techQualify")) {
                            techQua = "Accepted";
                            }
                            if (request.getParameter("techQualify").equalsIgnoreCase("techFail")) {
                            techQua = "Rejected";
                            }
                            } else if (methodType.equalsIgnoreCase("2STM")) {
                            if (request.getParameter("techQualify").equalsIgnoreCase("techQualify")) {
                            techQua = "Responsive";
                            }
                            if (request.getParameter("techQualify").equalsIgnoreCase("techFail")) {
                            techQua = "Unresponsive";
                            }
                            } else if (methodType.equalsIgnoreCase("REOI")) {
                            if (request.getParameter("techQualify").equalsIgnoreCase("techQualify")) {
                            techQua = "Excellent";
                            }
                            if (request.getParameter("techQualify").equalsIgnoreCase("techFail")) {
                            techQua = "Very Good";
                            }
                            if (request.getParameter("techQualify").equalsIgnoreCase("techFail2")) {
                            techQua = "Good & Poor";
                            }
                            } else if (methodType.equalsIgnoreCase("RFP")) {
                            techQua = request.getParameter("marks");
                            } else if (methodType.equalsIgnoreCase("PQ")) {
                            if (request.getParameter("techQualify").equalsIgnoreCase("techQualify")) {
                            techQua = "Qualified";
                            }
                            if (request.getParameter("techQualify").equalsIgnoreCase("techFail")) {
                            techQua = "Conditionally Qualified";
                            }
                            if (request.getParameter("techQualify").equalsIgnoreCase("techFail2")) {
                            techQua = "Disqualified";
                            }
                            } else {
                            if (request.getParameter("techQualify").equalsIgnoreCase("techQualify")) {
                            techQua = "complied";
                            }
                            if (request.getParameter("techQualify").equalsIgnoreCase("techFail")) {
                            techQua = "NonComplied";
                            }
                            } */
                          
                            boolean tenderTpe = false;
                            String postXml = "";
                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            if (request.getParameter("isTenPackageWis") != null)
                            {
                                tenderTpe = Boolean.parseBoolean(request.getParameter("isTenPackageWis"));
                            }
                            if (tenderTpe)
                            {
                                //isCheck = evalClariPostBidderSrBean.isChecked(tenderId, userId, "0");
                                if (request.getParameter("techQualify").equalsIgnoreCase("techQualify"))
                                {
                                    techQua = "Technically Responsive";
                                }
                                if (request.getParameter("techQualify").equalsIgnoreCase("techFail"))
                                {
                                    techQua = "Technically Unresponsive";
                                }
                                action = "Finalize "+ techQua + " by TEC CP";
								//changes make by dohatec to prevent double entry
                               // if (!isCheck)
                               // {
                                    
                                  /*  postXml = "<root><tbl_EvalBidderStatus tenderId=\"" + tenderId
                                            + "\" userId=\"" + userId + "\" bidderStatus=\"" + techQua
                                            + "\" bidderMarks=\"\" passingMarks=\"\""
                                            + " remarks=\"\" result=\"\""
                                            + " evalStatus=\"evaluation\" pkgLotId=\"0"
                                            + "\" evalBy=\"" + cuserId + "\" evalDt=\"" + format.format(new Date()) + "\" /></root>";
                                    */
                                    //CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalBidderStatus", postXml, "").get(0);
                                    CommonMsgChk commonMsgChk =addUpdate.addUpdOpeningEvaluation("FinalizeResponsiveness",tenderId,userId,techQua,"","","","","evaluation","0",cuserId,format.format(new Date()),"","","","","","","","").get(0);
                              //  }
                                /*else
                                {
                                    
                                    postXml = "bidderStatus='" + techQua + "'";
                                    String whereCondi = "tenderId = " + tenderId + " and userId=" + userId;
                                    CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_EvalBidderStatus", postXml, whereCondi).get(0);
                                    if (!commonMsgChk.getFlag())
                                    {
                                        postXml = "<root><tbl_EvalBidderStatus tenderId=\"" + tenderId
                                                + "\" userId=\"" + userId + "\" bidderStatus=\"" + techQua
                                                + "\" bidderMarks=\"\" passingMarks=\"\""
                                                + " remarks=\"\" result=\"\""
                                                + " evalStatus=\"evaluation\" pkgLotId=\"0"
                                                + "\" evalBy=\"" + cuserId + "\" evalDt=\"" + format.format(new Date()) + "\" /></root>";
                                        commonMsgChk =addUpdate.addUpdOpeningEvaluation("FinalizeResponsiveness",tenderId,userId,techQua,"","","","","evaluation","0",cuserId,format.format(new Date()),"","","","","","","","").get(0);
                                        //commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalBidderStatus", postXml, "").get(0);
                                    }
                                        
                                }*/
                            }
                            else
                            {
                                int int_counter = 0;
                                if (request.getParameter("lotCount") != null)
                                {
                                    int_counter = Integer.parseInt(request.getParameter("lotCount"));
                                }
                                isCheck = evalClariPostBidderSrBean.isChecked(tenderId, userId, "0");
                                String eval = "Evaluation";
                                String lotId = null;
                                for (int i = 0; i < int_counter; i++)
                                {
                                    if (request.getParameter("techQualify_" + i).equalsIgnoreCase("techQualify"))
                                    {
                                        techQua = "Technically Responsive";
                                    }
                                    if (request.getParameter("techQualify_" + i).equalsIgnoreCase("techFail"))
                                    {
                                        techQua = "Technically Unresponsive";
                                    }
                                    if (request.getParameter("lotId_" + i) != null)
                                    {
                                        lotId = request.getParameter("lotId_" + i);
                                    }
                                    String whereCondi = "tenderId = " + tenderId + " and userId=" + userId + "and pkgLotId=" + lotId;
                                    postXml = "<root><tbl_EvalBidderStatus tenderId=\"" + tenderId
                                            + "\" userId=\"" + userId + "\" bidderStatus=\"" + techQua
                                            + "\" evalBy=\"" + cuserId + "\" evalDt=\"" + format.format(new Date()) + "\" evalStatus=\"" + eval + "\" pkgLotId=\"" + lotId + "\"/></root>";
                                    CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insdel", "tbl_EvalBidderStatus", postXml, whereCondi).get(0);
                                }
                                    
                            }
                           
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                            action = "Error in " + action + " " + e.getMessage();
                        }
                        finally
                        {
                            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(tenderId), "TenderId", EgpModule.Evaluation.getName(), action, "");
                            
                        }
                         response.sendRedirect("Evalclarify.jsp?tenderId=" + tenderId + "&st=rp");
                    }
                    else
                    {
                             
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <% boolean isTenPackageWis = (Boolean) pageContext.getAttribute("isTenPackageWise");%>
                <div>&nbsp;</div>
                <%if (!"TEC".equalsIgnoreCase(request.getParameter("comType"))) {%>
                <jsp:include page="officerTabPanel.jsp" >
                    <jsp:param name="tab" value="7" />
                </jsp:include>
                <% }%>
                <div class="tabPanelArea_1">
                    <%  
                        if(isTenPackageWis){
                            isCheck = evalClariPostBidderSrBean.isChecked(tenderId, userId,"0");
                        }else{
                            isCheck = evalClariPostBidderSrBean.isChecked(tenderId, userId,s_lotId);
                        }       
                    pageContext.setAttribute("TSCtab", "3");
                    %>
                    <%@include  file="../resources/common/AfterLoginTSC.jsp"%>
                    <% for (SPTenderCommonData companyList : tenderCommonService.returndata("GetCompanynameByUserid", userId, "0")) {
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1">

                        <tr>
                            <td width="22%" class="t-align-left ff">Company Name :</td>
                            <td width="78%" class="t-align-left"><%=companyList.getFieldName1()%></td>
                        </tr>
                    </table>
                    <% }%>

                    <%  if (isTenPackageWis) {
                                    for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                    %>

                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td colspan="2" class="t-align-left ff">Tender Details</td>
                            </tr>

                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="4%" class="t-align-center ff">Sl. No.</th>
                                <th width="23%" class="t-align-center ff">Form Name</th>
                            <%for (SPCommonSearchData memList : commonSearchService.searchData("evalComMemberForReport", tenderId, userId, null, null, null, null, null, null, null)) {
                                %>
                                <th width="10%" class="t-align-center ff"><%=memList.getFieldName1()%></th>
                                <%}%>
                            </tr>
                        <%
                        int pcnt = 1;
                              for (SPCommonSearchData formdetails : commonSearchService.searchData("GetTenderFormsByUserId", tenderId, userId, "0", null, null, null, null, null, null)) {
                                // HIGHLIGHT THE ALTERNATE ROW
                                                                    if (Math.IEEEremainder(pcnt, 2) == 0) {
                            %>
                            <tr class="bgColor-Green">
                            <% } else {%>
                            <tr>
                            <% }%>
                                <td class="t-align-center"><%= pcnt%></td>
                                <td class="t-align-left"><a target="_blank" href="IndReport.jsp?uuId=<%=userId%>&tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&evalTenderer=true"><%=formdetails.getFieldName2()%></a></td>
                                <%
                                                              for (SPCommonSearchData memList1 : commonSearchService.searchData("evalComMemberForReport", tenderId, userId, null, null, null, null, null, null, null)) {

                                                                  List<SPCommonSearchData> evalList = commonSearchService.searchData("EvalMemberStatus", tenderId, userId, formdetails.getFieldName1(), memList1.getFieldName2(), null, null, null, null, null);
                            
                            //if(!cuserId.equalsIgnoreCase(userIdMatch)){
                            %>

                                <td width="22%" class="t-align-center">

                                <% if (!evalList.isEmpty()) { %>
                                    <%// if (evalList.getFieldName2().equalsIgnoreCase("Non Complied")) {%>
                                    <a href="#" onClick="window.open('EvalViewRemark.jsp?remark=<%=evalList.get(0).getFieldName3()%>','window','width=500,height=200,scrollbars=yes')">  <%=evalList.get(0).getFieldName2()%> </a>
                                    <%//} else {%>
                                    <%//=evalList.getFieldName2()%>
                                    <%//}
%>
                                    <br/>


                                <%
                                    int intShowSeekClariLink = 0;
                                    for (SPCommonSearchData listData : commonSearchService.searchData("getSeekClariLinkText", evalList.get(0).getFieldName5(), evalList.get(0).getFieldName1(), cuserId, userId, null, null, null, null, null)) {
                                        intShowSeekClariLink++;
                                        if (listData.getFieldName1() == null) {
                                    %>
                                <a href="#" onClick="window.open('EvalViewSeekClari.jsp?evalMemStatusId=<%=evalList.get(0).getFieldName4()%>&userId=<%=userId%>&tenderFormId=<%=evalList.get(0).getFieldName5()%>&memAnswerBy=<%=evalList.get(0).getFieldName1()%>','window','width=700,height=500,scrollbars=yes')">Seek Clarification from Committee Member</a>
                                <% } else if (listData.getFieldName1() != null) {
                                                                            if ("".equalsIgnoreCase(listData.getFieldName2())) {
                                %>
                                <a href="#" onClick="window.open('EvalViewSeekClari.jsp?evalMemStatusId=<%=evalList.get(0).getFieldName4()%>&userId=<%=userId%>&tenderFormId=<%=evalList.get(0).getFieldName5()%>&memAnswerBy=<%=evalList.get(0).getFieldName1()%>','window','width=700,height=500,scrollbars=yes')">Justification asked</a>

                                <%  } else if (!"".equalsIgnoreCase(listData.getFieldName2())) {%>

                                <a href="#" onClick="window.open('EvalViewSeekClari.jsp?evalMemStatusId=<%=evalList.get(0).getFieldName4()%>&userId=<%=userId%>&tenderFormId=<%=evalList.get(0).getFieldName5()%>&memAnswerBy=<%=evalList.get(0).getFieldName1()%>','window','width=700,height=500,scrollbars=yes')">Clarification received</a>

                                    <%    }
                                         }
                                        }

                                        // end for listData
%>

                                <%
                                if (intShowSeekClariLink == 0 && !memList1.getFieldName2().equalsIgnoreCase(cuserId)) {%>
                                <a href="#" onClick="window.open('EvalViewSeekClari.jsp?evalMemStatusId=<%=evalList.get(0).getFieldName4()%>&userId=<%=userId%>&tenderFormId=<%=evalList.get(0).getFieldName5()%>&memAnswerBy=<%=evalList.get(0).getFieldName1()%>','window','width=700,height=500,scrollbars=yes')">Seek Clarification from Committee Member</a>
                                <%}%>

                                    <%} else {%>
                                    -
                                <% } %>
                                </td>
                            <% } //} %>

                            </tr>
                        <% pcnt++;
                                                                    }%>
                        </table>

                        <% }
                          } else {

                              for (SPCommonSearchData packageList : commonSearchService.searchData("getlotorpackagebytenderid", tenderId, "Package", "1", null, null, null, null, null, null)) {
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <td colspan="2" class="t-align-left ff">Tender Details</td>
                            </tr>
                            <tr>
                                <td width="22%" class="t-align-left ff">Package No. :</td>
                                <td width="78%" class="t-align-left"><%=packageList.getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Package Description :</td>
                                <td class="t-align-left"><%=packageList.getFieldName2()%></td>
                            </tr>
                        </table>
                            <form id="frmEvalTender" name="frmEvalTender" method="POST" action="EvalTenderer.jsp?tenderId=<%=tenderId%>&userId=<%=userId%>">
                        <%  }
                              int lcnt = 1;
                              int lotcnt =  0;
                              CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService"); 
                              //for (SPCommonSearchData lotList : commonSearchService.searchData("GetBidderLots", tenderId, "Lot", userId, null, null, null, null, null, null)) {
                              List<Object[]> lotList = commonService.getLotDetailsByPkgLotId(s_lotId,tenderId);
                        %>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">

                            <tr>
                                <td width="22%" class="t-align-left ff">Lot No. :</td>
                                <td width="78%" class="t-align-left"><%=lotList.get(0)[0] %>
                                    <input type="hidden" value="<%=lotList.get(0)[2] %>" name="lotId_<%=lotcnt%>">
                                </td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Lot Description :</td>
                                <td class="t-align-left"><%=lotList.get(0)[1] %></td>
                            </tr>
                        </table><table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="1%" class="t-align-center ff">Sl. No.</th>
                                <th width="22%" class="t-align-center ff">Form Name</th>
                                <%
                                // for (SPCommonSearchData memList : commonSearchService.searchData("evalComMember", tenderId, userId, cuserId, null, null, null, null, null, null)) {
                                for (SPCommonSearchData memList : commonSearchService.searchData("evalComMemberForFinalEvaluation", tenderId, userId, cuserId, null, null, null, null, null, null)) {
                                %>
                                <th width="22%" class="t-align-center ff"><%=memList.getFieldName1()%></th>
                                <%}%>


                            </tr>
                            <%
                                                              for (SPCommonSearchData formdetails : commonSearchService.searchData("GetTenderFormsByLotIdUid", tenderId, "1", s_lotId, userId, null, null, null, null, null)) {
                                        // HIGHLIGHT THE ALTERNATE ROW
                                                                  if (Math.IEEEremainder(lcnt, 2) == 0) {
                            %>
                            <tr class="bgColor-Green">
                                <% } else {%>
                            <tr>
                                <% }%>
                                <td class="t-align-center"><%=lcnt%></td>
                                <td><a target="_blank" href="IndReport.jsp?uuId=<%=userId%>&tenderId=<%=tenderId%>&formId=<%=formdetails.getFieldName1()%>&evalTenderer=true"><%=formdetails.getFieldName2()%></a></td>
                                <%
                                //for (SPCommonSearchData memList : commonSearchService.searchData("evalComMember", tenderId, userId, null, null, null, null, null, null, null)) {
                                for (SPCommonSearchData memList : commonSearchService.searchData("evalComMemberForFinalEvaluation", tenderId, userId, cuserId, null, null, null, null, null, null)) {


                                                                       List<SPCommonSearchData> evalList1 = commonSearchService.searchData("EvalMemberStatus", tenderId, userId, formdetails.getFieldName1(), memList.getFieldName2(), null, null, null, null, null);%>
                                <td width="22%" class="t-align-center">

                                    <%if (!evalList1.isEmpty()) {%>
                                    <%// if (evalList.getFieldName2().equalsIgnoreCase("Non Complied")) {%>
                                    <a href="#" onClick="window.open('EvalViewRemark.jsp?remark=<%=evalList1.get(0).getFieldName3()%>','window','width=500,height=200,scrollbars=yes')">  <%=evalList1.get(0).getFieldName2()%> </a>
                                    <%//} else {%>
                                    <%//=evalList.getFieldName2()%>
                                    <%//}
%>
                                    <br/>
                                    <!--a href="#" onClick="window.open('EvalViewSeekClari.jsp?evalMemStatusId=<%=evalList1.get(0).getFieldName4()%>&userId=<%=userId%>&tenderFormId=<%=evalList1.get(0).getFieldName5()%>&memAnswerBy=<%=evalList1.get(0).getFieldName1()%>','window','width=700,height=500,scrollbars=yes')"> Seek Clarification</a-->

                                    <% 
                                    int i_MemQueCnt=0;
                                    for (SPCommonSearchData listData : commonSearchService.searchData("getSeekClariLinkText", evalList1.get(0).getFieldName5(), evalList1.get(0).getFieldName1(), cuserId, userId, null, null, null, null, null)) {
                                        i_MemQueCnt++;
                                            if (listData==null || listData.getFieldName1() == null || "" == listData.getFieldName1()) {%>
                                    <a href="#" onClick="window.open('EvalViewSeekClari.jsp?evalMemStatusId=<%=evalList1.get(0).getFieldName4()%>&userId=<%=userId%>&tenderFormId=<%=evalList1.get(0).getFieldName5()%>&memAnswerBy=<%=evalList1.get(0).getFieldName1()%>','window','width=700,height=500,scrollbars=yes')"> Seek Clarification from Committee Member</a>
                                    <% } else if (listData.getFieldName1() != null || "" != listData.getFieldName1()) {
                                                                                if (listData.getFieldName2() == null || "" == listData.getFieldName2()) {
                                    %>
                                    <a href="#" onClick="window.open('EvalViewSeekClari.jsp?evalMemStatusId=<%=evalList1.get(0).getFieldName4()%>&userId=<%=userId%>&tenderFormId=<%=evalList1.get(0).getFieldName5()%>&memAnswerBy=<%=evalList1.get(0).getFieldName1()%>','window','width=700,height=500,scrollbars=yes')"> Justification asked</a>

                                    <%  } else if (listData.getFieldName2() != null || "" != listData.getFieldName2()) {%>

                                    <a href="#" onClick="window.open('EvalViewSeekClari.jsp?evalMemStatusId=<%=evalList1.get(0).getFieldName4()%>&userId=<%=userId%>&tenderFormId=<%=evalList1.get(0).getFieldName5()%>&memAnswerBy=<%=evalList1.get(0).getFieldName1()%>','window','width=700,height=500,scrollbars=yes')"> Clarification received</a>

                                    <%    }
                                         }
                                     }// end for listData 
                                    if(i_MemQueCnt==0 && !memList.getFieldName2().equalsIgnoreCase(cuserId)){%>
                                    <a href="#" onClick="window.open('EvalViewSeekClari.jsp?evalMemStatusId=<%=evalList1.get(0).getFieldName4()%>&userId=<%=userId%>&tenderFormId=<%=evalList1.get(0).getFieldName5()%>&memAnswerBy=<%=evalList1.get(0).getFieldName1()%>','window','width=700,height=500,scrollbars=yes')"> Seek Clarification from Committee Member</a>

<%}
%>

                                    <%} else {%>
                                    -
                                    <%}%>
                                </td>
                                <%}%>
                            </tr>
                            <% lcnt++;
                                                              }%>
                        </table>
                        <%--Add this table for evalution for diffrent lot wise.... --%>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <%--<%List<SPCommonSearchData> evalPQ = commonSearchService.searchData("EvalchkPostQuali", tenderId, null, null, null, null, null, null, null, null);
                                                              if (!evalPQ.isEmpty()) {%>
<!--                            <tr>
                                <td width="14%" class="t-align-left ff">Post Qualification :</td>
                                <td width="86%" class="t-align-left"><a href="ViewPostQualificationDtl.jsp?uId=< %=userId%>&tenderId=< %=tenderId%>" >View</a></td>
                            </tr>-->
                            <%}%>--%>
                            <tr>
                                <td width="14%" class="t-align-left ff">Evaluation:</td>
                                <td width="86%" class="t-align-left">
                                    <%
                                                String methodType = "abc";
                                                String bidderStatus = "abc";
                                               for (SPCommonSearchData bidStatus : commonSearchService.searchData("getBidderStatus", tenderId, userId, s_lotId, null, null, null, null, null, null)) {
                                                   /*Condition when updating lotid is same then status is assigned */
                                                                            bidderStatus = "";
                                                                              bidderStatus = bidStatus.getFieldName1();
                                                  }
                                                                      if (!isCheck) {/*If not update*/
                                    %>
                                    <%if(Status.length()==2){%><label><input name="techQualify_<%=lotcnt%>" type="radio" value="techQualify" id="techQualify_<%=lotcnt%>"/> Technically Responsive  &nbsp;&nbsp;</label><%}else{out.print(Status);}%>
                                    <label><input name="techQualify_<%=lotcnt%>" type="radio" value="techFail" id="techFail_<%=lotcnt%>"/> Technically Non-responsive</label>
                                    <% } else {

                                                                            if (bidderStatus.equalsIgnoreCase("Technically Responsive")) {
                                    %>
                                    <%if(Status.length()==2){%><label><input name="techQualify_<%=lotcnt%>" type="radio" value="techQualify" id="techQualify_<%=lotcnt%>" checked/> Technically Responsive  &nbsp;&nbsp;</label><%}else{out.print(Status);}%>
                                    <label><input name="techQualify_<%=lotcnt%>" type="radio" value="techFail" id="techFail_<%=lotcnt%>"/> Technically Non-responsive</label>
                                    <% } else if (bidderStatus.equalsIgnoreCase("Technically Unresponsive")||bidderStatus.equalsIgnoreCase("Technically Non-responsive")) {%>
                                    <%if(Status.length()==2){%><label><input name="techQualify_<%=lotcnt%>" type="radio" value="techQualify" id="techQualify_<%=lotcnt%>"/> Technically Responsive  &nbsp;&nbsp;</label><%}else{out.print(Status);}%>
                                    <label><input name="techQualify_<%=lotcnt%>" type="radio" value="techFail" id="techFail_<%=lotcnt%>" checked/> Technically Non-responsive</label>
                                    <%       }else{%>
                                    <%if(Status.length()==2){%><label><input name="techQualify_<%=lotcnt%>" type="radio" value="techQualify" id="techQualify_<%=lotcnt%>"/> Technically Responsive  &nbsp;&nbsp;</label><%}else{out.print(Status);}%>
                                    <label><input name="techQualify_<%=lotcnt%>" type="radio" value="techFail" id="techFail_<%=lotcnt%>"/> Technically Non-responsive</label>
                                    <%}}%>
                                    <span  name="spantechQualFail" id="sptechQualFail" style="color: Red;"  width="100%"></span>
                                </td>

                            </tr>

                        </table>
                        <%     lotcnt++;
                            //  }/*Lot list loop ends here*/%>
                        <input type="hidden" value="<%=lotcnt%>" name="lotCount" id="lotCount" />
                                  <%  }/*Else Condition ends here if tender is lotwise*/%>
<!--                    </form>-->
                    <%if (isTenPackageWis) {%>
                        <form id="frmEvalTender" name="frmEvalTender" method="POST" action="EvalTenderer.jsp?tenderId=<%=tenderId%>&userId=<%=userId%>">
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <%List<SPCommonSearchData> evalPQ = commonSearchService.searchData("EvalchkPostQuali", tenderId, null, null, null, null, null, null, null, null);
                                if (!evalPQ.isEmpty()) {%>
<!--                            <tr>
                                <td width="14%" class="t-align-left ff">Post Qualification :</td>
                                <td width="86%" class="t-align-left"><a href="ViewPostQualificationDtl.jsp?uId=< %=userId%>&tenderId=< %=tenderId%>" >View</a></td>
                            </tr>-->
                            <%}%>
                            <tr>
                                <td width="14%" class="t-align-left ff">Evaluation:</td>
                                <td width="86%" class="t-align-left">
                                    <%
                                        String bidderStatus = "";
                                               for (SPCommonSearchData bidStatus : commonSearchService.searchData("getBidderStatus", tenderId, userId, "0", null, null, null, null, null, null)) {
                                            bidderStatus = bidStatus.getFieldName1();
                                                  }
                                    %>
                                    <% if (!isCheck) {%>
                                    <%if(Status.length()==2){%><label><input name="techQualify" type="radio" value="techQualify" id="techQualify_0"/> Technically Responsive  &nbsp;&nbsp;</label><%}else{out.print(Status);}%>
                                    <label><input name="techQualify" type="radio" value="techFail" id="techFail_0"/> Technically Non-responsive</label>
                                    <% } else {

                                                                            if (bidderStatus.equalsIgnoreCase("Technically Responsive")) {
                                    %>
                                    <%if(Status.length()==2){%><label><input name="techQualify" type="radio" value="techQualify" id="techQualify_0" checked/> Technically Responsive  &nbsp;&nbsp;</label><%}else{out.print(Status);}%>
                                    <label><input name="techQualify" type="radio" value="techFail" id="techFail_0"/> Technically Non-responsive</label>
                                    <% } else if (bidderStatus.equalsIgnoreCase("Technically Unresponsive")||bidderStatus.equalsIgnoreCase("Technically Non-responsive")) {%>
                                    <%if(Status.length()==2){%><label><input name="techQualify" type="radio" value="techQualify" id="techQualify_0"/> Technically Responsive  &nbsp;&nbsp;</label><%}else{out.print(Status);}%>
                                    <label><input name="techQualify" type="radio" value="techFail" id="techFail_0" checked/> Technically Non-responsive</label>
                                    <%       }else{%>
                                    <%if(Status.length()==2){%><label><input name="techQualify" type="radio" value="techQualify" id="techQualify_0"/> Technically Responsive  &nbsp;&nbsp;</label><%}else{out.print(Status);}%>
                                    <label><input name="techQualify" type="radio" value="techFail" id="techFail_0"/> Technically Non-responsive</label>
                                    <%}}%>
                                    <span  name="spantechQualFail" id="sptechQualFail" style="color: Red;"  width="100%"></span>
                                </td>

                            </tr>

                        </table>
                        <%}%>

                        <div class="t-align-center">
                            <label class="formBtn_1 t_space">
                                <input name="btnSubmit" type="submit" value="Submit" onclick="javascript: return validateSubmit(this.form)"/>
                            </label>

                        </div>
                        <input type="hidden" value="<%=isTenPackageWis%>" name="isTenPackageWis" id="isTenPackageWis"/>
                    </form>
                </div>
                <div>&nbsp;</div>
                
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <%}%>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>

