<%-- 
    Document   : MapEvalTenderLots
    Created on : Jan 17, 2011, 6:30:21 PM
    Author     : Karan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Lot Wise Mapping</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
         <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
         <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    </head>
    <body>

          <%
  String userId="";
  //String currUserTypeId="";
  String actionType="";
  String strCcomType="";
  String tenderId="";  

   HttpSession hs = request.getSession();
     if (hs.getAttribute("userId") != null) {
             userId = hs.getAttribute("userId").toString();
      }

   if(request.getParameter("tenderId")!=null){
            tenderId=request.getParameter("tenderId");
        }

   if(request.getParameter("comType")!=null){
            strCcomType=request.getParameter("comType");
        }

    if ( request.getParameter("action") != null) {
            if ( !"".equalsIgnoreCase(request.getParameter("action")) && !"null".equalsIgnoreCase(request.getParameter("action"))){
                   actionType = request.getParameter("action");
               }
        }

    // Variable tenderId is defined by u on ur current page.
    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
  %>

       <div class="dashboard_div">
  <!--Dashboard Header Start-->
  <%@include  file="../resources/common/AfterLoginTop.jsp" %>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
  <div class="pageHead_1">
      Share forms for TSC
      <span style="float: right;" ><a href="EvalComm.jsp?tenderid=<%=tenderId%>" class="action-button-goback">Go Back</a></span>
  </div>

  <%@include file="../resources/common/TenderInfoBar.jsp" %>
  
  <div>&nbsp;</div> 

  <div class="tabPanelArea_1">

      <%-- Start: CODE TO DISPLAY MESSAGES --%>
       <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("mapped")){
                            msgTxt="Forms mapped successfully";
                        } else  if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError){%>
                   <div class="responseMsg errorMsg" style="margin-bottom: 12px;" ><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                   <%}%>
                <%}
                 msgId = null;
                 msgTxt = null;
        }%>

     <%-- End: CODE TO DISPLAY MESSAGES --%>


    <table width="100%" cellspacing="0" class="tableList_1">
        <tr>
            <th width="7%" class="t-align-center">Lot No.</th>
            <th width="67%" class="t-align-center">Lot Description</th>
            <th class="t-align-center" width="10%">Status</th>
            <th class="t-align-center" width="10%">Action</th>
        </tr>

    <%  int j = 0;
       tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

     for (SPTenderCommonData sptcd : tenderCommonService.returndata("geTenderLotsForEvaluationMapping", tenderId, null)){
         j++;  %>
         <tr
              <%if(Math.IEEEremainder(j,2)==0) {%>
                                            class="bgColor-Green"
                                        <%} else {%>
                                            class="bgColor-white"
                                        <%}%>
             >
             <td class="t-align-center"><%=sptcd.getFieldName1()%></td>
             <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
             <td class="t-align-center"><%=sptcd.getFieldName4()%></td>
             <td class="t-align-center">
                 <a href="../officer/MapEvalForms.jsp?action=<%=request.getParameter("action")%>&tenderid=<%=tenderId%>&lotId=<%=sptcd.getFieldName3()%>&comType=<%=request.getParameter("comType")%>">Map</a>
             </td>
         </tr>
   <%}%>
</table>
  </div>
  </div>

<%
// Set To null
  userId=null;
  //currUserTypeId=null;
  actionType=null;
  strCcomType=null;
  tenderId=null;
%>

    <div>&nbsp;</div>
    </div>
    <!--Dashboard Content Part End-->
    <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->

    </body>
</html>
