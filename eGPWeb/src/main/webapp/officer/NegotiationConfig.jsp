<%--
    Document   : NegotiationConfig
    Created on : Dec 23, 2010, 3:22:15 PM
    Author     : parag
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<jsp:useBean id="negotiationdtbean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Configure Negotiation</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>


        <script type="text/javascript" >
            $(document).ready(function() {
                $("#idfrmnegotiation").validate({
                    rules: {
                        dtStartDate:{required:true,CompareToForToday:"#tst"},
                        dtEndDate:{required:true,CompareToForGreater:"#dtStartDate"},
                        txtDetails:{required:true,maxlength:2000}
                    },
                    messages: {                        
                        dtStartDate:{required:"<div class='reqF_1'>Please select negotiation start date.</div>",
                            CompareToForToday: "<div class='reqF_1'>Negotiation start date must be greater than current date.</div>"},

                        dtEndDate:{required: "<div class='reqF_1'>Please select negotiation end date.</div>",
                            CompareToForGreater: "<div class='reqF_1'>Negotiation end date must be greater than the negotiation start date.</div>"
                        },
                        txtDetails:{required:"<div class='reqF_1'>Please enter comments.</div>",
                            maxlength:"<div class='reqf_1'>Maximum 2000 characters are allowed.</div>"
                        }
                    },

                    errorPlacement: function(error, element)
                    {
                        if(element.attr("name")=="dtStartDate")
                        {
                            error.insertAfter("#dtStartDate1")
                        }
                        else if(element.attr("name")=="dtEndDate")
                        {
                            error.insertAfter("#dtEndDate1")
                        }
                        else
                            error.insertAfter(element);
                    }

                });
            });
        </script>

        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp"%>
                <%
                            int userId = 0;
                            byte suserTypeId1 = 0;
                            int tenderId = 1;

                            if (session.getAttribute("userTypeId") != null) {
                                suserTypeId1 = Byte.parseByte(session.getAttribute("userTypeId").toString());
                            }
                            if (session.getAttribute("userId") != null) {
                                userId = Integer.parseInt(session.getAttribute("userId").toString());
                            }
                            if (request.getParameter("tenderId") != null) {
                                tenderId = Integer.parseInt(request.getParameter("tenderId"));
                            }
                %>
                <div class="dashboard_div">
                    <!--Dashboard Header Start-->

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1">
                            Configure Negotiation
                            <span style="float:right;">
                                <a href="<%=request.getContextPath()%>/officer/NegotiationProcess.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back to Dashboard</a>
                            </span>
                        </div>
                        <%
                                    // Variable tenderId is defined by u on ur current page.
                                    pageContext.setAttribute("tenderId", tenderId);
                        %>

                        <%@include file="../resources/common/TenderInfoBar.jsp" %>
                        <%
                            String tenderRefNo = pageContext.getAttribute("tenderRefNo").toString();
                        %>
                         <div>&nbsp;</div>
                         
                        <% pageContext.setAttribute("tab", "7");%>
                        <%@include  file="/officer/officerTabPanel.jsp"%>
                        
                        <div class="tabPanelArea_1">
                         <%-- Start: Common Evaluation Table --%>
                      <%@include file="/officer/EvalCommCommon.jsp" %>
                      <div>&nbsp;</div>
                    <%-- End: Common Evaluation Table --%>
                    
                        <%  pageContext.setAttribute("TSCtab", "7");%>
                        <%@include file="../resources/common/AfterLoginTSC.jsp" %>

                        <div>&nbsp;</div>
                        <div class="tabPanelArea_1">
                        <%
                                   // List<SPTenderCommonData> companyList = negotiationdtbean.getNegotiationCompanyName(tenderId);
                                  //  EvaluationService evalService = (EvaluationService) AppContext.getSpringBean("EvaluationService");
                                  //  evalCount  = evalService.getEvaluationNo(Integer.parseInt(tenderid));
                        %>
                        
                            <form name="frmnegotiation" id="idfrmnegotiation" method="post" action="<%=request.getContextPath()%>/NegotiationSrBean">
                                <input type="hidden" name="action" id="idaction" value="create" />
                                <input type="hidden" name="tenderIdName" id="idtenderIdName" value="<%=tenderId%>" />
                                <input type="hidden" name="tenRefNo" value="<%=tenderRefNo%>" />
                                 <input type="hidden" name="evalCount" value="<%=evalCount%>" />
                                <div style="font-style: italic" class="ff" align="left">
                                    Fields marked with (<span class="mandatory">*</span>) are mandatory.
                                </div>
                                <table width="100%" cellspacing="0" class="tableList_1">

                                    <tr>
                                        <td class="t-align-left ff" width="25%">Bidder / Consultant Name : <span class="mandatory">*</span></td>
                                        <td class="t-align-left">
                                            <%
                                                CommonSearchDataMoreService dataMoreNeg = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                                                List<SPCommonSearchDataMore> negUserId = dataMoreNeg.geteGPDataMore("getNegotiatedBidders",String.valueOf(tenderId));
                                                if(negUserId!=null && (!negUserId.isEmpty())){
                                                     if(negUserId.size()==1){
                                            %>
                                                <%=negUserId.get(0).getFieldName3()%>
                                                <input type="hidden" value="<%=negUserId.get(0).getFieldName2()%>_<%=negUserId.get(0).getFieldName1()%>" name="roundId_cmbNegCompName"/>                                                
                                             <%}else{%>
                                             <select name="roundId_cmbNegCompName" class="formTxtBox_1" style="width: 200px;">
                                                <%for(SPCommonSearchDataMore negData : negUserId){%>
                                                    <option value="<%=negData.getFieldName2()%>_<%=negData.getFieldName1()%>"><%=negData.getFieldName3()%></option>
                                                <%}%>
                                              </select>
                                             <%}%>
                                             <input type="hidden" value="Online" name="negModeName"/>
                                             <%}%>
                                            <span id="SPErr" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <!--<tr>
                                        <td class="t-align-left ff">Mode of Negotiation : <span class="mandatory">*</span></td>
                                        <td class="t-align-left">
                                            <label><input type="radio" name="negModeName" id="idnegModeName" value="Online" /> Online&nbsp;</label>
                                            <label><input type="radio" name="negModeName" id="idnegModeName" value="Offline" checked="checked" /> Offline</label>
                                        </td>
                                    </tr>-->
                                    <tr>
                                        <td class="t-align-left ff">Negotiation Start Date and Time : <span class="mandatory">*</span></td>
                                        <td class="t-align-left">
                                            <input name="dtStartDate" type="text" class="formTxtBox_1" id="dtStartDate" onfocus="GetCal('dtStartDate','dtStartDate');" />
                                            <img id="dtStartDate1" name="dtStartDate1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('dtStartDate','dtStartDate1');"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Negotiation End Date and Time : <span class="mandatory">*</span></td>
                                        <td class="t-align-left">
                                            <input name="dtEndDate" type="text" class="formTxtBox_1" id="dtEndDate" readonly="true"  onfocus="GetCal('dtEndDate','dtEndDate');"/>
                                            <img id="dtEndDate1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('dtEndDate','dtEndDate1');"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="16%" class="t-align-left ff">Invitation details : <span class="mandatory">*</span></td>
                                        <td width="84%" class="t-align-left">
                                            <label>
                                                <textarea name="txtDetails" rows="3" class="formTxtBox_1" id="txtReply" style="width:675px;"></textarea>
                                                <span id="SPReply" class="reqF_1"></span>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="t-align-center t_space">
                                                <label class="formBtn_1">
                                                    <input name="btnSubmit" value="Submit" type="submit"  />
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                       
                    </div>
                </div>
                                            
                                             <%@include file="../resources/common/Bottom.jsp" %>
            </div>
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

