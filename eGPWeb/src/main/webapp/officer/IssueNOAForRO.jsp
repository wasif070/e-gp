<%-- 
    Document   : IssueNOAForRO
    Created on : Nov 16, 2011, 3:57:54 PM
    Author     : shreyansh Jogi
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.RepeatOrderService"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

                    String tenderId = "";
                    if (request.getParameter("tenderId") != null) {
                        tenderId = request.getParameter("tenderId");
                    }

                    String pckLotId = "";
                    if (request.getParameter("pckLotId") != null) {
                        pckLotId = request.getParameter("pckLotId");
                    }
                    
                    String NOADA = "";
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    String procurementNature = commonService.getProcNature(tenderId).toString();
                    if(procurementNature.equalsIgnoreCase("Services")){
                        NOADA = "Draft Agreement";
                    }else{
                        NOADA = "NOA";
                    }

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            <%if(procurementNature.equalsIgnoreCase("Services")){%>
            Draft Agreement
            <%}else{%>
            Issue Letter of Acceptance
            <% } %>
        </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <jsp:useBean id="issueNOASrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.IssueNOASrBean"/>
        <jsp:useBean id="issueNOADtBean" scope="request" class="com.cptu.egp.eps.web.databean.IssueNOADtBean"/>
        <jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
        <jsp:setProperty name="issueNOADtBean" property="*"/>

         <%

                    CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    String userId = "";
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = session.getAttribute("userId").toString();
                        issueNOASrBean.setLogUserId(userId);
                        commonSearchService.setLogUserId(userId);
                        tenderCommonService.setLogUserId(userId);
                    }
        
        
        if ("Submit".equals(request.getParameter("hdnbutton"))) {
        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
            int conId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
            makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), conId, "contractId", EgpModule.Repeat_Order.getName(), "Issuance of NOA For Repeat Order", "");
                    if(issueNOASrBean.addIssueNOA(issueNOADtBean, tenderId,true,userId)) {
                        response.sendRedirect("repeatOrderMain.jsp?action=success&tenderId=" + tenderId);
                    }

                }else{


%>
        <script type="text/javascript">
            function regSpace(value){
                return /^\s+|\s+$/g.test(value);
            }
            function alphaNumric(value){
                return /^\w+$/.test(value);
            }
            var bVal = true;
            $(function() {
                $('#txtContractNo').blur(function() {
                    $('.err1').remove();
                    if($('#txtContractNo') != null){
                        var flag = document.getElementById('txtContractNo').value;
                        if($('#txtContractNo').val() == ''){
                            $('#Msg').html('');
                        }else if(regSpace(flag)){
                            $('#Msg').html('');
                        }else if(flag.length > 100){
                            $('#Msg').html('');
                        }else if(!alphaNumric($('#txtContractNo').val())){
                            $('#Msg').html('');
                            $('#txtContractNo').parent().append("<div class = 'err1 reqF_1' >Please enter valid Contract No</div>")
                            return false;
                        }else{
                            $('#Msg').css("color","red");
                            $('#Msg').html("Checking for unique Contract No....");
                            $.post("<%=request.getContextPath()%>/CommonServlet", {contractNo:$.trim($('#txtContractNo').val()),funName:'verifyContractNo'},
                            function(j)
                            {
                                if(j.toString().indexOf("OK", 0)!=-1){
                                    $('#Msg').css("color","green");
                                    bVal = true;
                                }
                                else if(j.toString().indexOf("Contract No.", 0)!=-1){
                                    $('#Msg').css("color","red");
                                    bVal = false;
                                }
                                $('#Msg').html(j);
                            });
                        }
                    }else{
                        $('#Msg').html("");
                    }
                });
            });
        </script>
       
        <script type="text/javascript">

            $(document).ready(function(){
                $("#frmIssueNOA").validate({

                    rules:{
                        contractNo:{requiredWithoutSpace:true,maxlength:100,spacevalidate:true}/*,
                        contractAmtString:{required:true,decimal:true},
                        perfSecAmtString:{required:true,decimal:true},
                        contractName:{required:true,maxlength:100}*/

                    },
                    messages:{
                        contractNo:{requiredWithoutSpace:"<div class='reqF_1'>Please enter Contract No.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 100 characters are allowed</div>",
                            spacevalidate:"<div class='reqF_1'>Only space Is not allowed</div>"
                        }/*,
                        contractName:{
                            required:"<div class='reqF_1'>Please enter Contract/Project Name</div>",
                            maxlength:"<div class='reqF_1'>Maximum 100 characters are allowed</div>"
                        },
                        contractAmtString:{
                            required:"<div class='reqF_1'>Please enter contract price in Figure (In Tk.)</div>",
                            decimal:"<div class='reqF_1'>Allows numbers and 2 digits after decimal.</div>"
                        },
                        perfSecAmtString:{
                            required:"<div class='reqF_1'>Please enter Performance Security Amount in Figure (In BTN)</div>",
                            decimal:"<div class='reqF_1'>Allows numbers and 2 digits after decimal.</div>"
                        }*/
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#frmIssueNOA').submit(function() {

                });
            });
            $(function() {
                $('#frmIssueNOA').submit(function() {
                    if($('#frmIssueNOA').valid()){
                        if(!bVal){
                           return false;
                        }else{
                            if($('#Submit')!=null){
                                $('#Submit').attr("disabled","true");
                                $('#hdnbutton').val("Submit");
                            }
                        }
                    }
                });
            });
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            
            <div class="contentArea_1">
               

                <form id="frmIssueNOA" action="" method="POST">
                    <input type="hidden" name="RO" id="RO" value="RO" />
                    <div class="pageHead_1">
                        <%if(procurementNature.equalsIgnoreCase("Services")){%>
                        Draft Agreement
                        <%}else{%>
                        Issue Letter of Acceptance
                        <% } %>
                        <span class="c-alignment-right">
                            <a href="repeatOrderMain.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a>
                        </span>
                    </div>
                         <%
                                 String lotId="";
                                if (request.getParameter("pckLotId") != null) {
                                    pageContext.setAttribute("lotId", request.getParameter("pckLotId"));
                                    lotId = request.getParameter("pckLotId");
                                }
                                 pageContext.setAttribute("tenderId", request.getParameter("tenderId"));

                    %>

                    <%@include  file="../resources/common/ContractInfoBar.jsp"%>
                    <%
                     RepeatOrderService ros = (RepeatOrderService)AppContext.getSpringBean("RepeatOrderService");
                                CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "contractName",tenderId , pckLotId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                List<SPTenderCommonData> list = tenderCommonService.returndata("GetEvaluatedBidders",tenderId,pckLotId);

                                
                                List<Object> CV = ros.countNewContractValue(Integer.parseInt(request.getParameter("romId")));
                                BigDecimal db = new BigDecimal(0);
                                if(!CV.isEmpty()){
                                    db = (BigDecimal)CV.get(0);
                                    }
                                String useridB = "";
                                String roundId = "";
                    %>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                        <tr>
                            <td width="24%" class="ff">Contract No. : <span class="mandatory">*</span></td>
                            <td width="76%"><input name="contractNo" type="text" class="formTxtBox_1" id="txtContractNo" style="width:200px;" maxlength="101"/>
                                <div id="Msg" style="color: red; font-weight: bold"></div>
                            </td>
                        </tr>
                        <tr>
                            <td width="24%" class="ff">Advance Amount (in %) :
                            <td width="76%"><input name="amountCnt" type="text" class="formTxtBox_1" id="amountCnt" style="width:125px;" maxlength="12" onblur="return AdvAmtinPercentage()"/>
                                <span id="AdvAmtPercentageSpan" ></span>&nbsp;
                                <span id="AdvAmtinWordsSpan" class="ff"></span>
                                <span id="AdvAmtspan" class="reqF_1"></span>
                            </td>
                        </tr>
                        <tr>
                            <%
                                        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                        SimpleDateFormat simpl = new SimpleDateFormat("dd-MMM-yyyy");
                            %>
                            <td class="ff">Date of contract :</td>
                            <td class="formStyle_1"><%=simpl.format(new java.util.Date())%>
                                <input type="hidden" value="<%=dateFormat.format(new java.util.Date())%>" name="contractDtString"/></td>
                        </tr>
                        <tr>
                            <td class="ff">Name of Bidder/Consultant :</td>
                            <td>
                               
                                 <%=c_obj[10].toString()+" "+c_obj[11].toString()%>
                                 <input type="hidden" name="hdUserId" value="<%=Integer.parseInt(c_obj[14].toString())%>" />
                                 <input type="hidden" name="companyName" value=" <%=c_obj[10].toString()+" "+c_obj[11].toString()%>" />
                            </td>
                        </tr>
                        <tr>
                            <td width="24%" class="ff">Contract/Project Name : </td>
                            <td width="76%"><label><% if(!packageLotList.isEmpty()){%><%=packageLotList.get(0).getFieldName1() %> <% } %></label>
                                <% if(!packageLotList.isEmpty()){%> <input name="contractName" type="hidden" value="<%=packageLotList.get(0).getFieldName1() %>" class="formTxtBox_1" id="txtprojectName" style="width:200px;" /><% } %>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Contract price In Figure (In Tk.)</td>
                            <td width="76%">
                              
                                <%=db.setScale(3, 0) %>
                                <input name="contractAmtString" type="hidden" class="formTxtBox_1" id="txtContPriceFig" style="width:200px;" value="<%=db.setScale(3, 0) %>" /></td>
                           
                        </tr>
                        <tr>
                            <td class="ff">Contract price In Words (In Tk.)</td>
                            <td><label id="conPrice"></label>
                                <input id="contractAmtWords" name="contractAmtWords" type="hidden"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">No. of days from the date of issuance of <%=NOADA%></td>
                            <%
                                        List<SPCommonSearchData> issueCon = commonSearchService.searchData("ProcureNOA", tenderId, "", "", null, null, null, null, null, null);
                                        dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                        String NOADate = "";
                                        Calendar calendar = java.util.Calendar.getInstance();
                                        List<SPCommonSearchData> holidayFun = null;
                                        if (issueCon.size() != 0) {
                                        holidayFun = commonSearchService.searchData("getBusinessDays", dateFormat.format(new java.util.Date()), issueCon.get(0).getFieldName1(), "", null, null, null, null, null, null);
                                        }
                                        if (holidayFun != null) {
                                        NOADate = holidayFun.get(0).getFieldName1();
                                        dateFormat = new SimpleDateFormat("dd/MM/yyyy");

                                        //calendar.setTime(dateFormat.parse(NOADate));
                                        calendar.setTime(new Date());
                                        }
                                        if (issueCon.get(0).getFieldName2() != null) {
                                            calendar.add(Calendar.DATE, +Integer.parseInt(issueCon.get(0).getFieldName2()));
                                        } else {
                                            calendar.add(Calendar.DATE, +0);
                                        }
                            %>
                            <td><% if (issueCon.size() != 0) {%><%=issueCon.get(0).getFieldName1()%> <% }%>
                                <input type="hidden" <% if (issueCon.size() != 0) {%> value="<%=issueCon.get(0).getFieldName1()%>" <% }%> name="noaIssueDays"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff"><%=NOADA%> acceptance last date & time</td>
                            <td><%=simpl.format(DateUtils.formatStdString(NOADate))%>
                                <input type="hidden" value="<%=NOADate%>" name="noaAcceptDtString"/></td>
                        </tr>
                        <%
                        List<Object[]> obj = tenderSrBean.getConfiForTender(Integer.parseInt(tenderId));
                        
                                List<Object> rIds = ros.isPerfPaidOrNotInIssueNoa(tenderId);
                                if(!rIds.isEmpty())
                                roundId = rIds.get(0).toString();%>
                                <input type="hidden" value="<%=roundId%>" name="roundId" />
                        <%if(!obj.isEmpty() && obj.get(0)[1].toString().equalsIgnoreCase("yes")){
                            List<SPCommonSearchDataMore> perSecAmt = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsForIssue", "perSecAmt",tenderId , pckLotId, roundId);

                        %>
                        
                        <tr>
                            <td class="ff">Performance security amount in Figure (In Tk.)</td>
                            <td width="76%"><%if(!perSecAmt.isEmpty()){ %><%=new BigDecimal(perSecAmt.get(0).getFieldName1()).setScale(3,0) %>
                                <input value="<%=new BigDecimal(perSecAmt.get(0).getFieldName1()).setScale(3,0) %>" name="perfSecAmtString" type="hidden" class="formTxtBox_1" id="txtSecurityAmtFig" style="width:200px;" />
                                <%}%></td>
                        </tr>
                        <tr>
                            <td class="ff">Performance security amount in Words (In Tk.)</td>
                            <td><label id="perSecAmt"></label>
                                <input type="hidden" name="perfSecAmtWords" id="perfSecAmtWords"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">No. of days for performance security submission</td>
                            <td><% if (issueCon.size() != 0) {%><%=issueCon.get(0).getFieldName2()%> <% }%>
                                <input type="hidden" <% if (issueCon.size() != 0) {%> value="<%=issueCon.get(0).getFieldName2()%>" <% }%> name="perSecSubDays"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Last date & time for Performance security submission</td>
                            <td><%=simpl.format(calendar.getTime())%>
                                <input type="hidden" value="<%=dateFormat.format(calendar.getTime())%>" name="perSecSubDtString"/>
                            </td>
                        </tr>
                        <% }


                                    /*if (issueCon != null && issueCon.get(0).getFieldName3() != null) {
                                    calendar.setTime(dateFormat.parse(NOADate));
                                    calendar.add(Calendar.DATE, +Integer.parseInt(issueCon.get(0).getFieldName3()));
                                    } else {
                                    calendar.add(Calendar.DATE, +0);
                                    }*/

                        %>
                        <tr>
                            <td class="ff">Signing of contract in no. of days from the date of issuance</td>
                            <td><% if (issueCon.size() != 0) {%><%=issueCon.get(0).getFieldName3()%> <% }%>
                                <input type="hidden" <% if (issueCon.size() != 0) {%> value="<%=issueCon.get(0).getFieldName3()%>" <% }%> name="contractSignDays"/>
                            </td>
                        </tr>
                        <%
                                    dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                    calendar.setTime(dateFormat.parse(dateFormat.format(new java.util.Date())));
                                    if (issueCon != null && issueCon.get(0).getFieldName3() != null) {
                                    //calendar.add(Calendar.DATE, +Integer.parseInt(holidayFun.get(0).getFieldName3()));
                                    calendar.add(Calendar.DATE, +Integer.parseInt(issueCon.get(0).getFieldName3()));
                                    } else {
                                    calendar.add(Calendar.DATE, +0);
                                    }
                        %>
                        <tr>
                            <td class="ff">Last Date & time of contract signing</td>
                            <td><%=simpl.format(calendar.getTime())%>
                                <input type="hidden" value="<%=dateFormat.format(calendar.getTime())%>" name="contractSignDtString"/>
                                <input type="hidden" value="<%=userId%>" name="createdBy"/>
                                <input type="hidden" value="<%=pckLotId %>" name="pkgLotId"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Upload <%=NOADA%> Reference Document if any</td>
                            <td class="t-align-left"><a onclick="javascript:window.open('NOADocUpload.jsp?tenderid=<%=tenderId %>&pckLotid=<%=pckLotId %>&userid=<%=useridB%>&roundId=<%=roundId%>', '', 'resizable=yes,scrollbars=1','');" href="javascript:void(0);">Upload</a></td>
                        </tr>
                        <!--                        <tr>
                                                    <td class="ff">Mode of Payment</td>
                                                    <td><%// if (issueCon.size() != 0) {%><label><%//issueCon.get(0).getFieldName4()%></label><% //}%>
                                                    </td>
                                                </tr>-->

                        <!--                        <tr>
                                                    <td class="ff">Clause Reference</td>
                                                    <td><textarea rows="4" cols="100" id="clauseRef" name="clauseRef"></textarea>
                                                    </td>
                                                </tr>-->
                    </table>
                    <div id="dataTable">
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="4%" class="t-align-center">Sl. No.</th>
                            <th class="t-align-center" width="27%">File Name</th>
                            <th class="t-align-center" width="28%">File Description</th>
                            <th class="t-align-center" width="7%">File Size <br />
                                (in KB)</th>
                            <th class="t-align-center" width="18%">Action</th>
                        </tr>
                        <%

                                    int docCnt = 0;
                                    /*For data listing*/
                                    List<SPCommonSearchDataMore> sPCommonSearchDataMores = commonSearchDataMoreService.geteGPData("NoaDocInfo", tenderId, pckLotId, useridB, roundId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    for (SPCommonSearchDataMore sptcd : sPCommonSearchDataMores) {
                                        docCnt++;
                        %>
                        <tr>
                            <td class="t-align-center"><%=docCnt%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                            <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                            <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                            <td class="t-align-center">
                                <a href="<%=request.getContextPath()%>/ServletNOADoc?docName=<%=sptcd.getFieldName1()%>&tenderid=<%=tenderId %>&roundId=<%=roundId %>&userid=<%=useridB %>&pckLotid=<%=pckLotId %>&docSize=<%=sptcd.getFieldName3()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                &nbsp;
                                <a href="<%=request.getContextPath()%>/ServletNOADoc?docName=<%=sptcd.getFieldName1()%>&tenderid=<%=tenderId%>&roundId=<%=roundId %>&userid=<%=useridB %>&pckLotid=<%=pckLotId %>&docId=<%=sptcd.getFieldName4()%>&funName=remove" title="Remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                            </td>
                        </tr>

                        <%   if (sptcd != null) {
                                                 sptcd = null;
                                             }
                                         }%>
                        <% if (docCnt == 0) {%>
                        <tr>
                            <td colspan="5" class="t-align-center">No records found.</td>
                        </tr>
                        <%}%>
                    </table>
                </div>
                                                   
                    <div class="t-align-center t_space">
                        <label class="formBtn_1">
                            <input name="Submit" id="Submit" type="submit" value="Submit" onclick="return validateAdvAmt();"/>
                            <input type="hidden" name="hdnbutton" id="hdnbutton" value="" />                            
                        </label>
                    </div>
                </form>
            </div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script type="text/javascript">
        //function contractPrice(obj){
        document.getElementById('conPrice').innerHTML =WORD(document.getElementById('txtContPriceFig').value);
        document.getElementById('contractAmtWords').value =WORD(document.getElementById('txtContPriceFig').value);
        //}
        //function performancePrice(obj){
        document.getElementById('perSecAmt').innerHTML =WORD(document.getElementById('txtSecurityAmtFig').value);
        document.getElementById('perfSecAmtWords').value =WORD(document.getElementById('txtSecurityAmtFig').value);
        //}
    </script>
    <%
                issueNOASrBean = null;
                issueNOADtBean = null;
                tenderSrBean = null;
    %>

    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        function AdvAmtinPercentage()
        {
            var flag = true;
            if(document.getElementById("amountCnt").value!="")
            {
                if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("amountCnt").value))
                {
                    if(document.getElementById("amountCnt").value<=100)
                    {
                        document.getElementById("AdvAmtspan").innerHTML = "";
                        var num =parseFloat(((parseFloat(document.getElementById("amountCnt").value)*(parseFloat(document.getElementById("txtContPriceFig").value)))/(100)));
                        document.getElementById("AdvAmtPercentageSpan").innerHTML=num.toFixed(3);
                        //document.getElementById("AdvAmtinWordsSpan").innerHTML = DoIt(num);
                        //Numeric to word currency conversion by Emtaz on 20/April/2016
                        document.getElementById("AdvAmtinWordsSpan").innerHTML = CurrencyConverter(num);
                    }else
                    {
                        flag = false;
                        document.getElementById("AdvAmtspan").innerHTML = "Advance Amount can not be more than 100%";
                    }
                }else
                {
                    flag = false;
                    document.getElementById("AdvAmtspan").innerHTML = "Please Enter Numeric values";
                }

            }
            return flag;
        }
        function validateAdvAmt()
        {
            var flag = true;
            if(document.getElementById("amountCnt").value!="")
            {
                if(/^[-+]?\d*\.?\d*$/.test(document.getElementById("amountCnt").value))
                {
                    if(document.getElementById("amountCnt").value<=100)
                    {
                        document.getElementById("AdvAmtspan").innerHTML = "";
                    }
                    else
                    {
                        flag = false;
                        document.getElementById("AdvAmtspan").innerHTML = "Advance Amount can not be more than 100%";
                    }
                }
                else
                {
                    flag = false;
                    document.getElementById("AdvAmtspan").innerHTML = "Please Enter Numeric values";
                }
            }
            return flag;
        }
    </script>
    <%}%>
</html>

