<%-- 
    Document   : TenderComm
    Created on : Apr 3, 2016, 1:32:11 PM
    Author     : SRISTY
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderEstCost"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvaluationService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblWorkFlowLevelConfig"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommitteMemberService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    
                    String userId = "";
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userId = hs.getAttribute("userId").toString();
                    }
        %>
        <%
                    TenderEstCost estcost = (TenderEstCost) AppContext.getSpringBean("TenderEstCost");
                    boolean flagEstCost = false;
                    flagEstCost = estcost.checkForLink(Integer.parseInt(request.getParameter("tenderid")));
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    String eventType = commonService.getEventType(request.getParameter("tenderid")).toString();

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender Committee</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>

        <!--jalert -->

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        
    </head>
    <body>

        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <div class="contentArea_1">
        <div class="pageHead_1">Tender Committee Process</div>

            <%
                        String evalTendId = request.getParameter("tenderid");
                        // Variable tenderId is defined by u on ur current page.
                        pageContext.setAttribute("tenderId", evalTendId);
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>

            <div>&nbsp;</div>
            <% pageContext.setAttribute("isTenPackageWise",(Boolean)isTenPackageWise); %>
            <% pageContext.setAttribute("tab","23"); %>
            
            
            <div>
            
            <%@include  file="officerTabPanel.jsp"%>
            
            </div>
            <div class="tabPanelArea_1"> 
                <%if ("y".equals(request.getParameter("ispub"))) {%> <div class="responseMsg successMsg" style="margin-bottom: 12px;">Committee members notified successfully</div><%}%>
            </div>
            
            <%if(flagEstCost || eventType.equalsIgnoreCase("REOI")){%>
            <%@include file="/officer/TenderCommCommon.jsp" %>
            <%}else{%>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <td width="80%" class="t-align-left" style="color: red; font-size: 20px;">Add Official Cost Estimate</td>
                </tr>
            </table>
            <%}%>

            </div>
        
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>

    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

