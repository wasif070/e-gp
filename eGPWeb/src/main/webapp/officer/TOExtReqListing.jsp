<%-- 
    Document   : TOExtReqListing
    Created on : Dec 20, 2010, 7:00:42 PM
    Author     : TaherT
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <style>
            .ui-jqgrid tr.jqgrow td {
                white-space: normal !important;
            }
            .ui-jqgrid tr.jqgrow td {
                white-space: normal !important;
                height:auto;
                vertical-align:text-top;
                padding-top:2px;
            }
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Opening Date &amp; Time Extension Requests Listing</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="../text/javascript" src="resources/js/pngFix.js"></script>-->

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            function fillGridOnEvent(){
                $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                jQuery("#list").jqGrid({
                    url:'<%=request.getContextPath()%>/TimeExtTenderServlet?q=1&action=fetchData&status='+$('#status').val(),
                    datatype: "xml",
                    height: 250,
                    colNames:['Sl. No.','ID','Ref. No.','Brief',"Procuring Entity","Opening Date and Time","Action"],
                    colModel:[
                        {name:'srNo',index:'srNo', width:90,sortable:false,align:'center', search: false},
                        {name:'tenderId',index:'tenderId', width:90,sortable:false,align:'center'},
                        {name:'refNo',index:'refNo', width:90,sortable:false},
                        {name:'brief',index:'brief', width:280,sortable:false,height:10},
                        {name:'procureentity',index:'procureentity', width:150,sortable:false,align:'center'},
                        {name:'datetime',index:'datetime', width:150,sortable:false,align:'center'},
                        {name:'extid',index:'extid', width:90,sortable:false,align:'center', search: false}
                    ],
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    sortable:false,
                    caption: "List of Tender/Proposal for Time Extension",
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{edit:false,add:false,del:false,search:false});
            }
            jQuery().ready(function (){
                //fillGrid();
            });
        </script>
    </head>
    <body onload="fillGridOnEvent();">
        <div class="mainDiv">
            <div class="fixDiv">
                <div class="contentArea_1">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="pageHead_1">Opening Date and Time Extension Requests Listing</div>
                <%--<table align="center">--%>
                <%if("yes".equalsIgnoreCase(request.getParameter("succ"))){%>
                <br/><div class="responseMsg successMsg">Opening date & time extension request approved successfully</div>
                <%}%>
                    <ul class="tabPanel_1 t_space" id="tabForApproved">
                        <li><a href="javascript:void(0);" id="pendingTab" onclick="changeTab(1);" class="sMenu">Pending</a></li>
                        <li><a href="javascript:void(0);" id="processedTab" onclick="changeTab(2);">Processed</a></li>
                    </ul>
                    <div class="tabPanelArea_1 t_space">
                        <div id="jQGrid" align="center">
                            <table id="list"></table>
                            <div id="page"></div>
                        </div>
                    </div>
                    <input type="hidden" id="status" name="status" value="pending"/>
                <%--</table>--%>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            </div>
    </body>
    <script type="text/javascript">
        function changeTab(tabNo){
            if(tabNo == 1){
                $("#processedTab").removeClass("sMenu");
                $("#pendingTab").addClass("sMenu");
                $("#status").val("pending");
            }
            else if(tabNo == 2){
                $("#pendingTab").removeClass("sMenu");
                $("#processedTab").addClass("sMenu");
                $("#status").val("approved");
            }
            fillGridOnEvent();
        }
    </script>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabEval");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
