<%--
    Document   : ServiceWorkPlan
    Created on : Dec 5, 2011, 11:32:20 AM
    Author     : shreyansh.shah
--%>
<%@page import="java.io.PrintWriter"%>
<%@page import="com.cptu.egp.eps.web.utility.GenerateChart"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvSalaryRe"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvStaffSch"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvTeamComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvPaymentSch"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvCnsltComp"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvReExpense"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CMSService"%>
<%@page import="java.util.ResourceBundle" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <% ResourceBundle srbd = null;
                    srbd = ResourceBundle.getBundle("properties.cmsproperty");
                    CMSService cmss = (CMSService) AppContext.getSpringBean("CMSService");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=srbd.getString("CMS.service.staffInputTotal.title")%></title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

    </head>
    <body>
        <%
                    ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String type = "";
                    int tenderId = 0;
                    int lotId = 0;
                    int srvBoqId = 0;
                    String styleClass = "";

                    if (request.getParameter("tenderId") != null) {
                        tenderId = Integer.parseInt(request.getParameter("tenderId"));
                    }
                    if (request.getParameter("lotId") != null) {
                         pageContext.setAttribute("lotId", request.getParameter("lotId"));
                        lotId = Integer.parseInt(request.getParameter("lotId"));
                    }
                    if (request.getParameter("srvBoqId") != null) {
                        srvBoqId = Integer.parseInt(request.getParameter("srvBoqId"));
                    }
                    int ContractId = service.getContractId(Integer.parseInt(request.getParameter("tenderId")));
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->


            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">
                        <%
                        out.print(srbd.getString("CMS.service.staffInputTotal.title"));
                     if (request.getParameter("tenderId") != null) {
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }


                        %>
                        <span style="float: right; text-align: right;" class="noprint">
                    <a class="action-button-goback" href="WorkScheduleMain.jsp?tenderId=<%=tenderId%>" title="Go Back">Go Back</a>
                    </span>
                    </div>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <div>&nbsp;</div>
                    <%if (request.getParameter("lotId") != null) {%>
                    <%@include file="../resources/common/ContractInfoBar.jsp"%>
                    <%}%>
                        <div id="resultDiv" style="display: block;">
                            <div  id="print_area">

                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                                    
                                    <tr>
                                        <th width="20%" class="t-align-center"><%=srbd.getString("CMS.service.workSchedule.empName")%></th>
                                        <th width="50%" class="t-align-center"><%=srbd.getString("CMS.service.workSchedule.positionAssign")%></th>
                                        <th width="10%" class="t-align-center"><%=srbd.getString("CMS.service.workSchedule.workHome")%> (In Days)</th>
                                        <th width="10%" class="t-align-center"><%=srbd.getString("CMS.service.workSchedule.workField")%> (In Days)
                                        </th>
                                        <th width="10%" class="t-align-center"><%=srbd.getString("CMS.total")%>
                                        </th>

                                    </tr>
                                    <%
                                         List<Object[]> listt = cmss.getStaffInputTotal(tenderId,lotId);
                                         int TothomeCnt = 0;
                                         int TotCnt = 0;
                                         int TotfieldCnt = 0;
                                         try {
                                             if (listt != null && !listt.isEmpty()) {
                                                 int i = 0;
                                                 for (i = 0; i < listt.size(); i++) {
                                                     if (i % 2 == 0) {
                                                         styleClass = "bgColor-white";
                                                     } else {
                                                         styleClass = "bgColor-Green";
                                                     }

                                                     out.print("<tr class='" + styleClass + "'>");
                                                     out.print("<td  class=\"t-align-left\">" + listt.get(i)[0] + "</td>");
                                                     out.print("<td  class=\"t-align-left\">" + listt.get(i)[1] + "</td>");
                                                     out.print("<td  class=\"t-align-right\" style=\"text-align:right\">" + listt.get(i)[2] + "</td>");
                                                     out.print("<td  class=\"t-align-right\" style=\"text-align:right\">" + listt.get(i)[3] + "</td>");
                                                     out.print("<td  class=\"t-align-right\" style=\"text-align:right\">" + listt.get(i)[4] + "</td>");
                                                     out.print("</tr>");
                                                     TothomeCnt = TothomeCnt + (Integer)listt.get(i)[2];
                                                     TotfieldCnt = TotfieldCnt + (Integer)listt.get(i)[3];
                                                     TotCnt = TotCnt + (Integer)listt.get(i)[4];
                                                 }
                                             } else {
                                                 out.print("<tr>");
                                                 out.print("<td class=\"t-align-center\" id=\"noRecordFound\" value=\"noRecordFound\" colspan=\"9\" style=\"color: red;font-weight: bold\"> No Records Found</td>");
                                                 out.print("</tr>");
                                             }

                                         } catch (Exception e) {
                                             e.printStackTrace();
                                         }
                                         makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), ContractId, "ContractId", EgpModule.Work_Schedule.getName(), "View Staff InputTotal", "");
                                     %>
                                     <tr><td style="text-align: right" class="ff" colspan="2">Grand Total</td>
                                         <td class="ff" style="text-align: right"><%=TothomeCnt%></td>
                                         <td class="ff" style="text-align: right"><%=TotfieldCnt%></td>
                                         <td class="ff" style="text-align: right"><%=TotCnt%></td>
                                     </tr>
                                </table>
                            </div>
                        </div>


                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" id="first" value="0"/>
                        <br />
                        <center>

                        <%
                         GenerateChart gc = new GenerateChart();
                         String barfilename = gc.BarChart(session, new PrintWriter(out),tenderId,lotId);
                         String barchart = request.getContextPath()+ "/servlet/DisplayChart?filename=" + barfilename;
%>
<img src="<%=barchart%>" border="0" usemap="#<%= barfilename %>" />
                        </center>
                </div>

                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
