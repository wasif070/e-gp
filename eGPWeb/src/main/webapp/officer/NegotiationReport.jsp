<%-- 
    Document   : NegotiationReport
    Created on : Dec 26, 2011, 10:56:14 AM
    Author     : dipal.shah
--%>


<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>

<jsp:useBean id="negotiationProcessSrBean" class="com.cptu.egp.eps.web.servicebean.NegotiationProcessSrBean"></jsp:useBean>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">

    <html>
        <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");

            String tenderid = request.getParameter("tenderid");
            String roundId = request.getParameter("rId");
            String lotId = request.getParameter("lotId");
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
            TenderCommonService objTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            int uId=0;
            uId= Integer.parseInt(session.getAttribute("userId").toString());
            String negId = null;
            String negUserId = null;
            String tenderRefId = "";
            String peOfficerName = "";
            String nameOfConsultant = "";
            
            if(request.getParameter("negId")!= null)
            {
                negId=request.getParameter("negId");
            }
            if(request.getParameter("uId")!= null)
            {
                negUserId=request.getParameter("uId");
            }
            
            if (tenderid != null && (negId == null || negUserId == null))
            {
                Object[] objects = negotiationProcessSrBean.getNegoTenderInfoFromTenderId(Integer.parseInt(tenderid));
                if(objects!=null && objects.length>0)
                {
                    negId = objects[0].toString();
                    negUserId = objects[1].toString();
                    peOfficerName = objects[2].toString();
                    tenderRefId = objects[3].toString();
                }
            }
            
            if (negUserId != null && !negUserId.equalsIgnoreCase(""))
            {
                Object[] objects = negotiationProcessSrBean.getTendererInfoFromUserId(Integer.parseInt(negUserId));
                if (objects != null && objects[3] != null && objects[3].toString().equalsIgnoreCase("1"))
                {
                    nameOfConsultant = objects[01].toString() + " " + objects[2].toString();
                }
                else
                {
                    nameOfConsultant = objects[0].toString();
                }

            }


            boolean bole_ter_flag = true;
            if (request.getParameter("isPDF") != null && "true".equalsIgnoreCase(request.getParameter("isPDF")))
            {
                bole_ter_flag = false;
                //System.out.println("isPDF"+bole_ter_flag);
            }
            String userId = "";
            if (bole_ter_flag)
            {
                if (session.getAttribute("userId") != null)
                {
                    userId = session.getAttribute("userId").toString();
                }
            }
            else
            {
                if (request.getParameter("tendrepUserId") != null && !"".equals(request.getParameter("tendrepUserId")))
                {
                    userId = request.getParameter("tendrepUserId");
                }
            }

            boolean showGoBack = true;
            if ("y".equals(request.getParameter("in")))
            {
                showGoBack = false;
            }


        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Negotiation Report</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <body>
        <%if (bole_ter_flag)
             {%>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%}
            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            boolean isView = false;
            if ("y".equals(request.getParameter("isview")))
            {
                isView = true;
            }


            // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            String idType = "tenderId";
            int auditId = Integer.parseInt(tenderid);
            String auditAction = "View Negotiation Report";
            String moduleName = EgpModule.Negotiation.getName();
            String remarks = "User Id:" + session.getAttribute("userId") + " has view Negotiation Report for Tender Id:" + tenderid;
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

        %>
        <div class="tabPanelArea_1">
            <div  id="print_area">
                <div class="pageHead_1">View Negotiation Report<span style="float: right;">
                        <%
                            String backLink = request.getHeader("Referer");
                            if (request.getParameter("ReportFrom") != null)
                            {
                                if (request.getParameter("ReportFrom").equalsIgnoreCase("TER4"))
                                {
                                    backLink = "TER4.jsp?tenderid=" + tenderid + "&lotId=" + lotId + "&rId=" + roundId + "&isview=y";
                                }
                                else if (request.getParameter("ReportFrom").equalsIgnoreCase("TER3"))
                                {
                                    backLink = "TER3.jsp?tenderid=" + tenderid + "&lotId=" + lotId + "&rId=" + roundId + "&isview=y";
                                }
                            }else{
                        %>
                        <a id="goBack" class="action-button-goback" href="<%= backLink%>">Go Back to Dashboard</a>
                        <%}%>

                    </span>
                </div>

                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 b_space" width="100%">
                    <tr>
                        <td width="24%" class="ff">ID : </td>
                        <td> <%= tenderid%></td>
                    </tr>
                    <tr>
                        <td width="24%" class="ff">Reference No. : </td>
                        <td> <%= tenderRefId%></td>
                    </tr>
                    <tr>
                        <td width="24%" class="ff">PE Office Name : </td>
                        <td> <%= peOfficerName%></td>
                    </tr>
                    <tr>
                        <td width="24%" class="ff"> Name of Consultant : </td>
                        <td> <%= nameOfConsultant%> </td>
                    </tr>

                </table>

                <div class="inner-tableHead t_space">Negotiation Reference Documents:</div>
                 <div class="tabPanelArea_1">

                   <div class="t-align-left ff">
                        Document uploaded during Negotiation Bid Process:
                   </div>
                   <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-left">Sl. No.</th>
                        <th class="t-align-left" width="23%">File Name</th>
                        <th class="t-align-left" width="32%">File Description</th>
                        <th class="t-align-left" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-left" width="28%">File Uploaded by</th>
                        <th class="t-align-left" width="18%">Action</th>
                    </tr>
                    <%
                            int docCnt = 0;
                            TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                            for (SPTenderCommonData sptcd : tenderCS.returndata("NegotiationDocInfo", negId, "revisedoc")) {
                                docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-left">
                        <%
                            if(uId != Integer.parseInt(sptcd.getFieldName5()))
                            {
                                out.println("Evaluation Committee");
                            }
                            else
                            {
                                out.println("Bidder/Consultant ");
                            }
                        %>
                        </td>
                         <td class="t-align-center">
                             <%
                                if(uId != Integer.parseInt(sptcd.getFieldName5()))
                                 {
                                 %>
                                    <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderid%>&negId=<%=negId%>&funName=download&ub=2" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>&nbsp;
                                 <%
                                 }
                                else
                                    {
                                %>
                                    <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderid%>&negId=<%=negId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>&nbsp;
                                 <%
                                    }
                                %>

                        </td>
                    </tr>

                    <%   if (sptcd != null) {
                             sptcd = null;
                         }
                                     }%>
                    <% if (docCnt == 0) {%>
                        <tr>
                            <td colspan="6" class="t-align-center">No records found.</td>
                        </tr>
                    <%}%>
                </table>

                <BR>

                   <div class="t-align-left ff">
                        Document uploaded by Evaluation Committee during Negotiation Submission:
                   </div>
                   <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-left">Sl. No.</th>
                        <th class="t-align-left" width="23%">File Name</th>
                        <th class="t-align-left" width="32%">File Description</th>
                        <th class="t-align-left" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-left" width="18%">Action</th>
                    </tr>
                    <%
                        docCnt = 0;
                        for (SPTenderCommonData sptcd : tenderCS.returndata("NegotiationDocInfo", negId, "negotiation"))
                        {
                            docCnt++;
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/ServletNegotiationDocs?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderId=<%=tenderid%>&negId=<%=negId%>&funName=download&docType=negotiation" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>&nbsp;
                        </td>
                    </tr>

                    <%   if (sptcd != null)
                            {
                                sptcd = null;
                            }
                        }%>
                    <% if (docCnt == 0)
                        {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                </table>
                 </div>


                <BR>
                <div class="inner-tableHead t_space">Revised Forms:</div>
                <table width="100%" cellspacing="0" class="tableList_3">
                    <tr>
                        <th width="22%" class="t-align-left ff">Form Name</th>
                        <th width="78%" class="t-align-left">Action</th>
                    </tr>
                    <%
                        int formCnt = 0;
                        for (SPCommonSearchData formdetails : commonSearchService.searchData("GetNegTenderFormsList", tenderid, negUserId, null, null, null, null, null, null, null))
                        {
                    %>
                    <tr>
                        <td class="t-align-left ff">
                            <%=formdetails.getFieldName2()%>
                        </td>
                        <td class="t-align-left">
                            <%
                                for (SPCommonSearchData negoformdetails : commonSearchService.searchData("GetNegBidderFormsData", tenderid, negUserId, formdetails.getFieldName1(), null, null, null, null, null, null))
                                {
                                    formCnt++;
                                    if(negoformdetails.getFieldName4()!=null)
                                    {
                                        %>
                                        <a href="../resources/common/ViewNegBidform.jsp?tenderId=<%=tenderid%>&formId=<%=formdetails.getFieldName1()%>&bidId=<%=negoformdetails.getFieldName3()%>&uId=<%=negUserId%>&negId=<%=negId%>&negBidformId=<%= negoformdetails.getFieldName4() %>&lotId=0&type=negbiddata&action=View&boqId=<%=formdetails.getFieldName3()%>&ContractType=<%=formdetails.getFieldName5()%>"> View
                                        </a>
                                        <BR>
                                        <%
                                    }
                                    else
                                    {
                                        %>
                                        <span style='color:red;'>Bidder/Consultant has not revised this form</span>
                                        <%
                                    }
                                }

                            %>
                        </td>
                    </tr>
                    <%




                        } // End of Form loop

                        // Dispalying Grand Summary.            
                        List<SPTenderCommonData> grandsumlink = objTenderCommonService.returndata("getGrandSummaryLinkNego", tenderid, negId);
                        if (!"0".equals(grandsumlink.get(0).getFieldName1()))
                        {
                            formCnt++;
                    %>

                    <tr>
                        <td class="t-align-left ff"><span style="font-weight: bold;">Grand Summary Form</span></td>
                        <td>  
                            <a href="<%=request.getContextPath()%>/officer/NegoGrandSumm.jsp?tenderId=<%=tenderid%>&lotId=<%=lotId%>&rId=<%=roundId%>&negId=<%=negId%>&uId=<%=negUserId%>"> View </a>
                        </td>
                    </tr> 
                    <%
                        }

                        if (formCnt == 0)
                        {%>
                        <tr>
                            <td colspan="5" class="t-align-center">No records found.</td>
                        </tr>
                        <%
                         }
                    %>
                </table>
            </div>
        </div>

        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>

    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function() {

            $("#print").click(function() {
                //alert('sa');
                printElem({ leaveOpen: true, printMode: 'popup' });
            });

        });
        function printElem(options){
            //alert(options);
            $('#print').hide();
            $('#saveASPDF').hide();
            $('#goBack').hide();
            $('#print_area').printElement(options);
            $('#print').show();
            $('#saveASPDF').show();
            $('#goBack').show();
            //$('#trLast').hide();
        }
    </script>
</html>
