<%-- 
    Document   : GenerateInvoice
    Created on : Aug 5, 2011, 10:57:07 AM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsPrMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="dDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
                        pageContext.setAttribute("tab", "14");
                        String prId = "";
                        String tenderId = request.getParameter("tenderId");
                        String wpId = request.getParameter("wpId");
                        if (request.getParameter("prId") != null) {
                            prId = request.getParameter("prId");
                        }
                          ConsolodateService service = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
                          service.setLogUserId(session.getAttribute("userId").toString());

            %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Generate Invoice</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/common.js" type="text/javascript"></script>
<script type="text/javascript">
            /* check if pageNO is disable or not */
            var totalAmount = 0;
            function Calculate(id){

                var count = document.getElementById("listcount").value;
               
                    
                    if(document.getElementById("inv_"+id).checked){
                     // var rate = Math.round(eval(document.getElementById("rate_"+id).value)*Math.pow(10,3))/Math.pow(10,3)
                      totalAmount =   totalAmount + (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                      var totalrateofqty = eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value);
                      var a = (Math.round(eval(totalrateofqty)*Math.pow(10,3))/Math.pow(10,3))+"";
                      if(a.indexOf(".")<0){
                          a = a+".000";
                      }
                      document.getElementById("totalrateofqty_"+id).innerHTML=a;//Math.round(eval(totalrateofqty)*Math.pow(10,3))/Math.pow(10,3)
                    }else{
                        totalAmount =   totalAmount - (eval(document.getElementById("rate_"+id).value)*eval(document.getElementById("qty_"+id).value));
                        document.getElementById("totalrateofqty_"+id).innerHTML="0.000";
                    }
                    var b = (Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3))+"";
                      if(b.indexOf(".")<0){
                          b = b+".000";
                      }
                     document.getElementById("totamt").innerHTML=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
                     document.getElementById("totalamount").value=b;//Math.round(eval(totalAmount)*Math.pow(10,3))/Math.pow(10,3);
               
            }

            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        
        <script type="text/javascript">
            /*  Handle First click event */
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo =$('#pageNo').val();
                    $('#first').val(0);
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Last Button click event */
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var size = (parseInt($('#size').val())*totalPages)-(parseInt($('#size').val()));
                    $('#first').val(size);
                    if(totalPages>0){
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });

            function onFire(){
                var count = document.getElementById("listcount").value;
                var flag = true;
                for(var i=0;i<count;i++){
                    if(document.getElementById("inv_"+i).checked){
                        flag=false;
                    }
                    if(!flag){
                        break;
                    }
                }
                 if(flag){
                    jAlert("Please select at least one item to generate Invoice","Invoice", function(RetVal) {
                    });
                }else{
//                    document.frm.action = "<%=request.getContextPath()%>/InvoiceGenerationServlet";
//                    document.frm.method="post";
//                    document.frm.submit();
                    document.forms["frm"].submit();
                }
            }
        </script>
        <script type="text/javascript">
            /*  Handle Next Button click event */
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val());
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first+max);

                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo <= totalPages) {

                        loadTable();
                        $('#pageNo').val(Number(pageNo)+1);
                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            /*  Handle Previous click event */
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var first = parseInt($('#first').val());
                    var max = parseInt($('#size').val());
                    $('#first').val(first-max);
                    //$('#size').val(max+parseInt(document.getElementById("size").value));
                    var totalPages=parseInt($('#totalPages').val(),10);

                    $('#pageNo').val(Number(pageNo)-1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)-1);
                });
            });
        </script>
        <script type="text/javascript">
            function Search(){
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            }
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var size = (parseInt($('#size').val())*pageNo)-$('#size').val();
                    $('#first').val(size);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }

                });
            });
            function changetable(){
                var pageNo=parseInt($('#pageNo').val(),10);
                var first = parseInt($('#first').val());
                var max = parseInt($('#size').val());
                var totalPages=parseInt($('#totalPages').val(),10);
                loadTable();

            }

        </script>

    </head>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Invoice
            <span class="c-alignment-right"><a href="Invoice.jsp?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back</a></span>
            </div>
            <% pageContext.setAttribute("tenderId", request.getParameter("tenderId"));%>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
           
            <%@include  file="officerTabPanel.jsp"%>
            <div class="tabPanelArea_1">

                <%
                            pageContext.setAttribute("TSCtab", "4");

                            String lotId = "";
                            if (request.getParameter("lotId") != null) {
                                pageContext.setAttribute("lotId", request.getParameter("lotId"));
                                lotId = request.getParameter("lotId");
                            }
                            boolean isWCC = service.isWorkCompleteOrNot(lotId);

                %>
                <%@include  file="../resources/common/CMSTab.jsp"%>
                <div class="tabPanelArea_1">
                   
                    <%if(isWCC){%>
 <div  class='responseMsg noticeMsg t-align-left'>Current Invoice will be considered as a Final Invoice as Work Completion Certificate has been issued by Procuring Entity</div>
                    <%}%>

                    <%@include  file="../resources/common/ContractInfoBar.jsp"%>
                    <div align="center">

                        <%


                                    boolean flag = true;
                                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                    List<SPCommonSearchDataMore> packageLotList = commonSearchDataMoreService.geteGPData("GetLotPackageDetailsWithContractId", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    List<SPTenderCommonData> tenderValSec = tenderCommonService.returndata("getTenderDatesDiff", tenderId, null);
                                  
                                    int i = 0;
                                    boolean flags = false;
                                    CmsConfigDateService ccds = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                                    for (SPCommonSearchDataMore lotList : packageLotList) {

                                        flags = ccds.getConfigDatesdatabyPassingLotId(Integer.parseInt(lotList.getFieldName5()));

                        %>

                        <form name="frm" action="<%=request.getContextPath()%>/InvoiceGenerationServlet" method="post">

                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class=" tableList_1 t_space">
                                <tr>
                                    <td width="20%">Lot No.</td>
                                    <td width="80%"><%=lotList.getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td>Lot Description</td>
                                    <td class="t-align-left"><%=lotList.getFieldName4()%></td>
                                </tr>





                                <%
                                            }%>
                            </table>
                                            <div id="resultDiv" style="display: block;">
                            <div  id="print_area">

                                <table width="100%" cellspacing="0" class="tableList_1 t_space">

                                    <tr>
                                        <%if(procnature.equalsIgnoreCase("goods")){%>
                                        <th width="3%" class="t-align-center">Check</th>
                                        <th width="3%" class="t-align-center">S.No</th>
                                        <th width="20%" class="t-align-center">Description</th>
                                        <th width="15%" class="t-align-center">Unit<br />
                                            of Measurement
                                            <br />
                                        </th>
                                        <th width="10%" class="t-align-center">Qty
                                        </th>
                                        <th width="18%" class="t-align-center">Rate
                                        </th>
                                        <th width="18%" class="t-align-center">Qty for Invoice
                                        </th>
                                        <th width="18%" class="t-align-center">Item Invoice Amount
                                        </th>
                                        <%}else{%>
                                        <th width="3%" class="t-align-center">Check</th>
                                        <th width="3%" class="t-align-center">S.No</th>
                                        <th width="10%" class="t-align-center">Group</th>
                                        <th width="20%" class="t-align-center">Description</th>
                                        <th width="15%" class="t-align-center">Unit<br />
                                            of Measurement
                                            <br />
                                        </th>
                                        <th width="10%" class="t-align-center">Qty
                                        </th>
                                        <th width="18%" class="t-align-center">Rate
                                        </th>
                                        <th width="18%" class="t-align-center">Qty for Invoice
                                        </th>
                                        <th width="25%" class="t-align-center">Item Invoice Amount
                                        </th>
                                        <%}%>
                                    </tr>
                                    <tbody id="resultTable"></tbody>
                                </table>

                            </div>
                        </div>
                        <div id="tohide">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                                <tr>
                                    <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        Total Records per page : <select name="size" id="size" onchange="changetable();" style="width:40px;">
                                            <option value="1000" selected>10</option>
                                            <option value="20">20</option>
                                            <option value="30">30</option>

                                        </select>
                                    </td>
                                    <td align="center"><input name="textfield3" type="text" id="dispPage" onKeydown="javascript: if (event.keyCode==13) Search();" value="1" class="formTxtBox_1" style="width:20px;" />
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input type="submit" name="button"  id="btnGoto" value="Go To Page" />
                                        </label></td>
                                    <td  class="prevNext-container">
                                        <ul>
                                            <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                            <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                            <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                            <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                        </ul>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" id="first" value="0"/>
                        <br />
                        <%if(procnature.equalsIgnoreCase("goods")){%>
                        <div  class='responseMsg noticeMsg t-align-left'>Once the Invoice has been send to Supplier, it cannot be edited</div>
                        <%}else{%>
                        <div  class='responseMsg noticeMsg t-align-left'>Once the Invoice has been send to Contractor, it cannot be edited</div>
                        <%}%>
                        <br />

                        <center>
                           
                            <label class="formBtn_1">
                                <%if(procnature.equalsIgnoreCase("goods")){%>
                                <input type="button" name="Boqbutton" id="Boqbutton" value="Send To Supplier" onclick="onFire();" />
                                <%}else{%>
                                <input type="button" name="Boqbutton" id="Boqbutton" value="Send To Contractor" onclick="onFire();" />
                                <%}%>
                                <input type="hidden" name="cms" id="cms" value="cms"  />
                                <input type="hidden" name="wpId" id="wpId" value="<%=request.getParameter("wpId")%>"  />
                                <input type="hidden" name="tenderId" id="tenderId" value="<%=request.getParameter("tenderId")%>"  />
                                <input type="hidden" name="prId" id="prId" value="<%=request.getParameter("prId")%>"  />
                                <input type="hidden" name="action" id="action" value="submit"  />
                                <input type="hidden" name="lotId" id="action" value="<%=lotId%>"  />
                            </label>
                           
                        </center>
 </form>
                </div>

                    </div>
                </div></div></div>

        <%@include file="../resources/common/Bottom.jsp" %>
    <script>
        function Edit(wpId,tenderId,lotId){


            dynamicFromSubmit("EditDatesForBoq.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId+"&flag=cms");

        }
        function view(wpId,tenderId,lotId){
            dynamicFromSubmit("ViewDates.jsp?wpId="+wpId+"&tenderId="+tenderId+"&lotId="+lotId);
        }
    </script>
    <script type="text/javascript">
            /*  load Grid for the User Registration Report */
            function loadTable()
            {
                $('#tohide').hide();
                if($("#keyWord").val() == undefined)
                    $("#keyWord").val('');
                $.post("<%=request.getContextPath()%>/InvoiceGenerationServlet", {size: $("#size").val(),pageNo: $("#first").val(),action:'displayItem',wpId: '<%=wpId%>',paymentType:'<%=c_obj[5].toString()%>',lotId:'<%=lotId%>',prId:'<%=prId%>',tenderId:'<%= request.getParameter("tenderId") %>' },  function(j){
                    $('#resultTable').append(j.toString());
                    if($('#noRecordFound').val() == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }
        </script>
    <script>
        var headSel_Obj = document.getElementById("headTabTender");
        loadTable();
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>

</html>
