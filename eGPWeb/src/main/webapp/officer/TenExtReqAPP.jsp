<%--
    Document   : TenExtReqAPP
    Created on : Nov 29, 2010, 3:21:34 PM
    Author     : rajesh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender validity extension request</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmhope").validate({
                    rules: {
                        txtext: { required: true}
                    },
                    messages: {
                        txtext: { required: "<div class='reqF_1'>Please enter Extension Reason</div>"}
                    }
                }
            );
            });


        </script>
    </head>
    <body>
        <%
        HandleSpecialChar handleSpecialChar = new  HandleSpecialChar();
                    if (request.getParameter("btnhope") != null) {

                        //String dtXml = "";
                        java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                        String table="", updateString = "", whereCondition="";
                        table="tbl_TenderValidityExt";
                        updateString=" extReason='"+handleSpecialChar.handleSpecialChar(request.getParameter("txtext"))+"', extStatus='"+request.getParameter("select2")+"'";
                        whereCondition="valExtReqId=" + request.getParameter("ExtId");
                                    out.println("update "+table+ " set "+updateString +" where "+ whereCondition );

                        //dtXml = "<root><tbl_TenderValidityExt tenderId=\"" + request.getParameter("tenderId") + "\" "
                        //        + "extReason=\"" + request.getParameter("txtext").toString().replace("'", "''") + "\" extRequestSignature=\"\"  requestDate=\"" + format.format(new Date()) + "\" extStatus=\"Pending\" extReplyActionBy=\"" + request.getParameter("select2") + "\" extReplyActionDate=\"" + format.format(new Date()) + "\" extReplySignature=\"\" /></root>";

                        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                        CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_TenderValidityExt", updateString, whereCondition).get(0);
                        //out.println(commonMsgChk.getMsg());
                        if(commonMsgChk.getFlag()==true)
                        {
                            response.sendRedirect("TenderExtReqList.jsp");
                        }
                        else
                        {
                            out.println(commonMsgChk.getMsg());
                        }
                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Validity Extension Request</div>
            <div class="mainDiv">
                <%
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
            </div>
            <div>&nbsp;</div>
            <hr />
            <div class="t-align-left t_space">
                <span><a href="#" class="action-button-goback">Go Back to Dashboard</a></span>
                <span style="font-style: italic" class="t-align-left" style="float:left; font-weight:normal;">
			Fields marked with (<span class="mandatory">*</span>) are mandatory
                </span>
            </div>
            <%--<form id="frmhope" action="TenderExtReqList.jsp?tenderId=<%=request.getParameter("tenderId")%>" method="post">--%>
                <form id="frmhope" action="" method="post">
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <td width="14%" class="t-align-left ff">Extension Reason : <span class="mandatory">*</span> </td>
                        <td width="86%" class="t-align-left">
                        <%
                            List<SPTenderCommonData> list = tenderCommonService.returndata("GetExtDetails", request.getParameter("ExtId"), null);
                                if (!list.isEmpty()) {
                        %>
                            <textarea cols="100" rows="5" id="txtext" name="txtext" class="formTxtBox_1"><%=list.get(0).getFieldName1()%></textarea>
                        <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left ff">Action : <span class="mandatory">*</span></td>
                        <td class="t-align-left">
                            <select name="select2" class="formTxtBox_1" id="select2" style="width:100px;">
                                <option value="Approved">Approved</option>
                                <option value="Pending">Pending</option>
                            </select>

                        </td>
                    </tr>
                </table>
                <div class="t-align-center t_space">
                    <label class="formBtn_1">
                        <input name="btnhope" id="btnhope" type="submit" value="Submit" />
                    </label>
                </div>
            </form>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
