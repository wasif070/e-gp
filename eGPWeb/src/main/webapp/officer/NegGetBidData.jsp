<%--
    Document   : GetBidData
    Created on : Nov 27, 2010, 4:17:03 PM
    Author     : Administrator
--%>

<%@page import="org.apache.log4j.Logger"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.lang.reflect.Array"%>
<%@page import="com.cptu.egp.eps.web.utility.HashUtil"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ListIterator"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="tenderBidSrBean"  class="com.cptu.egp.eps.web.servicebean.TenderBidSrBean" />

<%
    Logger LOGGER = Logger.getLogger("NegGetBidData.jsp");
    String action = "Save";
    if(request.getParameter("save")!=null && !"".equalsIgnoreCase(request.getParameter("save"))){
        action = request.getParameter("save");
    }else{
        if(request.getParameter("encrypt")!=null && !"".equalsIgnoreCase(request.getParameter("encrypt"))){
            action = request.getParameter("encrypt");
        }
    }


    int bidId = 0;
    if(request.getParameter("hdnBidId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnBidId"))){
        bidId = Integer.parseInt(request.getParameter("hdnBidId"));
    }

    int lotId = 0;
    if(request.getParameter("hdnLotId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnLotId"))){
        lotId = Integer.parseInt(request.getParameter("hdnLotId"));
    }
    int negId = 0;
    if(request.getParameter("hidnegId")!=null && !"".equalsIgnoreCase(request.getParameter("hidnegId"))){
        negId = Integer.parseInt(request.getParameter("hidnegId"));
    }

    int uId = 0;
    if(request.getParameter("hiduId")!=null && !"".equalsIgnoreCase(request.getParameter("hiduId"))){
        uId = Integer.parseInt(request.getParameter("hiduId"));
    }

    String formType = "";
    if(request.getParameter("formType")!=null){
        formType = request.getParameter("formType");
    }
    
    
    int tableCount = 0;
    int tableId = 0;
    int columnId = 0;

    int tableIds[] = new int[1000];
    for(int i=0;i<1000;i++){
        tableIds[i] = 0;
    }

    if("Save".equalsIgnoreCase(action) || "Update".equalsIgnoreCase(action)){
        // General Declaration
        ListIterator<CommonFormData> tblBidCellsDtl = null;

        // Bid Form
        int tenderId = 0;
        if(request.getParameter("hdnTenderId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnTenderId"))){
            tenderId = Integer.parseInt(request.getParameter("hdnTenderId"));
        }

        int tenderFormId = 0;
        if(request.getParameter("hdnFormId")!=null && !"".equalsIgnoreCase(request.getParameter("hdnFormId"))){
            tenderFormId = Integer.parseInt(request.getParameter("hdnFormId"));
        }

        int userId = 0;
        if(session.getAttribute("userId") != null) {
            if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                userId = Integer.parseInt(session.getAttribute("userId").toString());
            }
        }

        int submittedTime = 1;
        String submissionDt =  new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new java.util.Date().getTime());
        

        List<CommonFormData> formTables = tenderBidSrBean.getFormTables(tenderFormId);

        int cntTableId = 0;
        int submittedTimes = 1;
        
        //tblXML_TenderBidTable = "<root>";
        for(CommonFormData formData : formTables){
            tableId = formData.getTableId();

            if(request.getParameter("tableCount"+tableId) != null){
                tableCount = Integer.parseInt(request.getParameter("tableCount"+tableId));
            }

            for(int i=1;i<=tableCount;i++)
            {
                tableIds[cntTableId++] = tableId;
            }
        }
        
        String tblXML_TenderBidDetail = "";

            // Bid Details - cellvalue
            int rowId = 0;
            int tmpRowId = 0;
            int cellId = 0;
            int cellCnt = 0;
            int clCnt = 0;
            boolean rFlag = true;
            String cellValue = "";
            int isQty = 0;
            tableId = 0;
            boolean bidFlag = false;
            int bidTableId = 0;
            tblXML_TenderBidDetail = "<root>";

            int identkey = 0;
            for(int i=0;i<tableIds.length;i++)
            {
                clCnt=0;

                if(tableIds[i]!=0)
                {
                    identkey++;
                    if(tableId != tableIds[i])
                    {
                        tableId = tableIds[i];
                        tblBidCellsDtl = tenderBidSrBean.getTableNegBidCells(tableId).listIterator();
                        tmpRowId = 0;
                        bidFlag = true;
                    }
                    else
                    {
                        while(tblBidCellsDtl.hasPrevious())
                        {
                            tblBidCellsDtl.previous();
                        }
                        bidFlag = false;
                        cellCnt = tenderBidSrBean.getBidCellCnt(tableId);
                        tmpRowId++;
                        rFlag = true;
                    }
                    //rowId++;

                    while(tblBidCellsDtl.hasNext())
                    {
                        CommonFormData bidCelData = tblBidCellsDtl.next();

                        columnId = bidCelData.getColumnId();
                        if(bidFlag)
                        {
                            rowId = bidCelData.getRowId();
                            tmpRowId = rowId;
                        }
                        else
                        {
                            if(rFlag)
                            {
                                clCnt++;
                            }
                            if((cellCnt==1) && (!(rFlag)))
                            {
                                tmpRowId++;
                                rFlag = false;
                            }
                            else
                            {
                                if(clCnt==(cellCnt+1))
                                {
                                    tmpRowId++;
                                    clCnt=0;
                                    rFlag = false;
                                }
                            }

                            rowId = bidCelData.getRowId();
                        }

                        
                        
                        if(request.getParameter("row"+tableId+"_"+tmpRowId)!=null){
                            isQty = 1;
                        }else{
                            isQty = 0;
                         }
                        
                        cellValue = request.getParameter("row"+tableId+"_"+tmpRowId+"_"+columnId);
                        cellId = bidCelData.getCellId();
                        bidTableId = Integer.parseInt(request.getParameter("hidbidTableId"+tableId));
                        tblXML_TenderBidDetail += "<tbl_NegotiatedBid "

                        + "bidtableId=\""+bidTableId+"\" "
                        + "tenderColumnId=\""+ columnId +"\" "
                        + "tenderTableId=\""+ tableId +"\" "
                        + "cellValue=\""+ cellValue +"\" "
                        + "rowId=\""+ (rowId) +"\" "
                        + "cellId=\""+ cellId +"\" "
                        + "formId=\""+ tenderFormId +"\" "
                        + "isQty=\""+isQty+"\" "
                        + "negId=\""+negId+"\" "
                        + "/>";
                    }
                }
                else
                {
                    break;
                }
            }

            tblXML_TenderBidDetail += "</root>";

            

           if("Update".equalsIgnoreCase(action)){
                try{
                    if(tenderBidSrBean.updateNegFormData(tblXML_TenderBidDetail,bidTableId)){
                        
                        if("Service".equalsIgnoreCase(formType)){
                            response.sendRedirect("NegForms.jsp?tenderId="+tenderId+"&uId="+uId+"&negId="+negId);
                        }else{
                            response.sendRedirect("NegBidPrepare.jsp?tenderId="+tenderId+"&uId="+uId+"&negId="+negId+"&msg=edit");
                        }
                    }else{
                        LOGGER.debug("Form Data updation unsuccessfully");
                    }
                }catch(Exception e){
                    LOGGER.error("Error in Insert form data : "+e.toString());
                }
            }
    }
%>
