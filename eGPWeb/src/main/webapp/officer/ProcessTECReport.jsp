<%-- 
    Document   : ProcessTECReport
    Created on : Feb 15, 2011, 12:10:05 PM
    Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk,com.cptu.egp.eps.web.utility.HandleSpecialChar,java.text.SimpleDateFormat,com.cptu.egp.eps.web.utility.MailContentUtility,com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Evaluation Committee Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
    </head>
    <body>
        <div class="dashboard_div">

            <%  String tenderId, strSt="";
                tenderId = request.getParameter("tenderId");
                strSt=request.getParameter("st");
                String sentBy = "", role = "";
                String aaName="", aaUserId="", evalRptAppId="0";
                boolean isSentToAA=false, isProcessByAA=false;
                if(session.getAttribute("userId") != null)
                  sentBy = session.getAttribute("userId").toString();

                CommonSearchService commonSearchService1 = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                String xmldata = "";
                String remark = "";


                List<SPTenderCommonData> lstgetAAName = tenderCS.returndata("getTenderConfigData",tenderId,null);
                if(!lstgetAAName.isEmpty()){
                    aaName=lstgetAAName.get(0).getFieldName2();
                    aaUserId=lstgetAAName.get(0).getFieldName7();
                }

                 List<SPTenderCommonData> lstgetSentToAAStatus = tenderCS.returndata("getSentToAAEntry",tenderId,null);
                if(!lstgetSentToAAStatus.isEmpty()){
                    if("yes".equalsIgnoreCase(lstgetSentToAAStatus.get(0).getFieldName1())){
                        isSentToAA=true;
                    }

                     if("yes".equalsIgnoreCase(lstgetSentToAAStatus.get(0).getFieldName2())){
                        isProcessByAA=true;
                    }

                    if(!"0".equalsIgnoreCase(lstgetSentToAAStatus.get(0).getFieldName3())){
                        evalRptAppId=lstgetSentToAAStatus.get(0).getFieldName3();
                    }
                }

         if (request.getParameter("btnSubmit") != null) {
             String redirectURL = "";
             String action="";
             HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
             SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

             action=request.getParameter("cmbAction");
             remark = request.getParameter("txtComments");

             remark = handleSpecialChar.handleSpecialChar(remark);

             xmldata = "<tbl_EvalRptApprove evalRptToAAId=\"" + evalRptAppId + "\" actionBy=\"" + aaUserId + "\" tenderId=\"" + tenderId + "\" action=\"" + action +  "\" remarks=\"" + remark + "\" actionDate=\"" + format.format(new Date()) + "\" />";
             xmldata = "<root>" + xmldata + "</root>";

             

             CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
             CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_EvalRptApprove", xmldata, "").get(0);

             if (commonMsgChk.getFlag().equals(true)) {
                 redirectURL = "ProcessTECReport.jsp?tenderId=" + tenderId + "&st=" + strSt + "&msgId=processed";
             } else {
                 redirectURL = "ProcessTECReport.jsp?tenderId=" + tenderId + "&st=" + strSt + "&msgId=error";
             }

             response.sendRedirect(redirectURL);
         }

                // IF ENTRY FOUND IN "tbl_EvalSentQueToCp" TABLE IN CASE OF "tec" MEMBER.
                // GET THE TEC-MEMBER COUNT.
                List<SPTenderCommonData> isTEC = tenderCS.returndata("IsTEC", tenderId, sentBy);
                int isTECCnt = isTEC.size();

                // CHECK FOR USER-ROLL AND SET THE LINK AS PER THE ROLL.
                List<SPCommonSearchData> roleList = commonSearchService1.searchData("EvalMemberRole", tenderId, sentBy, null, null, null, null, null, null, null);

                if (!roleList.isEmpty()){
                    role = roleList.get(0).getFieldName2();
                }

            %>
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <form id="frmEvalClari" name="frmTECRepoort" action="ProcessTECReport.jsp?tenderId=<%=tenderId%>&st=<%=strSt%>" method="post">
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                 <%
                String referer = "";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");
                }
               %>
                <div class="pageHead_1">Tender Evaluation Committee Report
                <span style="float: right;" ><a href="TECReportListing.jsp" class="action-button-goback">Go Back</a></span>
                </div>
                <%
                        pageContext.setAttribute("tenderId", tenderId);

                        String tenderid = tenderId;
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>
                <%if(!"TEC".equalsIgnoreCase(request.getParameter("comType"))) { %>
                <jsp:include page="officerTabPanel.jsp" >
                     <jsp:param name="tab" value="7" />
                </jsp:include>
                <% } %>
                <div class="tabPanelArea_1">
                 <%if (request.getParameter("msgId") != null) {
                    String msgId = "", msgTxt = "";
                    boolean isError = false;
                    msgId = request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")) {
                        if (msgId.equalsIgnoreCase("senttoaa")) {
                            msgTxt = "Sent to AA successfully.";
                        } else if (msgId.equalsIgnoreCase("processed")) {
                            msgTxt = "TEC Report processed successfully.";
                        } else if (msgId.equalsIgnoreCase("error")) {
                            isError = true;
                            msgTxt = "There was some error.";
                        } else {
                             msgTxt = "";
                        }
                    %>
                    <%if (isError) {%>
                    <div class="responseMsg errorMsg"><%=msgTxt%></div>
                    <%} else if (!"".equalsIgnoreCase(msgTxt)) {%>
                    <div class="responseMsg successMsg"><%=msgTxt%></div>
                    <%}%>
                    <%}
                         }
                    %>

                <% List<SPTenderCommonData> companyList = tenderCS.returndata("getFinalSubComp",tenderId,"0"); %>

                      <%
                        if (!companyList.isEmpty()) {
                            //if ("cp".equalsIgnoreCase(role)) {
                                 if (!"cl".equalsIgnoreCase(request.getParameter("st"))) {
                                     List<SPTenderCommonData> evalMemStatusCount = tenderCS.returndata("GetEvalMemfinalStatusCount", tenderId, "0");
                            // IF THIS COUNT MATCHED WITH BIDDERS COUNT THEN SHOW BELOW BUTTON
                            if (Integer.parseInt(evalMemStatusCount.get(0).getFieldName1()) == companyList.size()) {
                      %>

                      <table width="100%" cellspacing="0" class="tableList_1  t_space">
                          <tr>
                              <th  width="4%" class="t-align-center">Sl. No.</th>
                              <th  width="96%" class="t-align-center">Report Name</th>
                          </tr>
                          <tr>
                              <td class="t-align-center">1</td>
                              <td>
                                  <a href="TenderersCompRpt.jsp?tenderid=<%=tenderId%>&st=<%=request.getParameter("st")%>">Technical Evaluation Report</a>
                              </td>
                          </tr>
                          <tr>
                              <td class="t-align-center">2</td>
                              <td>
                                  <a href="ViewL1Report.jsp?tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>">View L1 Report </a>
                              </td>
                          </tr>
                      </table>


                     <%
                    if(isSentToAA && !isProcessByAA){
                    %>
                     <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                          <tr>
                                <td style="font-style: italic" class="ff t-align-right" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>

                             <tr>
                                <td class="ff">Approving Authority : </td>
                                <td>
                                    <%=aaName%>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff">Action : <span class="mandatory">*</span></td>
                                <td>
                                    <select name="cmbAction" class="formTxtBox_1" id="cmbAction" width="100px;">
                                        <option value="">Select</option>
                                        <option value="Approve" selected="selected">Approve</option>
                                        <option value="Reject">Reject</option>
                                        <option value="Reevaluate">Reevaluate</option>
                                        <option value="Seek Clarification">Seek Clarification</option>
                                    </select>
                                    <span id="SpError1" class="reqF_1"></span>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff">Remarks : <span class="mandatory">*</span></td>
                                <td>
                                    <textarea cols="100" rows="5" id="txtComments" name="txtComments" class="formTxtBox_1"></textarea>
                                    <span id="SpError" class="reqF_1"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <div class="t-align-left">
                                        <label class="formBtn_1"><input name="btnSubmit" id="btnSubmit" type="submit" value="Submit" /></label>
                                    </div>

                                </td>
                            </tr>

                     </table>
                    <script type="text/javascript" >
            $(document).ready(function(){
                $('#btnSubmit').click(function(){
                    var flag=true;

                      if($('#cmbAction').val()==''){
                             $('#SpError1').html('Please select the Action.');
                             flag=false;
                        } else {
                            $('#SpError1').html('');
                        }


                     if($.trim($('#txtComments').val())==''){
                            $('#SpError').html('Please enter Remarks.');
                            flag=false;
                        }
                        else {
                            if($('#txtComments').length<=1000) {
                                $('#SpError').html('');
                            }
                            else{
                                $('#SpError').html('Maximum 1000 characters are allowed.');
                                flag=false;
                            }
                        }

                         if(flag==false){
                            return false;
                        }

                });

                $('#txtComments').blur(function(){
                    var flag=true;

                     if($.trim($('#txtComments').val())==''){
                            $('#SpError').html('Please enter Remarks.');
                            flag=false;
                        }
                        else {
                            if($('#txtComments').length<=1000) {
                                $('#SpError').html('');
                            }
                            else{
                                $('#SpError').html('Maximum 1000 characters are allowed.');
                                flag=false;
                            }
                        }

                         if(flag==false){
                            return false;
                        }
                });

            });
        </script>

                    <%}%>
                      <%
                                }
                            } // END IF "EVAL. REPORT" TAB
                        //} // END IF "CP" ROLE
                       } // END IF OF "NO DATA FOUND"
                     %>
                </div>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
        </form>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
