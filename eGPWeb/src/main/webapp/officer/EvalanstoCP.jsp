<%-- 
    Document   : EvalanstoCP
    Created on : Jan 6, 2011, 3:47:37 PM
    Author     : Administrator

This page is used when evaluation member posted ans to the query and cp want to view that and give note of decedent as per question answer and the status....
--%>

<%@page import="com.cptu.egp.eps.web.utility.EvaluationMethodology"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Post Clarification</title>
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
    <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
    <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
    <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    <%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
    <%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
    <%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
    <%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
    <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
    %>
<%--<script type="text/javascript">
    function showHide(value){
        var id = value.id;
       // alert(id+'1stId');
        var len = id.length;
    var index = id.indexOf('_');
    id = id.substr(index+1, len);
    //alert(id+"id");
    //alert(value.checked+"value.checked");
    //alert($('#remark_'+id+1).val())
    if(value.checked){
    //$('#remarkTr_'+id).show();
   // $('#remarkTd_'+id).show()
   // $('#labelTd_'+id).show()
    $('#tabel_'+id).show()
    
    }else{
        $('#tabel_'+id).hide()
    }
    
    //$('#updateValtd_'+i).hide()
    //$('#editLbltd_'+i).hide()
        
    }
</script>--%>
<script type="text/javascript">
    function validate(){
        $('.reqF_1').remove();
        var cnt = $('#counter').val();
        var i_cnt = 0;
        //alert(cnt);
        //alert(cnt+'isCP'+isCp);
            for(var i=1;i<=cnt;i++){
                //alert($('#ansByCp_'+i).val()+'Valueof remark');
                    if(!$('#ansByCp_'+i).val()=='' ){
                        //alert($('#ansByCp_'+i).val().length>2000);
                        if($('#ansByCp_'+i).val().length>2000){
                            $('#ansByCp_'+i).parent().append("<div class='reqF_1'>Allows maximum 2000 characters only</div>")
                            return false;
                        }
                     i_cnt++;
                    }
            }
        if(i_cnt==0){
             jAlert("Please enter Clarification.","Alert", function(RetVal) {
                });
            return false;
        }
    }
</script>
</head>
<body>
    <div class="dashboard_div">
        <!--Dashboard Header Start-->

        <div class="topHeader">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
        </div>
        <%
                    String tenderId = "0";
                    String formId = "0";
                    String uId = "0";
                    String st = "";
                    String strProcurementNature="";
                    boolean isMultiLots=false;
                    int intEnvelopcnt=0;
                    
                    TenderCommonService objTSC = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                    if (request.getParameter("tenderId") != null) {
                        tenderId = request.getParameter("tenderId");
                    }
                    if (request.getParameter("uId") != null) {
                        uId = request.getParameter("uId");
                    }
                    String userId = session.getAttribute("userId").toString();
                    if (request.getParameter("st") != null && !"".equalsIgnoreCase(request.getParameter("st"))) {
                        st = request.getParameter("st");
                    }
                    String s_lotId = null;
                    if (request.getParameter("lotId") != null && !"".equalsIgnoreCase(request.getParameter("lotId"))) {
                        s_lotId = request.getParameter("lotId");
                    }
                    String viewIs = "";
                    boolean isView = false;
                    if (request.getParameter("viewIs") != null && !"".equalsIgnoreCase(request.getParameter("viewIs"))) {
                        viewIs = request.getParameter("viewIs");
                        if(viewIs.equals("Yes")){
                                isView = true;
                            }
                    }
                    EvaluationMethodology evalMethod = new EvaluationMethodology();
                    List<String> listMethod = evalMethod.getType(tenderId); 
                    /* START : CODE TO GET TENDER ENVELOPE COUNT */
                        List<SPTenderCommonData> lstCmnEnvelops =
                                objTSC.returndata("getTenderEnvelopeCount", tenderId, "0");

                        if(!lstCmnEnvelops.isEmpty()){
                            if(Integer.parseInt(lstCmnEnvelops.get(0).getFieldName1()) > 0){
                                intEnvelopcnt=Integer.parseInt(lstCmnEnvelops.get(0).getFieldName1());
                            }
                            if ("Yes".equalsIgnoreCase(lstCmnEnvelops.get(0).getFieldName2())){
                                isMultiLots=true;
                            }

                            strProcurementNature=lstCmnEnvelops.get(0).getFieldName3();
                        }
                        lstCmnEnvelops=null;

                        
        %>
        <!--Dashboard Header End-->
        <!--Dashboard Content Part Start-->
        <div class="contentArea_1">
            <div class="pageHead_1">Post Clarification
                <span style="float:right;">
                    <a href="Evalclarify.jsp?tenderId=<%=tenderId%>&st=rp&comType=null" class="action-button-goback">Go back to Dashboard</a>
                </span>
            </div>
            <div class="t_space">
                <%
                CommonSearchService commonSearchService1 = (CommonSearchService)AppContext.getSpringBean("CommonSearchService");
                List<SPCommonSearchData> roleList = commonSearchService1.searchData("EvalMemberRole", tenderId, userId, null, null, null, null, null, null, null);
                String role ="";
                boolean isCp = false;
                if (!roleList.isEmpty()){
                    role = roleList.get(0).getFieldName2();
                    if(role.equalsIgnoreCase("cp")){
                            isCp = true;
                        }
                    roleList.clear();
                    roleList = null;
                }


                            if (request.getParameter("err") != null) {
                                String errMsg = request.getParameter("err");
                                    if (errMsg != null && errMsg.equals("Sucess")) {%>
                <div class="responseMsg successMsg" style="margin-top: 10px;">Question Posted <%=errMsg%>fully</div>
                <%} else if (errMsg != null) {%>
                <div class="responseMsg errorMsg" style="margin-top: 10px;">Problem While Posting Your Answer.</div>
                <%}
                            }%>
            </div>
            <%
                        pageContext.setAttribute("tenderId", tenderId);
                        int cntQuest = 0;
                        int cntStatus = 0;
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <% for (SPTenderCommonData companyList : tenderCommonService.returndata("GetCompanynameByUserid", uId, "0")) {
            %>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <th colspan="2" class="t_align_left ff">Company Details</th>
                </tr>
                <tr>
                    <td width="22%" class="t-align-left ff">Company Name :</td>
                    <td width="78%" class="t-align-left"><%=companyList.getFieldName1()%></td>
                </tr>
            </table>
            <%
                                    } // END FOR LOOP OF COMPANY NAME
            %>
            <form id="frmViewQueAns" name="frmEvalPost" action="<%=request.getContextPath()%>/ServletEvalanstoCP" method="POST">
                <input type="hidden" name="uId" value="<%=uId%>" />
                <input type="hidden" name="tenderId" value="<%=tenderId%>" />
                <input type="hidden" name="st" value="<%=st%>" />
                <% CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                            int formCnt = 0;
                            for (SPCommonSearchData formNames : commonSearchService.searchData("GetCPTenderForm", tenderId, userId,
                                    uId, role, s_lotId, null, null, null, null)) {
                                formId = formNames.getFieldName1();
                                formCnt++;
                %>
                <table width="100%" cellspacing="0" class="tableList_k t_space">
                    <tr>
                       <th> <b>Form Name : </b><a href="ViewEvalBidform.jsp?tenderId=<%=tenderId%>&uId=<%=uId%>&formId=<%=formNames.getFieldName1()%>&lotId=0&bidId=<%=formNames.getFieldName3()%>&action=Edit&isSeek=true" target="_blank"><%=formNames.getFieldName2()%></a>
                                <span class="c-alignment-right"><a href="EvalDocList.jsp?tenderId=<%=tenderId%>&formId=<%=formNames.getFieldName1()%>&uId=<%=userId%>" class="action-button-download">Download Documents</a></span>
                            </th>
                    </tr>
                    <%
                        if(!"cp".equalsIgnoreCase(role)){
                    %>
                    <tr>
                        <td>
                            &nbsp;&nbsp<a href="SeekEvalClari.jsp?uId=<%=uId%>&tenderId=<%=tenderId%>&st=<%=request.getParameter("st")%>&lnk=et">Evaluate Bidder/Consultant</a><br>
                        </td>
                    </tr>
                    <%}%>
                </table>
                <%

                                                int memCount = 0;
                                                String memId = "";
                    
                    for (SPCommonSearchData sptcd : commonSearchService.searchData("GetComMemforEvalPost", formId, role, userId, tenderId, null, null, null, null, null)) {
                                                    memCount++;
                                                    memId = sptcd.getFieldName3();
                                                    /*
                                                    for (SPCommonSearchData sptcd : commonSearchService.searchData("GetEvalComMem", tenderId, formId, null, null, null, null, null, null, null)) {
                                                    countMem++;*/
                %>

                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th colspan="3" class="t-align-left">TEC / TSC Member Name : <%=sptcd.getFieldName2()%>
                        </th>
                    </tr>
                    <tr>
                        <th width="4%" class="t-align-left">Sl. No.</th>
                        <th width="46%" class="t-align-left">Query</th>
                        <th width="45%" class="t-align-left">Clarification</th>
                    </tr>
                    <%
                    String cpAns = "";
                    String cpRemark = "";
                        for (SPCommonSearchData question : commonSearchService.searchData("GetQuesPostedByCPforMem", formId, uId, userId, role, sptcd.getFieldName3(), null, null, null, null)) {
                                                                            String evalCpId = question.getFieldName2();
                                                                            cntQuest++;
                            
                            
                    %>
                    <tr>
                        <td class="t-align-left"><%=cntQuest%>
                        <input type="hidden" name="evalCpId_<%=cntQuest%>" value="<%=evalCpId%>"/>
                        </td>
                        <td class="t-align-left" ><div class="break-word" style="width: 450px;"><%=question.getFieldName1()%></div></td>
                        <td class="t-align-left">
                        <%if(isCp){
                                        for (SPCommonSearchData clariCp : commonSearchService.searchData("SeekClarTECCP",formId, uId,null, null, null, null, null, null, null)) {
                                            if(!clariCp.getFieldName5().equalsIgnoreCase("")){
                                                cpAns = clariCp.getFieldName6();
                                                cpRemark = clariCp.getFieldName8();
                                                }
                                        }
                        %>
                        <div class="break-word"><%=question.getFieldName3()%></div>
                        
                        <%
                            }else{
                            if(formNames.getFieldName5().equals("Yes")){
                        %>
                        <textarea rows="5" id="ansByCp_<%=cntQuest%>" name="ansByCp_<%=cntQuest%>" class="formTxtBox_1" style="width:99%"><%=question.getFieldName3()%></textarea>
                        <%
                                }else{
                                    out.print("<div class='break-word'>"+question.getFieldName3()+"</div>");
                                }
                            }
                         %>
                        </td>
                    </tr>
                    <%--<table width="100%" cellspacing="0" class="tableList_1 t_space" id="tabel_<%=cntQuest%>" style="display: block;">
                        
                    </tr>
                    </table>--%>
              

                    <%

                    if(!"Services".equalsIgnoreCase(strProcurementNature)) {%>
                    <tr>
                        <td colspan="2"  class="ff">
                            Evaluation Status :
                        </td>
                        
                        <td>
                            <%
                            SPCommonSearchData compliedStatus = commonSearchService.searchData("getEvalMemberStatus", tenderId, uId, formId, memId, null, null, null, null, null).get(0);
                                                                                                            if(isCp){
                            %>
                            <label name="evalStatus_<%=cntQuest%>" id="evalStatus_<%=cntQuest%>" ><%=compliedStatus.getFieldName1()%></label>
                            <%
                                    }else{
                                        String is_Checked = null;
                                        for(int int_i=0;int_i<listMethod.size();int_i++){
                                    if (compliedStatus.getFieldName1().equalsIgnoreCase(listMethod.get(int_i))) {
                                        is_Checked = "checked";
                                    }else{
                                        is_Checked = "";
                                    }
                            %>
                            <input type="radio" name="evalStatus_<%=cntQuest%>" id="evalStatus_<%=cntQuest%>" value="<%=listMethod.get(int_i)%>" <%=is_Checked%>/>&nbsp; <%=listMethod.get(int_i)%>
                            <%}
                                    }%>
                                <%--} else {
                                    if(isCp){
                            %>
                            <label name="evalStatus_<%=cntQuest%>" id="evalStatus_<%=cntQuest%>" ><%=compliedStatus.getFieldName1()%></label>
                            <%
                                    } else{
                            %>
                            <input type="radio" name="evalStatus_<%=cntQuest%>" id="evalStatus_<%=cntQuest%>" value="Yes" checked="checked" />&nbsp; Complied
                            <input type="radio" name="evalStatus_<%=cntQuest%>" id="evalStatus_<%=cntQuest%>" value="No" />&nbsp;Not complied
                            <%
                                }
                            }
                            %>--%>
                            <input type="hidden" name="evalStatusId_<%=cntQuest%>" value="<%=compliedStatus.getFieldName2()%>" />
                            <%
                                                                                                        if (compliedStatus != null) {
                                                                                                            compliedStatus = null;
                                                                                                        }
                            %>
                        </td>                        
                    </tr>
                    <%}%>
                    <%
                                                                            if (question != null) {
                                                                                question = null;
                                                                            }
                                                                        }/*Quetion Loop Ends Here*/
                     if (cntQuest == 0) {
                    %>
                    <tr>
                        <td colspan="4" class="t-align-center reqF_1 ff">No Record Found</td>
                    </tr>
                    <%
                        }
                    %>

                </table>

                <%
                                                    if (sptcd != null) {
                                                        sptcd = null;
                                                    }
                                                }//for table Member Name
                if (memCount == 0) {
                %>
                <div class="tabPanelArea_1 t-align-center reqF_1 ff"  >
                    No Record Found
                </div>
                <%}
                                if (formNames != null) {
                                    formNames = null;
                                }
                            }//Form Name ends
                            if (formCnt == 0) {
                %>
                <div class="tabPanelArea_1 t-align-center">
                    No Record Found
                </div>
                <%}
                            if (commonSearchService != null) {
                                commonSearchService = null;
                                            }
                if (cntQuest != 0 && !isCp) {
                %>
                <div class="t-align-center t_space">
                    <label class="formBtn_1">
                        <input type="hidden" id="counter" name="counter" value="<%=cntQuest%>"/>
                        <input type="hidden" id="cntStatus" name="cntStatus" value="<%=cntStatus%>"/>
                        <input name="btnPost" id="btnPost" type="submit" value="Submit" onclick="return validate();"/></label>
                </div>
                <%
                }//For Button Hide Condition.
                %>

            </form>

        </div>
        <div>&nbsp;</div>
        <!--Dashboard Content Part End-->
        <!--Dashboard Footer Start-->
        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        <!--Dashboard Footer End-->
    </div>
</body>
<script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
