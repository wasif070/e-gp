<%--
    Document   : WinningBidderHistory
    Created on : Apr 19, 2015, 1:55:24 PM
    Author     : Istiak (Dohatec)
--%>

<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.AppMISService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>
<jsp:useBean id="comDocSrBean" class="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"  scope="page"/>
<%@page import="com.cptu.egp.eps.model.table.TblTenderForms"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderSection"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.EvalCommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Previous Winning Bidder History</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <%
                    String tenderId = request.getParameter("tenderId");
                    String pkgLotId = "0";
                    if(!"".equalsIgnoreCase(request.getParameter("pkgLotId")) && request.getParameter("pkgLotId") != null)
                    {
                        pkgLotId = request.getParameter("pkgLotId");
                    }
                    String userId = request.getParameter("uId");

                    String flag = "gw"; // Goods - Works
                    if("s".equalsIgnoreCase(request.getParameter("flag")))
                    {
                        flag = "s"; //  Service
                    }

                    AppMISService mISService = (AppMISService) AppContext.getSpringBean("AppMISService");
                    String strUserTypeId = session.getAttribute("userTypeId").toString();
                    Object objUserId = session.getAttribute("userId");
                    if (objUserId != null) {
                        strUserTypeId = session.getAttribute("userTypeId").toString();
                    }
        %>
        <script type="text/javascript">

            function chkdisble(pageNo){

                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }
                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }

            function checkKey(e)
            {
                var keyValue = (window.event)? e.keyCode : e.which;
                if(keyValue == 13){
                    $('#pageNo').val('1')
                    $('#btnSearch').click();
                }
            }
            function checkKeyGoTo(e)
            {
                var keyValue = (window.event)? e.keyCode : e.which;
                if(keyValue == 13){
                    $(function() {
                        var pageNo=parseInt($('#dispPage').val(),10);
                        var totalPages=parseInt($('#totalPages').val(),10);
                        if(pageNo > 0)
                        {
                            if(pageNo <= totalPages) {
                                $('#pageNo').val(Number(pageNo));
                                $('#btnGoto').click();
                                $('#dispPage').val(Number(pageNo));
                            }
                        }
                    });
                }
            }

            function loadOffice() {
                var deptId=$('#txtdepartmentid').val();
                $.post("<%=request.getContextPath()%>/ComboServlet", {departmentId: deptId, funName:'officeCombo'},  function(j){
                    $('#cmbOffice').children().remove().end()
                    $("select#cmbOffice").html(j);
                });
            }

            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        $('#dispPage').val(Number(pageNo)+1);
                        loadTable();
                    }
                });
            });

            $(function() {
                $('#btnLast').click(function() {
                    var deptId=document.getElementById('txtdepartmentid').value
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        $('#dispPage').val(totalPages);
                        loadTable();
                    }
                });
            });

            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        $('#dispPage').val(Number(pageNo) - 1);
                        loadTable();
                    }
                });
            });

            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        $('#dispPage').val("1");
                        loadTable();
                    }
                });
            });

            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));

                            loadTable();
                        }
                    }
                });
            });

            function loadTable()
            {
                $("#progressBar").show();
                $("#progressBar").css("visibility","visible");

                var departmentId = $('#txtdepartmentid').val();
                var procEntity = $('#cmbOffice').val();        
                var procNature = $('#procNature').val();       
                var procMethod = $('#cmbProcMethod').val();    
                var procType = $('#cmbType').val();            
                var oldTenderId = $('#oldTenderId').val();
                var size = $('#size').val();
                var pageNo = $('#dispPage').val();

                $.post("<%=request.getContextPath()%>/EvalCommonServlet", {funName: "PreviousAllTenders", userId: <%=userId%>, pkgLotId: <%=pkgLotId%>, tenderId: <%=tenderId%>, size: size, pageNo: pageNo, departmentId: departmentId, procEntity: procEntity, procNature: procNature, procMethod: procMethod, procType: procType, oldTenderId: oldTenderId, flag: "<%=flag%>"},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    $("#progressBar").hide();
                    sortTable();
                    if($('#noRecordFound').attr("value") == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageTot").html($("#totalPages").val());
                    $("#pageNoTot").html($("#pageNo").val());
                    $('#resultDiv').show();

                    var counter = $('#cntTenBrief').val();
                    for(var i=0;i<counter;i++)
                    {
                        try
                        {
                            var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                            var temp1 = $('#tenderBrief_'+i).html();
                            if(temp.length > 250){
                                temp = temp1.substr(0, 250);
                                $('#tenderBrief_'+i).html(temp+'...');
                            }
                        }
                        catch(e){}
                    }
                });
            }

            $(document).ready(function(){

                loadTable();

                $('#btnSearch').click(function(){
                    loadTable();
                });

                $('#btnReset').click(function(){
                    $('#pageNo').val("1");
                    $('#dispPage').val("1");
                    $('#txtdepartment').val("");
                    $('#txtdepartmentid').val("");
                    $('#cmbOffice').val("0");
                    $('#procNature').val("0");
                    $('#cmbProcMethod').val("0");
                    $('#cmbType').val("0");
                    $('#tenderId').val("");
                    $('#oldTenderId').val("");

                    loadTable();
                });
            });

        </script>
    </head>

    <body>

        <div class="dashboard_div">
            <!--Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Header End-->

            <div class="contentArea_1">
                <div class="pageHead_1">Previous Winning Bidder History
                    <span class="c-alignment-right">
                        <%
                            if(flag.equalsIgnoreCase("gw")){
                        %>
                                <a href="ProcessPQ.jsp?tenderId=<%=tenderId%>&pkgLotId=<%=pkgLotId%>" class="action-button-goback">Go back</a>
                        <%
                            }else{
                        %>
                                <a href="NegotiationProcess.jsp?tenderId=<%=tenderId%>&comType=<%=request.getParameter("comType")%>" class="action-button-goback">Go back</a>
                        <%  }%>
                    </span>
                </div>
                <%
                            pageContext.setAttribute("tenderId", tenderId);
                %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <div>&nbsp;</div>


                    <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>">
                    <input type="hidden" name="pkgId" id="pkgId" value="<%=pkgLotId%>">
                    <input type="hidden" name="uId" id="uId" value="<%=userId%>">




                        <div class="tabPanelArea_1">
                            <div class="tableList_1" align="center">

                    <%
                    int comTenderStdId = comDocSrBean.getTenderSTDId(Integer.parseInt(tenderId));
                    List<TblTenderSection> tSections = comDocSrBean.getTenderSections(comTenderStdId);

                    CommonService comcommonService = (CommonService)AppContext.getSpringBean("CommonService");
                    String comEventType = comcommonService.getEventType(tenderId).toString();
                    String comProcNature = comcommonService.getProcNature(tenderId).toString();
                    String comProcMethod = comcommonService.getProcMethod(tenderId).toString();
                    boolean isTenderPkgWise = false;

                    int comTemplateId = 0;
                    int comDisp = 0;
                    int sectionId = 0;
                    String str_FolderName = "";
                    short disp = 0;
                    for (TblTenderSection tts : tSections){
                        disp++;

                        if("Form".equalsIgnoreCase(tts.getContentType())){
                    %>

                    <table class="tableList_1" width="100%" cellspacing="0">
                        <tbody>
                            <tr>
                                <td class="ff" colspan="5"> <%=tts.getSectionName()%> </td>
                            </tr>
                        </tbody>
                    </table>
                    <br/>

                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th style="text-align:center; width:4%;">Sl. No.</th>
                            <th width="33%">Form Name </th>
                            <th width="63%">Map Document List</th>

                        </tr>
                    <%
                       List<TblTenderForms> forms = comDocSrBean.getTenderForm(tts.getTenderSectionId());
                       int count=0;
                       List<Object[]> comDoc = comDocSrBean.commonDocMapList(tenderId, userId);
                       List<Object[]> winDoc = comDocSrBean.mappedWinningDocMapList(tenderId, userId);
                       for(int i=0;i<forms.size();i++){
                           if(!("c".equalsIgnoreCase(forms.get(i).getFormStatus())|| "createp".equalsIgnoreCase(forms.get(i).getFormStatus()))){
                           count++;
                    %>
                        <tr>
                            <td><%=count%></td>
                            <td><%out.print(forms.get(i).getFormName().replace("?s", "'s"));%></td>
                        <%
                            boolean isMapped = false;
                                if(comDoc!=null && !comDoc.isEmpty()){
                                    for(int j=0;j<comDoc.size();j++){
                                        if(forms.get(i).getTenderFormId()==Integer.parseInt(comDoc.get(j)[0].toString())){
                                            isMapped = true;
                                        }
                                    }
                                }if(isMapped){
                        %>

                            <td>
                        <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                            <th width="85%">Document Name </th>
                            <th width="10%" class="t-align-center">Action</th>
                            <th width="10%" class="t-align-center">Document<br/>History</th>
                            </tr>
                            <%for(int j=0;j<comDoc.size();j++){

                                        if(forms.get(i).getTenderFormId()==Integer.parseInt(comDoc.get(j)[0].toString())){
                            %>
                                            <tr>
                                                <td><%=comDoc.get(j)[1]%></td>
                                                <td class="t-align-center"><a view='link' href="<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=download&fileName=<%=comDoc.get(j)[1]%>&fileLen=<%=comDoc.get(j)[2]%>&uIdDoc=<%=userId%>&docUid=<%=userId%>" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a></td>
                                                <td><%
                                                for(int k=0;k<winDoc.size();k++){
                                                    //System.out.println(" winDoc.get(k)[0].toString() " + winDoc.get(k));
                                                    Object result =  winDoc.get(k);
                                                    Integer intResult = (Integer) result;
                                                    if(intResult==Integer.parseInt(comDoc.get(j)[3].toString())){
                                                        if(flag.equalsIgnoreCase("s")){%>
                                                        <a href="ViewWinningMappedDocuments.jsp?tenderId=<%=tenderId%>&pkgLotId=<%=pkgLotId%>&uId=<%=userId%>&docId=<%=String.valueOf(intResult)%>&flag=<%=flag%>">View</a>
                                                        <%} else {%>
                                                        <a href="ViewWinningMappedDocuments.jsp?tenderId=<%=tenderId%>&pkgLotId=<%=pkgLotId%>&uId=<%=userId%>&docId=<%=String.valueOf(intResult)%>">View</a>
                                                        <%}
                                                    }
                                                }
                                                %></td>
                                            </tr>
                                        <%}
                            }%>

                        </table>
                            </td></tr>
                        <%}/*Mapped Doc List*/else{%>
                        <td>-</td></tr>
                        <%}%>
                    <%}/*Cancled form is not displayed*/}/*Form loop Ends here*/%>
                    </table><br/>
                    <%}/*Form Condition Ends here*/%>

                    <%}/*For loop ends of tenderSection*/%>
                            </div>
                            
                            <div align="center">
                                <input type="hidden" id="pageNo" value="1"/>
                            </div>
                        </div>
                 

                    
            </div>
            <!--Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Footer End-->
        </div>
    </body>

</html>

