<%-- 
    Document   : CommonPackageLotDescription
    Created on : Mar 9, 2011, 12:37:27 PM
    Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>        
        <%
        String cmnTenderId = "0", cmnPkgLotId="0";
        

        if (request.getParameter("tenderId") != null) {
            cmnTenderId = request.getParameter("tenderId");
        }
        if (request.getParameter("lotId") != null) {
            if (request.getParameter("lotId") != "" && !request.getParameter("lotId").equalsIgnoreCase("null")) {
                cmnPkgLotId = request.getParameter("lotId");
            }
        }
        boolean b_isPDF = false;
        if(request.getParameter("isPDF")!=null){
            b_isPDF = true;
        }

        TenderCommonService objCommonTCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        if (session.getAttribute("userId") != null) {
                objCommonTCS.setLogUserId(session.getAttribute("userId").toString());
        }
        
        CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
        String pnature = cs.getProcNature(request.getParameter("tenderId")).toString();
        List<SPTenderCommonData> objPackageList = objCommonTCS.returndata("getlotorpackagebytenderid", cmnTenderId, "Package,1");
                     if (!objPackageList.isEmpty()) {
                %>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td width="<%
                            if(b_isPDF){
                                out.print("25%");
                            }else{
                                out.print("20%");
                            }
                            %>" class="t-align-left ff">Package No. :</td>
                            <td class="t-align-left"><%=objPackageList.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=objPackageList.get(0).getFieldName2()%></td>
                        </tr>
                        <%if(request.getAttribute("tenderDocSold")!=null){
                            String DocumentSoldFlag = " No ";
                            int DocumentSoldNumb = Integer.valueOf((request.getAttribute("tenderDocSold").toString()));
                            if(DocumentSoldNumb>0){ DocumentSoldFlag = " Yes "; }
                            %>
                        <tr>
                            <td class="t-align-left ff">Tender Document Sold :</td>
                            <td class="t-align-left"><%=DocumentSoldFlag%></td>
                        </tr>
                        <%}%>
                    </table>
                <%}%>

     <%
        List<SPTenderCommonData> objLotList = objCommonTCS.returndata("getlotorpackagebytenderid", cmnTenderId, "Lot," + cmnPkgLotId);
        if(!"services".equalsIgnoreCase(pnature)){
        if (!objLotList.isEmpty()) {%>
        <table width="100%" cellspacing="0" class="tableList_1 t_space">
            <tr>
                <td width="<%if(b_isPDF){
                                out.print("25%");
                            }else{
                                out.print("20%");
                            }%>" class="t-align-left ff">Lot No. :</td>
                <td><%=objLotList.get(0).getFieldName1()%></td>
            </tr>
            <tr>
                <td class="t-align-left ff">Lot Description :</td>
                <td><%=objLotList.get(0).getFieldName2()%></td>
            </tr>
            <%if(request.getAttribute("tenderDocSold")!=null){
                String DocumentSoldFlag = " No ";
                int DocumentSoldNumb = Integer.valueOf((request.getAttribute("tenderDocSold").toString()));
                 if(DocumentSoldNumb>0){ DocumentSoldFlag = " Yes "; }
                %>
                <tr>
                    <td class="t-align-left ff">Tender Document Sold :</td>
                    <td class="t-align-left"><%=DocumentSoldFlag%></td>
                </tr>
            <%}%>
        </table>
        <%}}
    %>
    </body>
</html>
