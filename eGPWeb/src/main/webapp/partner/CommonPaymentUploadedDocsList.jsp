
<%--
    Document   : CommonPaymentUploadedDocsList
    Created on : May 25, 2015, 11:00:00 AM
    Author     : Wahid
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>



    <script type="text/javascript" >
   
    $(document).ready(function(){        
        // $('#tblPaymentDocs').show();
    });
    </script>
<%
TenderCommonService objCommonDocTCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            if (session.getAttribute("userId") != null) {
                objCommonDocTCS.setLogUserId(session.getAttribute("userId").toString());
            }
            String cmnPaymentId = "0", docTenderId="0", docPayTyp="", docUserId="0", strPDFUrl="", pkglotId="";
            boolean docIsBranchMaker=false, docIsBranchChecker=false, docIsBankChecker=false;
            String docUserBranchId="";

             HttpSession docHS = request.getSession();
            if (docHS.getAttribute("userId") != null) {
                     docUserId = docHS.getAttribute("userId").toString();
              }

            if (request.getParameter("payId") != null) {
                cmnPaymentId = request.getParameter("payId");
            }

            if (request.getParameter("tenderId") != null) {
                docTenderId = request.getParameter("tenderId");
            }

            if (request.getParameter("payTyp") != null) {
                docPayTyp = request.getParameter("payTyp");
            }

            if (request.getParameter("pdfUrl") != null) {
                strPDFUrl = request.getParameter("pdfUrl");
            }

            if (request.getParameter("lotId") != null) {
                pkglotId = request.getParameter("lotId");
            }


             List<SPTenderCommonData> docCurBankUserRole = objCommonDocTCS.returndata("getBankUserRole", docUserId, null);
             if (!docCurBankUserRole.isEmpty()) {
                if ("BranchMaker".equalsIgnoreCase(docCurBankUserRole.get(0).getFieldName1())) {
                    docIsBranchMaker = true;
                } else if ("BranchChecker".equalsIgnoreCase(docCurBankUserRole.get(0).getFieldName1())) {
                    docIsBranchChecker = true;
                } else if ("BankChecker".equalsIgnoreCase(docCurBankUserRole.get(0).getFieldName1())) {
                    docIsBankChecker = true;
                }
                docUserBranchId = docCurBankUserRole.get(0).getFieldName2();
             }

%>


<table width="100%" cellspacing="0" >
    <tr>
        <td>
            <div style="margin-top: 12px;">
                <span style="float: right;">
                    <a id="anchPDF" class="action-button-savepdf" style="margin-right: 3px; display: none;"
                    href="<%=strPDFUrl%>" >Save As PDF </a>
                    <a href="javascript:void(0);" id="print" style="display: none;" class="action-button-view">Print</a>&nbsp;
                </span>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div style="margin-top: 12px;">           
                <%if(docIsBranchChecker){%>
                   <table width="100%" cellspacing="0" class="tableList_1 t_space">
                       <tr class="docsUploadMendatory">
                                <td class="docUploadMandatoryMessage">
                                    <label>Branch Maker do not upload any documents. So you have to upload reference document relevant to this performance security payment. </label>
                                </td>
                       </tr>
                   </table>
                <%}%>
            </div>
        </td>

    </tr>
    <tr>
        <td>
            <%--START CODE TO GET DOCS UPLOADED LIST--%>
           
<%--END CODE TO GET DOCS UPLOADED LIST--%>
        </td>
    </tr>
</table>

        <div id="tblPaymentUploadDocs">
              <table width="100%" cellspacing="0" class="tableList_1 t_space">
 <tr>
     <th width="4%" class="t-align-center">Sl. No.</th>
     <th class="t-align-center" width="23%">File Name</th>
     <th class="t-align-center" width="32%">File Description</th>
     <th class="t-align-center" width="7%">File Size <br />(in KB)</th>
     <th class="t-align-center" width="18%">Action</th>
 </tr>
<%

int docCnt = 0;

//TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

//for (SPTenderCommonData sptcd : objCommonDocTCS.returndata("TenderPaymentDocs", cmnPaymentId, "")) {
for (SPTenderCommonData sptcd : objCommonDocTCS.returndata("TenderPaymentDocsForView", cmnPaymentId, docUserId)) {
  docCnt++;
%>
<tr
 <%if(Math.IEEEremainder(docCnt,2)==0) {%>
                    class="bgColor-Green"
                <%} else {%>
                    class="bgColor-white"
                <%}%>
>
<td class="t-align-center"><%=docCnt%></td>
<td class="t-align-left"><%=sptcd.getFieldName1()%></td>
<td class="t-align-left"><%=sptcd.getFieldName2()%></td>
<td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()))/1024%>
</td>
 <td class="t-align-center">
     <a href="<%=request.getContextPath()%>/TenderPaymentDocUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderPaymentId=<%=cmnPaymentId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
</td>
</tr>

  <%   if(sptcd!=null){
sptcd = null;
}
}%>
<input type="hidden" name="manUpload" id="manUpload" value="<%=docCnt%>" >
<% if (docCnt == 0) {%>
<tr>
<td colspan="5" class="t-align-center">No docs found.</td>
</tr>
<%}%>
</table>

           </div>
