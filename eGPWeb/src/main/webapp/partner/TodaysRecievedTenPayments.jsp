<%-- 
    Document   : TodaysRecievedTenPayments
    Created on : Apr 6, 2011, 2:23:58 PM
    Author     : Karan
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Received Tender Payments Daily Transactions</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />

       <script type="text/javascript">
            function fillGridOnEvent(userId){
                
               // alert(userId);
                
                $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                jQuery("#list").jqGrid({
                    url:'<%=request.getContextPath()%>/VerifyTenderPaymentServlet?q=1&action=fetchReceivedDailyData&userId=' + userId,
                    datatype: "xml",
                    height: 250,
                    colNames:['Sl. No.','Tender ID','e-mail ID','Bidder / Consultant','Payment For',"Amount","Payment Date and Time",'Branch Maker',"Action"],
                    colModel:[
                        {name:'RowNumber',index:'RowNumber', width:40,sortable:false, align: 'center'},
                        {name:'tenderId',index:'tenderId', width:100,sortable:true, align: 'center'},
                        {name:'emailId',index:'emailId', width:250,sortable:true},
                        {name:'dbo.f_getbiddercompany(LM.userId)',index:'dbo.f_getbiddercompany(LM.userId)', width:250,sortable:true},
                        {name:'paymentFor',index:'paymentFor', width:150,sortable:true, align: 'center'},
                        {name:'amount',index:'amount', width:100,sortable:true, align: 'right'},
                        {name:'createdDate',index:'createdDate', width:140,sortable:true, align: 'center'},
                        {name:'PA.fullName',index:'PA.fullName', width:200,sortable:true},
                        {name:'action',index:'action', width:100,sortable:false, align: 'center'}

                    ],
                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    sortable:false,
                    caption: "",
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{edit:false,add:false,del:false, search:false});
            }
            jQuery().ready(function (){
                //fillGrid();
            });
        </script>

</head>


    <%
    TenderCommonService objTSCDaily = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    if (session.getAttribute("userId") != null) {
                objTSCDaily.setLogUserId(session.getAttribute("userId").toString());
        }
     boolean bolIsBranchMaker=false, bolIsBranchChecker=false, bolIsBankChecker=false;
     String strUserBranchId="";
    String strUserId = "";
                HttpSession objSession = request.getSession();
                if (objSession.getAttribute("userId") != null) {
                    strUserId = objSession.getAttribute("userId").toString();
                } else {
                    //response.sendRedirectFilter(request.getContextPath() + "/SessionTimedOut.jsp");
                    response.sendRedirect("SessionTimedOut.jsp");
                }

                List<SPTenderCommonData> objLstCurBankUserRole = objTSCDaily.returndata("getBankUserRole",strUserId,null);
                if(!objLstCurBankUserRole.isEmpty()){
                    if("BranchMaker".equalsIgnoreCase(objLstCurBankUserRole.get(0).getFieldName1())){
                        bolIsBranchMaker=true;
                    } else if("BranchChecker".equalsIgnoreCase(objLstCurBankUserRole.get(0).getFieldName1())){
                        bolIsBranchChecker=true;
                    } else if("BankChecker".equalsIgnoreCase(objLstCurBankUserRole.get(0).getFieldName1())){
                        bolIsBankChecker=true;
                    }
                    strUserBranchId=objLstCurBankUserRole.get(0).getFieldName2();
                }
    %>
    <body onload="fillGridOnEvent('<%=strUserId%>');">
        <div class="dashboard_div">
         <!--Dashboard Header Start-->
      <%@include  file="../resources/common/AfterLoginTop.jsp" %>
      <!--Dashboard Header End-->

      <!--Dashboard Content Part Start-->
      <div class="contentArea_1">
          <div class="pageHead_1">
              Received Tender/Proposal Payments Daily Transactions
              <span style="float: right;" ><a href="SearchTenPayment.jsp" class="action-button-goback">Go Back</a></span>
          </div>

          <form id="frmData" method="post">

          </form>

          <div class="tabPanelArea_1 t_space">
              <div id="jQGrid" align="center">
                  <table id="list"></table>
                  <div id="page"></div>
              </div>
          </div>
      </div>
  <!--Dashboard Content Part End-->

    <!--Dashboard Footer Start-->
 <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
  <!--Dashboard Footer End-->
</div>
</body>

<script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
