<%--
    Document   : TenPaymentListing
    Created on : Mar 4, 2011, 4:09:28 PM
    Author     : Karan
--%>

<%@page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Tender Payment Report</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
      
       <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
       

        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />

        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>

        <script type="text/javascript">
            /* Call Print function */

            $(document).ready(function() {
                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });
            function printElem(options){
                $('#titleDiv').show();
                $('#print_div').printElement(options);
                $('#titleDiv').hide();
            }
        </script>
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>

        <script type="text/javascript">
            function fillGridOnEvent(userId, isVerified, branchId, branchMakerId, fromDt, toDt, tenderId, paymentFor){
                /*
                alert(userId);
                alert(emailId);
                alert(isVerified);
                alert(branchId);
                alert(branchMakerId);
                alert(fromDt);
                alert(toDt);
                */

                $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                jQuery("#list").jqGrid({
                    url:'<%=request.getContextPath()%>/VerifyTenderPaymentServlet?q=1&action=fetchDataForReport&userId=' + userId + '&isVerified=' + isVerified + '&branchId=' + branchId + '&branchMakerId= '+""+' &fromDt=' + fromDt + '&toDt=' + toDt + '&tenderId=' + tenderId + '&paymentFor=' + paymentFor,
                    datatype: "xml",
                    colNames:['Sl. No.','Tender ID,Lot No.','Status','Branch Name','Branch Maker','Payment For',"Payment Date and Time","Amount(Nu.)","Amount(USD)"],
                    colModel:[
                        {name:'RowNumber',index:'RowNumber',width:40, sortable:true, align: 'center'},
                        {name:'tenderId',index:'tenderId',width:70,sortable:true, align: 'center'},
                        {name:'status',index:'status',width:60, align:'center'},
                        {name:'branchNname',index:'branchName',width:120, sortable:true},
                         {name:'PA.fullName',index:'PA.fullName',width:120,sortable:true},
                        {name:'paymentFor',index:'paymentFor',width:120,sortable:true, align: 'center'},
                        {name:'createdDate',index:'createdDate',width:100,sortable:true, align: 'center'},
                        {name:'amount',index:'amount',width:80, sortable:true, align: 'right'},
                        {name:'Amount(Others)',index:'Amount(Others)',width:80, sortable:true, align: 'right'}
                       // {name:'action',index:'action', width:140,sortable:false, align: 'center'}

                    ],
                    height: 'auto',
                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:20,
                    rowList:[20,50,100,1000],
                    pager: $("#page"),
                   // gridview:true,
                    
                   // sortable:true,
                    caption: "",
                    //footerrow : true, userDataOnFooter : true,
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                    //var grid = $("#list");
                   // var sum = grid.jqGrid('getCol', 'amount', false, 'sum');
                   // grid.jqGrid('footerData','set', { paymentFor:'Total',amount: sum});
                }
                }).navGrid('#page',{edit:false,add:false,del:false,search:false});
            }
            
            jQuery().ready(function (){
                //fillGrid();
            });
        </script>

        <script type="text/javascript">
function checkKey(e)
{
    var keyValue = (window.event)? e.keyCode : e.which;
    if(keyValue == 13){
    //Validate();
    $('#pageNo').val('1')
    $('#btnSearch').click();
    //loadTable();
    }
}
                        $(function() {
                           /* $('#comboBranches').change(function() {
                                if( $('#comboBranches').val()!=''){
                                    branchId = $('#comboBranches').val();
                                    $.post("<%=request.getContextPath()%>/VerifyPaymentServlet", {branchId: branchId, action: 'getBranchMembers'},  function(j){
                                        //alert(j);
                                        $("select#comboBranchMembers").html(j);
                                    });
                                }
                            });*/

                            $('#btnSearch').click(function() {
                               // alert('You clicked Search');
                                 var errflag = false;

                                $('#SpError1').html('');

                                var userId='', emailId='', isVerified='', branchId='', branchMakerId='', fromDt='', toDt='', tenderId='', tendererNm='', paymentFor='';
                                userId=$('#userId').val();
                              //  emailId=$('#txtEmailId').val();
                                isVerified=$('#comboStatus').val();


                                fromDt=$('#txtFromDt').val();
                                toDt=$('#txtToDt').val();

                                tenderId=$('#txtTenderId').val();
                              //  tendererNm=$('#txtTendererNm').val();
                                paymentFor=$('#comboPaymentFor').val();


                            if(document.getElementById("comboBranches")!=null){
                                if($('#comboBranches').val()!=''){
                                    branchId=$('#comboBranches').val();
                                }
                            }

                             if($('#comboBranchMembers').val()!=''){
                                    branchMakerId=$('#comboBranchMembers').val();
                                    //branchMakerId='';
                             }

                              if(fromDt!='' && toDt!=''){
                                //alert('Hi');
                                 var d =new Date();
                            var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());

                            var value=fromDt;
                            var mdy = value.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);

                             var value2=toDt;
                            var mdy2 = value2.split('/')  //Date and month split
                            var mdyhr2= mdy2[2].split(' ');  //Year and time split
                            var valuedate2= new Date(mdyhr2[0], mdy2[1]-1, mdy2[0]);

                            if(Date.parse(valuedate) > Date.parse(valuedate2)){
                                   errflag = true;
                                    $('#SpError1').html('Payment Date From must be less than or equal to Payment Date To');
                                }
                            }

                             if(!errflag){
                             fillGridOnEvent(userId, isVerified, branchId,branchMakerId, fromDt, toDt, tenderId, paymentFor);
                             }
                            });

                            $('#btnReset').click(function() {
                                //alert('Clearing...')

                                  // Clear Err Messages
                                $('#SpError1').html('');
                                //==========================

                               var userId=$('#userId').val();

                              //$('#txtEmailId').val('');
                                $('#comboStatus').val('no');
                                $('#txtFromDt').val('');
                                $('#txtToDt').val('');

                                $('#txtTenderId').val('');
                             //   $('#txtTendererNm').val('');
                                $('#comboPaymentFor').val('All');

                                if(document.getElementById("comboBranches")!=null){
                                    $('#comboBranches').val('');
                                }

                                 // if(document.getElementById("comboBranchMembers")!=null){
                               //      $('#comboBranchMembers').val('');
                              //  }

                               // var j="<option value=''>- Select Branch Maker -</option>";
                               // $("select#comboBranchMembers").html(j);

                                fillGridOnEvent(userId,'no','','','','','','All');
                            });


                        });
                    </script>

</head>


    <%
    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    if (session.getAttribute("userId") != null) {
                tenderCommonService.setLogUserId(session.getAttribute("userId").toString());
            }
     boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false;
     String userBranchId="";
    String userId = "";
                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") != null) {
                    userId = hs.getAttribute("userId").toString();
                } else {
                    //response.sendRedirectFILTER(request.getContextPath() + "/SessionTimedOut.jsp");
                }

              /*  List<SPTenderCommonData> lstCurBankUserRole = tenderCommonService.returndata("getBankUserRole",userId,null);
                if(!lstCurBankUserRole.isEmpty()){
                    if("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBranchMaker=true;
                    } else if("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBranchChecker=true;
                    } else if("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBankChecker=true;
                    }
                    userBranchId=lstCurBankUserRole.get(0).getFieldName2();
                }*/
    %>
    <body onload="fillGridOnEvent('<%=userId%>','no','','','','','','All');">
    <div class="dashboard_div">
      <!--Dashboard Header Start-->
      <%@include  file="../resources/common/AfterLoginTop.jsp" %>
      <!--Dashboard Header End-->
      <!--Dashboard Content Part Start-->
      <div class="contentArea_1">
        <div class="pageHead_1">Tender Payment Report
        <span style="float: right;">
            <a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('9');">Save as PDF</a>&nbsp;
            <!-- <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;-->
        </span>
        </div>
      <div>&nbsp;</div>

      <div class="tabPanelArea_1 t_space">

          <form id="frmData" method="post">
        <div class="formBg_1 t_space">
            <input type="hidden" name="userId" id="userId" value="<%=userId%>"></input>
            <table cellspacing="10" class="formStyle_1" width="100%">
                <tr>
                    <td width="20%" class="ff">Tender ID :</td>
                    <td width="30%"><input name="txtTenderId" type="text" class="formTxtBox_1" onkeypress="checkKey(event);" id="txtTenderId" style="width:195px;" /></td>
                    <td width="13%" class="ff">Verification Status :</td>
                    <td width="29%">
                          <select name="comboStatus" class="formTxtBox_1" id="comboStatus" style="width:75px;">
                              <option value='yes' >Verified</option>
                              <option value='no' selected="selected">Pending</option>
                          </select>
                    </td>
                </tr>
           <!-- <tr>
              <td width="14%" class="ff">e-mail ID :</td>
              <td width="44%"><input name="txtEmailId" type="text" class="formTxtBox_1" id="txtEmailId" onkeypress="checkKey(event);" style="width:195px;" /></td>
              
              </tr>-->
            <tr>
              <td class="ff">Payment Date From : </td>
              <td>
                  <input onfocus="GetCal('txtFromDt','txtFromDt');" name="txtFromDt" type="text" class="formTxtBox_1" id="txtFromDt" style="width:100px;" readonly="readonly" />
                  <img id="imgFromDt" name="imgFromDt" onclick="GetCal('txtFromDt','imgFromDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                  <span id="SpError1" class="reqF_1"></span>
              </td>
              <td class="ff">Payment Date To : </td>
              <td>
               <input onfocus="GetCal('txtToDt','txtToDt');" name="txtToDt" type="text" class="formTxtBox_1" id="txtToDt" style="width:100px;" readonly="readonly" />
                  <img id="imgToDt" name="imgToDt" onclick="GetCal('txtToDt','imgToDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                  <span id="SpError" class="reqF_1"></span>
              </td>
              </tr>
            <tr>

                    <td class="ff">Branch  Name : </td>
                    <td><select name="comboBranches" class="formTxtBox_1" id="comboBranches" style="width:155px;">
                            <option value='' selected="selected">- Select Branch Name -</option>
                            <%
                            int branchCnt=0;
                            for (SPTenderCommonData sptcd : tenderCommonService.returndata("getBankBranchOrMemberList", userId, "getBranchList")) {
                                branchCnt++;
                            %>
                            <option value="<%=sptcd.getFieldName1()%>" ><%=sptcd.getFieldName2()%></option>
                            <%}%>
                        </select>
                    </td>

                        <td class="ff">Payment For : </td>
                    <td >
                        <select name="comboPaymentFor" class="formTxtBox_1" id="comboPaymentFor" style="width:155px;">
                            <option value='All' selected="selected">All</option>
                            <option value='Document Fees'>Document Fees</option>
                            <option value='Bid Security'>Bid Security</option>
                            <option value='Performance Security'>Performance Security</option>
                            <option value='Bank Guarantee'>New Performance Security</option>
                        </select>
                       
                    </td>
              
            </tr>
               
            <tr>
              <td colspan="4" class="t-align-center ff"><label class="formBtn_1">
                <input type="button" name="btnSearch" id="btnSearch" value="Search" />
              </label>

                <label class="formBtn_1 l_space">
<!--                <input type="submit" name="btnReset" id="btnReset" value="Reset" />-->
                    <input type="button" name="btnReset" id="btnReset" value="Reset" />
              </label>
              </td>
              </tr>
            </table>
</div>


</form>

            </div>
 <div id="print_div">
   <div id='titleDiv' class="pageHead_1" style="display: none;">Document Fees and Other Payment Report</div>
<div class="tabPanelArea_1 t_space">
    <div id="jQGrid" align="center">
        <table id="list"></table>
        <div id="page"></div>
    </div>
</div>

        <div>&nbsp;</div>
</div>
</div>
</div>
  <!--For Generate PDF  Starts-->
    <form id="formstyle" action="" method="post" name="formstyle">
        <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
        <%
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
            String appenddate = dateFormat1.format(new Date());
        %>
        <input type="hidden" name="fileName" id="fileName" value="TenderPayment_<%=appenddate%>" />
        <input type="hidden" name="id" id="id" value="TenderPayment" />
    </form>
    <!--For Generate PDF  Ends-->
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
 <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
  <!--Dashboard Footer End-->

</body>

  <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
