<%-- 
    Document   : TodaysVerifiedTenPayments
    Created on : Apr 6, 2011, 10:44:14 AM
    Author     : Karan
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Verified Tender Payment Daily Transactions</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        
     <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>

        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link id="jqGrid_css" href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" media="screen"/>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />

        


          <script type="text/javascript">
            /* Call Print function */
            $(document).ready(function() {
                if (document.getElementById("print")!=null){
                    $('#print').show();
                    $("#print").click(function() {
                    printElem({leaveOpen: true, printMode: 'popup'});
                });
                }

            });

                function printElem(options){
                if (document.getElementById("print_area")!=null){
                    
//            var columnNames = $("#list").jqGrid('getGridParam','colNames');
            
            jQuery("#list").jqGrid('hideCol', 'action');
                    $('#span_goBack').hide();
                    $('#page').hide();
                    
                   $('#print_area').printElement(options);
                   $('#list_action').show();  
                   jQuery("#list").jqGrid('showCol', 'action');
                   
                   $('#span_goBack').show();
                    $('#page').show();
                }
            }
        </script>

        <script type="text/javascript">
            function fillGridOnEvent(userId){

                //alert(userId);

                $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                jQuery("#list").jqGrid({
                    url:'<%=request.getContextPath()%>/VerifyTenderPaymentServlet?q=1&action=fetchDailyVerifiedTenPayData&userId=' + userId,
                    datatype: "xml",
                    height: 250,
                    colNames:['Sl. No.','Tender ID, Lot No.','e-mail ID','Bidder / Consultant','Payment For',"Amount","Payment Date and Time",'Branch Maker',"Action"],
                    colModel:[
                        {name:'RowNumber',index:'RowNumber', width:40,sortable:false, align: 'center'},
                        {name:'tenderId',index:'tenderId', width:100,sortable:true, align: 'center'},
                        {name:'emailId',index:'emailId', width:250,sortable:true},
                        {name:'dbo.f_getbiddercompany(LM.userId)',index:'dbo.f_getbiddercompany(LM.userId)', width:250,sortable:true},
                        {name:'paymentFor',index:'paymentFor', width:150,sortable:true, align: 'center'},
                        {name:'amount',index:'amount', width:100,sortable:true, align: 'right'},
                        {name:'createdDate',index:'createdDate', width:140,sortable:true, align: 'center'},
                        {name:'PA.fullName',index:'PA.fullName', width:200,sortable:true},
                        {name:'action',index:'action', width:100,sortable:false, align: 'center'}

                    ],
                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    sortable:false,
                    caption: "",
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{edit:false,add:false,del:false,search:false});
            }
            jQuery().ready(function (){
                //fillGrid();
            });
        </script>

              

</head>


    <%
    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
     boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false;
     String userBranchId="";
    String userId = "";
                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") != null) {
                    userId = hs.getAttribute("userId").toString();
                    tenderCommonService.setLogUserId(userId);
                } else {
                    //response.sendRedirectFilter(request.getContextPath() + "/SessionTimedOut.jsp");
                    response.sendRedirect("SessionTimedOut.jsp");
                }

                List<SPTenderCommonData> lstCurBankUserRole = tenderCommonService.returndata("getBankUserRole",userId,null);
                if(!lstCurBankUserRole.isEmpty()){
                    if("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBranchMaker=true;
                    } else if("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBranchChecker=true;
                    } else if("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBankChecker=true;
                    }
                    userBranchId=lstCurBankUserRole.get(0).getFieldName2();
                }
    %>
    <body onload="fillGridOnEvent('<%=userId%>');">
    <div class="dashboard_div">
      <!--Dashboard Header Start-->
      <%@include  file="../resources/common/AfterLoginTop.jsp" %>
      <!--Dashboard Header End-->
      <!--Dashboard Content Part Start-->
      <div class="contentArea_1">
          
        <div class="pageHead_1">
            Verified Tender Payment Daily Transactions
            <span id="span_goBack" style="float: right;" >
                <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
                <a href="TenPaymentListing.jsp" class="action-button-goback">Go Back</a></span>
        </div>
<div id="print_area">

<div class="t_space">
    
    <div id="jQGrid" align="center">
        <table id="list"></table>
        <div id="page"></div>
    </div>
        </div>
    
</div>        
        </div>
</div>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
 <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
  <!--Dashboard Footer End-->

</body>

  <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>

</html>

