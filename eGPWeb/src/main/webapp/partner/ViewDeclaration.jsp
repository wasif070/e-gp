<%-- 
    Document   : ViewDeclaration
    Created on : Jul 25, 2016, 12:51:30 PM
    Author     : aprojit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Bid Securing Declaration</title>
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <div class="contentArea_1">
                    <div id="print_area">
                        <div class="pageHead_1">&nbsp;
                            <span class="t-align-left" >Bid Securing Declaration</span>  
                        </div>
                    </div>
                          <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">

                                <td class="contentArea_1">
                                    <!--Page Content Start-->
                                     <div style=" padding: 5px; border-top: 1px #ededed solid; border-left: 1px #ededed solid; width:100%; height: 100%; text-align: justify;">
                                                        <b style="color: green;">Bid Securing Declaration</b>
                                                        <br/><br/>I, the undersigned, declare that: 
                                                        <br/><br/>I understand that, according to your conditions, bids must be supported by a Bid-Securing Declaration. We accept that we will automatically be suspended from being eligible for bidding in any contract with the Employer for the period of time of [insert number of months or years, minimum of one year] starting on [insert date], if we are in breach of our obligation(s) under the bid conditions, because we: 
                                                        <br/><br/>&emsp;a) Have withdrawn our Bid during the period of bid validity specified in the Form of Bid; or
                                                        <br/><br/>&emsp;b) having been notified of the acceptance of our Bid by the Employer during the period of bid validity, (i) fail or refuse to execute the Contract, if required, or 
                                                        <br/>&emsp;&emsp;&emsp;&emsp;(ii) fail or refuse to furnish the Performance Security, in accordance with the ITB.
                                                        
                                                        <br/><br/>I understand that I shall be given an option for payment of an amount equivalent to the Bid Security within 10 days from the date of notification of suspension.
                                                        <br/><br/>Failure to make the above payment, I understand that I shall be debarred for a minimum of one year as per Debarment Rule, 2013 or for the period stated above, whichever is more.
                                               
                                                        <br/><br/>&emsp;&emsp;&emsp;I understand this Bid Securing Declaration shall expire if I am not the successful Bidder, upon the earlier of (i) our receipt of your notification to us of the name of the successful Bidder; or 
                                                        <br/>&emsp;&emsp;&emsp;(ii) twenty-eight days after the expiration of our Bid.      
                                                    </div>                        
                                    <!--Page Content End-->
                                </td>
                                  
                            </tr>
                       </table>
                </div>
            </div>
        </div>
    </body>
</html>
