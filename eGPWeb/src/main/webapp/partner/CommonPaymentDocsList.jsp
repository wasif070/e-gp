
<%--
    Document   : CommonPaymentDocsList
    Created on : Mar 9, 2011, 4:29:04 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>



    <script>
    function ShowDocs(){
        $('#tblPaymentDocs').show();
        $('#anchHide').show();
        $('#anchShow').hide();
    }

    function HideDocs(){
        $('#tblPaymentDocs').hide();
        $('#anchHide').hide();
        $('#anchShow').show();
    }

    $(document).ready(function(){
        HideDocs();
    });
    </script>
<%
TenderCommonService objCommonDocTCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            if (session.getAttribute("userId") != null) {
                objCommonDocTCS.setLogUserId(session.getAttribute("userId").toString());
            }
            String cmnPaymentId = "0", docTenderId="0", docPayTyp="", docUserId="0", strPDFUrl="";
            boolean docIsBranchMaker=false, docIsBranchChecker=false, docIsBankChecker=false;
            String docUserBranchId="";

             HttpSession docHS = request.getSession();
             if (docHS.getAttribute("userId") != null) {
                     docUserId = docHS.getAttribute("userId").toString();
              }

            if (request.getParameter("payId") != null) {
                cmnPaymentId = request.getParameter("payId");
            }

             if (request.getParameter("tenderId") != null) {
                docTenderId = request.getParameter("tenderId");
            }

             if (request.getParameter("payTyp") != null) {
                docPayTyp = request.getParameter("payTyp");
            }

               if (request.getParameter("pdfUrl") != null) {
                strPDFUrl = request.getParameter("pdfUrl");
            }


             List<SPTenderCommonData> docCurBankUserRole = objCommonDocTCS.returndata("getBankUserRole", docUserId, null);
    if (!docCurBankUserRole.isEmpty()) {
        if ("BranchMaker".equalsIgnoreCase(docCurBankUserRole.get(0).getFieldName1())) {
            docIsBranchMaker = true;
        } else if ("BranchChecker".equalsIgnoreCase(docCurBankUserRole.get(0).getFieldName1())) {
            docIsBranchChecker = true;
        } else if ("BankChecker".equalsIgnoreCase(docCurBankUserRole.get(0).getFieldName1())) {
            docIsBankChecker = true;
        }
        docUserBranchId = docCurBankUserRole.get(0).getFieldName2();
    }

%>


<table width="100%" cellspacing="0" >
    <tr>
        <td>
            <div style="margin-top: 12px;">
    <span style="float: right;">
            
        <a id="anchPDF" class="action-button-savepdf" style="margin-right: 3px; display: none;"
            href="<%=strPDFUrl%>" >Save As PDF </a>
        <a id="anchShow" onclick="return ShowDocs();" class="action-button-document">View reference document</a>
        <a id="anchHide" onclick="return HideDocs();" class="action-button-document">Hide reference document</a>
        <%if(docIsBranchMaker){%>
        <a class="action-button-upload" href="TenderPaymentDocsUpload.jsp?tenderId=<%=docTenderId%>&tenderPaymentId=<%=cmnPaymentId%>&payTyp=<%=docPayTyp%>">Upload reference document</a>
        <%}%>
        <a href="javascript:void(0);" id="print" style="display: none;" class="action-button-view">Print</a>&nbsp;
       
    </span>
</div>
        </td>

    </tr>
    <tr>
        <td>
            <%--START CODE TO GET DOCS UPLOADED LIST--%>
              <table  id="tblPaymentDocs"  width="100%" cellspacing="0" class="tableList_1 t_space">
 <tr>
     <th width="4%" class="t-align-center">Sl. No.</th>
     <th class="t-align-center" width="23%">File Name</th>
     <th class="t-align-center" width="32%">File Description</th>
     <th class="t-align-center" width="7%">File Size <br />(in KB)</th>
     <th class="t-align-center" width="18%">Action</th>
 </tr>
<%

int docCnt = 0;

//TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

//for (SPTenderCommonData sptcd : objCommonDocTCS.returndata("TenderPaymentDocs", cmnPaymentId, "")) {
for (SPTenderCommonData sptcd : objCommonDocTCS.returndata("TenderPaymentDocsForView", cmnPaymentId, docUserId)) {
  docCnt++;
%>
<tr
 <%if(Math.IEEEremainder(docCnt,2)==0) {%>
                    class="bgColor-Green"
                <%} else {%>
                    class="bgColor-white"
                <%}%>
>
<td class="t-align-center"><%=docCnt%></td>
<td class="t-align-left"><%=sptcd.getFieldName1()%></td>
<td class="t-align-left"><%=sptcd.getFieldName2()%></td>
<td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()))/1024%>
</td>
 <td class="t-align-center">
     <a href="<%=request.getContextPath()%>/TenderPaymentDocUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderPaymentId=<%=cmnPaymentId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
     &nbsp;

     <%
if ("Yes".equalsIgnoreCase(sptcd.getFieldName5())) {%>
     <a href="<%=request.getContextPath()%>/TenderPaymentDocUploadServlet?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&tenderPaymentId=<%=cmnPaymentId%>&pageName=TenderPaymentDetails.jsp&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
     <%}%>
</td>
</tr>

  <%   if(sptcd!=null){
sptcd = null;
}
}%>
<% if (docCnt == 0) {%>
<tr>
<td colspan="5" class="t-align-center">No docs found.</td>
</tr>
<%}%>
</table>
<%--END CODE TO GET DOCS UPLOADED LIST--%>
        </td>
    </tr>
</table>
