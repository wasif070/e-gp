<%-- 
    Document   : VerifyPayment
    Created on : Feb 2, 2011, 3:57:47 PM
    Author     : Karan
--%>
<%

String strUserTypeId = "";
        Object objUserId = session.getAttribute("userId");
        if (objUserId != null) {
            strUserTypeId = session.getAttribute("userId").toString();
        }
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Verify Registration Fee Payment Details</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />


        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />

        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript">
            function checkKey(e)
{
    var keyValue = (window.event)? e.keyCode : e.which;
    if(keyValue == 13){
    //Validate();
    $('#pageNo').val('1')
    $('#btnSearch').click();
    //loadTable();
    }
}
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>

        <script type="text/javascript">
            function fillGridOnEvent(userId, emailId, isVerified, branchId, branchMakerId, fromDt, toDt){
                /*
                alert(userId);
                alert(emailId);
                alert(isVerified);
                alert(branchId);
                alert(branchMakerId);
                alert(fromDt);
                alert(toDt);
                */
                
                $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                jQuery("#list").jqGrid({
                    url:'<%=request.getContextPath()%>/VerifyPaymentServlet?q=1&action=fetchData&userId=' + userId + '&emailId=' + emailId + '&isVerified=' + isVerified + '&branchId=' + branchId + '&branchMakerId=' + branchMakerId + '&fromDt=' + fromDt + '&toDt=' + toDt,
                    datatype: "xml",
                    height: 250,
                    colNames:['Sl. No.','e-mail ID','Branch Maker',"Date and Time of Payment","New Registration / Renewal","Action"],
                    colModel:[
                        {name:'RowNumber',index:'RowNumber', width:40,sortable:false, align: 'center'},
                        {name:'emailId',index:'emailId', width:250,sortable:true},
                        {name:'PA.fullName',index:'PA.fullName', width:200,sortable:true},
                        {name:'dtOfPayment',index:'dtOfPayment', width:140,sortable:true, align: 'center'},
                        {name:'RP.status',index:'RP.status', width:150,sortable:true, align: 'center'},
                        {name:'action',index:'action', width:100,sortable:false, align: 'center'}
                        
                    ],
                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    sortable:false,
                    caption: "",
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{edit:false,add:false,del:false,search:false});
            }
            jQuery().ready(function (){
                //fillGrid();
            });
        </script>

                    <script type="text/javascript">
                        $(function() {
                            $('#comboBranches').change(function() {
                                if( $('#comboBranches').val()!=''){
                                    branchId = $('#comboBranches').val();
                                    $.post("<%=request.getContextPath()%>/VerifyPaymentServlet", {branchId: branchId, action: 'getBranchMembers'},  function(j){
                                        //alert(j);
                                        $("select#comboBranchMembers").html(j);
                                    });
                                }
                            });

                            $('#btnSearch').click(function() {
                                //alert('You clicked Search');
                                   var errflag = false;

                                $('#SpError1').html('');

                                var userId='', emailId='', isVerified='', branchId='', branchMakerId='', fromDt='', toDt='';
                                userId=$('#userId').val();
                                emailId=$('#txtEmailId').val();
                                isVerified=$('#comboStatus').val();
                                
                               
                                fromDt=$('#txtFromDt').val();
                                toDt=$('#txtToDt').val();

                            if(document.getElementById("comboBranches")!=null){
                                if($('#comboBranches').val()!=''){
                                    branchId=$('#comboBranches').val();
                                }
                            }

                             if($('#comboBranchMembers').val()!=''){
                                    branchMakerId=$('#comboBranchMembers').val();
                             }


    if(fromDt!='' && toDt!=''){
                                //alert('Hi');
                                 var d =new Date();
                            var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());

                            var value=fromDt;
                            var mdy = value.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);

                             var value2=toDt;
                            var mdy2 = value2.split('/')  //Date and month split
                            var mdyhr2= mdy2[2].split(' ');  //Year and time split
                            var valuedate2= new Date(mdyhr2[0], mdy2[1]-1, mdy2[0]);

                            if(Date.parse(valuedate) > Date.parse(valuedate2)){
                                   errflag = true;
                                    $('#SpError1').html('Payment Date From must be less than or equal to Payment Date To');
                                }
                            }

                             if(!errflag){
                             fillGridOnEvent(userId, emailId, isVerified, branchId, branchMakerId, fromDt, toDt);
                             }
                            });

                            $('#btnReset').click(function() {
                                //alert('Clearing...')

                                 // Clear Err Messages
                                $('#SpError1').html('');
                                //==========================

                               var userId=$('#userId').val();                               
                              
                              $('#txtEmailId').val('');
                                $('#comboStatus').val('no');
                                $('#txtFromDt').val('');
                                $('#txtToDt').val('');

                                if(document.getElementById("comboBranches")!=null){
                                    $('#comboBranches').val('');
                                }

                                if(document.getElementById("comboBranchMembers")!=null){
                                     $('#comboBranchMembers').val('');
                                }
                                //var j="<option value=''>- Select Branch Maker -</option>";
                                //$("select#comboBranchMembers").html(j);

                                fillGridOnEvent(userId,'','no','','','','');
                            });

                            
                        });
                    </script>

</head>


    <%
    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
     boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false;
     tenderCommonService.setLogUserId(strUserTypeId);
     String userBranchId="";            
    String userId = "";
                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") != null) {
                    userId = hs.getAttribute("userId").toString();
                } else {
                    //response.sendRedirectFilter(request.getContextPath() + "/SessionTimedOut.jsp");
                    response.sendRedirect("SessionTimedOut.jsp");
                }

                List<SPTenderCommonData> lstCurBankUserRole = tenderCommonService.returndata("getBankUserRole",userId,null);
                if(!lstCurBankUserRole.isEmpty()){
                    if("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBranchMaker=true;
                    } else if("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBranchChecker=true;
                    } else if("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBankChecker=true;
                    }
                    userBranchId=lstCurBankUserRole.get(0).getFieldName2();
                }
    %>
    <body onload="fillGridOnEvent('<%=userId%>','','no','','','','');hide();">
    <div class="dashboard_div">
      <!--Dashboard Header Start-->
      <%@include  file="../resources/common/AfterLoginTop.jsp" %>
      <!--Dashboard Header End-->
      <!--Dashboard Content Part Start-->
      <div class="contentArea_1">
        <div class="pageHead_1">Verify Registration Fee Payment Details
        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('5');">Save as PDF</a></span>
        </div>

        <div class="tabPanelArea_1 t_space">
      <form id="frmData" method="post">
        <div class="formBg_1 t_space">
            <input type="hidden" name="userId" id="userId" value="<%=userId%>"></input>
            <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
            <table cellspacing="10" class="formStyle_1" width="100%" id="tblSearchBox">
            <tr>
              <td width="14%" class="ff">e-mail ID :</td>
              <td width="44%"><input name="txtEmailId" type="text" class="formTxtBox_1" onkeypress="checkKey(event);" id="txtEmailId" style="width:195px;" /></td>
              <td width="13%" class="ff">Verification Status :</td>
              <td width="29%">
                  <select name="comboStatus" class="formTxtBox_1" id="comboStatus" style="width:95px;">
                      <option value='yes' >Verified</option>
                      <option value='no' selected="selected">Pending</option>
                  </select>
              </td>
              </tr>
            <tr>
              <td class="ff">Payment Date From : </td>
              <td>
                  <input onfocus="GetCal('txtFromDt','txtFromDt');" name="txtFromDt" type="text" class="formTxtBox_1" id="txtFromDt" style="width:100px;" readonly="readonly" />
                  <img id="imgFromDt" name="imgFromDt" onclick="GetCal('txtFromDt','imgFromDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                                <span id="SpError1" class="reqF_1"></span>
              </td>
              <td class="ff">Payment Date To : </td>
              <td>
               <input onfocus="GetCal('txtToDt','txtToDt');" name="txtToDt" type="text" class="formTxtBox_1" id="txtToDt" style="width:100px;" readonly="readonly" />
                  <img id="imgToDt" name="imgToDt" onclick="GetCal('txtToDt','imgToDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                  <span id="SpError" class="reqF_1"></span>
              </td>
              </tr>
            <tr>
                            <%if (isBankChecker) {%>
                    <td class="ff">Branch  Name : </td>
                    <td><select name="comboBranches" class="formTxtBox_1" id="comboBranches" style="width:155px;">
                            <option value='' selected="selected">- Select Branch Name -</option>
                            <%
                                        int branchCnt = 0;
                            for (SPTenderCommonData sptcd : tenderCommonService.returndata("getBankBranchOrMemberList", userId, "getBranchList")) {
                                branchCnt++;
                            %>
                            <option value="<%=sptcd.getFieldName1()%>" ><%=sptcd.getFieldName2()%></option>
                            <%}%>
                        </select>
                    </td>

                    <td value='' class="ff">Branch  Maker : </td>
                    <td>
                                <select name="comboBranchMembers" class="formTxtBox_1" id="comboBranchMembers" style="width:155px;">
                            <option value=''>- Select Branch Maker -</option>
                        </select>
                    </td>
                            <%} else if (isBranchChecker) {%>
                    <td class="ff">Branch  Maker : </td>
                    <td>
                        <select name="comboBranchMembers" class="formTxtBox_1" id="comboBranchMembers" style="width:155px;">
                            <option value='' selected="selected">- Select Branch Maker -</option>
                            <%
                                        int branchCnt = 0;
                            for (SPTenderCommonData sptcd : tenderCommonService.returndata("getBankBranchOrMemberList", userBranchId, "getBranchMemberList")) {
                                branchCnt++;
                            %>
                            <option value="<%=sptcd.getFieldName1()%>" ><%=sptcd.getFieldName2()%></option>
                            <%}%>
                        </select>
                    </td>

                        <td style="visibility: hidden;" class="ff">Branch  Name : </td>
                    <td style="visibility: hidden;" ><select name="comboBranches" class="formTxtBox_1" id="comboBranches" style="width:155px;">
                            <option value='' selected="selected">- Select Branch Name -</option>
                        </select>
                    </td>
                <%}%>



            </tr>
            <tr>
              <td colspan="4" class="t-align-center ff"><label class="formBtn_1">
                <input type="button" name="btnSearch" id="btnSearch" value="Search" />
              </label>

                <label class="formBtn_1 l_space">

                                    <input type="button" name="btnReset" id="btnReset" value="Reset" />
              </label>
              </td>
              </tr>
            </table>

                </div>


               
            </form>

                           

                            
        </div>

                            <div style="margin-top: 6px;">
                                <span>&nbsp;</span>
                                <span style="float: right;">
                                    <a href="TodaysVerifiedRegPayments.jsp" class="action-button-viewTender">View Today's Verified Transactions</a>
                                </span>
                                <div style="height: 6px;">&nbsp;</div>
                            </div>
                            
        <div class="tabPanelArea_1 t_space">
    <div id="jQGrid" align="center">
        <table id="list"></table>
        <div id="page"></div>
    </div>
         </div>
                            

        <div>&nbsp;</div>
        </div>
</div>
<!--For Generate PDF  Starts-->
<form id="formstyle" action="" method="post" name="formstyle">
    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
    <%
        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
        String appenddate = dateFormat1.format(new Date());
    %>
    <input type="hidden" name="fileName" id="fileName" value="RegistrationFee_<%=appenddate%>" />
    <input type="hidden" name="id" id="id" value="RegistrationFee" />
</form>
<!--For Generate PDF  Ends-->
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
 <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
  <!--Dashboard Footer End-->

</body>

  <script type="text/javascript" language="Javascript">
        function showHide()
	{
		if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
			document.getElementById('tblSearchBox').style.display = 'table';
			document.getElementById('collExp').innerHTML = '- Advanced Search';
		}else{
			document.getElementById('tblSearchBox').style.display = 'none';
			document.getElementById('collExp').innerHTML = '+ Advanced Search';
		}
	}
	function hide()
	{
		document.getElementById('tblSearchBox').style.display = 'none';
		document.getElementById('collExp').innerHTML = '+ Advanced Search';
	}
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
