<%-- 
    Document   : TenderPaymentExtension
    Created on : Aug 20, 2014, 3:03:15 PM
    Author     : G. M. Rokibul Hasan
--%>


<%@page import="org.apache.poi.ss.usermodel.PrintCellComments"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Calendar"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
               <%if("extendValidityps".equalsIgnoreCase(request.getParameter("action"))) {%>
                Performance Security Validity Extension
                <%}%>
    </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
         <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript" >
            $(document).ready(function(){
                $('#btnSubmit').click(function(){
                    var flag=true;

                      if($.trim($('#txtextValidityDate').val())==''){
                            $('#SpError1').html('Please select new validity Date.');
                            flag=false;
                        }
                        else {
                            var parts =($.trim($('#txtextValidityDate').val())).split('/');
                            var date1 = new Date(parts[2],parts[1]-1,parts[0]);
                            var parts1 =($.trim($('#txtValidityDate').val())).split('/');
                            var date2 = new Date(parts1[2],parts1[1]-1,parts1[0]);
                             $('#SpError1').html('');
                           if(date2.getTime()>=date1.getTime()){ 
                                $('#SpError1').html(' new validity date has to be greater than previous date ');
                                flag=false;
                           }
                            else {
                                $('#SpError1').html('');
                           }
                        }

                     if($.trim($('#txtComments').val())==''){
                            $('#SpError').html('Please enter Remarks.');
                            flag=false;
                        }
                        else {
                            if($.trim($('#txtComments').val()).length >=1000) {
                                $('#SpError').html('Maximum 1000 characters are allowed.');
                                flag=false;
                            }
                        }
                        flag==false;
                         if(flag==false){
                            return false;
                        }
                });

                $('#txtextValidityDate').blur(function(){
                    var flag=true;

                     if($.trim($('#txtextValidityDate').val())==''){
                            $('#SpError1').html('Please select new validity Date.');
                            flag=false;
                        }
                        else {
                            var parts =($.trim($('#txtextValidityDate').val())).split('/');
                            var date1 = new Date(parts[2],parts[1]-1,parts[0]);
                            var parts1 =($.trim($('#txtValidityDate').val())).split('/');
                            var date2 = new Date(parts1[2],parts1[1]-1,parts1[0]);
                             $('#SpError1').html('');
                           if(date2.getTime()>=date1.getTime()){
                                $('#SpError1').html(' new validity date has to be greater than previous date ');
                                flag=false;
                           }
                            else {
                                $('#SpError1').html('');
                           }
                        }

                         if(flag==false){
                            return false;
                        }
                });

                $('#txtComments').blur(function(){
                    var flag=true;

                     if($.trim($('#txtComments').val())==''){
                            $('#SpError').html('Please enter Remarks.');
                            flag=false;
                        }
                        else {
                            if($('#txtComments').length<=1000) {
                                $('#SpError').html('');
                            }
                            else{
                                $('#SpError').html('Maximum 1000 characters are allowed.');
                                flag=false;
                            }
                        }

                         if(flag==false){
                            return false;
                        }
                });
                  });
        </script>

    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%
                    //Added by Salahuddin
                                 TenderCommonService tcs1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                 List<SPTenderCommonData> listDP = tcs1.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                                 boolean isIctTender = false;
                                 if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                                    isIctTender = true;
                                 }
                                 //Added by Salahuddin

                    ResourceBundle bdl = null;
                            bdl = ResourceBundle.getBundle("properties.cmsproperty");
                %>
                <%
                   TenderCommonService objTSC = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                   CommonSearchDataMoreService objTpayment = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    boolean isBankUser=false;
                    boolean isCurrUserPE = false;
                    String strPartTransId="0", userId="", payUserId="", regPaymentId="";
                    String tenderId="0", pkgLotId="0", payTyp="", paymentTxt="", strAction="";
                %>
                <%

                 HttpSession hs = request.getSession();
             if (hs.getAttribute("userId") != null) {
                     userId = hs.getAttribute("userId").toString();
                     objTSC.setLogUserId(userId);
              }
             else {//response.sendRedirectFILTER(request.getContextPath() + "/SessionTimedOut.jsp");
             }

                  if (hs.getAttribute("govUserId") != null) {
                     strPartTransId = hs.getAttribute("govUserId").toString();
              }

                String referer = "";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");
                }

                if ( hs.getAttribute("userTypeId")!= null) {
                    if("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())){
                        isBankUser=true; // userType is ScheduleBank";
                     }
                }

                if (request.getParameter("uId") != null) {
                    payUserId = request.getParameter("uId");
                }

                if (request.getParameter("payId") != null) {
                    regPaymentId = request.getParameter("payId");
                }

                if (request.getParameter("tenderId") !=null){
                        tenderId=request.getParameter("tenderId");
                    }
                     if (request.getParameter("lotId") != null){
                        if (request.getParameter("lotId")!="" && !request.getParameter("lotId").equalsIgnoreCase("null")){
                           pkgLotId=request.getParameter("lotId");
                        }
                    }

                     if (request.getParameter("payTyp") != null){
                        if (request.getParameter("payTyp")!="" && !request.getParameter("payTyp").equalsIgnoreCase("null")){
                           payTyp=request.getParameter("payTyp");

                            if ("df".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Document Fees";
                             } else if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Tender/Proposal Security";
                             } else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Performance Security";
                             }else if("bg".equalsIgnoreCase(request.getParameter("payTyp"))){
                                 paymentTxt = "New Performance Security";
                             }
                        }
                    }

                /* Start: CODE TO CHECK PE USER */
                List<SPTenderCommonData> listChkCurrUserPE = objTSC.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);

                if (!listChkCurrUserPE.isEmpty()) {
                    if (userId.equalsIgnoreCase(listChkCurrUserPE.get(0).getFieldName1())) {
                        isCurrUserPE = true;
                    }
                }

                listChkCurrUserPE = null;
                /* End: CODE TO CHECK PE USER */

                if (request.getParameter("action")!=null){
                    strAction = request.getParameter("action");
                }
                %>
                <div class="contentArea_1">
                    <div class="pageHead_1">
                        <%if("extendValidityps".equalsIgnoreCase(request.getParameter("action"))) {%>
                Performance Security Validity Extension
                <%}%>
                        <span style="float:right;">

                <%
                String strRefRedirect = "";
                    if (isCurrUserPE) {
                        if (referer.contains("PaidTendererList.jsp")) {
                            strRefRedirect = "../partner/PaidTendererList.jsp?tenderId=" + tenderId + "&lotId=" + pkgLotId + "&payTyp=" + payTyp;
                        } else if(referer.contains("Invoice.jsp")){
                            strRefRedirect = "../officer/Invoice.jsp?tenderId=" + tenderId;
                        }else if(referer.contains("InvoiceServiceCase.jsp")){
                            strRefRedirect = "../officer/InvoiceServiceCase.jsp?tenderId=" + tenderId;
                        } else {
                            strRefRedirect = "../officer/TECProcess.jsp?tenderId=" + tenderId;
                        }
                    } else {
                        strRefRedirect = "TenPaymentListing.jsp";
                    }
                %>
                            <a href="<%=strRefRedirect%>" class="action-button-goback">Go Back</a>

                        </span>
                    </div>

                     <% pageContext.setAttribute("tenderId", tenderId); %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>

                    <div class="tabPanelArea_1 t_space">

                        <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("payment")){
                            msgTxt="Payment information entered successfully.";
                        } else if(msgId.equalsIgnoreCase("updated")){
                            msgTxt="Payment updated successfully.";
                        } else if(msgId.equalsIgnoreCase("extended")){
                            msgTxt="Payment extended successfully.";
                        } else  if(msgId.equalsIgnoreCase("released")){
                            msgTxt="Payment released successfully.";
                        } else  if(msgId.equalsIgnoreCase("canceled")){
                            msgTxt="Payment canceled successfully.";
                        } else  if(msgId.equalsIgnoreCase("on-hold")){
                            msgTxt="Payment put on-hold successfully.";
                        } else  if(msgId.equalsIgnoreCase("forfeited")){
                            msgTxt="Payment forfeited successfully.";
                        } else  if(msgId.equalsIgnoreCase("forfeitrequested")){
                            msgTxt="Payment forfeit request submitted successfully.";
                        } else  if(msgId.equalsIgnoreCase("releaserequested")){
                            msgTxt="Payment release request submitted successfully.";
                        } else  if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError){%>
                        <div class="responseMsg errorMsg" ><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg" ><%=msgTxt%></div>
                   <%}%>
                <%}}%>

                <%
                            String bidderUserId = "0", bidderEmail = "";
                            String emailId = "";
                            boolean isPaymentVerified = false;

                            List<SPTenderCommonData> lstTendererEml = objTSC.returndata("getEmailIdfromUserId", payUserId, null);
                            emailId = lstTendererEml.get(0).getFieldName1();
                            bidderEmail = emailId;
                            bidderUserId = payUserId;

                %>

                <%
                            List<SPCommonSearchDataMore> lstPaymentDetail = objTpayment.geteGPData("getTenderPaymentDetail", regPaymentId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    if (!lstPaymentDetail.isEmpty()) {
                                        if ("yes".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName8())) {
                                            isPaymentVerified = true;
                                        }
                        /*if(isIctTender)
                        {
                            List<SPCommonSearchDataMore> lstPaymentDetail1 = objTpayment.geteGPData("getTenderPaymentDetail", regPaymentId, "Performance Security", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                        }*/
                %>
                 <!-- START: COMMON PACKAGE/LOT DESCRIPTION FILE -->
                    <div >
                        <%@include file="CommonPackageLotDescription.jsp" %>
                    </div>
                    <!-- END COMMON PACKAGE/LOT DESCRIPTION FILE -->

                <form id="frmVerifyPayment" action="<%=request.getContextPath()%>/VerifyTenderPaymentServlet?action=requestExtendValidityPS&payId=<%=regPaymentId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>&requesttype=<%=strAction%>" method="POST">
                        <input type="hidden" name="tenderRefNo" id="tenderRefNo" value="<%=toextTenderRefNo%>" />

                        <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                            <tr>
                                <td style="font-style: italic" class="ff t-align-right" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <tr>
                                <td width="18%" class="ff">Payment Status :</td>
                                <td>
                                     <% if (!"Pending".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName10()) && !isPaymentVerified) {%>
                                                Verification is Pending
                                                <%} else {%>
                                                   <%=lstPaymentDetail.get(0).getFieldName10()%>
                                                <%}%>
                                </td>
                            </tr>

                            <tr>
                                <td width="18%" class="ff">Email ID :</td>
                                <td><%=bidderEmail%></td>
                            </tr>
                            <tr>
                                <td class="ff">Bank Name :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName11()%></td>
                            </tr>
                        <tr>
                            <td class="ff">Branch Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Maker :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName2()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Checker :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName11()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Payment For : </td>
                            <td><%=paymentTxt%></td>
                        </tr>
                        <%if(!isIctTender){%>
                        <tr>
                            <td class="ff">Currency :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName3()%>
                            <input type="hidden" id="hdnCurrency" name="hdnCurrency" value="<%=lstPaymentDetail.get(0).getFieldName3()%>">
                            </td>
                        </tr>
                        <%}%>
                        <%if(!isIctTender && ("ps".equalsIgnoreCase(request.getParameter("payTyp")) || "bg".equalsIgnoreCase(request.getParameter("payTyp")))){
                        if("requestforfeit".equalsIgnoreCase(request.getParameter("action"))){%>
                        <tr>
                            <td class="ff">Compensate Amount : <span class="mandatory">*</span></td>
                            <td><input type="text" name="compAmt" id="compAmtid" class="formTxtBox_1" onblur=" return CompansiateAmt()" maxlength="12"/>
                                <input type="hidden" id="balanceAmtid" name="balanceAmt">
                                &nbsp;&nbsp;<span id="balanceAmtspan" ></span>
                                <span id="compAmtspan" class="reqF_1"></span>
                            </td>
                        </tr>
                        <%}}%>
                        <%
                            String allAmount="";
                            if(!isIctTender){%>
                                <tr>
                                    <td class="ff">Amount :</td>
                                    <td>
                                        <%if("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                        <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                        <%} else if("USD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                            <label>$</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                        <%} else if("Nu.".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                            <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                        <%}%>
                                        <input type="hidden" id="hdnAmount" name="hdnAmount" value="<%=lstPaymentDetail.get(0).getFieldName4()%>">

                                        <input type="hidden" id="hdnValidityRefId" name="hdnValidityRefId" value="<%=lstPaymentDetail.get(0).getFieldName9()%>">
                                        <input type="hidden" id="hdnPayStatus" name="hdnPayStatus" value="<%=lstPaymentDetail.get(0).getFieldName10()%>">
                                    </td>
                                </tr>
                            <%}else if(isIctTender && "ps".equalsIgnoreCase(request.getParameter("payTyp")) && "requestrelease".equalsIgnoreCase(request.getParameter("action"))){
                                //for currency symbol
                                List<SPCommonSearchDataMore> lstPerSecPayDetail = objTpayment.geteGPData("getPerformanceSecurityPaymentDetail", tenderId.toString());
                                List<SPCommonSearchDataMore> lstPaymentDetail1 = objTpayment.geteGPData("getTenderPaymentDetail", regPaymentId, "Performance Security", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                for(int i=0;i<lstPaymentDetail1.size();i++){%>
                                <tr>
                                    <td class="ff"><%out.println("Amount (in "+lstPaymentDetail1.get(i).getFieldName3()+")");%></td>
                                    <td>
                                        <%out.println(""+lstPerSecPayDetail.get(i).getFieldName13()+" "+lstPaymentDetail1.get(i).getFieldName4());
                                            if(i==lstPaymentDetail1.size()-1)
                                                allAmount=allAmount+lstPaymentDetail1.get(i).getFieldName3()+" " +lstPaymentDetail1.get(i).getFieldName4();
                                            else
                                                allAmount=allAmount+lstPaymentDetail1.get(i).getFieldName3()+" " +lstPaymentDetail1.get(i).getFieldName4()+", ";
                                        %>

                                        <input type="hidden" id="hdnAmount" name="hdnAmount" value="<%=allAmount%>">

                                    </td>
                                </tr>
                                <!-- Dohatec Start -->
                            <%}} else if(isIctTender && "bg".equalsIgnoreCase(request.getParameter("payTyp")) && ("requestrelease".equalsIgnoreCase(request.getParameter("action")) || "requestforfeit".equalsIgnoreCase(request.getParameter("action")))){
                                String txtCurrencySymbol = "";
                                String currencyType = "";
                                String perSecAmount = "";
                                CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                List<SPCommonSearchDataMore> lstNwPerSec = csdms.geteGPData("getNewPerSecDetail", tenderId.toString(), pkgLotId.toString(), payUserId.toString());
                                if(!lstNwPerSec.isEmpty())
                                {
                                    for(int i = 0; i<lstNwPerSec.size(); i++)
                                    {
                                        perSecAmount = lstNwPerSec.get(i).getFieldName1();
                                        currencyType = lstNwPerSec.get(i).getFieldName2();
                                        txtCurrencySymbol = lstNwPerSec.get(i).getFieldName3();
                                        if(i == lstNwPerSec.size()-1)
                                            allAmount = allAmount + currencyType + " " + perSecAmount;
                                        else
                                            allAmount = allAmount + currencyType + " " + perSecAmount + ", ";
                                        System.out.print("Amount : " + allAmount);
                            %>
                           
                                <tr>
                                    <td class="ff">Amount (in <%=currencyType%>) :</td>
                                    <td><label id="lblCurrencySymbol_<%=i%>"><%=txtCurrencySymbol%></label>&nbsp;
                                        <label id="lblAmount_<%=i%>"><%=perSecAmount%></label>
                                        <input type="hidden" id="hdnAmnt_<%=i%>" name="hdnAmnt" value="<%=perSecAmount%>">
                                    </td>
                                </tr>
                            <%}}%>
                                <input type="hidden" id="hdnAmount" name="hdnAmount" value="<%=allAmount%>">
                            <%}%>
                                <input type="hidden" id="payTyp" name="payTyp" value="<%=request.getParameter("payTyp")%>"
                                 <!-- Dohatec End -->
                                <input type="hidden" id="hdnBidderUserId" name="hdnBidderUserId" value="<%=bidderUserId%>">
                                <input type="hidden" name="view" value="CMS">
                         <tr>
                            <td class="ff">Mode of Payment :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName5()%>
                            </td>
                        </tr>
                        <%if("Pay Order".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5()) || "DD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5()) || "Bank Guarantee".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = objTSC.returndata("getTenderPaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Instrument No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Bank :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Bank Branch :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuance Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName4()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Validity Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName5()%></td>
                                </tr>
                                <tr>                                    
                                    <td class="ff">New Validity Date :</td>
                                    <td> <input name="extValidityDate" type="text" id="txtextValidityDate" style="width:100px;" readonly="true"  onfocus="GetCal('txtextValidityDate','txtextValidityDate');"/>
                                    <img id="txtextValidityDate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtextValidityDate','txtextValidityDateimg');"/>
                                    <input type="hidden" name="txtValidityDate" id="txtValidityDate" value="<%=lstPaymentDetailMore.get(0).getFieldName6()%>"/>
                                    <span id="SpError1" class="reqF_2"></span>
                                    </td>
                                </tr>

                               <% }
                            }%>

                            <%if("Account to Account Transfer".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = objTSC.returndata("getTenderPaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Account No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <%if(!"".equalsIgnoreCase(lstPaymentDetailMore.get(0).getFieldName3())){%>
                                <tr>
                                    <td class="ff">Branch Name :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <%}%>
                               <% }
                            }%>

                            <tr>
                                <td class="ff">Date of Payment :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName6()%></td>
                            </tr>

                             <tr>
                                <td class="ff">Branch Maker Remarks : </td>
                                <td>
                                    <%=lstPaymentDetail.get(0).getFieldName7()%>
                                </td>
                            </tr>

                            <tr>
                                <td class="ff">Remarks : <span class="mandatory">*</span></td>
                                <td>
                                    <textarea cols="100" rows="5" id="txtComments" name="txtComments" class="formTxtBox_1"></textarea>
                                    <span id="SpError" class="reqF_1"></span>
                                    <%//=lstPaymentDetail.get(0).getFieldName7()%>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <div class="t-align-left">
                                        <label class="formBtn_1">
                                            <input name="btnSubmit" id="btnSubmit" type="submit" value="Submit" <%if("ps".equalsIgnoreCase(request.getParameter("payTyp")) && !isIctTender){%>onclick=" return CompansiateAmt()"<%}%>/>
                                        </label>
                                    </div>

                                </td>
                            </tr>

                    </table>
                            <%  List<SPTenderCommonData> lstExtensionPaymentDetail =
                                objTSC.returndata("getTenderPaymentExtendDetail", regPaymentId, userId);
                                if (!lstExtensionPaymentDetail.isEmpty() && lstExtensionPaymentDetail != null) {%>
                            <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                                <tr>
                                    <th width="100%" class="t-align-left" colspan="5">Performance Security Extension validity History</th>
                                </tr>
                                <tr>
                                <th width="5%" class="t-align-left">Serial No.</th>
                                <th width="28%" class="t-align-left">Previous Validity Date</th>
                                <th width="28%" class="t-align-left">Extended Validity Date</th>
                                <th width="28%" class="t-align-left">Request Date</th>
                                <th width="11%" class="t-align-left">Action</th>
                                 </tr>
                                <% for(int i = 0; i< lstExtensionPaymentDetail.size(); i++)
                                    {%>
                                <tr>
                                    <td class="t-align-center"><%=i+1%></td>
                                    <td class="t-align-center"><%=lstExtensionPaymentDetail.get(i).getFieldName1()%></td>
                                    <td class="t-align-center"><%=lstExtensionPaymentDetail.get(i).getFieldName2()%></td>
                                    <td class="t-align-center"><%=lstExtensionPaymentDetail.get(i).getFieldName3()%></td>
                                    <td class="t-align-center"><%=lstExtensionPaymentDetail.get(i).getFieldName4()%></td>
                                </tr>
                                <%}%>
                            </table>
                            <%}%>
                    </form>
                    <% } else {%>
                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td>Payment information not found!</td>
                        </tr>
                    </table>
                    <%}%>
                </div>

                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->
                <!-- Dohatec Start -->
                <input type="hidden" id="isICT" value="<%if(isIctTender){out.print("ICT");}else{out.print("NCT");}%>"
                <!-- Dohatec End -->
        </div>
                 </div>
    </body>

    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabPayment");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

         
            function validateStartDate()
            {

                  // var a = getDate(document.getElementById("CstartDate").value);
                  // var enddate = new Date(a);
                  // var b = enddate.getTime()+(parseInt('1')*24*60*60*1000);
                  // var hdf = new Date(b);
                  // var monthname = new Array("Jan","Feb","Mar","Apr","May","Jun", "Jul","Aug","Sep","Oct","Nov","Dec");
                  // var finalenddate = hdf.getDate()+"-"+monthname[hdf.getMonth()]+"-"+hdf.getFullYear();
                  // document.getElementById("CendDate").value =finalenddate;

               if(document.getElementById("CstartDate").value!=""){
                   document.getElementById("CstartDatespan").innerHTML="";
               }
            }
    </script>
    <script type="text/javascript">
    function GetCal(txtname,controlname)
                        {
                            new Calendar({
                                inputField: txtname,
                                trigger: controlname,
                                showTime: false,
                                dateFormat:"%d/%m/%Y",
                                onSelect: function() {
                                    var date = Calendar.intToDate(this.selection.get());
                                    LEFT_CAL.args.min = date;
                                    LEFT_CAL.redraw();
                                    this.hide();
                                    document.getElementById(txtname).focus();
                                }
                            });

                            var LEFT_CAL = Calendar.setup({
                                weekNumbers: false
                            })
                        }
              </script>
     <script type="text/javascript">
         function ValidateDate(){
             var flag=true;
                     if($.trim($('#txtextValidityDate').val())==''){
                            $('#SpError1').html('Please Select Date.');
                            flag=false;
                        }
                        else {
                             var date1 = new Date($.trim($('#txtextValidityDate').val()));
                            var date2 = new Date($.trim($('#txtValidityDate').val()));
                            var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                            alert(diffDays)​;
                            if(diffDays >= '1') {
                                $('#SpError1').html('');
                            }
                            else{
                                $('#SpError1').html('Maximum 1000 characters are allowed.');
                                flag=false;
                            }
                        }
                         if(flag==false){
                            return false;
                        }
                }
    </script>
</html>


