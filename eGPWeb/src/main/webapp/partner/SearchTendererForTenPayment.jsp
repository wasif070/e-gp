
<%-- 
    Document   : SearchTendererForTenPayment
    Created on : Mar 2, 2011, 3:53:24 PM
    Author     : Karan
--%>

<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>
            <% 
            String ParentLink=null;
            if ("df".equalsIgnoreCase(request.getParameter("payTyp"))) { ParentLink="223";%>
                Search Email ID for Document Fees Payment
            <%} else if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) { ParentLink="908";%>
                Search Email ID for Tender Security Payment
            <%} else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) { ParentLink="231";%>
                Search Email ID for Performance Security Payment
            <%}%>

        </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
         <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript">
            function ErrorMsg(msg){
                if(msg != null || msg != '')
                {
                     jAlert(msg, "Payment");
                }
                return false;
            }
        </script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('#frmSearchTenderer').validate({
                    rules:{ txtEmailId: {required:true, email:true}

                    },
                    messages:{txtEmailId: {required:"<div class='reqF_1'>Please enter Email ID</div>",
                            email: "<div class='reqF_1'>Please enter valid Email ID</div>" }
                    }
                });
            });
        </script>
    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%
                    
                 String userId="", srchUserId="";
                 HttpSession hs = request.getSession();
                 if (hs.getAttribute("userId") != null) {
                         userId = hs.getAttribute("userId").toString();
                  }
                 /*else {response.sendRedirectFliter(request.getContextPath() + "/SessionTimedOut.jsp");}*/

                    boolean isBankUser=false;                    
                    boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false;
                    String userBranchId="";
                    String tenderId="0", pkgLotId="0", payTyp="";
                    String paymentTxt="", payLabel = "";
                    CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                    
                
                String referer = "";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");
                    
                }

                if ( hs.getAttribute("userTypeId")!= null) {
                    if("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())){
                        isBankUser=true; // userType is ScheduleBank";
                     }
                }

                if (request.getParameter("btnSearch") == null) {
                     if (request.getParameter("uId") != null){
                    if (request.getParameter("uId")!="" && !request.getParameter("uId").equalsIgnoreCase("null")){
                       srchUserId=request.getParameter("uId");
                       }
                    }
                }


                  if (request.getParameter("tenderId") !=null){
                        tenderId=request.getParameter("tenderId");
                    }
                     if (request.getParameter("lotId") != null){
                        if (request.getParameter("lotId")!="" && !request.getParameter("lotId").equalsIgnoreCase("null")){
                           pkgLotId=request.getParameter("lotId");
                        }
                    }

                     if (request.getParameter("payTyp") != null){
                        if (request.getParameter("payTyp")!="" && !request.getParameter("payTyp").equalsIgnoreCase("null")){
                           payTyp=request.getParameter("payTyp");
                           
                            if ("df".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Document Fees";
                                 payLabel = "Document Fees";
                             } else if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Tender Security";
                                 payLabel = "Tender/Proposal Security";
                             } else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Performance Security";
                                 payLabel = "Performance Security";
                             }
                        }
                    }
               
   boolean isTenderClosed = false;
                    boolean isFinalSubmissionDone = false;
                %>


                <div class="contentArea_1">
                    <div class="pageHead_1">Search e-mail ID for <%=payLabel%> Payment
                        <span style="float: right;" >
                            <a href="ForTenderPayment.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go To Main Payment Page</a>
                        </span>
<!--                        <span style="float:right;"><a href="<%=referer%>" class="action-button-goback">Go Back</a></span>-->
                    </div>

                <!--  START: TENDER INFO BAR -->
                <% pageContext.setAttribute("tenderId", tenderId); %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <!--  END: TENDER INFO BAR -->

                <%
                 /* Start: CODE TO GET TENDER STATUS */
                             boolean isCurTenCancelled=false;
                          List<SPTenderCommonData> listTenderStatus  = tenderCommonService.returndata("getTenderStatus", tenderId+"", null);
                           if (!listTenderStatus.isEmpty()) {
                              if("cancelled".equalsIgnoreCase(listTenderStatus.get(0).getFieldName1())){
                                isCurTenCancelled = true;
                              }
                          }
                           listTenderStatus = null;
              /* End: CODE TO GET TENDER STATUS */
                            List<SPTenderCommonData> lstCurBankUserRole = tenderCommonService.returndata("getBankUserRole", userId, null);
                            if (!lstCurBankUserRole.isEmpty()) {
                                if ("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                                    isBranchMaker = true;
                                } else if ("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                                    isBranchChecker = true;
                                } else if ("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                                    isBankChecker = true;
                                }
                                userBranchId = lstCurBankUserRole.get(0).getFieldName2();
                            }

                %>

                    <div class="tabPanelArea_1 t_space">
                       <div >
                            <%@include file="CommonPackageLotDescription.jsp" %>
                        </div>

                        
                        <div class="table-section-header_title">
                            Search Bidder/Consultant for Make / Release / Forfeit / Compensate / Extend / Cancel Payment
                        </div>

                        <form id="frmSearchTenderer" action="SearchTendererForTenPayment.jsp?tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>" method="POST">

                            <table width="100%" cellspacing="10" cellpadding="10" class="formBg_1 tableView_1" >
                                <tr>
                                    <td width="7%" class="t-align-left ff" style="vertical-align: text-top">e-mail ID : </td>
                                    <td width="22%" class="t-align-left">
                                        <input name="txtEmailId" type="text" class="formTxtBox_1" id="txtEmailId" style="width:95%;" />
                                    </td>
                                    <td width="71%" class="t-align-left" style="vertical-align: text-top">
                                        <label class="formBtn_1">
                                            <input id="btnSearch" type="submit" value="Search" />
                                            <input name="btnSearch" type="hidden" value="Search" />
                                        </label>
                                    </td>
                                </tr>
                            </table>
                        </form>

                        <%
                            int recordsCnt=0;
                            boolean isEmailVerifed=false, isPaymentDone=false, isPaymentVerified=false;
                            String bidderUserId="0", bidderEmail="", bidderCompany="", regPaymentId="0", payStatus="";

                            if (request.getParameter("btnSearch") != null || !"".equalsIgnoreCase(srchUserId)) {
                                String cmdName="", emailId="", bankUserId="", paidBranchId="0", payCon="";


                                if(!"".equalsIgnoreCase(srchUserId)){

                                    List<SPTenderCommonData> lstemails = tenderCommonService.returndata("getEmailIdfromUserId",srchUserId,null);
                                    if(!lstemails.isEmpty()){
                                        emailId=lstemails.get(0).getFieldName1();
                                    }
                                    //emailId = request.getParameter("txtEmailId");
                                } else {
                                    emailId = request.getParameter("txtEmailId");
                                }

                                //emailId = request.getParameter("txtEmailId");
                                cmdName = "SearchEmailForTenPayment";
                                bankUserId = userId;
                                if(pkgLotId!=null){
                                    if(!"0".equalsIgnoreCase(pkgLotId)){
                                        payCon="TP.tenderId=" + tenderId + " And TP.pkgLotId=" + pkgLotId + " And TP.paymentFor='" + paymentTxt + "'";
                                    } else {
                                        payCon="TP.tenderId=" + tenderId + " And TP.paymentFor='" + paymentTxt + "'";
                                    }
                                } else {
                                    payCon="TP.tenderId=" + tenderId + " And TP.paymentFor='" + paymentTxt + "'";
                                }

                                
                                for (SPTenderCommonData sptcd : tenderCommonService.returndata(cmdName, emailId, payCon)) {
                                    recordsCnt++;
                                    bidderUserId=sptcd.getFieldName1();
                                    bidderEmail=sptcd.getFieldName2();
                                    bidderCompany=sptcd.getFieldName4();
                                    if("yes".equalsIgnoreCase(sptcd.getFieldName5())){
                                        isEmailVerifed=true;
                                    }
                                    regPaymentId=sptcd.getFieldName6();
                                    //sptcd.getFieldName1();

                                    if(!"null".equalsIgnoreCase(sptcd.getFieldName7())){
                                        payStatus=sptcd.getFieldName7();
                                    } else {
                                        payStatus="Pending";
                                    }

                                    if("yes".equalsIgnoreCase(sptcd.getFieldName9())){
                                        isPaymentVerified=true;
                                    }

                                    paidBranchId = sptcd.getFieldName10();
                                }
                         
                        String bidderUid = "" ;
                        String procMethod ="";
                        List<SPTenderCommonData> lstBidderUid = tenderCommonService.returndata("getUserIdFromEmailID", emailId,null);        
                          bidderUid = lstBidderUid.get(0).getFieldName1();
                          
                        List<SPTenderCommonData> lstTenderInfo = tenderCommonService.returndata("getTenderInfoForDocView", tenderId, bidderUid);
                        if (!lstTenderInfo.isEmpty() && lstTenderInfo.size() > 0) {
                            procMethod = lstTenderInfo.get(0).getFieldName10();
                        }
                        boolean isEligible = false;       
                        List<SPTenderCommonData> lstUserBidPermision = tenderCommonService.returndata("getUserBidPermissionDetails", tenderId, bidderUid);
                        List<SPTenderCommonData> lstAppPermision = tenderCommonService.returndata("getAppPermissionDetails", tenderId, bidderUid);
                        
                        if((!lstAppPermision.isEmpty() && lstAppPermision.size()>0) && (!lstUserBidPermision.isEmpty() && lstUserBidPermision.size()>0) && !procMethod.equalsIgnoreCase("Goods")){
                            for(SPTenderCommonData appPermission : lstAppPermision){
                                String workType = appPermission.getFieldName1();
                                String workCategory = appPermission.getFieldName2();
                                for(SPTenderCommonData bidPermission : lstUserBidPermision){
                                    if(procMethod.equalsIgnoreCase(bidPermission.getFieldName1()) && workType.equalsIgnoreCase(bidPermission.getFieldName2()) && workCategory.equalsIgnoreCase(bidPermission.getFieldName3())){
                                           isEligible = true;
                                           break;
                                    
                                    }
                                }
                                if(isEligible) break;
                            }
                        }else if(procMethod.equalsIgnoreCase("Goods")){
                            for(SPTenderCommonData bidPermission : lstUserBidPermision){
                                    if(procMethod.equalsIgnoreCase(bidPermission.getFieldName1())){
                                           isEligible = true;
                                           break;
                                    
                                    }
                                }
                        }
                                
                                String pendingTxt="";
                                boolean allowPayment=false;
                                if("Pending".equalsIgnoreCase(payStatus)){
                                    if(isCurTenCancelled){
                                        pendingTxt="Payment not allowed: Tender has been canceled";
                                    }else{
                                    if (session.getAttribute("userId") != null) {
                                         commonSearchService.setLogUserId(session.getAttribute("userId").toString());
                                      }
                                    
                                    // Code to check whether or not this Tender is accessible for the current bidder
                                    List<SPCommonSearchData> lstChkTenderAccess = commonSearchService.searchData("ChkTenderAccess", paymentTxt, tenderId, pkgLotId, bidderUserId, null, null, null, null, null  );
                                    if (!lstChkTenderAccess.isEmpty()){
                                        if("Yes".equalsIgnoreCase(lstChkTenderAccess.get(0).getFieldName1())){
                                            allowPayment=true;
                                        } else {
                                            //allowPayment=false;
                                            pendingTxt="Payment not allowed: Bidder/Consultant is not mapped with this Tender.";
                                        }
                                    }
                                    lstChkTenderAccess = null;
                                    
                                    if (allowPayment) {
                                        // If Tender is accessible
                                    List<SPCommonSearchData> getPrePaymentList = commonSearchService.searchData("getPreTenderPaymentConditions", paymentTxt, tenderId, pkgLotId, bidderUserId, null, null, null, null, null  );

                                    if(!getPrePaymentList.isEmpty()){
                                        
                                        if ("df".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                             if(!"0.00".equalsIgnoreCase(getPrePaymentList.get(0).getFieldName1())){
                                                if("Yes".equalsIgnoreCase(getPrePaymentList.get(0).getFieldName2())){
                                                    allowPayment=true;
                                                } else {
                                                allowPayment = false;
                                                pendingTxt="Payment not allowed: Document Fees Submission Date has been lapsed";
                                                }
                                             } else {
                                                 allowPayment = false;
                                                pendingTxt="Payment not allowed: Documents are Free";
                                             }
                                         } else if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                             if(!"0.00".equalsIgnoreCase(getPrePaymentList.get(0).getFieldName1())){
                                                if("Yes".equalsIgnoreCase(getPrePaymentList.get(0).getFieldName2())){
                                                    if(!"0.00".equalsIgnoreCase(getPrePaymentList.get(0).getFieldName3())){
                                                        if("Paid".equalsIgnoreCase(getPrePaymentList.get(0).getFieldName4())){
                                                             allowPayment=true;
                                                        } else {
                                                            allowPayment = false;
                                                            pendingTxt="Payment not allowed: Document Fees Payment is Pending";
                                                        }
                                                    } else {
                                                        allowPayment=true;
                                                    }
                                                } else {
                                                allowPayment = false;
                                                pendingTxt="Last date for Tender Security Submission has been lapsed. Tender Security payment can’t be accepted now.";
                                                }
                                             } else {
                                                allowPayment = false;
                                                pendingTxt="Payment not allowed: Tender Security is Free";
                                             }
                                         } else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                          if(!"0.00".equalsIgnoreCase(getPrePaymentList.get(0).getFieldName1())){
                                            if("Yes".equalsIgnoreCase(getPrePaymentList.get(0).getFieldName2())){
                                                if("Approved".equalsIgnoreCase(getPrePaymentList.get(0).getFieldName3())){
                                                    allowPayment=true;
                                                } else {
                                                    allowPayment = false;
                                                    pendingTxt="Payment not allowed: LOA has not been Approved by Bidder/Consultant";
                                                }
                                                
                                            } else {
                                                allowPayment = false;
                                                pendingTxt="Payment not allowed: Performance Security Submission Date has been lapsed";
                                            }
                                          }   else {
                                                allowPayment = false;
                                                pendingTxt="Payment not allowed: Performance Security is Free";
                                             }
                                         }
                                    } else if(getPrePaymentList.isEmpty()){
                                        if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                            allowPayment = false;
                                            pendingTxt="Payment not allowed: LOA has not been issued";
                                        }
                                    }

                                    if(allowPayment){
                                    // Check For JVCA And Company Payment
                                        // Check For Document Fees / Tender Security Payment Only
                                        if ("df".equalsIgnoreCase(request.getParameter("payTyp")) || "ts".equalsIgnoreCase(request.getParameter("payTyp"))) {

                                            List<SPCommonSearchData> getJVCACompanyPay =
                                                commonSearchService.searchData("getJVCACompanyPay", paymentTxt,bidderUserId, tenderId, pkgLotId, null, null, null, null, null);

                                        if(!getJVCACompanyPay.isEmpty()){
                                            if("Yes".equalsIgnoreCase(getJVCACompanyPay.get(0).getFieldName1())){
                                                allowPayment=false; // JVCA Partner has already Paid
                                                if("Document Fees".equalsIgnoreCase(paymentTxt)){
                                                    if ("Yes".equalsIgnoreCase(getJVCACompanyPay.get(0).getFieldName3())){
                                                        pendingTxt="Payment not allowed: You can’t purchase Tender Document because any of your JVCA member has already purchased Tender Document.";
                                                    } else {
                                                        pendingTxt="Payment not allowed: You can’t purchase Tender Document because JVCA in which you are a member, has already purchased Tender Document.";
                                                    }
                                                    //pendingTxt="Payment not allowed: One of the JVCA Partner has purchased tender document and hence you cannot participate in this Tender as JVCA.";
                                                } else if("Tender Security".equalsIgnoreCase(paymentTxt)){
                                                    pendingTxt="Payment not allowed: One of the JVCA Partner has paid Tender Security and hence you cannot pay Tender Security  in this Tender as JVCA.";
                                                }
                                            } else if("Yes".equalsIgnoreCase(getJVCACompanyPay.get(0).getFieldName2())){
                                                allowPayment=false; // Payment is already done
                                                if("Document Fees".equalsIgnoreCase(paymentTxt)){
                                                    pendingTxt="Payment not allowed: Payment has already been done for this Company / Bidder";
                                                } else if("Tender Security".equalsIgnoreCase(paymentTxt)){
                                                    pendingTxt="Payment not allowed: Payment has already been done for this Company / Bidder";
                                                }
                                            }
                                        }

                                        }
                                    }

                                    }

                                                                       }
                                }

                                 String newExtId="0";

                                if(recordsCnt>0){%>
                                    <%if(isEmailVerifed){ %>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
<!--                                            <th width="30%" class="t-align-center">Company Name</th>-->
                                            <th class="t-align-center" width="25%">Email ID</th>
                                            <th class="t-align-center" width="20%">Company Name</th>
                                            <th class="t-align-center" width="10%">Payment Status</th>
                                            <th class="t-align-center" width="40%">Action</th>
                                        </tr>
                                        <tr>
<!--                                            <td><%=bidderCompany%></td>-->
                                            <td><%=bidderEmail%></td>
                                            <td><%=bidderCompany%></td>
                                            <td class="t-align-center">
                                                <% if (!"Pending".equalsIgnoreCase(payStatus) && !isPaymentVerified) {%>
                                                Verification is Pending
                                                <%} else {%>
                                                    <%=payStatus%>
                                                <%}%>

                                            </td>
                                            <td class="t-align-center">
                                                <%if("pending".equalsIgnoreCase(payStatus)){%>
                                                <% if (!isEligible) { %>
                                                 <span style="color:brown;">This bidder is not eligible for this tender!!!</span>
                                                <% } else {%>
                                                    <%if(allowPayment){%>
                                                    <a href="TenderPayment.jsp?uId=<%=bidderUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>&parentLink=<%=ParentLink%>">Make Payment</a>
                                                    <%} else {%>
                                                    <a href="#" onclick="return ErrorMsg('<%=pendingTxt%>');">Make Payment</a>
                                                    <% } %>
                                                
                                                <% } } else {%>
                                                <%if(paidBranchId.equalsIgnoreCase(userBranchId)) {%>

                                                   <%if(isPaymentVerified){%>
                                                   <a href="TenderPaymentDetails.jsp?payId=<%=regPaymentId%>&uId=<%=bidderUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>">View Payment Details</a>
                                                        <%
                                                        
                                                        boolean showExtensionLink=false;
                                                        boolean showCancelLink = false;
                                                        String latestExtId="0";

                                                        if ("Tender Security".equalsIgnoreCase(paymentTxt)) {

                                                            //for Cancel link
                                                            List<SPCommonSearchData> lstCancelLinkStatus =
                                                            commonSearchService.searchData("getCancelPaymentLinkStatus", paymentTxt,bidderUserId, tenderId, pkgLotId, regPaymentId, null, null, null, null);
                                                            if(!lstCancelLinkStatus.isEmpty()){
                                                                if("Yes".equalsIgnoreCase(lstCancelLinkStatus.get(0).getFieldName1())){
                                                                     isTenderClosed=true;
                                                                }
                                                                if("Yes".equalsIgnoreCase(lstCancelLinkStatus.get(0).getFieldName2())){
                                                                     isFinalSubmissionDone=true;
                                                                }
                                                            }
                                                            lstCancelLinkStatus = null;

                                                            if(isPaymentVerified && !isFinalSubmissionDone && !isTenderClosed){
                                                                if("paid".equalsIgnoreCase(payStatus)){
                                                                    showCancelLink=true;
                                                                }
                                                            }
                                                                                                                       //for Tender Security Payment Case
                                                                List<SPTenderCommonData> lstTSDtExtId = tenderCommonService.returndata("getValidityRefId", tenderId, null);
                                                                if (!lstTSDtExtId.isEmpty()) {
                                                                    if (lstTSDtExtId.get(0).getFieldName1() != null) {
                                                                        latestExtId = lstTSDtExtId.get(0).getFieldName1();
                                                                    }
                                                                }

                                                                List<SPTenderCommonData> lstCurrTSDtExtId = tenderCommonService.returndata("getTenderSecurityPayCurrExtId", regPaymentId, null);
                                                                
                                                                if (!lstCurrTSDtExtId.isEmpty()) {

                                                                        if(!"0".equalsIgnoreCase(latestExtId)){
                                                                            if(Integer.parseInt(latestExtId) > Integer.parseInt(lstCurrTSDtExtId.get(0).getFieldName1())){

                                                                                //List<SPTenderCommonData> lstTenSecExtLinkStatus =
                                                                                  //      tenderCommonService.returndata("getTenSecExtLinkStatus", lstCurrTSDtExtId.get(0).getFieldName1(), latestExtId);

                                                                                List<SPCommonSearchData> lstTenSecExtLinkStatus =
                                                                                        commonSearchService.searchData("getTenSecExtLinkStatusMore", lstCurrTSDtExtId.get(0).getFieldName1(), latestExtId, tenderId, bidderUserId, regPaymentId, null, null, null, null);
                                                                                
                                                                                if(!lstTenSecExtLinkStatus.isEmpty()){
                                                                                    if("Yes".equalsIgnoreCase(lstTenSecExtLinkStatus.get(0).getFieldName1())){
                                                                                      showExtensionLink=true;
                                                                                    }
                                                                                }
                                                                                lstTenSecExtLinkStatus = null;

                                                                                //showExtensionLink=true;
                                                                            }
                                                                        }
                                                                    
                                                                }
                                                            }                                                       

                                                        %>

                                                        <%
                                                           List<SPTenderCommonData> lstPaymentActionRequest =
                                                                   tenderCommonService.returndata("getRequestActionFromPaymentId", regPaymentId, null);
                                                           if(!lstPaymentActionRequest.isEmpty()){
                                                            if(!isBranchMaker){
                                                            if(!"0".equalsIgnoreCase(lstPaymentActionRequest.get(0).getFieldName1())){
                                                            if("Release".equalsIgnoreCase(lstPaymentActionRequest.get(0).getFieldName2())){
                                                            %>

                                                            &nbsp;&nbsp;|&nbsp;&nbsp;<a href="PostPaymentProcess.jsp?reqId=<%=lstPaymentActionRequest.get(0).getFieldName1()%>&payId=<%=regPaymentId%>&uId=<%=bidderUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>"><%=lstPaymentActionRequest.get(0).getFieldName2()%></a>
                                                            <%
                                                            }
                                                           }}
                                                           } else if(showExtensionLink){
                                                        %>
                                                        &nbsp;&nbsp;|&nbsp;&nbsp;<a href="TenderPayment.jsp?extId=<%=latestExtId%>&extPayId=<%=regPaymentId%>&uId=<%=bidderUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>">Extend Tender Security</a>
                                                        <%}%>

                                                            <%if(showCancelLink){%>
                                                                &nbsp;&nbsp;|&nbsp;&nbsp;<a href="PostPaymentProcess.jsp?reqId=0&opTyp=cancel&payId=<%=regPaymentId%>&uId=<%=bidderUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>">Cancel Payment</a>
                                                                &nbsp;&nbsp;|&nbsp;&nbsp;<a href="PostPaymentProcess.jsp?reqId=0&opTyp=release&payId=<%=regPaymentId%>&uId=<%=bidderUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>">Release Payment</a>
                                                            <%}%>

                                                            <%if(!isTenderClosed) {%>
                                                                <%if("cancelled".equalsIgnoreCase(payStatus)){%>
                                                                    &nbsp;&nbsp;|&nbsp;&nbsp;<a href="TenderPayment.jsp?updPayId=<%=regPaymentId%>&uId=<%=bidderUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>">Make Payment</a>
                                                                <%} else if ("released".equalsIgnoreCase(payStatus)) {%>
                                                                    &nbsp;&nbsp;|&nbsp;&nbsp;<a href="TenderPayment.jsp?updPayId=<%=regPaymentId%>&uId=<%=bidderUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>">Make Payment</a>
                                                                <%}%>
                                                            <%}%>

                                                   <%} else {%>
                                                    <a href="TenderPayment.jsp?payId=<%=regPaymentId%>&uId=<%=bidderUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>&action=edit">Edit Payment Details</a>
                                                    &nbsp;&nbsp;|&nbsp;&nbsp;<a href="TenderPaymentDetails.jsp?payId=<%=regPaymentId%>&uId=<%=bidderUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>">View Payment Details</a>
                                                    <%}%>

                                                <%} else {%>
                                                    Payment already done.
                                                    <%if(isPaymentVerified){%>
<!--                                                    <a href="TenderPaymentDetails.jsp?payId=<%=regPaymentId%>&uId=<%=bidderUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>">View Payment Details</a>-->
                                                    <%
                                                        boolean showExtensionLink=false;
                                                        String latestExtId="0";

                                                        if ("Tender Security".equalsIgnoreCase(paymentTxt)) {

                                                                //for Tender Security Payment Case
                                                                List<SPTenderCommonData> lstTSDtExtId = tenderCommonService.returndata("getValidityRefId", tenderId, null);
                                                                if (!lstTSDtExtId.isEmpty()) {
                                                                    if (lstTSDtExtId.get(0).getFieldName1() != null) {
                                                                        latestExtId = lstTSDtExtId.get(0).getFieldName1();
                                                                    }
                                                                }

                                                                List<SPTenderCommonData> lstCurrTSDtExtId = tenderCommonService.returndata("getTenderSecurityPayCurrExtId", regPaymentId, null);
                                                                
                                                                if (!lstCurrTSDtExtId.isEmpty()) {
                                                                        /*
                                                                        if(!"0".equalsIgnoreCase(latestExtId)){
                                                                            if(Integer.parseInt(latestExtId) > Integer.parseInt(lstCurrTSDtExtId.get(0).getFieldName1())){
                                                                                showExtensionLink=true;
                                                                            }
                                                                        }
                                                                        */

                                                                     if(!"0".equalsIgnoreCase(latestExtId)){
                                                                            if(Integer.parseInt(latestExtId) > Integer.parseInt(lstCurrTSDtExtId.get(0).getFieldName1())){

                                                                                //List<SPTenderCommonData> lstTenSecExtLinkStatus =
                                                                                  //      tenderCommonService.returndata("getTenSecExtLinkStatus", lstCurrTSDtExtId.get(0).getFieldName1(), latestExtId);

                                                                                List<SPCommonSearchData> lstTenSecExtLinkStatus =
                                                                                        commonSearchService.searchData("getTenSecExtLinkStatusMore", lstCurrTSDtExtId.get(0).getFieldName1(), latestExtId, tenderId, bidderUserId, regPaymentId, null, null, null, null);

                                                                                if(!lstTenSecExtLinkStatus.isEmpty()){
                                                                                    if("Yes".equalsIgnoreCase(lstTenSecExtLinkStatus.get(0).getFieldName1())){
                                                                                      showExtensionLink=true;
                                                                                    }
                                                                                }
                                                                                lstTenSecExtLinkStatus = null;

                                                                                //showExtensionLink=true;
                                                                            }
                                                                        }

                                                                }


                                                            }
 
                                                       

                                                        %>

                                                        <% if(showExtensionLink){%>
                                                            &nbsp;&nbsp;|&nbsp;&nbsp;<a href="TenderPayment.jsp?extId=<%=latestExtId%>&extPayId=<%=regPaymentId%>&uId=<%=bidderUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>">Issue</a>
                                                        <%}%>

                                                         <%if(!isTenderClosed) {%>
                                                                <%if("cancelled".equalsIgnoreCase(payStatus)){%>
                                                                    &nbsp;&nbsp;|&nbsp;&nbsp;<a href="TenderPayment.jsp?updPayId=<%=regPaymentId%>&uId=<%=bidderUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>">Make Payment</a>
                                                                <%} else if ("released".equalsIgnoreCase(payStatus)) {%>
                                                                    &nbsp;&nbsp;|&nbsp;&nbsp;<a href="TenderPayment.jsp?updPayId=<%=regPaymentId%>&uId=<%=bidderUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>">Make Payment</a>
                                                                <%}%>
                                                            <%}%>

                                                    <%} else {%>
                                                    Verification Pending
                                                    <%}%>
                                                <%}%>

                                                <%}%>
                                            </td>
                                        </tr>
                                    </table>
                                    <%} else {%>
                                    <div class="t-align-center t_space">
                                        <strong>Bidder/Consultant e-mail ID (<%=emailId%>) is not verified.</strong></div>
                                    <%}%>
                                <%} else {%>
                                <div class="t-align-center t_space">
                                    <strong>Bidder/Consultant with e-mail ID (<%=emailId%>) does not exist.</strong></div>
                                <%}
                            }
                        %>

                        
                    </div>

<!--                    <div class="tabPanelArea_1 t_space">-->
                        <% //@include  file="TenPaymentDailyTrans.jsp" %>
<!--                </div>-->

                <div class="tabPanelArea_1">
                    <span style="float: right;" ><a target="_blank" href="SearchTenderPayments.jsp" class="action-button-viewTender">View Today's / All Transactions</a></span>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <!--                                            <th width="30%" class="t-align-center">Company Name</th>-->
                            <th width="4%" class="t-align-center" width="26%">Sl. No.</th>
                            <th width="30%" class="t-align-center" width="26%">e-mail ID</th>
                            <th width="31%" class="t-align-center" width="26%">Bidder/Consultant</th>
                            <th width="15%" class="t-align-center" width="13%">Payment Status</th>
                            <th width="20%" class="t-align-center" width="13%">Detail</th>
                        </tr>
                        <%

  int intPay=0;
  boolean b_isVerified = true;
                    List<SPCommonSearchData> getList =  commonSearchService.searchData("getTenderPays", paymentTxt, tenderId, pkgLotId, userId, null, null, null, null, null);
                        for (SPCommonSearchData objSP : getList) 
                    {
                            if("yes".equalsIgnoreCase(objSP.getFieldName11())){
                                b_isVerified = true;
                            }else{
                                b_isVerified = false;
                            }
                        intPay++;

%>
<tr
     <%if(Math.IEEEremainder(intPay,2)==0) {%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>
    >

</tr>
<td class="t-align-center"><%=intPay%></td>
<td><%=objSP.getFieldName3()%></td>
<td><%=objSP.getFieldName4()%></td>
<%if(b_isVerified){%>
<td class="t-align-center"><%=objSP.getFieldName5()%></td>
<%}else{%>
<td class="t-align-center">Verification Pending</td>
<%}%>
<td class="t-align-center">
    <%if(!b_isVerified){%>
    <a href="TenderPayment.jsp?payId=<%=objSP.getFieldName1()%>&uId=<%=objSP.getFieldName2()%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>&action=edit">Edit</a>
    <%}%>
<a href="TenderPaymentDetails.jsp?payId=<%=objSP.getFieldName1()%>&uId=<%=objSP.getFieldName2()%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>">View</a>
</td>
<%}%>

<%if(intPay==0){%>
<tr>
    <td colspan="5">No Record Found</td>
</tr>
<%}%>

                    </table> 
                </div>

                </div>

                <!--Dashboard Content Part End-->



                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->


        </div>
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
