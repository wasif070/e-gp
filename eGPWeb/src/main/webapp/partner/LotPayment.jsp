<%-- 
    Document   : LotPayment
    Created on : Mar 8, 2011, 11:03:41 AM
    Author     : Karan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Lot Wise Payment</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
         <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
         <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<body>
<div class="dashboard_div">
      <%
      //TenderCommonService tcs = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
      TenderCommonService objTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
  String userId="", currUserTypeId="", tenderId="", payTyp="", paymentTxt="";;
  boolean isTenderer=false, isPEUser=false, isBankUser=false;
  boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false;
  String userBranchId="";

  HttpSession hs = request.getSession();
     if (hs.getAttribute("userId") != null) {
             userId = hs.getAttribute("userId").toString();
      }

   if(request.getParameter("tenderId")!=null){
    tenderId=request.getParameter("tenderId");
   }

    if (request.getParameter("payTyp") != null) {
        if (request.getParameter("payTyp") != "" && !request.getParameter("payTyp").equalsIgnoreCase("null")) {
            payTyp = request.getParameter("payTyp");

            if ("df".equalsIgnoreCase(request.getParameter("payTyp"))) {
                paymentTxt = "Document Fees";
            } else if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {
                paymentTxt = "Tender/Proposal Security";
            } else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {
                paymentTxt = "Performance Security";
            }
        }
    }

   /* Start: CODE TO SET CURRENT USER TYPE */
      if (hs.getAttribute("userTypeId") != null) {
             currUserTypeId = hs.getAttribute("userTypeId").toString();
             if(currUserTypeId.equalsIgnoreCase("2")){
                isTenderer=true; // userType is Tenderer";
             }else if(currUserTypeId.equalsIgnoreCase("3")){
                isPEUser=true; // userType is Officer";
             }else if(currUserTypeId.equalsIgnoreCase("7")){
                isBankUser=true; // userType is ScheduleBank";
             }
       }
       /* End: CODE TO SET CURRENT USER TYPE */

       /* Start: CODE TO CHECK PE USER */
             boolean objIsCurUserPE = false;
              List<SPTenderCommonData> objListChkCurUserPE  = objTenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, userId);

              if (!objListChkCurUserPE.isEmpty()) {
                  if(userId.equalsIgnoreCase(objListChkCurUserPE.get(0).getFieldName1())){
                    objIsCurUserPE = true;
                  }
              }
              objListChkCurUserPE = null;
              /* End: CODE TO CHECK PE USER */


%>

  <!--Dashboard Header Start-->
  <%@include  file="../resources/common/AfterLoginTop.jsp" %>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
  <div class="pageHead_1">
      <%=paymentTxt%> Payment
      <span style="float: right;" >
          <% if(isBankUser) {%>
          <a href="ForTenderPayment.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a>
          <%} else if (isTenderer) {%>
          <a href="ForViewTenderPayment.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a>
          <%} else if (objIsCurUserPE) {%>
          <a href="ForTenderPayment.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a>
          <%}%>
        </span>
  </div>



   <% // Variable tenderId is defined by u on ur current page.
    pageContext.setAttribute("tenderId", tenderId);
  %>
  <%@include file="../resources/common/TenderInfoBar.jsp" %>
  <div>&nbsp;</div>

    <%
    
    if (session.getAttribute("userId") != null) {
                tenderCommonService.setLogUserId(session.getAttribute("userId").toString());
    }
    List<SPTenderCommonData> lstCurBankUserRole = tenderCommonService.returndata("getBankUserRole", userId, null);
    if (!lstCurBankUserRole.isEmpty()) {
        if ("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
            isBranchMaker = true;
        } else if ("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
            isBranchChecker = true;
        } else if ("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
            isBankChecker = true;
        }
        userBranchId = lstCurBankUserRole.get(0).getFieldName2();
    }

 
%>

   <%
    boolean lotwisedebar=false;
    if (isTenderer) {
         pageContext.setAttribute("tab", "7");
    %>
    <%@include file="../tenderer/TendererTabPanel.jsp" %>
    <%lotwisedebar=is_debared;} else if (isPEUser) {
        pageContext.setAttribute("tab", "8");
    %>
    <%@include file="../officer/officerTabPanel.jsp" %>
    <%}%>
  <%if(!lotwisedebar){%>

  <div class="tabPanelArea_1">

<div >
    <%@include file="CommonPackageLotDescription.jsp" %>
</div>
<%if (isBranchMaker) {%>
    <table width="100%" cellspacing="0" class="tableList_1 t_space">
        <tr>
            <th width="7%" class="t-align-center">Lot. No.</th>
            <th width="67%" class="t-align-center">Lot Description</th>
            <th class="t-align-center" width="10%">Action</th>
        </tr>

    <%  int j = 0;
       

     for (SPTenderCommonData sptcd : tenderCommonService.returndata("gettenderlotbytenderidForPayment", tenderId, payTyp)){
         if ("Package".equalsIgnoreCase(sptcd.getFieldName1())){
            response.sendRedirect("SearchTendererForTenPayment.jsp?tenderId="+tenderId+"&payTyp="+payTyp);
         }

         j++;  %>
         <tr
            <%if(Math.IEEEremainder(j,2)==0) {%>
                class="bgColor-Green"
            <%} else {%>
                class="bgColor-white"
            <%}%>
             >
             <td class="t-align-center"><%=sptcd.getFieldName1()%></td>
             <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
             <td class="t-align-center">
                 <a href="SearchTendererForTenPayment.jsp?tenderId=<%=tenderId%>&lotId=<%=sptcd.getFieldName3()%>&payTyp=<%=payTyp%>">Payment</a>
             </td>
         </tr>
   <%}%>
</table>
 <% if (j == 0) { %>
                <br/>
               <div class="t-align-center"><span style="color: red; font-size: 14px;">There is no Lot of "Financial Institution Payment" for this Tender </span></div>
         <% } %>  
   
<%} else if (isTenderer) {%>
<br/>
<div class="t-align-center"><h2>Financial Institution Payment</h2></div>
<table width="100%" cellspacing="0" class="tableList_1 t_space">
        <tr>
            <th width="7%" class="t-align-center">Lot. No.</th>
            <th width="57%" class="t-align-center">Lot Description</th>
            <th width="10%" class="t-align-center">Status</th>
            <th class="t-align-center" width="10%">Action</th>
        </tr>

    <%  int j = 0;
        CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
        //List<SPCommonSearchData> submitDateSts= commonSearchService.searchData("chkSubmissionDt", tId, null, null, null, null, null, null, null, null);
        List<SPCommonSearchData> lstLots= commonSearchService.searchData("gettenderlotbytenderidForViewPayment", tenderId, payTyp, userId, null, null, null, null, null, null);
        
     //for (SPTenderCommonData sptcd : tenderCommonService.returndata("gettenderlotbytenderidForPayment", tenderId, payTyp)){
        for (SPCommonSearchData sptcd : lstLots){
         if ("Package".equalsIgnoreCase(sptcd.getFieldName1())){
            response.sendRedirect("ViewTenderPaymentDetails.jsp?payId="+sptcd.getFieldName4()+"&uId="+userId+"&tenderId="+tenderId+"&lotId=0&payTyp="+payTyp);
         }

         j++;  %>
         <tr
            <%if(Math.IEEEremainder(j,2)==0) {%>
                class="bgColor-Green"
            <%} else {%>
                class="bgColor-white"
            <%}%>
             >
             <td class="t-align-center"><%=sptcd.getFieldName1()%></td>
             <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
             <td class="t-align-center"><%=sptcd.getFieldName5()%></td>
             <td class="t-align-center">
                 <a href="ViewTenderPaymentDetails.jsp?payId=<%=sptcd.getFieldName4()%>&uId=<%=userId%>&tenderId=<%=tenderId%>&lotId=<%=sptcd.getFieldName3()%>&payTyp=<%=payTyp%>">View</a>
             </td>
         </tr>
     <%}%> 
</table>
    <% if (lstLots.size() == 0) { %>
               <div class="t-align-center"><span style="color: red; font-size: 14px">No Payment Found</span></div>
         <% } %>
<br/>
<div class="t-align-center"><h2>Bid Securing Declaration</h2></div>
   <table width="100%" cellspacing="0" class="tableList_1 t_space">
        <tr>
            <th width="7%" class="t-align-center">Lot. No.</th>
            <th width="50%" class="t-align-center">Lot Description</th>
            <th width="20%" class="t-align-center">Declaration Status</th>            
        </tr>

    <%  int k = 0; 
     boolean show = false;
        List<SPCommonSearchData> lstLots1= commonSearchService.searchLotbyDeclaration("searchLotbyDeclaration", tenderId, payTyp, userId, null, null, null, null, null, null);
        List<SPCommonSearchData> lstLotsParticipated= commonSearchService.searchLotbyDeclaration("searchLotbyParticipation", tenderId, userId, null, null, null, null, null, null, null);
        for (SPCommonSearchData sptcd1 : lstLots1){
          if (!commonSearchService.checkLotidPaid("checkLotidPaid",tenderId, payTyp, userId, sptcd1.getFieldName3(), null, null, null, null, null))
            {
                show = commonSearchService.checkLotidPaid("checkLotidDeclared",tenderId, payTyp, userId, sptcd1.getFieldName3(), null, null, null, null, null);
         k++;  %>
         <tr
            <%if(Math.IEEEremainder(k,2)==0) {%>
                class="bgColor-Green"
            <%} else {%>
                class="bgColor-white"
            <%}%>
             >
             <td class="t-align-center"><%=sptcd1.getFieldName1()%></td>
             <td class="t-align-left">
                 <%=sptcd1.getFieldName2()%>
                 <%
                     for (SPCommonSearchData LotParticipated : lstLotsParticipated)
                     {
                         if(sptcd1.getFieldName3().equalsIgnoreCase(LotParticipated.getFieldName1()))
                         {
                             %>
                                <span style="color: green; font-weight: bold;">(You have participated)</span>
                             <%
                         }
                     }
                 %>
             </td>
             <%if(show){%>
             <td class="t-align-center"><span style="color: black; font-weight: bold;">Declaration Submitted</span></td>
             <%}else {%>
             <td class="t-align-center"> 
                 <div id="declaration_<%=k%>"><input type="checkbox" id="chkDeclaration_<%=k%>" name="chkDeclaration_<%=k%>" onchange="chkmsg(<%=k%>);"/>&nbsp;<a href="javascript:void(0)" onclick="showDeclaration();">Declaration</a>  &nbsp; 
                     <label class="formBtn_1"> <input type="submit" id="sbmDeclaration_<%=k%>" name="sbmDeclaration_<%=k%>" value="Submit Declaration" onclick="ValidationandPost(<%=k%>);"/></label>
                 </div>  
                 <div><span id="msgDeclaration_<%=k%>" style="color: red;"></span></div>
             </td>
             <%}%>
         </tr>
             <input type="hidden" id="id_<%=k%>" name="id_<%=k%>" value="<%=k%>"/>
             <input type="hidden" id="hdntenderid_<%=k%>" name="hdntenderid_<%=k%>" value="<%=tenderId%>"/>
             <input type="hidden" id="hdnlotid_<%=k%>" name="hdnlotid_<%=k%>" value="<%=sptcd1.getFieldName3()%>"/>
             <input type="hidden" id="hdnuserid_<%=k%>" name="hdnuserid_<%=k%>" value="<%=userId%>"/>
             <%}
            }%>         
</table>
        <% if (lstLots1.size() == 0) { %>
               <div class="t-align-center"><span style="color: red; font-size: 14px">No Declaration Found</span></div>
         <% } %>
   
<%} else if (objIsCurUserPE) {%>
 <table width="100%" cellspacing="0" class="tableList_1 t_space">
        <tr>
            <th width="7%" class="t-align-center">Lot. No.</th>
            <th width="67%" class="t-align-center">Lot Description</th>
            <th class="t-align-center" width="10%">Action</th>
        </tr>

    <%  int j = 0;


     for (SPTenderCommonData sptcd : tenderCommonService.returndata("gettenderlotbytenderidForPayment", tenderId, payTyp)){
         if ("Package".equalsIgnoreCase(sptcd.getFieldName1())){
            response.sendRedirect("PaidTendererList.jsp?tenderId="+tenderId+"&payTyp="+payTyp);
         }

         j++;  %>
         <tr
            <%if(Math.IEEEremainder(j,2)==0) {%>
                class="bgColor-Green"
            <%} else {%>
                class="bgColor-white"
            <%}%>
             >
             <td class="t-align-center"><%=sptcd.getFieldName1()%></td>
             <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
             <td class="t-align-center">
                 <a href="PaidTendererList.jsp?tenderId=<%=tenderId%>&lotId=<%=sptcd.getFieldName3()%>&payTyp=<%=payTyp%>">View</a>
             </td>
         </tr>
   <%}%>
</table>
<%}%>


  </div>
<script type="text/javascript">
                function ValidationandPost(id){
                    var check = true;
                    if(document.getElementById("chkDeclaration_"+id).checked == false){
                        document.getElementById("msgDeclaration_"+id).innerHTML='Please Accept the Declaration';  
                        check = false;
                    }
                    if(check==true){
                         $.ajax({
                            url: "<%=request.getContextPath()%>/APPServlet?param1="+$('#hdntenderid_'+id).val()+"&param2="+$('#hdnlotid_'+id).val()+"&funName=addBidDeclaration",
                            method: 'POST',
                            async: false,
                            success: function(j) {
                              // alert(j); 
                               if(j=='Success'){
                                 document.getElementById("msgDeclaration_"+id).innerHTML='Declaration Submitted Sucessfully';
                                 $('#msgDeclaration_'+id).css("color","green");
                                 $('#declaration_'+id).hide();
                               }
                               if(j=='Fail'){
                                 document.getElementById("msgDeclaration_"+id).innerHTML='Declaration submission Failed';
                                 $('#msgDeclaration_'+id).css("color","red");
                               }
                            }
                        });   
                    }
             }     
    
            function showDeclaration(){                         
                var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
                height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
                var top = ((height / 2) - (450 / 2)) + dualScreenTop;
                
                newwindow = window.open("ViewDeclaration.jsp",'name','scrollbars=1 width=' + 450 + ', height=' + 450 + ', top=' + top + ', left=' + 120);
                if (window.focus) { 
                    newwindow.focus() 
                }
                return false;
            }
             
            function chkmsg(id){
                        if (document.getElementById("chkDeclaration_"+id).checked == false) {
                            document.getElementById("msgDeclaration_"+id).innerHTML='Please Accept the Declaration';
                        }  
                        else{
                            document.getElementById("msgDeclaration_"+id).innerHTML='';
                        }

                    }

    </script>
  
  <%}%>
    <div>&nbsp;</div>
    </div>
    <!--Dashboard Content Part End-->
    <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
</div>
</body>

            <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
            
        </script>
</html>
