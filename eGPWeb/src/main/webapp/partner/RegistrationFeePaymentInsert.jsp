<%-- 
    Document   : RegistrationFeePaymentInsert
    Created on : Feb 3, 2011, 7:34:27 PM
    Author     : Karan
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import="java.lang.String" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<%

TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        if (session.getAttribute("userId") != null) {
              tenderCommonService.setLogUserId(session.getAttribute("userId").toString());
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat formatVisible = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatVisibleWithMonthNm = new SimpleDateFormat("dd-MMM-yyyy");
        HandleSpecialChar handleSpecialChar = new HandleSpecialChar();

          String emailId="",payUserId="", payDt=formatVisible.format(new Date()), payDtWithMnthNm=formatVisibleWithMonthNm.format(new Date());
          payDt=payDt.replace("-", "/");
          //String amount= XMLReader.getMessage("localTenderRegFee");  //"5000";
          //String amountDollar=XMLReader.getMessage("intTednerRegFee");  //"5000";

          boolean isBankUser=false, isRenew=false, isEditCase=false, isChecker=false, isAmtError=true, isFreezed = false;
          String strPartTransId="0", userId="", action="", editPaymentId="0";

           HttpSession hs = request.getSession();
             if (hs.getAttribute("userId") != null) {
                     userId = hs.getAttribute("userId").toString();
              } /*else {response.sendRedirectFliter(request.getContextPath() + "/SessionTimedOut.jsp");}*/

            if (hs.getAttribute("govUserId") != null) {
                     strPartTransId = hs.getAttribute("govUserId").toString();
              }
          
          if (request.getParameter("uId") != null) {
            if (request.getParameter("uId") != "" && !request.getParameter("uId").equalsIgnoreCase("null")) {
                payUserId = request.getParameter("uId");
            }
          }
            
          if (request.getParameter("action") != null) {
            if (request.getParameter("action") != "" && !request.getParameter("action").equalsIgnoreCase("null")) {
                action = request.getParameter("action");
                
                if ("edit".equalsIgnoreCase(action)) {
                     if (request.getParameter("payId") != null) {
                         if (request.getParameter("payId") != "0" && request.getParameter("payId") != "" && !request.getParameter("payId").equalsIgnoreCase("null")) {
                             editPaymentId = request.getParameter("payId");
                             isEditCase = true;
                         }
                     }
                 }
            }
          }                    
            

            if ( hs.getAttribute("userTypeId")!= null) {
                    if("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())){
                        isBankUser=true; // userType is ScheduleBank";
                     }
                }
          // START CODE: TO INSERT PAYMENT DATA
            if (request.getParameter("btnSubmit") != null) {
                if (request.getParameter("freezed") != null && "yes".equalsIgnoreCase((request.getParameter("freezed")))){
                    isFreezed = true;
                }
                String path="";
                if (request.getParameter("action") != null) {
                    if (request.getParameter("action") != "" && !request.getParameter("action").equalsIgnoreCase("null")) {
                        action = request.getParameter("action");
                    }
                  }

                if("renew".equalsIgnoreCase(action)){
                 isRenew=true;
                }

                String paymentFor="", bankName = "", branchName = "", currency="", typOfPay = "", insRefNo = "", issuanceBank="", issuanceBranch="", issuanceDate="" , validityDt = "", dtOfPayment="", comments = "", paymentMode="", status="", validityPeriodRef="";
                String fnlAmt="0", currencySymbol="";
                paymentFor = "Registration Fee";
                bankName=request.getParameter("hdnBankNm");
                branchName=request.getParameter("hdnBranch");
                currency=request.getParameter("cmbCurrency");
                typOfPay = request.getParameter("cmbPayment");
                validityPeriodRef=request.getParameter("cmbValidityPeriod");
                
                if("Pay Order".equalsIgnoreCase(typOfPay)){
                    insRefNo = request.getParameter("txtInsRefNo");
                    issuanceBank=request.getParameter("txtIssuanceBankNm");
                    issuanceBranch=request.getParameter("txtIssuanceBranch");
                    issuanceDate = request.getParameter("txtIssuanceDt");
                    validityDt = request.getParameter("txtValidityDt");
                } else  if("Account to Account Transfer".equalsIgnoreCase(typOfPay)){
                    insRefNo = request.getParameter("txtInsRefNo");
                    issuanceBranch=request.getParameter("txtIssuanceBranch");
                }

                if("BTN".equalsIgnoreCase(currency)){
                    currencySymbol="Nu.";
                    //fnlAmt=request.getParameter("hdnAmount");
                    fnlAmt=request.getParameter("hdnFinalAmount");
                } else if("USD".equalsIgnoreCase(currency)){
                    currencySymbol="$";
                    //fnlAmt=request.getParameter("hdnDollarAmount");
                    fnlAmt=request.getParameter("hdnFinalAmount");
                }
                else if("Nu.".equalsIgnoreCase(currency)){
                    currencySymbol="Nu.";
                    //fnlAmt=request.getParameter("hdnDollarAmount");
                    fnlAmt=request.getParameter("hdnFinalAmount");
                }


                
                if(Double.parseDouble(fnlAmt) > 0 ){
                    isAmtError = false;
                }

                if(!isAmtError){
                    
                dtOfPayment = request.getParameter("hdnDateOfPayment");
                
                comments = request.getParameter("txtComments");

                if(isEditCase){
                     status = request.getParameter("hdnEditPayStatus");
                     editPaymentId = request.getParameter("hdnEditPaymentId");
                } else {
                     if (isRenew) {
                         status = "renewed";
                     } else {
                         status = "paid";
                     }
                }

                if(isBankUser){
                    paymentMode="bank";
                }
             

                /* START: CODE TO HANDLE SPECIAL CHARACTERS  */
                bankName = handleSpecialChar.handleSpecialChar(bankName);
                branchName = handleSpecialChar.handleSpecialChar(branchName);

                insRefNo = handleSpecialChar.handleSpecialChar(insRefNo);
                issuanceBank = handleSpecialChar.handleSpecialChar(issuanceBank);
                issuanceBranch = handleSpecialChar.handleSpecialChar(issuanceBranch);

                comments = handleSpecialChar.handleSpecialChar(comments);
                /* END CODE TO HANDLE SPECIAL CHARACTERS  */
               
                 if (issuanceDate != null) {
                    if (!issuanceDate.trim().equalsIgnoreCase("")) {
                        String[] dtArr = issuanceDate.split("/");
                        issuanceDate = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0];

                        String[] dtArrValDt = validityDt.split("/");
                        validityDt = dtArrValDt[2] + "-" + dtArrValDt[1] + "-" + dtArrValDt[0];
                    }
                 }


                  String strPayMakerUserId="0", strPayMakerPartTransId="0";
                if(isEditCase){
                List<SPTenderCommonData> lstBankInfo =
                                        tenderCommonService.returndata("getBankInfoForRegPayment", strPartTransId, editPaymentId);
                if(!lstBankInfo.isEmpty()){
                    if("checker".equalsIgnoreCase(lstBankInfo.get(0).getFieldName4())){
                        isChecker=true;
                        strPayMakerUserId = lstBankInfo.get(0).getFieldName5();
                        strPayMakerPartTransId = lstBankInfo.get(0).getFieldName6();
                    }
                }
                lstBankInfo = null;
            }
                
            String dtXml = "", regPayId = "";
            if(isChecker){
                dtXml = "<tbl_RegFeePayment paymentFor=\"" + paymentFor + "\" userId=\"" + payUserId + "\" createdBy=\"" + strPayMakerUserId + "\" bankName=\"" + bankName + "\" branchName=\"" + branchName + "\" currency=\"" + currency + "\" amount=\"" + fnlAmt + "\" paymentInstType=\"" + typOfPay + "\" instRefNumber=\"" + insRefNo + "\" issuanceBank=\"" + issuanceBank + "\" issuanceBranch=\"" + issuanceBranch + "\" issuanceDate=\"" + issuanceDate + "\" instValidityDt=\"" + validityDt + "\" dtOfPayment=\""+  dtOfPayment +"\" comments=\"" + comments + "\" paymentMode=\"" + paymentMode + "\" eSignature=\"\" status=\"" + status + "\" isVerified=\"no\" isLive=\"yes\" dtOfAction=\"" + format.format(new Date()) + "\" validityPeriodRef=\"" + validityPeriodRef + "\" partTransId=\"" + strPayMakerPartTransId + "\" />";
            } else {
                dtXml = "<tbl_RegFeePayment paymentFor=\"" + paymentFor + "\" userId=\"" + payUserId + "\" createdBy=\"" + userId + "\" bankName=\"" + bankName + "\" branchName=\"" + branchName + "\" currency=\"" + currency + "\" amount=\"" + fnlAmt + "\" paymentInstType=\"" + typOfPay + "\" instRefNumber=\"" + insRefNo + "\" issuanceBank=\"" + issuanceBank + "\" issuanceBranch=\"" + issuanceBranch + "\" issuanceDate=\"" + issuanceDate + "\" instValidityDt=\"" + validityDt + "\" dtOfPayment=\""+  dtOfPayment +"\" comments=\"" + comments + "\" paymentMode=\"" + paymentMode + "\" eSignature=\"\" status=\"" + status + "\" isVerified=\"no\" isLive=\"yes\" dtOfAction=\"" + format.format(new Date()) + "\" validityPeriodRef=\"" + validityPeriodRef + "\" partTransId=\"" + strPartTransId + "\" />";
            }
            //dtXml = "<tbl_RegFeePayment paymentFor=\"" + paymentFor + "\" userId=\"" + payUserId + "\" createdBy=\"" + userId + "\" bankName=\"" + bankName + "\" branchName=\"" + branchName + "\" currency=\"" + currency + "\" amount=\"" + fnlAmt + "\" paymentInstType=\"" + typOfPay + "\" instRefNumber=\"" + insRefNo + "\" issuanceBank=\"" + issuanceBank + "\" issuanceBranch=\"" + issuanceBranch + "\" issuanceDate=\"" + issuanceDate + "\" instValidityDt=\"" + validityDt + "\" dtOfPayment=\""+  dtOfPayment +"\" comments=\"" + comments + "\" paymentMode=\"" + paymentMode + "\" eSignature=\"\" status=\"" + status + "\" isVerified=\"no\" isLive=\"yes\" dtOfAction=\"" + format.format(new Date()) + "\" validityPeriodRef=\"" + validityPeriodRef + "\" partTransId=\"" + strPartTransId + "\" />";
            dtXml = "<root>" + dtXml + "</root>";
            //System.out.println("XML : " + dtXml);
            String auditAction = "";
            CommonMsgChk commonMsgChk = new CommonMsgChk();
            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
            try{
                if(isRenew){
                    auditAction = "Renewal Registration Fee Payment";
                }else{
                    auditAction = "Add Registration Fee Payment";
                }
                commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_RegFeePayment", dtXml, "").get(0);
                regPayId = commonMsgChk.getId().toString();
            }catch(Exception e){
                        auditAction = "Error in "+auditAction+" :"+e.getMessage();
                       }finally
                       {
                           if(!isEditCase)
                           {
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(payUserId), "userId", EgpModule.Bank_Module.getName(), auditAction, comments);
                            }
                       }
            
            if (commonMsgChk.getFlag().equals(true)) {
                if(isFreezed){
                    boolean isFlag = true;
                    UserRegisterService userRegisterService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                    userRegisterService.updateUserStatus(payUserId, "incomplete","");
                }
                if (isEditCase) {
                    // START CODE: TO UPDATE isLive Status of Old Record
                    try{
                        if(isRenew){
                            auditAction = "Edit Renewal Fee Payment";
                        }else{
                            auditAction = "Edit Registration Fee Payment";
                        }
                        commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_RegFeePayment", "isLive='no'", "regPaymentId=" + editPaymentId).get(0);
                    }catch(Exception e){
                        auditAction = "Error in "+auditAction+" :"+e.getMessage();
                    }finally{
                         MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                         makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(payUserId), "userId", EgpModule.Bank_Module.getName(), auditAction, comments);
                    }
                    auditAction = null;
                    if (commonMsgChk.getFlag().equals(true)) {
                        path = "RegistrationFeePaymentDetails.jsp?payId=" + regPayId + "&uId=" + payUserId + "&msgId=updated";
                    } else {
                        path = "RegistrationFeePayment.jsp?payId=" + editPaymentId + "&uId=" + payUserId + "&msgId=error";
                    }
                    // END CODE: TO UPDATE isLive Status of Old Record
                } else {
                    path = "RegistrationFeePaymentDetails.jsp?payId=" + regPayId + "&uId=" + payUserId + "&msgId=payment";
                }
            } else
                {
                if(isEditCase || isRenew){
                    path = "RegistrationFeePayment.jsp?payId=" + regPayId + "&uId=" + payUserId + "&msgId=error";
                } else {
                    path = "RegistrationFeePayment.jsp?uId=" + payUserId + "&msgId=error";
                }

            }

} else{
    // Redirect for Amount Error
    path = "RegistrationFeePayment.jsp?uId=" + payUserId + "&msgId=amterror";
}
            response.sendRedirect(path);
                
            }
            // END CODE: TO INSERT PAYMENT DATA

%>