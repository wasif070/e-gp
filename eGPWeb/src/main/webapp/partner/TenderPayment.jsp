<%-- 
    Document   : TenderPayment
    Created on : Mar 2, 2011, 5:35:47 PM
    Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.web.utility.CommonUtils"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import="java.lang.String" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

 <jsp:useBean id="tenderInfoServlet" scope="request" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
 <jsp:useBean id="appServlet" scope="request" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
    

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
          <% if ("df".equalsIgnoreCase(request.getParameter("payTyp"))) { %>
                Document Fees Payment
            <%} else if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {%>
                Tender Security Payment
            <%} else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {%>
                Performance Security Payment
            <%}else if("bg".equalsIgnoreCase(request.getParameter("payTyp"))){ %>
                New Performance Security Payment
            <% } %>
        </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript" >
                function ConfirmIsEdit(){
                    var IsEdit = $('#hdnIsEdit').val();
                    if(IsEdit == "edit") {
                        var val=confirm("If you want to edit this payment you must upload related documents again.");
                        if(val==true){
                              return true;
                          }else {
                              return false;
                          }
                    }
                    else {
                        return true;
                    }                    
                }
        </script>

  <script type="text/javascript" >
            var payType = 0; // 1 - Other Type, 2 - New Performance Security (bg)
            var noOfDaysNPS = 0;
            <%if ("bg".equalsIgnoreCase(request.getParameter("payTyp"))){%>
                payType = 2;
            <%}else{%>
                payType = 1;
            <%}%>
            $(function(){
                $('#btnSubmit').click(function(){
                    //alert(inProcess);
                 //$('#btnSubmit').attr("disabled", "disabled");

                 var IsPaymentEdit = ConfirmIsEdit();
                 if(IsPaymentEdit == true) {

                    var flag=true;

                    if($('#cmbPayment').val()==''){
                        $('#SpError2').html('Please select the Mode of Payment');
                        flag=false;
                    } else {
                        $('#SpError2').html('');


                        if($('#cmbPayment').val()=='Pay Order' || $('#cmbPayment').val()=='DD' || $('#cmbPayment').val()=='Bank Guarantee'){

                            if($.trim($('#txtInsRefNo').val())==''){
                                $('#SpError3').html('Please enter Instrument No.');
                                flag=false;
                            } else {$('#SpError3').html('');}

                            if($.trim($('#txtIssuanceBankNm').val())==''){
                                $('#SpError4').html('Please select Issuing Financial Institution');
                                flag=false;
                            } else {
                                //if(!chkSpecial($.trim($('#txtIssuanceBankNm').val())))
//                                if(!/^(([a-zA-Z])(?!$)([a-zA-Z \&\.\-]+[\&\.\-\a-zA-Z ])?)+$/.test(document.getElementById("txtIssuanceBankNm").value))
//                                {
//                                    $('#SpError4').html('Please enter a valid Bank Name');
//                                    flag=false;
//
//                                }
//                                else
//                                {
//                                    $('#SpError4').html('');
//                                }
                                    $('#SpError4').html('');
                            }
                            //$('#SpError4').html('');}

                            if($.trim($('#txtIssuanceBranch').val())==''){
                                $('#SpError5').html('Please select Issuing Financial Institution Branch');
                                flag=false;
                            } else
                            {
                                //if(!chkSpecial($.trim($('#txtIssuanceBranch').val())))
//                                if(!/^(([a-zA-Z])(?!$)([a-zA-Z \&\.\-]+[\&\.\-\a-zA-Z ])?)+$/.test(document.getElementById("txtIssuanceBranch").value))
//                                {
//                                    $('#SpError5').html('Please enter a valid Branch Name');
//                                    flag=false;
//
//                                }
//                                else
//                                {
//                                    $('#SpError5').html('');
//                                }

                                $('#SpError5').html('');
                            }

                            if($.trim($('#txtIssuanceDt').val())==''){
                                $('#SpError6').html('Please select Issuance Date');
                                flag=false;
                            } else {
                                var d =new Date();
                                var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());

                                var value=document.getElementById("txtIssuanceDt").value;
                                var mdy = value.split('/')  //Date and month split
                                var mdyhr= mdy[2].split(' ');  //Year and time split
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);


                                if(Date.parse(valuedate) > Date.parse(todaydate)){
                                    $('#SpError6').html('Issuance Date must be less than  or equal to the current date');
                                    flag=false;
                                }else {

                                    $('#SpError6').html('');
                                }
                            }


                            if($.trim($('#txtValidityDt').val())==''){
                                if($('#hdnPaymentFor').val()=='Tender Security')
                                {
                                    $('#SpError8').html('');
                                }
                                else
                                {
                                    $('#SpError8').html('Please select Validity Date');
                                }
                                flag=false;
                            } else {

                                if($('#hdnPaymentFor').val()=='Bid Security'){
                                //alert('Checking...');
                                tenderSecLstDtVal=document.getElementById("hdnTenderSecLastDt").value;
                                    var mdyTSLD = tenderSecLstDtVal.split('/')  //Date and month split
                                    var mdyTSLDhr= mdyTSLD[2].split(' ');  //Year and time split
                                    var tenderSecLstDt= new Date(mdyTSLDhr[0], mdyTSLD[1]-1, mdyTSLD[0]);

                                     var value=document.getElementById("txtValidityDt").value;
                                     var mdy = value.split('/')  //Date and month split
                                     var mdyhr= mdy[2].split(' ');  //Year and time split
                                     var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);

                                     //alert(tenderSecLstDt);
                                     //alert(valuedate);

                                      if(Date.parse(valuedate) < Date.parse(tenderSecLstDt)){
                                        $('#SpError8').html("Tender Security Valid up to Date can't be prior to Tender Security Date");
                                        flag=false;
                                    }else {$('#SpError8').html('');}

                                } else {
                                    var d =new Date();
                                    var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());

                                    var value=document.getElementById("txtValidityDt").value;
                                    var mdy = value.split('/')  //Date and month split
                                    var mdyhr= mdy[2].split(' ');  //Year and time split
                                    var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);

                                    if(Date.parse(valuedate) < Date.parse(todaydate)){
                                        $('#SpError8').html('Validity Date must be more than or equal to the current date');
                                        flag=false;
                                    }else {$('#SpError8').html('');}
                                }


                            }



                        } else if($('#cmbPayment').val()=='Account to Account Transfer'){

                            if($.trim($('#txtInsRefNo').val())==''){
                                $('#SpError3').html('Please enter Beneficiary Account No.');
                                flag=false;
                            } else {$('#SpError3').html('');}

                            if($.trim($('#txtIssuanceBranch').val())!=''){
                                //if(!chkSpecial($.trim($('#txtIssuanceBranch').val())))
                                if(!/^(([a-zA-Z])(?!$)([a-zA-Z \&\.\-]+[\&\.\-\a-zA-Z ])?)+$/.test(document.getElementById("txtIssuanceBranch").value))
                                {
                                    $('#SpError5').html('Please enter a valid Branch Name');
                                    flag=false;
                                }
                                else
                                {
                                    $('#SpError5').html('');
                                }
                            }



                        }
                    }




                    if($.trim($('#txtComments').val())==''){
                        $('#SpError7').html('Please enter Remarks');
                        flag=false;
                    }
                    else {
                        if($.trim($('#txtComments').val()).length >500) {
                            $('#SpError7').html('Maximum 500 characters are allowed');
                            flag=false;
                        }
                    }


                    if(flag==false){
                        $('#btnSubmit').removeAttr("disabled", "disabled");
                        return false;
                    } else {
                        document.getElementById("btnSubmit").style.display = 'none';
                    }
                    //return false;
                 }
                 else {
                     return false;
                 }                   

                });
            });

            function chkSpecial(value)
            {
                //Allow only characters
                //Characters + numbers
                //Characters + numbers +few special
                //Don't allow only numbers                
                return /^[a-zA-Z 0-9](?![0-9]+$)[a-zA-Z 0-9\&\.\-\(\)/]+$/.test(value);
            }

         </script>


        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }


            function GetCal2(txtname,controlname,optionalCtrl)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());

                        if(optionalCtrl!=null){
                            if(optionalCtrl!='' && optionalCtrl !='null')   {
                                //alert(date.getMonth());

                                //var value=document.getElementById(txtname).value;
                                //alert(value);
                                //var mdy = value.split('/')  //Date and month split
                                //var mdyhr= mdy[2].split(' ');  //Year and time split
                                //var valuedate= new Date(mdyhr[0], mdy[1]+7, mdy[0]);

                                var currdate = document.getElementById(txtname).value;
                                var temp=currdate.split('/');
                                //alert(temp[0]);
                                //alert(temp[1]);
                                //alert(temp[2]);
                                var newtemp=temp[1]+'/'+temp[0]+'/'+temp[2];
                                //alert(new Date(newtemp));
                                var today =new Date(newtemp);
                                //alert(today);
                                //alert(today.getYear());
                                //alert(today.getMonth());
                                //alert(today.getMonth() + 7);
                                if(payType==2){
                                    today.setDate(today.getDate()+noOfDaysNPS);
                                }else{
                                    today.setMonth(today.getMonth()+6)
                                }
                                //// Note: getMonth() funcation returns 1 month less than the actual value
                                // So to add 6 months we have to use +7
                                //alert(today);
                                // alert(today.getMonth());
                                var dtVal = today.getDate();
                                if(dtVal<10){
                                    dtVal="0" + dtVal.toString();
                                }

                                //var mnthVal = today.getMonth();
                                //Note: getMonth() funcation returns 1 month less than the actual value
                                var mnthVal = today.getMonth() + 1;
                                if(mnthVal<10){
                                    mnthVal="0" + mnthVal.toString();
                                }

                                var yrVal = today.getYear();
                                if(yrVal < 1900){
                                    yrVal= yrVal+ 1900;
                                }
                                valuedate=dtVal+"/"+mnthVal+"/"+yrVal;

                                //valuedate =new Date(valuedate);

                                document.getElementById(optionalCtrl).value=valuedate;
                            }
                        }



                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

        </script>

          <script type="text/javascript">
              function SetTenderSecLastDt(){
                  //alert('Hello');
                  if($('#hdnPaymentFor').val()=='Bid Security'){
                      var selPayType= $('#cmbPayment').val();

                      if (selPayType == "Pay Order" || selPayType == "DD" || selPayType == "Bank Guarantee"){
                          tenderSecLastDt=$('#hdnTenderSecLastDt').val();
                          $('#txtValidityDt').val(tenderSecLastDt);
                      }
                  }
              }
              //>>>>>>>>>>>>>>>>>>>This method id added by Salahuddin on September 30 2012 >>>>>>>>>>>>>>>>>>>>>>>>>>
              function setSymbol(value)
              {
                  if(value=='Nu.'/*'BTN'*/)
                      {               
                          $('#lblCurrencySymbol').text("Nu."/*"Tk."*/);
                          $('#lblAmount').text($('#hdnFinalAmount').val());
                      }
                  else if(value=='BTN')
                      {               
                          $('#lblCurrencySymbol').text("Nu."/*"Tk."*/);
                          $('#lblAmount').text($('#hdnFinalAmount').val());
                      }
                  else
                      {                       
                          $('#lblCurrencySymbol').text("$");
                          $('#lblAmount').text($('#hdnDollarAmount').val());
                      }
              }
              

            $(document).ready(function(){
                //alert($('#hdnAction').val());
                var inProcess = false;    
                var action=$('#hdnAction').val();
                $('#cmbPayment').val('');
                //$('#cmbPayment').val('Cash');
                //if(action.toLowerCase()=="edit"){
                //BindPaymentData();
                //}
                
                /*$('#cmbCurrency').change(function(){
                    var txtCurrency = $('#cmbCurrency').val();
                    $('#hdnCurrency').val(txtCurrency);

                });*/

                function BindPaymentData(){
                    //alert("Binding data")
                    //alert($('#hdnEditCurrency').val());

                    
                    var extId=$('#hdnEditExtId').val();
                    $('#hdnExtId').val(extId);

                    //The code below is added by Salahuddin on October 8
                    if($('#hdnIsICT').val()=="")      
                    {
                        var amt = $('#hdnEditAmount').val();
                        $('#lblAmount').html(amt);
                        $('#hdnFinalAmount').val(amt);
                    }
                    ///////////end of added code///////////

                    $('#cmbPayment').val($('#hdnEditPaymentType').val());

                   
                    var payType= $('#cmbPayment').val();
                    var IssuanceBankNm= $('#txtIssuanceBankNm').val();
                    //alert(payType);
                    $('#trInsRefNo').show();
                    $('#trIssuanceBankNm').show();
                    $('#trIssuanceBranch').show();
                    $('#trIssuanceDt').show();
                    $('#trInsValidity').show();
                    clear();
                    if (payType == "Cash"){
                        $('#trInsRefNo').hide();
                        $('#trIssuanceBankNm').hide();
                        $('#trIssuanceBranch').hide();
                        $('#trIssuanceDt').hide();
                        $('#trInsValidity').hide();

                    } else if (payType == "Pay Order" || payType == "DD" || payType == "Bank Guarantee"){
                        
                        $('#txtIssuanceBranchA2A').hide();
                        $('#txtIssuanceBranch').show();
                        $('#txtIssuanceBankNm').val($('#hdnEditIssuingBank').val());
                        IssuanceBankNm= $('#txtIssuanceBankNm').val();
                        var SelectedBranch = $('#hdnEditIssuingBranch').val();
                        
                        $.post("<%=request.getContextPath()%>/APPServlet", { param1:payType, param2:IssuanceBankNm, param4: SelectedBranch, funName:'getBankBranchListDD'},  function(j){
                            $("select#txtIssuanceBranch").html(j);
                        });
                        $('#lblInstrumentNo').html('Instrument No.');
                        $('#lblBankBranch').html('Issuing Financial Institution Branch');
                        $('#spnBranchStar').html('*');

                        // Bind Data
                        $('#txtInsRefNo').val($('#hdnEditInsNo').val());
                        //$('#txtIssuanceBranch').val($('#hdnEditIssuingBranch').val());
                        $('#txtIssuanceDt').val($('#hdnEditIssuingDt').val());
                        $('#txtValidityDt').val($('#hdnEditValidityDt').val());

                    }
                    else if (payType == "Account to Account Transfer"){
                        $('#trIssuanceBankNm').hide();
                        $('#txtIssuanceBranch').hide();
                        $('#txtIssuanceBranchA2A').show();
                        $('#trIssuanceDt').hide();
                        $('#trInsValidity').hide();
                        

                        $('#lblInstrumentNo').html('Beneficiary Account No.');
                        $('#lblBankBranch').html('Branch Name');
                        $('#spnBranchStar').html('');

                        // Bind Data
                        $('#txtInsRefNo').val($('#hdnEditInsNo').val());
                        $('#txtIssuanceBranchA2A').val($('#hdnEditIssuingBranch').val());

                    }

                    // Bind Date of Payment and Comments
                    $('#lblDtOfPayment').html($('#hdnEditDisplayedDateOfPayment').val());
                    $('#hdnDateOfPayment').val($('#hdnEditDateOfPayment').val());
                    $('#txtComments').val($('#hdnEditComments').val());
                }

               

                var payType= $('#cmbPayment').val();

                if (payType == "Cash" || payType == ""){
                    $('#trInsRefNo').hide();
                    $('#trIssuanceBankNm').hide();
                    $('#trIssuanceBranch').hide();
                    $('#trIssuanceDt').hide();
                    $('#trInsValidity').hide();
                }
                
                $('#txtIssuanceBankNm').change(function(){
                    var payType= $('#cmbPayment').val();
                    var IssuanceBankNm= $('#txtIssuanceBankNm').val();
                    $.post("<%=request.getContextPath()%>/APPServlet", { param1:payType, param2:IssuanceBankNm, funName:'getBankBranchListDD'},  function(j){
                        $("select#txtIssuanceBranch").html(j);
                    });
                })
                
                $('#cmbPayment').change(function(){
                    //alert($('#cmbPayment').val());

                    var payType= $('#cmbPayment').val();
                    var payFor = $('#hdnPaymentFor').val();
                    //alert(payFor);
                    $('#trInsRefNo').show();
                    $('#trIssuanceBankNm').show();
                    $('#trIssuanceBranch').show();
                    $('#trIssuanceDt').show();
                    $('#trInsValidity').show();
                    clear();
                    if (payType == "Cash" || payType==''){
                        $('#trInsRefNo').hide();
                        $('#trIssuanceBankNm').hide();
                        $('#trIssuanceBranch').hide();
                        $('#trIssuanceDt').hide();
                        $('#trInsValidity').hide();

                    } else if (payType == "Pay Order" || payType == "DD" || payType == "Bank Guarantee"){
                        
                        $('#txtIssuanceBranchA2A').hide();
                        $('#txtIssuanceBranch').show();
                        var IssuanceBankNm= $('#txtIssuanceBankNm').val();
                        
                        $.post("<%=request.getContextPath()%>/APPServlet", { param1:payType, param2:IssuanceBankNm, funName:'getBankBranchListDD'},  function(j){
                            $("select#txtIssuanceBranch").html(j);
                        });
                        
                        $('#lblInstrumentNo').html('Instrument No.');
                        $('#lblBankBranch').html('Issuing Financial Institution Branch');
                        $('#spnBranchStar').html('*');

                        
                        //if($.trim(payFor)=='Tender Security' && action==""){
                        if($.trim(payFor)=='Bid Security'){
                           SetTenderSecLastDt();
                        }

                    }
                    else if (payType == "Account to Account Transfer"){
                        $('#trIssuanceBankNm').hide();
                        $('#trIssuanceDt').hide();
                        $('#trInsValidity').hide();
                        $('#txtIssuanceBranch').hide();
                        $('#txtIssuanceBranchA2A').show();

                        $('#lblInstrumentNo').html('Beneficiary Account No.');
                        $('#lblBankBranch').html('Branch Name');
                        $('#spnBranchStar').html('');
                    }
                });


                function clear(){
                    //alert('Clearing...');
                    // Clear Error Messages

                    $('#txtInsRefNo').val('');
                    $('#txtIssuanceBankNm').val('');
                    $('#txtIssuanceBranch').val('');
                    $('#txtIssuanceBranchA2A').val('');
                    $('#txtIssuanceDt').val('');
                    $('#txtValidityDt').val('');
                    //$('#txtComments').val('');

                    $('#SpError1').html('');
                    $('#SpError2').html('');
                    $('#SpError3').html('');
                    $('#SpError4').html('');
                    $('#SpError5').html('');
                    $('#SpError6').html('');
                    $('#SpError7').html('');
                    $('#SpError8').html('');
                }
                
                
                
                

                if(document.getElementById("txtInsRefNo")!=null){
                    $('#txtInsRefNo').blur(function(){
                        if($.trim($('#txtInsRefNo').val())==''){
                            if($('#cmbPayment').val()=='Pay Order'){
                                $('#SpError3').html('Please enter Instrument No.');
                            } else if($('#cmbPayment').val()=='Account to Account Transfer'){
                                $('#SpError3').html('Please enter Beneficiary Account No.');
                            }

                            flag=false;
                        } else {$('#SpError3').html('');}

                        if(flag==false){return false;}
                    });
                }

                if(document.getElementById("txtIssuanceBankNm")!=null){
                    $('#txtIssuanceBankNm').blur(function(){
                        if($.trim($('#txtIssuanceBankNm').val())==''){
                            $('#SpError4').html('Please select Issuing Financial Institution.');
                            flag=false;
                        } else {
//                           if($.trim($('#txtIssuanceBankNm').val())!=''){
                             //if(!chkSpecial($.trim($('#txtIssuanceBankNm').val()))){
//                              if(!/^(([a-zA-Z])(?!$)([a-zA-Z \&\.\-]+[\&\.\-\a-zA-Z ])?)+$/.test(document.getElementById("txtIssuanceBankNm").value)){
//                                    $('#SpError4').html('Please enter a valid Bank Name.');
//                                    flag=false;
//                                }else {
//                                    $('#SpError4').html('');
//                                }
                                $('#SpError4').html('');
//                           }
                        }

                    });

                }

                if(document.getElementById("txtIssuanceBranch")!=null){
                    $('#txtIssuanceBranch').blur(function(){
                        if($.trim($('#txtIssuanceBranch').val())==''){
//                            if($('#cmbPayment').val()=='Pay Order'){
                                $('#SpError5').html('Please select Issuing Financial Institution Branch.');
//                            }

                            flag=false;
                        } else {
                             //if($.trim($('#txtIssuanceBranch').val())!=''){
                               //if(!chkSpecial($.trim($('#txtIssuanceBranch').val())))                                {
//                               if(!/^(([a-zA-Z])(?!$)([a-zA-Z \&\.\-]+[\&\.\-\a-zA-Z ])?)+$/.test(document.getElementById("txtIssuanceBranch").value)){
//                                    $('#SpError5').html('Please enter a valid Branch Name.');
//                                    flag=false;
//                                }
//                                else
//                                {
//                                    $('#SpError5').html('');
//                                }
                            //}
                            $('#SpError5').html('');
                        }

                    });

                }

                if(document.getElementById("txtIssuanceDt")!=null){
                    $('#txtIssuanceDt').blur(function(){
                        if($.trim($('#txtIssuanceDt').val())==''){
                            $('#SpError6').html('Please select Issuance Date.');
                            flag=false;
                        } else {
                            var d =new Date();
                            var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());

                            var value=document.getElementById("txtIssuanceDt").value;
                            var mdy = value.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);


                            if(Date.parse(valuedate) > Date.parse(todaydate)){
                                $('#SpError6').html('Issuance Date must be less than  or equal to the current date.');
                                flag=false;
                            }else {

                                $('#SpError6').html('');
                            }
                        }

                    });
                }

                if(document.getElementById("txtValidityDt")!=null){
                    $('#txtValidityDt').blur(function(){
                        if($.trim($('#txtValidityDt').val())==''){
                            if($('#hdnPaymentFor').val()=='Tender Security')
                            {
                                $('#SpError8').html('');
                            }
                            else
                            {
                                $('#SpError8').html('Please select Validity Date');
                            }
                            flag=false;
                        } else {
                            var d =new Date();
                            var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());

                            var value=document.getElementById("txtValidityDt").value;
                            var mdy = value.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);

                            if(Date.parse(valuedate) < Date.parse(todaydate)){
                                $('#SpError8').html('Validity Date must be more than or equal to the current date.');
                                flag=false;
                            }else {$('#SpError8').html('');}
                        }

                    });
                }

                $('#txtComments').blur(function(){
                    if($.trim($('#txtComments').val())==''){
                        $('#SpError7').html('Please enter Remarks.');
                        flag=false;
                    }
                    else {
                        if($.trim($('#txtComments')).length<=1000) {
                            $('#SpError7').html('');
                        }
                        else{
                            $('#SpError7').html('Maximum 1000 characters are allowed.');
                            flag=false;
                        }
                    }
                });

                //alert(action);
                             
                if(action.toLowerCase()=="edit"){
                    BindPaymentData();                    
                } 

            });
        </script>
    </head>
    
   
    <body>
        <%
                    //TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    TenderCommonService objTSC = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    if (session.getAttribute("userId") != null) {
                        objTSC.setLogUserId(session.getAttribute("userId").toString());
                    }
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
                    SimpleDateFormat formatVisible = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat formatVisibleWithMonthNm = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();

                    String emailId = "", payUserId = "", payDt = format.format(new Date()), payDtWithMnthNm = formatVisibleWithMonthNm.format(new Date());                    

                    // Dohatec Start
                    String cTypePSAmount = "";
                    // Dohatec
                    boolean isBankUser = false, isRenew = false, isEditCase = false, isChecker=false;
                    String strPartTransId="0", userId = "", action = "", editPaymentId = "0";
                     String tenderId="0", pkgLotId="0", payTyp="";
                    String paymentTxt="";
                    String amount="0.00", dollarAmount="0.00", txtAmount="0.00", noOfDaysForValidity="", currency="BTN", currencySymbol=/*"TK."*/"Nu.", extId="0", extForPayId="0";
                    if (request.getParameter("uId") != null) {
                        if (request.getParameter("uId") != "" && !request.getParameter("uId").equalsIgnoreCase("null")) {
                            payUserId = request.getParameter("uId");
                        }
                    }

                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userId = hs.getAttribute("userId").toString();
                    } else {
                       // response.sendRedirectFILTER(request.getContextPath() + "/SessionTimedOut.jsp");
                    }
                    
                     if (hs.getAttribute("govUserId") != null) {
                     strPartTransId = hs.getAttribute("govUserId").toString();
              }

                    if (request.getParameter("action") != null) {
                        if (request.getParameter("action") != "" && !request.getParameter("action").equalsIgnoreCase("null")) {
                            action = request.getParameter("action");

                            if ("edit".equalsIgnoreCase(action)) {
                                if (request.getParameter("payId") != null) {
                                    if (request.getParameter("payId") != "0" && request.getParameter("payId") != "" && !request.getParameter("payId").equalsIgnoreCase("null")) {
                                        editPaymentId = request.getParameter("payId");
                                        isEditCase = true;
                                    }
                                }
                            } else if ("renew".equalsIgnoreCase(action)) {
                                //amount= XMLReader.getMessage("localTenderRenewFee");
                                //amountDollar=XMLReader.getMessage("intTednerRenewFee");
                            }
                        }
                    }

                    

                    if (hs.getAttribute("userTypeId") != null) {
                        if ("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())) {
                            isBankUser = true; // userType is ScheduleBank";
                        }
                    }

                    String bidderEmail = "", registeredDt = "";

                    List<SPTenderCommonData> lstTendererEml = objTSC.returndata("getEmailIdfromUserId", payUserId, null);

                    if (!lstTendererEml.isEmpty()) {
                        emailId = lstTendererEml.get(0).getFieldName1();
                        registeredDt = lstTendererEml.get(0).getFieldName2();
                        bidderEmail = emailId;
                    }

                    if (request.getParameter("tenderId") !=null){
                        tenderId=request.getParameter("tenderId");
                    }
                     if (request.getParameter("lotId") != null){
                        if (request.getParameter("lotId")!="" && !request.getParameter("lotId").equalsIgnoreCase("null")){
                           pkgLotId=request.getParameter("lotId");
                        }
                    }
                    String bgId = "";
                    if (request.getParameter("bgId") !=null){
                        bgId = request.getParameter("bgId");
                    }
                    String payType = "";
                    String PaymentText2 = "";
                    
                     if (request.getParameter("payTyp") != null){
                        if (request.getParameter("payTyp")!="" && !request.getParameter("payTyp").equalsIgnoreCase("null")){
                           payTyp=request.getParameter("payTyp");

                            if ("df".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Document Fees";
                                 PaymentText2 = "Document Fees";
                                 payType = paymentTxt;
                             } else if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Tender Security";
                                 PaymentText2 = "Tender/Proposal Security";
                                 payType = paymentTxt;
                             } else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Performance Security";
                                 PaymentText2 = "Performance Security";
                                 payType = paymentTxt;
                             }else if ("bg".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "New Performance Security";
                                 PaymentText2 = "New Performance Security";
                                 payType = "Bank Guarantee";
                             }
                        }
                    }

                    if (request.getParameter("extId") != null){
                        if (request.getParameter("extId")!="" && !request.getParameter("extId").equalsIgnoreCase("null")){
                           extId=request.getParameter("extId");
                        }
                    }
                    
                     if (request.getParameter("extPayId") != null){
                        if (request.getParameter("extPayId")!="" && !request.getParameter("extPayId").equalsIgnoreCase("null")){
                           extForPayId=request.getParameter("extPayId");
                        }
                    }

                    String conPaymentAmt="";
                    if("bg".equalsIgnoreCase(request.getParameter("payTyp"))){
                        conPaymentAmt="A.tenderId=" + tenderId + " and A.bankGId = " + bgId;
                    }
                    else
                    {
                        if(pkgLotId!=null && !"0".equalsIgnoreCase(pkgLotId)){
                                if("ps".equalsIgnoreCase(payTyp)){
                                    conPaymentAmt="A.tenderId=" + tenderId + " And A.pkgLotId=" + pkgLotId+" And A.userId=" + payUserId;
                                }else {
                                    conPaymentAmt="A.tenderId=" + tenderId + " And A.appPkgLotId=" + pkgLotId;
                                }
                        } else {
                            if("ps".equalsIgnoreCase(payTyp)){
                                conPaymentAmt="A.tenderId=" + tenderId + " And A.userId=" + payUserId;
                            }else{
                                conPaymentAmt="A.tenderId=" + tenderId;
                            }
                        }
                    }
                    
                    String param2 = "";
                    
                    if ("bg".equalsIgnoreCase(request.getParameter("payTyp"))) {
                        param2= "Bank Guarantee";
                     }else{
                        param2 = paymentTxt;
                     }
                    List<SPTenderCommonData> lstPaymentAmt =
                            objTSC.returndata("getTenderPaymentAmount", param2, conPaymentAmt);
                    
                    if(!lstPaymentAmt.isEmpty()){
                        amount=lstPaymentAmt.get(lstPaymentAmt.size()-1).getFieldName1();
                        System.out.println("Amount is:::::::::::::::::::::::::::::::::::::::::::::"+amount);
                        dollarAmount = lstPaymentAmt.get(lstPaymentAmt.size()-1).getFieldName2();
                        System.out.println("Dollar Amount is:::::::::::::::::::::::::::::::::::::::::::::"+dollarAmount);
                        if ("bg".equalsIgnoreCase(request.getParameter("payTyp"))) {
                            noOfDaysForValidity = lstPaymentAmt.get(lstPaymentAmt.size()-1).getFieldName2();
                        %>
                            <script>
                                 noOfDaysNPS = parseInt('<%= noOfDaysForValidity %>');
                            </script>
                        <%
                        }
                    }

                    CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                     if (session.getAttribute("userId") != null) {
                        commonSearchService.setLogUserId(session.getAttribute("userId").toString());
                    }

                    String pendingTxt="";
                    boolean allowPayment=false;
                    String tenderSecLastDt="", tenderSecLastDtWithMnthNm="";

                    if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {
                         List<SPCommonSearchData> lstTenSecLastDt =
                             commonSearchService.searchData("getTenSecLastDt", action, tenderId, pkgLotId, payUserId, editPaymentId, extId, null, null, null  );

                         if (!lstTenSecLastDt.isEmpty()){
                            tenderSecLastDt = lstTenSecLastDt.get(0).getFieldName1();
                            tenderSecLastDtWithMnthNm = lstTenSecLastDt.get(0).getFieldName2();
                         }
                         lstTenSecLastDt = null;
                    }

                    /*
                    List<SPCommonSearchData> getPrePaymentList = commonSearchService.searchData("getPreTenderPaymentConditions", paymentTxt, tenderId, pkgLotId, payUserId, null, null, null, null, null  );{
                        if(!getPrePaymentList.isEmpty()){
                                 if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                             if(!"0.00".equalsIgnoreCase(getPrePaymentList.get(0).getFieldName1())){
                                                if("Yes".equalsIgnoreCase(getPrePaymentList.get(0).getFieldName2())){
                                                    if(!"0.00".equalsIgnoreCase(getPrePaymentList.get(0).getFieldName3())){
                                                        if("Paid".equalsIgnoreCase(getPrePaymentList.get(0).getFieldName4())){

                                                            if(!"null".equalsIgnoreCase(getPrePaymentList.get(0).getFieldName5())){
                                                                tenderSecLastDt=getPrePaymentList.get(0).getFieldName5();
                                                                tenderSecLastDtWithMnthNm=getPrePaymentList.get(0).getFieldName6();
                                                             }
                                                             allowPayment=true;
                                                        } else {
                                                            pendingTxt="Payment not allowed: Document Fees Payment is Pending";
                                                        }
                                                    } else {
                                                        allowPayment=true;
                                                    }
                                                } else {
                                                pendingTxt="Last date for Tender Security Submission has been lapsed. Tender Security payment cant be accepted now.";
                                                }
                                             } else {
                                                pendingTxt="Payment not allowed: Tender Security is Free";
                                             }
                                         }
                        }


                    }
                    */
                    //System.out.println("tenderSecLastDt: " + tenderSecLastDt);
                    //System.out.println("tenderSecLastDtWithMnthNm " + tenderSecLastDtWithMnthNm);


                    if (isEditCase) {
                        List<SPTenderCommonData> lstBankInfo =
                                objTSC.returndata("getBankInfoForTenPayment", strPartTransId, editPaymentId);
                        if (!lstBankInfo.isEmpty()) {
                            if ("checker".equalsIgnoreCase(lstBankInfo.get(0).getFieldName4())) {
                                isChecker = true;
                            }
                        }
                        lstBankInfo = null;
                    }

                    String ParentBranchId = "";

                    /*
                    System.out.println("strPartTransId:  " + strPartTransId);
                    System.out.println("editPaymentId:  " + editPaymentId);
                    System.out.println("isEditCase:  " + isEditCase);
                    System.out.println("isChecker:  " + isChecker);
                    */
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">
                    <%=PaymentText2%> Payment
                    <span style="float: right;">
                        <%if(isChecker) {%>
                            <a href="TenPaymentListing.jsp" class="action-button-goback">Go Back</a>
                        <%} else {%>
                            <%if("ps".equalsIgnoreCase(payTyp)){%>
                        <a href="NOAIssuedList.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a>
                        <%} else {%>
                        <a href="SearchTendererForTenPayment.jsp?uId=<%=payUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>" class="action-button-goback">Go Back</a>
                        <%}%>
                        <%}%>

                    </span>
                </div>

                      <% pageContext.setAttribute("tenderId", tenderId); %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <div class="tabPanelArea_1 t_space">

                    <%if (request.getParameter("msgId") != null) {
                                    String msgId = "", msgTxt = "";
                                    boolean isError = false;
                                    msgId = request.getParameter("msgId");
                                    System.out.print(msgId);
                                    if (!msgId.equalsIgnoreCase("")) {
                                        if (msgId.equalsIgnoreCase("error")) {
                                            isError = true;
                                            msgTxt = "There was some error";
                                        }else if (msgId.equalsIgnoreCase("amterror")) {
                                            isError = true;
                                            msgTxt = "Amount is not proper. Please try again";
                                        } else if (msgId.equalsIgnoreCase("allexists")) {
                                            isError = true;
                                            msgTxt = "Record already exist";
                                        }else {
                                            msgTxt = "";
                                        }
                    %>
                    <%if (isError) {%>
                    <div class="responseMsg errorMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                    <%} else {%>
                    <div class="responseMsg successMsg" style="margin-bottom: 12px;"><%=msgTxt%></div>
                    <%}%>
                    <%}
                    }%>

                    <!-- START: COMMON PACKAGE/LOT DESCRIPTION FILE -->
                    <div >
                        <%@include file="CommonPackageLotDescription.jsp" %>
                    </div>
                    <!-- END COMMON PACKAGE/LOT DESCRIPTION FILE -->

                    <form id="frmMakePayment" action="TenderPayInsert.jsp?uId=<%=request.getParameter("uId")%>&action=<%=action%>&payId=<%=editPaymentId%>&" method="POST">
                        <input type="hidden" name="bgId" id="bgId" value="<%=bgId%>" />
                        <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                            <tr>
                                <td class="ff">Email ID :</td>
                                <td><%=bidderEmail%></td>
                            </tr>
                            <tr>
                                <td class="ff">Date of Registration  :</td>
                                <td><%=registeredDt%></td>
                            </tr>

                            <% if (isBankUser) {
                                            //List<SPTenderCommonData> lstBankInfo = objTSC.returndata("getBankInfo", userId, null);
                                List<SPTenderCommonData> lstBankInfo = 
                                        objTSC.returndata("getBankInfoForTenPayment", strPartTransId, editPaymentId);
                                        ParentBranchId = lstBankInfo.get(0).getFieldName7();
                                            if (!lstBankInfo.isEmpty() && lstBankInfo.size() > 0) {
                            %>
                            <tr id="trBankNm">
                                <td class="ff">Financial Institution Name :
                                <td>
                                    <%=lstBankInfo.get(0).getFieldName1()%>
                                    <input type="hidden" value="<%=lstBankInfo.get(0).getFieldName1()%>" id="hdnBankNm" name="hdnBankNm">
                                </td>

                            </tr>
                            <tr id="trBranch">
                                <td class="ff">Branch Name :
                                <td>
                                    <%=lstBankInfo.get(0).getFieldName2()%>
                                    <input type="hidden" value="<%=lstBankInfo.get(0).getFieldName2()%>" id="hdnBranch" name="hdnBranch">
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Branch Maker :</td>
                                <td><%=lstBankInfo.get(0).getFieldName3()%></td>
                            </tr>
                            <%}
                                        }%>
                            <tr>
                                <td class="ff">Payment For : </td>
                                <td><label id="lblPaymentFor"><%=PaymentText2%></label>
                                <input type="hidden" id="hdnPaymentFor" name="hdnPaymentFor" value="<%=payType%>">
                                <input type="hidden" id="hdnPayType" name="hdnPayType" value="<%=payTyp%>">
                                <input type="hidden" id="hdnTenderId" name="hdnTenderId" value="<%=tenderId%>">
                                <input type="hidden" id="hdnLotId" name="hdnLotId" value="<%=pkgLotId%>">
                                <input type="hidden" id="hdnExtId" name="hdnExtId" value="<%=extId%>">
                                </td>
                            </tr>
                            <%
                                List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", tenderId, null);
                                boolean isIctTender = false;
                                if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                                    isIctTender = true;
                                }
                                // Dohatec : Checking for "Performance Security (PS) and "New Performance Security Payment (bg)"
                                if(("ps".equalsIgnoreCase(payTyp) || "bg".equalsIgnoreCase(payTyp)) && isIctTender)
                                {
                                    // Nothing to do
                                }
                                else{
                            %>
                            <!-- Edited by Salahuddin on September 30 for checking ICT/NCT  -->
                            <tr>
                                <td class="ff" width="18%">Currency : </td>
                                <td width="82%"><%
                                            /*List<SPTenderCommonData> listDP = tenderCommonService.returndata("chkDomesticPreference", tenderId, null);
                                                    boolean isIctTender = false;
                                                    if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                                                        isIctTender = true;
                                            }*/
                                                    if(isIctTender){
                                %><select name="cmbCurrency" class="formTxtBox_1" id="cmbCurrency" width="100px;" onchange="setSymbol(this.value);"  >
                                        <%if(isEditCase){
                                            CommonSearchDataMoreService objTpayment = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                            String regPayId="";
                                            if (request.getParameter("payId") != null) {
                                                regPayId = request.getParameter("payId");
                                            }
                                            List<SPCommonSearchDataMore> lstPaymentDetail = objTpayment.geteGPData("getTenderPaymentDetail", regPayId);
                                            // Changed by Emtaz on 12-April-2016. BTN -> Nu.
                                            if(lstPaymentDetail.get(0).getFieldName3().equals("BTN"))
                                            {
                                                //out.println("<option selected=\"selected\" value=\"BTN\">BTN</option>");
                                                out.println("<option selected=\"selected\" value=\"BTN\">Nu.</option>");
                                                out.println("<option value=\"USD\">USD</option>");
                                            }
                                            else
                                            {
                                                //out.println("<option value=\"BTN\">BTN</option>");
                                                out.println("<option value=\"BTN\">Nu.</option>");
                                                out.println("<option selected=\"selected\"  value=\"USD\">USD</option>");
                                            }
                                        }
                                        else{
                                            //out.println("<option selected=\"selected\" value=\"BTN\">BTN</option>");
                                            out.println("<option selected=\"selected\" value=\"BTN\">Nu.</option>");
                                            out.println("<option value=\"USD\">USD</option>");
                                        }    
                                        %>                                        
                                    </select><%}else{%>
                                    <label id="lblCurrency"><% if(currency.equalsIgnoreCase("BTN")){out.print("BTN");} else if(currency.equalsIgnoreCase("USD")){out.print("USD");} %></label>
                                    <%}%>
                                    <input type="hidden" id="hdnCurrency" name="hdnCurrency" value="<%=currency%>">
                                    <span id="SpError1" class="reqF_1"></span>
                                </td>
                            </tr>
                            <%}%>
                                <!-- Dohatec : Checking for "Performance Security (PS)" and "New Performance Security Payment (bg)" -->
                                <%if(("ps".equalsIgnoreCase(payTyp) || "bg".equalsIgnoreCase(payTyp)) && isIctTender){
                                    String txtCurrencySymbol = "";
                                    String currencyType = "";
                                    String perSecAmount = "";
                                    CommonSearchDataMoreService objTSDetails = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                    List<SPCommonSearchDataMore> lstPerSecPayDetail = null;
                                    if("ps".equalsIgnoreCase(payTyp))
                                        lstPerSecPayDetail = objTSDetails.geteGPData("getPerformanceSecurityPaymentDetail", tenderId.toString());
                                    else if("bg".equalsIgnoreCase(payTyp))
                                        lstPerSecPayDetail = objTSDetails.geteGPData("getNewPerSecDetail", tenderId.toString(), pkgLotId.toString(), payUserId.toString());
                                    if(!lstPerSecPayDetail.isEmpty())
                                    {
                                        System.out.print("\n List Size : " + lstPerSecPayDetail.size());
                                        for(int i = 0; i<lstPerSecPayDetail.size(); i++)
                                        {
                                            if("ps".equalsIgnoreCase(payTyp))
                                            {
                                                txtCurrencySymbol = lstPerSecPayDetail.get(i).getFieldName13();
                                                currencyType = lstPerSecPayDetail.get(i).getFieldName12();
                                                perSecAmount = lstPerSecPayDetail.get(i).getFieldName4();
                                            }
                                            else if("bg".equalsIgnoreCase(payTyp))
                                            {
                                                perSecAmount = lstPerSecPayDetail.get(i).getFieldName1();
                                                currencyType = lstPerSecPayDetail.get(i).getFieldName2();
                                                txtCurrencySymbol = lstPerSecPayDetail.get(i).getFieldName3();
                                            }
                                            cTypePSAmount = cTypePSAmount + currencyType + "@" + perSecAmount + "-";
                                %>
                                    <tr>
                                        <td class="ff">Amount (in <% if(currencyType.equalsIgnoreCase("USD")){out.print("USD");} else {out.print("BTN");} %>) :</td>
                                        <td><label id="lblCurrencySymbol_<%=i%>"><% if(txtCurrencySymbol.equalsIgnoreCase("TK.")){out.print("Nu.");} else if(txtCurrencySymbol.equalsIgnoreCase("Nu.")){out.print("Nu.");} else {out.print("$");}%></label>&nbsp;
                                            <label id="lblAmount_<%=i%>"><%=perSecAmount%></label>
                                        </td>
                                    </tr>
                                <%
                                        }
                                    }
                                }else{%>
                            <tr>
                                <td class="ff">Amount :</td>
                                <td><label id="lblCurrencySymbol" name="lblCurrencySymbol"><%
                                            CommonSearchDataMoreService objTpayment = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                            String regPayId="";
                                            if (request.getParameter("payId") != null) {
                                                regPayId = request.getParameter("payId");
                                            }
                                            List<SPCommonSearchDataMore> lstPaymentDetail = objTpayment.geteGPData("getTenderPaymentDetail", regPayId);

                                            if(isEditCase)
                                            {
                                                if(lstPaymentDetail.get(0).getFieldName3().equals("BTN")/*equals("BTN")*/){
                                                    out.println("Nu."/*"Tk."*/);
                                                    txtAmount = amount;
                                                }
                                                else{
                                                    out.println("$");
                                                    txtAmount = dollarAmount;
                                                }
                                            }
                                            else{out.println(/*"BTN"*/"Nu."); txtAmount = amount;}%></label>&nbsp;
                                    <label id="lblAmount"><%=txtAmount%></label>
                                </td>
                            </tr>
                                <%}%>                            
                                <!-- Dohatec End -->
                            <tr>
                                <td class="ff" width="18%">Mode of Payment : <span class="mandatory">*</span></td>
                                <td width="82%">
                                    <select name="cmbPayment" class="formTxtBox_1" id="cmbPayment">
                                        <option value="">--Select--</option>
                                      
                                        <%if("df".equalsIgnoreCase(payTyp)){%>
<!--                                        <option value="Pay Order">Pay Order</option>-->
                                        <option value="Cash">Cash</option>
                                        <option value="Account to Account Transfer">Account to Account Transfer</option>
                                        <option value="DD">DD</option>
                                        <% } else if("ts".equalsIgnoreCase(payTyp) || "ps".equalsIgnoreCase(payTyp) || "bg".equalsIgnoreCase(payTyp)){
                                        if(!listDP.isEmpty() && !listDP.get(0).getFieldName1().equalsIgnoreCase("true"))
                                         {

                                        %>
<!--                                          <option value="Pay Order">Pay Order</option>-->
                                          <option value="Bank Guarantee">Financial Institution Guarantee</option>
                                          <option value="Pay Order">Cheque/Cash Warrant</option>
                                          <option value="DD">DD</option>
                                          
                                          
                                        <%}

                                        if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true"))
                                         { %>
                                         <option value="Bank Guarantee">Financial Institution Guarantee</option>
                                        <%}}%>

                                    </select>
                                    <span id="SpError2" class="reqF_1"></span>
                                </td>
                            </tr>


                            <tr id="trInsRefNo">
                                <td class="ff">
                                    <label id="lblInstrumentNo"></label>
                                    <span class="mandatory">*</span></td>
                                <td><input name="txtInsRefNo" type="text" class="formTxtBox_1" id="txtInsRefNo" style="width:200px;" />
                                    <span id="SpError3" class="reqF_1"></span>
                                </td>
                            </tr>
                            
                            <%    
                                 List<SPTenderCommonData> BankList = tenderCommonService.returndata("getBankBranchList", "0", "");
                            %>
                            
                            <tr id="trIssuanceBankNm">
                                <td class="ff">Issuing Financial Institution : <span class="mandatory">*</span></td>
                                <td>
                                    <!--<input name="txtIssuanceBankNm" type="text" class="formTxtBox_1" id="txtIssuanceBankNm" style="width:200px;"/>-->
                                    <select name="txtIssuanceBankNm" class="formTxtBox_1" id="txtIssuanceBankNm" style="width:200px;">
                                        <option value="" >-- Select Issuing Financial Institution --</option>
                                        <%
                                            for(SPTenderCommonData BankInfo : BankList)
                                            {
                                        %>
                                            <option value="<%=BankInfo.getFieldName1()%>"><%=BankInfo.getFieldName1() %></option>
                                        <%
                                            }
                                        %>
                                    </select>
                                    <span id="SpError4" class="reqF_1"></span>
                                </td>
                            </tr>
                            
                            <%    
                                 List<SPTenderCommonData> BranchListA2A = tenderCommonService.returndata("getBankBranchList", ParentBranchId, "");
                            %>
                            
                            <tr id="trIssuanceBranch">
                                <td class="ff">
                                    <label id="lblBankBranch"></label> :
                                    <span id="spnBranchStar" class="mandatory"></span></td>
                                <td>
                                    <!--<input name="txtIssuanceBranch" type="text" class="formTxtBox_1" id="txtIssuanceBranch" style="width:200px;" />-->
                                    <select class="formTxtBox_1" name="txtIssuanceBranch" id="txtIssuanceBranch" style="width:200px;" >
                                        <option value="">-- Select Branch --</option>
                                    </select>
                                    <select name="txtIssuanceBranchA2A" class="formTxtBox_1" id="txtIssuanceBranchA2A" style="width:200px;">
                                        <option value="" >-- Select Branch --</option>
                                        <%
                                            for(SPTenderCommonData BranchInfo : BranchListA2A)
                                            {
                                        %>
                                                <option value="<%=BranchInfo.getFieldName1()%>"><%=BranchInfo.getFieldName1() %></option>
                                        <%
                                            }
                                        %>
                                    </select>
                                    <span id="SpError5" class="reqF_1"></span>
                                </td>
                            </tr>

                            <tr id="trIssuanceDt">
                                <td class="ff">Issuance Date : <span class="mandatory">*</span></td>
                                <%if("Bid Security".equalsIgnoreCase(paymentTxt)){%>
                                <td><input onfocus="GetCal('txtIssuanceDt','txtIssuanceDt');" name="txtIssuanceDt" type="text" class="formTxtBox_1" id="txtIssuanceDt" style="width:100px;" readonly="readonly" />
                                    <img id="imgIssuanceDt" name="imgIssuanceDt" onclick="GetCal('txtIssuanceDt','imgIssuanceDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                                    <span id="SpError6" class="reqF_1"></span>
                                </td>
                                <% } else if("Performance Security".equalsIgnoreCase(paymentTxt)){%>
                                <td><input onfocus="GetCal('txtIssuanceDt','txtIssuanceDt');" name="txtIssuanceDt" type="text" class="formTxtBox_1" id="txtIssuanceDt" style="width:100px;" readonly="readonly" />
                                    <img id="imgIssuanceDt" name="imgIssuanceDt" onclick="GetCal('txtIssuanceDt','imgIssuanceDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                                    <span id="SpError6" class="reqF_1"></span>
                                </td>
                                <%} else {%>
                                <td><input onfocus="GetCal2('txtIssuanceDt','txtIssuanceDt','txtValidityDt');" name="txtIssuanceDt" type="text" class="formTxtBox_1" id="txtIssuanceDt" style="width:100px;" readonly="readonly" />
                                    <img id="imgIssuanceDt" name="imgIssuanceDt" onclick="GetCal2('txtIssuanceDt','imgIssuanceDt','txtValidityDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                                    <span id="SpError6" class="reqF_1"></span>
                                </td>
                                <%}%>
                               
                            </tr>

                            <tr id="trInsValidity">
                                <td class="ff">Validity Date : <span class="mandatory" <%if("ts".equalsIgnoreCase(payTyp)){%>style="display: none;"<%}%>>*</span></td>
                                <td><input onfocus="GetCal('txtValidityDt','txtValidityDt');" name="txtValidityDt" type="text" class="formTxtBox_1" id="txtValidityDt" readonly="readonly" <%if("ts".equalsIgnoreCase(payTyp)){%> style="width:100px;display:none;" <%} else {%>style="width:100px;"<%}%> />
                                    <img id="imgValidityDt" name="imgValidityDt" onclick="GetCal('txtValidityDt','imgValidityDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" <%if("ts".equalsIgnoreCase(payTyp)){%>style="vertical-align:middle;cursor: pointer;display: none;"<%} else {%>style="vertical-align:middle;cursor: pointer;"<%}%> />
                                    <span id="SpError8" class="reqF_1"></span>
                                    <input type="hidden" id="hdnTenderSecLastDt" name="hdnTenderSecLastDt" value="<%=tenderSecLastDt%>">
                                    <label id="lblValidityDt" <%if(!"ts".equalsIgnoreCase(payTyp)){%>style="display: none;"<%}%>><%=tenderSecLastDtWithMnthNm%></label>
                                </td>
                            </tr>

                            <tr id="trDtOfPayment">
                                <td class="ff">Date and Time of Payment : </td>
                                <td>
                                    <label id="lblDtOfPayment"><%=payDtWithMnthNm%></label>
                                    <input type="hidden" id="hdnDateOfPayment" name="hdnDateOfPayment" value="<%=payDt%>">
                                </td>
                            </tr>

                            <tr>
                                <td class="ff">Remarks : <span class="mandatory">*</span></td>
                                <td><textarea cols="100" rows="5" id="txtComments" name="txtComments" class="formTxtBox_1"></textarea>
                                    <span id="SpError7" class="reqF_1"></span>

                                </td>
                            </tr>

                            <tr>
                                <td class="ff">&nbsp;</td>
                                <td>
                                    <div class="t-align-left t_space">
                                        <input type="hidden" id="hdnIsEdit" name="hdnIsEdit"  value="<%=action%>">
                                        <label class="formBtn_1"><input  id="btnSubmit" type="submit" value="Submit" /></label>
                                        <input type="hidden" id="hdnUserType" name="btnSubmit"  value="0">
                                        <input type="hidden" id="hdnUserType" name="hdnUserType"  value="BankUser">
                                        <label class="formBtn_1">
                                            <input type="button" name="btnReset" id="btnReset" value="Reset" onclick="reset();errorClear();"/>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        </table>

                                <%
                                    String editCurrency = "";
                                    String editAmount = "";
                                    String editPaymentType = "";
                                    String editInsNo = "";
                                    String editIssuingBank = "";
                                    String editIssuingBranch = "";
                                    String editIssuingDt = "";
                                    String editValidityDt = "";
                                    String editDisplayedDateOfPayment = "";
                                    String editDateOfPayment = "";
                                    String editComments = "";
                                    String isVerifiedChk = "";
                                    String editPayStatus = "";
                                    //String editValidityRefId = "";
                                    String editTenderId="";
                                    String editLotId="";
                                    String editPaymentFor="";
                                    String editExtId="";
                                    String editPayType="";
                                    if (isEditCase) {

                                        List<SPTenderCommonData> lstPaymentDetailForEdit = objTSC.returndata("getTenderPaymentDetailForEdit", editPaymentId, null);
                                        List<SPTenderCommonData> lstPaymentDetailExtraForEdit = objTSC.returndata("getTenderPaymentDetailExtraForEdit", editPaymentId, null);

                                        if(!lstPaymentDetailExtraForEdit.isEmpty()){
                                            editTenderId=lstPaymentDetailExtraForEdit.get(0).getFieldName1();
                                            editLotId=lstPaymentDetailExtraForEdit.get(0).getFieldName2();
                                            editPaymentFor=lstPaymentDetailExtraForEdit.get(0).getFieldName3();
                                            editPayType=lstPaymentDetailExtraForEdit.get(0).getFieldName4();
                                        }

                                        if (!lstPaymentDetailForEdit.isEmpty()) {
                                            editCurrency = lstPaymentDetailForEdit.get(0).getFieldName1();
                                            editAmount = lstPaymentDetailForEdit.get(0).getFieldName2();
                                            editPaymentType = lstPaymentDetailForEdit.get(0).getFieldName3();
                                            editDisplayedDateOfPayment = lstPaymentDetailForEdit.get(0).getFieldName4();
                                            editComments = URLDecoder.decode(lstPaymentDetailForEdit.get(0).getFieldName5(),"UTF-8");
                                            editDateOfPayment = lstPaymentDetailForEdit.get(0).getFieldName6();
                                            isVerifiedChk = lstPaymentDetailForEdit.get(0).getFieldName7();
                                            editPayStatus = lstPaymentDetailForEdit.get(0).getFieldName8();
                                            editExtId = lstPaymentDetailForEdit.get(0).getFieldName9();
                                        }

                                        if ("Pay Order".equalsIgnoreCase(editPaymentType) || "DD".equalsIgnoreCase(editPaymentType) || "Bank Guarantee".equalsIgnoreCase(editPaymentType)) {
                                            List<SPTenderCommonData> lstPaymentDetailMoreForEdit = objTSC.returndata("getTenderPaymentDetailMoreForEdit", editPaymentId, null);

                                            if (!lstPaymentDetailMoreForEdit.isEmpty()) {
                                                editInsNo = lstPaymentDetailMoreForEdit.get(0).getFieldName1();
                                                editIssuingBank = lstPaymentDetailMoreForEdit.get(0).getFieldName2();
                                                editIssuingBranch = lstPaymentDetailMoreForEdit.get(0).getFieldName3();
                                                editIssuingDt = lstPaymentDetailMoreForEdit.get(0).getFieldName4();
                                                editValidityDt = lstPaymentDetailMoreForEdit.get(0).getFieldName5();
                                            }
                                        } else if ("Account to Account Transfer".equalsIgnoreCase(editPaymentType)) {
                                            List<SPTenderCommonData> lstPaymentDetailMoreForEdit = objTSC.returndata("getTenderPaymentDetailMoreForEdit", editPaymentId, null);

                                            if (!lstPaymentDetailMoreForEdit.isEmpty()) {
                                                editInsNo = lstPaymentDetailMoreForEdit.get(0).getFieldName1();
                                                if (!"".equalsIgnoreCase(lstPaymentDetailMoreForEdit.get(0).getFieldName3())) {
                                                    editIssuingBranch = lstPaymentDetailMoreForEdit.get(0).getFieldName3();
                                                }
                                            }
                                        }
                        %>

                        <%}%>


                        <input type="hidden" id="hdnPayUserId" name="hdnPayUserId"
                               <%if (!"".equalsIgnoreCase(payUserId)) {%>
                               value="<%=payUserId%>"
                               <%} else {%>
                               value="0"
                               <%}%>
                               >
                        <input type="hidden" id="hdnPaymentId" name="hdnPaymentId"

                               <%if (request.getParameter("payId") != null) {%>
                               value="<%=request.getParameter("payId")%>"
                               <%} else {%>
                               value="0"
                               <%}%>
                               >
                        
                         <input type="hidden" id="hdnExtForPayId" name="hdnExtForPayId" value="<%=extForPayId%>" >
                        <!--Dohatec Start :: Hidden value is added for combine all Currency Type and PS Amount for Performance Security :: -->
                            <input type="hidden" id="hdnPSAmount" name ="hdnPSAmount" value="<%=cTypePSAmount%>"
                        <!--Dohatec End-->

                        <input type="hidden" id="hdnFinalAmount" name="hdnFinalAmount" value="<%=amount%>" >

                        <input type="hidden" id="hdnAction" name="hdnAction" value="<%=action%>">
                        <input type="hidden" id="hdnEditCurrency" name="hdnEditCurrency" value="<%=editCurrency%>">
                        <input type="hidden" id="hdnEditAmount" name="hdnEditAmount" value="<%=editAmount%>">
                        <input type="hidden" id="hdnEditPaymentType" name="hdnEditPaymentType" value="<%=editPaymentType%>">
                        <input type="hidden" id="hdnEditInsNo" name="hdnEditInsNo" value="<%=editInsNo%>">
                        <input type="hidden" id="hdnEditIssuingBank" name="hdnEditIssuingBank" value="<%=editIssuingBank%>">
                        <input type="hidden" id="hdnEditIssuingBranch" name="hdnEditIssuingBranch" value="<%=editIssuingBranch%>">
                        <input type="hidden" id="hdnEditIssuingDt" name="hdnEditIssuingDt" value="<%=editIssuingDt%>">
                        <input type="hidden" id="hdnEditValidityDt" name="hdnEditValidityDt" value="<%=editValidityDt%>">
                        <input type="hidden" id="hdnEditDisplayedDateOfPayment" name="hdnEditDisplayedDateOfPayment" value="<%=editDisplayedDateOfPayment%>">
                        <input type="hidden" id="hdnEditDateOfPayment" name="hdnEditDateOfPayment" value="<%=editDateOfPayment%>">
                        <input type="hidden" id="hdnEditComments" name="hdnEditComments" value="<%=editComments%>">
                        <input type="hidden" id="hdnEditPayStatus" name="hdnEditPayStatus" value="<%=editPayStatus%>">
                        <input type="hidden" id="hdnEditPaymentId" name="hdnEditPaymentId" value="<%=editPaymentId%>">
                        <input type="hidden" id="hdnEditExtId" name="hdnEditExtId" value="<%=editExtId%>">

                        <input type="hidden" id="hdnEditTenderId" name="hdnEditTenderId" value="<%=editTenderId%>">
                        <input type="hidden" id="hdnEditLotId" name="hdnEditLotId" value="<%=editLotId%>">
                        <input type="hidden" id="hdnEditPaymentFor" name="hdnEditPaymentFor" value="<%=editPaymentFor%>">
                        <input type="hidden" id="hdnDollarAmount" name="hdnDollarAmount" value="<%=CommonUtils.checkNull(dollarAmount)%>">
                        <input type="hidden" id="hdnIsICT" name="hdnIsICT" value="<%List<SPTenderCommonData> listDP1 = tenderCommonService.returndata("chkDomesticPreference", tenderId, null);
                                                    boolean isIctTender1 = false;
                                                    if(!listDP1.isEmpty() && listDP1.get(0).getFieldName1().equalsIgnoreCase("true")){
                                                        isIctTender1 = true;
                                                    }
                                                    if(isIctTender1){out.print(/*"ICT"*/"ICB");} else{out.print("");}%>">

                        <% System.out.println("#########################dollar amount:"+dollarAmount); %>
                   </form>
                </div>

            </div>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
        </div>
            <script>
                
                function errorClear(){
                   
                    $('#SpError1').html('');
                    $('#SpError2').html('');
                    $('#SpError3').html('');
                    $('#SpError4').html('');
                    $('#SpError5').html('');
                    $('#SpError6').html('');
                    $('#SpError7').html('');
                    $('#SpError8').html('');
                }
                
            </script>
    </body>


    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>


