<%-- 
    Document   : PaidTendererList
    Created on : Jun 10, 2011, 5:24:39 PM
    Author     : Karan
--%>

<%@page import="javax.swing.JOptionPane"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetails"%>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>

              <% if ("df".equalsIgnoreCase(request.getParameter("payTyp"))) { %>
                List of Tenderers/Consultants for Document Fees Payment
            <%} else if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {%>
                List of Tenderers/Consultants for Tender Security Payment
            <%} else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {%>
                List of Tenderers/Consultants for Performance Security Payment
            <%}%>
              </title>

            <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
         <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

      
    </head>
    <body>
         <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%
                    boolean isCurUserPE = false;
                    boolean allowReleaseForfeit = false;
                    String userId="", srchUserId="";                                        
                    String tenderId="0", pkgLotId="0", payTyp="";
                    String paymentTxt="";
                    String referer = "";
                    CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");

                 HttpSession hs = request.getSession();
                 if (hs.getAttribute("userId") != null) {
                         userId = hs.getAttribute("userId").toString();
                  } else {response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");}

                
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");

                }

                  if (request.getParameter("tenderId") !=null){
                        tenderId=request.getParameter("tenderId");
                    }
                     if (request.getParameter("lotId") != null){
                        if (request.getParameter("lotId")!="" && !request.getParameter("lotId").equalsIgnoreCase("null")){
                           pkgLotId=request.getParameter("lotId");
                        }
                    }

                     if (request.getParameter("payTyp") != null){
                        if (request.getParameter("payTyp")!="" && !request.getParameter("payTyp").equalsIgnoreCase("null")){
                           payTyp=request.getParameter("payTyp");

                            if ("df".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Document Fees";
                             } else if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Tender Security";
                             } else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Performance Security";
                             }
                        }
                    }
                TenderCommonService wfTenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                List<SPTenderCommonData> checkuserRights = wfTenderCommonService.returndata("CheckTenderUserRights",userId,tenderId);
                %>


                <div class="contentArea_1">
                    <div class="pageHead_1">Tenderers/Consultants List for <%=paymentTxt%> Payment
                        <span style="float: right;" >
                            <a href="ForTenderPayment.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go To Main Payment Page</a>
                        </span>
                    </div>

                <!--  START: TENDER INFO BAR -->
                <% pageContext.setAttribute("tenderId", tenderId); %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <!--  END: TENDER INFO BAR -->

                  <div class="tabPanelArea_1 t_space">
                       <div >
                            <%@include file="CommonPackageLotDescription.jsp" %>
                        </div>
                    </div>

                <%
                     /* Start: CODE TO CHECK PE USER */
  List<SPTenderCommonData> listChkCurUserPE  = tenderCommonService.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);

  if (!listChkCurUserPE.isEmpty()) {
      if(userId.equalsIgnoreCase(listChkCurUserPE.get(0).getFieldName1())){
        isCurUserPE = true;
      }
  }

 listChkCurUserPE = null;
/* End: CODE TO CHECK PE USER */
                %>

<%if(isCurUserPE) {%>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
        <tr>
            <th width="4%" class="t-align-center">Sl. No.</th>
            <th width="40%" class="t-align-center">Company Name</th>
            <th class="t-align-center" width="11%">Payment Status</th>
            <th class="t-align-center" width="30%">Action</th>
        </tr>

    <%  int j = 0;
                        /*Dohatec Start Tender Cancel*/
                        boolean isCancelled = false;
                        List<TblTenderDetails> tblTenderDetailTS = tenderSrBean.getTenderStatus(tenderId);
                            if (!tblTenderDetailTS.isEmpty()) {
                                System.out.println(" tblTenderDetailTS.get(0).getTenderStatus() Info " + tblTenderDetailTS.get(0).getTenderStatus());
                                if ("Cancelled".equalsIgnoreCase(tblTenderDetailTS.get(0).getTenderStatus())) {
                                    isCancelled = true;
                                }
                            }
                        // List<SPCommonSearchData> lstPayments= commonSearchService.searchData("getPaidTendererListForPE", tenderId, pkgLotId, paymentTxt, userId, null, null, null, null, null);
                        List<SPCommonSearchData> lstPayments= null;
                        if(!isCancelled){
                            lstPayments= commonSearchService.searchData("getPaidTendererListForPE", tenderId, pkgLotId, paymentTxt, userId, null, null, null, null, null);
                        }
                        else{
                            lstPayments= commonSearchService.searchData("getPaidTendererListForPETenderCancel", tenderId, pkgLotId, paymentTxt, userId, null, null, null, null, null);
                        }
                         /*Dohatec End Tender Cancel*/
     //for (SPTenderCommonData sptcd : tenderCommonService.returndata("gettenderlotbytenderidForPayment", tenderId, payTyp)){
        for (SPCommonSearchData sptcd : lstPayments){        

         j++;  %>
         <tr
            <%if(Math.IEEEremainder(j,2)==0) {%>
                class="bgColor-Green"
            <%} else {%>
                class="bgColor-white"
            <%}%>
             >
             <td class="t-align-center"><%=j%></td>
             <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
             <td class="t-align-center"><% if("Forfeited".equalsIgnoreCase(sptcd.getFieldName8())){
                                            out.print("compensated");
                                         }else{
                                            out.print(sptcd.getFieldName8());
                                         }
                 %></td>
             <td class="t-align-center">
                 <%if(!checkuserRights.isEmpty()){%>
                 <a href="ViewTenderPaymentDetails.jsp?payId=<%=sptcd.getFieldName1()%>&uId=<%=sptcd.getFieldName3()%>&tenderId=<%=sptcd.getFieldName4()%>&lotId=<%=sptcd.getFieldName5()%>&payTyp=<%=sptcd.getFieldName6()%>">Payment Details</a>

                 <%if("Paid".equalsIgnoreCase(sptcd.getFieldName8()) || "Extended".equalsIgnoreCase(sptcd.getFieldName8())){%>
                 <%if("ts".equalsIgnoreCase(sptcd.getFieldName6()) ) {%>
                 <%
                 String strActionRequest = "";
List<SPTenderCommonData> lstPaymentActionRequest = 
                              tenderCommonService.returndata("getRequestActionFromPaymentId", sptcd.getFieldName1(), null);
if(lstPaymentActionRequest.isEmpty()){
    allowReleaseForfeit = true;
} else {
    allowReleaseForfeit = false;
    strActionRequest = lstPaymentActionRequest.get(0).getFieldName2();
}

%>
                <%if(allowReleaseForfeit){%>
                 &nbsp;&nbsp;<a href="../partner/TenderPaymentReleaseForfeit.jsp?action=requestrelease&payId=<%=sptcd.getFieldName1()%>&uId=<%=sptcd.getFieldName3()%>&tenderId=<%=sptcd.getFieldName4()%>&lotId=<%=sptcd.getFieldName5()%>&payTyp=<%=sptcd.getFieldName6()%>">Release Request</a>
                 &nbsp;&nbsp;<a href="../partner/TenderPaymentReleaseForfeit.jsp?action=requestforfeit&payId=<%=sptcd.getFieldName1()%>&uId=<%=sptcd.getFieldName3()%>&tenderId=<%=sptcd.getFieldName4()%>&lotId=<%=sptcd.getFieldName5()%>&payTyp=<%=sptcd.getFieldName6()%>">Forfeit Request</a>
                <%} else {%>
                <%
                    if("Forfeit".equalsIgnoreCase(strActionRequest)){
                        if("ts".equalsIgnoreCase(sptcd.getFieldName6())){
                            out.print("Forfeit");
                        }else{
                            out.print("Compensate");
                        }
                    }else{
                        out.print(strActionRequest);
                    }    
                %> Requested
                <%}%>
                 <%}}}%>
             </td>
         </tr>
   <%}%>
   <%if(j==0){%>
   <tr>
       <td colspan="4">No records available</td>
   </tr>
   <%}%>
</table>
<%}%>
</div>
                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->

        </div>
    </body>
</html>
