<%--
    Document   : NOAIssuedList
    Created on : Mar 4, 2011, 4:09:28 PM
    Author     : Karan
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Performance Security Payment</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />


        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />

        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript">

            function DateOver(){
                jAlert("Last date for Performance Security Submission has been lapsed. Performance Security payment can't be accepted now.", "Payment");
                return true;
            }
            function cancelTender(){
                jAlert("Tender/Proposal has been cancled.", "Payment");
                return false;
            }


            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>

        <script type="text/javascript">
            function fillGridOnEvent(userId, branchMakerId, tenderId, emailId, tendererNm ){
                /*
                alert(userId);
                alert(branchMakerId);
                alert(tenderId);
                alert(emailId);
                alert(tendererNm);
                */

                $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                jQuery("#list").jqGrid({
                    url:'<%=request.getContextPath()%>/TenderPaymentServlet?q=1&action=fetchNOAData&userId=' + userId + '&emailId=' + emailId + '&branchMakerId=' + branchMakerId + '&tenderId=' + tenderId + '&tendererNm=' + tendererNm,
                    datatype: "xml",
                    height: 250,
                    colNames:['Sl. No.','e-mail ID','Name of Bidder / Consultant','Contract No.',"Date of Contract (NOA issuance Date)","Status","Action"],
                    colModel:[
                        {name:'RowNumber',index:'RowNumber', width:40,sortable:false, align: 'center'},                        
                        {name:'emailId',index:'emailId', width:250,sortable:false},
                        {name:'tendererName',index:'tendererName', width:250,sortable:false},
                        {name:'contractNo',index:'contractNo', width:100,sortable:false, align: 'center'},
                        {name:'dtOfContract',index:'dtOfContract', width:140,sortable:false, align: 'center'},
                        {name:'status',index:'status', width:100,sortable:false, align: 'center'},
                        {name:'action',index:'action', width:100,sortable:false, align: 'center'}

                    ],
                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    sortable:false,
                    caption: "",
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{edit:false,add:false,del:false});
            }
            jQuery().ready(function (){
                //fillGrid();
            });
        </script>

                    <script type="text/javascript">
                        $(function() {
                           
                            $('#btnSearch').click(function() {
                                //alert('You clicked Search');
                                var userId='', branchMakerId='', tenderId='', emailId='', tendererNm='';
                                userId=$('#userId').val();
                                branchMakerId=userId;
                                tenderId=$('#hdnTenderId').val();
                                emailId=$('#txtEmailId').val();
                                tendererNm=$('#txtTendererNm').val();
                                
                             fillGridOnEvent(userId, branchMakerId, tenderId, emailId, tendererNm);

                            });

                            $('#btnReset').click(function() {
                                //alert('Clearing...')
                                $('#txtEmailId').val('');
                                $('#txtTendererNm').val('');

                                var userId='', branchMakerId='', tenderId='', emailId='', tendererNm='';
                                userId=$('#userId').val();
                                branchMakerId=userId;
                                tenderId=$('#hdnTenderId').val();
                                emailId='';
                                tendererNm='';

                                fillGridOnEvent(userId, branchMakerId, tenderId, emailId, tendererNm);
                            });


                        });
                    </script>

                    
                  
</head>


    <%
    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    if (session.getAttribute("userId") != null) {
                                tenderCommonService.setLogUserId(session.getAttribute("userId").toString());
            }
    
     boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false;
     String userBranchId="";
     String userId = "";
     String tenderId="";
                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") != null) {
                    userId = hs.getAttribute("userId").toString();
                } else {
                    //response.sendRedirectFilter(request.getContextPath() + "/SessionTimedOut.jsp");
                }

                List<SPTenderCommonData> lstCurBankUserRole = tenderCommonService.returndata("getBankUserRole",userId,null);
                if(!lstCurBankUserRole.isEmpty()){
                    if("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBranchMaker=true;
                    } else if("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBranchChecker=true;
                    } else if("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBankChecker=true;
                    }
                    userBranchId=lstCurBankUserRole.get(0).getFieldName2();
                }

                 if (request.getParameter("tenderId") !=null){
                        tenderId=request.getParameter("tenderId");
                    }
    %>
    <body onload="fillGridOnEvent('<%=userId%>','<%=userId%>','<%=tenderId%>','','');">
    <div class="dashboard_div">
      <!--Dashboard Header Start-->
      <%@include  file="../resources/common/AfterLoginTop.jsp" %>
      <!--Dashboard Header End-->
      <!--Dashboard Content Part Start-->
      <div class="contentArea_1">
          <div class="pageHead_1">Performance Security Payment
              <span style="float: right;" >
                  <a href="ForTenderPayment.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go To Main Payment Page</a>
              </span>
        </div>
              <form id="frmData" method="post">
          <div class="formBg_1 t_space">
              <input type="hidden" name="userId" id="userId" value="<%=userId%>"></input>
              <input type="hidden" name="hdnTenderId" id="hdnTenderId" value="<%=tenderId%>"></input>
              <table cellspacing="10" class="formStyle_1" width="100%">
                  <tr>
                      <td width="10%" class="ff">e-mail ID :</td>
                      <td width="50%">
                          <input name="txtEmailId" type="text" class="formTxtBox_1" id="txtEmailId" style="width:195px;" />
                      </td>
                      <td width="15%" class="ff">Bidder / Consultant :</td>
                      <td width="25%" ><input name="txtTendererNm" type="text" class="formTxtBox_1" id="txtTendererNm" style="width:195px;" /></td>
                  </tr>                  
                  <tr>
                      <td colspan="4" class="t-align-center ff"><label class="formBtn_1">
                              <input type="button" name="btnSearch" id="btnSearch" value="Search" />
                          </label>

                          <label class="formBtn_1 l_space">
                              <input type="button" name="btnReset" id="btnReset" value="Reset" />
                          </label>
                      </td>
                  </tr>
              </table>
          </div>
      </form>

<div class="tabPanelArea_1 t_space">
    <div id="jQGrid" align="center">
        <table id="list"></table>
        <div id="page"></div>
    </div>
</div>

        <div>&nbsp;</div>
        </div>
</div>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
 <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
  <!--Dashboard Footer End-->

</body>

  <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
