<%--
    Document   : TenderPaymentDocsUpload
    Created on : Nov 29, 2010, 3:29:18 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Payment Reference Document</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">

            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:100}
                    },
                    messages: {
                        uploadDocFile: { required: "<div class='reqF_1'>Please select Document</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please enter Description</div>",
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                    }
                });
            });

            $(function() {
            $('#frmUploadDoc').submit(function() {
                if($('#frmUploadDoc').valid()){
                    $('.err').remove();
                    var count = 0;
                    var browserName=""
                    var maxSize = parseInt($('#fileSize').val())*1024*1024;
                    var actSize = 0;
                    var fileName = "";
                    jQuery.each(jQuery.browser, function(i, val) {
                         browserName+=i;
                    });
                    $(":input[type='file']").each(function(){
                        if(browserName.indexOf("mozilla", 0)!=-1){
                            actSize = this.files[0].size;
                            fileName = this.files[0].name;
                        }else{
                            var file = this;
                            var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                            var filepath = file.value;
                            var thefile = myFSO.getFile(filepath);
                            actSize = thefile.size;
                            fileName = thefile.name;
                        }
                        if(parseInt(actSize)==0){
                            $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                            count++;
                        }
                        if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                            $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                            count++;
                        }
                        if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                            $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                            count++;
                        }
                    });
                    if(count==0){
                        $('#button').attr("disabled", "disabled");
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            });
        });
        </script>

        <%--<script type="text/javascript">
            function checkfile()
            {
                var check = false;
                value1=document.getElementById('uploadDocFile').value;
                param="document";
                var ext = value1.split(".");
                var fileType = ext[ext.length-1];
                fileType=fileType.toLowerCase();
                if (value1 != '' && param=="uploadDocFile" && ( fileType =='pdf'|| fileType =='zip'|| fileType =='jpeg'||
                    fileType =='jpg' || fileType =='gif' || fileType =='doc' || fileType =='docx' ||fileType =='xls' || fileType =='xlsx' ||
                    fileType =='dwg' || fileType =='dwg' || fileType =='dxf')){
                    document.getElementById('filemsg').innerHTML='';
                    check = true;

                }
                if (document.getElementById('documentBrief').value=="" && check == false)
                {
                    document.getElementById('filemsg').innerHTML="Please select file.";
                    document.getElementById('dvDescpErMsg').innerHTML="<div class='reqF_1'>Please enter Description.</div>";
                    return false;
                }
                if (document.getElementById('documentBrief').value=='' && check == true){

                    document.getElementById('dvDescpErMsg').innerHTML="<div class='reqF_1'>Please enter Description.</div>";
                    return false;
                }else if (document.getElementById('documentBrief').value != '' && check == false){

                    document.getElementById('filemsg').innerHTML="Please select file.";
                    return false;
                }else{
                    document.getElementById('filemsg').innerHTML = '';
                    document.getElementById('dvDescpErMsg').innerHTML='';
                    return true;
                }

            }

        </script>--%>
    </head>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="contentArea_1">
                <%
                            String tenderPaymentId = "";
                            if (session.getAttribute("userId") == null) {
                                response.sendRedirect("SessionTimedOut.jsp");
                            }
                            String tenderId = "";
                            if (request.getParameter("tenderId") != null) {
                                tenderId = request.getParameter("tenderId");
                            }
                            String queryString = "";
                            if (request.getQueryString() != null) {
                                queryString = request.getQueryString();
                            }
                            String pageName = "";
                            String uri = "";
                            if (request.getRequestURI() != null) {
                                uri = request.getRequestURI();
                            }
                            String url = "";
                            if (request.getRequestURL() != null) {
                                url = request.getRequestURL().toString();
                            }
                            // Dohatec Work Start Here - Get Payment Type For this page - Wahid Abdullah
                            String payUserId = "", pkgLotId = "", payTyp = "", psdocupload = "";
                            if (request.getParameter("payTyp") != null) {
                               if (request.getParameter("payTyp") != "" && !request.getParameter("payTyp").equalsIgnoreCase("null")) {
                                     payTyp = request.getParameter("payTyp");
                               }
                            }
                            // Dohatec Work End Here - Get Payment Type For this page - Wahid Abdullah
                            if (request.getParameter("page") == null) {
                                String temp = url.substring(0, url.indexOf(uri));
                                pageName = request.getHeader("referer");
                                pageName = (pageName.replace(temp, ""));
                                pageName = pageName.replace("&", "_");
                                pageName = pageName.replace("?", "$");
                                pageName = pageName.replace("=", "-");

                            }


                            if (request.getParameter("tenderPaymentId") != null) {
                                tenderPaymentId = (request.getParameter("tenderPaymentId"));
                            }
                            if (request.getParameter("page") != null) {
                                pageName = (request.getParameter("page"));
 
                                pageName = pageName.replace("_", "&");
                                pageName = pageName.replace("$", "?");
                                pageName = pageName.replace("-", "=");

                            }

                            // Dohatec Work Start Here - Set Redirect page after any action occure on documents - Wahid Abdullah
                            if(payTyp.equalsIgnoreCase("ps")) {

                                 if (request.getParameter("lotId") != null) {
                                    if (request.getParameter("lotId") != "" && !request.getParameter("lotId").equalsIgnoreCase("null")) {
                                       pkgLotId = request.getParameter("lotId");
                                     }
                                  }

                                 if (request.getParameter("uId") != null) {
                                    if (request.getParameter("uId") != "" && !request.getParameter("uId").equalsIgnoreCase("null")) {
                                       payUserId = request.getParameter("uId");
                                     }
                                  }
                                  psdocupload = "partner/TenderPaymentDocsUpload.jsp?tenderId="+tenderId+"&tenderPaymentId="+tenderPaymentId+"&payTyp="+payTyp+"&uId="+payUserId+"&lotId="+pkgLotId;
                             }
                            // Dohatec Work End Here - Set Redirect page after any action occure on documents - Wahid Abdullah

                %>
                <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/TenderPaymentDocUploadServlet" enctype="multipart/form-data" name="frmUploadDoc">
                    <input type="hidden" name="tenderPaymentId" value="<%= tenderPaymentId%>"/>
                    <input type="hidden" name="pageName" value="<%=pageName%>"/>
                    <input type="hidden" name="psDocUpload" value="<%=psdocupload%>"/>
                    <div class="pageHead_1">Payment Reference Document
                        <%if (request.getParameter("page") != null) { %>
                        <span style="float:right;"><a href="<%=pageName%>" class="action-button-goback">Go Back</a></span>
                        <%} else {
                             // Dohatec Work Start Here - Set Go Back Button link - Wahid Abdullah
                              String PageRef = "";
                              if(payTyp.equalsIgnoreCase("ps")) {
                                  String docUserId="0";
                                  HttpSession docHS = request.getSession();
                                  if (docHS.getAttribute("userId") != null) {
                                      docUserId = docHS.getAttribute("userId").toString();
                                   }
                                  boolean docIsBranchMaker=false, docIsBranchChecker=false, docIsBankChecker=false;
                                  String docUserBranchId="";
                                  TenderCommonService objCommonDocTCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                  if (session.getAttribute("userId") != null) {
                                      objCommonDocTCS.setLogUserId(session.getAttribute("userId").toString());
                                   }
                                  List<SPTenderCommonData> docCurBankUserRole = objCommonDocTCS.returndata("getBankUserRole", docUserId, null);
                                    if (!docCurBankUserRole.isEmpty()) {
                                        if ("BranchMaker".equalsIgnoreCase(docCurBankUserRole.get(0).getFieldName1())) {
                                            docIsBranchMaker = true;
                                        } else if ("BranchChecker".equalsIgnoreCase(docCurBankUserRole.get(0).getFieldName1())) {
                                            docIsBranchChecker = true;
                                        } else if ("BankChecker".equalsIgnoreCase(docCurBankUserRole.get(0).getFieldName1())) {
                                            docIsBankChecker = true;
                                        }
                                        docUserBranchId = docCurBankUserRole.get(0).getFieldName2();
                                    }

                                    if(docIsBranchMaker){
                                        PageRef = "TenderPaymentDetails.jsp?payId="+tenderPaymentId+"&uId=" + payUserId + "&tenderId="+tenderId+"&lotId="+pkgLotId+"&payTyp="+payTyp;
                                     }
                                    if(docIsBranchChecker){
                                        PageRef = "VerifyTenderPayDetails.jsp?reqId=&payId="+tenderPaymentId+"&uId="+payUserId+"&tenderId="+tenderId+"&lotId="+pkgLotId+"&payTyp="+payTyp;
                                     }

                               }
                              else {
                                  PageRef = request.getHeader("referer");
                                }
                           // Dohatec Work End Here - Set Go Back Button link - Wahid Abdullah
                         %>
                        <!-- <span style="float:right;"><a href="<%=request.getHeader("referer")%>" class="action-button-goback">Go Back</a></span> -->
                         <span style="float:right;"><a href="<%=PageRef%>" class="action-button-goback">Go Back</a></span>
                        <%}%>
                    </div>
                    <%   pageContext.setAttribute("tenderId", tenderId);%>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                    <%
                                if (request.getParameter("fq") != null) {
                                    if (request.getParameter("fq").equals("Removed") || request.getParameter("fq").equals("Uploaded")) {
                    %>
                    <div class="responseMsg successMsg" style="margin-top: 10px;">File <%=request.getParameter("fq")%> Successfully</div>
                    <%  } else {
                    %>
                    <div class="responseMsg errorMsg t_space"><%=request.getParameter("fq")%></div>
                    <%
                                                        }}
                                                        if (request.getParameter("fs") != null) {
                    %>
                    <div class="responseMsg errorMsg t_space">
                        Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                    </div>
                    <%
                                    }
                               // }  Modified By dohatec for document upload checking
                    %>
                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>

                            <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                        </tr>
                        <tr>
                            <td width="10%" class="ff t-align-left">Document   : <span class="mandatory">*</span></td>
                            <td width="90%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Description : <span>*</span></td>
                            <td>
                                <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                <div id="dvDescpErMsg" class='reqF_1'></div>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <%--<label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Upload" onclick="return checkfile()" /></label>--%>
                                <label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Upload" /></label>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th  class="t-align-left">Instructions</th>
                        </tr>
                        <tr>
                            <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("common");%>
                            <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.

                            <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                        </tr>
                    </table>
                </form>
                  <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="23%">File Name</th>
                        <th class="t-align-center" width="32%">File Description</th>
                        <th class="t-align-center" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-center" width="18%">Action</th>
                    </tr>
                    <%
                                int docCnt = 0;
                                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                if (session.getAttribute("userId") != null) {
                                    tenderCS.setLogUserId(session.getAttribute("userId").toString());
                                 }
                                for (SPTenderCommonData sptcd : tenderCS.returndata("TenderPaymentDocs", tenderPaymentId, "")) {
                                    docCnt++;
                                    if (pageName.contains("&") && pageName.contains("?") && pageName.contains("=")) {
                                        pageName = pageName.replace("&", "_");
                                        pageName = pageName.replace("?", "$");
                                        pageName = pageName.replace("=", "-");
                                    }                                    
                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/TenderPaymentDocUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderPaymentId=<%=tenderPaymentId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                            &nbsp;
                            <a href="<%=request.getContextPath()%>/TenderPaymentDocUploadServlet?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&tenderPaymentId=<%=tenderPaymentId%>&pageName=<%=pageName%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                        </td>
                    </tr>

                    <%   if (sptcd != null) {
                                        sptcd = null;
                                    }
                                }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                </table>
            </div>
            <div>&nbsp;</div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>
       <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>

</html>
