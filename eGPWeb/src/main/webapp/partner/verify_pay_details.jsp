<%

            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userId").toString();
            }
%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Verify Payment Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" >
            $(document).ready(function(){
                $('#btnVerify').click(function(){
                    var flag=true;

                    if($.trim($('#txtComments').val())==''){
                        $('#SpError').html('Please enter Remarks.');
                        flag=false;
                    }
                    else {
                        if($('#txtComments').length<=1000) {
                            $('#SpError').html('');
                        }
                        else{
                            $('#SpError').html('Maximum 1000 characters are allowed.');
                            flag=false;
                        }
                    }

                    if(flag==false){
                        return false;
                    }

                });

                $('#txtComments').blur(function(){
                    var flag=true;

                    if($.trim($('#txtComments').val())==''){
                        $('#SpError').html('Please enter Remarks.');
                        flag=false;
                    }
                    else {
                        if($('#txtComments').length<=1000) {
                            $('#SpError').html('');
                        }
                        else{
                            $('#SpError').html('Maximum 1000 characters are allowed.');
                            flag=false;
                        }
                    }

                    if(flag==false){
                        return false;
                    }
                });
                
            });
        </script>

    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <%
                        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        tenderCommonService.setLogUserId(strUserTypeId);
                        boolean isBankUser = false;
                        String strPartTransId="0", userId = "", payUserId = "", regPaymentId = "";

            %>
            <%

                        HttpSession hs = request.getSession();
                        if (hs.getAttribute("userId") != null) {
                            userId = hs.getAttribute("userId").toString();
                        } else {
                            response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
                        }

                        if (hs.getAttribute("govUserId") != null) {
                     strPartTransId = hs.getAttribute("govUserId").toString();
              }


                        String referer = "";
                        if (request.getHeader("referer") != null) {
                            referer = request.getHeader("referer");
                            
                        }

                        if (hs.getAttribute("userTypeId") != null) {
                            if ("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())) {
                                isBankUser = true; // userType is ScheduleBank";
                            }
                        }

                        if (request.getParameter("uId") != null) {
                            payUserId = request.getParameter("uId");
                            
                        }

                        if (request.getParameter("payId") != null) {
                            regPaymentId = request.getParameter("payId");
                            
                        }
                        
                        

                        if (request.getParameter("btnVerify") != null) {
                            HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            //String regValidityYrPeriod=XMLReader.getMessage("RegValidityYearPeriod");
                            //String regValidityYrPeriodInWords=XMLReader.getMessage("RegValidityYearPeriodinWords");
                            String regValidityYrPeriod = "", regValidityYrPeriodInWords = "";
                            String path = "", remarks = "", verificationId = "0";
                            String currency = "", fnlAmt = "", currencySymbol = "", validityRefId = "", status = "", strBidderUserId = "0";

                            currency = request.getParameter("hdnCurrency");
                            fnlAmt = request.getParameter("hdnAmount");
                            remarks = request.getParameter("txtComments");

                            validityRefId = request.getParameter("hdnValidityRefId");
                            status = request.getParameter("hdnPayStatus");
                            strBidderUserId = request.getParameter("hdnBidderUserId");

                            if ("BTN".equalsIgnoreCase(currency)) {
                                currencySymbol = "Nu. ";
                            } else if ("USD".equalsIgnoreCase(currency)) {
                                currencySymbol = "USD $";
                            }
                            else if ("Nu.".equalsIgnoreCase(currency)) {
                                currencySymbol = "Nu. ";
                            }

                            /* START: CODE TO HANDLE SPECIAL CHARACTERS  */
                            remarks = handleSpecialChar.handleSpecialChar(remarks);
                            /* END CODE TO HANDLE SPECIAL CHARACTERS  */

                            String dtXml = "";
                            dtXml = "<tbl_PaymentVerification paymentId=\"" + regPaymentId + "\" verifiedBy=\"" + userId + "\" remarks=\"" + remarks + "\" verificationDt=\"" + format.format(new Date()) + "\" verifyPartTransId=\"" + strPartTransId + "\" />";
                            dtXml = "<root>" + dtXml + "</root>";

                            CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            CommonMsgChk commonMsgChk;
                            commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_PaymentVerification", dtXml, "").get(0);
                            verificationId = commonMsgChk.getId().toString();

                            if (commonMsgChk.getFlag().equals(true)) {
                                commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_RegFeePayment", "isVerified='yes'", "regPaymentId=" + regPaymentId).get(0);
                                if (commonMsgChk.getFlag().equals(true)) {
                                    List<SPTenderCommonData> lstValidityMonths = tenderCommonService.returndata("getValidityMonthsFromId", validityRefId, null);
                                    String ValidityMnths = "";

                                    if (!lstValidityMonths.isEmpty()) {
                                        ValidityMnths = lstValidityMonths.get(0).getFieldName1();
                                        regValidityYrPeriod = lstValidityMonths.get(0).getFieldName2();
                                        regValidityYrPeriodInWords = lstValidityMonths.get(0).getFieldName3();
                                    }

                                    if ("renewed".equalsIgnoreCase(status)) {
                                        // Start: Code To Update Valid Upto Date in Table - tbl_LoginMaster
                                        commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_LoginMaster", "validUpTo=dateadd(m," + ValidityMnths + ",validUpTo)", "userId=" + strBidderUserId).get(0);
                                        // End: Code To Update Valid Upto Date in Table - tbl_LoginMaster

                                        if (commonMsgChk.getFlag().equals(true)) {
                                            List<SPTenderCommonData> lstRenewed_ExpiryDts = tenderCommonService.returndata("getRenewed_ExpiryDt", regPaymentId, null);

                                            if (!lstRenewed_ExpiryDts.isEmpty()) {
                                                String renewalDt = "", expiryDt = "";
                                                renewalDt = lstRenewed_ExpiryDts.get(0).getFieldName1();
                                                expiryDt = lstRenewed_ExpiryDts.get(0).getFieldName2();

                                                /* START : CODE TO SEND RENEWAL MAIL */

                                                String mailSubject = "e-GP System: Your Account Renewed Successfully";
                                                String tendererCompanyNm = "", strFrmEmailId = "";

                                                List<SPTenderCommonData> frmEmail = tenderCommonService.returndata("getEmailIdfromUserId", userId, null);
                                                strFrmEmailId = frmEmail.get(0).getFieldName1();
                                                List<SPTenderCommonData> emails = tenderCommonService.returndata("getEmailIdfromUserId", payUserId, null);
                                                UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

                                                String[] mail = {emails.get(0).getFieldName1()};
                                                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                                                MailContentUtility mailContentUtility = new MailContentUtility();
                                                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                                                String mailText = mailContentUtility.contRegistrationRenewal(fnlAmt, currencySymbol, regValidityYrPeriod, regValidityYrPeriodInWords, renewalDt, expiryDt);

                                                sendMessageUtil.setEmailTo(mail);

                                                sendMessageUtil.setEmailSub(mailSubject);
                                                sendMessageUtil.setEmailMessage(mailText);

                                                //userRegisterService.contentAdmMsgBox(mail[0], strFrmEmailId, HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contRegistrationRenewal(fnlAmt, currencySymbol, regValidityYrPeriod, regValidityYrPeriodInWords, renewalDt, expiryDt));
                                                sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailId"));
                                                sendMessageUtil.sendEmail();

                                                sendMessageUtil = null;
                                                mailContentUtility = null;
                                                mail = null;
                                                msgBoxContentUtility = null;

                                                /* END : CODE TO SEND RENEWAL MAIL */

                                            }
                                        }
                                    } else if ("paid".equalsIgnoreCase(status)) {
                                        /* START : CODE TO SEND MAIL */
                                        String mailSubject = "e-GP System:  User Registration - Registration Fee Received";
                                        String tendererCompanyNm = "", strFrmEmailId = "";

                                        List<SPTenderCommonData> frmEmail = tenderCommonService.returndata("getEmailIdfromUserId", userId, null);
                                        strFrmEmailId = frmEmail.get(0).getFieldName1();
                                        List<SPTenderCommonData> emails = tenderCommonService.returndata("getEmailIdfromUserId", payUserId, null);
                                        UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");

                                        String[] mail = {emails.get(0).getFieldName1()};
                                        SendMessageUtil sendMessageUtil = new SendMessageUtil();
                                        MailContentUtility mailContentUtility = new MailContentUtility();
                                        MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                                        String mailText = mailContentUtility.contRegistrationPayment(fnlAmt, currencySymbol, regValidityYrPeriod, regValidityYrPeriodInWords);

                                        sendMessageUtil.setEmailTo(mail);

                                        sendMessageUtil.setEmailSub(mailSubject);
                                        sendMessageUtil.setEmailMessage(mailText);

                                        //userRegisterService.contentAdmMsgBox(mail[0], strFrmEmailId, HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contRegistrationPayment(fnlAmt, currencySymbol, regValidityYrPeriod, regValidityYrPeriodInWords));
                                        sendMessageUtil.setEmailFrom(XMLReader.getMessage("emailId"));
                                        sendMessageUtil.sendEmail();

                                        sendMessageUtil = null;
                                        mailContentUtility = null;
                                        mail = null;
                                        msgBoxContentUtility = null;
                                        /* END : CODE TO SEND MAIL */
                                    }

                                    path = "RegistrationFeePaymentDetails.jsp?payId=" + regPaymentId + "&uId=" + payUserId + "&msgId=verified";
                                } else {
                                    path = "VerifyPayDetails.jsp?payId=" + regPaymentId + "&uId=" + payUserId + "&msgId=error";
                                }
                            } else {
                                path = "VerifyPayDetails.jsp?payId=" + regPaymentId + "&uId=" + payUserId + "&msgId=error";
                            }

                            response.sendRedirect(path);
                        }

            %>
            <div class="contentArea_1">
                <div class="pageHead_1">

                    <label id="lblPageHead">Verify Registration Fee Payment Details</label>
                    <span style="float:right;"><a href="VerifyPayment.jsp" class="action-button-goback">Go Back</a></span>
                </div>

                <div class="tabPanelArea_1 t_space">

                    <%if (request.getParameter("msgId") != null) {
                                    String msgId = "", msgTxt = "";
                                    boolean isError = false;
                                    msgId = request.getParameter("msgId");
                                    if (!msgId.equalsIgnoreCase("")) {
                                        if (msgId.equalsIgnoreCase("payment")) {
                                            msgTxt = "Payment information entered successfully.";
                                        } else if (msgId.equalsIgnoreCase("updated")) {
                                            msgTxt = "Payment updated successfully.";
                                        } else if (msgId.equalsIgnoreCase("extended")) {
                                            msgTxt = "Payment extended successfully.";
                                        } else if (msgId.equalsIgnoreCase("released")) {
                                            msgTxt = "Payment released successfully.";
                                        } else if (msgId.equalsIgnoreCase("canceled")) {
                                            msgTxt = "Payment canceled successfully.";
                                        } else if (msgId.equalsIgnoreCase("on-hold")) {
                                            msgTxt = "Payment put on-hold successfully.";
                                        } else if (msgId.equalsIgnoreCase("forfeited")) {
                                            msgTxt = "Payment forfeited successfully.";
                                        } else if (msgId.equalsIgnoreCase("forfeitrequested")) {
                                            msgTxt = "Payment forfeit request submitted successfully.";
                                        } else if (msgId.equalsIgnoreCase("releaserequested")) {
                                            msgTxt = "Payment release request submitted successfully.";
                                        } else if (msgId.equalsIgnoreCase("error")) {
                                            isError = true;
                                            msgTxt = "There was some error.";
                                        } else {
                                            msgTxt = "";
                                        }
                    %>
                    <%if (isError) {%>
                    <div class="responseMsg errorMsg" ><%=msgTxt%></div>
                    <%} else {%>
                    <div class="responseMsg successMsg" ><%=msgTxt%></div>
                    <%}%>
                    <%}
                            }%>

                    <%
                                String bidderUserId = "0", bidderEmail = "";
                                String emailId = "";

                                List<SPTenderCommonData> lstTendererEml = tenderCommonService.returndata("getEmailIdfromUserId", payUserId, null);
                                emailId = lstTendererEml.get(0).getFieldName1();
                                bidderEmail = emailId;
                                bidderUserId = payUserId;
                    %>

                    <%
                                List<SPTenderCommonData> lstPaymentDetail = tenderCommonService.returndata("getRegistrationFeePaymentDetail", regPaymentId, null);
                                if (!lstPaymentDetail.isEmpty()) {
                    %>

                    <form id="frmVerifyPayment" action="VerifyPayDetails.jsp?payId=<%=regPaymentId%>&uId=<%=request.getParameter("uId")%>" method="POST">


                        <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                            <tr>
                                <td style="font-style: italic" class="ff t-align-right" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <tr>
                                <td width="18%" class="ff">e-mail ID :</td>
                                <td><%=bidderEmail%></td>
                            </tr>
                            <tr>
                                <td class="ff">Branch Name :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="ff">Branch Maker :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName2()%></td>
                            </tr>
                            <tr>
                                <td class="ff">Currency :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName3()%>
                                    <input type="hidden" id="hdnCurrency" name="hdnCurrency" value="<%=lstPaymentDetail.get(0).getFieldName3()%>">
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Amount :</td>
                                <td>
                                    <%if ("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())) {%>
                                    <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                    <%} else if ("USD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())) {%>
                                    <label>$</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                    <%} else if ("Nu.".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())) {%>
                                    <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                    <%}%>
                                    <input type="hidden" id="hdnAmount" name="hdnAmount" value="<%=lstPaymentDetail.get(0).getFieldName4()%>">
                                    <input type="hidden" id="hdnValidityRefId" name="hdnValidityRefId" value="<%=lstPaymentDetail.get(0).getFieldName9()%>">
                                    <input type="hidden" id="hdnPayStatus" name="hdnPayStatus" value="<%=lstPaymentDetail.get(0).getFieldName10()%>">
                                    <input type="hidden" id="hdnBidderUserId" name="hdnBidderUserId" value="<%=bidderUserId%>">

                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Mode of Payment :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName5()%></td>
                            </tr>
                            <%if ("Pay Order".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                                            List<SPTenderCommonData> lstPaymentDetailMore = tenderCommonService.returndata("getRegistrationFeePaymentDetailMore", regPaymentId, null);

                                                            if (!lstPaymentDetailMore.isEmpty()) {%>

                            <tr>
                                <td class="ff">Instrument No. :</td>
                                <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="ff">Issuing Financial Institute :</td>
                                <td><%=lstPaymentDetailMore.get(0).getFieldName2()%></td>
                            </tr>
                            <tr>
                                <td class="ff">Issuing Financial Institute Branch :</td>
                                <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                            </tr>
                            <tr>
                                <td class="ff">Issuance Date :</td>
                                <td><%=lstPaymentDetailMore.get(0).getFieldName4()%></td>
                            </tr>
                            <tr>
                                <td class="ff">Validity Date :</td>
                                <td><%=lstPaymentDetailMore.get(0).getFieldName5()%></td>
                            </tr>

                            <% }
                                                               }%>

                            <%if ("Account to Account Transfer".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                                                List<SPTenderCommonData> lstPaymentDetailMore = tenderCommonService.returndata("getRegistrationFeePaymentDetailMore", regPaymentId, null);

                                                                if (!lstPaymentDetailMore.isEmpty()) {%>

                            <tr>
                                <td class="ff">Account No. :</td>
                                <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                            </tr>
                            <%if (!"".equalsIgnoreCase(lstPaymentDetailMore.get(0).getFieldName3())) {%>
                            <tr>
                                <td class="ff">Branch Name :</td>
                                <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                            </tr>
                            <%}%>
                            <% }
                                                               }%>

                            <tr>
                                <td class="ff">Date of Payment :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName6()%></td>
                            </tr>

                            <tr>
                                <td class="ff">Branch Maker Remarks : </td>
                                <td>
                                    <%=lstPaymentDetail.get(0).getFieldName7()%>
                                </td>
                            </tr>

                            <tr>
                                <td class="ff">Remarks : <span class="mandatory">*</span></td>
                                <td>
                                    <textarea cols="100" rows="5" id="txtComments" name="txtComments" class="formTxtBox_1"></textarea>
                                    <span id="SpError" class="reqF_1"></span>
                                    <%//=lstPaymentDetail.get(0).getFieldName7()%>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <div class="t-align-left">
                                        <label class="formBtn_1"><input name="btnVerify" id="btnVerify" type="submit" value="Verify" /></label>
                                    </div>

                                </td>
                            </tr>

                        </table>

                    </form>
                    <% } else {%>
                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td>Payment information not found!</td>
                        </tr>
                    </table>
                    <%}%>
                </div>

                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->

                <script>
                    if(document.getElementById("hdnPayStatus")!=null){                        
                       if(document.getElementById("hdnPayStatus").value=="renewed"){                          
                            document.getElementById("lblPageHead").innerHTML="Verify Renewal Fee Payment Details";

                       }
                    }
                </script>

            </div>
    </body>
</html>
