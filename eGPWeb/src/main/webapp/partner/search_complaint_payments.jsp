<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%
            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userId").toString();
            }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Complaint <%=session.getAttribute("feetype")%> Payment</title>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

		<link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />

        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript" src="../js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery.alerts.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <link href="../js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
<style type="text/css">
.pg-normal {
color: #000000;
font-weight: normal;
text-decoration: none;
cursor: pointer;
}

.pg-selected {
color: #800080;
font-weight: bold;
text-decoration: underline;
cursor: pointer;
}
</style>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#frmSearchTenderer').validate({
                    rules:{ txtEmailId: {required:true, email:true}

                    },
                    messages:{txtEmailId: {required:"<div class='reqF_1'>Please enter e-mail ID</div>",
                            email: "<div class='reqF_1'>Please enter valid e-mail ID</div>" }
                    }
                });
            });
        </script>
<script type="text/javascript">
            function emailSearch(button)
            {
				if((document.getElementById("txtEmailId").value==""))
				{
					alert("enter the email id");
					return false;
				}
				else
				{
					document.frmSearchTenderer.action="dispTenderer.htm";
					document.frmSearchTenderer.submit();
					return true;
				}
            }
        </script>
<script type="text/javascript">

function Pager(tableName, itemsPerPage) {
this.tableName = tableName;
this.itemsPerPage = itemsPerPage;
this.currentPage = 1;
this.pages = 0;
this.inited = false;

this.showRecords = function(from, to) {
var rows = document.getElementById(tableName).rows;
// i starts from 1 to skip table header row
for (var i = 1; i < rows.length; i++) {
if (i < from || i > to)
rows[i].style.display = 'none';
else
rows[i].style.display = '';
}
}

this.showPage = function(pageNumber) {
if (! this.inited) {
alert("not inited");
return;
}

var oldPageAnchor = document.getElementById('pg'+this.currentPage);
oldPageAnchor.className = 'pg-normal';

this.currentPage = pageNumber;
var newPageAnchor = document.getElementById('pg'+this.currentPage);
newPageAnchor.className = 'pg-selected';

var from = (pageNumber - 1) * itemsPerPage + 1;
var to = from + itemsPerPage - 1;
this.showRecords(from, to);
}

this.first = function() {

this.showPage(1);
}

this.prev = function() {
if (this.currentPage > 1)
this.showPage(this.currentPage - 1);
}

this.next = function() {
if (this.currentPage < this.pages) {
this.showPage(this.currentPage + 1);
}


}
this.last = function() {

this.showPage(this.pages);
}
this.goton = function(pageNO) {

if(pageNO.value <= this.pages){
this.showPage(pageNO.value);
}
}


this.init = function() {
var rows = document.getElementById(tableName).rows;
var records = (rows.length - 1);
this.pages = Math.ceil(records / itemsPerPage);
this.inited = true;
}

this.showPageNav = function(pagerName, positionId) {
if (! this.inited) {
alert("not inited");
return;
}
var element = document.getElementById(positionId);
var pagerHtml = '<span>Page '+this.currentPage+' - '+this.pages +'</span>  &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; ';


pagerHtml += '&nbsp; &nbsp; &nbsp;<input type="text" style="width: 20px;" class="formTxtBox_1" value="" id="pageNo" >';
pagerHtml += '<label class="formBtn_1 l_space"><input type="button" value="Go To Page" onclick="'+pagerName+'.goton(pageNo);"></label> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;  ';
for (var page = 1; page <= this.pages; page++)
pagerHtml += '<span id="pg' + page + '" class="pg-normal" style="color: gray;" onclick="' + pagerName + '.showPage(' + page + ');"> </span>  ';
pagerHtml += '<span style="color: gray;" onclick="' + pagerName + '.first();" class="pg-normal"> << First &nbsp;  </span>  ';
pagerHtml += '<span style="color: gray;" onclick="' + pagerName + '.prev();" class="pg-normal"> < Previous&nbsp; &nbsp; </span>  ';

pagerHtml += '<span style="color: gray;" onclick="'+pagerName+'.next();" class="pg-normal"> Next&nbsp; &nbsp;  ></span>';
pagerHtml += '<span style="color: gray;" onclick="'+pagerName+'.last();" class="pg-normal"> Last >></span>';


element.innerHTML = pagerHtml;

}
}

</script>
    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <%
                        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        tenderCommonService.setLogUserId(strUserTypeId);
                        boolean isBankUser = false;
                        String userId = "", srchUserId = "";
                        boolean isBranchMaker = false, isBranchChecker = false, isBankChecker = false;
                        String userBranchId = "";

                        HttpSession hs = request.getSession();
                        if (hs.getAttribute("userId") != null) {
                            userId = hs.getAttribute("userId").toString();
                        } else {
                            response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
                        }

                        String referer = "";
                        if (request.getHeader("referer") != null) {
                            referer = request.getHeader("referer");
                            
                        }

                        if (hs.getAttribute("userTypeId") != null) {
                            if ("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())) {
                                isBankUser = true; // userType is ScheduleBank";
                            }
                        }

                        if (request.getParameter("btnSearch") == null) {
                            if (request.getParameter("uId") != null) {
                                if (request.getParameter("uId") != "" && !request.getParameter("uId").equalsIgnoreCase("null")) {
                                    srchUserId = request.getParameter("uId");
                                }
                            }
                        }

                        List<SPTenderCommonData> lstCurBankUserRole = tenderCommonService.returndata("getBankUserRole", userId, null);
                        if (!lstCurBankUserRole.isEmpty()) {
                            if ("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                                isBranchMaker = true;
                            } else if ("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                                isBranchChecker = true;
                            } else if ("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                                isBankChecker = true;
                            }
                            userBranchId = lstCurBankUserRole.get(0).getFieldName2();
                        }


            %>
            <div class="contentArea_1">
                <div class="pageHead_1"> Complaint <%=session.getAttribute("feetype")%> Payment
                    <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('7');">Save as PDF</a></span>
<!--                        <span style="float:right;"><a href="<%=referer%>" class="action-button-goback">Go Back</a></span>-->
                </div>

              

<script type="text/javascript"><!--
var pager = new Pager('results', 10);
pager.init();
pager.showPageNav('pager', 'pageNavPosition');
pager.showPage(1);
//--></script>
                <div class="tabPanelArea_1 t_space">
				    <%@include  file="complaint_fee_payments.jsp" %>
                </div>

            </div>
            <form id="formstyle" action="" method="post" name="formstyle">

               <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
               <%
                 SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                 String appenddate = dateFormat1.format(new Date());
               %>
               <input type="hidden" name="fileName" id="fileName" value="ComplaintFeePayment_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="ComplaintFeePayment" />
            </form>
            <!--Dashboard Content Part End-->



            <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->


        </div>
    </body>
</html>
