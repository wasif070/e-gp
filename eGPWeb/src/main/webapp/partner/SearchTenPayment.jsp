<%-- 
    Document   : SearchTenPayment
    Created on : Nov 25, 2010, 11:56:55 AM
    Author     : rajesh,rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Search Tender for making Payment</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <script type="text/javascript">
            function clearSearch(){
                document.getElementById('txtTenderId').value = '';
                document.getElementById('txtRefNO').value = '';
                loadTable();
            }
            function Validate(){
                if(($.trim(document.getElementById("txtTenderId").value)=='') && ($.trim(document.getElementById("txtRefNO").value)=='')){
                    document.getElementById("serror").innerHTML="<div class='reqF_1'>Please enter Search criteria.</div>";
                    return false;
                }
                else
                {
                    loadTable();
                    document.getElementById("serror").innerHTML="";
                }
            }

            function ValidateBlur(){
                if(($.trim(document.getElementById("txtTenderId").value)=='') && ($.trim(document.getElementById("txtRefNO").value)=='')){
                    document.getElementById("serror").innerHTML="<div class='reqF_1'>Please enter Search criteria.</div>";
                }
                else
                {
                    document.getElementById("serror").innerHTML="";
                }
            }

        </script>
        <script type="text/javascript">
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    document.getElementById('txtRefNO').value = '';
                    document.getElementById('txtTenderId').value = '';
                    if(totalPages>0 && $('#pageNo').val()!="1")
                    {
                        $('#pageNo').val("1");
                        //loadTenderTable();
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    document.getElementById('txtRefNO').value = '';
                    document.getElementById('txtTenderId').value = '';
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        //loadTenderTable();
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    document.getElementById('txtRefNO').value = '';
                    document.getElementById('txtTenderId').value = '';
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        //loadTenderTable();
                        loadTable();
                        $('#dispPage').val(Number(pageNo)+1);
                        $('#btnPrevious').removeAttr("disabled");
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    document.getElementById('txtRefNO').value = '';
                    document.getElementById('txtTenderId').value = '';
                    var pageNo=$('#pageNo').val();

                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        //loadTenderTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            //loadTenderTable();
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
        </script>
        <script type="text/javascript">
            function loadTable()
            {
                $.post("<%=request.getContextPath()%>/SearchTenPaymentServlet", {TenderID: $("#txtTenderId").val(),ReferenceNo: $("#txtRefNO").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    sortTable();
                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    chkdisble($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                });
            }
            function checkKey(e)
            {
                var keyValue = (window.event)? e.keyCode : e.which;
                if(keyValue == 13){
                    Validate();
                    loadTable();
                }
            }

        </script>
    </head>
    <body onload="loadTable();hide();" onkeypress="checkKey(event);">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
<!--                <form id="frmpaymentinfo" action="SearchTenPayment.jsp" method="POST">-->
                    <div class="pageHead_1">Search Tender for making Payment
                    <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('6');">Save as PDF</a></span>
                    </div>
                    <div>&nbsp;</div>

                    <div class="formBg_1 t_space">
                        <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                        <table cellspacing="10" class="formStyle_1" width="100%" id="tblSearchBox">
                            <tr>
                                <td width="10%" class="ff">Tender ID :</td>
                                <td width="90%"><input name="txtTenderId" type="text" class="formTxtBox_1" id="txtTenderId" style="width:195px;" onblur="ValidateBlur();"/></td>
                            </tr>
                            <tr>
                                <td class="ff">Reference No : </td>
                                <td><input name="txtRefNO" type="text" class="formTxtBox_1" id="txtRefNO" style="width:195px;" onblur="ValidateBlur();"/>
                                    <span id="serror" name="serror" />
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">&nbsp;</td>
                                <td><span class="formBtn_1">
                                        <input type="button" name="btnpaymentsearch" id="btnpaymentsearch" value="Search" onclick="return Validate();"/>
                                    </span>&nbsp;&nbsp;
                                    <span class="formBtn_1">
                                        <input type="button" name="btnpaymentreset" id="btnpaymentsearch" onclick="clearSearch();" value="Reset"/>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </div>
<!--                </form>-->

                <div class="t_space">
                    <span style="float: right;" ><a href="TodaysRecievedTenPayments.jsp" class="action-button-viewTender">View Today's Transactions</a></span>
                </div>
                
                <table width="100%" cellspacing="0" id="resultTable" class="tableList_1 t_space" cols="@0,6">
                    <tr>
                        <th class="t-align-center">Sl.&nbsp;No.</th>
                        <th class="t-align-center">Id, <br />
                            Reference No.</th>
                        <th class="t-align-center">Procurement Category, <br />
                            Title</th>
                        <th class="t-align-center"><div align="center">Hierarchy Node</div></th>
                        <th class="t-align-center"><div align="center">Procurement&nbsp;Type, <br />
                                Procurement&nbsp;Method</div></th>
                        <th class="t-align-center"><div align="center">Publishing Date | <br />
                                Closing Date</div></th>
                        <th width="10%" class="t-align-center"><div align="center">Dashboard</div></th>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                    <tr>
                        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                        <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                            &nbsp;
                            <label class="formBtn_1">
                                <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                            </label></td>
                        <td  class="prevNext-container"><ul>
                                <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                            </ul></td>
                    </tr>
                </table>
                <div align="center">
                    <input type="hidden" id="pageNo" value="1"/>
                    <input type="hidden" name="size" id="size" value="10"/>
                </div>
                <form id="formstyle" action="" method="post" name="formstyle">

                   <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                   <%
                     SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                     String appenddate = dateFormat1.format(new Date());
                   %>
                   <input type="hidden" name="fileName" id="fileName" value="TenderPayment_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="TenderPayment" />
                </form>
                <div>&nbsp;</div>
            </div>

            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
            
            function showHide()
            {
                if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
                    document.getElementById('tblSearchBox').style.display = 'table';
                    document.getElementById('collExp').innerHTML = '- Advanced Search';
                }else{
                    document.getElementById('tblSearchBox').style.display = 'none';
                    document.getElementById('collExp').innerHTML = '+ Advanced Search';
                }
            }
            function hide()
            {
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Advanced Search';
            }

        </script>

</html>
