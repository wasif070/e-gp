<%-- 
    Document   : RegistrationFeePaymentDetails
    Created on : Jan 22, 2011, 1:57:10 AM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%
            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userId").toString();
            }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration Payment Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
         <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />


             <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>

        <script type="text/javascript">
            /* Call Print function */
            $(document).ready(function() {
                if (document.getElementById("print")!=null){
                    $('#print').show();
                    $("#print").click(function() {
                    printElem({leaveOpen: true, printMode: 'popup'});
                });
                }

            });

                function printElem(options){
                if (document.getElementById("print_area")!=null){
                    $('#head_title').show();
                    $('#print_area').printElement(options);
                    $('#head_title').hide();
                }
            }
        </script>

    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%
                
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    CommonSearchDataMoreService objTpayment = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    if (session.getAttribute("userId") != null) {
                            tenderCommonService.setLogUserId(session.getAttribute("userId").toString());
                    }                    
                    boolean isBankUser=false;
                    boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false, isRedirect=false;
                    String userBranchId="";
                    String strPartTransId="0", userId="", payUserId="", regPaymentId="", redirectPath="";
                %>
                <%
                 HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                     userId = hs.getAttribute("userId").toString();
                 } /*else {
                     response.sendRedirectFilter(request.getContextPath() + "/SessionTimedOut.jsp");
                 }*/

                 if (hs.getAttribute("govUserId") != null) {
                     strPartTransId = hs.getAttribute("govUserId").toString();
                 }

                String referer = "";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");                    
                }

                if ( hs.getAttribute("userTypeId")!= null) {
                    if("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())){
                        isBankUser=true; // userType is ScheduleBank";
                     }
                }

                if (request.getParameter("uId") != null) {
                    payUserId = request.getParameter("uId");                    
                }

                if (request.getParameter("payId") != null) {
                    regPaymentId = request.getParameter("payId");                 
                }

                if(request.getParameter("btnOK")!=null){
                    isRedirect=true;
                    redirectPath="SearchTendererForRegistration.jsp";
                } else if(request.getParameter("btnEdit")!=null){
                    isRedirect=true;
                    redirectPath="RegistrationFeePayment.jsp?payId="+regPaymentId+"&uId="+payUserId+"&action=edit";
                }
                if(isRedirect){
                    response.sendRedirect(redirectPath);
                }

                List<SPTenderCommonData> lstCurBankUserRole = tenderCommonService.returndata("getBankUserRole",userId,null);
                if(!lstCurBankUserRole.isEmpty()){
                    if("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBranchMaker=true;
                    } else if("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBranchChecker=true;
                    } else if("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBankChecker=true;
                    }
                    userBranchId=lstCurBankUserRole.get(0).getFieldName2();
                }
               
                %>
                <div class="contentArea_1">
                    <div class="pageHead_1">Registration Payment Details
                        <span style="float:right;">
                        <%if(isBranchMaker){
                       
                       %>
                        <%if (referer.contains("RegFeePaymentDailyTrans")) {%>
                            <a href="RegFeePaymentDailyTrans.jsp" class="action-button-goback">Go Back</a>
                        <%} else {%>
                            <a href="SearchTendererForRegistration.jsp?uId=<%=payUserId%>" class="action-button-goback">Go Back</a>
                        <%}%>
                            
                        <%} else if(isBranchChecker || isBankChecker){%>
                            <a href="VerifyPayment.jsp" class="action-button-goback">Go Back</a>
                        <%}%>


                        </span>
                    </div>

                        <div class="t_space">
                            <span style="float: right;margin-bottom: 6px;">
                                <a href="javascript:void(0);" id="print" style="display: none;" class="action-button-view">Print</a>&nbsp;
                            </span>
                            <div>&nbsp;</div>
                        </div>

                    <div class="tabPanelArea_1 t_space">

                        <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("payment")){
                            msgTxt="Payment information entered successfully.";
                        } else if(msgId.equalsIgnoreCase("updated")){
                            msgTxt="Payment updated successfully.";
                        } else if(msgId.equalsIgnoreCase("verified")){
                            msgTxt="Payment verified successfully.";
                        } else if(msgId.equalsIgnoreCase("extended")){
                            msgTxt="Payment extended successfully.";
                        } else  if(msgId.equalsIgnoreCase("released")){
                            msgTxt="Payment released successfully.";
                        } else  if(msgId.equalsIgnoreCase("canceled")){
                            msgTxt="Payment canceled successfully.";
                        } else  if(msgId.equalsIgnoreCase("on-hold")){
                            msgTxt="Payment put on-hold successfully.";
                        } else  if(msgId.equalsIgnoreCase("forfeited")){
                            msgTxt="Payment forfeited successfully.";
                        } else  if(msgId.equalsIgnoreCase("forfeitrequested")){
                            msgTxt="Payment forfeit request submitted successfully.";
                        } else  if(msgId.equalsIgnoreCase("releaserequested")){
                            msgTxt="Payment release request submitted successfully.";
                        } else  if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError){%>
                        <div class="responseMsg errorMsg" ><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg" ><%=msgTxt%></div>
                   <%}%>
                <%}}%>

                         <%                            
                            boolean isPaymentVerified=false;
                            String bidderEmail="";
                            String emailId="", bankUserId="";

                                List<SPTenderCommonData> lstTendererEml = tenderCommonService.returndata("getEmailIdfromUserId",payUserId,null);
                                emailId=lstTendererEml.get(0).getFieldName1();
                                bidderEmail=emailId;
                                
                                bankUserId = userId;
                                Map<String,String> userTransMap = new HashMap<String, String>();
                                     //List<SPTenderCommonData> lstPaymentDetail = tenderCommonService.returndata("getRegistrationFeePaymentDetail", regPaymentId, null);
                                List<SPCommonSearchDataMore> lstPaymentDetail = objTpayment.geteGPData("getRegistrationFeePaymentDetailForVerify", regPaymentId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                    if (!lstPaymentDetail.isEmpty()) {
                                        if ("yes".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName8())) {
                                            isPaymentVerified = true;
                                        }
                                        if(lstPaymentDetail.get(0).getFieldName14()!=null){
                                            userTransMap.put(lstPaymentDetail.get(0).getFieldName14(), "7");
                                        }
                                        if(lstPaymentDetail.get(0).getFieldName15()!=null){
                                            userTransMap.put(lstPaymentDetail.get(0).getFieldName15(), "7");
                                        }
                                        
                        %>

                        
                        <div id="print_area">
                            <div style="display: none;" id="head_title" class="pageHead_1">Registration Payment Details</div>
                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td width="19%" class="ff">e-mail ID :</td>
                            <td><%=bidderEmail%></td>
                        </tr>
                        <tr>
                            <td class="ff">Financial Institution Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName11()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Maker :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName2()%></td>
                        </tr>
                        <%if(isPaymentVerified){%>
                        <tr>
                            <td class="ff">Branch Checker :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName13()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Verified Date :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName12()%></td>
                        </tr>
                        <%}%>
                        <tr>
                            <td class="ff">Currency :</td>
                            <td><%if(lstPaymentDetail.get(0).getFieldName3().equalsIgnoreCase("BTN")){out.print("Nu.");}else{out.print("USD");}%></td>
                        </tr>
                        <tr>
                            <td class="ff">Amount :</td>
                            <td>
                                <%if("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%} else if("USD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>$</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%} else if("Nu.".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%}%>
                            </td>
                        </tr>
                         <tr>
                            <td class="ff">Mode of Payment :</td>
                            <td>
                                <%=lstPaymentDetail.get(0).getFieldName5()%>
                            </td>
                        </tr>
                        <%if("Pay Order".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = tenderCommonService.returndata("getRegistrationFeePaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Instrument No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Financial Institution :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Financial Institution Branch :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuance Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName4()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Validity Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName5()%></td>
                                </tr>

                               <% }
                            }%>

                            <%if("Account to Account Transfer".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = tenderCommonService.returndata("getRegistrationFeePaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Beneficiary Account No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <%if(!"".equalsIgnoreCase(lstPaymentDetailMore.get(0).getFieldName3())){%>
                                <tr>
                                    <td class="ff">Branch Name :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <%}%>
                               <% }
                            }%>

                            <tr>
                                <td class="ff">Date and Time of Payment :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName6()%></td>
                            </tr>
                                
                            <tr>
                                <td class="ff">Remarks :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName7()%></td>
                            </tr>
                    </table>
</div>
                            <%if(isBranchMaker && !isPaymentVerified){%>
                            <form id="frmPaymentDetail" method="post" action="">
                                <table class="t_space" border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td width="18%" class="ff">&nbsp;</td>
                                <td>
                                    <label class="formBtn_1">
                                        <input name="btnOK" id="btnOK" type="submit" value="OK" />
                                    </label>

                                    &nbsp;
                                    <label class="formBtn_1">
                                        <input name="btnEdit" id="btnEdit" type="submit" value="Edit Payment Details" />
                                    </label>
                                </td>
                            </tr>
                            </table>
                            </form>
                            <%}%>

                       

                    <% } else {%>
                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td>Payment information not found!</td>
                        </tr>
                    </table>
                    <%}
                             request.setAttribute("listEmpTras", userTransMap);
                    %>

                       <!--jsp:include page="../resources/common/EmpTransferHist.jsp" /-->  
                </div>

                 <!--Dashboard Content Part End-->
                 
                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->


        </div>
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
