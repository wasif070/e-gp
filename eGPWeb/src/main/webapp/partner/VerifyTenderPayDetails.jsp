<%-- 
    Document   : VerifyTenderPayDetails
    Created on : Mar 4, 2011, 2:16:29 PM
    Author     : Karan
--%>

<%@page import="java.net.URLDecoder"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Verify Payment Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
         <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

       <style type="text/css">
           .docUploadMandatoryMessage {
               text-align: center !important;
               color: #FF0000;
            }
	</style>

        <script type="text/javascript" >
        function ConfirmVerify(){
            var val=confirm("Please confirm the details as once payment is verified it cannot be changed.");
               if(val==true){
                    return true;
                }else {
                    return false;
                }
        }
       // Dohatec Work Start Here - Show/Hide Verify Button Depends on No of Documents Uploaded - Wahid Abdullah
        function ShowHideVerifyButton() {
              if($.trim($('#manUpload').val())!= '' && $.trim($('#manUpload').val())!= null) {
                    var DocCount = parseInt($.trim($('#manUpload').val()),10);
                    if(DocCount <= 0){
                      HideVerifyButton();
                    }
                    else {
                        ShowVerifyButton();
                    }
                }
                else {
                   HideVerifyButton();
                }
        }

        function ShowVerifyButton() {
            $('.docsUploadedSuccessfully').show();
            $('.docsUploadMendatory').hide();
        }

        function HideVerifyButton() {
            $('.docsUploadedSuccessfully').hide();
            $('.docsUploadMendatory').show();
        }
        // Dohatec Work End Here - Show/Hide Verify Button Depends on No of Documents Uploaded - Wahid Abdullah
        </script>
        <script type="text/javascript" >
            $(document).ready(function(){

                $('#btnVerify').click(function(){
                    var flag=true;

                     if($.trim($('#txtComments').val())==''){
                            $('#SpError').html('Please enter Remarks');
                            flag=false;
                        }
                        else {
                            if($('#txtComments').length<=1000) {
                                $('#SpError').html('');
                            }
                            else{
                                $('#SpError').html('Maximum 1000 characters are allowed');
                                flag=false;
                            }
                        }

                         if(flag==false){
                            return false;
                        } else {
                            flag = ConfirmVerify();
                            if(flag==false){
                                return false;
                            } else {
                                document.getElementById("btnVerify").style.display = 'none';
                            }                            
                        }

                       

                });              

                $('#txtComments').blur(function(){
                    var flag=true;

                     if($.trim($('#txtComments').val())==''){
                            $('#SpError').html('Please enter Remarks');
                            flag=false;
                        }
                        else {
                            if($('#txtComments').length<=1000) {
                                $('#SpError').html('');
                            }
                            else{
                                $('#SpError').html('Maximum 1000 characters are allowed');
                                flag=false;
                            }
                        }

                         if(flag==false){
                            return false;
                        }
                });

            });
        </script>

    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%
                    //TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                TenderCommonService objTSC = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                CommonSearchDataMoreService objTpayment = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    boolean isBankUser=false;
                    String strPartTransId="0", userId="", payUserId="", regPaymentId="";
                    String tenderId="0", pkgLotId="0", payTyp="", paymentTxt="";
                %>
                <%

                 HttpSession hs = request.getSession();
             if (hs.getAttribute("userId") != null) {
                     userId = hs.getAttribute("userId").toString();
                     objTSC.setLogUserId(userId);
              }
             else {
                     //response.sendRedirectFilter(request.getContextPath() + "/SessionTimedOut.jsp");
                     response.sendRedirect("SessionTimedOut.jsp");
             }

                  if (hs.getAttribute("govUserId") != null) {
                     strPartTransId = hs.getAttribute("govUserId").toString();
              }

                String referer = "";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");                    
                }

                if ( hs.getAttribute("userTypeId")!= null) {
                    if("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())){
                        isBankUser=true; // userType is ScheduleBank";
                     }
                }

                if (request.getParameter("uId") != null) {
                    payUserId = request.getParameter("uId");                    
                }

                if (request.getParameter("payId") != null) {
                    regPaymentId = request.getParameter("payId");                
                }
                
                if (request.getParameter("tenderId") !=null){
                        tenderId=request.getParameter("tenderId");
                    }
                     if (request.getParameter("lotId") != null){
                        if (request.getParameter("lotId")!="" && !request.getParameter("lotId").equalsIgnoreCase("null")){
                           pkgLotId=request.getParameter("lotId");
                        }
                    }

                     if (request.getParameter("payTyp") != null){
                        if (request.getParameter("payTyp")!="" && !request.getParameter("payTyp").equalsIgnoreCase("null")){
                           payTyp=request.getParameter("payTyp");

                            if ("df".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Document Fees";
                             } else if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Tender/Proposal Security";
                             } else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Performance Security";
                             } else if ("bg".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "New Performance Security";
                             }
                           
                        }
                    }

                // Dohatec Start
                
                // Tender is ICT or not
                List<SPTenderCommonData> listICT = objTSC.returndata("chkDomesticPreference", tenderId, null);
                boolean isIctTender = false;
                if(!listICT.isEmpty() && listICT.get(0).getFieldName1().equalsIgnoreCase("true")){
                    isIctTender = true;
                }
                // Dohatec End

                %>
                <div class="contentArea_1">
                    <div class="pageHead_1">Verify Payment Details
                        <span style="float:right;"><a href="TenPaymentListing.jsp" class="action-button-goback">Go Back</a></span>
                    </div>

                     <% pageContext.setAttribute("tenderId", tenderId); %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>

                    <div class="tabPanelArea_1 t_space">

                        <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("payment")){
                            msgTxt="Payment information entered successfully.";
                        } else if(msgId.equalsIgnoreCase("updated")){
                            msgTxt="Payment updated successfully.";
                        } else if(msgId.equalsIgnoreCase("extended")){
                            msgTxt="Payment extended successfully.";
                        } else  if(msgId.equalsIgnoreCase("released")){
                            msgTxt="Payment released successfully.";
                        } else  if(msgId.equalsIgnoreCase("canceled")){
                            msgTxt="Payment canceled successfully.";
                        } else  if(msgId.equalsIgnoreCase("on-hold")){
                            msgTxt="Payment put on-hold successfully.";
                        } else  if(msgId.equalsIgnoreCase("forfeited")){
                            msgTxt="Payment forfeited successfully.";
                        } else  if(msgId.equalsIgnoreCase("forfeitrequested")){
                            msgTxt="Payment forfeit request submitted successfully.";
                        } else  if(msgId.equalsIgnoreCase("releaserequested")){
                            msgTxt="Payment release request submitted successfully.";
                        } else  if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError){%>
                        <div class="responseMsg errorMsg" ><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg" ><%=msgTxt%></div>
                   <%}%>
                <%}}%>

                <%
                            String bidderUserId = "0", bidderEmail = "";
                            String emailId = "";

                            List<SPTenderCommonData> lstTendererEml = objTSC.returndata("getEmailIdfromUserId", payUserId, null);
                            emailId = lstTendererEml.get(0).getFieldName1();
                            bidderEmail = emailId;
                            bidderUserId = payUserId;
                %>

                <%
                            //List<SPTenderCommonData> lstPaymentDetail = objTSC.returndata("getTenderPaymentDetail", regPaymentId, null);
                            List<SPCommonSearchDataMore> lstPaymentDetail = objTpayment.geteGPData("getTenderPaymentDetail", regPaymentId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            
                            if (!lstPaymentDetail.isEmpty()) {
                %>

<!--                <form id="frmVerifyPayment" action="VerifyTenderPayDetails.jsp?payId=<%=regPaymentId%>&uId=<%=request.getParameter("uId")%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>" method="POST">-->

                 <!-- START: COMMON PACKAGE/LOT DESCRIPTION FILE -->
                    <div >
                        <%@include file="CommonPackageLotDescription.jsp" %>
                    </div>
                    <!-- END COMMON PACKAGE/LOT DESCRIPTION FILE -->

                     <!-- START: COMMON PAYMENT UPLODED DOCS LISTS By Wahid -->
                        <div class="noprint">
                            <% if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) { %>
                               <%@include file="CommonPaymentUploadedDocsList.jsp"%>                               
                            <% } %>
                        </div>
                     <!-- END: COMMON PAYMENT UPLODED DOCS LISTS By Wahid -->

                <form id="frmVerifyPayment" action="<%=request.getContextPath()%>/VerifyTenderPaymentServlet?action=verfiyPayment&payId=<%=regPaymentId%>&uId=<%=request.getParameter("uId")%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>" method="POST">
                        <input type="hidden" name="tenderRefNo" id="tenderRefNo" value="<%=toextTenderRefNo%>" />

                        <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                            <tr>
                                <td style="font-style: italic" class="ff t-align-right" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <tr>
                                <td width="18%" class="ff">Payment Status :</td>
                                <td>
                                    Verification is Pending
                                    <%//=lstPaymentDetail.get(0).getFieldName10()%>
                                </td>
                            </tr>

                            <tr>
                                <td width="18%" class="ff">Email ID :</td>
                                <td><%=bidderEmail%></td>
                            </tr>
                        <tr>
                            <td class="ff">Financial Institution Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName11()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Maker :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName2()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Payment For : </td>
                            <td><%=paymentTxt%></td>
                        </tr>

                        
                        <!-- Dohatec Start -->
                        <%
                            if(("ps".equalsIgnoreCase(payTyp) || "bg".equalsIgnoreCase(payTyp)) && isIctTender){
                                String txtCurrencySymbol = "";
                                String currencyType = "";
                                String perSecAmount = "";

                                CommonSearchDataMoreService objTSDetails = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");

                                List<SPCommonSearchDataMore> lstPerSecPayDetail = null;
                                if("ps".equalsIgnoreCase(payTyp))
                                    lstPerSecPayDetail = objTSDetails.geteGPData("getPerformanceSecurityPaymentDetail", tenderId.toString());
                                else if("bg".equalsIgnoreCase(payTyp))
                                    lstPerSecPayDetail = objTSDetails.geteGPData("getNewPerSecDetail", tenderId.toString(), pkgLotId.toString(), payUserId.toString());

                                if(!lstPerSecPayDetail.isEmpty())
                                {
                                    System.out.print("\n List Size : " + lstPerSecPayDetail.size());
                                    for(int i = 0; i<lstPerSecPayDetail.size(); i++)
                                    {
                                        if("ps".equalsIgnoreCase(payTyp))
                                        {
                                            txtCurrencySymbol = lstPerSecPayDetail.get(i).getFieldName13();
                                            currencyType = lstPerSecPayDetail.get(i).getFieldName12();
                                            perSecAmount = lstPerSecPayDetail.get(i).getFieldName4();
                                        }
                                        else if("bg".equalsIgnoreCase(payTyp))
                                        {
                                            perSecAmount = lstPerSecPayDetail.get(i).getFieldName1();
                                            currencyType = lstPerSecPayDetail.get(i).getFieldName2();
                                            txtCurrencySymbol = lstPerSecPayDetail.get(i).getFieldName3();
                                        }
                        %>

                        <tr>
                            <td class="ff">Amount (in <% if(currencyType.equalsIgnoreCase("USD")){out.print("USD");} else {out.print("BTN");} %>) :</td>
                            <td><label id="lblCurrencySymbol_<%=i%>"><% if(txtCurrencySymbol.equalsIgnoreCase("TK.")){out.print("Nu.");} else if(txtCurrencySymbol.equalsIgnoreCase("Nu.")){out.print("Nu.");} else {out.print("$");} %></label>&nbsp;
                                <label id="lblAmount_<%=i%>"><%=perSecAmount%></label>
                            </td>
                        </tr>

                        <%
                                    }
                                }
                        }else{%>

                        <tr>
                            <td class="ff">Currency :</td>
                            <td><% if(lstPaymentDetail.get(0).getFieldName3().equalsIgnoreCase("USD")){out.print("USD");} else {out.print("BTN");}%></td>
                            <input type="hidden" id="hdnCurrency" name="hdnCurrency" value="<%=lstPaymentDetail.get(0).getFieldName3()%>">
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Amount :</td>
                            <td>
                                <%-- Changed By Emtaz on 13-April-2016. Taka -> Nu. --%>
                                <%if("Nu."/*"BTN"*/.equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                <label>Nu.<%-- Taka --%></label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%} else if("USD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>$</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%} else if("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%}%>
                                <input type="hidden" id="hdnAmount" name="hdnAmount" value="<%=lstPaymentDetail.get(0).getFieldName4()%>">
                                <input type="hidden" id="hdnValidityRefId" name="hdnValidityRefId" value="<%=lstPaymentDetail.get(0).getFieldName9()%>">
                                <input type="hidden" id="hdnPayStatus" name="hdnPayStatus" value="<%=lstPaymentDetail.get(0).getFieldName10()%>">
                                <input type="hidden" id="hdnBidderUserId" name="hdnBidderUserId" value="<%=bidderUserId%>">

                            </td>
                        </tr>
                        <%}%>
                         <tr>
                            <td class="ff">Mode of Payment :</td>
                            <td><% if(lstPaymentDetail.get(0).getFieldName5().equals("Pay Order")){out.print("Cheque/Cash Warrant");}else if(lstPaymentDetail.get(0).getFieldName5().equals("Bank Guarantee")){out.print("Financial Institution Guarantee");}else{out.print(lstPaymentDetail.get(0).getFieldName5());}%>
                            </td>
                        </tr>
                        <%if("Pay Order".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5()) || "DD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5()) || "Bank Guarantee".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = objTSC.returndata("getTenderPaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Instrument No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Financial Institution :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Financial Institution Branch :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuance Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName4()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Validity Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName5()%></td>
                                </tr>

                               <% }
                            }%>

                            <%if("Account to Account Transfer".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = objTSC.returndata("getTenderPaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Beneficiary Account No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <%if(!"".equalsIgnoreCase(lstPaymentDetailMore.get(0).getFieldName3())){%>
                                <tr>
                                    <td class="ff">Branch Name :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <%}%>
                               <% }
                            }%>

                            <tr>
                                <td class="ff">Date and Time of Payment :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName6()%></td>
                            </tr>

                             <tr>
                                <td class="ff">Branch Maker Remarks : </td>
                                <td>
                                    <%=URLDecoder.decode(lstPaymentDetail.get(0).getFieldName7(),"UTF-8")%>
                                </td>
                            </tr>
                           
                            <tr class="docsUploadedSuccessfully">
                                <td class="ff">Remarks : <span class="mandatory">*</span></td>
                                <td>
                                    <textarea cols="100" rows="5" id="txtComments" name="txtComments" class="formTxtBox_1"></textarea>
                                    <span id="SpError" class="reqF_1"></span>
                                    <%//=lstPaymentDetail.get(0).getFieldName7()%>
                                </td>
                            </tr>
                            <tr class="docsUploadedSuccessfully">
                                <td>&nbsp;</td>
                                <td>
                                    <div class="t-align-left">
                                        <label class="formBtn_1">
                                            <input name="btnVerify" id="btnVerify" type="submit" value="Verify" />
                                        </label>
                                    </div>

                                </td>
                            </tr>
                            <!--<tr class="docsUploadMendatory">
                                <td colspan="2" class="docUploadMandatoryMessage">
                                    <label>Branch Maker do not upload any documents. So you have to upload reference document relevant to this performance security payment. </label>
                                </td>
                            </tr> -->

                    </table>
                    <% 
                    // Dohatec Work Start Here - If Payment Type is Performance Security and No of Document Uploaded is 0 then it Hide the Verify Button - Wahid Abdullah
                    if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) { %>
                               <script type="text/javascript">
                                      ShowHideVerifyButton();
                               </script>
                    <% } 
                    // Dohatec Work End Here - If Payment Type is Performance Security and No of Document Uploaded is 0 then it Hide the Verify Button - Wahid Abdullah
                    %>

                    </form>
                    <% } else {%>
                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td>Payment information not found!</td>
                        </tr>
                    </table>
                    <%}%>
                </div>

                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->


        </div>
        </div>
    </body>


    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>


