<%-- 
    Document   : RegistrationFeePayment
    Created on : Jan 22, 2011, 12:27:48 AM
    Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Locale"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.text.DateFormat" %>
<%@page import="java.util.Date" %>
<%@page import="java.lang.String" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
           Registration Payment Details
        </title>
		<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
		<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
		<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
		<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
		<link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
		<link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
		<link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
		<link href="../js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
		<link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
		<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
		<link href="../css/home.css" rel="stylesheet" type="text/css" />
		<link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
		<link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
		<script type="text/javascript" src="../js/datepicker/js/jscal2.js"></script>
		<script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>
		<script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
		<script type="text/javascript" src="../js/jQuery/jquery.alerts.js"></script>
		<script type="text/javascript" src="../js/jQuery/jquery-1.4.1.js"></script>

		<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
		<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
		<script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
		<script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
		<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
   
	   <script type="text/javascript">
            function submitOkButton(button)
            {
            	  
					var complaintFee=document.getElementById("feetype").value;
      				//alert("complaintFee :"+complaintFee);
					if(document.getElementById("verify").value!=null){
      					var verifyFlag=document.getElementById("verify").value;
      				//	alert("if verifyFlag :"+verifyFlag);
      					document.paymentfee.action="searchFirstForComplaintPayments.htm?feetype="+complaintFee+"&verify="+verifyFlag;
      				}
      				else{
      				//alert("else verify ");
      					document.paymentfee.action="searchFirstForComplaintPayments.htm?feetype="+complaintFee;
      				}
      					document.paymentfee.submit();		
      					return true;
                  
            }
			  
        </script>

    
 
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }


            function GetCal2(txtname,controlname,optionalCtrl)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());

                        if(optionalCtrl!=null){
                            if(optionalCtrl!='' && optionalCtrl !='null')   {
                                //alert(date.getMonth());

                                //var value=document.getElementById(txtname).value;
                                //alert(value);
                                //var mdy = value.split('/')  //Date and month split
                                //var mdyhr= mdy[2].split(' ');  //Year and time split
                                //var valuedate= new Date(mdyhr[0], mdy[1]+7, mdy[0]);

                                var currdate = document.getElementById(txtname).value;
                                var temp=currdate.split('/');
                                //alert(temp[0]);
                                //alert(temp[1]);
                                //alert(temp[2]);
                                var newtemp=temp[1]+'/'+temp[0]+'/'+temp[2];
                                //alert(new Date(newtemp));
                                var today =new Date(newtemp);
                                //alert(today);
                                //alert(today.getYear());
                                //alert(today.getMonth());
                                today.setMonth(today.getMonth()+7)
                                //// Note: getMonth() funcation returns 1 month less than the actual value
                                // So to add 6 months we have to use +7
                                //alert(today);

                                var dtVal = today.getDate();
                                if(dtVal<10){
                                    dtVal="0" + dtVal.toString();
                                }
                               
                                var mnthVal = today.getMonth();
                                if(mnthVal<10){
                                    mnthVal="0" + mnthVal.toString();
                                }

                                var yrVal = today.getYear();
                                if(yrVal < 1900){
                                    yrVal= yrVal+ 1900;
                                }
                                valuedate=dtVal+"/"+mnthVal+"/"+yrVal;
                               
                                //valuedate =new Date(valuedate);
                              
                                document.getElementById(optionalCtrl).value=valuedate;
                            }
                        }



                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

        </script>

     
    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <%
                   
        			TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    SimpleDateFormat formatVisible = new SimpleDateFormat("dd-MM-yyyy");
					SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
					DateFormat sdfSource1 = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL, Locale.getDefault());
					SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MM-yyyy hh:mm");
                    SimpleDateFormat formatVisibleWithMonthNm = new SimpleDateFormat("dd-MMM-yyyy");
                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                    boolean isBankUser = false, isRenew = false, isEditCase = false;
                    String emailId = "", payUserId = "", payDt = format.format(new Date()), payDtWithMnthNm = formatVisibleWithMonthNm.format(new Date());;
                    String strPartTransId="0", userId = "", action = "", editPaymentId = "0";

                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userId = hs.getAttribute("userId").toString();
                    } else {
                        response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
                    }
                    
                    if (request.getParameter("uId") != null) {
                        if (request.getParameter("uId") != "" && !request.getParameter("uId").equalsIgnoreCase("null")) {
                            payUserId = request.getParameter("uId");
                        }
                    }

                    if (hs.getAttribute("govUserId") != null) {
                     strPartTransId = hs.getAttribute("govUserId").toString();
					}

                    if (hs.getAttribute("userTypeId") != null) {
                        if ("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())) {
                            isBankUser = true; // userType is ScheduleBank";
                        }
                    }
                    System.out.println("isBankUser : " + isBankUser);
                    String bidderEmail = "", registeredDt = "";

                    List<SPTenderCommonData> lstTendererEml = tenderCommonService.returndata("getEmailIdfromUserId", payUserId, null);

                    if (!lstTendererEml.isEmpty()) {
                        emailId = lstTendererEml.get(0).getFieldName1();
                        registeredDt = lstTendererEml.get(0).getFieldName2();
                        bidderEmail = emailId;
                    }


        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">
                    <fmt:message key="LBL_REGISTRATION_FEE_PAYMENT"/>
                    <span style="float: right;">
                    <!--<a href="SearchTendererForRegistration.jsp?uId=<%=payUserId%>" class="action-button-goback">Go Back</a>-->
					<% 
						String feetype=(String)session.getAttribute("feetype");
						if(session.getAttribute("verify")!=null){%>

					<a href="searchFirstForComplaintPayments.htm?verify=verify&feetype=<%=feetype%>" class="action-button-goback"><fmt:message key="LBL_GO_BACK"/></a></span>
					<% } else {%>
					<a href="searchFirstForComplaintPayments.htm?feetype=<%=feetype%>" class="action-button-goback"><fmt:message key="LBL_GO_BACK"/></a></span>
					<%}%>
                </div>
                <div class="tabPanelArea_1 t_space">
                    <form id="paymentfee" name="paymentfee" method="POST">
					<table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                            <tr>
                                <td class="ff"> <fmt:message key="MSG_OK_AND_EDIT_PAYMENT_DETAILS"/></td>

                            </tr>
						</table>
                        <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                            <tr>
                                <td class="ff"><fmt:message key="LBL_EMAIL_ID"/>:</td>
                                <td><%=request.getParameter("emailId")%>
								<input type="hidden" name="emailId" id="emailId" value='<%=request.getParameter("emailId")%>'/>
								</td>
                            </tr>
                            <tr id="trBankNm">
                                <td class="ff"><fmt:message key="LBL_BANK_NAME"/> : 
                                <td>${paydetails.bankName}</td>

                            </tr>
                            <tr id="trBranch">
                                <td class="ff"><fmt:message key="LBL_BRANCH_NAME"/> : 
                                <td>${paydetails.branchName} </td>
                            </tr>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_BRANCH_MAKER"/> :</td>
                                <td>${partnerAdmin.fullName}</td>
                            </tr>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_PAYMENT_FOR"/> : </td>
                                <td>Complaint ${paydetails.paymentFor}</td>
                            </tr>
                           
                            <tr>
                                <td class="ff" width="17%"><fmt:message key="LBL_CURRENCY"/> : <span class="mandatory"></span></td>
                                <td width="83%"><c:choose>
                                                <c:when test="${paydetails.currency=='Nu.'}">
                                                        Nu.
                                                </c:when>
                                                <c:when test="${paydetails.currency=='BTN'}">
                                                        Nu.
                                                </c:when>
                                                <c:otherwise>
                                                        USD
                                                </c:otherwise>
                                            </c:choose>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_AMOUNT"/> :</td>
                                <td>
								<label id="lblCurrencySymbol">
								<c:choose>
								<c:when test="${paydetails.currency=='Nu.'}">
									<fmt:message key="LBL_NU"/> 
								</c:when>
                                                                <c:when test="${paydetails.currency=='BTN'}">
									<fmt:message key="LBL_NU"/> 
								</c:when>
								<c:otherwise>
									<fmt:message key="LBL_DOLLAR"/> 
								</c:otherwise>
								</c:choose>
								
								</label>
								<c:choose>
								<c:when test="${paydetails.paymentFor == 'Registration Fee'}">
									<%=(XMLReader.getMessage("localComplaintRegistrationFee"))%>
								</c:when>
								<c:otherwise>
									<%=(XMLReader.getMessage("localComplaintSecurityFee"))%>
								</c:otherwise>
								</c:choose>
							</td>
                            </tr>
                            <tr>
                                <td class="ff" width="17%"><fmt:message key="LBL_MODE_PAYMENT"/>  : </td>
                                <td width="83%">${paydetails.paymentMode}
                                  
                                </td>
                            </tr>
							<c:if test="${paydetails.paymentMode == 'Pay Order'}">
                            <tr id="trInsRefNo">
                                <td class="ff">
                             <fmt:message key="LBL_INSTRUMENT_NUMBER"/>:
                                   </td>
                                <td>${paydetails.instRefNumber}
                                </td>
                            </tr>

                            <tr id="trIssuanceBankNm">
                                <td class="ff"> <fmt:message key="LBL_ISSUING_BANK"/> : </td>
                                <td>${paydetails.issuanceBank}
                                    </span>
                                </td>

                            </tr>
                            <tr id="trIssuanceBranch">
                                <td class="ff">
                                 <fmt:message key="LBL_ISSUANCE_BRANCH"/>  :
                                    </td>
                                <td>${paydetails.issuanceBranch}
                                </td>
                            </tr>

                            <tr id="trIssuanceDt">
                                <td class="ff"><fmt:message key="LBL_ISSUANCE_DATE"/>:</td>
                                <td>
                                    </span>
                                </td>
                            </tr>
							</c:if>
                           <tr id="trDtOfPayment">
                                <td class="ff"><fmt:message key="LBL_DATE_OF_PAYMENT"/> : </td> 
                                <td>
								<c:set var="createDate" scope="request" value="${paydetails.dtOfAction}"/> 
								${paydetails.dtOfAction}
                                </td>
                            </tr>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_REMARKS"/>  : </td>
                                <td> ${paydetails.comments}
                                </td>
                            </tr>
                        </table>
						<table class="t_space" border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td width="18%" class="ff">&nbsp;</td>
                                <td>
                                    <label class="formBtn_1">
									<input type="hidden" name="feetype" id="feetype" value='<%=session.getAttribute("feetype")%>'/>
										<input type="hidden" name="verify" id="verify" value='<%=session.getAttribute("verify")%>'/>
									    <input name="btnOK" id="btnOK" type="button" value="OK" onclick="javascript:submitOkButton(id)"/><!-- on click of ok button page navigates to SearchTendererReg.htm-->
                                    </label>
									
                                </td>
                            </tr>
                            </table>
                    </form>
                </div>

            </div>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
