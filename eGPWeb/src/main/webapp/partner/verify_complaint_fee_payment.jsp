<%-- 
    Document   : VerifyPayDetails
    Created on : Feb 4, 2011, 10:18:33 AM
    Author     : Karan
--%>

<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Verify Complaint <%=session.getAttribute("feetype")%> Payment</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../js/jQuery/jquery.alerts.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

      

    </head>

    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
          
            <div class="contentArea_1">
                <div class="pageHead_1">

                    <label id="lblPageHead">Verify Complaint <%=session.getAttribute("feetype")%> Payment  </label>
                   <%String feetype=(String)session.getAttribute("feetype");%>
                    <span style="float:right;"><a href="searchFirstForComplaintPayments.htm?verify=verify&feetype=<%=feetype%>" class="action-button-goback"><fmt:message key="LBL_GO_BACK"/></a>
					</span>
                </div>

                <div class="tabPanelArea_1 t_space">

                  <form id="frmVerifyPayment" name="frmVerifyPayment"  action="complaintFeePaymentSave.htm?complaintPaymentId=${payment.complaintPaymentId}" method="POST">


                        <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                            <tr>
                                <td style="font-style: italic" class="ff t-align-right" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <tr>
                                <td width="18%" class="ff">Email Id :</td>
                                <td><%=request.getAttribute("emailId")%>
								<input type="hidden" name="emailId" id="emailId" value='<%=request.getAttribute("emailId")%>'/></td>
                            </tr>
							 <tr>
                                <td class="ff"><fmt:message key="LBL_BANK_NAME"/> :</td>
                                <td>${payment.bankName}</td>
                            </tr>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_BRANCH_NAME"/> :</td>
                                <td>${payment.branchName}</td>
                            </tr>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_BRANCH_MAKER"/>:</td>
                                <td>${partnerAdmin.fullName}</td>
                            </tr>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_CURRENCY"/> :</td>
                                <td><c:choose>
                                        <c:when test="${paydetails.currency=='Nu.'}">
                                                Nu.
                                        </c:when>
                                        <c:when test="${paydetails.currency=='BTN'}">
                                                Nu.
                                        </c:when>
                                        <c:otherwise>
                                                USD
                                        </c:otherwise>
                                    </c:choose>
                                    <input type="hidden" id="hdnCurrency" name="hdnCurrency" value="${payment.currency}">
                                </td>
                            </tr>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_AMOUNT"/> :</td>
                                <td>
								<label id="lblCurrencySymbol">
								<c:choose>
								<c:when test="${paydetails.currency=='Nu.'}">
									<fmt:message key="LBL_NU"/> 
								</c:when>
                                                                <c:when test="${paydetails.currency=='BTN'}">
									<fmt:message key="LBL_NU"/> 
								</c:when>
								<c:otherwise>
									<fmt:message key="LBL_DOLLAR"/> 
								</c:otherwise>
								</c:choose>
								
								</label>
								<c:choose>
								<c:when test="${payment.paymentFor == 'Registration Fee'}">
									<%=(XMLReader.getMessage("localComplaintRegistrationFee"))%>
								</c:when>
								<c:otherwise>
									<%=(XMLReader.getMessage("localComplaintSecurityFee"))%>
								</c:otherwise>
								</c:choose>
							</td>
                            </tr>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_MODE_PAYMENT"/> :</td>
                                <td>${payment.paymentMode}</td>
                            </tr>
                          	<c:if test="${payment.paymentMode == 'Pay Order'}">	
		                            <tr>
		                                <td class="ff"><fmt:message key="LBL_INSTRUMENT_NUMBER"/> :</td>
		                                <td></td>
		                            </tr>
		                            <tr>
		                                <td class="ff"><fmt:message key="LBL_ISSUING_BANK"/> :</td>
		                                <td></td>
		                            </tr>
		                            <tr>
		                                <td class="ff"><fmt:message key="LBL_ISSUANCE_BRANCH"/> :</td>
		                                <td></td>
		                            </tr>
		                            <tr>
		                                <td class="ff"><fmt:message key="LBL_ISSUANCE_DATE"/>:</td>
		                                <td></td>
		                            </tr>
		                            <tr>
		                                <td class="ff"><fmt:message key="LBL_VALIDITY_DATE"/>:</td>
		                                <td></td>
		                            </tr>
							</c:if>
							                  
                             <tr>
                                <td class="ff"><fmt:message key="LBL_DATE_OF_PAYMENT"/>:</td>
                                <td>${payment.dtOfAction}</td>
                            </tr>

                            <tr>
                                <td class="ff"><fmt:message key="LBL_BRANCH_MAKER_REMARKS"/>: </td>
                                <td>
                                    ${payment.comments}
                                </td>
                            </tr>

                            <tr>
                                <td class="ff"><fmt:message key="LBL_REMARKS"/> : <span class="mandatory">*</span></td>
                                <td>
                                    <textarea cols="100" rows="5" id="txtComments" name="txtComments" class="formTxtBox_1"></textarea>
                                    <span id="SpError" class="reqF_1"></span>
                                    <%//=lstPaymentDetail.get(0).getFieldName7()%>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <div class="t-align-left">
                                        <label class="formBtn_1"><input name="btnVerify" id="btnVerify" type="submit" value="Verify" /></label>
                                    </div>

                                </td>
                            </tr>

                        </table>

                    </form>
                   
                </div>

                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->
            </div>
    </body>
</html>
