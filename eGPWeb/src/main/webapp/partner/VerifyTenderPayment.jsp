<%-- 
    Document   : VerifyTenderPayment
    Created on : Mar 3, 2011, 5:55:26 PM
    Author     : Karan
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Search and Verify Payment Details</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />


        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />

        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>

        <script type="text/javascript">
            function fillGridOnEvent(userId, emailId, isVerified, branchId, branchMakerId, fromDt, toDt, tenderId, tendererNm, paymentFor){
                /*
                alert(userId);
                alert(emailId);
                alert(isVerified);
                alert(branchId);
                alert(branchMakerId);
                alert(fromDt);
                alert(toDt);
                */

                $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                jQuery("#list").jqGrid({
                    url:'<%=request.getContextPath()%>/VerifyTenderPaymentServlet?q=1&action=fetchData&userId=' + userId + '&emailId=' + emailId + '&isVerified=' + isVerified + '&branchId=' + branchId + '&branchMakerId=' + branchMakerId + '&fromDt=' + fromDt + '&toDt=' + toDt + '&tenderId=' + tenderId + '&tendererNm=' + tendererNm + '&paymentFor=' + paymentFor,
                    datatype: "xml",
                    height: 250,
                    colNames:['Sl. No.','Tender ID','Email ID','Bidder / Consultant','Payment For',"Amount","Payment Date and Time",'Branch Maker',"Action"],
                    colModel:[
                        {name:'RowNumber',index:'RowNumber', width:40,sortable:false, align: 'center'},
                        {name:'tenderId',index:'tenderId', width:100,sortable:false, align: 'center'},
                        {name:'emailId',index:'emailId', width:250,sortable:false},
                        {name:'tendererName',index:'tendererName', width:250,sortable:false},
                        {name:'paymentFor',index:'paymentFor', width:150,sortable:false, align: 'center'},
                        {name:'amount',index:'amount', width:100,sortable:false, align: 'center'},
                        {name:'paymentDate',index:'paymentDate', width:140,sortable:false, align: 'center'},
                        {name:'branchMaker',index:'branchMaker', width:200,sortable:false},
                        {name:'action',index:'action', width:100,sortable:false, align: 'center'}

                    ],
                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    sortable:false,
                    caption: "",
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{edit:false,add:false,del:false});
            }
            jQuery().ready(function (){
                //fillGrid();
            });
        </script>

                    <script type="text/javascript">
                        $(function() {
                            $('#comboBranches').change(function() {
                                if( $('#comboBranches').val()!=''){
                                    branchId = $('#comboBranches').val();
                                    $.post("<%=request.getContextPath()%>/VerifyPaymentServlet", {branchId: branchId, action: 'getBranchMembers'},  function(j){
                                        //alert(j);
                                        $("select#comboBranchMembers").html(j);
                                    });
                                }
                            });

                            $('#btnSearch').click(function() {
                                //alert('You clicked Search');
                                var userId='', emailId='', isVerified='', branchId='', branchMakerId='', fromDt='', toDt='', tenderId='', tendererNm='', paymentFor='';
                                userId=$('#userId').val();
                                emailId=$('#txtEmailId').val();
                                isVerified=$('#comboStatus').val();


                                fromDt=$('#txtFromDt').val();
                                toDt=$('#txtToDt').val();
                                
                                tenderId=$('#txtTenderId').val();
                                tendererNm=$('#txtTendererNm').val();
                                paymentFor=$('#comboPaymentFor').val();


                            if(document.getElementById("comboBranches")!=null){
                                if($('#comboBranches').val()!=''){
                                    branchId=$('#comboBranches').val();
                                }
                            }

                             if($('#comboBranchMembers').val()!=''){
                                    branchMakerId=$('#comboBranchMembers').val();
                             }

                             fillGridOnEvent(userId, emailId, isVerified, branchId, branchMakerId, fromDt, toDt, tenderId, tendererNm, paymentFor);

                            });

                            $('#btnReset').click(function() {
                                //alert('Clearing...')
                               var userId=$('#userId').val();

                              $('#txtEmailId').val('');
                                $('#comboStatus').val('no');
                                $('#txtFromDt').val('');
                                $('#txtToDt').val('');

                                $('#txtTenderId').val('');
                                $('#txtTendererNm').val('');
                                $('#comboPaymentFor').val('All');

                                if(document.getElementById("comboBranches")!=null){
                                    $('#comboBranches').val('');
                                }

                                var j="<option value=''>- Select Branch Maker -</option>";
                                $("select#comboBranchMembers").html(j);

                                fillGridOnEvent(userId,'','no','','','','','','','All');
                            });


                        });
                    </script>

</head>


    <%
    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    if (session.getAttribute("userId") != null) {
            tenderCommonService.setLogUserId(session.getAttribute("userId").toString());
    }
     boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false;
     String userBranchId="";
    String userId = "";
                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") != null) {
                    userId = hs.getAttribute("userId").toString();
                } else {
                    //response.sendRedirectFilter(request.getContextPath() + "/SessionTimedOut.jsp");
                    response.sendRedirect("SessionTimedOut.jsp");
                }

                List<SPTenderCommonData> lstCurBankUserRole = tenderCommonService.returndata("getBankUserRole",userId,null);
                if(!lstCurBankUserRole.isEmpty()){
                    if("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBranchMaker=true;
                    } else if("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBranchChecker=true;
                    } else if("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBankChecker=true;
                    }
                    userBranchId=lstCurBankUserRole.get(0).getFieldName2();
                }
    %>
    <body onload="fillGridOnEvent('<%=userId%>','','no','','','','','','','All');">
    <div class="dashboard_div">
      <!--Dashboard Header Start-->
      <%@include  file="../resources/common/AfterLoginTop.jsp" %>
      <!--Dashboard Header End-->
      <!--Dashboard Content Part Start-->
      <div class="contentArea_1">
        <div class="pageHead_1">Search and Verify Payment Details</div>
      <div>&nbsp;</div>

      <form id="frmData" method="post">
        <div class="formBg_1 t_space">
            <input type="hidden" name="userId" id="userId" value="<%=userId%>"></input>
            <table cellspacing="10" class="formStyle_1" width="100%">
                <tr>
                    <td width="20%" class="ff">Tender ID :</td>
                    <td width="30%"><input name="txtTenderId" type="text" class="formTxtBox_1" id="txtTenderId" style="width:195px;" /></td>
                    <td width="20%" class="ff">Bidder / Consultant :</td>
                    <td width="30%" ><input name="txtTendererNm" type="text" class="formTxtBox_1" id="txtTendererNm" style="width:195px;" /></td>
                </tr>
            <tr>
              <td width="14%" class="ff">Email ID :</td>
              <td width="44%"><input name="txtEmailId" type="text" class="formTxtBox_1" id="txtEmailId" style="width:195px;" /></td>
              <td width="13%" class="ff">Verification Status :</td>
              <td width="29%">
                  <select name="comboStatus" class="formTxtBox_1" id="comboStatus" style="width:75px;">
                      <option value='yes' >Verified</option>
                      <option value='no' selected="selected">Pending</option>
                  </select>
              </td>
              </tr>
            <tr>
              <td class="ff">Payment Date From : </td>
              <td>
                  <input onfocus="GetCal('txtFromDt','txtFromDt');" name="txtFromDt" type="text" class="formTxtBox_1" id="txtFromDt" style="width:100px;" readonly="readonly" />
                  <img id="imgFromDt" name="imgFromDt" onclick="GetCal('txtFromDt','imgFromDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                  <span id="SpError" class="reqF_1"></span>
              </td>
              <td class="ff">Payment Date To : </td>
              <td>
               <input onfocus="GetCal('txtToDt','txtToDt');" name="txtToDt" type="text" class="formTxtBox_1" id="txtToDt" style="width:100px;" readonly="readonly" />
                  <img id="imgToDt" name="imgToDt" onclick="GetCal('txtToDt','imgToDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                  <span id="SpError" class="reqF_1"></span>
              </td>
              </tr>
            <tr>
                <%if(isBankChecker){%>
                    <td class="ff">Branch  Name : </td>
                    <td><select name="comboBranches" class="formTxtBox_1" id="comboBranches" style="width:155px;">
                            <option value='' selected="selected">- Select Branch Name -</option>
                            <%
                            int branchCnt=0;
                            for (SPTenderCommonData sptcd : tenderCommonService.returndata("getBankBranchOrMemberList", userId, "getBranchList")) {
                                branchCnt++;
                            %>
                            <option value="<%=sptcd.getFieldName1()%>" ><%=sptcd.getFieldName2()%></option>
                            <%}%>
                        </select>
                    </td>

                    <td value='' class="ff">Branch  Maker : </td>
                    <td>
                        <select name="comboBranchMembers" class="formTxtBox_1" id="comboBranchMembers" style="width:155px;">
                            <option value=''>- Select Branch Maker -</option>
                        </select>
                    </td>
                <%} else if(isBranchChecker){%>
                    <td class="ff">Branch  Maker : </td>
                    <td>
                        <select name="comboBranchMembers" class="formTxtBox_1" id="comboBranchMembers" style="width:155px;">
                            <option value='' selected="selected">- Select Branch Maker -</option>
                            <%
                            int branchCnt=0;
                            for (SPTenderCommonData sptcd : tenderCommonService.returndata("getBankBranchOrMemberList", userBranchId, "getBranchMemberList")) {
                                branchCnt++;
                            %>
                            <option value="<%=sptcd.getFieldName1()%>" ><%=sptcd.getFieldName2()%></option>
                            <%}%>
                        </select>
                    </td>

                        <td class="ff">Payment For : </td>
                    <td >
                        <select name="comboPaymentFor" class="formTxtBox_1" id="comboPaymentFor" style="width:155px;">
                            <option value='All' selected="selected">All</option>
                            <option value='Document Fees'>Document Fees</option>
                            <option value='Bid Security'>Bid Security</option>
                            <option value='Performance Security'>Performance Security</option>
                        </select>
                        <select name="comboBranches" class="formTxtBox_1" id="comboBranches" style="width:155px; visibility: hidden">
                            <option value='' selected="selected">- Select Branch Name -</option>
                        </select>
                    </td>
                <%}%>
            </tr>
             <%if(isBankChecker){%>
             <tr>
                 <td class="ff">Payment For : </td>
                 <td>
                     <select name="comboPaymentFor" class="formTxtBox_1" id="comboPaymentFor" style="width:155px;">
                         <option value='All' selected="selected">All</option>
                         <option value='Document Fees'>Document Fees</option>
                         <option value='Bid Security'>Bid Security</option>
                         <option value='Performance Security'>Performance Security</option>
                     </select>
                 </td>
                 <td></td>
                 <td></td>
             </tr>
             <%}%>
            <tr>
              <td colspan="4" class="t-align-center ff"><label class="formBtn_1">
                <input type="button" name="btnSearch" id="btnSearch" value="Search" />
              </label>

                <label class="formBtn_1 l_space">
<!--                <input type="submit" name="btnReset" id="btnReset" value="Reset" />-->
                    <input type="button" name="btnReset" id="btnReset" value="Reset" />
              </label>
              </td>
              </tr>
            </table>
</div>
</form>

<div class="tabPanelArea_1 t_space">
    <div id="jQGrid" align="center">
        <table id="list"></table>
        <div id="page"></div>
    </div>
</div>

        <div>&nbsp;</div>
        </div>
</div>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
 <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
  <!--Dashboard Footer End-->

</body>
</html>
