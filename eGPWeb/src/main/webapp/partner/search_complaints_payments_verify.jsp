		<%-- 
		    Document   : VerifyTenderPayment
		    Created on : Mar 3, 2011, 5:55:26 PM
		    Author     : Karan
		--%>
		
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<%@page import="java.util.List"%>
		<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
		<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Verify Complaint <%=session.getAttribute("feetype")%> Payment</title>
		<style type="text/css">
		.pg-normal {
		color: #000000;
		font-weight: normal;
		text-decoration: none;
		cursor: pointer;
		}
		
		.pg-selected {
		color: #800080;
		font-weight: bold;
		text-decoration: underline;
		cursor: pointer;
		}
		</style>
		
	
		<link
			href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css"
			rel="stylesheet" type="text/css" />
		
		<link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
		        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
		        <link href="../css/home.css" rel="stylesheet" type="text/css" />
		        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
		        <script src="../js/jQuery/jquery.validate.js"type="text/javascript"></script>
		        <script type="text/javascript" src="..js/jQuery/jquery.alerts.js"></script>
		        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
				<link type="text/css" rel="stylesheet" href="../css/demo_table.css" />
				<link type="text/css" rel="stylesheet" href="../css/demo_page.css" />
		
		
		        <script src="../js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
		        <script src="../js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
		        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
		        <link href="../js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
		        <link href="../js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
		
		        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
		        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
		        <script type="text/javascript" src="../js/datepicker/js/jscal2_1.js"></script>
		        <script  type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>
                        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
                        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
		        <script type="text/javascript">
		        function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }

                        function submitButton(button) {
		    		if ((document.getElementById("txtSearchEmailId").value == "")
		    				&& (document.getElementById("txtFromDt").value == "")
		    				&& (document.getElementById("txtToDt").value == "")
		    				&& (document.getElementById("comboStatus").value == "")) {
		    			return false;
		    		} else if(document.getElementById('txtTenderId').value!=""){
                                    if(!regForNumber(document.getElementById('txtTenderId').value)){
//                                    jAlert("Tender Id must be numeric","Complaint Management", function(RetVal) {
//                                        });
                                        alert("Tender Id must be numeric");
                                        document.getElementById('txtTenderId').value="";
                                        return false;
                                    }
                                }
                                else {
						document.searchsubmit.action = "searchForComplaintPayments.htm";
		    			document.searchsubmit.submit();
		    			return true;
		    		}
		    	}
		            function GetCal(txtname,controlname)
		            {
		                new Calendar({
		                    inputField: txtname,
		                    trigger: controlname,
		                    showTime: false,
		                    dateFormat:"%d/%m/%Y",
		                    onSelect: function() {
		                        var date = Calendar.intToDate(this.selection.get());
		                        LEFT_CAL.args.min = date;
		                        LEFT_CAL.redraw();
		                        this.hide();
		                    }
		                });
		
		                var LEFT_CAL = Calendar.setup({
		                    weekNumbers: false
		                })
		            }
		        </script>
		
		
		                    <script type="text/javascript">
		                        $(function() {
		                            $('#comboBranches').change(function() {
		                                if( $('#comboBranches').val()!=''){
		                                    branchId = $('#comboBranches').val();
		                                    $.post("<%=request.getContextPath()%>/VerifyPaymentServlet", {branchId: branchId, action: 'getBranchMembers'},  function(j){
		                                        //alert(j);
		                                        $("select#comboBranchMembers").html(j);
		                                    });
		                                }
		                            });
		
		                            
		                    </script>
							<script type="text/javascript">
		
		function Pager(tableName, itemsPerPage) {
		this.tableName = tableName;
		this.itemsPerPage = itemsPerPage;
		this.currentPage = 1;
		this.pages = 0;
		this.inited = false;
		
		this.showRecords = function(from, to) {
		var rows = document.getElementById(tableName).rows;
		// i starts from 1 to skip table header row
		for (var i = 1; i < rows.length; i++) {
		if (i < from || i > to)
		rows[i].style.display = 'none';
		else
		rows[i].style.display = '';
		}
		}
		
		this.showPage = function(pageNumber) {
		if (! this.inited) {
		alert("not inited");
		return;
		}
		
		var oldPageAnchor = document.getElementById('pg'+this.currentPage);
		oldPageAnchor.className = 'pg-normal';
		
		this.currentPage = pageNumber;
		var newPageAnchor = document.getElementById('pg'+this.currentPage);
		newPageAnchor.className = 'pg-selected';
		
		var from = (pageNumber - 1) * itemsPerPage + 1;
		var to = from + itemsPerPage - 1;
		this.showRecords(from, to);
		}
		
		this.first = function() {
		
		this.showPage(1);
		}
		
		this.prev = function() {
		if (this.currentPage > 1)
		this.showPage(this.currentPage - 1);
		}
		
		this.next = function() {
		if (this.currentPage < this.pages) {
		this.showPage(this.currentPage + 1);
		}
		
		
		}
		this.last = function() {
		
		this.showPage(this.pages);
		}
		this.goton = function(pageNO) {
		
		if(pageNO.value <= this.pages){
		this.showPage(pageNO.value);
		}
		}
		
		
		this.init = function() {
		var rows = document.getElementById(tableName).rows;
		var records = (rows.length - 1);
		this.pages = Math.ceil(records / itemsPerPage);
		this.inited = true;
		}
		
		this.showPageNav = function(pagerName, positionId) {
		if (! this.inited) {
		alert("not inited");
		return;
		}
		var element = document.getElementById(positionId);
		var pagerHtml = '<br /><span>Page '+this.currentPage+' - '+this.pages +'</span>  &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; ';
		
		
		pagerHtml += '&nbsp; &nbsp; &nbsp;<input type="text" style="width: 20px;" class="formTxtBox_1" value="" id="pageNo" >';
		pagerHtml += '<label class="formBtn_1 l_space"><input type="button" value="Go To Page" onclick="'+pagerName+'.goton(pageNo);"></label> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;  ';
		for (var page = 1; page <= this.pages; page++)
		pagerHtml += '<span id="pg' + page + '" class="pg-normal" style="color: gray;" onclick="' + pagerName + '.showPage(' + page + ');"> </span>  ';
		pagerHtml += '<span style="color: gray;" onclick="' + pagerName + '.first();" class="pg-normal"> << First &nbsp;  </span>  ';
		pagerHtml += '<span style="color: gray;" onclick="' + pagerName + '.prev();" class="pg-normal"> < Previous&nbsp; &nbsp; </span>  ';
		
		pagerHtml += '<span style="color: gray;" onclick="'+pagerName+'.next();" class="pg-normal"> Next&nbsp; &nbsp;  ></span>';
		pagerHtml += '<span style="color: gray;" onclick="'+pagerName+'.last();" class="pg-normal"> Last >></span>';
		
		
		element.innerHTML = pagerHtml;
		
		}
		}
		
		</script>
		
		</head>
		
		
		    <%
		    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
		     boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false;
		     String userBranchId="";
		    String userId = "";
		                HttpSession hs = request.getSession();
		                if (hs.getAttribute("userId") != null) {
		                    userId = hs.getAttribute("userId").toString();
		                } else {
		                    response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
		                }
		
		                List<SPTenderCommonData> lstCurBankUserRole = tenderCommonService.returndata("getBankUserRole",userId,null);
		                if(!lstCurBankUserRole.isEmpty()){
		                    if("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
		                        isBranchMaker=true;
		                    } else if("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
		                        isBranchChecker=true;
		                    } else if("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
		                        isBankChecker=true;
		                    }
		                    userBranchId=lstCurBankUserRole.get(0).getFieldName2();
		                }
		    %>
		    <body>
		   <div class="dashboard_div">
		      <!--Dashboard Header Start-->
		      <%@include  file="../resources/common/AfterLoginTop.jsp" %>
		      <!--Dashboard Header End-->
		      <!--Dashboard Content Part Start-->
		      <div class="contentArea_1">
		        <div class="pageHead_1">Verify Complaint <%=session.getAttribute("feetype")%> Payment
                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('6');">Save as PDF</a></span>
                        </div>
		      <div>&nbsp;</div>
		
		      <form id="searchsubmit" name="searchsubmit" method="POST">
		
		<div class="tabPanelArea_1 t_space">
		        <div class="formBg_1 t_space">
		            <input type="hidden" name="userId" id="userId" value="<%=userId%>"></input>
		            <table cellspacing="10" class="formStyle_1" width="100%">
		                <tr>
		                    <td width="20%" class="ff"><fmt:message key="LBL_TENDER_ID"/>:</td>
		                    <td width="30%"><input name="txtTenderId" type="text" class="formTxtBox_1" id="txtTenderId" style="width:195px;" /></td>
		                    <td width="20%" class="ff"><fmt:message key="LBL_TENDERER_OR_CONSULTANT"/> :</td>
		                    <td width="30%" ><input name="txtTendererNm" type="text" class="formTxtBox_1" id="txtTendererNm" style="width:195px;" /></td>
		                </tr>
		            <tr>
		              <td width="14%" class="ff"><fmt:message key="LBL_EMAIL_ID"/>:</td>
		              <td width="44%"><input name="txtSearchEmailId" type="text" class="formTxtBox_1" id="txtSearchEmailId" style="width:195px;" /></td>
		              <td width="13%" class="ff"><fmt:message key="LBL_VERIFICATION_STATUS"/> :</td>
		              <td width="29%">
		                  <select name="comboStatus" class="formTxtBox_1" id="comboStatus" style="width:75px;">
							  <option value='select' selected="selected">Select</option>
		                      <option value='Yes' ><fmt:message key="OPT_VERIFICATION"/></option>
		                      <option value='No'><fmt:message key="LBL_PENDING"/></option>
		                  </select>
		              </td>
		              </tr>
		            <tr>
		              <td class="ff"><fmt:message key="LBL_PAYMENT_DATE_FROM"/> : </td>
		              <td>
		                  <input onfocus="GetCal('txtFromDt','txtFromDt');" name="txtFromDt" type="text" class="formTxtBox_1" id="txtFromDt" style="width:100px;" readonly="readonly" />
		                  <img id="imgFromDt" name="imgFromDt" onclick="GetCal('txtFromDt','imgFromDt');" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
		                  <span id="SpError" class="reqF_1"></span>
		              </td>
		              <td class="ff"><fmt:message key="LBL_PAYMENT_DATE_TO"/>: </td>
		              <td>
		               <input onfocus="GetCal('txtToDt','txtToDt');" name="txtToDt" type="text" class="formTxtBox_1" id="txtToDt" style="width:100px;" readonly="readonly" />
		                  <img id="imgToDt" name="imgToDt" onclick="GetCal('txtToDt','imgToDt');" src="../images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
		                  <span id="SpError" class="reqF_1"></span>
		              </td>
		              </tr>
		            <tr>
		                <%if(isBankChecker){%>
		                    <td class="ff"><fmt:message key="LBL_BRANCH_NAME"/>: </td>
		                    <td><select name="comboBranches" class="formTxtBox_1" id="comboBranches" style="width:155px;">
		                            <option value='' selected="selected">- Select Branch Name -</option>
		                            <%
		                            int branchCnt=0;
		                            for (SPTenderCommonData sptcd : tenderCommonService.returndata("getBankBranchOrMemberList", userId, "getBranchList")) {
		                                branchCnt++;
		                            %>
		                            <option value="<%=sptcd.getFieldName1()%>" ><%=sptcd.getFieldName2()%></option>
		                            <%}%>
		                        </select>
		                    </td>
		
		                    <td value='' class="ff"><fmt:message key="LBL_BRANCH_MAKER"/>: </td>
		                    <td>
		                        <select name="comboBranchMembers" class="formTxtBox_1" id="comboBranchMembers" style="width:155px;">
		                            <option value=''>- Select Branch Maker -</option>
		                        </select>
		                    </td>
		                <%} else if(isBranchChecker){%>
		                    <td class="ff"><fmt:message key="LBL_BRANCH_MAKER"/> : </td>
		                    <td>
		                        <select name="comboBranchMembers" class="formTxtBox_1" id="comboBranchMembers" style="width:155px;">
		                            <option value='' selected="selected">- Select Branch Maker -</option>
		                          	<c:if test="${partnerAdmin != null}">
									  <c:forEach var="partner" items="${partnerAdmin}">
										 <option value="${partner.tblLoginMaster.userId}" >${partner.fullName}</option>
									  </c:forEach>
									</c:if>
		                        </select>
		                    </td>
		
		                     <td class="ff"> </td>
		                    <td >
		                        
		                    </td>
		                <%}%>
		            </tr>
		             <%if(isBankChecker){%>
		             <tr>
		                 <td class="ff"><fmt:message key="LBL_PAYMENT_FOR"/> : </td>
		                 <td>
		                     <select name="comboPaymentFor" class="formTxtBox_1" id="comboPaymentFor" style="width:155px;">
		                         <option value='All' selected="selected"><fmt:message key="OPT_ALL"/></option>
		                         <option value='Document Fees'><fmt:message key="OPT_DOCUMENT_FEES"/></option>
		                         <option value='Bid Security'><fmt:message key="OPT_TENDER_SECURITY"/></option>
		                         <option value='Performance Security'><fmt:message key="OPT_PERFORMANCE_SECURITY"/></option>
		                     </select>
		                 </td>
		                 <td></td>
		                 <td></td>
		             </tr>
		             <%}%>
		            <tr>
		              <td colspan="4" class="t-align-center ff"><label class="formBtn_1">
						<input type="hidden" name="verify" id="verify" value="verify"/>
		                <input type="button" name="btnSearch" id="btnSearch" value="Search" onclick="javascript:submitButton(id)"  />
		              </label>
		
		                <label class="formBtn_1 l_space">
		<!--                <input type="submit" name="btnReset" id="btnReset" value="Reset" />-->
		                    <input type="button" name="btnReset" id="btnReset" value="Reset" />
		              </label>
		              </td>
		              </tr>
		            </table>
		
		</div>
                </div>
		</form>
		
		<div class="tabPanelArea_1 t_space">
		   <table cellpadding="0" cellspacing="0" border="0" class="tableList_3"
                    id="resultTable" width="100%" style="table-layout: fixed" cols="@6">
			<thead>
				<tr>
					<th class="ff"><fmt:message key="LBL_COMPLAINT_ID"/></th>
					<th class="ff"><fmt:message key="LBL_EMAIL_ID"/></th>
					<th class="ff"><fmt:message key="LBL_TENDER_ID"/></th>
					<th class="ff"><fmt:message key="LBL_COMPLAINT_SUBJECT"/></th>
					<th class="ff"><fmt:message key="LBL_COMPLAINT_CREATION_DATE"/></th>
					<th class="ff"><fmt:message key="LBL_DATE_OF_PAYMENT"/></th>
					<th class="ff"><fmt:message key="LBL_ACTION"/></th>
				</tr>
			</thead>
			<tbody>
			<c:choose>
			<c:when test="${cmppy != null}">
				<c:forEach var="cmppy" items="${cmppy}">
					<tr>
						<td style="text-align: center; word-wrap:break-word;">${cmppy.complaintId}</td>
						<td style="text-align: center; word-wrap:break-word;">${cmppy.emailId}</td>
						<td style="text-align: center; word-wrap:break-word;">${cmppy.tenderId}</td>
						<td style="text-align: center; word-wrap:break-word;">${cmppy.complaintSubject}</td>
		
		
					
		            <c:set var="createDate" scope="request" value="${cmppy.createdDate}"/>   
					
				    <%
					   String createdDate = request.getAttribute("createDate").toString();
				       SimpleDateFormat sdfSource = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
					   Date dtCreate = sdfSource.parse(createdDate);
					   SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MM-yy hh:mm");
					   createdDate = sdfDestination.format(dtCreate);
		
					%>
		     
						<td style="text-align: center;"><%=createdDate%> </td>
						<td style="text-align: center;"><c:if test="${cmppy.status !=''}">
							<c:choose>
								<c:when test="${cmppy.dtOfAction != null}">
								<c:set var="actionDate" scope="request" value="${cmppy.dtOfAction}"/> 
								  <%
									   String strActionDt = request.getAttribute("actionDate").toString();
									   Date dtAction = sdfSource.parse(strActionDt);
									   strActionDt = sdfDestination.format(dtAction);
								  %>
									<%=strActionDt%><br/>
								</c:when>
								<c:otherwise>-
							    </c:otherwise>
							</c:choose>
						</c:if></td>
		
						<td><c:if test="${cmppy.isLive=='Yes'}">
							<c:if test="${cmppy.status == 'paid'}">
								<c:choose>
								<c:when test="${cmppy.isVerified=='No'}">
										<a href="complaintFeePayment.htm?complaintId=${cmppy.complaintId}&emailId=${cmppy.emailId}&complaintPaymentId=${cmppy.complaintPaymentId}&verify=verify">Verify</a>&nbsp|&nbsp
										<a href="viewComplaintFeeDetails.htm?complaintId=${cmppy.complaintId}&emailId=${cmppy.emailId}&complaintPaymentId=${cmppy.complaintPaymentId}">ViewPayment</a>
								</c:when>
								<c:otherwise >
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<a href="viewComplaintFeeDetails.htm?complaintId=${cmppy.complaintId}&emailId=${cmppy.emailId}&complaintPaymentId=${cmppy.complaintPaymentId}">View</a>
								</c:otherwise>
								</c:choose>
							</c:if>
							</c:if>
						</td>
					</tr>
				</c:forEach>
				</c:when>
				<c:otherwise>
		 				<td style="text-align: center;" colspan=7 > <fmt:message key="LBL_NO_RECORDS_FOUND"/></td>
				</c:otherwise>
		</c:choose>
			</tbody>
		</table>
		<div id="pageNavPosition"></div>
		
		</div>
		</div>
                <!--For Generate PDF  Starts-->
                <!--For Generate PDF  Ends-->
		<script type="text/javascript"><!--
		var pager = new Pager('resultTable', 10);
		pager.init();
		pager.showPageNav('pager', 'pageNavPosition');
		pager.showPage(1);
                sortTable();
		//--></script>
                <form id="formstyle" action="" method="post" name="formstyle">
                    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                    <%
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                        String appenddate = dateFormat1.format(new Date());
                    %>
                    <input type="hidden" name="fileName" id="fileName" value="ComplaintFee_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="ComplaintFee" />
                </form>
		  <!--Dashboard Content Part End-->
		  <!--Dashboard Footer Start-->
		 <div align="center">
		 <%@include file="../resources/common/Bottom.jsp" %>
		 </div>
		  <!--Dashboard Footer End-->
		  </div>
		</body>
		</html>