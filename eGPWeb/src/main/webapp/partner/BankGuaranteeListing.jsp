<%--
    Document   : TenderPaymentDetails
    Created on : Mar 3, 2011, 1:52:57 PM
    Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsNewBankGuarnateeService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New Performance Security Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%
                    TenderCommonService objTSC = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    CmsNewBankGuarnateeService cmsNewBankGuarnateeService = (CmsNewBankGuarnateeService) AppContext.getSpringBean("CmsNewBankGuarnateeService"); 
                    
                    CommonSearchDataMoreService objTpayment = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    if (session.getAttribute("userId") != null) {
                        objTSC.setLogUserId(session.getAttribute("userId").toString());
                    }
                    boolean isCurrUserPE = false;
                    boolean isBankUser=false;
                    boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false, isRedirect=false;
                    String userBranchId="";
                    String userId="", payUserId="", regPaymentId="", redirectPath="";
                    String tenderId="0", pkgLotId="0", payTyp="", paymentTxt="";

                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                             userId = hs.getAttribute("userId").toString();
                    }
                    
                    String referer = "";
                    if (request.getHeader("referer") != null) {
                        referer = request.getHeader("referer");
                    }

                    if ( hs.getAttribute("userTypeId")!= null) {
                        if("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())){
                            isBankUser=true; // userType is ScheduleBank";
                        }
                    }

                    if (request.getParameter("uId") != null) {
                        payUserId = request.getParameter("uId");
                    }

                    if (request.getParameter("tenderId") !=null){
                        tenderId=request.getParameter("tenderId");
                    }
                     if (request.getParameter("lotId") != null){
                        if (request.getParameter("lotId")!="" && !request.getParameter("lotId").equalsIgnoreCase("null")){
                           pkgLotId=request.getParameter("lotId");
                        }
                    }
                    
                    /* Start: CODE TO CHECK PE USER */
                    List<SPTenderCommonData> listChkCurrUserPE = objTSC.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);

                    if (!listChkCurrUserPE.isEmpty()) {
                        if (userId.equalsIgnoreCase(listChkCurrUserPE.get(0).getFieldName1())) {
                            isCurrUserPE = true;
                        }
                    }

                    // Dohate Start
                    List<SPTenderCommonData> listDP = objTSC.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                    boolean isIctTender = false;
                    if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true")){
                       isIctTender = true;
                    }                    
                    // Dohatec End
                    listChkCurrUserPE = null;
                    //  Dohatec Start
                    //List<Object[]> listNewBankGuarantee = cmsNewBankGuarnateeService.bankGuaranteeListing(Integer.parseInt(tenderId));
                    List<Object[]> listNewBankGuarantee = null;
                    if(isIctTender) // ICT
                    {
                        listNewBankGuarantee = cmsNewBankGuarnateeService.bankGuaranteeListingICT(Integer.parseInt(tenderId));
                    }
                    else    //  NCT
                    {
                        listNewBankGuarantee = cmsNewBankGuarnateeService.bankGuaranteeListing(Integer.parseInt(tenderId));
                    }
                    //  Dohatec End
                    
                    /* End: CODE TO CHECK PE USER */
                %>
                <div class="contentArea_1">
                    <div class="pageHead_1">New Performance Security Details
                        <span style="float:right;">                        
                            <a href="ForTenderPayment.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a>
                        </span>
                    </div>
                    <div id="resultDiv">
                        <div>&nbsp;</div>
                        <div class="tabPanelArea_1 t_space">  
                            <table width='100%' cellspacing='0' class='tableList_1 t_space'>
                             <tr>
                                <th width='4%' class='t-align-center'>Sl. No.</th>
                                <th width='25%' class='t-align-center'>e-mail Id</th>
                                <th class='t-align-center' width='31%%'>Name of Bidder/Consultant</th>
                                <th class='t-align-center' width='20%%'>Contract No.</th>
                                <th class='t-align-center' width='20%'>Action</th>                                
                             </tr>
                             <%
                                int srNo  = 0;
                                if(!listNewBankGuarantee.isEmpty()){
                                    for(Object[] bankGuarantee:listNewBankGuarantee){
                                        srNo++;
                             %>
                             <tr>
                                <td width='10%' class='t-align-center'><%=srNo%></td>
                                <td width='25%' class='t-align-center'><%=bankGuarantee[3]%></td>
                                <% if("1".equalsIgnoreCase(bankGuarantee[9].toString())){ %>
                                <td class='t-align-center' width='25%%'><%=bankGuarantee[7]%> <%=bankGuarantee[8]%></td>
                                <% }else{ %>
                                <td class='t-align-center' width='25%%'><%=bankGuarantee[6]%></td>
                                <% } %>
                                <td class='t-align-center' width='20%%'><%=bankGuarantee[4]%></td>
                                <td class='t-align-center' width='20%'>
                                <!--    Dohatec Start   -->
                                <%
                                    String noaId = "";
                                    if(isIctTender && !listNewBankGuarantee.isEmpty())
                                    {
                                        List<SPTenderCommonData> listNoaId = objTSC.returndata("getNoaIssueIdForICT", request.getParameter("tenderId"), bankGuarantee[0].toString());
                                        noaId = listNoaId.get(0).getFieldName1();
                                    }
                                %>
                                <!--    Dohatec End -->
                                    <% if("0".equals(bankGuarantee[10].toString())){ %>
                                        <%if(isIctTender) {%>   <!-- Added by Dohatec -->
                                            <a href="TenderPayment.jsp?uId=<%=bankGuarantee[11]%>&tenderId=<%=bankGuarantee[1]%>&lotId=<%=bankGuarantee[2]%>&payTyp=bg&bgId=<%=bankGuarantee[0]%>&extId=<%=noaId%>">Make Payment</a>
                                        <%}else{%>  <!-- Previous Version -->
                                            <a href="TenderPayment.jsp?uId=<%=bankGuarantee[11]%>&tenderId=<%=bankGuarantee[1]%>&lotId=<%=bankGuarantee[2]%>&payTyp=bg&bgId=<%=bankGuarantee[0]%>&extId=<%=bankGuarantee[12]%>">Make Payment</a>
                                        <%}%>
                                    <% }else{ 
                                        List<Object[]> statusInfo =  cmsNewBankGuarnateeService.getTenderpaymentStatus(Integer.parseInt(bankGuarantee[10].toString()));
                                        if(!statusInfo.isEmpty()){
                                        if("yes".equalsIgnoreCase(statusInfo.get(0)[1].toString())){
                                    %>
                                    <a href="TenderPaymentDetails.jsp?payId=<%=bankGuarantee[10]%>&uId=<%=bankGuarantee[11]%>&tenderId=<%=bankGuarantee[1]%>&lotId=<%=bankGuarantee[2]%>&payTyp=bg&bgId=<%=bankGuarantee[0]%>">View</a>
                                    <%  }else{ %>
                                    <a href="TenderPayment.jsp?payId=<%=bankGuarantee[10]%>&uId=<%=bankGuarantee[11]%>&tenderId=<%=bankGuarantee[1]%>&lotId=<%=bankGuarantee[2]%>&payTyp=bg&action=edit&bgId=<%=bankGuarantee[0]%>">Edit</a>&nbsp;|&nbsp;
                                    <a href="TenderPaymentDetails.jsp?payId=<%=bankGuarantee[10]%>&uId=<%=bankGuarantee[11]%>&tenderId=<%=bankGuarantee[1]%>&lotId=<%=bankGuarantee[2]%>&payTyp=bg&bgId=<%=bankGuarantee[0]%>">View</a>
                                    <% }} %>
                                    <% } %>
                                </td>
                             </tr>
                             <% 
                                }
                            } %>
                            </table>
                        </div>
                 <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                    <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->
                    </div>
                </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabPayment");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
