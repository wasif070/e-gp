<%-- 
    Document   : PostPaymentProcess
    Created on : Mar 7, 2011, 4:39:47 PM
    Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsCompansiateAmt"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AccPaymentService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Post Payment Process</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
         <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" >
            $(document).ready(function(){
                $('#btnSubmit').click(function(){
                    var flag=true;

                     if($.trim($('#txtComments').val())==''){
                            $('#SpError').html('Please enter Remarks.');
                            flag=false;
                        }
                        else {
                            if($('#txtComments').length<=1000) {
                                $('#SpError').html('');
                            }
                            else{
                                $('#SpError').html('Maximum 1000 characters are allowed.');
                                flag=false;
                            }
                        }

                         if(flag==false){
                            return false;
                        }

                });

                $('#txtComments').blur(function(){
                    var flag=true;

                     if($.trim($('#txtComments').val())==''){
                            $('#SpError').html('Please enter Remarks.');
                            flag=false;
                        }
                        else {
                            if($('#txtComments').length<=1000) {
                                $('#SpError').html('');
                            }
                            else{
                                $('#SpError').html('Maximum 1000 characters are allowed.');
                                flag=false;
                            }
                        }

                         if(flag==false){
                            return false;
                        }
                });

            });
        </script>

    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%
                    //ADDED BY SALAHUDDIN
                    TenderCommonService tcs1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> listDP = tcs1.returndata("chkDomesticPreference", request.getParameter("tenderId"), null);
                    boolean isIctTender = false;
                    if(!listDP.isEmpty() && listDP.get(0).getFieldName1().equalsIgnoreCase("true"))
                    {
                        isIctTender = true;
                    }                
                %>
                <%
                    boolean isBankUser=false;
                    boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false;
                    boolean isUnauthorizedAccess = false;
                    String userBranchId="";
                    String userId="", payUserId="", regPaymentId="", opTyp="", reqId="", reqComments="";
                    String tenderId="0", pkgLotId="0", payTyp="", paymentTxt="";
                    TenderCommonService objTSC = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                 HttpSession hs = request.getSession();
                 if (hs.getAttribute("userId") != null) {
                         userId = hs.getAttribute("userId").toString();
                  }
                 else {response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");}

                String referer = "";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");
                }

                if ( hs.getAttribute("userTypeId")!= null) {
                    if("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())){
                        isBankUser=true; // userType is ScheduleBank";
                     }
                }

                if (request.getParameter("uId") != null) {
                    payUserId = request.getParameter("uId");
                }

                if (request.getParameter("payId") != null) {
                    regPaymentId = request.getParameter("payId");
                }

                if (request.getParameter("reqId") != null) {
                    reqId = request.getParameter("reqId");

                }
                               
                if (request.getParameter("tenderId") != null) {
                    tenderId = request.getParameter("tenderId");
                }
                
                if (request.getParameter("lotId") != null) {
                    if (request.getParameter("lotId") != "" && !request.getParameter("lotId").equalsIgnoreCase("null")) {
                        pkgLotId = request.getParameter("lotId");
                    }
                }

                     if (request.getParameter("payTyp") != null){
                        if (request.getParameter("payTyp")!="" && !request.getParameter("payTyp").equalsIgnoreCase("null")){
                           payTyp=request.getParameter("payTyp");

                            if ("df".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Document Fees";
                             } else if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Tender/Proposal Security";
                             } else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Performance Security";
                             }else if ("bg".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "New Performance Security";
                             }
                           
                        }
                    }

                     
                List<SPTenderCommonData> lstCurBankUserRole =
                                    objTSC.returndata("getBankUserRole", userId, null);

                if (!lstCurBankUserRole.isEmpty()) {
                    if ("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                        isBranchMaker = true;
                    } else if ("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                        isBranchChecker = true;
                    } else if ("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                        isBankChecker = true;
                    }
                    userBranchId = lstCurBankUserRole.get(0).getFieldName2();
                }
                lstCurBankUserRole = null;

             
                          if(!"0".equalsIgnoreCase(reqId)){
                              // Action Requested from PE
                            List<SPTenderCommonData> lstRequestAction = objTSC.returndata("getRequestActionFromRequestId", reqId, regPaymentId);
                                    if (!lstRequestAction.isEmpty()) {
                                        opTyp = lstRequestAction.get(0).getFieldName2();
                                        reqComments = lstRequestAction.get(0).getFieldName3();

                                        if(!lstRequestAction.get(0).getFieldName4().equalsIgnoreCase(userBranchId)){
                                            // User is from different Branch
                                            isUnauthorizedAccess = true;
                                        } else if ("Forfeit".equalsIgnoreCase(opTyp) && !isBranchChecker){
                                            // Forfeit can only be Processed by Branch Checker
                                            isUnauthorizedAccess = true;
                                        } else if ("Release".equalsIgnoreCase(opTyp) && !isBranchChecker){
                                            // Release can only be Processed by Branch Cheker
                                            isUnauthorizedAccess = true;
                                        }

                                    } else {
                                        isUnauthorizedAccess = true;
                                    }
                                    lstRequestAction = null;
                          } else if("0".equalsIgnoreCase(reqId)){
                            // Direct Action by Branch Maker
                              if(request.getParameter("opTyp")!=null){
                                if("cancel".equalsIgnoreCase(request.getParameter("opTyp")))
                                    opTyp="Cancel";
                                else if("release".equalsIgnoreCase(request.getParameter("opTyp")))
                                    opTyp="Release";
                              }

                              reqComments="";
                          }

                        %>

                <div class="contentArea_1">
                    <div class="pageHead_1">Post Payment Process
                        <span style="float:right;">
                            <% if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {%>
                            <%if("Forfeit".equalsIgnoreCase(opTyp) || "Release".equalsIgnoreCase(opTyp)) {%>
                                <a href="TenPaymentListing.jsp" class="action-button-goback">Go Back</a>
                            <%} else {%>
                                <a href="SearchTendererForTenPayment.jsp?uId=<%=payUserId%>&tenderId=<%=tenderId%>&lotId=<%=pkgLotId%>&payTyp=<%=payTyp%>" class="action-button-goback">Go Back</a>
                            <%}%>
                                
                            <%} else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {%>
                            <%if("Forfeit".equalsIgnoreCase(opTyp) || "Release".equalsIgnoreCase(opTyp)) {%>
                                <a href="TenPaymentListing.jsp" class="action-button-goback">Go Back</a>
                            <%} else {%>
                                 <a href="NOAIssuedList.jsp?tenderId=<%=tenderId%>" class="action-button-goback">Go Back</a>
                            <%}%>
                            <%}%>

                        </span>
                    </div>


                     <% pageContext.setAttribute("tenderId", tenderId ); %>
                     <%@include file="../resources/common/TenderInfoBar.jsp" %>

                    <div class="tabPanelArea_1 t_space">

                        <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("payment")){
                            msgTxt="Payment information entered successfully.";
                        } else if(msgId.equalsIgnoreCase("updated")){
                            msgTxt="Payment updated successfully.";
                        } else if(msgId.equalsIgnoreCase("extended")){
                            msgTxt="Payment extended successfully.";
                        } else  if(msgId.equalsIgnoreCase("released")){
                            msgTxt="Payment released successfully.";
                        } else  if(msgId.equalsIgnoreCase("cancelled")){
                            msgTxt="Payment cancelled successfully.";
                        } else  if(msgId.equalsIgnoreCase("on-hold")){
                            msgTxt="Payment put on-hold successfully.";
                        } else  if(msgId.equalsIgnoreCase("forfeited")){
                            msgTxt="Payment forfeited successfully.";
                        } else  if(msgId.equalsIgnoreCase("forfeitrequested")){
                            msgTxt="Payment forfeit request submitted successfully.";
                        } else  if(msgId.equalsIgnoreCase("releaserequested")){
                            msgTxt="Payment release request submitted successfully.";
                        } else  if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError){%>
                        <div class="responseMsg errorMsg" ><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg" ><%=msgTxt%></div>
                   <%}%>
                <%}}%>

                <%if(isUnauthorizedAccess){%>
                    <div class="responseMsg errorMsg" >Unauthorized User.</div>
                <%} else {%>

                <%
                            String bidderUserId = "0", bidderEmail = "";
                            String emailId = "";

                            List<SPTenderCommonData> lstTendererEml = tenderCommonService.returndata("getEmailIdfromUserId", payUserId, null);
                            emailId = lstTendererEml.get(0).getFieldName1();
                            bidderEmail = emailId;
                            bidderUserId = payUserId;
                %>

                <%
                    //List<SPTenderCommonData> lstPaymentDetail = tenderCommonService.returndata("getTenderPaymentDetail", regPaymentId, null);
                    CommonSearchDataMoreService objPayment = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");                    
                    List<SPCommonSearchDataMore> lstPaymentDetail =objPayment.geteGPData("getTenderPaymentDetail", regPaymentId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                            if (!lstPaymentDetail.isEmpty()) {
                %>

                <form id="frmVerifyPayment" action="<%=request.getContextPath()%>/TenderPaymentServlet?action=updatePayStatus" method="POST">

                    <input type="hidden" name="reqId" id="reqId" value="<%=reqId%>" />
                    <input type="hidden" name="payId" id="payId" value="<%=regPaymentId%>" />
                    <input type="hidden" name="uId" id="uId" value="<%=payUserId%>" />
                    <input type="hidden" name="tenderId" id="tenderId" value="<%=tenderId%>" />
                    <input type="hidden" name="lotId" id="lotId" value="<%=pkgLotId%>" />
                    <input type="hidden" name="payTyp" id="payTyp" value="<%=payTyp%>" />
                    <input type="hidden" name="operation" id="payId" value="<%=opTyp%>" />
                    <input type="hidden" name="tenderRefNo" id="tenderRefNo" value="<%=toextTenderRefNo%>" />

                    
                   <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td width="18%" class="ff">Action :</td>
                            <td><%if("Forfeit".equalsIgnoreCase(opTyp)){out.print("Compensate");}else{out.print(opTyp);}%> Payment</td>
                        </tr>
                        <%if(!"".equalsIgnoreCase(reqComments)){%>
                          <tr>
                            <td class="ff">Remarks :</td>
                            <td><%=reqComments%></td>
                        </tr>
                        <%}%>
                    </table>

                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">                           
                            <tr>
                            <td width="18%" class="ff">Email ID :</td>
                            <td><%=bidderEmail%></td>
                        </tr>
                         <tr>
                            <td class="ff">Bank Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName11()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Maker :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName2()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Payment For : </td>
                            <td><%=paymentTxt%></td>
                        </tr>
                        <%if(!isIctTender){%>
                        <tr>
                            <td class="ff">Currency :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName3()%>
                            <input type="hidden" id="hdnCurrency" name="hdnCurrency" value="<%=lstPaymentDetail.get(0).getFieldName3()%>">
                            </td>
                        </tr>
                        <%}%>
                        <%
                          if(!isIctTender && "Forfeit".equalsIgnoreCase(opTyp))
                          {
                              AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
                              List<TblCmsCompansiateAmt> listCompAmt = accPaymentService.getTblCmsCompansiateAmt(Integer.parseInt(regPaymentId));
                              if(!listCompAmt.isEmpty())
                              {
                        %>
                        <tr>
                            <td class="ff">Compensate Amount :</td>
                            <td><%=listCompAmt.get(0).getCompAmt()%> &nbsp;&nbsp; Balance Value : [<%=listCompAmt.get(0).getBalanceAmt()%>]
                            </td>
                        </tr>
                        <%}}%>
                        <%if(!isIctTender){%>
                        <tr>
                            <td class="ff">Amount :</td>
                            <td>
                                <%if("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%} else if("USD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>$</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%} else if("Nu.".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%}%>
                                <input type="hidden" id="hdnAmount" name="hdnAmount" value="<%=lstPaymentDetail.get(0).getFieldName4()%>">
                                <input type="hidden" id="hdnValidityRefId" name="hdnValidityRefId" value="<%=lstPaymentDetail.get(0).getFieldName9()%>">
                                <input type="hidden" id="hdnPayStatus" name="hdnPayStatus" value="<%=lstPaymentDetail.get(0).getFieldName10()%>">
                                <input type="hidden" id="hdnBidderUserId" name="hdnBidderUserId" value="<%=bidderUserId%>">

                            </td>
                        </tr>
                        <%}else{%>
                        <%
                            boolean flag=false;
                            String tID = request.getParameter("tenderId");
                            //for currency symbol
                            List<SPCommonSearchDataMore> lstPerSecPayDetail = objPayment.geteGPData("getPerformanceSecurityPaymentDetail", tenderId.toString());
                            //if("Forfeit".equalsIgnoreCase(opTyp))
                            //{
                            List<TblCmsCompansiateAmt> listCompAmt2 = null;
                            List<SPCommonSearchDataMore> lstPaymentDetail2 = null;
                            if("bg".equalsIgnoreCase(request.getParameter("payTyp")))
                            {
                                lstPaymentDetail2 = objPayment.geteGPData("getTenderPaymentDetail", null, tID, "New Performance Security", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                                if(!lstPaymentDetail2.isEmpty())
                                    flag=true;                               
                            }

                           AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
                           //}

                            List<SPCommonSearchDataMore> lstPaymentDetail1 = objPayment.geteGPData("getTenderPaymentDetail", regPaymentId, "Performance Security", tID, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                            for(int i=0;i<lstPaymentDetail1.size();i++){
                                if("bg".equalsIgnoreCase(request.getParameter("payTyp")))
                                {
                                    listCompAmt2 = accPaymentService.getTblCmsCompansiateAmt(Integer.parseInt(lstPaymentDetail2.get(i).getFieldName2()));
                                }
                                //List<TblCmsCompansiateAmt> listCompAmt2 = accPaymentService.getTblCmsCompansiateAmt(Integer.parseInt(lstPaymentDetail2.get(i).getFieldName2()));
                                //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>list size:"+listCompAmt2.size());
                                //System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>paymentID:"+Integer.parseInt(lstPaymentDetail2.get(i).getFieldName2()));
                        %>
                        <%if("Forfeit".equalsIgnoreCase(opTyp) && flag){%>
                        <tr>
                            <td class="ff"><%out.println("Compensate Amount ( in "+lstPaymentDetail1.get(i).getFieldName3()+") :");%></td>
                            <td><%=listCompAmt2.get(0).getCompAmt()%> &nbsp;&nbsp; Balance Value : [<%=listCompAmt2.get(0).getBalanceAmt()%>]
                            </td>
                        </tr>
                        <%}%>
                        <tr>
                            <td class="ff"><%out.println("Amount ( in "+lstPaymentDetail1.get(i).getFieldName3()+") :");%></td>
                            <td>
                            <%
                                if("Forfeit".equalsIgnoreCase(opTyp) && flag){
                                    out.println(""+lstPerSecPayDetail.get(i).getFieldName13()+" " +lstPaymentDetail2.get(i).getFieldName1());
                                }else{
                                    out.println(""+lstPerSecPayDetail.get(i).getFieldName13()+" " +lstPaymentDetail1.get(i).getFieldName4());
                                }
                            %>
                                
                                <input type="hidden" id="hdnAmount" name="hdnAmount" value="<%=lstPaymentDetail.get(0).getFieldName4()%>">
                                <input type="hidden" id="hdnValidityRefId" name="hdnValidityRefId" value="<%=lstPaymentDetail.get(0).getFieldName9()%>">
                                <input type="hidden" id="hdnPayStatus" name="hdnPayStatus" value="<%=lstPaymentDetail.get(0).getFieldName10()%>">
                                <input type="hidden" id="hdnBidderUserId" name="hdnBidderUserId" value="<%=bidderUserId%>">
                            </td>
                        </tr>
                        <%}}%>
                         <tr>
                            <td class="ff">Mode of Payment :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName5()%></td>
                        </tr>
                        <%
                        if("Pay Order".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5()) || "DD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5()) || "Bank Guarantee".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = tenderCommonService.returndata("getTenderPaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Instrument No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Bank :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Bank Branch :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuance Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName4()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Validity Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName5()%></td>
                                </tr>

                               <% }
                            }%>

                            <%if("Account to Account Transfer".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = tenderCommonService.returndata("getTenderPaymentDetailMore", regPaymentId, null);
                                if (session.getAttribute("userId") != null) {
                                        tenderCommonService.setLogUserId(session.getAttribute("userId").toString());
                                }
                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Account No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <%if(!"".equalsIgnoreCase(lstPaymentDetailMore.get(0).getFieldName3())){%>
                                <tr>
                                    <td class="ff">Branch Name :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <%}%>
                               <% }
                            }%>

                            <tr>
                                <td class="ff">Date of Payment :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName6()%></td>
                            </tr>

                             <tr>
                                <td class="ff">Branch Maker Remarks : </td>
                                <td>
                                    <%=lstPaymentDetail.get(0).getFieldName7()%>
                                </td>
                            </tr>
                    </table>

                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td style="font-style: italic" class="ff t-align-right" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                        </tr>
                        <tr>
                            <td class="ff">Remarks : <span class="mandatory">*</span></td>
                            <td>
                                <textarea cols="100" rows="5" id="txtComments" name="txtComments" class="formTxtBox_1"></textarea>
                                <span id="SpError" class="reqF_1"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <div class="t-align-left">
                                    <label class="formBtn_1"><input name="btnSubmit" id="btnSubmit" type="submit" value="Submit" /></label>
                                </div>

                            </td>
                        </tr>
                    </table>

                    </form>
                    <% } else {%>
                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td>Payment information not found!</td>
                        </tr>
                    </table>
                    <%}%>


                <%}%>
  </div>
                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->


        </div>
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>


