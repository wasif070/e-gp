<%-- 
    Document   : TenPaymentDailyTrans
    Created on : Apr 6, 2011, 11:25:21 AM
    Author     : Karan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Search and Verify Payment Details</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />


        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />

        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>

        <script type="text/javascript">
            function fillGridOnEvent(userId, emailId, isVerified, branchId, branchMakerId, fromDt, toDt, tenderId, tendererNm, paymentFor){
                /*
                alert(userId);
                alert(emailId);
                alert(isVerified);
                alert(branchId);
                alert(branchMakerId);
                alert(fromDt);
                alert(toDt);
                */
                

                $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                jQuery("#list").jqGrid({
                    url:'<%=request.getContextPath()%>/VerifyTenderPaymentServlet?q=1&action=fetchDailyData&userId=' + userId + '&emailId=' + emailId + '&isVerified=' + isVerified + '&branchId=' + branchId + '&branchMakerId=' + branchMakerId + '&fromDt=' + fromDt + '&toDt=' + toDt + '&tenderId=' + tenderId + '&tendererNm=' + tendererNm + '&paymentFor=' + paymentFor,
                    datatype: "xml",
                    height: 250,
                    colNames:['Sl. No.','Tender ID','e-mail ID','Bidder / Consultant','Payment For',"Amount","Payment Date and Time",'Branch Maker',"Action"],
                    colModel:[
                        {name:'RowNumber',index:'RowNumber', width:40,sortable:false, align: 'center'},
                        {name:'tenderId',index:'tenderId', width:100,sortable:true, align: 'center'},
                        {name:'emailId',index:'emailId', width:250,sortable:true},
                        {name:'dbo.f_getbiddercompany(LM.userId)',index:'dbo.f_getbiddercompany(LM.userId)', width:250,sortable:true},
                        {name:'paymentFor',index:'paymentFor', width:150,sortable:true, align: 'center'},
                        {name:'amount',index:'amount', width:100,sortable:true, align: 'center'},
                        {name:'createdDate',index:'createdDate', width:140,sortable:true, align: 'center'},
                        {name:'PA.fullName',index:'PA.fullName', width:200,sortable:true},
                        {name:'action',index:'action', width:100,sortable:false, align: 'center'}

                    ],
                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    sortable:false,
                    caption: "",
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{edit:false,add:false,del:false, search:false});
            }
            jQuery().ready(function (){
                //fillGrid();
            });
        </script>

                    <script type="text/javascript">
                        $(function() {
                            $('#comboBranches').change(function() {
                                if( $('#comboBranches').val()!=''){
                                    branchId = $('#comboBranches').val();
                                    $.post("<%=request.getContextPath()%>/VerifyPaymentServlet", {branchId: branchId, action: 'getBranchMembers'},  function(j){
                                        //alert(j);
                                        $("select#comboBranchMembers").html(j);
                                    });
                                }
                            });

                           $('#btnSearchData').click(function() {
                                // alert('You clicked Search');

                                  var errflag = false;

                                $('#SpError1').html('');
                                
                                var userId='', emailId='', isVerified='', branchId='', branchMakerId='', fromDt='', toDt='', tenderId='', tendererNm='', paymentFor='';
                                userId=$('#userId').val();
                                emailId=$('#txtSearchEmailId').val();
                                isVerified=$('#comboStatus').val();


                                fromDt=$('#txtFromDt').val();
                                toDt=$('#txtToDt').val();

                                tenderId=$('#txtTenderId').val();
                                tendererNm=$('#txtTendererNm').val();
                                paymentFor=$('#comboPaymentFor').val();


                            if(document.getElementById("comboBranches")!=null){
                                if($('#comboBranches').val()!=''){
                                    branchId=$('#comboBranches').val();
                                }
                            }

                    if(document.getElementById("comboBranchMembers")!=null){
                        if($('#comboBranchMembers').val()!=''){
                            branchMakerId=$('#comboBranchMembers').val();
                        }
                    }

                                 if(fromDt!='' && toDt!=''){
                                //alert('Hi');
                                 var d =new Date();
                            var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());

                            var value=fromDt;
                            var mdy = value.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);

                             var value2=toDt;
                            var mdy2 = value2.split('/')  //Date and month split
                            var mdyhr2= mdy2[2].split(' ');  //Year and time split
                            var valuedate2= new Date(mdyhr2[0], mdy2[1]-1, mdy2[0]);

                            if(Date.parse(valuedate) > Date.parse(valuedate2)){
                                   errflag = true;
                                    $('#SpError1').html('Payment Date From must be less than or equal to Payment Date To');
                                }
                            }

                            if(!errflag){
                             fillGridOnEvent(userId, emailId, isVerified, branchId, branchMakerId, fromDt, toDt, tenderId, tendererNm, paymentFor);
                            }
                            });

                            $('#btnReset').click(function() {
                                //alert('Clearing...')

                                 // Clear Err Messages
                                $('#SpError1').html('');
                                //==========================

                               var userId=$('#userId').val();

                              $('#txtSearchEmailId').val('');
                                $('#comboStatus').val('');
                                $('#txtFromDt').val('');
                                $('#txtToDt').val('');

                                $('#txtTenderId').val('');
                                $('#txtTendererNm').val('');
                                $('#comboPaymentFor').val('All');

                                if(document.getElementById("comboBranches")!=null){
                                    $('#comboBranches').val('');
                                }

                                var j="<option value=''>- Select Branch Maker -</option>";
                                $("select#comboBranchMembers").html(j);

                                fillGridOnEvent(userId,'','','','','0','0','','','All');
                            });


                        });
                    </script>

</head>


    <%
    TenderCommonService objTSCDaily = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        if (session.getAttribute("userId") != null) {
         objTSCDaily.setLogUserId(session.getAttribute("userId").toString());
      }
     boolean bolIsBranchMaker=false, bolIsBranchChecker=false, bolIsBankChecker=false;
     String strUserBranchId="";
    String strUserId = "";
                HttpSession objSession = request.getSession();
                if (objSession.getAttribute("userId") != null) {
                    strUserId = objSession.getAttribute("userId").toString();
                } else {
                    //response.sendRedirectFILTER(request.getContextPath() + "/SessionTimedOut.jsp");
                }

                List<SPTenderCommonData> objLstCurBankUserRole = objTSCDaily.returndata("getBankUserRole",strUserId,null);
                if(!objLstCurBankUserRole.isEmpty()){
                    if("BranchMaker".equalsIgnoreCase(objLstCurBankUserRole.get(0).getFieldName1())){
                        bolIsBranchMaker=true;
                    } else if("BranchChecker".equalsIgnoreCase(objLstCurBankUserRole.get(0).getFieldName1())){
                        bolIsBranchChecker=true;
                    } else if("BankChecker".equalsIgnoreCase(objLstCurBankUserRole.get(0).getFieldName1())){
                        bolIsBankChecker=true;
                    }
                    strUserBranchId=objLstCurBankUserRole.get(0).getFieldName2();
                }
    %>
    <body onload="fillGridOnEvent('<%=strUserId%>','','','','','0','0','','','All');">
       
      <!--Dashboard Content Part Start-->
<!--      <form id="frmData">-->
        <div class="formBg_1 t_space">
            <input type="hidden" name="userId" id="userId" value="<%=strUserId%>"></input>
            <table cellspacing="10" class="formStyle_1" width="100%">
                <tr>
                    <td width="20%" class="ff">Tender ID :</td>
                    <td width="30%"><input name="txtTenderId" type="text" class="formTxtBox_1" id="txtTenderId" style="width:195px;" /></td>
                    <td width="20%" class="ff">Bidder / Consultant :</td>
                    <td width="30%" ><input name="txtTendererNm" type="text" class="formTxtBox_1" id="txtTendererNm" style="width:195px;" /></td>
                </tr>
            <tr>
              <td width="14%" class="ff">e-mail ID :</td>
              <td width="44%"><input name="txtSearchEmailId" type="text" class="formTxtBox_1" id="txtSearchEmailId" style="width:195px;" /></td>
              <td width="13%" class="ff">Verification Status :</td>
              <td width="29%">
                  <select name="comboStatus" class="formTxtBox_1" id="comboStatus" style="width:75px;">
                      <option value='' selected="selected">Select</option>
                      <option value='yes' >Verified</option>
                      <option value='no'>Pending</option>
                  </select>
              </td>
              </tr>
            <tr>
              <td class="ff">Payment Date From : </td>
              <td>
                  <input onfocus="GetCal('txtFromDt','txtFromDt');" name="txtFromDt" type="text" class="formTxtBox_1" id="txtFromDt" style="width:100px;" readonly="readonly" />
                  <img id="imgFromDt" name="imgFromDt" onclick="GetCal('txtFromDt','imgFromDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                  <span id="SpError1" class="reqF_1"></span>
              </td>
              <td class="ff">Payment Date To : </td>
              <td>
               <input onfocus="GetCal('txtToDt','txtToDt');" name="txtToDt" type="text" class="formTxtBox_1" id="txtToDt" style="width:100px;" readonly="readonly" />
                  <img id="imgToDt" name="imgToDt" onclick="GetCal('txtToDt','imgToDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                  <span id="SpError" class="reqF_1"></span>
              </td>
              </tr>
            <tr>
                <%if(bolIsBankChecker){%>
                    <td class="ff">Branch  Name : </td>
                    <td><select name="comboBranches" class="formTxtBox_1" id="comboBranches" style="width:155px;">
                            <option value='' selected="selected">- Select Branch Name -</option>
                            <%
                            int branchCnt=0;
                            for (SPTenderCommonData sptcd : objTSCDaily.returndata("getBankBranchOrMemberList", strUserId, "getBranchList")) {
                                branchCnt++;
                            %>
                            <option value="<%=sptcd.getFieldName1()%>" ><%=sptcd.getFieldName2()%></option>
                            <%}%>
                        </select>
                    </td>

                    <td value='' class="ff">Branch  Maker : </td>
                    <td>
                        <select name="comboBranchMembers" class="formTxtBox_1" id="comboBranchMembers" style="width:155px;">
                            <option value=''>- Select Branch Maker -</option>
                        </select>
                    </td>
                <%} else if(bolIsBranchChecker){%>
                    <td class="ff">Branch  Maker : </td>
                    <td>
                        <select name="comboBranchMembers" class="formTxtBox_1" id="comboBranchMembers" style="width:155px;">
                            <option value='' selected="selected">- Select Branch Maker -</option>
                            <%
                            int branchCnt=0;
                            for (SPTenderCommonData sptcd : objTSCDaily.returndata("getBankBranchOrMemberList", strUserBranchId, "getBranchMemberList")) {
                                branchCnt++;
                            %>
                            <option value="<%=sptcd.getFieldName1()%>" ><%=sptcd.getFieldName2()%></option>
                            <%}%>
                        </select>
                    </td>

                        <td class="ff">Payment For : </td>
                    <td >
                        <select name="comboPaymentFor" class="formTxtBox_1" id="comboPaymentFor" style="width:155px;">
                            <option value='All' selected="selected">All</option>
                            <option value='Document Fees'>Document Fees</option>
                            <option value='Bid Security'>Bid Security</option>
                            <option value='Performance Security'>Performance Security</option>
                        </select>
                        <select name="comboBranches" class="formTxtBox_1" id="comboBranches" style="width:155px; visibility: hidden">
                            <option value='' selected="selected">- Select Branch Name -</option>
                        </select>
                    </td>
                <%} else if(bolIsBranchMaker){%>
                     <td class="ff">Payment For : </td>
                    <td >
                        <select name="comboPaymentFor" class="formTxtBox_1" id="comboPaymentFor" style="width:155px;">
                            <option value='All' selected="selected">All</option>
                            <option value='Document Fees'>Document Fees</option>
                            <option value='Bid Security'>Bid Security</option>
                            <option value='Performance Security'>Performance Security</option>
                        </select>
                        <select name="comboBranches" class="formTxtBox_1" id="comboBranches" style="width:155px; visibility: hidden">
                            <option value='' selected="selected">- Select Branch Name -</option>
                        </select>
                    </td>
                     <td></td>
                 <td></td>
                <%}%>
            </tr>
             <%if(bolIsBankChecker){%>
             <tr>
                 <td class="ff">Payment For : </td>
                 <td>
                     <select name="comboPaymentFor" class="formTxtBox_1" id="comboPaymentFor" style="width:155px;">
                         <option value='All' selected="selected">All</option>
                         <option value='Document Fees'>Document Fees</option>
                         <option value='Bid Security'>Bid Security</option>
                         <option value='Performance Security'>Performance Security</option>
                     </select>
                 </td>
                 <td></td>
                 <td></td>
             </tr>
             <%}%>
            <tr>
              <td colspan="4" class="t-align-center ff"><label class="formBtn_1">
                <input type="button" name="btnSearchData" id="btnSearchData" value="Search" />
              </label>

                <label class="formBtn_1 l_space">
<!--                <input type="submit" name="btnReset" id="btnReset" value="Reset" />-->
                    <input type="button" name="btnReset" id="btnReset" value="Reset" />
              </label>
              </td>
              </tr>
            </table>
</div>
<!--</form>-->

<div class="tabPanelArea_1 t_space">
    <div id="jQGrid" align="center">
        <table id="list"></table>
        <div id="page"></div>
    </div>
</div>

  <!--Dashboard Content Part End--> 

</body>
</html>
