<%-- 
    Document   : RegistrationFeePayment
    Created on : Jan 22, 2011, 12:27:48 AM
    Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import="java.lang.String" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            <%if(request.getParameter("action")!=null){%>
            <%if("renew".equalsIgnoreCase(request.getParameter("action"))){%>
                Renewal Fee Payment
                <%}%>
            <%} else {%>
            Registration Fee Payment
            <%}%>

        </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript" >
            $(function(){

                $('#btnSubmit').click(function(){
                
                    var flag=true;
                        
                    if($('#cmbCurrency').val()==''){
                        $('#SpError1').html('Please select the Currency.');
                        flag=false;
                    } else {
                        $('#SpError1').html('');
                    }

                    if($('#cmbPayment').val()==''){
                        $('#SpError2').html('Please select the Mode of Payment.');
                        flag=false;
                    } else {
                        $('#SpError2').html('');
                            
                        if($('#cmbPayment').val()=='Pay Order'){

                            if($.trim($('#txtInsRefNo').val())==''){
                                $('#SpError3').html('Please enter Instrument No.');
                                flag=false;
                            } else {$('#SpError3').html('');}

                            if($.trim($('#txtIssuanceBankNm').val())==''){                                
                                $('#SpError4').html('Please enter Issuing Financial Institution.');
                                flag=false;
                            } else {                             
                                if(!chkSpecial($.trim($('#txtIssuanceBankNm').val())))
                                {
                                    $('#SpError4').html('Please enter a valid Financial Institution Name.');
                                    flag=false;

                                }
                                else
                                {
                                    $('#SpError4').html('');
                                }
                            }
                            //$('#SpError4').html('');}

                            if($.trim($('#txtIssuanceBranch').val())==''){
                                $('#SpError5').html('Please enter Issuing Financial Institution Branch.');
                                flag=false;
                            } else 
                            {
                                if(!chkSpecial($.trim($('#txtIssuanceBranch').val())))
                                {
                                    $('#SpError5').html('Please enter a valid Branch Name.');
                                    flag=false;

                                }
                                else
                                {
                                    $('#SpError5').html('');
                                }
                                
                                //$('#SpError5').html('');
                            }

                            if($.trim($('#txtIssuanceDt').val())==''){
                                $('#SpError6').html('Please select Issuance Date.');
                                flag=false;
                            } else {
                                var d =new Date();
                                var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());

                                var value=document.getElementById("txtIssuanceDt").value;
                                var mdy = value.split('/')  //Date and month split
                                var mdyhr= mdy[2].split(' ');  //Year and time split
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);


                                if(Date.parse(valuedate) > Date.parse(todaydate)){
                                    $('#SpError6').html('Issuance Date must be less than  or equal to the current date.');
                                    flag=false;
                                }else {
                                    
                                    $('#SpError6').html('');
                                }
                            }

                            if($.trim($('#txtValidityDt').val())==''){
                                $('#SpError8').html('Please select Validity Date.');
                                flag=false;
                            } else {
                                var d =new Date();
                                var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());

                                var value=document.getElementById("txtValidityDt").value;
                                var mdy = value.split('/')  //Date and month split
                                var mdyhr= mdy[2].split(' ');  //Year and time split
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);

                                if(Date.parse(valuedate) < Date.parse(todaydate)){
                                    $('#SpError8').html('Validity Date must be more than or equal to the current date.');
                                    flag=false;
                                }else {$('#SpError8').html('');}
                            }

                               
                           
                        } else if($('#cmbPayment').val()=='Account to Account Transfer'){

                            if($.trim($('#txtInsRefNo').val())==''){
                                $('#SpError3').html('Please enter Beneficiary Account No.');
                                flag=false;
                            } else {$('#SpError3').html('');}

                            if($.trim($('#txtIssuanceBranch').val())!=''){
                               if(!chkSpecial($.trim($('#txtIssuanceBranch').val())))                                {
                                    //$('#SpError5').html('Only special characters not allowed.');
                                    $('#SpError5').html('Please enter a valid Branch Name.');
                                    flag=false;
                                }
                                else
                                {
                                    $('#SpError5').html('');
                                }
                               
                            }

                            
                        }
                    }


                    if($('#cmbValidityPeriod').val()==''){
                        $('#SpError9').html('Please select Validity Period.');
                        flag=false;
                    } else {
                        $('#SpError9').html('');
                    }

                    if($.trim($('#txtComments').val())==''){
                        $('#SpError7').html('Please enter Remarks.');
                        flag=false;
                    }
                    else {
                        if($.trim($('#txtComments')).length<=1000) {
                            $('#SpError7').html('');
                        }
                        else{
                            $('#SpError7').html('Maximum 1000 characters are allowed.');
                            flag=false;
                        }
                    }
                        
                   
                    if(flag==false){
                        return false;
                    } else {
                        document.getElementById("btnSubmit").style.display = "none";
                    }
                    
                });
            });
            
            function chkSpecial(value)
            {
                //Allow only characters
                //Characters + numbers
                //Characters + numbers +few special
                //Don’t allow only numbers                                    
                //return /^([a-zA-Z 0-9]([a-zA-Z 0-9\&\.\(\)\-])?)+$/.test(value);
                return /^[a-zA-Z 0-9](?![0-9]+$)[a-zA-Z 0-9\&\.\-\(\)/]+$/.test(value);
            }


        </script>


        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }


            function GetCal2(txtname,controlname,optionalCtrl)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());

                        if(optionalCtrl!=null){
                            if(optionalCtrl!='' && optionalCtrl !='null')   {
                                //alert(date.getMonth());

                                //var value=document.getElementById(txtname).value;
                                //alert(value);
                                //var mdy = value.split('/')  //Date and month split
                                //var mdyhr= mdy[2].split(' ');  //Year and time split
                                //var valuedate= new Date(mdyhr[0], mdy[1]+7, mdy[0]);

                                var currdate = document.getElementById(txtname).value;
                                var temp=currdate.split('/');
                                //alert(temp[0]);
                                //alert(temp[1]);
                                //alert(temp[2]);
                                var newtemp=temp[1]+'/'+temp[0]+'/'+temp[2];
                                //alert(new Date(newtemp));
                                var today =new Date(newtemp);
                                //alert(today);
                                //alert(today.getYear());
                                //alert(today.getMonth());
                                //today.setMonth(today.getMonth()+7)
                                today.setMonth(today.getMonth()+6)
                                //// Note: getMonth() funcation returns 1 month less than the actual value
                                // So to add 6 months we have to use +7
                                //alert(today);

                                var dtVal = today.getDate();
                                if(dtVal<10){
                                    dtVal="0" + dtVal.toString();
                                }
                               
                               // var mnthVal = today.getMonth();
                               // Note: getMonth() funcation returns 1 month less than the actual value
                                var mnthVal = today.getMonth() + 1;
                                if(mnthVal<10){
                                    mnthVal="0" + mnthVal.toString();
                                }

                                var yrVal = today.getYear();
                                if(yrVal < 1900){
                                    yrVal= yrVal+ 1900;
                                }
                                valuedate=dtVal+"/"+mnthVal+"/"+yrVal;
                               
                                //valuedate =new Date(valuedate);
                              
                                document.getElementById(optionalCtrl).value=valuedate;
                            }
                        }



                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

        </script>

        <script type="text/javascript">
            $(document).ready(function(){
                //alert($('#hdnAction').val());

                var action=$('#hdnAction').val();

                //if(action.toLowerCase()=="edit"){
                //    BindPaymentData();
                //}

                function BindPaymentData(){
                    //alert("Binding data")
                    //alert($('#hdnEditCurrency').val());
                    $('#cmbCurrency').val($('#hdnEditCurrency').val());

                    var curVal=$('#cmbCurrency').val();

                    if (curVal == "BTN"){
                        $('#lblCurrencySymbol').html('Nu.');
                    } else if (curVal == "USD"){
                        $('#lblCurrencySymbol').html('$');
                    }

                    var amt = $('#hdnEditAmount').val();
                    $('#lblAmount').html(amt);
                    $('#hdnFinalAmount').val(amt);

                    $('#cmbPayment').val($('#hdnEditPaymentType').val());

                    //alert($('#hdnEditValidityRefId').val());
                    $('#cmbValidityPeriod').val($('#hdnEditValidityRefId').val());
                    //alert($('#cmbValidityPeriod').val());

                    var payType= $('#cmbPayment').val();
                    //alert(payType);
                    $('#trInsRefNo').show();
                    $('#trIssuanceBankNm').show();
                    $('#trIssuanceBranch').show();
                    $('#trIssuanceDt').show();
                    $('#trInsValidity').show();
                    clear();
                    if (payType == "Cash"){
                        $('#trInsRefNo').hide();
                        $('#trIssuanceBankNm').hide();
                        $('#trIssuanceBranch').hide();
                        $('#trIssuanceDt').hide();
                        $('#trInsValidity').hide();

                    } else if (payType == "Pay Order"){
                        $('#lblInstrumentNo').html('Instrument No.');
                        $('#lblBankBranch').html('Issuing Financial Institution Branch');
                        $('#spnBranchStar').html('*');

                        // Bind Data
                        $('#txtInsRefNo').val($('#hdnEditInsNo').val());
                        $('#txtIssuanceBankNm').val($('#hdnEditIssuingBank').val());
                        $('#txtIssuanceBranch').val($('#hdnEditIssuingBranch').val());
                        $('#txtIssuanceDt').val($('#hdnEditIssuingDt').val());
                        $('#txtValidityDt').val($('#hdnEditValidityDt').val());

                    }
                    else if (payType == "Account to Account Transfer"){
                        $('#trIssuanceBankNm').hide();
                        $('#trIssuanceDt').hide();
                        $('#trInsValidity').hide();

                        $('#lblInstrumentNo').html('Beneficiary Account No.');
                        $('#lblBankBranch').html('Branch Name');
                        $('#spnBranchStar').html('');

                        // Bind Data
                        $('#txtInsRefNo').val($('#hdnEditInsNo').val());
                        $('#txtIssuanceBranch').val($('#hdnEditIssuingBranch').val());

                    }

                    // Bind Date of Payment and Comments
                    $('#lblDtOfPayment').html($('#hdnEditDisplayedDateOfPayment').val());
                    $('#hdnDateOfPayment').val($('#hdnEditDateOfPayment').val());
                    $('#txtComments').val($('#hdnEditComments').val());
                }

                var payType= $('#cmbPayment').val();
                  
                if (payType == "Cash"){
                    $('#trInsRefNo').hide();
                    $('#trIssuanceBankNm').hide();
                    $('#trIssuanceBranch').hide();
                    $('#trIssuanceDt').hide();
                    $('#trInsValidity').hide();
                }

                $('#cmbValidityPeriod').val('1');
                var curVal=$('#cmbCurrency').val();


                if (curVal == "BTN"){
                    $('#lblCurrencySymbol').html('Nu.');
                    //GetPaymentAmt();

                } else if (curVal == "USD"){
                    $('#lblCurrencySymbol').html('$');
                    //GetPaymentAmt();
                }
               
                $('#cmbValidityPeriod').change(function(){
                    if($('#cmbValidityPeriod').val()!=''){
                        GetPaymentAmt();
                    }
                });

                $('#cmbCurrency').change(function(){
                    var curVal=$('#cmbCurrency').val();

                    if (curVal == "BTN"){
                        $('#lblCurrencySymbol').html('Nu.');
                        GetPaymentAmt();
                    } else if (curVal == "USD"){
                        $('#lblCurrencySymbol').html('$');
                        GetPaymentAmt();
                    }
                });

                $('#cmbPayment').change(function(){
                    //alert($('#cmbPayment').val());
                   
                    var payType= $('#cmbPayment').val();
                    //alert(payType);
                    $('#trInsRefNo').show();
                    $('#trIssuanceBankNm').show();
                    $('#trIssuanceBranch').show();
                    $('#trIssuanceDt').show();
                    $('#trInsValidity').show();
                    clear();
                    if (payType == "Cash" || payType==''){
                        $('#trInsRefNo').hide();
                        $('#trIssuanceBankNm').hide();
                        $('#trIssuanceBranch').hide();
                        $('#trIssuanceDt').hide();
                        $('#trInsValidity').hide();

                    } else if (payType == "Pay Order"){
                        $('#lblInstrumentNo').html('Instrument No.');
                        $('#lblBankBranch').html('Issuing Financial Institution Branch');
                        $('#spnBranchStar').html('*');

                    }
                    else if (payType == "Account to Account Transfer"){
                        $('#trIssuanceBankNm').hide();
                        $('#trIssuanceDt').hide();
                        $('#trInsValidity').hide();
                         
                        $('#lblInstrumentNo').html('Beneficiary Account No.');
                        $('#lblBankBranch').html('Branch Name');
                        $('#spnBranchStar').html('');
                    }
                });


                function GetPaymentAmt(){
                    //alert('Getting Payment Amount');
                    var payCase;
                    var payCurrency;
                    var validityId;
                    var userId;

                    if($('#hdnAction').val()=="renew" || $('#hdnEditPayStatus').val()=="renewed"){
                        payCase="renewal";
                    } else {
                        payCase="firstpay";
                    }

                    payCurrency=$('#cmbCurrency').val();
                    validityId=$('#cmbValidityPeriod').val();
                    userId = $('#hdnPayUserId').val();

                    //alert(payCase);
                    //alert(payCurrency);
                    //alert(validityId);
                    //alert(userId);

                    $.post("<%=request.getContextPath()%>/VerifyPaymentServlet", {action: 'getRegFeePaymentAmt', payCase: payCase, payCurrency: payCurrency, validityId: validityId, userId: userId  },  function(j){
                        //alert(j);
                        document.getElementById("lblAmount").innerHTML=j.toString();
                        document.getElementById("hdnFinalAmount").value=j.toString();

                        //alert(document.getElementById("lblAmount").innerHTML);
                    });
                }

                function clear(){
                    //alert('Clearing...');
                    // Clear Error Messages

                    $('#txtInsRefNo').val('');
                    $('#txtIssuanceBankNm').val('');
                    $('#txtIssuanceBranch').val('');
                    $('#txtIssuanceDt').val('');
                    $('#txtValidityDt').val('');
                    $('#txtComments').val('');

                    $('#SpError1').html('');
                    $('#SpError2').html('');
                    $('#SpError3').html('');
                    $('#SpError4').html('');
                    $('#SpError5').html('');
                    $('#SpError6').html('');
                    $('#SpError7').html('');
                    $('#SpError8').html('');
                }

                if(document.getElementById("txtInsRefNo")!=null){
                    $('#txtInsRefNo').blur(function(){
                        if($.trim($('#txtInsRefNo').val())==''){
                            if($('#cmbPayment').val()=='Pay Order'){
                                $('#SpError3').html('Please enter Instrument No.');
                            } else if($('#cmbPayment').val()=='Account to Account Transfer'){
                                $('#SpError3').html('Please enter Beneficiary Account No.');
                            }

                            flag=false;
                        } else {$('#SpError3').html('');}

                        if(flag==false){return false;}
                    });
                }

                if(document.getElementById("txtIssuanceBankNm")!=null){
                    $('#txtIssuanceBankNm').blur(function(){
                        if($.trim($('#txtIssuanceBankNm').val())==''){
                            $('#SpError4').html('Please enter Issuing Financial Institution.');
                            flag=false;
                        } else {
                            //$('#SpError4').html('');
                             if($.trim($('#txtIssuanceBankNm').val())!=''){
                             if(!chkSpecial($.trim($('#txtIssuanceBankNm').val()))){
                                    $('#SpError4').html('Please enter a valid Financial Institution Name.');
                                    flag=false;
                                }else {
                                    $('#SpError4').html('');
                                }
                           }

                    }
        
                    });

                }

                if(document.getElementById("txtIssuanceBranch")!=null){
                    $('#txtIssuanceBranch').blur(function(){
                        if($.trim($('#txtIssuanceBranch').val())==''){
                            if($('#cmbPayment').val()=='Pay Order'){
                                $('#SpError5').html('Please enter Issuing Financial Institution Branch.');
                            }

                            flag=false;
                        } else {
                            //$('#SpError5').html('');
                             if($.trim($('#txtIssuanceBranch').val())!=''){
                               if(!chkSpecial($.trim($('#txtIssuanceBranch').val())))                                {
                                    //$('#SpError5').html('Only special characters not allowed.');
                                    $('#SpError5').html('Please enter a valid Branch Name.');
                                    flag=false;
                                }
                                else
                                {
                                    $('#SpError5').html('');
                                }
                            }
                        }
            
                    });
         
                }

                if(document.getElementById("txtIssuanceDt")!=null){
                    $('#txtIssuanceDt').blur(function(){
                        if($.trim($('#txtIssuanceDt').val())==''){
                            $('#SpError6').html('Please select Issuance Date.');
                            flag=false;
                        } else {
                            var d =new Date();
                            var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());

                            var value=document.getElementById("txtIssuanceDt").value;
                            var mdy = value.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);


                            if(Date.parse(valuedate) > Date.parse(todaydate)){
                                $('#SpError6').html('Issuance Date must be less than  or equal to the current date.');
                                flag=false;
                            }else {

                                $('#SpError6').html('');
                            }
                        }
            
                    });
                }

                if(document.getElementById("txtValidityDt")!=null){
                    $('#txtValidityDt').blur(function(){
                        if($.trim($('#txtValidityDt').val())==''){
                            $('#SpError8').html('Please select Validity Date.');
                            flag=false;
                        } else {
                            var d =new Date();
                            var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());

                            var value=document.getElementById("txtValidityDt").value;
                            var mdy = value.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);

                            if(Date.parse(valuedate) < Date.parse(todaydate)){
                                $('#SpError8').html('Validity Date must be more than or equal to the current date.');
                                flag=false;
                            }else {$('#SpError8').html('');}
                        }
            
                    });
                }

                $('#txtComments').blur(function(){
                    if($.trim($('#txtComments').val())==''){
                        $('#SpError7').html('Please enter Remarks.');
                        flag=false;
                    }
                    else {
                        if($.trim($('#txtComments')).length<=1000) {
                            $('#SpError7').html('');
                        }
                        else{
                            $('#SpError7').html('Maximum 1000 characters are allowed.');
                            flag=false;
                        }
                    }
                });

                //alert(action);
                if(action.toLowerCase()=="edit"){
                    BindPaymentData();
                    GetPaymentAmt();
                    if($('#hdnEditPayStatus').val()=='paid'){
                        $('#trValidityPeriod').hide(); // Hide the Validity Period Combo for 1st Time Payment
                    }
                } else {                    
                    GetPaymentAmt();
                    if(action.toLowerCase()!="renew"){
                        $('#trValidityPeriod').hide(); // Hide the Validity Period Combo for 1st Time Payment
                    }
                }

            });
        </script>
    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <%      
        
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    if (session.getAttribute("userId") != null) {
                        tenderCommonService.setLogUserId(session.getAttribute("userId").toString());
                    }
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
                    SimpleDateFormat formatVisible = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat formatVisibleWithMonthNm = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                    boolean isBankUser = false, isRenew = false, isEditCase = false, isChecker=false;
                    String emailId = "", payUserId = "", payDt = format.format(new Date()), payDtWithMnthNm = formatVisibleWithMonthNm.format(new Date());;
                    String strPartTransId="0", userId = "", action = "", editPaymentId = "0";

                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userId = hs.getAttribute("userId").toString();
                    } else {
                        response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
                    }
                    //payDt=payDt.replace("-", "/");
                    //String amount= XMLReader.getMessage("localTenderRegFee");
                    //String amountDollar=XMLReader.getMessage("intTednerRegFee");

                    if (request.getParameter("uId") != null) {
                        if (request.getParameter("uId") != "" && !request.getParameter("uId").equalsIgnoreCase("null")) {
                            payUserId = request.getParameter("uId");
                        }
                    }

                    if (request.getParameter("action") != null) {
                        if (request.getParameter("action") != "" && !request.getParameter("action").equalsIgnoreCase("null")) {
                            action = request.getParameter("action");

                            if ("edit".equalsIgnoreCase(action)) {
                                if (request.getParameter("payId") != null) {
                                    if (request.getParameter("payId") != "0" && request.getParameter("payId") != "" && !request.getParameter("payId").equalsIgnoreCase("null")) {
                                        editPaymentId = request.getParameter("payId");
                                        isEditCase = true;
                                    }
                                }
                            } else if ("renew".equalsIgnoreCase(action)) {
                                //amount= XMLReader.getMessage("localTenderRenewFee");
                                //amountDollar=XMLReader.getMessage("intTednerRenewFee");
                            }
                        }
                    }

                    

                    if (hs.getAttribute("govUserId") != null) {
                     strPartTransId = hs.getAttribute("govUserId").toString();
                    }

                    if (hs.getAttribute("userTypeId") != null) {
                        if ("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())) {
                            isBankUser = true; // userType is ScheduleBank";
                        }
                    }
                    
                    String bidderEmail = "", registeredDt = "";

                    List<SPTenderCommonData> lstTendererEml = tenderCommonService.returndata("getEmailIdfromUserId", payUserId, null);

                    if (!lstTendererEml.isEmpty()) {
                        emailId = lstTendererEml.get(0).getFieldName1();
                        registeredDt = lstTendererEml.get(0).getFieldName2();
                        bidderEmail = emailId;
                    }

                     if (isEditCase) {
                        List<SPTenderCommonData> lstBankInfo =
                                tenderCommonService.returndata("getBankInfoForRegPayment", strPartTransId, editPaymentId);
                                //tenderCommonService.returndata("getBankInfo", strPartTransId, editPaymentId);

                        if (!lstBankInfo.isEmpty()) {
                            if ("checker".equalsIgnoreCase(lstBankInfo.get(0).getFieldName4())) {
                                isChecker = true;
                            }
                        }
                        lstBankInfo = null;
                    }
                   String ParentBranchId = "";

        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">
                    <%if (request.getParameter("action") != null) {%>
                    <%if ("renew".equalsIgnoreCase(request.getParameter("action"))) {%>
                Renewal Fee Payment
                    <%} else {%>
                    Edit Payment Details
                <%}%>
            <%} else {%>
            Registration Fee Payment
            <%}%>
                    <span style="float: right;">
                        <%if(isChecker){%>
                            <a href="VerifyPayment.jsp?uId=<%=payUserId%>" class="action-button-goback">
                            Go Back</a>
                        <%} else {%>
                            <a href="SearchTendererForRegistration.jsp?uId=<%=payUserId%>" class="action-button-goback">
                            Go Back</a>
                        <%}%>
                        
                    </span>
                </div>

                <%if (request.getParameter("msgId") != null) {
                                String msgId = "", msgTxt = "";
                                boolean isError = false;
                                msgId = request.getParameter("msgId");
                                if (!msgId.equalsIgnoreCase("")) {
                                    if (msgId.equalsIgnoreCase("error")) {
                                        isError = true;
                                        msgTxt = "There was some error.";
                                    }else if (msgId.equalsIgnoreCase("amterror")) {
                                        isError = true;
                                        msgTxt = "Registration Fee Amount is not proper. Please try again.";
                                    } else {
                                        msgTxt = "";
                                    }
                %>
                <%if (isError) {%>
                <div class="responseMsg errorMsg"><%=msgTxt%></div>
                <%} else {%>
                <div class="responseMsg successMsg"><%=msgTxt%></div>
                <%}%>
                <%}
                            }%>

                <div class="tabPanelArea_1 t_space">
                    <form id="frmMakePayment" action="RegistrationFeePaymentInsert.jsp?uId=<%=payUserId%>&action=<%=action%>&payId=<%=editPaymentId%>" method="POST">
                        <input type="hidden" name="freezed" value="<%=request.getParameter("freezed")%>" />
                        <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                            <tr>
                                <td width="18%" class="ff">e-mail ID :</td>
                                <td width="82%"><%=bidderEmail%></td>
                            </tr>
                            <tr>
                                <td class="ff">Date of Registration  :</td>
                                <td><%=registeredDt%></td>
                            </tr>

                            <% 
                            
                            if (isBankUser) {
                                            //List<SPTenderCommonData> lstBankInfo = tenderCommonService.returndata("getBankInfo", userId, null);
                                List<SPTenderCommonData> lstBankInfo =
                                         tenderCommonService.returndata("getBankInfoForRegPayment", strPartTransId, editPaymentId);
                                       // tenderCommonService.returndata("getBankInfo", strPartTransId, editPaymentId);
                                        ParentBranchId = lstBankInfo.get(0).getFieldName7();
                                            if (!lstBankInfo.isEmpty() && lstBankInfo.size() > 0) {
                            %>
                            <tr id="trBankNm">
                                <td class="ff">Financial Institution Name : 
                                <td>
                                    <%=lstBankInfo.get(0).getFieldName1()%>
                                    <input type="hidden" value="<%=lstBankInfo.get(0).getFieldName1()%>" id="hdnBankNm" name="hdnBankNm">                                    
                                </td>

                            </tr>
                            <tr id="trBranch">
                                <td class="ff">Branch Name : 
                                <td>
                                    <%=lstBankInfo.get(0).getFieldName2()%>
                                    <input type="hidden" value="<%=lstBankInfo.get(0).getFieldName2()%>" id="hdnBranch" name="hdnBranch">
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Branch Maker :</td>
                                <td><%=lstBankInfo.get(0).getFieldName3()%></td>
                            </tr>
                            <%}
                                        }%>


                            <tr>
                                <td class="ff">Payment For : </td>
                                <td>Registration Fee</td>
                            </tr>
                            <tr id="trValidityPeriod">
                                <td class="ff" width="17%">Validity Period : <span class="mandatory">*</span></td>
                                <td width="83%">
                                    <select name="cmbValidityPeriod" class="formTxtBox_1" id="cmbValidityPeriod">
                                        <!--                                        <option value="">Select</option>-->
                                        <%for (SPTenderCommonData sptcd : tenderCommonService.returndata("getRegistrationValidityInfo", null, null)) {%>
                                        <option value="<%=sptcd.getFieldName1()%>"><%=sptcd.getFieldName2()%></option>
                                        <%}%>
                                    </select>
                                    <span id="SpError9" class="reqF_1"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="17%">Currency : <span class="mandatory">*</span></td>
                                <td width="83%">
                                    <select name="cmbCurrency" class="formTxtBox_1" id="cmbCurrency" width="100px;">
                                        <!--                                        <option value="">Select</option>-->
                                        <option value="BTN" selected="selected">Nu.</option>
                                        <option value="USD">USD</option>
                                    </select>
                                    <span id="SpError1" class="reqF_1"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Amount :</td>
                                <td><label id="lblCurrencySymbol"></label>&nbsp;
                                    <label id="lblAmount"></label>
                                    <!--                                    <input type="hidden" id="hdnAmount" name="hdnAmount" value="">
                                                                        <input type="hidden" id="hdnDollarAmount" name="hdnDollarAmount" value="">-->
                                </td>
                            </tr>

                            <tr>
                                <td class="ff" width="17%">Mode of Payment : <span class="mandatory">*</span></td>
                                <td width="83%">
                                    <select name="cmbPayment" class="formTxtBox_1" id="cmbPayment">
                                        <option value="">Select</option>
                                        <option value="Cash" selected="selected">Cash</option>                                        
<!--                                        <option value="Pay Order">Pay Order</option>-->
                                        <option value="Account to Account Transfer">Account to Account Transfer</option>
                                    </select>
                                    <span id="SpError2" class="reqF_1"></span>
                                </td>
                            </tr>


                            <tr id="trInsRefNo">
                                <td class="ff">
                                    <label id="lblInstrumentNo"></label>
                                    <span class="mandatory">*</span></td>
                                <td><input name="txtInsRefNo" type="text" class="formTxtBox_1" id="txtInsRefNo" style="width:200px;" />
                                    <span id="SpError3" class="reqF_1"></span>
                                </td>
                            </tr>

                            <tr id="trIssuanceBankNm">
                                <td class="ff">Issuing Financial Institution : <span class="mandatory">*</span></td>
                                <td><input name="txtIssuanceBankNm" type="text" class="formTxtBox_1" id="txtIssuanceBankNm" style="width:200px;"/>
                                    <span id="SpError4" class="reqF_1"></span>
                                </td>

                            </tr>
                            
                            <%    
                                 List<SPTenderCommonData> BranchList = tenderCommonService.returndata("getBankBranchList", ParentBranchId, "");
                            %>
                            
                            <tr id="trIssuanceBranch">
                                <td class="ff">
                                    <label id="lblBankBranch"></label> :
                                    <span id="spnBranchStar" class="mandatory"></span></td>
                                <td>
                                    <select name="txtIssuanceBranch" class="formTxtBox_1" id="txtIssuanceBranch" style="width:200px;">
                                                <option value="" >-- Select Branch --</option>
                                            <%
                                                for(SPTenderCommonData BranchInfo : BranchList)
                                                {
                                            %>
                                                <option value="<%=BranchInfo.getFieldName1()%>"><%=BranchInfo.getFieldName1() %></option>
                                            <%
                                                }
                                            %>
                                    </select>
                                    <!--<input name="txtIssuanceBranch" type="text" class="formTxtBox_1" id="txtIssuanceBranch" style="width:200px;" />-->
                                    <span id="SpError5" class="reqF_1"></span>
                                </td>
                            </tr>

                            <tr id="trIssuanceDt">
                                <td class="ff">Issuance Date : <span class="mandatory">*</span></td>
                                <td><input onfocus="GetCal2('txtIssuanceDt','txtIssuanceDt','txtValidityDt');" name="txtIssuanceDt" type="text" class="formTxtBox_1" id="txtIssuanceDt" style="width:100px;" readonly="readonly" />
                                    <img id="imgIssuanceDt" name="imgIssuanceDt" onclick="GetCal2('txtIssuanceDt','imgIssuanceDt','txtValidityDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                                    <span id="SpError6" class="reqF_1"></span>
                                </td>
                            </tr>

                            <tr id="trInsValidity">
                                <td class="ff">Validity Date : <span class="mandatory">*</span></td>
                                <td><input onfocus="GetCal('txtValidityDt','txtValidityDt');" name="txtValidityDt" type="text" class="formTxtBox_1" id="txtValidityDt" style="width:100px;" readonly="readonly" />                                    
                                    <img id="imgValidityDt" name="imgValidityDt" onclick="GetCal('txtValidityDt','imgValidityDt');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                                    <span id="SpError8" class="reqF_1"></span>
                                </td>
                            </tr>

                            <tr id="trDtOfPayment">
                                <td class="ff">Date and Time of Payment : </td>
                                <td>
<%//=payDt%>
<%//=payDtWithMnthNm%>
                                    <label id="lblDtOfPayment"><%=payDtWithMnthNm%></label>
                                    <input type="hidden" id="hdnDateOfPayment" name="hdnDateOfPayment" value="<%=payDt%>">
                                </td>
                            </tr>

                            <tr>
                                <td class="ff">Remarks : <span class="mandatory">*</span></td>
                                <td><textarea cols="100" rows="5" id="txtComments" name="txtComments" class="formTxtBox_1"></textarea>
                                    <span id="SpError7" class="reqF_1"></span>

                                </td>
                            </tr>

                            <tr>
                                <td class="ff">&nbsp;</td>
                                <td>
                                    <div class="t-align-left t_space">
                                        <label class="formBtn_1"><input name="btnSubmit" id="btnSubmit" type="submit" value="Submit" /></label>
                                        <input type="hidden" id="hdnUserType" name="hdnUserType"  value="BankUser">
                                    </div>
                                </td>
                            </tr>
                        </table>




                        <%
                                    String editCurrency = "";
                                    String editAmount = "";
                                    String editPaymentType = "";
                                    String editInsNo = "";
                                    String editIssuingBank = "";
                                    String editIssuingBranch = "";
                                    String editIssuingDt = "";
                                    String editValidityDt = "";
                                    String editDisplayedDateOfPayment = "";
                                    String editDateOfPayment = "";
                                    String editComments = "";
                                    String isVerifiedChk = "";
                                    String editPayStatus = "";
                                    String editValidityRefId = "";
                                    if (isEditCase) {

                                        List<SPTenderCommonData> lstPaymentDetailForEdit = tenderCommonService.returndata("getRegistrationFeePaymentDetailForEdit", editPaymentId, null);

                                        if (!lstPaymentDetailForEdit.isEmpty()) {
                                            editCurrency = lstPaymentDetailForEdit.get(0).getFieldName1();
                                            editAmount = lstPaymentDetailForEdit.get(0).getFieldName2();
                                            editPaymentType = lstPaymentDetailForEdit.get(0).getFieldName3();
                                            editDisplayedDateOfPayment = lstPaymentDetailForEdit.get(0).getFieldName4();
                                            editComments = lstPaymentDetailForEdit.get(0).getFieldName5();
                                            editDateOfPayment = lstPaymentDetailForEdit.get(0).getFieldName6();
                                            isVerifiedChk = lstPaymentDetailForEdit.get(0).getFieldName7();
                                            editPayStatus = lstPaymentDetailForEdit.get(0).getFieldName8();
                                            editValidityRefId = lstPaymentDetailForEdit.get(0).getFieldName9();
                                        }

                                        if ("Pay Order".equalsIgnoreCase(editPaymentType)) {
                                            List<SPTenderCommonData> lstPaymentDetailMoreForEdit = tenderCommonService.returndata("getRegistrationFeePaymentDetailMoreForEdit", editPaymentId, null);

                                            if (!lstPaymentDetailMoreForEdit.isEmpty()) {
                                                editInsNo = lstPaymentDetailMoreForEdit.get(0).getFieldName1();
                                                editIssuingBank = lstPaymentDetailMoreForEdit.get(0).getFieldName2();
                                                editIssuingBranch = lstPaymentDetailMoreForEdit.get(0).getFieldName3();
                                                editIssuingDt = lstPaymentDetailMoreForEdit.get(0).getFieldName4();
                                                editValidityDt = lstPaymentDetailMoreForEdit.get(0).getFieldName5();
                                            }
                                        } else if ("Account to Account Transfer".equalsIgnoreCase(editPaymentType)) {
                                            List<SPTenderCommonData> lstPaymentDetailMoreForEdit = tenderCommonService.returndata("getRegistrationFeePaymentDetailMoreForEdit", editPaymentId, null);

                                            if (!lstPaymentDetailMoreForEdit.isEmpty()) {
                                                editInsNo = lstPaymentDetailMoreForEdit.get(0).getFieldName1();
                                                if (!"".equalsIgnoreCase(lstPaymentDetailMoreForEdit.get(0).getFieldName3())) {
                                                    editIssuingBranch = lstPaymentDetailMoreForEdit.get(0).getFieldName3();
                                                }
                                            }
                                        }
                        %>

                        <%}%>


                        <input type="hidden" id="hdnPayUserId" name="hdnPayUserId"
                               <%if (!"".equalsIgnoreCase(payUserId)) {%>
                               value="<%=payUserId%>"
                               <%} else {%>
                               value="0"
                               <%}%>
                               >
                        <input type="hidden" id="hdnPaymentId" name="hdnPaymentId"

                               <%if (request.getParameter("payId") != null) {%>
                               value="<%=request.getParameter("payId")%>"
                               <%} else {%>
                               value="0"
                               <%}%>
                               >

                        <input type="hidden" id="hdnFinalAmount" name="hdnFinalAmount" >

                        <input type="hidden" id="hdnAction" name="hdnAction" value="<%=action%>">
                        <input type="hidden" id="hdnEditCurrency" name="hdnEditCurrency" value="<%=editCurrency%>">
                        <input type="hidden" id="hdnEditAmount" name="hdnEditAmount" value="<%=editAmount%>">
                        <input type="hidden" id="hdnEditPaymentType" name="hdnEditPaymentType" value="<%=editPaymentType%>">
                        <input type="hidden" id="hdnEditInsNo" name="hdnEditInsNo" value="<%=editInsNo%>">
                        <input type="hidden" id="hdnEditIssuingBank" name="hdnEditIssuingBank" value="<%=editIssuingBank%>">
                        <input type="hidden" id="hdnEditIssuingBranch" name="hdnEditIssuingBranch" value="<%=editIssuingBranch%>">
                        <input type="hidden" id="hdnEditIssuingDt" name="hdnEditIssuingDt" value="<%=editIssuingDt%>">
                        <input type="hidden" id="hdnEditValidityDt" name="hdnEditValidityDt" value="<%=editValidityDt%>">
                        <input type="hidden" id="hdnEditDisplayedDateOfPayment" name="hdnEditDisplayedDateOfPayment" value="<%=editDisplayedDateOfPayment%>">
                        <input type="hidden" id="hdnEditDateOfPayment" name="hdnEditDateOfPayment" value="<%=editDateOfPayment%>">
                        <input type="hidden" id="hdnEditComments" name="hdnEditComments" value="<%=editComments%>">
                        <input type="hidden" id="hdnEditPayStatus" name="hdnEditPayStatus" value="<%=editPayStatus%>">
                        <input type="hidden" id="hdnEditPaymentId" name="hdnEditPaymentId" value="<%=editPaymentId%>">
                        <input type="hidden" id="hdnEditValidityRefId" name="hdnEditValidityRefId" value="<%=editValidityRefId%>">

                    </form>
                </div>

            </div>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
