<%--
    Document   : MakePaymentView
    Created on : Dec 2, 2010, 12:32:42 PM
    Author     : Karan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import="java.lang.String" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService,com.cptu.egp.eps.web.utility.XMLReader" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Payment Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
       <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
         <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $("#frmMakePaymentView").validate({
                    rules:{
                        txtInsValidity:{required:true,CompareToWithoutEqual:"#txtDtOfIns", CompareToForToday:"#txtDtOfIns"},
                        txtComments:{required:true,maxlength:1000}
                    },

                    messages:{

                        txtInsValidity:{required:"<div class='reqF_1'>Please select instrument valid upto Date.</div>",
                            CompareToWithoutEqual:"<div class='reqF_1'>Instrument validity must be greater than the date of instrument.</div>",
                            CompareToForToday:"<div class='reqF_1'>Instrument validity must be greater than the current date.</div>"
                        },
                        txtComments:{required:"<div class='reqF_1'>Please enter comments.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 1000 characters are allowed</div>"
                        }
                    },

                    errorPlacement: function(error, element) {
                         if (element.attr("name") == "txtInsValidity")
                            error.insertAfter("#imgInsValidity");
                        else
                            error.insertAfter(element);
                    }

                });

            });

        </script>
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>

    <body>
             <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->

                <!--Dashboard Content Part Start-->
                <%
                String referer = "", orgReferer="";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");

                    if(referer.indexOf("MakePaymentView")>0){
                        if(request.getParameter("hdnReferer")!=null){
                            orgReferer=request.getParameter("hdnReferer");
                        }
                    } else{
                        orgReferer = request.getHeader("referer");
                    }
                }

               %>
                <div class="contentArea_1">
                    <div class="pageHead_1">
                        &nbsp;
<!--                        <span style="float: right;" ><a href="SearchTenderer.jsp?tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=request.getParameter("lotId")%>&payTyp=<%=request.getParameter("payTyp")%>" class="action-button-goback">Go Back</a></span>-->
                        <span style="float: right;" ><a href="<%=orgReferer%>" class="action-button-goback">Go Back</a></span>
                    </div>
               <%
                pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
               %>
                <%@include file="../resources/common/TenderInfoBar.jsp" %>

                <%
                            TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                            if (session.getAttribute("userId") != null) {
                                tenderCommonService1.setLogUserId(session.getAttribute("userId").toString());
                            }
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            HandleSpecialChar handleSpecialChar = new HandleSpecialChar();

                            String tenderId = "", pkgLotId = "", userId = "", currUserTypeId = "", lotId = "", payUserId = "", paymentId = "", actionType = "";
                            String paymentFor = "", typOfPay = "", amount = "", insRefNo = "", insDate = "", validityDt = "", bankName = "", branchName = "", comments = "";
                            String dtXml = "", statusXML="", tenderPaymentId="", paymentMode="",
                                   CurrPaidMode="", CurrPaidFor="", CurrPayStatus="", CurrPayDt="";
                            boolean isTenderer = false, isPEUser = false, isBankUser = false;
                            boolean isPackage = false, isLot = false; // for docAvlMethod
                            boolean sendMail = false;
                            boolean allowPayModifications = true;
                            boolean redirectToOfficerPage=false;
                            String validityRefId="0";
                            String refForOfficerPage="";

                            HttpSession hs = request.getSession();
                            if (hs.getAttribute("userId") != null) {
                                userId = hs.getAttribute("userId").toString();
                            }

                            if (request.getParameter("tenderId") != null) {
                                tenderId = request.getParameter("tenderId");
                            }

                            if (request.getParameter("lotId") != null) {
                                if (!request.getParameter("lotId").equalsIgnoreCase("") && !request.getParameter("lotId").equalsIgnoreCase("null")) {
                                    isLot = true;
                                    lotId = request.getParameter("lotId");
                                    pkgLotId = request.getParameter("lotId");
                                }
                            }

                            if (request.getParameter("uId") != null) {
                                if (request.getParameter("uId") != "" && !request.getParameter("uId").equalsIgnoreCase("null")) {
                                    payUserId = request.getParameter("uId");
                                }
                            }

                            if (request.getParameter("payId") != null) {
                                if (request.getParameter("payId") != "" && !request.getParameter("payId").equalsIgnoreCase("null")) {
                                    paymentId = request.getParameter("payId");
                                }
                            }

                            if (request.getParameter("action") != null) {
                                if (request.getParameter("action") != "" && !request.getParameter("action").equalsIgnoreCase("null")) {
                                    actionType = request.getParameter("action");
                                }
                            }


                            /* Start: CODE TO SET CURRENT USER TYPE */
                            if (hs.getAttribute("userTypeId") != null) {
                                currUserTypeId = hs.getAttribute("userTypeId").toString();
                                if (currUserTypeId.equalsIgnoreCase("2")) {
                                    isTenderer = true; // userType is Tenderer";
                                } else if (currUserTypeId.equalsIgnoreCase("3")) {
                                    isPEUser = true; // userType is Officer";
                                } else if (currUserTypeId.equalsIgnoreCase("7")) {
                                    isBankUser = true; // userType is ScheduleBank";
                                }
                            }
                            /* End: CODE TO SET CURRENT USER TYPE */

                            if (request.getParameter("valId") != null) {
                                if (!"0".equalsIgnoreCase(request.getParameter("valId")) && request.getParameter("valId")!="" && !request.getParameter("valId").equalsIgnoreCase("null")){
                                    validityRefId=request.getParameter("valId");
                                }
                            }

                            // Variable tenderId is defined by u on ur current page.
                            pageContext.setAttribute("tenderId", request.getParameter("tenderId"));

                            /* START : CODE TO GET ADDITIONAL PAYMENT INFORMATION */
                            List<SPTenderCommonData> lstAditionalPayDetail = tenderCommonService1.returndata("getAdditionalPaymentDetail", paymentId, null);
                            if (!lstAditionalPayDetail.isEmpty() && lstAditionalPayDetail.size()>0){
                                CurrPaidFor=lstAditionalPayDetail.get(0).getFieldName1();
                                CurrPaidMode=lstAditionalPayDetail.get(0).getFieldName2();
                                CurrPayStatus=lstAditionalPayDetail.get(0).getFieldName6();
                                CurrPayDt=lstAditionalPayDetail.get(0).getFieldName7();
                                   if (CurrPayStatus.equalsIgnoreCase("released") || CurrPayStatus.equalsIgnoreCase("forfeited") || CurrPayStatus.equalsIgnoreCase("canceled")){
                                        allowPayModifications = false;
                                   }

                            }
                            /* END : CODE TO GET ADDITIONAL PAYMENT INFORMATION */

                            if (request.getParameter("btnReleaseForfeit") != null) {
                                String requestXML="";
                                CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                CommonMsgChk commonMsgChk;
                                String requestTo="",requestAction="", requestComments="", msg="";

                                List<SPTenderCommonData> lstPayerInfo = tenderCommonService.returndata("getPaymentPaidByUserId",paymentId,null);

                                if(!lstPayerInfo.isEmpty()){
                                    requestTo=lstPayerInfo.get(0).getFieldName1();
                                }
                                //requestTo="271";

                                requestComments = request.getParameter("txtComments");
                                /* START: CODE TO HANDLE SPECIAL CHARACTERS  */
                                    requestComments = handleSpecialChar.handleSpecialChar(requestComments);
                                /* END: CODE TO HANDLE SPECIAL CHARACTERS  */


                                if (actionType.equalsIgnoreCase("requestrelease")) {
                                     requestAction="release";
                                     msg="releaserequested";
                                     sendMail = true;
                                } else if (actionType.equalsIgnoreCase("requestforfeit")) {
                                     requestAction="forfeit";
                                     msg="forfeitrequested";
                                     sendMail = true;
                                }

                                requestXML = "<tbl_PaymentActionRequest paymentId=\"" + paymentId + "\" requestBy=\"" + userId + "\" requestTo=\"" + requestTo + "\" requestAction=\"" + requestAction + "\" requestComments=\"" + requestComments + "\" actionStatus=\"pending\" actionCompDt=\"\" actionComments=\"\" requestDate=\"" + format.format(new Date()) + "\" />";
                                requestXML = "<root>" + requestXML + "</root>";


                                 commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_PaymentActionRequest", requestXML, "").get(0);

                                  if (commonMsgChk.getFlag().equals(true)) {
                                     if(isPEUser){
                                        redirectToOfficerPage=true;
                                     }

                                    if(sendMail){
                                         String mailSubject = "", actionToTake="";

                                         if("release".equalsIgnoreCase(requestAction)){
                                            mailSubject="Release request of "+ paymentFor +" on e-GP";
                                            actionToTake="released";
                                         } else if("forfeit".equalsIgnoreCase(requestAction)){
                                            mailSubject="Compensate request of "+ paymentFor +" on e-GP";
                                            actionToTake="forfeited";
                                         }

                                          /* START : CODE TO SEND MAIL */
                                                String tendererCompanyNm="";

                                                List<SPTenderCommonData> peBUEmail = tenderCommonService.returndata("getEmailIdfromUserId",requestTo,null);

                                                List<SPTenderCommonData> lstTendererCompany = tenderCommonService.returndata("getTendererCompanyName",payUserId,null);
                                                tendererCompanyNm = lstTendererCompany.get(0).getFieldName1();

                                                UserRegisterService userRegisterService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                                                String [] mail={peBUEmail.get(0).getFieldName1()};
                                                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                                                MailContentUtility mailContentUtility = new MailContentUtility();
                                                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                                                //String mailText = mailContentUtility.contRequest_ReleaseForfeit(tenderId,toextTenderRefNo, paymentFor, actionToTake, tendererCompanyNm);
                                                sendMessageUtil.setEmailTo(mail);
                                                sendMessageUtil.setEmailSub(HandleSpecialChar.handleSpecialChar(mailSubject));
                                                //sendMessageUtil.setEmailMessage(mailText);
                                                //userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("msgboxfrom"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contRequest_ReleaseForfeit(tenderId,toextTenderRefNo,paymentFor, actionToTake, tendererCompanyNm));
                                                sendMessageUtil.sendEmail();
                                                sendMessageUtil=null;
                                                mailContentUtility=null;
                                                mail=null;
                                                msgBoxContentUtility=null;
                                            /* END : CODE TO SEND MAIL */

                                    }

                                    if(isPEUser && redirectToOfficerPage){
                                        // Redirect to Rejesh's Page
                                         response.sendRedirect(orgReferer +"&msgId="+msg);
                                     } else {
                                        response.sendRedirect("MakePaymentView.jsp?payId=" + paymentId + "&uId=" + payUserId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&msgId=" + msg + "&payTyp=" + request.getParameter("payTyp"));
                                     }

                                    //response.sendRedirect("MakePaymentView.jsp?payId=" + paymentId + "&uId=" + payUserId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&msgId=" + msg + "&payTyp=" + request.getParameter("payTyp"));
                                  } else {
                                    response.sendRedirect("MakePaymentView.jsp?payId=" + paymentId + "&uId=" + payUserId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&msgId=error" + "&payTyp=" + request.getParameter("payTyp"));
                                  }


                            }
                            /* START CODE: TO INSERT/UPDATE PAYMENT DATA */
                            if (request.getParameter("btnSubmit") != null) {
                                String payStatus="";
                                /* START: SET THE STATUS TO BE UPDATED */
                                if (actionType.equalsIgnoreCase("extend")) {
                                     payStatus="extended";
                                     sendMail=true;
                                } else if (actionType.equalsIgnoreCase("release")) {
                                    payStatus="released";
                                    sendMail = true;
                                } else if (actionType.equalsIgnoreCase("cancel")) {
                                    payStatus="canceled";
                                    sendMail = true;
                                } else if (actionType.equalsIgnoreCase("hold")) {
                                    payStatus="on-hold";
                                } else if (actionType.equalsIgnoreCase("forfeit")) {
                                    payStatus="forfeited";
                                    sendMail = true;
                                } else {
                                }
                                /* END SET THE STATUS TO BE UPDATED */

                                // Only PE Officer can perform this action
                                /*
                                if ("forfeit".equalsIgnoreCase(payStatus)){
                                    if (!isPEUser){
                                        response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
                                    }
                                }
                                 */
                                //=====================================

                                if (actionType.equalsIgnoreCase("forfeit") || actionType.equalsIgnoreCase("extend") || actionType.equalsIgnoreCase("release") || actionType.equalsIgnoreCase("cancel") || actionType.equalsIgnoreCase("hold")) {

                                    /*
                                    paymentFor = request.getParameter("hdnPaymentFor");
                                    typOfPay = request.getParameter("hdnPayInsType");
                                    insRefNo = request.getParameter("hdnInsRefNo");
                                    amount = request.getParameter("hdnAmount");
                                    insDate = request.getParameter("txtDtOfIns");
                                    validityDt = request.getParameter("txtInsValidity");
                                    bankName = request.getParameter("hdnBankName");
                                    branchName = request.getParameter("hdnBranchName");
                                    comments = request.getParameter("txtComments");
                                    */
                                    if (actionType.equalsIgnoreCase("extend")) {

                                        paymentFor = request.getParameter("hdnPaymentFor");
                                        typOfPay = request.getParameter("hdnPayInsType");
                                        insRefNo = request.getParameter("hdnInsRefNo");
                                        amount = request.getParameter("hdnAmount");
                                        insDate = request.getParameter("txtDtOfIns");
                                        validityDt = request.getParameter("txtInsValidity");
                                        bankName = request.getParameter("hdnBankName");
                                        branchName = request.getParameter("hdnBranchName");
                                        comments = request.getParameter("txtComments");

                                        if(isBankUser){paymentMode="bank";} else if(isPEUser || isTenderer){paymentMode="offline";}

                                         /* START: CODE TO HANDLE SPECIAL CHARACTERS  */
                                        insRefNo = handleSpecialChar.handleSpecialChar(insRefNo);
                                        bankName = handleSpecialChar.handleSpecialChar(bankName);
                                        branchName = handleSpecialChar.handleSpecialChar(branchName);
                                        comments = handleSpecialChar.handleSpecialChar(comments);
                                     /* END: CODE TO HANDLE SPECIAL CHARACTERS  */

                                        if (insDate != null && validityDt != null) {
                                            if (!insDate.trim().equalsIgnoreCase("") && !validityDt.trim().equalsIgnoreCase("")) {
                                                String[] dtArr = insDate.split("/");
                                                insDate = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0];
                                                dtArr = validityDt.split("/");
                                                validityDt = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0];
                                            }
                                        }

                                    } else if (actionType.equalsIgnoreCase("forfeit") || actionType.equalsIgnoreCase("release") || actionType.equalsIgnoreCase("cancel") || actionType.equalsIgnoreCase("hold")) {
                                        comments = request.getParameter("txtComments");

                                        /* START: CODE TO HANDLE SPECIAL CHARACTERS  */
                                        comments = handleSpecialChar.handleSpecialChar(comments);
                                     /* END: CODE TO HANDLE SPECIAL CHARACTERS  */
                                    }

                                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                                    CommonMsgChk commonMsgChk;

                                    // CASE FOR EXTEND
                                    if (actionType.equalsIgnoreCase("extend")) {
                                         dtXml = "<tbl_TenderPayment paymentFor=\"" + paymentFor + "\" paymentInstType=\"" + typOfPay + "\" instRefNumber=\"" + insRefNo + "\" amount=\"" + amount + "\" instDate=\"" + insDate + "\" instValidUpto=\"" + validityDt + "\" bankName=\"" + bankName + "\" branchName=\"" + branchName + "\" comments=\"" + comments + "\" tenderId=\"" + tenderId + "\" userId=\"" + payUserId + "\" createdBy=\"" + userId + "\" createdDate=\"" + format.format(new Date()) + "\" pkgLotId=\"" + lotId + "\" eSignature=\"\" status=\"" + payStatus + "\" paymentMode=\"" + paymentMode + "\" extValidityRef=\"" + validityRefId + "\" />";
                                        dtXml = "<root>" + dtXml + "</root>";

                                        commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_TenderPayment", dtXml, "").get(0);

                                        tenderPaymentId=commonMsgChk.getId().toString();
                                    }else if (actionType.equalsIgnoreCase("forfeit") || actionType.equalsIgnoreCase("release") || actionType.equalsIgnoreCase("cancel") || actionType.equalsIgnoreCase("hold")) {
                                        String whereCodition="";
                                        dtXml="comments='"+ comments + "', status='"+ payStatus + "'";

                                        whereCodition="tenderPaymentId=" + paymentId;
                                        commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_TenderPayment", dtXml, whereCodition).get(0);

                                        tenderPaymentId=paymentId;
                                    }

                                    /* START : CODE TO INSERT INTO PAYMENT STATUS TABLE*/
                                    statusXML="<tbl_TenderPaymentStatus tenderPaymentId=\"" + tenderPaymentId + "\" paymentStatus=\"" + payStatus + "\" paymentStatusDt=\"" + format.format(new Date()) + "\" createdBy=\"" + userId + "\" comments=\"" + comments + "\" tenderPayRefId=\"" + paymentId + "\" />";
                                    statusXML = "<root>" + statusXML + "</root>";


                                    commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_TenderPaymentStatus", statusXML, "").get(0);



                                    if (commonMsgChk.getFlag().equals(true)) {


                                     if(isPEUser){
                                        redirectToOfficerPage=true;
                                     }

                                        if(sendMail){
                                             String mailSubject = "", actionTaken="";

                                             if("released".equalsIgnoreCase(payStatus)){
                                                mailSubject="Release of "+ paymentFor +" on e-GP";
                                                actionTaken="released";
                                             } else if("forfeited".equalsIgnoreCase(payStatus)){
                                                mailSubject="Compensate of "+ paymentFor +" on e-GP";
                                                actionTaken="forfeited";
                                             }
                                             else if("extended".equalsIgnoreCase(payStatus)){
                                                mailSubject="Extension of "+ paymentFor +" on e-GP";
                                                actionTaken="extended";
                                             }
                                              else if("canceled".equalsIgnoreCase(payStatus)){
                                                mailSubject="Cancelation of "+ paymentFor +" on e-GP";
                                                actionTaken="canceled";
                                             }

                                             if(isPEUser){
                                                 /* START : CODE TO SEND MAIL */
                                                 List<SPTenderCommonData> emails = tenderCommonService.returndata("getEmailIdfromUserId",payUserId,null);
                                                UserRegisterService userRegisterService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                                                String [] mail={emails.get(0).getFieldName1()};
                                                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                                                MailContentUtility mailContentUtility = new MailContentUtility();
                                                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                                                String mailText = mailContentUtility.contTenderPayment(tenderId,toextTenderRefNo, paymentFor, actionTaken);
                                                sendMessageUtil.setEmailTo(mail);
                                                sendMessageUtil.setEmailSub(HandleSpecialChar.handleSpecialChar(mailSubject));
                                                sendMessageUtil.setEmailMessage(mailText);
                                                userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("msgboxfrom"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contTenderPayment(tenderId,toextTenderRefNo,paymentFor, actionTaken));
                                                sendMessageUtil.sendEmail();
                                                sendMessageUtil=null;
                                                mailContentUtility=null;
                                                mail=null;
                                                msgBoxContentUtility=null;
                                            /* END : CODE TO SEND MAIL */
                                             } else if (isBankUser){
                                                /* START : CODE TO SEND MAIL */
                                                 String peOfficerUserId="", tendererCompanyNm="";
                                                 List<SPTenderCommonData> lstOfficerUserId = tenderCommonService.returndata("getPEOfficerUserIdfromTenderId",tenderId,null);
                                                 peOfficerUserId=lstOfficerUserId.get(0).getFieldName1();

                                                 List<SPTenderCommonData> peOfficerEmail = tenderCommonService.returndata("getEmailIdfromUserId",peOfficerUserId,null);

                                                 List<SPTenderCommonData> lstTendererCompany = tenderCommonService.returndata("getTendererCompanyName",payUserId,null);
                                                 tendererCompanyNm = lstTendererCompany.get(0).getFieldName1();


                                                List<SPTenderCommonData> emails = tenderCommonService.returndata("getEmailIdfromUserId",payUserId,null);
                                                UserRegisterService userRegisterService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                                                String [] mail={emails.get(0).getFieldName1(), peOfficerEmail.get(0).getFieldName1()};
                                                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                                                MailContentUtility mailContentUtility = new MailContentUtility();
                                                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                                                //String mailText = mailContentUtility.contBankUser_ReleaseForfeit(tenderId,toextTenderRefNo, paymentFor, actionTaken, tendererCompanyNm);
                                                sendMessageUtil.setEmailTo(mail);
                                                sendMessageUtil.setEmailSub(HandleSpecialChar.handleSpecialChar(mailSubject));
                                                //sendMessageUtil.setEmailMessage(mailText);
                                                //userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("msgboxfrom"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contBankUser_ReleaseForfeit(tenderId,toextTenderRefNo,paymentFor, actionTaken, tendererCompanyNm));
                                                sendMessageUtil.sendEmail();
                                                sendMessageUtil=null;
                                                mailContentUtility=null;
                                                mail=null;
                                                msgBoxContentUtility=null;
                                                /* END : CODE TO SEND MAIL */
                                             } else if(isTenderer){
                                                /* START : CODE TO SEND MAIL */
                                                 String peOfficerUserId="", tendererCompanyNm="";
                                                 List<SPTenderCommonData> lstOfficerUserId = tenderCommonService.returndata("getPEOfficerUserIdfromTenderId",tenderId,null);
                                                 peOfficerUserId=lstOfficerUserId.get(0).getFieldName1();

                                                 List<SPTenderCommonData> peOfficerEmail = tenderCommonService.returndata("getEmailIdfromUserId",peOfficerUserId,null);

                                                 List<SPTenderCommonData> lstTendererCompany = tenderCommonService.returndata("getTendererCompanyName",payUserId,null);
                                                 tendererCompanyNm = lstTendererCompany.get(0).getFieldName1();

                                                UserRegisterService userRegisterService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                                                String [] mail={peOfficerEmail.get(0).getFieldName1()};
                                                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                                                MailContentUtility mailContentUtility = new MailContentUtility();
                                                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                                                //String mailText = mailContentUtility.contBankUser_ReleaseForfeit(tenderId,toextTenderRefNo, paymentFor, actionTaken, tendererCompanyNm);
                                                sendMessageUtil.setEmailTo(mail);
                                                sendMessageUtil.setEmailSub(HandleSpecialChar.handleSpecialChar(mailSubject));
                                                //sendMessageUtil.setEmailMessage(mailText);
                                                //userRegisterService.contentAdmMsgBox(mail[0], XMLReader.getMessage("msgboxfrom"), HandleSpecialChar.handleSpecialChar(mailSubject), msgBoxContentUtility.contBankUser_ReleaseForfeit(tenderId,toextTenderRefNo,paymentFor, actionTaken, tendererCompanyNm));
                                                sendMessageUtil.sendEmail();
                                                sendMessageUtil=null;
                                                mailContentUtility=null;
                                                mail=null;
                                                msgBoxContentUtility=null;
                                             }
                                        }

                                        if (isPEUser && redirectToOfficerPage) {
                                            if("canceled".equalsIgnoreCase(payStatus)){
                                                response.sendRedirect(orgReferer +"&msgId="+payStatus);
                                            } else {
                                                response.sendRedirect(orgReferer +"&msgId="+payStatus);
                                            }

                                        } else {
                                            response.sendRedirect("MakePaymentView.jsp?payId=" + tenderPaymentId + "&uId=" + payUserId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&msgId=" + payStatus + "&payTyp=" + request.getParameter("payTyp"));
                                        }

                                        //response.sendRedirect("MakePaymentView.jsp?payId=" + tenderPaymentId + "&uId=" + payUserId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&msgId=" + payStatus + "&payTyp=" + request.getParameter("payTyp"));
                                    } else {

                                        response.sendRedirect("MakePaymentView.jsp?payId=" + tenderPaymentId + "&uId=" + payUserId + "&tenderId=" + tenderId + "&lotId=" + lotId + "&msgId=error" + "&payTyp=" + request.getParameter("payTyp"));
                                    }

                                    /* END : CODE TO INSERT INTO PAYMENT STATUS TABLE*/
                                }
                            }else{

                            /* END CODE: TO INSERT/UPDATE PAYMENT DATA */

                %>


                <%  boolean paymentdebar=false;if (isTenderer) {
                     pageContext.setAttribute("tab", "7");
                %>
                <%@include file="../tenderer/TendererTabPanel.jsp" %>
                <%paymentdebar=is_debared;}%>
                <% if (isPEUser) {
                     pageContext.setAttribute("tab", "8");
                %>
                     <%@include file="../officer/officerTabPanel.jsp" %>
                <%}%>
                <%if(!paymentdebar){%>
                <div class="tabPanelArea_1">

                       <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("payment")){
                            msgTxt="Payment information entered successfully.";
                        } else if(msgId.equalsIgnoreCase("extended")){
                            msgTxt="Payment extended successfully.";
                        } else  if(msgId.equalsIgnoreCase("released")){
                            msgTxt="Payment released successfully.";
                        } else  if(msgId.equalsIgnoreCase("canceled")){
                            msgTxt="Payment canceled successfully.";
                        } else  if(msgId.equalsIgnoreCase("on-hold")){
                            msgTxt="Payment put on-hold successfully.";
                        } else  if(msgId.equalsIgnoreCase("forfeited")){
                            msgTxt="Payment forfeited successfully.";
                        } else  if(msgId.equalsIgnoreCase("forfeitrequested")){
                            msgTxt="Payment forfeit request submitted successfully.";
                        } else  if(msgId.equalsIgnoreCase("releaserequested")){
                            msgTxt="Payment release request submitted successfully.";
                        } else  if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError){%>
                        <div class="responseMsg errorMsg" ><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg" ><%=msgTxt%></div>
                   <%}%>
                <%}}%>

                 <div class="pageHead_1">
                    <%if (actionType.equalsIgnoreCase("extend")) {%>
                    &nbsp;&nbsp;Action &gt;&gt; Extend
                    <%} else if (actionType.equalsIgnoreCase("release")) {%>
                    &nbsp;&nbsp;Action &gt;&gt; Release
                    <%} else if (actionType.equalsIgnoreCase("cancel")) {%>
                    &nbsp;&nbsp;Action &gt;&gt; Cancel
                    <%} else if (actionType.equalsIgnoreCase("hold")) {%>
                    &nbsp;&nbsp;Action &gt;&gt; Hold
                      <%} else if (actionType.equalsIgnoreCase("forfeit")) {%>
                    &nbsp;&nbsp;Action &gt;&gt; Compensate
                     <%} else if (actionType.equalsIgnoreCase("requestrelease")) {%>
                    &nbsp;&nbsp;Action &gt;&gt; Request Release
                     <%} else if (actionType.equalsIgnoreCase("requestforfeit")) {%>
                    &nbsp;&nbsp;Action &gt;&gt; Request Compensate
                    <%} else {%>
<!--                    &nbsp;&nbsp;Action &gt;&gt; View-->
                    <%}%>
                </div>


                    <%

                    List<SPTenderCommonData> PackageList = tenderCommonService1.returndata("getlotorpackagebytenderid",
                                        tenderId,
                                        "Package,1");

                                if (isLot) {
                                    List<SPTenderCommonData> lotList = tenderCommonService1.returndata("getlotorpackagebytenderid",
                                            tenderId,
                                            "Lot," + pkgLotId);

                                    if (!PackageList.isEmpty() && PackageList.size() > 0 && !lotList.isEmpty() && lotList.size() > 0) {%>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td width="15%" class="t-align-left ff">Package No. :</td>
                            <td class="t-align-left"><%=PackageList.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=PackageList.get(0).getFieldName2()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Lot No. :</td>
                            <td><%=lotList.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Lot Description :</td>
                            <td><%=lotList.get(0).getFieldName2()%></td>
                        </tr>
                    </table>
                    <%}
                                                    } else {
                                                        if (!PackageList.isEmpty() && PackageList.size() > 0) {%>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <td width="15%" class="t-align-left ff">Package No. :</td>
                            <td class="t-align-left"><%=PackageList.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Package Description :</td>
                            <td class="t-align-left"><%=PackageList.get(0).getFieldName2()%></td>
                        </tr>
                    </table>
                    <%}
                                }%>


                    <%
                                List<SPTenderCommonData> lstPaymentDetail = tenderCommonService1.returndata("getPaymentDetail", paymentId, null);
                                if (!lstPaymentDetail.isEmpty() && lstPaymentDetail.size() > 0) {
                    %>
                    <form id="frmMakePaymentView" action="MakePaymentView.jsp?action=<%=request.getParameter("action")%>&payId=<%=request.getParameter("payId")%>&uId=<%=request.getParameter("uId")%>&tenderId=<%=request.getParameter("tenderId")%>&lotId=<%=request.getParameter("lotId")%>&payTyp=<%=request.getParameter("payTyp")%>&valId=<%=validityRefId%>" method="post">
                        <input type="hidden" name="hdnReferer" value="<%=orgReferer%>"></input>
                        <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">

                         <tr>
                            <td class="ff">Payment Status :</td>
                            <td><%=CurrPayStatus%></td>
                        </tr>
                         <tr>
                            <td class="ff">Payment Date :</td>
                            <td><%=CurrPayDt%></td>
                        </tr>
                        <tr>
                            <td class="ff">Payment Mode :</td>
                            <td><%=CurrPaidMode%></td>
                        </tr>

                        <tr>
                            <td class="ff">Payment For :</td>
                            <td>
                                <%=lstPaymentDetail.get(0).getFieldName1()%>
                                <input value="<%=lstPaymentDetail.get(0).getFieldName1()%>" type="hidden" id="hdnPaymentFor" name="hdnPaymentFor"></input>
                            </td>
                        </tr>

                        <tr>
                            <td class="ff" width="17%">Payment Instrument Type :</td>
                            <td width="83%"><%=lstPaymentDetail.get(0).getFieldName2()%>
                                <input value="<%=lstPaymentDetail.get(0).getFieldName2()%>" type="hidden" id="hdnPayInsType" name="hdnPayInsType"></input>
                            </td>

                        </tr>

                        <tr>
                            <td class="ff">Amount :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName4()%>
                                <input value="<%=lstPaymentDetail.get(0).getFieldName4()%>" type="hidden" id="hdnAmount" name="hdnAmount"></input>
                            </td>
                        </tr>

                        <% if (!lstPaymentDetail.get(0).getFieldName2().equalsIgnoreCase("cash")) {%>

                        <tr>
                            <td class="ff">Instrument  Reference Number : </td>
                            <td>
                                <%=lstPaymentDetail.get(0).getFieldName3()%>
                                <input value="<%=lstPaymentDetail.get(0).getFieldName3()%>" type="hidden" id="hdnInsRefNo" name="hdnInsRefNo"></input>
                            </td>
                        </tr>

                        <tr>
                            <td class="ff">Date of instrument : </td>
                            <td>
                                <input value="<%=lstPaymentDetail.get(0).getFieldName5()%>"
                                       name="txtDtOfIns" type="text" class="formTxtBox_1" id="txtDtOfIns" style="width:100px; border: 0px;" readonly="readonly" />
                                <img id="imgDtOfIns" name="imgDtOfIns" onclick="GetCal('txtDtOfIns','imgDtOfIns');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer; display: none;" />
                                <input value="<%=lstPaymentDetail.get(0).getFieldName5()%>" type="hidden" id="hdnDtOfIns" name="hdnDtOfIns"></input>
                            </td>
                        </tr>

                        <tr>
                            <td class="ff">Instrument Validity :
                             <% if (actionType.equalsIgnoreCase("extend")) {%>
                            <span class="mandatory">*</span>
                                <%}%>
                            </td>
                            <td>
                                <%--<%=lstPaymentDetail.get(0).getFieldName6()%>--%>
                                <input value="<%=lstPaymentDetail.get(0).getFieldName6()%>"
                                       <%if (actionType.equalsIgnoreCase("extend")) {%>onfocus="GetCal('txtInsValidity','txtInsValidity');" <%}%>
                                       name="txtInsValidity" type="text" class="formTxtBox_1" id="txtInsValidity"
                                       <%if (actionType.equalsIgnoreCase("extend")) {%>style="width:100px;"<%} else {%>style="width:100px; border: 0px;"<%}%> readonly="readonly" />
                                <% if (actionType.equalsIgnoreCase("extend")) {%>
                                <img id="imgInsValidity" name="imgInsValidity" onclick="GetCal('txtInsValidity','imgInsValidity');" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;cursor: pointer;" />
                                <%}%>
                                <input value="<%=lstPaymentDetail.get(0).getFieldName6()%>" type="hidden" id="hdnInsValidityDt" name="hdnInsValidityDt"></input>
                            </td>
                        </tr>

                        <tr>
                            <td class="ff">Name of Bank : </td>
                            <td><%=lstPaymentDetail.get(0).getFieldName7()%>
                                <input value="<%=lstPaymentDetail.get(0).getFieldName7()%>" type="hidden" id="hdnBankName" name="hdnBankName"></input>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Name of Branch : </td>
                            <td><%=lstPaymentDetail.get(0).getFieldName10()%>
                                <input value="<%=lstPaymentDetail.get(0).getFieldName10()%>" type="hidden" id="hdnBranchName" name="hdnBranchName"></input>
                            </td>
                        </tr>
                        <%}%>
                        <%if(allowPayModifications){%>
                        <tr>
                            <td class="ff">Comments :
                            <% if (actionType.equalsIgnoreCase("requestrelease") || actionType.equalsIgnoreCase("requestforfeit") || actionType.equalsIgnoreCase("forfeit") || actionType.equalsIgnoreCase("extend") || actionType.equalsIgnoreCase("release") || actionType.equalsIgnoreCase("cancel") || actionType.equalsIgnoreCase("hold")) {%>
                            <span class="mandatory">*</span>
                                <%}%>
                            </td>
                            <td>
                                <% if ( actionType.equalsIgnoreCase("requestrelease") || actionType.equalsIgnoreCase("requestforfeit") || actionType.equalsIgnoreCase("forfeit") || actionType.equalsIgnoreCase("extend") || actionType.equalsIgnoreCase("release") || actionType.equalsIgnoreCase("cancel") || actionType.equalsIgnoreCase("hold")) {%>
                                <textarea cols="100" rows="5" id="txtComments" name="txtComments" class="formTxtBox_1"></textarea>
                                <%} else {%><%=lstPaymentDetail.get(0).getFieldName8()%><%}%>

                            </td>
                        </tr>
                         <%} else {%>
                         <tr>
                            <td class="ff">Comments :
                            </td>
                            <td>
                              <%=lstPaymentDetail.get(0).getFieldName8()%>
                            </td>
                        </tr>

                        <%}%>

                        <%if (allowPayModifications) {%>
                            <% if (!actionType.equalsIgnoreCase("requestrelease") && !actionType.equalsIgnoreCase("requestforfeit") && !actionType.equalsIgnoreCase("forfeit") && !actionType.equalsIgnoreCase("extend") && !actionType.equalsIgnoreCase("release") && !actionType.equalsIgnoreCase("cancel") && !actionType.equalsIgnoreCase("hold")) {%>
                            <%if (isPEUser) {%>
                                <%if ("offline".equalsIgnoreCase(CurrPaidMode) && "document fees".equalsIgnoreCase(CurrPaidFor)){%>
                                    <tr>
                                        <td style="text-align: center;" colspan="2"><a href="TenderPaymentDocsUpload.jsp?tenderId=<%=tenderId%>&tenderPaymentId=<%=paymentId%>&payTyp=<%=request.getParameter("payTyp")%>">Upload reference document</a>
                                        </td>
                                    </tr>
                                    <%}%>
                                <%} else {%>
                                <tr>
                                    <td style="text-align: center;" colspan="2"><a href="TenderPaymentDocsUpload.jsp?tenderId=<%=tenderId%>&tenderPaymentId=<%=paymentId%>&payTyp=<%=request.getParameter("payTyp")%>">Upload reference document</a>
                                    </td>
                                </tr>
                            <%}%>
                            <%}%>
                        <%}%>
                    </table>

                        <%if(allowPayModifications){%>
                        <% if (actionType.equalsIgnoreCase("forfeit") || actionType.equalsIgnoreCase("extend") || actionType.equalsIgnoreCase("release") || actionType.equalsIgnoreCase("cancel") || actionType.equalsIgnoreCase("hold")) {%>
                    <div class="t-align-center t_space">
                        <label class="formBtn_1"><input name="btnSubmit" type="submit" value="Submit" /></label>
                    </div>
                    <%} else if (actionType.equalsIgnoreCase("requestrelease") || actionType.equalsIgnoreCase("requestforfeit")) {%>
                        <div class="t-align-center t_space">
                        <label class="formBtn_1"><input name="btnReleaseForfeit" type="submit" value="Submit" /></label>
                        </div>
                    <%}%>
                    <%}%>
                    <% } else {%>
                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td>Payment information not found!</td>
                        </tr>
                    </table>
                    <%}%>
                    </form>

                   <% if (!actionType.equalsIgnoreCase("requestrelease") && !actionType.equalsIgnoreCase("requestforfeit") && !actionType.equalsIgnoreCase("forfeit") && !actionType.equalsIgnoreCase("extend") && !actionType.equalsIgnoreCase("release") && !actionType.equalsIgnoreCase("cancel") && !actionType.equalsIgnoreCase("hold")) {%>
                   <%--START CODE TO GET DOCS UPLOADED LIST--%>
                   <div>&nbsp;</div>
                   <div>Payment Reference Documents</div>
                     <table width="100%" cellspacing="0" class="tableList_1 t_space">
                         <tr>
                             <th width="4%" class="t-align-center">Sl. No.</th>
                             <th class="t-align-center" width="23%">File Name</th>
                             <th class="t-align-center" width="32%">File Description</th>
                             <th class="t-align-center" width="7%">File Size <br />(in KB)</th>
                             <th class="t-align-center" width="18%">Action</th>
                         </tr>
                    <%

                   // String tenderPaymentId = request.getParameter("tenderPaymentId");

                    int docCnt = 0;
                     TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                     if (session.getAttribute("userId") != null) {
                        tenderCS.setLogUserId(session.getAttribute("userId").toString());
                        }
                    for (SPTenderCommonData sptcd : tenderCS.returndata("TenderPaymentDocs", paymentId, "")) {
                                                        docCnt++;
                    %>
                    <tr
                         <%if(Math.IEEEremainder(docCnt,2)==0) {%>
                                            class="bgColor-Green"
                                        <%} else {%>
                                            class="bgColor-white"
                                        <%}%>
                        >
                        <td class="t-align-left"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-left"><%=(Long.parseLong(sptcd.getFieldName3()))/1024%>
                        </td>
                         <td class="t-align-left">
                             <a href="<%=request.getContextPath()%>/TenderPaymentDocUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&tenderPaymentId=<%=tenderPaymentId%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                             &nbsp;
                        </td>
                    </tr>

                          <%   if(sptcd!=null){
                        sptcd = null;
                    }
                    }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="5" class="t-align-center">No docs found.</td>
                    </tr>
                    <%}%>
            </table>
                    <%--END CODE TO GET DOCS UPLOADED LIST--%>
                  <%}%>


                </div><%}%>
                <div>&nbsp;</div>
                </div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->

            </div>
       <%
                }
        %>
    </body>
</html>
