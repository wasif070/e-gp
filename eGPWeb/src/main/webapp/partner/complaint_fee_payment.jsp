<%-- 
    Document   : RegistrationFeePayment
    Created on : Jan 22, 2011, 12:27:48 AM
    Author     : Karan
--%>

<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.lang.String"%>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Complaint Registration Fee Payment </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="../js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery.alerts.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery-1.4.1.js"></script>

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(function(){

                $('#btnSubmit').click(function(){
                
                    var flag=true;
                        
                    if($('#cmbCurrency').val()==''){
                        $('#SpError1').html('Please select the Currency.');
                        flag=false;
                    } else {
                        $('#SpError1').html('');
                    }

                    if($('#cmbPayment').val()==''){
                        $('#SpError2').html('Please select the Mode of Payment.');
                        flag=false;
                    } else {
                        $('#SpError2').html('');
                            
                        if($('#cmbPayment').val()=='Pay Order'){

                            if($.trim($('#txtInsRefNo').val())==''){
                                $('#SpError3').html('Please enter Instrument No.');
                                flag=false;
                            } else {$('#SpError3').html('');}

                            if($.trim($('#txtIssuanceBankNm').val())==''){
                                $('#SpError4').html('Please enter Issuing Bank.');
                                flag=false;
                            } else {
                                if(!chkSpecial($.trim($('#txtIssuanceBankNm').val())))
                                {
                                    $('#SpError4').html('Please enter a valid Financial Institute Name.');
                                    flag=false;
                                }
                                else
                                {
                                    $('#SpError4').html('');
                                }
                            }
                            //$('#SpError4').html('');}
                            if($.trim($('#txtIssuanceBranch').val())==''){
                                $('#SpError5').html('Please enter Issuing Financial Institute Branch.');
                                flag=false;
                            } else
                            {
                                if(!chkSpecial($.trim($('#txtIssuanceBranch').val())))
                                {
                                    $('#SpError5').html('Please enter a valid Branch Name.');
                                    flag=false;
                                }
                                else
                                {
                                    $('#SpError5').html('');
                                }
                                //$('#SpError5').html('');
                            }
                            if($.trim($('#txtIssuanceDt').val())==''){
                                $('#SpError6').html('Please select Issuance Date.');
                                flag=false;
                            } else {
                                var d =new Date();
                                var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());
                                var value=document.getElementById("txtIssuanceDt").value;
                                var mdy = value.split('/')  //Date and month split
                                var mdyhr= mdy[2].split(' ');  //Year and time split
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);

                                if(Date.parse(valuedate) > Date.parse(todaydate)){
                                    $('#SpError6').html('Issuance Date must be less than  or equal to the current date.');
                                    flag=false;
                                }else {
                                    $('#SpError6').html('');
                                }
                            }

                            if($.trim($('#txtValidityDt').val())==''){
                                $('#SpError8').html('Please select Validity Date.');
                                flag=false;
                            } else {
                                var d =new Date();
                                var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());
                                var value=document.getElementById("txtValidityDt").value;
                                var mdy = value.split('/')  //Date and month split
                                var mdyhr= mdy[2].split(' ');  //Year and time split
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);

                                if(Date.parse(valuedate) < Date.parse(todaydate)){
                                    $('#SpError8').html('Validity Date must be more than or equal to the current date.');
                                    flag=false;
                                }else {$('#SpError8').html('');}
                            }
                        } else if($('#cmbPayment').val()=='Account to Account Transfer'){
                            if($.trim($('#txtInsRefNo').val())==''){
                                $('#SpError3').html('Please enter Account No.');
                                flag=false;
                            } else {$('#SpError3').html('');}
                            if($.trim($('#txtIssuanceBranch').val())!=''){
                                if(!chkSpecial($.trim($('#txtIssuanceBranch').val())))                                {
                                    $('#SpError5').html('Only special characters not allowed.');
                                    flag=false;
                                }
                                else
                                {
                                    $('#SpError5').html('');
                                }
                            }
                        }
                    }
                    if($('#cmbValidityPeriod').val()==''){
                        $('#SpError9').html('Please select Validity Period.');
                        flag=false;
                    } else {
                        $('#SpError9').html('');
                    }
                    if($.trim($('#txtComments').val())==''){
                        $('#SpError7').html('Please enter Remarks.');
                        flag=false;
                    }
                    else {
                        if($.trim($('#txtComments')).length<=1000) {
                            $('#SpError7').html('');
                        }
                        else{
                            $('#SpError7').html('Maximum 1000 characters are allowed.');
                            flag=false;
                        }
                    }
                    if(flag==false){
                        return false;
                    }
                });
            });
            function chkSpecial(value)
            {
                //Allow only characters
                //Characters + numbers
                //Characters + numbers +few special
                //Don’t allow only numbers
                //return /^([a-zA-Z 0-9]([a-zA-Z 0-9\&\.\(\)\-])?)+$/.test(value);
                return /^[a-zA-Z 0-9](?![0-9]+$)[a-zA-Z 0-9\&\.\-\(\)/]+$/.test(value);
            }
        </script>
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });
                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            function GetCal2(txtname,controlname,optionalCtrl)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        if(optionalCtrl!=null){
                            if(optionalCtrl!='' && optionalCtrl !='null')   {
                                //alert(date.getMonth());
                                //var value=document.getElementById(txtname).value;
                                //alert(value);
                                //var mdy = value.split('/')  //Date and month split
                                //var mdyhr= mdy[2].split(' ');  //Year and time split
                                //var valuedate= new Date(mdyhr[0], mdy[1]+7, mdy[0]);
                                var currdate = document.getElementById(txtname).value;
                                var temp=currdate.split('/');
                                //alert(temp[0]);
                                //alert(temp[1]);
                                //alert(temp[2]);
                                var newtemp=temp[1]+'/'+temp[0]+'/'+temp[2];
                                //alert(new Date(newtemp));
                                var today =new Date(newtemp);
                                //alert(today);
                                //alert(today.getYear());
                                //alert(today.getMonth());
                                today.setMonth(today.getMonth()+7)
                                //// Note: getMonth() funcation returns 1 month less than the actual value
                                // So to add 6 months we have to use +7
                                //alert(today);
                                var dtVal = today.getDate();
                                if(dtVal<10){
                                    dtVal="0" + dtVal.toString();
                                }
                                var mnthVal = today.getMonth();
                                if(mnthVal<10){
                                    mnthVal="0" + mnthVal.toString();
                                }
                                var yrVal = today.getYear();
                                if(yrVal < 1900){
                                    yrVal= yrVal+ 1900;
                                }
                                valuedate=dtVal+"/"+mnthVal+"/"+yrVal;
                                //valuedate =new Date(valuedate);
                                document.getElementById(optionalCtrl).value=valuedate;
                            }
                        }
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });
                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
                //alert($('#hdnAction').val());
                var action=$('#hdnAction').val();
                //if(action.toLowerCase()=="edit"){
                //    BindPaymentData();
                //}
                function BindPaymentData(){
                    //alert("Binding data")
                    //alert($('#hdnEditCurrency').val());
                    $('#cmbCurrency').val($('#hdnEditCurrency').val());
                    var curVal=$('#cmbCurrency').val();
                    /*if (curVal == "BTN"){
                        $('#lblCurrencySymbol').html('Taka');*/
                    if (curVal == "Nu."){
                        $('#lblCurrencySymbol').html('Nu.');
                    } else if (curVal == "USD"){
                        $('#lblCurrencySymbol').html('$');
                    }
                    else if (curVal == "BTN"){
                        $('#lblCurrencySymbol').html('Nu.');
                    }
                    var amt = $('#hdnEditAmount').val();
                    $('#lblAmount').html(amt);
                    $('#hdnFinalAmount').val(amt);
                    $('#cmbPayment').val($('#hdnEditPaymentType').val());
                    //alert($('#hdnEditValidityRefId').val());
                    $('#cmbValidityPeriod').val($('#hdnEditValidityRefId').val());
                    //alert($('#cmbValidityPeriod').val());
                    var payType= $('#cmbPayment').val();
                    //alert(payType);
                    $('#trInsRefNo').show();
                    $('#trIssuanceBankNm').show();
                    $('#trIssuanceBranch').show();
                    $('#trIssuanceDt').show();
                    $('#trInsValidity').show();
                    clear();
                    if (payType == "Cash"){
                        $('#trInsRefNo').hide();
                        $('#trIssuanceBankNm').hide();
                        $('#trIssuanceBranch').hide();
                        $('#trIssuanceDt').hide();
                        $('#trInsValidity').hide();
                    } else if (payType == "Pay Order"){
                        $('#lblInstrumentNo').html('Instrument No.');
                        $('#lblBankBranch').html('Issuing Financial Institute Branch');
                        $('#spnBranchStar').html('*');

                        // Bind Data
                        $('#txtInsRefNo').val($('#hdnEditInsNo').val());
                        $('#txtIssuanceBankNm').val($('#hdnEditIssuingBank').val());
                        $('#txtIssuanceBranch').val($('#hdnEditIssuingBranch').val());
                        $('#txtIssuanceDt').val($('#hdnEditIssuingDt').val());
                        $('#txtValidityDt').val($('#hdnEditValidityDt').val());
                    }
                    else if (payType == "Account to Account Transfer"){
                        $('#trIssuanceBankNm').hide();
                        $('#trIssuanceDt').hide();
                        $('#trInsValidity').hide();

                        $('#lblInstrumentNo').html('Account No.');
                        $('#lblBankBranch').html('Branch Name');
                        $('#spnBranchStar').html('');

                        // Bind Data
                        $('#txtInsRefNo').val($('#hdnEditInsNo').val());
                        $('#txtIssuanceBranch').val($('#hdnEditIssuingBranch').val());
                    }

                    // Bind Date of Payment and Comments
                    $('#lblDtOfPayment').html($('#hdnEditDisplayedDateOfPayment').val());
                    $('#hdnDateOfPayment').val($('#hdnEditDateOfPayment').val());
                    $('#txtComments').val($('#hdnEditComments').val());
                }
                var payType= $('#cmbPayment').val();
                if (payType == "Cash"){
                    $('#trInsRefNo').hide();
                    $('#trIssuanceBankNm').hide();
                    $('#trIssuanceBranch').hide();
                    $('#trIssuanceDt').hide();
                    $('#trInsValidity').hide();
                }
                $('#cmbValidityPeriod').val('1');
                var curVal=$('#cmbCurrency').val();
                /*if (curVal == "BTN"){
                    $('#lblCurrencySymbol').html('Taka');*/
                    //GetPaymentAmt();
                if (curVal == "Nu."){
                    $('#lblCurrencySymbol').html('Nu.');
                } else if (curVal == "USD"){
                    $('#lblCurrencySymbol').html('$');
                    //GetPaymentAmt();
                }
                else if (curVal == "BTN"){
                    $('#lblCurrencySymbol').html('Nu.');
                    //GetPaymentAmt();
                }
                $('#cmbValidityPeriod').change(function(){
                    if($('#cmbValidityPeriod').val()!=''){
                        GetPaymentAmt();
                    }
                });
                $('#cmbCurrency').change(function(){
                    var curVal=$('#cmbCurrency').val();
                    /*if (curVal == "BTN"){
                        $('#lblCurrencySymbol').html('Taka');
                        GetPaymentAmt();*/
                    if (curVal == "Nu."){
                        $('#lblCurrencySymbol').html('Nu.');
                        GetPaymentAmt();
                    } else if (curVal == "USD"){
                        $('#lblCurrencySymbol').html('$');
                        GetPaymentAmt();
                    }
                    else if (curVal == "BTN"){
                        $('#lblCurrencySymbol').html('Nu.');
                        GetPaymentAmt();
                    }
                });
                $('#cmbPayment').change(function(){
                    //alert($('#cmbPayment').val());
                    var payType= $('#cmbPayment').val();
                    //alert(payType);
                    $('#trInsRefNo').show();
                    $('#trIssuanceBankNm').show();
                    $('#trIssuanceBranch').show();
                    $('#trIssuanceDt').show();
                    $('#trInsValidity').show();
                    clear();
                    if (payType == "Cash" || payType==''){
                        $('#trInsRefNo').hide();
                        $('#trIssuanceBankNm').hide();
                        $('#trIssuanceBranch').hide();
                        $('#trIssuanceDt').hide();
                        $('#trInsValidity').hide();
                    } else if (payType == "Pay Order"){
                        $('#lblInstrumentNo').html('Instrument No.');
                        $('#lblBankBranch').html('Issuing Bank Branch');
                        $('#spnBranchStar').html('*');
                    }
                    else if (payType == "Account to Account Transfer"){
                        $('#trIssuanceBankNm').hide();
                        $('#trIssuanceDt').hide();
                        $('#trInsValidity').hide();
                         
                        $('#lblInstrumentNo').html('Account No.');
                        $('#lblBankBranch').html('Branch Name');
                        $('#spnBranchStar').html('');
                    }
                });
                function GetPaymentAmt(){
                    //alert('Getting Payment Amount');
                    var payCase;
                    var payCurrency;
                    var validityId;
                    var userId;
                    if($('#hdnAction').val()=="renew" || $('#hdnEditPayStatus').val()=="renewed"){
                        payCase="renewal";
                    } else {
                        payCase="firstpay";
                    }
                    payCurrency=$('#cmbCurrency').val();
                    validityId=$('#cmbValidityPeriod').val();
                    userId = $('#hdnPayUserId').val();
                    //alert(payCase);
                    //alert(payCurrency);
            //alert(validityId        );
                    //alert(userId);
                    $.post("<%=request.getContextPath()%>/VerifyPaymentServlet", {action: 'getRegFeePaymentAmt', payCase: payCase, payCurrency: payCurrency, validityId: validityId, userId: userId  },  function(j){
                        //alert(j);
                        document.getElementById("lblAmount").innerHTML=j.toString();
                        document.getElementById("hdnFinalAmount").value=j.toString();
                        //alert(document.getElementById("lblAmount").innerHTML);
                    });
                }
                function clear(){
                    //alert('Clearing...');
                    // Clear Error Messages

                    $('#txtInsRefNo').val('');
                    $('#txtIssuanceBankNm').val('');
                    $('#txtIssuanceBranch').val('');
                    $('#txtIssuanceDt').val('');
                    $('#txtValidityDt').val('');
                    $('#txtComments').val('');

                    $('#SpError1').html('');
                    $('#SpError2').html('');
                    $('#SpError3').html('');
                    $('#SpError4').html('');
                    $('#SpError5').html('');
                    $('#SpError6').html('');
                    $('#SpError7').html('');
                    $('#SpError8').html('');
                }
                if(document.getElementById("txtInsRefNo")!=null){
                    $('#txtInsRefNo').blur(function(){
                        if($.trim($('#txtInsRefNo').val())==''){
                            if($('#cmbPayment').val()=='Pay Order'){
                                $('#SpError3').html('Please enter Instrument No.');
                            } else if($('#cmbPayment').val()=='Account to Account Transfer'){
                                $('#SpError3').html('Please enter Account No.');
                            }
                            flag=false;
                        } else {$('#SpError3').html('');}

                        if(flag==false){return false;}
                    });
                }
                if(document.getElementById("txtIssuanceBankNm")!=null){
                    $('#txtIssuanceBankNm').blur(function(){
                        if($.trim($('#txtIssuanceBankNm').val())==''){
                            $('#SpError4').html('Please enter Issuing Bank.');
                            flag=false;
                        } else {$('#SpError4').html('');}
        
                    });

                }
                if(document.getElementById("txtIssuanceBranch")!=null){
                    $('#txtIssuanceBranch').blur(function(){
                        if($.trim($('#txtIssuanceBranch').val())==''){
                            if($('#cmbPayment').val()=='Pay Order'){
                                $('#SpError5').html('Please enter Issuing Financial Institute Branch.');
                            }

                            flag=false;
                        } else {
                            //$('#SpError5').html('');
                            if($.trim($('#txtIssuanceBranch').val())!=''){
                                if(!chkSpecial($.trim($('#txtIssuanceBranch').val())))                                {
                                    $('#SpError5').html('Only special characters not allowed.');
                                    flag=false;
                                }
                                else
                                {
                                    $('#SpError5').html('');
                                }
                            }
                        }
            
                    });
                }
                if(document.getElementById("txtIssuanceDt")!=null){
                    $('#txtIssuanceBranch').blur(function(){
                        if($.trim($('#txtIssuanceDt').val())==''){
                            $('#SpError6').html('Please enter Issuance Date.');
                            flag=false;
                        } else {
                            var d =new Date();
                            var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());

                            var value=document.getElementById("txtIssuanceDt").value;
                            var mdy = value.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                            if(Date.parse(valuedate) > Date.parse(todaydate)){
                                $('#SpError6').html('Issuance Date must be less than  or equal to the current date.');
                                flag=false;
                            }else {

                                $('#SpError6').html('');
                            }
                        }
                    });
                }
                if(document.getElementById("txtValidityDt")!=null){
                    $('#txtIssuanceBranch').blur(function(){
                        if($.trim($('#txtValidityDt').val())==''){
                            $('#SpError8').html('Please enter Validity Date.');
                            flag=false;
                        } else {
                            var d =new Date();
                            var todaydate= new Date(d.getFullYear(), d.getMonth(), d.getDate());

                            var value=document.getElementById("txtValidityDt").value;
                            var mdy = value.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);

                            if(Date.parse(valuedate) < Date.parse(todaydate)){
                                $('#SpError8').html('Validity Date must be more than or equal to the current date.');
                                flag=false;
                            }else {$('#SpError8').html('');}
                        }
            
                    });
                }
                $('#txtComments').blur(function(){
                    if($.trim($('#txtComments').val())==''){
                        $('#SpError7').html('Please enter Remarks.');
                        flag=false;
                    }
                    else {
                        if($.trim($('#txtComments')).length<=1000) {
                            $('#SpError7').html('');
                        }
                        else{
                            $('#SpError7').html('Maximum 1000 characters are allowed.');
                            flag=false;
                        }
                    }
                });

                //alert(action);
                if(action.toLowerCase()=="edit"){
                    BindPaymentData();
                    GetPaymentAmt();
                    if($('#hdnEditPayStatus').val()=='paid'){
                        $('#trValidityPeriod').hide(); // Hide the Validity Period Combo for 1st Time Payment
                    }
                } else {
                    GetPaymentAmt();
                    if(action.toLowerCase()!="renew"){
                        $('#trValidityPeriod').hide(); // Hide the Validity Period Combo for 1st Time Payment
                    }
                }

            });
        </script>
    </head>
    <jsp:useBean id="tenderInfoServlet"
                 class="com.cptu.egp.eps.web.servicebean.TenderSrBean" />
    <body>
        <%
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    SimpleDateFormat formatVisible = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat formatVisibleWithMonthNm = new SimpleDateFormat("dd-MMM-yyyy");
                    SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MM-yyyy hh:mm");
                    HandleSpecialChar handleSpecialChar = new HandleSpecialChar();
                    boolean isBankUser = false, isRenew = false, isEditCase = false;
                    String emailId = "", payUserId = "", paymentDate = sdfDestination.format(new Date()), payDt = format.format(new Date()), payDtWithMnthNm = formatVisibleWithMonthNm.format(new Date());
                    ;
                    String strPartTransId = "0", userId = "", action = "", editPaymentId = "0";

                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userId = hs.getAttribute("userId").toString();
                    } else {
                        response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
                    }
                    //payDt=payDt.replace("-", "/");
                    //String amount= XMLReader.getMessage("localTenderRegFee");
                    //String amountDollar=XMLReader.getMessage("intTednerRegFee");

                    if (request.getParameter("uId") != null) {
                        if (request.getParameter("uId") != "" && !request.getParameter("uId").equalsIgnoreCase("null")) {
                            payUserId = request.getParameter("uId");
                        }
                    }
                    if (hs.getAttribute("govUserId") != null) {
                        strPartTransId = hs.getAttribute("govUserId").toString();
                    }

                    if (hs.getAttribute("userTypeId") != null) {
                        if ("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())) {
                            isBankUser = true; // userType is ScheduleBank";
                        }
                    }
                    System.out.println("isBankUser : " + isBankUser);
                    String bidderEmail = "", registeredDt = "";
        %>
        <div class="dashboard_div"><!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End--> <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">
                    Complaint Registration Fee Payment <%String feetype = (String) session.getAttribute("feetype");%>
                    <span style="float:right;"><a href="searchFirstForComplaintPayments.htm?feetype=<%=feetype%>" class="action-button-goback"><fmt:message key="LBL_GO_BACK"/></a>
                    </span></div>
                <div class="tabPanelArea_1 t_space">
                    <form id="complaintFeePaymentSave.htm" action="complaintFeePaymentSave.htm?complaintPaymentId=${payment.complaintPaymentId}" method="POST">
                        <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                            <tr>
                                <td class="ff">Email Id:</td>
                                <td><%=request.getAttribute("emailId")%>
                                    <input type="hidden" name="emailId" id="emailId" value='<%=request.getAttribute("emailId")%>'/>
                                </td>
                            </tr>
                            <%
                                        System.out.println("isBankUser : " + isBankUser);
                                        if (isBankUser) {
                                            //List<SPTenderCommonData> lstBankInfo = tenderCommonService.returndata("getBankInfo", userId, null);
                                            List<SPTenderCommonData> lstBankInfo = tenderCommonService.returndata("getBankInfo", strPartTransId, null);

                                            if (!lstBankInfo.isEmpty() && lstBankInfo.size() > 0) {
                            %>
                            <tr id="trBankNm">
                                <td class="ff"><fmt:message key="LBL_BANK_NAME"/> :
                                <td><%=lstBankInfo.get(0).getFieldName1()%> <input type="hidden" value="<%=lstBankInfo.get(0).getFieldName1()%>" id="hdnBankNm" name="hdnBankNm"></td>
                            </tr>
                            <tr id="trBranch">
                                <td class="ff"><fmt:message key="LBL_BRANCH_NAME"/> :
                                <td><%=lstBankInfo.get(0).getFieldName2()%> <input type="hidden" value="<%=lstBankInfo.get(0).getFieldName2()%>" id="hdnBranch" name="hdnBranch"></td>
                            </tr>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_BRANCH_MAKER"/>:</td>
                                <td><%=lstBankInfo.get(0).getFieldName3()%><input type="hidden" value="<%=lstBankInfo.get(0).getFieldName2()%>" id="hdnBranchMaker" name="hdnBranchMaker"></td>
                            </tr>
                            <%}
                    }%>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_PAYMENT_FOR"/>:</td>
                                <td> Complaint <%=request.getAttribute("complaintFeeType")%>
                                    <input type="hidden" value='<%=request.getAttribute("complaintFeeType")%>' id="hdnPaymentFor" name="hdnPaymentFor">
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="17%"><fmt:message key="LBL_CURRENCY"/> : <span class="mandatory">*</span></td>
                                <td width="83%"><select name="cmbCurrency" class="formTxtBox_1" id="cmbCurrency" width="100px;">
                                        <%-- <option value="BTN" selected="selected">BTN</option> --%>
                                        <option value="BTN" selected="selected">Nu.</option>
                                        <option value="USD">USD</option>
                                    </select> <span id="SpError1" class="reqF_1"></span></td>
                            </tr>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_AMOUNT"/> :</td>
                                <td><label id="lblCurrencySymbol"></label>&nbsp; <% if (request.getAttribute("complaintFeeType") != null && request.getAttribute("complaintFeeType").equals("Registration Fee")) {%>
                                    <%=(XMLReader.getMessage("localComplaintRegistrationFee"))%>
                                    <input type="hidden" id="hdnAmount" name="hdnAmount" value='<%=(XMLReader.getMessage("localComplaintRegistrationFee"))%>'>
                                    <%} else {%> <%=(XMLReader.getMessage("localComplaintSecurityFee"))%>
                                    <input type="hidden" id="hdnAmount" name="hdnAmount" value='<%=(XMLReader.getMessage("localComplaintSecurityFee"))%>'>
                                    <% }%>
                                </td>
                            </tr>

                            <tr>
                                <td class="ff" width="17%"><fmt:message key="LBL_MODE_PAYMENT"/> : <span
                                        class="mandatory">*</span></td>
                                <td width="83%"><select name="cmbPayment" class="formTxtBox_1"
                                                        id="cmbPayment">
                                        <option value="">Select</option>
                                        <option value="Cash" selected="selected">Cash</option>
                                        <!--option value="Pay Order">Pay Order</option-->
                                        <option value="Account to Account Transfer">Account to
			Account Transfer</option>
                                    </select> <span id="SpError2" class="reqF_1"></span></td>
                            </tr>
                            <tr id="trInsRefNo">
                                <td class="ff"><label id="lblInstrumentNo"></label> <span
                                        class="mandatory">*</span></td>
                                <td><input name="txtInsRefNo" type="text" class="formTxtBox_1" id="txtInsRefNo" style="width: 200px;" /> <span id="SpError3" class="reqF_1"></span></td>
                            </tr>

                            <tr id="trIssuanceBankNm">
                                <td class="ff"><fmt:message key="LBL_ISSUING_BANK"/> : <span class="mandatory">*</span></td>
                                <td><input name="txtIssuanceBankNm" type="text" class="formTxtBox_1" id="txtIssuanceBankNm" style="width: 200px;" />
                                    <span id="SpError4" class="reqF_1"></span></td>

                            </tr>
                            <tr id="trIssuanceBranch">
                                <td class="ff"><label id="lblBankBranch"></label> : <span
                                        id="spnBranchStar" class="mandatory"></span></td>
                                <td><input name="txtIssuanceBranch" type="text" class="formTxtBox_1" id="txtIssuanceBranch" style="width: 200px;" />
                                    <span id="SpError5" class="reqF_1"></span></td>
                            </tr>

                            <tr id="trIssuanceDt">
                                <td class="ff"><fmt:message key="LBL_ISSUANCE_DATE"/> : <span class="mandatory">*</span></td>
                                <td><input onfocus="GetCal2('txtIssuanceDt','txtIssuanceDt','txtValidityDt');" name="txtIssuanceDt" type="text" class="formTxtBox_1" id="txtIssuanceDt" style="width: 100px;" readonly="readonly" />
                                    <img id="imgIssuanceDt" name="imgIssuanceDt" onclick="GetCal2('txtIssuanceDt','imgIssuanceDt','txtValidityDt');" src="<%=request.getContextPath()%>/resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align: middle; cursor: pointer;" />
                                    <span id="SpError6" class="reqF_1"></span></td>
                            </tr>

                            <tr id="trInsValidity">
                                <td class="ff"><fmt:message key="LBL_VALIDITY_DATE"/> : <span class="mandatory">*</span></td>
                                <td><input onfocus="GetCal('txtValidityDt','txtValidityDt');" name="txtValidityDt" type="text" class="formTxtBox_1" id="txtValidityDt" style="width: 100px;" readonly="readonly" />
                                    <img id="imgValidityDt" name="imgValidityDt" onclick="GetCal('txtValidityDt','imgValidityDt');" src="<%=request.getContextPath()%>/resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align: middle; cursor: pointer;" />
                                    <span id="SpError8" class="reqF_1"></span></td>
                            </tr>

                            <tr id="trDtOfPayment">
                                <td class="ff"><fmt:message key="LBL_DATE_OF_PAYMENT"/> : <span class="mandatory">*</span></td>
                                <td><label id="lblDtOfPayment">

                                        <%=paymentDate%>
                                    </label> <input type="hidden" id="hdnDateOfPayment" name="hdnDateOfPayment" value="<%=payDt%>"></td>
                            </tr>

                            <tr>
                                <td class="ff">Remarks : <span class="mandatory">*</span></td>
                                <td>
                                    <textarea  class="formTxtBox_1" cols="20" style="width: 400px;" rows="5" id="txtComments" name="txtComments" ><c:if test="${payment != null}">${payment.comments}</c:if></textarea>
                                    <span id="SpError7" class="reqF_1"></span>

                                </td>
                            </tr>
                            <tr>
                                <td class="ff">&nbsp;</td>
                                <td>
                                    <div class="t-align-left t_space">
                                        <label class="formBtn_1">
                                            <input name="btnSubmit" id="btnSubmit" type="submit" value="Submit" /></label>
                                        <input type="hidden" id="hdnUserType" name="hdnUserType" value="BankUser">
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>

            </div>
            <!--Dashboard Content Part End--> <!--Dashboard Footer Start-->
            <div align="center"><%@include
                    file="../resources/common/Bottom.jsp"%></div>
                <!--Dashboard Footer End--></div>
        </body>
    </html>
