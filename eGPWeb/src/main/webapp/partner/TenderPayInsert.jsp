<%-- 
    Document   : TenderPayInsert
    Created on : Oct 4, 2018, 3:25:29 PM
    Author     : MD. Emtazul Haque
--%>

<%@page import="java.net.URLEncoder"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConsolodateService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AccPaymentService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CmsConfigDateService"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Date" %>
<%@page import="java.lang.String" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<%
            
        HttpSession hs = request.getSession();
        String path="";
        if (hs.getAttribute("userId") != null) 
        {
            ConsolodateService c_ConsSrv = (ConsolodateService) AppContext.getSpringBean("ConsolodateService");
            AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");
            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            if (session.getAttribute("userId") != null) 
            {
                tenderCommonService.setLogUserId(session.getAttribute("userId").toString());
            }
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            SimpleDateFormat formatVisible = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat formatVisibleWithMonthNm = new SimpleDateFormat("dd-MMM-yyyy");
            HandleSpecialChar handleSpecialChar = new HandleSpecialChar();

            String emailId="",payUserId="", payDt=formatVisible.format(new Date()), payDtWithMnthNm=formatVisibleWithMonthNm.format(new Date());;
            payDt=payDt.replace("-", "/");

            boolean isBankUser=false, isRenew=false, isEditCase=false, isExtenison=false, isChecker=false, isAmtError=true;;
            String strPartTransId="0", userId="", action="", editPaymentId="0";
            
            String[] regPayIdArray = new String[4];
            String[] editRegPayIdArray = new String[4];
            int totalPayId = 0;
                
            userId = hs.getAttribute("userId").toString();

            if (hs.getAttribute("govUserId") != null) 
            {
                strPartTransId = hs.getAttribute("govUserId").toString();
            }

            if (request.getParameter("uId") != null) 
            {
                if (request.getParameter("uId") != "" && !request.getParameter("uId").equalsIgnoreCase("null")) 
                {
                    payUserId = request.getParameter("uId");
                }
            }

            if (request.getParameter("action") != null) 
            {
                if (request.getParameter("action") != "" && !request.getParameter("action").equalsIgnoreCase("null")) 
                {
                    action = request.getParameter("action");

                    if ("edit".equalsIgnoreCase(action)) 
                    {
                        if (request.getParameter("payId") != null) 
                        {
                            if (request.getParameter("payId") != "0" && request.getParameter("payId") != "" && !request.getParameter("payId").equalsIgnoreCase("null")) 
                            {
                                editPaymentId = request.getParameter("payId");
                                isEditCase = true;
                            }
                        }
                    }
                }
            }

            if ( hs.getAttribute("userTypeId")!= null) 
            {
                if("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString()))
                {
                    isBankUser=true; // userType is ScheduleBank";
                }
            }
            
            // START CODE: TO INSERT PAYMENT DATA
            if (request.getParameter("btnSubmit") != null) 
            {
                if (request.getParameter("action") != null) 
                {
                    if (request.getParameter("action") != "" && !request.getParameter("action").equalsIgnoreCase("null")) 
                    {
                        action = request.getParameter("action");
                    }
                }
                
                if("renew".equalsIgnoreCase(action))
                {
                    isRenew=true;
                }

                String paymentFor="", bankName = "", branchName = "", currency="", typOfPay = "", insRefNo = "", issuanceBank="", issuanceBranch="", issuanceDate="" , validityDt = "", dtOfPayment="", comments = "", paymentMode="", status="", validityPeriodRef="",bgId = "0";
                String fnlAmt="0", currencySymbol="";
                String payTyp="", tenderId="0", pkgLotId="0", extId="0", extForPayId="0";

                payTyp= request.getParameter("hdnPayType");
                tenderId= request.getParameter("hdnTenderId");
                pkgLotId= request.getParameter("hdnLotId");
                paymentFor = request.getParameter("hdnPaymentFor");
                extId = request.getParameter("hdnExtId");
                extForPayId = request.getParameter("hdnExtForPayId");
                
                if(request.getParameter("bgId")!=null && !"".equals(request.getParameter("bgId")))
                {
                    bgId = request.getParameter("bgId");
                }

                if (request.getParameter("hdnExtId") != null)
                {
                    if (!"0".equalsIgnoreCase(request.getParameter("hdnExtId")) && request.getParameter("hdnExtId")!="" && !request.getParameter("hdnExtId").equalsIgnoreCase("null"))
                    {
                        if("Bid Security".equalsIgnoreCase(paymentFor))
                        {
                            isExtenison=true;
                        }
                    }
                }
                
                // Dohatec Start
                String bgIds[] = new String[4];
                List<SPTenderCommonData> bankInfo = tenderCommonService.returndata("getBankInfoForTenPayment", strPartTransId, editPaymentId);
                CommonSearchDataMoreService objBgIds = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> lstBgIds = null;

                if("ICT".equalsIgnoreCase(request.getParameter("hdnIsICT")))
                {
                    if("bg".equalsIgnoreCase(payTyp))
                    {                        
                        if("checker".equalsIgnoreCase(bankInfo.get(0).getFieldName4()))
                        {
                            lstBgIds = objBgIds.geteGPData("getBankGuaranteeIdsICTBank", "tenderId", tenderId);
                        }
                        else    // Maker
                        {
                            lstBgIds = objBgIds.geteGPData("getBankGuaranteeIdsICTBank", "bgId", bgId);
                        }

                        if(!lstBgIds.isEmpty())
                        {
                            for(int bg = 0; bg < lstBgIds.size(); bg++)
                            {
                                bgIds[bg] = lstBgIds.get(bg).getFieldName1();
                            }
                        }
                    }
                }
                else    //  NCT
                {
                    if("0".equals(bgId))
                    {
                        if("checker".equalsIgnoreCase(bankInfo.get(0).getFieldName4()))
                        {
                            lstBgIds = objBgIds.geteGPData("getBankGuaranteeIdsICTBank", "tenderId", tenderId);
                            if(!lstBgIds.isEmpty())
                                bgId = lstBgIds.get(0).getFieldName1();
                        }
                    }
                }
                // Dohatec End
                
                bankName=request.getParameter("hdnBankNm");
                branchName=request.getParameter("hdnBranch");
                
                if(request.getParameter("cmbCurrency") != null)
                {
                    currency=request.getParameter("cmbCurrency");
                }
                else
                {
                    currency=request.getParameter("hdnCurrency");
                }
                System.out.println("Currency is : "+currency);
                typOfPay = request.getParameter("cmbPayment");

                
                if("Pay Order".equalsIgnoreCase(typOfPay) || "DD".equalsIgnoreCase(typOfPay) || "Bank Guarantee".equalsIgnoreCase(typOfPay))
                {
                    insRefNo = request.getParameter("txtInsRefNo");
                    issuanceBank=request.getParameter("txtIssuanceBankNm");
                    issuanceBranch=request.getParameter("txtIssuanceBranch");
                    issuanceDate = request.getParameter("txtIssuanceDt");
                    validityDt = request.getParameter("txtValidityDt");
                } 
                else  if("Account to Account Transfer".equalsIgnoreCase(typOfPay))
                {
                    insRefNo = request.getParameter("txtInsRefNo");
                    issuanceBranch=request.getParameter("txtIssuanceBranchA2A");
                }

                
                if("BTN".equalsIgnoreCase(currency))
                {
                    currencySymbol="Nu.";
                    fnlAmt=request.getParameter("hdnFinalAmount");
                } 
                else if("USD".equalsIgnoreCase(currency))
                {
                    currencySymbol="$";
                    fnlAmt=request.getParameter("hdnDollarAmount");
                }
                else if("Nu.".equalsIgnoreCase(currency))
                {
                    currencySymbol="Nu.";
                    fnlAmt=request.getParameter("hdnFinalAmount");
                }

                if((!"".equalsIgnoreCase(request.getParameter("hdnPSAmount")) || !request.getParameter("hdnPSAmount").isEmpty()) && "ICT".equalsIgnoreCase(request.getParameter("hdnIsICT")))   
                {
                    // Performence Security Payment
                    String split[] = request.getParameter("hdnPSAmount").substring(0, request.getParameter("hdnPSAmount").length()-1).split("-");;
                    for(int i = 0; i< split.length; i++)
                    {
                        String psAmt[] = split[i].split("@");
                        fnlAmt = psAmt[1];
                        if(Double.parseDouble(fnlAmt) > 0 )
                        {
                            isAmtError = false;
                        }
                        else
                        {
                            isAmtError = true;
                            break;
                        }
                    }
                }
                else
                {
                    if(Double.parseDouble(fnlAmt) > 0 ){
                        isAmtError = false;
                    }
                }
                
                //Dohatec End
                // Dohate Start
                if(isEditCase)
                {
                    // Edit by Maker
                    if((!"".equalsIgnoreCase(request.getParameter("hdnPSAmount")) || !request.getParameter("hdnPSAmount").isEmpty())  && "ICT".equalsIgnoreCase(request.getParameter("hdnIsICT")))
                    {
                        System.out.print("TenderID : " + tenderId.toString() + " \nuserID : " + payUserId.toString());
                        CommonSearchDataMoreService objTSDetails = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        List<SPCommonSearchDataMore> lstPerSecPayIDDetail = null;
                        if("bg".equalsIgnoreCase(payTyp))
                        {
                            lstPerSecPayIDDetail = objTSDetails.geteGPData("getNewPerSecPaymentIDDetail", tenderId.toString(), pkgLotId, payUserId.toString());
                        }
                        else
                        {
                            lstPerSecPayIDDetail = objTSDetails.geteGPData("getPerSecPaymentIDDetail", tenderId.toString(), payUserId.toString());
                        }

                        System.out.print("Old Paymend id list size " + lstPerSecPayIDDetail.size());
                        if(!lstPerSecPayIDDetail.isEmpty())
                        {
                            for(int i = 0; i< lstPerSecPayIDDetail.size(); i++)
                            {
                                editRegPayIdArray[i] = lstPerSecPayIDDetail.get(i).getFieldName1();
                                System.out.print("Old Payment Id : " + editRegPayIdArray[i]);
                            }
                        }
                    }
                }
                
                // Dohatec End
                boolean isRecrdExists = false;
                if(!isEditCase)
                {
                    CommonSearchDataMoreService spCommonSearchDataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> spcsdms = spCommonSearchDataMoreService.geteGPData("chkRecordExist",tenderId,pkgLotId,payUserId,paymentFor,extId);
                    if(spcsdms!=null && !spcsdms.isEmpty())
                    {
                        if("yes".equalsIgnoreCase(spcsdms.get(0).getFieldName1()))
                        {
                            isRecrdExists = true;
                        }
                    }
                }
                
                if(!isAmtError && !isRecrdExists)
                {
                    dtOfPayment = request.getParameter("hdnDateOfPayment");
                    comments = request.getParameter("txtComments");

                    if(isEditCase)
                    {
                        status = request.getParameter("hdnEditPayStatus");
                        editPaymentId = request.getParameter("hdnEditPaymentId");
                    } 
                    else 
                    {
                        if(isExtenison)
                        {
                            status = "extended";
                        } 
                        else if (isRenew) 
                        {
                            status = "renewed";
                        } 
                        else 
                        {
                            status = "paid";
                        }
                    }

                    if(isBankUser)
                    {
                        paymentMode="bank";
                    }

                    /* START: CODE TO HANDLE SPECIAL CHARACTERS  */
                    bankName = handleSpecialChar.handleSpecialChar(bankName);
                    branchName = handleSpecialChar.handleSpecialChar(branchName);

                    insRefNo = handleSpecialChar.handleSpecialChar(insRefNo);
                    issuanceBank = handleSpecialChar.handleSpecialChar(issuanceBank);
                    issuanceBranch = handleSpecialChar.handleSpecialChar(issuanceBranch);

                    comments = URLEncoder.encode(comments,"UTF-8");
                    /* END CODE TO HANDLE SPECIAL CHARACTERS  */

                    if (issuanceDate != null) 
                    {
                        if (!issuanceDate.trim().equalsIgnoreCase("")) 
                        {
                            String[] dtArr = issuanceDate.split("/");
                            issuanceDate = dtArr[2] + "-" + dtArr[1] + "-" + dtArr[0];

                            String[] dtArrValDt = validityDt.split("/");
                            validityDt = dtArrValDt[2] + "-" + dtArrValDt[1] + "-" + dtArrValDt[0];
                        }
                    }

                    String strPayMakerUserId="0", strPayMakerPartTransId="0";
                    
                    if(isEditCase)
                    {
                        List<SPTenderCommonData> lstBankInfo = tenderCommonService.returndata("getBankInfoForTenPayment", strPartTransId, editPaymentId);
                        if(!lstBankInfo.isEmpty())
                        {
                            if("checker".equalsIgnoreCase(lstBankInfo.get(0).getFieldName4()))
                            {
                                isChecker=true;
                                strPayMakerUserId = lstBankInfo.get(0).getFieldName5();
                                strPayMakerPartTransId = lstBankInfo.get(0).getFieldName6();
                            }
                        }
                        lstBankInfo = null;
                    }
                
                    //String dtXml = "";
                    String regPayId = "";
                    // Dohatec Start
                    CommonMsgChk commonMsgChk = new CommonMsgChk();
                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                    if (session.getAttribute("userId") != null) 
                    {
                          commonXMLSPService.setLogUserId(session.getAttribute("userId").toString());
                    }
                    
                    String idType = "tenderId";
                    int id = Integer.parseInt(tenderId);
                    String auditAction = "Add "+paymentFor+" Payment";
                    if("Bank Guarantee".equalsIgnoreCase(paymentFor))
                    {
                        auditAction = "Add New Performance Security Payment";
                        idType = "ContractId";
                        id = c_ConsSrv.getContractId(Integer.parseInt(tenderId));
                    }
                    
                    if(isChecker)
                    {
                        // Edit Payment by Checker Case
                        // Dohate :: Performance Security :: If the hidden value of Performance Security Payment is not null
                        if((!"".equalsIgnoreCase(request.getParameter("hdnPSAmount")) || !request.getParameter("hdnPSAmount").isEmpty())  && "ICT".equalsIgnoreCase(request.getParameter("hdnIsICT")))
                        {
                            // Performence Security Payment
                            String hdnPSAmount = request.getParameter("hdnPSAmount");
                            System.out.print("hidden " + hdnPSAmount);
                            hdnPSAmount = hdnPSAmount.substring(0, (hdnPSAmount.length()-1));
                            System.out.print("hidden (sub) " + hdnPSAmount);
                            String cTypePSAmountSplit[] = hdnPSAmount.split("-");
                            totalPayId = cTypePSAmountSplit.length;
                            for(int i=0; i<cTypePSAmountSplit.length; i++)
                            {
                                try
                                {
                                    String cTypePSAmount[] = cTypePSAmountSplit[i].split("@");
                                    System.out.print("Currency : "+ cTypePSAmount[0] + " PS Amount : " + cTypePSAmount[1]);
                                    commonMsgChk = addUpdate.addUpdOpeningEvaluation("InsertPaymentAndPaymentStatus",paymentFor,typOfPay,insRefNo,cTypePSAmount[1],issuanceDate,validityDt,bankName,branchName,comments,tenderId,payUserId,strPayMakerUserId,dtOfPayment,pkgLotId,status,paymentMode,extId,cTypePSAmount[0],issuanceBank,issuanceBranch,format.format(new Date()),strPayMakerPartTransId).get(0); 
                                    regPayIdArray[i] = commonMsgChk.getId().toString();
                                }
                                catch(Exception e)
                                {
                                    auditAction = "Error In "+auditAction+" :"+e.getMessage();
                                }
                                finally
                                {
                                    if(!isEditCase)
                                    {
                                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), id, idType, EgpModule.Bank_Module.getName(), auditAction, comments);
                                    }
                                }
                            }
                            regPayId= regPayIdArray[0];
                        }
                        else
                        {
                            try
                            {
                               commonMsgChk = commonMsgChk = addUpdate.addUpdOpeningEvaluation("InsertPaymentAndPaymentStatus",paymentFor,typOfPay,insRefNo,fnlAmt,issuanceDate,validityDt,bankName,branchName,comments,tenderId,payUserId,strPayMakerUserId,dtOfPayment,pkgLotId,status,paymentMode,extId,currency,issuanceBank,issuanceBranch,format.format(new Date()),strPayMakerPartTransId).get(0); 
                               regPayId = commonMsgChk.getId().toString();
                            }
                            catch(Exception e)
                            {
                                auditAction = "Error In "+auditAction+" :"+e.getMessage();
                            }
                            finally
                            {
                                if(!isEditCase)
                                {
                                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), id, idType, EgpModule.Bank_Module.getName(), auditAction, comments);
                                }
                            }
                        }
                    } 
                    else 
                    {
                        // Edit Payment by Maker Case                
                        // Dohate :: Performance Security :: If the hidden value of Performance Security Payment is not null
                        if((!"".equalsIgnoreCase(request.getParameter("hdnPSAmount")) || !request.getParameter("hdnPSAmount").isEmpty())  && "ICT".equalsIgnoreCase(request.getParameter("hdnIsICT")))   
                        {
                            // Performence Security Payment
                            String hdnPSAmount = request.getParameter("hdnPSAmount");
                            System.out.print("hidden " + hdnPSAmount);
                            hdnPSAmount = hdnPSAmount.substring(0, (hdnPSAmount.length()-1));
                            System.out.print("hidden (sub) " + hdnPSAmount);
                            String cTypePSAmountSplit[] = hdnPSAmount.split("-");
                            totalPayId = cTypePSAmountSplit.length;
                            for(int i=0; i<cTypePSAmountSplit.length; i++)
                            {
                                try
                                {
                                    String cTypePSAmount[] = cTypePSAmountSplit[i].split("@");
                                    System.out.print("Currency : "+ cTypePSAmount[0] + " PS Amount : " + cTypePSAmount[1]);
                                    commonMsgChk = addUpdate.addUpdOpeningEvaluation("InsertPaymentAndPaymentStatus",paymentFor,typOfPay,insRefNo,cTypePSAmount[1],issuanceDate,validityDt,bankName,branchName,comments,tenderId,payUserId,userId,dtOfPayment,pkgLotId,status,paymentMode,extId,cTypePSAmount[0],issuanceBank,issuanceBranch,format.format(new Date()),strPartTransId).get(0); 
                                    regPayIdArray[i] = commonMsgChk.getId().toString();
                                }
                                catch(Exception e)
                                {
                                    auditAction = "Error In "+auditAction+" :"+e.getMessage();
                                }
                                finally
                                {
                                    if(!isEditCase)
                                    {
                                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), id, idType, EgpModule.Bank_Module.getName(), auditAction, comments);
                                    }
                                }
                            }
                            regPayId= regPayIdArray[0];
                        }
                        else // Old version
                        {
                            try
                            {
                               commonMsgChk = addUpdate.addUpdOpeningEvaluation("InsertPaymentAndPaymentStatus",paymentFor,typOfPay,insRefNo,fnlAmt,issuanceDate,validityDt,bankName,branchName,comments,tenderId,payUserId,userId,dtOfPayment,pkgLotId,status,paymentMode,extId,currency,issuanceBank,issuanceBranch,format.format(new Date()),strPartTransId).get(0); 
                               regPayId = commonMsgChk.getId().toString();
                            }
                            catch(Exception e)
                            {
                                auditAction = "Error In "+auditAction+" :"+e.getMessage();
                            }
                            finally
                            {
                                if(!isEditCase)
                                {
                                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), id, idType, EgpModule.Bank_Module.getName(), auditAction, comments);
                                }
                            }
                        }
                    }

                    // Dohatec End
                    // Not Work For : Maker, Edit (Edit by Maker)
                    // Work For     : Checker, Insert by Maker (isChecker || (!isChecker && !isEditCase))
                    CmsConfigDateService cmsConfigDateService = (CmsConfigDateService) AppContext.getSpringBean("CmsConfigDateService");
                    AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
                    UserRegisterService  registerService = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                    CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");

                    if("Bank Guarantee".equalsIgnoreCase(paymentFor))
                    { 
                        List<Object[]> GetCinfoBar = cmsConfigDateService.getContractInfoBar(Integer.parseInt(tenderId),Integer.parseInt(pkgLotId));
                        Object[] obj = null;
                        if(!GetCinfoBar.isEmpty())
                        {
                            obj = GetCinfoBar.get(0);                    
                            List<Object> peNameObj = cmsConfigDateService.getPeName(Integer.parseInt(session.getAttribute("userId").toString()));
                            String BankName = "";
                            BankName = accPaymentService.getBankName(regPayId);
                            String peEmailId = "";
                            List<Object[]> peEmailObj = accPaymentService.getPEemailID(Integer.parseInt(tenderId));
                            if(!peEmailObj.isEmpty())
                            {
                                Object[] objj = peEmailObj.get(0);
                                peEmailId = objj[0].toString();
                            }
                            String strFrom = commonservice.getEmailId(session.getAttribute("userId").toString());
                            List<Object[]> AcEmailId = accPaymentService.getAccountantEmailId(Integer.parseInt(tenderId));
                            String[] strTo = null;
                            int emailIndex = 0;
                            if("7".equals(session.getAttribute("userTypeId").toString()))
                            {
                                strTo = new String[AcEmailId.size()+2];
                                for(Object[] objemailId : AcEmailId){
                                    strTo[emailIndex] = objemailId[4].toString();
                                    emailIndex++;
                                }
                                strTo[emailIndex] = obj[9].toString();
                                strTo[emailIndex+1] = peEmailId;
                            }
                            boolean c_isPhysicalPrComplete = c_ConsSrv.checkForItemFullyReceivedOrNot(Integer.parseInt(pkgLotId));
                            SendMessageUtil sendMessageUtil = new SendMessageUtil();
                            sendMessageUtil.setEmailFrom(strFrom);

                            MailContentUtility mailContentUtility = new MailContentUtility();
                            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                            String emailIdArray[] = new String[1];
                            String mailSubject = "e-GP : New Performance Security Issued";
                            String mobileno = "";
                            for(int i=0; i<strTo.length; i++)
                            {
                                emailIdArray[0] = strTo[i];
                                registerService.contentAdmMsgBox(emailIdArray[0],strFrom, mailSubject, msgBoxContentUtility.mailForBankGuaranteeRequest(c_isPhysicalPrComplete,BankName,obj,accPaymentService.getUserTypeId(emailIdArray[0]),session.getAttribute("userTypeId").toString(),accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailIdArray[0]))));
                                String mailText = mailContentUtility.mailForBankGuaranteeRequest(c_isPhysicalPrComplete,BankName,obj,accPaymentService.getUserTypeId(emailIdArray[0]),session.getAttribute("userTypeId").toString(),accPaymentService.getProcurementRoleID(accPaymentService.getUserId(emailIdArray[0])));
                                sendMessageUtil.setEmailTo(emailIdArray);
                                sendMessageUtil.setEmailSub(mailSubject);
                                sendMessageUtil.setEmailMessage(mailText);
                                sendMessageUtil.sendEmail();
                                if("2".equalsIgnoreCase(accPaymentService.getUserTypeId(emailIdArray[0])))
                                {
                                    mobileno = accPaymentService.getMobileNoWithCountryCode(accPaymentService.getUserId(emailIdArray[0]));
                                }
                                else
                                {
                                    mobileno = accPaymentService.getPEMobileNoWithCountryCode(accPaymentService.getUserId(emailIdArray[0]));
                                }
                                sendMessageUtil.setSmsNo(mobileno);
                                StringBuilder sb = new StringBuilder(); 
                                sb.append("Dear User,%0AThis is to inform you that Bank "+BankName+" has issued New Performance Security ");
                                sb.append("for Contract No."+obj[1].toString() +" ("+ obj[15].toString() +")"+" ");
                                sb.append("%0AThanks,%0AeGP System");
                                sendMessageUtil.setSmsBody(sb.toString());
                                sendMessageUtil.sendSMS();
                            }
                            strTo =null;
                            sendMessageUtil=null;
                            mailContentUtility=null;
                            msgBoxContentUtility=null;
                        }
                    }

                    // Dohatec Start
                    if("Bank Guarantee".equalsIgnoreCase(paymentFor))
                    {
                        if((!"".equalsIgnoreCase(request.getParameter("hdnPSAmount")) || !request.getParameter("hdnPSAmount").isEmpty())  && "ICT".equalsIgnoreCase(request.getParameter("hdnIsICT")))
                        {
                            for(int i = 0; i < totalPayId; i++)
                            {
                                commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_Cms_NewBankGuarnatee", "paymentId="+regPayIdArray[i], "bankGId="+bgIds[i]).get(0);
                            }
                        }
                        else
                        {
                            commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_Cms_NewBankGuarnatee", "paymentId="+regPayId, "bankGId="+bgId).get(0);
                        }
                    }
                    
                    // Dohatec End
                    if (commonMsgChk.getFlag().equals(true)) 
                    {
                        if(isEditCase) 
                        {
                            // START CODE: TO UPDATE isLive Status of Old Record
                            //Dohatec Start
                            if((!"".equalsIgnoreCase(request.getParameter("hdnPSAmount")) || !request.getParameter("hdnPSAmount").isEmpty())  && "ICT".equalsIgnoreCase(request.getParameter("hdnIsICT")))
                            {
                                for(int j=0;j<totalPayId;j++)
                                {
                                    try
                                    {
                                        commonMsgChk = addUpdate.addUpdOpeningEvaluation("UpdatePayment",editRegPayIdArray[j]).get(0); 
                                        //commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_TenderPayment", "isLive='no'", "tenderPaymentId=" + editRegPayIdArray[j]).get(0);
                                        commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_Cms_NewBankGuarnatee", "paymentId="+regPayIdArray[j], "bankGId="+bgIds[j]).get(0);
                                        auditAction = "Edit "+paymentFor+" Payment";
                                        if("Bank Guarantee".equalsIgnoreCase(paymentFor))
                                        {
                                            auditAction = "Edit New Performance Security Payment";
                                        }
                                    }
                                    catch(Exception e)
                                    {
                                        auditAction = "Error in "+auditAction+" :"+e.getMessage();
                                    }
                                    finally
                                    {
                                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(tenderId), "tenderId", EgpModule.Bank_Module.getName(), auditAction, comments);
                                    }
                                }
                            }
                            else
                            {   // Old version
                                try
                                {
                                    commonMsgChk = addUpdate.addUpdOpeningEvaluation("UpdatePayment",editPaymentId).get(0);
                                    //commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_TenderPayment", "isLive='no'", "tenderPaymentId=" + editPaymentId).get(0);
                                    commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_Cms_NewBankGuarnatee", "paymentId="+regPayId, "bankGId="+bgId).get(0);
                                    auditAction = "Edit "+paymentFor+" Payment";
                                    if("Bank Guarantee".equalsIgnoreCase(paymentFor))
                                    {
                                        auditAction = "Edit New Performance Security Payment";
                                    }
                                }
                                catch(Exception e)
                                {
                                    auditAction = "Error in "+auditAction+" :"+e.getMessage();
                                }
                                finally
                                {
                                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(tenderId), "tenderId", EgpModule.Bank_Module.getName(), auditAction, comments);
                                }
                            }
                            
                            // Dohatec End
                            if (commonMsgChk.getFlag().equals(true)) 
                            {
                                // START CODE: TO UPDATE PAYMENT ID IN TENDER REFERENCE DOCS TABLE
                                // Dohatec Start
                                if((!"".equalsIgnoreCase(request.getParameter("hdnPSAmount")) || !request.getParameter("hdnPSAmount").isEmpty())  && "ICT".equalsIgnoreCase(request.getParameter("hdnIsICT")))
                                {
                                    for(int j=0;j<totalPayId;j++)
                                    {
                                        commonMsgChk = commonXMLSPService.insertDataBySP("updatetable", "tbl_TenderPaymentDocs", "tenderPaymentId=" + regPayIdArray[j], "tenderPaymentId=" + editRegPayIdArray[j]).get(0);
                                    }
                                }
                                // Dohatec End
                                
                                // END CODE: TO UPDATE PAYMENT ID IN TENDER REFERENCE DOCS TABLE
                                // Dohatec Work Start Here - If Payment Type is Performance Security then Redirect to this page  - Wahid Abdullah
                                if(payTyp.equalsIgnoreCase("ps")) 
                                {
                                    path = "TenderPaymentDocsUpload.jsp?tenderId=" +tenderId+"&tenderPaymentId="+regPayId+"&payTyp="+payTyp+"&uId="+payUserId+"&lotId="+pkgLotId;
                                }
                                // Dohatec Work End Here - If Payment Type is Performance Security then Redirect to this page - Wahid Abdullah
                                else 
                                {
                                    path = "TenderPaymentDetails.jsp?payId=" + regPayId+ "&uId=" + payUserId + "&tenderId="+tenderId+"&lotId="+pkgLotId+"&payTyp="+payTyp+ "&msgId=updated&bgId="+bgId;
                                }
                        
                            } 
                            else 
                            {
                                path = "TenderPayment.jsp?payId=" + editPaymentId + "&uId=" + payUserId + "&tenderId="+tenderId+"&lotId="+pkgLotId+"&payTyp="+payTyp+"&msgId=error&bgId="+bgId;
                            }
                            // END CODE: TO UPDATE isLive Status of Old Record
                        } 
                        else 
                        {
                            // Dohatec Work Start Here - If Payment Type is Performance Security then Redirect to this page  - Wahid Abdullah
                            if(payTyp.equalsIgnoreCase("ps")) 
                            {
                                path = "TenderPaymentDocsUpload.jsp?tenderId=" +tenderId+"&tenderPaymentId="+regPayId+"&payTyp="+payTyp+"&uId="+payUserId+"&lotId="+pkgLotId;
                            }
                            // Dohatec Work End Here - If Payment Type is Performance Security then Redirect to this page - Wahid Abdullah
                            else 
                            {
                                path = "TenderPaymentDetails.jsp?payId=" + regPayId + "&uId=" + payUserId + "&tenderId="+tenderId+"&lotId="+pkgLotId+"&payTyp="+payTyp+ "&msgId=payment&bgId="+bgId;
                            }
                    
                        }
                    } 
                    else
                    {
                        if(isEditCase || isRenew)
                        {   
                            path = "TenderPayment.jsp?payId=" + regPayId + "&uId=" + payUserId + "&tenderId="+tenderId+"&lotId="+pkgLotId+"&payTyp="+payTyp+"&msgId=error&bgId="+bgId;
                        } 
                        else 
                        {
                            path = "TenderPayment.jsp?uId=" + payUserId + "&tenderId="+tenderId+"&lotId="+pkgLotId+"&payTyp="+payTyp+"&msgId=error&bgId="+bgId;
                        }
                    }      
                } 
                else 
                {
                    // Redirect for Amount Error
                    if(isRecrdExists)
                    {
                        path = "TenderPayment.jsp?uId=" + payUserId + "&tenderId="+tenderId+"&lotId="+pkgLotId+"&payTyp="+payTyp+"&msgId=allexists&bgId="+bgId;
                    }
                    else
                    {
                        path = "TenderPayment.jsp?uId=" + payUserId + "&tenderId="+tenderId+"&lotId="+pkgLotId+"&payTyp="+payTyp+"&msgId=amterror&bgId="+bgId;
                    }
                }
            }
        } 
        else 
        {
            path = request.getContextPath() + "/SessionTimedOut.jsp";
        }
        response.sendRedirect(path);
        // END CODE: TO INSERT PAYMENT DATA
%>

