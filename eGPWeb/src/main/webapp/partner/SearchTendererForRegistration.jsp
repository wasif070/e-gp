<%--
    Document   : SearchTendererForRegistration
    Created on : Jan 21, 2011, 11:05:19 PM
    Author     : Karan
--%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%
            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userId").toString();
            }
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search e-mail ID for Registration Fee Payment</title>
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
         <script  src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#frmSearchTenderer').validate({
        rules:{ txtEmailId: {required:true, email:true}
        },
        messages:{txtEmailId: {required:"<div class='reqF_1'>Please enter e-mail ID</div>",
                email: "<div class='reqF_1'>Please enter valid e-mail ID</div>" }
        }
    });
});
</script>
<%--<script type="text/javascript">
function submitForm(e){
    if(e.keyCode==13){
        document.getElementById("frmSearchTenderer").submit();
    }
}
</script>--%>
    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        if (session.getAttribute("userId") != null) {
                            tenderCommonService.setLogUserId(session.getAttribute("userId").toString());
                        }
                        boolean isBankUser = false;
                        String userId = "", srchUserId = "";
                        boolean isBranchMaker = false, isBranchChecker = false, isBankChecker = false;
                        String userBranchId = "";

                 HttpSession hs = request.getSession();
             if (hs.getAttribute("userId") != null) {
                     userId = hs.getAttribute("userId").toString();
                        } /*else {
                            response.sendRedirectFliter(request.getContextPath() + "/SessionTimedOut.jsp");
                        }*/

                String referer = "";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");
                            
                }

                        if (hs.getAttribute("userTypeId") != null) {
                            if ("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())) {
                                isBankUser = true; // userType is ScheduleBank";
                     }
                }

                if (request.getParameter("btnSearch") == null) {
                            if (request.getParameter("uId") != null) {
                                if (request.getParameter("uId") != "" && !request.getParameter("uId").equalsIgnoreCase("null")) {
                                    srchUserId = request.getParameter("uId");
                       }
                    }
                }

                        List<SPTenderCommonData> lstCurBankUserRole = tenderCommonService.returndata("getBankUserRole", userId, null);
                        if (!lstCurBankUserRole.isEmpty()) {
                            if ("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                                isBranchMaker = true;
                            } else if ("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                                isBranchChecker = true;
                            } else if ("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                                isBankChecker = true;
                    }
                            userBranchId = lstCurBankUserRole.get(0).getFieldName2();
                }


                %>
                <div class="contentArea_1">
                    <div class="pageHead_1">Search e-mail ID for Registration Fee Payment
                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('5');">Save as PDF</a></span>
                    </div>

                    <div class="tabPanelArea_1 t_space">
                        <div class="table-section-header_title">Search Bidder/Consultant or Individual Consultants</div>
                        <div style="font-style: italic" class="formStyle_1 t_space b_space ff">Fields marked with (<span class="mandatory">*</span>) are mandatory</div>
                            <form id="frmSearchTenderer" action="SearchTendererForRegistration.jsp" method="POST">
                                <input type="hidden" name="searchAction" id="searchAction" value="Search" />
                            <table width="100%" cellspacing="10" cellpadding="10" class="formBg_1 tableView_1" >
                                <tr>
                                    <td width="7%" class="t-align-left ff" style="vertical-align: text-top">e-mail ID : <span class="mandatory">*</span></td>
                                    <td width="22%" class="t-align-left">
                                        <input name="txtEmailId" type="text" class="formTxtBox_1" id="txtEmailId" style="width:95%;" />
                                    </td>
                                    <td width="71%" class="t-align-left" style="vertical-align: text-top">
                                        <label class="formBtn_1">
                                            <input name="btnSearch" id="btnSearch" type="submit" value="Search"  />
                                        </label>
                                    </td>
                                </tr>
                            </table>
                        </form>

                        <%
                                int recordsCnt = 0;
                                boolean isEmailVerifed = false, isPaymentDone = false, isPaymentVerified = false, isRegValidityExpired=false;
                                String bidderUserId = "0", bidderEmail = "", bidderCompany = "", regPaymentId = "0", payStatus = "";

                            if (request.getParameter("searchAction") != null || !"".equalsIgnoreCase(srchUserId)) {
                                    String cmdName = "", emailId = "", bankUserId = "", paidBranchId = "0";


                                    if (!"".equalsIgnoreCase(srchUserId)) {

                                        List<SPTenderCommonData> lstemails = tenderCommonService.returndata("getEmailIdfromUserId", srchUserId, null);
                                        if (!lstemails.isEmpty()) {                                            
                                            emailId = lstemails.get(0).getFieldName1();                                                   
                                    }
                                    //emailId = request.getParameter("txtEmailId");
                                } else {
                                    emailId = request.getParameter("txtEmailId");
                                }

                                //emailId = request.getParameter("txtEmailId");
                                cmdName = "SearchEmailForRegFee";
                                bankUserId = userId;
                                for (SPTenderCommonData sptcd : tenderCommonService.returndata(cmdName, emailId, null)) {
                                    recordsCnt++;
                                        bidderUserId = sptcd.getFieldName1();
                                        bidderEmail = sptcd.getFieldName2();
                                        bidderCompany = sptcd.getFieldName4();
                                        if ("yes".equalsIgnoreCase(sptcd.getFieldName5())) {
                                            isEmailVerifed = true;
                                    }
                                        regPaymentId = sptcd.getFieldName6();
                                    //sptcd.getFieldName1();

                                        if (!"null".equalsIgnoreCase(sptcd.getFieldName7())) {
                                            payStatus = sptcd.getFieldName7();
                                    } else {
                                            payStatus = "Pending";
                                    }

                                        if ("yes".equalsIgnoreCase(sptcd.getFieldName9())) {
                                            isPaymentVerified = true;
                                    }

                                    paidBranchId = sptcd.getFieldName10();
                                }

                                        if (recordsCnt > 0) {%>
                    <%if (isEmailVerifed) {

                        List<SPTenderCommonData> lstRegValStatus = 
                                tenderCommonService.returndata("getRegValidityStatus", bidderUserId, null);
                        if(!lstRegValStatus.isEmpty()){
                            if("No".equalsIgnoreCase(lstRegValStatus.get(0).getFieldName1())){
                                isRegValidityExpired=true;
                            }
                        }
                        lstRegValStatus = null;
    %>
                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                            <!--                                            <th width="30%" class="t-align-center">Company Name</th>-->
                                            <th width="30%" class="t-align-center" width="26%">e-mail ID</th>
                                            <th width="10%" class="t-align-center" width="13%">Payment Status</th>
                                            <th width="30%" class="t-align-center" width="13%">Action</th>
                                        </tr>
                                        <tr>
                                            <td><%=bidderEmail%></td>
                                            <td class="t-align-center">
                                <%//=payStatus%>
                                <%
                                   List<SPTenderCommonData> lstRenewed_ExpiryDts = tenderCommonService.returndata("getRenewed_ExpiryDt", regPaymentId, null);
                                   String expiryDt = "",currentDt="", isRenew="false";
                                   Date expiryDate=null, currentDate=null;
                                   SimpleDateFormat dtFormat = new SimpleDateFormat("dd-MMM-yyyy");
                                   currentDt= dtFormat.format(new Date());
                                   Calendar calExpiry = Calendar.getInstance(), calCurrent= Calendar.getInstance();
                                   if (!lstRenewed_ExpiryDts.isEmpty()) {
                                       expiryDt = lstRenewed_ExpiryDts.get(0).getFieldName2();
                                       expiryDate = DateUtils.formatStdString(DateUtils.formatStdDate(DateUtils.convertStringtoDate(expiryDt, "dd-MMM-yyyy")));
                                       currentDate = DateUtils.formatStdString(DateUtils.formatStdDate(DateUtils.convertStringtoDate(currentDt, "dd-MMM-yyyy")));
                                       calExpiry.setTime(expiryDate);
                                       calCurrent.add(Calendar.DATE, +30);
                                       if(expiryDate!=null && (calExpiry.equals(calCurrent) || calExpiry.before(calCurrent))){
                                            isRenew="true";
                                       }
                                       calExpiry.add(Calendar.YEAR, 1);
                                       expiryDate=calExpiry.getTime();
                                   }

                                if(expiryDate!=null && expiryDate.before(currentDate) ){
                                out.print("Tender/consultant cannot be renewed  as Renewal period has been lapsed");
                                }
                                 else{
                                if (!"Pending".equalsIgnoreCase(payStatus) && !isPaymentVerified) {%>
                                    Verification is Pending
                                <%} else if (!"Pending".equalsIgnoreCase(payStatus) && isPaymentVerified && isRegValidityExpired) {%>
                                <%if(!"Freezed".equalsIgnoreCase(payStatus)){%>
                                    Renewal is Pending
                                    <%}else{
                                    out.print(payStatus);
                                    }%>
                                <%} else {%>                                    
                                <%=payStatus%>
                                <%}}%>
                            </td>
                            <td class="t-align-center">
                                <%if ("pending".equalsIgnoreCase(payStatus)) {%>
                                                <a href="RegistrationFeePayment.jsp?uId=<%=bidderUserId%>">Make Payment</a>
                                                <%} else {%>
                                <%if (paidBranchId.equalsIgnoreCase(userBranchId)) {%>

                                <%if (isPaymentVerified) {%>
                                    <%if("Freezed".equalsIgnoreCase(payStatus)){%>
                                    <a href="RegistrationFeePaymentDetails.jsp?payId=<%=regPaymentId%>&uId=<%=bidderUserId%>">View Payment Details</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                    <a href="RegistrationFeePayment.jsp?uId=<%=bidderUserId%>&freezed=yes">Make Payment</a>
                                    <%}else {
                                                   if(expiryDate!=null && (expiryDate.after(currentDate) || expiryDate.equals(currentDate) )){%>
                                                   <a href="RegistrationFeePaymentDetails.jsp?payId=<%=regPaymentId%>&uId=<%=bidderUserId%>">View Payment Details</a>
                                                   <%if(isRenew.equalsIgnoreCase("true")){%>
                                                    &nbsp;&nbsp;|&nbsp;&nbsp;<a href="RegistrationFeePayment.jsp?uId=<%=bidderUserId%>&action=renew">Renew</a>
                                                    <%}%>
                                                    <%} else {%>
                                                     Bidder/Consultant cannot be renewed.
                                    <%}}%>
                               <%} else {%>
                                <a href="RegistrationFeePayment.jsp?payId=<%=regPaymentId%>&uId=<%=bidderUserId%>&action=edit">Edit Payment Details</a>
                                &nbsp;&nbsp;|&nbsp;&nbsp;<a href="RegistrationFeePaymentDetails.jsp?payId=<%=regPaymentId%>&uId=<%=bidderUserId%>">View Payment Details</a>
                                <%}%>
                                  <%} else {%>
                                        Payment already done.
                                <%if (isPaymentVerified) {%>
                                <%if("Freezed".equalsIgnoreCase(payStatus)){%>
                                <a href="RegistrationFeePayment.jsp?uId=<%=bidderUserId%>&freezed=yes">Make Payment</a>
                                <%}else{%>
                                                    &nbsp;&nbsp;|&nbsp;&nbsp;<a href="RegistrationFeePayment.jsp?uId=<%=bidderUserId%>&action=renew">Renew</a>
                                                    <%}} else {%>
                                                    Verification Pending
                                                    <%}%>
                                                <%}%>

                                                <%}%>
                                            </td>
                                        </tr>
                                    </table>
                                    <%} else {%>
                                    <div class="t-align-center t_space">
                                        <strong>Bidder/Consultant e-mail ID (<%=emailId%>) is not verified.</strong></div>
                                    <%}%>
                                <%} else {%>
                                <div class="t-align-center t_space">
                                    <strong>Bidder/Consultant with e-mail ID (<%=emailId%>) does not exist.</strong></div>
                                <%}
                            }
                        %>



                    </div>

                <div class="tabPanelArea_1 t_space">
                    <%@include  file="RegFeePaymentDailyTrans.jsp" %>
                </div>

            </div>
            <!--For Generate PDF  Starts-->
            <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                    String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="RegistrationFee_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="RegistrationFee" />
            </form>
            <!--For Generate PDF  Ends-->
                <!--Dashboard Content Part End-->



                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->


        </div>
    </body>
       <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
