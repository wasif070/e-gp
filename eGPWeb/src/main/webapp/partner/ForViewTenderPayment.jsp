<%-- 
    Document   : ForViewTenderPayment
    Created on : Jun 9, 2011, 2:07:26 PM
    Author     : Karan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payment</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <body>
 <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->

                <!--Dashboard Content Part Start-->
                <%
                    String referer = "";
                    if (request.getHeader("referer") != null) {referer = request.getHeader("referer");}
                %>
                <div class="contentArea_1">
                    <div class="pageHead_1">
                        &nbsp;Payment
                        <span style="float: right;" ><a href="<%=referer%>" class="action-button-goback">Go Back</a></span>
                    </div>

<%
TenderCommonService objTCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
if (session.getAttribute("userId") != null) {
                objTCS.setLogUserId(session.getAttribute("userId").toString());
}
HttpSession hs = request.getSession();
boolean isTenderer = false;
boolean isBankUser= false;
boolean isPEUser = false;
//boolean isBankUser= false;
//boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false;
boolean showDocFeesTab=true, showTStab=true, showPStab=true;
boolean isTenderPublished=false;
// boolean isOfflinePay = false, isBankPay=false; // for docFeesMode
String tenderId="", pkgLotId="", payTyp="", userId="", currUserTypeId="";
String tenderStatus="", docFeesMethod="", docFeesMode="";
String docFeesPayId="0";

if (request.getParameter("tenderId") !=null){
    tenderId=request.getParameter("tenderId");
}

 if (hs.getAttribute("userId") != null) {
    userId = hs.getAttribute("userId").toString();
  }

/* Start: CODE TO SET CURRENT USER TYPE */
  if (hs.getAttribute("userTypeId") != null) {
         currUserTypeId = hs.getAttribute("userTypeId").toString();
       if(currUserTypeId.equalsIgnoreCase("2")){
            isTenderer=true; // userType is Tenderer";

         }else if(currUserTypeId.equalsIgnoreCase("3")){
                isPEUser=true; // userType is Officer";
             }else if(currUserTypeId.equalsIgnoreCase("7")){
                isBankUser=true; // userType is ScheduleBank";
             }
   }


    if (request.getParameter("lotId") != null){
            if (request.getParameter("lotId")!="" && !request.getParameter("lotId").equalsIgnoreCase("null")){
               pkgLotId=request.getParameter("lotId");
            }
    }

    if (request.getParameter("payTyp") != null){
            if (request.getParameter("payTyp")!="" && !request.getParameter("payTyp").equalsIgnoreCase("null")){
               payTyp=request.getParameter("payTyp");
            }
    }

/* End: CODE TO SET CURRENT USER TYPE */

/* Start: CODE TO GET BANK USER ROLE */
/*
List<SPTenderCommonData> lstCurBankUserRole = objTCS.returndata("getBankUserRole",userId,null);
if(!lstCurBankUserRole.isEmpty()){
    if("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
        isBranchMaker=true;
    } else if("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
        isBranchChecker=true;
    } else if("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
        isBankChecker=true;
    }
}
 */
/* End: CODE TO GET BANK USER ROLE */

 List<SPTenderCommonData> lstBasicTenderInfo = objTCS.returndata("getBasicInfoForViewPayment", tenderId, userId);
 if (!lstBasicTenderInfo.isEmpty() && lstBasicTenderInfo.size() > 0) {

    tenderStatus=lstBasicTenderInfo.get(0).getFieldName3();
    docFeesMode=lstBasicTenderInfo.get(0).getFieldName4();

   if ("pending".equalsIgnoreCase(tenderStatus)){
        if (request.getParameter("msgId")==null){
            response.sendRedirect("ForTenderPayment.jsp?tenderId=" + tenderId + "&msgId=pending");
        }
     } else {
        isTenderPublished=true; // Tender is Published
     }

    /*
    if (docFeesMode.equalsIgnoreCase("offline")){
        isOfflinePay= true;
    } else if (docFeesMode.equalsIgnoreCase("bank")){
        isBankPay = true;
    } else if (docFeesMode.equalsIgnoreCase("offline/bank")) {
        isOfflinePay = true; isBankPay = true;
    }
    */


    if("null".equalsIgnoreCase(lstBasicTenderInfo.get(0).getFieldName1())){
        showDocFeesTab=false; // Documents are Free
    } else {
        docFeesMethod=lstBasicTenderInfo.get(0).getFieldName1(); // Documents are Paid
    }

    if("free".equalsIgnoreCase(lstBasicTenderInfo.get(0).getFieldName2())){
        showTStab=false; // Tender Security is Free
    }

     if("free".equalsIgnoreCase(lstBasicTenderInfo.get(0).getFieldName5())){
        showDocFeesTab=false; // Documents are Free
   }

    if("No".equalsIgnoreCase(lstBasicTenderInfo.get(0).getFieldName6())){
     showDocFeesTab = false;
    }

    if("No".equalsIgnoreCase(lstBasicTenderInfo.get(0).getFieldName7())){
     //showTStab = false;
    }

    if("No".equalsIgnoreCase(lstBasicTenderInfo.get(0).getFieldName8())){
     showPStab = false;
    }

    if(!"0".equalsIgnoreCase(lstBasicTenderInfo.get(0).getFieldName9())){
        docFeesPayId = lstBasicTenderInfo.get(0).getFieldName9();
    }

   }
 %>
                <% pageContext.setAttribute("tenderId", request.getParameter("tenderId")); %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <div>&nbsp;</div>

<!--                <div class="tabPanelArea_1">-->

                <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error.";
                        } else if(msgId.equalsIgnoreCase("pending")){
                           isError=true; msgTxt="Payment details can not be entered. Tender not yet published.";
                        } else if(msgId.equalsIgnoreCase("tsfree")){
                           isError=true; msgTxt="Payment not allowed. Tender Security is not payable.";
                        } else if(msgId.equalsIgnoreCase("dfpending")){
                           isError=true; msgTxt="Payment not allowed. Document Fees is not yet paid.";
                        } else if(msgId.equalsIgnoreCase("tspaid")){
                           isError=true; msgTxt="Payment not allowed. Tender Security is already paid.";
                        } else if(msgId.equalsIgnoreCase("offlinepay")){
                           isError=true; msgTxt="Payment not allowed. Mode of payment is Offline";
                        } else if(msgId.equalsIgnoreCase("nopaymentmode")){
                           isError=true; msgTxt="Payment not allowed. Mode of payment not specified";
                        } else if(msgId.equalsIgnoreCase("noaccess")){
                           isError=true; msgTxt="Payment not allowed. You don't have rights.";
                        }  else {
                            msgTxt="";
                        }

                       
                    %>
                   <%if (isError){%>
                   <div class="responseMsg errorMsg"><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg"><%=msgTxt%></div>
                   <%}%>
                <%}}%>


                 <%
    if (isTenderer) {
         pageContext.setAttribute("tab", "7");
    %>
    <%@include file="../tenderer/TendererTabPanel.jsp" %>
    <%} else if (isPEUser) {
        pageContext.setAttribute("tab", "8");
    %>
    <%@include file="../officer/officerTabPanel.jsp" %>
    <%}%>

    <div class="tabPanelArea_1">
                <%if (isTenderPublished) {%>
                <div>
                     <ul class="tabPanel_1">
                        <%if (showDocFeesTab) {%>
                            <%if ("package wise".equalsIgnoreCase(docFeesMethod)) {%>                            
                            <li><a href="ViewTenderPaymentDetails.jsp?payId=<%=docFeesPayId%>&uId=<%=userId%>&tenderId=<%=tenderId%>&lotId=0&payTyp=df" <% if (payTyp.equalsIgnoreCase("df")) {%>class="sMenu"<%}%> >Document Fees</a></li>
                            <%} else if ("lot wise".equalsIgnoreCase(docFeesMethod)) {%>
                                <li><a href="LotPayment.jsp?tab=8&tenderId=<%=tenderId%>&payTyp=df" <% if (payTyp.equalsIgnoreCase("df")) {%>class="sMenu"<%}%> >Document Fees</a></li>
                            <%}%>
                        <%}%>

                        <%if (showTStab) {%>
                                <li><a href="LotPayment.jsp?tenderId=<%=tenderId%>&payTyp=ts" <% if (payTyp.equalsIgnoreCase("ts")){%>class="sMenu"<%}%> >Tender Security</a></li>
                        <%}%>
                        <%if(showPStab){%>
                        <li><a href="LotPayment.jsp?tenderId=<%=tenderId%>&payTyp=ps" <% if (payTyp.equalsIgnoreCase("ps")){%>class="sMenu"<%}%> >Performance Security</a></li>
                        <%}%>
                    </ul>
                </div>


                    <div class="tabPanelArea_1">
                      
                          <%if(!showPStab && !showTStab && !showDocFeesTab){%>
                          <div class='responseMsg noticeMsg'>
                          No Payment information found.
                          </div>
                          <%} else {%>
<!--
<div class='responseMsg noticeMsg'>
Select appropriate tab for respective payment.
</div>
-->
                          <%}%>
                       
                          <div class='responseMsg noticeMsg'>
                              Please select an appropriate tab to view the payment details.
                          </div >
                          <!-- START: COMMON PACKAGE/LOT DESCRIPTION FILE -->
                          <div class="t_space">
                              <%@include file="CommonPackageLotDescription.jsp" %>
                          </div>
                        <!-- END COMMON PACKAGE/LOT DESCRIPTION FILE -->
                    </div>

                 <%}%>

   </div>
<!--   </div>-->
                <div>&nbsp;</div>

                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->

                   </div>

            </div>
                <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    </body>
</html>
