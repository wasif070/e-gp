<%--
    Document   : TenderPaymentDetails
    Created on : Mar 3, 2011, 1:52:57 PM
    Author     : Karan
--%>

<%@page import="java.net.URLDecoder"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsCompansiateAmt"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AccPaymentService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payment Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
         <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<!--         <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>-->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>

        <script type="text/javascript">
            /* Call Print function */
            $(document).ready(function() {
                if (document.getElementById("print")!=null){
                    $('#print').show();
                    $("#print").click(function() {
                    printElem({leaveOpen: true, printMode: 'popup'});
                });
                }

            });
             
                function printElem(options){
                if (document.getElementById("print_area")!=null){
                    $('#divOfficerTab').hide();
                    $('#print_area').printElement(options);
                    $('#divOfficerTab').show();
                }
            }
        </script>
    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%
                    //TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                TenderCommonService objTSC = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                CommonSearchDataMoreService objTpayment = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    if (session.getAttribute("userId") != null) {
                        objTSC.setLogUserId(session.getAttribute("userId").toString());
                    }
                boolean isCurrUserPE = false;
                boolean isBankUser=false;
                    boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false, isRedirect=false;
                    String userBranchId="";
                    String userId="", payUserId="", regPaymentId="", redirectPath="";
                    String tenderId="0", pkgLotId="0", payTyp="", paymentTxt="",reqId="",opTyp="",reqComments="";
                %>
                <%
                 HttpSession hs = request.getSession();
                 if (hs.getAttribute("userId") != null) {
                         userId = hs.getAttribute("userId").toString();
                  } else {//response.sendRedirectFILTER(request.getContextPath() + "/SessionTimedOut.jsp");
                  }

                String referer = "";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");
                }

                if ( hs.getAttribute("userTypeId")!= null) {
                    if("7".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())){
                        isBankUser=true; // userType is ScheduleBank";
                     }
                }

                if (request.getParameter("uId") != null) {
                    payUserId = request.getParameter("uId");
                }

                if (request.getParameter("payId") != null) {
                    regPaymentId = request.getParameter("payId");
                }
                if (request.getParameter("reqId") != null) {
                    reqId = request.getParameter("reqId");
                }
                String bgId= "";
                if (request.getParameter("bgId") != null) {
                    bgId = request.getParameter("bgId");
                }

                if (request.getParameter("tenderId") !=null){
                        tenderId=request.getParameter("tenderId");
                    }
                     if (request.getParameter("lotId") != null){
                        if (request.getParameter("lotId")!="" && !request.getParameter("lotId").equalsIgnoreCase("null")){
                           pkgLotId=request.getParameter("lotId");
                        }
                    }

                     if (request.getParameter("payTyp") != null){
                        if (request.getParameter("payTyp")!="" && !request.getParameter("payTyp").equalsIgnoreCase("null")){
                           payTyp=request.getParameter("payTyp");

                            if ("df".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Document Fees";
                             } else if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Tender/Proposal Security";
                             } else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Performance Security";
                             }else if ("bg".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "New Performance Security";
                             }
                        }
                    }

                if(request.getParameter("btnOK")!=null){
                    isRedirect=true;

                    if("ps".equalsIgnoreCase(payTyp)){
                        redirectPath="NOAIssuedList.jsp?tenderId="+tenderId;
                    } else {
                        redirectPath="SearchTendererForTenPayment.jsp?tenderId="+tenderId+"&lotId="+pkgLotId+"&payTyp="+payTyp;
                    }

                } else if(request.getParameter("btnEdit")!=null){
                    isRedirect=true;
                    redirectPath="TenderPayment.jsp?payId="+regPaymentId+"&uId="+payUserId+"&tenderId="+tenderId+"&lotId="+pkgLotId+"&payTyp="+payTyp+"&action=edit&bgId="+bgId;
                }
                if(isRedirect){
                    response.sendRedirect(redirectPath);
                }

                List<SPTenderCommonData> lstCurBankUserRole = objTSC.returndata("getBankUserRole", userId, null);
                if (!lstCurBankUserRole.isEmpty()) {
                    if ("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                        isBranchMaker = true;
                    } else if ("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                        isBranchChecker = true;
                    } else if ("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())) {
                        isBankChecker = true;
                    }
                    userBranchId = lstCurBankUserRole.get(0).getFieldName2();
                }


                 /* Start: CODE TO CHECK PE USER */
                List<SPTenderCommonData> listChkCurrUserPE = objTSC.returndata("getPEOfficerUserIdfromTenderId", tenderId, null);

                if (!listChkCurrUserPE.isEmpty()) {
                    if (userId.equalsIgnoreCase(listChkCurrUserPE.get(0).getFieldName1())) {
                        isCurrUserPE = true;
                    }
                }

                listChkCurrUserPE = null;
                /* End: CODE TO CHECK PE USER */
                CommonService commonservice = (CommonService) AppContext.getSpringBean("CommonService");
                String procnaturee = commonservice.getProcNature(request.getParameter("tenderId")).toString();

                // Dohatec Start
                // Tender is ICT or not
                List<SPTenderCommonData> listICT = objTSC.returndata("chkDomesticPreference", tenderId, null);
                boolean isIctTender = false;
                if(!listICT.isEmpty() && listICT.get(0).getFieldName1().equalsIgnoreCase("true")){
                    isIctTender = true;
                }
                // Dohatec End
                %>
                <div class="contentArea_1">
                    <div class="pageHead_1">Payment Details
                        <span style="float:right;">
                        <%
String strRefRedirect = "";   
if(isBankUser) {
    if(isBranchMaker){
        if("ps".equalsIgnoreCase(payTyp)){
            strRefRedirect = "NOAIssuedList.jsp?tenderId=" + tenderId;
        }else if("bg".equalsIgnoreCase(payTyp)){
            strRefRedirect = "BankGuaranteeListing.jsp?tenderId=" + tenderId;
        } else {
            if (referer.toString().toLowerCase().contains("searchtenderpayments.jsp")) {
                strRefRedirect = "SearchTenderPayments.jsp";
            } else {
                strRefRedirect = "SearchTendererForTenPayment.jsp?uId=" + payUserId + "&tenderId=" + tenderId + "&lotId=" + pkgLotId + "&payTyp=" + payTyp;
            }
        }
    } else if(isBranchChecker || isBankChecker){
         strRefRedirect = "TenPaymentListing.jsp";
    }
} else if(isCurrUserPE){
    if(request.getParameter("view")!=null && "CMS".equalsIgnoreCase(request.getParameter("view"))){
            if("services".equalsIgnoreCase(procnaturee))
            {
                strRefRedirect = ""+request.getContextPath()+"/officer/InvoiceServiceCase.jsp?tenderId=" + tenderId;
            }else{
                strRefRedirect = ""+request.getContextPath()+"/officer/Invoice.jsp?tenderId=" + tenderId;
            }}else{strRefRedirect = "PaidTendererList.jsp?tenderId=" + tenderId + "&lotId=" + pkgLotId + "&payTyp=" + payTyp;}
}
%>
                  <a href="<%=strRefRedirect%>" class="action-button-goback">Go Back</a>
                        </span>
                    </div>

               <div id="resultDiv">
                        <div id="print_area">
                       
                        <% pageContext.setAttribute("tenderId", tenderId); %>
                    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                     <div>&nbsp;</div>
                      <% if (isCurrUserPE) {
        pageContext.setAttribute("tab", "8");
    %>
    <div id="divOfficerTab">
    <%@include file="../officer/officerTabPanel.jsp" %>
    </div>
    <%}%>

                    <div class="tabPanelArea_1 t_space">

                        <%if (request.getParameter("msgId")!=null){
                    String msgId="", msgTxt="";
                    boolean isError=false;
                    msgId=request.getParameter("msgId");
                    if (!msgId.equalsIgnoreCase("")){
                        if(msgId.equalsIgnoreCase("payment")){
                            msgTxt="Payment information entered successfully";
                        } else if(msgId.equalsIgnoreCase("updated")){
                            msgTxt="Payment updated successfully";
                        } else if(msgId.equalsIgnoreCase("verified")){
                            msgTxt="Payment verified successfully";
                        } else if(msgId.equalsIgnoreCase("extended")){
                            msgTxt="Payment extended successfully";
                        } else  if(msgId.equalsIgnoreCase("released")){
                            msgTxt="Payment released successfully";
                        } else  if(msgId.equalsIgnoreCase("cancelled")){
                            msgTxt="Payment cancelled successfully";
                        } else  if(msgId.equalsIgnoreCase("on-hold")){
                            msgTxt="Payment put on-hold successfully";
                        } else  if(msgId.equalsIgnoreCase("forfeited")){
                            msgTxt="Payment Compensated successfully";
                        } else  if(msgId.equalsIgnoreCase("forfeitrequested")){
                            if("ts".equalsIgnoreCase(payTyp))
                            {
                                msgTxt="Payment Forfeit request submitted successfully";
                            }else{
                                msgTxt="Payment Compensate request submitted successfully";
                            }    
                        } else  if(msgId.equalsIgnoreCase("releaserequested")){
                            msgTxt="Payment release request submitted successfully";
                        } else  if(msgId.equalsIgnoreCase("error")){
                           isError=true; msgTxt="There was some error";
                        }  else {
                            msgTxt="";
                        }
                    %>
                   <%if (isError){%>
                   <div class="responseMsg errorMsg noprint" style="margin-bottom: 12px;" ><%=msgTxt%></div>
                   <%} else {%>
                        <div class="responseMsg successMsg noprint" style="margin-bottom: 12px;"><%=msgTxt%></div>
                   <%}%>
                <%}}%>

                         <%
                            boolean isPaymentVerified=false;
                            String bidderEmail="";
                            String emailId="", bankUserId="";

                            List<SPTenderCommonData> lstTendererEml = objTSC.returndata("getEmailIdfromUserId",payUserId,null);

                            if(!lstTendererEml.isEmpty()){
                                emailId=lstTendererEml.get(0).getFieldName1();
                                bidderEmail=emailId;
                            }
                            List<SPTenderCommonData> lstRequestAction = objTSC.returndata("getRequestActionFromRequestId", reqId, regPaymentId);
                            if (!lstRequestAction.isEmpty()) {
                                opTyp = lstRequestAction.get(0).getFieldName2();
                                reqComments = lstRequestAction.get(0).getFieldName3();
                            }


                            bankUserId = userId;
                            Map<String,String> userTransMap = new HashMap<String, String>();
                            
                                     //List<SPCommonSearchDataMore> lstPaymentDetail = objTpayment.geteGPData("getTenderPaymentDetail", regPaymentId, null,null,null,, null,null,null,, null,null,null,, null,null,null,, null,null,null,, null,null,null);
                                List<SPCommonSearchDataMore> lstPaymentDetail = objTpayment.geteGPData("getTenderPaymentDetail", regPaymentId);
                                    if (!lstPaymentDetail.isEmpty()) {
                                        if(lstPaymentDetail.get(0).getFieldName17()!=null){
                                            userTransMap.put(lstPaymentDetail.get(0).getFieldName17(), "7");
                                        }
                                        if(lstPaymentDetail.get(0).getFieldName18()!=null){
                                            userTransMap.put(lstPaymentDetail.get(0).getFieldName18(), "7");
                                        }
                                        if ("yes".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName8())) {
                                            isPaymentVerified = true;
                                        }
                        %>

                        <!-- START: COMMON PACKAGE/LOT DESCRIPTION FILE -->
                        <div >
                            <%@include file="CommonPackageLotDescription.jsp" %>
                        </div>
                        <!-- END COMMON PACKAGE/LOT DESCRIPTION FILE -->


                        <!-- START: COMMON PAYMENT RELATED DOCS -->

                        <div class="noprint">
                            <%@include file="CommonPaymentDocsList.jsp" %>
                        </div>

                        <!-- END COMMON PAYMENT RELATED DOCS -->
<!--                        <div id="resultDiv">
                            <div id="print_area">-->
                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td width="18%" class="ff">Action :</td>
                            <td><%if("Forfeit".equalsIgnoreCase("Compensate")){out.print("Compensate");}else{out.print("Release");}%> Payment</td>
                        </tr>
                        <%if(!"".equalsIgnoreCase(reqComments)){%>
                          <tr>
                            <td class="ff">Remarks :</td>
                            <td><%=reqComments%></td>
                        </tr>
                        <%}else{
                              if("Released".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName10()) || "Forfeited".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName10()))
                              {String sttr ="";
                                  if("Released".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName10()))
                                  {
                                    sttr = "release";
                                  }else{sttr = "forfeit";}
                                  List<SPTenderCommonData> lstReqComments = objTSC.returndata("getReqcomment",regPaymentId,sttr);
                                  if(!lstReqComments.isEmpty()){
                        %>
                                    <tr>
                                        <td class="ff">Remarks :</td>
                                        <td><%=lstReqComments.get(0).getFieldName1()%></td>
                                    </tr>
                        <%                                      
                                  }
                              }
                         }
                        %>
                        
                        
                    </table>
                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td width="18%" class="ff">Payment Status :</td>
                            <td>

                                  <% if (!"Pending".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName10()) && !isPaymentVerified) {%>
                                                Verification is Pending
                                                <%} else {%>
                                                <%if("Forfeited".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName10())){
                                                out.print("Compensated");}else{out.print(lstPaymentDetail.get(0).getFieldName10());}
                                                %>   
                                                <%}%>
                            </td>
                        </tr>
                        <tr>
                            <td width="18%" class="ff">Email ID :</td>
                            <td><%=bidderEmail%></td>
                        </tr>
                         <tr>
                            <td class="ff">Financial Institution Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName11()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Maker :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName2()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Payment For : </td>
                            <td><%=lstPaymentDetail.get(0).getFieldName12().equalsIgnoreCase("Tender Security") ? "Tender/Proposal Security" : lstPaymentDetail.get(0).getFieldName12() %></td>
                        </tr>
                        <!-- Dohatec Start -->
                                <%if(("ps".equalsIgnoreCase(payTyp) || "bg".equalsIgnoreCase(payTyp))  && isIctTender){
                                    String txtCurrencySymbol = "";
                                    String currencyType = "";
                                    String perSecAmount = "";
                                    CommonSearchDataMoreService objTSDetails = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                                    List<SPCommonSearchDataMore> lstPerSecPayDetail = null;
                                    if("ps".equalsIgnoreCase(payTyp))
                                        lstPerSecPayDetail = objTSDetails.geteGPData("getPerformanceSecurityPaymentDetail", tenderId.toString());
                                    else if("bg".equalsIgnoreCase(payTyp))
                                        lstPerSecPayDetail = objTSDetails.geteGPData("getNewPerSecDetail", tenderId.toString(), pkgLotId.toString(), payUserId.toString());

                                    String[] regPayId = new String[4];
                                    if("bg".equalsIgnoreCase(payTyp) && ("forfeitrequested".equalsIgnoreCase(request.getParameter("msgId")) || "Forfeited".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName10())))
                                    {
                                        List<SPCommonSearchDataMore> lstNewPerSecPayIDCompensate = objTSDetails.geteGPData("getNewPerSecPaymentIDDetail", tenderId.toString(), pkgLotId, payUserId.toString());
                                        System.out.print("Old Paymend id list size " + lstNewPerSecPayIDCompensate.size());
                                        if(!lstNewPerSecPayIDCompensate.isEmpty())
                                        {
                                            for(int i = 0; i< lstNewPerSecPayIDCompensate.size(); i++)
                                            {
                                                regPayId[i] = lstNewPerSecPayIDCompensate.get(i).getFieldName1();
                                                System.out.print("Old Payment Id : " + regPayId[i]);
                                            }
                                        }
                                    }

                                    if(!lstPerSecPayDetail.isEmpty())
                                    {
                                        System.out.print("\n List Size : " + lstPerSecPayDetail.size());
                                        for(int i = 0; i<lstPerSecPayDetail.size(); i++)
                                        {
                                            if("ps".equalsIgnoreCase(payTyp))
                                            {
                                                txtCurrencySymbol = lstPerSecPayDetail.get(i).getFieldName13();
                                                currencyType = lstPerSecPayDetail.get(i).getFieldName12();
                                                perSecAmount = lstPerSecPayDetail.get(i).getFieldName4();
                                            }
                                            else if("bg".equalsIgnoreCase(payTyp))
                                            {
                                                perSecAmount = lstPerSecPayDetail.get(i).getFieldName1();
                                                currencyType = lstPerSecPayDetail.get(i).getFieldName2();
                                                txtCurrencySymbol = lstPerSecPayDetail.get(i).getFieldName3();
                                            }
                                %>

                                <%
                                  if("bg".equalsIgnoreCase(payTyp) && ("forfeitrequested".equalsIgnoreCase(request.getParameter("msgId")) || "Forfeited".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName10())))
                                  {
                                      AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
                                      List<TblCmsCompansiateAmt> listCompAmt = accPaymentService.getTblCmsCompansiateAmt(Integer.parseInt(regPayId[i]));
                                      if(!listCompAmt.isEmpty())
                                      {
                                %>
                                    <tr>
                                        <td class="ff">Compensate Amount :</td>
                                        <td><%=listCompAmt.get(0).getCompAmt()%> &nbsp;&nbsp; Balance Value : [<%=listCompAmt.get(0).getBalanceAmt()%>]
                                        </td>
                                    </tr>
                                <%}}%>

                                    <tr>
                                        <td class="ff">Amount (in <% if(currencyType.equalsIgnoreCase("USD")){out.print("USD");} else {out.print("Nu.");} %>) :</td>
                                        <td><label id="lblCurrencySymbol_<%=i%>"><% if(txtCurrencySymbol.equalsIgnoreCase("TK.")){out.print("Nu.");} else if(txtCurrencySymbol.equalsIgnoreCase("Nu.")){out.print("Nu.");} else {out.print("$");} %></label>&nbsp;
                                            <label id="lblAmount_<%=i%>"><%=perSecAmount%></label>
                                        </td>
                                    </tr>
                                <%
                                        }
                                    }
                                }else{%>
                            <tr>
                                <td class="ff">Currency :</td>
                                <td><% if(lstPaymentDetail.get(0).getFieldName3().equalsIgnoreCase("USD")){out.print("USD");} else {out.print("BTN");}%></td>
                            </tr>
                            <%
                              if("forfeitrequested".equalsIgnoreCase(request.getParameter("msgId")) || "Forfeited".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName10()))
                              {
                                  AccPaymentService accPaymentService = (AccPaymentService) AppContext.getSpringBean("AccPaymentService");
                                  List<TblCmsCompansiateAmt> listCompAmt = accPaymentService.getTblCmsCompansiateAmt(Integer.parseInt(regPaymentId));
                                  if(!listCompAmt.isEmpty())
                                  {
                            %>
                            <tr>
                                <td class="ff">Compensate Amount :</td>
                                <td><%=listCompAmt.get(0).getCompAmt()%> &nbsp;&nbsp; Balance Value : [<%=listCompAmt.get(0).getBalanceAmt()%>]
                                </td>
                            </tr>
                            <%}}%>
                            <tr>
                                <td class="ff">Amount :</td>
                                <td>
                                    <%-- Changed by Emtaz on 13-April-2016. Taka -> Nu. --%>
                                    <%-- <%if("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%> --%>
                                    <%if("Nu.".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>Nu.<%-- Taka --%></label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                    <%} else if("USD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                        <label>$</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                    <% }else if("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                        <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                    <%}%>
                                </td>
                            </tr>
                        <%}%>
                        <!-- Dohatec End -->
                         <tr>
                            <td class="ff">Mode of Payment :</td>
                            <td><% if(lstPaymentDetail.get(0).getFieldName5().equals("Pay Order")){out.print("Cheque/Cash Warrant");}else if(lstPaymentDetail.get(0).getFieldName5().equals("Bank Guarantee")){out.print("Financial Institution Guarantee");}else{out.print(lstPaymentDetail.get(0).getFieldName5());}%>
                            </td>
                        </tr>
                        <%if("Pay Order".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5()) || "DD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5()) || "Bank Guarantee".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = objTSC.returndata("getTenderPaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Instrument No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Financial Institution :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Financial Institution Branch :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuance Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName4()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Validity Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName5()%></td>
                                </tr>

                               <% }
                            }%>

                            <%if("Account to Account Transfer".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = objTSC.returndata("getTenderPaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Beneficiary Account No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <%if(!"".equalsIgnoreCase(lstPaymentDetailMore.get(0).getFieldName3())){%>
                                <tr>
                                    <td class="ff">Branch Name :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <%}%>
                               <% }
                            }%>

                            <tr>
                                <td class="ff">Date and Time of Payment :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName6()%></td>
                            </tr>

                            <tr>
                                <td class="ff">Remarks :</td>
                                <td><%=URLDecoder.decode(lstPaymentDetail.get(0).getFieldName7(),"UTF-8")%></td>
                            </tr>
                    </table>
                              <%   List<SPTenderCommonData> lstExtensionPaymentDetail = null;
                                    if (isBankUser) {lstExtensionPaymentDetail = objTSC.returndata("getTenderPaymentExtendDetailBank", regPaymentId,userId);}
                                else {lstExtensionPaymentDetail =  objTSC.returndata("getTenderPaymentExtendDetail", regPaymentId, userId);}
                                if (!lstExtensionPaymentDetail.isEmpty() && lstExtensionPaymentDetail != null) {%>
                            <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                                <tr>
                                    <th width="100%" class="t-align-left" colspan="5">Performance Security Extension validity History</th>
                                </tr>
                                <tr>
                                <th width="5%" class="t-align-left">Serial No.</th>
                                <th width="28%" class="t-align-left">Previous Validity Date</th>
                                <th width="28%" class="t-align-left">Extended Validity Date</th>
                                <th width="28%" class="t-align-left">Request Date</th>
                                <th width="11%" class="t-align-left">Action</th>
                                 </tr>
                                <% for(int i = 0; i< lstExtensionPaymentDetail.size(); i++)
                                    {%>
                                <tr>
                                    <td class="t-align-center"><%=i+1%></td>
                                    <td class="t-align-center"><%=lstExtensionPaymentDetail.get(i).getFieldName1()%></td>
                                    <td class="t-align-center"><%=lstExtensionPaymentDetail.get(i).getFieldName2()%></td>
                                    <td class="t-align-center"><%=lstExtensionPaymentDetail.get(i).getFieldName3()%></td>
                                    <td class="t-align-center"><%=lstExtensionPaymentDetail.get(i).getFieldName4()%></td>
                                </tr>
                                <%}%>
                            </table>
                            <%}%>
 </div>
                              </div>
                            <%if(isBranchMaker && !isPaymentVerified){%>
                            <%
                            boolean isUserPaymentMaker=false;
                                List<SPTenderCommonData> lstPaymentBranchMakerInfo = objTSC.returndata("getBranchMakerIdFromPaymentId", regPaymentId, null);
                               if (!lstPaymentBranchMakerInfo.isEmpty()){

                                   if(userId.equalsIgnoreCase(lstPaymentBranchMakerInfo.get(0).getFieldName1())){
                                    isUserPaymentMaker=true;
                                }
                               }
                            %>
                            <%if (isUserPaymentMaker) {%>
                            <form id="frmPaymentDetail" method="post" action="">
                                <input type="hidden" name="bgId" id="bgId" value="<%=bgId%>" />
                                <table class="t_space" border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                        <td width="18%" class="ff">&nbsp;</td>
                                <td>
                                    <label class="formBtn_1">
                                        <input name="btnOK" id="btnOK" type="submit" value="OK" />
                                    </label>

                                    &nbsp;
                                    <label class="formBtn_1">
                                        <input name="btnEdit" id="btnEdit" type="submit" value="Edit Payment Details" />
                                    </label>
                                </td>
                            </tr>
                                </table>
                            </form>
                            <%}
                            }
                             request.setAttribute("listEmpTras", userTransMap);
            %>

                       <!--jsp:include page="../resources/common/EmpTransferHist.jsp" /-->     
                       
                    <% } else {%>
                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td>Payment information not found!</td>
                        </tr>
                    </table>
                    <%}%>
<!--                </div>-->

                 <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->
        </div>
        </div>
        </div>
    </body>

    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabPayment");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
