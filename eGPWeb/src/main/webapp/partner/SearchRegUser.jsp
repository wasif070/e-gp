<%-- 
    Document   : SearchRegUser
    Created on : Feb 1, 2011, 3:51:04 PM
    Author     : Karan
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Search User for Payment</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />


            <script type="text/javascript">
        function chkdisble(pageNo){
            $('#dispPage').val(Number(pageNo));
            if(parseInt($('#pageNo').val(), 10) != 1){
                $('#btnFirst').removeAttr("disabled");
                $('#btnFirst').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnFirst').attr("disabled", "true");
                $('#btnFirst').css('color', 'gray');
            }

            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnPrevious').attr("disabled", "true")
                $('#btnPrevious').css('color', 'gray');
            }

            if(parseInt($('#pageNo').val(), 10) > 1){
                $('#btnPrevious').removeAttr("disabled");
                $('#btnPrevious').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnLast').attr("disabled", "true");
                $('#btnLast').css('color', 'gray');
            }

            else{
                $('#btnLast').removeAttr("disabled");
                $('#btnLast').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnNext').attr("disabled", "true")
                $('#btnNext').css('color', 'gray');
            }
            else{
                $('#btnNext').removeAttr("disabled");
                $('#btnNext').css('color', '#333');
            }
        }
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnFirst').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);

                if(totalPages>0 && pageNo!="1")
                {
                    $('#pageNo').val("1");
                    loadTable();
                    $('#dispPage').val("1");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnLast').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);
                if(totalPages>0)
                {
                    $('#pageNo').val(totalPages);
                    loadTable();
                    $('#dispPage').val(totalPages);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnNext').click(function() {
                var pageNo=parseInt($('#pageNo').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo <= totalPages) {
                    $('#pageNo').val(Number(pageNo)+1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)+1);
                    $('#btnPrevious').removeAttr("disabled");
                }
            });
        });

    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnPrevious').click(function() {
                var pageNo=$('#pageNo').val();

                if(parseInt(pageNo, 10) > 1)
                {
                    $('#pageNo').val(Number(pageNo) - 1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo) - 1);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnGoto').click(function() {
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            });
        });
    </script>
    <script type="text/javascript">
        function loadTable()
        {
            $.post("<%=request.getContextPath()%>/SearchRegisterationUserServlet", {pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);

                if($('#noRecordFound').val() == "noRecordFound"){
                    $('#pagination').hide();
                }else{
                    $('#pagination').show();
                }
                if($("#totalPages").val() == 1){
                    $('#btnNext').attr("disabled", "true");
                    $('#btnLast').attr("disabled", "true");
                }else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnLast').removeAttr("disabled");
                }
                $("#pageNoTot").html($("#pageNo").val());
                chkdisble($("#pageNo").val());
                $("#pageTot").html($("#totalPages").val());
            });
        }
    </script>

    </head>
<body>
    <div>
    <div class="dashboard_div">
      <!--Dashboard Header Start-->
      <%@include  file="../resources/common/AfterLoginTop.jsp" %>
      <!--Dashboard Header End-->
      <!--Dashboard Content Part Start-->
      <div class="contentArea_1">
        <div class="pageHead_1">Search Registration User</div>
      <div>&nbsp;</div>

        <div class="formBg_1 t_space">
            <table cellspacing="10" class="formStyle_1" width="100%">
            <tr>
              <td width="14%" class="ff">e-mail ID :</td>
              <td width="35%"><input name="textfield" type="text" class="formTxtBox_1" id="textfield" style="width:195px;" /></td>
              <td width="13%">&nbsp;</td>
              <td width="38%">&nbsp;</td>
              </tr>
            <tr>
              <td class="ff">Payment Date From : </td>
              <td>
              	<input name="textfield6" type="text" class="formTxtBox_1" id="textfield5" style="width:100px;" readonly="readonly" />
              &nbsp;<a href="javascript:void(0);" onclick="" title="Calender"><img src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" /></a>
              </td>
              <td class="ff">Payment Date To : </td>
              <td>
              	<input name="textfield6" type="text" class="formTxtBox_1" id="textfield5" style="width:100px;" readonly="readonly" />
              &nbsp;<a href="javascript:void(0);" onclick="" title="Calender"><img src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" /></a>
              </td>
              </tr>
            <tr>
              <td class="ff">Branch  Maker : </td>
              <td>
              	<select name="select" class="formTxtBox_1" id="select" style="width:155px;">
                  <option selected="selected">- Select Branch Maker -</option>
                </select>
              </td>
              <td class="ff">Branch  Name : </td>
              <td><select name="select2" class="formTxtBox_1" id="select2" style="width:155px;">
                <option selected="selected">- Select Branch Name -</option>
              </select></td>
            </tr>
            <tr>
              <td colspan="4" class="t-align-center ff"><label class="formBtn_1">
                <input type="submit" name="btnSearch" id="button" value="Search" />
              </label>

                <label class="formBtn_1 l_space">
                <input type="submit" name="btnReset" id="button" value="Reset" />
              </label>
              </td>
              </tr>
            </table>
</div>

<!--      <table width="100%" cellspacing="0" class="tableList_1 t_space">
          <tr>
              <th width="6%" class="t-align-center">S. No.</th>
              <th width="25%" class="t-align-center">e-mail ID</th>
              <th width="29%" class="t-align-center">Branch Maker</th>
              <th width="15%" class="t-align-center">Date and Time of Payment</th>
              <th width="15%" class="t-align-center">New Registration / Renewal</th>
              <th width="10%" class="t-align-center">Action</th>
          </tr>
          <tr>
              <td class="t-align-center">1</td>
              <td class="t-align-left">Online.urmil@gmail.com</td>
              <td class="t-align-left">Urmil  Mehta</td>
              <td class="t-align-left">31-Jan-2011  12:00</td>
              <td class="t-align-left">New  Registration</td>
              <td class="t-align-left"><a href="#">Verify</a> | <a href="#">View</a></td>
          </tr>
      </table>-->

<!--    Start:  Dynamic Table-->
<table width="100%" cellspacing="0" id="resultTable" class="tableList_3">

    <tr>
        <th width="4%" class="t-align-center">Sl. No.</th>
        <th width="25%" class="t-align-center">e-mail ID</th>
        <th width="31%" class="t-align-center">Branch Maker</th>
        <th width="15%" class="t-align-center">Date and Time of Payment</th>
        <th width="15%" class="t-align-center">New Registration / Renewal</th>
        <th width="10%" class="t-align-center">Action</th>
    </tr>
        
</table>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                    <tr>
                        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                        <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                            &nbsp;
                            <label class="formBtn_1">
                                <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                            </label></td>
                        <td  class="prevNext-container"><ul>
                                <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                            </ul></td>
                    </tr>
                </table>
                <div align="center">
                    <input type="hidden" id="pageNo" value="1"/>
                    <input type="hidden" name="size" id="size" value="10"/>
                </div>
<script type="text/javascript">
    loadTable();
</script>
<!--    End: Dynamic Table-->

        <div>&nbsp;</div>
        </div>
</div>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
  <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
  <!--Dashboard Footer End-->
</div>
</body>
</html>
