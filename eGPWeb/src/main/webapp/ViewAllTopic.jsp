<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.
    <%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
    0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            Object objUName = session.getAttribute("userName");
            boolean isLoggedIn = false;
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userTypeId").toString();
            }
            if (objUName != null) {
                isLoggedIn = true;
            }
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Procurement Forum</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%-- <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />--%>

        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="resources/js/jQuery/jquery.validate.js"></script>
       <link type="text/css" rel="stylesheet" href="resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="resources/js/datepicker/js/lang/en.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <script type="text/javascript">
            $(function() {
                $('#btnReset').click(function() {
                    $("#pageNo").val("1");
                    $("#keyword").val('');
                    $("#textfield5").val('');
                    $("#select2").val('');
                    $("#viewType").val('');
                    $("#all").attr('selected','selected');
                    loadTable();
                    });
                });
        </script>


        <script type="text/javascript">
            function loadTable1()
            {
                $.post("<%=request.getContextPath()%>/getAdminPost", {funName: "AllTopics",keyword:$("#keyword").val(),textfield5:$("#textfield5").val(),select2:$("#select2").val() ,viewType:$("#viewType").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    var noRecordFound;
                    $('#resultTable').find("tr:gt(0)").remove();

                    $('#resultTable tr:last').after(j);
                    sortServletTable();
                    $('#resultTable tr').each(function() {
                        noRecordFound = $(this).find('td#noRecordFound');
                    });
                    if(noRecordFound.length == 1){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageTot").html($("#totalPages").val());
                    $("#pageNoTot").html($("#pageNo").val());
                    $('#resultDiv').show();
                });
            }
        </script>
         <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
        <script type="text/javascript">
    function test(type){
            if(type==2){
              
                document.getElementById("linkApproved").className = "sMenu";
                document.getElementById("linkRejected").className = "";
                document.getElementById("viewType").value = "";
                //$("#resultTable").attr('cols', '@0,5');
                $("#resultTable").get(0).setAttribute('cols', '@0,5');
                loadTable();
                 }else {
                     $("#resultTable").get(0).setAttribute('cols', '@0,6');
                //$("#resultTable").attr('cols', '@0,6');
                document.getElementById("linkRejected").className = "sMenu";
                document.getElementById("linkApproved").className = "";
                document.getElementById("viewType").value = "MYREPLIED";
                
                loadTable();
            }
            
        }
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            $(function() {

                $('#btnSearch').click(function() {

                    $("#pageNo").val("1");
                   
                    $.post("<%=request.getContextPath()%>/getAdminPost", {action: $("#action").val(), funName: "AllTopics",keyword:$("#keyword").val(),textfield5:$("#textfield5").val(),select2:$("#select2").val() ,viewType:$("#viewType").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){

                        var noRecordFound;
                        $('#resultTable').find("tr:gt(0)").remove();

                        $('#resultTable tr:last').after(j);
                        sortServletTable();
                        $('#resultTable tr').each(function() {
                            noRecordFound = $(this).find('td#noRecordFound');
                        });
                        if(noRecordFound.length == 1){
                            $('#pagination').hide();
                        }else{
                            $('#pagination').show();
                        }
                        chkdisble($("#pageNo").val());
                        if($("#totalPages").val() == 1){
                            $('#btnNext').attr("disabled", "true");
                            $('#btnLast').attr("disabled", "true");
                        }else{
                            $('#btnNext').removeAttr("disabled");
                            $('#btnLast').removeAttr("disabled");
                        }
                        $("#pageTot").html($("#totalPages").val());
                        $("#pageNoTot").html($("#pageNo").val());
                        $('#resultDiv').show();
                    });
                }
               );
            });
        </script>
        <script type="text/javascript">
            function loadTable()
            {
                
                $.post("<%=request.getContextPath()%>/getAdminPost", {action: $("#action").val(),funName: "AllTopics",keyword:$("#keyword").val(),textfield5:$("#textfield5").val(),select2:$("#select2").val() ,viewType:$("#viewType").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    var noRecordFound;
                    
                    $('#resultTable').find("tr:gt(0)").remove();

                    $('#resultTable tr:last').after(j);
                    sortServletTable();
                    $('#resultTable tr').each(function() {
                        noRecordFound = $(this).find('td#noRecordFound');
                    });
                    if(noRecordFound.length == 1){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageTot").html($("#totalPages").val());
                    $("#pageNoTot").html($("#pageNo").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
              
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);

                        loadTable();

                        $('#dispPage').val(totalPages);
                       
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);

                        loadTable();

                        $('#dispPage').val(Number(pageNo)+1);
                        //chkdisble($('#pageNo').val());
                        $('#dispPage').val(Number(pageNo)+1);
                       
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);

                        loadTable();

                        $('#dispPage').val(Number(pageNo) - 1);
                        
                    }
                });
            });
        </script>
        <script type="text/javascript">
            var postLink = "PostTopic.jsp";
            //alert("postLink : "+pos tLink);
            var user = '<%=strUserTypeId%>';

       
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));

                            loadTable();
                           
                        }
                    }
                });
            });
        </script>
    </head>
    <body onload="loadTable();">
     <form id="alltenderFrm" method="post">
    <input type="hidden" name="viewType" id="viewType" value=""/>
    <input type="hidden" id="pageNo" value="1"/>
    <input type="hidden" name="size" id="size" value="10"/>
            <div class="dashboard_div">
                <%@include file="resources/common/AfterLoginTop.jsp" %>
 <div class="contentArea_1">
                    <div class="pageHead_1"> Procurement Forum Topics
                        <span class="c-alignment-right"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('5');">Save as PDF</a>
                            <a href="contentPublicForumBoard.jsp" class="action-button-goback">Go Back</a></span>  </div>
                    <%if ("y".equals(request.getParameter("succFlag"))) {
                                    if ("verifyTopic".equalsIgnoreCase(request.getParameter("activity"))) {
                                        if ("Accepted".equalsIgnoreCase(request.getParameter("forumStatus"))) {%>
            <div id="succMsg" class="responseMsg successMsg">Content accepted successfully</div>
                    <%} else {%>
            <div id="succMsg" class="responseMsg successMsg">Content rejected successfully</div>
        <%}
                                                        } else if ("verifyReply".equalsIgnoreCase(request.getParameter("activity"))) {
                                                            if ("Accepted".equalsIgnoreCase(request.getParameter("forumStatus"))) {%>
            <div id="succMsg" class="responseMsg successMsg">Reply accepted successfully</div>
                    <%} else {%>
            <div id="succMsg" class="responseMsg successMsg">Reply rejected successfully</div>
        <%}
                                                        } else if ("postTopic".equalsIgnoreCase(request.getParameter("activity"))) {
                                                            if ("yes".equalsIgnoreCase(request.getParameter("verificationRequired"))) {%>
            <div id="succMsg" class="responseMsg successMsg">Topic posted successfully. Topic will be displayed to the other user only if content is verified and accepted by Content Admin. Content verification usually takes <%=XMLReader.getMessage("PPFValidationDays")%> of time.</div>
                    <%} else {%>
            <div id="succMsg" class="responseMsg successMsg">Topic posted successfully.</div>
        <%}
                                                        } else if ("postReply".equalsIgnoreCase(request.getParameter("activity"))) {%>
                    <div id="succMsg" class="responseMsg successMsg">Reply given successfully. Reply will be displayed to the other users only if the reply content is verified and accepted by the Content Admin.</div>
                    <%} else {
    }
                                }%>
    <%if(request.getParameter("msg") != null && request.getParameter("msg").equals("delsucc")){%>
    <br/><div align="left" id="sucMsg" class="responseMsg successMsg">Topic/Reply deleted successfully</div>
    <% } %>
     <%if(request.getParameter("msg") != null && request.getParameter("msg").equals("delfail")){%>
        <br/><div align="left" id="sucMsg" class="responseMsg errorMsg">Problem in deleting Topic/Reply</div>
     <% } %>
  <div class="formBg_1 t_space">

    <table cellspacing="10" class="formStyle_1" width="100%">
      <tr>
        <td width="12%" class="ff">Keyword :</td>
        <td width="47%"><input type="text" class="formTxtBox_1" name ="keyword" id="keyword" style="width:200px;" /></td>
        <td width="9%"><span class="ff">Posted By : </span></td>
        <td width="32%"><input type="text" class="formTxtBox_1" name="select2" id="select2" style="width:200px;" /></td>
      </tr>
      <tr>
        <td class="ff">Date of Posting : </td>
                                <td><input type="text" class="formTxtBox_1"  name="textfield5" id="textfield5" style="width:100px;" readonly="readonly" onClick="GetCal('textfield5','textfield5');" />
          &nbsp;<a href="javascript:void(0);" onclick="" title="Calender"><img src="resources/images/Dashboard/calendarIcn.png" id="calendarIcn" name ="calendarIcn" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('textfield5','calendarIcn');" /></a></td>
        <td class="ff">Action : </td>
                                <td><select name="action" id="action" class="formTxtBox_1" style="width:100px;">
                                        <option id="all" value ="" selected="selected">All</option>
                <option value ="Pending">Pending</option>
                <option value ="Accepted">Accepted</option>
                <option value ="Rejected">Rejected</option>

                </select>

        </td>

        <td width="9%">&nbsp;</td>
        <td width="32%">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="4" class="t-align-center ff">
            <span class="formBtn_1">
                <input type="button" name="btnSearch" id="btnSearch" value="Search" /></span>
          <span class="formBtn_1">
          <input type="submit" name="btnReset" id="btnReset" value="Reset" />
          </span></td>
      </tr>
    </table>
  </div>

    <div id="resultDiv" style="display: none">
        <ul class="tabPanel_1 t_space">

            <li><a href="javascript:void(0);" id="linkApproved" onclick="test(2);" class="sMenu">Topics</a></li>


            <li><a href="javascript:void(0);" id="linkRejected" onclick="test(3);">Replies</a></li>

        </ul>

  <div class="tabPanelArea_1">
    <table id="resultTable" width="100%" cellspacing="0" class="tableList_3 t_space" cols="@0,5">
      <tr>
        
      </tr>
    </table>
  </div>
    <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                                        <tr>
                                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                            <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                &nbsp;
                                                <label class="formBtn_1 t-align-center">

                                                    <input type="button" name="button" id="btnGoto" value="Go To Page" />


                                                </label>


                                            </td>
                                                <td align="right" class="prevNext-container"><ul>
                                                        <li><font size="3">&laquo;</font> <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                                        <li><font size="3">&#8249;</font> <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                                        <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                        <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                                </ul></td>
                                        </tr>
                                    </table>
                                    <div align="center">
                                        <input type="hidden" id="pageNo" value="1"/>
                                        <input type="hidden" name="size" id="size" value="10"/>
                                    </div>
  </div>
<form id="formstyle" action="" method="post" name="formstyle">
   <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
   <%
     SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
     String appenddate = dateFormat1.format(new Date());
   %>
   <input type="hidden" name="fileName" id="fileName" value="ProcurementForumTopics_<%=appenddate%>" />
    <input type="hidden" name="id" id="id" value="ProcurementForumTopics" />
</form>
  <div>&nbsp;</div>

  <!--Dashboard Content Part End-->

                </div>
  <!--Dashboard Footer Start-->
  <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
  <!--Dashboard Footer End-->
            </div>
     </form>

    </body>
</html>
