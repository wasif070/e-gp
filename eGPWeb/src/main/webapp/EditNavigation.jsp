<%-- 
    Document   : EditNavigation
    Created on : Nov 4, 2010, 12:26:18 PM
    Author     : Taher
--%>

<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.web.databean.OnlinePaymentDtBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page  import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>    
    <body>
        <div class="stepWiz_1 t_space" style="width: 100%; height: 18px;">
            <div style="width:80%; float: left;">
            <ul>
               
        <%
                session.removeAttribute("payment");
                    UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                    List<String> pageList = userRegisterService.pageNavigationList(Integer.parseInt(request.getSession().getAttribute("userId").toString()));//
                    String pageName=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1];
                    if (pageList.size() == 3) {
                        for(String pages : pageList){
                            if(pages.trim().startsWith("I")){
                                %>
                                <li <%if(pageName.equals("IndividualConsultant.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("IndividualConsultant.jsp")){if(pages.trim().contains("-E")){%><a href="IndividualConsultant.jsp"><%}}%>Individual Consultant<%if(!pageName.equals("IndividualConsultant.jsp")){if(pages.trim().contains("-E")){%></a><%}}%></li> >
                                <%
                            }
                            else if(pages.trim().startsWith("S")){
                                %>
                                <li <%if(pageName.equals("SupportingDocuments.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("SupportingDocuments.jsp")){if(pages.trim().contains("-E")){%><a href="SupportingDocuments.jsp"><%}}%>Supporting Documents<%if(!pageName.equals("SupportingDocuments.jsp")){if(pages.trim().contains("-E")){%></a><%}}%></li>
                                <%
                            }
                            //else{
                                %>
                                <%--<li <%if(pages.trim().contains("-D")){%>class="sMenu"<%}%>><%if(pages.trim().contains("-E")){%><a href="Authenticate.jsp"><%}%>e-Signature.<%if(pages.trim().contains("-E")){%></a><%}%></li>--%>
                                <%
                            //}
                        }
                    } else if (pageList.size() == 4) {
                        for(String pages : pageList){
                            if(pages.trim().startsWith("C")){
                                %>
                                <li <%if(pageName.equals("CompanyDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("CompanyDetails.jsp")){if(pages.trim().contains("-E")){%><a href="CompanyDetails.jsp"><%}}%>Company Details<%if(!pageName.equals("CompanyDetails.jsp")){if(pages.trim().contains("-E")){%></a><%}}%></li> >
                                <%
                            }
                            else if(pages.trim().startsWith("P")){
                                %>
                                <li <%if(pageName.equals("PersonalDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("PersonalDetails.jsp")){if(pages.trim().contains("-E")){%><a href="PersonalDetails.jsp"><%}}%>Company Contact Person Details<%if(!pageName.equals("PersonalDetails.jsp")){if(pages.trim().contains("-E")){%></a><%}}%></li> > 
                                <%
                            }
                            else if(pages.trim().startsWith("S")){
                                %>
                                <li <%if(pageName.equals("SupportingDocuments.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("SupportingDocuments.jsp")){if(pages.trim().contains("-E")){%><a href="SupportingDocuments.jsp"><%}}%>Supporting Documents<%if(!pageName.equals("SupportingDocuments.jsp")){if(pages.trim().contains("-E")){%></a><%}}%></li>
                                <%
                            }
                           // else {
                                %>
                                <%--<li <%if(pages.trim().contains("-D")){%>class="sMenu"<%}%>><%if(pages.trim().contains("-E")){%><a href="Authenticate.jsp"><%}%>e-Signature.<%if(pages.trim().contains("-E")){%></a><%}%></li>--%>
                                <%
                            //}
                        }
                    } else if (pageList.size() == 5) {
                       for(String pages : pageList){
                            if(pages.trim().startsWith("C")){
                                %>
                                <li <%if(pageName.equals("CompanyDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("CompanyDetails.jsp")){if(pages.trim().contains("-E")){%><a href="CompanyDetails.jsp"><%}}%>Company Details<%if(!pageName.equals("CompanyDetails.jsp")){if(pages.trim().contains("-E")){%></a><%}}%></li> >
                                <%
                            }
                            else if(pages.trim().startsWith("P")){
                                %>
                                <li <%if(pageName.equals("PersonalDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("PersonalDetails.jsp")){if(pages.trim().contains("-E")){%><a href="PersonalDetails.jsp"><%}}%>Company Contact Person Details<%if(!pageName.equals("PersonalDetails.jsp")){if(pages.trim().contains("-E")){%></a><%}}%></li> >
                                <%
                            }
                            else if(pages.trim().startsWith("J")){
                                %>
                                <li <%if(pageName.equals("JvcaDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("JvcaDetails.jsp")){if(pages.trim().contains("-E")){%><a href="JvcaDetails.jsp?fun=edit"><%}}%>JVCA details<%if(!pageName.equals("JvcaDetails.jsp")){if(pages.trim().contains("-E")){%></a><%}}%></li> >
                                <%
                            }
                            else if(pages.trim().startsWith("S")){
                                %>
                                <li <%if(pageName.equals("SupportingDocuments.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("SupportingDocuments.jsp")){if(pages.trim().contains("-E")){%><a href="SupportingDocuments.jsp"><%}}%>Supporting Documents<%if(!pageName.equals("SupportingDocuments.jsp")){if(pages.trim().contains("-E")){%></a><%}}%></li>
                                <%
                            }
                           // else if(pages.trim().startsWith("A")){
                                %>
                                <%--<li <%if(pages.trim().contains("-D")){%>class="sMenu"<%}%>><%if(pages.trim().contains("-E")){%><a href="Authenticate.jsp"><%}%>e-Signature.<%if(pages.trim().contains("-E")){%></a><%}%></li>--%>
                                <%
                            //}
                    }
               }
        %>         
            </ul>
            </div>
<!--        <div class="txt_1" style="width: 60px; height: 25px; padding-right: 10px; float: right; line-height: 28px; font-weight:bold;"><a href="<%=request.getContextPath()%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1] %>" target="_blank" ><img src="<%=request.getContextPath()%>/resources/images/Dashboard/helpIcon.png" style="margin-right: 5px; float: left;" />Help</a></div>-->
        </div>                                 
    </body>
</html>
