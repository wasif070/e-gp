 <!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Supported Browsers</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
       
        <%
    response.setHeader("Expires", "-1");
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
    response.setHeader("Pragma", "no-cache");

    HttpSession hs = request.getSession();
    String id="0";
    hs.setAttribute("userId",id);
%>

<!--        <script type="text/javascript">
            $(document).ready(function(){
                jQuery.each(jQuery.browser, function(i, val) {
                    browserVer = jQuery.browser.version;
                    if(i == "mozilla" && browserVer.substr(0,3) == "1.9"){
                        document.getElementById("browserversion").innerHTML= i +" "+ nomVersionNavig();
                        document.getElementById("browserversion1").innerHTML= i +" "+ nomVersionNavig();
                    }
                    if(i == "msie" && (browserVer.substr(0,3) == "8.0" || browserVer.substr(0,3) == "7.0")){
                        document.getElementById("browserversion").innerHTML= i +" "+ browserVer.substr(0,3);
                        document.getElementById("browserversion1").innerHTML= i +" "+ browserVer.substr(0,3);
                    }
                });
            });
        </script>-->




    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Header Table-->
                <!--Middle Content Table-->
                <div  class="contentArea_1" align="center">
                    <table border=0 cellSpacing=7 cellPadding=0
                           width="90%" class="instructionTable" >
                        <tr>
                            <td align="center">
                                <div><strong>    Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan</strong></div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center"><a href="Index.jsp"><img src="<%=request.getContextPath()%>/resources/images/cptuEgpLogo.gif" alt="" width="160" height="71"></a>
                        </tr>
                        <tr>
                            <td align="center">
                                <div>   <strong> Government Procurement and Property Management Division (GPPMD) </strong></div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div>    Department of National Properties, Ministry of Finance, Royal Government of Bhutan</div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div style="color:#FF9326;font-size: medium;">    <strong>Need a GPPMD Certified Browser</strong></div>
                                <%--<table width="100%" cellspacing="0" id="header" class="t_space">
                                    <tr><td width="50%" align="right">
                                            <a class="action-button-home" style="vertical-align: middle;" href="Index.jsp" >Go To e-GP Home Page</a>
                                        </td>
                                    </tr>
                                </table>--%>
                            </td>
                        </tr>
                        <tr>
                            <td class="atxt_1 c_t_space" vAlign=top>
                                <div class="atxt_1">
                                    Dear User,<br/><br/>
                                    Safety and security of your information and transactions, integrity of your data, and availability of complete e-GP System functionality is of prime
                                    importance for carrying out procurement processes through e-GP System. Because of the sensitive nature of the transactions done on e-GP
                                    System, <strong>CPTU has tested and certified following browsers to be used and the system is tested for following browsers only.</strong>
                                </div><br />
                            </td>
                        </tr>
                        <tr>
                            <td  align="center" style="width:50%;">
                                <table class="tableList_2" style="width:50%;" align="center" >
                                    <tr>
                                        <td class="ff" align="center" style="background-color: #CCCCCC;text-align: center;">
                                            Sl. No.
                                        </td>
                                        <td class="ff" align="center" style="background-color: #CCCCCC;text-align: center;">
                                            Certified Browser
                                        </td>
                                        <td class="ff" align="center" style="background-color: #CCCCCC;text-align: center;">
                                            Download Link
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            1
                                        </td>
                                        <td style="text-align: center;">
                                            Internet Explorer 8.x
                                        </td>
                                        <td style="text-align: center;">
                                               <a href="javascript:void(0)" onclick="location.href='../../help/browser/Internet Explorer 8 (For Windows XP).zip'" class="" target="_blank"> Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            2
                                        </td>
                                        <td style="text-align: center;">
                                            Internet Explorer 9.x
                                        </td>
                                        <td style="text-align: center;">
                                           <a href="javascript:void(0)" onclick="location.href='../../help/browser/Internet Explorer 9 (For Windows 7).zip'" class="" target="_blank"> Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            3
                                        </td>
                                        <td style="text-align: center;">
                                            Internet Explorer 10.x
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="javascript:void(0)" onclick="location.href='../../help/browser/Internet Explorer 10.zip'" class="" target="_blank"> Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            4
                                        </td>
                                        <td style="text-align: center;">
                                            Mozila Firefox 3.6x
                                        </td>
                                        <td style="text-align: center;">
                                             <a href="javascript:void(0)" onclick="location.href='../../help/browser/Firefox 3.6.15.zip'" class="" target="_blank"> Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            5
                                        </td>
                                        <td style="text-align: center;">
                                            Mozila Firefox  13.x
                                        </td>
                                        <td style="text-align: center;">
                                            <a href="javascript:void(0)" onclick="location.href='../../help/browser/Firefox 13.0.1.zip'" class="" target="_blank"> Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            6
                                        </td>
                                        <td style="text-align: center;">
                                            Mozila Firefox 14.x
                                        </td>
                                        <td style="text-align: center;">
<!--                                            <a href="http://www.mozilla.com/en-US/firefox/" target="_blank"> Download</a>-->
                                            <a href="javascript:void(0)" onclick="location.href='../../help/browser/Firefox 14.0.1.zip'" class="" target="_blank"> Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            7
                                        </td>
                                        <td style="text-align: center;">
                                            Mozila Firefox 29.x
                                        </td>
                                        <td style="text-align: center;">
<!--                                            <a href="http://www.mozilla.com/en-US/firefox/" target="_blank"> Download</a>-->
                                            <a href="javascript:void(0)" onclick="location.href='../../help/browser/Firefox 29.0.zip'" class="" target="_blank"> Download</a>
                                        </td>
                                    </tr>
                                </table>

                            </td></tr>
                        <%--<tr>
                            <td class="atxt_1 c_t_space" vAlign=top>
                                <div class="atxt_1">
                                    <br></br>

                                    <strong>Your browser <span id="browserversion1" name="browserversion1" ></span></strong> is not a CPTU certified browser.  Request you to download and install any of the certified browsers mentioned above, and start using e-GP system.
                                </div><br />
                            </td>
                        </tr>--%>
                        <tr>
                            <td class="atxt_1 c_t_space" vAlign=top>
                                <div class="atxt_1">
                                    CPTU is not responsible for any issues or problems resulting from use of browsers which are not certified by CPTU.
                                </div><br />
                            </td>
                        </tr>

                        <tr>
                            <td align="center" style="color:#FF9326;font-size: medium;"><br/><strong>Thank You for your support.</strong><br/><br/></td></tr>
                        <tr><td align="center"><img alt="" src="<%=request.getContextPath()%>/resources/images/cptuLogo.jpg" width="435" height="37"></td>
                        </tr>
                        <tr><td align="center">Box 116, Thimphu, Bhutan.<br/><br/>
                                <b>Phone</b>: +975-02-336962 |   <b>Fax</b>: +975-02-336961 | <b>Email</b>: <a href="mailto:info.bhutan@dohatec.com.bd" >info.bhutan@dohatec.com.bd</a>  | <b>Web</b>: <a href="http://www.pppd.gov.bt/" >www.pppd.gov.bt</a>
                            </td></tr>
                        <tr>
                            <td class="headerCss"></td>
                        </tr>
                        <tr>
                            <td align="center" style="color:#FF9326;font-size: medium;"><br/><strong>e-GP Support Contact Details </strong><br/></td></tr>
                        <tr>
                            <td class="atxt_1 c_t_space" vAlign=top>
                                <div class="atxt_1" style="text-align: center;">
                                    <b>Phone</b>: +880-2-9144 252/53 | <b>Email</b>: <a href="mailto:helpdesk@eprocure.gov.bd" >helpdesk@eprocure.gov.bd</a>
                                </div><br />
                            </td>
                        </tr>

                    </table>
                </div>
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
