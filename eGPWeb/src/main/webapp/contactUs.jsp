<%--
    Document   : contactUs
    Created on : Dec 12, 2010, 12:54:06 PM
    Author     : parag
--%>

<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Contact Us</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(){

                var pauseA = 2500;
                var pause = 3500;

                function newstickerA()
                {
                    // algorithm:
                    // get last element, remove it from the list,
                    // add to first position with hidden style
                    // slideDown the new first element
                    // continue
                    last = $('ul#listtickerA li:last').hide().remove();
                    $('ul#listtickerA').prepend(last);
                    $('ul#listtickerA li:first').slideDown("slow");
                }

                interval = setInterval(newstickerA, pauseA);

                function newsticker()
                {
                    // algorithm:
                    // get last element, remove it from the list,
                    // add to first position with hidden style
                    // slideDown the new first element
                    // continue
                    last = $('ul#listticker li:last').hide().remove();
                    $('ul#listticker').prepend(last);
                    $('ul#listticker li:first').slideDown("slow");
                }

                interval = setInterval(newsticker, pause);
            });
        </script>
    </head>
    <body>
        <%
                    String lang = null, contentcontactus = null;

                    if (request.getParameter("lang") != null && request.getParameter("lang") != "") {
                        lang = request.getParameter("lang");
                    } else {
                        lang = "en_US";
                    }

                    MultiLingualService multiLingualService = (MultiLingualService) AppContext.getSpringBean("MultiLingualService");
                    List<TblMultiLangContent> langContentList = multiLingualService.findContent(lang, "contact");

                    if (!langContentList.isEmpty()) {
                        for (TblMultiLangContent tblMultiLangContent : langContentList) {
                            if (tblMultiLangContent.getSubTitle().equals("content_contactus")) {
                                if ("bn_IN".equals(lang)) {
                                    contentcontactus = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                                } else {
                                    contentcontactus = new String(tblMultiLangContent.getValue());
                                }
                            }
                        }
                    }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        
                        <td class="contentArea-Blogin">
                            <!--Page Content Start-->

                            <%=contentcontactus%>



                            
                      <!--      <div style="height: 1px; border-top: rgb(204,204,204) 1px dashed"> -->
                                &nbsp;
                                
                                
                                
                                
<!--                             <table border="0" cellpadding="0" cellspacing="0" class="tableView_1" width="100%">
                                <tbody>
                                    <tr>
                                        <td valign="top" width="48%">
                                            <table border="0" cellpadding="0" cellspacing="0" class="contactUs_border t_space" width="100%">
                                                <tbody>
                                                    <tr class="t_space">
                                                        <td class="contactUs_header" colspan="4">
								Rural Electrification Board (REB)</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td width="5%">
                                                            &nbsp;</td>
                                                        <td width="25%">
                                                            &nbsp;</td>
                                                        <td width="2%">
                                                            &nbsp;</td>
                                                        <td width="68%">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr valign="top">
							<td class="t-align-left">
								&nbsp;</td>
							<td class="t-align-left" height="25">
								Name</td>
							<td class="t-align-center">
								:</td>
							<td align="left">
								&nbsp;</td>
						</tr>						<tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
                                                            <strong>Designation</strong></td>
                                                        <td class="t-align-left">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;Chief Engineer (Project)</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>Address</strong></td>
                                                        <td class="t-align-left">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;REB, Nikunja, Joharsahara, Khilkhet, &nbsp;Dhaka</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
                                                            <strong>Phone No.</strong></td>
                                                        <td class="t-align-left">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;+ 880-2-8916423</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
                                                            <strong>Fax No.</strong></td>
                                                        <td class="t-align-left">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;+ 880-2-8916400</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>Mobile No.</strong></td>
                                                        <td class="t-align-left">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>e-mail ID</strong></td>
                                                        <td class="t-align-left">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;ceprj@citecho.com
                                                            
                                                        </td>
                                                    </tr>
                                                <tr valign="top">
                                                    <td height="15"></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td valign="top" width="4%">
                                            &nbsp;</td>
                                        <td valign="top" width="48%">
                                            <table border="0" cellpadding="0" cellspacing="0" class="contactUs_border t_space" width="100%">
                                                <tbody>
                                                    <tr class="t_space">
                                                        <td class="contactUs_header" colspan="4">
								Local Government Engineering Department (LGED)</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td width="5%">
                                                            &nbsp;</td>
                                                        <td width="25%">
                                                            &nbsp;</td>
                                                        <td width="2%">
                                                            &nbsp;</td>
                                                        <td width="68%">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr valign="top">
							<td class="t-align-left">
								&nbsp;</td>
							<td class="t-align-left" height="25">
								Name</td>
							<td class="t-align-center">
								:</td>
							<td align="left">
								&nbsp;</td>
						</tr>						<tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
                                                            <strong>Designation</strong></td>
                                                        <td class="t-align-left">
                                                            <strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;Superintending Engineer</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
                                                            <strong>Address</strong></td>
                                                        <td class="t-align-center">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;LGED Bhaban, Sher-e-Bangla Nagar, &nbsp;Dhaka.</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
                                                            <strong>Phone No.</strong></td>
                                                        <td class="t-align-center">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;+ 880-2-8159379</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>Fax No.</strong></td>
                                                        <td class="t-align-center">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;+ 880-2-9120476</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
                                                            <strong>Mobile No.</strong></td>
                                                        <td class="t-align-center">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>e-mail ID</strong></td>
                                                        <td class="t-align-center">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;mohd_azizulhaque@yahoo.com</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" class="contactUs_border t_space" width="100%">
                                                <tbody>
                                                    <tr class="t_space">
                                                        <td class="contactUs_header" colspan="4">
								Bangladesh Water Development Board (BWDB)</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td width="5%">
                                                            &nbsp;</td>
                                                        <td width="25%">
                                                            &nbsp;</td>
                                                        <td width="2%">
                                                            &nbsp;</td>
                                                        <td width="68%">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr valign="top">
							<td class="t-align-left">
								&nbsp;</td>
							<td class="t-align-left" height="25">
								Name</td>
							<td class="t-align-center">
								:</td>
							<td align="left">
								&nbsp;</td>
						</tr>						<tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>Designation</strong></td>
                                                        <td class="t-align-center">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;Director (Procurement Cell)</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>Address</strong></td>
                                                        <td class="t-align-center">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;BWDB, WAPDA Building, Motijheel C/A &nbsp;Dhaka.</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>Phone No.</strong></td>
                                                        <td class="t-align-center">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;+ 880-2-7126155</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>Fax No.</strong></td>
                                                        <td class="t-align-center">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;+ 880-2-9564763</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>Mobile No.</strong></td>
                                                        <td class="t-align-center">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>e-mail ID</strong></td>
                                                        <td class="t-align-center">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;director-cpc@yahoo.com</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td valign="top">
                                            &nbsp;</td>
                                        <td valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" class="contactUs_border t_space" width="100%">
                                                <tbody>
                                                    <tr class="t_space">
                                                        <td class="contactUs_header" colspan="4">
								Roads & Highways Department (RHD)</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td width="5%">
                                                            &nbsp;</td>
                                                        <td width="25%">
                                                            &nbsp;</td>
                                                        <td width="2%">
                                                            &nbsp;</td>
                                                        <td width="68%">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr valign="top">
							<td class="t-align-left">
								&nbsp;</td>
							<td class="t-align-left" height="25">
								Name</td>
							<td class="t-align-center">
								:</td>
							<td align="left">
								&nbsp;</td>
						</tr>						<tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
                                                            <strong>Designation</strong></td>
                                                        <td class="t-align-left">
							<strong>:</strong>
                                                        </td>
                                                        <td align="left">
                                                             &nbsp;Superintending Engineer</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>Address</strong></td>
                                                        <td class="t-align-left">
								<strong>:</strong></td>
                                                        <td align="left">
                                                             &nbsp; Sarak Bhaban, Ramna, Dhaka</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>Phone No.</strong></td>
                                                        <td class="t-align-left">
								<strong>:</strong></td>
                                                        <td align="left">
                                                             &nbsp;+ 880-2-9557989</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
                                                            <strong>Fax No.</strong></td>
                                                        <td class="t-align-left">
								<strong>:</strong></td>
                                                        <td align="left">
                                                             &nbsp;+ 880-2-9551033</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>Mobile No.</strong></td>
                                                        <td class="t-align-left">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr valign="top">
                                                        <td class="t-align-left">
                                                            &nbsp;</td>
                                                        <td class="t-align-left" height="25">
								<strong>e-mail ID</strong></td>
                                                        <td class="t-align-left">
								<strong>:</strong></td>
                                                        <td align="left">
                                                            &nbsp; ea_hasan@yahoo.com</td>
                                                    </tr>
                                                 <tr valign="top">
                                                    <td height="18"></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>  -->




                            <!--Page Content End-->
                        </td>
                            <td width="266">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include></td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
