<%-- 
    Document   : GenerateDao
    Created on : Sep 12, 2011, 10:38:59 AM
    Author     : taher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Automated DAO and IMPL</title>
        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript">
            function addMore(){                
                var val = "<br/><input type='text' name='tabName'/>";
                $("#tabTD").append(val);
            }
            function checkSub() {
                var cnt = 0;
                $(":input[type='text']").each(function(){
                     if($.trim($(this).val())==""){
                         cnt++;
                     }
                })
                if(cnt!=0){
                    alert('All Fields are mandatory');
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <form action="<%=request.getContextPath()%>/GenerServlet.jsp" method="post">
            <table align="center" width="50%" border="2">
                <tr>
                    <th colspan="2">Generate DAO and IMPL</th>
                </tr>
                <tr>
                    <td>Enter Table Name : <a onclick="addMore();" href="javascript:void(0);">Add More</a></td>
                    <td id="tabTD">
                        <input type="text" name="tabName"/>
                    </td>
                </tr>
                <tr>
                    <td>DAO Path : </td>
                    <td>
                        <input type="text" name="daoPath" value="E:\eGPV6\eGPDao\src\main\java\com\cptu\egp\eps\dao\daointerface"/>
                    </td>
                    <!--                <td>
                                        <label>
                                            Tick for Same Path : <input type="checkbox" name="daoPath"/>
                                        </label>
                                    </td>-->
                </tr>
                <tr>
                    <td>IMPL Path : </td>
                    <td>
                        <input type="text" name="implPath" value="E:\eGPV6\eGPDao\src\main\java\com\cptu\egp\eps\dao\daoimpl"/>
                    </td>
                </tr>
                <tr>
                    <td>Import : </td>
                    <td>
                        <input type="text" name="importVar" value="com.cptu.egp.eps"/>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <input type="submit" name="genButton" value="Generate" onclick="return checkSub();"/>
                        <input type="hidden" name="action" value="genDao"/>
                    </td>
                </tr>
            </table>
        </form>
    </body>
<!--
    EXAMPLE -    
            DAO Path : E:\eGPV6\eGPDao\src\main\java\com\cptu\egp\eps\dao\daointerface
            IMPL Path : E:\eGPV6\eGPDao\src\main\java\com\cptu\egp\eps\dao\daoimpl
            Import : com.cptu.egp.eps
-->
</html>
