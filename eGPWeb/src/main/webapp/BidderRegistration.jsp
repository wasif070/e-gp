<%-- 
    Document   : BidderRegistration
    Created on : Apr 8, 2018, 5:23:27 PM
    Author     : Aprojit
--%>

<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.web.servicebean.LoginMasterSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--<%@ page import="net.tanesha.recaptcha.ReCaptcha" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaFactory" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaImpl" %>
<%@ page import="net.tanesha.recaptcha.ReCaptchaResponse" %>--%>
<%@ page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@ page import="com.cptu.egp.eps.web.utility.AppContext"%>
<jsp:useBean id="loginMasterDtBean" class="com.cptu.egp.eps.web.databean.LoginMasterDtBean"/>
<jsp:setProperty property="*" name="loginMasterDtBean"/>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head> 
        <%
            response.setHeader("Cache-Control","no-cache");
            response.setHeader("Cache-Control","no-store");
            response.setHeader("Pragma","no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Bidder Registration - Login Account Details</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/js/poshytipLib/tip-yellowsimple/tip-yellowsimple.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>

        <link href="resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="resources/js/poshytipLib/jquery.poshytip.js" type="text/javascript"></script>
        <style>
            .formNoteTxt, #cpvTip {
                font-style: italic;
                    
            }
        </style>
        <script type="text/javascript">


            $(window).bind("load",function() {
                $("#cmbNational").val("Bhutanese");
                
//               $('#recaptcha_response_field').blur(function() {
//                   if($('#recaptcha_response_field').val()!=""){
//                       $.post("<%//=request.getContextPath()%>/CommonServlet", {challenge:$('#recaptcha_challenge_field').val(),userInput:$('#recaptcha_response_field').val(),funName:'captchaValid'},
//                       function(j){
//                           if(j.toString()=="OK"){
//                               $('span.#vericode').css("color","green");
//                           }
//                           else{
//                               javascript:Recaptcha.reload();
//                               $('span.#vericode').css("color","red");
//                           }
//                           $('span.#vericode').html(j);
//                       });
//                   }else{
//                       $('span.#vericode').css("color","red");
//                       $('#vericode').html("Please enter Verification Code");
//                   }
//               });
           });



            $(document).ready(function() {
             $('.formTxtBox_1').poshytip({
                className: 'tip-yellowsimple',
                showOn: 'focus',
                alignTo: 'target',
                alignX: 'right',
                alignY: 'center',
                offsetX: 5,
                showTimeout: 100
                 }); 
                $("#frmUserReg").validate({
                    rules: {
                        //emailId:{email:true},
                        //,spacevalidate:true
                        password: { required: true, minlength: 8, maxlength:25, alphaForPassword:true},
                        confirmPassWord: { required: true,equalTo: "#txtPass"},
                        hintQuestion: { required: true},
                        hintAnswer: { required: true,maxlength:100},
                        businessCountryName1 : { required: true},
                        checkbox: { required: true }
                        //isJvca: { required: true }
                    },
                    messages: {
                        //emailId:{email:"<div class='reqF_1'>e-mail ID must be in xyz@abc.com format</div>" },
                        businessCountryName1 : { required: "<div class='reqF_1'>Please select Country of Business</div>"},
                        password: { required: "<div class='reqF_1'>Please enter Password</div>",
                            minlength: "<div class='reqF_1'>Please enter at least 8 characters and password must contain both alphabets and numbers</div>",
                           maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>",

                            alphaForPassword: "<div class='reqF_1'>Please enter alphanumeric only</div>"},
                            //spacevalidate:"<div class='reqF_1'>Space is not allowed.</div>"},

                        confirmPassWord: {required: "<div class='reqF_1'>Please retype the Password</div>",
                            //spacevalidate:"<div class='reqF_1'>Space is not allowed.</div>",
                            equalTo: "<div class='reqF_1' id='msgPwdNotMatch'>Password does not match. Please try again</div>"},
                            //NotequalTo: "<div style='color:green'><b>Password Matches</b></div>",
                        hintQuestion: {required: "<div class='reqF_1'>Please select Hint Question</div>"},
                        hintAnswer: {required: "<div class='reqF_1'>Please enter your Hint Answer</div>",
                                    maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed</div>"},
                        checkbox: {required: "<div class='reqF_1'>Please accept Terms and Conditions</div>"}
                        //isJvca: {required: "<div class='reqF_1'>Please Select JVCA.</div>"}
                    },
                    errorPlacement: function(error, element) {                                
                        if (element.attr("name") == "checkbox"){
                            error.insertAfter("#tnc");
                        }                        
                        else if (element.attr("name") == "password"){
                            error.insertAfter("#passnote");
                        }
                        else if (element.attr("name") == "hintAnswer"){
                            error.insertAfter("#hintanssp");
                        }
                        else{
                            error.insertAfter(element);
                        }
                    }
                });
                
                $('#frmUserReg').submit(function() {

                    var dataArray = $("#frmUserReg").serializeArray(),
                        dataObj = {};

                    $(dataArray).each(function(i, field){
                        dataObj[field.name] = field.value;
                    });

                    var recaptcha = (dataObj['g-recaptcha-response']);

                    if(recaptcha=="")
                    {
                        $('#captchaMsg').html("Are you a robot?");
                        return false;
                    }
                    else
                    {
                        $('#captchaMsg').html("");
                        return true;
                    }
                });
                               
            });
        </script>

<!--        <script type="text/javascript">
            $(function() {
                $('#cmbHintQue').change( function(){
                    if($('#cmbHintQue').val() == 'other' ){
                        $('#hintQueTr').show();
                    }
                    else{
                        $('#hintQueTr').hide();
                    }
                });
            });
        </script>-->
        <script type="text/javascript">
            $(function() {
                $('#txtMail').blur(function() {
                    if($('#txtMail').val()==""){
                        $('span.#mailMsg').css("color","red");
                        $('span.#mailMsg').html("Please enter e-mail ID");
                    }else{
                        $('span.#mailMsg').css("color","red");
                        $('#mailMsg').html("Checking for unique e-mail ID...");
                        $.post("<%=request.getContextPath()%>/CommonServlet", {mailId:$('#txtMail').val(),funName:'verifyMail'},
                        function(j){
                            if(j.toString().indexOf("OK", 0)!=-1){
                                $('span.#mailMsg').css("color","green");
                            }
                            else{
                                $('span.#mailMsg').css("color","red");
                            }
                            $('span.#mailMsg').html(j);
                        });
                    }
                });
            });
            <%-- If captcha is not working then enable it Rokib Start--%>
           <%-- $(function() {
                $('#recaptcha_response_field').blur(function() {
                    if($('#recaptcha_response_field').val()!=""){
                        $.post("<%=request.getContextPath()%>/CommonServlet", {challenge:$('#recaptcha_challenge_field').val(),userInput:$('#recaptcha_response_field').val(),funName:'captchaValid'},
                        function(j){
                            if(j.toString()=="OK"){
                                $('span.#vericode').css("color","green");
                            }
                            else{
                                javascript:Recaptcha.reload();
                                $('span.#vericode').css("color","red");
                            }
                            $('span.#vericode').html(j);
                        });
                    }else{
                        $('span.#vericode').css("color","red");
                        $('#vericode').html("Please enter Verification Code");
                    }
                });
            });--%>
                <%-- If captcha is not working then enable it Rokib End--%>
        </script>
        <script type="text/javascript">
            $(function() {
                $('#frmUserReg').submit(function() {
                    if($('#txtMail').val()==""){
                        $('span.#mailMsg').css("color","red");
                        $('span.#mailMsg').html("Please enter e-mail ID");
                        return false;
                    }
                    if($('span.#mailMsg').html()!="OK"){
                        return false;
                    }
                });
            });
        </script>
        <script type="text/javascript">

            //Jvca tr hide and show
//            $(function() {
//                $('#cmbRegType').change(function() {
//                    if(($('#cmbRegType').val()=="individualconsultant")||($('#cmbRegType').val()=="govtundertaking")||($('#cmbRegType').val()=="media")||($('#cmbRegType').val()=="other")){
//                        $('#cmbJvca').val("no");
//                        $('#trJvca').css("display", "none");
//                    }else{
//                        $('#cmbJvca').val("");
//                        $('#trJvca').css("display", "table-row");
//                    }
//                });
//            });

            function Validate()
            {
//                if(document.getElementById("cmbHintQue").value=="other")
//                {
//                    if($.trim(document.getElementById("txtaQuestion").value)=='')
//                    {
//                        document.getElementById("hintquestionerror").innerHTML="Please enter Hint Question.";
//                        return false;
//                    }
//                }    
                var rType = document.getElementById("cmbRegType").value;
                if(rType == "")
                {                    
                   document.getElementById("hintRType").innerHTML="Please select Registration Type.";
                   return false; 
                }        
            }

            function chkPasswordMatches(){
                var objPwd = document.getElementById("txtPass");
                var objConfirmPwd = document.getElementById("txtConPass");
                if(objPwd != null && objConfirmPwd != null){
                    if($.trim(objPwd.value) == "" || $.trim(objConfirmPwd.value) == ""){
                        document.getElementById("pwdMatchMsg").innerHTML = "";
                    }else{
                        var msgPwdMatchObjDiv = document.getElementById("pwdMatchMsg");
                        if(objPwd.value == objConfirmPwd.value){
                            var msgPwdNotMatch = document.getElementById("msgPwdNotMatch");
                            if(msgPwdNotMatch!=null){
                                if(msgPwdNotMatch.innerHTML == "Password does not match. Please try again"){
                                    msgPwdNotMatch.style.diplay = "none";
                                    msgPwdNotMatch.innerHTML = "";
                                }
                            }
                            msgPwdMatchObjDiv.innerHTML = "Password Matches";
                        }else{
                            msgPwdMatchObjDiv.style.diplay = "none";
                            msgPwdMatchObjDiv.innerHTML = "";
                        }
                    }
                }
                
            }

            function ClearMessage(){
                if(document.getElementById("cmbHintQue").value=='other')
                {
                    if($.trim(document.getElementById("txtaQuestion").value)!='')
                    {
                         if($.trim(document.getElementById("txtaQuestion").value).length <= 50)
                            {
                                document.getElementById("hintquestionerror").innerHTML="";
                            }
                            else
                            {
                                document.getElementById("hintquestionerror").innerHTML="Maximum 50 characters are allowed.";
                                return false;
                            }
                    }
                    else
                    {
                           document.getElementById("hintquestionerror").innerHTML="Please enter Hint Question.";
                           return false;
                    }
                }
            }
            $(function() {
                $('#frmUserReg').submit(function() {
                    if($('#recaptcha_response_field').val()==""){
                        $('span.#vericode').css("color","red");
                        $('#vericode').html("Please enter Verification Code");
                        return false;
                    }
                    if($('#vericode').html()!="OK"){
                        javascript:Recaptcha.reload();
                        return false;
                    }
                });
            });
            $(function() {
                $('#btnReset').click(function() {
                    $('#vericode').html(null);
                    $('#pwdMatchMsg').html(null);
                    $('#mailMsg').html(null);
                    javascript:Recaptcha.reload();
                    var validator = $("#frmUserReg").validate();
                    validator.resetForm();
                });
            });            
            /*function myMethod(e){
               if(e.keyCode==17 || e.keyCode==93){                   
                   alert('Copy Paste not allowed.');
                   return false;                    
               }
            }
            function rajesh(e){
              $(e).bind("contextmenu",function(e){
                return false;
            });
          }*/
          function copyPasteCheck(){
            jAlert("Copy and Paste not allowed.","Bidder Registration", function(RetVal) {
            });
            return false;
          }        
          
         function individualSelection(rType){
            var rTV = document.getElementById("cmbRegType").options[document.getElementById('cmbRegType').selectedIndex].text;
            if(rType == ""){       
                document.getElementById("hintRType").innerHTML="Please select Registration Type.";
            }else{
                document.getElementById("hintRType").innerHTML="";
                jAlert(" \""+rTV+"\" is selected as Registration Type?","Registration Type");
            }
         }
          
        </script>
    </head>
    <body>
        <%
            String uId="0";
            if(session.getAttribute("userId")!=null){
                uId=session.getAttribute("userId").toString();
            }
            LoginMasterSrBean loginMasterSrBean = new LoginMasterSrBean(uId);
            uId=null;
                                            String msg=null;
                                            if ("Submit".equals(request.getParameter("button"))) {
                                                if(loginMasterDtBean.getEmailId()!=null){
                                                    if(!loginMasterDtBean.getEmailId().matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9\\-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")){                                                    
                                                        msg="Please enter Valid e-mail ID";
                                                    }else{
                                                        CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                        msg=commonService.verifyMail(loginMasterDtBean.getEmailId());
                                                    }
                                                }else{
                                                    msg="Please enter e-mail ID";
                                                }
                                                if(msg.equalsIgnoreCase("ok")){
                                                    String tmp = "";
                                                    for (String str : request.getParameterValues("businessCountryName1")) {
                                                        tmp += str + ",";
                                                    }
                                                    loginMasterDtBean.setBusinessCountryName(tmp.substring(0, tmp.length() - 1));
                                                    if(loginMasterSrBean.userRegister(loginMasterDtBean,request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/")))){
                                                    response.sendRedirect(request.getContextPath()+"/Index.jsp?succMsg=y");
                                                    }else{
                                                        msg="Problem in Bidder Registration";
                                                }
                                            }
                                            }
                                %>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0" >
                    <tr valign="top">
                                    
                        <td class="contentArea-Blogin">
                            <!--Page Content Start-->
                            
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="pageHead_1">
                                <tr>
                                    <td align="left">Bidder Registration - Login Account Details</td>
<!--                                    <td align="right" valign="middle"><a href="<%=request.getContextPath()%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1] %>" target="_blank" ><img src="<%=request.getContextPath()%>/resources/images/Dashboard/helpIcon.png" /></a></td>
                                    <td width="40" align="left" valign="middle"><a href="<%=request.getContextPath()%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1] %>" target="_blank" style="color: #333;" > &nbsp;Help</a></td>-->
                                </tr>
                            </table>
                            
                            <form id="frmUserReg" method="post" action="NewUserRegistrationLoginDetails.jsp">
                                <%if(msg!=null){%><br/><div class="responseMsg errorMsg"><%=msg%></div><%}%>
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                   
                                    <tr>
                                        <td colspan="2" class="ff" align="left" style="font-style: italic; font-weight: normal;">Bidders to register in English only. <br/>Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">Email: <span>*</span></td>
                                        <td width="80%"><input name="emailId" type="text" class="formTxtBox_1" id="txtMail" style="width:200px;" title="Bidder Registration Instruction, please ensure valid e-mail ID is used to Register or Log In.  <br/> <br/>  Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. Special characters may be added. <br/> <br/> 
 The email should be registered in the Bidders name, which is only used by the Bidder. <br/> <br/>  All e-mail alerts will be sent by GPPMD Office of Electronic Government Procurement (e-GP) System to this e-mail ID only. Example: abcd@wxyz.com
" />
                                            <span id="emailnote" class="formNoteTxt">E-mail ID should be valid. Example: abcd@wxyz.com</span>
                                            <span id="mailMsg" style="color: red; "></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Password : <span>*</span></td>
                                        <td><input name="password" type="password" class="formTxtBox_1" id="txtPass" style="width:200px;" onblur="chkPasswordMatches();" autocomplete="off" title="Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. Special characters may be added."/><span id="passnote" class="formNoteTxt">
                                                
                                                
                                                Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. 
                                                <br/>Special characters may be added.</span></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Confirm Password : <span>*</span></td>
                                        <td><input name="confirmPassWord" type="password" class="formTxtBox_1" id="txtConPass" style="width:200px;" onblur="chkPasswordMatches();" onpaste="return copyPasteCheck();" autocomplete="off"/>
                                            <div style="color: green" id="pwdMatchMsg"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Hint Question : <span>*</span></td>
                                        <td><select name="hintQuestion" class="formTxtBox_1" id="cmbHintQue" style="width:auto;min-width: 200px;">
                                                <option value="">Select Hint Question</option>
                                                <%for(SelectItem hintQus : loginMasterSrBean.getHintQueList()){%>
                                                                <option value="<%=hintQus.getObjectId()%>"><%=hintQus.getObjectValue()%></option>
                                                <%}%>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr style="display: none;" id="hintQueTr">
                                        <td class="ff">Create your Own Hint Question :<span>*</span></td>
                                        <td><textarea name="hintQuestionOwn" rows="3" class="formTxtBox_1" id="txtaQuestion" style="width:400px;" onblur="ClearMessage()"></textarea>
                                            <span id="hintquestionerror" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Hint Answer : <span>*</span></td>
                                        <td><input name="hintAnswer" type="text" maxlength="50" class="formTxtBox_1" id="txtHintAns" style="width:200px;" />
                                            <span class="formNoteTxt" id="hintanssp">Please remember the answer of the Hint Questions. This is Mandatory during Change Password phase only, <br/> until SMS alert is active</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Nationality : <span>*</span></td>
                                        <td><select name="nationality" class="formTxtBox_1" id="cmbNational" style="width:auto;">
                                                <%for(SelectItem nationality : loginMasterSrBean.getNationalityList()){%>
                                                        <option value="<%=nationality.getObjectId()%>"><%=nationality.getObjectValue()%></option>
                                                <%}%>                                                
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Registration Type : <span>*</span></td>
                                        <td><select name="registrationType" class="formTxtBox_1" id="cmbRegType" style="width:200px;" title="Individual Consultant is a self-employed independent businessperson who has a special field of expertise or skill" onchange="individualSelection(this.value);">
                                                <option value="">Select Registration Type</option>
                                                <option value="contractor">Bidder / Consultant</option>
                                                <%--<option value="supplier">Applicant</option>
                                                <option value="contractsupplier">Contractor & Supplier</option>
                                                <option value="consultant">Consultant</option>--%>
                                                <option value="individualconsultant">Individual Consultant</option>
                                                <%--<option value="govtundertaking">Government owned Enterprise</option>
                                                <option value="media">Media</option>--%>
                                                <%--<option value="other">Other</option>--%>
                                            </select>
                                        <span class="formNoteTxt">Please select correct Registration Type</span>
                                        <span id="hintRType" class="reqF_1"></span>
                                        </td>
                                            
                                    </tr>
                                    <input type="hidden" value="no" name="isJvca" id="cmbJvca"/>
<!--                                    <tr id="trJvca">
                                        <td class="ff">Is JVCA? : <span>*</span></td>
                                        <td><select name="isJvca" class="formTxtBox_1" id="cmbJvca">
                                                <option value="">Select JVCA</option>
                                                <option value="yes">Yes</option>
                                                <option value="no">No</option>
                                            </select></td>
                                    </tr>-->
                                    <tr>
                                        <td class="ff">Country of Business : <span>*</span></td>
                                        <td><select name="businessCountryName1" class="formTxtBox_1" id="cmbCountryBus" multiple size="5">
                                                <%for(SelectItem country : loginMasterSrBean.getCountryList()){%>
                                                <option value="<%=country.getObjectId()%>" <%if(country.getObjectValue().equals("Bhutan")){%>selected<%}%>><%=country.getObjectValue()%></option>
                                                <%}%>                                                
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Captcha: <span>*</span></td>
                                        <td>
                                            <div class="g-recaptcha" name="recaptcha" id="recaptcha"
                                                    data-sitekey="6LdvXjEUAAAAABHwviA2vAyVh74WMTSc1Gh_oh3h">
                                            </div>
                                            <div class="has-error">
                                                <span id="captchaMsg" style="color: red; "></span>
                                            </div>
                                            <%
//                                                        String pubKey = "6Lfw4r0SAAAAAO2WEc7snoFlSixnCQ2IGaO7k_iO";
//                                                        String privKey = "6Lfw4r0SAAAAAN_Pr3YGPA39kRIyNsuMCDJIpUsS";
//                                                        StringBuffer reqURL = request.getRequestURL();
//                                                        if(reqURL.toString().substring(reqURL.toString().indexOf("//")+2,reqURL.toString().lastIndexOf("/")).equals("www.eprocure.gov.bd")){
//                                                            pubKey = "6Lc0ssISAAAAAKh369J1vrGKmSfpEGhbr07sl1gS";
//                                                            privKey = "6Lc0ssISAAAAAJf6jPcfSBElxYSRk1LB46MBsWUk";
//                                                        }
//                                                        ReCaptcha c = ReCaptchaFactory.newSecureReCaptcha(pubKey, privKey, false);
//                                                        out.print(c.createRecaptchaHtml(null, null));
//                                                        pubKey=null;
//                                                        privKey=null;
//                                                        reqURL=null;
                                            %>
                                            <span class="formNoteTxt">If you cannot read the text, you may get new Verification Code by clicking <img alt="refresh"  src="resources/images/refresh.gif"/>,<br/> and if you want to hear the Verification Code, please click <img alt="sound"  src="resources/images/sound.gif"/> </span>
                                            <span class="reqF_1" id="vericode"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Terms and Conditions : <span>*</span></td>
                                        <%--<textarea cols="20" name="textfield10" rows="6" class="formTxtBox_1" id="textfield9" style="width:700px;font-weight: bold; text-align: justify;" readonly>--%>
                                        <td>
                                            <div style=" padding: 5px; border-top: 1px #ededed solid; border-left: 1px #ededed solid; width:100%; height: 250px; text-align: justify;overflow: scroll">
                                                <b style="color: green;">Terms and Conditions for e-GP System User Agreement</b>
                                                <br/><br/>Electronic Government Procurement (e-GP) system (i.e. https://www.egp.gov.bt) of the Royal Government of Bhutan is developed, owned and operated by Government Procurement and Property Management Division (GPPMD), Ministry of Finance, Royal Government of Bhutan (RGOB) for carrying out the procurement activities of the Procuring Agencies of the RGOB.
                                                <br/><br/>GPPMD  of the RGoB also runs a training server (http://egptraining.egp.gov.bt/) to allow the users to try and learn by themselves all the functionalities of e-GP system through an online mock-up of real transactional e-GP system. Users may try all activities, which is available in real transaction system. None of the activities done in training servers will be taken as real transactions.
                                                <br/><br/>For carrying out the real procurement transactions, users must use the e-GP system at https://www.egp.gov.bt/). 
                                                <br/><br/>User account will be created only when the following terms and conditions of e-GP system user agreement is read and accepted.
                                                <br/><br/><b>TERMS AND CONDITIONS OF E-GP SYSTEM USER AGREEMENT</b>
                                                <br/><br/>For accessing and using this e-GP user services, you shall be deemed to have accepted to be legally bound by these terms and conditions of use and comply with all of the terms and conditions given below, and the guidelines as stipulated in Electronic Government Procurement guidelines:
                                                <br/><br/>Bidder/Supplier/Consultant registration request, Email verification, and Credential documents verification
                                                <br/><br/><b>Email Verification</b>
                                                <br/><br/>Your email will be used as the username for accessing e-GP system. Upon submission of your basic user identity information opened by clicking on the New User Registration button from the home page of e-GP system, you will receive an email in the email address provided by you, an email from e-GP system (https://www.egp.gov.bt ) with a link to click, your unique security key, and other instruction related to your credential documents verification, and payment process. When you click the link provided in your email, an email verification page with a form will open. You need to enter the email, password and the received security key, and press the Submit button. If you correctly enter the information, this process will complete the email verification process successfully.
                                                <br/><br/>Your account will be successfully created, and you will be displayed another form for entering your specific information, uploading digitally scanned mandatory credential documents (scanned documents of Tax Payment No., valid Trade License, CDB No., Company Registration Certificate, CID No. of contact person, must be easily readable).
                                                <br/><br/><b>Credential documents verification (In case of Hard Copy documents) </b>
                                                <br/><br/>Bidders/ Suppliers/Consultants, may visit e-GP Users Registration Desk  in the GPPMD, Ministry of Finance (MoF), RGoB, Deobum Lam, Box 116, Thimphu, Bhutan with the original credential documents used during online registration process or send the documents via registered post or courier service for the post-verification for authenticity. Bidders/ Suppliers/Consultants, must also include envelope return address written or typed, and with required postage stamp. The verification process may take from one day to two weeks.
                                                <br/><br/>After verification of the original credential documents, Bidders/ Suppliers/Consultants get the confirmation email notification of registration and will instantly get full access to secured personal dashboard for user specific functions of the e-GP system as the e-GP system User.
                                                <br/><br/>Procuring agencies, development partners, payment network partners (banks and others), and media will be registered through official communication with GPPMD, MoF, RGoB.
                                                <br/><br/><b>Maintaining confidentiality</b>
                                                <br/><br/>Users are responsible for maintaining the confidentiality of their password and are fully responsible for all activities that occur using your account (e-mail ID and password). e-GP system does not store user passwords, but it will store only the generated irreversible hash value of the password as e-signature. User must notify e-GP Admin of GPPMD via ( Admin email address) of any unauthorized use of your password or any other suspected security breaches. Users must ensure that they appropriately log-out every time from their unattended computers or from the computers you are using in public places. GPPMD is not liable for any loss or damage arising from such compromise of your user account and password.
                                                <br/><br/>The e-GP system allows modifying, updating their user details including password except the login e-mail ID. 
                                                <br/><br/><b>Internet Browser and Users' Computer compatibility</b>
                                                <br/><br/>To access the e-GP system securely, users should use appropriate web browsers and their associated security settings. However because of the rapid development of new browsers and new security measures coming up frequently, users need to update or install new components and configuration settings as and when these come into effect. Current version of e-GP system can be best viewed at all popular latest browsers.
                                                <br/><br/><b>Applicable Time</b>
                                                <br/><br/>The e-GP system shall use the Government Data Centre  server time as the reference time for all time-bound activities of procurement processes. The Government Data Centre is located  at Thimphu TechPark, Babesa.
                                                <br/><br/><b>Proprietary Rights</b>
                                                <br/><br/>This e-GP system is developed and maintained by the GPPMD, Ministry of Finance of the RGoB.
                                                <br/><br/>The materials located on this e-GP web system including the information and software programs (source code) are copyrighted to GPPMD, MoF, RGoB.
                                                <br/><br/><b>Registration Fees</b>
                                                <br/><br/>Bidders/Suppliers/Consultants will be charged a minimal fee of Nu. 500 (Ngultrum Five Hundred) for the user registration, and annually it should be renewed. Nu. 250 (Ngultrum Tow Hundred Fifty) will be charged each year for renewal of their account. For international Bidders/Consultants and Consultants, registration fee is USD $100 (US Dollars One Hundred Only) and annual renewal fee is USD $30 (US Dollars Thirty Only). Users must make sure the required amount is deposited to GPPMD designated account prior to expiry of membership.
                                                <br/><br/>Users may be charged and/or waived specified amount of money for different categories of use including registration, subscription and periodic renewal, additional storage space, transactions, facilities to use specific features/modules of the e-GP system and different services from the operation, maintenance and management entity. GPPMD shall have the rights to set reasonable charges or waiver to promote the use of the e-GP system and sustainability of the system in long run.
                                                <br/><br/><b>Tender Submission</b>
                                                <br/><br/>The Bidders/Suppliers/Consultants are responsible to plan their time sufficient to complete the documents upload, third party transactions like bid security preparation and submission through banks, verify completeness of tender, and final submission of Bidders? documents for the specific tenders. Before final submission, the Bidders/Suppliers/Consultants may upload documents, fill-in required online forms, modify and verify the documents, and complete other activities, part by part. But attempt to submit incomplete tender will not be allowed by the e-GP system.
                                                <br/><br/><b>Payment process</b>
                                                <br/><br/>Until the e-Payment infrastructure is available in Bhutan, the e-GP system will use the service of Financial Institutions. Financial Institutions and other payment service providers will get secured access to the e-GP system with their own dedicated and secured dashboard, from where, they can carry out the financial transactions related to public procurement such as collecting fees and charges, providing guarantees, tracking the guarantees, making payment transactions, and other service fees, etc.
                                                <br/><br/>Bidders/Suppliers/Consultants should pay to bank the required amount of money for the specific purpose of transaction with e-GP system. Financial Institute will collect the charges and fees from bidders crediting the account opened by GPPMD for specific service/transaction in e-GP system, and bank will immediately update the payment information in the e-GP system through the provided bank user access.
                                                <br/><br/>When bank guarantees and securities (bid security, performance security, etc.) are issued by the bank, the same should be immediately updated in the e-GP system.
                                                <br/><br/>When procuring agencies or GPPMD instructs the bank for releasing the guarantees or securities, and deposit in specific procuring agencies or GPPMD accounts, the bank will carry out the transactions, and update the transaction information in the e-GP system.
                                                <br/><br/>The GPPMD shall not be responsible for the transactions made by banks using bank rules with the e-GP system users.
                                                <br/><br/>In case of international Bidders/Suppliers/consultants, payments should be made to the designated bank account opened by GPPMD through bank wire transfer or any other method clearly mentioning the purpose of payment.
                                                <br/><br/>International Bidders/Suppliers/Consultants must communicate with the banks of e-GP online payment network for updating their payment details in e-GP system. Any charges incurred for payment transfer, communication or any currency conversion should be borne by the bidders/applicants/consultants themselves.
                                                
                                                <br/><br/><b>Virus and Integrity of documents</b>
                                                <br/><br/>If the electronic records entered online and files containing the tender/ application/ proposal are corrupt, contain a virus, or are unreadable for any reason, the tender will not be considered.  It is strictly the responsibility of the bidder/applicant/Consultant (national or international) to ensure the integrity, completeness and authenticity of the Tender, and also should comply with the applicable laws of the Kingdom of Bhutan.
                                                <br/><br/><b>External Web References</b>
                                                <br/><br/>GPPMD does not take any responsibility of its availability and authenticity of the external third party web references, links referred in the e-GP system, as GPPMD does not have any control over those websites.
                                                <br/><br/><b>Operation, Maintenance and Management</b>
                                                <br/><br/>The GPPMD reserves the right to outsource operation, maintenance and management services of e-GP data center, e-GP system and other related services to any third party. The users of e-GP system are to be obliging such any agreement with any outsourced firm/company.
                                                <br/><br/><b>Governing Law</b>
                                                <br/><br/>This terms and conditions of use agreement of e-GP system shall all be governed by the laws of the Kingdom Bhutan applicable to agreements made and to be performed in Bhutan.
                                                <br/><br/>Royal Government of Bhutan and GPPMD reserve the right to initiate any legal action against those users violating any of the above mentioned terms & conditions of e-GP system user agreement.
                                                <br/><br/>Changes in e-GP system and terms and conditions of use
                                                <br/><br/>GPPMD shall have the right to modify clauses of the terms and conditions without prior notice.
                                                <br/><br/>GPPMD reserves the right to modify, add, delete and/or change the functions, user interface, contents, and other items in e-GP system at any time without any prior notice. User is responsible to use the updated e-GP system functions and terms and conditions of use.
                                                <br/><br/>Issued on 29 April 2016<br/>&nbsp;
                                            </div>
                                            <!--<a target="_blank" href="termsncondition/Disclaimer_and_Privacy_Policy.pdf">Download Terms and Conditions</a>-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><input type="checkbox" name="checkbox" id="chkAgree" />
                                            <span id="tnc">I have read, understood and accepted the Terms and Conditions</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><label class="formBtn_1">
                                                <input type="submit" name="button" id="btnSubmit" value="Submit" onclick="return Validate();"/>
                                            </label>
                                            &nbsp;
                                            <label class="formBtn_1">
                                                <input type="reset" name="Reset" id="btnReset" value="Reset" />
                                            </label></td>
                                    </tr>
                                        <tr>

                                        <!--<td colspan="2"> <span style="color : red">"To ensure you receive future emails, please add <a href="mailto:<%=XMLReader.getMessage("spammsgemailid") %>"><%=XMLReader.getMessage("spammsgemailid")%></a> email address of e-GP User Registration Desk to your Safe Sender list"</span></td>-->
                                            <td colspan="2" style="padding-left: 30px;"> <span style="color : #FF9326">"To ensure you receive future emails, please add <a href="mailto:<%=XMLReader.getMessage("spammsgemailid") %>">abcd@xxx.gov.bt</a> email address of Electronic Government Procurement (e-GP) <br/> GPPMD office to your Safe Sender list"</span></td>
                                    </tr>
                                </table>
                                       
                            </form>
                            <!--Page Content End-->
                        </td>
<!--                                    <td width="266"  valign="top" class="td-background">
                                        <%--<jsp:include page="resources/common/Left.jsp" ></jsp:include>--%>
                                    </td>-->
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>

<%
   loginMasterDtBean=null;
   loginMasterSrBean=null;
%>
</html>