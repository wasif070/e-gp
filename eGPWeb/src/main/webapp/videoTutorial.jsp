<%-- 
    Document   : videoTutorials
    Created on : May 31, 2017, 12:25:56 PM
    Author     : feroz
--%>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Interactive Audio Visual</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/simpletreemenu.js"></script>
        <style>
        div.gallery {
            margin: 5px;
            border: 1px solid #ccc;
            float: left;
            width: 180px;
        }

        div.gallery:hover {
            border: 1px solid #777;
            background-color: lightyellow;
        }

        div.gallery img {
            width: 100%;
            height: auto;
        }

        div.desc {
            padding: 15px;
            text-align: center;
        }
        
        #homePage{
		text-decoration:none;
		color:#FFF;
		padding-bottom:10px;
	}
        
        </style>
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link href="css/video-js.css" rel="stylesheet">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/videojs-ie8.min.js"></script>
  <script src="js/video.js"></script>
  	<script type="text/javascript" src="js/ddlevelsmenu.js"></script>
	<script type="text/javascript" src="js/simpletreemenu.js"></script>
	
	<link href="css/home.css" rel="stylesheet" type="text/css" />
	<link href="css/simpletree.css" rel="stylesheet" type="text/css" />	
	<script type="text/javascript">

		$(document).ready(function () {
			$("#videoDiv").hide();
			$("#listDiv").show();
			ddtreemenu.flatten('treemenu1', status);
		});
		
		function loadVideo(fileName, elId, name){

			document.getElementById("video").children[0].src=fileName.concat('.mp4');
			document.getElementById("video").children[1].src=fileName.concat('.webm');
			$("#listDiv").hide();	
			$("#videoDiv").show();
			$("#videoname").text(name);
			$("a").removeAttr('style');
			$("a").addClass({ 
				"display":"block", 
				"color":"#333", 
				"background":"#e4fad0", 
				"padding":"4px 6px",
				"padding-left":"10px",
				"text-decoration":" none"
			});
			$("#".concat(elId)).css("background", "#ffb84d");
		}

            </script>

</head>
    <body>
        <div class="mainDiv" style="display:block;" id="listDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->  
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                       
                        <td class="contentArea-Blogin"><!--Page Content Start-->   
                         
                            <br><br>
                            <div class="pageHead_1" style="padding-left:0%;">Interactive Audio Visual (Phase I)
                                <span style="float:right;"><a href="eLearning.jsp" class="action-button-goback">Go Back To e-Learning</a></span>
                            </div>
                            <br>
                            <span id="lTable">
                            <table   style="padding-left: 10%;width: 100%;">
                                <tr>
                                    <td align="center" style="padding-left: 28%;">
                                        <div class="gallery" style="opacity: 0.650;">
                                            <a href="#" onclick="">
                                              <img src="/videoBanner.png" alt="PA User Registration" width="300" height="200">
                                              <div class="desc"><b>Bidder Registration Process</div>
                                            </a>
                                          </div>
                                        <div class="gallery" style="padding-bottom: 1.8%;opacity: 0.70;">
                                            <a href="#" onclick="">
                                              <img src="/videoBanner.png" alt="PA User Registration" width="300" height="200">
                                              <div class="desc"><b>Bidding Process</div>
                                            </a>
                                          </div>
                                        <div class="gallery">
                                            <a id="link1" name="Government User Registration Process" href=" #" onclick="loadVideo('video\\PA_Registration', this.id, this.name)">
                                              <img src="/videoBanner.png" alt="Government User Registration Process" width="300" height="200">
                                              <div class="desc"><b>Government User Registration Process</div>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            </span>
                                
                                
                        </td>
                    </tr>
                </table>
                
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
           <div style="background-color: black; display: none;" id="videoDiv">
            <div style="padding-left: 20%; padding-bottom: 2%; padding-right: 20%; padding-top: 2%">
                <div class="pageHead_1" style="padding-left:0%;"><div id="videoname"></div>
                    <span style="float:right;"><a href="videoTutorial.jsp" class="action-button-goback">Go Back To Interactive Audio Visual</a></span>
                </div></br>
               <video id="video" class="video-js" controls preload="auto" autoplay ="false" width="640" height="264"
		  poster="/videoBanner.png" data-setup='{"fluid": true}'>
			<source type='video/mp4'>
			<source type='video/webm'>
			<p class="vjs-no-js">
			  To view this video please enable JavaScript, and consider upgrading to a web browser that
			  <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
			</p>
		</video>
            </div>
        </div>
        
            


    </body>
</html>
