<%-- 
    Document   : RegistrationDetails
    Created on : Mar 21, 2011, 4:38:45 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reports</title>
<script type="text/javascript" src="resources/js/ddlevelsmenu.js"></script>
<link href="resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
</head>
<body>
    <%  
        // Get Report Data
        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
        List<SPCommonSearchDataMore> reportData = commonSearchDataMoreService.getCommonSearchData("GetMISTwo", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
    %>
<div class="mainDiv">
    <div class="fixDiv">
        <!-- include Top Panel -->
         <jsp:include page="resources/common/Top.jsp" ></jsp:include>
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top">
                <td width="266">
                    <!-- include Left Panel -->
                    <jsp:include page="resources/common/Left.jsp" ></jsp:include></td>
                <td class="contentArea-Blogin">
                    <div class="pageHead_1">Reports</div>
            <ul class="tabPanel_1 t_space">
               <li><a href="RegistrationDetails.jsp" class="sMenu">Registration Details</a></li>
               <li><a href="AnnualProcurementPlans.jsp">Annual Procurement Plans </a></li>
               <li><a href="Tenders.jsp">Tenders/Proposals </a></li>
               <li><a href="BidOpeningInfo.jsp">Bid Opening Information</a></li>
                <!-- End-->
            </ul>
                <div class="tabPanelArea_1">
                    <!-- Display Registered Tenderers, consultant and IC -->
                    <div class="tableHead_1">Registered Bidders / Consultants, Individual Consultants</div>
                    <table width="100%" cellspacing="0" class="tableList_3">
                      <tr>
                        <th width="25%"> </th>
                        <th width="25%">National</th>
                        <th width="25%">International</th>
                        <th width="25%">Total</th>
                      </tr>
                      <%
                            //Tenderer / Consultant
                            String strNatTenderer = reportData.get(0).getFieldName1();
                            String strIntTenderer = reportData.get(0).getFieldName2();
                            int strTotTenderer = Integer.parseInt(strNatTenderer) + Integer.parseInt(strIntTenderer);

                            //Individual Consultanat
                            String strNatIC = reportData.get(0).getFieldName3();
                            String strIntIC = reportData.get(0).getFieldName4();
                            int strTotIC = Integer.parseInt(strNatIC) + Integer.parseInt(strIntIC);

                            int intTotNational = Integer.parseInt(strNatTenderer) + Integer.parseInt(strNatIC);
                            int intTotInt = Integer.parseInt(strIntTenderer) + Integer.parseInt(strIntIC);
                      %>
                    <tr>
                      <td class="t-align-left ff">Bidders / Consultants :</td>
                      <td class="t-align-center"><%=strNatTenderer%></td>
                      <td class="t-align-center"><%=strIntTenderer%></td>
                      <td class="t-align-center"><%=strTotTenderer%></td>
                    </tr>
                    <tr>
                      <td class="t-align-left ff">Individual Consultant :</td>
                      <td class="t-align-center"><%=strNatIC%></td>
                      <td class="t-align-center"><%=strIntIC%></td>
                      <td class="t-align-center"><%=strTotIC%></td>
                    </tr>
                    <tr>
                      <td class="t-align-left ff">Total :</td>
                      <td class="t-align-center"><%=intTotNational%></td>
                      <td class="t-align-center"><%=intTotInt%></td>
                      <td class="t-align-center"><%=intTotNational+intTotInt%></td>
                    </tr>
                    </table>
        <!-- Display Registered Ministry, Division and Organization -->
        <div class="tableHead_1 t_space">Registered Ministry, Division, Organization and PE offices </div>
        <%
            // Ministry
            String strMinistryDetails = reportData.get(0).getFieldName5();
            String strMinistryPEOffices = reportData.get(0).getFieldName6();

            // Divisions
            String strDivisionDetails = reportData.get(0).getFieldName7();
            String strDivisionPEOffices = reportData.get(0).getFieldName8();

            // Organizations
            String strOrgDetails = reportData.get(0).getFieldName9();
            String strOrgPEOffices = reportData.get(0).getFieldName10();
          %>
      <table width="100%" cellspacing="0" class="tableList_3">

      <tr>
            <th width="25%">&nbsp;</th>
            <th width="45%">Details</th>
            <th width="30%">PE Offices</th>
          </tr>
        <tr>
          <td class="ff">No. of Ministry :</td>
          <td class="t-align-center"><%=strMinistryDetails%></td>
          <td class="t-align-center">
              <% if("0".equals(strMinistryPEOffices)){ %>
                No PE Offices Avaliable
              <% }else{ %>
                <a href="PEOfficeDetailsPage.jsp?type=Ministry"><%=strMinistryPEOffices%></a>
              <% } %>
              
         </td>
        </tr>
        <tr>
          <td class="ff">No. of Division :</td>
          <td class="t-align-center"><%=strDivisionDetails%></td>
          <td class="t-align-center">
              <% if("0".equals(strDivisionPEOffices)){ %>
                No PA Offices Avaliable
              <% }else{ %>
              <a href="PEOfficeDetailsPage.jsp?type=Division"><%=strDivisionPEOffices%></a></td>
              <% } %>
        </tr>
        <tr>
          <td class="ff">No. of Organization :</td>
          <td class="t-align-center"><%=strOrgDetails%></td>
          <td class="t-align-center">
              <% if("0".equals(strOrgPEOffices)){ %>
                No PE Office Avaliable
              <% }else{ %>
              <a href="PEOfficeDetailsPage.jsp?type=Organization"><%=strOrgPEOffices%></a></td>
              <% } %>
        </tr>
        </table>
        <%
            // Get Schedule Bank information
            List<SPCommonSearchDataMore> schBankInfo = commonSearchDataMoreService.getCommonSearchData("GeteGPBankInfo", "ScheduleBank", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
        %>
       <div class="tableHead_1 t_space">Registered Financial Institute and Branches </div>
      <table width="100%" cellspacing="0" class="tableList_3">

      <tr>
            <th width="80%">Name of the Financial Institute</th>
            <th width="20%">Branch</th>
          </tr>
         <% for(SPCommonSearchDataMore bankInfo:schBankInfo){ %>
         <tr>
             <td class="table-description"><%=bankInfo.getFieldName1()%></td>
             <td class="t-align-center"><% if("0".equals(bankInfo.getFieldName2())){ %>No Branch available<% }else{ %><a href="BranchDetail.jsp?sbankdevelopid=<%=bankInfo.getFieldName3()%>&name=<%=bankInfo.getFieldName1()%>"><%=bankInfo.getFieldName2()%></a><% }%></td>
        </tr>
         <% } %>
        </table>
        
      <div class="tableHead_1 t_space">Registered Development Partners</div>
        <%
             //Get Development Partner information
             List<SPCommonSearchDataMore> devBankInfo = commonSearchDataMoreService.getCommonSearchData("GeteGPBankInfo", "Development", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
        %>
      <table width="100%" cellspacing="0" class="tableList_3">
      <tr>
<!--            <th width="10%">S. No.</th>-->
            <th width="90%">Name of the Development Partner</th>
            <th width="10%">No. of Users</th>
          </tr>
           <%   
                int intSrNo = 1;
                if(devBankInfo.size() > 0){
                for(SPCommonSearchDataMore bankInfo:devBankInfo){
           %>
        <tr>
<!--          <td class="t-align-center"><//%=intSrNo%></td>-->
          <td class="t-align-left"><%=bankInfo.getFieldName1()%></td>
          <td class="t-align-center"><%=bankInfo.getFieldName2()%></td>
        </tr>
        <% intSrNo++; } }else{  %>
        <tr>
            <td class="t-align-center" style="color: red; font-weight: bold;" colspan="2">No Records Found</td>
        </tr>
        <% } %>
        </table>
        </div>
        </td>
    </tr>
 </table>
  <!--Dashboard Footer Start-->
  <!-- include Bottom Panel -->
<jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
  <!--Dashboard Footer End-->
</div>
</div>
</body>
</html>
