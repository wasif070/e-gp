<%--
    Document   : SupportingDocuments
    Created on : Oct 21, 2010, 6:13:00 PM
    Author     : taher
--%>

<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblMandatoryDoc"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.model.table.TblTempCompanyDocuments"%>
<%@page import="com.cptu.egp.eps.web.utility.CheckExtension" %>
<jsp:useBean id="briefcaseDoc" class="com.cptu.egp.eps.web.servicebean.DocumentBriefcaseSrBean" />
<html xmlns="http://www.w3.org/1999/xhtml"> 
    <jsp:useBean id="tempCompanyDocumentsSrBean" class="com.cptu.egp.eps.web.servicebean.TempCompanyDocumentsSrBean"/>    
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Bidder Registration : Supporting Documents</title>
        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="resources/js/jQuery/jquery.validate.js"></script>
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            $(document).ready(function () {

                $('#btnUpload').click(function () {

                    //alert($('#uploadDocFile').val());
                    var errVal = 0;

                    $('#dvUploadFileErMsg').html('');
                    $('#dvDescpErMsg').html('');

                    if ($('#uploadDocFile').val() == "") {
                        //alert('in1');
                        $('#dvUploadFileErMsg').html('Please select Document')
                        errVal = 1;
                    }

                    if ($('#cmbDocType').val() == "other" && $.trim($('#docName').val()) == "") {
                        //alert('in2');
                        $('#dvFileNameErMsg').html('Please enter Description of Document');
                        errVal = 1;
                    }


                    if (errVal == 1)
                        return false;
                });

//            $("#cmbDocType").on('change',function(){
//                alert("I am in");
////                if(this.value === "others"){
////                    
////                }
//                
//                
//            });

//            $("#cmbDocType").change(function () {
//               
//                if($("#cmbDocType").val()==="other"){
//                    
//                    $("#docNameRow").show();
//                }
//            });


                //nishith-11/05/2016
                $("#cmbDocType").change(function () {
                    $(this).find("option:selected").each(function () {
                        if ($(this).attr("value") === "other") {
                            //$(".box").not(".red").hide();
                            $("#docNameRow").show();
                        } else {
                            $("#docName").val("");
                            $('#dvFileNameErMsg').html("");
                            $("#docNameRow").hide();
                        }
                    });
                }).change();



            });

        </script>

        <%--<script type="text/javascript" src="resources/js/upload.js"> </script>
        <script type="text/javascript" src="dwr/interface/UploadMonitor.js"> </script>
        <script type="text/javascript" src="dwr/engine.js"> </script>
        <script type="text/javascript" src="dwr/util.js"> </script>--%>
        <link href="resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <%--<style type="text/css">
            body { font: 11px Lucida Grande, Verdana, Arial, Helvetica, sans serif; }
            #progressBar { padding-top: 5px; }
            #progressBarBox { width: 350px; height: 20px; border: 1px inset; background: #eee;}
            #progressBarBoxContent { width: 0; height: 20px; border-right: 1px solid #444; background: #9ACB34; }
        </style>--%>
        <script type="text/javascript">
            <%--$(function() {
               $('#btnFinal').click(function() {
                   $.post("<%=request.getContextPath()%>/CommonServlet", {funName:'addUser'},  function(j){
                       if(j.toString()=="User created."){
                           jAlert("Your Profile is successfully submitted to the administrator for review. You will be notified for approval / rejection status of your profile through your registered mail id","Successful Registered", function(RetVal) {
                           });
                           window..location..href="<%=request.getContextPath()%>/UserStatus.jsp?status=p";
                       }else{
                           jAlert("User not Created.","User creation alert", function(RetVal) {
                           });
                       }

                    });
                });
            });--%>
            $(function () {
                $('#cmbDocType').change(function () {
                    var cmb = document.getElementById('cmbDocType');
                    $('#hdndocBrief').val(cmb.options[cmb.selectedIndex].text);
                });
            });
            <%--Redirecting to Authenticate.jsp--%>
            <%--$(function() {
                $('#btnNext').click(function() {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {pageName:'Authenticate',funName:'updateNextScr'},  function(j){
                         window..location..href="<%=request.getContextPath()%>/Authenticate.jsp"
                    });
                });
            });--%>
            $(function () {
                $('#frmUploadDoc').submit(function () {
                    if ($('#frmUploadDoc').valid()) {
                        $('.err').remove();
                        var count = 0;
                        var browserName = ""
                        var maxSize = parseInt($('#fileSize').val()) * 1024 * 1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function (i, val) {
                            browserName += i;
                        });
                        $(":input[type='file']").each(function () {
                            if (browserName.indexOf("mozilla", 0) != -1) {
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            } else {
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if (parseInt(actSize) == 0) {
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if (fileName.indexOf("&", "0") != -1 || fileName.indexOf("%", "0") != -1) {
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if (parseInt(parseInt(maxSize) - parseInt(actSize)) < 0) {
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed " + $('#fileSize').val() + " MB. </div>");
                                count++;
                            }
                        });
                        if (count == 0) {
                            $('#btnUpload').attr("disabled", "disabled");
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body>
        <%
            tempCompanyDocumentsSrBean.setLogUserId(session.getAttribute("userId").toString());
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                    <!--Middle Content Table Start-->                
                    <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0" >
                        <tr valign="top">

                            <td class="contentArea-Blogin">
                                <!--Page Content Start-->
                                <div class="pageHead_1">Bidder Registration - Supporting Documents</div>
                            <jsp:include page="EditNavigation.jsp" ></jsp:include>
                            <%
                                if (request.getParameter("fq") != null) {
                            %>
                            <br/><div class="responseMsg errorMsg"><%=request.getParameter("fq")%></div>
                            <%
                                }
                                if (request.getParameter("fs") != null) {
                            %>
                            <br/><div class="responseMsg errorMsg">
                                Max file size of a single file must not exceed <%=request.getParameter("fs")%> MB.
                            </div>
                            <%
                                }
                                if (request.getParameter("ft") != null) {
                            %>
                            <br/><div class="responseMsg errorMsg">
                                Acceptable file types <%=request.getParameter("ft")%>.
                            </div>                                            
                            <%
                                }
                                if (request.getParameter("sf") != null) {
                            %>
                            <br/><div class="responseMsg errorMsg">
                                File already exist
                            </div>
                            <%
                                }
                                if ("fail".equals(request.getParameter("msg"))) {
                            %>
                            <br/><div class="responseMsg errorMsg">
                                Problem in uploading Document
                            </div>
                            <%
                                }
                                int btnShow = 0;
                                String msg = "";
                                StringBuffer sb = new StringBuffer();
                                int tendererId = Integer.parseInt(tempCompanyDocumentsSrBean.checkJVCA(session.getAttribute("userId").toString()));
                                java.util.List<TblTempCompanyDocuments> documentsList = new java.util.ArrayList<TblTempCompanyDocuments>();
                                documentsList = tempCompanyDocumentsSrBean.getTblTempCompanyDocumentses(tendererId);
                                int cnt = documentsList.size();
                                boolean FileExists = true;
                                String FileNotExistsString = "";
                                for (TblTempCompanyDocuments documents : documentsList) {
                                    if (!briefcaseDoc.isRegisDocumentExistsOrNot(documents.getDocumentName(), session.getAttribute("userId").toString())) {
                                        FileNotExistsString = FileNotExistsString + " " + documents.getDocumentName() + ",";
                                        if (FileExists) {
                                            FileExists = false;
                                        }
                                    }
                                }
                                UserRegisterService service = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                                List<String> pageList = service.pageNavigationList(Integer.parseInt(request.getSession().getAttribute("userId").toString()));//
                                boolean showListofDoc = false;
                                if (pageList.size() == 3) {
                                    for (String pages : pageList) {
                                        if (pages.trim().startsWith("I")) {
                                            showListofDoc = true;
                                        }
                                    }
                                }
                                List<TblMandatoryDoc> list = service.getMandatoryDocs(session.getAttribute("userId").toString(), true, false);
                                int cnt1 = 0;
                                int totalCnt = 0;
                                int mandDocs = 0;
                                //TblMandatoryDoc doc1 = new TblMandatoryDoc();
                                System.out.println(list.get(0).getIsMandatory().toString());
                                for (TblMandatoryDoc doc : list) {
                                    totalCnt++;
                                    boolean bool = true;
                                    if (doc.getIsMandatory().equals("yes")) {
                                        cnt1++;
                                    }
                                    for (TblTempCompanyDocuments documents : documentsList) {
                                        if (documents.getDocumentType().equals(doc.getDocTypeValue())) {
                                            bool = false;
                                        }
                                        if (documents.getDocumentType().equals(doc.getDocTypeValue()) && doc.getIsMandatory().equals("yes")) {
                                            mandDocs++;
                                        }
                                    }
                                    if (bool) {
                                        sb.append("<option value='" + doc.getDocTypeValue() + "'>" + doc.getDocType() + "</option>");
                                        btnShow++;
                                    }
                                }
                                /*out.println("Mandatory : "+cnt1);
                            out.println("Uploaded : "+cnt);
                            out.println("Total : "+totalCnt);*/
                                CheckExtension ext = new CheckExtension();
                                TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("tenderer");
                                byte fileSize = configurationMaster.getFileSize();
                            %>









                            <%if (btnShow != 0) {%>
                            <form  id="frmUploadDoc" method="post" action="FileUploadServlet" enctype="multipart/form-data" name="frmUploadDoc"><%-- onsubmit="startProgress()"--%>
                                <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1 c_t_space" width="100%">
                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="" align="right">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>

                                    <% if (!showListofDoc) {%>
                                    <tr>
                                        <td colspan="2">
                                            <div>
                                                <!--                                                <table border="1" cellspacing="0" cellpadding="0" class="formStyle_1 c_t_space" width="100%">
                                                                                                    <th>
                                                                                                        <td>Mandatory Documents</td>
                                                                                                        <td>Optional Documents</td>
                                                                                                    </th>
                                                                                                    <tr>
                                                                                                        <td></td>
                                                                                                        <td></td>
                                                                                                    </tr>
                                                                                                </table>-->

                                                <h2><span style="color:#DAA520;">List of Documents</span></h2>
                                                <hr/>

                                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                    <tr>

                                                        <th class="t-align-center" width="50%">Mandatory Documents</th>
                                                        <th class="t-align-center" width="50%">Optional Documents</th>

                                                    </tr>

                                                    <tr class="bgColor-white">

                                                        
                                                        <td class="t-align-left"><b>Self Declaration </b><span style="color:red">*</span><i> (For Company Owner)</i></td>
                                                        <td class="t-align-left"><b>Company Registration No</b></td>

                                                    </tr>
                                                    <tr class="bgColor-white">

                                                        <td class="t-align-left"><b>Citizenship Identity Card</b> <span style="color:red">*</span><i> (For Company Contact Person)</i></td>
                                                        <td class="t-align-left"><b>Tax Payment No. (TPN)</b></td>

                                                    </tr>
                                                    <tr class="bgColor-white">

                                                        <td class="t-align-left"><b>Power Of Attorney</b><span style="color:red">*</span><i> (For Company's Authorized User)</i></td>
                                                        <td class="t-align-left"><b>Statutory Certificate No</b></td>

                                                    </tr>
                                                     <tr class="bgColor-white">

                                                        <td class="t-align-left"><b>Trade License</b><span style="color:red">*</span><i> (For Goods, Works and Service)</i></td>
                                                        <td class="t-align-left"><b>Others/More</b></td>

                                                    </tr>
                                                    <tr class="bgColor-white">

                                                        <td class="t-align-left"><b>Construction Development Board (CDB) Registration Certificate</b><span style="color:red">*</span><i> (For Works Only)</i></td>
                                                        <td class="t-align-left"><b></b></td>

                                                    </tr>





                                                </table>



                                            </div>

                                        </td>
                                    </tr>
                                    <%}%>
                                    <tr>
                                        <td colspan="2" height="5"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="5"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="5"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="5"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="16%">Document Type : <span>*</span></td>
                                        <td width="84%">
                                            <select name="documentType" class="formTxtBox_1" id="cmbDocType">                                                
                                                <%=sb.toString()%>
                                            </select> <br/>All documents listed above are mandatory except ones labeled as "(If applicable)". "If applicable" indicates optional document.<!--Document listed in combo are mandatory, except ones labeled (if applicable).-->                                            
                                            <input type="hidden" value="<%=tendererId%>" name="tendererId" id="hdnTendererId"/>                                            
                                            <input type="hidden" name="documentBrief" id="hdndocBrief"/>
                                            <input type="hidden" name="funName" value="supportdoc" id="hdnFunction"/>
                                            <script type="text/javascript">
                                                var cmb = document.getElementById('cmbDocType');
                                                $('#hdndocBrief').val(cmb.options[cmb.selectedIndex].text);
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="5"></td>
                                    </tr>

                                    <tr id="docNameRow" style="display:none;">
                                        <td class="ff" width="16%">Document Name : <span>*</span></td>
                                        <td width="84%">
                                            <input type="text" name="docName" id="docName" class="formTxtBox_1" />
                                            <div id="dvFileNameErMsg" class='reqF_1'></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="5"></td>
                                    </tr>

                                    <tr>
                                        <td class="ff" width="16%">Document Ref No :</td>
                                        <td width="84%">
                                            <input type="text" name="docRefNo" id="docRefNo" class="formTxtBox_1" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="5"></td>
                                    </tr>




                                    <tr>
                                        <td class="ff" width="11%"> Select Document   : <span>*</span></td>
                                        <td width="89%"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:450px; background:none;"/><br/>
                                            Acceptable File Types <span class="formNoteTxt">(<%out.print(configurationMaster.getAllowedExtension().replace(",", ", "));%>)</span>
                                            <br/>Maximum file size of single file should not exceed <%out.print(fileSize);%>MB.
                                            <input type="hidden" value="<%=fileSize%>" id="fileSize"/>
                                            <div id="dvUploadFileErMsg" class='reqF_1'></div>
                                            <%--<br/>
                                            <div id="progressBar" style="display: none;">

                                                <div id="theMeter">
                                                    <div id="progressBarText"></div>

                                                    <div id="progressBarBox">
                                                        <div id="progressBarBoxContent"></div>
                                                    </div>
                                                </div>
                                            </div>--%>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="5"></td>
                                    </tr>
                                    <!--tr id="desc" style="display: none;">
                        <td class="ff">Description : <span>*</span></td>
                        <td>
                            <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="txtDescription" style="width:200px;"/>
                            <div id="dvDescpErMsg" class='reqF_1'></div>
                        </td>
                    </tr-->
                                    <tr>
                                        <td colspan="2" height="5"></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <%
                                                //java.util.List<TblTempCompanyDocuments> documentsList = new java.util.ArrayList<TblTempCompanyDocuments>();

                                                //Below one is not working for redirection
                                                if ("Next".equals(request.getParameter("btnNext"))) {
                                                    if (documentsList.isEmpty()) {
                                                        msg = "Please upload atleast one document.";
                                                        response.sendRedirect("SupportingDocuments.jsp");
                                                    } else {
                                                        response.sendRedirect("Authenticate.jsp");
                                                    }
                                                }
                                                //Ends
                                            %>                                            
                                            <label class="formBtn_1" style="visibility:visible;" id="lbUpload">
                                                <input type="submit" name="upload" id="btnUpload" value="Upload"/>
                                            </label>                                            
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <%}%>
                            <%
                                if (!msg.equals("")) {
                            %>
                            <div class="responseMsg errorMsg"><%=msg%></div>
                            <%
                                }
                            %>
                            <div class="pageHead_1"> Uploaded Documents</div>
                            <div class="t_space">
                                <b><font color="Black">Total Documents Uploaded:</font> <font color="Green"><%= cnt%></font></b><br/><br/>
                                <b>Mandatory Document Upload Status: <font color="Black">Total: <%= cnt1%> </font> | <font color="Green">Uploaded: <%= mandDocs%></font> | <font color="Red">Pending: <%=cnt1 - mandDocs%></font></b>
                            </div><br/>
                            <div>
                                <font color="red">Submit button for final submission will appear automatically once all the mandatory documents are uploaded.</font>
                            </div>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th class="t-align-center" width="4%">Sl. No.</th>
                                    <th class="t-align-center" width="30%">Document Name</th>
                                    <th class="t-align-center" width="41%">Document Description</th>
                                    <th class="t-align-center" width="10%">File Size<br/> (In KB)</th>
                                    <th class="t-align-center" width="15%">Action</th>
                                </tr>
                                <%
                                    int count = 1;
                                    for (TblTempCompanyDocuments ttcd : documentsList) {
                                %>
                                <tr class="<%if (count % 2 == 0) {
                                        out.print("bgColor-Green");
                                    } else {
                                        out.print("bgColor-white");
                                    }%>">
                                    <td class="t-align-center"><%=count%></td>
                                    <td class="t-align-left"><%=ttcd.getDocumentName()%></td>
                                    <td class="t-align-left"><%for (TblMandatoryDoc doc : list) {
                                            if (doc.getDocTypeValue().equals(ttcd.getDocumentType())) {
                                                out.print(doc.getDocType());
                                            }
                                        }%></td>
                                    <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");
                                        out.print(twoDForm.format(Double.parseDouble((ttcd.getDocumentSize())) / 1024));%></td>
                                    <td class="t-align-center">
                                        <a href="SupportDocServlet?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocumentSize()%>&funName=download" title="Download"><img src="resources/images/Dashboard/Download.png" alt="Download" /></a>                                        
                                        <a href="SupportDocServlet?docId=<%=ttcd.getCompanyDocId()%>&docName=<%=ttcd.getDocumentName()%>&funName=remove" title="Remove"><img src="resources/images/Dashboard/delIcn.png" alt="Remove" width="16" height="16" /></a>
                                    </td>
                                </tr>
                                <%count++;
                                    }%>
                            </table>                            
                            <%-- Hide 
                           <table border="0" cellspacing="10" cellpadding="0" width="100%" class="formStyle_1">
                               <tr>
                                   <td>
                                       <div>                                            
                                           <b style="color: red;">Important Note :</b>                                                

                                            <br/>
                                            <table cellspacing="10" cellpadding="0" width="100%">
                                                <tr>
                                                    <td colspan="2">Before starting your Profile and document submission, please read the following information and make sure the required <span style="color: red;">authentic</span> MANDATORY documents <span style="color: red;">are</span> ready:</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">1.</td>
                                                    <td align="left">
                                                        Scan those mandatory documents you have been asked for and convert into <span style="background-color: yellow;">PDF or Zip file format</span>. Size of a single file must not exceed <b><%=fileSize%>MB</b>. Scan the files in such a way that it can be easily readable. And also ensure that the documents are virus free. GPPMD as a system provider does not take any responsibility of any consequences to your future transactions in case ineligible scanned document, file containing virus or misrepresentation of the information in the uploaded documents.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2.</td>
                                                    <td>
                                                       Upload the following mandatory documents for the registration. Failure to upload any one of the documents, e-GP system does not allow you to further proceed.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3.</td>
                                                    <td>
                                                        <span style="color: red;">If the uploaded documents are found forged or false, your case will be treated under section 64 (Professional misconduct) of the Public Procurement Act 2006.</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4.</td>
                                                    <td>
                                                        In case of physical submission of mandatory document, advice for physical submission of <span style="color: red;">attested (by the government class-1 official) copies of scanned and already uploaded mandatory documents through Courier or Registered mail.</span>
                                                    </td>
                                                </tr>                                                
                                            </table>                                            
<!--                                            After submission of these documents online, attested copy of documents should be submitted to GPPMD User Registration Desk for the verification of authenticity, either by visiting physically or sending the documents through registered post, or courier with required return ticket stamped or draft cheque for returning through courier. After final submission and verification by GPPMD, if you need to change these documents, you may need to go through the verification process by GPPMD again. Need to correct the highlighted things in the Document Upload Page.-->
                                        </div>
                                    </td>
                                </tr>
                            </table>--%>
                            <%
                                if (FileExists) {
                                    if (cnt >= cnt1) {
                                        if (service.checkDocCount(session.getAttribute("userId").toString(), true) == 0) {
                                            //    if(true){
                            %>
                            <table border="0" cellspacing="10" cellpadding="0" width="100%" class="t_space">
                                <tr>
                                    <td align="center">
                                        <a href="FinalSubmission.jsp"  class="anchorLink">Click here to proceed for final profile submission</a>
                                    </td>
                                </tr>
                            </table>
                            <%}
                                }%>
                            <%} else {
                                FileNotExistsString = FileNotExistsString.substring(0, FileNotExistsString.length() - 1);
                            %>
                            <table border="0" cellspacing="10" cellpadding="0" width="100%" class="t_space">
                                <tr>
                                    <td align="center">
                                        <span style="color: red; font-size: large"><b>Problem occurred during file upload.</b></span> </br> <b>Try to remove and again upload these files: </b> </br> <b><%=FileNotExistsString%></b>
                                    </td>
                                </tr>
                            </table>
                            <%
                                }
                            %>
                            <!--Page Content End-->
                        </td>
                        <!--                        <td width="266" class="td-background">
                        <%--<jsp:include page="resources/common/Left.jsp" ></jsp:include>--%>
                        </td>-->
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
