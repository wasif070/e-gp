<%-- 
    Document   : GetOTP
    Created on : Mar 23, 2017, 12:29:51 PM
    Author     : MD. Emtazul Haque
--%>

<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.dao.daointerface.HibernateQueryDao"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="java.util.List"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Get OTP Interface</title>
        <link REL="SHORTCUT ICON" HREF="/resources/favicon2.ico">
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script type="text/javascript">
            $(function() {
                $('#GetOTP').submit(function() {
                    if($.trim($('#tenderId').val())==""){
                        alert("Please Enter TenderId.");
                        return false;
                    }else{
                        if(isNaN($('#tenderId').val())){
                            alert("Please Enter Numerics in TenderId.");
                            return false;
                        }
                    }
                    if($.trim($('#BidderEmail').val())==""){
                        alert("Please Enter Bidder Email.");
                        return false;
                    }
                   
                    return confirm("Are You sure you want to know the OTP");
                    
                    
                    
                });
            });
        </script>
    </head>
    <%
                if ((request.getSession().getAttribute("userTypeId")!=null && !request.getSession().getAttribute("userTypeId").equals("2")) && true || request.getRemoteAddr().equals("192.168.100.152") || request.getRemoteAddr().equals("10.1.4.21")) {
                    String tenderId = request.getParameter("tenderId");
                    String BidderEmail = request.getParameter("BidderEmail");
                    String msgId = request.getParameter("msgId");
                    String tId = request.getParameter("tId");
                    String OTP = request.getParameter("OTP");
                    if ("Submit".equals(request.getParameter("submit")) && tenderId != null && BidderEmail != null) {
                        
                        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                        List<SPTenderCommonData> GetOTP = tenderCommonService.returndata("GetOTPByBidderEmail", tenderId, BidderEmail);
                        msgId = "0";
                        tId = tenderId;
                        
                        if(GetOTP.size() > 0 && GetOTP!=null)
                        {
                            msgId = "1";
                            OTP = GetOTP.get(0).getFieldName1();
                        }
                        
                       
                        response.sendRedirect(request.getContextPath() + "/GetOTP.jsp?msgId=" + msgId + "&tId=" + tId + "&OTP=" + OTP);
                    }
    %>
    <body>
        <form action="GetOTP.jsp" method="post" id="GetOTP">
            <div align="center">
                <%
                    if (tId != null && msgId != null && !msgId.equals("0")) {
                        out.print("<div class='responseMsg successMsg t_space'>");
                        out.print(" OTP for TenderID : " + tId + " is " + OTP + "</div>");
                    }
                    if (msgId != null && msgId.equals("0")) {
                        out.print("<div class='responseMsg errorMsg t_space'>Your provided values are incorrect.</div>");
                    }
                %>
                <table border="1" cellspacing="10" cellpadding="0" class="tableList_1 t_space" width="50%">
                    <tr>
                        <th>Tender ID</th>
                        <td>
                            <input type="text" class="formTxtBox_1"  style="width: 200px;" id="tenderId" name="tenderId"/>
                        </td>
                    </tr>
                    <tr>
                        <th>Bidder Email</th>
                        <td>
                            <input type="text" class="formTxtBox_1"  style="width: 200px;" id="BidderEmail" name="BidderEmail"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="t-align-center">
                            <input type="submit" value="Submit" name="submit" class="formTxtBox_1"/>
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </body>
    <%
                } else {
                    response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
                }
    %>
</html>
