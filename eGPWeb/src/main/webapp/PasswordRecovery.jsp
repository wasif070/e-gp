<%-- 
    Document   : PasswordRecovery
    Created on : Apr 10, 2018, 1:28:14 PM
    Author     : Aprojit
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Password Recovery</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="resources/js/jQuery/jquery.validate.js"></script>

        <link href="resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#clickHere").attr('disabled', false);
                $("#frmForgotPass").validate({
                    rules: {
                        emailId:{required: true,email:true},
                        hintAns:{required:true,maxlength:50}
                    },
                    messages: {
                        emailId:{ required: "<div class='reqF_1'>Please Enter e-mail ID </div>",
                            email:"<div class='reqF_1'>Please Enter Valid e-mail ID</div>"},

                        hintAns:{required:"<div class='reqF_1'>Please enter Hint Answer.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 50 characters allowed.</div>"}
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "emailId")
                            error.insertAfter("#btnAdd");
                        else
                            error.insertAfter(element);
                    }
                     
                }
            );
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#clickHere').click(function() {
                    var spaceTest = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;
                    if($.trim($('#txtforgetEmailId').val()).length !=0) {
                        if(spaceTest.test($('#txtforgetEmailId').val())){
                        $('span.#mailMsg').html("Checking for e-mail ID...");
                        $.post("<%=request.getContextPath()%>/ForgotPasswordSrBean", {forgetemailId:$('#txtforgetEmailId').val(),funName:'chkMailId',action: 'forgot'},  function(j){
                            if(j.toString().indexOf("E:")!= -1 || j.toString().length ==0)
                            {
                                $('span.#mailMsg').html("e-mail ID not found in e-GP system. Please enter registered e-mail ID");
                                $('#hint').hide();
                            }
                            else if(j.toString().indexOf("O:")!= -1 || j.toString().length ==0)
                            {
                                $('span.#mailMsg').html(' ');
                                $('#resetPwd').show();
                                $('#btnAdd1lab').hide();
                                $('#txtforgetEmailId').attr("readonly", true);
                                
                            }
                            else
                            {
                                 $('span.#mailMsg').html(' ');
                                $('span.#sucMsg').html(' ');
                                $('#hint').show();
                                $('#txtforgetEmailId').attr("readonly", true);
                                $('#btnAdd1lab').hide();
                                $("#txtHintQuestion").val(j);
                            }
                        });
                         }else{
                         $('span.#mailMsg').html("Please enter Valid e-mail ID");
                        $('#hint').css("display", "none");
                    }
                    }
                    else
                    {
                        $('span.#mailMsg').html("Please enter e-mail ID");
                        $('#hint').css("display", "none");
                    }
                });
            });

            $(function() {
                $('#frmForgotPass').submit(function() {
                    if($('span.#mailMsg').html().length == 1)
                        return true;
                    else
                        return false;
                });
            });
        </script>
       <script type="text/javascript">
            $(function() {
                $('#btnSubmit').click(function() {
                    jQuery.ajax({
                        url:    '<%=request.getContextPath()%>/ResetPasswordSrBean'
                            + '?forgetemailId='+ $('#txtforgetEmailId').val()
                            + '&funName=' + 'chkMailId'
                            + '&action=' +'reset',
                        type: 'POST',
                        success: function(result) {
                            if(result.toString().indexOf("B",0)!= -1){
                                $('span.#mailMsg').css("color", "red");
                                $('span.#mailMsg').html("Please enter e-mail ID");
                            }else{
                            if(result.toString().indexOf("E:",0) != -1)
                            {

                                $('span.#mailMsg').css("color", "red");
                                $('span.#mailMsg').html("e-mail ID does not exist");

                            }
                            else
                            {

                                $('span.#sucMsg').css("color", "green");
                                 //$('#btnSubmit').hide();
                                 $('#lblSubmitBtn').hide();
                                $('span.#sucMsg').html(result);

                            }
                            }
                        },
                        async:   false
                    });
                    if($('span.#mailMsg').html().indexOf("NOT", 0) == -1)
                        return true;
                    else
                        return false;
                });
            });
        </script>
        <script type="text/javascript">
           $(function() {
           $('#resetBtn').click(function() {
            jQuery.ajax({
            url:    '<%=request.getContextPath()%>/LoginSrBean'
                + '?emailId='+ $j('#txtforgetEmailId').val()
                + '&funName=' + 'resendCode',
            type: 'POST',
            success: function(result) {
                window.location.href = <%=request.getContextPath()%>'/VerifyCode.jsp?msg=verify'
            }
            });
         });
       });
          /*  $(function() {
                $('#resetBtn').click(function() {
                    jQuery.ajax({
                        url:    '<%=request.getContextPath()%>/ResetPasswordSrBean'
                            + '?forgetemailId='+ $('#txtforgetEmailId').val()
                            + '&funName=' + 'chkMailId'
                            + '&action=' +'reset',
                        type: 'POST',
                        success: function(result) {
                            if(result.toString().indexOf("B",0)!= -1){
                                $('span.#mailMsg').css("color", "red");
                                $('span.#mailMsg').html("Please enter e-mail ID");
                            }else{
                            if(result.toString().indexOf("E:",0) != -1)
                            {

                                $('span.#mailMsg').css("color", "red");
                                $('span.#mailMsg').html("e-mail ID does not exist");

                            }
                            else
                            {

                                $('span.#sucMsg1').css("color", "green");
                                 //$('#btnSubmit').hide();
                                 $('#lblSubmitBtn1').hide();
                                $('span.#sucMsg1').html(result);

                            }
                            }
                        },
                        async:   false
                    });
                    if($('span.#mailMsg').html().indexOf("NOT", 0) == -1)
                        return true;
                    else
                        return false;
                });
            });*/
        </script>
        <script type="text/javascript">
         $(function() {
           $('#aSendCode').click(function() {
            jQuery.ajax({
            url:    '<%=request.getContextPath()%>/LoginSrBean'
                + '?emailId='+ $j('#txtforgetEmailId').val()
                + '&funName=' + 'resendCode',
            type: 'POST',
            success: function(result) {
                    if(result=='ok')
                       window.location.href = <%=request.getContextPath()%>'/VerifyCode.jsp?msg=verify'
                    //else if(result=='over')
                    //     $j('span.#smsMsg').html("Maximum limit of requesting verification code has been exceeded.");
                    else
                        $j('span.#smsMsg').html("Error Occured.");
            }
            });
         });
       });
   </script>
    </head>
    <body>
        <div class="mainDiv">
            <jsp:include page="/resources/common/Top.jsp"/>
        
                            
    
    
            <div class="beforeLoginDiv">

                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                        <td>
                            <!--Page Content Start-->
                           

                            <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                   
                                    <td class="contentArea" style="min-height: 350px;">
                                    
                                        <div class="pageHead_1">Forgot Password &ndash; Reset Password</div>
                                        <%
                                                    String msg = request.getParameter("msg");
                                                    if (msg != null && msg.equals("fail")) {
                                        %>
                                        <div class="responseMsg errorMsg t_space" style="margin-top: 10px;">Hint Question Verification Failed</div>

                                        <%}%>

                                        <table width="100%" cellpadding="0" cellspacing="10" class=" t_space" >
                                          <tr>
                                              <td>
                                                  <div class="atxt_1 c_t_space" style="font-size: larger">
                                                        Dear User, <br/><br/>
                                                        As Electronic Government Procurement (e-GP) System does not store your password in Database, System cannot retrieve your password.  Hence if you have forgotten your password you have to reset your password as per following process.<br/><br/>
                                                        Please enter your e-mail ID which is registered with Electronic Government Procurement (e-GP) System in the text box given below. On click of <b>Click Here</b> button System will verify your e-mail ID. On successful verification, Electronic Government Procurement (e-GP) System will send an email to your registered e-mail ID with a verification code.  When you receive email please follow instructions given in email. <br/><br/>
                                                        
                                                        Thank You, <br/> <br/>
                                                        Government Procurement and Property Management Division (GPPMD) <br/>
                                                        Ministry of Finance <br/>
                                                        Box 116 <br/>
                                                        Thimphu, Bhutan <br/>
                                                        Tel No. : 02 - 336962 <br/>
                                                        Fax No.: 02 - 336961 <br/>


                                                    </div>
                                              </td>
                                          </tr>
                                        </table>

                                        <form id="frmForgotPass" action="<%=request.getContextPath()%>/ForgotPasswordSrBean" name="frmForgotPass" method="post">
                                         <%if("y".equals(request.getParameter("sucfpmsg"))){out.print("<div class=\"t_space\"><div class=\"responseMsg successMsg t_space\">An e-mail has been sent to your mail id for verification. Please verify the link (url) in the mail to change your password</div></div>");}else{%>
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1 t_space">
                                                <tr>
                                                    <td width="8%" class="ff">e-mail ID : <span>*</span></td>
                                                    <td  width="82%">
                                                        <input class="formTxtBox_1" type="text" id="txtforgetEmailId" name="forgetemailId" style="width: 200px"/>
                                                        <label id="btnAdd1lab" class="formBtn_1">
                                                            <input type="button" name="Add" id="clickHere" value="Click Here"  disabled/>
                                                        </label><br />
                                                        <span id="mailMsg" style="color: red;">&nbsp;</span>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="resetPwd" style="display: none;" width="100%" cellpadding="0" cellspacing="10">
                                                <tr>
                                                    <td class="mandatory "> Hint Question & Answer is not available in the system. Please check the
                                                        user registration confirmation mail for detail.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    
                                                    <td align="left">Alternatively, you may Reset Password. <label id="lblSubmitBtn1" class="formBtn_1">
                                                            <input type="button" name="submit" id="resetBtn" value="Reset Password" />
                                                        </label>
                                                    </td>
                                                </tr>
                                                <tr><td><span id="sucMsg1" style="color: red;font-weight: bold;">&nbsp;</span></td></tr>
                                                
                                            </table>
                                            <table id="hint" width="100%" style="display: none" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td  width="20%" class="ff">Hint Question : <span>*</span></td>
                                                    <td  width="80%" colspan="2">
                                                        <input class="formTxtBox_1" style="width: 200px" readonly type="text" id="txtHintQuestion" name="hintQuestion" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%" class="ff">Hint Answer : <span>*</span></td>
                                                    <td width="20%"><input class="formTxtBox_1" style="width: 200px" type="text" id="txtHintAns" name="hintAns" />
                                                    </td>
                                                    <td width="60%" class="ff">
                                                        If you forget Hint Answer then
                                                        <!--<label class="formBtn_1" id="lblResend">-->
                                                            <a href='#' id="aSendCode">Click here</a>
                                                            <!--<input type="button" name="button" id="btnResend" value="Send Code"/>-->
                                                        <!--</label>-->
                                                    </td>
                                               </tr>
                                               <tr>
                                                   <td colspan="2"><span id="smsMsg" style="color: red;font-weight: bold;"></span></td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td align="left" colspan="2">
                                                        <label class="formBtn_1">
                                                            <input type="hidden" name="funName" value="verifyHint"/>
                                                            <input type="submit" name="Add" id="hintAns" value="Submit" />
                                                        </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <div class="atxt_1 c_t_space">
                                                            <%-- .bd to .bt--%>
                                                            If you have forgotten your hint answer then please contact e-GP Helpdesk at <a href="helpdesk@pppd.gov.bt">helpdesk@pppd.gov.bt</a> for further assistance.
                                                        </div>
                                                    </td>
                                                </tr>
                                                <%--<tr>
                                                    <td>&nbsp;</td>
                                                    <td class="ff"> Forgot your hint answer? No problem, click here to reset your password. </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td><label id="lblSubmitBtn" class="formBtn_1">
                                                            <input type="button" name="submit" id="btnSubmit" value="Reset Password" />
                                                        </label>
                                                    <span id="sucMsg" style="color: red;font-weight: bold;">&nbsp;</span></td>
                                                </tr>--%>

                                            </table>
                                            <%}%>
                                        </form>
                                    </td>
                                        
                                         <% if (session.getAttribute("userId") == null) { %>
                                    <td width="266">
                                        <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                                    </td>
                                            <%}%>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->

            </div>
            <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
</html>

