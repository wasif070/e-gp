<%-- 
    Document   : eLearning
    Created on : May 25, 2017, 1:34:03 PM
    Author     : Nitish Oritro
--%>

<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-Learning</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        
    </head>
    <body>
        
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->                
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        
                        <td class="contentArea-Blogin"><!--Page Content Start-->
                        
                        <div align="center" style="margin-bottom: 30px;">
                            <div style="width:50%; padding:20px;" >                        
                                <h1> WELCOME </h1><br/>
<b style="line-height:20px; font-size: 14px;"> Electronic Government Procurement (e-GP) System<br/>
                                    e-Learning (Online Training Module)<br/>
                                </b>                            </div>
                        </div>
                            
                        <div class="eLearnBox">       
                            <div>
                                <a href="videoTutorial.jsp"><img src="<%=request.getContextPath()%>/resources/images/Dashboard/video.png"></img></a>
                                    <a href="videoTutorial.jsp">Interactive Audio Visual</a>
                            </div>
                         
                            <!--div>
                                <a href="quizModule.jsp"><img src="<%=request.getContextPath()%>/resources/images/Dashboard/Quiz.png"></a>
                                    <a href="quizModule.jsp">e-GP Quiz</a>
                            </div-->
                                
                            <div>
                                <a href="eGpGuidelines.jsp"><img src="<%=request.getContextPath()%>/resources/images/Dashboard/guideline.png"></a>
                                    <a href="eGpGuidelines.jsp">e-GP Guidelines</a>
                            </div>
                                    
                             <div>
                                <a href="userManuals.jsp"><img src="<%=request.getContextPath()%>/resources/images/Dashboard/userManual.gif"></a>
                                    <a href="userManuals.jsp">User Manuals</a>
                            </div>
                                
                            <div>
                                <a href="<%= request.getContextPath()%>/FAQ.jsp"><img src="<%=request.getContextPath()%>/resources/images/Dashboard/faq.png"></a>
                                    <a href="<%= request.getContextPath()%>/FAQ.jsp">FAQ</a>
                            </div>

                        </div>
                            
                        </td>
                            <%--<td width="266">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include></td>--%>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
