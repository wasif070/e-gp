<%--
Document   : Reset Password
Created on : Oct 23, 2010, 6:13:00 PM
Author     : Sanjay
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Reset Password</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
</head>
<body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
        <script src="resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            $j(document).ready(function() {
                $j("#btnSubmit").attr('disabled', false);
                $j("#frmReset").validate({
                    rules: {
                        emailId:{required: true,email:true}
                    },
                    messages: {
                        emailId:{ required: "",
                            email:""}
                    }
                }
            );
            });
        </script>
        <script type="text/javascript">
            $j(function() {
                $j('#btnSubmit').click(function() {
                    jQuery.ajax({
                        url:    '<%=request.getContextPath()%>/ResetPasswordSrBean'
                            + '?forgetemailId='+ $j('#txtreEmailId').val()
                            + '&funName=' + 'chkMailId'
                            + '&action=' +'reset',
                        type: 'POST',
                        success: function(result) {
                            if(result.toString().indexOf("B",0)!= -1){
                                $j('span.#mailMsg').css("color", "red");
                                $j('span.#mailMsg').html("Please Enter Mail-Id");
                            }else{
                            if(result.toString().indexOf("E:",0) != -1)
                            {
                                
                                $j('span.#mailMsg').css("color", "red");
                                $j('span.#mailMsg').html("Mail-ID Does NOT Exist");
                                
                            }
                            else
                            {

                                $j('span.#mailMsg').css("color", "green");
                                 $j('#btnTr').hide();
                                $j('span.#mailMsg').html(result);
                               
                            }
                            }
                        },
                        async:   false
                    });
                    if($j('span.#mailMsg').html().indexOf("NOT", 0) == -1)
                        return true;
                    else
                        return false;
                });
            });
        </script>

                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                       <td width="266"><jsp:include page="resources/common/Left.jsp" ></jsp:include></td>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1"><div class="pageHead_1">Reset Password</div>
                                        <form id="frmReset" name="frmReset" method="post" action="">
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td colspan="2">
                                                        Dear User,<br /><br />
                                                        Due to subsequent failed login attempts, system has locked your login account. To reset your password and to unlock your account, please enter your registered e-mail ID in the e-mail ID box given below. On submission of the e-mail ID, system will send you an e-mail with the URL and Verification code for resetting your password.

                                                    </td>
                                                </tr>
                                                <tr>
                                                   <td class="ff">e-mail ID : <span>*</span></td>
                                                    <td>
                                                        <input class="formTxtBox_1" type="text" id="txtreEmailId" name="emailId" style="width: 200px"/>
                                                        <span id="mailMsg" style="font-weight: bold">&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr id="btnTr">
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <label class="formBtn_1">
                                                            <input type="button" name="submit" id="btnSubmit" value="Submit" disabled/>
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
