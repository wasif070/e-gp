<%-- 
    Document   : OnlinePmtForRenewal
    Created on : Feb 16, 2012, 11:47:42 AM
    Author     : nishit
--%>

<%@page import="ipgclient2.CShroff2"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.web.databean.OnlinePaymentDtBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <title>Online Profile Renewal - Pay Renewal Fee Online </title>
    </head>
    <%
                UserRegisterService userRegisterServiceNavigate = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                if(session.getAttribute("payment")!=null){
                  session.removeAttribute("payment");
                }
                if (session.getAttribute("payment") == null) {
                    Object[] paymentNavigateObj = userRegisterServiceNavigate.getLoginDetails(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
                    if (paymentNavigateObj != null) {
                        OnlinePaymentDtBean onpdb = new OnlinePaymentDtBean(String.valueOf(paymentNavigateObj[0]), XMLReader.getMessage("localTenderRenewFee"), "renewal", "0", XMLReader.getMessage("ServiceCharge"));
                        session.setAttribute("payment", onpdb);
                        onpdb = null;
                        paymentNavigateObj = null;
                    }
                }

    %>
    <body>
        <div class="dashboard_div">
            <div class="mainDiv">
                <div class="fixDiv">
                    <%@include  file="resources/common/AfterLoginTop.jsp" %>
                    <div class="contentArea_1">
                        <div class="pageHead_1">Online Profile Renewal - Pay Online Renewal Fee
                            <span style="float:right;"><a href="resources/common/InboxMessage.jsp" class="action-button-goback">Go Back</a></span>
                        </div>
                        <div>&nbsp;</div>
                        <%
                                    OnlinePaymentDtBean opdb = (OnlinePaymentDtBean) session.getAttribute("payment");
                                    if (opdb != null) {
                                        System.out.println(opdb.getCardNumber());
                                        if (opdb.getResponse() != null && ((opdb.getResponse().indexOf("000") > 0 && opdb.getResponse().indexOf("OK") > 0) || opdb.getResponse().indexOf("<txn_status>ACCEPTED</txn_status>") > 0)) {
                        %>
                        <jsp:include page="resources/common/OnlinePaymentSuc.jsp" ></jsp:include>
                        <%} else {%>
                        <jsp:include page="resources/common/OnlinePayment.jsp" ></jsp:include>
                        <%}
                        }%>
                    </div>
                </div>
            </div>
        </div>
        <div align="center">
            <%@include file="resources/common/Bottom.jsp" %>
        </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabMyAcc");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
