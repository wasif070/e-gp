<%-- 
    Document   : TOR1
    Created on : Apr 11, 2011, 10:31:01 AM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="java.util.List"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    String repLabel=null;
                    if(commonService.getProcNature(request.getParameter("tenderid")).toString().equals("Services")){
                        repLabel = "Tender"; //change from 'Proposal'
                    }else{
                        repLabel = "Tender";
                    }
                     boolean bole_tor_flag = true;
                    if(request.getParameter("isPDF")!=null && "true".equalsIgnoreCase(request.getParameter("isPDF"))){
                        bole_tor_flag = false;
                    }
                     
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender Opening Report 1</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
         <script src="../../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link href="../../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        
        <!--
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <body>
        <%
                    String tenderid = request.getParameter("tenderid");
                    String lotId = request.getParameter("lotId");
                   
                    
                    ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                    List<SPCommonSearchDataMore> tendOpeningRpt = creationService.getOpeningReportData("getTenderOpeningRpt", tenderid, lotId,null);//lotid
                    List<SPCommonSearchDataMore> advertDetail = creationService.getOpeningReportData("getAdvertisementDetail", tenderid, lotId,null);
                    List<SPCommonSearchDataMore> openingDateAndTime = creationService.getOpeningReportData("getOpeningDateAndTime", tenderid, lotId,null);
                    List<SPCommonSearchDataMore> tendDocument = creationService.getOpeningReportData("getTenderDocument", tenderid, lotId,null);//lotid
                    List<SPCommonSearchDataMore> TOCMembers = creationService.getOpeningReportData("getTOCMembers", tenderid, lotId,"TOR1"); //lotid ,"TOR1"
                    List<SPCommonSearchDataMore> TOCCP = creationService.getOpeningReportData("getOpeningCommitteeCPId", tenderid, "","");
                    String TOCCPUserID = "0";
                    if(TOCCP!=null || !TOCCP.isEmpty()){
                        TOCCPUserID = TOCCP.get(0).getFieldName1();
                    }
                    boolean list1 = true;
                    boolean list2 = true;
                    boolean list4 = true;
                    if (tendOpeningRpt.isEmpty()) {
                        list1 = false;
                    }
                    if (advertDetail.isEmpty()) {
                        list2 = false;
                    }
                    if (tendDocument.isEmpty()) {
                        list4 = false;
                    }
                    int openDate = openingDateAndTime.size();
        %>
        <div class="contentArea_1">
               <%if(bole_tor_flag){%>
                <span style="float: right;" style="margin-top: 15px;">
                    <%if(request.getParameter("goback")==null || request.getParameter("goback")==""){%>
                    &nbsp;&nbsp;
                    <a class="action-button-goback" href="<%if("eval".equals(request.getParameter("frm"))){out.print(request.getHeader("referer"));}else{out.print("OpenComm.jsp?tenderid="+tenderid);}%>">Go Back to Dashboard</a>
                    <%}%>
                </span>
                <%}%>
                <%
                    //boolean tendDisp = false;
                    if("y".equals(request.getParameter("isT"))){
                        out.print("<span style='float: right;' style='margin-top: 15px;'><a href='javascript:void(0);' id='print' class='action-button-view'>Print</a>&nbsp;&nbsp;<a class='action-button-savepdf' href='"+request.getContextPath()+"/TorRptServlet?tenderId="+tenderid+"&lotId="+lotId+"&action=TOR1'>Save As PDF</a></span>");
                        //tendDisp = true;
                    }
                %>
                <div>

                </div>
               <div  id="print_area">
            <div class="pageHead_1">Tender Opening Report 1
                
                </div>
            <%
                        pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                        pageContext.setAttribute("tab", "6");
                        pageContext.setAttribute("isPDF", request.getParameter("isPDF"));
                        pageContext.setAttribute("userId", 0);
                        if(request.getParameter("msg")!=null){out.print("<br/><div class='responseMsg successMsg'>TOR1 Signed Successfully</div>");}
            %>
            <div class="tableHead_1 t_space"><%if(repLabel.equals("Tender")){%>Tender Opening Report 1<%}else{%>Tender OPENING SUMMARY<%}%></div>
            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 b_space" width="100%">
                <tr>
                    <td width="24%" class="ff">Ministry Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName1());
                                }%></td>
                    <td width="16%" class="ff">Division Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName2());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff">Organization/Agency Name :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName3());
                                }%></td>
                    <td class="ff">Procuring Entity :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName4());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff">Tender Package No. and Description :</td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName5()+" & "+tendOpeningRpt.get(0).getFieldName6());
                                }%></td>
                </tr>
                <%if(!lotId.equals("0")){%>
                <tr>
                    <td class="ff">Lot No. and Description : </td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName12());
                                    if(!tendOpeningRpt.get(0).getFieldName12().equals("")){
                                        out.print(" & ");
                                    }
                                    out.print(tendOpeningRpt.get(0).getFieldName13());
                                }%></td>
                </tr>
                <%}%>
            </table>
            <table width="100%" cellspacing="0" class="tableList_3">
                <tr>
                    <td class="tableHead_1" colspan="4" >Procurement Data</td>
                </tr>
                <tr>
                    <th width="25%" >Procurement Type</th>
                    <th width="25%">Funding By</th>
                    <th width="25%">Budget Type</th>
                    <th width="25%">Method</th>
                </tr>
                <tr>
                    <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName7());
                                }%></td>
                    <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName15());
                                }%></td>
                    <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName8());
                                }%></td>
                    <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName9());
                                }%></td>
                </tr>
            </table>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="4" >Procurement Plan</td>
                </tr>
                <tr>
                    <th width="50%" >Approving Authority</th>
                    <th width="50%">Approval Status</th>
                </tr>
                <tr>
                    <td >
                        <!--Name of the Approving Authority, Designation, Role, Parent Name of office, Office Name-->
                        <%if(list1){out.print(tendOpeningRpt.get(0).getFieldName19());}%>
                    </td>
                    <td class="t-align-center">Approved</td>
                </tr>
            </table>

            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="4" >Advertisement details</td>
                </tr>
                <tr>
                    <th width="23%" class="t-align-center">Newspaper Name</th>
                    <th width="27%" class="t-align-center">Newspaper Date</th>
                    <th width="27%" class="t-align-center">URL Page</th>
                    <th width="23%" class="t-align-center">URL Publishing Date</th>
                </tr>
                <%for (SPCommonSearchDataMore dataMore : advertDetail) {%>
                <tr>
                    <td  class="t-align-center"><%=dataMore.getFieldName1()%></td>
                    <td  class="t-align-center"><%=dataMore.getFieldName2()%></td>
                    <td  class="t-align-center"><a href="<%if(dataMore.getFieldName3().startsWith("http")){out.print(dataMore.getFieldName3());}else{out.print("http://"+dataMore.getFieldName3());}%>" target="_blank"><%=dataMore.getFieldName3()%></a></td>
                    <td  class="t-align-center"><%=dataMore.getFieldName4()%></td>
                </tr>
                <%
                    }
                    if(!list2){out.print("<tr><td colspan='4' class='t-align-center' style='color:red;'>No Records Found</td></tr>");}
                %>
            </table>

            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="2" >Tender Date and Time</td>
                </tr>
                <tr>
                    <th width="73%" >Date and Time of Publishing </th>
                    <th width="27%">Date and Time of Closing</th>
                </tr>
                <tr>
                    <td  class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName10());
                                }%></td>
                    <td  class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName11());
                                }%></td>
                </tr>
            </table>

            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="4" >Opening Date and Time</td>
                </tr>
                <tr>
                    <%
                                for (int i = 0; i < openDate; i++) {
                                    if (i == 0) {
                                        out.print("<th>Original Date and Time</th>");
                                    } else {
                                        out.print("<th>" + (i + 1) + " Extension Date if any</th>");
                                    }
                                }
                    %>
                </tr>
                <tr>
                    <%
                                for (SPCommonSearchDataMore dataMore : openingDateAndTime) {
                                    out.print("<td  class='t-align-center'>" + dataMore.getFieldName1() + "</td>");
                                }
                    %>
                </tr>
            </table>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="4" ><%if(repLabel.equals("Tender")){%>Tender<%}else{%>Event Type (RFA/REOI/RFP)<%}%> Document</td>
                </tr>
                <tr>
                    <th width="25%" >Documents Sold/Downloaded</th>
                    <th width="25%">Nos. of Submissions</th>
                    <th width="25%">Nos. Withdrawn</th>
                    <th width="25%">Nos. Substituted / Modified</th>
                </tr>
                <tr>
                    <td  class="t-align-center"><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName3());
                                }%></td>
                    <td  class="t-align-center"><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName1());
                                }%></td>
                    <td  class="t-align-center"><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName2());
                                }%></td>
                    <td  class="t-align-center"><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName4());
                                }%></td>
                </tr>
            </table>

            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="5" ><%=repLabel.charAt(0)%>OC Members</td>
                </tr>
                <tr>
                    <th width="20%" ><span id="signLabel">Committee Members</span></th>                    
                    <%
                    int svpdftor1 = 0;
                    for (SPCommonSearchDataMore dataMore : TOCMembers) {                        
                       out.print("<td>"+dataMore.getFieldName1()+"</td>");
                    }
                  %>
               </tr>
                <tr>
                    <th width="20%">Committee Role</th>
                    <%for (SPCommonSearchDataMore dataMore : TOCMembers) {
                            if (dataMore.getFieldName2().equals("cp")) {
                            out.print("<td>Chairperson</td>");
                        } else if (dataMore.getFieldName2().equals("ms")) {
                            out.print("<td>Member Secretary</td>");
                        } else if (dataMore.getFieldName2().equals("m")) {
                            out.print("<td>Member</td>");
                            }}%>
                </tr>
                <tr>
                    <th width="20%">Designation</th>
                    <%for (SPCommonSearchDataMore dataMore : TOCMembers) {out.print("<td>"+dataMore.getFieldName3()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">PA Office</th>
                    <%for (SPCommonSearchDataMore dataMore : TOCMembers) {out.print("<td>"+dataMore.getFieldName4()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Electronically Signed <%=repLabel.charAt(0)%>OR On</th>
                    <%for(SPCommonSearchDataMore dataMore : TOCMembers) {if(dataMore.getFieldName6().equals("-")){svpdftor1++;}out.print("<td>"+dataMore.getFieldName6()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Comments</th>
                    <%for(SPCommonSearchDataMore dataMore : TOCMembers) {out.print("<td>"+dataMore.getFieldName8()+"</td>");}%>
                </tr>
            </table>
                <%if(svpdftor1!=0){%>
                    <script type="text/javascript">
                        $('#svpdftor1').hide();
                    </script>
                <%}%>
        </div>
        </div>               
    </body>
    <%
                tendOpeningRpt = null;
                advertDetail = null;
                tendDocument = null;
                TOCMembers = null;
    %>
    
    <script type="text/javascript">
    $(document).ready(function() {

        $("#print").click(function() {
            //alert('sa');
            printElem({ leaveOpen: true, printMode: 'popup' });
        });

    });
    function printElem(options){
        if($('#signId').html()!=null){
            var temp = $('#signId').html().toString();
            $('#signId').hide();
            $('#signId').parent().append("<div class='error'>"+temp+"</div>");
        }
        $('#print_area').printElement(options);
        $('#signId').show();
        if($('#signId').html()!=null){
            $('.error').remove();
        }
        //$('#trLast').hide();
    }

</script>
</html>
<%if(request.getParameter("goback")!=null && request.getParameter("goback")!=""){%>
<script type="text/javascript">
    var headSel_Obj = document.getElementById("headTabEval");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
</script>
<%}else{%>
<script type="text/javascript">
    var headSel_Obj = document.getElementById("headTabTender");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
</script>
<%}%>
