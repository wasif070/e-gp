<%--
    Document   : CompanyDetails
    Created on : Oct 21, 2010, 6:13:00 PM
    Author     : taher
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.UserPrefService"%>
<%@page import="com.cptu.egp.eps.web.databean.TempBiddingPermission"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblTempCompanyMaster,com.cptu.egp.eps.model.table.TblUserPrefrence,com.cptu.egp.eps.web.utility.BanglaNameUtils,com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblTempBiddingPermission"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@ page language="java" import="java.util.*,java.lang.*" %> 

<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="tempCompanyMasterDtBean" class="com.cptu.egp.eps.web.databean.TempCompanyMasterDtBean"/>
    <jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
    <jsp:useBean id="tempBiddingPermission" class="com.cptu.egp.eps.web.databean.TempBiddingPermission"/>
    <jsp:setProperty property="*" name="tempCompanyMasterDtBean"/>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Bidder Registration : Company Details</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%-- <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />--%>

        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="resources/js/datepicker/css/border-radius.css" />
        <link href="resources/js/poshytipLib/tip-yellowsimple/tip-yellowsimple.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="resources/js/datepicker/js/lang/en.js"></script>
        <script src="resources/js/poshytipLib/jquery.poshytip.js" type="text/javascript"></script>

        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <style>
            .formNoteTxt, #cpvTip {
                font-style: italic;

            }
        </style>

        <script type="text/javascript">
            $(document).ready(function () {

                $('.formTxtBox_1').poshytip({
                    className: 'tip-yellowsimple',
                    showOn: 'focus',
                    alignTo: 'target',
                    alignX: 'right',
                    alignY: 'center',
                    offsetX: 5,
                    showTimeout: 100
                });
                $('.InstructionLabel').poshytip({
                    className: 'tip-yellowsimple',
                    showOn: 'focus',
                    alignTo: 'target',
                    alignX: 'right',
                    alignY: 'center',
                    offsetX: 5,
                    showTimeout: 100
                });



                $("#frmComp").validate({
                    rules: {
                        companyName: {required: true, maxlength: 200, alphaCompanyName: true},
                        companyRegNumber: {maxlength: 30},
                        tradeLicenseNumber: {maxlength: 30},
                        specialization: {required: true},
                        legalStatus: {required: true},
                        regOffAddress: {required: true, maxlength: 250},
                        regOffCity: {maxlength: 50, DigAndNum: true},
//                        regOffUpjilla: {maxlength: 50, alphaNationalId: true},
                        regOffPostcode: {/*required: true,*/number: true, minlength: 4},
                        corpOffPostcode: {/*required: true,*/number: true, minlength: 4},
                        regPhoneSTD: {maxlength: 10, digits: true},
                        regOffPhoneNo: {maxlength: 20, digits: true},
                        regOffFaxNo: {maxlength: 20, digits: true},
                        regFaxSTD: {maxlength: 10, digits: true},
                        //tinNo: {required: true},
                        establishmentYear: {required: true, digits: true, maxlength: 4, CompareYear: "#currYear"},
                        website: {url: true},
                        regOffMobileNo: {required: true, number: true, minlength: 8, maxlength: 8},
                        corpOffAddress: {required: true, maxlength: 250},
                        corpOffMobMobileNo: {required: true, number: true, minlength: 8, maxlength: 8}

                    },
                    messages: {
                        companyName: {required: "<div class='reqF_1'>Please enter Company Name</div>",
                            maxlength: "<div class='reqF_1'>Maximum 200 characters are allowed</div>",
                            alphaCompanyName: "<div class='reqF_1'>Allows alpha numerics and the following special characters (&/-.)</div>"},
                        companyRegNumber: {
                            //required:"<div class='reqF_1'>Please enter Company Registration No.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 30 characters are allowed</div>"},
                        tradeLicenseNumber: {
                            maxlength: "<div class='reqF_1'>Maximum 30 characters are allowed</div>"},
                        specialization: {required: "<div class='reqF_1'>Please select Nature of Business</div>"},
                        legalStatus: {required: "<div class='reqF_1'>Please select Company's Legal Status</div>"},
                        regOffAddress: {required: "<div class='reqF_1'>Please enter Registered office Address</div>",
                            maxlength: "<div class='reqF_1'>Maximum 150 characters are allowed</div>"},
                        regOffCity: {required: "<div class='reqF_1'>Please enter City / Town</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>",
                            DigAndNum: "<div class='reqF_1'>Allows Characters and Special characters (, . - '&) only. But only Special Characters are not allowed.</div>"},
//                        regOffUpjilla: {
//                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>",
//                            alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>"},
                        regOffPostcode: {/* required: "<div class='reqF_1'>Please enter Postcode </div>",*/
                            number: "<div class='reqF_1'>Only numbers (0-9) are allowed.</div>",
                            maxlength: "<div class='reqF_1'>Minimum 4 Characters are required.</div>"},
                        corpOffPostcode: {/* required: "<div class='reqF_1'>Please enter Postcode </div>",*/
                            number: "<div class='reqF_1'>Only numbers (0-9) are allowed.</div>",
                            maxlength: "<div class='reqF_1'>Minimum 4 Characters are required.</div>"},
                        regPhoneSTD: {required: "<div class='reqF_1'>Please enter Area Code.</div>",
                            digits: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 10 Digits are allowed</div>"},
                        regOffPhoneNo: {required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            digits: "<div class='reqF_1'>Please enter Numbers Only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 20 Digits are allowed</div>"},
                        regOffFaxNo: {digits: "<div class='reqF_1'>Please enter Numbers Only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 20 Digits are allowed</div>"},
                        regFaxSTD: {digits: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 10 Digits are allowed</div>"},
                        //tinNo: {required: "<div class='reqF_1'>Please enter Tax Payment Number</div>"},
                        establishmentYear: {required: "<div class='reqF_1'>Please enter Company's Establishment Year</div>",
                            digits: "<div class='reqF_1'>Please enter numeric values only</div>",
                            maxlength: "<div class='reqF_1'>Please enter 4 digits only. Only 0's not allowed</div>",
                            CompareYear: "<div class='reqF_1'>Company Establishment Year should be current or less than the current year</div>"},
                        website: {url: "<div class='reqF_1'>Please enter valid Website Name</div>"},
                        regOffMobileNo: {required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            minlength: "<div class='reqF_1'>Exactly 8 digits required</div>",
                            maxlength: "<div class='reqF_1'>Exactly 8 digits required</div>"
                        },
                        corpOffMobMobileNo: {required: "<div class='reqF_1'>Please Enter Mobile No.</div>",
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            minlength: "<div class='reqF_1'>Exactly 8 digits required</div>",
                            maxlength: "<div class='reqF_1'>Exactly 8 digits required</div>"
                        },
                        corpOffAddress: {required: "<div class='reqF_1'>Please Enter Corporate office Address.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 150 Characters are allowed.</div>"
                        },
                    },
                    errorPlacement: function (error, element) {
//                        if (element.attr("name") == "licExpiryDateStr") {
//                            error.insertAfter("#txttenderLastSellDateimg1");
//                        }
                        /*else
                         error.insertAfter(element);*/


//                        else if (element.attr("name") == "licIssueDateStr") {
//                            error.insertAfter("#txttenderLastSellDateimg");
//                        }
                        /*else
                         error.insertAfter(element);*/

                        if (element.attr("name") == "specialization") {
                            error.insertAfter("#cpvTip");
//                        }
                        /*else
                         error.insertAfter(element);*/

//                        else if (element.attr("name") == "establishmentYear") {
//                            error.insertAfter("#cey");
                        } else if (element.attr("name") == "regPhoneSTD") {
                            error.insertAfter("#txtRegPhone");
                        } else if (element.attr("name") == "regOffPhoneNo") {
                            error.insertAfter("#hdnvalue");
                        }
                        /*else
                         error.insertAfter(element);*/

                        else if (element.attr("name") == "regFaxSTD") {
                            error.insertAfter("#txtRegFax");
                        } else if (element.attr("name") == "website") {
                            error.insertAfter("#webnote");
                        } else {
                            error.insertAfter(element);
                        }

                    }

                });


            });



            function ValidateUpdate() {

                var check = "";
                if ($("#serviceschk").checked)
                {//alert();
                    if (!document.getElementsById("consultingchk").checked && !document.getElementsById("nonconsultingchk").checked)
                    {//alert();
                        $('#msgprocurementtype').html('Please Select Consulting or Non-consulting');
                        //check = "true";
                        return false;
                    }
                }

                //taher May17
                $(".err").remove();

                if (check == "true")
                {
                    return false;
                }
            }

            function cleartinDocName() {

            }

            function cleartxtaODoc() {
                var check2 = ''
            }

            function ValidateissueDate()
            {
                var chck = "true";
                if ($('#frmComp').validate())
                {
                    if ($('#cmbRegCountry').val() == "150")
                    {
                        return true;
                    }
                }
                if (chck == "false") {
                    return false;
                }
            }
            function ValidateexpiryDate()
            {
                var chck = "false";
                if ($('#frmComp').validate())
                {
                    if ($('#cmbRegCountry').val() == "150")
                    {
                        //if (document.getElementById("txtExpiryDate").value == '')
                        //{
                        //    document.getElementById("expiryDate").innerHTML = "<br/>Please enter Trade License Expiry Date";
                        //    chck = "false";
                        //}
                    }
                }
                if (chck == 'false') {
                    return false;
                }
            }
            function clearIssueDate() {

            }
            function clearExpiryDate() {

            }

            function setRegisteredCategory(obj) {
                if (document.getElementById('registeredchk').checked == true) {
                    document.getElementById('w2largechk').checked = true;
                    document.getElementById('w2mediumchk').checked = true;
                    document.getElementById('w2smallchk').checked = true;

                } else {

                    if ("registeredchk" == obj.id) {
                        document.getElementById('w2largechk').checked = false;
                        document.getElementById('w2mediumchk').checked = false;
                        document.getElementById('w2smallchk').checked = false;

                    }
                }
            }
            function checkW2Category()
            {
                if (document.getElementById('registeredchk').checked)
                {

                    if (document.getElementById('w2largechk').checked) {
                        document.getElementById('largechkbox').disabled = false;
                        document.getElementById('largechkbox').checked = true;

                        EnableLargeSubCat();
                    }
                    if (document.getElementById('w2mediumchk').checked) {
                        document.getElementById('mediumchkbox').disabled = false;
                        document.getElementById('mediumchkbox').checked = true;
                        EnableMediumSubCat();
                    }
                    if (document.getElementById('w2smallchk').checked) {
                        document.getElementById('smallchkbox').disabled = false;
                        document.getElementById('smallchkbox').checked = true;
                        EnableSmallSubCat();
                    }
                }
                if (document.getElementById('w2largechk').checked == false || document.getElementById('w2mediumchk').checked == false || document.getElementById('w2smallchk').checked == false)
                {
                    document.getElementById('registeredchk').checked = false;
                }
                if (document.getElementById('w2largechk').checked == true && document.getElementById('w2mediumchk').checked == true && document.getElementById('w2smallchk').checked == true)
                {
                    //   document.getElementById('registeredchk').checked = true;
                }

                // if()
            }

//            function CheckedCategory()
//            {
//                if (document.getElementById('w1largechk').checked == true || document.getElementById('w2largechk').checked == true || document.getElementById('w3largechk').checked == true || document.getElementById('w4largechk').checked == true)
//                {
//                    //document.getElementById('largechkbox').checked = true;
//                }
//                if (document.getElementById('w1mediumchk').checked == true || document.getElementById('w2mediumchk').checked == true || document.getElementById('w3mediumchk').checked == true || document.getElementById('w4mediumchk').checked == true)
//                {
//                    //document.getElementById('mediumchkbox').checked = true;
//                }
//                if (document.getElementById('w1smallchk').checked == true || document.getElementById('w2smallchk').checked == true || document.getElementById('w3smallchk').checked == true || document.getElementById('w4smallchk').checked == true)
//                {
//                    //document.getElementById('smallchkbox').checked = true;
//                }
//                if (document.getElementById('w1largechk').checked == false && document.getElementById('w2largechk').checked == false && document.getElementById('w3largechk').checked == false && document.getElementById('w4largechk').checked == false)
//                {
//                    //document.getElementById('largechkbox').checked = false;
//                }
//                if (document.getElementById('w1mediumchk').checked == false && document.getElementById('w2mediumchk').checked == false && document.getElementById('w3mediumchk').checked == false && document.getElementById('w4mediumchk').checked == false)
//                {
//                    //document.getElementById('mediumchkbox').checked = false;
//                }
//                if (document.getElementById('w1smallchk').checked == false && document.getElementById('w2smallchk').checked == false && document.getElementById('w3smallchk').checked == false && document.getElementById('w4smallchk').checked == false)
//                {
//                    //document.getElementById('smallchkbox').checked = false;
//                }
//                if (document.getElementById('largechkbox').checked == true || document.getElementById('mediumchkbox').checked == true || document.getElementById('smallchkbox').checked == true) {
//                    document.getElementById('workschk').checked = true;
//                }
//                if (document.getElementById('largechkbox').checked == false && document.getElementById('mediumchkbox').checked == false && document.getElementById('smallchkbox').checked == false) {
//                    //document.getElementById('workschk').checked = false;
//                }
//
//
//
//                if (document.getElementById('consultingchk').checked == true || document.getElementById('nonconsultingchk').checked == true) {
//
//                    document.getElementById('serviceschk').checked = true;
//                }
//
//
//                if (document.getElementById('consultingchk').checked == false && document.getElementById('nonconsultingchk').checked == false)
//                {
//                    document.getElementById('serviceschk').checked = false;
//                }
//            }

            function ProcurementType()
            {
                if (document.getElementById('goodschk').checked == false && document.getElementById('workschk').checked == false && document.getElementById('serviceschk').checked == false)
                {
                    $('#msgprocurementtype').html('Please Select Procurement Category');
                    return false;
                } else
                    $('#msgprocurementtype').html('');
            }

            function hideprocurementmsg()
            {
                if (document.getElementById('goodschk').checked == true || document.getElementById('workschk').checked == true || document.getElementById('serviceschk').checked == true)
                    $('#msgprocurementtype').html('');
            }



            //nishith-15/05/2016
            function chkWorks() {
                if (document.getElementById('workschk').checked) {
                    EnableLMSR();
                } else {
                    DisableLMSR();
                    DisableLargeSubCat();
                    DisableMediumSubCat();
                    DisableSmallSubCat();

                }
            }

            function chkSerivce()
            {
                if (document.getElementById('serviceschk').checked) {
                    document.getElementById('consultingchk').disabled = false;
                    document.getElementById('nonconsultingchk').disabled = false;
                } else
                {
                    document.getElementById('consultingchk').disabled = true;
                    document.getElementById('consultingchk').checked = false;
                    document.getElementById('nonconsultingchk').disabled = true;
                    document.getElementById('nonconsultingchk').checked = false;
                }
            }

            function chkLarge() {
                if (document.getElementById('largechkbox').checked) {
                    EnableLargeSubCat();
                } else {
                    DisableLargeSubCat();
                    document.getElementById('registeredchk').checked = false;
                }
            }
            function chkMedium() {
                if (document.getElementById('mediumchkbox').checked) {
                    EnableMediumSubCat()
                } else {
                    DisableMediumSubCat();
                    document.getElementById('registeredchk').checked = false;
                }
            }
            function chkSmall() {
                if (document.getElementById('smallchkbox').checked) {
                    EnableSmallSubCat();
                } else {
                    DisableSmallSubCat();
                    document.getElementById('registeredchk').checked = false;
                }
            }




            function EnableLMSR() {
                document.getElementById('largechkbox').disabled = false;
                document.getElementById('mediumchkbox').disabled = false;
                document.getElementById('smallchkbox').disabled = false;
                document.getElementById('registeredchk').disabled = false;
            }

            function DisableLMSR() {
                document.getElementById('largechkbox').disabled = true;
                document.getElementById('mediumchkbox').disabled = true;
                document.getElementById('smallchkbox').disabled = true;
                document.getElementById('registeredchk').disabled = true;

                document.getElementById('largechkbox').checked = false;
                document.getElementById('mediumchkbox').checked = false;
                document.getElementById('smallchkbox').checked = false;
                document.getElementById('registeredchk').checked = false;
            }


            function EnableLargeSubCat() {
                document.getElementById('w1largechk').disabled = false;
                document.getElementById('w2largechk').disabled = false;
                document.getElementById('w3largechk').disabled = false;
                document.getElementById('w4largechk').disabled = false;
            }

            function EnableMediumSubCat() {
                document.getElementById('w1mediumchk').disabled = false;
                document.getElementById('w2mediumchk').disabled = false;
                document.getElementById('w3mediumchk').disabled = false;
                document.getElementById('w4mediumchk').disabled = false;
            }

            function EnableSmallSubCat() {
                document.getElementById('w1smallchk').disabled = false;
                document.getElementById('w2smallchk').disabled = false;
                document.getElementById('w3smallchk').disabled = false;
                document.getElementById('w4smallchk').disabled = false;
            }



            function DisableLargeSubCat() {
                document.getElementById('w1largechk').disabled = true;
                document.getElementById('w2largechk').disabled = true;
                document.getElementById('w3largechk').disabled = true;
                document.getElementById('w4largechk').disabled = true;

                document.getElementById('w1largechk').checked = false;
                document.getElementById('w2largechk').checked = false;
                document.getElementById('w3largechk').checked = false;
                document.getElementById('w4largechk').checked = false;
            }

            function DisableMediumSubCat() {
                document.getElementById('w1mediumchk').disabled = true;
                document.getElementById('w2mediumchk').disabled = true;
                document.getElementById('w3mediumchk').disabled = true;
                document.getElementById('w4mediumchk').disabled = true;

                document.getElementById('w1mediumchk').checked = false;
                document.getElementById('w2mediumchk').checked = false;
                document.getElementById('w3mediumchk').checked = false;
                document.getElementById('w4mediumchk').checked = false;
            }
            function DisableSmallSubCat() {
                document.getElementById('w1smallchk').disabled = true;
                document.getElementById('w2smallchk').disabled = true;
                document.getElementById('w3smallchk').disabled = true;
                document.getElementById('w4smallchk').disabled = true;

                document.getElementById('w1smallchk').checked = false;
                document.getElementById('w2smallchk').checked = false;
                document.getElementById('w3smallchk').checked = false;
                document.getElementById('w4smallchk').checked = false;
            }





        </script>
        <script type="text/javascript">
            
            function GetCal(txtname, controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat: "%d/%m/%Y",
                    onSelect: function () {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>

        <script type="text/javascript">
            function onTickCountry(data) {
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: data, funName: 'stateComboValue'}, function (j) {
                    $("select#cmbCorpState").html(j);
                    //alert($("select#cmbCorpState").html(j));
                    //Code by Proshanto
                    if ($('#cmbCorpCountry').val() == "150") {//136
                        $('#trCthana').css("display", "table-row");
                        $('#trCSubdistrict').css("display", "table-row");
                    } else {
                        $('#trCthana').css("display", "none");
                        $('#trCSubdistrict').css("display", "none");
                    }
                });
                
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                    $('#cmbCorpSubdistrict').html(j);
                });
                
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtCorpThana').html(j);
                    });
                
                $.post("<%=request.getContextPath()%>/CommonServlet", {countryId: $('#cmbCorpCountry').val(), funName: 'countryCode'}, function (j) {
                    $('#corpPhoneCode').val(j.toString());
                    $('#corpFaxCode').val(j.toString());
                    $('#corpMobCode').val(j.toString());
                });
                
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: 'Bumthang', funName: 'AreaCodeSelection'}, function (j) {
                    $('#corpPhoneSTD').val(j);
                    $('#corpFaxSTD').val(j);
                });
            }
            $(function () { //Taher
                $('#cmbRegCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegCountry').val(), funName: 'stateComboValue'}, function (j) {
                        $("select#cmbRegState").html(j);
                        //Code by Proshanto
                        if ($('#cmbRegCountry').val() == "150") {//136
                            $('#trRthana').css("display", "table-row")
                            $('#trRSubdistrict').css("display", "table-row")
                            $('#trtlid').css("display", "inline")
                            $('#trtled').css("display", "inline")
                            
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbRegState').val(), funName: 'AreaCodeSelection'}, function (j) {
                                $('#regPhoneSTD').val(j);
                                $('#regFaxSTD').val(j);
                            });
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbRegState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                                $('#txtRegThana').html(j);
                            });
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbRegState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                                $('#cmbRegSubdistrict').html(j);
                            });
                            
                        } else {
                            $('#trRthana').css("display", "none")
                            $('#trRSubdistrict').css("display", "none")
                            //rajesh
                            $('#trtlid').css("display", "none")
                            $('#trtled').css("display", "none")
                            
                            $('#issueDate').html(null);
                            $('#expiryDate').html(null);
                            $('#issueDatestart').html(null);
                            $('#cmpDate').html(null);
                        }
                    });
                    $('#txtRegCity').val(null);
                    $('#txtRegPost').val(null);
                    $('#txtregOffMobileNo').val(null);
                    $('#txtRegPhone').val(null);
                    $('#txtRegFax').val(null);
                    $('#regPhoneSTD').val(null);
                    $('#regFaxSTD').val(null);
                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId: $('#cmbRegCountry').val(), funName: 'countryCode'}, function (j) {
                        $('#regPhoneCode').val(j.toString());
                        $('#regFaxCode').val(j.toString());
                        $('#regMobCode').val(j.toString());
                        
                    });
                    });
                    
                    $('#cmbCorpSubdistrict').change(function () {
                        //alert($('#cmbCorpSubdistrict').val());
                        if($('#cmbCorpSubdistrict').val()=='Phuentsholing')
                            {$('#corpPhoneSTD').val('05');
                            $('#corpFaxSTD').val('05');}
                        else
                        {
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'AreaCodeSelection'}, function (j) {
                                $('#corpPhoneSTD').val(j);
                                $('#corpFaxSTD').val(j);
                            });
                        }
                    });
                    
                    $('#cmbRegSubdistrict').change(function () {
                        //alert($('#cmbRegSubdistrict').val());
                        if($('#cmbRegSubdistrict').val()=='Phuentsholing')
                            {$('#regPhoneSTD').val('05');
                            $('#regFaxSTD').val('05');}
                        else
                        {
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'AreaCodeSelection'}, function (j) {
                                $('#regPhoneSTD').val(j);
                                $('#regFaxSTD').val(j);
                            });
                        }
                    });
            });
//            $(function () {// Automatic SubDistrict & Area code selection by Emtaz 21/May/2016
//                $('#cmbRegState').change(function () {
//                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'subDistrictComboValue'}, function (j) {
//                        $('#cmbRegSubdistrict').html(j);
//                    });
//                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'AreaCodeSelection'}, function (j) {
//                        $('#regPhoneSTD').val(j);
//                        $('#regFaxSTD').val(j);
//                    });
//                });
//            });
            function SubDistrictANDAreaCodeLoadOffice()
            {// Automatic SubDistrict & Area code selection by Emtaz 22/May/2016
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'GetSubdistrictList', SubDistrict: $('#cmbRegSubdistrict').val()}, function (j) {
                    $('#cmbRegSubdistrict').html(j);
                });
                if($('#cmbRegSubdistrict').val()=="Phuentsholing")
                {
                    $('#regPhoneSTD').val('05');
                    $('#regFaxSTD').val('05');
                }
                else
                {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'AreaCodeSelection'}, function (j) {
                        $('#regPhoneSTD').val(j);
                        $('#regFaxSTD').val(j);
                    });
                }
            }
            function SubDistrictANDAreaCodeLoadCorporation()
            {// Automatic SubDistrict & Area code selection by Emtaz 22/May/2016
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'GetSubdistrictList', SubDistrict: $('#cmbCorpSubdistrict').val()}, function (j) {
                    $('#cmbCorpSubdistrict').html(j);
                });
                if($('#cmbCorpSubdistrict').val()=="Phuentsholing")
                {
                    $('#corpPhoneSTD').val('05');
                    $('#corpFaxSTD').val('05');
                }
                else
                {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'AreaCodeSelection'}, function (j) {
                        $('#corpPhoneSTD').val(j);
                        $('#corpFaxSTD').val(j);
                    });
                }
            }

            $(function () {
                $('#cmbCorpCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpCountry').val(), funName: 'stateComboValue'}, function (j) {
                        $("select#cmbCorpState").html(j);
                        //Code by Proshanto
                        if ($('#cmbCorpCountry').val() == "150") {//136
                            $('#trCthana').css("display", "table-row")
                            $('#trCSubdistrict').css("display", "table-row")
                            
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbCorpState').val(), funName: 'AreaCodeSelection'}, function (j) {
                                $('#corpPhoneSTD').val(j);
                                $('#corpFaxSTD').val(j);
                            });
                            
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbCorpState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                                $('#txtCorpThana').html(j);
                            });
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbCorpState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                                $('#cmbCorpSubdistrict').html(j);
                            });
                            
                        } else {
                            $('#trCthana').css("display", "none")
                            $('#trCSubdistrict').css("display", "none")
                        }
                    });
                    $('#txtCorpCity').val(null);
                    $('#txtCorpPost').val(null);
                    $('#txtcorpOffMobileNo').val(null);
                    $('#txtCorpPhone').val(null);
                    $('#txtCorpFax').val(null);
                    $('#corpPhoneSTD').val(null);
                    $('#corpFaxSTD').val(null);
                });
            });
//            $(function () {// Automatic SubDistrict & Area code selection by Emtaz 21/May/2016
//                $('#cmbCorpState').change(function () {
//                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'subDistrictComboValue'}, function (j) {
//                        $('#cmbCorpSubdistrict').html(j);
//                    });
//                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'AreaCodeSelection'}, function (j) {
//                        $('#corpPhoneSTD').val(j);
//                        $('#corpFaxSTD').val(j);
//                    });
//                });
//            });

            $(function () {
                $('#frmComp').submit(function () {
                    var bol = true;


                    if (!document.getElementById('workschk').checked) {

                        if (document.getElementById('goodschk').checked || document.getElementById('serviceschk').checked) {
                            //return true;
                        } else {
                            $('#msgprocurementtype').html('Please Select a Procurement Category');
                            return false;
                        }
                    } else {
                        if (document.getElementById('registeredchk').checked) {
                            return true;
                        } else if (document.getElementById('w1largechk').checked == true || document.getElementById('w2largechk').checked == true || document.getElementById('w3largechk').checked == true || document.getElementById('w4largechk').checked == true) {
                            return true;
                        } else if (document.getElementById('w1mediumchk').checked == true || document.getElementById('w2mediumchk').checked == true || document.getElementById('w3mediumchk').checked == true || document.getElementById('w4mediumchk').checked == true) {
                            return true;
                        } else if (document.getElementById('w1smallchk').checked == true || document.getElementById('w2smallchk').checked == true || document.getElementById('w3smallchk').checked == true || document.getElementById('w4smallchk').checked == true) {
                            return true;
                        } else {
                            $('#msgprocurementtype').html('Please Select Classification of Contractor');
                            return false;
                        }
                    }


                    if (document.getElementById('serviceschk').checked)
                    {

                        if (!document.getElementById('consultingchk').checked && !document.getElementById('nonconsultingchk').checked)
                        {
                            $('#msgprocurementtype').html('Please Select Consulting or Non-consulting');
                            return false;
                        }
                    }

                    //Code by Proshanto aka spiderman
                    if ($('#cmbRegCountry').val() == "150") {//136                        

                        if ($.trim($('#txtRegThana').val()) == "") {
//                            $('#spRthana').html("Please enter Gewog");
//                            $('#txtRegThana').focus();
//                            bol = false;
                            //return true;
                        } else {
                            $('#spRthana').html(null);
                        }
                    }

                    //Code by Proshanto the boss aka spiderman
                    if ($('#cmbCorpCountry').val() == "150") {//136
                        if ($.trim($('#txtCorpThana').val()) == "") {
//                            $('#spCthana').html("Please enter Gewog");
//                            $('#txtCorpThana').focus();
//                            bol = false;
                            //return true;
                        } else {
                            if ($.trim($('#txtCorpThana').val()).lenght > 50) {
                                //$('#spCthana').html("Maximum 50 Characters are allowed.");
                                //bol = false;
                            } else
                            {
                                if (!alphaNationalId($.trim($('#txtCorpThana').val()))) {
                                    //$('#spCthana').html("Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.");
                                   //bol = false;
                                } else
                                {
                                    $('#spCthana').html(null);
                                }

                            }
                        }
                    }


                    /*Company Reg no unique change removed
                     *if($.trim($('#txtCompNo').val())==""){
                     $('span.#comMsg').css("color","red");
                     $('span.#comMsg').html('Please enter Company Registration Number');
                     bol= false;
                     }
                     if($('#ifedit').html()==null){
                     if($('span.#comMsg').html()=="OK"){
                     }
                     else{
                     bol= false;
                     }
                     }*/

                    if ($.trim($('#txtaCorpAddr').val()) == "") {
                        $('#stxtaCorpAddr').html('Please enter Corporate Office Address');
                        bol = false;
                    } else
                    {
                        if (document.getElementById("txtaCorpAddr").value.length > 150) {
                            $('#stxtaCorpAddr').html('Maximum 150 Characters are allowed');
                            bol = false;
                        } else
                        {
                            $('#stxtaCorpAddr').html('');
                        }
                    }


                    if ($.trim($('#txtCorpCity').val()) == "") {
                        //$('#stxtCorpCity').html('Please enter City / Town');
                        //bol = false;
                        //return true;
                    } else
                    {
                        if ($.trim(document.getElementById("txtCorpCity").value.length) > 50) {
                            $('#stxtCorpCity').html('Maximum 50 Characters are allowed');
                            bol = false;
                        } else
                        {
                            if (!DigAndNum(document.getElementById("txtCorpCity").value)) {
                                $('#stxtCorpCity').html("Allows characters and Special characters (, . - '&) only");
                            } else {
                                $('#stxtCorpCity').html('');
                            }
                        }
                    }

                    if ($.trim($('#corpPhoneSTD').val()) == "") {
                        //$('#scorpPhoneSTD').html('Please enter Area Code.');
                        //bol = false;
                        //return true;
                    } else
                    {
                        if ($.trim(document.getElementById("corpPhoneSTD").value.length) > 10) {
                            $('#scorpPhoneSTD').html('Maximum 10 Digits are allowed');
                            bol = false;
                        } else
                        {

                            if (!digits(document.getElementById("corpPhoneSTD").value)) {
                                $('#scorpPhoneSTD').html("Please enter Numbers only");
                            } else {
                                $('#scorpPhoneSTD').html('');
                            }
                        }
                    }

                    if ($.trim($('#txtCorpPhone').val()) == "") {
                        //$('#stxtCorpPhone').html('Please enter Phone No.');
                        //bol = false;
                        //erturn true;
                    } else
                    {
                        if ($.trim(document.getElementById("txtCorpPhone").value.length) > 20) {
                            $('#stxtCorpPhone').html('Maximum 20 Digits are allowed.');
                            bol = false;
                        } else
                        {

                            if (!digits(document.getElementById("txtCorpPhone").value)) {
                                $('#stxtCorpPhone').html("Please enter Numbers Only");
                            } else {
                                $('#stxtCorpPhone').html('');
                            }
                        }
                    }

                    //corporate mobile
                    if ($.trim($('#txtcorpOffMobileNo').val()) == "") {
                        $('#corpmobMsg').html('Please enter Mobile No.');
                        bol = false;
                        //return true;
                    } else
                    {
                        if ($.trim(document.getElementById("txtcorpOffMobileNo").value.length) > 8) {
                            $('#corpmobMsg').html('Maximum 8 Digits are allowed.');
                            bol = false;
                        } else
                        {

                            if (!digits(document.getElementById("txtcorpOffMobileNo").value)) {
                                $('#corpmobMsg').html("Please enter Numbers Only");
                            } else {
                                $('#corpmobMsg').html('');
                            }
                        }
                    }


                    if (!bol) {
                        return false;
                    } else {

                        if (($('#cmpSubmit').valid()) && (document.getElementById("hdnerrorcount").value == 0)) {
                            if ($('#btnSave').val() != null) {
                                //$('#btnSave').attr("disabled","true");
                                $('#hdnsaveNEdit').val("Save");
                            }
                            if ($('#btnUpdate').val() != null) {
                                //$('#btnUpdate').attr("disabled","true");
                                $('#hdnsaveNEdit').val("Update");
                            }
                        }
                    }
                });
            });

            function DigAndNumThana(value) {
                return /^([\a-zA-Z0-9]+([\a-zA-Z]+[\s-.&,\(\)\']+)?)+$/.test(value);
            }
            function alphaNationalId(value) {
                return /^([a-zA-Z 0-9]([a-zA-Z 0-9\s\[\]\!\(\)\-\.\&\*\#\@/\\(/)/)])+$)/.test(value);
            }
            function DigAndNum(value) {
                return /^([\a-zA-Z]+([\a-zA-Z\s.]+[\s-.&,\(\)\']+)?)+$/.test(value);
            }
            
            function alphaCompanyName(value) {
                return /^[a-zA-Z 0-9](?![0-9]+$)[a-zA-Z 0-9\&\.\-\(\)/]+$/.test(value);
            }

            function alphanumeric(value) {
                return /^[\sa-zA-Z0-9\'\-]+$/.test(value);
            }

            function digits(value) {
                return /^\d+$/.test(value);
            }

            function CompareToForGreater1(value, params)
            {


                var mdy = value.split('/')  //Date and month split
                var mdyhr = mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr = mdyp[2].split(' ');


                if (mdyhr[1] == undefined && mdyphr[1] == undefined)
                {
                    var date = new Date(mdyhr[0], mdy[1] - 1, mdy[0]);
                    var datep = new Date(mdyphr[0], mdyp[1] - 1, mdyp[0]);
                } else if (mdyhr[1] != undefined && mdyphr[1] != undefined)
                {
                    //alert('Both DateTime');
                    var mdyhrsec = mdyhr[1].split(':');
                    var date = new Date(mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                    var mdyphrsec = mdyphr[1].split(':');
                    var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                } else
                {
                    //alert('one Date and One DateTime');
                    var a = mdyhr[1];  //time
                    var b = mdyphr[1]; // time

                    if (a == undefined && b != undefined)
                    {
                        //alert('First Date');
                        var date = new Date(mdyhr[0], mdy[1], mdy[0], '00', '00');
                        var mdyphrsec = mdyphr[1].split(':');
                        var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    } else
                    {
                        //alert('Second Date');
                        var mdyhrsec = mdyhr[1].split(':');
                        var date = new Date(mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], '00', '00');
                    }
                }

                return Date.parse(date) > Date.parse(datep);
            }
            //Company Name unique change removed
            $(function() {
                $('#txtCompName').blur(function() {
                    //alert($('#txtCompName').val());
                    if($.trim($('#txtCompName').val())==""){
                        $('span.#comMsg').css("color","red");
                        //$('span.#comMsg').html('Please enter Company Name');
                        $('#tbodyHide').show();
                        $('span.#redirect').css("display","none");
                    }
                    else{
                        if(alphaCompanyName($.trim($('#txtCompName').val()))){
                        $('span.#comMsg').css("color","red");
                        $('span.#comMsg').html("Checking for Company Name");
                        $.post("<%=request.getContextPath()%>/CommonServlet", {companyName:$('#txtCompName').val(),funName:'compNameChk'},
                        function(j){
                            if(j.toString()!=""){
                                if(j.toString().indexOf("OK", 0)!=-1){
                                    $('span.#comMsg').css("color","green");
                                    $('#tbodyHide').show();
                                    $('span.#redirect').css("display","none");
                                }
                                else {
                                    $('span.#comMsg').css("color","red");
                                    j = j.toString();
                                }
                                $('span.#comMsg').html(j);
                            }else{
                                $('span.#comMsg').html(null);
                                $('#tbodyHide').show();
                                $('span.#redirect').css("display","none");
                            }
                        });
                       }
                       }
                });
                });
        </script>
        <!--Company Reg no unique change removed
            script type="text/javascript">
            $(function() {
                $('#txtCompNo').blur(function() {
                    if($.trim($('#txtCompNo').val())==""){
                        $('span.#comMsg').css("color","red");
                        $('span.#comMsg').html('Please enter Company Registration Number');
                        $('#tbodyHide').show();
                        $('span.#redirect').css("display","none");
                    }else{
                        $('span.#comMsg').css("color","red");
                        $('span.#comMsg').html("Checking for Unique Registration No...");
                        $.post("<%=request.getContextPath()%>/CommonServlet", {compRegNo:$('#txtCompNo').val(),funName:'compNoChk'},
                        function(j){
                            if(j.toString()!=""){
                                if(j.toString().indexOf("OK", 0)!=-1){
                                    $('span.#comMsg').css("color","green");
                                    $('#tbodyHide').show();
                                    $('span.#redirect').css("display","none");
                                }
                                else {
                                    /*
                                     *Below block of code is commented for change that
                                     *already exist company dump is
                                     *no more required.
                                    */
                                    $('span.#comMsg').css("color","red");
                                    /*$('#tbodyHide').hide();
                                    $('span.#redirect').css("display","block");
                                    var temp = "<=request.getContextPath()%>/PersonalDetails.jsp?cmpId="+j.toString().substring(j.toString().indexOf(",", 0)+1, j.toString().length);
                                    $('#hdnCmp').html("<=request.getContextPath()%>/PersonalDetails.jsp?cmpId="+j.toString().substring(j.toString().indexOf(",", 0)+1, j.toString().indexOf("@", 0)));
                                    $('#txtCmpName').val(j.toString().substring(j.toString().indexOf("@", 0)+1, j.toString().length));*/
                                    j = j.toString().substring(0,j.toString().indexOf(",", 0));
                                }
                                $('span.#comMsg').html(j);
                            }else{
                                $('span.#comMsg').html(null);
                                $('#tbodyHide').show();
                                $('span.#redirect').css("display","none");
                            }
                        });
                       }
                });
            });
        </script-->
        <script type="text/javascript">
            $(function () {
                $('#btnNext').click(function () {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {objectId: $('#txtCompNo').val(), funName: 'tmpCmpInsert'}, function (j) {
                        document.getElementById("cmpSubmit").action = "<%=request.getContextPath()%>/PersonalDetails.jsp";
                        document.getElementById("cmpSubmit").submit();
            <%--?cmpId="+j.toString();-- if replicate entry is removed
            than uncomment this and append it above (j) contains cmpId--%>
                    });
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#chkRC').click(function () {
                    if ($('#chkRC').attr("checked")) {
                        $('#txtaCorpAddr').val($('#txtaRegAddr').val());
                        $('#txtCorpCity').val($('#txtRegCity').val());
                        //$('#txtCorpThana').val($('#txtRegThana').val());
                        $('#txtCorpPost').val($('#txtRegPost').val());
//                        alert($('#regPhoneSTD').val());
                        $('#corpPhoneSTD').val($('#regPhoneSTD').val());
                        $('#txtCorpPhone').val($('#txtRegPhone').val());
//                        alert($('#regFaxSTD').val());
                        $('#corpFaxSTD').val($('#regFaxSTD').val());
                        $('#txtCorpFax').val($('#txtRegFax').val());
                        $('#cmbCorpCountry').val($('#cmbRegCountry').val());
                        $('#corpPhoneCode').val($('#regPhoneCode').val());
                        $('#corpFaxCode').val($('#regFaxCode').val());
                        $('#corpMobCode').val($('#regMobCode').val());
                        //corpMobCode
                        $('#txtaCorpAddr').attr("readonly", "true");
                        $('#corpPhoneSTD').attr("readonly", "true");
                        $('#corpFaxSTD').attr("readonly", "true");
                        $('#txtCorpCity').attr("readonly", "true");
                        $('#txtCorpThana').attr("readonly", "true");
                        $('#txtCorpPost').attr("readonly", "true");
                        $('#corpPhoneCode').attr("readonly", "true");
                        $('#corpFaxCode').attr("readonly", "true");
                        $('#txtCorpPhone').attr("readonly", "true");
                        $('#txtCorpFax').attr("readonly", "true");
                        $('#txtcorpOffMobileNo').val($('#txtregOffMobileNo').val());
                       
                        //Code by Proshanto
                        if ($('#cmbRegCountry').val() != "150") {//136
                            $('#trCthana').css("display", "none")
                            $('#trCSubdistrict').css("display", "none")
                            
                        }
                        $('#cmbCorpState').html($('#cmbRegState').html());
                        for (var i = 0; i < document.getElementById('cmbRegState').options.length; i++) {
                            if (document.getElementById('cmbRegState').options[i].selected) {
                                document.getElementById('cmbCorpState').options[i].selected = "selected";
                            } else {
                                document.getElementById('cmbCorpState').options[i].disabled = "true";
                            }
                        }
                        $('#cmbCorpSubdistrict').html($('#cmbRegSubdistrict').html());
                        for (var i = 0; i < document.getElementById('cmbRegSubdistrict').options.length; i++) {
                            if (document.getElementById('cmbRegSubdistrict').options[i].selected) {
                                document.getElementById('cmbCorpSubdistrict').options[i].selected = "selected";
                            } else {
                                document.getElementById('cmbCorpSubdistrict').options[i].disabled = "true";
                            }
                        }
                        $('#txtCorpThana').html($('#txtRegThana').html());
                        for (var i = 0; i < document.getElementById('txtRegThana').options.length; i++) {
                            if (document.getElementById('txtRegThana').options[i].selected) {
                                document.getElementById('txtCorpThana').options[i].selected = "selected";
                            } else {
                                document.getElementById('txtCorpThana').options[i].disabled = "true";
                            }
                        }

                        for (var i = 0; i < document.getElementById('cmbRegCountry').options.length; i++) {
                            if (document.getElementById('cmbRegCountry').options[i].selected) {
                                document.getElementById('cmbCorpCountry').options[i].selected = "selected";
                            } else {
                                document.getElementById('cmbCorpCountry').options[i].disabled = "true";
                            }
                        }

                        if ($.trim($('#txtaRegAddr').val()) == "") {
                            $('#stxtaCorpAddr').html('Please enter Corporate Office Address');
                            bol = false;
                        } else
                        {
                            if (document.getElementById("txtaRegAddr").value.length > 150) {
                                $('#stxtaCorpAddr').html('Maximum 150 Characters are allowed');
                                bol = false;
                            } else
                            {
                                $('#stxtaCorpAddr').html('');
                            }
                        }


                        if ($.trim($('#txtRegCity').val()) == "") {
                            //$('#stxtCorpCity').html('Please enter City / Town');
                            //bol = false;
                            //return  true;
                        } else
                        {
                            if ($.trim(document.getElementById("txtRegCity").value.length) > 50) {
                                $('#stxtCorpCity').html('Maximum 50 Characters are allowed');
                                bol = false;
                            } else
                            {
                                if (!DigAndNum(document.getElementById("txtRegCity").value)) {
                                    $('#stxtCorpCity').html("Allows characters and Special characters (, . - '&) only");
                                } else {
                                    $('#stxtCorpCity').html('');
                                }
                            }
                        }

                        if ($.trim($('#regPhoneSTD').val()) == "") {
                            //$('#scorpPhoneSTD').html('Please enter Area Code.');
                            //bol = false;
                            //return true;
                        } else
                        {
                            if ($.trim(document.getElementById("regPhoneSTD").value.length) > 10) {
                                $('#scorpPhoneSTD').html('Maximum 10 Digits are allowed');
                                bol = false;
                            } else
                            {

                                if (!digits(document.getElementById("regPhoneSTD").value)) {
                                    $('#scorpPhoneSTD').html("Please enter Numbers only");
                                } else {
                                    $('#scorpPhoneSTD').html('');
                                }
                            }
                        }

                        if ($.trim($('#txtRegPhone').val()) == "") {
                            //$('#stxtCorpPhone').html('Please enter Phone No.');
                            //bol = false;
                        } else
                        {
                            if ($.trim(document.getElementById("txtRegPhone").value.length) > 20) {
                                $('#stxtCorpPhone').html('Maximum 20 Digits are allowed.');
                                bol = false;
                            } else
                            {

                                if (!digits(document.getElementById("txtRegPhone").value)) {
                                    $('#stxtCorpPhone').html("Please enter Numbers Only");
                                } else {
                                    $('#stxtCorpPhone').html('');
                                }
                            }
                        }

                        if ($.trim($('#txtRegThana').val()) == "") {
                            $('#spCthana').html("");
                        } else
                        {
                            $('#spCthana').html(null);
                        }



                    } else {
                        for (var i = 0; i < document.getElementById('cmbCorpCountry').options.length; i++) {
                            document.getElementById('cmbCorpCountry').options[i].disabled = false;
                            //Code by Proshanto
                            if (document.getElementById('cmbCorpCountry').options[i].value == "150") {//136
                                document.getElementById('cmbCorpCountry').options[i].selected = "selected";
                            }
                        }
                        //Code by Proshanto
                        onTickCountry("150");//136                        
                        $('#txtaCorpAddr').val(null);
                        $('#txtCorpCity').val(null);
                        $('#txtCorpThana').val(null);
                        $('#txtCorpPost').val(null);
                        $('#corpPhoneSTD').val(null);
                        $('#txtCorpPhone').val(null);
                        $('#corpFaxSTD').val(null);
                        $('#txtCorpFax').val(null);
                        $('#txtcorpOffMobileNo').val(null);
                        
                        $('#cmbCorpSubdistrict').val(null);
                        $('#txtaCorpAddr').removeAttr("readonly");
                        $('#cmbCorpSubdistrict').removeAttr("readonly");
                        $('#corpPhoneSTD').removeAttr("readonly");
                        $('#corpFaxSTD').removeAttr("readonly");
                        $('#txtCorpCity').removeAttr("readonly");
                        $('#txtCorpThana').removeAttr("readonly");
                        $('#txtCorpPost').removeAttr("readonly");
                        $('#corpPhoneCode').removeAttr("readonly");
                        $('#corpFaxCode').removeAttr("readonly");
                        $('#txtCorpPhone').removeAttr("readonly");
                        $('#txtCorpFax').removeAttr("readonly");
                        $('#txtcorpOffMobileNo').removeAttr("readonly");
                    }
                });
            });
            <%--For emptying thana msg on blur--%>
            $(function () {
                $('#txtRegThana').blur(function () {
                    if ($.trim($('#txtRegThana').val()) == "") {
                        //$('#spRthana').html("Please enter Gewog");
                        //return true;
                    } else
                    {
                        $('#spRthana').html(null);
                    }
                });

                $('#txtaCorpAddr').blur(function () {
                    if ($.trim($('#txtaCorpAddr').val()) == "") {
                        //$('#stxtaCorpAddr').html('Please enter Corporate Office Address');

                    } else
                    {
                        if (document.getElementById("txtaCorpAddr").value.length > 150) {
                            $('#stxtaCorpAddr').html('Maximum 150 Characters are allowed');

                        } else
                        {
                            $('#stxtaCorpAddr').html('');
                        }
                    }
                });

                $('#txtCorpCity').blur(function () {
                    if ($.trim($('#txtCorpCity').val()) == "") {
                        //$('#stxtCorpCity').html('Please enter City / Town');
                        //return true;

                    } else
                    {
                        if ($.trim(document.getElementById("txtCorpCity").value.length) > 50) {
                            $('#stxtCorpCity').html('Maximum 50 Characters are allowed');

                        } else
                        {
                            if (!DigAndNum(document.getElementById("txtCorpCity").value)) {
                                $('#stxtCorpCity').html("Allows characters and Special characters (, . - '&) only");
                            } else {
                                $('#stxtCorpCity').html('');
                            }
                        }
                    }
                });

                $('#corpPhoneSTD').blur(function () {
                    if ($.trim($('#corpPhoneSTD').val()) == "") {
                        //$('#scorpPhoneSTD').html('Please enter Area Code.');
//                        return true;

                    } else
                    {
                        if ($.trim(document.getElementById("corpPhoneSTD").value.length) > 10) {
                            $('#scorpPhoneSTD').html('Maximum 10 Digits are allowed');

                        } else
                        {

                            if (!digits(document.getElementById("corpPhoneSTD").value)) {
                                $('#scorpPhoneSTD').html("Please enter Numbers only");
                            } else {
                                $('#scorpPhoneSTD').html('');
                            }
                        }
                    }
                });

                $('#txtCorpPhone').blur(function () {
                    if ($.trim($('#txtCorpPhone').val()) == "") {
                        //$('#stxtCorpPhone').html('Please enter Phone No.');
//                        return true;

                    } else
                    {
                        if ($.trim(document.getElementById("txtCorpPhone").value.length) > 20) {
                            $('#stxtCorpPhone').html('Maximum 20 Digits are allowed.');

                        } else
                        {

                            if (!digits(document.getElementById("txtCorpPhone").value)) {
                                $('#stxtCorpPhone').html("Please enter Numbers Only");
                            } else {
                                $('#stxtCorpPhone').html('');
                            }
                        }
                    }
                });


                $('#corpFaxSTD').blur(function () {
                    if ($.trim(document.getElementById("corpFaxSTD").value).length != 0) {
                        if ($.trim(document.getElementById("corpFaxSTD").value.length) > 10) {
                            $('#scorpFaxSTD').html('Maximum 10 Digits are allowed.');

                        } else
                        {

                            if (!digits(document.getElementById("corpFaxSTD").value)) {
                                $('#scorpFaxSTD').html("Please enter Numbers Only");

                            } else {
                                $('#scorpFaxSTD').html('');
                            }
                        }
                    }
                });

                $('#txtCorpFax').blur(function () {
                    if ($.trim(document.getElementById("txtCorpFax").value).length != 0) {
                        if ($.trim(document.getElementById("txtCorpFax").value.length) > 20) {
                            $('#scorpFaxSTD').html('Maximum 20 Digits are allowed.');

                        } else
                        {

                            if (!digits(document.getElementById("txtCorpFax").value)) {
                                $('#scorpFaxSTD').html("Please enter Numbers Only");

                            } else {
                                $('#scorpFaxSTD').html('');
                            }
                        }
                    }
                });
            });
            $(function () {
                $('#txtCorpThana').blur(function () {
                    if ($.trim($('#txtCorpThana').val()) == "") {
                        //$('#spCthana').html("Please enter Gewog");
//                        return true;
                    } else
                    {
                        $('#spCthana').html(null);
                    }
                });
            });
            <%--that msg ends--%>
            $(function () {
                $('#cmbCorpCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId: $('#cmbCorpCountry').val(), funName: 'countryCode'}, function (j) {
                        $('#corpPhoneCode').val(j.toString());
                        $('#corpFaxCode').val(j.toString());
                        $('#corpMobCode').val(j.toString());
                    });
                });
            });
//            $(function () {
//                $('#cmbRegCountry').change(function () {
//                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId: $('#cmbRegCountry').val(), funName: 'countryCode'}, function (j) {
//                        $('#regPhoneCode').val(j.toString());
//                        $('#regFaxCode').val(j.toString());
//                        $('#regMobCode').val(j.toString());
//                        
//                    });
//                });
//            });
            
            $(function () {
                $('#cmbRegState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtRegThana').html(j);
                    });
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                        $('#cmbRegSubdistrict').html(j);
                    });
                    
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'AreaCodeSelection'}, function (j) {
                        $('#regPhoneSTD').val(j);
                        $('#regFaxSTD').val(j);
                    });
                });
            });
            $(function () {
                $('#cmbRegSubdistrict').change(function () {
                    if($('#cmbRegSubdistrict').val() == "--Select Dungkhag--")
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtRegThana').html(j);
                        });
                    }
                    else
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbRegSubdistrict').val(), funName: 'subDistrictComboValue4'}, function (j) {
                        $('#txtRegThana').html(j);
                        });
                    }
                });
            });
            
            $(function () {
                $('#cmbCorpState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtCorpThana').html(j);
                    });
                    
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                        $('#cmbCorpSubdistrict').html(j);
                    });
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'AreaCodeSelection'}, function (j) {
                        $('#corpPhoneSTD').val(j);
                        $('#corpFaxSTD').val(j);
                    });
                });
            });
            $(function () {
                $('#cmbCorpSubdistrict').change(function () {
                    if($('#cmbCorpSubdistrict').val() == "--Select Dungkhag--")
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtCorpThana').html(j);
                        });
                    }
                    else
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCorpSubdistrict').val(), funName: 'subDistrictComboValue4'}, function (j) {
                            $('#txtCorpThana').html(j);
                        });
                    }
                });
            });
            
            function loadCPVTree()
            {
                window.open('resources/common/CPVTree.jsp', 'CPVTree', 'menubar=0,scrollbars=1,width=700px');
            }
            $(function () {

                $("#dialog:ui-dialog").dialog("destroy");
                $("#dialog-form").dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: true,
                    height: 600,
                    width: 920,
                    modal: true,
                    close: function () {
                    }
                });
                
                
                $("#btnPreview").click(function () {
                    //document.getElementsById("#dialog-form").style.display = "block";
                    //$("#dialog-form").style.display("");
                    //alert();
                    $("#dialog-form").dialog("open");

                    $("#prevtxtCompName").text($("#txtCompName").val() == "" ? "N/A" : $("#txtCompName").val());
                    $("#prevcmbLegal").text($("#cmbLegal").val() == "" ? "N/A" : $("#cmbLegal option:selected").text());
                    $("#prevtxtComYear").text($("#txtComYear").val() == "" ? "N/A" : $("#txtComYear").val());
                    //var prevchkProcurementCategory = '';
                    //alert($("#txtCorpFax").val());
                    var procCategory = "";
                    if(document.getElementById('goodschk').checked) {
                        procCategory = "Goods";
                    }
                    if(document.getElementById('workschk').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + "Works:";
                    }
                    if(document.getElementById('largechkbox').checked) {
                        procCategory = procCategory + " Large-";
                    }
                    if(document.getElementById('w1largechk').checked) {
                        procCategory = procCategory + " W1";
                    }
                    if(document.getElementById('w2largechk').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + " W2";
                    }
                    if(document.getElementById('w3largechk').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + " W3";
                    }
                    if(document.getElementById('w4largechk').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + " W4";
                    }
                    if(document.getElementById('mediumchkbox').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + " Medium-";
                    }
                    if(document.getElementById('w1mediumchk').checked) {
                        procCategory = procCategory + " W1";
                    }
                    if(document.getElementById('w2mediumchk').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + " W2";
                    }
                    if(document.getElementById('w3mediumchk').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + " W3";
                    }
                    if(document.getElementById('w4mediumchk').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + " W4";
                    }
                    if(document.getElementById('smallchkbox').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + " Small-";
                    }
                    if(document.getElementById('w1smallchk').checked) {
                        procCategory = procCategory + " W1";
                    }
                    if(document.getElementById('w2smallchk').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + " W2";
                    }
                    if(document.getElementById('w3smallchk').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + " W3";
                    }
                    if(document.getElementById('w4smallchk').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + " W4";
                    }
                    if(document.getElementById('registeredchk').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + " Registered";
                    }
                    if(document.getElementById('serviceschk').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + " Service: ";
                    }
                    if(document.getElementById('consultingchk').checked) {
                        procCategory = procCategory + " Consulting";
                    }
                    if(document.getElementById('nonconsultingchk').checked) {
                        if(procCategory!="")
                        {
                            procCategory = procCategory + ",";
                        }
                        procCategory = procCategory + " Non-Consulting ";
                    }

                    $("#prevchkProcurementCategory").text(procCategory);

                    $("#prevcmbOrgCountry").text($("#cmbOrgCountry").val() == "" ? "N/A" : $("#cmbOrgCountry option:selected").text());
                    $("#prevtxtaRegAddr").text($("#txtaRegAddr").val() == "" ? "N/A" : $("#txtaRegAddr").val());
                    $("#prevcmbRegCountry").text($("#cmbRegCountry").val() == "" ? "N/A" : $("#cmbRegCountry option:selected").text());
                    $("#prevcmbRegState").text($("#cmbRegState").val() == "" ? "N/A" : $("#cmbRegState option:selected").text());
                    $("#prevcmbRegSub").text($("#cmbRegSubdistrict").val() == "--Select Dungkhag--" ? "N/A" : $("#cmbRegSubdistrict option:selected").val());
                    $("#prevtxtRegCity").text($("#txtRegCity").val() == "" ? "N/A" : $("#txtRegCity").val());
                    $("#prevtxtRegThana").text($("#txtRegThana").val() == "--Select Gewog--" ? "N/A" : $("#txtRegThana option:selected").val());
                    $("#prevtxtRegPost").text($("#txtRegPost").val() == "" ? "N/A" : $("#txtRegPost").val());
                    $("#prevtxtRegMobNo").text($("#txtregOffMobileNo").val() == "" ? "N/A" : $("#txtregOffMobileNo").val());
                    $("#prevregPhoneCode").text($("#txtRegPhone").val() == "" ? "N/A" : $("#txtRegPhone").val());
                    $("#prevregFaxCode").text($("#txtRegFax").val() == "" ? "N/A" : $("#txtRegFax").val());
                    //corporate
                    $("#prevtxtaCorpAddr").text($("#txtaCorpAddr").val() == "" ? "N/A" : $("#txtaCorpAddr").val());
                    $("#prevcmbCorpCountry").text($("#cmbCorpCountry").val() == "" ? "N/A" : $("#cmbCorpCountry option:selected").text());
                    $("#prevcmbCorpState").text($("#cmbCorpState").val() == "" ? "N/A" : $("#cmbCorpState option:selected").text());
                    $("#prevcmbCorpSub").text($("#cmbCorpSubdistrict").val() == "--Select Dungkhag--" ? "N/A" : $("#cmbCorpSubdistrict option:selected").val());
                    $("#prevtxtCorpCity").text($("#txtCorpCity").val() == "" ? "N/A" : $("#txtCorpCity").val());
                    $("#prevtxtCorpThana").text($("#txtCorpThana").val() == "--Select Gewog--" ? "N/A" : $("#txtCorpThana ").val());
                    $("#prevtxtCorpPost").text($("#txtCorpPost").val() == "" ? "N/A" : $("#txtCorpPost").val());
                    $("#prevtxtCorpMobNo").text($("#txtcorpOffMobileNo").val() == "" ? "N/A" : $("#txtcorpOffMobileNo").val());
                    $("#prevCorpPhoneCode").text($("#txtCorpPhone").val() == "" ? "N/A" : $("#txtCorpPhone").val());
                    $("#prevcorpFaxCode").text($("#txtCorpFax").val() == "" ? "N/A" : $("#txtCorpFax").val());
                    $("#prevCompanyWebsite").text($("#txtWeb").val() == "" ? "N/A" : $("#txtWeb").val());



                });
            });

            function ifEditdisablechkbox()
            {
                document.getElementById("goodschk").disabled = true;
            }

        </script>
    </head>
    <body onload="SubDistrictANDAreaCodeLoadOffice();SubDistrictANDAreaCodeLoadCorporation();">
        <form name="cmpSubmit" id="cmpSubmit" method="post"></form>
        <%
            boolean ifEdit = false;
            boolean ifCmpRegExist = false;
            String cmpName = null;
            boolean isEmailAlert = false, isSMSAlert = false, isBothAlert = false;
            int prefId=0;
            TblTempCompanyMaster dtBean = tendererMasterSrBean.getCompanyDetails(Integer.parseInt(session.getAttribute("userId").toString()));//Integer.parseInt(session.getAttribute("userId").toString())
            List<TblTempBiddingPermission> dtBeanBiddingPermission = tendererMasterSrBean.getBiddingPermission(Integer.parseInt(session.getAttribute("userId").toString()));
            if (dtBean != null) {
                /*cmpName=tendererMasterSrBean.cmpAlreadyExist(dtBean.getCompanyRegNumber());
                        if (cmpName!=null) {
                            ifCmpRegExist = true;
                        }*/
                ifEdit = true;
                String srtEmailAalert = "", strSmsAlert = "";
                
                UserPrefService userPrefService = (UserPrefService) AppContext.getSpringBean("UserPrefService");
                List<TblUserPrefrence> getdata = userPrefService.getemailsmsAlert(Integer.parseInt(session.getAttribute("userId").toString()));
                if (!getdata.isEmpty()) {
                    srtEmailAalert = getdata.get(0).getEmailAlert();
                    strSmsAlert = getdata.get(0).getSmsAlert();
                    prefId = getdata.get(0).getUserPrefId();
                }

                if (srtEmailAalert != null && !srtEmailAalert.isEmpty()) {
                    isEmailAlert = true;
                }
                if (strSmsAlert != null && !strSmsAlert.isEmpty()) {
                    isSMSAlert = true;
                }
                if (srtEmailAalert != null && !srtEmailAalert.isEmpty() && strSmsAlert != null && !strSmsAlert.isEmpty()) {
                    isBothAlert = true;
                    isEmailAlert = false;
                    isSMSAlert = false;
                }

            }
            if (ifEdit == true) {
                //ifEditdisablechkbox();
            }

        %>


        <div style="display: none" id="dialog-form" title="Electronic Procurement System (e-GP) Bidder Registration - Company Details">

            <div class="pageHead_1">Company Details Preview</div>

            <div style='padding:15px;'>
                <table border='0' cellspacing='0' cellpadding='0' class='formStyle_1 c_t_space' width='100%'>


                    <tr>
                        <td class='ff'>Company Name : </td>
                        <td id='prevtxtCompName'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>

                    <tr>
                        <td class='ff'>Company's Legal Status : </td>
                        <td id='prevcmbLegal'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Company's Establishment Year : </td>
                        <td id='prevtxtComYear'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Procurement Category : </td>
                        <td id='prevchkProcurementCategory'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Country of Origin : </td>
                        <td id='prevcmbOrgCountry'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>

                    <tr>
                        <td class='ff'>Address : </td>
                        <td id='prevtxtaRegAddr'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Country : </td>
                        <td id='prevcmbRegCountry'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Dzongkhag / District : </td>
                        <td id='prevcmbRegState'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Dungkhag / Sub-District : </td>
                        <td id='prevcmbRegSub'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Gewog : </td>
                        <td id='prevtxtRegThana'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>City / Town : </td>
                        <td id='prevtxtRegCity'></td>
                    </tr>
                    
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>

                    <tr>
                        <td class='ff'>Postcode : </td>
                        <td id='prevtxtRegPost'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Mobile No. : </td>
                        <td id='prevtxtRegMobNo'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>

                    <tr>
                        <td class='ff'>Phone No. : </td>
                        <td id='prevregPhoneCode'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Fax No. : </td>
                        <td id='prevregFaxCode'></td>
                    </tr>

                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>

                    <tr>
                        <td class='ff'>Corporate / Head Office Address : </td>
                        <td id='prevtxtaCorpAddr'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Country : </td>
                        <td id='prevcmbCorpCountry'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Dzongkhag / District : </td>
                        <td id='prevcmbCorpState'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Dungkhag / Sub-District : </td>
                        <td id='prevcmbCorpSub'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Gewog : </td>
                        <td id='prevtxtCorpThana'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>City / Town : </td>
                        <td id='prevtxtCorpCity'></td>
                    </tr>
                    
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>

                    <tr>
                        <td class='ff'>Post Code : </td>
                        <td id='prevtxtCorpPost'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Mobile No. : </td>
                        <td id='prevtxtCorpMobNo'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>

                    <tr>
                        <td class='ff'>Phone No. : </td>
                        <td id='prevCorpPhoneCode'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Fax No. : </td>
                        <td id='prevcorpFaxCode'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Company's Website  : </td>
                        <td id='prevCompanyWebsite'></td>
                    </tr>

                </table>

            </div>          

        </div>


        <div class="mainDiv">
            <input type="hidden" id="hdnerrorcount" name="hdnerrorcount" value="0"/>
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp"/>
                <!--Middle Content Table Start-->
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                        <td class="contentArea-Blogin"><div class="pageHead_1">Bidder Registration - Company Details</div>
                            <!--Page Content Start-->
                            <%@include file="EditNavigation.jsp"%>
                            <form id="frmComp" method="post" action="InsertCompanyDetails.jsp">
                                <%if (!ifCmpRegExist) {%>
                                <%if ("s".equals(request.getParameter("msg"))) {%><br/><div id="succMsg" class="responseMsg successMsg"  >Information successfully updated.</div><%}%>
                                <%if ("error".equals(request.getParameter("msg"))) {%><br/><div id="succMsg" class="responseMsg errorMsg"  >Problem in inserting Company Details.</div><%}%>
                                <%if ("fail".equals(request.getParameter("msg"))) {%><br/><div id="succMsg" class="responseMsg errorMsg"  >Problem in updating Company Details.</div><%}%>
                                <%if ("y".equals(request.getParameter("cpr"))) {%><br/><div class="responseMsg errorMsg">Company Registration No. already exist</div><%}%>
                                <%if (ifEdit) {%><input type="hidden" value="y" id="ifedit"/><%}%>
                                <input type="hidden" value="<%=DateUtils.formatStdDate(new java.util.Date())%>" name="currDate" id="hdnCurrDate"/>
                                <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1 c_t_space" id="tb2" width="100%">


                                    <tr>
                                        <td colspan="2" height="10" class="" align="right" style="font-style: italic">Bidders to register in English only. <br/>Fields marked with (<span class="mandatory">*</span>) are mandatory.
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="2" height="20"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span id="regAndLicensespan" class="reqF_1" ></span>
                                        </td>
                                    </tr>

                                </table>
                                <table border="0" cellspacing="5" cellpadding="0" class="formStyle_1"  id="tbodyHide"  width="100%">

                                    <tr>
                                        <td class="ff" width="30%" height="30">Company Name : <span>*</span></td>
                                        <td width="70%"><input name="companyName" type="text" class="formTxtBox_1" id="txtCompName" maxlength="200" style="width:200px;" title="Instruction to Bidders:<br/>

                                                               During registration, Bidders need to provide the following information:<br/>
                                                               *	Company Name (English - No Abbreviations)<br/>
                                                               *	Documents to be uploaded<br/>
                                                               Mandatory Documents:Self Declaration (For Company Owner), Power Of Attorney (For Company's Authorized User), Citizenship Identity Card, Trade License (All), Construction Development Board Certificate (Works only).<br/>

                                                               Optional Documents: Company Registration No., Tax Payment No. (TPN), Statutory Certificate No, Others/More<br/>
                                                               *	Organization Year of Establishment (YYYY)<br/>
                                                               *	Company URL<br/>
                                                               *	Name and address of Company Contact Person<br/> 

                                                               " onblur="ToUpperCase(this)" value="<%if (ifEdit) {
                                                                       out.print(dtBean.getCompanyName());
                                                               }%>"/><span id="comMsg" style="color: red;">&nbsp;</span></td>
                                    </tr>
                                    <tr style="display: none">
                                        <td class="ff" height="30">Company Name in Dzongkha : </td>
                                        <td><input name="banglaName" type="text" class="formTxtBox_1" id="txtCompBang" maxlength="300" style="width:200px;" 
                                                   value="<%if (ifEdit) {
                                                           if (dtBean.getCompanyNameInBangla() != null) {
                                                               out.print(BanglaNameUtils.getUTFString(dtBean.getCompanyNameInBangla()));
                                                           }
                                                       }%>" /></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" height="30">Company's Legal Status : <span>*</span></td>
                                        <td><select name="legalStatus" class="formTxtBox_1" id="cmbLegal" title="Company Details:  <br/><br/>'Public Limited Company' <br/>In case of Company, the name should be provided without any abbreviations. For example, different variations of 'Public Limited' viz. Pub Ltd, Public Ltd, Pvt Limited, P Ltd, P. Ltd., P. Ltd are not allowed.  It should be 'Public Limited' only.<br/><br/>'Private Limited Company' <br/>In case of Company, the name should be provided without any abbreviations. For example, different variations of 'Private Limited' viz. Pvt Ltd, Private Ltd, Pvt Limited, P Ltd, P. Ltd., P. Ltd are not allowed.  It should be 'Private Limited' only.<br/><br/>'Sole Proprietorship' <br/>In case of sole proprietorship concern, the proprietor should apply for E-GP Registration in his/her own name.">
                                                <%if (!ifEdit) {%><option  value="">Select Company Status</option><%}%>
                                                <option  value="public" <%if (ifEdit) {
                                                                 if (dtBean.getLegalStatus().equals("public")) {%>selected<%}
                                                                     }%>>Public Limited Company</option>
                                                <option value="private" <%if (ifEdit) {
                                                                if (dtBean.getLegalStatus().equals("private")) {%>selected<%}
                                                                    }%>>Private Limited Company</option>

                                                <option value="proprietor" <%if (ifEdit) {
                                                                if (dtBean.getLegalStatus().equals("proprietor")) {%>selected<%}
                                                                    }%>>Sole Proprietorship</option>
                                                <%--<option value="partnership" <%if (ifEdit) {
                                                                if (dtBean.getLegalStatus().equals("partnership")) {%>selected<%}
                                                                    }%>>Partnership</option>--%>
                                                <%--<option value="government" <%if (ifEdit) {
                                                        if (dtBean.getLegalStatus().equals("government")) {%>selected<%}
                                                                    }%>>Government Undertaking</option>--%>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" height="30">Company's
                                            Establishment Year : <span>*</span></td>
                                        <td><input name="establishmentYear" type="text" placeholder="Year e.g 2000" class="formTxtBox_1" id="txtComYear" style="width:200px;" value="<%if (ifEdit) {
                                                out.print(dtBean.getEstablishmentYear());
                                            }%>"/> </td>
                                    </tr>

                                    <tr>
                                        <td class="ff" height="30"> Procurement Category : <span>*</span><br/>
                                            <a href="#" class="InstructionLabel" title="During E-GP Registration Process Bidder can apply for Single or Multiple Procurement Categories. <br/> Bidder may register for Multiple Procurement Category during Registration by following all processes.   <br/>However, Bidder can apply for additional Procurement Categories at a later date by following all processes using the same Email ID

                                               <br/><br/><b>Select Procurement Category</b>

                                               <br/>For Goods select <b>Goods</b> and complete the Form.  Submit Documents as required
                                               <br/><br/>For Works select <b>Works</b> and select <b>Classification of Contractors</b>.  Accordingly select Works categories.   
                                               <ul>
                                               <br/>Large (w1,w2,w3,w4)
                                               <br/>Medium (w1,w2,w3,w4)
                                               <br/>Small (w1,w2,w3,w4)
                                               </ul>
                                               <br/>or <b>Registered:</b> if selected the all Large (W2), Medium (W2), Small (W2) will be auto selected. User can also select other options under Large, Medium, Small category
                                               Submit Documents as required
                                               <br/>

                                               <br/>For <b>Services</b> select <b>Consulting or Non consulting</b>" style="color: orange">Instruction</a>

                                        </td>
                                        <td style="color: black">

                                            <table width="100%" class="inner-table-new" style="padding: 10px;" cellspacing="0">

                                                <tr width="100%" height="30" >
                                                    <td width="10%" style="border-bottom:1px solid #CCC;">

                                                        <input name="ProcurementType1" value="Goods" type="checkbox" id="goodschk" onchange="hideprocurementmsg();"
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {

                                                                           if ("Goods".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getProcurementCategory())) {
                                                                               out.print("checked");

                                                                           }
                                                                       }
                                                                   }%>
                                                               title="test"/>&nbsp;Goods&nbsp;&nbsp;

                                                    </td>
                                                    <td width="30%" style="border-bottom:1px solid #CCC;">

                                                        <input name="ProcurementType2" value="Works"  type="checkbox"  id="workschk" onclick="chkWorks()" onchange="hideprocurementmsg();" 
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("Works".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getProcurementCategory())) {
                                                                               out.print("checked");
                                                                               break;
                                                                           }
                                                                       }
                                                                   }%>
                                                               />&nbsp;Works&nbsp;&nbsp;

                                                    </td>
                                                    <td width="20%" style="border-bottom:1px solid #CCC;">
                                                        <input name="ProcurementType3" value="Services" type="checkbox" id="serviceschk" onclick="chkSerivce()"  onchange="hideprocurementmsg();"
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("Services".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getProcurementCategory())) {
                                                                               out.print("checked");
                                                                               break;
                                                                           }
                                                                       }
                                                                   }%> 
                                                               />&nbsp;Services&nbsp;&nbsp;


                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td style="border-bottom:1px solid #CCC;padding: 10px;color:#888;border-left: 1px solid #CCC;border-right: 1px solid #CCC;">Classification of Contractor</td>
                                                    <td></td>
                                                </tr>       

                                                <tr id="largecategorytr">
                                                    <td class="ff" height="30"> </td>
                                                    <td style="color: black;padding: 10px 0px;border-left: 1px solid #CCC;border-right: 1px solid #CCC;">
                                                        &nbsp;&nbsp;
                                                        <input name="worktype1" value="Large" disabled type="checkbox"  id="largechkbox" onclick="chkLarge()"
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("Large".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");
                                                                               break;
                                                                           }
                                                                       }
                                                                   }%>/>
                                                        Large : &nbsp;
                                                        <input name="large0" value="W1"  disabled="true" type="checkbox" id="w1largechk" 
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("W1".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Large".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");

                                                                           }
                                                                       }
                                                               }%>/>  W1
                                                        <input name="large1" value="W2"  disabled="true" type="checkbox" id="w2largechk"  onchange="checkW2Category();"
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("W2".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Large".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");

                                                                           }
                                                                       }
                                                                   }%>/> W2
                                                        <input name="large2" value="W3"  disabled="true" type="checkbox" id="w3largechk" 
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("W3".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Large".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");

                                                                           }
                                                                       }
                                                                   }%>/> W3
                                                        <input name="large3" value="W4" disabled="true" type="checkbox" id="w4largechk" 
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("W4".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Large".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");

                                                                           }
                                                                       }
                                                                   }%>/> W4
                                                    </td> 
                                                    <td>

                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input name="servicetype0" value="Consulting" type="checkbox" id="consultingchk" 
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("Consulting".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");
                                                                           }
                                                                       }
                                                                   }%>/> Consulting &nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                                    </td>
                                                </tr>
                                                <tr id="mediumcategorytr">
                                                    <td class="ff" height="30"> </td>
                                                    <td style="color: black;border-left: 1px solid #CCC;border-right: 1px solid #CCC;">
                                                        &nbsp;&nbsp;
                                                        <input name="worktype2" disabled="true" value="Medium" type="checkbox"  id="mediumchkbox" onclick="chkMedium()"
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("Medium".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");
                                                                               break;
                                                                           }
                                                                       }
                                                                   }%>/>Medium  : 
                                                        <input name="medium0" value="W1" disabled="true" type="checkbox" id="w1mediumchk" 
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("W1".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Medium".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");

                                                                           }
                                                                       }
                                                                   }%>
                                                               /> W1
                                                        <input name="medium1" value="W2" disabled="true" type="checkbox" id="w2mediumchk"  onchange="checkW2Category();"
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("W2".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Medium".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");

                                                                           }
                                                                       }
                                                                   }%>/> W2
                                                        <input name="medium2" value="W3" disabled="true" type="checkbox" id="w3mediumchk" 
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("W3".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Medium".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");

                                                                           }
                                                                       }
                                                                   }%>/> W3
                                                        <input name="medium3" value="W4" disabled="true" type="checkbox" id="w4mediumchk" 
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("W4".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Medium".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");

                                                                           }
                                                                       }
                                                                   }%>/> W4
                                                    </td> 
                                                    <td>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input name="servicetype1" value="Non-Consulting" type="checkbox" id="nonconsultingchk"
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("Non-Consulting".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");
                                                                           }
                                                                       }
                                                                   }%>
                                                               /> Non-Consulting
                                                        <br/>

                                                    </td>
                                                </tr>
                                                <tr id="smallcategorytr">
                                                    <td class="ff" height="30"> </td>
                                                    <td style="color: black;border-left: 1px solid #CCC;border-right: 1px solid #CCC;">
                                                        &nbsp;&nbsp;
                                                        <input name="worktype3" value="Small" disabled="true" type="checkbox" id="smallchkbox" onclick="chkSmall()" 
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("Small".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");
                                                                               break;
                                                                           }
                                                                       }
                                                                   }%> />Small : &nbsp;&nbsp;&nbsp;
                                                        <input name="small0" value="W1" disabled="true" type="checkbox" id="w1smallchk" 
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("W1".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Small".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");

                                                                           }
                                                                       }
                                                                   }%>/> W1
                                                        <input name="small1" value="W2" disabled="true" type="checkbox" id="w2smallchk"  onchange="checkW2Category();"
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("W2".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Small".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");

                                                                           }
                                                                       }
                                                                   }%>/> W2
                                                        <input name="small2" value="W3" disabled="true" type="checkbox" id="w3smallchk" 
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("W3".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Small".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");

                                                                           }
                                                                       }
                                                                   }%>/> W3
                                                        <input name="small3" value="W4" disabled="true" type="checkbox" id="w4smallchk" 
                                                               <% if (ifEdit) {
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {
                                                                           if ("W4".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Small".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               out.print("checked");

                                                                           }
                                                                       }
                                                                   }%>/> W4
                                                    </td> 
                                                    <td></td>
                                                </tr>
                                                <tr id="registeredcategorytr" >
                                                    <td class="ff" height="30" style="border-bottom:1px solid #CCC"> </td>
                                                    <td style="border-bottom:1px solid #CCC;color: black;border-left: 1px solid #CCC;border-right: 1px solid #CCC;">
                                                        &nbsp;&nbsp;
                                                        <input name="Registered" value="Registered" disabled="true" type="checkbox" id="registeredchk" onchange="setRegisteredCategory(this);"
                                                               <% if (ifEdit) {
                                                                       int countOfW2 = 0;
                                                                       for (int i = 0; i < dtBeanBiddingPermission.size(); i++) {

                                                                           if ("W2".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Large".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               countOfW2++;
                                                                           }
                                                                           if ("W2".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Medium".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               countOfW2++;
                                                                           }
                                                                           if ("W2".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkCategroy()) && "Small".equalsIgnoreCase(dtBeanBiddingPermission.get(i).getWorkType())) {
                                                                               countOfW2++;
                                                                           }
                                                                           if (countOfW2 == 3) {
                                                                               out.print("checked");
                                                                               break;
                                                                           }
                                                                       }
                                                               %> disabled="true" <%}%>/> 
                                                        Registered

                                                    </td> 
                                                    <td style="border-bottom:1px solid #CCC"></td>
                                                </tr>



                                            </table>
                                        </td>  

                                    </tr>
                                    <tr>
                                        <td class="ff"></td>
                                        <td><span id="msgprocurementtype" style="color: red"></span></td>
                                    </tr>

                                    <tr>
                                        <td colspan="2" height="5"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="2" height="5"></td>
                                    </tr>

                                    <tr>
                                        <td colspan="2" height="5"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" height="30">Country of Origin : <span>*</span></td>
                                        <td><select name="originCountry" class="formTxtBox_1" id="cmbOrgCountry">
                                                <%
                                                    String originCountry = "Bhutan";
                                                    if (ifEdit) {
                                                        originCountry = dtBean.getOriginCountry();
                                                    }
                                                    for (SelectItem country : tendererMasterSrBean.getCountryList()) {
                                                %>
                                                <option  value="<%=country.getObjectId()%>" <%if (country.getObjectValue().equals(originCountry)) {%>selected<%}%>><%=country.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>


                                    <tr>
                                        <td class="ff">Registered Office Address : <span>*</span></td>
                                        <td><textarea cols="10" name="regOffAddress" rows="3" class="formTxtBox_1" id="txtaRegAddr" style="width:345px;" title=""><%if (ifEdit) {
                                                out.print(dtBean.getRegOffAddress());
                                            }%></textarea></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="5"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" height="30">Country : <span>*</span></td>
                                        <td><select name="regOffCountry" class="formTxtBox_1" id="cmbRegCountry">
                                                <%
                                                    String countryReg = "Bhutan";
                                                    if (ifEdit) {
                                                        countryReg = dtBean.getRegOffCountry();
                                                    }
                                                    for (SelectItem country : tendererMasterSrBean.getCountryList()) {
                                                %>
                                                <option  value="<%=country.getObjectId()%>" <%if (country.getObjectValue().equals(countryReg)) {%>selected<%}%>><%=country.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>

                                        <tr>
                                            <td class="ff" height="30">Dzongkhag / District : <span>*</span></td>
                                            <td><select name="regOffState" class="formTxtBox_1" id="cmbRegState"  style="width:218px;">
                                                    <%String regCntName = "";
                                                        if (ifEdit) {
                                                            regCntName = dtBean.getRegOffCountry();
                                                        } else {
                                                            regCntName = "Bhutan";
                                                        }
                                                        for (SelectItem state : tendererMasterSrBean.getStateList(regCntName)) {%>
                                                    <option value="<%=state.getObjectValue()%>" <%if (ifEdit) {
                                                            if (state.getObjectValue().equals(dtBean.getRegOffState())) {
                                                            %>selected<% System.out.print("if condition");
                                                                }
                                                                //Code by Proshanto,Change Thimphu,Instead of Dhaka
                                                            } else if (state.getObjectValue().equals("Thimphu")) {%>
                                                            selected<% System.out.print("else  condition");
                                                                }%>><%=state.getObjectValue()%></option>
                                                    <%}%>
                                                </select></td>
                                        </tr>


                                        <tr id="trRSubdistrict">
                                            <td class="ff" height="30">Dungkhag / Sub-district : </td>
                                            <td>
                                                <select name="regOffSubDistrict" class="formTxtBox_1" id="cmbRegSubdistrict"  style="width:218px;">
                                                    <%String regdistrictName = "";
                                                        if (ifEdit) {
                                                            regdistrictName = dtBean.getRegOffState();
                                                        } else {
                                                            regdistrictName = "Thimphu";
                                                        }
                                                        for (SelectItem subdistrict : tendererMasterSrBean.getSubdistrictList(regdistrictName)) {%>
                                                    <option value="<%=subdistrict.getObjectValue()%>" <%if (ifEdit) {
                                                            if (subdistrict.getObjectValue().equals(dtBean.getRegOffSubDistrict())) {
                                                            %>selected<%
                                                                }
                                                            }%>><%=subdistrict.getObjectValue()%></option>
                                                    <%}%>
                                                </select>
                                                
                                                
                                            
                                            </td>
                                        </tr>
                                        <tr id="trRthana">
                                            <td class="ff" height="30">Gewog : </td>
                                            <td><select name="regOffUpjilla" class="formTxtBox_1" id="txtRegThana" style="width:218px;">
                                                    <%String regState = "Thimphu";
                                                    String regSubdistrict = "";
                                                        if (ifEdit) {
                                                            regState = dtBean.getRegOffState();
                                                            regSubdistrict = dtBean.getRegOffSubDistrict();
                                                        }
                                                        if(!regState.isEmpty() && !regSubdistrict.isEmpty()){
                                                        for (SelectItem gewoglist : tendererMasterSrBean.getGewogList2(regSubdistrict)) {%>
                                                    <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                            if (gewoglist.getObjectValue().equals(dtBean.getRegOffUpjilla())) {
                                                            %>selected<%
                                                                }
                                                            } else if (gewoglist.getObjectValue().equals("")) {%>
                                                            selected<%
                                                                }%>><%=gewoglist.getObjectValue()%></option>
                                                    <%}}
                                                    else {
                                                        for (SelectItem gewoglist : tendererMasterSrBean.getGewogList(regState)) {%>
                                                    <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                            if (gewoglist.getObjectValue().equals(dtBean.getRegOffUpjilla())) {
                                                            %>selected<%
                                                                }
                                                            } %>
                                                            
                                                                ><%=gewoglist.getObjectValue()%></option>
                                                    <%}}
                                                    %>
                                                </select></td>
                                        </tr>



                                        <tr>
                                            <td class="ff" height="30">City / Town : </td>
                                            <td><input name="regOffCity" type="text" maxlength="50" class="formTxtBox_1" id="txtRegCity" style="width:200px;" 
                                                       value="<%if (ifEdit) {
                                                               if (dtBean.getRegOffCity() != null) {
                                                                   out.print(dtBean.getRegOffCity());
                                                               }
                                                           }
                                                       %>"/></td>
                                        </tr>
<!--                                        <tr>
                                            <td colspan="2" height="7"></td>
                                        </tr>-->
                                        <%--<tr id="trRthana" >
                                            <td class="ff">Gewog :</td>
                                            <td><input name="regOffUpjilla" type="text" maxlength="50" class="formTxtBox_1" id="txtRegThana" style="width:200px;"  
                                                       value="<%if (ifEdit) {
                                                               if (dtBean.getRegOffUpjilla() != null) {
                                                                   out.print(dtBean.getRegOffUpjilla());
                                                               }
                                                           }%>"
                                                       value="test"/><div id="spRthana" style="color: red;"></div></td>
                                        </tr>--%>
<!--                                        <tr id="trThanaHgt">
                                            <td colspan="2" height="7"></td>
                                        </tr>-->
                                        <%if (ifEdit) {
                                                if (!dtBean.getRegOffCountry().equalsIgnoreCase("Bhutan")) {%>
                                        <script type="text/javascript">
                                            $('#trtlid').css("display", "none");
                                            $('#trtled').css("display", "none");
                                            $('#trRthana').css("display", "none");
                                            $('#trRSubdistrict').css("display", "none");
                                            
                                            $('#trThanaHgt').css("display", "none");
                                        </script>
                                        <%}
                                            }%>
                                        <tr >
                                            <td class="ff" height="30">Postcode : <!--span>*</span--></td>
                                            <td><input name="regOffPostcode" type="text" maxlength="10" class="formTxtBox_1" id="txtRegPost" style="width:200px;"  
                                                       value="<%if (ifEdit) {
                                                               if (dtBean.getRegOffPostcode() != null) {
                                                                   out.print(dtBean.getRegOffPostcode());
                                                               }
                                                           }%>"
                                                       /></td>
                                        </tr>


                                        <tr>
                                            <td class="ff">Mobile No. : <span>*</span></td>
                                            <td>
                                                <input class="formTxtBox_1" style="width: 30px" id="regMobCode" value="<%CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                    if (ifEdit) {
                                                        out.print(commonService.countryCode(dtBean.getRegOffCountry(), false));
                                                    } else {
                                                        out.print(commonService.countryCode("Bhutan", false));
                                                    }%>" readonly/>-<input name="regOffMobileNo" type="text" class="formTxtBox_1" maxlength="20" id="txtregOffMobileNo" style="width:160px;"  value="<%if (ifEdit) {
                                                            out.print(dtBean.getRegOffMobileNo());
                                                        }%>"/>
                                                <span id="regmobMsg" style="color: red;">&nbsp;</span>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td class="ff" height="30">Phone No. : </td>
                                            <td><input class="formTxtBox_1" style="width: 30px" id="regPhoneCode" value="<%
                                                if (ifEdit) {

                                                    out.print(commonService.countryCode(dtBean.getRegOffCountry(), false));
                                                } else {
                                                    out.print(commonService.countryCode("Bhutan", false));
                                                }%>" readonly/>-<input type="text" value="<%if (ifEdit) {
                                                        if (dtBean.getRegOffPhoneNo() != null && !dtBean.getRegOffPhoneNo().isEmpty()) {
                                                            if (dtBean.getRegOffPhoneNo().split("-")[0] != null  && !dtBean.getRegOffPhoneNo().isEmpty()) {
                                                                out.print(dtBean.getRegOffPhoneNo().split("-")[0]);
                                                            }
                                                        }
                                                    }%>" id="regPhoneSTD" name="regPhoneSTD" class="formTxtBox_1" style="width:50px;"  />-<input name="regOffPhoneNo" placeholder="Phone No." type="text" maxlength="20" class="formTxtBox_1" id="txtRegPhone" style="width:100px;"  value="<%if (ifEdit) {
                                                            if (dtBean.getRegOffPhoneNo() != null  && !dtBean.getRegOffPhoneNo().isEmpty()) {
                                                                if (dtBean.getRegOffPhoneNo().split("-")[1] != null  && !dtBean.getRegOffPhoneNo().isEmpty()) {
                                                                    out.print(dtBean.getRegOffPhoneNo().split("-")[1]);
                                                                }
                                                            }
                                                        }%>"/>
                                                <input type="hidden" id="hdnvalue" name="hdnvalue" />
                                                <!--<span class="formNoteTxt" id="cey"> (Area Code - Phone No.)</span></td>-->
                                        </tr>
                                        <tr>
                                            <td class="ff" height="30">Fax No. : </td>
                                            <td><input class="formTxtBox_1" style="width: 30px" id="regFaxCode" value="<%
                                                if (ifEdit) {
                                                    out.print(commonService.countryCode(dtBean.getRegOffCountry(), false));
                                                } else {
                                                    out.print(commonService.countryCode("Bhutan", false));
                                                }%>" readonly/>-<input type="text" value="<%if (ifEdit) {
                                                        if (dtBean.getRegOffFaxNo() != null  && !dtBean.getRegOffFaxNo().isEmpty()) {
                                                            if (dtBean.getRegOffFaxNo().contains("-")) {
                                                                if (dtBean.getRegOffFaxNo().split("-")[0] != null  && !dtBean.getRegOffFaxNo().isEmpty()) {
                                                                    out.print(dtBean.getRegOffFaxNo().split("-")[0]);
                                                                }
                                                            }
                                                        }
                                                    }%>" id="regFaxSTD" name="regFaxSTD" class="formTxtBox_1" style="width:50px;"  />-<input name="regOffFaxNo" type="text" maxlength="20" class="formTxtBox_1" id="txtRegFax"  style="width:100px;"  value="<%if (ifEdit) {
                                                            if (dtBean.getRegOffFaxNo() != null && !dtBean.getRegOffFaxNo().isEmpty()) {
                                                                if (dtBean.getRegOffFaxNo().contains("-")) {
                                                                    if (dtBean.getRegOffFaxNo().split("-")[1] != null && !dtBean.getRegOffFaxNo().isEmpty()) {
                                                                        out.print(dtBean.getRegOffFaxNo().split("-")[1]);
                                                                    }
                                                                }
                                                            }
                                                        }%>"/></td>
                                        </tr>
                                        <tr>
                                            <td class="ff" height="30">&nbsp;</td>
                                            <td>
                                                <div class="formNoteTxt"><input name="chkRC" type="checkbox" id="chkRC" /> (Check if Registered and Corporate office details are same)</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" height="30">Corporate / Head office<br />
                                                Address : <span>*</span></td>
                                            <td><textarea cols="10" name="corpOffAddress" rows="3" class="formTxtBox_1" id="txtaCorpAddr" style="width:345px;"><%if (ifEdit) {
                                                    out.print(dtBean.getCorpOffAddress());
                                                }%></textarea>
                                                <span id="stxtaCorpAddr" class='reqF_1'></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" height="5"></td>
                                        </tr>
                                        <tr>
                                            <td class="ff" height="30">Country : <span>*</span></td>
                                            <td><select name="corpOffCountry" class="formTxtBox_1" id="cmbCorpCountry">
                                                    <%
                                                        String countryCorp = "Bhutan";
                                                        if (ifEdit) {
                                                            countryCorp = dtBean.getCorpOffCountry();
                                                        }
                                                        for (SelectItem country : tendererMasterSrBean.getCountryList()) {
                                                    %>
                                                    <option  value="<%=country.getObjectId()%>" <%if (country.getObjectValue().equals(countryCorp)) {%>selected<%}%>><%=country.getObjectValue()%></option>
                                                    <%}%>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <td class="ff" height="30">Dzongkhag / District : <span>*</span></td>
                                            <td><select name="corpOffState" class="formTxtBox_1" id="cmbCorpState" style="width:218px;">
                                                    <%String corpCntName = "";
                                                        if (ifEdit) {
                                                            corpCntName = dtBean.getCorpOffCountry();
                                                        } else {
                                                            corpCntName = "Bhutan";
                                                        }
                                                        for (SelectItem state : tendererMasterSrBean.getStateList(corpCntName)) {%>
                                                    <option value="<%=state.getObjectValue()%>" <%if (ifEdit) {
                                                            if (state.getObjectValue().equals(dtBean.getCorpOffState())) {%>selected<%}
                                                                //Code by Proshanto,Change Thimphu,Instead of Dhaka
                                                            } else if (state.getObjectValue().equals("Thimphu")) {%>selected<%}%>><%=state.getObjectValue()%></option>
                                                    <%}%>
                                                </select></td>
                                        </tr>

                                        <tr id="trCSubdistrict">
                                            <td class="ff" height="30">Dungkhag / Sub-district : </td>
                                            <td><select name="corpOffSubDistrict" class="formTxtBox_1" id="cmbCorpSubdistrict" style="width:218px;">
                                                    <%String corpStateName = "";
                                                        if (ifEdit) {
                                                            corpStateName = dtBean.getCorpOffState();
                                                        } else {
                                                            corpStateName = "Thimphu";
                                                        }
                                                        for (SelectItem subdistrict : tendererMasterSrBean.getSubdistrictList(corpStateName)) {%>
                                                    <option value="<%=subdistrict.getObjectValue()%>" <%if (ifEdit) {
                                                            if (subdistrict.getObjectValue().equals(dtBean.getCorpOffSubDistrict())) {
                                                            %>selected<%
                                                                }
                                                            }%>
                                                            <%
                                                                %>><%=subdistrict.getObjectValue()%></option>
                                                    <%}%>
                                                </select></td>
                                        </tr>
                                                
                                        <tr id="trCthana">
                                            <td class="ff" height="30">Gewog : </td>
                                            <td><select name="corpOffUpjilla" class="formTxtBox_1" id="txtCorpThana" style="width:218px;">
                                                    <%String corpState = "Thimphu";
                                                    String corpSubdistrict = "";
                                                        if (ifEdit) {
                                                            corpState = dtBean.getCorpOffState();
                                                            corpSubdistrict = dtBean.getCorpOffSubDistrict();
                                                        }
                                                        if(!corpState.isEmpty()&&!corpSubdistrict.isEmpty()){
                                                        for (SelectItem gewoglist : tendererMasterSrBean.getGewogList2(corpSubdistrict)) {%>
                                                    <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                            if (gewoglist.getObjectValue().equals(dtBean.getCorpOffUpjilla())) {
                                                            %>selected<%
                                                                }
                                                            } else if (gewoglist.getObjectValue().equals("")) {%>
                                                            selected<%
                                                                }%>><%=gewoglist.getObjectValue()%></option>
                                                    <%}}
                                                    else {
                                                        for (SelectItem gewoglist : tendererMasterSrBean.getGewogList(corpState)) {%>
                                                    <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                            if (gewoglist.getObjectValue().equals(dtBean.getCorpOffUpjilla())) {
                                                            %>selected<%
                                                                }
                                                            } %>
                                                            <%
                                                                %>><%=gewoglist.getObjectValue()%></option>
                                                    <%}}
                                                    %>
                                                </select></td>
                                        </tr>


                                        <tr>
                                            <td class="ff" height="30">City / Town : </td>
                                            <td><input name="corpOffCity" type="text" maxlength="50" class="formTxtBox_1" id="txtCorpCity" style="width:200px;"  
                                                       value="<%if (ifEdit) {
                                                               if (dtBean.getCorpOffCity() != null) {
                                                                   out.print(dtBean.getCorpOffCity());
                                                               }
                                                           }%>"/>
                                                <span id="stxtCorpCity" class='reqF_1'></span>
                                            </td>
                                        </tr>
                                        <%--<tr id="trCthana" height="30" >
                                            <td class="ff">Gewog : </td>
                                            <td><input name="corpOffUpjilla" type="text" maxlength="50" class="formTxtBox_1" id="txtCorpThana" style="width:200px;"  
                                                       value="<%if (ifEdit) {
                                                               if (dtBean.getCorpOffUpjilla() != null) {
                                                                   out.print(dtBean.getCorpOffUpjilla());
                                                               }
                                                           }%>"
                                                       value="test"/><div id="spCthana" style="color: red;"></div>
                                                <span id="stxtCorpThana" class='reqF_1'></span>
                                            </td>
                                        </tr>--%>
                                        <%if (ifEdit) {
                                                if (!dtBean.getCorpOffCountry().equalsIgnoreCase("Bhutan")) {%>
                                        <script type="text/javascript">
                                            $('#trCthana').css("display", "none");
                                            $('#trCSubdistrict').css("display", "none");
                                            
                                        </script>
                                        <%}
                                            }%>
                                        <tr >
                                            <td class="ff" height="30">Postcode : <!--span>*</span--></td>
                                            <td><input name="corpOffPostcode" type="text" maxlength="10" class="formTxtBox_1" id="txtCorpPost" style="width:200px;"   
                                                       value="<%if (ifEdit) {
                                                               if (dtBean.getCorpOffPostcode() != null) {
                                                                   out.print(dtBean.getCorpOffPostcode());
                                                               }
                                                           }%>"
                                                       /></td>
                                        </tr>

                                        <tr>
                                            <td class="ff">Mobile No. : <span>*</span></td>
                                            <td>
                                                <input class="formTxtBox_1" style="width: 30px" id="corpMobCode" value="<%
                                                    if (ifEdit) {
                                                        out.print(commonService.countryCode(dtBean.getRegOffCountry(), false));
                                                    } else {
                                                        out.print(commonService.countryCode("Bhutan", false));
                                                    }%>" readonly/>-<input name="corpOffMobMobileNo" type="text" class="formTxtBox_1" maxlength="20" id="txtcorpOffMobileNo" style="width:160px;"  value="<%if (ifEdit) {
                                                            out.print(dtBean.getCorpOffMobMobileNo());
                                                        }%>"/>
                                                <span id="corpmobMsg" style="color: red;">&nbsp;</span>
                                            </td>
                                        </tr>


                                        <tr>
                                            <td class="ff" height="30">Phone No. : </td>
                                            <td><input class="formTxtBox_1" style="width: 30px" id="corpPhoneCode" value="<%
                                                if (ifEdit) {
                                                    out.print(commonService.countryCode(dtBean.getCorpOffCountry(), false));
                                                } else {
                                                    out.print(commonService.countryCode("Bhutan", false));
                                                }%>" readonly/>-<input type="text" value="<%if (ifEdit) {
                                                        if (dtBean.getCorpOffPhoneno() != null && !dtBean.getCorpOffPhoneno().isEmpty()) {
                                                            if (dtBean.getCorpOffPhoneno().split("-")[0] != null && !dtBean.getCorpOffPhoneno().isEmpty()) {
                                                                out.print(dtBean.getCorpOffPhoneno().split("-")[0]);
                                                            }
                                                        }
                                                    }%>" id="corpPhoneSTD" name="corpPhoneSTD" class="formTxtBox_1" style="width:50px;"  />-<input name="corpOffPhoneno" placeholder="Phone No." type="text" maxlength="20" class="formTxtBox_1" id="txtCorpPhone"  style="width:100px;"   value="<%if (ifEdit) {
                                                            if (dtBean.getCorpOffPhoneno() != null && !dtBean.getCorpOffPhoneno().isEmpty()) {
                                                                if (dtBean.getCorpOffPhoneno().split("-")[1] != null && !dtBean.getCorpOffPhoneno().isEmpty()) {
                                                                    out.print(dtBean.getCorpOffPhoneno().split("-")[1]);
                                                                }
                                                            }
                                                        }%>"/>
                                                <input type="hidden" id="hdnvaluec" name="hdnvaluec" />
                                                <!--<span class="formNoteTxt" id="cey"> (Area Code - Phone No.)</span>-->
                                                <span id="scorpPhoneSTD" class='reqF_1'></span>
                                                <span id="stxtCorpPhone" class='reqF_1'></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" height="30">Fax No. : </td>
                                            <td><input class="formTxtBox_1" style="width: 30px" id="corpFaxCode" value="<%
                                                if (ifEdit) {
                                                    if (dtBean.getCorpOffCountry() != null) {
                                                        out.print(commonService.countryCode(dtBean.getCorpOffCountry(), false));
                                                    }
                                                } else {
                                                    out.print(commonService.countryCode("Bhutan", false));
                                                }%>" readonly/>-<input type="text" value="<%if (ifEdit) {
                                                    if (dtBean.getCorpOffFaxNo() != null && !dtBean.getCorpOffFaxNo().isEmpty()) {        
                                                        if (dtBean.getCorpOffFaxNo().contains("-")) {
                                                                if (dtBean.getCorpOffFaxNo().split("-")[0] != null && !dtBean.getCorpOffFaxNo().isEmpty()) {
                                                                    out.print(dtBean.getCorpOffFaxNo().split("-")[0]);
                                                                }
                                                            }
                                                        }
                                                    }%>" id="corpFaxSTD" name="corpFaxSTD" class="formTxtBox_1" style="width:50px;"  />-<input name="corpOffFaxNo" type="text" maxlength="20" class="formTxtBox_1" id="txtCorpFax"  style="width:100px;"  value="<%if (ifEdit) {
                                                        if (dtBean.getCorpOffFaxNo() != null && !dtBean.getCorpOffFaxNo().isEmpty()) {    
                                                            if (dtBean.getCorpOffFaxNo().contains("-")) {
                                                                    if (dtBean.getCorpOffFaxNo().split("-")[1] != null && !dtBean.getCorpOffFaxNo().isEmpty()) {
                                                                        out.print(dtBean.getCorpOffFaxNo().split("-")[1]);
                                                                    }
                                                                }
                                                            }
                                                        }%>"/>
                                                <span id="scorpFaxSTD" class='reqF_1'></span>
                                            </td>
                                        </tr>
                                        <!--                                    <tr>
                                                                                    <td class="ff" height="30">Nature of Business : <span>*</span></td>
                                                                                    <td><textarea cols="10"  name="specialization" rows="3" class="formTxtBox_1" id="txtaCPV" style="width:345px;" readonly><%/*if (ifEdit) {
                                                                                            out.print(dtBean.getSpecialization());
                                                                                        }*/%></textarea> <a href="javascript:void(0);" class="action-button-select" onclick="loadCPVTree()" id="aTree">Select Categories</a>
                                                                                        <div id="cpvTip">(Please select the relevant category in which you wish to get enlisted for)</div>
                                                                                    </td>
                                                                                </tr>-->
                                        <input type="hidden" name="specialization" class="formTxtBox_1" id="txtaCPV" value="disabled"/>

                                        <tr>
                                            <td class="ff" height="30">Notification/ Alert : <br/>
                                                <!--<a href="#" class="InstructionLabel" title="Notification / Alert will be sent to the e-mail or SMS or both">Instruction</a>-->
                                            </td>
                                            <td>
                                                <form>
                                                    <table style="width: 40%">
                                                        <tr>
                                                            <td><input type="radio" value="EmailAlert" id="EmailAlert" name="Alert"
                                                                       <% if(isEmailAlert) out.print("checked");
                                                                       if(!ifEdit) out.print("checked");%>  />&nbsp;Email
                                                            </td>
                                                            <td><input type="radio" value="SMSAlert" id="SMSAlert" name="Alert"  <% if(isSMSAlert) out.print("checked"); %>/>&nbsp;SMS
                                                            </td>
                                                            <td><input type="radio" value="BothAlert" id="BothAlert" name="Alert"  <% if(isBothAlert) out.print("checked"); %>/>&nbsp;Both
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </form>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="ff" height="30">Company's Website : </td>
                                            <td><input name="website" type="text" maxlength="100" class="formTxtBox_1" id="txtWeb" style="width:200px;"   value="<%if (ifEdit) {
                                                    out.print(dtBean.getWebsite());
                                                }%>"/>
                                                <span class="formNoteTxt" id="webnote">(Enter website name without 'http://' e.g. pppd.gov.bt)</span></td>
                                        </tr>
                                        <tr>
                                            <td height="30">&nbsp;</td>
                                            <td height="30">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td height="30">&nbsp;</td>
                                            <td>
                                                <%if (ifEdit) {%>
                                                <label class="formBtn_1">
                                                    <input type="button" name="preview" id="btnPreview" value="Preview"/>
                                                </label>
                                                <label class="formBtn_1">
                                                    <input type="submit" name="saveNEdit" id="btnUpdate" value="Update" onclick="return ValidateUpdate();" />
                                                </label>
                                                <input type="hidden" name="hdnsaveNEdit" id="hdnsaveNEdit" value="Update"/>
                                                <input type="hidden" name="userPrefId" id="userPrefId" value="<%= prefId %>"/>
                                                <%} else {%>
                                                <label class="formBtn_1">
                                                    <input type="button" name="preview" id="btnPreview" value="Preview"/>
                                                </label>
                                                <label class="formBtn_1">
                                                    <input type="submit" name="saveNEdit" id="btnSave" value="Save" onclick="ProcurementType();" />
                                                </label>
                                                <input type="hidden" name="hdnsaveNEdit" id="hdnsaveNEdit" value="Save"/>
                                                <%--<input type="hidden" name="userPrefId" id="userPrefId" value="<%= prefId %>"/>--%>
                                                <%}%>
                                            </td>
                                        </tr>
                                        <%--</tbody>
                                        </td>
                                                </tr>--%>

                                </table>
                                <%} else {%>
                                <br/>
                                <div class="responseMsg successMsg">Company with the name "<%=cmpName%>" is already approved by e-GP Admin so it can't be modified.</div>
                                <%}%>
                                <span id="redirect" style="display: none;">
                                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" id="tb2" width="100%">
                                        <tr>
                                            <td class="ff" width="20%">Company Name : </td>
                                            <td align="80%"><input name="cmpName" type="text" class="formTxtBox_1" id="txtCmpName" maxlength="30" style="width:200px;" readonly/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <label class="formBtn_1">
                                                    <%--<input type="hidden" id="hdnCmp"/>--%>
                                                    <span id="hdnCmp" style="display: none"></span>
                                                    <input type="button" name="next" id="btnNext" value="Next"/>

                                                </label>
                                            </td>
                                        </tr>
                                    </table>
                                </span>
                            </form>
                            <input type="hidden" id="currYear" value="<%
                                Date date = new Date();
                                String temp = date.toString();

                                out.println(temp.substring(temp.length() - 4, temp.length()));%>"/>
                            <!--Page Content End-->
                        </td>

                        <!--                        <td width="266" class="td-background">
                        <%--<jsp:include page="resources/common/Left.jsp" ></jsp:include>--%>
                        </td>
                    </tr>-->
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp"/>
            </div>
        </div>


        <script type="text/javascript">
            $(document).ready(function () {
                if (<%=ifEdit%>) {
                    if (document.getElementById('workschk').checked) {
                        EnableLMSR();
                        if (document.getElementById('largechkbox').checked) {
                            EnableLargeSubCat();
                        }
                        if (document.getElementById('mediumchkbox').checked) {
                            EnableMediumSubCat();
                        }
                        if (document.getElementById('smallchkbox').checked) {
                            EnableSmallSubCat();
                        }
                    }
                }
            });
        </script>
    </body>
    <script>
        function ToUpperCase(obj)
        {
            obj.value = obj.value.toUpperCase();
        }
    </script>
</html>