<%--
    Document   : OnlinePaymentVASSuc
    Created on : Mar 07, 2012, 4:12:01 PM
    Author     : Dixit
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
            
            CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            List<SPCommonSearchDataMore> listData = csdms.geteGPDataMore("getRegVASPaymentData",request.getParameter("tansId"));
            if (listData != null && !listData.isEmpty()) {
%>

<div class="responseMsg successMsg t_space" >
    Your transaction is successful. Transaction information is as below
</div>
<table border="0" cellspacing="20" cellpadding="20" class="tableList_1 c_t_space" id="tb2" width="100%">
    <tr>
        <td class="ff" width="25%">Payee Name: </td>
        <td><%=listData.get(0).getFieldName4()%></td>
    </tr>
    <tr>
        <td class="ff" width="25%">Payee Address: </td>
        <td><%=listData.get(0).getFieldName5()%></td>
    </tr>
    <tr>
        <td class="ff" width="25%">E-mail Id: </td>
        <td><%=listData.get(0).getFieldName1()%></td>
    </tr>
    <tr>
        <td class="ff" width="25%">
            Amount (in Nu.) :
        </td>
        <td><%=listData.get(0).getFieldName7()%></td>

    </tr>
    <tr>
        <td class="ff" width="25%">
            Service Charge (%) :
        </td>
        <td><%=listData.get(0).getFieldName8()%></td>

    </tr>
    <tr>
        <td class="ff" width="25%">
            Total Amount (in Nu.) :
        </td>
        <td><%=listData.get(0).getFieldName9()%></td>

    </tr>
     <tr>
        <td class="ff" width="25%">Remarks : </td>
        <td><%=listData.get(0).getFieldName6()%></td>
    </tr>
    <tr>
        <td class="ff" width="25%">
            Transaction Id :
        </td>
        <td><%=URLDecoder.decode(request.getParameter("tansId"), "UTF-8") %></td>
    </tr>
    <tr>
        <td class="ff" width="25%">
            Card Number :
        </td>
        <td><%=listData.get(0).getFieldName2()%></td>
    </tr>
    <tr>
        <td class="ff" width="25%">
            Payment Date Time :
        </td>
        <td><%=listData.get(0).getFieldName3()%></td>
    </tr>
    <tr>
        <td class="ff" width="25%">
            Online Payment Gateway :
        </td>
        <td><%=listData.get(0).getFieldName13() %></td>
    </tr>
</table>
<%}%>