<%--
    Document   MakeAdvertisement
    Created on : jan 27, 2013, 6:13:00 PM
    Author     : lakshmi
--%>

<%@page import="com.cptu.egp.eps.model.table.TblAdvtConfigurationMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.web.servicebean.LoginMasterSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Cache-Control", "no-store");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Advertisement - user Details</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="resources/js/jQuery/jquery.validate.js"></script>
        <link href="resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(function() {
                $('#btnReset').click(function() {
                    $("#txtduration").html("");
                });
                $('#frmUserReg').submit(function() {
                    var msg=true;
                    var fileName=($('#txtFile').val());
                    var ext = (fileName.substring(fileName.lastIndexOf('.'))).toLowerCase();
                    var formats=document.getElementById("txtfileformat").value;
                    var strForms=formats.split(",");
                    var msgformat="true";
                    for(var i=0;i<strForms.length;i++){
                        if(ext==strForms[i].toLowerCase() || (ext==".jpg" && strForms[i].toLowerCase()==".jpeg") )
                            msgformat="false";
                    }
                   
                    if($('#txtuser').val()==""){
                        $('span.#userMsg').css("color","red");
                        $('span.#userMsg').html("Please enter username");
                        msg= false;
                    }else if($.trim(document.getElementById("txtuser").value.length) >50){
                        $('span.#userMsg').html('Maximum 50 characters are allowed.');
                        msg= false;
                    }else if(digits(document.getElementById("txtuser").value)){
                        $('span.#userMsg').html("Please enter characters Only");
                        msg= false;
                    }
                    else  if ($('#txtuser').val()!=""){
                        $('span.#userMsg').html("");
              
                        if($('#txtlocation').val()=='select'){
                            $('span.#msglocationType').css("color","red");
                            $('span.#msglocationType').html("Please select location");
                            msg= false;
                        }
                         else if($('#txtlocation').val()!='select'){
                            $('span.#msglocationType').html("");
                            if($('#txtduration').val()=="")
                                {
                                     $('span.#msglocationType').css("color","red");
                            $('span.#msglocationType').html("Please select valid location");
                            msg= false;
                                }
                         else if($('#txtdimension').val()==""){

                            $('span.#msglocationType').css("color","red");
                            $('span.#msglocationType').html("Please select valid location");
                            msg= false;
                        }
                        
                        else if($('#txtsize').val()==""){

                            $('span.#msglocationType').css("color","red");
                            $('span.#msglocationType').html("Please select valid location");
                            msg= false;

                        }


                        else if($('#txtfileformat').val()==""){

                            $('span.#msglocationType').css("color","red");
                            $('span.#msglocationType').html("Please select valid location");
                            msg= false;
                        }

                        else if($('#txtprice').val()==""){

                            $('span.#msglocationType').css("color","red");
                            $('span.#msglocationType').html("Please select valid location");
                            msg= false;
                        }
                               else if($('#txtlocation').val()!=""){
                              $('span.#msglocationType').html("");
                            if($('#txtFile').val()==""){
                                $('span.#extMsg').css("color","red");
                                $('span.#extMsg').html("Please upload image");
                                msg= false;
                            }else if(msgformat=="true"){
                                $('span.#extMsg').css("color","red");
                                $('span.#extMsg').html("Please upload image in specified formats");
                                $('#txtFile').focus();
                                msg= false;
                            }
                            else if($('#txtFile').val()!="")
                            {
                                $('span.#extMsg').html("");
                    
                    
                                if($('#txtdesc').val()==""){
                                    $('span.#desclMsg').css("color","red");
                                    $('span.#desclMsg').html("Please enter description");
                                    msg= false;
                                }else
                                    if($.trim(document.getElementById("txtdesc").value.length) >200){
                                        $('span.#desclMsg').html('Maximum 200 characters are allowed.');
                                        msg= false;
                                    }
                                else if($('#txtdesc').val()!=""){
                                    $('span.#desclMsg').html("");
                                    if($('#txtaddress').val()==""){
                                        $('span.#addressMsg').css("color","red");
                                        $('span.#addressMsg').html("Please enter address");
                                        msg= false;
                                    }else if($.trim(document.getElementById("txtaddress").value.length) >250){
                                        $('span.#addressMsg').html('Maximum 250 characters are allowed.');
                                        msg= false;
                                    }
                                    else if($('#txtaddress').val()!=""){
                                        $('span.#addressMsg').html("");


                                        if($('#txtorg').val()==""){
                                            $('span.#orgMsg').css("color","red");
                                            $('span.#orgMsg').html("Please enter Organization");
                                            msg= false;
                                        }else
                                            if($.trim(document.getElementById("txtorg").value.length) >50){
                                                $('span.#orgMsg').html('Maximum 50 characters are allowed.');
                                                msg= false;
                                            }
                                        else if($('#txtorg').val()!=""){
                                            $('span.#orgMsg').html("");



                                            if($('#txtMailId').val()==""){
                                                $('span.#mailMsg').css("color","red");
                                                $('span.#mailMsg').html("Please enter e-mail ID");
                                                msg= false;
                                            }else if(!$('#txtMailId').val().match("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9\\-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"))
                                            {
                                                $('span.#mailMsg').css("color","red");
                                                $('span.#mailMsg').html("Please enter valid e-mail ID");
                                                msg= false
                                            }

                                            else if($('#txtMailId').val()!=""){
                                                $('span.#mailMsg').html("");
                                                if($('#txtPhone').val()==""){
                                                    $('span.#phoneMsg').css("color","red");
                                                    $('span.#phoneMsg').html("Please enter phone number");
                                                    msg= false;
                                                }else if($.trim(document.getElementById("txtPhone").value.length) >20){
                                                    $('span.#phoneMsg').html('Maximum 20 Digits are allowed.');
                                                    msg= false;
                                                }else if(!digits(document.getElementById("txtPhone").value)){
                                                    $('span.#phoneMsg').html("Please enter Numbers Only");
                                                    msg= false;
                                                }
                                                else if($('#txtPhone').val()!=""){
                                                    $('span.#phoneMsg').html("");


                                                    if($('#txturl').val()==""){
                                                        $('span.#urlMsg').css("color","red");
                                                        $('span.#urlMsg').html("Please enter url");
                                                        msg= false;
                                                    }
                                                    else if(!($('#txturl').val().match("^([http|https|HTTP|HTTPs][://][www.|WWW.]?[A-Za-z0-9]+[.][A-Za-z])|([www.|WWW.]?[A-Za-z0-9]+[.][A-Za-z])")))
                                                    {
                                                        $('span.#urlMsg').css("color","red");
                                                        $('span.#urlMsg').html("Please enter valid  url formate");
                                                        msg=false;
                                                    }else if($('#txturl').val()!=""){
                                                        $('span.#urlMsg').html("");
                                                    }

                                                }
                                            }
                                        }

                                    }

                                    }

                                }
                            }
                  
                         }
                     }

                   if(msg==false){

                        return false;
                    }
                });
            });
            function digits(value) {
                return /^\d+$/.test(value);
            }
            $(function() {
                $('#txtlocation').change(function() {
                    $.post("<%=request.getContextPath()%>/MakeAdvertisement", {funcName:'location',txtlocation:document.getElementById("txtlocation").value},  function(j){
                        var str=j.split("~");
                        $("select#txtduration").html(str[0]);
                        document.getElementById("txtdimension").value=str[1];
                        document.getElementById("txtsize").value=str[2];
                        document.getElementById("txtprice").value=str[4];
                        document.getElementById("txtfileformat").value=str[3];
                        $("#instructions").html("<span style='color : red'><b>Note: </b>"+str[5]+"</span>");

                    });
                });
                $('#txtduration').change(function() {
                    $.post("<%=request.getContextPath()%>/MakeAdvertisement", {funcName:'duration',adCId:document.getElementById("txtduration").value},  function(j){
                        var str=j.split("~");
                        document.getElementById("txtdimension").value=str[0];
                        document.getElementById("txtsize").value=str[2];
                        document.getElementById("txtprice").value=str[1];
                        document.getElementById("txtfileformat").value=str[3];
                        $("#instructions").html("<span style='color : red'><b>Note: </b>"+str[4]+"</span>");
                    });
                });
            });
        </script>
                       
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td width="250"  valign="top">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                        </td>
                        <td class="contentArea-Blogin">
                            <!--Page Content Start-->
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="pageHead_1">
                                <tr>
                                    <td align="left">Advertisement - User Details</td>
                                </tr>
                            </table>
                            <form id="frmUserReg" name="frmUserReg"  action="../MakeAdvertisement?funcName=view" method="post" enctype="multipart/form-data" enctype="multipart/form-data">
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="ff" align="left">
                                            <%
                                                        if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("create")) {%>
                                            <div align="left" id="sucMsg" class="responseMsg successMsg">Advertisement posted successfully</div><br/>
                                            <% }%>
                                            Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">User Name: <span>*</span></td>
                                        <td width="80%"><input name="txtuser" type="text" class="formTxtBox_1" id="txtuser" style="width:200px;" />
                                            <br/> <span id="userMsg" style="color: red; ">&nbsp;</span></td>
                                    </tr>


                                    <tr>
                                        <td class="ff" width="20%"> Location: <span>*</span></td>
                                        <td>
                                            <select id="txtlocation" name="txtlocation" class="formTxtBox_1" style="width:200px;" >
                                                <option value="select">select</option>
                                                <option value="Bottom">Bottom</option>
                                                <option value="TopLeft">TopLeft</option>
                                            </select> <br></br>
                                            <span style="color: red;" id="msglocationType"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Duration : <span id="spanduration" name="spanduration" style="visibility:hidden">*</span></td>
                                        <td>
                                            <select class="formTxtBox_1" name="txtduration" id="txtduration" style="width:200px;" >
                                            </select>
                                            <span id="errdurationMsg" style="color: red;"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">Dimension <span>*</span></td>
                                        <td width="80%"><input  name="txtdimension" type="text"  readonly class="formTxtBox_1" id="txtdimension" value="" style="width:200px;" />
                                            <br/> <span id="dimensionMsg" style="color: red; ">&nbsp;</span></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">Size (in KB)<span>*</span></td>
                                        <td width="80%"><input name="txtsize" type="text" readonly class="formTxtBox_1" id="txtsize" value="" style="width:200px;" />
                                            <br/> <span id="sizeMsg" style="color: red; ">&nbsp;</span></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">File Format: <span>*</span></td>
                                        <td width="80%"><input name="txtfileformat"  type="text" readonly class="formTxtBox_1" id="txtfileformat" value="" style="width:200px;" />
                                            <br/> <span id="formatMsg" style="color: red; ">&nbsp;</span></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">Price (in Nu.): <span>*</span></td>
                                        <td width="80%"><input name="txtprice" type="text" readonly class="formTxtBox_1" id="txtprice"value="" style="width:200px;" />
                                            <br/> <span id="priceMsg" style="color: red; ">&nbsp;</span></td>
                                    </tr>
                                    <tr>
                                        <td width="35%"><span class="ff">Select the design/banner to upload :<span>*</span> </span></td>
                                        <td width="10%">
                                            <input type="file" name="txtFile" id="txtFile"  class="formTxtBox_1" style="background-color: #fff;" />&nbsp;&nbsp;<span style="color: red; text-align: left;" id="extMsg"></span><br />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff" width="20%">Description: <span>*</span></td>
                                        <td width="80%"><input name="txtdesc" type="text" class="formTxtBox_1" id="txtdesc" style="width:200px;" />
                                            <br/><span id="desclMsg" style="color: red; ">&nbsp;</span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff" width="20%">Address: <span>*</span></td>
                                        <td><textarea name="txtaddress" rows="3" class="formTxtBox_1" id="txtaddress"" style="width:200px;" ></textarea>
                                            <br/><span id="addressMsg"  class="reqF_1" style="color: red; "></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">Organization: <span>*</span></td>
                                        <td width="80%"><input name="txtorg" type="text" class="formTxtBox_1" id="txtorg" style="width:200px;" />
                                            <br/><span id="orgMsg" style="color: red; ">&nbsp;</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">e-mail ID: <span>*</span></td>
                                        <td width="80%"><input name="txtMailId" type="text" class="formTxtBox_1" id="txtMailId" style="width:200px;" /> <span id="emailnote" class="formNoteTxt"><br/>(e-mail ID should be valid. Example: xyz@gmail.com)</span>
                                            <br/> <span id="mailMsg" style="color: red; ">&nbsp;</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Phone Number: <span>*</span></td>
                                        <td><input name="txtPhone" type="telephoneId" class="formTxtBox_1" id="txtPhone" style="width:200px;" />
                                            <br/><span id="phoneMsg" style="color: red; ">&nbsp;</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">URL <span>*</span></td>
                                        <td width="80%"><input  name="txturl" type="text" class="formTxtBox_1" id="txturl" style="width:200px;" /><span id="urlnote" class="formNoteTxt"><br/>(url should be valid. Example: http://www.xyz.com)</span>
                                            <br/><span id="urlMsg" style="color: red; ">&nbsp;</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><label class="formBtn_1">
                                                <input type="submit" name="button" id="btnSubmit" value="Submit" />
                                            </label>
                                            &nbsp;
                                            <label class="formBtn_1">
                                                <input type="reset" name="Reset" id="btnReset" value="Reset" />
                                            </label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"> &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div id ="instructions" name="instructions"></div>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>