<%--
    Document   : OpeningReports
    Created on : Dec 18, 2010, 6:20:42 PM
    Author     : Rikin
--%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>

<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bid Opening Information</title>
        <%String contextPath = request.getContextPath();%>
        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%
                 HttpSession hs = request.getSession();
                 
                  if(hs==null){
                      hs = request.getSession();
                      String id="0";
                      hs.setAttribute("userId",id);
                  }
                    String userId = "";
                    boolean isPackage = false;
                    boolean isLotId = false;
                    String strLotId = "";
                    boolean isLot = false;

                    TenderCommonService tenderCommonService1 = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                     TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");


                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = session.getAttribute("userId").toString();
                        commonService.setUserId(userId);
                        tenderCommonService1.setLogUserId(userId);
                        commonSearchDataMoreService.setLogUserId(userId);
                    }

                    String tenderId = "";
                    if (request.getParameter("tenderId") != null) {
                        tenderId = request.getParameter("tenderId");
                        pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    }

                    if (request.getParameter("lotId") != null) {
                       if (!"0".equalsIgnoreCase(request.getParameter("lotId")) && !"".equalsIgnoreCase(request.getParameter("lotId")) && !"null".equalsIgnoreCase(request.getParameter("lotId"))){
                            isLotId = true;
                            strLotId=request.getParameter("lotId");
                       } else {
                            isPackage = true;
                       }
                    }

                    String procureNature = "";
                    for (CommonTenderDetails commonTenderDetails : tenderSrBean.getAPPDetails(Integer.parseInt(tenderId), "tender")) {
                        procureNature = commonTenderDetails.getProcurementNature();
                    }

                    List<Object[]> listPackage =  new ArrayList<Object[]>();
                    List<Object[]> lotlist =  new ArrayList<Object[]>();

                    boolean is2Env = false;
                    List<SPCommonSearchDataMore> envDataMores = commonSearchDataMoreService.geteGPData("GetTenderEnvCount", tenderId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                    if(envDataMores!=null && (!envDataMores.isEmpty()) && envDataMores.get(0).getFieldName1().equals("2")){
                        is2Env = true;
                    }
                %>
               
                <div class="contentArea_1">
                    <div class="pageHead_1">
                        Opening  Information                        
                    </div>
                    <div  id="print_area">
                        <div class="t_space">&nbsp;</div>
                        <div class="tabPanelArea_1">
                        <%  listPackage = commonService.getPkgDetialByTenderId(Integer.parseInt(tenderId));
                            if(isPackage){
                                lotlist = commonService.getLotDetailsByTenderIdforEval(tenderId);
                            %>
                            <table width="100%" cellspacing="0" class="tableList_1">
                            <%
                                for(Object[] pkg:listPackage){
                            %>
                                    <tr>
                                        <td width="22%" class="t-align-left ff">Package No. :</td>
                                        <td width="78%" class="t-align-left"><%=pkg[0].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Package Description :</td>
                                        <td class="t-align-left"><%=pkg[1].toString()%></td>
                                    </tr>
                            <%
                                }
                            %>
                            </table>
                            <div class="t_space">&nbsp;</div>
                             <% if(!is2Env){ %>

                             <%
                                for(Object[] lot:lotlist){
                             %>
                             <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <td width="22%" class="t-align-left ff">Lot No. :</td>
                                        <td width="78%" class="t-align-left"><%=lot[0].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Lot Description :</td>
                                        <td class="t-align-left"><%=lot[1].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Opening Report 1</td>
                                        <td class="t-align-left"><a href="<%=request.getContextPath()%>/TOR1.jsp?tenderid=<%=tenderId%>&lotId=<%=lot[2].toString()%>&frm=eval">View</a></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Opening Report 2</td>
                                        <td class="t-align-left"><a href="<%=request.getContextPath()%>/TOR2.jsp?tenderid=<%=tenderId%>&lotId=<%=lot[2].toString()%>&frm=eval">View</a></td>
                                    </tr>
                             </table>
                             
                             <%
                                 }
                               }
                             %>
                            <% 
                           }
                           else
                           {
                        %>                            
                            <table width="100%" cellspacing="0" class="tableList_1">
                                <tbody>
                                <%
                                    for(Object[] pkg:listPackage){
                                %>
                                    <tr>
                                        <td width="22%" class="t-align-left ff">Package No. :</td>
                                        <td width="78%" class="t-align-left"><%=pkg[0].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Package Description :</td>
                                        <td class="t-align-left"><%=pkg[1].toString()%></td>
                                    </tr>
                                <%
                                    }
                                %>
                                </tbody>
                            </table>
                            <table width="100%" cellspacing="0" class="tableList_1">
                            <%
                                lotlist = commonService.getLotDetailsByPkgLotId(strLotId,tenderId);
                                for(Object[] lot:lotlist){
                             %>
                                    <tr>
                                        <td width="22%" class="t-align-left ff">Lot No. :</td>
                                        <td width="78%" class="t-align-left"><%=lot[0].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">Lot Description :</td>
                                        <td class="t-align-left"><%=lot[1].toString()%></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">TOR1</td>
                                        <td class="t-align-left"><a href="<%=request.getContextPath()%>/TOR1.jsp?tenderid=<%=tenderId%>&lotId=<%=strLotId%>&frm=eval">View</a></td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-left ff">TOR2</td>
                                        <td class="t-align-left"><a href="<%=request.getContextPath()%>/TOR2.jsp?tenderid=<%=tenderId%>&lotId=<%=strLotId%>&frm=eval">View</a></td>
                                    </tr>
                                </tbody>
                             </table>
                            
                             <% }                               
                          }
                        %>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
