<%-- 
    Document   : ePaymentSuccessful
    Created on : Feb 4, 2012, 1:56:06 PM
    Author     : darshan
--%>

<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.util.Enumeration"%>
<%@page import="com.cptu.egp.eps.web.databean.OnlinePaymentDtBean"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.io.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%
/*
            String strResultMsg = "";
            String strResultCode = "";
            String strTranID = "";
            String strResponseMsg="";
            boolean onlinePaymentStatusSuccess = false;
            int onlineId = 0;
*/
            String stripAddress = request.getHeader("X-FORWARDED-FOR");

            OnlinePaymentDtBean opdb = (OnlinePaymentDtBean)session.getAttribute("payment");
            if (stripAddress == null) {
                stripAddress = request.getRemoteAddr();
            }

            if (request.getParameter("trans_id") != null) {

/*
                strTranID = request.getParameter("trans_id");
                opdb = null;                

                try {
                    String strExecCMD = "java -jar C:/paymentGatewayTest/ecomm_merchant.jar  C:/paymentGatewayTest/merchant.properties -c " +  strTranID +" "+stripAddress;

                    Writer output = null;
                    File file = new File("C:/paymentGatewayTest/paymetResult.cmd");
                    if (file.exists()) {
                        file.delete();
                    }
                    file.createNewFile();
                    output = new BufferedWriter(new FileWriter(file));
                    output.write(strExecCMD);
                    output.close();

                    Process p = Runtime.getRuntime().exec("C:/paymentGatewayTest/paymetResult.cmd");
                    BufferedReader brSucc = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    BufferedReader brFail = new BufferedReader(new InputStreamReader(p.getErrorStream()));

                    /*Success reponse from Process*/
                    /*try {
                        while ((strTranID = brSucc.readLine()) != null) {
                            // System.out.println("Sucess strTranID = " + strTranID+" strTranID.indexOf(TRANSACTION_ID)  "+strTranID.startsWith("TRANSACTION_ID") );
                            if (strTranID.indexOf("RESULT_CODE") >= 0) {
                                strResultCode = strTranID.substring(12);
                            }
                            if (strTranID.indexOf("RESULT") >= 0) {
                                strResultMsg = strTranID.substring(8);
                            }
                            if(!strResultCode.equals("") && !strResultMsg.equals(""))
                            {
                                break;
                            }
                        }
                        System.out.println("Result Code = " + strResultCode+" Message "+strResultMsg);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    brSucc.close();

                    /*Fail reponse from Process*/
                    /*if(strResultCode.equals("") && strResultMsg.equals("")){
                        try {
                            while ((strTranID = brFail.readLine()) != null) {
                                    onlinePaymentStatusSuccess = false;
                            }
                        } catch (IOException e) {
                            onlinePaymentStatusSuccess = false;
                            e.printStackTrace();
                        }
                        brFail.close();
                    }
                } catch (IOException e1) {
                    onlinePaymentStatusSuccess = false;
                    e1.printStackTrace();
                }
                   
                /*Final response recirect page on base of "onlinePaymentStatusSuccess"*/
               /*if(strResultCode.equalsIgnoreCase("000") && strResultMsg.equalsIgnoreCase("OK")) {
                    //response.sendRedirect("https://ecomtest.dutchbanglabank.com/ecomm2/ClientHandler?trans_id=" + strTranID);
                   strResponseMsg = "Your transaction is successfull <br /><p>=====================::TRANSACTION RESULT::=======================</p>";
                   strResponseMsg += " <table border='0' cellspacing='10' cellpadding='0' class='formStyle_1' width='100%'><tr><td width=156>$outputArray[0]</td> </tr><tr>    <td>$outputArray[1]</td>  </tr>  <tr>    <td>$outputArray[2]</td>  </tr>  <tr>    <td>$outputArray[3]</td>  </tr>  <tr>    <td>$outputArray[4]</td>  </tr>  <tr><td>$outputArray[5]</td> </tr></table>";
                   onlinePaymentStatusSuccess = true;
                } else {
                    //response.sendRedirect(request.getContextPath()+"/tenderer/merchant_client.jsp?er=1");
                    strResponseMsg = "Your transaction is failed. <a href='/tenderer/merchant_client.jsp'>Try again</a> or go for offline payment.";
                }
            } else {
                   strResponseMsg ="There is no response from bank kindly contect to Dutch Bangla bank.";
            }
            opdb.setOnlineTraId("strTranID");
            opdb.setOnlineMsg("strResultMsg");
            opdb.setOnlineResp("strResultCode");
            session.removeAttribute("payment");
            session.setAttribute("payment", opdb);
*/
            if (request.getParameter("trans_id") != null) {
                String requestFrom ="";
                String strTrnsID = request.getParameter("trans_id");

                CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> listData = csdms.geteGPDataMore("getRegVASPaymentData", request.getParameter("trans_id"));
                if (listData != null && !listData.isEmpty() && listData.get(0).getFieldName11() != null && (listData.get(0).getFieldName11().equalsIgnoreCase("fromvas"))) {
                    System.out.print("fromvas"+requestFrom);
                     OnlinePaymentDtBean onlinePaymentDtBean = new OnlinePaymentDtBean(listData.get(0).getFieldName1(), listData.get(0).getFieldName7(), "fromvas", "0", listData.get(0).getFieldName8());
                     onlinePaymentDtBean.setOnlineTraId(strTrnsID);
                     onlinePaymentDtBean.setOnlineId(Integer.parseInt(listData.get(0).getFieldName12()));
                     request.setAttribute("vasPayment", onlinePaymentDtBean);
                     requestFrom ="&reqFrom=regVasFeePayment";
                     System.out.print("requestFrom:"+requestFrom);
                    RequestDispatcher dispatcher = request.getRequestDispatcher(request.getContextPath()+"/OnlinePaymentServlet?action=update"+requestFrom);
                    if (dispatcher != null){
                        dispatcher.forward(request, response);
                    }
                }else{
                       System.out.print("requestFrom:"+requestFrom);
                       response.sendRedirect(request.getContextPath()+"/OnlinePaymentServlet?action=update"+requestFrom);
                }
            }
    }
%>
<%--<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <title>Online Payment Response</title>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="../resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td width="250"  valign="top">
                            <jsp:include page="../resources/common/Left.jsp" ></jsp:include>
                        </td>
                        <td class="contentArea-Blogin">
                            <!--Page Content Start-->
                             <div class="t_space">
                            </div>
                            <table width="100%" cellpadding="0" cellspacing="0" border="0" class="pageHead_1">
                                <tr>
                                    <td align="left">Online Payment Response</td>
                                    <td align="right" valign="middle"><a href="<%=request.getContextPath()%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1] %>" target="_blank" ><img src="<%=request.getContextPath()%>/resources/images/Dashboard/helpIcon.png" /></a></td>
                                    <td width="40" align="left" valign="middle"><a href="<%=request.getContextPath()%>/resources/common/Help.jsp?pageName=<%=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1] %>" target="_blank" style="color: #333;" > &nbsp;Help</a></td>
                                </tr>
                            </table>
                                <%=strResponseMsg%>
                                <%=strTranID%>
                            <!--<form name="fr_merchant" action="merchant_client.jsp"  method="post" onSubmit="return checkForm();"><br>
                               <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">Amount: <span>*</span></td>
                                        <td width="80%"><input type="text" maxlength="12" name="Amount" class="formTxtBox_1"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">Description: <span>*</span></td>
                                        <td width="80%"><input type="text" maxlength="125" name="Desc" class="formTxtBox_1"></td>
                                    </tr>

                                    <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                               <label class="formBtn_1">
                                                    <input type="submit"  value="Submit"  onClick="return checkForm();" name="btnSubmit" id="btnSubmit"/>
                                               </label>
                                           </td>
                                    </tr>
                                </table>
                            </form>-->
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>--%>