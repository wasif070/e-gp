<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link href="Include/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="Include/theme_1.css" rel="stylesheet" type="text/css" />
<!--<script type="text/javascript" src="Include/pngFix.js"></script>-->
</head>
<body>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <div class="topHeader">
    <table width="100%" cellspacing="0">
      <tr valign="top">
        <td><div class="dash_menu_1">
            <ul>
              <li class="mSel"><a href="#"><img src="Images/Dashboard/msgBoxIcn.png" />Message Box</a></li>
              <li><a href="#"><img src="Images/Dashboard/appIcn.png" />APP</a></li>
              <li><a href="#"><img src="Images/Dashboard/tenderIcn.png" />Tender</a></li>
              <li><a href="#"><img src="Images/Dashboard/committeeIcn.png" />Evaluation</a></li>
              <li><a href="#"><img src="Images/Dashboard/reportIcn.png" />Report</a></li>
              <li><a href="#"><img src="Images/Dashboard/myAccountIcn.png" />My Account</a></li>
              <li><a href="#"><img src="Images/Dashboard/helpIcn.png" />Help</a></li>
            </ul>
          </div>
          <table width="100%" cellspacing="6" class="loginInfoBar">
            <tr>
              <td align="left"><b>Friday 27/08/2010 21:45</b></td>
              <td align="center"><b>Last Login :</b> Friday 27/08/2010 21:45</td>
              <td align="right"><img src="Images/Dashboard/userIcn.png" class="linkIcon_1" /><b>Welcome,</b> User   &nbsp;|&nbsp; <img src="Images/Dashboard/logoutIcn.png" class="linkIcon_1" alt="Logout" /><a href="#" title="Logout">Logout</a></td>
            </tr>
          </table></td>
        <td width="141"><img src="Images/Dashboard/e-GP.gif" width="141" height="64" alt="e-GP" /></td>
      </tr>
    </table>
  </div>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <table width="100%" cellspacing="0">
    <tr valign="top">
      <td class="lftNav"><ul class="lftLinkBtn_1"></ul>
    	 <jsp:include page="MsgBoxLeft.jsp" ></jsp:include></td>    
      <td class="contentArea"><div class="pageHead_1">Sent </div>      
        <table width="100%" cellspacing="0" class="tableList_1 t_space">
          <tr>
            <th>Select</th>
            <th >Sl. No.</th>            
            <th >To </th>            
            <th >Subject </th>
             <th >Priority</th>
            <th >Date </th>
          </tr>
          <tr>
             <td><input type="checkbox" name="" value="" /> </td>
            <td >1</td>            
            <td width="2%">Urmil Mehta(urmil.mehta@abcprocure.com)</td>
            <td><a href="#">Opening of Tender</a></td>
            <td >High</td>
            <td >27/08/2010</td>
         </tr>
        </table>
        <!--Bottom controls-->
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pagging_1">
          <tr>            
            <td align="left">   
              <label class="formBtn_1">
              <input type="submit" name="button" id="button" value="Archive" />
              </label></td>      
          </tr>
        </table>
        <div>&nbsp;</div></td>
    </tr>
  </table>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
  <table width="100%" cellspacing="0" class="footerCss">
    <tr>
      <td align="left">e-GP &copy; All Rights Reserved
        <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
      <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a></td>
    </tr>
  </table>
  <!--Dashboard Footer End-->
</div>
</body>
</html>
