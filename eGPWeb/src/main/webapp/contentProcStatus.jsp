<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.

    0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            Object objUName = session.getAttribute("userName");
            boolean isLoggedIn = false;
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userTypeId").toString();
            }
            if (objUName != null) {
                isLoggedIn = true;
            }
%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Procurement Forum</title>
<link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%-- <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />--%>

        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="resources/js/jQuery/jquery.validate.js"></script>
       <link type="text/css" rel="stylesheet" href="resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript">
            $(function() {
                $('#btnReset').click(function() {
                    $("#pageNo").val("1");
                    $.post("<%=request.getContextPath()%>/getAdminPost", {funName: "AllTopics",keyword:$("#keyword").val(),textfield6:$("#textfield6").val(),select2:$("#select2").val() ,viewType:$("#viewType").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                        $('#resultTable').find("tr:gt(0)").remove();
                        $('#resultTable tr:last').after(j);

                        if($('#noRecordFound').val() == "noRecordFound"){
                            $('#pagination').hide();
                        }else{
                            $('#pagination').show();
                        }
                        chkdisble($("#pageNo").val());
                        if($("#totalPages").val() == 1){
                            $('#btnNext').attr("disabled", "true");
                            $('#btnLast').attr("disabled", "true");
                        }else{
                            $('#btnNext').removeAttr("disabled");
                            $('#btnLast').removeAttr("disabled");
                        }
                        $("#pageTot").html($("#totalPages").val());
                        $("#pageNoTot").html($("#pageNo").val());
                        $('#resultDiv').show();
                    });
                });
            });
        </script>


        <script type="text/javascript">
            function loadTable()
            {
                    $.post("<%=request.getContextPath()%>/getAdminPost", {funName: "AllTopics",keyword:$("#keyword").val(),textfield6:$("#textfield6").val(),select2:$("#select2").val() ,viewType:$("#viewType").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    if($('#noRecordFound').val() == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageTot").html($("#totalPages").val());
                    $("#pageNoTot").html($("#pageNo").val());
                    $('#resultDiv').show();
                });
            }
        </script>
         <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
<script type="text/javascript">
    function test(type){
            if(type==1){
              
                document.getElementById("linkApproved").className = "sMenu";
                document.getElementById("linkRejected").className = "";
                document.getElementById("viewType").value = "";
                //fillGridOnEvent('pending');
                loadTable();
                 }else {
                document.getElementById("linkRejected").className = "sMenu";
                document.getElementById("linkApproved").className = "";
                document.getElementById("viewType").value = "MYREPLIED";
                //fillGridOnEvent('rejected');
                loadTable();
            }
            
        }
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            $(function() {

                $('#btnSearch').click(function() {

                    $("#pageNo").val("1");
                   
                    $.post("<%=request.getContextPath()%>/getAdminPost", {action: $("#action").val(), funName: "AllTopics",keyword:$("#keyword").val(),textfield5:$("#textfield5").val(),select2:$("#select2").val() ,viewType:$("#viewType").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){

                        $('#resultTable').find("tr:gt(0)").remove();

                        $('#resultTable tr:last').after(j);
                        if($('#noRecordFound').val() == "noRecordFound"){
                            $('#pagination').hide();
                        }else{
                            $('#pagination').show();
                        }
                        chkdisble($("#pageNo").val());
                        if($("#totalPages").val() == 1){
                            $('#btnNext').attr("disabled", "true");
                            $('#btnLast').attr("disabled", "true");
                        }else{
                            $('#btnNext').removeAttr("disabled");
                            $('#btnLast').removeAttr("disabled");
                        }
                        $("#pageTot").html($("#totalPages").val());
                        $("#pageNoTot").html($("#pageNo").val());
                        $('#resultDiv').show();
                    });
                });
            });
        </script>
        <script type="text/javascript">
            function loadTable()
            {

        $.post("<%=request.getContextPath()%>/getAdminPost", {action: $("#action").val(),funName: "AllTopics",keyword:$("#keyword").val(),textfield6:$("#textfield6").val(),select2:$("#select2").val() ,viewType:$("#viewType").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    if($('#noRecordFound').val() == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageTot").html($("#totalPages").val());
                    $("#pageNoTot").html($("#pageNo").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo=$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                        //chkdisble($('#pageNo').val());

                        /*if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnFirst').attr("disabled", "true")

                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnLast').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnNext').removeAttr("disabled");*/
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);

                        loadTable();

                        $('#dispPage').val(totalPages);
                        //chkdisble($('#pageNo').val());
                        /*if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnFirst').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnPrevious').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnLast').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnNext').attr("disabled", "true");*/
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);

                        loadTable();

                        $('#dispPage').val(Number(pageNo)+1);
                        //chkdisble($('#pageNo').val());
                        $('#dispPage').val(Number(pageNo)+1);
                        /*if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnFirst').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != 1)
                            $('#btnPrevious').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnLast').attr("disabled", "true");

                        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                            $('#btnNext').attr("disabled", "true");*/
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);

                        loadTable();

                        $('#dispPage').val(Number(pageNo) - 1);
                        //chkdisble($('#pageNo').val());
                        /*if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnFirst').attr("disabled", "true")

                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnLast').removeAttr("disabled");

                        if(parseInt($('#pageNo').val(), 10) != parseInt($('#totalPages').val()))
                            $('#btnNext').removeAttr("disabled");*/
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));

                            loadTable();
                            //chkdisble($('#pageNo').val());
                            /*if(parseInt($('#pageNo').val(), 10) != 1)
                                $('#btnFirst').removeAttr("disabled");
                            if(parseInt($('#pageNo').val(), 10) == 1)
                                $('#btnFirst').attr("disabled", "true")

                            if(parseInt($('#pageNo').val(), 10) == 1)
                                $('#btnPrevious').attr("disabled", "true")
                            if(parseInt($('#pageNo').val(), 10) > 1)
                                $('#btnPrevious').removeAttr("disabled");

                            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                                $('#btnLast').attr("disabled", "true");
                            else
                                $('#btnLast').removeAttr("disabled");

                            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val()))
                                $('#btnNext').attr("disabled", "true")
                            else
                                $('#btnNext').removeAttr("disabled");*/
                        }
                    }
                });
            });
        </script>



</head>
<body onload="loadTable();">
     <form id="alltenderFrm" method="post">
    <input type="hidden" name="viewType" id="viewType" value="ALL"/>
    <input type="hidden" id="pageNo" value="1"/>
    <input type="hidden" name="size" id="size" value="10"/>
<div class="dashboard_div">
 <%
                        if (objUName!=null) {
            %>
            <div class="dashboard_div">
                <%@include file="resources/common/AfterLoginTop.jsp" %> <%} else {%>
                <div class="fixDiv">

                    <jsp:include page="resources/common/Top.jsp" ></jsp:include> <%}%>
<div class="pageHead_1"> Procurement Forum Topics  </div>
  <div class="formBg_1 t_space">

    <table cellspacing="10" class="formStyle_1" width="100%">
      <tr>
        <td width="12%" class="ff">Keyword :</td>
        <td width="47%"><input type="text" class="formTxtBox_1" name ="keyword" id="keyword" style="width:200px;" /></td>
        <td width="9%"><span class="ff">Posted By : </span></td>
        <td width="32%"><input type="text" class="formTxtBox_1" name="select2" id="select2" style="width:200px;" /></td>
      </tr>
      <tr>
        <td class="ff">Date of Posting : </td>
        <td><input name="textfield6" type="text" class="formTxtBox_1"  name="textfield5" id="textfield5" style="width:100px;" readonly="readonly" onClick="GetCal('textfield5','textfield5');" />
          &nbsp;<a href="javascript:void(0);" onclick="" title="Calender"><img src="resources/images/Dashboard/calendarIcn.png" id="calendarIcn" name ="calendarIcn" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('textfield5','calendarIcn');" /></a></td>
        <td class="ff">Action : </td>
        <td><select name="action" id="action" class="txtSearchBox" style="width:100px;">
                <option value ="">All</option>
                <option value ="Pending">Pending</option>
                <option value ="Accepted">Accepted</option>
                <option value ="Rejected">Rejected</option>
                
                </select>

        </td>

        <td width="9%">&nbsp;</td>
        <td width="32%">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="4" class="t-align-center ff">
            <span class="formBtn_1">
                <input type="button" name="btnSearch" id="btnSearch" value="Search" /></span>
          <span class="formBtn_1">
          <input type="submit" name="btnReset" id="btnReset" value="Reset" />
          </span></td>
      </tr>
    </table>
  </div><%
                                    if (objUName != null) {
                            %>
  <div class="t_space b_space t-align-right">
      <a href="PostTopic.jsp" class="action-button-add">Post New Topic</a>
  </div>
  <%}%>
    <div id="resultDiv" style="display: none">
        <ul class="tabPanel_1 t_space">
           
            <li><a href="javascript:void(0);" id="linkApproved" onclick="test(1);" class="sMenu">Topics</a></li>
            

            <li><a href="javascript:void(0);" id="linkRejected" onclick="test(3);">Replies</a></li>
           
        </ul>

  <div class="tabPanelArea_1">
    <table id="resultTable" width="100%" cellspacing="0" class="tableList_3 t_space">
      <tr>
          <th width="5%" class="t-align-left">Sr.No</th>
        <th width="40%" class="t-align-left">Topics </th>
        <th width="11%" class="t-align-left">Username</th>
        <th width="20%" class="t-align-left">Date and Time</th>
        <th width="10%" class="t-align-left">Verification Status</th>
        <th width="10%" class="t-align-left">Action</th>
      </tr>

    </table>
  </div>
    <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                                        <tr>
                                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                            <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                &nbsp;
                                                <label class="formBtn_1 t-align-center">

                                                    <input type="button" name="button" id="btnGoto" value="Go To Page" />


                                                </label>


                                            </td>
                                                <td align="right" class="prevNext-container"><ul>
                                                        <li><font size="3">&laquo;</font> <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                                        <li><font size="3">&#8249;</font> <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                                        <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                        <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                                </ul></td>
                                        </tr>
                                    </table>
                                    <div align="center">
                                        <input type="hidden" id="pageNo" value="1"/>
                                        <input type="hidden" name="size" id="size" value="10"/>
                                    </div>
  </div>
  <div>&nbsp;</div>

  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
  <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
  <!--Dashboard Footer End-->
</div>
</div>
     </form>
</body>
</html>
