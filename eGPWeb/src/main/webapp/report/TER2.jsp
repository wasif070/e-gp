<%-- 
    Document   : TER2
    Created on : Apr 14, 2011, 10:31:01 AM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
       <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    String tenderid = request.getParameter("tenderid");
                    String lotId = request.getParameter("lotId");
                    String roundId = "0";
                    if(request.getParameter("rId")!=null){
                        roundId = request.getParameter("rId");
                    }
                    String stat = request.getParameter("stat");//stat value is 'eval'
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    String pNature = commonService.getProcNature(request.getParameter("tenderid")).toString();
                    String eventType = commonService.getEventType(tenderid).toString();
                    String tenderPaidORFree = commonService.tenderPaidORFree(tenderid, lotId);
                    String repLabel = "Tender";
                    if(pNature.equalsIgnoreCase("Services")){
                        repLabel = "Proposal";
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=repLabel%> Evaluation Report 2</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%
                ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");

                    List<SPCommonSearchDataMore> tendOpeningRpt = creationService.getOpeningReportData("getTenderOpeningRpt", tenderid, lotId,null);//lotid

                    List<SPCommonSearchDataMore> TECMembers = dataMoreService.getCommonSearchData("getTECMembers", tenderid, lotId,"TER2",roundId); //lotid ,"TOR1"

                    List<Object[]> finalSubmissionUser = creationService.getFinalSubmissionUserForTer2(tenderid,lotId,roundId);

                    List<Object[]> ter2Criteria = creationService.getTERCriteria("TER2", pNature);
                    List<SPCommonSearchDataMore> NoteofDissent = creationService.getOpeningReportData("getNoteofDissent", tenderid, lotId,"ter2");
                    List<SPCommonSearchDataMore> pMethodEstCost = dataMoreService.getCommonSearchData("GetPMethodEstCostTender",tenderid);
                    String estCost = pMethodEstCost.get(0).getFieldName2();
                    String estCostStatus = "-";
                    if(estCost!=null)
                    {
                        estCostStatus = (Double.parseDouble(estCost) > 0?"Approved":"-");
                    }

                    boolean list1 = true;
                    boolean list5 = true;
                    if (tendOpeningRpt.isEmpty()) {
                        list1 = false;
                    }
                    if (TECMembers.isEmpty()) {
                        list5 = false;
                    }
                   boolean isView = false;
                    boolean isSign = false;
                    boolean isMEM = true;
                    if("y".equals(request.getParameter("isview"))){
                        isView = true;
                    }
                    if("y".equals(request.getParameter("sign"))){
                        isSign = true;
                    }
                    if("y".equals(request.getParameter("ismem"))){
                        isMEM = false;
                    }
        %>
        <div class="tabPanelArea_1">
            <div class="pageHead_1"><%=repLabel%> Evaluation Report 2 - Technical Responsiveness Report<span style="float: right;">
                    <%
                        boolean isCommCP = false;                        
                        List<SPCommonSearchDataMore> commCP = dataMoreService.getCommonSearchData("evaluationMemberCheck", tenderid, session.getAttribute("userId").toString(), "1", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);                        
                        if (commCP != null && (!commCP.isEmpty()) && (!commCP.get(0).getFieldName1().equals("0"))) {
                            isCommCP = true;
                        }                        
                        commCP=null;
                        if(isCommCP){
                    %>
                    <a class="action-button-goback" href="../officer/Evalclarify.jsp?tenderId=<%=tenderid%>&st=rp&comType=TEC">Go Back to Dashboard</a>
                    <%}else{%>
                    <a class="action-button-goback" href="../officer/MemberTERReport.jsp?tenderId=<%=tenderid%>&comType=TEC">Go Back to Dashboard</a>
                    <%}%>
                </span></div>
            <%
                        pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                        pageContext.setAttribute("tab", "6");
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div class="tableHead_1 t_space t-align-center">Technical Responsiveness Report</div>
            <form action="<%=request.getContextPath()%>/ServletEvalCertiService?funName=evalTERReportIns&rep=ter2" method="post">
            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 b_space" width="100%">
                <tr>
                    <td width="24%" class="ff">Ministry Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName1());
                                }%></td>
                    <td width="16%" class="ff">Division Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName2());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff">Organization/Agency Name :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName3());
                                }%></td>
                    <td class="ff">Procuring Entity :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName4());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff"><%=repLabel%> Package No. and Description :</td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName5());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff">Lot No. and Description : </td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName12()+" & "+tendOpeningRpt.get(0).getFieldName6());
                                }%></td>
                </tr>
            </table>
            <div class="inner-tableHead t_space">Procurement Data</div>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>

                        <th width="47%">Procurement Type</th>
                        <th width="53%">Procurement Method</th>
                    </tr>
                    <tr>
                        <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName7());
                                }%></td>
                       <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName9());
                                }%></td>
                    </tr>
                </tbody></table>

            <div class="inner-tableHead t_space">Procurement Plan</div>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>
                        <th width="17%">Approval<br>
                            Status</th>
                        <th width="31%">Budget Type</th>
                        <%if(!pNature.equalsIgnoreCase("Services")){%>
                        <th width="14%">Approval Status
                            of<br>
                            Official Estimates</th>
                       <%}%>
                    </tr>
                    <tr>
                        <td class="t-align-center">Approved</td>
                        <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName8());
                                }%></td>
                        <%if(!pNature.equalsIgnoreCase("Services")){%>
                        <td class="t-align-center"><%=estCostStatus%></td>
                        <%}%>
                    </tr>
                </tbody></table>
            <div class="inner-tableHead t_space">Criteria</div>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>
                        <th width="35%">Name of Bidder/Consultant</th>
                         <%for(Object[] data : ter2Criteria){%>
                         <th><%=data[1]%></th>
                         <%}%>
                    </tr>
                    <%
                        List<Object[]> evalRepData = creationService.evalRepData(tenderid, lotId, stat, "TER2",session.getAttribute("userId").toString(),isSign);
                       //0 tfs.userId,1 tcm.companyId,2 tcm.companyName,3 ttm.firstName,4 ttm.lastName
                           for(Object[] data : finalSubmissionUser){
                           String userBidder = null;
                           if(!data[1].toString().equals("1")){
                               userBidder = data[2].toString();
                           }else{
                                userBidder = data[3].toString()+" "+data[4].toString();
                           }
                    %>
                    <tr>
                        <td><%=userBidder%></td>
                        <%
                            for(Object[] data2 : ter2Criteria){
                              if(!data2[1].toString().equalsIgnoreCase("Evaluation Status")){
                        %>
                        <td class="t-align-center">
                            <%
                                String isYes = "";
                                String isNo = "";
                                String isNA = "";
                                String evalValue=null;
                                //0. et.userId,1. et.criteriaId,2. et.value
                                for(Object[] objects : evalRepData){
                                    boolean tempbool = data[0].toString().equals(objects[0].toString()) && data2[0].toString().equals(objects[1].toString());
                                    if(tempbool && objects[2].toString().equalsIgnoreCase("yes")){
                                        isYes = "selected";
                                        evalValue = "Yes";
                                    }
                                    if(tempbool && objects[2].toString().equalsIgnoreCase("no")){
                                        isNo = "selected";
                                        evalValue = "No";
                                    }
                                    if(tempbool && objects[2].toString().equalsIgnoreCase("na")){
                                        isNA = "selected";
                                        evalValue = "NA";
                                    }
                                }
                                if(isView){
                                    out.print(evalValue);
                                }else{
                            %>
                             <select name="critValue" class="formTxtBox_1">
                                <option value="Yes_<%=data[0]%>_<%=data2[0]%>" <%=isYes%>>Yes</option>
                                <option value="No_<%=data[0]%>_<%=data2[0]%>" <%=isNo%>>No</option>
                                <option value="NA_<%=data[0]%>_<%=data2[0]%>" <%=isNA%>>NA</option>
                            </select>
                            <%}%>
                         </td>
                         <%
                            isYes=null;
                            isNo=null;
                            isNA=null;
                             evalValue=null;
                              }}
                          %>
                          <td class="t-align-center">
                          <%
                                String evalStatus = creationService.getBidderStatus(data[0].toString(), tenderid, lotId);
                                if("Technically Unresponsive".equalsIgnoreCase(evalStatus)){
                                    out.print("Technically Non-responsive");
                                }else{
                                    out.print(evalStatus);
                                }
                          %>
                          </td>
                    </tr>
                    <%
                           }
                    %>
                </tbody></table>
                <%if(!"y".equals(request.getParameter("config")) && isMEM){%>
                <%if(NoteofDissent!=null && (!NoteofDissent.isEmpty())){%>
            <div class="inner-tableHead t_space">Note of Dissent:</div>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <th>TEC Member</th>
                    <th>Note of Dissent</th>
                </tr>
                <%for(SPCommonSearchDataMore nod : NoteofDissent){%>
                    <td><%=nod.getFieldName1()%></td>
                    <td><%=nod.getFieldName2()%></td>
                <%}%>
            </table>
            <%}%>
            <div class="c_t_space atxt_1" style="color: red;">I  do hereby declare and confirm that I have no business or other links to any of the competing Tenderers/Consultants.<br /><br />

                The Evaluation Committee certifies that the examination and evaluation has followed the requirements of the Act, the Rules made there under and the terms and conditions of the prescribed Application, Tender or Proposal Document and that all facts and information have been correctly reflected in theEvaluation Report and, that no substantial or important information has been omitted. </div>

            <div class="inner-tableHead t_space">TEC Members</div>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <td class="tableHead_1" colspan="5" >TEC Members</td>
                </tr>
                <tr>
                    <th width="20%" >Name</th>
                    <%
                    for (SPCommonSearchDataMore dataMore : TECMembers) {
                        out.print("<td>");
                        if(dataMore.getFieldName5().equals(session.getAttribute("userId").toString())){
                         if(dataMore.getFieldName6().equals("-")){
                             String valChk="";
                             if(!isCommCP){
                                valChk = "&nDis=y";
                             }
                   %>
                    <a href="../officer/TORSigningApp.jsp?id=<%=dataMore.getFieldName7()%>&uid=<%=dataMore.getFieldName5()%>&tid=<%=tenderid%>&lotId=<%=lotId%>&stat=<%=stat%>&rpt=ter2<%=valChk%>">
                            <%=dataMore.getFieldName1()%>
                        </a>
                   <%
                        }else{
                             out.print(dataMore.getFieldName1());
                        }

                    }else{
                        out.print(dataMore.getFieldName1());
                    }
                        out.print("</td>");
                    }
                  %>
               </tr>
                <tr>
                    <th width="20%">Committee Role</th>
                    <%for (SPCommonSearchDataMore dataMore : TECMembers) {
                        out.print("<td>");
                            if (dataMore.getFieldName2().equals("cp")) {
                            out.print("Chairperson");
                        } else if (dataMore.getFieldName2().equals("ms")) {
                            out.print("Member Secretary");
                        } else if (dataMore.getFieldName2().equals("m")) {
                            out.print("Member");
                            }
                        out.print("</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Designation</th>
                    <%for (SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName3()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">PE Office</th>
                    <%for (SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName4()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Signed <%=repLabel%> Evaluation Report 2 On</th>
                    <%for(SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName6()+"</td>");}%>
                </tr>
            </table>
                <%}%>
                <input type="hidden" value="<%=tenderid%>" name="tId">
                <input type="hidden" value="<%=lotId%>" name="lId">
                <input type="hidden" value="<%=stat%>" name="stat">
                <input type="hidden" value="TER2" name="rType">
                <input type="hidden" value="<%if(isCommCP){out.print("cp");}else{out.print("m");}%>" name="role">
                <%if(!isView){%>
                <div class="t-align-center t_space">
                    <label class="formBtn_1" accesskey="">
                        <input type="submit" value="Save"/>
                    </label>
                </div>
                <%}%>
            </form>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <%
    %>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
