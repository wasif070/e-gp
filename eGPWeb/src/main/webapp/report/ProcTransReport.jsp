<%-- 
    Document   : ProcTransReport
    Created on : Mar 19, 2012, 11:31:15 AM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.GetDepartMentChildIdsByParentId"%>
<%@page import="com.cptu.egp.eps.dao.daointerface.HibernateQueryDao"%>
<%@page import="com.cptu.egp.eps.web.databean.ProcReportDtBean"%>
<%@page import="java.util.ArrayList"%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.AppMISService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    boolean isNotPDF = true;
                    if (request.getParameter("isPDF") != null && "true".equalsIgnoreCase(request.getParameter("isPDF"))) {
                        isNotPDF = false;
                    }
                    int uTypeId = 0;
                    String suserId = null;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Procurement Transaction Report (Category Wise)</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />        
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script type="text/javascript">
        function showHide()
	{
		if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
			document.getElementById('tblSearchBox').style.display = 'table';
			document.getElementById('collExp').innerHTML = '- Advanced Search';
		}else{
			document.getElementById('tblSearchBox').style.display = 'none';
			document.getElementById('collExp').innerHTML = '+ Advanced Search';
		}
	}
	function hide()
	{
		document.getElementById('tblSearchBox').style.display = 'none';
		document.getElementById('collExp').innerHTML = '+ Advanced Search';
	}
            function onSelectionOfTreeOff(){
                $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId:$('#txtdepartmentid').val(),funName:'Office'}, function(j){
                    $("select#cmboffice").html(j);
                    var cmb = document.getElementById('cmboffice');
                    $("#offName").val(cmb.options[cmb.selectedIndex].text);
                });
            }

            function onSelectionOfProject(){
                var cmb = document.getElementById('program');
                $("#programname").val(cmb.options[cmb.selectedIndex].text);
                $.post("<%=request.getContextPath()%>/GovtUserSrBean", {progId:$('#program').val(),action:'getProjectFrmPrograme'}, function(j){
                    $("select#project").html(j);                    
                });
            }

            function setOffName(){
                var cmb = document.getElementById('cmboffice');
                $("#offName").val(cmb.options[cmb.selectedIndex].text);
            }
            function setQuaterName(){
                var cmb = document.getElementById('quater');
                $("#quatername").val(cmb.options[cmb.selectedIndex].text);
            }
            function setProjectName(){
                var cmb = document.getElementById('project');
                $("#projectname").val(cmb.options[cmb.selectedIndex].text);
            }
            function changeQuater(){
                var cmb = document.getElementById('cmbFinancialYear');
                var cval1 = cmb.value.split("-")[0];
                var cval2 = cmb.value.split("-")[1];
//                var qstring = "<option value='0'>Please Select Quarter</option><option value='"+cval1+"-07-1_"+cval1+"-09-30'>Q1</option><option value='"+cval1+"-10-1_"+cval1+"-12-31'>Q2</option><option value='"+cval2+"-01-1_"+cval2+"-03-31'>Q3</option><option value='"+cval2+"-04-1_"+cval2+"-06-30'>Q4</option><option value='"+cval1+"-07-1_"+cval1+"-12-31'>Q1 and Q2</option><option value='"+cval1+"-10-1_"+cval2+"-03-31'>Q2 and Q3</option><option value='"+cval2+"-01-1_"+cval2+"-06-30'>Q3 and Q4</option><option value='"+cval1+"-07-1_"+cval2+"-03-31'>Q1 and Q2 and Q3</option><option value'"+cval1+"-10-1_"+cval2+"-06-30'>Q2 and Q3 and Q4</option><option value='"+cval1+"-07-1_"+cval2+"-06-30'>All</option>";
                var qstring = "<option value='"+cval1+"-07-1_"+cval2+"-06-30'>All Four Quarter</option><option value='"+cval1+"-07-1_"+cval1+"-09-30'>Q1</option><option value='"+cval1+"-10-1_"+cval1+"-12-31'>Q2</option><option value='"+cval2+"-01-1_"+cval2+"-03-31'>Q3</option><option value='"+cval2+"-04-1_"+cval2+"-06-30'>Q4</option><option value='"+cval1+"-07-1_"+cval1+"-12-31'>Q1 and Q2</option><option value='"+cval1+"-10-1_"+cval2+"-03-31'>Q2 and Q3</option><option value='"+cval2+"-01-1_"+cval2+"-06-30'>Q3 and Q4</option><option value='"+cval1+"-07-1_"+cval2+"-03-31'>Q1 and Q2 and Q3</option><option value'"+cval1+"-10-1_"+cval2+"-06-30'>Q2 and Q3 and Q4</option>";
                //var qcombo1 = document.getElementById('quater').options[1].value.substring(0, 4);
                //var qcombo2 = document.getElementById('quater').options[3].value.substring(0, 4);                
                $("#quater").html(qstring);
            }
            function onSelectionOfTreeDesig(){}
//            function showHide(){}
            function checkCondition(){}
            function checkHOPE(){}
            function validate(){
                $(".err").remove();
                var cnt=0;
                if($('#cmbFinancialYear').val()=='0'){
                    $('#cmbFinancialYear').parent().append("<div class='err' style='color:red;'>Please select Financial Year</div>");
                    cnt++;
                }
                return (cnt==0);
            }
        </script>
    </head>
    <body onload="hide();" <%=isNotPDF ? " onload='changeQuater();'" : ""%>>
        <%
                    GetDepartMentChildIdsByParentId tc = new GetDepartMentChildIdsByParentId();
                    String finYear = (request.getParameter("financialyear") == null ? "" : request.getParameter("financialyear"));
                    String quater = request.getParameter("quater");
                    String depId = "".equals(request.getParameter("depId")) ? "" : tc.getDepIds(request.getParameter("depId"));
                    String offId = request.getParameter("offId");
                    String program = request.getParameter("program");
                    String project = request.getParameter("project");
                    String appType = request.getParameter("appType");
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%if (isNotPDF) {
                                suserId = session.getAttribute("userId").toString();%>
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <%uTypeId = Integer.parseInt(objUserTypeId.toString());
                            } else {
                                uTypeId = Integer.parseInt(request.getParameter("uTypeId"));
                                suserId = request.getParameter("suserId");
                            }%>
                <%
                            //if(("19".equals(suserId) && uTypeId==19)||("19".equals(suserId) && uTypeId==19)){
                            if ((uTypeId == 19) || (uTypeId == 20)) {
                                //suserId = "1";
                                uTypeId = 1;
                            }
                            CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                            List<SPCommonSearchDataMore> appListDtBean = dataMoreService.geteGPDataMore("FinancialYear");
                            AppMISService mISService = (AppMISService) AppContext.getSpringBean("AppMISService");
                %>
                <div class="contentArea_1">                    
                    <div class="pageHead_1">Procurement Transaction Report (Category Wise)
                        <%if (isNotPDF) {%>
                        <span style="float: right;">
                            <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                            &nbsp;&nbsp;
                            <%
                                StringBuffer hrefQString = new StringBuffer();
                                hrefQString.append("&financialyear=");
                                hrefQString.append(finYear);
                                hrefQString.append("&quater=");
                                hrefQString.append(quater);
                                hrefQString.append("&depId=");
                                hrefQString.append(depId);
                                hrefQString.append("&offId=");
                                hrefQString.append(offId);
                                hrefQString.append("&program=");
                                hrefQString.append(program);
                                hrefQString.append("&project=");
                                hrefQString.append(project);
                                hrefQString.append("&quatername=");
                                hrefQString.append(request.getParameter("quatername"));
                                hrefQString.append("&depName=");
                                hrefQString.append(request.getParameter("depName"));
                                hrefQString.append("&offName=");
                                hrefQString.append(request.getParameter("offName"));
                                hrefQString.append("&programname=");
                                hrefQString.append(request.getParameter("programname"));
                                hrefQString.append("&projectname=");
                                hrefQString.append(request.getParameter("projectname"));

                            %>
                            <a class="action-button-savepdf" id="saveASPDF" href="<%=request.getContextPath()%>/TorRptServlet?uTypeId=<%=uTypeId%>&action=PTRpt&generate=Generate<%=hrefQString.toString()%>">Save As PDF</a>
                        </span>
                        <%}%>
                    </div>
                    <%if (isNotPDF) {%>
                    <div class="formBg_1 t_space">
                        <form method="post" action="ProcTransReport.jsp">
                            <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>

                            <table width="100%" cellspacing="8" cellpadding="0"  class="formStyle_1" id="tblSearchBox">
                                <tr>
                                    <td width="15%" class="ff" align="left">By Financial Year :<span class="mandatory">*</span></td>
                                    <td width="35%" align="left">
                                        <select name="financialyear" class="formTxtBox_1" id="cmbFinancialYear" style="width:200px;" onchange="changeQuater();">
                                            <option value="0">Please Select Financial Year</option>
                                            <!--%
                                                        if (appListDtBean != null && !appListDtBean.isEmpty()) {
                                                            for (SPCommonSearchDataMore commonApp : appListDtBean) {
                                            %>
                                            <option < %if ("Yes".equalsIgnoreCase(commonApp.getFieldName3())) {
                                                                                                                out.print("selected");
                                                                                                            }
                                                %> value="< %=commonApp.getFieldName2()%>">< %=commonApp.getFieldName2()%></option>
                                            < %
                                                            }
                                                        }
                                            %-->
                                            <%
                                                if (appListDtBean != null && !appListDtBean.isEmpty()) {
                                                    for (SPCommonSearchDataMore commonApp : appListDtBean) {
                                                        out.print("<option  value='" + commonApp.getFieldName2() + "'>" + commonApp.getFieldName2() + "</option>");
                                                    }
                                                }
                                            %>
                                        </select>
                                    </td>
                                    <td width="15%" class="ff" align="left">By Quarter :</td>
                                    <td width="35%" align="left">
                                        <select name="quater" id="quater" style="width:200px;" class="formTxtBox_1" onchange="setQuaterName()">
                                            <option value="2012-07-1_2013-06-30">All Four Quarter</option>
                                            <option value="2012-07-1_2012-09-30">Q1</option>
                                            <option value="2012-10-1_2012-12-31">Q2</option>
                                            <option value="2013-01-1_2013-03-31">Q3</option>
                                            <option value="2013-04-1_2013-06-30">Q4</option>
                                            <option value="2012-07-1_2012-12-31">Q1 and Q2</option>
                                            <option value="2012-10-1_2013-03-31">Q2 and Q3</option>
                                            <option value="2013-01-1_2013-06-30">Q3 and Q4</option>
                                            <option value="2012-07-1_2013-03-31">Q1 and Q2 and Q3</option>
                                            <option value="2012-10-1_2013-06-30">Q2 and Q3 and Q4</option>
                                        </select>
                                        <input type="hidden" name="quatername" id="quatername"/>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td class="ff t-align-left">Hierarchy Node : <!--%if (uTypeId == 1) {%><span class="mandatory">*</span>< %}%--></td>
                                    <td class="t-align-left">
                                        <%if (uTypeId == 5) {
                                                Object[] objData = mISService.getOrgAdminOrgName(Integer.parseInt(suserId));
                                                out.print(objData[1].toString());
                                                out.print("<input type=\"hidden\" id=\"txtdepartment\" name=\"depName\" value=\"" + objData[1].toString() + "\"/>");
                                                out.print("<input type=\"hidden\" id=\"txtdepartmentid\" name=\"depId\" value=\"" + objData[0] + "\"/>");
                                            }
                                        else if(uTypeId==3){
                                            Object[] objData = mISService.getOrgAdminOrgNameByUserId(Integer.parseInt(session.getAttribute("userId").toString()));
                                            out.print(objData[1].toString());
                                            out.print("<input type=\"hidden\" id=\"txtdepartment\" name=\"depName\" value=\"" + objData[1].toString() + "\"/>");
                                            out.print("<input type=\"hidden\" id=\"txtdepartmentid\" name=\"depId\" value=\"" + objData[0] + "\"/>");
                                            out.print("<input type=\"hidden\" id=\"uTypeId\" name=\"uTypeId\" value=\"" + uTypeId + "\"/>");
                                         }
                                        else {%>
                                        <input type="text" id="txtdepartment" style="width: 200px;" class="formTxtBox_1" readonly name="depName"/>
                                        <input type="hidden" id="cmborg">
                                        <!--To add CCGP and Cabinet Division add this in below as queryString ?operation=govuser-->
                                        <a id="imgTree" href="javascript:void(0);" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=govuser', '', 'width=350px,height=400px,scrollbars=1','');">
                                            <img alt="Department" style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                        </a>
                                        <input type="hidden" id="txtdepartmentid" name="depId">
                                        <input type="hidden" id="txtDepartmentType">
                                        <%}%>
                                    </td>
                                    <!--Change PE to PA by Proshanto-->
                                    <td class="ff t-align-left">PA Office :</td>
                                    <td class="t-align-left">
                                        <%if (uTypeId == 5 || uTypeId == 1 || uTypeId == 3) {%>
                                        <select id="cmboffice" style="width: 200px;" class="formTxtBox_1" name="offId" onchange="setOffName()">
                                            <%

                                                if (uTypeId == 1) {
                                            %>
                                            <option value="0"> No Office Found.</option>
                                            <%                                    }
                                                List<Object[]> cmbLists = null;
                                                if (uTypeId == 5 || uTypeId == 3) {
                                                    cmbLists = mISService.getOfficeList(suserId, "" + uTypeId);
                                                    for (Object[] cmbList : cmbLists) {
                                                        String selected = "";
                                                        if (request.getParameter("offId") != null) {
                                                            if (cmbList[0].toString().equals(request.getParameter("offId"))) {
                                                                selected = "selected";
                                                            }
                                                        }
                                                        out.print("<option value='" + cmbList[0] + "' " + selected + ">" + cmbList[1] + "</option>");
                                                        selected = null;
                                                    }
                                                    if (cmbLists.isEmpty()) {
                                                        out.print("<option value='0'> No Office Found.</option>");
                                                    }
                                                }
                                            %>
                                        </select>
                                        <input type="hidden" id="offName" value="<%=(uTypeId == 5) ? (request.getParameter("offName") != null && !"".equals(request.getParameter("offName"))) ? request.getParameter("offName") : !cmbLists.isEmpty() ? cmbLists.get(0)[1].toString() : "" : ""%>" name="offName" />
                                        <%
                                            }
                                        %>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="ff">By Programme :</td>
                                    <td>
                                        <select name="program" id="program" style="width:200px;" class="formTxtBox_1" onchange="onSelectionOfProject()">
                                            <option value="0">Please Select Programme</option>
                                            <%
                                                appListDtBean = dataMoreService.geteGPDataMore("getProgramName");
                                                for (SPCommonSearchDataMore prog : appListDtBean) {
                                                    out.print("<option value='" + prog.getFieldName1() + "'>" + prog.getFieldName2() + "</option>");
                                                }
                                            %>
                                        </select>
                                        <input type="hidden" name="programname" id="programname"/>
                                    </td>
                                    <td class="ff">By Project :</td>
                                    <td>
                                        <select name="project" id="project"  style="width:200px;" class="formTxtBox_1" onchange="setProjectName()">
                                            <option  value="0">Please Select Project</option>
                                            <%
                                                appListDtBean = dataMoreService.geteGPDataMore("getProjectName", "");
                                                for (SPCommonSearchDataMore prog : appListDtBean) {
                                                    out.print("<option value='" + prog.getFieldName1() + "'>" + prog.getFieldName2() + "</option>");
                                                }
                                            %>
                                        </select>
                                        <input type="hidden" name="projectname" id="projectname"/>
                                    </td>
                                </tr>    
                                <tr>
                                    <td class="ff">By App Type:</td>
                                    <td>
                                        <select name="appType" id="appType" style="width:200px" class="formTxtBox_1">
                                            <option value="0" selected>All APP</option>
                                            <option value="1">Planned Work</option>
                                            <option value="2">Deposit Work</option>               
                                            <option value="3" >Adhoc Work</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="t-align-center">
                                        <label class="formBtn_1"><input type="submit" value="Generate" id="generate" name="generate" onclick="return validate();"></label>
                                        <label class="anchorLink l_space">
                                            <!--                                            <input type="reset" value="Clear" id="clear" name="clear" onclick="window.location.reload()">-->
                                            <a href="ProcTransReport.jsp" style="text-decoration: none; color: #fff; font-size: 12px;">Reset</a>
                                        </label>

                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <%}%>
                    <%
                                if ("Generate".equals(request.getParameter("generate"))) {
                    %>
                    <div  id="print_area">
                        <div class="formStyle_1 t_space ff">
                            <!--                                Search Criteria : Ministry / Division / Organization : RNBORG, PE Office : --Select PE Office---->
                            <h3>Search Criteria : By Financial Year : <%=finYear%>
                                <%
                                                                    //txtdepartment
                                                                    //offName
                                                                    //projectname
                                                                    //programname
                                                                    //quatername
                                                                    if (depId != null && !depId.equals("")) {
                                                                        out.print(", Hierarchy Node : " + request.getParameter("depName"));
                                                                    }
                                                                    if (!offId.equals("0")) {
                                                                        //Change PE to PA by Proshanto
                                                                        out.print(", PA Office : " + request.getParameter("offName"));
                                                                    }
                                                                    if (!program.equals("0")) {
                                                                        out.print(", By Programme : " + request.getParameter("programname"));
                                                                    }
                                                                    if (!project.equals("0")) {
                                                                        out.print(", By Project : " + request.getParameter("projectname"));
                                                                    }
                                                                    if (request.getParameter("appType").equals("1")) {
                                                                        out.print(", By APP Type : Planned Work");
                                                                    }
                                                                    else if (request.getParameter("appType").equals("2")) {
                                                                        out.print(", By APP Type : Deposit Work");
                                                                    }
                                                                    else if (request.getParameter("appType").equals("3")) {
                                                                        out.print(", By APP Type : Adhoc Work");
                                                                    }
                                                                    else if (request.getParameter("appType").equals("0")) {
                                                                        out.print(", By APP Type : All");
                                                                    }
                                %>
                            </h3>
                        </div>
                        <div>
                            <%if (true && !finYear.equals("")) {%>
                            <%
                                int countFlag;
                                List<SPCommonSearchDataMore> spcsdmsN = dataMoreService.geteGPDataMore("getProcTransactReport", "1", "NCT", finYear, quater.equals("0") ? "0" : quater.split("_")[0], depId, offId, program, project, quater.equals("0") ? "0" : quater.split("_")[1],appType);
                                List<SPCommonSearchDataMore> spcsdmsI = dataMoreService.geteGPDataMore("getProcTransactReport", "1", "ICT", finYear, quater.equals("0") ? "0" : quater.split("_")[0], depId, offId, program, project, quater.equals("0") ? "0" : quater.split("_")[1],appType);
                                List<ProcReportDtBean> data = new ArrayList<ProcReportDtBean>();
                                List<ProcReportDtBean> finaldata = new ArrayList<ProcReportDtBean>();
                                int countOne = 1;
                                //System.out.println("I : " + spcsdmsI.size());//22
                                //System.out.println("N : " + spcsdmsN.size());//25
                                if (spcsdmsI.size() > spcsdmsN.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        data.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6()));
                                        countOne++;
                                    }
                                    for (ProcReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsN) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setnAPPNos(data2.getFieldName3());
                                                prdb.setnAPPEst(data2.getFieldName4());
                                                prdb.setnContractNos(data2.getFieldName5());
                                                prdb.setnContractAmount(data2.getFieldName6());
                                            }
                                        }
                                        finaldata.add(prdb);
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        countFlag = 0;
                                        for (ProcReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6(), null));
                                            countOne++;
                                        }
                                    }
                                }

                                if (spcsdmsN.size() > spcsdmsI.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        data.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6(), null));
                                        countOne++;
                                    }
                                    for (ProcReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsI) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setiAPPNos(data2.getFieldName3());
                                                prdb.setiAPPEst(data2.getFieldName4());
                                                prdb.setiContractNos(data2.getFieldName5());
                                                prdb.setiContractAmount(data2.getFieldName6());
                                            }
                                        }
                                        finaldata.add(prdb);
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        countFlag = 0;
                                        for (ProcReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6()));
                                            countOne++;
                                        }
                                    }
                                }

                                if (spcsdmsN.size() == spcsdmsI.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        data.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6(), null));
                                        countOne++;
                                    }
                                    for (ProcReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsI) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setiAPPNos(data2.getFieldName3());
                                                prdb.setiAPPEst(data2.getFieldName4());
                                                prdb.setiContractNos(data2.getFieldName5());
                                                prdb.setiContractAmount(data2.getFieldName6());
                                            }
                                        }
                                        finaldata.add(prdb);
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        countFlag = 0;
                                        for (ProcReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6()));
                                            countOne++;
                                        }
                                    }
                                }
                            %>
                            <!--Change Development to Capital by Proshanto-->
                            <div class="inner-tableHead t_space">Capital Budget</div>
                            <table class="tableList_3" cellspacing="0" width="100%" id="tab1">
                                <tr>
                                    <th width="4%" rowspan="3" valign="middle">Sl. No </th>
                                    <th width="16%" rowspan="3" valign="middle">Method of<br />
                                        Procurement</th>
                                    <th colspan="12">Procurements </th>
                                </tr>
                                <tr>
                                    <th colspan="2" valign="top">Total Contract Package/Lot estimated as per Proc. Plan (No.)</th>
                                    <!--Change BTN to Nu. by Proshanto-->
                                    <th colspan="2" valign="top">Total estimated amount as per Proc. Plan (Million Nu.)</th>
                                    <th colspan="2" valign="top">Total Contract Package/Lot  awarded/contract signed (No.)</th>
                                    <!--Change BTN to Nu. by Proshanto-->
                                    <th colspan="2" valign="top">Total Contracted amount (Million Nu.)</th>
                                    <th colspan="2" valign="top">Progress as against estimated No. of package/Lot (%)</th>
                                    <th colspan="2" valign="top">Progress as against estimated amount (%)</th>
                                </tr>
                                <tr>
                                    <th width="5%" valign="top">National</th>
                                    <th width="8%" valign="top">International</th>
                                    <th valign="top">National</th>
                                    <th valign="top">International</th>
                                    <th valign="top">National</th>
                                    <th valign="top">International</th>
                                    <th valign="top">National</th>
                                    <th valign="top">International</th>
                                    <th valign="top">National<br />
		(Col 7/3)*100
                                    </th>
                                    <th valign="top">International<br />
		(Col 8/4)*100
                                    </th>
                                    <th valign="top">National<br />
		(Col 9/5)*100
                                    </th>
                                    <th valign="top">International<br />
		(Col 10/6)*100
                                    </th>
                                </tr>
                                <tr>
                                    <th>1</th>
                                    <th>2</th>
                                    <th>3</th>
                                    <th>4</th>
                                    <th>5</th>
                                    <th>6</th>
                                    <th>7</th>
                                    <th>8</th>
                                    <th>9</th>
                                    <th>10</th>
                                    <th>11</th>
                                    <th>12</th>
                                    <th>13</th>
                                    <th>14</th>
                                </tr>
                                <%
                                    for (ProcReportDtBean reportDtBean : finaldata) {
                                %>
                                <tr>
                                    <td style="text-align: left;" id="t1col1_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getCounter()%></td>
                                    <td style="text-align: left;" id="t1col2_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getpMethod()%>(<%=reportDtBean.getpNature()%>)</td>
                                    <td style="text-align: right;" id="t1col3_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPNos() == null ? 0 : reportDtBean.getnAPPNos()%></td>
                                    <td style="text-align: right;" id="t1col4_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPNos() == null ? 0 : reportDtBean.getiAPPNos()%></td>
                                    <td style="text-align: right;" id="t1col5_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPEst() == null ? "N.A." : reportDtBean.getnAPPEst()%></td>
                                    <td style="text-align: right;" id="t1col6_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPEst() == null ? "N.A." : reportDtBean.getiAPPEst()%></td>
                                    <td style="text-align: right;" id="t1col7_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnContractNos() == null ? 0 : reportDtBean.getnContractNos()%></td>
                                    <td style="text-align: right;" id="t1col8_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiContractNos() == null ? 0 : reportDtBean.getiContractNos()%></td>
                                    <td style="text-align: right;" id="t1col9_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnContractAmount() == null ? "N.A." : reportDtBean.getnContractAmount()%></td>
                                    <td style="text-align: right;" id="t1col10_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiContractAmount() == null ? "N.A." : reportDtBean.getiContractAmount()%></td>
                                    <td style="text-align: right;" id="t1col11_<%=reportDtBean.getCounter()%>">
                                        <label id="t1lab1_<%=reportDtBean.getCounter()%>"></label>
                                        <script type="text/javascript">
                                            var t1col7 = parseFloat($('#t1col7_<%=reportDtBean.getCounter()%>').html());
                                            var t1col3 = parseFloat($('#t1col3_<%=reportDtBean.getCounter()%>').html());
                                            var t1ans = (t1col7/t1col3)*100;
                                            $('#t1lab1_<%=reportDtBean.getCounter()%>').html(isNaN(t1ans)? 'N.A.' : t1ans.toFixed(3));
                                        </script>
                                    </td>
                                    <td style="text-align: right;" id="t1col12_<%=reportDtBean.getCounter()%>">
                                        <label id="t1lab2_<%=reportDtBean.getCounter()%>"></label>
                                        <script type="text/javascript">
                                            var t1col8 = parseFloat($('#t1col8_<%=reportDtBean.getCounter()%>').html());
                                            var t1col4 = parseFloat($('#t1col4_<%=reportDtBean.getCounter()%>').html());
                                            var t1ans = (t1col8/t1col4)*100;
                                            $('#t1lab2_<%=reportDtBean.getCounter()%>').html(isNaN(t1ans)? 'N.A.' : t1ans.toFixed(3));
                                        </script>
                                    </td>
                                    <td style="text-align: right;" id="t1col13_<%=reportDtBean.getCounter()%>">
                                        <label id="t1lab3_<%=reportDtBean.getCounter()%>"></label>
                                        <script type="text/javascript">
                                            var t1col9 = parseFloat($('#t1col9_<%=reportDtBean.getCounter()%>').html());
                                            var t1col5 = parseFloat($('#t1col5_<%=reportDtBean.getCounter()%>').html());
                                            var t1ans = (t1col9/t1col5)*100;
                                            $('#t1lab3_<%=reportDtBean.getCounter()%>').html(isNaN(t1ans)? 'N.A.' : t1ans.toFixed(3));
                                        </script>
                                    </td>
                                    <td style="text-align: right;" id="t1col14_<%=reportDtBean.getCounter()%>">
                                        <label id="t1lab4_<%=reportDtBean.getCounter()%>"></label>
                                        <script type="text/javascript">
                                            var t1col10 = parseFloat($('#t1col10_<%=reportDtBean.getCounter()%>').html());
                                            var t1col6 = parseFloat($('#t1col6_<%=reportDtBean.getCounter()%>').html());
                                            var t1ans = (t1col10/t1col6)*100;
                                            $('#t1lab4_<%=reportDtBean.getCounter()%>').html(isNaN(t1ans)? 'N.A.' : t1ans.toFixed(3));
                                        </script>
                                    </td>
                                </tr>
                                <%}%>
                                <tr>
                                    <!--Change Development to Capital by Proshanto-->
                                    <th colspan="2" align="left">Total Capital Budget<input type="hidden" id="tab1cnt" value="<%=countOne%>"/></th>
                                    <th style="text-align: right;" id="t1total1"></th>
                                    <th style="text-align: right;" id="t1total2"></th>
                                    <th style="text-align: right;" id="t1total3"></th>
                                    <th style="text-align: right;" id="t1total4"></th>
                                    <th style="text-align: right;" id="t1total5"></th>
                                    <th style="text-align: right;" id="t1total6"></th>
                                    <th style="text-align: right;" id="t1total7"></th>
                                    <th style="text-align: right;" id="t1total8"></th>
                                    <th style="text-align: right;" id="t1total9"></th>
                                    <th style="text-align: right;" id="t1total10"></th>
                                    <th style="text-align: right;" id="t1total11"></th>
                                    <th style="text-align: right;" id="t1total12"></th>
                                </tr>
                            </table>
                            <%}%>

                            <%if (true && !finYear.equals("")) {%>
                            <%
                                int countFlag;
                                List<SPCommonSearchDataMore> spcsdmsN = dataMoreService.geteGPDataMore("getProcTransactReport", "2", "NCT", finYear, quater.equals("0") ? "0" : quater.split("_")[0], depId, offId, program, project, quater.equals("0") ? "0" : quater.split("_")[1],appType);
                                List<SPCommonSearchDataMore> spcsdmsI = dataMoreService.geteGPDataMore("getProcTransactReport", "2", "ICT", finYear, quater.equals("0") ? "0" : quater.split("_")[0], depId, offId, program, project, quater.equals("0") ? "0" : quater.split("_")[1],appType);
                                List<ProcReportDtBean> data = new ArrayList<ProcReportDtBean>();
                                List<ProcReportDtBean> finaldata = new ArrayList<ProcReportDtBean>();
                                int countOne = 1;
                                //System.out.println("I : " + spcsdmsI.size());//22
                                //System.out.println("N : " + spcsdmsN.size());//25
                                if (spcsdmsI.size() > spcsdmsN.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        data.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6()));
                                        countOne++;
                                    }
                                    for (ProcReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsN) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setnAPPNos(data2.getFieldName3());
                                                prdb.setnAPPEst(data2.getFieldName4());
                                                prdb.setnContractNos(data2.getFieldName5());
                                                prdb.setnContractAmount(data2.getFieldName6());
                                            }
                                        }
                                        finaldata.add(prdb);
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        countFlag = 0;
                                        for (ProcReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6(), null));
                                            countOne++;
                                        }
                                    }
                                }

                                if (spcsdmsN.size() > spcsdmsI.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        data.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6(), null));
                                        countOne++;
                                    }
                                    for (ProcReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsI) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setiAPPNos(data2.getFieldName3());
                                                prdb.setiAPPEst(data2.getFieldName4());
                                                prdb.setiContractNos(data2.getFieldName5());
                                                prdb.setiContractAmount(data2.getFieldName6());
                                            }
                                        }
                                        finaldata.add(prdb);
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        countFlag = 0;
                                        for (ProcReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6()));
                                            countOne++;
                                        }
                                    }
                                }

                                if (spcsdmsN.size() == spcsdmsI.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        data.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6(), null));
                                        countOne++;
                                    }
                                    for (ProcReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsI) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setiAPPNos(data2.getFieldName3());
                                                prdb.setiAPPEst(data2.getFieldName4());
                                                prdb.setiContractNos(data2.getFieldName5());
                                                prdb.setiContractAmount(data2.getFieldName6());
                                            }
                                        }
                                        finaldata.add(prdb);
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        countFlag = 0;
                                        for (ProcReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6()));
                                            countOne++;
                                        }
                                    }
                                }
                            %>
                            <!--Change Revenue to Recurrent by Proshanto-->
                            <div class="inner-tableHead t_space">Recurrent Budget</div>
                            <table class="tableList_3" cellspacing="0" width="100%" id="tab2">
                                <tr>
                                    <th width="4%" rowspan="3" valign="middle">Sl. No </th>
                                    <th width="16%" rowspan="3" valign="middle">Method of<br />
                                        Procurement</th>
                                    <th colspan="12">Procurements </th>
                                </tr>
                                <tr>
                                    <th colspan="2" valign="top">Total Contract Package/Lot estimated as per Proc. Plan (No.)</th>
                                    <!--Change BTN to Nu. by Proshanto-->
                                    <th colspan="2" valign="top">Total estimated amount as per Proc. Plan (Million Nu.)</th>
                                    <th colspan="2" valign="top">Total Contract Package/Lot  awarded/contract signed (No.)</th>
                                    <!--Change BTN to Nu. by Proshanto-->
                                    <th colspan="2" valign="top">Total Contracted amount (Million Nu.)</th>
                                    <th colspan="2" valign="top">Progress as against estimated No. of package/Lot (%)</th>
                                    <th colspan="2" valign="top">Progress as against estimated amount (%)</th>
                                </tr>
                                <tr>
                                    <th width="5%" valign="top">National</th>
                                    <th width="8%" valign="top">International</th>
                                    <th valign="top">National</th>
                                    <th valign="top">International</th>
                                    <th valign="top">National</th>
                                    <th valign="top">International</th>
                                    <th valign="top">National</th>
                                    <th valign="top">International</th>
                                    <th valign="top">National<br />
		(Col 7/3)*100
                                    </th>
                                    <th valign="top">International<br />
		(Col 8/4)*100
                                    </th>
                                    <th valign="top">National<br />
		(Col 9/5)*100
                                    </th>
                                    <th valign="top">International<br />
		(Col 10/6)*100
                                    </th>
                                </tr>
                                <tr>
                                    <th>1</th>
                                    <th>2</th>
                                    <th>3</th>
                                    <th>4</th>
                                    <th>5</th>
                                    <th>6</th>
                                    <th>7</th>
                                    <th>8</th>
                                    <th>9</th>
                                    <th>10</th>
                                    <th>11</th>
                                    <th>12</th>
                                    <th>13</th>
                                    <th>14</th>
                                </tr>
                                <%
                                    for (ProcReportDtBean reportDtBean : finaldata) {
                                %>
                                <tr>
                                    <td style="text-align: left;" id="t2col1_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getCounter()%></td>
                                    <td style="text-align: left;" id="t2col2_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getpMethod()%>(<%=reportDtBean.getpNature()%>)</td>
                                    <td style="text-align: right;" id="t2col3_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPNos() == null ? 0 : reportDtBean.getnAPPNos()%></td>
                                    <td style="text-align: right;" id="t2col4_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPNos() == null ? 0 : reportDtBean.getiAPPNos()%></td>
                                    <td style="text-align: right;" id="t2col5_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPEst() == null ? "N.A." : reportDtBean.getnAPPEst()%></td>
                                    <td style="text-align: right;" id="t2col6_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPEst() == null ? "N.A." : reportDtBean.getiAPPEst()%></td>
                                    <td style="text-align: right;" id="t2col7_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnContractNos() == null ? 0 : reportDtBean.getnContractNos()%></td>
                                    <td style="text-align: right;" id="t2col8_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiContractNos() == null ? 0 : reportDtBean.getiContractNos()%></td>
                                    <td style="text-align: right;" id="t2col9_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnContractAmount() == null ? "N.A." : reportDtBean.getnContractAmount()%></td>
                                    <td style="text-align: right;" id="t2col10_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiContractAmount() == null ? "N.A." : reportDtBean.getiContractAmount()%></td>
                                    <td style="text-align: right;" id="t2col11_<%=reportDtBean.getCounter()%>">
                                        <label id="t2lab1_<%=reportDtBean.getCounter()%>"></label>
                                        <script type="text/javascript">
                                            var t2col7 = parseFloat($('#t2col7_<%=reportDtBean.getCounter()%>').html());
                                            var t2col3 = parseFloat($('#t2col3_<%=reportDtBean.getCounter()%>').html());
                                            var t2ans = (t2col7/t2col3)*100;
                                            $('#t2lab1_<%=reportDtBean.getCounter()%>').html(isNaN(t2ans)? 'N.A.' : t2ans.toFixed(3));
                                        </script>
                                    </td>
                                    <td style="text-align: right;" id="t2col12_<%=reportDtBean.getCounter()%>">
                                        <label id="t2lab2_<%=reportDtBean.getCounter()%>"></label>
                                        <script type="text/javascript">
                                            var t2col8 = parseFloat($('#t2col8_<%=reportDtBean.getCounter()%>').html());
                                            var t2col4 = parseFloat($('#t2col4_<%=reportDtBean.getCounter()%>').html());
                                            var t2ans = (t2col8/t2col4)*100;
                                            $('#t2lab2_<%=reportDtBean.getCounter()%>').html(isNaN(t2ans)? 'N.A.' : t2ans.toFixed(3));
                                        </script>
                                    </td>
                                    <td style="text-align: right;" id="t2col13_<%=reportDtBean.getCounter()%>">
                                        <label id="t2lab3_<%=reportDtBean.getCounter()%>"></label>
                                        <script type="text/javascript">
                                            var t2col9 = parseFloat($('#t2col9_<%=reportDtBean.getCounter()%>').html());
                                            var t2col5 = parseFloat($('#t2col5_<%=reportDtBean.getCounter()%>').html());
                                            var t2ans = (t2col9/t2col5)*100;
                                            $('#t2lab3_<%=reportDtBean.getCounter()%>').html(isNaN(t2ans)? 'N.A.' : t2ans.toFixed(3));
                                        </script>
                                    </td>
                                    <td style="text-align: right;" id="t2col14_<%=reportDtBean.getCounter()%>">
                                        <label id="t2lab4_<%=reportDtBean.getCounter()%>"></label>
                                        <script type="text/javascript">
                                            var t2col10 = parseFloat($('#t2col10_<%=reportDtBean.getCounter()%>').html());
                                            var t2col6 = parseFloat($('#t2col6_<%=reportDtBean.getCounter()%>').html());
                                            var t2ans = (t2col10/t2col6)*100;
                                            $('#t2lab4_<%=reportDtBean.getCounter()%>').html(isNaN(t2ans)? 'N.A.' : t2ans.toFixed(3));
                                        </script>
                                    </td>
                                </tr>
                                <%}%>
                                <tr>
                                    <!--Change Revenue to Recurrent by Proshanto-->
                                    <th colspan="2" align="left">Total Recurrent Budget<input type="hidden" id="tab2cnt" value="<%=countOne%>"/></th>
                                    <th style="text-align: right;" id="t2total1"></th>
                                    <th style="text-align: right;" id="t2total2"></th>
                                    <th style="text-align: right;" id="t2total3"></th>
                                    <th style="text-align: right;" id="t2total4"></th>
                                    <th style="text-align: right;" id="t2total5"></th>
                                    <th style="text-align: right;" id="t2total6"></th>
                                    <th style="text-align: right;" id="t2total7"></th>
                                    <th style="text-align: right;" id="t2total8"></th>
                                    <th style="text-align: right;" id="t2total9"></th>
                                    <th style="text-align: right;" id="t2total10"></th>
                                    <th style="text-align: right;" id="t2total11"></th>
                                    <th style="text-align: right;" id="t2total12"></th>
                                </tr>
                            </table>
                            <%}%>

                            <%if (true && !finYear.equals("")) {%>
                            <%
                                int countFlag;
                                List<SPCommonSearchDataMore> spcsdmsN = dataMoreService.geteGPDataMore("getProcTransactReport", "3", "NCT", finYear, quater.equals("0") ? "0" : quater.split("_")[0], depId, offId, program, project, quater.equals("0") ? "0" : quater.split("_")[1],appType);
                                List<SPCommonSearchDataMore> spcsdmsI = dataMoreService.geteGPDataMore("getProcTransactReport", "3", "ICT", finYear, quater.equals("0") ? "0" : quater.split("_")[0], depId, offId, program, project, quater.equals("0") ? "0" : quater.split("_")[1],appType);
                                List<ProcReportDtBean> data = new ArrayList<ProcReportDtBean>();
                                List<ProcReportDtBean> finaldata = new ArrayList<ProcReportDtBean>();
                                int countOne = 1;
                                // System.out.println("I : " + spcsdmsI.size());//22
                                // System.out.println("N : " + spcsdmsN.size());//25
                                if (spcsdmsI.size() > spcsdmsN.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        data.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6()));
                                        countOne++;
                                    }
                                    for (ProcReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsN) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setnAPPNos(data2.getFieldName3());
                                                prdb.setnAPPEst(data2.getFieldName4());
                                                prdb.setnContractNos(data2.getFieldName5());
                                                prdb.setnContractAmount(data2.getFieldName6());
                                            }
                                        }
                                        finaldata.add(prdb);
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        countFlag = 0;
                                        for (ProcReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6(), null));
                                            countOne++;
                                        }
                                    }
                                }

                                if (spcsdmsN.size() > spcsdmsI.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        data.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6(), null));
                                        countOne++;
                                    }
                                    for (ProcReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsI) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setiAPPNos(data2.getFieldName3());
                                                prdb.setiAPPEst(data2.getFieldName4());
                                                prdb.setiContractNos(data2.getFieldName5());
                                                prdb.setiContractAmount(data2.getFieldName6());
                                            }
                                        }
                                        finaldata.add(prdb);
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        countFlag = 0;
                                        for (ProcReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6()));
                                            countOne++;
                                        }
                                    }
                                }

                                if (spcsdmsN.size() == spcsdmsI.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        data.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6(), null));
                                        countOne++;
                                    }
                                    for (ProcReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsI) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setiAPPNos(data2.getFieldName3());
                                                prdb.setiAPPEst(data2.getFieldName4());
                                                prdb.setiContractNos(data2.getFieldName5());
                                                prdb.setiContractAmount(data2.getFieldName6());
                                            }
                                        }
                                        finaldata.add(prdb);
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        countFlag = 0;
                                        for (ProcReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, data1.getFieldName3(), null, data1.getFieldName4(), null, data1.getFieldName5(), null, data1.getFieldName6()));
                                            countOne++;
                                        }
                                    }
                                }
                            %>
                            <div class="inner-tableHead t_space">Own Fund</div>
                            <table class="tableList_3" cellspacing="0" width="100%" id="tab3">
                                <tr>
                                    <th width="74%" rowspan="3" valign="middle">Sl. No </th>
                                    <th width="16%" rowspan="3" valign="middle">Method of<br />
                                        Procurement</th>
                                    <th colspan="12">Procurements </th>
                                </tr>
                                <tr>
                                    <th colspan="2" valign="top">Total Contract Package/Lot estimated as per Proc. Plan (No.)</th>
                                    <!--Change BTN to Nu. by Proshanto-->
                                    <th colspan="2" valign="top">Total estimated amount as per Proc. Plan (Million Nu.)</th>
                                    <th colspan="2" valign="top">Total Contract Package/Lot  awarded/contract signed (No.)</th>
                                    <!--Change BTN to Nu. by Proshanto-->
                                    <th colspan="2" valign="top">Total Contracted amount (Million Nu.)</th>
                                    <th colspan="2" valign="top">Progress as against estimated No. of package/Lot (%)</th>
                                    <th colspan="2" valign="top">Progress as against estimated amount (%)</th>
                                </tr>
                                <tr>
                                    <th width="5%" valign="top">National</th>
                                    <th width="8%" valign="top">International</th>
                                    <th valign="top">National</th>
                                    <th valign="top">International</th>
                                    <th valign="top">National</th>
                                    <th valign="top">International</th>
                                    <th valign="top">National</th>
                                    <th valign="top">International</th>
                                    <th valign="top">National<br />
		(Col 7/3)*100
                                    </th>
                                    <th valign="top">International<br />
		(Col 8/4)*100
                                    </th>
                                    <th valign="top">National<br />
		(Col 9/5)*100
                                    </th>
                                    <th valign="top">International<br />
		(Col 10/6)*100
                                    </th>
                                </tr>
                                <tr>
                                    <th>1</th>
                                    <th>2</th>
                                    <th>3</th>
                                    <th>4</th>
                                    <th>5</th>
                                    <th>6</th>
                                    <th>7</th>
                                    <th>8</th>
                                    <th>9</th>
                                    <th>10</th>
                                    <th>11</th>
                                    <th>12</th>
                                    <th>13</th>
                                    <th>14</th>
                                </tr>
                                <%
                                    for (ProcReportDtBean reportDtBean : finaldata) {
                                %>
                                <tr>
                                    <td style="text-align: left;" id="t3col1_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getCounter()%></td>
                                    <td style="text-align: left;" id="t3col2_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getpMethod()%>(<%=reportDtBean.getpNature()%>)</td>
                                    <td style="text-align: right;" id="t3col3_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPNos() == null ? 0 : reportDtBean.getnAPPNos()%></td>
                                    <td style="text-align: right;" id="t3col4_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPNos() == null ? 0 : reportDtBean.getiAPPNos()%></td>
                                    <td style="text-align: right;" id="t3col5_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPEst() == null ? "N.A." : reportDtBean.getnAPPEst()%></td>
                                    <td style="text-align: right;" id="t3col6_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPEst() == null ? "N.A." : reportDtBean.getiAPPEst()%></td>
                                    <td style="text-align: right;" id="t3col7_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnContractNos() == null ? 0 : reportDtBean.getnContractNos()%></td>
                                    <td style="text-align: right;" id="t3col8_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiContractNos() == null ? 0 : reportDtBean.getiContractNos()%></td>
                                    <td style="text-align: right;" id="t3col9_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnContractAmount() == null ? "N.A." : reportDtBean.getnContractAmount()%></td>
                                    <td style="text-align: right;" id="t3col10_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiContractAmount() == null ? "N.A." : reportDtBean.getiContractAmount()%></td>
                                    <td style="text-align: right;" id="t3col11_<%=reportDtBean.getCounter()%>">
                                        <label id="t3lab1_<%=reportDtBean.getCounter()%>"></label>
                                        <script type="text/javascript">
                                            var t3col7 = parseFloat($('#t3col7_<%=reportDtBean.getCounter()%>').html());
                                            var t3col3 = parseFloat($('#t3col3_<%=reportDtBean.getCounter()%>').html());
                                            var t3ans = (t3col7/t3col3)*100;
                                            $('#t3lab1_<%=reportDtBean.getCounter()%>').html(isNaN(t3ans)? 'N.A.' : t3ans.toFixed(3));
                                        </script>
                                    </td>
                                    <td style="text-align: right;" id="t3col12_<%=reportDtBean.getCounter()%>">
                                        <label id="t3lab2_<%=reportDtBean.getCounter()%>"></label>
                                        <script type="text/javascript">
                                            var t3col8 = parseFloat($('#t3col8_<%=reportDtBean.getCounter()%>').html());
                                            var t3col4 = parseFloat($('#t3col4_<%=reportDtBean.getCounter()%>').html());
                                            var t3ans = (t3col8/t3col4)*100;
                                            $('#t3lab2_<%=reportDtBean.getCounter()%>').html(isNaN(t3ans)? 'N.A.' : t3ans.toFixed(3));
                                        </script>
                                    </td>
                                    <td style="text-align: right;" id="t3col13_<%=reportDtBean.getCounter()%>">
                                        <label id="t3lab3_<%=reportDtBean.getCounter()%>"></label>
                                        <script type="text/javascript">
                                            var t3col9 = parseFloat($('#t3col9_<%=reportDtBean.getCounter()%>').html());
                                            var t3col5 = parseFloat($('#t3col5_<%=reportDtBean.getCounter()%>').html());
                                            var t3ans = (t3col9/t3col5)*100;
                                            $('#t3lab3_<%=reportDtBean.getCounter()%>').html(isNaN(t3ans)? 'N.A.' : t3ans.toFixed(3));
                                        </script>
                                    </td>
                                    <td style="text-align: right;" id="t3col14_<%=reportDtBean.getCounter()%>">
                                        <label id="t3lab4_<%=reportDtBean.getCounter()%>"></label>
                                        <script type="text/javascript">
                                            var t3col10 = parseFloat($('#t3col10_<%=reportDtBean.getCounter()%>').html());
                                            var t3col6 = parseFloat($('#t3col6_<%=reportDtBean.getCounter()%>').html());
                                            var t3ans = (t3col10/t3col6)*100;
                                            $('#t3lab4_<%=reportDtBean.getCounter()%>').html(isNaN(t3ans)? 'N.A.' : t3ans.toFixed(3));
                                        </script>
                                    </td>
                                </tr>
                                <%}%>
                                <tr>
                                    <th colspan="2" align="left">Total Own Fund<input type="hidden" id="tab3cnt" value="<%=countOne%>"/></th>
                                    <th style="text-align: right;" id="t3total1"></th>
                                    <th style="text-align: right;" id="t3total2"></th>
                                    <th style="text-align: right;" id="t3total3"></th>
                                    <th style="text-align: right;" id="t3total4"></th>
                                    <th style="text-align: right;" id="t3total5"></th>
                                    <th style="text-align: right;" id="t3total6"></th>
                                    <th style="text-align: right;" id="t3total7"></th>
                                    <th style="text-align: right;" id="t3total8"></th>
                                    <th style="text-align: right;" id="t3total9"></th>
                                    <th style="text-align: right;" id="t3total10"></th>
                                    <th style="text-align: right;" id="t3total11"></th>
                                    <th style="text-align: right;" id="t3total12"></th>
                                </tr>
                                <tr style="border-color: #ffffff;">
                                    <td colspan="14"></td>
                                </tr>
                                <tr>
                                    <th colspan="2" align="left">GRAND TOTAL</th>
                                    <th style="text-align: right;" id="total1"></th>
                                    <th style="text-align: right;" id="total2"></th>
                                    <th style="text-align: right;" id="total3"></th>
                                    <th style="text-align: right;" id="total4"></th>
                                    <th style="text-align: right;" id="total5"></th>
                                    <th style="text-align: right;" id="total6"></th>
                                    <th style="text-align: right;" id="total7"></th>
                                    <th style="text-align: right;" id="total8"></th>
                                    <th style="text-align: right;" id="total9"></th>
                                    <th style="text-align: right;" id="total10"></th>
                                    <th style="text-align: right;" id="total11"></th>
                                    <th style="text-align: right;" id="total12"></th>
                                </tr>
                            </table>
                            <%}%>
                            <%if (finYear.equals("")) {%>
                            <table class="tableList_3" cellspacing="0" width="100%">
                                <tr>
                                    <td style="text-align:center;font-weight: bold;">
                                        No Data Found
                                    </td>
                                </tr>
                            </table>
                            <%}%>
                        </div>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            var tab1cnt = $('#tab1cnt').val();
            var t1total1 = 0.0;
            var t1total2 = 0.0;
            var t1total3 = 0.0;
            var t1total4 = 0.0;
            var t1total5 = 0.0;
            var t1total6 = 0.0;
            var t1total7 = 0.0;
            var t1total8 = 0.0;
            var t1total9 = 0.0;
            var t1total10 = 0.0;
            var t1total11 = 0.0;
            var t1total12 = 0.0;
            for(var i = 1; i < tab1cnt; i++){
                t1total1 = t1total1 + parseFloat(isNaN($('#t1col3_'+i).html()) ? 0.0 : $('#t1col3_'+i).html());
                t1total2 = t1total2 + parseFloat(isNaN($('#t1col4_'+i).html()) ? 0.0 : $('#t1col4_'+i).html());
                t1total3 = t1total3 + parseFloat(isNaN($('#t1col5_'+i).html()) ? 0.0 : $('#t1col5_'+i).html());
                t1total4 = t1total4 + parseFloat(isNaN($('#t1col6_'+i).html()) ? 0.0 : $('#t1col6_'+i).html());
                t1total5 = t1total5 + parseFloat(isNaN($('#t1col7_'+i).html()) ? 0.0 : $('#t1col7_'+i).html());
                t1total6 = t1total6 + parseFloat(isNaN($('#t1col8_'+i).html()) ? 0.0 : $('#t1col8_'+i).html());
                t1total7 = t1total7 + parseFloat(isNaN($('#t1col9_'+i).html()) ? 0.0 : $('#t1col9_'+i).html());
                t1total8 = t1total8 + parseFloat(isNaN($('#t1col10_'+i).html()) ? 0.0 : $('#t1col10_'+i).html());
                t1total9 = t1total9 + parseFloat(isNaN($('#t1lab1_'+i).html()) ? 0.0 : $('#t1lab1_'+i).html());
                t1total10 = t1total10 + parseFloat(isNaN($('#t1lab2_'+i).html()) ? 0.0 : $('#t1lab2_'+i).html());
                t1total11 = t1total11 + parseFloat(isNaN($('#t1lab3_'+i).html()) ? 0.0 : $('#t1lab3_'+i).html());
                t1total12 = t1total12 + parseFloat(isNaN($('#t1lab4_'+i).html()) ? 0.0 : $('#t1lab4_'+i).html());
            }
            $('#t1total1').html(t1total1);
            $('#t1total2').html(t1total2);
            $('#t1total3').html(t1total3.toFixed(3));
            $('#t1total4').html(t1total4.toFixed(3));
            $('#t1total5').html(t1total5);
            $('#t1total6').html(t1total6);
            $('#t1total7').html(t1total7.toFixed(3));
            $('#t1total8').html(t1total8.toFixed(3));
            //for total
            t1total9 = (parseFloat(t1total5)/(parseFloat(t1total1))*100);
            t1total10 = (parseFloat(t1total6)/(parseFloat(t1total2))*100);
            t1total11 = (parseFloat(t1total7)/(parseFloat(t1total3))*100);            
            t1total12 = (parseFloat(t1total8)/(parseFloat(t1total4))*100);            
            $('#t1total9').html(isNaN(t1total9)?0.0:t1total9.toFixed(3));
            $('#t1total10').html(isNaN(t1total10)?0.0:t1total10.toFixed(3));
            $('#t1total11').html(isNaN(t1total11)?0.0:t1total11.toFixed(3));
            $('#t1total12').html(isNaN(t1total12)?0.0:t1total12.toFixed(3));
      
            var tab2cnt = $('#tab2cnt').val();
            var t2total1 = 0.0;
            var t2total2 = 0.0;
            var t2total3 = 0.0;
            var t2total4 = 0.0;
            var t2total5 = 0.0;
            var t2total6 = 0.0;
            var t2total7 = 0.0;
            var t2total8 = 0.0;
            var t2total9 = 0.0;
            var t2total10 = 0.0;
            var t2total11 = 0.0;
            var t2total12 = 0.0;
            for(var i = 1; i < tab2cnt; i++){
                t2total1 = t2total1 + parseFloat(isNaN($('#t2col3_'+i).html()) ? 0.0 : $('#t2col3_'+i).html());
                t2total2 = t2total2 + parseFloat(isNaN($('#t2col4_'+i).html()) ? 0.0 : $('#t2col4_'+i).html());
                t2total3 = t2total3 + parseFloat(isNaN($('#t2col5_'+i).html()) ? 0.0 : $('#t2col5_'+i).html());
                t2total4 = t2total4 + parseFloat(isNaN($('#t2col6_'+i).html()) ? 0.0 : $('#t2col6_'+i).html());
                t2total5 = t2total5 + parseFloat(isNaN($('#t2col7_'+i).html()) ? 0.0 : $('#t2col7_'+i).html());
                t2total6 = t2total6 + parseFloat(isNaN($('#t2col8_'+i).html()) ? 0.0 : $('#t2col8_'+i).html());
                t2total7 = t2total7 + parseFloat(isNaN($('#t2col9_'+i).html()) ? 0.0 : $('#t2col9_'+i).html());
                t2total8 = t2total8 + parseFloat(isNaN($('#t2col10_'+i).html()) ? 0.0 : $('#t2col10_'+i).html());
                t2total9 = t2total9 + parseFloat(isNaN($('#t2lab1_'+i).html()) ? 0.0 : $('#t2lab1_'+i).html());
                t2total10 = t2total10 + parseFloat(isNaN($('#t2lab2_'+i).html()) ? 0.0 : $('#t2lab2_'+i).html());
                t2total11 = t2total11 + parseFloat(isNaN($('#t2lab3_'+i).html()) ? 0.0 : $('#t2lab3_'+i).html());
                t2total12 = t2total12 + parseFloat(isNaN($('#t2lab4_'+i).html()) ? 0.0 : $('#t2lab4_'+i).html());
            }
            $('#t2total1').html(t2total1);
            $('#t2total2').html(t2total2);
            $('#t2total3').html(t2total3.toFixed(3));
            $('#t2total4').html(t2total4.toFixed(3));
            $('#t2total5').html(t2total5);
            $('#t2total6').html(t2total6);
            $('#t2total7').html(t2total7.toFixed(3));
            $('#t2total8').html(t2total8.toFixed(3));
            //            for total            
            t2total9 = (parseFloat(t2total5)/(parseFloat(t2total1))*100);
            t2total10 = (parseFloat(t2total6)/(parseFloat(t2total2))*100);
            t2total11 = (parseFloat(t2total7)/(parseFloat(t2total3))*100);
            t2total12 = (parseFloat(t2total8)/(parseFloat(t2total4))*100);
            $('#t2total9').html(isNaN(t2total9)?0.0:t2total9.toFixed(3));
            $('#t2total10').html(isNaN(t2total10)?0.0:t2total10.toFixed(3));
            $('#t2total11').html(isNaN(t2total11)?0.0:t2total11.toFixed(3));
            $('#t2total12').html(isNaN(t2total12)?0.0:t2total12.toFixed(3));
        
            var tab3cnt = $('#tab3cnt').val();
            var t3total1 = 0.0;
            var t3total2 = 0.0;
            var t3total3 = 0.0;
            var t3total4 = 0.0;
            var t3total5 = 0.0;
            var t3total6 = 0.0;
            var t3total7 = 0.0;
            var t3total8 = 0.0;
            var t3total9 = 0.0;
            var t3total10 = 0.0;
            var t3total11 = 0.0;
            var t3total12 = 0.0;
            for(var i = 1; i < tab3cnt; i++){
                t3total1 = t3total1 + parseFloat(isNaN($('#t3col3_'+i).html()) ? 0.0 : $('#t3col3_'+i).html());
                t3total2 = t3total2 + parseFloat(isNaN($('#t3col4_'+i).html()) ? 0.0 : $('#t3col4_'+i).html());
                t3total3 = t3total3 + parseFloat(isNaN($('#t3col5_'+i).html()) ? 0.0 : $('#t3col5_'+i).html());
                t3total4 = t3total4 + parseFloat(isNaN($('#t3col6_'+i).html()) ? 0.0 : $('#t3col6_'+i).html());
                t3total5 = t3total5 + parseFloat(isNaN($('#t3col7_'+i).html()) ? 0.0 : $('#t3col7_'+i).html());
                t3total6 = t3total6 + parseFloat(isNaN($('#t3col8_'+i).html()) ? 0.0 : $('#t3col8_'+i).html());
                t3total7 = t3total7 + parseFloat(isNaN($('#t3col9_'+i).html()) ? 0.0 : $('#t3col9_'+i).html());
                t3total8 = t3total8 + parseFloat(isNaN($('#t3col10_'+i).html()) ? 0.0 : $('#t3col10_'+i).html());
                t3total9 = t3total9 + parseFloat(isNaN($('#t3lab1_'+i).html()) ? 0.0 : $('#t3lab1_'+i).html());
                t3total10 = t3total10 + parseFloat(isNaN($('#t3lab2_'+i).html()) ? 0.0 : $('#t3lab2_'+i).html());
                t3total11 = t3total11 + parseFloat(isNaN($('#t3lab3_'+i).html()) ? 0.0 : $('#t3lab3_'+i).html());
                t3total12 = t3total12 + parseFloat(isNaN($('#t3lab4_'+i).html()) ? 0.0 : $('#t3lab4_'+i).html());
            }
            $('#t3total1').html(t3total1);
            $('#t3total2').html(t3total2);
            $('#t3total3').html(t3total3.toFixed(3));
            $('#t3total4').html(t3total4.toFixed(3));
            $('#t3total5').html(t3total5);
            $('#t3total6').html(t3total6);
            $('#t3total7').html(t3total7.toFixed(3));
            $('#t3total8').html(t3total8.toFixed(3));
            //            for total            
            t3total9 = (parseFloat(t3total5)/(parseFloat(t3total1))*100);
            t3total10 = (parseFloat(t3total6)/(parseFloat(t3total2))*100);
            t3total11 = (parseFloat(t3total7)/(parseFloat(t3total3))*100);
            t3total12 = (parseFloat(t3total8)/(parseFloat(t3total4))*100);
            $('#t3total9').html(isNaN(t3total9)?0.0:t3total9.toFixed(3));
            $('#t3total10').html(isNaN(t3total10)?0.0:t3total10.toFixed(3));
            $('#t3total11').html(isNaN(t3total11)?0.0:t3total11.toFixed(3));
            $('#t3total12').html(isNaN(t3total12)?0.0:t3total12.toFixed(3));
            //
            $('#total1').html((t1total1+t2total1+t3total1));
            $('#total2').html((t1total2+t2total2+t3total2));
            $('#total3').html((t1total3+t2total3+t3total3).toFixed(3));
            $('#total4').html((t1total4+t2total4+t3total4).toFixed(3));
            $('#total5').html((t1total5+t2total5+t3total5));
            $('#total6').html((t1total6+t2total6+t3total6));
            $('#total7').html((t1total7+t2total7+t3total7).toFixed(3));
            $('#total8').html((t1total8+t2total8+t3total8).toFixed(3));
            //            for total
            if(parseFloat($('#total5').html())!=0.0 && parseFloat($('#total1').html())!=0.0){
                $('#total9').html((parseFloat($('#total5').html())/(parseFloat($('#total1').html()))*100).toFixed(3));
            }else{
                $('#total9').html("0.0");
            }

            if(parseFloat($('#total6').html())!=0.0 && parseFloat($('#total2').html())!=0.0){
                $('#total10').html((parseFloat($('#total6').html())/(parseFloat($('#total2').html()))*100).toFixed(3));
            }else{
                $('#total10').html("0.0");
            }
            if(parseFloat($('#total7').html())!=0.0 && parseFloat($('#total3').html())!=0.0){
                $('#total11').html((parseFloat($('#total7').html())/(parseFloat($('#total3').html()))*100).toFixed(3));
            }else{
                $('#total11').html("0.0");
            }
            
            if(parseFloat($('#total8').html())!=0.0 && parseFloat($('#total4').html())!=0.0){
                $('#total12').html((parseFloat($('#total8').html())/(parseFloat($('#total4').html()))*100).toFixed(3));
            }else{
                $('#total12').html("0.0");
            }
            $('#tab1 tr').each(function() {
                for(var i = 0;i<this.cells.length;i++){
                    if(this.cells[i].innerHTML=="0.00"){
                        this.cells[i].innerHTML = '0';
                    }
                    if(this.cells[i].innerHTML.toString().indexOf('label')!=-1){
                        if($(this.cells[i]).children()[0].innerHTML=="0.00"){
                            $(this.cells[i]).children()[0].innerHTML = '0';
                        }
                        //this.cells[i].innerHTML = '0';
                    }
                }                
                //$(this).html($(this).find("td").html());
            });
            $('#tab2 tr').each(function() {
                for(var i = 0;i<this.cells.length;i++){
                    if(this.cells[i].innerHTML=="0.00"){
                        this.cells[i].innerHTML = '0';
                    }
                    if(this.cells[i].innerHTML.toString().indexOf('label')!=-1){
                        if($(this.cells[i]).children()[0].innerHTML=="0.00"){
                            $(this.cells[i]).children()[0].innerHTML = '0';
                        }
                        //this.cells[i].innerHTML = '0';
                    }
                }
                //var customerId = $(this).find("td:first").html();
            });
            $('#tab3 tr').each(function() {
                for(var i = 0;i<this.cells.length;i++){
                    if(this.cells[i].innerHTML=="0.00"){
                        this.cells[i].innerHTML = '0';
                    }
                    if(this.cells[i].innerHTML.toString().indexOf('label')!=-1){
                        if($(this.cells[i]).children()[0].innerHTML=="0.00"){
                            $(this.cells[i]).children()[0].innerHTML = '0';
                        }
                        //this.cells[i].innerHTML = '0';
                    }
                }   
                //var customerId = $(this).find("td:first").html();
            });

            $(document).ready(function() {

                $("#print").click(function() {
                    //alert('sa');
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });
            
            function printElem(options){
                //alert(options);
                /*$('#print').hide();
                $('#saveASPDF').hide();
                $('#goBack').hide();*/
                $('#print_area').printElement(options);
                /*$('#print').show();
                $('#saveASPDF').show();
                $('#goBack').show();*/
                //$('#trLast').hide();
            }

        </script>
        <%if (isNotPDF) {%>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <%}%>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabReport");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>   
</html>
