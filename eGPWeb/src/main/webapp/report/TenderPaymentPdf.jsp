<%--
    Document   : TenderPaymentPdf
    Created on : Jul 15, 2011, 10:52:11 AM
    Author     : shreyansh
--%>

<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />

    </head>
    <body>
        <%
                    String deptment = "";
                    String cmbOffice = "";
                    String bankName = "";
                    String amount = "";
                    String cmbamount = "";
                    String DateTO = "";
                    String DateFrom = "";
                    String paymentfor = "";
                    String TenderId= "";
                    String branchName="";
                    String paymentMode= "All";
                    if (request.getParameter("txtdepartmentid") != null) {
                        deptment = request.getParameter("txtdepartmentid");
                    }
                    if (request.getParameter("cmbOffice") != null) {
                        cmbOffice = request.getParameter("cmbOffice");
                    }
                    if (request.getParameter("bankName") != null) {
                        bankName = request.getParameter("bankName");
                    }
                    if (request.getParameter("amount") != null) {
                        amount = request.getParameter("amount");
                    }
                    if (request.getParameter("cmbamount") != null) {
                        cmbamount = request.getParameter("cmbamount");
                    }
                    if (request.getParameter("DateTO") != null) {
                        DateTO = request.getParameter("DateTO");
                    }
                    if (request.getParameter("DateFrom") != null) {
                        DateFrom = request.getParameter("DateFrom");
                    }
                    if (request.getParameter("paymentfor") != null) {
                        paymentfor = request.getParameter("paymentfor");
                    }
                    if (request.getParameter("tenderid") != null) {
                        TenderId = request.getParameter("tenderid");
                    }
                    if (request.getParameter("paymentMode") != null) {
                        paymentMode = request.getParameter("paymentMode");
                    }
                    if (request.getParameter("branchName") != null) {
                       branchName = request.getParameter("branchName");
                    }
                    String strPageNo = request.getParameter("strPageNo");
                    int pageNo = Integer.parseInt(strPageNo);
                    if(paymentfor.contains("Document")){
                        paymentfor="Document Fees";
                    }


                    String strOffset = request.getParameter("strOffset");
                    int recordOffset = Integer.parseInt(strOffset);

                    String isPDF = "abc";
                    if (request.getParameter("isPDF") != null) {
                        isPDF = request.getParameter("isPDF");
                    }
        %>

        <div class="pageHead_1">Document Fees Report</div>

        <div id="resultDiv" style="display: block;">
            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                <tr>
                    <th width="3%" class="t-align-center">S.No</th>
                    <th width="15%" class="t-align-center">Tender ID,Reference No.,<br />
                        Lot No. </th>
                    <th width="20%" class="t-align-center">Ministry,
                        Division,
                        <br />
                        Organization, Procuring Agency </th>
                    <th width="15%" class="t-align-center">Bank Name,<br />
                        Branch Name </th>
                    <th width="18%" class="t-align-center">Amount <br />
                        (In BD. Tk.)</th>
                    <th width="18%" class="t-align-center">Payment Date<br />
                        and Time</th>
                    <th width="18%" class="t-align-center">Payment Type</th>
                    <th width="14%" class="t-align-center">Bidder / Consultant</th>

                </tr>
                <%
                            CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                            List<SPCommonSearchDataMore> list = csdms.geteGPData("getTenderPaymentLisForAdmin", strPageNo, strOffset, deptment, cmbOffice, bankName, cmbamount, amount, DateTO, DateFrom, paymentfor, TenderId, branchName, paymentMode, null, null, null, null, null);

      for(int i = 0; i < list.size(); i++){ %>
                <tr>
                    <td class="t-align-center"><%=list.get(i).getFieldName19() %></td>
                    <td class="t-align-left"><%=list.get(i).getFieldName1()%>,<%=list.get(i).getFieldName2()%>,<%=list.get(i).getFieldName3()%></td>
                    <td class="t-align-left"><%=list.get(i).getFieldName7()%>,<%=list.get(i).getFieldName6()%>,<%=list.get(i).getFieldName5()%>,<%=list.get(i).getFieldName4()%></td>
                    <% if(list.get(i).getFieldName9().trim().equalsIgnoreCase("Online")){%>
                        <td class="t-align-center"><%=list.get(i).getFieldName8()%></td>
                    <%}else{%>
                        <td class="t-align-left"><%=list.get(i).getFieldName8()%>,<%=list.get(i).getFieldName9()%></td>
                    <%}%>
                    <td style="text-align: right;"><%=list.get(i).getFieldName10()%></td>
                    <td class="t-align-center"><%=list.get(i).getFieldName11()%></td>
                    <% if(list.get(i).getFieldName9().trim().equalsIgnoreCase("Online")){%>
                        <td class="t-align-center"><%=list.get(i).getFieldName9()%></td>
                    <%}else{%>
                        <td class="t-align-left">Offline</td>
                    <%}%>
                    <td class="t-align-center"><%=list.get(i).getFieldName12()%></td>
                </tr>
                <% }
                %>
            </table>
        </div>

    </body>
</html>
