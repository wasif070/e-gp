<%-- 
    Document   : eGPStatistics
    Created on : Jun 28, 2012, 3:13:41 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Statistics</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%
                    CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> egpStatistics = null;
                    egpStatistics = dataMoreService.geteGPDataMore("geteGPStatistics");
                    List<SPCommonSearchDataMore> FinancialYear = null;
                    FinancialYear = dataMoreService.geteGPDataMore("getCurrentYear");
                    Date d = new Date();
                    DateFormat dt = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        %>
        <div class="tabPanelArea_1">
            <div class="pageHead_1">e-GP Statistics as on <%=dt.format(d)%></div>
            <div class="formBg_1">
                        <table width="100%" cellspacing="8" cellpadding="0"  class="formStyle_1">
                            <tr>
                                <td class="t-align-right" colspan="2">

                                    <a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('-1');">Save as PDF</a>
                                    &nbsp;

                                    <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
                                </td>
                            </tr>
                        </table>
                </div>
        <div  id="print_area">
              
            <table border="1" cellspacing="10" cellpadding="0" class="tableList_1 t_space" id="resultTable" width="100%">

                <tr>
                    <th width="40%">Parameter</th>
                    <th width="20%">National</th>
                    <th width="20%">International</th>
                    <th width="20%">Total</th>
                </tr>
                <tr>
                    <td>Registered Bidders/Consultants</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName1().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName1().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName1().split("#")[2]%></td>
                </tr>
                <tr><td>Valid Bidders/Consultants</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName2().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName2().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName2().split("#")[2]%></td>
                </tr>
                <tr><td>Participating Bidders/Consultants</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName3().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName3().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName3().split("#")[2]%></td>
                </tr>
<!--                <tr><td>Registered Government Organizations</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName4().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName4().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName4().split("#")[2]%></td>
                </tr>-->
                <tr><td>Registered Procuring Agencies</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName5().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName5().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName5().split("#")[2]%></td>
                </tr>
                <tr><td>Registered Government Procurement Users </td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName6().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName6().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName6().split("#")[2]%></td>
                </tr>
                <tr><td>Procuring Agencies using e-GP for procurement</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName7().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName7().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName7().split("#")[2]%></td>
                </tr>
                <tr><td>Registered Development Partners</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName8().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName8().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName8().split("#")[2]%></td>
                </tr>
                <tr style="display: none"><td>Registered Media</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName9().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName9().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName9().split("#")[2]%></td>
                </tr>
                <tr><td>Registered Financial Institutions</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName10().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName10().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName10().split("#")[2]%></td>
                </tr>
                <tr><td>Procurement Packages</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName11().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName11().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName11().split("#")[2]%></td>
                </tr>
                <tr><td>Procurement Notice published</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName12().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName12().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName12().split("#")[2]%></td>
                </tr>
                <tr><td>Goods Tender  published</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName13().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName13().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName13().split("#")[2]%></td>
                </tr>
                <tr><td>Works Tender  published</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName14().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName14().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName14().split("#")[2]%></td>
                </tr>
                <tr><td>Services REOI/RFP  published</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName15().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName15().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName15().split("#")[2]%></td>
                </tr>
                <tr><td>Letter of Acceptance Issued</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName16().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName16().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName16().split("#")[2]%></td>
                </tr>
                <tr><td>Contracts Awarded</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName17().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName17().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName17().split("#")[2]%></td>
                </tr>
                <tr><td>Contracts Canceled</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName18().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName18().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName18().split("#")[2]%></td>
                </tr>
                <tr><td>Contracts Completed</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName19().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName19().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName19().split("#")[2]%></td>
                </tr>
                <tr><td>Value of Procurement </td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName20().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName20().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName20().split("#")[2]%></td>
                </tr>
                <tr><td>Total Amount of Procurement in fiscal year <%=FinancialYear.get(0).getFieldName1()%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName21().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName21().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName21().split("#")[2]%></td>
                </tr>
                <tr><td>No. of Complaints lodged</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName22().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName22().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName22().split("#")[2]%></td>
                </tr>
                <tr><td>No. of Complaints Resolved</td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName23().split("#")[0]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName23().split("#")[1]%></td>
                    <td class="t-align-center"><%=egpStatistics.get(0).getFieldName23().split("#")[2]%></td>
                </tr>
            </table>
            </div>
        </div>
                              <!--For Generate PDF  Starts-->
                <form id="formstyle" action="" method="post" name="formstyle">
                    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                    <%
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                        String appenddate = dateFormat1.format(new Date());
                    %>
                    <input type="hidden" name="fileName" id="fileName" value="eGPStatistics_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="eGPStatisticsReport" />
                </form>
                <!--For Generate PDF  Ends-->
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

         $(document).ready(function() {

                $("#print").click(function() {
                    //alert('sa');
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });

            function printElem(options){
                //alert(options);
                /*$('#print').hide();
                $('#saveASPDF').hide();
                $('#goBack').hide();*/
                $('#print_area').printElement(options);
                /*$('#print').show();
                $('#saveASPDF').show();
                $('#goBack').show();*/
                //$('#trLast').hide();
            }
            
        function getHeading(id)
        {
            var SubHeading = '';
            switch (parseInt(id)) {
                case 1:
                    SubHeading = "Registered Tenderers/Consultants";
                    break;
                case 2:
                    SubHeading = "Valid Tenderers/Consultants";
                    break;
                case 3:
                    SubHeading = "Participating Tenderers/Consultants";
                    break;
                case 4:
                    SubHeading = "Government Organizations Registered";
                    break;
                case 5:
                    SubHeading = "Procuring Agencies Registered";
                    break;
                case 6:
                    SubHeading = "Registered Government Procurement Users";
                    break;
                case 7:
                    SubHeading = "Procuring Entities using e-GP for procurement";
                    break;
                case 8:
                    SubHeading = "Registered Development Partners";
                    break;
                case 9:
                    SubHeading = "Registered Media";
                    break;
                case 10:
                    SubHeading = "Registered Banks";
                    break;
                case 11:
                    SubHeading = "Procurement Packages";
                    break;
                case 12:
                    SubHeading = "Procurement Notice published";
                    break;
                case 13:
                    SubHeading = "Goods Tender  published";
                    break;
                case 14:
                    SubHeading = "Works Tender  published";
                    break;
                case 15:
                    SubHeading = "Services REOI/RFP  published";
                    break;
                case 16:
                    SubHeading = "LOA Issued";
                    break;
                case 17:
                    SubHeading = "Contracts Awarded";
                    break;
                case 18:
                    SubHeading = "Contracts Cancelled";
                    break;
                case 19:
                    SubHeading = "Contracts Completed";
                    break;
                case 20:
                    SubHeading = "Value of Procurement";
                    break;
                case 21:
                    SubHeading = "Total Amount of Procurement in fiscal year";
                    break;

                }
                return SubHeading;
        }
    </script>
</html>
