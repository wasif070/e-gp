<%-- 
    Document   : AppMIS
    Created on : Apr 20, 2011, 6:22:26 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.AppMISService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MIS: Procurement Statistics</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            function onSelectionOfTreeOff(){
                $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId:$('#txtdepartmentid').val(),funName:'Office'}, function(j){
                    $("select#cmboffice").html(j);
                    var cmb = document.getElementById('cmboffice');
                    $("#offName").val(cmb.options[cmb.selectedIndex].text);
                });
            }

            function setOffName(){
                var cmb = document.getElementById('cmboffice');
                $("#offName").val(cmb.options[cmb.selectedIndex].text);
            }

            function changeData(data){
                if(data=='1'){
                    $('#t1').addClass('sMenu');
                    $('#t2').removeClass('sMenu');
                    $('#t3').removeClass('sMenu');
                    $('#tab1').show();
                    $('#tab2_1').hide();
                    $('#tab3').hide();
                    $('#tab4').hide();
                    $('#tab2_2').hide();
                }
                if(data=='2_1'){
                    $('#t1').addClass('sMenu');
                    $('#t2').removeClass('sMenu');
                    $('#t3').removeClass('sMenu');
                    $('#tab1').hide();
                    $('#tab2_2').hide();
                    $('#tab2_1').show();
                    $('#tab3').hide();
                    $('#tab4').hide();
                }
                if(data=='2_2'){
                    $('#t1').addClass('sMenu');
                    $('#t2').removeClass('sMenu');
                    $('#t3').removeClass('sMenu');
                    $('#tab1').hide();
                    $('#tab2_2').show();
                    $('#tab3').hide();
                    $('#tab4').hide();
                    $('#tab2_1').hide();
                }
                if(data=='3'){
                    $('#t1').removeClass('sMenu');
                    $('#t2').addClass('sMenu');
                    $('#t3').removeClass('sMenu');
                    $('#tab1').hide();
                    $('#tab2_1').hide();
                    $('#tab3').show();
                    $('#tab4').hide();
                    $('#tab2_2').hide();
                }
                if(data=='4'){
                    $('#t1').removeClass('sMenu');
                    $('#t2').removeClass('sMenu');
                    $('#t3').addClass('sMenu');
                    $('#tab1').hide();
                    $('#tab2_1').hide();
                    $('#tab3').hide();
                    $('#tab4').show();
                    $('#tab2_2').hide();
                }
            }
            function onSelectionOfTreeDesig(){}
//            function showHide(){}
            function checkCondition(){}
            function checkHOPE(){}

            function validate(){
                $(".err").remove();
                var valid = true;                
                if($('#txtdepartmentid')!=null && $('#txtdepartmentid').val()==""){
                     $("#txtdepartmentid").parent().append("<div class='err' style='color:red;'>Please select Hierarchy Node</div>");
                     valid = false;
                }
                if($('#txtDepartmentType').val()!=null && ($('#txtDepartmentType').val()=="CCGP" || $('#txtDepartmentType').val()=="Cabinet Div")){
                    $("#txtDepartmentType").parent().append("<div class='err' style='color:red;'>You can't search on CCGP or Cabinet Division</div>");
                     valid = false;
                }
                return valid;
            }
        </script>
    </head>
    <body onload="hide();">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%
                    List<SPCommonSearchDataMore> tab1 = new ArrayList<SPCommonSearchDataMore>();
                    List<SPCommonSearchDataMore> tab2_1 = new ArrayList<SPCommonSearchDataMore>();
                    List<SPCommonSearchDataMore> tab2_2 = new ArrayList<SPCommonSearchDataMore>();
                    List<SPCommonSearchDataMore> tab3 = new ArrayList<SPCommonSearchDataMore>();
                    List<SPCommonSearchDataMore> tab4 = new ArrayList<SPCommonSearchDataMore>();
                    AppMISService mISService = (AppMISService) AppContext.getSpringBean("AppMISService");
                    List<Object[]> pecmbLists = null;
                    if ("Generate".equals(request.getParameter("generate")) || "4".equals(objUserTypeId.toString())) {
                        try{
                            String depId = request.getParameter("depId");
                            String offId = request.getParameter("offId");
                            /*if (offId != null && !"0".equals(offId)) {
                            depId = null;
                            }*/
                            if( "4".equals(objUserTypeId.toString())){
                                pecmbLists = mISService.getOfficeList(session.getAttribute("userId").toString(), objUserTypeId.toString());
                                if(!pecmbLists.isEmpty()){
                                    offId = pecmbLists.get(0)[0].toString();
                                }
                            }
                            tab1 = mISService.getAppMIS("GetAppCount", depId, offId);
                            tab2_1 = mISService.getAppMIS("GetAppClassifiedCount_Pending", depId, offId);
                            tab2_2 = mISService.getAppMIS("GetAppClassifiedCount_Approved", depId, offId);
                            tab3 = mISService.getAppMIS("GetTenderClassifiedCount", depId, offId);
                            tab4 = mISService.getAppMIS("GetTenderContractInfo", depId, offId);
                        }catch(Exception e){
                            System.out.println("Error in AppMis.jsp "+e.toString());
                        }finally{
                            MakeAuditTrailService auditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            auditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(request.getSession().getAttribute("userId").toString()), "userId", EgpModule.Report.getName(), "Generate Annual Procurement Plan Report", "");
                        }
                   }
                    boolean chk1 = true;
                    boolean chk2_1 = true;
                    boolean chk2_2 = true;
                    boolean chk3 = true;
                    boolean chk4 = true;
                    if (tab1.isEmpty()) {
                        chk1 = false;
                    }
                    if (tab2_1.isEmpty()) {
                        chk2_1 = false;
                    }
                    if (tab2_2.isEmpty()) {
                        chk2_2 = false;
                    }
                    if (tab3.isEmpty()) {
                        chk3 = false;
                    }
                    if (tab4.isEmpty()) {
                        chk4 = false;
                    }
        %>
        <div class="pageHead_1">MIS: Procurement Statistics</div>
<!--            <span style="float: right;"><a class="action-button-goback" href="#">Go Back to Dashboard</a></span>-->
        <div class="formBg_1 t_space">
            <form method="post" action="AppMIS.jsp">
                <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                <table width="100%" cellspacing="8" class="formStyle_1" id="tblSearchBox">
                    <%
                        int uTypeId = Integer.parseInt(objUserTypeId.toString());
                        if (uTypeId == 1 || uTypeId==5 || uTypeId==19  || uTypeId==20 || uTypeId==3) {
                    %>
                    <tr>
                        <td width="20%" class="ff t-align-left">Hierarchy Node : <%if(uTypeId == 1 || uTypeId==19  || uTypeId==20){%><span class="mandatory">*</span><%}%></td>
                        <td width="80%" class="t-align-left">
                            <%if (uTypeId==5) {out.print(mISService.getOrgAdminOrgName(session.getAttribute("userId").toString()));}
                            else if(uTypeId==3){
                            Object[] objData = mISService.getOrgAdminOrgNameByUserId(Integer.parseInt(session.getAttribute("userId").toString()));
                            out.print(objData[1].toString());
                            out.print("<input type=\"hidden\" id=\"txtdepartment\" name=\"depName\" value=\"" + objData[1].toString() + "\"/>");
                            out.print("<input type=\"hidden\" id=\"txtdepartmentid\" name=\"depId\" value=\"" + objData[0] + "\"/>");
                            
                            }

                            else{%>
                            <input type="text" id="txtdepartment" style="width: 200px;" class="formTxtBox_1" readonly name="depName"/>
                            <input type="hidden" id="cmborg">
                            <!--To add CCGP and Cabinet Division add this in below as queryString ?operation=govuser-->
                            <a id="imgTree" href="javascript:void(0);" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=govuser', '', 'width=350px,height=400px,scrollbars=1','');">
                                <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                            </a>
                            <input type="hidden" id="txtdepartmentid" name="depId">
                            <input type="hidden" id="txtDepartmentType">
                            <%}%>
                        </td>
                    </tr>
                    <%}%>
                    <tr>
                        <!--Change PE to PA by Proshanto-->
                        <td class="ff t-align-left" width="20%">PA Office :</td>
                        <td width="80%" class="t-align-left">
                            <%if (uTypeId == 5 || uTypeId == 1 || uTypeId==19  || uTypeId==20 || uTypeId==3) {%>
                            <select id="cmboffice" style="width: auto;" class="formTxtBox_1" name="offId" onchange="setOffName()">
                                <%
                                   
                                    if(uTypeId==1 || uTypeId==19  || uTypeId==20){
                                %>
                                <option value="0"> No Office Found.</option>
                                <%
                                    }   
                                    if(uTypeId==5 || uTypeId==3){
                                        List<Object[]> cmbLists = mISService.getOfficeList(session.getAttribute("userId").toString(), "" + uTypeId);
                                        for (Object[] cmbList : cmbLists) {
                                            String selected="";
                                            if(request.getParameter("offId")!=null){
                                                if(cmbList[0].toString().equals(request.getParameter("offId"))){
                                                    selected="selected";
                                                }
                                            }
                                            out.print("<option value='" + cmbList[0] + "' "+selected+">" + cmbList[1] + "</option>");
                                            selected=null;
                                        }
                                        if(cmbLists.isEmpty()){
                                            out.print("<option value='0'> No Office Found.</option>");
                                        }
                                    }
                                %>
                            </select>
                            <input type="hidden" id="offName" value="" name="offName"/>
                            <%
                                }else if(uTypeId == 4){                                    
                                    for (Object[] cmbList : pecmbLists) {
                                        out.print(cmbList[1]);
                                    }                                    
                                }
                            %>
                        </td>
                    </tr>
                     <%if(uTypeId != 4){%>
                    <tr>                        
                        <td colspan="2" class="t-align-center">
                            <label class="formBtn_1"><input type="submit" value="Generate" id="button3" name="generate" onclick="return validate();"></label>
                            <label class="anchorLink l_space">
                            <a href="AppMIS.jsp" style="text-decoration: none; color: #fff; font-size: 12px;">Reset</a>
                            </label>
<!--                            <label class="formBtn_1 l_space"><input type="reset" value="Clear" id="button3" name="button3"></label>-->

                        </td>
                    </tr>
                     <%}%>
                </table>
            </form>
        </div>        
            <%
                if((uTypeId==1 || uTypeId==19  || uTypeId==20 ) && request.getParameter("depName")!=null){
                    out.print("<div class='formStyle_1 t_space ff'>Search Criteria : ");
                    out.print("Hierarchy Node : "+request.getParameter("depName"));
                    if(!request.getParameter("offName").equals("-----Please Select Office-----")){
                        out.print(", PA Office : "+request.getParameter("offName"));
                    }
                    out.print("</div>");
                }
            %>
        <div class="formStyle_1 t_space ff">Click on Count to view Package detail</div>
        <ul class="tabPanel_1 t_space">
            <li><a href="javascript:void(0);" class="sMenu" onclick="changeData('1')" id="t1">Annual Procurement Plan</a></li>
            <li><a href="javascript:void(0);" onclick="changeData('3')" id="t2">Tenders/Proposals</a></li>
            <li><a href="javascript:void(0);" onclick="changeData('4')" id="t3">Awarded Contracts</a></li>
        </ul>
        <div class="tabPanelArea_1">
            <!--Tabel - 1-->
            <table class="tableList_3" cellspacing="0" width="100%" id="tab1">
                <tbody>
                    <tr>
                        <th width="50%">Total No. of APP pending</th>
                        <th width="50%">Total No. of APP approved</th>
                    </tr>
                    <tr>
                        <td class="t-align-center"><a href="javascript:void(0);" onclick="changeData('2_1')"><%if (chk1) {
                                        out.print(tab1.get(0).getFieldName1());
                                    }%></a></td>
                        <td class="t-align-center"><a href="javascript:void(0);" onclick="changeData('2_2')"><%if (chk1) {
                                        out.print(tab1.get(0).getFieldName2());
                                    }%></a></td>
                    </tr>
                </tbody>
            </table>

            <!--Tabel - 2_1-->
            <table class="tableList_3" cellspacing="0" width="100%" id="tab2_1" style="display: none;">
                <tbody><tr>
                        <th width="30%">Description</th>
                        <th width="18%">Goods</th>
                        <th width="18%">Works</th>
                        <th width="23%">Services</th>
                        <th width="11%">Total</th>
                    </tr>

                    <tr>
                        <td class="table-description">Total No. of prepared Packages</td>
                        <td class="t-align-center"><%if (chk2_1) {
                                        out.print(tab2_1.get(0).getFieldName1());
                                    }%></td>
                        <td class="t-align-center"><%if (chk2_1) {
                                        out.print(tab2_1.get(0).getFieldName2());
                                    }%></td>
                        <td class="t-align-center"><%if (chk2_1) {
                                        out.print(tab2_1.get(0).getFieldName3());
                                    }%></td>
                        <td class="t-align-center"><%if (chk2_1) {
                                        out.print(tab2_1.get(0).getFieldName4());
                                    }%></td>
                    </tr>
                    <tr>

                        <td class="table-description">Total Amount of prepared Packages</td>
                        <td class="t-align-center"><%if (chk2_1) {
                                        out.print(tab2_1.get(0).getFieldName5());
                                    }%></td>
                        <td class="t-align-center"><%if (chk2_1) {
                                        out.print(tab2_1.get(0).getFieldName6());
                                    }%></td>
                        <td class="t-align-center"><%if (chk2_1) {
                                        out.print(tab2_1.get(0).getFieldName7());
                                    }%></td>
                        <td class="t-align-center"><%if (chk2_1) {
                                        out.print(tab2_1.get(0).getFieldName8());
                                    }%></td>
                    </tr>
                </tbody>
            </table>
            <!--Tabel - 2_2-->
            <table class="tableList_3" cellspacing="0" width="100%" id="tab2_2" style="display: none;">
                <tbody><tr>
                        <th width="30%">Description</th>
                        <th width="18%">Goods</th>
                        <th width="18%">Works</th>
                        <th width="23%">Services</th>
                        <th width="11%">Total</th>
                    </tr>

                    <tr>
                        <td class="table-description">Total No. of approved Packages</td>
                        <td class="t-align-center"><%if (chk2_2) {
                                        out.print(tab2_2.get(0).getFieldName1());
                                    }%></td>
                        <td class="t-align-center"><%if (chk2_2) {
                                        out.print(tab2_2.get(0).getFieldName2());
                                    }%></td>
                        <td class="t-align-center"><%if (chk2_2) {
                                        out.print(tab2_2.get(0).getFieldName3());
                                    }%></td>
                        <td class="t-align-center"><%if (chk2_2) {
                                        out.print(tab2_2.get(0).getFieldName4());
                                    }%></td>
                    </tr>
                    <tr>

                        <td class="table-description">Total Amount of approved Packages</td>
                        <td class="t-align-center"><%if (chk2_2) {
                                        out.print(tab2_2.get(0).getFieldName5());
                                    }%></td>
                        <td class="t-align-center"><%if (chk2_2) {
                                        out.print(tab2_2.get(0).getFieldName6());
                                    }%></td>
                        <td class="t-align-center"><%if (chk2_2) {
                                        out.print(tab2_2.get(0).getFieldName7());
                                    }%></td>
                        <td class="t-align-center"><%if (chk2_2) {
                                        out.print(tab2_2.get(0).getFieldName8());
                                    }%></td>
                    </tr>
                </tbody>
            </table>

            <!--Tabel - 3-->
            <table class="tableList_3" cellspacing="0" width="100%" id="tab3" style="display: none;">
                <tbody><tr>
                        <th width="30%">Description</th>
                        <th width="18%">Goods</th>
                        <th width="18%">Works</th>
                        <th width="23%">Services</th>
                        <th width="11%">Total</th>

                    </tr>
                    <tr>
                        <td class="table-description">Total No. of Tenders/Proposals invited</td>
                        <td class="t-align-center"><%if (chk3) {
                                        out.print(tab3.get(0).getFieldName1());
                                    }%></td>
                        <td class="t-align-center"><%if (chk3) {
                                        out.print(tab3.get(0).getFieldName2());
                                    }%></td>
                        <td class="t-align-center"><%if (chk3) {
                                        out.print(tab3.get(0).getFieldName3());
                                    }%></td>
                        <td class="t-align-center"><%if (chk3) {
                                        out.print(tab3.get(0).getFieldName4());
                                    }%></td>
                    </tr>

                    <tr>
                        <td class="table-description">Total No. of Re-Tendered</td>
                        <td class="t-align-center"><%if (chk3) {
                                        out.print(tab3.get(0).getFieldName5());
                                    }%></td>
                        <td class="t-align-center"><%if (chk3) {
                                        out.print(tab3.get(0).getFieldName6());
                                    }%></td>
                        <td class="t-align-center"><%if (chk3) {
                                        out.print(tab3.get(0).getFieldName7());
                                    }%></td>
                        <td class="t-align-center"><%if (chk3) {
                                        out.print(tab3.get(0).getFieldName8());
                                    }%></td>
                    </tr>
                    <tr>

                        <td class="table-description">Total Amount of Tenders/Proposals invited</td>
                        <td class="t-align-center"><%if (chk3) {
                                        out.print(tab3.get(0).getFieldName9());
                                    }%></td>
                        <td class="t-align-center"><%if (chk3) {
                                        out.print(tab3.get(0).getFieldName10());
                                    }%></td>
                        <td class="t-align-center"><%if (chk3) {
                                        out.print(tab3.get(0).getFieldName11());
                                    }%></td>
                        <td class="t-align-center"><%if (chk3) {
                                        out.print(tab3.get(0).getFieldName12());
                                    }%></td>
                    </tr>
                </tbody>
            </table>

            <!--Tabel - 4-->
            <table class="tableList_3" cellspacing="0" width="100%" id="tab4" style="display: none;">
                <tbody><tr>
                        <th width="30%">Description</th>
                        <th width="18%">Goods</th>
                        <th width="18%">Works</th>
                        <th width="23%">Services</th>
                        <th width="11%">Total</th>

                    </tr>
                    <tr>
                        <td class="table-description">Total No. of Contracts Awarded</td>
                        <td class="t-align-center"><%if (chk4) {
                                        out.print(tab4.get(0).getFieldName1());
                                    }%></td>
                        <td class="t-align-center"><%if (chk4) {
                                        out.print(tab4.get(0).getFieldName2());
                                    }%></td>
                        <td class="t-align-center"><%if (chk4) {
                                        out.print(tab4.get(0).getFieldName3());
                                    }%></td>
                        <td class="t-align-center"><%if (chk4) {
                                        out.print(tab4.get(0).getFieldName4());
                                    }%></td>
                    </tr>

                    <tr>
                        <td class="table-description">Total Amount of Contracts Awarded</td>
                        <td class="t-align-center"><%if (chk4) {
                                        out.print(tab4.get(0).getFieldName5());
                                    }%></td>
                        <td class="t-align-center"><%if (chk4) {
                                        out.print(tab4.get(0).getFieldName6());
                                    }%></td>
                        <td class="t-align-center"><%if (chk4) {
                                        out.print(tab4.get(0).getFieldName7());
                                    }%></td>
                        <td class="t-align-center"><%if (chk4) {
                                        out.print(tab4.get(0).getFieldName8());
                                    }%></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <%
                tab1 = null;
                tab2_1 = null;
                tab2_2 = null;
                tab3 = null;
                tab4 = null;
    %>
    <script type="text/javascript">
        function showHide()
	{
		if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
			document.getElementById('tblSearchBox').style.display = 'table';
			document.getElementById('collExp').innerHTML = '- Advanced Search';
		}else{
			document.getElementById('tblSearchBox').style.display = 'none';
			document.getElementById('collExp').innerHTML = '+ Advanced Search';
		}
	}
	function hide()
	{
		document.getElementById('tblSearchBox').style.display = 'none';
		document.getElementById('collExp').innerHTML = '+ Advanced Search';
	}
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>