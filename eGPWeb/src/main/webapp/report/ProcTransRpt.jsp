<%--
    Document   : ProcTransRpt
    Created on : Jun 29, 2012, 12:35:15 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.web.databean.ProcTranReportDtBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.GetDepartMentChildIdsByParentId"%>
<%@page import="com.cptu.egp.eps.dao.daointerface.HibernateQueryDao"%>
<%@page import="com.cptu.egp.eps.web.databean.ProcReportDtBean"%>
<%@page import="java.util.ArrayList"%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.AppMISService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    boolean isNotPDF = true;
                    if (request.getParameter("isPDF") != null && "true".equalsIgnoreCase(request.getParameter("isPDF"))) {
                        isNotPDF = false;
                    }
                    int uTypeId = 0;
                    String suserId = null;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Procurement Transaction Report (Status Wise)</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script type="text/javascript">
            function onSelectionOfTreeOff(){
                $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId:$('#txtdepartmentid').val(),funName:'Office'}, function(j){
                    $("select#cmboffice").html(j);
                    var cmb = document.getElementById('cmboffice');
                    $("#offName").val(cmb.options[cmb.selectedIndex].text);
                });
            }

            function onSelectionOfProject(){
                var cmb = document.getElementById('program');
                $("#programname").val(cmb.options[cmb.selectedIndex].text);
                $.post("<%=request.getContextPath()%>/GovtUserSrBean", {progId:$('#program').val(),action:'getProjectFrmPrograme'}, function(j){
                    $("select#project").html(j);
                });
            }

            function setOffName(){
                var cmb = document.getElementById('cmboffice');
                $("#offName").val(cmb.options[cmb.selectedIndex].text);
            }
            function setQuaterName(){
                var cmb = document.getElementById('quater');
                $("#quatername").val(cmb.options[cmb.selectedIndex].text);
            }
            function setProjectName(){
                var cmb = document.getElementById('project');
                $("#projectname").val(cmb.options[cmb.selectedIndex].text);
            }
            function changeQuater(){
                var cmb = document.getElementById('cmbFinancialYear');
                var cval1 = cmb.value.split("-")[0];
                var cval2 = cmb.value.split("-")[1];
//                var qstring = "<option value='0'>Please Select Quarter</option><option value='"+cval1+"-07-1_"+cval1+"-09-30'>Q1</option><option value='"+cval1+"-10-1_"+cval1+"-12-31'>Q2</option><option value='"+cval2+"-01-1_"+cval2+"-03-31'>Q3</option><option value='"+cval2+"-04-1_"+cval2+"-06-30'>Q4</option><option value='"+cval1+"-07-1_"+cval1+"-12-31'>Q1 and Q2</option><option value='"+cval1+"-10-1_"+cval2+"-03-31'>Q2 and Q3</option><option value='"+cval2+"-01-1_"+cval2+"-06-30'>Q3 and Q4</option><option value='"+cval1+"-07-1_"+cval2+"-03-31'>Q1 and Q2 and Q3</option><option value'"+cval1+"-10-1_"+cval2+"-06-30'>Q2 and Q3 and Q4</option><option value='"+cval1+"-07-1_"+cval2+"-06-30'>All</option>";
                var qstring = "<option value='"+cval1+"-07-1_"+cval2+"-06-30'>All Four Quarter</option><option value='"+cval1+"-07-1_"+cval1+"-09-30'>Q1</option><option value='"+cval1+"-10-1_"+cval1+"-12-31'>Q2</option><option value='"+cval2+"-01-1_"+cval2+"-03-31'>Q3</option><option value='"+cval2+"-04-1_"+cval2+"-06-30'>Q4</option><option value='"+cval1+"-07-1_"+cval1+"-12-31'>Q1 and Q2</option><option value='"+cval1+"-10-1_"+cval2+"-03-31'>Q2 and Q3</option><option value='"+cval2+"-01-1_"+cval2+"-06-30'>Q3 and Q4</option><option value='"+cval1+"-07-1_"+cval2+"-03-31'>Q1 and Q2 and Q3</option><option value'"+cval1+"-10-1_"+cval2+"-06-30'>Q2 and Q3 and Q4</option>";
                //var qcombo1 = document.getElementById('quater').options[1].value.substring(0, 4);
                //var qcombo2 = document.getElementById('quater').options[3].value.substring(0, 4);
                $("#quater").html(qstring);
            }
            function onSelectionOfTreeDesig(){}
            function showHide(){}
            function checkCondition(){}
            function checkHOPE(){}
            function validate(){
                $(".err").remove();
                var cnt=0;
                if($('#cmbFinancialYear').val()=='0'){
                    $('#cmbFinancialYear').parent().append("<div class='err' style='color:red;'>Please select Financial Year</div>");
                    cnt++;
                }
                return (cnt==0);
            }
        </script>
    </head>
    <body onload="hide();" <%=isNotPDF ? " onload='changeQuater();'" : ""%>>
        <%
                    GetDepartMentChildIdsByParentId tc = new GetDepartMentChildIdsByParentId();
                    String finYear = (request.getParameter("financialyear") == null ? "" : request.getParameter("financialyear"));
                    String quater = request.getParameter("quater");
                    String depId = "".equals(request.getParameter("depId")) ? "" : tc.getDepIds(request.getParameter("depId"));
                    String offId = request.getParameter("offId");
                    String program = request.getParameter("program");
                    String project = request.getParameter("project");
                    String appType = request.getParameter("appType");
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%if (isNotPDF) {
                                suserId = session.getAttribute("userId").toString();%>
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <%uTypeId = Integer.parseInt(objUserTypeId.toString());
                            } else {
                                uTypeId = Integer.parseInt(request.getParameter("uTypeId"));
                                suserId = request.getParameter("suserId");
                            }%>
                <%
                            //if(("19".equals(suserId) && uTypeId==19)||("19".equals(suserId) && uTypeId==19)){
                            if ((uTypeId == 19) || (uTypeId == 20)) {
                                //suserId = "1";
                                uTypeId = 1;
                            }
                            CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                            List<SPCommonSearchDataMore> appListDtBean = dataMoreService.geteGPDataMore("FinancialYear");
                            AppMISService mISService = (AppMISService) AppContext.getSpringBean("AppMISService");
                %>
                <div class="contentArea_1">
                    <div class="pageHead_1">Procurement Transaction Report (Status Wise)
                        <%if (isNotPDF) {%>
                        <span style="float: right;" id="btnprint" >
                            <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                            &nbsp;&nbsp;
                            <%
                                StringBuffer hrefQString = new StringBuffer();
                                hrefQString.append("&financialyear=");
                                hrefQString.append(finYear);
                                hrefQString.append("&quater=");
                                hrefQString.append(quater);
                                hrefQString.append("&depId=");
                                hrefQString.append(depId);
                                hrefQString.append("&offId=");
                                hrefQString.append(offId);
                                hrefQString.append("&program=");
                                hrefQString.append(program);
                                hrefQString.append("&project=");
                                hrefQString.append(project);
                                hrefQString.append("&quatername=");
                                hrefQString.append(request.getParameter("quatername"));
                                hrefQString.append("&depName=");
                                hrefQString.append(request.getParameter("depName"));
                                hrefQString.append("&offName=");
                                hrefQString.append(request.getParameter("offName"));
                                hrefQString.append("&programname=");
                                hrefQString.append(request.getParameter("programname"));
                                hrefQString.append("&projectname=");
                                hrefQString.append(request.getParameter("projectname"));

                            %>
                            <a class="action-button-savepdf" id="saveASPDF" href="<%=request.getContextPath()%>/TorRptServlet?uTypeId=<%=uTypeId%>&action=PTRpt2&generate=Generate<%=hrefQString.toString()%>">Save As PDF</a>
                        </span>
                        <%}%>
                    </div>
                    <%if (isNotPDF) {%>
                    <div class="formBg_1 t_space">
                        <form method="post" action="ProcTransRpt.jsp">
                            <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                            <table width="100%" cellspacing="8" cellpadding="0"  class="formStyle_1" id="tblSearchBox">
                                <tr>
                                    <td width="15%" class="ff" align="left">By Financial Year :<span class="mandatory">*</span></td>
                                    <td width="35%" align="left">
                                        <select name="financialyear" class="formTxtBox_1" id="cmbFinancialYear" style="width:200px;" onchange="changeQuater();">
                                            <option value="0">Please Select Financial Year</option>
                                            <!--%
                                                        if (appListDtBean != null && !appListDtBean.isEmpty()) {
                                                            for (SPCommonSearchDataMore commonApp : appListDtBean) {
                                            %>
                                            <option < %if ("Yes".equalsIgnoreCase(commonApp.getFieldName3())) {
                                                                                                                out.print("selected");
                                                                                                            }
                                                %> value="< %=commonApp.getFieldName2()%>">< %=commonApp.getFieldName2()%></option>
                                            < %
                                                            }
                                                        }
                                            %-->
                                            <%
                                                if (appListDtBean != null && !appListDtBean.isEmpty()) {
                                                    for (SPCommonSearchDataMore commonApp : appListDtBean) {
                                                        out.print("<option  value='" + commonApp.getFieldName2() + "'>" + commonApp.getFieldName2() + "</option>");
                                                    }
                                                }
                                            %>
                                        </select>
                                    </td>
                                    <td width="15%" class="ff" align="left">By Quarter :</td>
                                    <td width="35%" align="left">
                                        <select name="quater" id="quater" style="width:200px;" class="formTxtBox_1" onchange="setQuaterName()">
                                            <option value="2012-07-1_2013-06-30">All Four Quarter</option>
                                            <option value="2012-07-1_2012-09-30">Q1</option>
                                            <option value="2012-10-1_2012-12-31">Q2</option>
                                            <option value="2013-01-1_2013-03-31">Q3</option>
                                            <option value="2013-04-1_2013-06-30">Q4</option>
                                            <option value="2012-07-1_2012-12-31">Q1 and Q2</option>
                                            <option value="2012-10-1_2013-03-31">Q2 and Q3</option>
                                            <option value="2013-01-1_2013-06-30">Q3 and Q4</option>
                                            <option value="2012-07-1_2013-03-31">Q1 and Q2 and Q3</option>
                                            <option value="2012-10-1_2013-06-30">Q2 and Q3 and Q4</option>
                                        </select>
                                        <input type="hidden" name="quatername" id="quatername"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff t-align-left">Hierarchy Node : <!--%if (uTypeId == 1) {%><span class="mandatory">*</span>< %}%--></td>
                                    <td class="t-align-left">
                                        <%if (uTypeId == 5) {
                                                Object[] objData = mISService.getOrgAdminOrgName(Integer.parseInt(suserId));
                                                out.print(objData[1].toString());
                                                out.print("<input type=\"hidden\" id=\"txtdepartment\" name=\"depName\" value=\"" + objData[1].toString() + "\"/>");
                                                out.print("<input type=\"hidden\" id=\"txtdepartmentid\" name=\"depId\" value=\"" + objData[0] + "\"/>");
                                            } else {%>
                                        <input type="text" id="txtdepartment" style="width: 200px;" class="formTxtBox_1" readonly name="depName"/>
                                        <input type="hidden" id="cmborg">
                                        <!--To add CCGP and Cabinet Division add this in below as queryString ?operation=govuser-->
                                        <a id="imgTree" href="javascript:void(0);" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=govuser', '', 'width=350px,height=400px,scrollbars=1','');">
                                            <img alt="Department" style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                        </a>
                                        <input type="hidden" id="txtdepartmentid" name="depId">
                                        <input type="hidden" id="txtDepartmentType">
                                        <%}%>
                                    </td>
                                    <td class="ff t-align-left">PA Office :</td>
                                    <td class="t-align-left">
                                        <%if (uTypeId == 5 || uTypeId == 1) {%>
                                        <select id="cmboffice" style="width: 200px;" class="formTxtBox_1" name="offId" onchange="setOffName()">
                                            <%

                                                if (uTypeId == 1) {
                                            %>
                                            <option value="0"> No Office Found.</option>
                                            <%                                    }
                                                List<Object[]> cmbLists = null;
                                                if (uTypeId == 5) {
                                                    cmbLists = mISService.getOfficeList(suserId, "" + uTypeId);
                                                    for (Object[] cmbList : cmbLists) {
                                                        String selected = "";
                                                        if (request.getParameter("offId") != null) {
                                                            if (cmbList[0].toString().equals(request.getParameter("offId"))) {
                                                                selected = "selected";
                                                            }
                                                        }
                                                        out.print("<option value='" + cmbList[0] + "' " + selected + ">" + cmbList[1] + "</option>");
                                                        selected = null;
                                                    }
                                                    if (cmbLists.isEmpty()) {
                                                        out.print("<option value='0'> No Office Found.</option>");
                                                    }
                                                }
                                            %>
                                        </select>
                                        <input type="hidden" id="offName" value="<%=(uTypeId == 5) ? (request.getParameter("offName") != null && !"".equals(request.getParameter("offName"))) ? request.getParameter("offName") : !cmbLists.isEmpty() ? cmbLists.get(0)[1].toString() : "" : ""%>" name="offName" />
                                        <%
                                            }
                                        %>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="ff">By Programme :</td>
                                    <td>
                                        <select name="program" id="program" style="width:200px;" class="formTxtBox_1" onchange="onSelectionOfProject()">
                                            <option value="0">Please Select Programme</option>
                                            <%
                                                appListDtBean = dataMoreService.geteGPDataMore("getProgramName");
                                                for (SPCommonSearchDataMore prog : appListDtBean) {
                                                    out.print("<option value='" + prog.getFieldName1() + "'>" + prog.getFieldName2() + "</option>");
                                                }
                                            %>
                                        </select>
                                        <input type="hidden" name="programname" id="programname"/>
                                    </td>
                                    <td class="ff">By Project :</td>
                                    <td>
                                        <select name="project" id="project"  style="width:200px;" class="formTxtBox_1" onchange="setProjectName()">
                                            <option  value="0">Please Select Project</option>
                                            <%
                                                appListDtBean = dataMoreService.geteGPDataMore("getProjectName", "");
                                                for (SPCommonSearchDataMore prog : appListDtBean) {
                                                    out.print("<option value='" + prog.getFieldName1() + "'>" + prog.getFieldName2() + "</option>");
                                                }
                                            %>
                                        </select>
                                        <input type="hidden" name="projectname" id="projectname"/>
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="ff">By App Type:</td>
                                    <td>
                                        <select name="appType" id="appType" style="width:200px" class="formTxtBox_1">
                                            <option value="0" selected>All APP</option>
                                            <option value="1">Planned Work</option>
                                            <option value="2">Deposit Work</option>               
                                            <option value="3" >Adhoc Work</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="t-align-center">
                                        <label class="formBtn_1"><input type="submit" value="Generate" id="generate" name="generate" onclick="return validate();"></label>
                                        <label class="anchorLink l_space">
                                            <!--                                            <input type="reset" value="Clear" id="clear" name="clear" onclick="window.location.reload()">-->
                                            <a href="ProcTransRpt.jsp" style="text-decoration: none; color: #fff; font-size: 12px;">Reset</a>
                                        </label>

                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <%}%>
                    <%
                                if ("Generate".equals(request.getParameter("generate"))) {
                    %>
                    <div  id="print_area">
                        <div class="formStyle_1 t_space ff">
                            <!--                                Search Criteria : Ministry / Division / Organization : RNBORG, PE Office : --Select PE Office---->
                            <h3>Search Criteria : By Financial Year : <%=finYear%>
                                <%
                                                                    //txtdepartment
                                                                    //offName
                                                                    //projectname
                                                                    //programname
                                                                    
                                                                    if (depId != null && !depId.equals("")) {
                                                                        out.print(", Hierarchy Node : " + request.getParameter("depName"));
                                                                    }
                                                                    if (!offId.equals("0")) {
                                                                        out.print(", PA Office : " + request.getParameter("offName"));
                                                                    }
                                                                    if (!program.equals("0")) {
                                                                        out.print(", By Programme : " + request.getParameter("programname"));
                                                                    }
                                                                    if (!project.equals("0")) {
                                                                        out.print(", By Project : " + request.getParameter("projectname"));
                                                                    }
                                                                    if (request.getParameter("appType").equals("1")) {
                                                                        out.print(", By APP Type : Planned Work");
                                                                    }
                                                                    else if (request.getParameter("appType").equals("2")) {
                                                                        out.print(", By APP Type : Deposit Work");
                                                                    }
                                                                    else if (request.getParameter("appType").equals("3")) {
                                                                        out.print(", By APP Type : Adhoc Work");
                                                                    }
                                                                    else if (request.getParameter("appType").equals("0")) {
                                                                        out.print(", By APP Type : All App");
                                                                    }
                                %>
                            </h3>
                        </div>
                        <div>
                            <%if (true && !finYear.equals("")) {%>
                            <%
                                List<SPCommonSearchDataMore> spcsdmsN = dataMoreService.geteGPDataMore("getProcTransactOverAllReport", "1", "NCT", finYear, quater.equals("0") ? "0" : quater.split("_")[0], depId, offId, program, project, quater.equals("0") ? "0" : quater.split("_")[1],appType);
                                List<SPCommonSearchDataMore> spcsdmsI = dataMoreService.geteGPDataMore("getProcTransactOverAllReport", "1", "ICT", finYear, quater.equals("0") ? "0" : quater.split("_")[0], depId, offId, program, project, quater.equals("0") ? "0" : quater.split("_")[1],appType);
                                List<ProcTranReportDtBean> data = new ArrayList<ProcTranReportDtBean>();
                                List<ProcTranReportDtBean> finaldata = new ArrayList<ProcTranReportDtBean>();
                                int countOne = 1;
                                int countFlag;
                                // System.out.println("I : " + spcsdmsI.size());//22
                                //System.out.println("N : " + spcsdmsN.size());//25
                                if (spcsdmsI.size() > spcsdmsN.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        data.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, null, data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12()));
                                        countOne++;
                                    }
                                    for (ProcTranReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsN) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setnAPPPlanNo(data2.getFieldName3());
                                                prdb.setnAPPPlanAmount(data2.getFieldName4());
                                                prdb.setnAPPPkgLiveNo(data2.getFieldName5());
                                                prdb.setnAPPPkgLiveAmount(data2.getFieldName6());
                                                prdb.setnAPPPkgProcessNo(data2.getFieldName7());
                                                prdb.setnAPPPkgProcessAmount(data2.getFieldName8());
                                                prdb.setnNoaIssuedNo(data2.getFieldName9());
                                                prdb.setnNoaIssuedAmount(data2.getFieldName10());
                                                prdb.setnContractAwardedNo(data2.getFieldName11());
                                                prdb.setnContractAwardedAmt(data2.getFieldName12());
                                            }
                                        }
                                        finaldata.add(prdb);
                                        //countFlag = 0;
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        countFlag = 0;
                                        for (ProcTranReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12(), null, null));
                                            countOne++;
                                        }
                                    }

                                }

                                if (spcsdmsN.size() > spcsdmsI.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        data.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12(), null, null));
                                        countOne++;
                                    }
                                    for (ProcTranReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsI) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setiAPPPlanNo(data2.getFieldName3());
                                                prdb.setiAPPPlanAmount(data2.getFieldName4());
                                                prdb.setiAPPPkgLiveNo(data2.getFieldName5());
                                                prdb.setiAPPPkgLiveAmount(data2.getFieldName6());
                                                prdb.setiAPPPkgProcessNo(data2.getFieldName7());
                                                prdb.setiAPPPkgProcessAmount(data2.getFieldName8());
                                                prdb.setiNoaIssuedNo(data2.getFieldName9());
                                                prdb.setiNoaIssuedAmount(data2.getFieldName10());
                                                prdb.setiContractAwardedNo(data2.getFieldName11());
                                                prdb.setiContractAwardedAmt(data2.getFieldName12());
                                            }
                                        }
                                        finaldata.add(prdb);
                                        // countFlag = 0;
                                    }

                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        countFlag = 0;
                                        for (ProcTranReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, null, data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12()));
                                            countOne++;
                                        }
                                    }
                                }

                                if (spcsdmsI.size() == spcsdmsN.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        data.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, null, data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12()));
                                        countOne++;
                                    }
                                    for (ProcTranReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsN) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setnAPPPlanNo(data2.getFieldName3());
                                                prdb.setnAPPPlanAmount(data2.getFieldName4());
                                                prdb.setnAPPPkgLiveNo(data2.getFieldName5());
                                                prdb.setnAPPPkgLiveAmount(data2.getFieldName6());
                                                prdb.setnAPPPkgProcessNo(data2.getFieldName7());
                                                prdb.setnAPPPkgProcessAmount(data2.getFieldName8());
                                                prdb.setnNoaIssuedNo(data2.getFieldName9());
                                                prdb.setnNoaIssuedAmount(data2.getFieldName10());
                                                prdb.setnContractAwardedNo(data2.getFieldName11());
                                                prdb.setnContractAwardedAmt(data2.getFieldName12());
                                            }
                                        }
                                        finaldata.add(prdb);
                                        //countFlag = 0;
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        countFlag = 0;
                                        for (ProcTranReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12(), null, null));
                                            countOne++;
                                        }
                                    }
                                }
                            %>
                            <div class="inner-tableHead t_space">Capital Budget</div>
                            <table class="tableList_3" cellspacing="0" width="100%" id="tab1">
                                <tr>
                                    <th rowspan="4" >Sl. No </th>
                                    <th rowspan="3" >Method of Procurement</th>
                                    <%-- Changed By Emtazul on 16/April/2016. BTN->Nu. --%>
                                    <th colspan="4" rowspan="2" >Total Contract Package/ Lot estimated (No &amp; Million Nu.)</th>
                                    <th colspan="18" >Procurement Progress (No &amp; Million Nu.)</th>
                                </tr>

                                <tr>
                                    <th colspan="4" >Procurement package/lot Live</th>
                                    <th colspan="4" >Procurement Package/lot being processed</th>
                                    <th colspan="4" >Procurement Package/Lot Letter of Acceptance (LOA) issued </th>
                                    <th colspan="4" >Total contract awarded</th>
                                    <!--                                    <th colspan="2" rowspan="2" valign="top" >Total tender Package/Lot in progress</th>-->
                                </tr>

                                <tr>
                                    <th colspan="2" >National</th>
                                    <th colspan="2" >International

                                    </th>
                                    <th colspan="2" >National</th>
                                    <th colspan="2" >International</th>
                                    <th colspan="2" >National</th>
                                    <th colspan="2" >International</th>
                                    <th colspan="2" >National </th>
                                    <th colspan="2" >International</th>
                                    <th colspan="2" >National </th>
                                    <th colspan="2" >International</th>
                                </tr>

                                <tr>
                                    <th valign="top" > </th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No </th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <!--                                    <th valign="top" >No</th>
                                                                        <th valign="top" >Amount</th>-->
                                </tr>

                                <tr>
                                    <th>1</th>
                                    <th>2</th>
                                    <th>3</th>
                                    <th>4</th>
                                    <th>5</th>
                                    <th>6</th>
                                    <th>7</th>
                                    <th>8</th>
                                    <th>9</th>
                                    <th>10</th>
                                    <th>11</th>
                                    <th>12</th>
                                    <th>13</th>
                                    <th>14</th>
                                    <th>15</th>
                                    <th>16</th>
                                    <th>17</th>
                                    <th>18</th>
                                    <th>19</th>
                                    <th>20</th>
                                    <th>21</th>
                                    <th>22</th>
                                    <!--                                    <td>23</td>
                                                                        <td>24</td>-->
                                </tr>
                                <%
                                    if (finaldata != null && finaldata.size() > 0) {
                                    for (ProcTranReportDtBean reportDtBean : finaldata) {
                                %>
                                <tr>
                                    <td style="text-align: left;" id="t1col1_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getCounter()%></td>
                                    <td style="text-align: left;" id="t1col2_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getpMethod()%>(<%=reportDtBean.getpNature()%>)</td>
                                    <td style="text-align: right;" id="t1col3_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPlanNo() == null ? 0 : reportDtBean.getnAPPPlanNo()%></td>
                                    <td style="text-align: right;" id="t1col4_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPlanAmount() == null ? "N.A." : reportDtBean.getnAPPPlanAmount()%></td>
                                    <td style="text-align: right;" id="t1col5_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPlanNo() == null ? 0 : reportDtBean.getiAPPPlanNo()%></td>
                                    <td style="text-align: right;" id="t1col6_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPlanAmount() == null ? "N.A." : reportDtBean.getiAPPPlanAmount()%></td>
                                    <td style="text-align: right;" id="t1col7_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPkgLiveNo() == null ? 0 : reportDtBean.getnAPPPkgLiveNo()%></td>
                                    <td style="text-align: right;" id="t1col8_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPkgLiveAmount() == null ? "N.A." : reportDtBean.getnAPPPkgLiveAmount()%></td>
                                    <td style="text-align: right;" id="t1col9_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPkgLiveNo() == null ? 0 : reportDtBean.getiAPPPkgLiveNo()%></td>
                                    <td style="text-align: right;" id="t1col10_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPkgLiveAmount() == null ? "N.A." : reportDtBean.getiAPPPkgLiveAmount()%></td>
                                    <td style="text-align: right;" id="t1col11_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPkgProcessNo() == null ? 0 : reportDtBean.getnAPPPkgProcessNo()%></td>
                                    <td style="text-align: right;" id="t1col12_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPkgProcessAmount() == null ? "N.A." : reportDtBean.getnAPPPkgProcessAmount()%></td>
                                    <td style="text-align: right;" id="t1col13_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPkgProcessNo() == null ? 0 : reportDtBean.getiAPPPkgProcessNo()%></td>
                                    <td style="text-align: right;" id="t1col14_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPkgProcessAmount() == null ? "N.A." : reportDtBean.getiAPPPkgProcessAmount()%></td>
                                    <td style="text-align: right;" id="t1col15_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnNoaIssuedNo() == null ? 0 : reportDtBean.getnNoaIssuedNo()%></td>
                                    <td style="text-align: right;" id="t1col16_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnNoaIssuedAmount() == null ? "N.A." : reportDtBean.getnNoaIssuedAmount()%></td>
                                    <td style="text-align: right;" id="t1col17_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiNoaIssuedNo() == null ? 0 : reportDtBean.getiNoaIssuedNo()%></td>
                                    <td style="text-align: right;" id="t1col18_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiNoaIssuedAmount() == null ? "N.A." : reportDtBean.getiNoaIssuedAmount()%></td>
                                    <td style="text-align: right;" id="t1col19_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnContractAwardedNo() == null ? 0 : reportDtBean.getnContractAwardedNo()%></td>
                                    <td style="text-align: right;" id="t1col20_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnContractAwardedAmt() == null ? "N.A." : reportDtBean.getnContractAwardedAmt()%></td>
                                    <td style="text-align: right;" id="t1col21_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiContractAwardedNo() == null ? 0 : reportDtBean.getiContractAwardedNo()%></td>
                                    <td style="text-align: right;" id="t1col22_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiContractAwardedAmt() == null ? "N.A." : reportDtBean.getiContractAwardedAmt()%></td>
                                </tr>
                                <%
                                        }
                                    }
                                %>
                                <tr>
                                    <th colspan="2" align="left">Total Development Budget<input type="hidden" id="tab1cnt" value="<%=countOne%>"/></th>
                                    <th style="text-align: right;" id="t1total1"></th>
                                    <th style="text-align: right;" id="t1total2"></th>
                                    <th style="text-align: right;" id="t1total3"></th>
                                    <th style="text-align: right;" id="t1total4"></th>
                                    <th style="text-align: right;" id="t1total5"></th>
                                    <th style="text-align: right;" id="t1total6"></th>
                                    <th style="text-align: right;" id="t1total7"></th>
                                    <th style="text-align: right;" id="t1total8"></th>
                                    <th style="text-align: right;" id="t1total9"></th>
                                    <th style="text-align: right;" id="t1total10"></th>
                                    <th style="text-align: right;" id="t1total11"></th>
                                    <th style="text-align: right;" id="t1total12"></th>
                                    <th style="text-align: right;" id="t1total13"></th>
                                    <th style="text-align: right;" id="t1total14"></th>
                                    <th style="text-align: right;" id="t1total15"></th>
                                    <th style="text-align: right;" id="t1total16"></th>
                                    <th style="text-align: right;" id="t1total17"></th>
                                    <th style="text-align: right;" id="t1total18"></th>
                                    <th style="text-align: right;" id="t1total19"></th>
                                    <th style="text-align: right;" id="t1total20"></th>
                                </tr>
                            </table>
                            <%}%>

                            <%if (true && !finYear.equals("")) {%>
                            <%
                                List<SPCommonSearchDataMore> spcsdmsN = dataMoreService.geteGPDataMore("getProcTransactOverAllReport", "2", "NCT", finYear, quater.equals("0") ? "0" : quater.split("_")[0], depId, offId, program, project, quater.equals("0") ? "0" : quater.split("_")[1],appType);
                                List<SPCommonSearchDataMore> spcsdmsI = dataMoreService.geteGPDataMore("getProcTransactOverAllReport", "2", "ICT", finYear, quater.equals("0") ? "0" : quater.split("_")[0], depId, offId, program, project, quater.equals("0") ? "0" : quater.split("_")[1],appType);
                                List<ProcTranReportDtBean> data = new ArrayList<ProcTranReportDtBean>();
                                List<ProcTranReportDtBean> finaldata = new ArrayList<ProcTranReportDtBean>();
                                List<ProcTranReportDtBean> tempData = new ArrayList<ProcTranReportDtBean>();
                                int countOne = 1;
                                int countFlag;
                                // System.out.println("I : " + spcsdmsI.size());//22
                                //System.out.println("N : " + spcsdmsN.size());//25
                                if (spcsdmsI.size() > spcsdmsN.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        data.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, null, data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12()));
                                        countOne++;
                                    }
                                    for (ProcTranReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsN) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setnAPPPlanNo(data2.getFieldName3());
                                                prdb.setnAPPPlanAmount(data2.getFieldName4());
                                                prdb.setnAPPPkgLiveNo(data2.getFieldName5());
                                                prdb.setnAPPPkgLiveAmount(data2.getFieldName6());
                                                prdb.setnAPPPkgProcessNo(data2.getFieldName7());
                                                prdb.setnAPPPkgProcessAmount(data2.getFieldName8());
                                                prdb.setnNoaIssuedNo(data2.getFieldName9());
                                                prdb.setnNoaIssuedAmount(data2.getFieldName10());
                                                prdb.setnContractAwardedNo(data2.getFieldName11());
                                                prdb.setnContractAwardedAmt(data2.getFieldName12());
                                            }
                                        }
                                        finaldata.add(prdb);
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        countFlag = 0;
                                        for (ProcTranReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12(), null, null));
                                            countOne++;
                                        }
                                    }
                                }

                                if (spcsdmsN.size() > spcsdmsI.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        data.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12(), null, null));
                                        countOne++;
                                    }
                                    for (ProcTranReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsI) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setiAPPPlanNo(data2.getFieldName3());
                                                prdb.setiAPPPlanAmount(data2.getFieldName4());
                                                prdb.setiAPPPkgLiveNo(data2.getFieldName5());
                                                prdb.setiAPPPkgLiveAmount(data2.getFieldName6());
                                                prdb.setiAPPPkgProcessNo(data2.getFieldName7());
                                                prdb.setiAPPPkgProcessAmount(data2.getFieldName8());
                                                prdb.setiNoaIssuedNo(data2.getFieldName9());
                                                prdb.setiNoaIssuedAmount(data2.getFieldName10());
                                                prdb.setiContractAwardedNo(data2.getFieldName11());
                                                prdb.setiContractAwardedAmt(data2.getFieldName12());
                                            }
                                        }
                                        finaldata.add(prdb);
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        countFlag = 0;
                                        for (ProcTranReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, null, data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12()));
                                            countOne++;
                                        }
                                    }
                                }

                                if (spcsdmsI.size() == spcsdmsN.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        data.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, null, data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12()));
                                        countOne++;
                                    }
                                    for (ProcTranReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsN) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setnAPPPlanNo(data2.getFieldName3());
                                                prdb.setnAPPPlanAmount(data2.getFieldName4());
                                                prdb.setnAPPPkgLiveNo(data2.getFieldName5());
                                                prdb.setnAPPPkgLiveAmount(data2.getFieldName6());
                                                prdb.setnAPPPkgProcessNo(data2.getFieldName7());
                                                prdb.setnAPPPkgProcessAmount(data2.getFieldName8());
                                                prdb.setnNoaIssuedNo(data2.getFieldName9());
                                                prdb.setnNoaIssuedAmount(data2.getFieldName10());
                                                prdb.setnContractAwardedNo(data2.getFieldName11());
                                                prdb.setnContractAwardedAmt(data2.getFieldName12());
                                            }
                                        }
                                        finaldata.add(prdb);
                                        //countFlag = 0;
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        countFlag = 0;
                                        for (ProcTranReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12(), null, null));
                                            countOne++;
                                        }
                                    }
                                }
                            %>
                            <div class="inner-tableHead t_space">Recurrent Budget</div>
                            <table class="tableList_3" cellspacing="0" width="100%" id="tab2">
                                <tr>
                                    <th rowspan="4" >Sl. No </th>
                                    <th rowspan="3" >Method of Procurement</th>
                                    
                                    <th colspan="4" rowspan="2" >Total Contract Package/ Lot estimated (No &amp; Million Nu.)</th>
                                    <th colspan="18" >Procurement Progress (No &amp; Million Nu.)</th>
                                </tr>

                                <tr>
                                    <th colspan="4" >Procurement package/lot Live</th>
                                    <th colspan="4" >Procurement Package/lot being processed</th>
                                    <th colspan="4" >Procurement Package/Lot Letter of Acceptance (LOA) issued </th>
                                    <th colspan="4" >Total contract awarded</th>
                                    <!--                                    <th colspan="2" rowspan="2" valign="top" >Total tender Package/Lot in progress</th>-->
                                </tr>

                                <tr>
                                    <th colspan="2" >National</th>
                                    <th colspan="2" >International

                                    </th>
                                    <th colspan="2" >National</th>
                                    <th colspan="2" >International</th>
                                    <th colspan="2" >National</th>
                                    <th colspan="2" >International</th>
                                    <th colspan="2" >National </th>
                                    <th colspan="2" >International</th>
                                    <th colspan="2" >National </th>
                                    <th colspan="2" >International</th>
                                </tr>

                                <tr>
                                    <th valign="top" > </th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No </th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <!--                                    <th valign="top" >No</th>
                                                                        <th valign="top" >Amount</th>-->
                                </tr>

                                <tr>
                                    <th>1</th>
                                    <th>2</th>
                                    <th>3</th>
                                    <th>4</th>
                                    <th>5</th>
                                    <th>6</th>
                                    <th>7</th>
                                    <th>8</th>
                                    <th>9</th>
                                    <th>10</th>
                                    <th>11</th>
                                    <th>12</th>
                                    <th>13</th>
                                    <th>14</th>
                                    <th>15</th>
                                    <th>16</th>
                                    <th>17</th>
                                    <th>18</th>
                                    <th>19</th>
                                    <th>20</th>
                                    <th>21</th>
                                    <th>22</th>
                                    <!--                                    <td>23</td>
                                                                        <td>24</td>-->
                                </tr>
                                <%
                                    int countsrno = 0;
                                    for (ProcTranReportDtBean reportDtBean : finaldata) {
                                %>
                                <tr>
                                    <td style="text-align: left;" id="t2col1_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getCounter()%></td>
                                    <td style="text-align: left;" id="t2col2_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getpMethod()%>(<%=reportDtBean.getpNature()%>)</td>
                                    <td style="text-align: right;" id="t2col3_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPlanNo() == null ? 0 : reportDtBean.getnAPPPlanNo()%></td>
                                    <td style="text-align: right;" id="t2col4_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPlanAmount() == null ? "N.A." : reportDtBean.getnAPPPlanAmount()%></td>
                                    <td style="text-align: right;" id="t2col5_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPlanNo() == null ? 0 : reportDtBean.getiAPPPlanNo()%></td>
                                    <td style="text-align: right;" id="t2col6_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPlanAmount() == null ? "N.A." : reportDtBean.getiAPPPlanAmount()%></td>
                                    <td style="text-align: right;" id="t2col7_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPkgLiveNo() == null ? 0 : reportDtBean.getnAPPPkgLiveNo()%></td>
                                    <td style="text-align: right;" id="t2col8_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPkgLiveAmount() == null ? "N.A." : reportDtBean.getnAPPPkgLiveAmount()%></td>
                                    <td style="text-align: right;" id="t2col9_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPkgLiveNo() == null ? 0 : reportDtBean.getiAPPPkgLiveNo()%></td>
                                    <td style="text-align: right;" id="t2col10_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPkgLiveAmount() == null ? "N.A." : reportDtBean.getiAPPPkgLiveAmount()%></td>
                                    <td style="text-align: right;" id="t2col11_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPkgProcessNo() == null ? 0 : reportDtBean.getnAPPPkgProcessNo()%></td>
                                    <td style="text-align: right;" id="t2col12_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPkgProcessAmount() == null ? "N.A." : reportDtBean.getnAPPPkgProcessAmount()%></td>
                                    <td style="text-align: right;" id="t2col13_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPkgProcessNo() == null ? 0 : reportDtBean.getiAPPPkgProcessNo()%></td>
                                    <td style="text-align: right;" id="t2col14_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPkgProcessAmount() == null ? "N.A." : reportDtBean.getiAPPPkgProcessAmount()%></td>
                                    <td style="text-align: right;" id="t2col15_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnNoaIssuedNo() == null ? 0 : reportDtBean.getnNoaIssuedNo()%></td>
                                    <td style="text-align: right;" id="t2col16_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnNoaIssuedAmount() == null ? "N.A." : reportDtBean.getnNoaIssuedAmount()%></td>
                                    <td style="text-align: right;" id="t2col17_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiNoaIssuedNo() == null ? 0 : reportDtBean.getiNoaIssuedNo()%></td>
                                    <td style="text-align: right;" id="t2col18_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiNoaIssuedAmount() == null ? "N.A." : reportDtBean.getiNoaIssuedAmount()%></td>
                                    <td style="text-align: right;" id="t2col19_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnContractAwardedNo() == null ? 0 : reportDtBean.getnContractAwardedNo()%></td>
                                    <td style="text-align: right;" id="t2col20_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnContractAwardedAmt() == null ? "N.A." : reportDtBean.getnContractAwardedAmt()%></td>
                                    <td style="text-align: right;" id="t2col21_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiContractAwardedNo() == null ? 0 : reportDtBean.getiContractAwardedNo()%></td>
                                    <td style="text-align: right;" id="t2col22_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiContractAwardedAmt() == null ? "N.A." : reportDtBean.getiContractAwardedAmt()%></td>
                                </tr>
                                <%
                                    }
                                %>
                                <tr>
                                    <th colspan="2" align="left">Total Revenue Budget<input type="hidden" id="tab2cnt" value="<%=countOne%>"/></th>
                                    <th style="text-align: right;" id="t2total1"></th>
                                    <th style="text-align: right;" id="t2total2"></th>
                                    <th style="text-align: right;" id="t2total3"></th>
                                    <th style="text-align: right;" id="t2total4"></th>
                                    <th style="text-align: right;" id="t2total5"></th>
                                    <th style="text-align: right;" id="t2total6"></th>
                                    <th style="text-align: right;" id="t2total7"></th>
                                    <th style="text-align: right;" id="t2total8"></th>
                                    <th style="text-align: right;" id="t2total9"></th>
                                    <th style="text-align: right;" id="t2total10"></th>
                                    <th style="text-align: right;" id="t2total11"></th>
                                    <th style="text-align: right;" id="t2total12"></th>
                                    <th style="text-align: right;" id="t2total13"></th>
                                    <th style="text-align: right;" id="t2total14"></th>
                                    <th style="text-align: right;" id="t2total15"></th>
                                    <th style="text-align: right;" id="t2total16"></th>
                                    <th style="text-align: right;" id="t2total17"></th>
                                    <th style="text-align: right;" id="t2total18"></th>
                                    <th style="text-align: right;" id="t2total19"></th>
                                    <th style="text-align: right;" id="t2total20"></th>
                                </tr>
                            </table>
                            <%}%>

                            <%if (true && !finYear.equals("")) {%>
                            <%
                                List<SPCommonSearchDataMore> spcsdmsN = dataMoreService.geteGPDataMore("getProcTransactOverAllReport", "3", "NCT", finYear, quater.equals("0") ? "0" : quater.split("_")[0], depId, offId, program, project, quater.equals("0") ? "0" : quater.split("_")[1],appType);
                                List<SPCommonSearchDataMore> spcsdmsI = dataMoreService.geteGPDataMore("getProcTransactOverAllReport", "3", "ICT", finYear, quater.equals("0") ? "0" : quater.split("_")[0], depId, offId, program, project, quater.equals("0") ? "0" : quater.split("_")[1],appType);
                                List<ProcTranReportDtBean> data = new ArrayList<ProcTranReportDtBean>();
                                List<ProcTranReportDtBean> finaldata = new ArrayList<ProcTranReportDtBean>();
                                List<ProcTranReportDtBean> tempData = new ArrayList<ProcTranReportDtBean>();
                                int countOne = 1;
                                int countFlag;
                                // System.out.println("I : " + spcsdmsI.size());//22
                                //System.out.println("N : " + spcsdmsN.size());//25
                                if (spcsdmsI.size() > spcsdmsN.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        data.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, null, data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12()));
                                        countOne++;
                                    }
                                    for (ProcTranReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsN) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setnAPPPlanNo(data2.getFieldName3());
                                                prdb.setnAPPPlanAmount(data2.getFieldName4());
                                                prdb.setnAPPPkgLiveNo(data2.getFieldName5());
                                                prdb.setnAPPPkgLiveAmount(data2.getFieldName6());
                                                prdb.setnAPPPkgProcessNo(data2.getFieldName7());
                                                prdb.setnAPPPkgProcessAmount(data2.getFieldName8());
                                                prdb.setnNoaIssuedNo(data2.getFieldName9());
                                                prdb.setnNoaIssuedAmount(data2.getFieldName10());
                                                prdb.setnContractAwardedNo(data2.getFieldName11());
                                                prdb.setnContractAwardedAmt(data2.getFieldName12());
                                            }
                                        }
                                        finaldata.add(prdb);
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        countFlag = 0;
                                        for (ProcTranReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12(), null, null));
                                            countOne++;
                                        }
                                    }
                                }

                                if (spcsdmsN.size() > spcsdmsI.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        data.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12(), null, null));
                                        countOne++;
                                    }
                                    for (ProcTranReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsI) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setiAPPPlanNo(data2.getFieldName3());
                                                prdb.setiAPPPlanAmount(data2.getFieldName4());
                                                prdb.setiAPPPkgLiveNo(data2.getFieldName5());
                                                prdb.setiAPPPkgLiveAmount(data2.getFieldName6());
                                                prdb.setiAPPPkgProcessNo(data2.getFieldName7());
                                                prdb.setiAPPPkgProcessAmount(data2.getFieldName8());
                                                prdb.setiNoaIssuedNo(data2.getFieldName9());
                                                prdb.setiNoaIssuedAmount(data2.getFieldName10());
                                                prdb.setiContractAwardedNo(data2.getFieldName11());
                                                prdb.setiContractAwardedAmt(data2.getFieldName12());
                                            }
                                        }
                                        finaldata.add(prdb);
                                    }

                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        countFlag = 0;
                                        for (ProcTranReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, null, data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12()));
                                            countOne++;
                                        }
                                    }
                                }

                                                                if (spcsdmsI.size() == spcsdmsN.size()) {
                                    for (SPCommonSearchDataMore data1 : spcsdmsI) {
                                        data.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), null, null, data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12()));
                                        countOne++;
                                    }
                                    for (ProcTranReportDtBean prdb : data) {
                                        for (SPCommonSearchDataMore data2 : spcsdmsN) {
                                            if (prdb.getpNature().equals(data2.getFieldName1()) && prdb.getpMethod().equals(data2.getFieldName2())) {
                                                prdb.setnAPPPlanNo(data2.getFieldName3());
                                                prdb.setnAPPPlanAmount(data2.getFieldName4());
                                                prdb.setnAPPPkgLiveNo(data2.getFieldName5());
                                                prdb.setnAPPPkgLiveAmount(data2.getFieldName6());
                                                prdb.setnAPPPkgProcessNo(data2.getFieldName7());
                                                prdb.setnAPPPkgProcessAmount(data2.getFieldName8());
                                                prdb.setnNoaIssuedNo(data2.getFieldName9());
                                                prdb.setnNoaIssuedAmount(data2.getFieldName10());
                                                prdb.setnContractAwardedNo(data2.getFieldName11());
                                                prdb.setnContractAwardedAmt(data2.getFieldName12());
                                            }
                                        }
                                        finaldata.add(prdb);
                                        //countFlag = 0;
                                    }
                                    for (SPCommonSearchDataMore data1 : spcsdmsN) {
                                        countFlag = 0;
                                        for (ProcTranReportDtBean prdb : data) {
                                            if (prdb.getpNature().equals(data1.getFieldName1()) && prdb.getpMethod().equals(data1.getFieldName2())) {
                                                countFlag = 0;
                                                break;
                                            } else {
                                                countFlag = 1;
                                            }
                                        }
                                        if (countFlag == 1) {
                                            finaldata.add(new ProcTranReportDtBean(countOne, data1.getFieldName1(), data1.getFieldName2(), data1.getFieldName3(), data1.getFieldName4(), null, null, data1.getFieldName5(), data1.getFieldName6(), null, null, data1.getFieldName7(), data1.getFieldName8(), null, null, data1.getFieldName9(), data1.getFieldName10(), null, null, data1.getFieldName11(), data1.getFieldName12(), null, null));
                                            countOne++;
                                        }
                                    }
                                }
                            %>
                            <div class="inner-tableHead t_space">Own Fund</div>
                            <table class="tableList_3" cellspacing="0" width="100%" id="tab3">
                                <tr>
                                    <th rowspan="4" >Sl. No </th>
                                    <th rowspan="3" >Method of Procurement</th>
                                    <th colspan="4" rowspan="2" >Total Contract Package/ Lot estimated (No &amp; Million Nu.)</th>
                                    <th colspan="18" >Procurement Progress (No &amp; Million Nu.)</th>
                                </tr>

                                <tr>
                                    <th colspan="4" >Procurement package/lot Live</th>
                                    <th colspan="4" >Procurement Package/lot being processed</th>
                                    <th colspan="4" >Procurement Package/Lot Letter of Acceptance (LOA) issued </th>
                                    <th colspan="4" >Total contract awarded</th>
                                    <!--                                    <th colspan="2" rowspan="2" valign="top" >Total tender Package/Lot in progress</th>-->
                                </tr>

                                <tr>
                                    <th colspan="2" >National</th>
                                    <th colspan="2" >International

                                    </th>
                                    <th colspan="2" >National</th>
                                    <th colspan="2" >International</th>
                                    <th colspan="2" >National</th>
                                    <th colspan="2" >International</th>
                                    <th colspan="2" >National </th>
                                    <th colspan="2" >International</th>
                                    <th colspan="2" >National </th>
                                    <th colspan="2" >International</th>
                                </tr>

                                <tr>
                                    <th valign="top" > </th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No </th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <th>No</th>
                                    <th>Amount</th>
                                    <!--                                    <th valign="top" >No</th>
                                                                        <th valign="top" >Amount</th>-->
                                </tr>

                                <tr>
                                    <th>1</th>
                                    <th>2</th>
                                    <th>3</th>
                                    <th>4</th>
                                    <th>5</th>
                                    <th>6</th>
                                    <th>7</th>
                                    <th>8</th>
                                    <th>9</th>
                                    <th>10</th>
                                    <th>11</th>
                                    <th>12</th>
                                    <th>13</th>
                                    <th>14</th>
                                    <th>15</th>
                                    <th>16</th>
                                    <th>17</th>
                                    <th>18</th>
                                    <th>19</th>
                                    <th>20</th>
                                    <th>21</th>
                                    <th>22</th>
                                    <!--                                    <td>23</td>
                                                                        <td>24</td>-->
                                </tr>
                                <%
                                    int countsrno = 0;
                                    for (ProcTranReportDtBean reportDtBean : finaldata) {
                                %>
                                <tr>
                                    <td style="text-align: left;" id="t3col1_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getCounter()%></td>
                                    <td style="text-align: left;" id="t3col2_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getpMethod()%>(<%=reportDtBean.getpNature()%>)</td>
                                    <td style="text-align: right;" id="t3col3_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPlanNo() == null ? 0 : reportDtBean.getnAPPPlanNo()%></td>
                                    <td style="text-align: right;" id="t3col4_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPlanAmount() == null ? "N.A." : reportDtBean.getnAPPPlanAmount()%></td>
                                    <td style="text-align: right;" id="t3col5_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPlanNo() == null ? 0 : reportDtBean.getiAPPPlanNo()%></td>
                                    <td style="text-align: right;" id="t3col6_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPlanAmount() == null ? "N.A." : reportDtBean.getiAPPPlanAmount()%></td>
                                    <td style="text-align: right;" id="t3col7_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPkgLiveNo() == null ? 0 : reportDtBean.getnAPPPkgLiveNo()%></td>
                                    <td style="text-align: right;" id="t3col8_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPkgLiveAmount() == null ? "N.A." : reportDtBean.getnAPPPkgLiveAmount()%></td>
                                    <td style="text-align: right;" id="t3col9_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPkgLiveNo() == null ? 0 : reportDtBean.getiAPPPkgLiveNo()%></td>
                                    <td style="text-align: right;" id="t3col10_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPkgLiveAmount() == null ? "N.A." : reportDtBean.getiAPPPkgLiveAmount()%></td>
                                    <td style="text-align: right;" id="t3col11_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPkgProcessNo() == null ? 0 : reportDtBean.getnAPPPkgProcessNo()%></td>
                                    <td style="text-align: right;" id="t3col12_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnAPPPkgProcessAmount() == null ? "N.A." : reportDtBean.getnAPPPkgProcessAmount()%></td>
                                    <td style="text-align: right;" id="t3col13_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPkgProcessNo() == null ? 0 : reportDtBean.getiAPPPkgProcessNo()%></td>
                                    <td style="text-align: right;" id="t3col14_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiAPPPkgProcessAmount() == null ? "N.A." : reportDtBean.getiAPPPkgProcessAmount()%></td>
                                    <td style="text-align: right;" id="t3col15_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnNoaIssuedNo() == null ? 0 : reportDtBean.getnNoaIssuedNo()%></td>
                                    <td style="text-align: right;" id="t3col16_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnNoaIssuedAmount() == null ? "N.A." : reportDtBean.getnNoaIssuedAmount()%></td>
                                    <td style="text-align: right;" id="t3col17_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiNoaIssuedNo() == null ? 0 : reportDtBean.getiNoaIssuedNo()%></td>
                                    <td style="text-align: right;" id="t3col18_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiNoaIssuedAmount() == null ? "N.A." : reportDtBean.getiNoaIssuedAmount()%></td>
                                    <td style="text-align: right;" id="t3col19_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnContractAwardedNo() == null ? 0 : reportDtBean.getnContractAwardedNo()%></td>
                                    <td style="text-align: right;" id="t3col20_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getnContractAwardedAmt() == null ? "N.A." : reportDtBean.getnContractAwardedAmt()%></td>
                                    <td style="text-align: right;" id="t3col21_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiContractAwardedNo() == null ? 0 : reportDtBean.getiContractAwardedNo()%></td>
                                    <td style="text-align: right;" id="t3col22_<%=reportDtBean.getCounter()%>"><%=reportDtBean.getiContractAwardedAmt() == null ? "N.A." : reportDtBean.getiContractAwardedAmt()%></td>

                                </tr>
                                <%
                                    }
                                %>
                                <tr>
                                    <th colspan="2" align="left">Total Own Fund<input type="hidden" id="tab3cnt" value="<%=countOne%>"/></th>
                                    <th style="text-align: right;" id="t3total1"></th>
                                    <th style="text-align: right;" id="t3total2"></th>
                                    <th style="text-align: right;" id="t3total3"></th>
                                    <th style="text-align: right;" id="t3total4"></th>
                                    <th style="text-align: right;" id="t3total5"></th>
                                    <th style="text-align: right;" id="t3total6"></th>
                                    <th style="text-align: right;" id="t3total7"></th>
                                    <th style="text-align: right;" id="t3total8"></th>
                                    <th style="text-align: right;" id="t3total9"></th>
                                    <th style="text-align: right;" id="t3total10"></th>
                                    <th style="text-align: right;" id="t3total11"></th>
                                    <th style="text-align: right;" id="t3total12"></th>
                                    <th style="text-align: right;" id="t3total13"></th>
                                    <th style="text-align: right;" id="t3total14"></th>
                                    <th style="text-align: right;" id="t3total15"></th>
                                    <th style="text-align: right;" id="t3total16"></th>
                                    <th style="text-align: right;" id="t3total17"></th>
                                    <th style="text-align: right;" id="t3total18"></th>
                                    <th style="text-align: right;" id="t3total19"></th>
                                    <th style="text-align: right;" id="t3total20"></th>
                                </tr>
                                <tr style="border-color: #ffffff;">
                                    <td colspan="14"></td>
                                </tr>
                                <tr>
                                    <th colspan="2" align="left">GRAND TOTAL</th>
                                    <th style="text-align: right;" id="total1"></th>
                                    <th style="text-align: right;" id="total2"></th>
                                    <th style="text-align: right;" id="total3"></th>
                                    <th style="text-align: right;" id="total4"></th>
                                    <th style="text-align: right;" id="total5"></th>
                                    <th style="text-align: right;" id="total6"></th>
                                    <th style="text-align: right;" id="total7"></th>
                                    <th style="text-align: right;" id="total8"></th>
                                    <th style="text-align: right;" id="total9"></th>
                                    <th style="text-align: right;" id="total10"></th>
                                    <th style="text-align: right;" id="total11"></th>
                                    <th style="text-align: right;" id="total12"></th>
                                    <th style="text-align: right;" id="total13"></th>
                                    <th style="text-align: right;" id="total14"></th>
                                    <th style="text-align: right;" id="total15"></th>
                                    <th style="text-align: right;" id="total16"></th>
                                    <th style="text-align: right;" id="total17"></th>
                                    <th style="text-align: right;" id="total18"></th>
                                    <th style="text-align: right;" id="total19"></th>
                                    <th style="text-align: right;" id="total20"></th>
                                </tr>
                            </table>
                            <%}%>
                            <%if (finYear.equals("")) {%>
                            <table class="tableList_3" cellspacing="0" width="100%">
                                <tr>
                                    <td style="text-align:center;font-weight: bold;">
                                        No Data Found
                                    </td>
                                </tr>
                            </table>
                            <%}%>
                        </div>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        function showHide()
	{
		if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
			document.getElementById('tblSearchBox').style.display = 'table';
			document.getElementById('collExp').innerHTML = '- Advanced Search';
		}else{
			document.getElementById('tblSearchBox').style.display = 'none';
			document.getElementById('collExp').innerHTML = '+ Advanced Search';
		}
	}
	function hide()
	{
		document.getElementById('tblSearchBox').style.display = 'none';
		document.getElementById('collExp').innerHTML = '+ Advanced Search';
	}
            var tab1cnt = $('#tab1cnt').val();
            var t1total1 = 0.0;
            var t1total2 = 0.0;
            var t1total3 = 0.0;
            var t1total4 = 0.0;
            var t1total5 = 0.0;
            var t1total6 = 0.0;
            var t1total7 = 0.0;
            var t1total8 = 0.0;
            var t1total9 = 0.0;
            var t1total10 = 0.0;
            var t1total11 = 0.0;
            var t1total12 = 0.0;
            var t1total13 = 0.0;
            var t1total14 = 0.0;
            var t1total15 = 0.0;
            var t1total16 = 0.0;
            var t1total17 = 0.0;
            var t1total18 = 0.0;
            var t1total19 = 0.0;
            var t1total20 = 0.0;
            for(var i = 1; i < tab1cnt; i++){
                t1total1 = t1total1 + parseFloat(isNaN($('#t1col3_'+i).html()) ? 0.0 : $('#t1col3_'+i).html());
                t1total2 = t1total2 + parseFloat(isNaN($('#t1col4_'+i).html()) ? 0.0 : $('#t1col4_'+i).html());
                t1total3 = t1total3 + parseFloat(isNaN($('#t1col5_'+i).html()) ? 0.0 : $('#t1col5_'+i).html());
                t1total4 = t1total4 + parseFloat(isNaN($('#t1col6_'+i).html()) ? 0.0 : $('#t1col6_'+i).html());
                t1total5 = t1total5 + parseFloat(isNaN($('#t1col7_'+i).html()) ? 0.0 : $('#t1col7_'+i).html());
                t1total6 = t1total6 + parseFloat(isNaN($('#t1col8_'+i).html()) ? 0.0 : $('#t1col8_'+i).html());
                t1total7 = t1total7 + parseFloat(isNaN($('#t1col9_'+i).html()) ? 0.0 : $('#t1col9_'+i).html());
                t1total8 = t1total8 + parseFloat(isNaN($('#t1col10_'+i).html()) ? 0.0 : $('#t1col10_'+i).html());
                t1total9 = t1total9 + parseFloat(isNaN($('#t1col11_'+i).html()) ? 0.0 : $('#t1col11_'+i).html());
                t1total10 = t1total10 + parseFloat(isNaN($('#t1col12_'+i).html()) ? 0.0 : $('#t1col12_'+i).html());
                t1total11 = t1total11 + parseFloat(isNaN($('#t1col13_'+i).html()) ? 0.0 : $('#t1col13_'+i).html());
                t1total12 = t1total12 + parseFloat(isNaN($('#t1col14_'+i).html()) ? 0.0 : $('#t1col14_'+i).html());
                t1total13 = t1total13 + parseFloat(isNaN($('#t1col15_'+i).html()) ? 0.0 : $('#t1col15_'+i).html());
                t1total14 = t1total14 + parseFloat(isNaN($('#t1col16_'+i).html()) ? 0.0 : $('#t1col16_'+i).html());
                t1total15 = t1total15 + parseFloat(isNaN($('#t1col17_'+i).html()) ? 0.0 : $('#t1col17_'+i).html());
                t1total16 = t1total16 + parseFloat(isNaN($('#t1col18_'+i).html()) ? 0.0 : $('#t1col18_'+i).html());
                t1total17 = t1total17+ parseFloat(isNaN($('#t1col19_'+i).html()) ? 0.0 : $('#t1col19_'+i).html());
                t1total18 = t1total18 + parseFloat(isNaN($('#t1col20_'+i).html()) ? 0.0 : $('#t1col20_'+i).html());
                t1total19 = t1total19 + parseFloat(isNaN($('#t1col21_'+i).html()) ? 0.0 : $('#t1col21_'+i).html());
                t1total20 = t1total20 + parseFloat(isNaN($('#t1col22_'+i).html()) ? 0.0 : $('#t1col22_'+i).html());
            }
            $('#t1total1').html(t1total1);
            $('#t1total2').html(t1total2.toFixed(3));
            $('#t1total3').html(t1total3);
            $('#t1total4').html(t1total4.toFixed(3));
            $('#t1total5').html(t1total5);
            $('#t1total6').html(t1total6.toFixed(3));
            $('#t1total7').html(t1total7);
            $('#t1total8').html(t1total8.toFixed(3));
            $('#t1total9').html(t1total9);
            $('#t1total10').html(t1total10.toFixed(3));
            $('#t1total11').html(t1total11);
            $('#t1total12').html(t1total12.toFixed(3));
            $('#t1total13').html(t1total13);
            $('#t1total14').html(t1total14.toFixed(3));
            $('#t1total15').html(t1total15);
            $('#t1total16').html(t1total16.toFixed(3));
            $('#t1total17').html(t1total17);
            $('#t1total18').html(t1total18.toFixed(3));
            $('#t1total19').html(t1total19);
            $('#t1total20').html(t1total20.toFixed(3));

            var tab2cnt = $('#tab2cnt').val();
            var t2total1 = 0.0;
            var t2total2 = 0.0;
            var t2total3 = 0.0;
            var t2total4 = 0.0;
            var t2total5 = 0.0;
            var t2total6 = 0.0;
            var t2total7 = 0.0;
            var t2total8 = 0.0;
            var t2total9 = 0.0;
            var t2total10 = 0.0;
            var t2total11 = 0.0;
            var t2total12 = 0.0;
            var t2total13 = 0.0;
            var t2total14 = 0.0;
            var t2total15 = 0.0;
            var t2total16 = 0.0;
            var t2total17 = 0.0;
            var t2total18 = 0.0;
            var t2total19 = 0.0;
            var t2total20 = 0.0;
            for(var i = 1; i < tab2cnt; i++){
                t2total1 = t2total1 + parseFloat(isNaN($('#t2col3_'+i).html()) ? 0.0 : $('#t2col3_'+i).html());
                t2total2 = t2total2 + parseFloat(isNaN($('#t2col4_'+i).html()) ? 0.0 : $('#t2col4_'+i).html());
                t2total3 = t2total3 + parseFloat(isNaN($('#t2col5_'+i).html()) ? 0.0 : $('#t2col5_'+i).html());
                t2total4 = t2total4 + parseFloat(isNaN($('#t2col6_'+i).html()) ? 0.0 : $('#t2col6_'+i).html());
                t2total5 = t2total5 + parseFloat(isNaN($('#t2col7_'+i).html()) ? 0.0 : $('#t2col7_'+i).html());
                t2total6 = t2total6 + parseFloat(isNaN($('#t2col8_'+i).html()) ? 0.0 : $('#t2col8_'+i).html());
                t2total7 = t2total7 + parseFloat(isNaN($('#t2col9_'+i).html()) ? 0.0 : $('#t2col9_'+i).html());
                t2total8 = t2total8 + parseFloat(isNaN($('#t2col10_'+i).html()) ? 0.0 : $('#t2col10_'+i).html());
                t2total9 = t2total9 + parseFloat(isNaN($('#t2col11_'+i).html()) ? 0.0 : $('#t2col11_'+i).html());
                t2total10 = t2total10 + parseFloat(isNaN($('#t2col12_'+i).html()) ? 0.0 : $('#t2col12_'+i).html());
                t2total11 = t2total11 + parseFloat(isNaN($('#t2col13_'+i).html()) ? 0.0 : $('#t2col13_'+i).html());
                t2total12 = t2total12 + parseFloat(isNaN($('#t2col14_'+i).html()) ? 0.0 : $('#t2col14_'+i).html());
                t2total13 = t2total13 + parseFloat(isNaN($('#t2col15_'+i).html()) ? 0.0 : $('#t2col15_'+i).html());
                t2total14 = t2total14 + parseFloat(isNaN($('#t2col16_'+i).html()) ? 0.0 : $('#t2col16_'+i).html());
                t2total15 = t2total15 + parseFloat(isNaN($('#t2col17_'+i).html()) ? 0.0 : $('#t2col17_'+i).html());
                t2total16 = t2total16 + parseFloat(isNaN($('#t2col18_'+i).html()) ? 0.0 : $('#t2col18_'+i).html());
                t2total17 = t2total17 + parseFloat(isNaN($('#t2col19_'+i).html()) ? 0.0 : $('#t2col19_'+i).html());
                t2total18 = t2total18 + parseFloat(isNaN($('#t2col20_'+i).html()) ? 0.0 : $('#t2col20_'+i).html());
                t2total19 = t2total19 + parseFloat(isNaN($('#t2col21_'+i).html()) ? 0.0 : $('#t2col21_'+i).html());
                t2total20 = t2total20 + parseFloat(isNaN($('#t2col22_'+i).html()) ? 0.0 : $('#t2col22_'+i).html());

            }
            $('#t2total1').html(t2total1);
            $('#t2total2').html(t2total2.toFixed(3));
            $('#t2total3').html(t2total3);
            $('#t2total4').html(t2total4.toFixed(3));
            $('#t2total5').html(t2total5);
            $('#t2total6').html(t2total6.toFixed(3));
            $('#t2total7').html(t2total7);
            $('#t2total8').html(t2total8.toFixed(3));
            $('#t2total9').html(t2total9);
            $('#t2total10').html(t2total10.toFixed(3));
            $('#t2total11').html(t2total11);
            $('#t2total12').html(t2total12.toFixed(3));
            $('#t2total13').html(t2total13);
            $('#t2total14').html(t2total14.toFixed(3));
            $('#t2total15').html(t2total15);
            $('#t2total16').html(t2total16.toFixed(3));
            $('#t2total17').html(t2total17);
            $('#t2total18').html(t2total18.toFixed(3));
            $('#t2total19').html(t2total19);
            $('#t2total20').html(t2total20.toFixed(3));

            var tab3cnt = $('#tab3cnt').val();
            var t3total1 = 0.0;
            var t3total2 = 0.0;
            var t3total3 = 0.0;
            var t3total4 = 0.0;
            var t3total5 = 0.0;
            var t3total6 = 0.0;
            var t3total7 = 0.0;
            var t3total8 = 0.0;
            var t3total9 = 0.0;
            var t3total10 = 0.0;
            var t3total11 = 0.0;
            var t3total12 = 0.0;
            var t3total13 = 0.0;
            var t3total14 = 0.0;
            var t3total15 = 0.0;
            var t3total16 = 0.0;
            var t3total17 = 0.0;
            var t3total18 = 0.0;
            var t3total19 = 0.0;
            var t3total20 = 0.0;
            for(var i = 1; i < tab3cnt; i++){
                t3total1 = t3total1 + parseFloat(isNaN($('#t3col3_'+i).html()) ? 0.0 : $('#t3col3_'+i).html());
                t3total2 = t3total2 + parseFloat(isNaN($('#t3col4_'+i).html()) ? 0.0 : $('#t3col4_'+i).html());
                t3total3 = t3total3 + parseFloat(isNaN($('#t3col5_'+i).html()) ? 0.0 : $('#t3col5_'+i).html());
                t3total4 = t3total4 + parseFloat(isNaN($('#t3col6_'+i).html()) ? 0.0 : $('#t3col6_'+i).html());
                t3total5 = t3total5 + parseFloat(isNaN($('#t3col7_'+i).html()) ? 0.0 : $('#t3col7_'+i).html());
                t3total6 = t3total6 + parseFloat(isNaN($('#t3col8_'+i).html()) ? 0.0 : $('#t3col8_'+i).html());
                t3total7 = t3total7 + parseFloat(isNaN($('#t3col9_'+i).html()) ? 0.0 : $('#t3col9_'+i).html());
                t3total8 = t3total8 + parseFloat(isNaN($('#t3col10_'+i).html()) ? 0.0 : $('#t3col10_'+i).html());
                t3total9 = t3total9 + parseFloat(isNaN($('#t3col11_'+i).html()) ? 0.0 : $('#t3col11_'+i).html());
                t3total10 = t3total10 + parseFloat(isNaN($('#t3col12_'+i).html()) ? 0.0 : $('#t3col12_'+i).html());
                t3total11 = t3total11 + parseFloat(isNaN($('#t3co113_'+i).html()) ? 0.0 : $('#t3col13_'+i).html());
                t3total12 = t3total12 + parseFloat(isNaN($('#t3col14_'+i).html()) ? 0.0 : $('#t3col14_'+i).html());
                t3total13 = t3total13 + parseFloat(isNaN($('#t3col15_'+i).html()) ? 0.0 : $('#t3col15_'+i).html());
                t3total14 = t3total14 + parseFloat(isNaN($('#t3col16_'+i).html()) ? 0.0 : $('#t3col16_'+i).html());
                t3total15 = t3total15 + parseFloat(isNaN($('#t3col17_'+i).html()) ? 0.0 : $('#t3col17_'+i).html());
                t3total16 = t3total16 + parseFloat(isNaN($('#t3col18_'+i).html()) ? 0.0 : $('#t3col18_'+i).html());
                t3total17 = t3total17 + parseFloat(isNaN($('#t3col19_'+i).html()) ? 0.0 : $('#t3col19_'+i).html());
                t3total18 = t3total18 + parseFloat(isNaN($('#t3col20_'+i).html()) ? 0.0 : $('#t3col20_'+i).html());
                t3total19 = t3total19 + parseFloat(isNaN($('#t3col21_'+i).html()) ? 0.0 : $('#t3col21_'+i).html());
                t3total20 = t3total20 + parseFloat(isNaN($('#t3col22_'+i).html()) ? 0.0 : $('#t3col22_'+i).html());

            }
            $('#t3total1').html(t3total1);
            $('#t3total2').html(t3total2.toFixed(3));
            $('#t3total3').html(t3total3);
            $('#t3total4').html(t3total4.toFixed(3));
            $('#t3total5').html(t3total5);
            $('#t3total6').html(t3total6.toFixed(3));
            $('#t3total7').html(t3total7);
            $('#t3total8').html(t3total8.toFixed(3));
            $('#t3total9').html(t3total9);
            $('#t3total10').html(t3total10.toFixed(3));
            $('#t3total11').html(t3total11);
            $('#t3total12').html(t3total12.toFixed(3));
            $('#t3total13').html(t3total13);
            $('#t3total14').html(t3total14.toFixed(3));
            $('#t3total15').html(t3total15);
            $('#t3total16').html(t3total16.toFixed(3));
            $('#t3total17').html(t3total17);
            $('#t3total18').html(t3total18.toFixed(3));
            $('#t3total19').html(t3total19);
            $('#t3total20').html(t3total20.toFixed(3));

            $('#total1').html((t1total1+t2total1+t3total1));
            $('#total2').html((t1total2+t2total2+t3total2).toFixed(3));
            $('#total3').html((t1total3+t2total3+t3total3));
            $('#total4').html((t1total4+t2total4+t3total4).toFixed(3));
            $('#total5').html((t1total5+t2total5+t3total5));
            $('#total6').html((t1total6+t2total6+t3total6).toFixed(3));
            $('#total7').html((t1total7+t2total7+t3total7));
            $('#total8').html((t1total8+t2total8+t3total8).toFixed(3));
            $('#total9').html((t1total9+t2total9+t3total9));
            $('#total10').html((t1total10+t2total10+t3total10).toFixed(3));
            $('#total11').html((t1total11+t2total11+t3total11));
            $('#total12').html((t1total12+t2total12+t3total12).toFixed(3));
            $('#total13').html((t1total13+t2total13+t3total13));
            $('#total14').html((t1total14+t2total14+t3total14).toFixed(3));
            $('#total15').html((t1total15+t2total15+t3total15));
            $('#total16').html((t1total16+t2total16+t3total16).toFixed(3));
            $('#total17').html((t1total17+t2total17+t3total17));
            $('#total18').html((t1total18+t2total18+t3total18).toFixed(3));
            $('#total19').html((t1total19+t2total19+t3total19));
            $('#total20').html((t1total20+t2total20+t3total20).toFixed(3));

            $('#tab1 tr').each(function() {
                for(var i = 0;i<this.cells.length;i++){
                    if(this.cells[i].innerHTML=="0.00"){
                        this.cells[i].innerHTML = '0';
                    }
                    if(this.cells[i].innerHTML.toString().indexOf('label')!=-1){
                        if($(this.cells[i]).children()[0].innerHTML=="0.00"){
                            $(this.cells[i]).children()[0].innerHTML = '0';
                        }
                        //this.cells[i].innerHTML = '0';
                    }
                }
                //$(this).html($(this).find("td").html());
            });
            $('#tab2 tr').each(function() {
                for(var i = 0;i<this.cells.length;i++){
                    if(this.cells[i].innerHTML=="0.00"){
                        this.cells[i].innerHTML = '0';
                    }
                    if(this.cells[i].innerHTML.toString().indexOf('label')!=-1){
                        if($(this.cells[i]).children()[0].innerHTML=="0.00"){
                            $(this.cells[i]).children()[0].innerHTML = '0';
                        }
                        //this.cells[i].innerHTML = '0';
                    }
                }
                //var customerId = $(this).find("td:first").html();
            });
            $('#tab3 tr').each(function() {
                for(var i = 0;i<this.cells.length;i++){
                    if(this.cells[i].innerHTML=="0.00"){
                        this.cells[i].innerHTML = '0';
                    }
                    if(this.cells[i].innerHTML.toString().indexOf('label')!=-1){
                        if($(this.cells[i]).children()[0].innerHTML=="0.00"){
                            $(this.cells[i]).children()[0].innerHTML = '0';
                        }
                        //this.cells[i].innerHTML = '0';
                    }
                }
                //var customerId = $(this).find("td:first").html();
            });

            $(document).ready(function() {

                $("#print").click(function() {
                    //alert('sa');
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });

            function printElem(options){
                //alert(options);
                /*$('#print').hide();
                $('#saveASPDF').hide();
                $('#goBack').hide();*/
                $('#print_area').printElement(options);
                /*$('#print').show();
                $('#saveASPDF').show();
                $('#goBack').show();*/
                //$('#trLast').hide();
            }

        </script>
        <%if (isNotPDF) {%>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <%}%>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabReport");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>