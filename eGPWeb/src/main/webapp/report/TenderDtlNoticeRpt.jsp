<%-- 
    Document   : TenderDtlNoticeRpt
    Created on : Nov 29, 2010, 11:27:54 AM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>View IFB /PQ / REOI / RFP Notice details</title>
        <style type="text/css">
            body
            {
                margin: 0 0 0 0;
                padding: 0 0 0 0;
                font-family: verdana, arial;
                font-size: 12px;
                color: #0D0D0D;
            }
        </style>
    </head>
    <body>
        <div class="dashboard_div">
            <div style="height: 30px;line-height: 30px;font-weight: bold;font-size: 14px;font-family: arial,verdana;">View Notice Details</div>
            <div>&#160;</div>
            <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
            <%
                        String id = "";
                        if (!request.getParameter("id").equals("")) {
                            id = request.getParameter("id");
                        }

            %>
            <%
                        for (CommonTenderDetails commonTenderDetails : tenderInfoServlet.getAPPDetails(Integer.parseInt(id), "tender")) {
            %>
            <table style="empty-cells: show;" border="1" cellspacing="0" cellpadding="10" class="formStyle_1 t_space" width="100%">
                <tr>
                    <td width="20%" class="ff">Ministry :</td>
                    <td width="30%"><%=commonTenderDetails.getMinistry()%></td>
                    <td width="20%" class="ff">Division :</td>
                    <td width="30%"><%=commonTenderDetails.getDivision()%></td>
                </tr>
                <tr>
                    <td class="ff">Agency :</td>
                    <td><%=commonTenderDetails.getAgency()%></td>
                    <td class="ff">Procuring Entity Name :</td>
                    <td><%=commonTenderDetails.getPeOfficeName()%></td>
                </tr>
                <tr>
                    <td class="ff">Procuring Entity Code :</td>
                    <td><%=commonTenderDetails.getPeCode()%></td>
                    <td class="ff">Procuring Entity Dzongkhag / District :</td>
                    <td><%=commonTenderDetails.getPeDistrict()%></td>
                </tr>
                <tr>
                    <td class="ff">Event Type  :</td>
                    <td><% if (commonTenderDetails.getEventType() != null) {%><%=commonTenderDetails.getEventType()%><% }%></td>
                    <% if (commonTenderDetails.getProcurementNature().equals("Goods") || commonTenderDetails.getProcurementNature().equals("Works")) {%> <td class="ff">Invitation For :</td>
                    <td><% if (commonTenderDetails.getInvitationFor() != null) {%><%=commonTenderDetails.getInvitationFor()%><% }%></td> <% }%>
                </tr><% if (commonTenderDetails.getProcurementNature().equals("Services")) {

                                                String msg = "";
                                                if (commonTenderDetails.getEventType() != null) {
                                                    if (commonTenderDetails.getEventType().equals("REOI")) {
                                                        msg = "Request for expression of interest for selection of";
                                                    }
                                                    else if (commonTenderDetails.getEventType().equals("RFP")) {
                                                        msg = "RFP for selection of";
                                                    }
                                                    else if (commonTenderDetails.getEventType().equals("RFA")) {
                                                        msg = "Request For Application";
                                                    }
                                                }
                %>
                <tr>
                    <td class="ff"><%=msg%><% if (!msg.equals("")) {%><span>*</span></td>
                    <td><label><%=commonTenderDetails.getReoiRfpFor()%></label><%--<input name="expInterestSel" type="text" class="formTxtBox_1" id="txtexpInterestSel" style="width:200px;"  />--%></td><% }%>
                    <td class="ff"></td>
                    <td></td>
                </tr><% }%>
                <tr>
                    <td class="ff">Procurement type :</td>
                    <td><%=commonTenderDetails.getProcurementType()%></td>
                    <td class="ff"></td>
                    <td></td>
                </tr>

                <tr>
                    <td class="ff">Contract Type : </td>
                    <td><%=commonTenderDetails.getContractType()%></td>
                    <%  String msgTender = "";
                                                if (commonTenderDetails.getEventType().equals("PQ") || commonTenderDetails.getEventType().equals("TENDER")) {
                                                    msgTender = "Invitation Reference No.";
                                                }
                                                else if (commonTenderDetails.getEventType().equals("REOI")) {
                                                    msgTender = "Invitation REOI No.";
                                                }
                                                else if (commonTenderDetails.getEventType().equals("RFP")) {
                                                    msgTender = "Invitation RFP No.";
                                                }
                                                else if (commonTenderDetails.getEventType().equals("RFA")) {
                                                    msgTender = "Invitation RFA No.";
                                                }
                    %>
                    <td class="ff"><%=msgTender%> <% if (!msgTender.equals("")) {%>: <%}%></td>
                    <td>
                        <% if (!msgTender.equals("")) {%>
                        <%=commonTenderDetails.getReoiRfpRefNo()%><% }%>
                    </td>
                </tr>

            </table>

            <div style="height: 30px;line-height: 30px;font-weight: bold;">Key Information:</div>
            <table style="empty-cells: show;" border="1" cellspacing="0" cellpadding="10" class="formStyle_1 t_space" width="100%">
                <tr>
                    <td width="20%" class="ff">Procurement Method : </td>
                    <td width="30%"><%=commonTenderDetails.getProcurementMethod()%></td>
                    <td width="20%" class="ff">&#160;</td>
                    <td width="30%">&#160;</td>
                </tr>
            </table>

            <div style="height: 30px;line-height: 30px;font-weight: bold;">Funding information :</div>
            <table style="empty-cells: show;" border="1" cellspacing="0" cellpadding="10" class="formStyle_1 t_space" width="100%">
                <tr>
                    <td width="20%" class="ff">Budget Type :</td>
                    <td width="30%"><%=commonTenderDetails.getBudgetType()%></td>
                    <td width="20%" class="ff">Source of Funds :</td>
                    <td width="30%"><%=commonTenderDetails.getSourceOfFund()%></td>
                </tr>
                <tr>
                    <td width="20%" class="ff">Development Partner :</td>
                    <td width="30%"><% if (commonTenderDetails.getDevPartners() != null) {%><%=commonTenderDetails.getDevPartners()%><% }%></td>
                    <td width="20%" class="ff"></td>
                    <td width="30%"></td>
                </tr>

            </table>

            <div style="height: 30px;line-height: 30px;font-weight: bold;">Particular Information :</div>
            <table style="empty-cells: show;" border="1" cellspacing="0" cellpadding="10" class="formStyle_1 t_space" width="100%">
                <tr>
                    <td width="20%" class="ff">Project Code : </td>
                    <td width="30%"><% if (commonTenderDetails.getProjectCode() != null) {%><%=commonTenderDetails.getProjectCode()%><% }%></td>
                    <td width="20%" class="ff">Project Name : </td>
                    <td width="30%"><% if (commonTenderDetails.getProjectName() != null) {%><%=commonTenderDetails.getProjectName()%><% }%></td>
                </tr>
                <tr>
                    <td class="ff"><% if (!(commonTenderDetails.getEventType().equals("REOI") || commonTenderDetails.getEventType().equals("RFA") || commonTenderDetails.getEventType().equals("RFP"))) {%>Tender/Proposal <% }%>Package No. : </td>
                    <td><%=commonTenderDetails.getPackageNo()%></td>
                    <td class="ff"><% if (!(commonTenderDetails.getEventType().equals("REOI") || commonTenderDetails.getEventType().equals("RFA") || commonTenderDetails.getEventType().equals("RFP"))) {%>Tender/Proposal <% }%> Package Name. : </td>
                    <td><%=commonTenderDetails.getPackageDescription()%></td>
                </tr>
                <tr>
                    <td class="ff">Category : </td>
                    <td><%=commonTenderDetails.getCpvCode()%></td>
                    <td class="ff">&#160;</td>
                    <td class="formStyle_1">&#160;</td>
                </tr>
                <tr>
                    <td class="ff"><% if (!commonTenderDetails.getEventType().equals("PQ")) {%><%=commonTenderDetails.getEventType()%> <% }
                     else {%>Pre-Qaulification <% }%>Publication date &amp; time : </td>
                    <td class="formStyle_1"><% if (commonTenderDetails.getTenderPubDt() != null) {%><%=commonTenderDetails.getTenderPubDt()%><% }%></td>
                    <td class="ff"><% if (!commonTenderDetails.getEventType().equals("PQ")) {%><%=commonTenderDetails.getEventType()%> <% }
                     else {%>Pre-Qaulification <% }%> Document last selling date &amp; time :</td>
                    <td class="formStyle_1"><% if (commonTenderDetails.getDocEndDate() != null) {%><%=commonTenderDetails.getDocEndDate()%><% }%></td>
                </tr>

                <tr>
                    <td class="ff"><% if (commonTenderDetails.getEventType().equals("PQ")) {%>Pre - Qualification <% }
                     else {%>Pre - Tender/Proposal <%}%>meeting Start Date &amp; Time :</td>
                    <td class="formStyle_1"><% if (commonTenderDetails.getPreBidStartDt() != null) {%><%=commonTenderDetails.getPreBidStartDt()%><% }%></td>
                    <td class="ff"><% if (commonTenderDetails.getEventType().equals("PQ")) {%>Pre - Qualification <% }
                     else {%>Pre - Tender/Proposal <%}%>meeting End Date &amp; Time :</td>
                    <td class="formStyle_1"><% if (commonTenderDetails.getPreBidEndDt() != null) {%><%=commonTenderDetails.getPreBidEndDt()%><% }%></td>
                </tr>
                <tr>
                        <td class="ff"><% if (commonTenderDetails.getEventType().equals("PQ")) {%>Pre-Qualification <% }
                                                    if (commonTenderDetails.getEventType().equals("TENDER")) {%>Tender/Proposal <% }
                                                                                if (commonTenderDetails.getEventType().equals("REOI") || commonTenderDetails.getEventType().equals("RFP")) {%>Proposal <% }%> EOI Opening Date &amp; Time : </td>
                    <td class="formStyle_1"><% if (commonTenderDetails.getSubmissionDt() != null) {%><%=commonTenderDetails.getSubmissionDt()%><% }%></td>
                        <td class="ff"><% if (commonTenderDetails.getEventType().equals("PQ")) {%>Pre-Qualification <% }
                                                    if (commonTenderDetails.getEventType().equals("TENDER")) {%>Tender/Proposal <% }
                                                                                if (commonTenderDetails.getEventType().equals("REOI") || commonTenderDetails.getEventType().equals("RFP")) {%>Proposal <% }%> EOI Closing Date &amp; Time :</td>
                    <td class="formStyle_1"><% if (commonTenderDetails.getOpeningDt() != null) {%><%=commonTenderDetails.getOpeningDt()%><% }%></td>
                </tr>
            </table>

                <div style="height: 30px;line-height: 30px;font-weight: bold;">Information for Bidder/Consultant :</div>

            <table style="empty-cells: show;" border="1" cellspacing="0" cellpadding="10" class="formStyle_1 t_space" width="100%">
                <tr>
                    <td width="41%" class="ff">Eligibility  of Bidder/Consultant :</td>
                    <td width="59%"><%=commonTenderDetails.getEligibilityCriteria()%></td>
                </tr>
                <tr>
                    <td class="ff">Brief description of Goods and Related <br />
                        Service / Works Brief description of assignment :</td>
                    <td><%=commonTenderDetails.getTenderBrief()%></td>
                </tr>
                <tr>
                    <td class="ff">Experience,  Resources and delivery capacity required :</td>
                    <td><%=commonTenderDetails.getDeliverables()%></td>
                </tr>
                <tr>
                    <td class="ff">Other details (if  applicable) :</td>
                    <td><%=commonTenderDetails.getOtherDetails()%></td>
                </tr>
                <tr>
                    <td class="ff"> Association with
                        foreign  firm :</td>
                    <td><%=commonTenderDetails.getForeignFirm()%></td>
                </tr>
                <tr>
                    <td class="ff"> Document  available :</td>
                    <td><%=commonTenderDetails.getDocAvlMethod()%></td>
                </tr>
                <tr>
                    <td class="ff"> Evaluation Type :</td>
                    <td><%=commonTenderDetails.getEvalType()%></td>
                </tr>
                <tr>
                    <td class="ff">Document Fees :</td>
                    <td><%=commonTenderDetails.getDocFeesMethod()%></td>
                </tr>
                <tr>
                    <td class="ff">
                        Pre-Qualification  document price (Nu.)<br />
                        Price of Application Form (Nu.)<br />
                        Tender Document Price (Nu.) :</td>
                    <td><%=commonTenderDetails.getPkgDocFees()%></td>
                </tr>

                <tr>
                    <td class="ff">Mode  of document fees :</td>
                    <td><%=commonTenderDetails.getDocFeesMode()%> </td>
                </tr>
                <tr>
                    <td class="ff">
      		Name &amp; Address
			of the Office(s)<br />
                        Selling Pre-qualification /
	  Tender Document :</td>
                    <td><%=commonTenderDetails.getDocOfficeAdd()%></td>
                </tr>
                <tr>
                    <td class="ff">
      	Last date &amp; time
		for Tender Security <br />
	  Submission :</td>
                    <td><% if (commonTenderDetails.getSecurityLastDt() != null) {%><%=commonTenderDetails.getSecurityLastDt()%><% }%></td>
                </tr>

                <tr>
                    <td class="ff"> Name &amp; Address<br />
                            of the Office(s) for Tender security submission :</td>
                    <td><%=commonTenderDetails.getSecuritySubOff()%></td>
                </tr>
            </table>

            <table style="empty-cells: show;" border="1" cellspacing="0" cellpadding="10" class="formStyle_1 t_space" width="100%">
                <tr>
                    <th class="t-align-center">Ref. No. <br /></th>
                    <th class="t-align-center">Phasing of service <br /></th>
                    <th class="t-align-center">Location</th>
                    <th class="t-align-center" >Indicative Start Date <br /></th>
                    <th class="t-align-center">Indicative Completion Date <br /></th>
                </tr>
                <% int i = 0;
                                            for (CommonTenderDetails commonTdetails : tenderInfoServlet.getAPPDetails(Integer.parseInt(id), "Phase")) {
                                                i++;
                %>
                <tr>
                    <td class="t-align-center"><%=commonTdetails.getReoiRfpRefNo()%></td>
                    <td class="t-align-center"><%=commonTdetails.getPhasingOfService()%></td>
                    <td class="t-align-center"><% if (commonTenderDetails.getLocation() != null) {%><%=commonTenderDetails.getLocation()%><% }%></td>
                    <td class="t-align-center"><%=commonTdetails.getIndStartDt()%></td>
                    <td class="t-align-center"><%=commonTdetails.getIndEndDt()%></td>
                </tr>
                <%}%>
            </table>


            <table style="empty-cells: show;" border="1" cellspacing="0" cellpadding="10" class="formStyle_1 t_space" width="100%">
                <tr>
                    <th class="t-align-center">Lot No. <br /></th>
                    <th class="t-align-center">Identification of Lot <br /></th>
                    <th class="t-align-center">Location <br /></th>
                    <th class="t-align-center">Document Fees <br /> (Amount in  Nu.) <br /></th>
                    <th class="t-align-center">Tender security <br /> (Amount in  Nu.) <br /></th>
                    <th class="t-align-center">Completion time <br /> in weeks/months <br /></th>
                </tr>
                <% int j = 0;
                                            for (CommonTenderDetails commonTdetails : tenderInfoServlet.getAPPDetails(Integer.parseInt(id), "lot")) {
                                                j++;
                %>
                <tr>
                    <td class="t-align-center"><%=commonTdetails.getLotNo()%></td>
                    <td class="t-align-center"><%=commonTdetails.getLotDesc()%></td>
                    <td class="t-align-center"><% if (commonTenderDetails.getLocation() != null) {%><%=commonTenderDetails.getLocation()%><% }%></td>
                    <td class="t-align-center"><%=commonTdetails.getDocFess()%></td>
                    <td class="t-align-center"><%=commonTdetails.getTenderSecurityAmt()%></td>
                    <td class="t-align-center"><%=commonTdetails.getCompletionTime()%></td>
                </tr>
                <%}%>
            </table>

            <div style="height: 30px;line-height: 30px;font-weight: bold;">Procuring Entity Details:</div>
            <table style="empty-cells: show;" border="1" cellspacing="0" cellpadding="10" class="formStyle_1 t_space" width="100%">
                <tr>
                    <td class="ff" width="20%"> Name  of official Inviting Tender / Pre-Qualification / REOI :</td>
                    <td width="30%"><%=commonTenderDetails.getPeName()%></td>
                    <td class="ff" width="20%"> Designation  of official inviting Tender / Pre-Qualification / Application :</td>
                    <td width="30%"><%=commonTenderDetails.getPeDesignation()%></td>
                </tr>

                <tr>
                    <td class="ff">Address of official inviting Tender / Pre-Qualification / Application : </td>
                    <td><%=commonTenderDetails.getPeAddress()%></td>
                    <td class="ff">Contact details of official inviting Tender / Pre-Qualification / Application :</td>
                    <td><%=commonTenderDetails.getPeContactDetails()%></td>
                </tr>

                <tr>
                    <td colspan="4" class="ff mandatory">The procuring entity  reserves the right to accept or reject all tenders / Pre-Qualifications / EOIs</td>
                </tr>
            </table>
            <% }%>
            <div>&#160;</div>
        </div>
    </body>
</html>
