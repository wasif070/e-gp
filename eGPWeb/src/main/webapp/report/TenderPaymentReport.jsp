<%-- 
    Document   : TenderPaymentReport
    Created on : Jul 14, 2011, 12:14:30 PM
    Author     : shreyansh
--%>
<%@page import="java.util.Collection"%>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Document Fees Report</title>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
<!--        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>-->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <script src="../resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript">
            /* Call Print function */
            $(document).ready(function() {
                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });
            });
            function printElem(options){
                $('#titleDiv').show();
                $('#print_area').printElement(options);
                $('#titleDiv').hide();
            }
        </script>
        <script language="javascript">
            function regForNumber(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function CompareToForGreater()
            {
                var flag = true;
                document.getElementById("pageNo").value="1";
                var value = document.getElementById("DateTO").value;
                var txtdepartment = document.getElementById("txtdepartment").value;
                var bankName = document.getElementById("bankName").value;
                var amount = document.getElementById("amount").value;
                var params = document.getElementById("DateFrom").value;
                var tenderId = document.getElementById("tenderid").value;
                var paymentMode = document.getElementById("cmbPaymentFor").value;
                if(value=='' && txtdepartment=='' && bankName=='' && amount=='' && params=='' && tenderId=='' && paymentMode==''){
                    jAlert("Please enter search criteria","Tender Payment Report", function(RetVal) {
                    });
                    flag=false;
                }else{
                    if(value!='' && params!=''){
                        var mdy = value.split('/')  //Date and month split
                        var mdyhr=mdy[2].split(' ');  //Year and time split
                        var mdyp = params.split('/')
                        var mdyphr=mdyp[2].split(' ');
                        if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                        {
                            //alert('Both Date');
                            var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                        }
                        else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                        {
                            //alert('Both DateTime');
                            var mdyhrsec=mdyhr[1].split(':');
                            var date =  new Date( mdyhr[0], parseInt(mdy[1])-1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                            var mdyphrsec=mdyphr[1].split(':');
                            var datep =  new Date(mdyphr[0], parseInt(mdyp[1])-1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                        }
                        else
                        {
                            var a = mdyhr[1];  //time
                            var b = mdyphr[1]; // time
                            if(a == undefined && b != undefined)
                            {
                                var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                                var mdyphrsec=mdyphr[1].split(':');
                                var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                            }
                            else
                            {
                                var mdyhrsec=mdyhr[1].split(':');
                                var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                            }
                        }
                        if((Date.parse(date) > Date.parse(datep))){
                            jAlert("Payment Date From  must be prior to Payment Date to ","Tender/Proposal Payment Report", function(RetVal) {
                            });
                            flag=false;
                        }
                    }
                    
                    if(amount!=''){
                        if(!regForNumber(amount)){
                            jAlert("Please enter number(0-9) only","Tender/Proposal Payment Report", function(RetVal) {
                            });
                            flag=false;
                        }else if(amount.length>12){
                            jAlert("Amount must not be greater then 12 digits","Tender/Proposal Payment Report", function(RetVal) {
                            });
                            flag=false;
                        }
                        else{
                            if(document.getElementById("cmbamount").value=='0'){
                                jAlert("Please select amount operator","Tender/Proposal Payment Report", function(RetVal) {
                                });
                                flag=false
                            }
                        }
                    
                    }
                    if(tenderId!=''){
                        if(!regForNumber(tenderId)){
                            jAlert("Please enter number(0-9) only","Tender/Proposal Payment Report", function(RetVal) {
                            });
                            flag=false;
                        }

                    }
                }
                if(flag){
                    loadTable();
                }
            }

            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: true,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
        <script type="text/javascript">
            /* check if pageNO is disable or not */
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            /*  load Grid for the User Registration Report */
            function loadTable()
            {
                
                if($("#keyWord").val() == undefined)
                    $("#keyWord").val('');
                $.post("<%=request.getContextPath()%>/TenderPaymentReportServlet", {paymentMode : $('#cmbPaymentFor').val(),tenderid: $("#tenderid").val(),size: $("#size").val(),txtdepartmentid : $('#txtdepartmentid').val(),cmbOffice :$("#cmbOffice").val(),bankName : $("#bankName").val(),amount : $("#amount").val(),cmbamount : $("#cmbamount").val(),DateTO : $("#DateTO").val(),DateFrom: $("#DateFrom").val(),pageNo: $("#pageNo").val(),paymentfor: $("#paymentfor").val(),branchName:$('#cmbDescmbBankignation').val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    sortTable();
                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#tohide').show();
                    document.getElementById("tobehide").style.display='';
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            /*  Handle First click event */
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo =$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Last Button click event */
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0){
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            /*  Handle Next Button click event */
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo)+1);

                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            /*  Handle Previous click event */
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            function Search(){
                var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
            }
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                   
                });
            });
        </script>
        <script language="javascript">
            function checkExpiry(stausValue){
                if(stausValue=="Expired"){
                    document.getElementById("paymentRow").style.display = "none";
                }else{
                    document.getElementById("paymentRow").style.display = "table-row";
                }
            }
            function loadOffice() {
                if($('#txtdepartment').val()!=''){
                    $('#cmbOffice').removeAttr('disabled');
                }
                var deptId=$('#txtdepartmentid').val();
                $.post("<%=request.getContextPath()%>/ComboServlet", {departmentId: deptId,funName:'tenderOfficeCombo'},  function(j){
                    $('#cmbOffice').children().remove().end()
                    $("select#cmbOffice").html(j);
                });
            }
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });


                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            function hideonload(){
                $('#tohide').hide();
                document.getElementById("tobehide").style.display="none";

            }
function setBranch(){
    if($('#bankName').val()!= "" && $('#bankName').val()!= "0" && $('#bankName').val()!= 'Dutch Bangla Bank(DBBL)' && $('#bankName').val()!= 'BRAC Bank'){
        $.post("<%=request.getContextPath()%>/ComboServlet", {type:'Scheduled Bank',objectId:$('#bankName').val(),funName:'branchCombo'}, function(j){
            $('#cmbDescmbBankignation').removeAttr("disabled");
            $("select#cmbDescmbBankignation").html(j);
        });
    }else{
        $('#cmbDescmbBankignation').attr('disabled',true);
    }
}
function chkBank(){
    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbPaymentFor').val(),funName:'paymentType'}, function(j){
        $("select#bankName").html(j);
     });
//  if($('#cmbPaymentFor').val()== "Online"){
//      $('#bankName').attr('disabled',true);
//      $('#cmbDescmbBankignation').attr('disabled',true);
//  }else{
//      $('#bankName').removeAttr("disabled");
//      $('#cmbDescmbBankignation').removeAttr("disabled");
//  }  
}
        </script>
        <%
                    int userid = 0;
                    int usertypeid = 0;
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userid = Integer.parseInt(hs.getAttribute("userId").toString());
                    }
                    if (hs.getAttribute("userTypeId") != null) {
                        usertypeid = Integer.parseInt(hs.getAttribute("userTypeId").toString());
                    }
                    CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> list = csdms.geteGPData("getMainPaymentBanksList", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
               /**
                Online Bank Name added from List
                **/
                Collection<String> bankval = null;
                bankval = XMLReader.getBnkMsg().values();
                Object[] valArr = bankval.toArray();
                for (int i = 0; i < valArr.length; i++) {
                    if (i % 2 == 1) {
                        SPCommonSearchDataMore onlineBank = new SPCommonSearchDataMore();
                        onlineBank.setFieldName1(valArr[i].toString());
                        onlineBank.setFieldName2(valArr[i].toString());
                        list.add(onlineBank);
                    }
                }
        %>
    </head>
    <body onload="hideonload();">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <div class="contentArea_1">
                <div class="DashboardContainer">
                    <div class="pageHead_1">Document Fees Report</div>
                    <div class="formBg_1 t_space">
                        <form id="frmtenderreport" name="frmtenderreport" action="TenderPaymentReport.jsp" method="post">
                            <table width="100%" cellspacing="8" class="formStyle_1">
                                <tr>
                                    <td width="21%" class="ff">Payment Type:</td>
                                    <td>
                                        <select name="cmbPaymentFor" class="formTxtBox_1" id="cmbPaymentFor" style="width:80px;" onchange="chkBank();">
                                            <option value="All">All</option>
                                            <option value="Online">Online</option>
                                            <option value="Offline">Offline</option>
                                        </select>
                                    </td>
                                    <td class="ff">&nbsp;</td>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="21%" class="ff">Ministry / Division / Organization :</td>
                                    <td>
                                        <input type="text" style="width: 200px;" readonly  id="txtdepartment" name="txtdepartment" class="formTxtBox_1">
                                        <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />
                                        <input type="hidden"  name="paymentfor" id="paymentfor" value="Document Fees" />

                                        <a href="javascript:void(0);" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=homeallTenders', '', 'width=350px,height=400px,scrollbars=1','');">
                                            <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                        </a>
                                    </td>
                                    <td class="ff">Procuring Agency :</td>
                                    <td>
                                        <select style="width: 208px;" id="cmbOffice" class="formTxtBox_1" name="cmbOffice" disabled>
                                            <option value="">-- Select Procuring Agency --</option>
                                            <option value="0">All</option>
                                        </select>
                                    </td>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="21%" class="ff">Financial Institution Name:</td>
                                    <td><select name="bankName" class="formTxtBox_1" id="bankName" style="width:250px;" onchange="setBranch();">
                                            <option value="">--Select Financial Institution--</option>
                                            <option value="0">All</option>
                                            <%if (!list.isEmpty() && list != null) {
                                                            for (int i = 0; i < list.size(); i++) {
                                            %>
                                            <option value=<%=list.get(i).getFieldName1()%>><%=list.get(i).getFieldName2()%></option>
                                            <%}
                                                        }%>
                                        </select></td>
                                        <td class="ff">Financial Institution Branch:</td>
                                        <td width="21%">
                                            <select name="branch"  id="cmbDescmbBankignation" class="formTxtBox_1" style="width: 200px" disabled>
                                                <option selected="selected" value="">-- Financial Institution Branch --</option>
                                            </select>
                                        </td>
                                    
                                </tr>
                                <tr>
                                    <td class="ff">Amount :</td>
                                    <td><select name="cmbamount" class="formTxtBox_1" id="cmbamount" style="width:80px;">
                                            <option value="">-Select-</option>
                                            <option value="=">=</option>
                                            <option value="<="><=</option>
                                            <option value=">=">>=</option>
                                        </select>&nbsp;&nbsp;<input name="amount" type="text" class="formTxtBox_1" id="amount" style="width:100px;"  /></td>
                                    <td width="21%" class="ff">Tender/Proposal ID:</td>
                                    <td width="21%"><input type="text" style="width: 200px;"  id="tenderid" name="tenderid" class="formTxtBox_1"></td>
                                </tr>
                                <tr>
                                    <td class="ff">Payment Date From :</td>
                                    <td><input name="DateTO" type="text" class="formTxtBox_1" id="DateTO" style="width:100px;" readonly="readonly" onclick ="GetCal('DateTO','DateTO');" />
                                        &nbsp;<a href="javascript:void(0);" onclick="" title="Calender"><img id="calcfrom" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('DateTO','calcfrom');" /></a></td>

                                    <td class="ff">Payment Date To :</td>
                                    <td><input name="DateFrom" type="text" class="formTxtBox_1" id="DateFrom" style="width:100px;" readonly="readonly" onclick ="GetCal('DateFrom','DateFrom');" />
                                        &nbsp;<a href="javascript:void(0);" onclick="" title="Calender" ><img id="calc" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('DateFrom','calc');"/></a></td>
                                </tr>
                                <tr>
                                    <td height="5"></td>
                                </tr>
                                
                                <tr>
                                    <td colspan="3" class="t-align-center">
                                        <label class="formBtn_1">
                                            <input type="button" name="button" id="button" value="Generate" onclick="CompareToForGreater();" />
                                        </label>
                                        <label class="formBtn_1">
                                            <input type="submit" name="btnReset" id="btnReset" value="Reset" />
                                        </label>
                                    </td>
                                    <%
                                                String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
                                                String folderName = pdfConstant.TenderPayment;
                                                String genId = cdate + "_" + userid;
                                    %>
                                    <td class="t-align-right" id="tobehide">
                                        <%if(usertypeid == 1 || usertypeid == 8) {%>
                                        <!--<a class="action-button-savepdf" href="<%//=request.getContextPath()%>/GeneratePdf?reqURL=&reqQuery=&folderName=<%=folderName%>&id=<%=genId%>" >Save As PDF </a>-->
                                        <a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('8');">Save as PDF</a>
                                        <%} else {%>
                                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('2');">Save as PDF</a></span>
                                        <%}%>
                                        &nbsp;
                                        <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
                                    </td>
                                </tr>
                               
                            </table>
                        </form>
                    </div>
                                        <div id="tohide">
                    <div id="resultDiv" style="display: block;">
                        <div  id="print_area">
                            <div id='titleDiv' class="pageHead_1" style="display: none;">Document Fees Report</div>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable" cols="@0,8">
                                <tr>
                                    <th width="3%" class="t-align-center">Sl. No</th>
                                    <th width="15%" class="t-align-center">Tender/Proposal ID,<nr />Reference No.,<br />
                                        Lot No. </th>
                                    <th width="20%" class="t-align-center">Ministry,<br />
                                        Division,
                                        <br />
                                        Organization,<br /> Procuring Agency </th>
                                    <th width="15%" class="t-align-center">Financial Institution Name,<br />
                                        Branch Name </th>
                                    <th width="14%" class="t-align-center">Amount<br />
                                        (In Nu.)</th>
                                    <th width="13%" class="t-align-center">Payment Date<br />
                                        and Time</th>
                                    <th width="13%" class="t-align-center">Payment Type</th>
                                    <th width="14%" class="t-align-center">Bidder / Consultant</th>
                                    <th width="14%" class="t-align-center noprint">Action</th>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                        <tr>
                            <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                Total Records per page : <input type="text" name="size" id="size" onKeydown="javascript: if (event.keyCode==13) Search();" value="10" style="width:70px;"/>
                            </td>
                            <td align="center"><input name="textfield3" type="text" id="dispPage" onKeydown="javascript: if (event.keyCode==13) Search();" value="1" class="formTxtBox_1" style="width:20px;" />
                                &nbsp;
                                <label class="formBtn_1">
                                    <input type="submit" name="button"  id="btnGoto" value="Go To Page" />
                                </label></td>
                            <td  class="prevNext-container">
                                <ul>
                                    <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                    <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" id="pageNo" value="1"/>
                                        </div>
                    <!--Dashboard Content Part End-->
                </div>
                <form id="formstyle" action="" method="post" name="formstyle">
                   <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                   <%
                     SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                     String appenddate = dateFormat1.format(new Date());
                   %>
                   <input type="hidden" name="fileName" id="fileName" value="DocumentFeesReport_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="DocumentFeesReport" />
                </form>
                <!--Dashboard Footer Start-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End-->
            </div>
    </body>
    <script>
        var headSel_Obj = document.getElementById("headTabReport");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
