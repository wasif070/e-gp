<%-- 
    Document   : PackageDetailReport
    Created on : Nov 10, 2010, 7:23:03 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>View Package Report</title>
        <style type="text/css">
            body
            {
                margin: 0 0 0 0;
                padding: 0 0 0 0;
                font-family: verdana, arial;
                font-size: 12px;
                color: #0D0D0D;
            }

        </style>
         <%if(request.getParameter("print")==null) {%>
    <script type="text/javascript">
        function printPage()
        {
            //alert($("#viewForm"));
            document.viewForm.submit();
        }
    </script>
    <%}%>
    </head>
    <body>
        <jsp:useBean id="appViewPkgDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AppViewPkgDtBean"/>
        <%
                    String strAppId = request.getParameter("appId");
                    String strPkgId = request.getParameter("pkgId");
                    String strPkgType = request.getParameter("pkgType");
                    int appId = 0;
                    int pkgId = 0;
                    if (strAppId != null && !strAppId.equals("")) {
                        appId = Integer.parseInt(strAppId);
                    }
                    if (strPkgId != null && !strPkgId.equals("")) {
                        pkgId = Integer.parseInt(strPkgId);
                    }
        %>
        <div class="t_space">
            <div class="pageHead_1">View APP: Package Details</div>
            <div>&#160;</div>
            <form method="POST" action="<%=request.getContextPath()%>/PDFServlet" name="viewForm">
                <table style="empty-cells: show;" border="1" cellspacing="0" cellpadding="10" class="formStyle_1 t_space" width="100%">
                    <tr>
                        <th colspan="2" valign="top" class="table-section-header"><span>Key fields information:</span>
                        <input type="hidden" name="reportName" value="Package Detail"/></th>
                    </tr>
                    <%
                                if (appId != 0 && pkgId != 0) {
                                    appViewPkgDtBean.populateInfo(appId, pkgId, strPkgType,0);
                    %>
                    <tr>
                        <td width="28%" class="ff">APP Id :</td>
                        <td width="72%" valign="top">${appViewPkgDtBean.appId}</td>
                    </tr>
                    <tr>
                        <td class="ff">Letter Ref. No. :</td>
                        <td valign="top">${appViewPkgDtBean.appCode}</td>
                    </tr>
                    <tr>
                        <td class="ff">Financial Year :</td>
                        <td valign="top">${appViewPkgDtBean.financialYear}</td>
                    </tr>
                    <tr>
                        <td class="ff">Budget Type :</td>
                        <td valign="top">${appViewPkgDtBean.budgetType}</td>
                    </tr>
                    <tr>
                        <td class="ff">Project Name :</td>
                        <td valign="top">${appViewPkgDtBean.projectName}</td>
                    </tr>
                    <tr>
                        <td class="ff">Procuring Entity :</td>
                        <td valign="top">${appViewPkgDtBean.PE}</td>
                    </tr>
                    <tr>
                        <td class="ff">Dzongkhag / District</td>
                        <td valign="top">${appViewPkgDtBean.district}</td>
                    </tr>
                    <tr>
                        <th colspan="2" valign="top" class="table-section-header"><span>Package Details:</span></th>
                    </tr>
                    <tr>
                        <td class="ff">Procurement Category</td>
                        <td valign="top">${appViewPkgDtBean.procurementnature}</td>
                    </tr>
                    <tr>
                        <td class="ff">Service Type</td>
                        <td valign="top">${appViewPkgDtBean.servicesType}</td>
                    </tr>
                    <tr>
                        <td class="ff">Package No</td>
                        <td valign="top">${appViewPkgDtBean.packageNo}</td>
                    </tr>
                    <tr>
                        <td class="ff">Package Description</td>
                        <td valign="top">${appViewPkgDtBean.packageDesc}</td>
                    </tr>
                    <tr>
                        <td class="ff">Allocated Budget (In Mn. Tk.)</td>
                        <td valign="top">${appViewPkgDtBean.allocateBudget}</td>
                    </tr>
                    <tr>
                        <th colspan="2" valign="top" class="table-section-header"><span>Lot Details:</span></th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <%if (appViewPkgDtBean.getPkgLotDeail().size() > 0) {%>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th class="t-align-center">Sl. No.</th>
                                    <th class="t-align-center">Lot Description</th>
                                    <th class="t-align-center">Qty.</th>
                                    <th class="t-align-center">Unit</th>
                                    <th class="t-align-center">Official Cost Estimate (In Crore Tk.)</th>
                                </tr>
                                <c:forEach items="${appViewPkgDtBean.pkgLotDeail}" var="pkgLotDetail" varStatus="status">
                                    <tr>
                                        <td class="t-align-center">${status.count}</td>
                                        <td class="t-align-center">${pkgLotDetail.lotDesc}</td>
                                        <td class="t-align-center">${pkgLotDetail.quantity}</td>
                                        <td class="t-align-center">${pkgLotDetail.unit}</td>
                                        <td class="t-align-center">${pkgLotDetail.lotEstCost}</td>
                                    </tr>
                                </c:forEach>
                            </table>
                            <%}
                                                    else {%>
                            <label class="ff">No Data Found</label> <%}%>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Package Official Cost Estimate (In Crore Tk.)</td>
                        <td class="t-align-left">${appViewPkgDtBean.pkgEstCost}</td>
                    </tr>
                    <tr>
                        <td class="ff">PQ Requires</td>
                        <td class="t-align-left">${appViewPkgDtBean.isPQRequired}</td>
                    </tr>
                    <tr>
                        <td class="ff">REOI/RFA Require</td>
                        <td class="t-align-left">${appViewPkgDtBean.reoiRfaRequired}</td>
                    </tr>
                    <%
                                                                                short methodId = appViewPkgDtBean.getProcurementMethodId();
                                                                                String methodName = appViewPkgDtBean.getProcMethodName(methodId);
                    %>
                    <tr>
                        <td class="ff">Procurement Method</td>
                        <td class="t-align-left"><%=methodName%></td>
                    </tr>
                    <tr>
                        <td class="ff">Procurement Type</td>
                        <td class="t-align-left">${appViewPkgDtBean.procurementType}</td>
                    </tr>
                    <tr>
                        <td class="ff">Source of Fund</td>
                        <td class="t-align-left">${appViewPkgDtBean.sourceOfFund}</td>
                    </tr>
                    <tr>
                        <td class="ff">Development Partners</td>
                        <td class="t-align-left">&#160;</td>
                    </tr>

                    <tr>
                        <th colspan="2" valign="top" class="table-section-header"><span>Package Date Information:</span></th>
                    </tr>
                    <%
                                                                                String nature = appViewPkgDtBean.getProcurementnature();
                                                                                String isPQReqd = appViewPkgDtBean.getIsPQRequired();
                                                                                if ((nature.equalsIgnoreCase("goods") || nature.equalsIgnoreCase("works")) && isPQReqd.equals("Yes")) {
                    %>
                    <tr>
                        <th colspan="2" valign="top" class="table-section-header"><span>PQ Tender Dates:</span></th>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Advertisement of Invitation on e-GP website</td>
                        <td class="t-align-left">${appViewPkgDtBean.advtDt} </td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Applications  Submission</td>
                        <td class="t-align-left">${appViewPkgDtBean.subDt}</td>
                    </tr>
                    <%}
                                                                                String type = "";
                                                                                if (isPQReqd.equalsIgnoreCase("Yes")) {
                                                                                    type = "Tender/Proposal";
                                                                                }
                                                                                if (methodName.equalsIgnoreCase("RFQ") || methodName.equalsIgnoreCase("RFQU") || methodName.equalsIgnoreCase("RFQL")) {
                                                                                    type = "RFQ";
                                                                                }
                                                                                else {
                                                                                    type = "Tender/Proposal";
                                                                                }
                    %>

                    <tr>
                        <th colspan="2" valign="top" class="table-section-header"><span><%=type%> Dates:</span></th>
                    </tr>

                    <tr>
                        <td class="ff">Expected Date of Advertisement of <%=type%> on e-GP website</td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderAdvertDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of submission of <%=type%></td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderSubDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Opening of <%=type%></td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderOpenDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Issuance of the Letter of Acceptance (LOA)</td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderNoaIssueDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Signing of Contract</td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderContractSignDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Completion of Contract</td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderContractCompDt}</td>
                    </tr>
                    <%
                                                                                if (methodName.equalsIgnoreCase("TSTM")) {
                    %>
                    <tr>
                        <th colspan="2" valign="top" class="table-section-header"><span>TSTM</span></th>
                    </tr>
                    <tr>
                        <th colspan="2" valign="top" class="table-section-header"><span>1st Stage:</span></th>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Advertisement of IFB on e-GP website</td>
                        <td class="t-align-left">${appViewPkgDtBean.advtDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of submission of Tenders</td>
                        <td class="t-align-left">${appViewPkgDtBean.subDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Opening of Tenders</td>
                        <td class="t-align-left">${appViewPkgDtBean.openDt}</td>
                    </tr>
                    <tr>
                        <th colspan="2" valign="top" class="table-section-header"><span>2nd Stage:</span></th>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of issue of Final Tender Document to qualified bidders</td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderAdvertDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Submission of Tender</td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderSubDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Opening of Tender</td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderOpenDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Issue of Letter of Acceptance (LOA)</td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderNoaIssueDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Signing of Contract</td>
                        <td class="t-align-left"><${appViewPkgDtBean.tenderContractSignDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected date of Completion of Contract</td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderContractCompDt}</td>
                    </tr>
                    <%}
                                                                                if (nature.equalsIgnoreCase("Services")) {
                                                                                    String isREOI = appViewPkgDtBean.getReoiRfaRequired();
                                                                                    if (isREOI.equalsIgnoreCase("REOI")) {
                    %>
                    <tr>
                        <th colspan="2" valign="top" class="table-section-header"><span>REOI Dates: </span></th>
                    </tr>

                    <tr>
                        <td class="ff">Expected Date of Advertisement of REOI on e-GP website</td>
                        <td class="t-align-left">${appViewPkgDtBean.advtDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Last Date of Receipt of EOI</td>
                        <td class="t-align-left">${appViewPkgDtBean.subDt}</td>
                    </tr>
                    <tr>
                        <th colspan="2" valign="top" class="table-section-header"><span>RFP Dates:</span></th>
                    </tr>
                    <tr>
                        <td class="ff">Expected date of Issue of RFP</td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderAdvertDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Submission of Proposal</td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderSubDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Technical proposal opening</td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderOpenDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Technical Proposal Evaluation</td>
                        <td class="t-align-left">${appViewPkgDtBean.rfpTechEvalDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Financial proposal opening</td>
                        <td class="t-align-left">${appViewPkgDtBean.rfpFinancialOpenDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Signing of Contract</td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderContractSignDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Completion of Contract</td>
                        <td class="t-align-left">${appViewPkgDtBean.tenderContractCompDt}</td>
                    </tr>
                    <%}
                                                                else if (isREOI.equalsIgnoreCase("RFA")) {
                    %>
                    <tr>
                        <th colspan="2" valign="top" class="table-section-header"><span>RFA Dates: </span></th>
                    </tr>
                    <tr>
                        <td class="ff">Expected Date of Advertisement of RFA on e-GP Website</td>
                        <td class="t-align-left">${appViewPkgDtBean.rfaAdvertDt}</td>
                    </tr>
                    <tr>
                        <td class="ff">Date of Receipt of application</td>
                        <td class="t-align-left">${appViewPkgDtBean.rfaReceiptDt}</td>
                    </tr>
                    <%}
                                                                                                        }%>
                    <%if (request.getParameter("print")==null) {%>
                    <tr>
                        <td align="center" colspan="2"><a class="action-button-savepdf" onclick="printPage()">Save As PDF</a></td>
                    </tr>
                    <% }
                 }
                else {%>
                    <tr>
                        <td align="center" colspan="2" class="responseMsg errorMsg">
                            <div>Provide Valid App-Id, Package-Id * Packgae Type</div></td>
                    </tr>
                    <%}%>
                </table>
            </form>
            <div>&#160;</div>
        </div>
    </body>
</html>
