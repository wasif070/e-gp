<%-- 
    Document   : TER4
    Created on : Apr 14, 2011, 10:31:01 AM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    String tenderid = request.getParameter("tenderid");
                    String lotId = request.getParameter("lotId");
                    String stat = request.getParameter("stat");//stat value is 'eval'
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    String pNature = commonService.getProcNature(request.getParameter("tenderid")).toString();
                    String eventType = commonService.getEventType(tenderid).toString();
                    String tenderPaidORFree = commonService.tenderPaidORFree(tenderid, lotId);

                    String repLabel = "Tender";
                    if(pNature.equalsIgnoreCase("Services")){
                        repLabel = "Proposal";
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=repLabel%> Evaluation Report 4</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            function signCheck(){
                jAlert("TER3 is not signed by all the Evaluation Committee members yet.","Member Signing", function(RetVal) {
                });
                return false;
            }
        </script>
    </head>
    <body>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>        
        <div class="tabPanelArea_1">
            <div class="pageHead_1"><%=repLabel%> Evaluation Report 4 - Final Evaluation Report<span style="float: right;">
                    <%
                        CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        boolean isCommCP = false;                        
                        List<SPCommonSearchDataMore> commCP = dataMoreService.getCommonSearchData("evaluationMemberCheck", tenderid, session.getAttribute("userId").toString(), "1", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);                        
                        if (commCP != null && (!commCP.isEmpty()) && (!commCP.get(0).getFieldName1().equals("0"))) {
                            isCommCP = true;
                        }                        
                        commCP=null;
                        if(isCommCP){
                    %>
                    <a class="action-button-goback" href="../officer/Evalclarify.jsp?tenderId=<%=tenderid%>&st=rp&comType=TEC">Go Back to Dashboard</a>
                    <%}else{%>
                    <a class="action-button-goback" href="../officer/MemberTERReport.jsp?tenderId=<%=tenderid%>&comType=TEC">Go Back to Dashboard</a>
                    <%}%>
                </span></div>
            <%
                    pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                    pageContext.setAttribute("tab", "6");
                    ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");

                    List<SPCommonSearchDataMore> tendOpeningRpt = creationService.getOpeningReportData("getTenderOpeningRpt", tenderid, lotId,null);//lotid

                    List<SPCommonSearchDataMore> TECMembers = creationService.getOpeningReportData("getTECMembers", tenderid, lotId,"TER4"); //lotid ,"TOR1"

                    List<SPCommonSearchDataMore> TendererAwarded = creationService.getOpeningReportData("getQualifiedBidders", tenderid, lotId,null); //lotid ,"TOR1"

                    List<SPCommonSearchDataMore> ter3Signed = creationService.getOpeningReportData("isTERSignedByAll", tenderid, lotId,"3");

                    boolean isTER3Signed = false;
                    if(ter3Signed!=null && (!ter3Signed.isEmpty()) && ter3Signed.get(0).getFieldName1().equals("1")){
                        isTER3Signed = true;
                    }

                    List<Object[]> finalSubmissionUser = creationService.getFinalSubmissionUser(tenderid);

                    List<Object[]> ter1Criteria = creationService.getTERCriteria("TER1", pNature);
                    List<SPCommonSearchDataMore> NoteofDissent = creationService.getOpeningReportData("getNoteofDissent", tenderid, lotId,"ter4");
                    List<SPCommonSearchDataMore> pMethodEstCost = dataMoreService.getCommonSearchData("GetPMethodEstCostTender", tenderid);

                    String estCost = pMethodEstCost.get(0).getFieldName2();
                    String estCostStatus = "-";
                    if (estCost != null) {
                        estCostStatus = (Double.parseDouble(estCost) > 0 ? "Approved" : "-");
                    }

                    boolean list1 = true;
                    boolean list5 = true;
                    if (tendOpeningRpt.isEmpty()) {
                        list1 = false;
                    }
                    if (TECMembers.isEmpty()) {
                        list5 = false;
                    }
            %>

            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <!--%@include  file="officerTabPanel.jsp"%-->
            <div class="tableHead_1 t_space t-align-center">Final Evaluation Report</div>
            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 b_space" width="100%">
                <tr>
                    <td width="24%" class="ff">Ministry Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName1());
                                }%></td>
                    <td width="16%" class="ff">Division Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName2());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff">Organization/Agency Name :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName3());
                                }%></td>
                    <td class="ff">Procuring Entity :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName4());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff"><%=repLabel%> Package No. and Description :</td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName5());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff">Lot No. and Description : </td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName12()+" & "+tendOpeningRpt.get(0).getFieldName6());
                                }%></td>
                </tr>
            </table>
            <div class="inner-tableHead t_space">Procurement Data</div>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>

                        <th width="47%">Procurement Type</th>
                        <th width="53%">Procurement Method</th>
                    </tr>
                    <tr>
                        <td class="t-align-center" ><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName7());
                                }%></td>
                       <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName9());
                                }%></td>
                    </tr>
                </tbody></table>
            <div class="inner-tableHead t_space">Procurement Plan</div>
            <table class="tableList_3" cellspacing="0" width="100%">
                <tbody><tr>
                        <th width="17%">Approval<br>
                            Status</th>
                        <th width="31%">Budget Type</th>
                        <%if(!pNature.equalsIgnoreCase("Services")){%>
                        <th width="14%">Approval Status
                            of<br>
                            Official Estimates</th>
                        <%}%>
                    </tr>
                    <tr>
                        <td class="t-align-center">Approved</td>
                        <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName8());
                                }%></td>
                        <%if(!pNature.equalsIgnoreCase("Services")){%>
                       <td class="t-align-center"><%=estCostStatus%></td>
                       <%}%>
                    </tr>
                </tbody></table>
            <table class="tableList_3 t_space" cellspacing="0" width="100%">
                <tbody><tr>
                        <th class="table-fileName">Name of Bidder/Consultant
                            Recommended for Award</th>

                    </tr>
                    <%for(SPCommonSearchDataMore bidder : TendererAwarded){%>
                    <tr>
                        <td style="text-align: left;"><%=bidder.getFieldName1()%></td>
                    </tr>
                    <%}%>
                </tbody></table>
            <table class="tableList_3 t_space" cellspacing="0" width="100%">
                <tbody><tr>
<!--                        <th width="33%">Negotiation Report</th>-->

                        <th width="50%">Post Qualification Report</th>
                        <th width="50%">Clarification on Tenders/Proposals</th>
                    </tr>
                    <tr>
<!--                        <td><a href="#">View</a></td>-->
                        <td class="t-align-center"><a href="<%=request.getContextPath()%>/officer/ViewPQReport.jsp?tenderId=<%=tenderid%>&pkgLotId=<%=lotId%>" target="_blank">View</a></td>
                        <td class="t-align-center"><a href="<%=request.getContextPath()%>/officer/AllQeryResp.jsp?tenderid=<%=tenderid%>">View</a></td>

                    </tr>
                </tbody></table>
                        <%if(NoteofDissent!=null && (!NoteofDissent.isEmpty())){%>
            <div class="inner-tableHead t_space">Note of Dissent:</div>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <th>TEC Member</th>
                    <th>Note of Dissent</th>
                </tr>
                <%for(SPCommonSearchDataMore nod : NoteofDissent){%>
                    <td><%=nod.getFieldName1()%></td>
                    <td><%=nod.getFieldName2()%></td>
                <%}%>
            </table>
            <%}%>
            <div class="inner-tableHead t_space">TEC Members</div>
            <table width="100%" cellspacing="0" class="tableList_1">
                <tr>
                    <td class="tableHead_1" colspan="5" >TEC Members</td>
                </tr>
                <tr>
                    <th width="20%" >Name</th>
                    <%
                    for (SPCommonSearchDataMore dataMore : TECMembers) {
                        out.print("<td>");
                        if(dataMore.getFieldName5().equals(session.getAttribute("userId").toString())){
                         if(dataMore.getFieldName6().equals("-")){
                             String valChk="";
                             if(!isCommCP){
                                valChk = "&nDis=y";
                             }
                   %>
                    <a href="../officer/TORSigningApp.jsp?id=<%=dataMore.getFieldName7()%>&uid=<%=dataMore.getFieldName5()%>&tid=<%=tenderid%>&lotId=<%=lotId%>&stat=<%=stat%>&rpt=ter4<%=valChk%>" <%if(!isTER3Signed){out.print(" onclick='return signCheck();'");}%>>
                            <%=dataMore.getFieldName1()%>
                        </a>
                   <%
                        }else{
                             out.print(dataMore.getFieldName1());
                        }

                    }else{
                        out.print(dataMore.getFieldName1());
                    }
                        out.print("</td>");
                    }
                  %>
               </tr>
                <tr>
                    <th width="20%">Committee Role</th>
                    <%for (SPCommonSearchDataMore dataMore : TECMembers) {
                        out.print("<td>");
                            if (dataMore.getFieldName2().equals("cp")) {
                            out.print("Chairperson");
                        } else if (dataMore.getFieldName2().equals("ms")) {
                            out.print("Member Secretary");
                        } else if (dataMore.getFieldName2().equals("m")) {
                            out.print("Member");
                            }
                        out.print("</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Designation</th>
                    <%for (SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName3()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">PE Office</th>
                    <%for (SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName4()+"</td>");}%>
                </tr>
                <tr>
                    <th width="20%">Signed <%=repLabel%> Evaluation Report 4 On</th>
                    <%for(SPCommonSearchDataMore dataMore : TECMembers) {out.print("<td>"+dataMore.getFieldName6()+"</td>");}%>
                </tr>
            </table>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <%
    %>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
