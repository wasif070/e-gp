<%-- 
    Document   : AnnualProcPro
    Created on : Jun 29, 2012, 4:16:05 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.model.table.TblStateMaster"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.AppMISService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//Dth HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dth">

<html>
    <head>
        <%
            boolean isNotPDF = true;
            if (request.getParameter("isPDF") != null && "true".equalsIgnoreCase(request.getParameter("isPDF"))) {
                isNotPDF = false;
            }
            int uTypeId = 0;
            String suserId = null;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Annual Procurement Progress</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />        
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
        <script type="text/javascript">
            function onSelectionOfTreeOff() {
                $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId: $('#txtdepartmentid').val(), funName: 'Office'}, function (j) {
                    $("select#cmboffice").html(j);
                    var cmb = document.getElementById('cmboffice');
                    $("#offName").val(cmb.options[cmb.selectedIndex].text);
                });
            }
            function setOffName() {
                var cmb = document.getElementById('cmboffice');
                $("#offName").val(cmb.options[cmb.selectedIndex].text);
            }
            function setProcMethodName() {
                var cmb = document.getElementById('cmbProcMethod');
                $("#procMethodName").val(cmb.options[cmb.selectedIndex].text);
            }
            function setBudgetType() {
                var cmb = document.getElementById('cmbBudgetType');
                $("#budgetTypeName").val(cmb.options[cmb.selectedIndex].text);
            }
            function getOfficesForDept() {
                $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId: $('#txtdepartmentid').val(), funName: 'Office'}, function (j) {
                    $("select#cmboffice").html(j);
                });
            }
        </script>
    </head>
    <body onload="hide();">
        <%if (isNotPDF) {
                suserId = session.getAttribute("userId").toString();%>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%uTypeId = Integer.parseInt(objUserTypeId.toString());
            } else {
                uTypeId = Integer.parseInt(request.getParameter("uTypeId"));
                suserId = request.getParameter("suserId");
            }
            AppMISService mISService = (AppMISService) AppContext.getSpringBean("AppMISService");
            CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            List<SPCommonSearchDataMore> appListDtBean = dataMoreService.geteGPDataMore("FinancialYear");
            String departmentId = request.getParameter("depId");
            String officeId = (request.getParameter("offId") == null || request.getParameter("offId").equals("0") ? "" : request.getParameter("offId"));
            String procurementType = request.getParameter("cmbProcNature");
            String budgetId = request.getParameter("budgetType");
//                    System.out.println("budget id :"+budgetId);
            String district = request.getParameter("cmbDistrict");
            String cpvCategory = request.getParameter("cpvCat");
            String procurementMethodId = request.getParameter("procMethod");
            String financialYearId = request.getParameter("financialyear");
            String procMethodName = request.getParameter("procMethodName");
            String budgetName = request.getParameter("budgetTypeName");
            String deptName = request.getParameter("depName");
        %>
        <jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>
        <div class="contentArea_1">
            <%if (isNotPDF) {%>
            <!--<div class="tabPanelArea_1">-->
                <div class="pageHead_1">Annual Procurement Progress<span style="float: right;"></span></div>
                <div class="formBg_1">
                    <form method="post" action="AnnualProcPro.jsp">
                        <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                        <table id="tblSearchBox" width="100%" cellspacing="8" cellpadding="0"  class="formStyle_1">
                            <tr>
                                <td width="15%" class="ff" align="left">By Financial Year :<span class="mandatory">*</span></td>
                                <td width="35%" align="left">
                                    <select name="financialyear" class="formTxtBox_1" id="cmbFinancialYear" style="width:200px;" onchange="changeQuater();">
                                        <option value="">Please Select Financial Year</option>

                                        <%
                                            if (appListDtBean != null && !appListDtBean.isEmpty()) {
                                                for (SPCommonSearchDataMore commonApp : appListDtBean) {
                                                    out.print("<option  value='" + commonApp.getFieldName2() + "'>" + commonApp.getFieldName2() + "</option>");
                                                }
                                            }
                                        %>
                                    </select>
                                </td>
                                <td colspan="2">&nbsp;</td>
                            </tr>

                            <tr>
                                <td class="ff t-align-left">Hierarchy Node : <!--%if (uTypeId == 1) {%><span class="mandatory">*</span>< %}%--></td>
                                <td class="t-align-left">
                                    <%if (uTypeId == 5) {
                                            Object[] objData = mISService.getOrgAdminOrgName(Integer.parseInt(suserId));
                                            out.print(objData[1].toString());
                                            out.print("<input type=\"hidden\" id=\"txtdepartment\" name=\"depName\" value=\"" + objData[1].toString() + "\"/>");
                                            out.print("<input type=\"hidden\" id=\"txtdepartmentid\" name=\"depId\" value=\"" + objData[0] + "\"/>");
                                        } else {%>
                                    <input type="text" id="txtdepartment" style="width: 200px;" class="formTxtBox_1" readonly name="depName"/>
                                    <input type="hidden" id="cmborg">
                                    <!--To add CCGP and Cabinet Division add this in below as queryString ?operation=govuser-->
                                    <a id="imgTree" href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=PackageReport', '', 'width=350px,height=400px,scrollbars=1', '');">
                                        <img alt="Department" style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                    </a>
                                    <input type="hidden" id="txtdepartmentid" name="depId">
                                    <input type="hidden" id="txtDepartmentType">
                                    <%}%>
                                </td>
                                <td class="ff t-align-left">PA Office :</td>
                                <td class="t-align-left">
                                    <%if (uTypeId == 5 || uTypeId == 1) {%>
                                    <select id="cmboffice" style="width: 200px;" class="formTxtBox_1" name="offId" onchange="setOffName()">
                                        <%

                                            if (uTypeId == 1) {
                                        %>
                                        <option value=""> No Office Found.</option>
                                        <%  }
                                            List<Object[]> cmbLists = null;
                                            if (uTypeId == 5) {
                                                cmbLists = mISService.getOfficeList(suserId, "" + uTypeId);
                                                for (Object[] cmbList : cmbLists) {
                                                    String selected = "";
                                                    if (request.getParameter("offId") != null) {
                                                        if (cmbList[0].toString().equals(request.getParameter("offId"))) {
                                                            selected = "selected";
                                                        }
                                                    }
                                                    out.print("<option value='" + cmbList[0] + "' " + selected + ">" + cmbList[1] + "</option>");
                                                    selected = null;
                                                }
                                                if (cmbLists.isEmpty()) {
                                                    out.print("<option value='0'> No Office Found.</option>");
                                                }
                                            }
                                        %>
                                    </select>
                                    <input type="hidden" id="offName" value="<%=(uTypeId == 5) ? (request.getParameter("offName") != null && !"".equals(request.getParameter("offName"))) ? request.getParameter("offName") : !cmbLists.isEmpty() ? cmbLists.get(0)[1].toString() : "" : ""%>" name="offName" />
                                    <%
                                        }
                                    %>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Procurement Category :</td>
                                <td><select name="cmbProcNature" class="formTxtBox_1" id="cmbProcNature" style="width:200px;">
                                        <option value="" selected="selected">--Select Procurement  Category--</option>
                                        <option value="Goods">Goods</option>
                                        <option value="Works">Works</option>
                                        <option value="services">Services</option>
                                    </select></td>
                                <td class="ff">Procurement Method :</td>
                                <td><select name="procMethod" class="formTxtBox_1" id="cmbProcMethod" onchange="setProcMethodName()" style="width:208px;">
                                        <option value="" selected="selected">- Select Procurement Method -</option>
                                        <c:forEach var="procMethod" items="${advAppSearchSrBean.procMethodList}">
                                            <option value="${procMethod.objectId}">${procMethod.objectValue}</option>
                                        </c:forEach>
                                    </select>
                                    <input type="hidden" id="procMethodName" value="<%= (request.getParameter("procMethod") != null && !"".equals(request.getParameter("procMethod"))) ? request.getParameter("procMethod") : ""%>" name="procMethodName" />
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Source of Fund :</td>
                                <td>
                                    <select name="budgetType" class="formTxtBox_1" id="cmbBudgetType" onchange="setBudgetType()" style="width:200px;" >
                                        <option value="" >All</option>
                                        <option value="1">Development</option>
                                        <option value="2">Revenue</option>
                                        <option value="3">Own Fund</option>
                                        <option value="1,2">Development and Revenue</option>
                                        <option value="2,3">Revenue and Own Fund</option>
                                        <option value="1,3">Development and Own Fund</option>
                                    </select>
                                    <input type="hidden" id="budgetTypeName" value="<%= (request.getParameter("budgetType") != null && !"".equals(request.getParameter("budgetType"))) ? request.getParameter("budgetType") : ""%>" name="budgetTypeName" />
                                    <br />
                                    <span style="color: red;" id="msgBudgetType"></span>
                                </td>
                                <td class="ff">Category :</td>
                                <td>
                                    <input name="cpvCat" id="txtaCPV" type="text" class="formTxtBox_1" style="width:202px;" />
                                    <a href="javascript:void(0);" class="action-button-select" onclick="loadCPVTree()">Select Category</a>
                                </td>
                            </tr>
                            <!--                            <tr>
                                                            <td width="20%" class="ff">Dzongkhag / District : </td>
                            <%
                                CommonService cservice = (CommonService) AppContext.getSpringBean("CommonService");
                                //Code by Proshanto
                                short countryId = 150;//136
                                List<TblStateMaster> liststate = cservice.getState(countryId);
                            %>
                            <td width="30%">
                                <select name="cmbDistrict" class="formTxtBox_1" id="cmbDistrict" style="width:95%;">
                                    <option value="">-- Select --</option>
                            <%
                                for (TblStateMaster state : liststate) {
                                    out.println("<option value='" + state.getStateName() + "'>" + state.getStateName() + "</option>");
                                }
                            %>
                        </select>
                        
                    </td>
                </tr>-->
                            <span id="msgdistrict" class="reqF_1"></span>
                            <input type="hidden" value="" name="cmbDistrict" class="formTxtBox_1" id="cmbDistrict" style="width:95%;">


                            <tr>
                                <td>&nbsp;</td>
                                <td class="ff">
                                    <label class="formBtn_1"><input type="submit" value="Generate" id="generate"  onclick="return validate();" name="generate"></label>
                                    <label class="anchorLink l_space">
                                        <!--                                            <input type="reset" value="Clear" id="clear" name="clear" onclick="window.location.reload()">-->
                                        <a href="AnnualProcPro.jsp" style="text-decoration: none; color: #fff; font-size: 12px;">Reset</a>
                                    </label>
                                </td>
                                <td class="t-align-right" colspan="2">
                                    <%if (isNotPDF) {%>
                                    <span style="float: right;">
                                        <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                                        &nbsp;&nbsp;
                                        <%
                                            StringBuffer hrefQString = new StringBuffer();
                                            hrefQString.append("&depId=");
                                            hrefQString.append(departmentId);
                                            hrefQString.append("&offId=");
                                            hrefQString.append(officeId);
                                            hrefQString.append("&cmbProcNature=");
                                            hrefQString.append(procurementType);
                                            hrefQString.append("&budgetType=");
                                            hrefQString.append(budgetId);
                                            hrefQString.append("&budgetTypeNamePdf=");
                                            hrefQString.append(budgetName);
                                            hrefQString.append("&cmbDistrict=");
                                            hrefQString.append(district);
                                            hrefQString.append("&cpvCat=");
                                            hrefQString.append(cpvCategory);
                                            hrefQString.append("&procMethod=");
                                            hrefQString.append(procurementMethodId);
                                            hrefQString.append("&procMethodNamePdf=");
                                            hrefQString.append(procMethodName);
                                            hrefQString.append("&financialyear=");
                                            hrefQString.append(financialYearId);
                                            hrefQString.append("&depName=");
                                            hrefQString.append(deptName);
                                        %>
                                        <a class="action-button-savepdf" id="saveASPDF" href="<%=request.getContextPath()%>/TorRptServlet?uTypeId=<%=uTypeId%>&action=annualProcRpt&generate=Generate<%=hrefQString.toString()%>">Save As PDF</a>
                                    </span>
                                    <%}%>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <%}%>
                <%
                    List<SPCommonSearchDataMore> spAnnualProc1 = null;
                    List<SPCommonSearchDataMore> spAnnualProc2 = null;
                    List<SPCommonSearchDataMore> spAnnualProc3 = null;
                    if ("Generate".equals(request.getParameter("generate"))) {
                        //    System.out.println("tenderWisePartRpt" + "," +financialYearId+ ","+ departmentId + "," + officeId + "," + procurementType + "," + budgetId + "," + district + "," + cpvCategory + "," + procurementMethodId);
                        if (budgetId.equals("1")) {
                            spAnnualProc1 = dataMoreService.geteGPDataMore("getAPPProgress", financialYearId, departmentId, officeId, procurementType, district, cpvCategory, procurementMethodId, budgetId);
                        } else if (budgetId.equals("2")) {
                            spAnnualProc2 = dataMoreService.geteGPDataMore("getAPPProgress", financialYearId, departmentId, officeId, procurementType, district, cpvCategory, procurementMethodId, budgetId);
                        } else if (budgetId.equals("3")) {
                            spAnnualProc3 = dataMoreService.geteGPDataMore("getAPPProgress", financialYearId, departmentId, officeId, procurementType, district, cpvCategory, procurementMethodId, budgetId);
                        } else if (budgetId.equals("1,2")) {
                            spAnnualProc1 = dataMoreService.geteGPDataMore("getAPPProgress", financialYearId, departmentId, officeId, procurementType, district, cpvCategory, procurementMethodId, "1");
                            spAnnualProc2 = dataMoreService.geteGPDataMore("getAPPProgress", financialYearId, departmentId, officeId, procurementType, district, cpvCategory, procurementMethodId, "2");
                        } else if (budgetId.equals("2,3")) {
                            spAnnualProc2 = dataMoreService.geteGPDataMore("getAPPProgress", financialYearId, departmentId, officeId, procurementType, district, cpvCategory, procurementMethodId, "2");
                            spAnnualProc3 = dataMoreService.geteGPDataMore("getAPPProgress", financialYearId, departmentId, officeId, procurementType, district, cpvCategory, procurementMethodId, "3");
                        } else if (budgetId.equals("1,3")) {
                            spAnnualProc1 = dataMoreService.geteGPDataMore("getAPPProgress", financialYearId, departmentId, officeId, procurementType, district, cpvCategory, procurementMethodId, "1");
                            spAnnualProc3 = dataMoreService.geteGPDataMore("getAPPProgress", financialYearId, departmentId, officeId, procurementType, district, cpvCategory, procurementMethodId, "3");
                        } else if (budgetId.equals("")) {
                            spAnnualProc1 = dataMoreService.geteGPDataMore("getAPPProgress", financialYearId, departmentId, officeId, procurementType, district, cpvCategory, procurementMethodId, "1");
                            spAnnualProc2 = dataMoreService.geteGPDataMore("getAPPProgress", financialYearId, departmentId, officeId, procurementType, district, cpvCategory, procurementMethodId, "2");
                            spAnnualProc3 = dataMoreService.geteGPDataMore("getAPPProgress", financialYearId, departmentId, officeId, procurementType, district, cpvCategory, procurementMethodId, "3");
                        }
                        //      System.out.println("spAnnualProc1 size :" + spAnnualProc1.size());
                        //     System.out.println("spAnnualProc2 size :" + spAnnualProc2.size());
                        //     System.out.println("spAnnualProc3 size :" + spAnnualProc3.size());

                %>
                <div  id="print_area">
                    <div class="formStyle_1 t_space ff" id="searchCriteria">
                        <h3>
                            Search Criteria : By Financial Year : <%=financialYearId%>
                            <%
                                if (departmentId != null && !departmentId.equals("")) {
                                    out.print(", Hierarchy Node : " + deptName);
                                }
                                if (officeId != null && !officeId.equals("")) {
                                    out.print(", PA Office : " + request.getParameter("offName"));
                                }
                                if (procurementType != null && !procurementType.equals("")) {
                                    out.print(", Type of Procurement : " + procurementType);
                                }
                                if (procurementMethodId != null && !procurementMethodId.equals("")) {
                                    if (!isNotPDF) {
                                        out.print(", Procurement Method : " + request.getParameter("procMethodNamePdf"));
                                    } else {
                                        out.print(", Procurement Method : " + request.getParameter("procMethodName"));
                                    }
                                }
                                if (budgetId != null && !budgetId.equals("")) {
                                    if (!isNotPDF) {
                                        out.print(", Source of Fund : " + request.getParameter("budgetTypeNamePdf"));
                                    } else {
                                        out.print(", Source of Fund : " + request.getParameter("budgetTypeName"));
                                    }
                                }
                                if (district != null && !district.equals("")) {
                                    out.print(", Dzongkhag / District : " + request.getParameter("cmbDistrict"));
                                }
                                if (cpvCategory != null && !cpvCategory.equals("")) {
                                    out.print(", Category : " + request.getParameter("cpvCat"));
                                }
                            %>
                        </h3>
                    </div>
                    <% if (budgetId != null && (budgetId.equals("1") || budgetId.equals("1,2") || budgetId.equals("1,3") || budgetId.equals(""))) {%>
                    <div class="tableHead_1 t_space" id="getSubHeading">
                        Procurement Progress of Capital Budget for Financial year July, <%=financialYearId.split("-")[0]%> - June, <%=financialYearId.split("-")[1]%>
                    </div>

                    <table border="1" cellspacing="0" cellpadding="0" class="tableList_1" width="100%" id="resultTable">
                        <tbody>
                            <tr>
                                <th rowspan="2" >Sl. No.</th>
                                <th rowspan="2" >Hierarchy Node</th>
                                <!--                                    <th rowspan="2" >No. of Development Projects</th>
                                                                    <th rowspan="2" >No. of Projects from development Budget

                                                                    </th>-->
                                <th rowspan="2" >Total Estimated Cost for all procurement Packages</th>
                                <th colspan="4" >Approved Annual Procurement Plan Packages</th>
                                <th colspan="4" >Awarded Contracts</th>
                                <th rowspan="2" >% of awarded contract out of APP Packages

                                    (11/7*100)</th>
                                <th rowspan="2" >Cost of Awarded Contracts</th>
                                <th rowspan="2" >Actual Expenditure</th>
                                <th rowspan="2" >% of expenditure out of Estimated Cost

                                    (14/3*100)</th>
                            </tr>

                            <tr>
                                <th valign="top" >Goods</th>
                                <th valign="top" >Works</th>
                                <th valign="top" >Services</th>
                                <th valign="top" >Total Packages

                                    (4+5+6)</th>
                                <th valign="top" >Goods</th>
                                <th valign="top" >Works </th>
                                <th valign="top" >Services</th>
                                <th valign="top" >Total Awarded Contracts

                                    (8+9+10)</th>
                            </tr>

                            <tr>
                                <th valign="top" >1</th>
                                <th valign="top" >2</th>
                                <th valign="top" >3</th>
                                <th valign="top" >4</th>
                                <th valign="top" >5</th>
                                <th valign="top" >6</th>
                                <th valign="top" >7</th>
                                <th valign="top" >8</th>
                                <th valign="top" >9</th>
                                <th valign="top" >10</th>
                                <th valign="top" >11</th>
                                <th valign="top" >12</th>
                                <th valign="top" >13</th>
                                <th valign="top" >14</th>
                                <th valign="top" >15</th>
                            </tr>
                            <%

                                if (spAnnualProc1 != null && spAnnualProc1.size() > 0) {
                                    //   System.out.println("sp devbudget size :" + spAnnualProc1.size());
                                    int count = 1;
                                    for (SPCommonSearchDataMore spdpbudget : spAnnualProc1) {
                            %>
                            <tr>
                                <td style="text-align: left;" id="t1col1_<%=count%>"><%=count%></td>
                                <td style="text-align: left;" id="t1col2_<%=count%>"><%=spdpbudget.getFieldName1()%></td>
                                <td style="text-align: left;" id="t1col3_<%=count%>"><%=spdpbudget.getFieldName2()%></td>
                                <td style="text-align: left;" id="t1col4_<%=count%>"><%=spdpbudget.getFieldName3()%></td>
                                <td style="text-align: left;" id="t1col5_<%=count%>"><%=spdpbudget.getFieldName4()%></td>
                                <td style="text-align: left;" id="t1col6_<%=count%>"><%=spdpbudget.getFieldName5()%></td>
                                <td style="text-align: left;" id="t1col7_<%=count%>"><%=spdpbudget.getFieldName6()%></td>
                                <td style="text-align: left;" id="t1col8_<%=count%>"><%=spdpbudget.getFieldName7()%></td>
                                <td style="text-align: left;" id="t1col9_<%=count%>"><%=spdpbudget.getFieldName8()%></td>
                                <td style="text-align: left;" id="t1col10_<%=count%>"><%=spdpbudget.getFieldName9()%></td>
                                <td style="text-align: left;" id="t1col11_<%=count%>"><%=spdpbudget.getFieldName10()%></td>
                                <td style="text-align: left;" id="t1col12_<%=count%>"><%=spdpbudget.getFieldName11()%></td>
                                <td style="text-align: left;" id="t1col13_<%=count%>"><%=spdpbudget.getFieldName12()%></td>
                                <td style="text-align: left;" id="t1col14_<%=count%>"><%=spdpbudget.getFieldName13()%></td>
                                <td style="text-align: left;" id="t1col15_<%=count%>"><%=spdpbudget.getFieldName14()%></td>
                            </tr>

                            <%
                                        count++;
                                    }
                                }
                            %>
                        </tbody>
                    </table>
                    <%}%>
                    <% if (budgetId != null && (budgetId.equals("2") || budgetId.equals("1,2") || budgetId.equals("2,3") || budgetId.equals("") || budgetId.equals(null))) {%>
                    <div class="tableHead_1 t_space" id="getSubHeading">
                        Procurement Progress of Recurrent Budget for Financial year July, <%=financialYearId.split("-")[0]%> - June, <%=financialYearId.split("-")[1]%>
                    </div>

                    <table border="1" cellspacing="0" cellpadding="0" class="tableList_1" width="100%" id="resultTable">
                        <tbody>
                            <tr>
                                <th rowspan="2">Sl. No.</th>
                                <th rowspan="2">Hierarchy Node</th>
                                <!--                                            <th rowspan="2">No. of Packages from Own Fund</th>-->
                                <th rowspan="2">Total Estimated Cost for all procurement Packages</th>
                                <th colspan="4">Approved Annual Procurement Plan Packages</th>
                                <th colspan="4">Awarded Contracts</th>
                                <th rowspan="2">% of awarded contract out of APP Packages

                                    (11/7*100)</th>
                                <th rowspan="2">Cost of Awarded Contracts</th>
                                <th rowspan="2">Actual Expenditure</th>
                                <th rowspan="2">% of expenditure out of Estimated Cost

                                    (14/3*100)</th>
                            </tr>
                            <tr>
                                <th valign="top">Goods</th>
                                <th valign="top">Works</th>
                                <th valign="top">Services</th>
                                <th valign="top">Total Packages

                                    (4+5+6)</th>
                                <th valign="top">Goods</th>
                                <th valign="top">Works </th>
                                <th valign="top">Services</th>
                                <th valign="top">Total Awarded Contracts

                                    (8+9+10)</th>
                            </tr>
                            <tr>
                                <th valign="top">1</th>
                                <th valign="top">2</th>
                                <th valign="top">3</th>
                                <th valign="top">4</th>
                                <th valign="top">5</th>
                                <th valign="top">6</th>
                                <th valign="top">7</th>
                                <th valign="top">8</th>
                                <th valign="top">9</th>
                                <th valign="top">10</th>
                                <th valign="top">11</th>
                                <th valign="top">12</th>
                                <th valign="top">13</th>
                                <th valign="top">14</th>
                                <th valign="top">15</th>
                            </tr>
                            <%

                                if (spAnnualProc2 != null && spAnnualProc2.size() > 0) {
                                    //       System.out.println("sp revenew size :" + spAnnualProc1.size());
                                    int count = 1;
                                    for (SPCommonSearchDataMore sprnwbudget : spAnnualProc2) {
                            %>
                            <tr>
                                <td style="text-align: left;" id="t2col1_<%=count%>"><%=count%></td>
                                <td style="text-align: left;" id="t2col2_<%=count%>"><%=sprnwbudget.getFieldName1()%></td>
                                <td style="text-align: left;" id="t2col3_<%=count%>"><%=sprnwbudget.getFieldName2()%></td>
                                <td style="text-align: left;" id="t2col4_<%=count%>"><%=sprnwbudget.getFieldName3()%></td>
                                <td style="text-align: left;" id="t2col5_<%=count%>"><%=sprnwbudget.getFieldName4()%></td>
                                <td style="text-align: left;" id="t2col6_<%=count%>"><%=sprnwbudget.getFieldName5()%></td>
                                <td style="text-align: left;" id="t2col7_<%=count%>"><%=sprnwbudget.getFieldName6()%></td>
                                <td style="text-align: left;" id="t2col8_<%=count%>"><%=sprnwbudget.getFieldName7()%></td>
                                <td style="text-align: left;" id="t2col9_<%=count%>"><%=sprnwbudget.getFieldName8()%></td>
                                <td style="text-align: left;" id="t2col10_<%=count%>"><%=sprnwbudget.getFieldName9()%></td>
                                <td style="text-align: left;" id="t2col11_<%=count%>"><%=sprnwbudget.getFieldName10()%></td>
                                <td style="text-align: left;" id="t2col12_<%=count%>"><%=sprnwbudget.getFieldName11()%></td>
                                <td style="text-align: left;" id="t2col13_<%=count%>"><%=sprnwbudget.getFieldName12()%></td>
                                <td style="text-align: left;" id="t2col14_<%=count%>"><%=sprnwbudget.getFieldName13()%></td>
                                <td style="text-align: left;" id="t2col15_<%=count%>"><%=sprnwbudget.getFieldName14()%></td>
                            </tr>

                            <%
                                        count++;
                                    }
                                }
                            %>
                        </tbody>
                    </table>
                    <%}%>
                    <% if (budgetId != null && (budgetId.equals("3") || budgetId.equals("1,3") || budgetId.equals("2,3") || budgetId.equals("") || budgetId.equals(null))) {%>
                    <div class="tableHead_1 t_space" id="getSubHeading">
                        Procurement Progress of Own Fund Budget for Financial year July, <%=financialYearId.split("-")[0]%> - June, <%=financialYearId.split("-")[1]%>
                    </div>

                    <table border="1" cellspacing="0" cellpadding="0" class="tableList_1" width="100%" id="resultTable">
                        <tbody>
                            <tr>
                                <th rowspan="2">Sl. No.</th>
                                <th rowspan="2">Hierarchy Node</th>
                                <!--                                                    <th rowspan="2">No. of Packages from Own Fund</th>-->
                                <th rowspan="2">Total Estimated Cost for all procurement Packages</th>
                                <th colspan="4">Approved Annual Procurement Plan Packages</th>
                                <th colspan="4">Awarded Contracts</th>
                                <th rowspan="2">% of awarded contract out of APP Packages

                                    (11/7*100)</th>
                                <th rowspan="2">Cost of Awarded Contracts</th>
                                <th rowspan="2">Actual Expenditure</th>
                                <th rowspan="2">% of expenditure out of Estimated Cost

                                    (14/3*100)</th>
                            </tr>
                            <tr>
                                <th valign="top">Goods</th>
                                <th valign="top">Works</th>
                                <th valign="top">Services</th>
                                <th valign="top">Total Packages

                                    (4+5+6)</th>
                                <th valign="top">Goods</th>
                                <th valign="top">Works </th>
                                <th valign="top">Services</th>
                                <th valign="top">Total Awarded Contracts

                                    (8+9+10)</th>
                            </tr>
                            <tr>
                                <th valign="top">1</th>
                                <th valign="top">2</th>
                                <th valign="top">3</th>
                                <th valign="top">4</th>
                                <th valign="top">5</th>
                                <th valign="top">6</th>
                                <th valign="top">7</th>
                                <th valign="top">8</th>
                                <th valign="top">9</th>
                                <th valign="top">10</th>
                                <th valign="top">11</th>
                                <th valign="top">12</th>
                                <th valign="top">13</th>
                                <th valign="top">14</th>
                                <th valign="top">15</th>
                            </tr>
                            <%

                                if (spAnnualProc3 != null && spAnnualProc3.size() > 0) {
                                    //    System.out.println("sp ownfund size :" + spAnnualProc3.size());
                                    int count = 1;
                                    for (SPCommonSearchDataMore spowfbudget : spAnnualProc3) {
                            %>
                            <tr>
                                <td style="text-align: left;" id="t3col1_<%=count%>"><%=count%></td>
                                <td style="text-align: left;" id="t3col2_<%=count%>"><%=spowfbudget.getFieldName1()%></td>
                                <td style="text-align: left;" id="t3col3_<%=count%>"><%=spowfbudget.getFieldName2()%></td>
                                <td style="text-align: left;" id="t3col4_<%=count%>"><%=spowfbudget.getFieldName3()%></td>
                                <td style="text-align: left;" id="t3col5_<%=count%>"><%=spowfbudget.getFieldName4()%></td>
                                <td style="text-align: left;" id="t3col6_<%=count%>"><%=spowfbudget.getFieldName5()%></td>
                                <td style="text-align: left;" id="t3col7_<%=count%>"><%=spowfbudget.getFieldName6()%></td>
                                <td style="text-align: left;" id="t3col8_<%=count%>"><%=spowfbudget.getFieldName7()%></td>
                                <td style="text-align: left;" id="t3col9_<%=count%>"><%=spowfbudget.getFieldName8()%></td>
                                <td style="text-align: left;" id="t3col10_<%=count%>"><%=spowfbudget.getFieldName9()%></td>
                                <td style="text-align: left;" id="t3col11_<%=count%>"><%=spowfbudget.getFieldName10()%></td>
                                <td style="text-align: left;" id="t3col12_<%=count%>"><%=spowfbudget.getFieldName11()%></td>
                                <td style="text-align: left;" id="t3col13_<%=count%>"><%=spowfbudget.getFieldName12()%></td>
                                <td style="text-align: left;" id="t3col14_<%=count%>"><%=spowfbudget.getFieldName13()%></td>
                                <td style="text-align: left;" id="t3col15_<%=count%>"><%=spowfbudget.getFieldName14()%></td>
                            </tr>

                            <%
                                        count++;
                                    }
                                }
                            %>
                        </tbody>
                    </table>
                    <%}%>
                </div>
                <% } // end of generate button %>
<!--            </div>-->
            <%if (isNotPDF) {%>
            <div align="center">
                <%@include file="../resources/common/Bottom.jsp" %>
            </div>
            <%}%>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }
        function loadCPVTree()
        {
            window.open('../resources/common/CPVTree.jsp', 'CPVTree', 'menubar=0,scrollbars=1,width=700px');
        }
        $(document).ready(function () {

            $("#print").click(function () {
                //alert('sa');
                printElem({leaveOpen: true, printMode: 'popup'});
            });

        });
        function showHide()
	{
		if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
			document.getElementById('tblSearchBox').style.display = 'table';
			document.getElementById('collExp').innerHTML = '- Advanced Search';
		}else{
			document.getElementById('tblSearchBox').style.display = 'none';
			document.getElementById('collExp').innerHTML = '+ Advanced Search';
		}
	}
	function hide()
	{
		document.getElementById('tblSearchBox').style.display = 'none';
		document.getElementById('collExp').innerHTML = '+ Advanced Search';
	}
        function onSelectionOfTreeDesig() {}
//        function showHide() {}
        function checkCondition() {}
        function checkHOPE() {}
        function validate() {
            $(".err").remove();
            var cnt = 0;
            if ($('#cmbFinancialYear').val() == '') {
                $('#cmbFinancialYear').parent().append("<div class='err' style='color:red;'>Please select Financial Year</div>");
                cnt++;
            }
            return (cnt == 0);
        }
        function onSelectionOfTreeOff() {
            $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId: $('#txtdepartmentid').val(), funName: 'Office'}, function (j) {
                $("select#cmboffice").html(j);
                var cmb = document.getElementById('cmboffice');
                $("#offName").val(cmb.options[cmb.selectedIndex].text);
            });
        }

        function printElem(options) {
            //alert(options);
            /*$('#print').hide();
             $('#saveASPDF').hide();
             $('#goBack').hide();*/
            $('#print_area').printElement(options);
            /*$('#print').show();
             $('#saveASPDF').show();
             $('#goBack').show();*/
            //$('#trLast').hide();
        }
    </script>
</body>
</html>
