<%-- 
    Document   : TOR2
    Created on : Apr 9, 2011, 11:58:05 AM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cptu.egp.eps.model.table.TblReportColumnMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<jsp:useBean id="tenderSrBean" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    String repLabel=null;
                    String eventType = commonService.getEventType(request.getParameter("tenderid")).toString();
                    if(commonService.getProcNature(request.getParameter("tenderid")).toString().equals("Services")){
                        repLabel = "Proposal";
                    }else{
                        repLabel = "Tender";
                    }
                    boolean bol_flag= true;
                    if(request.getParameter("isPDF")!=null && "true".equalsIgnoreCase(request.getParameter("isPDF"))){
                        bol_flag = false;
                    }
                    
        // Coad added by Dipal for Audit Trail Log.
        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        String idType="tenderId";
        int auditId=Integer.parseInt(request.getParameter("tenderid"));
        String auditAction="TOR2/POR2 viewed by Tenderer";
        String moduleName=EgpModule.Tender_Opening.getName();
        String remarks="";
        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%if(repLabel.equals("Tender")){%>Bid<%}else{%>Proposal<%}%> Opening Report</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
         <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        
        <!--
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script type="text/javascript">
            function checkPriceBid(tenderId,lotId,frm){
                var vbool=true;
                $.ajax({
                    type: "POST",
                    url: "<%=request.getContextPath()%>/TorRptServlet",
                    data:"tId="+tenderId+"&lId="+lotId+"&action=checkPriceBid",
                    async: false,
                    success: function(j){
                        if(j.toString()=="0"){
                            vbool=false;
                            jAlert("Please make sure Price Comparison Report is prepared as it cannot be prepared after signing the report.","<%if(repLabel.equals("Tender")){%>B<%}else{%>P<%}%>OR", function(RetVal) {
                               if(RetVal){
                                  $(frm).submit();
                                }
                            });
                        }else{
                            $(frm).submit();
                        }
                    }
                });                 
            }
        </script>
    </head>
    <body>
        <%if(bol_flag){%>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%}
                    String tenderid = request.getParameter("tenderid");
                    String lotId = request.getParameter("lotId");
                    ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");

                    List<SPCommonSearchDataMore> tendOpeningRpt = creationService.getOpeningReportData("getTenderOpeningRpt", tenderid, lotId,null);//lotid

                    List<SPCommonSearchDataMore> tendererList = creationService.getOpeningReportData("getTOR2BidderList", tenderid, lotId,null);//lotid
                    boolean isSecurityAvail = false;

                    List<SPCommonSearchDataMore> tendDocument = creationService.getOpeningReportData("getTenderDocument", tenderid, lotId,null);//lotid

                    List<SPCommonSearchDataMore> TOCMembers = creationService.getOpeningReportData("getTOCMembers", tenderid, lotId,"TOR2"); //lotid

                    boolean list1 = true;
                    boolean list2 = true;
                    boolean list4 = true;
                    boolean list5 = true;
                    if (tendOpeningRpt.isEmpty()) {
                        list1 = false;
                    }
                    if (tendererList.isEmpty()) {
                        list2 = false;
                    } else {
                        if (tendererList.get(0).getFieldName1().equals("1")) {
                            isSecurityAvail = true;
                        }
                    }

                    if (tendDocument.isEmpty()) {
                        list4 = false;
                    }
                    if (TOCMembers.isEmpty()) {
                        list5 = false;
                    }
                    boolean is2Env = false;
                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> envDataMores = commonSearchDataMoreService.geteGPData("GetTenderEnvCount", tenderid);
                    if(envDataMores!=null && (!envDataMores.isEmpty())){
                        if(envDataMores.get(0).getFieldName1().equals("2")){
                            is2Env = true;
                        }
                    }
        %>
        <div class="contentArea_1">
            <%if(bol_flag){%>
            <span style="float: right;" style="margin-top: 15px;">
                <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                <span id="svpdftor2">
                &nbsp;&nbsp;
                <a class="action-button-savepdf" href="<%=request.getContextPath()%>/TorRptServlet?tenderId=<%=tenderid%>&lotId=<%=lotId%>&action=TOR2">Save As PDF</a>
                </span>
                <%if(request.getParameter("goback")==null || request.getParameter("goback")==""){%>
                &nbsp;&nbsp;
                <a class="action-button-goback" href="<%if("eval".equals(request.getParameter("frm"))){out.print(request.getHeader("referer"));}else{out.print("../officer/OpenComm.jsp?tenderid="+tenderid);}%>">Go Back to Dashboard</a>
                <%}%>
            </span>
            <%}%>
            <%
                    //boolean tendDisp = false;
                    if("y".equals(request.getParameter("isT"))){
                        out.print("<span style='float: right;' style='margin-top: 15px;'><a href='javascript:void(0);' id='print' class='action-button-view'>Print</a>&nbsp;&nbsp;<a class='action-button-savepdf' href='"+request.getContextPath()+"/TorRptServlet?tenderId="+tenderid+"&lotId="+lotId+"&action=TOR2'>Save As PDF</a></span>");
                        //tendDisp = true;
                    }
            %>
            <div  id="print_area">
            <div class="pageHead_1">Bid Opening Report
            </div>
            <%
                        pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                        pageContext.setAttribute("tab", "6");
                        pageContext.setAttribute("isPDF", request.getParameter("isPDF"));
                        pageContext.setAttribute("userId", 0);
                        if(request.getParameter("msg")!=null){out.print("<br/><div class='responseMsg successMsg'>BOR Signed Successfully</div>");}
            %>
            
            <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <!--%@include  file="officerTabPanel.jsp"%-->
            <div class="tableHead_1 t_space"><%if(repLabel.equals("Tender")){%>Bid Opening Report<%}else{%>PROPOSAL OPENING<%}%></div>
            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 b_space" width="100%">
                <tr>
                    <td width="24%" class="ff">Hierarchy Node :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName1()+", ");
                                }%><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName2()+", ");
                                }%><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName3());
                                }%></td>
                     </tr>
                    <tr>
                    <td class="ff">Procuring Agency :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName4());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff"><%=repLabel%> Package No. and Description :</td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName5()+" & "+tendOpeningRpt.get(0).getFieldName6());
                                }%></td>
                </tr>
                <%if(!lotId.equals("0")){%>
                <tr>
                    <td class="ff">Lot No. and Description : </td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName12());
                                    if(!tendOpeningRpt.get(0).getFieldName12().equals("")){
                                        out.print(" & ");
                                    }
                                    out.print(tendOpeningRpt.get(0).getFieldName13());
                                }%></td>
                </tr>
                <%}%>
            </table>            
            

            
            <%
              if(!(eventType.equals("REOI") || eventType.equals("PQ") || eventType.equals("1 stage-TSTM")) && !is2Env){
                List<SPCommonSearchDataMore> getreportID = null;
                String repId = null;
                getreportID = commonSearchDataMoreService.geteGPData("isCRFormulaMadeTORTER", tenderid,lotId, "TOR");
                boolean isCRFormulaMadeTOR = false;
                if(getreportID!=null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")){
                    isCRFormulaMadeTOR = true;
                    getreportID = commonSearchDataMoreService.geteGPData("getTORReportId", tenderid,lotId, null);
                    if(getreportID!=null && (!getreportID.isEmpty())){
                        repId = getreportID.get(0).getFieldName1();
                    }
                }
                //Object repId = creationService.findRepIdofTOR(tenderid, lotId,"TOR");
                if(isCRFormulaMadeTOR){
                    if(repId!=null){
                        String tendrepUserId = request.getParameter("tendrepUserId");
            %>
            
            
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="4" ><%=repLabel%> Document</td>
                </tr>
                <tr>
                    <th width="25%" >Documents Sold</th>
                    <th width="25%">Nos. of Submissions</th>
                    <th width="25%">Nos. Withdrawn</th>
                    <th width="25%">Nos. Substituted / Modified</th>
                </tr>
                <tr>
                    <td class="t-align-center"><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName3());
                                }%></td>
                    <td class="t-align-center"><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName1());
                                }%></td>
                    <td class="t-align-center"><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName2());
                                }%></td>
                    <td class="t-align-center"><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName4());
                                }%></td>
                </tr>
            </table>
            
            
            
            
            
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <th><!--%=repLabel%-->Sl. No.</th>
                    <th>Name of Bidder / Consultant</th>
                    <th>Total Quoted Amount</th>
                    <th>Discount Amount</th>
                    <th>Bid Security Amount</th>
                    <th>Validity of Bid Security</th>
                </tr>
                <%
                        ReportCreationService reportCreationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                        
                        int reportTabId = 0;
                        if(lotId.equalsIgnoreCase("0")){
                            reportTabId = Integer.parseInt(reportCreationService.getReportTableId(repId));
                        } else{
                            reportTabId = Integer.parseInt(reportCreationService.getReportTableIdLot(repId,lotId));
                        }
                        List<TblReportColumnMaster> columnMasters = reportCreationService.getReportColumns(reportTabId);

                        int govColId = 0;

                        for (TblReportColumnMaster tRCM : columnMasters) {
                            if (tRCM.getFilledBy() == 2 && tRCM.getGovCol().equals("yes")) {
                                govColId = tRCM.getColumnId();
                            }
                        }
                        
                        List<CommonTenderDetails> tenderList = tenderSrBean.getAPPDetails(Integer.parseInt(tenderid), "tender");
//                        List<CommonTenderDetails> lotList = tenderSrBean.getAPPDetails(Integer.parseInt(tenderid), "lot");
                        java.text.SimpleDateFormat format2 = new java.text.SimpleDateFormat("dd-MMM-yyyy");
                        int cnt_no = 1;
                        for (SPCommonSearchDataMore tendererData : tendererList) {%>
                <tr>
                    <td class="t-align-center"><%=cnt_no%></td>
                    <%
                        //14 userId,15 tendererId,16 companyId
                        String[] jvSubData = creationService.jvSubContractChk(tenderid, tendererData.getFieldName14());
                        StringBuilder name_link = new StringBuilder();
                        name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewRegistrationDetail.jsp?uId="+tendererData.getFieldName14()+"&tId="+tendererData.getFieldName15()+"&cId="+tendererData.getFieldName16()+"&top=no' target='_blank'>"+tendererData.getFieldName4()+"</a>");
                        if(jvSubData[0]!=null && jvSubData[0].equals("jvyes")){
                            name_link.delete(0, name_link.length());
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewJVCADetails.jsp?uId="+tendererData.getFieldName14()+"' target='_blank'>"+tendererData.getFieldName4()+"</a>");
                            name_link.append("<br/>");
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewJVCADetails.jsp?uId="+tendererData.getFieldName14()+"' target='_blank' style='color:red'>(JVCA - View Details)</a>");
                        }
                        if(jvSubData[1]!=null && jvSubData[1].equals("subyes")){
                            name_link.append("<br/>");
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewTenderSubContractor.jsp?uId="+tendererData.getFieldName14()+"&tenderId="+tenderid+"&tId="+tendererData.getFieldName15()+"&cId="+tendererData.getFieldName16()+"' target='_blank' style='color:red'>(Sub Contractor/Consultant - View Details)</a>");
                        }
                    %>
                    <td><%if(bol_flag){out.print(name_link.toString());}else{out.print(tendererData.getFieldName4());}%></td>
                    
                    <%
                        List<SPCommonSearchDataMore> discountAmount = null;
                        discountAmount = commonSearchDataMoreService.geteGPData("getDiscountAmount", tenderid,lotId, tendererData.getFieldName3());
                        String discount = (discountAmount == null || discountAmount.isEmpty()) ? "0" : discountAmount.get(0).getFieldName1();
                        DecimalFormat df = new DecimalFormat("0.000");
                        df.setMaximumFractionDigits(3);
                    %>
                    
                    <td class="t-align-right"><%=df.format(Float.parseFloat(reportCreationService.getBidderDataForReport(reportTabId, govColId, Integer.parseInt(tendererData.getFieldName3())))+Float.parseFloat(discount))%></td>
                    <td class="t-align-right"><%=df.format(Float.parseFloat(discount))%></td>
                    <%for (CommonTenderDetails lotList : tenderSrBean.getAPPDetails(Integer.parseInt(tenderid), "lot")) {
                        if(lotList.getAppPkgLotId()==Integer.parseInt(lotId)){
                    %>
                    <td class="t-align-right"><%=df.format(lotList.getTenderSecurityAmt().setScale(0,0))%></td>
                    <%}}%>
                    <td class="t-align-center"><%=format2.format(tenderList.get(0).getTenderSecurityDt())%></td>
                </tr>
                <%
                                cnt_no++;
                            }
                            if (!list2) {
                                out.print("<tr><td colspan='");
                                if (isSecurityAvail) {
                                    out.print("8");
                                } else {
                                    out.print("3");
                                }
                                out.print("' style='color:red;' class='t-align-center'>No Tender Security Verified Payment Found</td></tr>");
                            }
                %>
            </table>
            <%
                    }else{out.print("<br/><div class='responseMsg noticeMsg'>Please Prepare a Price Comparison Report first</div>");}
                }else{out.print("<br/><div class='responseMsg noticeMsg'>Report Formula not created.</div>");
            }}%>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="5" ><!--%=repLabel.charAt(0)%-->TOC Members</td>
                </tr>
                <tr>
                    <th width="20%" ><span id="signLabel">Committee Members</span></th>                    
                    <%
                                int cnt_tor2=1;
                                int svpdftor2 = 0;
                                for (SPCommonSearchDataMore dataMore : TOCMembers) {
                                    out.print("<td>");
                                    if(bol_flag){
                                    if (dataMore.getFieldName5().equals(session.getAttribute("userId").toString())) {
                                        if (dataMore.getFieldName6().equals("-")) {
                                         boolean checkPriceBid = true;
                                         String var_data_1 = eventType;
                                         String var_data_2= commonService.getProcMethod(tenderid).toString();
                                         if((var_data_1.equals("REOI") || var_data_1.equals("RFP") || var_data_1.equals("PQ") || var_data_1.equals("1 stage-TSTM") || var_data_2.equals("OSTETM")) || is2Env){
                                            checkPriceBid = false;
                                         }
                    %>

                    <%if(checkPriceBid){%>
                <form id="signFrm_<%=cnt_tor2%>" action="TORSigningApp.jsp?id=<%=dataMore.getFieldName7()%>&uid=<%=dataMore.getFieldName5()%>&tid=<%=tenderid%>&lotId=<%=lotId%>&rpt=torCommon" method="post"></form>
                        <a href="javascript:void(0);" onclick="return checkPriceBid(<%=tenderid%>,<%=lotId%>,'#signFrm_<%=cnt_tor2%>');">
                            <%=dataMore.getFieldName1()%>
                        </a>
                     <%}else{%>
                    <a href="TORSigningApp.jsp?id=<%=dataMore.getFieldName7()%>&uid=<%=dataMore.getFieldName5()%>&tid=<%=tenderid%>&lotId=<%=lotId%>&rpt=torCommon" >
                        <%=dataMore.getFieldName1()%>
                    </a>
                    <%}%>
                    <script type="text/javascript">
                        $('#signLabel').html('Click on link to Sign');
                    </script>
                <%
                                    } else {
                                        out.print(dataMore.getFieldName1());
                                    }

                                } else {
                                    out.print(dataMore.getFieldName1());
                                }
                               }else {
                                    out.print(dataMore.getFieldName1());
                                }
                                out.print("</td>");
                           cnt_tor2++; }
                %>
                </tr>
                <tr>
                    <th width="20%">Committee Role</th>
                    <%for (SPCommonSearchDataMore dataMore : TOCMembers) {
                                    out.print("<td>");
                                    if (dataMore.getFieldName2().equals("cp")) {
                                        out.print("Chairperson");
                                    } else if (dataMore.getFieldName2().equals("ms")) {
                                        out.print("Member Secretary");
                                    } else if (dataMore.getFieldName2().equals("m")) {
                                        out.print("Member");
                                    }
                                    out.print("</td>");
                                }%>
                </tr>
                <tr>
                    <th width="20%">Designation</th>
                    <%for (SPCommonSearchDataMore dataMore : TOCMembers) {
                                    out.print("<td>" + dataMore.getFieldName3() + "</td>");
                                }%>
                </tr>
                <tr>
                    <th width="20%">PA Office</th>
                    <%for (SPCommonSearchDataMore dataMore : TOCMembers) {
                                    out.print("<td>" + dataMore.getFieldName4() + "</td>");
                                }%>
                </tr>
                <tr>
                    <th width="20%">Electronically Signed BOR On</th>
                    <%for (SPCommonSearchDataMore dataMore : TOCMembers) {
                        if(dataMore.getFieldName6().equals("-")){
                            svpdftor2++;
                        }
                                    out.print("<td>" + dataMore.getFieldName6() + "</td>");
                                }%>
                </tr>
                <tr>
                    <th width="20%">Comments</th>
                    <%for(SPCommonSearchDataMore dataMore : TOCMembers) {out.print("<td>"+dataMore.getFieldName8()+"</td>");}%>
                </tr>
            </table>
                 <%if(svpdftor2!=0){%>
                    <script type="text/javascript">
                        $('#svpdftor2').hide();
                    </script>
                <%}%>
        </div>
        </div>
                <%if(bol_flag){%>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <%}%>
    </body>
    <%
                tendOpeningRpt = null;
                tendDocument = null;
                TOCMembers = null;
                tendererList = null;
    %>
    <script type="text/javascript">
    $(document).ready(function() {

        $("#print").click(function() {
            //alert('sa');
            printElem({ leaveOpen: true, printMode: 'popup' });
        });

    });
    function printElem(options){
        //alert(options);

        $('#print_area').printElement(options);
        //$('#trLast').hide();
    }

</script>
</html>
<%if(request.getParameter("goback")!=null && request.getParameter("goback")!=""){%>
<script type="text/javascript">
    var headSel_Obj = document.getElementById("headTabEval");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
</script>
<%}else{%>
<script type="text/javascript">
    var headSel_Obj = document.getElementById("headTabTender");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
</script>
<%}%>

