<%-- 
    Document   : TendererReport
    Created on : Oct 29, 2013, 12:49:23 PM
    Author     : Ahsan
--%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.web.servicebean.LoginMasterSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
<jsp:useBean id="mapTendererDtBean" class="com.cptu.egp.eps.web.databean.MapTendererDtBean"/>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean" %>
<jsp:useBean id="dtBean" class="com.cptu.egp.eps.web.servlet.TendererServlet" scope="request"/>

<jsp:setProperty property="*" name="mapTendererDtBean"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
    <%
    response.setHeader("Expires", "-1");
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
    response.setHeader("Pragma", "no-cache");
    boolean isPDF = false;
     if(request.getParameter("isPDF")!=null){
            isPDF = true;
        }
    %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tenderer Report</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <script src="../resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jquery.dataTables.js"></script>
        <link href="../resources/css/demo_table.css" type="text/css" rel="stylesheet">

         <script type="text/javascript">
            /* Call Print function */

            $(document).ready(function() {
                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });
            function printElem(options){
                $('#titleDiv').show();
                $('#print_area').printElement(options);
                $('#titleDiv').hide();
            }
        </script>
    </head>
<%
                int userid = 0;

                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") != null) {
                    userid = Integer.parseInt(hs.getAttribute("userId").toString());
                }
CommonSearchDataMoreService commonSearchDataMoreServiceForMap = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
String cntName = "";
%>
<script type="text/javascript">
     var perCountryVal = "";
     var comCountryVal = "";
     var corCountryVal = "";
     function loadTable()
            {  
                if (typeof oTable == 'undefined') {
                oTable = $('#gridPlan').dataTable({
                    "sDom": '<rt"top"><"bottom"li><"clear">',
                    //"bPaginate": false,
                    "sPaginationType": "full_numbers",
                    "bProcessing": true,
                    "bFilter": true,
                    //"aaSorting": [],
                    "aoColumns": [null, null, null, null, null, null, null,null],
                    "sAjaxSource": "<%=request.getContextPath()%>/TendererServlet",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": sSource,
                            "data": { "comCountry": comCountryVal,"comState":$("#cmbComRegAddState").val(),"comCity": $("#txtComRegCityTown").val(),"comThana":$("#txtComRegThanaUpzilla").val(),"corCountry":corCountryVal,"corState":$("#cmbCorHeadState").val(),"corCity":$("#txtcorHeadCityTown").val(),"corThana":$("#txtcorHeadThanaUpzilla").val(),"perCountry":perCountryVal,"perState":$("#cmbPerAddState").val(),"perCity":$("#txtperAddCityTown").val(),"perThana":$("#txtperAddThanaUpzilla").val()},
                            "success": fnCallback
                        });
                    }
                });
            }
            else {
                $('#gridPlan').dataTable({
                   "sDom": '<rt"top"><"bottom"li><"clear">',
                  // "bPaginate": false,
                    "sPaginationType": "full_numbers",
                    "bProcessing": true,
                   //  "bFilter": true,
                   //"bServerSide": true,
                    "bDestroy": true,
                    "bRegex":true,
                    //"aaSorting": [],
                    "aoColumns": [null, null, null, null, null, null, null,null],
                    "sAjaxSource": "<%=request.getContextPath()%>/TendererServlet",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": sSource,
                            "data": { "comCountry":comCountryVal,"comState":$("#cmbComRegAddState").val(),"comCity": $("#txtComRegCityTown").val(),"comThana":$("#txtComRegThanaUpzilla").val(),"corCountry":corCountryVal,"corState":$("#cmbCorHeadState").val(),"corCity":$("#txtcorHeadCityTown").val(),"corThana":$("#txtcorHeadThanaUpzilla").val(),"perCountry":perCountryVal,"perState":$("#cmbPerAddState").val(),"perCity":$("#txtperAddCityTown").val(),"perThana":$("#txtperAddThanaUpzilla").val()},
                            //"data": { "status": status },
                            "success": fnCallback
                        });
                    }
                });

            }
          }

            $(function() {
                $('#cmbComRegAddContry').change(function() {
                    var comCountry = $('#cmbComRegAddContry').val();
                    comCountry = comCountry.substring(0, comCountry.indexOf("_", 0));
                    comCountryVal = $('#cmbComRegAddContry').val().substring($('#cmbComRegAddContry').val().indexOf("_", 0)+1, $('#cmbComRegAddContry').val().length);

                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:comCountry,funName:'stateComboValue'},  function(j){
                        j = j.toString().replace('selected', '');
                        $("select#cmbComRegAddState").html('<option value="" selected >Please select State</option>'+j);
                    });
                });
                $('#cmbCorHeadContry').change(function() {
                    var corCountry = $('#cmbCorHeadContry').val();
                    corCountry = corCountry.substring(0, corCountry.indexOf("_", 0));
                    corCountryVal = $('#cmbCorHeadContry').val().substring($('#cmbCorHeadContry').val().indexOf("_", 0)+1, $('#cmbCorHeadContry').val().length);

                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:corCountry,funName:'stateComboValue'},  function(j){
                        j = j.toString().replace('selected', '');
                        $("select#cmbCorHeadState").html('<option value="" selected >Please select State</option>'+j);
                    });
                });
                $('#cmbPerAddContry').change(function() {
                    var perCountry = $('#cmbPerAddContry').val()
                    var perCountry = perCountry.substring(0, perCountry.indexOf("_", 0));
                    perCountryVal = $('#cmbPerAddContry').val().substring($('#cmbPerAddContry').val().indexOf("_", 0)+1, $('#cmbPerAddContry').val().length);
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:perCountry,funName:'stateComboValue'},  function(j){
                        j = j.toString().replace('selected', '');
                        $("select#cmbPerAddState").html('<option value="" selected >Please select State</option>'+j);
                    });
                });
            });

           function resetTable(){

             comCountryVal ="";
             corCountryVal ="";
             perCountryVal ="";
             
             $("#txtComRegCityTown").val("");
             $("#txtComRegThanaUpzilla").val("");
             $("#txtcorHeadCityTown").val("");
             $("#txtcorHeadThanaUpzilla").val("");
             $("#txtperAddCityTown").val("");
             $("#txtperAddThanaUpzilla").val("");
             $("#cmbComRegAddState :selected").val("");
             $("#cmbCorHeadState :selected").val("");
             $("#cmbCorHeadState :selected").val("");
             $("#cmbPerAddState :selected").val("");

            
             loadTable();
            }
            

</script>

<body onload="loadTable()">
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <%@include file="../resources/common/AfterLoginTop.jsp" %>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
  <div class="pageHead_1">Bidder Report</div>
  <div class="formBg_1 t_space">
      <form name="frmTendererSearch" id="frmTendererSearch" method="post">
  <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">

    <tr>
        <th colspan="4" class="formSubHead_1">Company Registered Address</th>
    </tr>

     <tr>
        <td class="ff" width="20%">Country :</td>
        <td width="30%">
           <select name="comRegAddContry" class="formTxtBox_1" id="cmbComRegAddContry" style="width: 200px;">
                <option value="" selected >Please select Country</option>
                <%for (SelectItem country : tendererMasterSrBean.getCountryList()) {%>
                <option value="<%=country.getObjectId()%>_<%=country.getObjectValue()%>"><%=country.getObjectValue()%></option>
                <%}%>
            </select>

        </td>
        <td class="ff" width="20%">Dzongkhag / District :</td>
        <td width="30%">
            <select name="comRegAddState" class="formTxtBox_1" id="cmbComRegAddState" style="width: 200px;">

                <%cntName = "Bangladesh";
                for (SelectItem state : tendererMasterSrBean.getStateList(cntName)) {%>
                <option value="<%=state.getObjectValue()%>" ><%=state.getObjectValue()%></option>
                <%}%>

                <option value="" selected >Please select Dzongkhag / District</option>

            </select>
        </td>
    </tr>
    <tr>
        <td class="ff" width="20%">City / Town :</td>
        <td width="30%"><input name="comRegCityTown" type="text" class="formTxtBox_1" id="txtComRegCityTown" style="width:200px;"/></td>
        <td class="ff" width="20%">Gewog :</td>
        <td width="30%">
            <input name="comRegThanaUpzilla" type="text" class="formTxtBox_1" id="txtComRegThanaUpzilla" style="width:200px;"/>
        </td>
    </tr>
     <tr>
        <th colspan="4" class="formSubHead_1">Corporate / Head office</th>
    </tr>
    <tr>
        <td class="ff" width="20%">Country :</td>
        <td width="30%">
           <select name="corHeadContry" class="formTxtBox_1" id="cmbCorHeadContry" style="width: 200px;">
                <option value="" selected >Please select Country</option>
                <%for (SelectItem country : tendererMasterSrBean.getCountryList()) {%>
                <option value="<%=country.getObjectId()%>_<%=country.getObjectValue()%>"><%=country.getObjectValue()%></option>
                <%}%>
            </select>

        </td>
        <td class="ff" width="20%">Dzongkhag / District :</td>
        <td width="30%">
            <select name="corHeadState" class="formTxtBox_1" id="cmbCorHeadState" style="width: 200px;">

                <%cntName = "Bangladesh";
                for (SelectItem state : tendererMasterSrBean.getStateList(cntName)) {%>
                <option value="<%=state.getObjectValue()%>" ><%=state.getObjectValue()%></option>
                <%}%>

                <option value="" selected >Please select Dzongkhag / District</option>

            </select>
        </td>
    </tr>
    <tr>
        <td class="ff" width="20%">City / Town :</td>
        <td width="30%"><input name="corHeadCityTown" type="text" class="formTxtBox_1" id="txtcorHeadCityTown" style="width:200px;"/></td>
        <td class="ff" width="20%">Gewog :</td>
        <td width="30%">
            <input name="corHeadThanaUpzilla" type="text" class="formTxtBox_1" id="txtcorHeadThanaUpzilla" style="width:200px;"/>
        </td>
    </tr>
    <tr>
        <th colspan="4" class="formSubHead_1">Personal Address</th>
    </tr>
    <tr>
        <td class="ff" width="20%">Country :</td>
        <td width="30%">
           <select name="perAddContry" class="formTxtBox_1" id="cmbPerAddContry" style="width: 200px;">
                <option value="" selected >Please select Country</option>
                <%for (SelectItem country : tendererMasterSrBean.getCountryList()) {%>
                <option value="<%=country.getObjectId()%>_<%=country.getObjectValue()%>"><%=country.getObjectValue()%></option>
                <%}%>
            </select>

        </td>
        <td class="ff" width="20%">Dzongkhag / District :</td>
        <td width="30%">
            <select name="perAddState" class="formTxtBox_1" id="cmbPerAddState" style="width: 200px;">

                <%cntName = "Bangladesh";
                for (SelectItem state : tendererMasterSrBean.getStateList(cntName)) {%>
                <option value="<%=state.getObjectValue()%>" ><%=state.getObjectValue()%></option>
                <%}%>

                <option value="" selected >Please select Dzongkhag / District</option>

            </select>
        </td>
    </tr>
    <tr>
        <td class="ff" width="20%">City / Town :</td>
        <td width="30%"><input name="perAddCityTown" type="text" class="formTxtBox_1" id="txtperAddCityTown" style="width:200px;"/></td>
        <td class="ff" width="20%">Gewog :</td>
        <td width="30%">
            <input name="perAddThanaUpzilla" type="text" class="formTxtBox_1" id="txtperAddThanaUpzilla" style="width:200px;"/>
        </td>
    </tr>
    <tr>
      <td colspan="4" class="t-align-center">
          <label class="formBtn_1">
              <input type="button" name="button" id="button" value="Search" onclick="loadTable()" />
          </label>
          <label class="formBtn_1">
              <input type="reset" name="Reset" id="btnReset" value="Reset" onclick="resetTable()" />
              
          </label>
      </td></tr>
    <tr>
      <td colspan="4" class="t-align-right">
        <%
           String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
           String folderName = pdfConstant.MISCPMTRPT;
           String genId = cdate + "_" + userid;
        %>
        <span style="float: right;display: none;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('8');">Save as PDF</a></span>
       <!--<a class="action-button-savepdf" href="<%=request.getContextPath()%>/GeneratePdf?reqURL=&reqQuery=&folderName=<%=folderName%>&id=<%=genId%>" >Save As PDF </a>-->
                                           &nbsp;
       <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
      </td>
    </tr>
  </table>
  </form>
  </div>
 <br>
<div id="resultDiv" style="display: block;">
   <div  id="print_area">
       <div id="titleDiv" class="pageHead_1" style="display: none">Bidder Report</div>
      <div id="gridContainer">
                    <table cellpadding="0" cellspacing="0" border="1" class="display table-style" id="gridPlan">
                        <thead>
                            <tr>
                                <th width="3%">Sl. No</th>
                                <th width="15%">Company Name</th>
                                <th width="12%">Company Registration No</th>
                                <th width="15%">E-mail ID</th>
                                <th width="14%">Company Registered Country</th>
                                <th width="10%">Company Registered Dzongkhag / District</th>
                                <th width="10%">Registration Type</th>
                                <th width="7%">Legal Status</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
            </div>
<!--<table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable" cols="@0,8">
      <tr>
        <th width="3%" class="t-align-center">S. No.</th>
        <th width="15%" class="t-align-center">Company Name</th>
        <th width="10%" class="t-align-center">Company Registration No</th>
        <th width="15%" class="t-align-center">e-mail ID</th>
        <th width="20%" class="t-align-center">Company Registered Country</th>
        <th width="10%" class="t-align-center">Company Registered District</th>
        <th width="10%" class="t-align-center">Registration Type</th>
        <th width="12%" class="t-align-center">Legal Status</th>
       
    </tr>
</table> -->
</div>

<!--<table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
    <tr>
        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">20</span></td>
        <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
            &nbsp;
            <label class="formBtn_1">
                <input type="button" name="button" id="btnGoto" id="button" value="Go To Page" />
            </label></td>
        <td align="right" class="prevNext-container"><ul>
                <li><font size="3">&laquo;</font> <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                <li><font size="3">&#8249;</font> <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                <li><a href="javascript:void(0)" id="btnNext">Next</a><font size="3"> &#8250;</font></li>
                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
            </ul></td>
    </tr>
</table>-->
 <input type="hidden" id="pageNo" value="1"/>
 <input type="hidden" id="size" value="20"/>
</div>
  <p>&nbsp;</p>
    <div>&nbsp;</div>
</div>
<form id="formstyle" action="" method="post" name="formstyle">
   <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
   <%
     SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
     String appenddate = dateFormat1.format(new Date());
   %>
   <input type="hidden" name="fileName" id="fileName" value="MiscellaneousPaymentReport_<%=appenddate%>" />
    <input type="hidden" name="id" id="id" value="MiscellaneousPaymentReport" />
</form>
    <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
 <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
  <!--Dashboard Footer End-->
</div>
</body>
 <script>
        var headSel_Obj = document.getElementById("headTabReport");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
