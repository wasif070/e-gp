<%-- 
    Document   : eGPStatistics
    Created on : Jun 28, 2012, 3:13:41 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.model.table.TblStateMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.AppMISService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            boolean isNotPDF = true;
            if (request.getParameter("isPDF") != null && "true".equalsIgnoreCase(request.getParameter("isPDF"))) {
                isNotPDF = false;
            }
            int uTypeId = 0;
            String suserId = null;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Dynamic Saving Calculation Report</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    </head>
    <body onload="hide();">

        <%if (isNotPDF) {
                suserId = session.getAttribute("userId").toString();%>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <%uTypeId = Integer.parseInt(objUserTypeId.toString());
            } else {
                uTypeId = Integer.parseInt(request.getParameter("uTypeId"));
                suserId = request.getParameter("suserId");
            }
            AppMISService mISService = (AppMISService) AppContext.getSpringBean("AppMISService");
            CommonSearchDataMoreService dataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            String departmentId = request.getParameter("depId");
            String officeId = (request.getParameter("offId") == null || request.getParameter("offId").equals("0") ? "" : request.getParameter("offId"));
            String procurementType = request.getParameter("cmbProcNature");
            String budgetId = request.getParameter("budgetType");
            String district = request.getParameter("cmbDistrict");
            String cpvCategory = request.getParameter("cpvCat");
            String procurementMethodId = request.getParameter("procMethod");
            String procMethodName = request.getParameter("procMethodName");
            String budgetName = request.getParameter("budgetTypeName");
            String deptName = request.getParameter("depName");
        %>
        <jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>
        <div class="contentArea_1">

            <div class="pageHead_1">Dynamic Saving Calculation Report</div>

            <%if (isNotPDF) {%>
            <div class="formBg_1">
                <form method="post" action="SaveCalculation.jsp">
                    <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                    <table width="100%" cellspacing="8" cellpadding="0"  class="formStyle_1" id="tblSearchBox">
                        <tr>
                            <td class="ff t-align-left">Hierarchy Node : <!--%if (uTypeId == 1) {%><span class="mandatory">*</span>< %}%--></td>
                            <td class="t-align-left">
                                <%if (uTypeId == 5) {
                                        Object[] objData = mISService.getOrgAdminOrgName(Integer.parseInt(suserId));
                                        out.print(objData[1].toString());
                                        out.print("<input type=\"hidden\" id=\"txtdepartment\" name=\"depName\" value=\"" + objData[1].toString() + "\"/>");
                                        out.print("<input type=\"hidden\" id=\"txtdepartmentid\" name=\"depId\" value=\"" + objData[0] + "\"/>");
                                    } else {%>
                                <input type="text" id="txtdepartment" style="width: 200px;" class="formTxtBox_1" readonly name="depName"/>
                                <input type="hidden" id="cmborg">
                                <!--To add CCGP and Cabinet Division add this in below as queryString ?operation=govuser-->
                                <a id="imgTree" href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=PackageReport', '', 'width=350px,height=400px,scrollbars=1', '');">
                                    <img alt="Department" style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                </a>
                                <input type="hidden" id="txtdepartmentid" name="depId">
                                <input type="hidden" id="txtDepartmentType">
                                <%}%>
                            </td>
                            <td class="ff t-align-left">PA Office :</td>
                            <td class="t-align-left">
                                <%if (uTypeId == 5 || uTypeId == 1) {%>
                                <select id="cmboffice" style="width: 200px;" class="formTxtBox_1" name="offId" onchange="setOffName()">
                                    <%

                                        if (uTypeId == 1) {
                                    %>
                                    <option value=""> No Office Found.</option>
                                    <%                                    }
                                        List<Object[]> cmbLists = null;
                                        if (uTypeId == 5) {
                                            cmbLists = mISService.getOfficeList(suserId, "" + uTypeId);
                                            for (Object[] cmbList : cmbLists) {
                                                String selected = "";
                                                if (request.getParameter("offId") != null) {
                                                    if (cmbList[0].toString().equals(request.getParameter("offId"))) {
                                                        selected = "selected";
                                                    }
                                                }
                                                out.print("<option value='" + cmbList[0] + "' " + selected + ">" + cmbList[1] + "</option>");
                                                selected = null;
                                            }
                                            if (cmbLists.isEmpty()) {
                                                out.print("<option value='0'> No Office Found.</option>");
                                            }
                                        }
                                    %>
                                </select>
                                <input type="hidden" id="offName" value="<%=(uTypeId == 5) ? (request.getParameter("offName") != null && !"".equals(request.getParameter("offName"))) ? request.getParameter("offName") : !cmbLists.isEmpty() ? cmbLists.get(0)[1].toString() : "" : ""%>" name="offName" />
                                <%
                                    }
                                %>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Procurement Category:</td>
                            <td><select name="cmbProcNature" class="formTxtBox_1" id="cmbProcNature" style="width:200px;">
                                    <option value="" selected="selected">--Select Procurement  Category--</option>
                                    <option value="Goods">Goods</option>
                                    <option value="Works">Works</option>
                                    <option value="services">Services</option>
                                </select></td>
                            <td class="ff">Procurement Method :</td>
                            <td><select name="procMethod" class="formTxtBox_1" id="cmbProcMethod" onchange="setProcMethodName()" style="width:208px;">
                                    <option value="" selected="selected">- Select Procurement Method -</option>
                                    <c:forEach var="procMethod" items="${advAppSearchSrBean.procMethodList}">
                                        <c:choose>
                                            <c:when test = "${procMethod.objectValue=='RFQ'}">
                                                <option value="${procMethod.objectId}">LEM</option>
                                            </c:when>
                                            <c:when test = "${procMethod.objectValue=='DPM'}">
                                                <option value="${procMethod.objectId}">DCM</option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${procMethod.objectId}">${procMethod.objectValue}</option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                                <input type="hidden" id="procMethodName" value="<%= (request.getParameter("procMethod") != null && !"".equals(request.getParameter("procMethod"))) ? request.getParameter("procMethod") : ""%>" name="procMethodName" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Source of Fund :</td>
                            <td>
                                <select name="budgetType" class="formTxtBox_1" id="cmbBudgetType" onchange="setBudgetType()" style="width:200px;" >
                                    <option value="" >All</option>
                                    <option value="1">Development</option>
                                    <option value="2">Revenue</option>
                                    <option value="3">Own Fund</option>
                                    <option value="1,2">Development and Revenue</option>
                                    <option value="2,3">Revenue and Own Fund</option>
                                    <option value="1,3">Development and Own Fund</option>
                                </select>
                                <input type="hidden" id="budgetTypeName" value="<%= (request.getParameter("budgetType") != null && !"".equals(request.getParameter("budgetType"))) ? request.getParameter("budgetType") : ""%>" name="budgetTypeName" />
                                <br />
                                <span style="color: red;" id="msgBudgetType"></span>
                            </td>
                            <td class="ff">Category :</td>
                            <td colspan="3">
                                <input name="cpvCat" id="txtaCPV" type="text" class="formTxtBox_1" style="width:202px;" />
                                <a href="javascript:void(0);" class="action-button-select" onclick="loadCPVTree()">Select Category</a>
                            </td>
                        </tr>
                        <tr>
                            <!--                            <td width="20%" class="ff">Dzongkhag / District : </td>
                            <%
                                CommonService cservice = (CommonService) AppContext.getSpringBean("CommonService");
                                //Code by proshanto
                                short countryId = 150;//136
                                List<TblStateMaster> liststate = cservice.getState(countryId);
                            %>
                            <td width="30%">
                                <select name="cmbDistrict" class="formTxtBox_1" id="cmbDistrict" style="width:95%;">
                                    <option value="">-- Select --</option>
                            <%
                                for (TblStateMaster state : liststate) {
                                    out.println("<option value='" + state.getStateName() + "'>" + state.getStateName() + "</option>");
                                }
                            %>
                        </select>
                        <span id="msgdistrict" class="reqF_1"></span>
                    </td>-->
                        <span id="msgdistrict" class="reqF_1"></span>
                        <input type="hidden" name="cmbDistrict" class="formTxtBox_1" id="cmbDistrict" style="width:95%;" value="">
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <label class="formBtn_1"><input type="submit" value="Generate" id="generate" name="generate"></label>
                                <label class="anchorLink l_space">
                                    <!--                                            <input type="reset" value="Clear" id="clear" name="clear" onclick="window.location.reload()">-->
                                    <a href="SaveCalculation.jsp" style="text-decoration: none; color: #fff; font-size: 12px;">Reset</a>
                                </label>
                            </td>
                            <td class="ff" colspan="2">
                                <%if (isNotPDF) {%>
                                <span style="float: right;">
                                    <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>
                                    &nbsp;&nbsp;
                                    <%
                                        StringBuffer hrefQString = new StringBuffer();
                                        hrefQString.append("&depId=");
                                        hrefQString.append(departmentId);
                                        hrefQString.append("&offId=");
                                        hrefQString.append(officeId);
                                        hrefQString.append("&budgetType=");
                                        hrefQString.append(budgetId);
                                        hrefQString.append("&budgetTypeNamePdf=");
                                        hrefQString.append(budgetName);
                                        hrefQString.append("&cmbDistrict=");
                                        hrefQString.append(district);
                                        hrefQString.append("&cpvCat=");
                                        hrefQString.append(cpvCategory);
                                        hrefQString.append("&procMethod=");
                                        hrefQString.append(procurementMethodId);
                                        hrefQString.append("&procMethodNamePdf=");
                                        hrefQString.append(procMethodName);
                                        hrefQString.append("&cmbProcNature=");
                                        hrefQString.append(procurementType);
                                        hrefQString.append("&depName=");
                                        hrefQString.append(deptName);
                                    %>
                                    <a class="action-button-savepdf" id="saveASPDF" href="<%=request.getContextPath()%>/TorRptServlet?uTypeId=<%=uTypeId%>&action=saveCalc&generate=Generate<%=hrefQString.toString()%>">Save As PDF</a>
                                </span>
                                <%}%>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

            <%}%>
            <%
                List<SPCommonSearchDataMore> spsaveCalcNCT = null;
                List<SPCommonSearchDataMore> spsaveCalcICT = null;
                List<SPCommonSearchDataMore> spsaveCalcALL = null;
                if ("Generate".equals(request.getParameter("generate"))) {
                    //System.out.println("geteGPSaveCalc"+","+departmentId + "," + officeId + "," + procurementType + "," + budgetId + "," + district + "," + cpvCategory+","+"NCT"+","+procurementMethodId);
                    spsaveCalcNCT = dataMoreService.geteGPDataMore("geteGPSaveCalc", departmentId, officeId, procurementType, budgetId, district, cpvCategory, "NCT", procurementMethodId);
                    spsaveCalcICT = dataMoreService.geteGPDataMore("geteGPSaveCalc", departmentId, officeId, procurementType, budgetId, district, cpvCategory, "ICT", procurementMethodId);
                    spsaveCalcALL = dataMoreService.geteGPDataMore("geteGPSaveCalc", departmentId, officeId, procurementType, budgetId, district, cpvCategory, "", procurementMethodId);

            %>
            <div  id="print_area">
                <div class="formStyle_1 t_space ff" id="searchCriteria">
                    <h3>
                        Search Criteria :
                        <%                            String commaSepr = "";
                            if (departmentId != null && !departmentId.equals("")) {
                                out.print("Hierarchy Node : " + deptName);
                                commaSepr = ",";
                            }
                            if (officeId != null && !officeId.equals("") && !officeId.equals("0")) {
                                out.print(commaSepr + " PA Office : " + request.getParameter("offName"));
                                commaSepr = ",";
                            }
                            if (procurementType != null && !procurementType.equals("")) {
                                out.print(commaSepr + " Type of Procurement : " + procurementType);
                                commaSepr = ",";
                            }
                            if (procurementMethodId != null && !procurementMethodId.equals("")) {
                                if (!isNotPDF) {
                                    out.print(commaSepr + " Procurement Method : " + request.getParameter("procMethodNamePdf"));
                                } else {
                                    out.print(commaSepr + " Procurement Method : " + request.getParameter("procMethodName"));
                                }
                                commaSepr = ",";
                            }
                            if (budgetId != null && !budgetId.equals("")) {
                                if (!isNotPDF) {
                                    out.print(commaSepr + " Source of Fund : " + request.getParameter("budgetTypeNamePdf"));
                                } else {
                                    out.print(commaSepr + " Source of Fund : " + request.getParameter("budgetTypeName"));
                                }
                                commaSepr = ",";
                            }
                            if (district != null && !district.equals("")) {
                                out.print(commaSepr + " Dzongkhag / District : " + request.getParameter("cmbDistrict"));
                                commaSepr = ",";
                            }
                            if (cpvCategory != null && !cpvCategory.equals("")) {
                                out.print(commaSepr + " Category : " + request.getParameter("cpvCat"));
                                commaSepr = ",";
                            }
                        %>
                    </h3>
                </div>

                <%if (spsaveCalcNCT != null && spsaveCalcNCT.size() > 0) {%>
                <div class="inner-tableHead t_space"> National Contracts</div>

                <table width="100%" cellspacing="0" class="tableList_1" id="resultTable">
                    <tr id="subtableheading">
                        <th rowspan=3 class="t-align-center">Sl. No.</th>
                        <th rowspan=3 class="t-align-center">Name of Agency</th>
                        <th rowspan=3 class="t-align-center">The number of Awarded contracts </th>
                            <%-- Changed By Emtaz on 16/April/2016. BTN-> Nu. --%>
                        <th rowspan=3 class="t-align-center">Official Estimated Cost (Nu. In Million)</th>
                        <th rowspan=3 class="t-align-center">Total Value of Contracts, (Nu. In Million)</th>
                        <th rowspan=3 class="t-align-center">Total Amount Saved, (Nu. In Million)</th>
                        <th rowspan=3 class="t-align-center">Total Savings by Contracts,%</th>
                        <th colspan=12 class="t-align-center">Procurements</th>
                        <th rowspan=3 class="t-align-center">Total No. of Contract Executed</th>
                    </tr>

                    <tr>
                        <th colspan=4>Goods</th>
                        <th colspan=4>works</th>
                        <th colspan=4>services</th>
                    </tr>

                    <tr>
                        <th>Total number of Contracts</th>
                        <th>Total Value of Contracts (Nu. In Million)</th>
                        <th>Total Amount Saved (Nu. In Million)</th>
                        <th>Saving by Contracts, %</th>
                        <th>Total number of Contracts</th>
                        <th>Total Value of Contracts (Nu. In Million)</th>
                        <th>Total Amount Saved (Nu. In Million)</th>
                        <th>Saving by Contracts, %</th>
                        <th>Total number of Contracts (Nu. In Million)</th>
                        <th>Total Value of Contracts (Nu. In Million)</th>
                        <th>Total Amount Saved (Nu. In Million)</th>
                        <th>Total Savings by Contracts, %</th>
                    </tr>

                    <tr>
                        <th>1</th>
                        <th>2</th>
                        <th>3</th>
                        <th>4</th>
                        <th>5</th>
                        <th>6</th>
                        <th>7</th>
                        <th>8</th>
                        <th>9</th>
                        <th>10</th>
                        <th>11</th>
                        <th>12</th>
                        <th>13</th>
                        <th>14</th>
                        <th>15</th>
                        <th>16</th>
                        <th>17</th>
                        <th>18</th>
                        <th>19</th>
                        <th>20</th>
                    </tr>
                    <%

                        int count = 1;
                        for (SPCommonSearchDataMore saveList : spsaveCalcNCT) {
                    %>
                    <tr>
                        <td style="text-align: left;" id="t1col1_<%=count%>"><%=count%></td>
                        <td style="text-align: left;" id="t1col2_<%=count%>"><%=saveList.getFieldName1()%></td>
                        <td style="text-align: left;" id="t1col3_<%=count%>"><%=saveList.getFieldName2()%></td>
                        <td style="text-align: left;" id="t1col4_<%=count%>"><%=saveList.getFieldName3()%></td>
                        <td style="text-align: left;" id="t1col5_<%=count%>"><%=saveList.getFieldName4()%></td>
                        <td style="text-align: left;" id="t1col6_<%=count%>"><%=saveList.getFieldName5()%></td>
                        <td style="text-align: left;" id="t1col7_<%=count%>"><%=saveList.getFieldName6()%></td>
                        <td style="text-align: left;" id="t1col8_<%=count%>"><%=saveList.getFieldName7()%></td>
                        <td style="text-align: left;" id="t1col9_<%=count%>"><%=saveList.getFieldName8()%></td>
                        <td style="text-align: left;" id="t1col10_<%=count%>"><%=saveList.getFieldName9()%></td>
                        <td style="text-align: left;" id="t1col11_<%=count%>"><%=saveList.getFieldName10()%></td>
                        <td style="text-align: left;" id="t1col12_<%=count%>"><%=saveList.getFieldName11()%></td>
                        <td style="text-align: left;" id="t1col13_<%=count%>"><%=saveList.getFieldName12()%></td>
                        <td style="text-align: left;" id="t1col14_<%=count%>"><%=saveList.getFieldName13()%></td>
                        <td style="text-align: left;" id="t1col15_<%=count%>"><%=saveList.getFieldName14()%></td>
                        <td style="text-align: left;" id="t1col16_<%=count%>"><%=saveList.getFieldName15()%></td>
                        <td style="text-align: left;" id="t1col17_<%=count%>"><%=saveList.getFieldName16()%></td>
                        <td style="text-align: left;" id="t1col18_<%=count%>"><%=saveList.getFieldName17()%></td>
                        <td style="text-align: left;" id="t1col19_<%=count%>"><%=saveList.getFieldName18()%></td>
                        <td style="text-align: left;" id="t1col20_<%=count%>"><%=saveList.getFieldName19()%></td>
                    </tr>
                    <%
                                count++;
                            }
                        }
                    %>


                </table>
                <% if (spsaveCalcICT != null && spsaveCalcICT.size() > 0) { %>
                <div class="inner-tableHead t_space">
                    International Procurement
                </div>
                <table width="100%" cellspacing="0" class="tableList_1" id="resultTable">
                    <tr id="subtableheading">
                        <th rowspan=3 class="t-align-center">Sl. No.</th>
                        <th rowspan=3 class="t-align-center">Name of Agency</th>
                        <th rowspan=3 class="t-align-center">The number of Awarded contracts </th>
                        <th rowspan=3 class="t-align-center">Official Estimated Cost (Nu. In Million)</th>
                        <th rowspan=3 class="t-align-center">Total Value of Contracts, (Nu. In Million)</th>
                        <th rowspan=3 class="t-align-center">Total Amount Saved, (Nu. In Million)</th>
                        <th rowspan=3 class="t-align-center">Total Savings by Contracts,%</th>
                        <th colspan=12 class="t-align-center">Procurements</th>
                        <th rowspan=3 class="t-align-center">Total No. of Contract Executed</th>
                    </tr>

                    <tr>
                        <th colspan=4>Goods</th>
                        <th colspan=4>works</th>
                        <th colspan=4>services</th>
                    </tr>

                    <tr>
                        <th>Total number of Contracts</th>
                        <th>Total Value of Contracts (Nu. In Million)</th>
                        <th>Total Amount Saved (Nu. In Million)</th>
                        <th>Saving by Contracts, %</th>
                        <th>Total number of Contracts</th>
                        <th>Total Value of Contracts (Nu. In Million)</th>
                        <th>Total Amount Saved (Nu. In Million)</th>
                        <th>Saving by Contracts, %</th>
                        <th>Total number of Contracts</th>
                        <th>Total Value of Contracts (Nu. In Million)</th>
                        <th>Total Amount Saved (Nu. In Million)</th>
                        <th>Total Savings by Contracts, %</th>
                    </tr>

                    <tr>
                        <th>1</th>
                        <th>2</th>
                        <th>3</th>
                        <th>4</th>
                        <th>5</th>
                        <th>6</th>
                        <th>7</th>
                        <th>8</th>
                        <th>9</th>
                        <th>10</th>
                        <th>11</th>
                        <th>12</th>
                        <th>13</th>
                        <th>14</th>
                        <th>15</th>
                        <th>16</th>
                        <th>17</th>
                        <th>18</th>
                        <th>19</th>
                        <th>20</th>
                    </tr>
                    <%

                        int count = 1;
                        for (SPCommonSearchDataMore saveList : spsaveCalcICT) {
                    %>
                    <tr>
                        <td style="text-align: left;" id="t2col1_<%=count%>"><%=count%></td>
                        <td style="text-align: left;" id="t2col2_<%=count%>"><%=saveList.getFieldName1()%></td>
                        <td style="text-align: left;" id="t2col3_<%=count%>"><%=saveList.getFieldName2()%></td>
                        <td style="text-align: left;" id="t2col4_<%=count%>"><%=saveList.getFieldName3()%></td>
                        <td style="text-align: left;" id="t2col5_<%=count%>"><%=saveList.getFieldName4()%></td>
                        <td style="text-align: left;" id="t2col6_<%=count%>"><%=saveList.getFieldName5()%></td>
                        <td style="text-align: left;" id="t2col7_<%=count%>"><%=saveList.getFieldName6()%></td>
                        <td style="text-align: left;" id="t2col8_<%=count%>"><%=saveList.getFieldName7()%></td>
                        <td style="text-align: left;" id="t2col9_<%=count%>"><%=saveList.getFieldName8()%></td>
                        <td style="text-align: left;" id="t2col10_<%=count%>"><%=saveList.getFieldName9()%></td>
                        <td style="text-align: left;" id="t2col11_<%=count%>"><%=saveList.getFieldName10()%></td>
                        <td style="text-align: left;" id="t2col12_<%=count%>"><%=saveList.getFieldName11()%></td>
                        <td style="text-align: left;" id="t2col13_<%=count%>"><%=saveList.getFieldName12()%></td>
                        <td style="text-align: left;" id="t2col14_<%=count%>"><%=saveList.getFieldName13()%></td>
                        <td style="text-align: left;" id="t2col15_<%=count%>"><%=saveList.getFieldName14()%></td>
                        <td style="text-align: left;" id="t2col16_<%=count%>"><%=saveList.getFieldName15()%></td>
                        <td style="text-align: left;" id="t2col17_<%=count%>"><%=saveList.getFieldName16()%></td>
                        <td style="text-align: left;" id="t2col18_<%=count%>"><%=saveList.getFieldName17()%></td>
                        <td style="text-align: left;" id="t2col19_<%=count%>"><%=saveList.getFieldName18()%></td>
                        <td style="text-align: left;" id="t2col20_<%=count%>"><%=saveList.getFieldName19()%></td>
                    </tr>
                    <%
                                count++;
                            }
                        }
                    %>
                </table>
                <% if (spsaveCalcALL != null && spsaveCalcALL.size() > 0) { %>
                <div class="inner-tableHead t_space">
                    Summary of Procurements (including National and International)
                </div>
                <table width="100%" cellspacing="0" class="tableList_1" id="resultTable">
                    <tr id="subtableheading">
                        <th rowspan=3 class="t-align-center">Sl. No.</th>
                        <th rowspan=3 class="t-align-center">Name of Agency</th>
                        <th rowspan=3 class="t-align-center">The number of Awarded contracts </th>
                        <th rowspan=3 class="t-align-center">Official Estimated Cost (Nu. In Million)</th>
                        <th rowspan=3 class="t-align-center">Total Value of Contracts, (Nu. In Million)</th>
                        <th rowspan=3 class="t-align-center">Total Amount Saved,  (Nu. In Million)</th>
                        <th rowspan=3 class="t-align-center">Total Savings by Contracts,%</th>
                        <th colspan=12 class="t-align-center">Procurements</th>
                        <th rowspan=3 class="t-align-center">Total No. of Contract Executed</th>
                    </tr>

                    <tr>
                        <th colspan=4>Goods</th>
                        <th colspan=4>works</th>
                        <th colspan=4>services</th>
                    </tr>

                    <tr>
                        <th>Total number of Contracts</th>
                        <th>Total Value of Contracts (Nu. In Million)</th>
                        <th>Total Amount Saved(Nu. In Million)</th>
                        <th>Saving by Contracts, %</th>
                        <th>Total number of Contracts</th>
                        <th>Total Value of Contracts (Nu. In Million)</th>
                        <th>Total Amount Saved (Nu. In Million)</th>
                        <th>Saving by Contracts, %</th>
                        <th>Total number of Contracts</th>
                        <th>Total Value of Contracts (Nu. In Million) </th>
                        <th>Total Amount Saved (Nu. In Million)</th>
                        <th>Total Savings by Contracts, %</th>
                    </tr>

                    <tr>
                        <th>1</th>
                        <th>2</th>
                        <th>3</th>
                        <th>4</th>
                        <th>5</th>
                        <th>6</th>
                        <th>7</th>
                        <th>8</th>
                        <th>9</th>
                        <th>10</th>
                        <th>11</th>
                        <th>12</th>
                        <th>13</th>
                        <th>14</th>
                        <th>15</th>
                        <th>16</th>
                        <th>17</th>
                        <th>18</th>
                        <th>19</th>
                        <th>20</th>
                    </tr>
                    <%

                        int count = 1;
                        for (SPCommonSearchDataMore saveList : spsaveCalcALL) {
                    %>
                    <tr>
                        <td style="text-align: left;" id="t3col1_<%=count%>"><%=count%></td>
                        <td style="text-align: left;" id="t3col2_<%=count%>"><%=saveList.getFieldName1()%></td>
                        <td style="text-align: left;" id="t3col3_<%=count%>"><%=saveList.getFieldName2()%></td>
                        <td style="text-align: left;" id="t3col4_<%=count%>"><%=saveList.getFieldName3()%></td>
                        <td style="text-align: left;" id="t3col5_<%=count%>"><%=saveList.getFieldName4()%></td>
                        <td style="text-align: left;" id="t3col6_<%=count%>"><%=saveList.getFieldName5()%></td>
                        <td style="text-align: left;" id="t3col7_<%=count%>"><%=saveList.getFieldName6()%></td>
                        <td style="text-align: left;" id="t3col8_<%=count%>"><%=saveList.getFieldName7()%></td>
                        <td style="text-align: left;" id="t3col9_<%=count%>"><%=saveList.getFieldName8()%></td>
                        <td style="text-align: left;" id="t3col10_<%=count%>"><%=saveList.getFieldName9()%></td>
                        <td style="text-align: left;" id="t3col11_<%=count%>"><%=saveList.getFieldName10()%></td>
                        <td style="text-align: left;" id="t3col12_<%=count%>"><%=saveList.getFieldName11()%></td>
                        <td style="text-align: left;" id="t3col13_<%=count%>"><%=saveList.getFieldName12()%></td>
                        <td style="text-align: left;" id="t3col14_<%=count%>"><%=saveList.getFieldName13()%></td>
                        <td style="text-align: left;" id="t3col15_<%=count%>"><%=saveList.getFieldName14()%></td>
                        <td style="text-align: left;" id="t3col16_<%=count%>"><%=saveList.getFieldName15()%></td>
                        <td style="text-align: left;" id="t3col17_<%=count%>"><%=saveList.getFieldName16()%></td>
                        <td style="text-align: left;" id="t3col18_<%=count%>"><%=saveList.getFieldName17()%></td>
                        <td style="text-align: left;" id="t3col19_<%=count%>"><%=saveList.getFieldName18()%></td>
                        <td style="text-align: left;" id="t3col20_<%=count%>"><%=saveList.getFieldName19()%></td>
                    </tr>
                    <%
                                count++;
                            }
                        }
                    %>
                </table>
            </div>
            <% }%>
        </div>
        <%if (isNotPDF) {%>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
        <%}%>

    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }

        function loadCPVTree()
        {
            window.open('../resources/common/CPVTree.jsp', 'CPVTree', 'menubar=0,scrollbars=1,width=700px');
        }
        function onSelectionOfTreeDesig() {}
//        function showHide() {}
function showHide()
	{
		if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
			document.getElementById('tblSearchBox').style.display = 'table';
			document.getElementById('collExp').innerHTML = '- Advanced Search';
		}else{
			document.getElementById('tblSearchBox').style.display = 'none';
			document.getElementById('collExp').innerHTML = '+ Advanced Search';
		}
	}
	function hide()
	{
		document.getElementById('tblSearchBox').style.display = 'none';
		document.getElementById('collExp').innerHTML = '+ Advanced Search';
	}
        function checkCondition() {}
        function checkHOPE() {}
        function onSelectionOfTreeOff() {
            $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId: $('#txtdepartmentid').val(), funName: 'Office'}, function (j) {
                $("select#cmboffice").html(j);
                var cmb = document.getElementById('cmboffice');
                $("#offName").val(cmb.options[cmb.selectedIndex].text);
            });
        }
        function setOffName() {
            var cmb = document.getElementById('cmboffice');
            $("#offName").val(cmb.options[cmb.selectedIndex].text);
        }
        function setProcMethodName() {
            var cmb = document.getElementById('cmbProcMethod');
            $("#procMethodName").val(cmb.options[cmb.selectedIndex].text);
        }
        function setBudgetType() {
            var cmb = document.getElementById('cmbBudgetType');
            $("#budgetTypeName").val(cmb.options[cmb.selectedIndex].text);
        }
        $(document).ready(function () {

            $("#print").click(function () {
                //alert('sa');
                printElem({leaveOpen: true, printMode: 'popup'});
            });

        });

        function printElem(options) {
            //alert(options);
            /*$('#print').hide();
             $('#saveASPDF').hide();
             $('#goBack').hide();*/
            $('#print_area').printElement(options);
            /*$('#print').show();
             $('#saveASPDF').show();
             $('#goBack').show();*/
            //$('#trLast').hide();
        }
        function getOfficesForDept() {
            $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId: $('#txtdepartmentid').val(), funName: 'Office'}, function (j) {
                $("select#cmboffice").html(j);
            });
        }
    </script>
</html>
