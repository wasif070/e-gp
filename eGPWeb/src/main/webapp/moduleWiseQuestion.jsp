<%-- 
    Document   : quizModule
    Created on : May 25, 2017, 1:34:03 PM
    Author     : feroz
--%>

<%@page import="com.cptu.egp.eps.model.table.TblQuizAnswer"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuizAnswerService"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuizQuestion"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuizQuestionService"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuestionModule"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuestionModuleService"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Quiz Question</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        
        
    </head>
    <body>
        <%
                    int moduleId = -1;
                    if(request.getParameter("moduleId")!=null)
                    {
                        moduleId = Integer.parseInt(request.getParameter("moduleId"));
                    }
                    QuestionModuleService questionModuleService = (QuestionModuleService) AppContext.getSpringBean("QuestionModuleService");
                    List<TblQuestionModule> tblQuestionModule = questionModuleService.findTblQuestionModule(moduleId);
                    QuizQuestionService quizQuestionService = (QuizQuestionService) AppContext.getSpringBean("QuizQuestionService");
                    List<TblQuizQuestion> tblQuizQuestion = quizQuestionService.findTblQuizQuestion(moduleId);
         %>
         
         
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->  
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                       
                        <td class="contentArea-Blogin"><!--Page Content Start-->   
                         
                            <br><br>
                            <div class="pageHead_1" style="padding-left:0%;">Module Name: <%out.print(tblQuestionModule.get(0).getModuleName());%>
                                <span style="float:right;"><a href="quizModule.jsp" class="action-button-goback">Go Back To Quiz Module</a></span>
                            </div>
                            <br>
                            <table name="resultTable" id="resultTable" style="padding-left: 10%;">
                                <%
                                  int cnt = 0;
                                  int listSize = tblQuizQuestion.size();
                                  QuizAnswerService quizAnswerService = (QuizAnswerService) AppContext.getSpringBean("QuizAnswerService");
                                  List<TblQuizAnswer> tblQuizAnswer = null;
                                  if(tblQuizQuestion.isEmpty())
                                  {
                                      %><b><span style="color: red; padding-left: 42%; font-size:20px;"><%out.print("No data found!");%></span></b><%
                                  }
                                  for(TblQuizQuestion tblQuizQuestionThis : tblQuizQuestion){
                                        tblQuizAnswer = quizAnswerService.findTblQuizAnswer(tblQuizQuestionThis.getQuestionId());
                                        cnt++;
                                %>
                                <tr>
                                    <td>
                                        <span><b><%out.print("("+cnt+"/"+listSize+").  "+tblQuizQuestionThis.getQuestion());%></span></b><br><br>
                                            <%
                                                int i = 0;
                                                for(TblQuizAnswer tblQuizAnswerThis : tblQuizAnswer){%>
                                                <input type="checkbox" name="ans_<%=tblQuizAnswerThis.getAnswerId()%>" id="ans_<%=tblQuizAnswerThis.getAnswerId()%>" value="<%=i%>" onchange="handleChange(<%=tblQuizAnswerThis.getAnswerId()%>,<%=tblQuizAnswerThis.isIsCorrect()%>);">
                                                <span id="ansTxt_<%=tblQuizAnswerThis.getAnswerId()%>"><b><%out.print(tblQuizAnswerThis.getAnswer());%></b></span><br><br>
                                              <%i++;}
                                              if(cnt!=1){
                                                %>
                                                <label class="formBtn_1">
                                                    <input type="submit" name="button" id="btnPrev" value="Previous" onclick="showPrev()"/>
                                                </label>&nbsp;
                                                <%}
                                                if(cnt!=listSize){%>
                                                <label class="formBtn_1">
                                                    <input type="submit" name="button" id="btnNxt" value="Next" onclick="showNext(<%=tblQuizQuestionThis.getQuestionId()%>);"/>
                                                </label><br><br>
                                                <%}%>
                                    </td>
                               </tr>
                                    <%break;}%>
                            </table>
                            
                            
                        </td>
                    </tr>
                </table>

                <script>
                    function handleChange(answerId, isCorrect)
                    {
                       var ansTxt = "ansTxt_"+answerId;
                       var ans = "ans_"+answerId;
                       if(isCorrect)
                       {
                            document.getElementById(ansTxt).style.color = "green";
                       }
                       else
                       {
                           document.getElementById(ansTxt).style.color = "red";
                           document.getElementById(ans).checked = false;
                       }
                       
                    }
                    
                    $(function() {
                            var count;
                            $('#btnSearch').click(function() {
                                     
                            });
                        });
                        var noQuestion = 1;
                    function showNext(next)
                    {
                        noQuestion++;
                        var tableBody = "";
                        <%
                            int totalQuestion = tblQuizQuestion.size();
                            for(TblQuizQuestion tblQuizQuestionThis : tblQuizQuestion){
                                %>
                                        if((next+1)== "<%=tblQuizQuestionThis.getQuestionId()%>")
                                        {
                                            tableBody+="<tr><td><span><b>("+noQuestion+"/"+<%out.print(totalQuestion);%>+").  <%out.print(tblQuizQuestionThis.getQuestion());%></span></b><br><br>";
                                            <%
                                                QuizAnswerService quizAnswerService2 = (QuizAnswerService) AppContext.getSpringBean("QuizAnswerService");
                                                List<TblQuizAnswer> tblQuizAnswer2 = null;
                                                tblQuizAnswer = quizAnswerService.findTblQuizAnswer(tblQuizQuestionThis.getQuestionId());
                                                int i=0;
                                                for(TblQuizAnswer tblQuizAnswerThis : tblQuizAnswer){
                                                    i++;
                                                    %>
                                                      tableBody+="<input type='checkbox' name='ans_<%=tblQuizAnswerThis.getAnswerId()%>' id='ans_<%=tblQuizAnswerThis.getAnswerId()%>' value='<%=i%>' onchange='handleChange(<%=tblQuizAnswerThis.getAnswerId()%>,<%=tblQuizAnswerThis.isIsCorrect()%>);'>&nbsp;<span id='ansTxt_<%=tblQuizAnswerThis.getAnswerId()%>'><b><%out.print(tblQuizAnswerThis.getAnswer());%></b></span><br><br>";
                                                    <%
                                                }
                                                
                                            %>
                                            if(noQuestion != 1)
                                            {
                                                tableBody+= "<label class='formBtn_1'><input type='submit' name='button' id='btnPrev' value='Previous' onclick='showPrev(<%=tblQuizQuestionThis.getQuestionId()%>);'/></label>&nbsp;"
                                            }
                                            if(noQuestion != "<%=totalQuestion%>")
                                            {
                                                tableBody+= "<label class='formBtn_1'><input type='submit' name='button' id='btnNxt' value='Next' onclick='showNext(<%=tblQuizQuestionThis.getQuestionId()%>);'/></label><br><br>"
                                            }
                                            
                                        }
                                <%
                            }
                        %>
                                resultTable.innerHTML = tableBody;
                        
                    }
                    function showPrev(next)
                    {
                        noQuestion--;
                        var tableBody = "";
                        <%
                            int totalQuestion2 = tblQuizQuestion.size();
                            for(TblQuizQuestion tblQuizQuestionThis : tblQuizQuestion){
                                %>
                                        if((next-1)== "<%=tblQuizQuestionThis.getQuestionId()%>")
                                        {
                                            tableBody+="<tr><td><span><b>("+noQuestion+"/"+<%out.print(totalQuestion);%>+").  <%out.print(tblQuizQuestionThis.getQuestion());%></span></b><br><br>";
                                            <%
                                                QuizAnswerService quizAnswerService2 = (QuizAnswerService) AppContext.getSpringBean("QuizAnswerService");
                                                List<TblQuizAnswer> tblQuizAnswer2 = null;
                                                tblQuizAnswer = quizAnswerService.findTblQuizAnswer(tblQuizQuestionThis.getQuestionId());
                                                int i=0;
                                                for(TblQuizAnswer tblQuizAnswerThis : tblQuizAnswer){
                                                    i++;
                                                    %>
                                                      tableBody+="<input type='checkbox' name='ans_<%=tblQuizAnswerThis.getAnswerId()%>' id='ans_<%=tblQuizAnswerThis.getAnswerId()%>' value='<%=i%>' onchange='handleChange(<%=tblQuizAnswerThis.getAnswerId()%>,<%=tblQuizAnswerThis.isIsCorrect()%>);'>&nbsp;<span id='ansTxt_<%=tblQuizAnswerThis.getAnswerId()%>'><b><%out.print(tblQuizAnswerThis.getAnswer());%></b></span><br><br>";
                                                    <%
                                                }
                                                
                                            %>
                                            if(noQuestion != 1)
                                            {
                                                tableBody+= "<label class='formBtn_1'><input type='submit' name='button' id='btnPrev' value='Previous' onclick='showPrev(<%=tblQuizQuestionThis.getQuestionId()%>);'/></label>&nbsp;"
                                            }
                                            if(noQuestion != "<%=totalQuestion%>")
                                            {
                                                tableBody+= "<label class='formBtn_1'><input type='submit' name='button' id='btnNxt' value='Next' onclick='showNext(<%=tblQuizQuestionThis.getQuestionId()%>);'/></label><br><br>"
                                            }
                                            
                                        }
                                <%
                            }
                        %>
                                resultTable.innerHTML = tableBody;
                        
                    }
                    
                </script>  
                
                        
                    
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
