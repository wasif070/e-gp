<%@page import="net.tanesha.recaptcha.ReCaptchaFactory"%>
<%@page import="net.tanesha.recaptcha.ReCaptcha"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%
            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            Object objUName = session.getAttribute("userName");
            boolean isLoggedIn = false;
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userTypeId").toString();
            }
            if (objUName != null) {
                isLoggedIn = true;
            }
            if ("8".equals(strUserTypeId)) {
                response.sendRedirect("ViewAllTopic.jsp");
            }
            /*if("2".equals(strUserTypeId) || "3".equals(strUserTypeId)){
            isLoggedIn = true;
            }else{
            response.sendRedirect("ViewAllTopic.jsp");
            }*/

%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Post Topic</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Post Issue</title>
        <link href="resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="resources/js/form/CommonValidation.js"></script>
        <!--
        <script src="ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script type="text/javascript">
            $(function(){
                $('#txtsubject').change(function() {
                    /*   alert($("#txtsubject").val().length);
                            if($('#txtsubject').val()==''){
                                $('span.#subjectmsg').css("color","red");
                                $('span.#subjectmsg').html("Please enter Topic");
                            }
                            else if($('#txtsubject').val().trim()==''){
                                $('span.#subjectmsg').css("color","red");
                                $('span.#subjectmsg').html("Only Space is not allowed");
                            }
                            else if($("#txtsubject").val().length > 100){
                                $('span.#subjectmsg').css("color","red");
                                $('span.#subjectmsg').html("Maximum 100 characters are allowed");
                            }
                            else
                            {
                                $('span.#subjectmsg').css("color","green");
                                $('span.#subjectmsg').html("OK");
                            }*/
                    $(".err").remove();
                });

                $('#topicdetail').change(function() {
                    /*  if($('#topicdetail').val().trim()==""){
                                $('span.#detailmsg').css("color","red");
                                $('span.#detailmsg').html("Please enter Detail");
                            }
                            else if($("#topicdetail").val().length > 2000){
                                $('span.#detailmsg').css("color","red");
                                $('span.#detailmsg').html("Maximum 2000 characters are allowed");
                            }
                            else
                            {
                                $('span.#detailmsg').css("color","green");
                                $('span.#detailmsg').html("OK");
                            }*/
                    $(".err").remove();
                });
            });

            $(function() {
                $('#recaptcha_response_field').blur(function() {
            if($('#recaptcha_response_field').val()!=""){
                        $.post("<%=request.getContextPath()%>/CommonServlet", {challenge:$('#recaptcha_challenge_field').val(),userInput:$('#recaptcha_response_field').val(),funName:'captchaValid'},
                        function(j){
                            if(j.toString()=="OK"){
                                $('span.#vericode').css("color","green");
                            }
                            else{
                                javascript:Recaptcha.reload();
                                $('span.#vericode').css("color","red");
                            }
                            $('span.#vericode').html(j);
                        });
                    }else{
                        $('span.#vericode').css("color","red");
                        $('#vericode').html("Please enter Verification Code");
                    }
                });
            });
            function validate()
            {
                //  alert("In validate");
                /*  if($('#txtsubject').val()==''){
                                $('span.#subjectmsg').css("color","red");
                                $('span.#subjectmsg').html("Please enter Topic");
                                return false;
                        }
                        if($('#txtsubject').val().trim()==''){
                                $('span.#subjectmsg').css("color","red");
                                $('span.#subjectmsg').html("Only Space is not allowed");
                                return false;
                        }
                        if($("#txtsubject").val().length > 100){
                                $('span.#detailmsg').css("color","red");
                                $('span.#detailmsg').html("Maximum 100 characters are allowed");
                        }
                        if($('#topicdetail').val().trim()==""){
                                $('span.#detailmsg').css("color","red");
                                $('span.#detailmsg').html("Please enter Detail");
                                return false;
                        }
                        if($("#topicdetail").val().length > 2000){
                                $('span.#detailmsg').css("color","red");
                                $('span.#detailmsg').html("Maximum 2000 characters are allowed");
                        }
                        $('span.#subjectmsg').css("color","green");
                        $('span.#detailmsg').css("color","green");
                        $('span.#subjectmsg').html("OK");
                        $('span.#detailmsg').html("OK");
                        document.frmPublicProcForum.action="PostTopic.jsp";
                        document.frmPublicProcForum.submit();  */


                $(".err").remove();

                var valid = true;
                //alert("value: "+$("#txtsubject").val().length);
                if($("#txtsubject").val().length == 0) {
                    $("#txtsubject").parent().append("<div class='err' style='color:red;'>Please enter Topic</div>");
                    valid = false;
                }
                if($.trim($("#txtuname").val()) == '') {
                    $("#txtuname").parent().append("<div class='err' style='color:red;'>Please enter User's Name</div>");
                    valid = false;
                }
                if($('#txtsubject').val().length !=0 && $.trim($('#txtsubject').val()).length == 0){
                    $("#txtsubject").parent().append("<div class='err' style='color:red;'>Only space is not allowed</div>");
                    valid = false;
                }
                if($("#txtsubject").val().length > 100){
                    $("#txtsubject").parent().append("<div class='err' style='color:red;'>Maximum 100 characters are allowed</div>");
                    valid = false;
                }
                if(isCKEditorFieldBlank($.trim(CKEDITOR.instances.topicdetail.getData().replace(/<[^>]*>|\s/g, '')))){
                    $("#topicdetail").parent().append("<div class='err' style='color:red;'>Please enter Detail</div>");
                    CKEDITOR.instances.topicdetail.setData("");
                    valid = false;
                }
                else if($.trim(CKEDITOR.instances.topicdetail.getData().replace(/<[^>]*>|\s/g, '')).length > 2000) {
                    $("#topicdetail").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters are allowed</div>");
                    valid = false;
                }
                if($('#recaptcha_response_field').val() != undefined){
                    if($('#recaptcha_response_field').val()==""){
                        $('span.#vericode').css("color","red");
                        $('#vericode').html("Please enter Verification Code");
                        valid =  false;
                    }else{
                        if($('#vericode').html()!="OK"){
                            javascript:Recaptcha.reload();
                            valid =  false;
                        }
                    }
                }
                if(!valid){
                    return false;
                }
            }
        </script>
    </head>



    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%if (isLoggedIn) {%>
                <%@include file="resources/common/AfterLoginTop.jsp" %>
                <%} else {%>
                <jsp:include page="resources/common/Top.jsp"/>
                <%}%>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1"> Post Topic
                    <%if (strUserTypeId.equalsIgnoreCase("8")) {%>
                    <span class="c-alignment-right"><a href="ViewAllTopic.jsp" class="action-button-goback">Go Back</a></span> </div>
                    <%} else {%>
                <span class="c-alignment-right"><a href="procForum.jsp?verificationRequired=no" class="action-button-goback">Go Back</a></span> </div>
                <%}%>
            <form id="frmPublicProcForum" name="frmPublicProcForum" method="post" action="<%=request.getContextPath()%>/PublicProcForumServlet">
                <div style="font-style: italic" class="formStyle_1 ff t_space">Fields marked with (<span class="mandatory">*</span>) are mandatory</div>
                <table width="100%" cellspacing="5" class="formStyle_1">
                    <tr>
                        <td width="10%" class="ff">User's Name:<span class="mendatory" >*</span></td>
                        <td width="90%">
                            <input type="text" name="txtuname" class="formTxtBox_1" id="txtuname" style="width:200px;" maxlength="100"  value="<%=session.getAttribute("userId") != null ? session.getAttribute("userName") : ""%>" <%=session.getAttribute("userId") != null ? " readonly " : ""%> />
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Topic :<span class="mendatory" >*</span></td>
                        <td><input type="text" name="txtsubject" class="formTxtBox_1" id="txtsubject" style="width:200px;" maxlength="100" />
                        </td>

                    </tr>
                    <tr>
                        <td class="ff"></td>
                        <td class="ff"></td>
                    </tr>
                    <tr>
                        <td class="ff">Detail :<span class="mendatory" >*</span></td>
                        <td>
                            <textarea rows="5" id="topicdetail" name="topicdetail" class="formTxtBox_1" style="width:60%;" maxlength="2000"></textarea>
                            <script type="text/javascript">
                                CKEDITOR.replace( 'topicdetail',
                                {
                                    fullPage : false
                                });
                            </script>
                        </td>
                    </tr>
                    <%if (session.getAttribute("userId") == null) {%>
                    <tr>
                        <td class="ff">Verification Code : <span>*</span></td>
                        <td>
                            <%
                                String pubKey = "6Lfw4r0SAAAAAO2WEc7snoFlSixnCQ2IGaO7k_iO";
                                String privKey = "6Lfw4r0SAAAAAN_Pr3YGPA39kRIyNsuMCDJIpUsS";
                                StringBuffer reqURL = request.getRequestURL();
                                if (reqURL.toString().substring(reqURL.toString().indexOf("//") + 2, reqURL.toString().lastIndexOf("/")).equals("www.eprocure.gov.bd")) {
                                    pubKey = "6Lc0ssISAAAAAKh369J1vrGKmSfpEGhbr07sl1gS";
                                    privKey = "6Lc0ssISAAAAAJf6jPcfSBElxYSRk1LB46MBsWUk";
                                }
                                ReCaptcha c = ReCaptchaFactory.newReCaptcha(pubKey, privKey, false);
                                out.print(c.createRecaptchaHtml(null, null));
                                pubKey = null;
                                privKey = null;
                                reqURL = null;
                            %>
                            <span class="formNoteTxt">(If you cannot read the text, you may get new Verification Code by clicking <img alt="refresh"  src="resources/images/refresh.gif"/>, and if you want to hear the Verification Code, please click <img alt="sound"  src="resources/images/sound.gif"/> )</span>
                            <span class="reqF_1" id="vericode"></span>
                        </td>
                    </tr>
                    <%}%>                    
                    <tr>
                        <td class="ff">&nbsp;</td>
                        <td><label class="formBtn_1"><input name="button" type="submit" id="button" value="Submit" onclick="return validate()"/></label></td>
                    </tr>

                    <tr>
                        <td colspan="2" class="t-align-left">
                            <font color="red"><strong>Important Note:</strong> Please note, posting of any issue which is not related to Procurement or use of abusive language can lead to debarment & deactivation of your account with or without penalty.</font>
                        </td>
                    </tr>
                </table>
                <input type="hidden" value="postTopic" name="action" id="action" />

            </form>

            <div>&nbsp;</div>

            <!--Dashboard Content Part End-->

        </div>
        <!--Dashboard Footer Start-->
        <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
        <!--Dashboard Footer End-->
    </body>
</html>
