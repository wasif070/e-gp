<%-- 
    Document   : promiseimporttest
    Created on : Feb 10, 2012, 11:43:34 AM
    Author     : shreyansh.shah
--%>

<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2> Click on Button to get data for promise report </h2>
        <%if("Insert".equals(request.getParameter("Submit"))) {
            CommonSearchDataMoreService spimport = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
            spimport.geteGPDataMore("forimportpromisedata");
        }%>
        <form name="p" action="">
            <input type="Submit" name="Submit" value="Insert"/>
        </form>

    </body>
</html>
