<%-- 
    Document   : Individual Report
    Created on : Nov 30, 2010, 6:11:30 PM
    Author     : Kinjal Shah
    Purpose    : Genrate report based on userID
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPReportCommonData" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
 <head>
             <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        int tenderId = 0;
        boolean bol_isPdf = true;
        boolean bol_forEvalTend = true;
        if(request.getParameter("isPDF")!=null && "true".equalsIgnoreCase(request.getParameter("isPDF"))){
            bol_isPdf = false;
        }
        if("true".equalsIgnoreCase(request.getParameter("evalTenderer"))){
            bol_forEvalTend = false;
        }
        String uuId = request.getParameter("uuId");
         if(request.getParameter("tenderId")!=null)
                tenderId = Integer.parseInt(request.getParameter("tenderId").toString());
        String tId = "";
        String fId = "";
                    if ((!request.getParameter("tenderId").equals("")) && (!request.getParameter("formId").equals(""))) {
                        tId = request.getParameter("tenderId");
                        fId = request.getParameter("formId");
                    }
        
        // Coad added by Dipal for Audit Trail Log.
        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
        String idType="tenderId";
        int auditId=tenderId;
        String auditAction="Individual report viewed by Opening Committee Member";
        String moduleName=EgpModule.Tender_Opening.getName();
        String remarks="User Id: "+session.getAttribute("userId")+" has viewed individual report for Form id: "+fId+" of Tender Id: "+tenderId;
        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    
        %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Individual Report</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<!--
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
-->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>

        <script type="text/javascript">
                    $(document).ready(function() {
                                $("#print").click(function() {
                                    $("a[view='link']").each(function(){
                                        var temp = $(this).html().toString();
                                        if(temp=="(Sub Contractor/Consultant - View Details)"){
                                            $(this).hide();
                                            $(this).parent().append("<div class='reqF_1'>(Sub Contracting/Consultancy)</div>");
                                        }else if(temp=="(JVCA - View Details)"){
                                            $(this).hide();
                                            $(this).parent().append("<div class='reqF_1'>(JVCA)</div>");
                                        }else{
                                            $(this).parent().append("<span class='disp_link'>"+temp+"</span>");
                                            $(this).hide();
                                            $('.reqF_1').remove();
                                        }                                      
                                    })
                                    $("th[view='print']").each(function(){
                                            $(this).hide();
                                    })
                                    $("td[view='print']").each(function(){
                                            $(this).hide();
                                    })

                                    printElem({ leaveOpen: true, printMode: 'popup' });
                                    $("a[view='link']").each(function(){
                                        var temp = $(this).html().toString();
                                        if(temp=="(Sub Contractor/Consultant - View Details)"){
                                            $(this).show();
                                        }else if(temp=="(JVCA - View Details)"){
                                            $(this).show();
                                        }else{
                                            $('.disp_link').remove();
                                            $('.reqF_1').remove();
                                            $(this).show();
                                        }
                                    })
                                    $("th[view='print']").each(function(){
                                            $(this).show();
                                    })
                                    $("td[view='print']").each(function(){
                                            $(this).show();
                                    })
                                });

                            });
                            function printElem(options){
                                $('#print_area').printElement(options);
                            }
        </script>
</head>
    <body>
       
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
      <%if(bol_isPdf){%>
       <span style="float:right;">
            <%

            int userTypeid = 0;
            
            if(session.getAttribute("userTypeId")!=null){
                userTypeid = Integer.parseInt(session.getAttribute("userTypeId").toString());
            }
           
            if(bol_forEvalTend){
            if(userTypeid==2){
          %>
                <a href="<%=request.getContextPath()%>/tenderer/TendererDashboard.jsp?tenderid=<%=tenderId%>" class="action-button-goback">Go Back To Dashboard</a>
          <% }else{ %>
          <a href="<%if("eval".equals(request.getParameter("frm"))){out.print(request.getHeader("referer"));}else{%><%=request.getContextPath()%>/officer/OpenComm.jsp?tenderid=<%=tenderId%>&lotId=<%=request.getParameter("lotId")%><%}%>" class="action-button-goback">Go Back To Dashboard</a>
          <% } %>
          <%}%>
       </span>
       <%}%>
  
      
      <div  id="print_area">
      <div class="pageHead_1" style="margin-top: 15px;">Individual Report
          
      </div>
          <%
                    pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                    pageContext.setAttribute("isPDF", request.getParameter("isPDF"));
                    pageContext.setAttribute("userId", 0);

          %>
       
   <%
                    List<Object[]> list_lorPkg = new ArrayList<Object[]>();                    
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    list_lorPkg = commonService.getPkgDetialByTenderId(tenderId);                                                            
   %>  
   <table width="100%" cellspacing="0" class="tableList_1 t_space">
       <tr>
            <th width="25%" class="t-align-left ff">Package No</th>
            <th width="75%" class="t-align-left ff">Package Description</th>
        </tr>
        <tr >
            <td class="t-align-center"><%=list_lorPkg.get(0)[0]%></td>
            <td class="t-align-left"><%=list_lorPkg.get(0)[1]%></td>
        </tr>
   </table>
   <% Boolean isTenPackageWise= false;
   TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
   for (SPTenderCommonData sptcd : tenderCommonService.returndata("tenderinfobar", String.valueOf(tenderId), null)) {
                if ("Package".equalsIgnoreCase(sptcd.getFieldName7())) {      isTenPackageWise = true; }}
      if(!isTenPackageWise){
          list_lorPkg = commonService.getLotDetiasByFormId(Integer.parseInt(fId));
   %>
   <table width="100%" cellspacing="0" class="tableList_1 t_space">
       <tr>
            <th width="25%" class="t-align-left ff">Lot No</th>
            <th width="75%" class="t-align-left ff">Lot Description</th>
        </tr>
        <tr>
            <td class="t-align-center"><%=list_lorPkg.get(0)[0]%></td>
            <td class="t-align-left"><%=list_lorPkg.get(0)[1]%></td>
        </tr>
   </table>
    <%}%>
     <%    
         ReportCommonService reportCommonService = (ReportCommonService) AppContext.getSpringBean("ReportCommonService");
         List<SPTenderCommonData> list =  tenderCommonService.returndata("getUserIdForReport",tId,fId);
         if (!list.isEmpty()) {
         int formCnt=1;
         for( SPTenderCommonData sptcd : list){
             if(uuId!=null && !uuId.equals(sptcd.getFieldName1())){
                continue;
             }
            // out.println(Integer.parseInt(sptcd.getFieldName1()));             
             List<SPReportCommonData> reportList = reportCommonService.returndata(Integer.parseInt(fId), Integer.parseInt(sptcd.getFieldName1()));
             //List<SPReportCommonData> reportList = reportCommonService.returndata(19, 7);
          if (!reportList.isEmpty()) {
            for(SPReportCommonData sprcd : reportList){
                out.println(sprcd.getReportHtml().replaceAll("\n","<br/>"));
            }
        }%>
        <script type="text/javascript">
                //Below script is used as Div is generated in sp with all same ids
                //so to dispaly it counter wise id's are changed
                $('#spanMsg').attr('id', 'spanMsg_<%=formCnt%>');
        </script>
        <%CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
        List<SPCommonSearchData> DocList = commonSearchService.searchData("getTenderDocForIndi", tId, fId,sptcd.getFieldName1(), null, null, null, null, null, null) ;
        if (!DocList.isEmpty()) {
            int cnt =1;%>            
    <table width="100%" cellspacing="0" class="tableList_1">
                            <tr>
                                <th width="4%" class="ff">Sl. No.</th>
                                <th width="32%" class="ff">Mapped Document Name</th>
                                <th width="38%" class="ff">File Name</th>
                                <th width="15%" class="ff">File Size(In Kb)</th>
<!--                                <th width="45%" class="t-align-left ff">Description</th>-->
                                <%if(bol_isPdf){%>
                                <th width="10%" class="ff" view='print'>Action</th>
                                <%}%>
                            </tr>
   <%for (SPCommonSearchData mapedDL : DocList){  %>
   <tr>
       <td class="t-align-center"> <%=cnt%></td>
       <td class="t-align-center"><%=mapedDL.getFieldName1()%></td>
       <td class="t-align-center"> <%=mapedDL.getFieldName2()%></td>
       <td class="t-align-center" > <%=new BigDecimal(Double.parseDouble((mapedDL.getFieldName3()))/1024).setScale(2, 0)%></td>
<!--       <td class="t-align-left ff"> < %=mapedDL.getFieldName4()%></td>-->
<%if(bol_isPdf){%>
       <td class="t-align-center" view='print'><a class="action-button-download" href="<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=download&fileName=<%=mapedDL.getFieldName2()%>&fileLen=<%=mapedDL.getFieldName3()%>&docUid=<%=mapedDL.getFieldName5()%>&isInd=y">Download</a></td>
       <%}%>
  </tr> 
  <%cnt++;}%>
   </table>
   <div id="docspanMsg_<%=formCnt%>" style="color: red;"></div>
   <script type="text/javascript">
        //Below script is used when documents is available as div is generated in sp with all same ids
        //and was displaying after forms so to dispaly it after document list script is written
        $('#docspanMsg_<%=formCnt%>').html($('#spanMsg_<%=formCnt%>').html());
        $('#spanMsg_<%=formCnt%>').html(null);
   </script>
  <%}%>

             <%formCnt++;}}else {  out.print("<div width='100%' align='center'><font style='color:red;'><b>Final Submission Pending / Form is not filled <b></font></div>");;};%>
  
  </div>
  </div>
  <!--Dashboard Content Part End-->

</div>
    </body>
    <%if(!bol_isPdf){%>
    <script type="text/javascript">        
        $("a").each(function(){  
            var temp = $(this).html().toString();
            if(temp=="(Sub Contractor/Consultant - View Details)"){
                $(this).hide();
                $(this).parent().append("<div class='reqF_1'>(Sub Contracting/Consultancy)</div>");
            }else if(temp=="(JVCA - View Details)"){
                $(this).hide();
                $(this).parent().append("<div class='reqF_1'>(JVCA)</div>");
            }else{
                $(this).parent().append(temp);
                $(this).hide();                
            }                                      
        })
     </script>
    <%}%>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabTender");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

    </script>
</html>
