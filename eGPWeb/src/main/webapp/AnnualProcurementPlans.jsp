<%--
    Document   : Tenders
    Created on : Mar 21, 2011, 5:30:28 PM
    Author     : Administrator
--%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Reports</title>
<script type="text/javascript" src="resources/js/ddlevelsmenu.js"></script>
<link href="resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
</head>
<body>
<div class="mainDiv">
    <div class="fixDiv">
         <jsp:include page="resources/common/Top.jsp" ></jsp:include>
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top">
                <td width="266">
                    <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                </td>
                <td class="contentArea-Blogin">
                      <div class="DashboardContainer">
                    <div class="pageHead_1">Reports</div>
                        <ul class="tabPanel_1 t_space">
                           <li><a href="RegistrationDetails.jsp">Registration Details</a></li>
                           <li><a href="AnnualProcurementPlans.jsp" class="sMenu">Annual Procurement Plans </a></li>
                           <li><a href="Tenders.jsp">Tenders/Proposals</a></li>
                          <!-- Edit By Palash, Dohatec-->
                           <li><a href="ReportDownload.jsp">Performance Indicators Report</a></li>
                          <!-- End-->
                        </ul>
                        <div class="tabPanelArea_1">
                      <%
                        // Get Report Data
                        CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                        List<SPCommonSearchDataMore> reportData = commonSearchDataMoreService.getCommonSearchData("GetAppCount", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                      %>
                            <table width="100%" cellspacing="0" class="tableList_3">
                            <tr>
                              <th width="33%">Total No. of Annual Procurement Plans under Preparation</th>
                              <th width="33%">Total No. of Annual Procurement Plans approved</th>
                              <th width="33">Total</th>
                            </tr>
                            <tr>
                              <td class="t-align-center"><%=reportData.get(0).getFieldName1()%></td>
                              <td class="t-align-center"><%=reportData.get(0).getFieldName2()%></td>
                              <td class="t-align-center">
                              <%
                                    long total= 0 ;
                                    if(reportData.get(0).getFieldName1() !=null && !reportData.get(0).getFieldName1().trim().equalsIgnoreCase(""))
                                        total = total + Long.parseLong(reportData.get(0).getFieldName1());
                                    if(reportData.get(0).getFieldName2() !=null && !reportData.get(0).getFieldName2().trim().equalsIgnoreCase(""))
                                        total = total + Long.parseLong(reportData.get(0).getFieldName2());
                                    out.println(total); 
                              %>
                              </td>
                            </tr>
                          </table>
                        </div>
                      </div>
                </td>
            </tr>
         </table>
         <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
    </div>
</div>
</body>
</html>
