<%@page import="com.cptu.egp.eps.model.table.TblComplaintMaster"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.*,java.text.*" %>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html >
     <%
	response.setHeader("Cache-Control","no-cache");
	response.setHeader("Cache-Control","no-store"); 
	response.setHeader("Pragma","no-cache");
    %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>complaint-management system</title>
<link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../css/home.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

<SCRIPT TYPE="text/javascript">
<!--
function popup(mylink, windowname)
{
if (! window.focus)return true;
var href;
if (typeof(mylink) == 'string')
   href=mylink;
else
   href=mylink.href;
window.open(href, windowname, 'width=800,height=600,scrollbars=yes');
return false;
}
//-->
</SCRIPT>



</head> 
<body onload="getQueryData('getmyprebidquery');"> 
    
                 
                 
                 <div class="dashboard_div">
  <!--Dashboard Header Start-->
   <%@include file="../resources/common/AfterLoginTop.jsp"%>
  <!--Dashboard Header End-->

  <!--Dashboard Content Part Start-->
  
  
   <%
               // Variable tenderId is defined by u on ur current page.
    String c_tenderid = ""; 
    if (request.getParameter("tenderid") != null && !"null".equalsIgnoreCase(request.getParameter("tenderid"))) {
                c_tenderid = request.getParameter("tenderid");
    } else if (request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId"))) {
                c_tenderid = request.getParameter("tenderId");
    }
	pageContext.setAttribute("tenderId",c_tenderid); 
  %>
        
  <div class="pageHead_1">View Complaint Details

                      <%
                                String role = "";
                                if (session.getAttribute("procurementRole") != null) {
                                    String ck[] = session.getAttribute("procurementRole").toString().split(",");
                                    String link = "";
                                    for (int ii = 0; ii < ck.length; ii++) {
                  if (ck[ii].equalsIgnoreCase("PE")) {
                      link = "complaintOfficer.htm?tenderId=" + c_tenderid;
                  } else if (ck[ii].equalsIgnoreCase("HOPE") || ck[ii].equalsIgnoreCase("Secretary")) {
                      link = "complaintOfficerHope.htm";
                      role = "hope";
                  }
              }%>
              <span class="c-alignment-right"> <a class="action-button-goback" href="<%=link%>"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a></span>
                                              <%} else {
                                              if(userTypeId==2){
%>
                    <span class="c-alignment-right">
                        <a class="action-button-goback"	href="../tenderer/complaint.htm?tenderId=<%=c_tenderid%>"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a>
                    </span>
                    <%                            }else if(userTypeId==16){%>
                     <span class="c-alignment-right">
                        <a class="action-button-goback"	href="ViewComplaintsCPTU.htm?tenderId=<%=c_tenderid%>"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a>
                    </span>
                    <%}else{%>
                    <span class="c-alignment-right">
                        <a class="action-button-goback"	href="TendererDashboard.htm?tenderId=<%=c_tenderid%>"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a>
                    </span>
                    <%}
                                              }
                    %>
  </div>
             <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
            <%
            if(!session.getAttribute("userTypeId").toString().equals("17")){
if(session.getAttribute("userTypeId").toString().equals("2")){%>
 <% pageContext.setAttribute("tab", 11);%>
  <%@include file="../tenderer/TendererTabPanel.jsp" %>
  <%}else {
   if(!session.getAttribute("userTypeId").toString().equals("16") && "".equals(role)){
  %>
  <% pageContext.setAttribute("tab", 16);%>
  <%@include file="../officer/officerTabPanel.jsp" %>
  <%}}}%>
            <div class="tabPanelArea_1">
                <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_1 tableView_1" style="table-layout: fixed">

				 <c:set var="creationDate" scope="session" value="${complaint.complaintCreationDt}"/>      
		       
           
		    <%
                    
                        TblComplaintMaster abc = (TblComplaintMaster) request.getAttribute("complaint");
                        Date cDate = (Date) session.getAttribute("creationDate");
                        SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
          
		   %>
                    <tr>
                        <td width="194" class="ff" valign="top"><fmt:message key="LBL_NAME_OF_TENDERER_CONSULTANT"/> : </td>
                        <td width="709"><label id="lblNameofTenderer">

                                <c:choose>
                                    <c:when test="${complaint.tendererMaster.tblCompanyMaster.companyId==1}">
                                        ${complaint.tendererMaster.firstName} ${complaint.tendererMaster.lastName}
                                    </c:when>
                                    <c:otherwise>
                                        ${complaint.tendererMaster.tblCompanyMaster.companyName}
                                    </c:otherwise>
                                </c:choose>
                                    </label>
                            </td>
                    </tr>
                    <tr>
                        <td width="194" class="ff" valign="top"><fmt:message key="LBL_COMPLAINT_DATE_TIME"/> : </td>
                        <td width="751"><label id="lblComplaintDateTime"><%=DateUtils.gridDateToStrWithoutSec(cDate)%></label></td>
                    </tr>
                    <tr>
                        <td width="194" class="ff" valign="top"><fmt:message key="LBL_COMPLAINT_FOR"/> : </td>
                        <td width="751"><label id="lblComplaintFor">${complaint.complaintType.complaintType}</label></td>
                    </tr>
                    <tr>
                        <td width="194" class="ff" valign="top"><fmt:message key="LBL_COMPLAINT_SUBJECT"/> : </td>
                        <td width="751" style="word-wrap:break-word;"><label id="lblComplaintDateTime">${complaint.complaintSubject }</label></td>
                    </tr>
                    <tr>
                        <td width="194" class="ff" valign="top"><fmt:message key="LBL_COMPLAINT_DETAIL"/> : </td>
                        <td width="751" style="word-wrap:break-word;"><label id="lblComplaintDetail">${complaint.complaintDetails}</label></td>
                    </tr>
                    <tr>
                        <td width="194" class="ff" valign="top"><fmt:message key="LBL_REFERENCE_DOCUMENTS"/> :</td>
                        <td width="751"><table width="100%" cellspacing="0" class="tableList_3">
                                <tr>
                                    <th width="7%"><fmt:message key="LBL_sNO"/></th>
                                    <th width="20%"><fmt:message key="LBL_FILE_NAME"/></th>
                                    <th width="33%"><fmt:message key="LBL_DESCRIPTION"/></th>
                                    <th width="20%"><fmt:message key="LBL_UPLOAD_BY"/></th>
                                    <th width="11%"><fmt:message key="LBL_SIZE"/></th>
                                    <th width="9%"><fmt:message key="LBL_ACTION"/></th>
                                </tr>

								 <c:if test="${empty  docList}">
  <tr> <td colspan="6"><fmt:message key="LBL_NO_REFERENCE_DOCUMENTS_ARE_FOUND"/></td></tr>
 </c:if>
 <c:if test="${docList!=null}">
     <c:set var="i" value="1"/>
        <c:forEach  items="${docList}" var="doc">
                         <c:set value="Test" var="uploadby"/>
                        <c:choose>
                            <c:when test="${doc.userRole.userTypeId == 2}">
                                <c:set value="Tenderer" var="uploadby"/>
                            </c:when>
                            <c:when test="${doc.complaintLevel.complaintLevelId == 1}">
                                <c:set value="PE" var="uploadby"/>
                            </c:when>
                            <c:when test="${doc.complaintLevel.complaintLevelId == 2}">
                                <c:set value="HOPE" var="uploadby"/>
                            </c:when>
                            <c:when test="${doc.complaintLevel.complaintLevelId == 3}">
                                <c:set value="Secretary" var="uploadby"/>
                            </c:when>
                            <c:when test="${doc.complaintLevel.complaintLevelId == 4}">
                                <c:set value="Review Panel" var="uploadby"/>
                            </c:when>
                        </c:choose>
                                <tr>
                                   
                                    <td class="t-align-center">${i}</td>
                                     <c:set var="i" value="${i + 1}"/>
                                    <td>${doc.docName}</td>
                                    <td class="table-description"> ${doc.docDescription} </td>
                                    <td><c:out value="${uploadby}"></c:out> </td>
                                    <td class="t-align-center"><fmt:formatNumber type="number" maxFractionDigits="2" value="${doc.docSize/1024}"/></td>
                                    <td><a href="download.htm?docname=${doc.docName}&tenderId=<%=c_tenderid%>&complaintId=${doc.complaintMaster.complaintId}">Download</a></td>
                                </tr>

</c:forEach>
</c:if>
                            </table></td>
                    </tr>
                </table>
                <table width="100%" cellspacing="0" class="tableList_3 t_space">
                    <tr>
                        <th width="44"><fmt:message key="LBL_sNO"/></th>
                        <th width="69"><fmt:message key="LBL_LEVEL"/></th>
                        <th width="246">View History</th>
                        <th width="81"><fmt:message key="LBL_RESPONSE"/></th>
                        <th width="128"><fmt:message key="LBL_RESPONSE_DATE_TIME"/></th>
                   
                        
                    </tr>
	<c:set var="i" value="1"/>

	<c:forEach  items="${historyList}" var="history">
                <tr>
                        <td>${i}</td> 
                        <c:set var="i" value="${i+1}"/>
                        <td>${history.level}</td>

<c:choose>
    <c:when test="${false && history.tendererComments==' '}">
        <td style="text-align:center;">--</td>
    </c:when>
    <c:otherwise>
                        <td class="table-description"><a href="link" onClick="return popup('ViewHistory_tenderer.htm?complaintId=${history.complaintId}&complaintLevelId=${history.levelId}&tenderId=<%=c_tenderid%>', 'tendererpopup')">View history</a></td>
 </c:otherwise>
</c:choose>

 

              
                     
                      
 <c:set var="status" scope="session" value="${history.response}"/>
  <c:set var="datecomplaint" scope="session" value="${history.responseDate}"/>     
<% 
    int escalate=Integer.parseInt(XMLReader.getMessage("DaysforEscalation"));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dt = session.getAttribute("datecomplaint").toString(); // Start date
		
		Calendar c = Calendar.getInstance();
		Calendar c1 = Calendar.getInstance();
		
		c.setTime(sdf.parse(dt)); 
		
		c.add(Calendar.DATE,escalate);

if(c1.after(c)&& (session.getAttribute("status").toString().equals("Pending")) ){


 %>
     <td>No Response</td>
	 <%}else{%>
     <td>${history.response} &nbsp;
         <c:if test="${not empty history.ResolvedStatus}">
             <c:choose>
                 <c:when test="${history.ResolvedStatus == 'Yes'}">
                     (Resolved satisfactory)
                 </c:when>
                 <c:otherwise>
                    (Not Resolved)
                 </c:otherwise>
             </c:choose>
         </c:if>
     </td>
	 <%}%>





						
						
					<%
		       Date strDate = (Date) session.getAttribute("datecomplaint");
          		   %>
						  <td><%=DateUtils.gridDateToStrWithoutSec(strDate)%>
						</td> 
                        
                </tr> 
	</c:forEach> 
</table>
            </div>
            
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@ include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
