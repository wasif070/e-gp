<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd"> 
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.Calendar" %>
<html >
     <%
	response.setHeader("Cache-Control","no-cache");
	response.setHeader("Cache-Control","no-store");
	response.setHeader("Pragma","no-cache");
    %>    
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>complaint-management system</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />


<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>


<script type="text/javascript">
 function  validateForm(){ 
	  var uploadstatusvalue;
            		var radios = document.complaintsubmit.elements["uploadstatus"];
                      for (var i=0; i <radios.length; i++) {
		              if (radios[i].checked) {
		            	  uploadstatusvalue=radios[i].value; 
		            	  
		              
		              }
		             }
       
	     if(document.getElementById("complaintType").value==""){
             alert("Please select complaintType  "); 
			   document.getElementById("complaintType").focus();
		       return false;		  
		 }
	    else if(document.getElementById("txtaComplaintBrief").value==""){
           alert("Please enter Complaint Subject ");
           document.getElementById("txtaComplaintBrief").focus();
		   return false;
		 }

	    
		 else if(document.getElementById("txtaComplaintDetail").value==""){
               alert("Please enter Complaint Details "); 
			   document.getElementById("txtaComplaintDetail").focus();
		       return false;		  
		 }

		 else{

		  document.complaintsubmit.action="submitComplaint.htm?tenderId=<%=request.getParameter("tenderId")%>&uploadstatus="+uploadstatusvalue;
		  document.complaintsubmit.submit();		
		  return true;
		 }

 }


</script>

<script language="javascript" type="text/javascript">

function textCounter( field, countfield, maxlimit ) {
  if ( field.value.length > maxlimit )
  {
    field.value = field.value.substring( 0, maxlimit );
    alert( 'Textarea value can only be '+maxlimit+' characters in length.' );
    return false;
  }
  else
  {
    countfield.value = maxlimit - field.value.length;
  }
}

</script>




</head>
<body onload="getQueryData('getmyprebidquery');">
    <div class="mainDiv">
            <div class="fixDiv">
                 <%@include file="../resources/common/AfterLoginTop.jsp"%>
                 
                 <div class="dashboard_div">
  <!--Dashboard Header Start-->
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
  <div class="pageHead_1">Register Complaint
      <span class="c-alignment-right"><a class="action-button-goback" href="complaint.htm?tenderId=<%=request.getParameter("tenderId")%>"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD" /></a></span>
  </div>
  
	 <%
	    pageContext.setAttribute("tenderId",request.getParameter("tenderId"));
       
		
		%>
     <%@include file="../resources/common/TenderInfoBar.jsp" %>
      <div>&nbsp;</div>
 <% pageContext.setAttribute("tab", 11);%>
     <%@include file="../tenderer/TendererTabPanel.jsp" %>
   


 <div class="tabPanelArea_1">
 <form  name="complaintsubmit" id="complaintsubmit" method="post" >
     <input type="hidden" name="refno" value="<%=toextTenderRefNo%>" />
   	<table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_1 t_space tableView_1">
     <tr>
      <td  class="t-align-left ff" valign="top" colspan="2"><fmt:message key="LBL_FIELDS_MARKED_WITH"/>(<span style="color:Red">*</span>)<fmt:message key="LBL_ARE_MANDATORY"/></td>
    </tr>
    <tr>
      <td width="18%" class="t-align-left ff" valign="top"><fmt:message key="LBL_COMPLAINT_FOR"/>:&nbsp;<span style="color:Red">*</span> </td>
      <td width="82%" class="t-align-left">
         
        <select style="width:150px"  id="complaintType"  name="complaintType">
            
	            <option value="" selected="selected">Select complaint type</option>
		        <c:forEach var = "tender" items = "${regardingtenders}">             
		                  
                           <option value="${tender.complaintTypeId}"  ><c:out value="${tender.complaintType}" /></option>        
		                  
	            </c:forEach>
        </select> 
      
   

      </td> 
    </tr>
    <tr>
      <td width="18%" class="t-align-left ff" valign="top"><fmt:message key="LBL_COMPLAINT_SUBJECT"/>:&nbsp;<span style="color:Red">*</span> </td> 
      <td width="82%" class="t-align-left"><textarea id="txtaComplaintBrief" name="txtaComplaintBrief" rows="4" cols="80" onkeypress="textCounter(this,this.form.counter,200);"></textarea></td>
    </tr>
    <tr>
      <td class="t-align-left ff" valign="top">Instruction :</td>
    <td>
        <a onclick="javascript:window.open('ViewInstruction.htm', '', 'width=800px,height=600px,scrollbars=1','');" href="javascript:void(0);">View</a>

    </td></tr>
    
    <tr>
      <td class="t-align-left ff" valign="top"><fmt:message key="LBL_COMPLAINT_DETAIL"/>:&nbsp;<span style="color:Red">*</span></td>
      <td class="t-align-left"><textarea id="txtaComplaintDetail" name="txtaComplaintDetail" rows="4" cols="80" onkeypress="textCounter(this,this.form.counter,2000);"></textarea></td>
    </tr>
   
   
    </tr>
               <tr>
                  <td width="127">Do you want to upload documents?</td>
   			      <td width="818"><input type="radio" name="uploadstatus" id="uploadstatus" value="yes" checked>Yes <input type="radio" name="uploadstatus"  id="uploadstatus" value="no">No</td>
                </tr> 
     <tr>
      <td class="t-align-left ff" valign="top"><fmt:message key="LBL_SEND_TO"/> :&nbsp;<span style="color:Red">*</span></td>
      <td class="t-align-left">
        PE
      </td>
    </tr>
   <tr>
      <td></td>
      <td class="t-align-left"><label class="formBtn_1">
        	<input type="BUTTON" value="Submit" onclick="javascript:validateForm();"/>
        </label></td>
    </tr>
  </table>   
   	 <div class="t_space" style="text-align:left;">
    	
    </div>  </form>
   </div>
 
   <div>&nbsp;</div>
    <!--Dashboard Content Part End-->

	<!--Dashboard Footer Start-->
            <%@ include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
                 </div>
            </div>
    </body>
</html>
   
    
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   