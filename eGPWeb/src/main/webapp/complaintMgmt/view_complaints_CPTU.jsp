<%@page import="java.util.*"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:directive.page import="com.cptu.egp.eps.web.servicebean.MessageProcessSrBean" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Complaint Management</title>
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../css/demo_table.css" />
        <link type="text/css" rel="stylesheet" href="../css/demo_page.css" />
        <script src="../js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
		<script type="text/javascript" 	src="../js/jquery-ui-1.8.6.custom.min.js"></script>
        <link type="text/css" rel="stylesheet" href="../css/border-radius.css" />
        <script type="text/javascript" src="../js/jquery.dataTables.js"></script>
               <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>


        <script type="text/javascript" src="../js/lang/en.js"></script>
	<style type="text/css">
		.pg-normal {
		color: #000000;
		font-weight: normal;
		text-decoration: none;
		cursor: pointer;
		}
		
		.pg-selected {
		color: #800080;
		font-weight: bold;
		text-decoration: underline;
		cursor: pointer;
		}
	</style>


	<script type="text/javascript">
function regForNumber(value)
            {
                if(value!=""){
                    return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
                }else{
                    return true;
                }
                

            }
            function checkDate(){
                document.searchDGCPTU.action="ViewComplaintsCPTU.htm";
                        document.searchDGCPTU.submit();
            }



		function Pager(tableName, itemsPerPage) {
		this.tableName = tableName;
		this.itemsPerPage = itemsPerPage;
		this.currentPage = 1;
		this.pages = 0;
		this.inited = false;
		
		this.showRecords = function(from, to) {
		var rows = document.getElementById(tableName).rows;
		// i starts from 1 to skip table header row
		for (var i = 1; i < rows.length; i++) {
		if (i < from || i > to)
		rows[i].style.display = 'none';
		else
		rows[i].style.display = '';
		}
		}
		
		this.showPage = function(pageNumber) {
		if (! this.inited) {
		alert("not inited");
		return;
		}
		
		var oldPageAnchor = document.getElementById('pg'+this.currentPage);
		oldPageAnchor.className = 'pg-normal';
		
		this.currentPage = pageNumber;
		var newPageAnchor = document.getElementById('pg'+this.currentPage);
		newPageAnchor.className = 'pg-selected';
		
		var from = (pageNumber - 1) * itemsPerPage + 1;
		var to = from + itemsPerPage - 1;
		this.showRecords(from, to);
		}
		
		this.first = function() {
		
		this.showPage(1);
		}
		
		this.prev = function() {
		if (this.currentPage > 1)
		this.showPage(this.currentPage - 1);
		}
		
		this.next = function() {
		if (this.currentPage < this.pages) {
		this.showPage(this.currentPage + 1);
		}
		
		
		}
		this.last = function() {
		
		this.showPage(this.pages);
		}
		this.goton = function(pageNO) {
		
		if(pageNO.value <= this.pages){
		this.showPage(pageNO.value);
		}
		}
		
		
		this.init = function() {
		var rows = document.getElementById(tableName).rows;
		var records = (rows.length - 1);
		this.pages = Math.ceil(records / itemsPerPage);
		this.inited = true;
		}
		
		this.showPageNav = function(pagerName, positionId) {
		if (! this.inited) {
		alert("not inited");
		return;
		}
		var element = document.getElementById(positionId);
		var pagerHtml = '<span>Page '+this.currentPage+' - '+this.pages +'</span>  &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; ';
		
		
		pagerHtml += '&nbsp; &nbsp; &nbsp;<input type="text" style="width: 20px;" class="formTxtBox_1" value="" id="pageNo" >';
		pagerHtml += '<label class="formBtn_1 l_space"><input type="button" value="Go To Page" onclick="'+pagerName+'.goton(pageNo);"></label> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;  ';
		for (var page = 1; page <= this.pages; page++)
		pagerHtml += '<span id="pg' + page + '" class="pg-normal" style="color: gray;" onclick="' + pagerName + '.showPage(' + page + ');"> </span>  ';
		pagerHtml += '<span style="color: black;" onclick="' + pagerName + '.first();" class="pg-normal"> << First &nbsp;  </span>  ';
		pagerHtml += '<span style="color: black;" onclick="' + pagerName + '.prev();" class="pg-normal"> < Previous&nbsp; &nbsp; </span>  ';
		
		pagerHtml += '<span style="color: black;" onclick="'+pagerName+'.next();" class="pg-normal"> Next>&nbsp;</span>';
		pagerHtml += '<span style="color: black;" onclick="'+pagerName+'.last();" class="pg-normal"> Last >></span>';
		
		
		element.innerHTML = pagerHtml;
		
		}
		}
	
	</script>
<script type="text/javascript">
		function searchValidate()
		{
                    var compId = $('#txtCompID').val();
                var tenderId = $('#txtTenderID').val();
                var status = $('#status').val();
               if(compId =="" && tenderId=="" && $('#txtReferneceNo').val()=="" && status == -1){
                    jAlert("Please enter any search Parameter","Complaint Management", function(RetVal) {
                    });
                    return false;
                }
                else if(!regForNumber(compId)){
                    jAlert("Please enter Numeric","Complaint Management", function(RetVal) {
                    });
                    return false;
                }
                else if(!regForNumber(tenderId)){
                    jAlert("Tender Id must be Numeric","Complaint Management", function(RetVal) {
                    });
                    return false;
                }
                else
                {
                        document.searchDGCPTU.action="searchviewcomplaintsCPTU.htm";
                        document.searchDGCPTU.submit();
                        return true;
                }
		}
	</script>
        <script>
            var arrReviewMember = new Array();
            var arrReviewPanelIds = new Array();
            function displayRWMem(rwPId){
                for(var y = 0;y<arrReviewPanelIds.length;y++){
                    if(arrReviewPanelIds[y] == rwPId){
                        //alert(arrReviewMember[y]);
                        jAlert(arrReviewMember[y],"Review Panel Member's", function(RetVal) {

                        });
                    }
                }
            }
        </script>
	</head>

    <body>

        <div  class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->


            <!--Dashboard Content Part Start-->
            <div class="DashboardContainer">
                <%
                HashMap<Integer,String> map = null;
if(request.getAttribute("map")!=null){
    map = (HashMap) request.getAttribute("map");
    }
%>


                <div class="inner-tableHead t_space"><fmt:message key="LBL_COMPLAINT_ASSIGNMENT_SUMMARY"/></div>
                <form action=""  name="searchDGCPTU" id="searchDGCPTU" method="post">
                <table width="970" cellspacing="0" class="tableList_3">
					<tr>
                        <th width="206"><fmt:message key="LBL_REVIEWPANEL"/></th>
							<c:forEach var="lstReviewPanels" items="${lstReviewPanels}">
                                                            <script>
                                                                arrReviewMember.push("<c:out value="${lstReviewPanels.reviewMembers}" />");
                                                                arrReviewPanelIds.push(<c:out value="${lstReviewPanels.reviewPanelId}" />);
                                                            </script>
                                                                <th width="148">
                                                                    <a style="text-decoration: underline; cursor: pointer;" id="rwName_<c:out value="${lstReviewPanels.reviewPanelId}" />" onclick="displayRWMem(<c:out value="${lstReviewPanels.reviewPanelId}" />)">${lstReviewPanels.reviewPanelName}</a>
                                                                </th>
							</c:forEach>
					</tr>
					<tr>
                        <td style="text-align:center;">Assigned Complaints</td>
            			     <c:forEach var="lstCompaintsCount" items="${lstCompaintsCount}">
                		        <td style="text-align:center;">${lstCompaintsCount}</td>
							</c:forEach>
					</tr>
			
                </table>
                <div class="inner-tableHead t_space"><fmt:message key="LBL_SEARCH_COMPLAINTS"/></div>
                <div class="formBg_1">
                    <table width="970" cellspacing="10" border="0" id="SearchTable" class="formStyle_1">
                        <tr>
                            <td width="102" class="ff"><fmt:message key="LBL_COMPLAINT_ID"/>:</td>
                            <td width="201"><input id="txtCompID" name="txtCompID" /></td>
                            <td width="74" class="ff"><fmt:message key="LBL_TENDER_ID"/>:</td>
                            <td width="205"><input id="txtTenderID" name="txtTenderID" /></td>
                        </tr>
                            <tr>
                            <td width="102" class="ff"><fmt:message key="LBL_REFERENCE_NO"/> : </td>
                            <td width="204"><input id="txtReferneceNo" name="txtReferneceNo" /></td>
                            <td width="102" class="ff">Status : </td>
                            <td width="204"><select id="status" class="formStyle_1" name="status">
                                    <option value="-1">select</option>
                                    <option value="0">Not Assigned</option>
                                    <option value="1">Assigned</option>
                                </select>

                            </td>
                        </tr>
                        <tr>
                            <td class="ff" colspan="6" style="text-align:center;">
                                <label class="formBtn_1"><input type="button" name="search" id="btnSearch" value="Search" onClick="javascript:searchValidate(id);" /></label>&nbsp;
                                <label class="formBtn_1"><input type="reset" name="btnReset" id="btnReset" value="Reset" onclick="checkDate()" /></label>
                        </tr>
                    </table>
                </div>
                <div class="tabPanelArea_1">
                   <table class="tableList_3" id="results" width="100%" cols="@9">
                      <thead>
						<tr>
							<th width="50" class="ff"><fmt:message key="LBL_COMPLAINT_ID"/></th>
							<th width="45" class="ff"><fmt:message key="LBL_TENDER_ID"/></th>

							<th width="60" class="ff"><fmt:message key="LBL_COMPLAINT_FOR"/></th>
							<th width="60" class="ff"><fmt:message key="LBL_COMPLAINT_DETAIL"/></th>
							<th width="60" class="ff"><fmt:message key="LBL_COMPLAINT_DATE_TIME"/></th>
							<th width="60" class="ff"><fmt:message key="LBL_NAME_OF_TENDERER_CONSULTANT"/></th>
							<th width="60" class="ff"><fmt:message key="LBL_ASSIGNMENT_DATE"/></th>
							<th width="60" class="ff">Review Panel Name</th>
							<th width="60" class="ff">Status</th>

							<th width="60" class="ff"><fmt:message key="LBL_ACTION"/></th>
                        </tr>
                       </thead>
					    <tbody>
			
						<c:forEach var="lstViewComplCPTU" items="${lstViewComplCPTU}">
                    	    <tr>
  								<td style="text-align:center;">${lstViewComplCPTU.complaintId}</td>
	                           <td style="text-align:center;">${lstViewComplCPTU.tenderMaster.tenderId}</td>
	                           <td style="text-align:center;">${lstViewComplCPTU.complaintType.complaintType}</td>
	                           <td style="word-wrap:break-word;">${lstViewComplCPTU.complaintDetails}</td>
                                   <c:set var="creationDate" scope="session" value="${lstViewComplCPTU.complaintCreationDt}"/>
                                   <c:set var="assignedtoPanelDt" scope="session" value="${lstViewComplCPTU.assignedtoPanelDt}"/>
                                   <%
                                        Date cDate = (Date)session.getAttribute("creationDate");
                                        Date aDate = (Date)session.getAttribute("assignedtoPanelDt");
                                   %>
                                   <td style="text-align:center;"><%=DateUtils.gridDateToStrWithoutSec(cDate) %></td>
	                           <td style="text-align:center;">
                                        <c:choose>
                                    <c:when test="${lstViewComplCPTU.tendererMaster.tblCompanyMaster.companyId==1}">
                                        ${lstViewComplCPTU.tendererMaster.firstName} ${complaint.tendererMaster.lastName}
                                    </c:when>
                                    <c:otherwise>
                                        ${lstViewComplCPTU.tendererMaster.tblCompanyMaster.companyName}
                                    </c:otherwise>
                                </c:choose>
                                   </td>
	                           <td style="text-align:center;"><%=DateUtils.gridDateToStrWithoutSec(aDate).split(" ")[0] %></td>
	                           <td style="text-align:center;">
                                       <c:set var="panel" value="${lstViewComplCPTU.panelId}" scope="session" />
                                       <%
int panelId = (Integer)session.getAttribute("panel");
%>

<%if(map.get(panelId)!=null){
    out.print(map.get(panelId));
    }else{
    out.print("-");
    }%></td>
	                           <td style="text-align:center;">
                                       <c:choose>
                                    <c:when test="${lstViewComplCPTU.panelId==0}">
                                        Not Assigned
                                    </c:when>
                                    <c:otherwise>
                                       Assigned
                                    </c:otherwise>
                                </c:choose></td>

	                           <td>
	                           		<c:if  test="${lstViewComplCPTU.panelId==0}">
							   			<a href="AssignComplaints.htm?complaintId=${lstViewComplCPTU.complaintId}">Assign </a>&nbsp|&nbsp<br>
							  		</c:if>
								<a href="ViewComplHistoryTenderer.htm?complaintId=${lstViewComplCPTU.complaintId}&tenderId=${lstViewComplCPTU.tendererMaster.tendererId}">View Response</a>&nbsp|&nbsp<br><a href="ComplNotificationPE.htm?complaintId=${lstViewComplCPTU.complaintId}">Notify PE</a></td></tr>
						</c:forEach>
						</tbody>
                    </table>
                                      
					<br />
<div id="pageNavPosition"></div>
<br />


</form>
<script type="text/javascript">
	var pager = new Pager('results', 10);
	pager.init();
	pager.showPageNav('pager', 'pageNavPosition');
	pager.showPage(1);
        sortResultTable();
//--></script>
<!-- page nation starts here -->
					 
<!-- page nation ends here-->
                </div>
            </div>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@ include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>

    </body>

</html>
