<%@page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:directive.page import="com.cptu.egp.eps.web.servicebean.MessageProcessSrBean" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Notification to PE</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />

        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript" src="../js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery.alerts.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="../js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />

      
        <script type="text/javascript">
            function GetCal(txtname,controlname){
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            function validatesearch()
            {
                document.getElementById("todatespan").innerHTML="";
                if(document.getElementById("txtPublicationDate").value!="")
                {
                    if(document.getElementById("txtPublicationDate1").value=="")
                    {
                        document.getElementById("todatespan").innerHTML="Please select to date";
                        return false;
                    }
                }
            }
            function popup(mylink, windowname)
            {
                if (! window.focus)return true;
                    var href;
                        if (typeof(mylink) == 'string')
                            href=mylink;
                        else
                            href=mylink.href;
                    window.open(href, windowname, 'width=800,height=600,scrollbars=yes');
                return false;
            }
        </script>
    </head>
    <body>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <div class="tabPanelArea_1">
            <!--Dashboard Content Part Start-->
            <div class="DashboardContainer">
                <div class="pageHead_1">Notification to PE<span class="c-alignment-right"><a href="ViewComplaintsCPTU.htm" class="action-button-goback">Go Back to Dashboard</a></span></div>
<form action="ComplNotificationCPTU.htm?complaintId=${master.complaintId}" method="post">
                <table width="100%" cellspacing="5" class="t_space tableList_3">
                    <tr>
                        <th width="20%" class="ff">Complaint ID </th>
                        <th width="32%" class="ff">Tender/Proposal ID </th>
                        <th width="25%" class="ff">Reference No. </th>
                        <th width="23%" class="ff">Procuring Entity </th>
                    </tr>
                    <tr>
                        <td>${master.complaintId}</td>
                        <td>${master.tenderMaster.tenderId}</td>
                        <td>${master.tenderRefNo}</td>
                        <td>${master.tenderMaster.tblAppPackages.tblProcurementRole.procurementRole}</td>
                    </tr>
                    <tr>
                        <td colspan="4" height="3"></td>
                    </tr>
                    <tr>
                        <th class="ff">Complaint For </th>
                        <th class="ff">Complaint Brief </th>
                        <th class="ff">Complaint Date and Time </th>
                        <th class="ff">Name of the Bidder / Consultant </th>
                    </tr>
                     <c:set var="creationDate" scope="session" value="${master.complaintCreationDt}" />
                     <%
Date cDate = (Date)session.getAttribute("creationDate");
%>
                    <tr>
                        <td>${master.complaintType.complaintType}</td>
                        <td>${master.complaintDetails}</td>
                        <td><%=DateUtils.gridDateToStrWithoutSec(cDate)%></td>
                        <td>${master.tendererMaster.firstName}</td>
                    </tr>
                    <tr>
                        <td colspan="4" height="3"></td>
                    </tr>
                    <tr>
                        <th class="ff">Complaint Status </th>
                        <th class="ff">Registration Fee Detail </th>
                        <th class="ff">Security Deposit </th>
                        <th class="ff">&nbsp;</th>
                    </tr>
                    <tr>
                        <td>${master.complaintStatus}</td>
                        <td><a href="viewFeesDetails.htm?complId=${master.complaintId}&feetype=Registration&tenderId=${master.tenderMaster.tenderId}" onClick="return popup(this, 'Registration Fee')">View</a></td>
                        <td><a href="viewFeesDetails.htm?complId=${master.complaintId}&feetype=Security&tenderId=${master.tenderMaster.tenderId}" onClick="return popup(this, 'Security fee')">View</a></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4" height="3"></td>
                    </tr>
                    <tr>
                        <td valign="top" class="ff">Comments : <span class="mandatory">*</span></td>
                        <td colspan="3"><textarea id="txtaCommets" name="txtaCommets" cols="80" rows="4"></textarea>
                            <label class="formBtn_1 l_space"><input name="submit" type="submit" value="Notify PE" />
                                
                            </label>
                        </td>
                    </tr>
                </table>
                        </form>
            </div>
            </div>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@ include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>

</body>
</html>
