<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.*,java.text.*" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<html >
     <%
	response.setHeader("Cache-Control","no-cache");
	response.setHeader("Cache-Control","no-store"); 
	response.setHeader("Pragma","no-cache");
    %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>complaint-management system</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

<%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>   
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
<link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
<SCRIPT TYPE="text/javascript">
<!--
function popup(mylink, windowname)
{
if (! window.focus)return true;
var href;
if (typeof(mylink) == 'string')
   href=mylink;
else
   href=mylink.href;
window.open(href, windowname, 'width=800,height=600,scrollbars=yes');
return false;
}
//-->
</SCRIPT>
<script type="text/javascript">
				function escalateSubmit(){
					
					//alert("comments to hope are"+document.getElementById("txtaComments").value);
                    var comments=document.getElementById("txtaComments").value;  
                    var uploadstatusvalue;
            		var radios = document.escalate.elements["uploadstatus"];
                      for (var i=0; i <radios.length; i++) {
		              if (radios[i].checked) {
		            	  uploadstatusvalue=radios[i].value; 
		            	  
		              
		              }
		             }
                    if(document.getElementById("txtaComments").value==""){
                         jAlert("Please enter your comments","Complaint Management", function(RetVal) {
                                        });
 					   document.getElementById("txtaComments").focus(); 
 					   return false;
 					 }
                    else{
					   document.forms[0].action="escalateSubmit.htm?complaintId=${complaint.complaintId}&complaintLevelId=${complaint.complaintLevel.complaintLevelId}&tenderId=${complaint.tenderMaster.tenderId}&txtaComments="+comments+"&uploadstatus="+uploadstatusvalue;
                       document.forms[0].submit();
                       return true;     	  
		 
                    }	 
							
	}

	//escalate secretary

function escalateSubmitN(){
					
	var uploadstatusvalue;
	var radios = document.escalate.elements["uploadstatus"];
for (var i=0; i <radios.length; i++) {
if (radios[i].checked) {
  uploadstatusvalue=radios[i].value; 
  

}
}
	if(document.getElementById("txtaComments").value==""){
        jAlert("Please enter your comments","Complaint Management", function(RetVal) {
                                        });
		   document.getElementById("txtaComments").focus(); 
		   return false;
		 }
    else{
                    var comments=document.getElementById("txtaComments").value;  
                    var secId=document.getElementById("secretary").value;
                    document.forms[0].action="escalateSubmit.htm?complaintId=${complaint.complaintId}&complaintLevelId=${complaint.complaintLevel.complaintLevelId}&tenderId=${complaint.tenderMaster.tenderId}&txtaComments="+comments+"&secretary="+secId+"&uploadstatus="+uploadstatusvalue;
                    document.forms[0].submit();  
    } 
							
	}

	//escalate review panel
	
	function escalateSubmitR(){
		var uploadstatusvalue;
		var radios = document.escalate.elements["uploadstatus"];
 for (var i=0; i <radios.length; i++) {
  if (radios[i].checked) {
	  uploadstatusvalue=radios[i].value; 
  }
 }
		if(document.getElementById("txtaComments").value==""){
             jAlert("Please enter your comments","Complaint Management", function(RetVal) {
                                        });
			   document.getElementById("txtaComments").focus(); 
			   return false;
			 }
        else{
					
					//alert("comments to review panel are"+document.getElementById("txtaComments").value);
                    var comments=document.getElementById("txtaComments").value;  
					document.forms[0].action="escalateSubmit.htm?complaintId=${complaint.complaintId}&complaintLevelId=${complaint.complaintLevel.complaintLevelId}&tenderId=${complaint.tenderMaster.tenderId}&txtaComments="+comments+"&uploadstatus="+uploadstatusvalue;
                    document.forms[0].submit();  
						 
        }		
	}
	
	


</script>
<script language="javascript" type="text/javascript">

function textCounter( field, countfield, maxlimit ) {
  if ( field.value.length > maxlimit )
  {
    field.value = field.value.substring( 0, maxlimit );
     jAlert("Textarea value can only be 255 characters in length","Complaint Management", function(RetVal) {
    });
    return false;
  }
  else
  {
    countfield.value = maxlimit - field.value.length;
  }
}

</script>





</head>
<body onload="getQueryData('getmyprebidquery');">
    
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <%@include file="../resources/common/AfterLoginTop.jsp"%>
  <!--Dashboard Header End-->

  <!--Dashboard Content Part Start-->
  
   <%
               
               // Variable tenderId is defined by u on ur current page.
            
    String c_tenderid = ""; 
    if (request.getParameter("tenderid") != null && !"null".equalsIgnoreCase(request.getParameter("tenderid"))) {
                c_tenderid = request.getParameter("tenderid");
    } else if (request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId"))) {
                c_tenderid = request.getParameter("tenderId");
    }
	pageContext.setAttribute("tenderId",c_tenderid); 
  %>

            <!--Dashboard Content Part Start-->
            <div class="DashboardContainer">
             <div class="pageHead_1"><fmt:message key="LBL_COMPLAINT_DETAIL"/><span class="c-alignment-right"><a href="complaint.htm?tenderId=<%=c_tenderid%>" class="action-button-goback"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a></span></div>
             <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
 <% pageContext.setAttribute("tab", 11);%>
  <%@include file="../tenderer/TendererTabPanel.jsp" %>
<br />
<%if(request.getAttribute("msg")!=null){%>
 <div class="responseMsg successMsg">Your Complaint ID - ${master.complaintId} has been successfully sent to  ${master.complaintLevel.complaintLevel} </div>
<%}%>

<form name="escalate" method="post" enctype="multipart/form-data" >
            <div class="tabPanelArea_1">

                <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_1 tableView_1" style="table-layout: fixed">


				 <c:set var="creationDate" scope="session" value="${complaint.complaintCreationDt}"/>      
		       
           
		    <%
			   Date cDate = (Date) session.getAttribute("creationDate");
          
		   %>
                    <tr>
                        <td width="200" class="ff" valign="top"><fmt:message key="LBL_TENDER_DETAILS"/> : </td>
                        <td width="719"><label id="lblNameofTenderer">
                                 <c:choose>
                                    <c:when test="${complaint.tendererMaster.tblCompanyMaster.companyId==1}">
                                        ${complaint.tendererMaster.firstName} ${complaint.tendererMaster.lastName}
                                    </c:when>
                                    <c:otherwise>
                                        ${complaint.tendererMaster.tblCompanyMaster.companyName}
                                    </c:otherwise>
                                </c:choose>
                            </label></td>
                    </tr>
                    <tr>
                        <td class="ff" valign="top"><fmt:message key="LBL_COMPLAINT_DATE_TIME"/> : </td>
                        <td><label id="lblComplaintDateTime"><%=DateUtils.gridDateToStrWithoutSec(cDate)%></label></td>
                    </tr>
                    <tr>
                        <td class="ff" valign="top"><fmt:message key="LBL_COMPLAINT_FOR"/> : </td>
                        <td><label id="lblComplaintFor">${complaint.complaintType.complaintType}</label></td>
                    </tr>
                    <tr>
                        <td class="ff" valign="top"><fmt:message key="LBL_COMPLAINT_SUBJECT"/> : </td>
                        <td style="word-wrap:break-word;">${complaint.complaintSubject }</td>
                    </tr>
                    <tr>
                        <td class="ff" valign="top"><fmt:message key="LBL_COMPLAINT_DETAIL"/> : </td>
                        <td><label id="lblComplaintDetail">${complaint.complaintDetails}</label></td>
                    </tr>
                    <tr>
                        <td class="ff" valign="top"><fmt:message key="LBL_REFERENCE_DOCUMENTS"/>:</td>
                        <td><table width="100%" cellspacing="0" class="tableList_3">
                                <tr>
                                    <th width="7%"><fmt:message key="LBL_sNO"/></th>
                                    <th width="30%"><fmt:message key="LBL_FILE_NAME"/></th>
                                    <th width="43%"><fmt:message key="LBL_DESCRIPTION"/></th>
                                    <th width="11%"><fmt:message key="LBL_SIZE"/></th>
                                    <th width="9%"><fmt:message key="LBL_ACTION"/></th>
                                </tr>
<c:if test="${empty  docList}">
  <tr> <td colspan="5"><fmt:message key="LBL_NO_REFERENCE_DOCUMENTS_ARE_FOUND"/></td></tr>
 </c:if>
								<c:if test="${docList!=null}">
<c:forEach  items="${docList}" var="doc">
                                <tr>

                                    <td>${doc.complaintDocId}</td>
                                    <td>${doc.docName}</td>
                                    <td class="table-description">Reference Doc.</td> 
                                    <td>${doc.docSize}</td>
                                    <td><a href="download.htm?docname=${doc.docName}&tenderId=<%=c_tenderid%>"><fmt:message key="LBL_DOWNLOAD"/></a></td>
                                </tr> 

</c:forEach>
</c:if>
                            </table></td>
                    </tr>
                </table>
                <table width="100%" cellspacing="0" class="tableList_3 t_space">
                    <tr>
                        <th width="44"><fmt:message key="LBL_sNO"/></th>
                        <th width="69"><fmt:message key="LBL_LEVEL"/></th>
                        <th width="246"><fmt:message key="LBL_COMMENTS_OF_TENDERER_CONSULTANT"/></th>
                        <th width="81"><fmt:message key="LBL_RESPONSE"/></th>
                        <th width="128"><fmt:message key="LBL_RESPONSE_DATE_TIME"/></th>
                        
                    </tr>
	<c:set var="i" value="1"/>

	<c:forEach  items="${historyList}" var="history">
          
                <tr>
                        <td>${i}</td> 
                        <c:set var="i" value="${i+1}"/>
                        <td>${history.level}</td>
                        <td class="table-description"><a target="_blank" href="ViewHistory_tenderer.htm?complaintId=${history.complaintId}&complaintLevelId=${history.levelId}&tenderId=<%=c_tenderid%>" onClick="return popup(this, 'historyPopUP')"><fmt:message key="LBL_VIEW_HISTORY"/></a></td>
                     
						
						
						
						
						 <c:set var="status" scope="session" value="${history.response}"/>
  <c:set var="datecomplaint" scope="session" value="${history.responseDate}"/>    
<% 
    int escalate=Integer.parseInt(XMLReader.getMessage("DaysforEscalation"));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dt = session.getAttribute("datecomplaint").toString(); // Start date
		
		Calendar c = Calendar.getInstance();
		Calendar c1 = Calendar.getInstance();
		
		c.setTime(sdf.parse(dt)); 
		
		c.add(Calendar.DATE,escalate);

if(c1.after(c)&& (session.getAttribute("status").toString().equals("Pending")) ){


 %>
     <td>No Response</td>
	 <%}else{%>
     <td>${history.response}</td>
	 <%}%>

						
											
						
						
						
					
						     
						
					<%
		       Date strDate = (Date) session.getAttribute("datecomplaint");
          		   %>
                           <td><%=DateUtils.gridDateToStrWithoutSec(strDate)%></td>



                </tr>  
  

	</c:forEach> 

</table>

                <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_1 tableView_1 t_space">
                    <tr>
      <td class="t-align-left ff" valign="top">Instruction :</td>
    <td>
        <a onclick="javascript:window.open('ViewInstruction.htm', '', 'width=800px,height=600px,scrollbars=1','');" href="javascript:void(0);">View</a>

    </td></tr>
                    <tr>
                        <td width="127" class="ff" valign="top"><fmt:message key="LBL_COMMENTS_ESCALATE"/> : <span class="mandatory">*</span></td>
                        <td width="818"><textarea id="txtaComments" name="txtaComments" rows="4" class="width_textarea" onkeypress="textCounter(this,this.form.counter,2000);"></textarea></td>
                    </tr>
                 <tr>
                  <td width="127">Do you want to upload documents?</td>
   			    		
						<td width="818"><input type="radio" name="uploadstatus" id="uploadstatus" value="yes" checked>Yes <input type="radio" name="uploadstatus" id="uploadstatus" value="no">No</td>
                </tr>
                    <tr>
                        <td width="127" class="ff" valign="top"><fmt:message key="LBL_SEND_TO"/> : </td>
<c:if test="${complaint.complaintLevel.complaintLevelId==1}">
                        <td width="818" class=""><label id="lblSentTo">HOPA</label></td>
</c:if>


<c:if test="${complaint.complaintLevel.complaintLevelId==2}">
<c:if test="${secretaryList!=null && not empty secretaryList}">
<td width="818" class="">
 <select name="secretary" id="secretary">   
<c:forEach  items="${secretaryList}" var="secretary">
                    
                          <option value="${secretary.govUserId}">${secretary.employeeName}</option>   
                     
</c:forEach>
  </select> 
  </td>
</c:if>
</c:if>
<c:if test="${complaint.complaintLevel.complaintLevelId==3}">
                        <td width="818" class=""><label id="lblSentTo"><fmt:message key="LBL_REVIEWPANEL"/></label></td>
</c:if>

                    </tr>
                </table>
                <table width="945" cellspacing="0" cellpadding="0" border="0"  class="formStyle_1 t_space">
                    <tr>
                        <td width="143" class="ff" valign="top"></td>
                        <td width="802" class=""><label class="formBtn_1">


						<c:if test="${complaint.complaintLevel.complaintLevelId==1}">
                                <input name="submit" type="button" value="Submit" onclick="escalateSubmit()"/>
								</c:if>
                       <c:if test="${complaint.complaintLevel.complaintLevelId==2}"> 
                                <input name="submit" type="button" value="Submit" onclick="escalateSubmitN()"/>   
								</c:if>

                         <c:if test="${complaint.complaintLevel.complaintLevelId==3}"> 
                                <input name="submit" type="button" value="Submit" onclick="escalateSubmitR()"/>   
								</c:if>

                            </label></td>
                    </tr>
                </table>

			

            </div>
</form>
            </div>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@ include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
	

    </body>
</html>
