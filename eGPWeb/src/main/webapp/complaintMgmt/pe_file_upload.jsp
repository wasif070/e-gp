
<%@page import="com.cptu.egp.eps.dao.generic.Complaint_Level_enum"%>
<!DOCTYPE link PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="fm" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@page import="com.cptu.egp.eps.web.utility.AppMessage"%>
<%@page import="com.cptu.egp.eps.model.table.TblLocationMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderSubRightServiceImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.Date,java.text.DateFormat"%>
<%@page import="com.cptu.egp.eps.web.utility.NavigationRuleUtil"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<noscript>
    <meta http-equiv="refresh" content="0; URL=<%= request.getContextPath()%>/JSInstruction.jsp">
</noscript>
<%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
%>
<html>
    <head>
        <title>Upload Documents</title>
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <%--<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/ddlevelsmenu.js"></script>
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>

        <link href="<%=request.getContextPath()%>/resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/demo_table.css" />
        <link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/demo_page.css" />

        <script type="text/javascript">
            function upload(){

                if(document.getElementById("file").value==""){
                    jAlert("Please upload a document","Complaint Management", function(RetVal) {
                    });
                    document.getElementById("file").focus();
                    return false;
                }
                else if(document.getElementById("txtuploaddesc").value==""){
                    jAlert("Please enter description","Complaint Management", function(RetVal) {
                    });
                    document.getElementById("txtuploaddesc").focus();
                    count++;
                }
                else{
                    document.forms[1].action="peFileUpload.htm?complId=${complaintObject.complaintId}";
                    document.forms[1].submit();
                    return true;
                }
            }
        </script>
    </head>
    <body>
        <%@include file="../resources/common/AfterLoginTop.jsp"%>
        <%
                    String exterror = "";
                    if (request.getAttribute("errormsg") != null && request.getAttribute("errormsg") != "null") {
                        exterror = request.getAttribute("errormsg").toString();
                    }
        %>
        <div class="dashboard_div">
            <form method="post" enctype="multipart/form-data" >
                <!--Dashboard Header Start-->

                <!--Dashboard Header End-->

                <!--Dashboard Content Part Start-->
                <div class="DashboardContainer t_space">
                    <div	class="pageHead_1 t_space">Upload Documents
                        <%
                                    if (session.getAttribute("procurementRole") != null) {
                                        String ck[] = session.getAttribute("procurementRole").toString().split(",");

                                        for (int ii = 0; ii < ck.length; ii++) {
                                            if (ck[ii].equalsIgnoreCase("PE")) {%>
                        <span class="c-alignment-right"><a  href="complaintOfficer.htm?tenderId=${complaintObject.tenderMaster.tenderId}" class="action-button-goback"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a></span>
                        <%} else if (ck[ii].equalsIgnoreCase("HOPE")) {%>
                        <span class="c-alignment-right">
                            <a class="action-button-goback"	href="complaintOfficerHope.htm"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a>
                        </span>
                        <%} else if (ck[ii].equalsIgnoreCase("Secretary")) {%>
                        <span class="c-alignment-right">
                            <a class="action-button-goback"	href="complaintOfficerHope.htm"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a>
                        </span>
                        <%}
                                                                }
                                                            } else {%>
                        <span class="c-alignment-right">
                            <a class="action-button-goback"	href="TendererDashboard.htm"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a>
                        </span>
                        <%                            }
                        %>

                    </div>
                    <%
                                String disp = "";
                                CommonService cs = (CommonService) AppContext.getSpringBean("CommonService");
                                String tenderId = "";
                                if (request.getParameter("tenderId") != null) {
                                    tenderId = request.getParameter("tenderId");
                                } else if (request.getAttribute("tenderId") != null) {
                                    tenderId = request.getAttribute("tenderId").toString();
                                }
                                String procNature = cs.getProcNature(tenderId).toString();
                                if ("goods".equalsIgnoreCase(procNature)) {
                                    disp = "Supplier";
                                } else if ("works".equalsIgnoreCase(procNature)) {
                                    disp = "Contractor";
                                } else {
                                    disp = "Consultant";
                                }

                    %>
                    <% if (exterror == "") {%>
                    <div class="responseMsg successMsg" style="margin-top:12px;">Your comments have been sent to the <%=disp%> for complaint Id:${complaintObject.complaintId}</div>
                    <% }%>
                    <% if (exterror != "") {
                                    // While Displaying msg File Deleted Sucessfully it was showing error icon to solve that problem here
                                    // we add patch so it will display succssmsg icon
                                    if (exterror.equalsIgnoreCase("Del")) {
                    %>
                    <div class="responseMsg successMsg" style="margin-top:12px;">File Deleted Successfully</div>
                    <% } else if (exterror.equalsIgnoreCase("typecheck")) {%>
                    <div class="responseMsg errorMsg" style="margin-top:12px;">This type of file is not allowed. </div>
                    <% } else if (exterror.equalsIgnoreCase("fileerror")) {%>
                    <div class="responseMsg errorMsg" style="margin-top:12px;"> Error occurred during file Upload. </div>
                    <% } else if (exterror.equalsIgnoreCase("fileexist")) {%>
                    <div class="responseMsg errorMsg" style="margin-top:12px;">File already exist. </div>
                    <% } else if (exterror.equalsIgnoreCase("upload")) {%>
                    <div class="responseMsg successMsg" style="margin-top:12px;">File Uploaded Successfully</div>
                    <% }
                                }%>

                    <table width="100%" cellspacing="5" cellpadding="0" class="tableList_1 t_space" style="table-layout: fixed">
                        <tr>
                            <td width="320" class="ff"><fmt:message key="LBL_COMPLAINT_ID"/></td>
                            <td width="330" class="ff"><fmt:message key="LBL_COMPLAINT_SUBJECT"/></td>
                            <td width="320" class="ff"><fmt:message key="LBL_TENDER_ID"/></td>
                        </tr>
                        <tr>
                            <td><c:out value="${complaintObject.complaintId}"/></td>
                            <td style="word-wrap:break-word;"><c:out value="${complaintObject.complaintSubject}"/></td>
                            <td><c:out value="${complaintObject.tenderMaster.tenderId}"/></td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>
                            <td  class="ff" width="15%">Select a file to upload : <span class="mandatory">*</span></td>
                            <td ><input type="file" class="formTxtBox_1" name="file" id="file" style="width:250px; background-color: white; "/> </td>
                        </tr>
                        <tr>
                            <td class="ff" width="15%"> Descriptions :<font color="red">*</font> </td>
                            <td><input type="text" value="" name="uploaddesc" id="txtuploaddesc" size="50" class="formTxtBox_1" style="width:200px;"/> </td>
                        </tr>
                        <tr>
                            <td class="ff" width="15%"></td>
                            <td> <label class="formBtn_1">
                                    <input type="button"  value="<fmt:message key='BTN_UPLOAD'/>" onclick="upload()"/></label> </td>
                        </tr>
                    </table>

                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="100%"  class="t-align-left">Instructions</th>
                        </tr>
                        <tr>
                            <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("tenderer");%>
                            <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                        </tr>
                    </table>



                    <c:if test="${uploadedDocs!=null}" >



                        <table width="100%" cellspacing="5" cellpadding="0" class="tableList_3 t_space">
                            <thead>
                                <tr><th width="200"><fmt:message key="LBL_FILE_ID"/></th><th width="350"><fmt:message key="LBL_FILE_NAME"/> <th width="300"><fmt:message key="LBL_DESCRIPTION"/></th><th width="20%"><fmt:message key="LBL_UPLOAD_BY"/></th> <th width="120"><fmt:message key="LBL_SIZE"/></th><th width="220"><fmt:message key="LBL_DELETE"/></th></tr>
                            </thead>

                            <c:forEach var = "doc" items = "${uploadedDocs}">
                                <c:set value="Test" var="uploadby"/>
                                <c:choose>
                                    <c:when test="${doc.userRole.userTypeId == 2}">
                                        <c:set value="Tenderer" var="uploadby"/>
                                    </c:when>
                                    <c:when test="${doc.complaintLevel.complaintLevelId == 1}">
                                        <c:set value="PE" var="uploadby"/>
                                    </c:when>
                                    <c:when test="${doc.complaintLevel.complaintLevelId == 2}">
                                        <c:set value="HOPE" var="uploadby"/>
                                    </c:when>
                                    <c:when test="${doc.complaintLevel.complaintLevelId == 3}">
                                        <c:set value="Secretary" var="uploadby"/>
                                    </c:when>
                                    <c:when test="${doc.complaintLevel.complaintLevelId == 4}">
                                        <c:set value="Review Panel" var="uploadby"/>
                                    </c:when>
                                </c:choose>
                                <tr><td>${doc.complaintDocId } </td>
                                    <td>  ${doc.docName }</td>
                                    <td> ${doc.docDescription} </td>
                                    <td><c:out value="${uploadby}"></c:out> </td>
                                    <td>   ${doc.docSize }</td>
                                    <td><a href="<%=request.getContextPath()%>/tenderer/download.htm?docname=${doc.docName }&complaintId=${complaintObject.complaintId}&tenderId=${complaintObject.tenderMaster.tenderId}"><img src="<%=request.getContextPath()%>/resources/images/Dashboard/Download.png" alt="Download" /></a>
                                            <c:set var="docUserTypeId" scope="session" value="${doc.userRole.userTypeId}"/>
                                            <c:set var="docComplaintLevelId" scope="session" value="${doc.complaintLevel.complaintLevelId}"/>
                                            <%
                                                        String level_id = "";
                                                        if (userTypeId == 3) {

                                                            Object acc_objProRole = session.getAttribute("procurementRole");
                                                            String acc_strProRole = "";
                                                            if (acc_objProRole != null) {
                                                                acc_strProRole = acc_objProRole.toString();
                                                            }
                                                            String acc_chk = acc_strProRole;
                                                            String acc_ck1[] = acc_chk.split(",");
                                                            for (int acc_iAfterLoginTop = 0; acc_iAfterLoginTop < acc_ck1.length; acc_iAfterLoginTop++) {
                                                                if (acc_ck1[acc_iAfterLoginTop].equalsIgnoreCase("PE")) {
                                                                    level_id = "1";
                                                                    break;
                                                                } else if (acc_ck1[acc_iAfterLoginTop].equalsIgnoreCase("HOPE")) {
                                                                    level_id = "2";
                                                                    break;
                                                                } else if (acc_ck1[acc_iAfterLoginTop].equalsIgnoreCase("SECRETARY")) {
                                                                    level_id = "3";
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        if (Integer.parseInt(session.getAttribute("docUserTypeId").toString()) == userTypeId) {
                                                            if (userTypeId == 3) {
                                                                if (level_id.equalsIgnoreCase(session.getAttribute("docComplaintLevelId").toString())) {
                                            %>
                                        &nbsp;&nbsp;<a href="<%=request.getContextPath()%>/deletePopUp.htm?docId=${doc.complaintDocId}&tenderId=${complaintObject.tenderMaster.tenderId}&complaintId=${complaintObject.complaintId}&docname=${doc.docName }&flag=PE" onClick="return confirm('Are you sure you want to delete');"><img src="<%=request.getContextPath()%>/resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                            <%}
                                                                                                        } else {%>
                                        &nbsp;&nbsp;<a href="<%=request.getContextPath()%>/deletePopUp.htm?docId=${doc.complaintDocId}&tenderId=${complaintObject.tenderMaster.tenderId}&complaintId=${complaintObject.complaintId}&docname=${doc.docName }&flag=PE" onClick="return confirm('Are you sure you want to delete');"><img src="<%=request.getContextPath()%>/resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                            <%}
                                                        }%>
                                    </td>
                                </tr>


                            </c:forEach>
                        </table>

                    </c:if>

                </div>
            </form>
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
