
<%@page import="com.cptu.egp.eps.model.table.TblComplaintDocs"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.*,java.text.*,java.util.Calendar" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<html >
    <%
                response.setHeader("Cache-Control", "no-cache");
                response.setHeader("Cache-Control", "no-store");
                response.setHeader("Pragma", "no-cache");
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>complaint-management system</title>
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body onload="getQueryData('getmyprebidquery');">
        <div class="dashboard_div">



            <!--Dashboard Header Start-->

            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->









            <c:set var="creationDate" scope="session" value="${complaint.complaintCreationDt}"/>


            <%
                        String cDate = session.getAttribute("creationDate").toString();

            %>






            <table width="800" cellspacing="0" class="tableList_3 t_space" style="table-layout: fixed">
                <tr>
                    <th width="44"><fmt:message key="LBL_sNO"/></th>
                    <th width="106"><fmt:message key="LBL_LEVEL"/></th>
                    <th width="400"><fmt:message key="LBL_COMMENTS"/>/Comments</th>
                    <th width="113"><fmt:message key="LBL_COMMENTS_DATE"/></th>
                </tr>
                <c:set var="i" value="1"/>
               
                <c:forEach items="${listTenderer}" var="history" >
                        <c:set var="flagDocExist" value="false"/>
                        <c:forEach items="${complainDocs}" var="docs" >
                                <c:if test="${history.userRole.userTypeId == docs.userRole.userTypeId}">
                                    <c:set var="flagDocExist" value="true"/>
                                </c:if>
                        </c:forEach>

                    <c:set var="datecomplaint" scope="session" value="${history.commentsdt}"/>

                    <%
                                Date strDate = (Date) session.getAttribute("datecomplaint");
                    %>

                    <tr>
                        <td 
                            <c:if test="${flagDocExist eq 'true'}">
                                    rowspan="1"  
                            </c:if>
                            class="t-align-center">${i}</td>
                        <c:set var="i" value="${i+1}"/>
                        <td width="106">
                            <c:if test="${history.userRole.userTypeId==3}">
                                ${history.complaintLevel.complaintLevel}
                            </c:if>
                            <c:if test="${history.userRole.userTypeId==17}">
                                ${history.complaintLevel.complaintLevel}
                            </c:if>
                            <c:if test="${history.userRole.userTypeId==2}">
                                ${history.complaint.tendererMaster.firstName}
                            </c:if>
                        </td>
                        <c:choose>
                            <c:when test="${history.comments==' '}">
                                <td style="text-align:center;">--</td>
                            </c:when>
                            <c:otherwise>
                                <td width="697" style="word-wrap:break-word;">${history.comments}</td>
                            </c:otherwise>
                        </c:choose>

                        <td width="113"><%=DateUtils.gridDateToStrWithoutSec(strDate)%></td>
                    </tr>
                    <c:if test="${flagDocExist eq 'true'}">
                        <tr>
                            <td></td>
                            <td colspan="3" style="text-align: center;">
                                <table width="100%" cellspacing="0">
                                    <tr>
                                        <th colspan="5"> Reference Documents</th>
                                    </tr>
                                    <tr>
                                        <th width="4%" class="t-align-left">Sl. No.</th>
                                        <th class="t-align-left" width="27%">File Name</th>
                                        <th class="t-align-left" width="28%">File Description</th>
                                        <th class="t-align-left" width="7%">File Size <br />
                                            (in KB)</th>
                                        <th class="t-align-left" width="18%">Action</th>
                                    </tr>
                                <c:set var="j" value="1"/>
                                <c:forEach items="${complainDocs}" var="docs" >
                                    <c:if test="${history.userRole.userTypeId == docs.userRole.userTypeId}">
                                   <tr>
                                        <td  class="t-align-center">${j}</td>
                                        <c:set var="j" value="${j+1}"/>
                                        <td>${docs.docName}</td>
                                        <td>${docs.docDescription}</td>
                                        <td class="t-align-center">
                                            <fmt:formatNumber type="number" maxFractionDigits="2" value="${docs.docSize/1024}"/>
                                        </td>
                                        <td  class="t-align-center"><a href="download.htm?docname=${docs.docName}&tenderId=${tenderId}&complaintId=${complaint.complaintId}">Download</a></td>
                                    </tr>

                                    </c:if>
                                </c:forEach>
                                </table>
                            </td>
                        </tr>

                    </c:if>
                </c:forEach>
            </table>
                

            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->

            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
