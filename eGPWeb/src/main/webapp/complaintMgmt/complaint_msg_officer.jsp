<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.Calendar" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>complaint-management system</title>
<link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../css/home.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
</head>
<body >
    
  <div  class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
  <!--Dashboard Header Start-->
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  

  
   <%
               
               // Variable tenderId is defined by u on ur current page.
            
    String c_tenderid = ""; 
    if (request.getParameter("tenderid") != null && !"null".equalsIgnoreCase(request.getParameter("tenderid"))) {
                c_tenderid = request.getParameter("tenderid");
    } else if (request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId"))) {
                c_tenderid = request.getParameter("tenderId");
    }
                pageContext.setAttribute("tenderId",c_tenderid); 
  %>
        
            <!--Dashboard Header Start-->
           
            <!--Dashboard Header End-->

            <!--Dashboard Content Part Start-->
            <div class="DashboardContainer">
             <div class="pageHead_1"><fmt:message key="LBL_VIEW_COMPLAINT_DETAIL"/><span class="c-alignment-right">
			 
			 <%if(session.getAttribute("procurementRole").toString().equals("PE")) {%>
			<a href="TendererDashboard.htm?tenderId=<%=c_tenderid%>" class="action-button-goback"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a></span></div>
           <%}else if(session.getAttribute("procurementRole").toString().equals("HOPE")){%>
           <a href="complaintOfficerHope.htm" class="action-button-goback"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a></span></div> 
			<%}else if(session.getAttribute("procurementRole").toString().equals("Secretary")){%>
           <a href="complaintOfficerHope.htm" class="action-button-goback"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a></span></div>
             <%}%> 
			 
			 
			 
			

            <div class="responseMsg successMsg" style="margin-top:12px;">Your comments have been sent to the tenderer for compalintId:${complaintId}</div>
                   <!--Dashboard Footer Start-->
           
            <!--Dashboard Footer End-->
           </div>
<%@ include file="../resources/common/Bottom.jsp" %>
        </div>
        </body>
</html>
