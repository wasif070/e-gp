<%--
    Document   : ViewInstructionRP
    Created on : Feb 15, 2012, 2:19:46 PM
    Author     : shreyansh Jogi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />


<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Instruction</title>
    </head>
    <body>
        <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_1 t_space tableView_1">
           <tr>
               <!-- Hide 
               <td style="color:Red;">
                    60. Disposal of Appeal by Review Panels :- 
                    <ol style="padding-left:25px;">
                         <li style="margin:2px;">The Review Panel, upon receiving a
complaint through the CPTU and having been properly provided with the Security Deposit and
Registration Fee , shall advise the Procuring Entity to continue the suspension of the issuance of
Notification of Award until such time as the decisions of the Review Panel have been announced.
</li>
<li style="margin:2px;">The Review Panel shall within the period specified in Schedule II, issue a written
decision to the Person with a copy to the Secretary of the concerned Ministry or Division, the
CPTU and the Procuring Entity.
</li>
<li style="margin:2px;">Unless it dismisses the complaint as being frivolous and as applicable, in the case
of forfeiture of the Security Deposit of the Person, in the disposal of appeal the Review Panel
may take, either any or in combination thereof of the following decisions, as deemed appropriate -
</li>
                         <ol style="list-style-type:lower-alpha; padding-left:25px;">
                             <li style="margin:2px;">reject the appeal , stating its reasons and suggest that a Procuring Entity
continue with Procurement proceedings; or
</li>
                             <li style="margin:2px;">state the Rules or principles that govern the subject matter of the appeal and
advise the parties to act accordingly for its disposal ; or
</li>
                             <li style="margin:2px;">recommend remedial measures if the Procuring Entity has taken action
contrary to its obligations under the Rules; or
</li>
                             <li style="margin:2px;">suggest annulment in whole or in part of a non-compliant action or decision of a
Procuring Entity, other than any action or decision bringing the Procurement
contract into force; or
</li>
                             <li style="margin:2px;">suggest the payment of compensation by a Procuring Entity for costs incurred
by the Person, such as, cost of preparation of Tender Document and expenses
associated with legal fees and other expenses incurred in lodging his or her
complaint, including the return of the Security Deposit paid under Rule 57 (12)
</li>
                             <li style="margin:2px;">, if a Procuring Entity is in breach of its obligations under these Rules; orrecommend that the Procurement proceedings be completed.</li>

                         </ol>
<li style="margin:2px;">Decisions of the Review Panel shall be taken on the basis of majority opinion.</li>
<li style="margin:2px;">The decision of the Review Panel shall be final and all concerned parties will act
upon such decision.
</li>
<li style="margin:2px;">After the decision has been issued by the Review Panel, the complaint raised in the
appeal and the decision shall be promptly made available for inspection to the general public,
provided that no information shall be disclosed if its disclosurea)
would be contrary to laws of Bangladesh;
</li>
<ol style="list-style-type:lower-alpha; padding-left:25px;">
    <li style="margin:2px;">would impede law enforcement;</li>
<li style="margin:2px;">would not be in the public interest;</li>
<li style="margin:2px;">would prejudice legitimate commercial interests of the parties; or</li>
<li style="margin:2px;">would inhibit fair competition.</li>


</ol>
<li style="margin:2px;">Any decision by a Procuring Entity or by the Review Panel under this Rule and the
grounds and circumstances thereof shall be made part of the record of the Procurement
proceedings
</li>

                    </ol>
               </td> -->
               
                <td style="color:Red;">
                    Review Panels Formation Instruction to be provided by GPPMD 
               </td>
           </tr>
        </table>
    </body>
</html>
