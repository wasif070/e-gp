<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<HTML>
    <HEAD>
        <TITLE>Creation Of ReviewPanel  </TITLE>
    <%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.*,java.text.*,java.lang.Math" %>
    <%@page import="com.cptu.egp.eps.service.serviceinterface.ComplaintMgmtService"%>
    <%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
    <html>

        <head>

            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>complaint-management system</title>
            <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
            <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
            <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
            <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
            <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
            <link href="../css/home.css" rel="stylesheet" type="text/css" />
            <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
            <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
            <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
            <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
            <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>
            <script>
                function imposeMaxLengthRWPMem(Object, MaxLen, e){    // RISHITA - setting max length for text area
                    var keyValue = (window.event)? e.keyCode : e.which;
                    if(keyValue == 13){
                        return false;
                    }
                    if((keyValue == 8 || (Object.value.length < MaxLen))){
                         return true;
                    }
                    return false;
                }
            </script>
        </head>
        <body>

            <%
                ComplaintMgmtService mgmtService = (ComplaintMgmtService) AppContext.getSpringBean("complaintMgmtService");
                int rpId=0;
                if(request.getParameter("rpId")!=null){
                    rpId = Integer.parseInt(request.getParameter("rpId"));
                    }
                int empId = mgmtService.getEmpId(rpId);
                List<Object[]> list = mgmtService.getReviewPanel(empId);

%>
            <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>

                <!--Middle Content Table Start-->
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr valign="top">
                        
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        
                        <td class="contentArea-Blogin">

                            <!--Dashboard Content Part Start-->
                                <%if(request.getAttribute("msg")!=null){
                                if("created".equalsIgnoreCase(request.getAttribute("msg").toString())){
    %>
                                <div class="responseMsg successMsg">Review Panel has been successfully created</div>
                                <%}if("edited".equalsIgnoreCase(request.getAttribute("msg").toString())){
    %>
                                <div class="responseMsg successMsg">Review Panel has been successfully edited</div>
                                <%}
                                }%>
                                <div	class="pageHead_1 t_space"><%if(request.getParameter("isEdit")!=null){ out.print("Edit Review Panel Name");}else{ out.print("Review Panel Name");}%></div></div>
                                <form id="frmGovUser" method="post">
                                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" >
                                        <tr>
                                            <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                        </tr>
                                        <tr>
                                            <td width="200" class="ff">Email Id : <span>*</span></td>
                                            <td>
                                                <%if(request.getParameter("isEdit")!=null && list != null){
                                                        if("approved".equalsIgnoreCase("approved")){ //need to put employee status
                                                        out.print((String)list.get(0)[0]);%>
                                                 <input name="emailId" id="emailId" type="hidden" value="<%=(String) list.get(0)[0]%>" /><%}else{%>
                                               
                                                        <input name="emailId" id="emailId" type="text" class="formTxtBox_1"  style="width:200px;" onblur="return checkMail();" value="<%=(String) list.get(0)[0]%>" />
                                                        <span id="mailMsg" style="color: red; font-weight: bold"></span>
                                                        <%}}else{%>
                                                        <input name="emailId" id="emailId" type="text" class="formTxtBox_1"  style="width:200px;" onblur="return checkMail();" />
                                                        <span id="mailMsg" style="color: red; font-weight: bold"></span>
                                                        <%}if(request.getParameter("isEdit")==null){%>
                                                        <span id="phno" style="color: grey;"><br />Please enter Official and Designation specific e-mail ID i.e. ce@gppmd.gov.bt</span> <%-- .bd to .bt--%>
                                                        <%}%>
                                            </td>
                                        </tr>
                                         <%if(request.getParameter("isEdit")==null){%>
                                        <tr>
                                            <td width="200" class="ff">Password : <span>*</span></td>
                                            <td><input name="password" id="txtPass" type="password" class="formTxtBox_1" style="width:200px;" maxlength="25" value="" autocomplete="off" /><br/><span id="tipPassword" style="color: grey;"> (Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. 
                                                <br/>Special characters may be added) </span>
                                            </td>
                                        </tr>
                                       
                                        <tr>
                                            <td width="200" class="ff">Confirm Password : <span>*</span></td>
                                            <td><input name="confPassword" id="txtConfPass" type="password" class="formTxtBox_1"
                                                       style="width:200px;" onpaste="return passMsg();" autocomplete="off" /></td>
                                        </tr>
                                         <%}%>
                                        <tr>
                                            <td width="200" class="ff">Full Name : <span>*</span></td>
                                            <td><input name="fullName" id="txtaFullName" type="text" class="formTxtBox_1"
                                                       value='<%if(request.getParameter("isEdit")!=null && list != null){ out.print((String)list.get(0)[2]);} else{%><%}%>' style="width:200px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="200" class="ff">Name in Dzongkha : </td>
                                            <td><input name="bngName" id="txtBngName" type="text" class="formTxtBox_1"
                                                       value='<%if(request.getParameter("isEdit")!=null && list != null){
                                                           if(list.get(0)[3]!=null){
                                                           //out.print(new String());
                                                           out.print(""+ new String(((byte[])list.get(0)[3]),"UTF-8"));
                                                            }
                                                        } else{%><%}%>' style="width:200px;" maxlength="101" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="200" class="ff">CID No.: </td>
                                            <td><input name="ntnlId" id="txtNtnlId" type="text" class="formTxtBox_1"
                                                       style="width:200px;" maxlength="26"
                                                       onblur="return checkNationalId();"
                                                       value='<%if(request.getParameter("isEdit")!=null && list != null){ out.print(list.get(0)[5]);} else{%><%}%>' />
                                                <span id="natMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="200" class="ff">Mob.No. : </td>
                                            <td><input name="mobileNo" onchange="checkMobileNo();" maxlength="16" id="txtMob" type="text" class="formTxtBox_1"
                                                       style="width:200px;"
                                                       value='<%if(request.getParameter("isEdit")!=null && list != null){ out.print(list.get(0)[4]);} else{%><%}%>' /><span id="mNo" style="color: grey;"> (Mobile No. format should be e.g 12345678)</span>
                                                <span id="mobMsg" style="color: red; font-weight: bold">&nbsp;</span></td>
                                        </tr>
                                        <tr>
                                            <td width="200" class="ff">Review Panel Name :<span>*</span></td>
                                            <td><input type="text" class="formTxtBox_1" 
                                                                   name="reviewPanelName"
                                                                    value='<%if(request.getParameter("isEdit")!=null && list != null){ out.print((String)list.get(0)[7]);} else{%><%}%>'
                                                                   id="reviewPanelName" /></td>
                                        </tr>
                                        <tr>
      <td class="t-align-left ff" valign="top">Instruction :</td>
    <td>
        <a onclick="javascript:window.open('ViewInstructionRP.htm', '', 'width=800px,height=600px,scrollbars=1','');" href="javascript:void(0);">View</a>

    </td></tr>
                                        <tr>
                                            <td width="200" width="200" class="ff">Review Panel Member's :<span>*</span></td>
                                            <td>
                                                <textarea cols="20" rows="6" class="formTxtBox_1"  id="rwPanelMem" name="rwPanelMem"
                                                    style="width: 400px" onkeypress="return imposeMaxLengthRWPMem(this, 301, event);"><%
                                                        if(request.getParameter("isEdit")!=null && list != null){
                                                            out.print((String)list.get(0)[9]);
                                                        }
                                                    %></textarea>
                                                    <br/>
                                                    <span id="tipRWMember" style="color: grey;"> (Please enter comma separated name) </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            <td>
                                                    <%if(request.getParameter("isEdit")==null){%>
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnSubmit" id="btnSubmit" value="Submit" onclick="return checkData();" />
                                                        </label>
                                                        <%}else{%>
                                                        &nbsp;
                                                        <label class="formBtn_1">
                                                            <%--<form action="GovUserCreateRoleOff.html" method="POST">--%>
                                                                <input type="submit" name="btnUpdate" id="btnUpdate" value="Update" onclick="return checkData();"/>
                                                                <input type="hidden" name="userId" id="userId" value="<%=list.get(0)[8]%>" />
                                                                <input type="hidden" name="empId" id="empId" value="<%=empId %>" />
                                                                <input type="hidden" name="isEdit" id="isEdit" value="edit" />
                                                                <input type="hidden" name="rpId" id="rpId" value="<%=rpId%>" />
                                                            <%--</form>--%>
                                                        </label>
                                                        <%}%>

                                        </tr>
                                    </table>
                                </form>
                        </td>
                    </tr></table>
                            </div>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $("#frmGovUser").validate({
                            rules: {
                                emailId: { required: true, email: true },
                                password: {spacevalidate: true, requiredWithoutSpace: true , minlength: 8, maxlength: 25 ,alphaForPassword : true },
                                confPassword: { required: true, equalTo: "#txtPass" },
                                fullName: {requiredWithoutSpace: true , alphaName:true, maxlength:100 },
                                ntnlId: { number: true,maxlength: 25,minlength: 13},
                                mobileNo: {number: true, minlength: 8, maxlength:8 },
                                bngName:{ maxlength:100},
                                rwPanelMem: {required: true, maxlength: 300 },
                                reviewPanelName:{requiredWithoutSpace: true , alphaName:true, maxlength:100 }

                            },
                            messages: {
                                emailId: { required: "<div class='reqF_1'>Please enter e-mail ID</div>",
                                    email: "<div id='e1' class='reqF_1'>Please Enter Valid e-mail ID</div>"
                                },
                                password: { spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                                    requiredWithoutSpace: "<div class='reqF_1'>Please enter Password</div>",
                                    alphaForPassword : "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>",
                                    minlength: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>",
                                    maxlength:"<div class='reqF_1'>Maximum 25 characters are allowed</div>"
                                },
                                confPassword:{ required: "<div class='reqF_1'>Please retype Password</div>",
                                    equalTo: "<div class='reqF_1'>Password does not match. Please try again</div>"
                                },
                                fullName: { //spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                                    requiredWithoutSpace: "<div class='reqF_1'>Please enter Full Name</div>",
                                    alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only </div>",
                                    maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"},

                                //                        ntnlId: { required: "<div class='reqF_1'>Please enter National ID</div>",
                                //                            number:"<div class='reqF_1'>Please enter Numbers Only</div>" ,
                                //                            maxlength: "<div class='reqF_1'>Maximum 25 digits are allowed</div>",
                                //                            minlength: "<div class='reqF_1'>Please enter minimum 13 digits</div>"
                                //
                                //                        },
                                ntnlId: {
                                    number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                                    maxlength: "<div class='reqF_1'>CID No. should comprise of maximum 25 digits</div>",
                                    minlength: "<div class='reqF_1'>CID No. should comprise of minimum 13 digits</div>"
                                },

                                phoneNo: { required: "<div class='reqF_1'>Please enter Phone No.</div>",
                                    number: "<div class='reqF_1'>Please enter Numbers Only</div>",
                                    maxlength: "<div class='reqF_1'>Maximum 20 digits are allowed</div>",
                                    minlength: "<div class='reqF_1'>Please enter Minimum 6 Digits</div>"
                                },

                                //                        mobileNo: { required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                                //                            number: "<div class='reqF_1'>Please enter Numbers Only</div>",
                                //                             minlength:"<div class='reqF_1'>Minimum 10 digits required.</div>",
                                //                            maxlength:"<div class='reqF_1'>Maximum 15 digits are allowed.</div>"
                                //                        },
                                mobileNo: {
                                    number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" ,
                                    minlength:"<div class='reqF_1'>Minimum 8 digits are required</div>",
                                    maxlength:"<div class='reqF_1'>Allows maximum 8 digits only</div>"
                                },

//                                faxNo: { numberWithHyphen: "<div class='reqF_1'>Allows numbers (0-9) and hyphen (-) only</div>",
//                                    PhoneFax:"<div class='reqF_1'>Please enter valid Phone No. Area code should contain min 2 digits and max 5 digits. Phone no. should contain min 3 digits and max 10 digits</div>"},
                                bngName:{
                                    maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>"
                                },
                                rwPanelMem: {
                                    required: "<div class='reqF_1'>Please enter Review Panel Member's</div>",
                                    maxlength:"<div class='reqF_1'>Allows maximum 300 characters only</div>"
                                },
                                reviewPanelName: { //spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                                    requiredWithoutSpace: "<div class='reqF_1'>Please enter Review Panel Name</div>",
                                    alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only </div>",
                                    maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"}

                            },
                            errorPlacement:function(error ,element)
                            {
                                if(element.attr("name")=="password")
                                {
                                    error.insertAfter("#tipPassword");
                                }
                                else if(element.attr("name")=="mobileNo")
                                {
                                    error.insertAfter("#mobMsg");
                                }
                                else
                                {
                                    error.insertAfter(element);

                                }
                            }
                        }
                    );
                    });
                </script>

                <script type="text/javascript">

                    function numeric(value) {
                        return /^\d+$/.test(value);
                    }

                    function checkMail(){
                        var mailflag=false;
                        // alert('::'+document.getElementById("txtMail").value+'::');

                        if($.trim(document.getElementById("emailId").value) != "" ){
                            $('span.#mailMsg').css("color","red");
                            $('span.#mailMsg').html("Checking for unique Mail Id...");
                            $.post("<%=request.getContextPath()%>/CommonServlet", {mailId:$.trim($('#emailId').val()),funName:'verifyMail'}, function(j){
                                if(j == "OK"){
                                    $('span.#mailMsg').css("color","green");
                                    mailflag = true;
                                }else{
                                    $('span.#mailMsg').css("color","red");
                                    document.getElementById("emailId").value="";
                                }
                                $('span.#mailMsg').html(j);
                      
                            });
                        }
                    }


                    function checkNationalId(){
                        var flagNat=false;
                        if($.trim(document.getElementById("txtNtnlId").value) != "" ){
                     
                            $('span.#natMsg').css("color","red");
                    // $('span.#natMsg').html("Checking f        or unique National Id...");
                            $.post("<%=request.getContextPath()%>/CommonServlet",{nationalId:$.trim($('#txtNtnlId').val()),funName:'verifyNationalId'},function(j){
                                if(j == "OK"){
                                    $('span.#natMsg').css("color","white");
                                    flagNat = true;
                                }
                                else{
                                    $('span.#natMsg').css("color","red");
                                    flagNat = false;
                                }
                                $('span.#natMsg').html(j);
                                return flagNat;
                            });
                        }
                    }



                    function checkMobileNo(){
                        var flagMob=false;
                        if($.trim(document.getElementById("txtMob").value) != "" && $.trim(document.getElementById("txtMob").value.length) >9 &&$.trim(document.getElementById("txtMob").value.length) <16 && numeric($.trim($('#txtMob').val())) ){
                            //$('#txtMob').val($.trim($('#txtMob').val()));

                            $('span.#mobMsg').css("color","red");
                    $('span.#mobMsg').html("Checking for unique Mobile No...");
                            $.post("<%=request.getContextPath()%>/CommonServlet",{mobileNo:$.trim($('#txtMob').val()),funName:'verifyMobileNo'},function(j){
                                if(j == "OK"){
                                    $('span.#mobMsg').css("color","green");
                                    flagMob = true;
                                }
                                else{
                                    $('span.#mobMsg').css("color","red");
                                    flagMob = false;
                                }
                                $('span.#mobMsg').html(j);
                                if(document.getElementById("m1") != null){
                                    document.getElementById("m1").innerHTML="";
                                }
                                return flagMob;
                            });
                           
                        }else{
                            //$('#txtMob').val($.trim($('#txtMob').val()));
                            document.getElementById("mobMsg").innerHTML="";
                        }
                    }

                    function checkData(){
                        //var isMail=false;
                        if($('#frmGovUser').valid()){
                        //$('#btnSubmit').attr("disabled", true);
                        $(".err").remove();
                        //                    if($('#txtaFullName').val().length !=0 && $.trim($('#txtaFullName').val()).length == 0){
                        //                        $("#txtaFullName").parent().append("<div class='err' style='color:red;'>Only space is not allowed</div>");
                        //                    }else{
                        //document.frmGovUser.action="submitReviewPanel.htm";
                        //document.frmGovUser.submit();
                        document.getElementById("frmGovUser").action="submitReviewPanel.htm";
                        document.getElementById("frmGovUser").submit();
                        // }
                        //return true;
              
                        }
                    }


                </script>

                <!--Dashboard Footer Start-->
                <%@include file="../resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
        </body>

    </html>
