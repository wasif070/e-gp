<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.*,java.text.*,java.util.Calendar" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<html >
     <%
	response.setHeader("Cache-Control","no-cache");
	response.setHeader("Cache-Control","no-store");
	response.setHeader("Pragma","no-cache");
    %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>complaint-management system</title>
<link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
		<link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
		<link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
		<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
		<link href="../css/home.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript"> 

</SCRIPT> 
</head>
<body onload="getQueryData('getmyprebidquery');">
   
               
                 
                 <div class="dashboard_div">
  <!--Dashboard Header Start-->
     
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="DashboardContainer">

           
   <%
               
        String tenderId="";
        if(request.getParameter("tenderId") != null){
    	tenderId = request.getParameter("tenderId");
    	
    }
			  // Variable tenderId is defined by u on ur current page.
             pageContext.setAttribute("tenderId",tenderId);
  %>

<form name="uploadedDoc">
                <table width="970" cellspacing="0" class="tableList_3 t_space">
                    <tr>
                        <th width="45"><fmt:message key="LBL_sNO"/></th>
                        <th width="72"><fmt:message key="LBL_LEVEL"/></th>
                        <th width="286"><fmt:message key="LBL_FILE_NAME"/></th>
                      
                        <th width="70"><fmt:message key="LBL_SIZE"/></th>
                        <th width="147"><fmt:message key="LBL_UPLOADED_DATE"/></th>
                        <th width="120"><fmt:message key="LBL_ACTION"/></th>
                    </tr>

					 
 <c:if test="${empty  docsList}">
  <tr> <td colspan="6"><fmt:message key="LBL_NO_REFERENCE_DOCUMENTS_ARE_FOUND"/></td></tr>
 </c:if>

 <c:set var="i" value="1"/>
 <c:if test="${docsList!=null}">
<c:forEach  items="${docsList}" var="doc">
 <c:set var="uploadedDate" scope="session" value="${doc.uploadedDt}"/>      
		       
           
		    <%
			   String cDate = session.getAttribute("uploadedDate").toString();
		       SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
               Date crdate = sdfSource.parse(cDate);
               SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MM-yy hh:mm");
		       cDate = sdfDestination.format(crdate);
          
		   %>
                    <tr>
                      
                        <td>${i}</td> 
						<c:set var="i" value="${i+1}"/>
						<c:set var="level"  scope="session" value="${doc.complaintLevel.complaintLevel}"/>
                        <td>${doc.complaintLevel.complaintLevel}</td>
                        <td>${doc.docName }</td>
                        <td>${doc.docSize}</td>
                        <td><%=cDate%></td>
                        <%int usertypeid=Integer.parseInt(session.getAttribute("userTypeId").toString());%>
					
                        <td><a href="download.htm?docname=${doc.docName}&tenderid=<%=tenderId%>"><fmt:message key="LBL_DOWNLOAD"/></a> <%if(usertypeid!=2 && session.getAttribute("procurementRole").toString().equalsIgnoreCase(session.getAttribute("level").toString())){%> |<a href="delete.htm?docId=${doc.complaintDocId}&tenderId=<%=tenderId%>"  onClick="return confirm('Are you sure you want to delete');"><fmt:message key="LBL_DELETE"/></a><% }%></td>
						
                    </tr>
</c:forEach>
</c:if>
 
                </table>
				</form>
            </div>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
         
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
