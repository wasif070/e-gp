<%-- 
    Document   : Process_complaint_RP
    Created on : Jan 13, 2012, 11:25:34 AM
    Author     : shreyansh Jogi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@page import="java.util.*,java.text.*" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Complaints Management</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery.alerts.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="../js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript">

            function GetCal(txtname,controlname){
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
        <script type="text/javascript">
            function newDoc()
            {
                var status=document.getElementById("status").value;
                var txtaComplaintDetail=document.getElementById("txtaComplaintDetail").value;
                var uploadstatusvalue;
                var radios = document.peresponse.elements["uploadstatus"];
                for (var i=0; i <radios.length; i++) {
                    if (radios[i].checked) {
                        uploadstatusvalue=radios[i].value;


                    }
                }
                var ResolvedSatisfactoryvalue;
                var radiosres = document.peresponse.elements["ResolvedSatisfactory"];
                for (var i=0; i <radiosres.length; i++) {
                    if (radiosres[i].checked) {
                        ResolvedSatisfactoryvalue=radiosres[i].value;
                    }
                }
                if(document.getElementById("txtaComplaintDetail").value==""){
                    alert("Please comment"+document.getElementById("txtaComplaintDetail").value);
                    document.getElementById("txtaComplaintDetail").focus();
                    return false;
                }
                else{
                    document.forms[0].action ="response.htm?complaintId=<%=request.getParameter("complaintId")%>&ResolvedSatisfactory="+ResolvedSatisfactoryvalue+"&uploadstatus="+uploadstatusvalue+"&status="+status+"&txtaComplaintDetail="+txtaComplaintDetail+"&tenderId=<%=request.getParameter("tenderId")%>&complaintLevelId=${complaint.complaintLevel.complaintLevelId}";
                    document.forms[0].submit();
                    return true;
                }

            }
        </script>
        <script language="javascript" type="text/javascript">

            function textCounter( field, countfield, maxlimit ) {
                if ( field.value.length > maxlimit )
                {
                    field.value = field.value.substring( 0, maxlimit );
                    alert( 'Textarea value can only be '+maxlimit+' characters in length.' );
                    return false;
                }
                else
                {
                    countfield.value = maxlimit - field.value.length;
                }
            }

        </script>

    </head>
    <body>
        <div class="dashboard_div">
            <input id="curDate" value="Sunday, January 16, 2011 11:24:11 AM" type="hidden" />
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->

            <!--Dashboard Content Part Start-->



            <%

                        String tenderId = "";
                        if (request.getParameter("tenderId") != null) {
                            tenderId = request.getParameter("tenderId");
                            session.setAttribute("tenderId", tenderId);
                        }
                        // Variable tenderId is defined by u on ur current page.
                        pageContext.setAttribute("tenderId", tenderId);
            %>



            <!--Dashboard Header End-->

            <!--Dashboard Content Part Start-->

            <div class="DashboardContainer">
                <div class="tabPanelArea_1">
                    <div class="pageHead_1"><fmt:message key="LBL_PROCESS_COMPLAINT"/><span class="c-alignment-right">

                            <a href="TendererDashboard.htm" class="action-button-goback"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a></span></div>


                    <div class="tableHead_1 t_space"><fmt:message key="LBL_COMPLAINT_DETAIL"/></div>
                    <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_1 tableView_1" style="table-layout: fixed">
                        <c:set var="creationDate" scope="session" value="${complaint.complaintCreationDt}"/>


                        <%
                                    Date cDate = (Date) session.getAttribute("creationDate");

                        %>
                        <tr>
                            <td valign="top" colspan="2" class="ff"><fmt:message key="LBL_FIELDS_MARKED_WITH"/> (<span class="mandatory">*</span>) <fmt:message key="LBL_ARE_MANDATORY"/></td>
                        </tr>
                        <tr>
                            <td width="198" valign="top" class="ff"><fmt:message key="LBL_NAME_OF_TENDERER_CONSULTANT"/>: </td>
                            <td width="772" class=""><label id="lblNameofTenderer">
                                    <c:choose>
                                        <c:when test="${complaint.tendererMaster.tblCompanyMaster.companyId==1}">
                                            ${complaint.tendererMaster.firstName} ${complaint.tendererMaster.lastName}
                                        </c:when>
                                        <c:otherwise>
                                            ${complaint.tendererMaster.tblCompanyMaster.companyName}
                                        </c:otherwise>
                                    </c:choose>
                                </label></td>
                        </tr>
                        <tr>
                            <td class="ff" valign="top"><fmt:message key="LBL_COMPLAINT_DATE_TIME"/> : </td>
                            <td class=""><label id="Label1"><%=DateUtils.gridDateToStrWithoutSec(cDate)%></label></td>
                        </tr>
                        <tr>
                            <td class="ff" valign="top"><fmt:message key="LBL_COMPLAINT_FOR"/> : </td>
                            <td class=""><label id="Label2">${complaint.complaintType.complaintType}</label></td>
                        </tr>
                        <tr>
                            <td class="ff" valign="top"><fmt:message key="LBL_COMPLAINT_SUBJECT"/> : </td>
                            <td class="" style="word-wrap:break-word;">${complaint.complaintSubject }</td>
                        </tr>
                        <tr>
                            <td class="ff" valign="top"><fmt:message key="LBL_COMPLAINT_DETAIL"/> : </td>
                            <td class="" style="word-wrap:break-word;">${complaint.complaintDetails}</td>
                        </tr>
                        <tr>
                            <td class="ff" valign="top"><fmt:message key="LBL_REFERENCE_DOCUMENTS"/> : </td>
                            <td class=""><table width="100%" cellspacing="0" class="tableList_3">
                                    <tr>
                                        <th width="7%"><fmt:message key="LBL_sNO"/></th>
                                        <th width="30%"><fmt:message key="LBL_FILE_NAME"/></th>
                                        <th width="44%"><fmt:message key="LBL_DESCRIPTION"/></th>
                                        <th width="10%"><fmt:message key="LBL_SIZE"/></th>
                                        <th width="9%"><fmt:message key="LBL_ACTION"/></th>
                                    </tr>

                                    <c:set var="i" value="1"/>
                                    <c:forEach var="doc" items="${docList}">
                                        <tr>
                                            <td>${i}</td>
                                            <c:set var="i" value="${i+1}"/>
                                            <td>${doc.docName}</td>
                                            <td class="table-description">Reference Doc.</td>
                                            <td>${doc.docSize}</td>
                                            <td><a href="download.htm?docname=${doc.docName}&tenderId=<%=request.getParameter("tenderId")%>&complaintId=${doc.complaintMaster.complaintId}">Download</a></td>
                                        </tr>
                                    </c:forEach>
                                </table></td>
                        </tr>
                    </table>

                    <table width="100%" cellspacing="0" class="tableList_3 t_space">
                        <tr>
                            <th width="44"><fmt:message key="LBL_sNO"/></th>
                            <th width="69"><fmt:message key="LBL_LEVEL"/></th>
                            <th width="246"><fmt:message key="LBL_COMMENTS_OF_TENDERER_CONSULTANT"/></th>
                            <th width="81"><fmt:message key="LBL_RESPONSE"/></th>
                            <th width="128"><fmt:message key="LBL_RESPONSE_DATE_TIME"/></th>

                        </tr>
                        <c:set var="i" value="1"/>

                        <c:forEach  items="${historyList}" var="history">
                            <tr>
                                <td>${i}</td>
                                <c:set var="i" value="${i+1}"/>
                                <td>${history.level}</td>

                                <c:choose>
                                    <c:when test="${history.tendererComments==' '}">
                                        <td style="text-align:center;">--</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td class="table-description"><a target="_blank" href="ViewHistory_tenderer.htm?complaintId=${history.complaintId}&complaintLevelId=${history.levelId}&tenderId=<%=tenderId%>" onClick="return popup(this, 'tendererpopup')">View history</a></td>
                                    </c:otherwise>
                                </c:choose>







                                <c:set var="status" scope="session" value="${history.response}"/>
                                <c:set var="datecomplaint" scope="session" value="${history.responseDate}"/>
                                <%
                                            int escalate = Integer.parseInt(XMLReader.getMessage("DaysforEscalation"));

                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                            String dt = session.getAttribute("datecomplaint").toString(); // Start date

                                            Calendar c = Calendar.getInstance();
                                            Calendar c1 = Calendar.getInstance();

                                            c.setTime(sdf.parse(dt));

                                            c.add(Calendar.DATE, escalate);

                                            if (c1.after(c) && (session.getAttribute("status").toString().equals("Pending"))) {

                                %>
                                <td>No Response</td>
                                <%} else {%>
                                <td>${history.response}</td>
                                <%}%>







                                <%
                                            Date strDate = (Date) session.getAttribute("datecomplaint");
                                %>
                                <td><%=DateUtils.gridDateToStrWithoutSec(strDate)%>
                                </td>

                            </tr>
                        </c:forEach>
                    </table>

                    <form name="peresponse" id="peresponse">
                        <table width="970" cellspacing="5" class="formStyle_1 t_space">
                            <tr>
                                <td class="ff" valign="top"><fmt:message key="LBL_ACTION"/> :&nbsp;<span class="mandatory">*</span></td>
                                <td class=""><select id="status" name="status" >
                                        <option value="Accept">Accept </option>
                                        <option value="Reject">Reject</option>
                                        <option value="Clarification">Seek Clarification</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td width="198" class="ff"> Resolved Status:&nbsp;<span class="mandatory">*</span> </td>
                                <td width="772" class=""><input type="radio" value="Yes" name="ResolvedSatisfactory" checked> Resolved satisfactory
                                    <input type="radio" name="ResolvedSatisfactory" value="No"> Not Resolved </td>
                            </tr>
                            <tr>
                                <td width="198" class="ff" valign="top"><fmt:message key="LBL_COMMENTS"/> :&nbsp;<span class="mandatory">*</span></td>
                                <td width="772" class=""><textarea id="txtaComplaintDetail" name="txtaComplaintDetail" rows="4" class="width_textarea" onkeypress="textCounter(this,this.form.counter,200);" ></textarea></td>
                            </tr>
                            <tr>
                                <td class="ff" width="198"><fmt:message key="LBL_DO_YOU_WANT_TO_UPLOAD_DOCUMENTS"/></td>
                                <td width="772"><input type="radio" name="uploadstatus" id="uploadstatus" value="yes" checked>Yes <input type="radio" name="uploadstatus" id="uploadstatus" value="no">No</td>
                            </tr>

                        </table>
                        <table width="970" cellspacing="5" class="formStyle_1">
                            <tr>
                                <td colspan="2" height="20" class="dashedLine"></td>
                            </tr>
                            <tr>
                                <td width="198"></td>
                                <td width="772" class="t-align-submit"><label class="formBtn_1">
                                        <input type="button"  value="Submit" onClick="newDoc()"/>
                                    </label></td>
                            </tr>

                        </table>
                    </form>
                </div>
                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <%@include file="../resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
                </body>
                </html>
