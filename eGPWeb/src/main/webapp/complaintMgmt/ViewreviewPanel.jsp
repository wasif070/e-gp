<%-- 
    Document   : ViewreviewPanel
    Created on : Jan 9, 2012, 5:41:07 PM
    Author     : shreyansh Jogi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Review Panel</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript">
            //jQuery().ready(function (){
            function loadGrid(){                
                jQuery("#list").jqGrid({
                    url: '<%=request.getContextPath()%>/CMSSerCaseServlet?q=1&action=fetchData',
                    datatype: "xml",
                    height: 250,
                    colNames:['Sl. No.','email Id','Review Panel',"Action"],
                    colModel:[
                        {name:'Sl. No.',index:'Sl. No.', width:20,sortable: false,search:false,align:'center'},
                        {name:'emailId',index:'emailId', width:200,sortable: false},
                        {name:'rpanel',index:'rpanel', width:80,sortable: false, search:false, align:'center'},
                        {name:'action',index:'action', width:50,sortable: false, search:false,align:'center'}
                    ],
                    autowidth:true,
                    multiselect: false,
                    sortable: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    caption: "View Review Panel",
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{search:false,edit:false,add:false,del:false});
           }
        </script>
    </head>
    <body onload="loadGrid();">
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>

                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" ></jsp:include>

                        <td class="contentArea">

                            <div class="pageHead_1">View Review Panel
                            <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('3');">Save as PDF</a></span>
                            </div>
                            <div align="center" class="t_space">
                            <div class="t-align-left ff formStyle_1">To sort click on the relevant column header</div>

                                <table id="list"></table>
                                <div id="page">

                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <!--For Generate PDF  Starts-->
            <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                    String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="ReviewPanel_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="ReviewPanel" />
            </form>
            <!--For Generate PDF  Ends-->
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script type="text/javascript">
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
