<%-- 
    Document   : successPage
    Created on : Jan 19, 2012, 12:21:43 PM
    Author     : yagnesh
--%>

<%
    Integer compId = 0;
    String upStatus = "";
    String tenderId = "";
    if(request.getAttribute("complaintId")!=null){
        compId = (Integer) request.getAttribute("complaintId");
    }
    if(request.getAttribute("upStatus")!=null){
        upStatus = request.getAttribute("upStatus").toString().trim();
    }
    if(request.getAttribute("tenderId")!=null){
        tenderId = request.getAttribute("tenderId").toString().trim();
    }
    //response.sendRedirect("/goToFileUpload.htm?compId="+compId+"&upStatus="+upStatus+"&tenderId="+tenderId);
%>
<html>
    <head>
        <script type="text/javascript">
            function submitForm(frm){
                document.getElementById('frm').submit();
            }
        </script>
    </head>
    <body onload="submitForm('frm')">
        <form id="frm" action="<%=request.getContextPath()%>/goToFileUpload.htm?compId=<%=compId%>&upStatus=<%=upStatus%>&tenderId=<%=tenderId%>" method="post">

        </form>
    </body>
</html>