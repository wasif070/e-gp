<%@page import="com.cptu.egp.eps.dao.generic.Complaint_Level_enum"%>
<%@page import="com.cptu.egp.eps.web.utility.CheckExtension"%>
<!DOCTYPE link PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="fm" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<%@page import="com.cptu.egp.eps.web.utility.AppMessage"%>
<%@page import="com.cptu.egp.eps.model.table.TblLocationMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderSubRightServiceImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.Date,java.text.DateFormat"%>
<%@page import="com.cptu.egp.eps.web.utility.NavigationRuleUtil"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<noscript>
    <meta http-equiv="refresh" content="0; URL=<%= request.getContextPath()%>/JSInstruction.jsp">
</noscript>

<html>
    <head>
        <title>Upload Documents</title>
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <%--<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/ddlevelsmenu.js"></script>
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>

        <link href="<%=request.getContextPath()%>/resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/demo_table.css" />
        <link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/demo_page.css" />

        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    String exterror = "";

                    if (request.getAttribute("errormsg") != null && request.getAttribute("errormsg") != "null") {
                        exterror = request.getAttribute("errormsg").toString();
                    }

        %>
        <script type="text/javascript">
            $(document).ready(function() {
                jQuery.fn.dataTableExt.oPagination.iFullNumbersShowPages = 0;
                $('#exampleTest').dataTable( {
                    "aaSorting": [[ 2, "desc" ]],
                    "aoColumnDefs": [
                        { "bSortable": true, "aTargets": [ 0 ] }
                    ] ,
                    "sPaginationType": "full_numbers",
                    "sDom": 'lpftrip'
                } );
            } );
        </script>




    </head>
    <body>
        <%@include file="../resources/common/AfterLoginTop.jsp"%>
        <script type="text/javascript">
            function newDoc(){
                $('.err').remove();
                var count = 0;
                var browserName=""
                var maxSize = parseInt($('#fileSize').val())*1024*1024;
                var actSize = 0;
                var fileName = "";
                jQuery.each(jQuery.browser, function(i, val) {
                    browserName+=i;
                });
                $(":input[type='file']").each(function(){
                    if(browserName.indexOf("mozilla", 0)!=-1){
                        actSize = this.files[0].size;
                        fileName = this.files[0].name;
                    }else{
                        var file = this;
                        var myFSO;
                        try{
                            myFSO = new ActiveXObject("Scripting.FileSystemObject");
                        }catch(e){
                            alert("If you are using IE and Unable to upload document then follow below steps \n Tools > InternetOptions > Security > Click Custom Level > Group ActiveX Control and Plug ins >Initilize and Script ActiveX Controls not marked as safe for scripting > Select Prompt or Enable");
                        }
                        var filepath = file.value;
                        var thefile = myFSO.getFile(filepath);
                        actSize = thefile.size;
                        fileName = thefile.name;
                    }
                    if(parseInt(actSize)==0){
                        $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                        count++;
                    }
                    if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                        $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                        count++;
                    }
                    if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                        $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                        count++;
                    }
                    if(document.getElementById("file").value==""){
                        $(this).parent().append("<div class='err' style='color:red;'>Please upload document. </div>");
                        document.getElementById("file").focus();
                        count++;
                    }
                    if(document.getElementById("txtuploaddesc").value==""){
                         jAlert("Please enter description","Complaint Management", function(RetVal) {
                    });
                        document.getElementById("txtuploaddesc").focus();
                        count++;
                    }

                  });
                if(count==0){
                      if(<%=userTypeId%> == 3)
                    {
                        document.forms[1].action="peFileUpload.htm?complId=${complaintObject.complaintId}";
                        document.forms[1].submit();
                    }
                    else {
                          //  $('#btnUpld').attr("disabled", "disabled");
                        document.forms[1].action = "/tenderer/fileUpload.htm?complId=${complaintObject.complaintId}&tenderId=${complaintObject.tenderMaster.tenderId}";
                        document.forms[1].submit();
                    }
                    return true;
                }else{
                    return false;
                }
            }




        </script>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="DashboardContainer">
                <div class="tabPanelArea_1">
                    <div class="pageHead_1">Upload Documents<span class="c-alignment-right">
                            <a class="action-button-goback" href="<%=request.getContextPath()%>/tenderer/complaint.htm?tenderId=${complaintObject.tenderMaster.tenderId}"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a></span>
                    </div>
                    <% if (exterror == "") {

                    %>
                    <c:choose>
                        <c:when test="${complaintObject.complaintLevel.complaintLevelId==4}">
                            <div class="responseMsg successMsg">Your Complaint ID - ${complaintObject.complaintId} request has been sent to Review Panel, please pay Complaint Registration Fees and Complaint Security fees for further processing.</div>
                        </c:when>
                        <c:otherwise>
                            <div class="responseMsg successMsg" style="margin-top:12px;">Your Complaint ID : ${complaintObject.complaintId} has been successfully sent to ${complaintObject.complaintLevel.complaintLevel}</div>
                        </c:otherwise>
                    </c:choose>

                    <% }%>
                    <% if (exterror != "") {
                                    // While Displaying msg File Deleted Sucessfully it was showing error icon to solve that problem here
                                    // we add patch so it will display succssmsg icon
                                    if (exterror.equalsIgnoreCase("Del")) {
                    %>
                    <div class="responseMsg successMsg" style="margin-top:12px;">File Deleted Successfully</div>
                    <% } else if (exterror.equalsIgnoreCase("typecheck")) {%>
                    <div class="responseMsg errorMsg" style="margin-top:12px;">This type of file is not allowed. </div>
                    <% } else if (exterror.equalsIgnoreCase("fileexist")) {%>
                    <div class="responseMsg errorMsg" style="margin-top:12px;">File already exist. </div>
                    <% } else if (exterror.equalsIgnoreCase("fileerror")) {%>
                    <div class="responseMsg errorMsg" style="margin-top:12px;">Error occurred during file Upload. </div>
                    <% } else if (exterror.equalsIgnoreCase("upload")) {%>
                    <div class="responseMsg successMsg" style="margin-top:12px;">File Uploaded Successfully</div>
                    <% }
                        }%>

                    <table width="100%" cellspacing="5" cellpadding="0" class="tableList_3 t_space" style="table-layout: fixed" >
                        <tr>
                            <td width="320" class="ff"><fmt:message key="LBL_COMPLAINT_ID"/></td>
                            <td width="330" class="ff"><fmt:message key="LBL_COMPLAINT_SUBJECT"/></td>
                            <td width="320" class="ff"><fmt:message key="LBL_TENDER_ID"/></td>
                        </tr>
                        <tr>
                            <td><c:out value="${complaintObject.complaintId}"/></td>
                            <td style="word-wrap:break-word;"><c:out value="${complaintObject.complaintSubject}"/></td>
                            <td><c:out value="${complaintObject.tenderMaster.tenderId}"/></td>
                        </tr>
                    </table>
                    <br />
                    <font color="red"> Note :- If you have high volume of document/data, you can submit documents in physical form to CPTU/Review Panel</font>
                    <form action="" method="post" enctype="multipart/form-data">
                        
                                    <table width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                                        <tr>
                                            <td  class="ff" width="15%">Select a file to upload : <font color="red">*</font></td>
                                            <td ><input type="file" class="formTxtBox_1" name="file" id="file" style="width:250px; background-color: white; "/> </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="15%"> Descriptions :<font color="red">*</font> </td>
                                            <td><input type="text" value="" name="uploaddesc" id="txtuploaddesc" size="50" class="formTxtBox_1" style="width:200px;"/> </td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="15%"></td>
                                            <td>  <label class="formBtn_1">
                                                    <input type="button"  value="<fmt:message key="BTN_UPLOAD"/> " onclick="newDoc()"/></label> </td>
                                        </tr>
                                    </table>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="100%"  class="t-align-left">Instructions</th>
                            </tr>
                            <tr>
                                <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("tenderer");%>
                                <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                    <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/></td>
                            </tr>
                            <tr>
                                <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                            </tr>
                            <tr>
                                <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                            </tr>
                        </table>
                    </form>
                    <c:if test="${uploadedDocs!=null}" >
                        <table  cellspacing="5" cellpadding="0" border="0" class="tableList_3  display"  id="exampleTest"  width="100%">
                            <thead>
                                <tr>
                                    <th width="7%"><fmt:message key="LBL_FILE_ID"/></th>
                                    <th width="20%"><fmt:message key="LBL_FILE_NAME"/></th>
                                    <th width="33%"><fmt:message key="LBL_DESCRIPTION"/></th>
                                    <th width="20%"><fmt:message key="LBL_UPLOAD_BY"/></th>
                                    <th width="11%"><fmt:message key="LBL_SIZE"/></th>
                                    <th width="9%"><fmt:message key="LBL_ACTION"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var = "doc" items = "${uploadedDocs}">
                                    <c:set value="Test" var="uploadby"/>
                        <c:choose>
                            <c:when test="${doc.userRole.userTypeId == 2}">
                                <c:set value="Tenderer" var="uploadby"/>
                            </c:when>
                            <c:when test="${doc.complaintLevel.complaintLevelId == 1}">
                                <c:set value="PE" var="uploadby"/>
                            </c:when>
                            <c:when test="${doc.complaintLevel.complaintLevelId == 2}">
                                <c:set value="HOPE" var="uploadby"/>
                            </c:when>
                            <c:when test="${doc.complaintLevel.complaintLevelId == 3}">
                                <c:set value="Secretary" var="uploadby"/>
                            </c:when>
                            <c:when test="${doc.complaintLevel.complaintLevelId == 4}">
                                <c:set value="Review Panel" var="uploadby"/>
                            </c:when>
                        </c:choose>
                                    <tr><td>${doc.complaintDocId }</td>
                                        <td> ${doc.docName }</td>
                                        <td> ${doc.docDescription} </td>
                                        <td><c:out value="${uploadby}"></c:out> </td>
                                        <td>  ${doc.docSize } </td>
                                        <td><a href="<%=request.getContextPath()%>/tenderer/download.htm?docname=${doc.docName }&tenderId=${complaintObject.tenderMaster.tenderId}&complaintId=${complaintObject.complaintId}"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                                <c:set var="docUserTypeId" scope="session" value="${doc.userRole.userTypeId}"/>
                                                <c:set var="docComplaintLevelId" scope="session" value="${doc.complaintLevel.complaintLevelId}"/>
                                                <%
                                                            String level_id = "";
                                                            if (userTypeId == 3) {

                                                                Object acc_objProRole = session.getAttribute("procurementRole");
                                                                String acc_strProRole = "";
                                                                if (acc_objProRole != null) {
                                                                    acc_strProRole = acc_objProRole.toString();
                                                                }
                                                                String acc_chk = acc_strProRole;
                                                                String acc_ck1[] = acc_chk.split(",");
                                                                for (int acc_iAfterLoginTop = 0; acc_iAfterLoginTop < acc_ck1.length; acc_iAfterLoginTop++) {
                                                                    if (acc_ck1[acc_iAfterLoginTop].equalsIgnoreCase("PE")) {
                                                                        level_id = "1";
                                                                        break;
                                                                    } else if (acc_ck1[acc_iAfterLoginTop].equalsIgnoreCase("HOPE")) {
                                                                        level_id = "2";
                                                                        break;
                                                                    } else if (acc_ck1[acc_iAfterLoginTop].equalsIgnoreCase("SECRETARY")) {
                                                                        level_id = "3";
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            if (Integer.parseInt(session.getAttribute("docUserTypeId").toString()) == userTypeId) {
                                                                if (userTypeId == 3) {
                                                                    if (level_id.equalsIgnoreCase(session.getAttribute("docComplaintLevelId").toString())) {
                                                %>
                                            &nbsp;&nbsp;<a href="<%=request.getContextPath()%>/deletePopUp.htm?docId=${doc.complaintDocId}&tenderId=${complaintObject.tenderMaster.tenderId}&complaintId=${complaintObject.complaintId}&docname=${doc.docName }" onClick="return confirm('Are you sure you want to delete');"><img src="<%=request.getContextPath()%>/resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                                <%}
                                                                                    } else {%>
                                            &nbsp;&nbsp;<a href="<%=request.getContextPath()%>/deletePopUp.htm?docId=${doc.complaintDocId}&tenderId=${complaintObject.tenderMaster.tenderId}&complaintId=${complaintObject.complaintId}&docname=${doc.docName }" onClick="return confirm('Are you sure you want to delete');"><img src="<%=request.getContextPath()%>/resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                                                <%}
                                                }%>
                                        </td>

                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>

                    </c:if>


                </div>
            </div>
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->

        </div>

    </body>
</html>