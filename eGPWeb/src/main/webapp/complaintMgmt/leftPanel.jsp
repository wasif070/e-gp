<%-- 
    Document   : leftPanel
    Created on : Jan 9, 2012, 2:47:06 PM
    Author     : shreyansh Jogi
--%>

<%@page import="org.hibernate.usertype.UserType"%>
<%@page import="com.cptu.egp.eps.web.utility.NavigationRuleUtil"%>
<html>
    <head>

               <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
       <script type="text/javascript" src="../../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../../resources/js/jQuery/jquery.validate.js"></script>
       <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<script type="text/javascript" src="../resources/js/simpletreemenu.js"></script>

<script type="text/javascript">

            $(document).ready(function(){

                var status=ddtreemenu.getCookie('treemenu1');
                if(status=="none open")
                {
                    document.getElementById("status").innerHTML='+ Expand';
                }
                else
                {
                    document.getElementById("status").innerHTML='- Collapse';
                }
            });
 </script>
<script>
    function redirectToPage(action,whereToGo) {
        document.getElementById("hdnUserType").value = action;
        document.forms[0].action = whereToGo;
        document.forms[0].submit();
    }

    function toggleTree()
    {
        var status=document.getElementById("treeStatus").value;
        if(status=="collapse")
        {
            status="expand";
            document.getElementById("status").innerHTML='- Collapse';
        }
        else if(status=="expand")
        {
            status="collapse";
            document.getElementById("status").innerHTML='+ Expand';
        }

        document.getElementById("treeStatus").value=status;
        ddtreemenu.flatten('treemenu1', status);
    }
</script>
    </head>
    <input type="hidden" name="hdnUserType" id="hdnUserType">
    <input type="hidden" id="treeStatus" name="treeStatus" value="collapse">
<td class="lftNav">
    <table width="100%" cellspacing="0">
        <tr valign="top">
            <td class="lftNav">
                <div class="treemenu-controler"><a onclick="toggleTree()" href="javascript:void(0);"><span id="status">+ Expand</span></a> <%--&nbsp; | &nbsp; <a href="javascript:ddtreemenu.flatten('treemenu1', 'contact')">- Collapse</a>--%></div>
                <ul id="treemenu1" class="treeview TreeViewBorder">
                    <li><a href="#">Review Panel</a>
                        <ul>
                            <li><a id="lblMinistryCreation" href="/admin/reviewPanel.htm">Create</a></li>
                            <li><a id="lblMinistryCreation" href="/admin/ViewreviewPanel.htm">View</a></li>
                             </ul>
                    </li>
                </ul>
            </td>

                <script type="text/javascript">
                    //ddtreemenu.createTree(treeid, enablepersist, opt_persist_in_days (default is 1))
                    ddtreemenu.createTree("treemenu1", false)
                </script>


        </tr>
    </table>
                    </td>