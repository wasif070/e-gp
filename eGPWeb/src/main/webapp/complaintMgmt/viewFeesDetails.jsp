<%-- 
    Document   : viewFeesDetails
    Created on : Jan 27, 2012, 12:43:09 PM
    Author     : shreyansh Jogi
--%>

<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="javax.mail.SendFailedException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Locale"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.text.DateFormat" %>
<%@page import="java.util.Date" %>
<%@page import="java.lang.String" %>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
             Complaint <%=request.getParameter("feetype")%> Fees Payment
        </title>
		<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
		<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
		<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
		<link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
		<link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
		<link href="../css/home.css" rel="stylesheet" type="text/css" />

	

    </head>
    <body>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="contentArea_1">
                <br />
                <div class="pageHead_1">
                   Complaint <%=request.getParameter("feetype") %> Fees Payment
					
                </div>
                <div class="tabPanelArea_1 t_space">
                    <form id="paymentfee" name="paymentfee" method="POST">
					
                        <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                            <tr>
                                <td class="ff">Email Id:</td>
                                <td>${paydetails.complaintMaster.tendererMaster.tblLoginMaster.emailId}
								</td>
                            </tr>
                            <tr id="trBankNm">
                                <td class="ff"><fmt:message key="LBL_BANK_NAME"/> :
                                <td>${paydetails.bankName}</td>

                            </tr>
                            <tr id="trBranch">
                                <td class="ff"><fmt:message key="LBL_BRANCH_NAME"/> :
                                <td>${paydetails.branchName} </td>
                            </tr>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_PAYMENT_FOR"/> : </td>
                                <td>${paydetails.paymentFor}</td>
                            </tr>

                            <tr>
                                <td class="ff" width="17%"><fmt:message key="LBL_CURRENCY"/> : <span class="mandatory"></span></td>
                                <td width="83%">${paydetails.currency}
                                </td>
                            </tr>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_AMOUNT"/> :</td>
                                <td>
								<label id="lblCurrencySymbol">
								<c:choose>
								<c:when test="${paydetails.currency=='BTN'}">
									<fmt:message key="LBL_NU"/>
								</c:when>
								<c:otherwise>
									<fmt:message key="LBL_DOLLAR"/>
								</c:otherwise>
								</c:choose>

								</label>
								<c:choose>
								<c:when test="${paydetails.paymentFor == 'Registration Fee'}">
									<%=(XMLReader.getMessage("localComplaintRegistrationFee"))%>
								</c:when>
								<c:otherwise>
									<%=(XMLReader.getMessage("localComplaintSecurityFee"))%>
								</c:otherwise>
								</c:choose>
							</td>
                            </tr>
                            <tr>
                                <td class="ff" width="17%"><fmt:message key="LBL_MODE_PAYMENT"/>  : </td>
                                <td width="83%">${paydetails.paymentMode}

                                </td>
                            </tr>
							<c:if test="${paydetails.paymentMode == 'Pay Order'}">
                            <tr id="trInsRefNo">
                                <td class="ff">
                             <fmt:message key="LBL_INSTRUMENT_NUMBER"/>:
                                   </td>
                                <td>${paydetails.instRefNumber}
                                </td>
                            </tr>

                            <tr id="trIssuanceBankNm">
                                <td class="ff"> <fmt:message key="LBL_ISSUING_BANK"/> : </td>
                                <td>${paydetails.issuanceBank}
                                    </span>
                                </td>

                            </tr>
                            <tr id="trIssuanceBranch">
                                <td class="ff">
                                 <fmt:message key="LBL_ISSUANCE_BRANCH"/>  :
                                    </td>
                                <td>${paydetails.issuanceBranch}
                                </td>
                            </tr>

                            <tr id="trIssuanceDt">
                                <td class="ff"><fmt:message key="LBL_ISSUANCE_DATE"/>:</td>
                                <td>
                                    </span>
                                </td>
                            </tr>
							</c:if>
                           <tr id="trDtOfPayment">
                                <td class="ff"><fmt:message key="LBL_DATE_OF_PAYMENT"/> : </td>
                                <td>
								<c:set var="createDate" scope="request" value="${paydetails.dtOfAction}"/>
								${paydetails.dtOfAction}
                                </td>
                            </tr>
                            <tr>
                                <td class="ff"><fmt:message key="LBL_REMARKS"/>  : </td>
                                <td> ${paydetails.comments}
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>

            </div>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <div align="center"></div>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>

