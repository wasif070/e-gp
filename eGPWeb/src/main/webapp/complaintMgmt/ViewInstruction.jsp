<%-- 
    Document   : ViewInstruction
    Created on : Feb 15, 2012, 2:19:46 PM
    Author     : shreyansh Jogi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />


<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Instruction</title>
    </head>
    <body>
        <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_1 t_space tableView_1">
           <tr>
            <td style="color:Red;">
        The following shall not be the basis for a complaint under sub-section (1), namely —
        <ol style="padding-left:25px;">
            <li style="margin:2px;">choice of procurement method for goods, works or services, or</li>
            <li style="margin:2px;">a refusal to short-list an applicant; or</li>
            <li style="margin:2px;">where the  following decisions have been made -
                <ol style="list-style-type:lower-roman; padding-left:25px;">
                    <li style="margin:2px;">a decision to reject applications for pre-qualifications, tenders, quotations or proposals; or</li>
                    <li style="margin:2px;">a decision to award a contract following approval by the Cabinet Committee on Government Purchase.</li>
                </ol>
            </li>
        </ol>
      </td>
           </tr>
           <tr>
               <td style="color:Red;">
                    PART - 12 COMPLAINTS AND APPEALS 56. Right to Complaint 
                    lodged against a Procuring Entity are outlined below, such as :-
                    <ol style="padding-left:25px;">
                         <li style="margin:2px;">Circumstances under which a formal complaint may be lodged against a Procuring Entity are outlined below, such as : -</li>
                         <ol style="list-style-type:lower-alpha; padding-left:25px;">
                             <li style="margin:2px;">in the case of invitations for Pre-Qualification -</li>
                             <ol style="list-style-type:lower-roman; padding-left:25px;">
                                <li style="margin:2px;"> Pre-Qualification Documents were not ready when the advertisement was
published by the Procuring Entity or not available when requested by a
potential Applicant;</li>
                                <li style="margin:2px;">failure to respond promptly to a request for clarification from a potential
Applicant</li>
                                <li style="margin:2px;">failure by the TEC to evaluate the qualifications in the light of criteria
stated in the Pre-Qualification Document</li>
                                <li style="margin:2px;">perceived unfair denial of Pre-Qualification; or</li>
                                <li style="margin:2px;">apprehended possible corrupt or collusive practices.</li>


                             </ol>
                             <li style="margin:2px;">in the case of Open Tendering Method, Limited Tendering method, Two- Stage
Tendering Method and Request for Quotations Method-</li>
                             <ol style="list-style-type:lower-roman; padding-left:25px;">
                                 <li style="margin:2px;">advertisement procedures not properly adhered to in accordance with Rule
90, where applicable; or</li>
                                 <li style="margin:2px;">Tender Documents not ready when the advertisement was published by
the Procuring Entity or not available when requested by a potential Person,
where applicable or</li>
                                 <li style="margin:2px;">failure to respond promptly to a request for clarification from a potential
Bidder or</li>
                                 <li style="margin:2px;"> framing technical specification that can be met by only one or a scanty
number of manufacturers</li>
                                 <li style="margin:2px;">failure to hold a pre-Tender meeting as per condition of the published
advertisement or to timely notify the potential Persons of a change in the
pre-fixed date, location, and time , that resulted in some potential Persons’
failure to attend the meeting,where applicable</li>
                                 <li style="margin:2px;"> failure to open the Tender as stated in the advertisement of Invitation for
Tenders or improper conduct at the time of Tender opening,</li>
                                 <li style="margin:2px;"> mishandling of Tenders received from Persons resulting in the opening of
one or more Tenders before the specified time that causes either a loss
of confidentiality of the Tender or an actual failure to open a Tender at a
public opening</li>
                                 <li style="margin:2px;">failure to open all Tenders which were received prior to the deadline for the
submission of Tenders</li>
                                 <li style="margin:2px;">failure by the TEC to evaluate the Tenders in compliance with the
evaluation criteria stated in the Tender Documents</li>
                                 <li style="margin:2px;">any attempt by the Procuring Entity to ‘negotiate’ with the successful
Bidder</li>
                                 <li style="margin:2px;">apprehended possible corrupt or collusive practices</li>
                                 <li style="margin:2px;"> perceived unfair or erroneous award of Contract ; and</li>
                                 <li style="margin:2px;">a breaking of the condition of confidentiality by the Procuring Entity at the
time of clarifications with each Person in the case of evaluation of the
first-stage Tender under the Two-stage Tendering Method.</li>

                             </ol>
                                 <li style="margin:2px;">in the case of Requests for Proposals:</li>
                                 <ol style="list-style-type:lower-roman; padding-left:25px;">
                                     <li style="margin:2px;">failure on the part of the Procuring Entity to maintain confidentiality
following the opening of the envelopes containing the technical Proposals</li>
                                 <li style="margin:2px;">opening of the financial Proposals at the same time as the opening of the
technical Proposals</li>
                                 <li style="margin:2px;">failure to evaluate the Proposals in accordance with the evaluation criteria
set out in the RFP</li>
                                 <li style="margin:2px;">attempt by the Procuring Entity to force an Applicant to revise fee rates
during the negotiation of the Contract where price is a factor in the
evaluation</li>
                                 <li style="margin:2px;">possible corrupt or collusive practices; and</li>
                                 <li style="margin:2px;">perceived unfair and not impartial award of Contract</li>
                                 </ol>

                         </ol>
                    </ol>
               </td>
           </tr>
        </table>
    </body>
</html>
