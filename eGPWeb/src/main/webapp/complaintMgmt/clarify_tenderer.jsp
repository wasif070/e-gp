<!DOCTYPE link PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="fm" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<html>
    <head>
        <title>Clarification</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script language="javascript" type="text/javascript">

            function textCounter( field, countfield, maxlimit ) {
                if ( field.value.length > maxlimit )
                {
                    field.value = field.value.substring( 0, maxlimit );
                    jAlert("Textarea value can only be 255 characters in length","Complaint Management", function(RetVal) {
                    });
                    return false;
                }
                else
                {
                    countfield.value = maxlimit - field.value.length;
                }
            }

        </script>
        <script type="text/javascript">
            function newDoc(){
                var comments = document.getElementById("txtaComplaintDetail").value;
                if(document.getElementById("txtaComplaintDetail").value==""){
                    jAlert("Please enter your comments","Complaint Management", function(RetVal) {
                    });
                    document.getElementById("txtaComplaintDetail").focus();
                    return false;
					
					
                }
                else{
                    document.forms[0].action = "clarifySubmit.htm?complaintId=${master.complaintId}&tenderId=${master.tenderMaster.tenderId}&comments="+comments+" ";
                    document.forms[0].submit();
                    return true;
                }
					
            }
        </script>
    </head>
    <body>


        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp"%>
            <div class="tabPanelArea_1">
                <div class="pageHead_1">Clarification
                    <span class="c-alignment-right">
                        <a class="action-button-goback"	href="TendererDashboard.htm?tenderId=${master.tenderMaster.tenderId}"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a>
                    </span></div>

                <form method="post" >
                    <table width="100%" cellspacing="5" cellpadding="0" class="tableList_3">
                        <tr>
                            <c:choose>
                                <c:when test="${master.complaintLevel.complaintLevelId == 1}">
                                    <c:set value="PE" var="officertype"></c:set>
                                </c:when>
                                <c:when test="${master.complaintLevel.complaintLevelId == 2}">
                                    <c:set value="HOPE" var="officertype"></c:set>
                                </c:when>
                                <c:when test="${master.complaintLevel.complaintLevelId == 3}">
                                    <c:set value="Secretary" var="officertype"></c:set>
                                </c:when>
                                <c:when test="${master.complaintLevel.complaintLevelId == 4}">
                                    <c:set value="Review Panel" var="officertype"></c:set>
                                </c:when>
                            </c:choose>
                                <td class="t-align-left ff" valign="top">Clarification Required by ${officertype} : </td>
                                <td class="t-align-left">
                                        ${master.officerComments}
                                </td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff" valign="top">Complaint Id : </td>
                                <td class="t-align-left"> ${master.complaintId}
                                </td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff" valign="top"><fmt:message key="LBL_COMPLAINT_DETAIL_CLARIFICATION"/> :&nbsp;<span style="color:Red">*</span></td>
                                <td class="t-align-left"><textarea id="txtaComplaintDetail" name="comments" rows="4" cols="80" onkeypress="textCounter(this,this.form.counter,200);"></textarea>
                                </td>
                            </tr>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <tr>
                                <td class="t-align-left ff" colspan="2">
                                    <center><label class="formBtn_1"><input type="button"  value="Clarify" onclick="newDoc()"/></label></center> </td>
                            </tr>

                        </table>

                    </form>



                </div>
            </div>
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->

        </div>

    </body>
</html>