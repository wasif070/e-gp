<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.Calendar" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<html >
     <%
	response.setHeader("Cache-Control","no-cache");
	response.setHeader("Cache-Control","no-store"); 
	response.setHeader("Pragma","no-cache");
    %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>complaint-management system</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

<%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>   
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>

<link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
<script type="text/javascript">
				
			</script>




</head>
<body onload="getQueryData('getmyprebidquery');">
    
            <div class="dashboard_div">
                 <%@include file="../resources/common/AfterLoginTop.jsp"%>
                 
                 
  <!--Dashboard Header Start-->
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  

  
   <%
               
               // Variable tenderId is defined by u on ur current page.
            
    String c_tenderid = ""; 
    if (request.getParameter("tenderid") != null && !"null".equalsIgnoreCase(request.getParameter("tenderid"))) {
                c_tenderid = request.getParameter("tenderid");
    } else if (request.getParameter("tenderId") != null && !"null".equalsIgnoreCase(request.getParameter("tenderId"))) {
                c_tenderid = request.getParameter("tenderId");
    }
	pageContext.setAttribute("tenderId",c_tenderid); 
  %>
        
            <!--Dashboard Header Start-->
           
            <!--Dashboard Header End-->

            <!--Dashboard Content Part Start-->
            <div class="DashboardContainer">
             <div class="pageHead_1"><fmt:message key="LBL_VIEW_COMPLAINT_DETAIL"/><span class="c-alignment-right"><a href="TendererDashboard.htm?tenderId=<%=c_tenderid%>" class="action-button-goback"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a></span></div>
            <div class="tableHead_1 t_space"><fmt:message key="LBL_TENDER_DETAILS"/></div>
             <%@include file="../resources/common/TenderInfoBar.jsp" %>
            <div>&nbsp;</div>
 <% pageContext.setAttribute("tab", 10);%>
  <%@include file="../tenderer/TendererTabPanel.jsp" %>
                <div class="tabPanelArea_1">
                    <div class="responseMsg successMsg">Your Complaint ID - ${master.complaintId} has been successfully sent to  ${master.complaintLevel.complaintLevel} </div>
                </div>
            
            <!--Dashboard Content Part End--> 






<c:if test="${uploadedDocs!=null}" >

  <div class="inner-tableHead t_space">
<fmt:message key="LBL_REFERENCE_DOCUMENTS"/>:</div>

<table width="970" cellspacing="5" cellpadding="0" class="tableList_3">
<tr> <th width="200"><fmt:message key="LBL_FILE_ID"/></th><th width="550"><fmt:message key="LBL_FILE_NAME"/> </th><th width="220"><fmt:message key="LBL_SIZE"/></th></tr>

<c:forEach var = "doc" items = "${uploadedDocs}"> 
  <tr><td>${doc.complaintDocId } </td>
<td>  ${doc.docName }</td>
<td>   ${doc.docSize }</td>
  </tr>
  

</c:forEach>  
</table>
</c:if>
            <!--Dashboard Footer Start-->
           <%@ include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
