<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.*,java.text.*,java.lang.Math" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>



<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<html >
     <%
	response.setHeader("Cache-Control","no-cache");
	response.setHeader("Cache-Control","no-store");
	response.setHeader("Pragma","no-cache");
    %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Complaint Management System</title>
            <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
            <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
            <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
            <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
            <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
            <link href="../css/home.css" rel="stylesheet" type="text/css" />
            <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
            <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
            <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
            <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>





</head>

<script type="text/javascript">

       function Pager(tableName, itemsPerPage) {
           this.tableName = tableName;
           this.itemsPerPage = itemsPerPage;
           this.currentPage = 1;
           this.pages = 0;
           this.inited = false;

           this.showRecords = function(from, to) {
               var rows = document.getElementById(tableName).rows;
               // i starts from 1 to skip table header row
               for (var i = 1; i < rows.length; i++) {
                   if (i < from || i > to)
                       rows[i].style.display = 'none';
                   else
                       rows[i].style.display = '';
               }
           }

           this.showPage = function(pageNumber) {
               if (! this.inited) {
                   alert("not inited");
                   return;
               }

               var oldPageAnchor = document.getElementById('pg'+this.currentPage);
               oldPageAnchor.className = 'pg-normal';

               this.currentPage = pageNumber;
               var newPageAnchor = document.getElementById('pg'+this.currentPage);
               newPageAnchor.className = 'pg-selected';

               var from = (pageNumber - 1) * itemsPerPage + 1;
               var to = from + itemsPerPage - 1;
               this.showRecords(from, to);
           }

           this.first = function() {

               this.showPage(1);
           }

           this.prev = function() {
               if (this.currentPage > 1)
                   this.showPage(this.currentPage - 1);
           }

           this.next = function() {
               if (this.currentPage < this.pages) {
                   this.showPage(this.currentPage + 1);
               }


           }
           this.last = function() {

               this.showPage(this.pages);
           }
           this.goton = function(pageNO) {

               if(pageNO.value <= this.pages){
                   this.showPage(pageNO.value);
               }
           }


           this.init = function() {
               var rows = document.getElementById(tableName).rows;
               var records = (rows.length - 1);
               this.pages = Math.ceil(records / itemsPerPage);
               this.inited = true;
           }

           this.showPageNav = function(pagerName, positionId) {
               if (! this.inited) {
                   alert("not inited");
                   return;
               }
               var element = document.getElementById(positionId);
               var pagerHtml = '<span>Page '+this.currentPage+' - '+this.pages +'</span>  &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; ';


               pagerHtml += '&nbsp; &nbsp; &nbsp;<input type="text" style="width: 20px;" class="formTxtBox_1" value="" id="pageNo" >';
               pagerHtml += '<label class="formBtn_1 l_space"><input type="button" value="Go To Page" onclick="'+pagerName+'.goton(pageNo);"></label> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;  ';
               for (var page = 1; page <= this.pages; page++)
                   pagerHtml += '<span id="pg' + page + '" class="pg-normal" style="color: gray;" onclick="' + pagerName + '.showPage(' + page + ');"> </span>  ';
               pagerHtml += '<span style="color: gray;" onclick="' + pagerName + '.first();" class="pg-normal"> << First &nbsp;  </span>  ';
               pagerHtml += '<span style="color: gray;" onclick="' + pagerName + '.prev();" class="pg-normal"> < Previous&nbsp; &nbsp; </span>  ';

               pagerHtml += '<span style="color: gray;" onclick="'+pagerName+'.next();" class="pg-normal"> Next&nbsp; &nbsp;  ></span>';
               pagerHtml += '<span style="color: gray;" onclick="'+pagerName+'.last();" class="pg-normal"> Last >></span>';


element.innerHTML = pagerHtml;

}
}

</script>
<body>

    <div class="mainDiv">

            <div class="fixDiv">
                 <%@include file="../resources/common/AfterLoginTop.jsp"%>

                 <div class="dashboard_div">
  <!--Dashboard Header Start-->
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
  <div class="pageHead_1"> </div>




   <%



        String tenderId="";
        if(request.getParameter("tenderId") != null){
    	tenderId = request.getParameter("tenderId");
    	session.setAttribute("tenderId",tenderId);
    }


			  // Variable tenderId is defined by u on ur current page.
             pageContext.setAttribute("tenderId",tenderId);
  %>

    <%@include file="../resources/common/TenderInfoBar.jsp" %>
    <div>&nbsp;</div>
    <% pageContext.setAttribute("tab", 11);%>

   <%@include file="../tenderer/TendererTabPanel.jsp" %>



 <div class="tabPanelArea_1">
     <%if(request.getAttribute("msg")!=null){%>
     <c:choose>
         <c:when test="${complaint.complaintLevel.complaintLevelId==4}">
             <div class="responseMsg successMsg">Your Complaint ID - ${complaint.complaintId} request has been sent to Review Panel, please pay Complaint Registration Fees and Complaint Security fees for further processing.</div>
         </c:when>
         <c:otherwise>
             <div class="responseMsg successMsg">Your Complaint ID - ${complaint.complaintId} has been successfully sent to  ${complaint.complaintLevel.complaintLevel} </div>
         </c:otherwise>
     </c:choose>
      
      <%}%>
     <%if(request.getAttribute("msgg")!=null){%>

      <c:choose>
         <c:when test="${master.complaintLevel.complaintLevelId==4}">
             <div class="responseMsg successMsg">Your Complaint ID - ${master.complaintId} request has been sent to Review Panel, please pay Complaint Registration Fees and Complaint Security fees for further processing.</div>
         </c:when>
         <c:otherwise>
             <div class="responseMsg successMsg">Your Complaint ID - ${master.complaintId} has been successfully sent to  ${master.complaintLevel.complaintLevel} </div>
         </c:otherwise>
     </c:choose>



      <%}%>
      <br />
   	<table width="100%" cellspacing="0" class="tableList_1">
        <tr>
          <td class="t-align-left"><fmt:message key="LBL_COMPLAINT"/> : <a href="<%=request.getContextPath()%>/tenderer/fileComplaint.htm?tenderId=<%= tenderId %>"><fmt:message key="LBL_REGISTER"/></a></td>
        </tr>
   </table>
<form action="" method="get" enctype="application/x-www-form-urlencoded">
<br />
   <table class="tableList_1" id="results" width="100%">
        <tr>
          <th width="20" class="ff" style="text-align:center;">Sr.No.</th>
          <th width="70" class="ff" style="text-align:center;">System Level Complaint ID</th>
          <th width="53" class="ff" style="text-align:center;"><fmt:message key="LBL_COMPLAINT_FOR"/></th>
          <th width="30%" class="ff" style="text-align:center;"><fmt:message key="LBL_COMPLAINT_SUBJECT"/></th>
          <th width="10%" class="ff" style="text-align:center;"><fmt:message key="LBL_COMPLAINT_DATE_TIME"/></th>
          <th width="10%" class="ff" style="text-align:center;"><fmt:message key="LBL_COMPLAINT_SENT_TO"/></th>
          <th width="10%" class="ff" style="text-align:center;"><fmt:message key="LBL_COMPLAINT_STATUS"/></th>
          <th width="10%" class="ff" style="text-align:center;"><fmt:message key="LBL_RESPONSE_DATE_TIME"/></th>
          <th width="15%" class="ff" style="text-align:center;"><fmt:message key="LBL_ACTION"/></th>
	    </tr>
<%int i =1 ;%>
<c:forEach var="complaint" items="${complaintList}">
      <tr>
        <td style="text-align:center;"><%=i%></td>
        <td style="text-align:center;">${complaint.complaintId }</td>
          <td style="word-wrap:break-word;">${complaint.complaintType.complaintType}</td>
     	  <td style="word-wrap:break-word;">${complaint.complaintSubject }</td>
            <c:set var="creationDate" scope="session" value="${complaint.complaintCreationDt}"/>
		    <c:set var="datecomplaint" scope="session" value="${complaint.lastModificationDt}"/>
           <c:set var="status" scope="session" value="${complaint.complaintStatus}"/>
		    <c:set var="level" scope="session" value="${complaint.complaintLevel.complaintLevelId}"/>
		    <%
                            Date cDate = (Date) session.getAttribute("creationDate");
                            Date strDate = (Date) session.getAttribute("datecomplaint");
                            //SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            //Date crdate = sdfSource.parse(cDate);
                            //Date date = sdfSource.parse(strDate);
                            //SimpleDateFormat sdfDestination = new SimpleDateFormat("dd-MM-yy hh:mm");
                            //cDate = sdfDestination.format(crdate);
                            //strDate = sdfDestination.format(date);


		   %>
                   <td style="text-align:center;"><%=DateUtils.gridDateToStrWithoutSec(cDate)%></td>


			<c:if test="${complaint.complaintLevel.complaintLevelId==1}" >
			 <td style="text-align:center;">PE</td>
			 </c:if>
			<c:if test="${complaint.complaintLevel.complaintLevelId==2}" >
			 <td style="text-align:center;">HOPA</td>
			 </c:if>

			<c:if test="${complaint.complaintLevel.complaintLevelId==3}" >
			 <td style="text-align:center;">Secretary</td>
			 </c:if>
			<c:if test="${complaint.complaintLevel.complaintLevelId==4}" >
			 <td style="text-align:center;">ReviewPanel</td>
			 </c:if>

<%
     int escalate=Integer.parseInt(XMLReader.getMessage("DaysforEscalation"));

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dt = session.getAttribute("datecomplaint").toString(); // Start date

		Calendar c = Calendar.getInstance();
		Calendar c1 = Calendar.getInstance();

		c.setTime(sdf.parse(dt));

		c.add(Calendar.DATE,escalate);

if(c1.after(c)&& (session.getAttribute("status").toString().equals("Pending")) ){


 %>
			 <td style="text-align:center;">No Response</td>
			<%} else { %>
  <td style="text-align:center;">${complaint.complaintStatus}</td>


<%}%>



<c:choose>
    <c:when test="${complaint.complaintCreationDt==complaint.lastModificationDt && complaint.complaintLevel.complaintLevelId==1 && complaint.complaintStatus=='Pending'}">
        <td style="text-align:center;">--</td>
    </c:when>
    <c:otherwise>
         <td style="text-align:center;"><%=DateUtils.gridDateToStrWithoutSec(strDate)%></td>
    </c:otherwise>
</c:choose>



	 <%
			if((c1.after(c) ||  session.getAttribute("status").toString().equals("Reject"))&&((!session.getAttribute("status").toString().equals("Accept")) && (!session.getAttribute("status").toString().equals("Clarification")) && (!session.getAttribute("level").toString().equals("4")) )){



%>


		  <td style="text-align:center;"><a href="ViewComplHistoryTenderer.htm?complaintId=${complaint.complaintId}&tenderId=<%=tenderId%>"><fmt:message key="LBL_VIEW"/></a>&nbsp;|&nbsp;
		  <a href="/tenderer/EscalateCompl.htm?tenderId=<%=tenderId%>&complaintId=${complaint.complaintId }&complaintLevelId=${complaint.complaintLevel.complaintLevelId}">Escalate to Next Higher Level</a>&nbsp;|&nbsp;
		  <%if(session.getAttribute("status").toString().equals("Clarification")){%>
		 <a href="clarify.htm?complaintId=${complaint.complaintId }"><fmt:message key="LBL_CLARIFY"/></a><%} else {%><fmt:message key="LBL_CLARIFY"/><% } %>




		  </td>

		  <%}else{%>
  <td style="text-align:center;"><a href="ViewComplHistoryTenderer.htm?complaintId=${complaint.complaintId}&tenderId=<%=tenderId%>"><fmt:message key="LBL_VIEW"/></a>&nbsp;|&nbsp;
		 <fmt:message key="LBL_ESCALATE"/>&nbsp;|&nbsp;<%if(session.getAttribute("status").toString().equals("Clarification")){%>
		 <a href="clarify.htm?complaintId=${complaint.complaintId }"><fmt:message key="LBL_CLARIFY"/></a><%} else {%><fmt:message key="LBL_CLARIFY"/> <% } %>

		  </td>
<%}%>



     </tr>
     <%i++;%>
</c:forEach>
   </table>
<br />
<div id="pageNavPosition"></div>
<br />

</form>

<script type="text/javascript"><!--
var pager = new Pager('results', 10);
pager.init();
pager.showPageNav('pager', 'pageNavPosition');
pager.showPage(1);
//--></script>

</div>
  </div>
                 </div>
            </div>
    <!--Dashboard Content Part End-->

   <!--Dashboard Footer Start-->
            <%@ include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
