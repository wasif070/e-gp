<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@page import="java.util.*,java.text.*" %>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Complaints Management</title>
<link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />

<link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/js/datepicker/css/jscal2.css" />
<link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/js/datepicker/css/border-radius.css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/datepicker/js/jscal2.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/datepicker/js/lang/en.js"></script>

<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.1.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js"></script>
<link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/demo_table.css" />
<link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/css/demo_page.css" />
<script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
<script type="text/javascript">
            $(document).ready(function() {
                sortTable();
            });
</script>
<style type="text/css">
.pg-normal {
color: #000000;
font-weight: normal;
text-decoration: none;
cursor: pointer;
}

.pg-selected {
color: #800080;
font-weight: bold;
text-decoration: underline;
cursor: pointer;
}
</style>
</head>

<script type="text/javascript">

function Pager(tableName, itemsPerPage) {
this.tableName = tableName;
this.itemsPerPage = itemsPerPage;
this.currentPage = 1;
this.pages = 0;
this.inited = false;

this.showRecords = function(from, to) {
var rows = document.getElementById(tableName).rows;
// i starts from 1 to skip table header row
for (var i = 1; i < rows.length; i++) {
if (i < from || i > to)
rows[i].style.display = 'none';
else
rows[i].style.display = '';
}
}

this.showPage = function(pageNumber) {
if (! this.inited) {
alert("not inited");
return;
}

var oldPageAnchor = document.getElementById('pg'+this.currentPage);
oldPageAnchor.className = 'pg-normal';

this.currentPage = pageNumber;
var newPageAnchor = document.getElementById('pg'+this.currentPage);
newPageAnchor.className = 'pg-selected';

var from = (pageNumber - 1) * itemsPerPage + 1;
var to = from + itemsPerPage - 1;
this.showRecords(from, to);
}

this.first = function() {

this.showPage(1);
}

this.prev = function() {
if (this.currentPage > 1)
this.showPage(this.currentPage - 1);
}

this.next = function() {
if (this.currentPage < this.pages) {
this.showPage(this.currentPage + 1);
}


}
this.last = function() {

this.showPage(this.pages);
}
this.goton = function(pageNO) {

if(pageNO.value <= this.pages){
this.showPage(pageNO.value);
}
}


this.init = function() {
var rows = document.getElementById(tableName).rows;
var records = (rows.length - 1);
this.pages = Math.ceil(records / itemsPerPage);
this.inited = true;
}

this.showPageNav = function(pagerName, positionId) {
if (! this.inited) {
alert("not inited");
return;
}
var element = document.getElementById(positionId);
var pagerHtml = '<span>Page '+this.currentPage+' - '+this.pages +'</span>  &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; ';


pagerHtml += '&nbsp; &nbsp; &nbsp;<input type="text" style="width: 20px;" class="formTxtBox_1" value="" id="pageNo" >';
pagerHtml += '<label class="formBtn_1 l_space"><input type="button" value="Go To Page" onclick="'+pagerName+'.goton(pageNo);"></label> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;  ';
for (var page = 1; page <= this.pages; page++)
pagerHtml += '<span id="pg' + page + '" class="pg-normal" style="color: gray;" onclick="' + pagerName + '.showPage(' + page + ');"> </span>  ';
pagerHtml += '<span style="color: gray;" onclick="' + pagerName + '.first();" class="pg-normal"> << First &nbsp;  </span>  ';
pagerHtml += '<span style="color: gray;" onclick="' + pagerName + '.prev();" class="pg-normal"> < Previous&nbsp; &nbsp; </span>  ';

pagerHtml += '<span style="color: gray;" onclick="'+pagerName+'.next();" class="pg-normal"> Next&nbsp; &nbsp;  ></span>';
pagerHtml += '<span style="color: gray;" onclick="'+pagerName+'.last();" class="pg-normal"> Last >></span>';


element.innerHTML = pagerHtml;

}
}

</script>	
<style>
   .BreakWord { word-break: break-all; }
</style>

<script type="text/javascript">

function GetCal(txtname,controlname){
new Calendar({
inputField: txtname,
trigger: controlname,
showTime: false,
dateFormat:"%d/%m/%Y",
onSelect: function() {
var date = Calendar.intToDate(this.selection.get());
LEFT_CAL.args.min = date;
LEFT_CAL.redraw();
this.hide();
}
});

var LEFT_CAL = Calendar.setup({
weekNumbers: false
})
}


</script>
</head>
<body>
<div class="dashboard_div">
<input id="curDate" value="Sunday, January 16, 2011 11:24:11 AM" type="hidden" />
<!--Dashboard Header Start-->
<%@include  file="../resources/common/AfterLoginTop.jsp"%>
<!--Dashboard Header End-->
</
<!--Dashboard Content Part Start-->
<div class="DashboardContainer">
<div class="pageHead_1"><fmt:message key="LBL_COMPLAINT_MANAGEMENT"/>
<span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('10');">Save as PDF</a></span>
</div>
 <%
 CommonService cs = (CommonService)AppContext.getSpringBean("CommonService");
 String tenderId="";
 String procNature="";
if(request.getParameter("tenderId") != null){
tenderId = request.getParameter("tenderId");
procNature = cs.getProcNature(tenderId).toString();
}
 
    String disp = "";
    if("goods".equalsIgnoreCase(procNature)){
        disp = "Supplier";
        }else if("works".equalsIgnoreCase(procNature)){
        disp = "Contractor";
            }else{
    disp = "Consultant";
            }

    %>
    <%if(request.getAttribute("msg")!=null){%>
     <div class="responseMsg successMsg" style="margin-top:12px;">Your comments have been sent to the <%=disp%> for complaint Id:${complaint.complaintId}</div>
    <%}%>

<br />
<div class="formBg_1">
<form name="searchform" method="post" action="search.htm">
<table width="970" cellspacing="10" border="0" id="SearchTable" class="formStyle_1">

<tr>
<td width="102" class="ff"><fmt:message key="LBL_COMPLAINT_ID"/>:</td>
<td width="201"><input id="txtCompID" name="txtCompID" /></td>
<td width="74" class="ff"><fmt:message key="LBL_TENDER_ID"/> :</td>
<td width="205"><input id="txtTenderID" name="txtTenderID" /></td>
<td width="102" class="ff"><fmt:message key="LBL_REFERENCE_NO"/> : </td>
<td width="204"><input id="txtReferneceNo" name="txtReferneceNo" /></td>
</tr>
<tr>
<input type="hidden" name="compstatus" value="<%if(request.getAttribute("status")==null){%>Pending<%} else {%>Process<%}%>"
<td class="ff" colspan="6" style="text-align:center;"><label class="formBtn_1">
<input name="submit" type="submit" value="Search" />
</label>
&nbsp;
<label class="formBtn_1">
<input name="submit" type="reset" value="Reset" />
</label></td>
</tr>
</table>
</form>
</div>

<ul class="tabPanel_1 t_space">
    <li><a href="javascript:void(0);" id="pending" onclick="callpending('pending')"   <%if(request.getAttribute("status")==null){%> class="sMenu" <%}%> ><fmt:message key="LBL_PENDING"/></a></li>
<li><a href="javascript:void(0);" id="prs" onclick="callpending('process')" <%if(request.getAttribute("status")!=null){%> class="sMenu" <%}%> ><fmt:message key="LBL_PROCESSED"/></a></li>
</ul>
<div class="tabPanelArea_1">
<form action="" method="get" enctype="application/x-www-form-urlencoded">
<table width="100%" cellspacing="0" cellpadding="0" border="0" class="tableList_3 display"  id="resultTable"  cols="@0,10" style="table-layout: fixed" >
<thead>

<tr>
<th><fmt:message key="LBL_COMPLAINT_ID"/></th>
<th><fmt:message key="LBL_TENDER_ID"/></th>
<th><fmt:message key="LBL_REFERENCE_NO"/></th>
<th>Procurement Role</th>
<th><fmt:message key="LBL_COMPLAINT_FOR"/></th>
<th><fmt:message key="LBL_COMPLAINT_SUBJECT"/></th>
<th><fmt:message key="LBL_COMPLAINT_DATE_TIME"/></th>
<th><fmt:message key="LBL_NAME_OF_TENDERER_CONSULTANT"/></th>
<th><fmt:message key="LBL_COMPLAINT_STATUS"/></th>
<th><fmt:message key="LBL_RESPONSE_DATE_TIME"/></th>
<th><fmt:message key="LBL_ACTION"/></th>
</tr>
</thead>
<tbody>
<c:if test="${empty hopeSearchList}">
<tr>
<td colspan="11"><fmt:message key="LBL_NO_SEARCH_RESULTS_FOUND"/></td>
</tr>
</c:if>
<c:if test="${hopeSearchList!=null}">
<c:forEach var="complaint" items="${hopeSearchList}">

<c:set var="creationDate" scope="session" value="${complaint.complaintCreationDt}"/>
<c:set var="commentsdate" scope="session" value="${complaint.lastModificationDt}"/>
<c:set var="status" scope="session" value="${complaint.complaintStatus}"/>
<%
Date cDate = (Date)session.getAttribute("creationDate");
Date strDate = (Date)session.getAttribute("commentsdate");
%>

<tr>
<td>${complaint.complaintId}</td>
<td>${complaint.tenderMaster.tenderId}</td>
<td class="break-word" >${complaint.tenderRefNo}</td>
<td class="break-word">${complaint.complaintLevel.complaintLevel}</td>
<td class="break-word">${complaint.complaintType.complaintType}</td>
<td class="break-word">${complaint.complaintSubject}</td>
<td class="break-word"><%=DateUtils.gridDateToStrWithoutSec(cDate)%></td>
<td class="break-word"> <c:choose>
                                    <c:when test="${complaint.tendererMaster.tblCompanyMaster.companyId==1}">
                                        ${complaint.tendererMaster.firstName} <br />${complaint.tendererMaster.lastName}
                                    </c:when>
                                    <c:otherwise>
                                        ${complaint.tendererMaster.tblCompanyMaster.companyName}
                                    </c:otherwise>
                                </c:choose></td>

<%
int escalate=Integer.parseInt(XMLReader.getMessage("DaysforEscalation"));

SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
String dt = session.getAttribute("commentsdate").toString(); // Start date

Calendar c = Calendar.getInstance();
Calendar c1 = Calendar.getInstance();

c.setTime(sdf.parse(dt));

c.add(Calendar.DATE,escalate);
if(c1.after(c)&& (session.getAttribute("status").toString().equals("Pending")) ){

%>
<td style="text-align:center;">No Response</td>

<c:set var="flag" value="1"/>
<%} else { %>

<c:set var="flag" value="0"/>
<td>${complaint.complaintStatus}</td>
<%}%>

<td class="table-date"><%=DateUtils.gridDateToStrWithoutSec(strDate)%></td>


<c:choose>
<c:when test="${complaint.complaintStatus=='Clarification'  || flag=='1'}">
<td class="t-align-left" style="text-align:center;"><fmt:message key="LBL_PROCESS"/> | <a href="ViewComplHistoryTenderer.htm?complaintId=${complaint.complaintId}&tenderId=${complaint.tenderMaster.tenderId}"><fmt:message key="LBL_VIEW"/></a> | <br />
<c:if test="${complaint.notificationRemarksToPe == ' '}">
<fmt:message key="LBL_NOTIFICATION_FROM_CPTU"/></td>
</c:if>
<c:if test="${complaint.notificationRemarksToPe!=' '}">
<a href="ComplNotification.htm?complaintId=${complaint.complaintId}"><fmt:message key="LBL_NOTIFICATION_FROM_CPTU"/></a></td>
</c:if>

</c:when>

<c:otherwise>
<td class="t-align-left" style="text-align:center;">
    <%if(request.getAttribute("status")==null){%>
    <a href="processComplaint.htm?tenderId=${complaint.tenderMaster.tenderId}&uploadedBy=${complaint.tendererMaster.tendererId}&complaintlevelid=${complaint.complaintLevel.complaintLevelId}&complaintId=${complaint.complaintId}"><fmt:message key="LBL_PROCESS"/></a>
|
<%}%>
<a href="ViewComplHistoryTenderer.htm?complaintId=${complaint.complaintId}&tenderId=${complaint.tenderMaster.tenderId}"><fmt:message key="LBL_VIEW"/></a> | <br />
<c:if test="${complaint.notificationRemarksToPe == ' '}">
<fmt:message key="LBL_NOTIFICATION_FROM_CPTU"/></td>
</c:if>
<c:if test="${complaint.notificationRemarksToPe!=' '}">
<a href="ComplNotification.htm?complaintId=${complaint.complaintId}"><fmt:message key="LBL_NOTIFICATION_FROM_CPTU"/></a></td>
</c:if>

</c:otherwise>
</c:choose>
</tr>

</c:forEach>
</c:if>
</tbody>
</table>
<br />
<div id="pageNavPosition"></div>
<br />
</form>
<script type="text/javascript">
var pager = new Pager('resultTable', 10);
pager.init();
pager.showPageNav('pager', 'pageNavPosition');
pager.showPage(1);

function callpending(str){

if(str == "pending"){
//    document.getElementById("pending").className = "sMenu";
//    document.getElementById("prs").className = "";
    document.forms[0].action = "/resources/common/complaintOfficerHope.htm"
    document.forms[0].submit();
}else{
//    document.getElementById("prs").className = "sMenu";
//    document.getElementById("pending").className = "";
    document.forms[0].action = "/resources/common/complaintOfficerProcessedHope.htm"
    document.forms[0].submit();
}
}


</script>
<form id="formstyle" action="" method="post" name="formstyle">

   <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
   <%
     SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
     String appenddate = dateFormat1.format(new Date());
   %>
   <input type="hidden" name="fileName" id="fileName" value="ComplaintManagement_<%=appenddate%>" />
    <input type="hidden" name="id" id="id" value="ComplaintManagement" />
</form>
</div>
</div>
<!--Dashboard Content Part End-->

<!--Dashboard Footer Start-->
<%@include file="../resources/common/Bottom.jsp" %>
<!--Dashboard Footer End-->
</div>
</body>
</html>
