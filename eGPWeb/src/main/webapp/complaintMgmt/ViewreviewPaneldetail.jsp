<%-- 
    Document   : ViewreviewPaneldetail
    Created on : Jan 10, 2012, 6:35:38 PM
    Author     : shreyansh Jogi
--%>

        <%@page contentType="text/html" pageEncoding="UTF-8"%>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
    <HEAD>
        <TITLE>View Review Panel  </TITLE>
    <%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.*,java.text.*,java.lang.Math" %>
    <%@page import="com.cptu.egp.eps.service.serviceinterface.ComplaintMgmtService"%>
    <%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
    <%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
    <html>

        <head>

            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <title>complaint-management system</title>
            <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
            <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
            <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
            <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
            <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
            <link href="../css/home.css" rel="stylesheet" type="text/css" />
            <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
            <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
            <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
            <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        </head>
        <body>

            <%
                ComplaintMgmtService mgmtService = (ComplaintMgmtService) AppContext.getSpringBean("complaintMgmtService");
                int rpId=0;
                if(request.getParameter("rpId")!=null){
                    rpId = Integer.parseInt(request.getParameter("rpId"));
                    }
                int empId = mgmtService.getEmpId(rpId);
                List<Object[]> list = mgmtService.getReviewPanel(empId);

%>
            <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>

                <!--Middle Content Table Start-->
                <table border="0" cellspacing="0" cellpadding="0" width="100%">
                    <tr valign="top">

                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" />

                        <td class="contentArea-Blogin">

                            <!--Dashboard Content Part Start-->
                                <div class="pageHead_1 t_space">View Review Panel Details</div>
                                <form name="reviewPanel" method="post">
                                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" >
                                        
                                        <tr>
                                            <td width="200" class="ff">Email Id :</td>
                                            <td>
                                                       <%=list.get(0)[0]%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff">Full Name : </td>
                                            <td><%=list.get(0)[2]%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="200" class="ff">Name in Dzongkha : </td>
                                            <td>
                                                      <%=(""+ new String(((byte[])list.get(0)[3]),"UTF-8"))%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="200" class="ff">CID No.: </td>
                                            <td>
                                                       <%=list.get(0)[5]%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="200" class="ff">Mob.No. : </td>
                                            <td>
                                                      <%=list.get(0)[4]%>
                                  </td>
                                        </tr>
                                        <tr>
                                            <td width="200" width="200" class="ff">Review Panel Name :</td>
                                            <td>
                                                       <%=list.get(0)[7]%>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="200" width="200" class="ff">Review Panel Member's :</td>
                                            <td>
                                                       <%=list.get(0)[9]%>
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                        </td>
                    </tr></table>
                            </div>
               

                <!--Dashboard Footer Start-->
                <%@include file="../resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
        </body>

    </html>
