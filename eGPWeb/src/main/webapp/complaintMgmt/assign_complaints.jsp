<%@page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:directive.page import="com.cptu.egp.eps.web.servicebean.MessageProcessSrBean" />
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Assign Complaint to Review Panel</title>
        <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../css/home.css" rel="stylesheet" type="text/css" />

        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../js/jQuery/jquery.alerts.js"></script>
         <script type="text/javascript" src="../js/jQuery/jquery.validate.js"></script>
        <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="../js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />

       
        <script type="text/javascript">
            function GetCal(txtname,controlname){
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            function validatesearch()
            {
                document.getElementById("todatespan").innerHTML="";
                if(document.getElementById("txtPublicationDate").value!="")
                {
                    if(document.getElementById("txtPublicationDate1").value=="")
                    {
                        document.getElementById("todatespan").innerHTML="Please select to date";
                        return false;
                    }
                }
            }
            function checkSubmit(){
                 var RetVal = confirm("Are you sure want assign complaint to Review Panel?");
                if(RetVal)
                {
                                  return true;
                }else{
                    return false;
                }
    
                
            }
        </script>
    </head>
    <body>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->

            <div class="contentArea_1">
            <!--Dashboard Content Part Start-->
            <div class="DashboardContainer">
                <div class="pageHead_1">Assign Complaint to Review Panel<span class="c-alignment-right"><a href="ViewComplaintsCPTU.htm" class="action-button-goback">Go Back to Dashboard</a></span></div>
                <form action="ViewComplaintsCPTU.htm?complaintId=${master.complaintId}"  id="frmreview" name="frmreview"  method="post">
                <table width="100%" cellspacing="5" class="t_space tableList_3">
                    <tr>
                        <th width="221" class="ff"><fmt:message key="LBL_COMPLAINT_ID"/></th>
                        <th width="216" class="ff"><fmt:message key="LBL_TENDER_ID"/></th>
                         <th class="ff"><fmt:message key="LBL_COMPLAINT_FOR"/></th>
                    </tr>
                    <tr>
                        <td>${master.complaintId}</td>
                        <td>${master.tenderMaster.tenderId}</td>
                         <td>${master.complaintType.complaintType}</td>
                       
                    </tr>
                    <tr>
                       
                        <th class="ff"><fmt:message key="LBL_COMPLAINT_SUBJECT"/> </th>
                        <th class="ff"><fmt:message key="LBL_COMPLAINT_DATE_TIME"/> </th>
                        <th class="ff"><fmt:message key="LBL_COMPLAINT_STATUS"/></th>
                    </tr>
                    <tr>
                       
                        <td>${master.complaintDetails}</td>
                        <td>${master.complaintCreationDt}</td>
                        <td>${master.complaintStatus}</td>
                    </tr>
                   
                    <tr>
                        
                        <th class="ff"><fmt:message key="LBL_REGISTRATION_FEE_DETAIL"/> </th>
                        <th class="ff"><fmt:message key="LBL_SECURITY_DEPOSIT"/> </th>
                        <th valign="top" class="ff">Assing to Review Panel  <span class="mandatory">*</span></th>
                    </tr>
                    <tr>
                        
                        <td><a target="_blank" href="viewFeesDetails.htm?complId=${master.complaintId}&feetype=Registration&tenderId=${master.tenderMaster.tenderId}" onClick="return popup(this, 'Registration Fee')">View</a></td>
                        <td><a target="_blank" href="viewFeesDetails.htm?complId=${master.complaintId}&feetype=Security&tenderId=${master.tenderMaster.tenderId}" onClick="return popup(this, 'Security fee')">View</a></td>


                        <td><select class="formTxtBox_1" style="width:150px;" name="reviewPanel">
                               <c:forEach items="${lstReviewPanels }" var="reviewpanel">
                                <option value="${reviewpanel.reviewPanelId}">${reviewpanel.reviewPanelName}</option>
                              </c:forEach>
                            </select></td>
                    </tr>
                    
                    <tr>
                        <td colspan="4" class="t-align-center"><label class="formBtn_1">
                                <input name="submit" type="submit" value="Submit" onclick="return checkSubmit()" />
                            </label></td>
                    </tr>
                </table>
                        </form>
            </div>
            </div>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@ include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>

</body>
</html>
