<%--
    Document   : compl_mgmt_RP
    Created on : Jan 13, 2012, 11:39:27 AM
    Author     : shreyansh Jogi 18th Jan 2012
--%>
<%@page import="com.cptu.egp.eps.model.table.TblComplaintMaster"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.*,java.text.*,java.lang.Math" %>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<html >
    <%
                response.setHeader("Cache-Control", "no-cache");
                response.setHeader("Cache-Control", "no-store");
                response.setHeader("Pragma", "no-cache");
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>complaint-management system</title>
                <link href="../css/defaultDashboard.css" rel="stylesheet" type="text/css" />
                <link href="../css/theme_1.css" rel="stylesheet" type="text/css" />
                <link href="../css/home.css" rel="stylesheet" type="text/css" />
                <link type="text/css" rel="stylesheet" href="../js/datepicker/css/jscal2.css" />
                <link type="text/css" rel="stylesheet" href="../js/datepicker/css/border-radius.css" />
                <script type="text/javascript" src="../js/datepicker/js/jscal2.js"></script>
                <script type="text/javascript" src="../js/datepicker/js/lang/en.js"></script>
                <script type="text/javascript" src="../js/jQuery/jquery-1.4.1.js"></script>
                <script type="text/javascript" src="../js/jQuery/jquery.alerts.js"></script>
                <link href="../js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
                <link href="../js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />

        <style type="text/css">
            .pg-normal {
                color: #000000;
                font-weight: normal;
                text-decoration: none;
                cursor: pointer;
            }

            .pg-selected {
                color: #800080;
                font-weight: bold;
                text-decoration: underline;
                cursor: pointer;
            }
        </style>
    </head>

    <script type="text/javascript">

        function Pager(tableName, itemsPerPage) {
            this.tableName = tableName;
            this.itemsPerPage = itemsPerPage;
            this.currentPage = 1;
            this.pages = 0;
            this.inited = false;

            this.showRecords = function(from, to) {
                var rows = document.getElementById(tableName).rows;
                // i starts from 1 to skip table header row
                for (var i = 1; i < rows.length; i++) {
                    if (i < from || i > to)
                        rows[i].style.display = 'none';
                    else
                        rows[i].style.display = '';
                }
            }

            this.showPage = function(pageNumber) {
                if (! this.inited) {
                    alert("not inited");
                    return;
                }

                var oldPageAnchor = document.getElementById('pg'+this.currentPage);
                oldPageAnchor.className = 'pg-normal';

                this.currentPage = pageNumber;
                var newPageAnchor = document.getElementById('pg'+this.currentPage);
                newPageAnchor.className = 'pg-selected';

                var from = (pageNumber - 1) * itemsPerPage + 1;
                var to = from + itemsPerPage - 1;
                this.showRecords(from, to);
            }

            this.first = function() {

                this.showPage(1);
            }

            this.prev = function() {
                if (this.currentPage > 1)
                    this.showPage(this.currentPage - 1);
            }

            this.next = function() {
                if (this.currentPage < this.pages) {
                    this.showPage(this.currentPage + 1);
                }


            }
            this.last = function() {

                this.showPage(this.pages);
            }
            this.goton = function(pageNO) {

                if(pageNO.value <= this.pages){
                    this.showPage(pageNO.value);
                }
            }


            this.init = function() {
                var rows = document.getElementById(tableName).rows;
                var records = (rows.length - 1);
                this.pages = Math.ceil(records / itemsPerPage);
                this.inited = true;
            }

            this.showPageNav = function(pagerName, positionId) {
                if (! this.inited) {
                    alert("not inited");
                    return;
                }
                var element = document.getElementById(positionId);
                var pagerHtml = '<span>Page '+this.currentPage+' - '+this.pages +'</span>  &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; ';


                pagerHtml += '&nbsp; &nbsp; &nbsp;<input type="text" style="width: 20px;" class="formTxtBox_1" value="" id="pageNo" >';
                pagerHtml += '<label class="formBtn_1 l_space"><input type="button" value="Go To Page" onclick="'+pagerName+'.goton(pageNo);"></label> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;  ';
                for (var page = 1; page <= this.pages; page++)
                    pagerHtml += '<span id="pg' + page + '" class="pg-normal" style="color: gray;" onclick="' + pagerName + '.showPage(' + page + ');"> </span>  ';
                pagerHtml += '<span style="color: gray;" onclick="' + pagerName + '.first();" class="pg-normal"> << First &nbsp;  </span>  ';
                pagerHtml += '<span style="color: gray;" onclick="' + pagerName + '.prev();" class="pg-normal"> < Previous&nbsp; &nbsp; </span>  ';

                pagerHtml += '<span style="color: gray;" onclick="'+pagerName+'.next();" class="pg-normal"> Next&nbsp; &nbsp;  ></span>';
                pagerHtml += '<span style="color: gray;" onclick="'+pagerName+'.last();" class="pg-normal"> Last >></span>';


                element.innerHTML = pagerHtml;

            }
        }

    </script>
</head>
<body onload="getQueryData('getmyprebidquery');">
    <div class="dashboard_div">

        <div class="mainDiv">

            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp"%>

                <div class="dashboard_div">



                    <div class="contentArea_1">
                        <div class="pageHead_1"> </div>
                        <div>&nbsp;</div>
                        <div class="tabPanelArea_1">
                            <form action="" method="get" enctype="application/x-www-form-urlencoded">
                                <table width="100%" id="results" class="tableList_1" >
                                    <thead>

                                        <tr>
                                            <th width="43"><fmt:message key="LBL_COMPLAINT_ID"/></th>
                                            <th width="44"><fmt:message key="LBL_NAME_OF_TENDERER_CONSULTANT"/></th>
                                            <th width="54"><fmt:message key="LBL_COMPLAINT_FOR"/></th>
                                            <th width="288"><fmt:message key="LBL_COMPLAINT_SUBJECT"/></th>
                                            <th width="67"><fmt:message key="LBL_COMPLAINT_DATE_TIME"/></th>
                                            <th width="45"><fmt:message key="LBL_COMPLAINT_STATUS"/></th>
                                            <th width="56"><fmt:message key="LBL_RESPONSE_DATE_TIME"/></th>
                                            <th width="96"><fmt:message key="LBL_ACTION"/></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="complaint" items="${listpe}">
                                            <tr>
                                                <td style="text-align:center;">${complaint.complaintId}</td>
                                                <td class="t-align-left"></td>
                                                <td style="text-align:center;">${complaint.complaintType.complaintType}</td>
                                                <td style="text-align:left;word-wrap:break-word;">${complaint.complaintSubject }</td>


                                                <c:set var="creationDate" scope="session" value="${complaint.complaintCreationDt}"/>
                                                <c:set var="commentsdate" scope="session" value=""/>
                                                <c:set var="status" scope="session" value="${complaint.complaintStatus}"/>
                                                <%
                                                            Date cDate = (Date) session.getAttribute("creationDate");



                                                %>
                                                <td style="text-align:center;"></td>
                                                <%
                                                            int escalate = Integer.parseInt(XMLReader.getMessage("DaysforEscalation"));

                                                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                                            // Start date

                                                            Calendar c = Calendar.getInstance();
                                                            Calendar c1 = Calendar.getInstance();
                                                            c.add(Calendar.DATE, escalate);

                                                            if (c1.after(c) && (session.getAttribute("status").toString().equals("Pending"))) {

                                                %>
                                                <td style="text-align:center;">No Response</td>
                                                <c:set var="flag" value="1"/>
                                                <%} else {%>

                                                <c:set var="flag" value="0"/>
                                                <td class="t-align-left"style="text-align:center;">${complaint.complaintStatus}</td>
                                                <% }%>
                                                <c:choose>
                                                    <c:when test="${complaint.complaintCreationDt==complaint.lastModificationDt && complaint.complaintLevel.complaintLevelId==1 && complaint.complaintStatus=='Pending'}">
                                                        <td style="text-align:center;">--</td>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <td class="t-align-left"></td>
                                                    </c:otherwise>
                                                </c:choose>

                                                <c:choose>
                                                    <c:when test="${complaint.complaintStatus=='Clarification' || complaint.complaintStatus =='Accept' || complaint.complaintStatus =='Reject' || flag=='1'}" >
                                                        <td class="t-align-left" style="text-align:center;"><fmt:message key="LBL_PROCESS"/> | <a href="ViewComplHistoryTenderer.htm?complaintId=${complaint.complaintId}&tenderId=${complaint.tenderId}"><fmt:message key="LBL_VIEW"/></a> | <br />
                                                            <c:if  test="${complaint.notificationRemarksToPe == ' '}">
                                                                <fmt:message key="LBL_NOTIFICATION_FROM_CPTU"/></td>
                                                            </c:if>
                                                            <c:if test="${complaint.notificationRemarksToPe!=' '}">
                                                    <td><a href="ComplNotification.htm?complaintId=${complaint.complaintId}"><fmt:message key="LBL_NOTIFICATION_FROM_CPTU"/></a></td>
                                                </c:if>
                                            </c:when>

                                            <c:otherwise>


                                                <td class="t-align-left" style="text-align:center;"><a href="processcomplaintRP.htm?tenderId=${complaint.tenderId}&uploadedBy=${complaint.tendererId}&complaintlevelid=${complaint.complaintLevel.complaintLevelId}&complaintId=${complaint.complaintId}"><fmt:message key="LBL_PROCESS"/></a>
                                                    | <a href="ViewComplHistoryTenderer.htm?complaintId=${complaint.complaintId}&tenderId=${complaint.tenderId}"><fmt:message key="LBL_VIEW"/></a> | <br />
                                                    <c:if test="${complaint.notificationRemarksToPe == ' '}">
                                                        <fmt:message key="LBL_NOTIFICATION_FROM_CPTU"/></td>
                                                    </c:if>
                                                    <c:if test="${complaint.notificationRemarksToPe!=' '}">
                                                    <td><a href="ComplNotification.htm?complaintId=${complaint.complaintId}"><fmt:message key="LBL_NOTIFICATION_FROM_CPTU"/></a></td>
                                                </c:if>
                                            </c:otherwise>
                                        </c:choose>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <br />
                                <div id="pageNavPosition"></div>
                                <br />
                            </form>
                        </div></div></div>
                <script type="text/javascript"><!--
                    var pager = new Pager('results', 10);
                    pager.init();
                    pager.showPageNav('pager', 'pageNavPosition');
                    pager.showPage(1);
                    //--></script>
            </div>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@ include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
    </div>
</body>
</html>