<%@page import="java.util.*"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<jsp:directive.page import="com.cptu.egp.eps.web.servicebean.MessageProcessSrBean" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View CPTU Notification</title>
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="<%=request.getContextPath()%>/resources/js/datepicker/css/border-radius.css" />
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
		<link href="<%=request.getContextPath()%>/resources/resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
		
		<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js"></script>
        <script src="<%=request.getContextPath()%>/resources/resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
		
       
        <script type="text/javascript">
            function GetCal(txtname,controlname){
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            function validatesearch()
            {
                document.getElementById("todatespan").innerHTML="";
                if(document.getElementById("txtPublicationDate").value!="")
                {
                    if(document.getElementById("txtPublicationDate1").value=="")
                    {
                        document.getElementById("todatespan").innerHTML="Please select to date";
                        return false;
                    }
                }
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->

            <!--Dashboard Content Part Start-->
            <div class="DashboardContainer">
                <div class="tabPanelArea_1">
                <div class="pageHead_1">View Notification from CPTU against a Complaint<span class="c-alignment-right">

				 <%
				 String procurementRole=(String)session.getAttribute("procurementRole");
					 if(procurementRole!=null && "PE".equals(procurementRole))
					 {%>
					<a href="complaintOfficer.htm?tenderId=${master.tenderMaster.tenderId}" class="action-button-goback"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a></span></div>
           <%
		   }else if(procurementRole!=null &&"HOPE".equals(procurementRole)){
			   %>
	           <a href="complaintOfficerHope.htm" class="action-button-goback"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a></span></div> 
			<%
				   }else if(procurementRole!=null && "Secretary".equals(procurementRole)){
				   %>
           <a href="complaintOfficerHope.htm" class="action-button-goback"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a></span></div>
             <%
					   }else{
                                     if(session.getAttribute("userTypeId").toString().equals("17")){
					   %>
                                       <a href="TendererDashboard.htm" class="action-button-goback"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a></span></div>
				<%}else{%>
				<a href="ViewComplaintsCPTU.htm" class="action-button-goback"><fmt:message key="LBL_GO_BACK_TO_DASHBOARD"/></a></span></div>
				<%
                                }
					}
				%>

				
                <table width="970" cellspacing="5" class="tableList_3 t_space">
                     <tr>
                        <th width="240" class="ff"><fmt:message key="LBL_COMPLAINT_ID"/>:</th>
                        <th width="421" class="ff"><fmt:message key="LBL_TENDER_ID"/>:</th>
                        <th width="281" class="ff"><fmt:message key="LBL_REFERENCE_NO"/> :</th>
                    </tr>
                    <tr>
                        <td>${master.complaintId}</td>
                        <td>${master.tenderMaster.tenderId}</td>
                        <td>${master.tenderRefNo}</td>
                    </tr>
                    <tr>
                        <td colspan="3" height="3"></td>
                    </tr>
                    <tr>
                        <td class="ff">Procurement Role :</td>
                        <td class="ff"><fmt:message key="LBL_COMPLAINT_FOR"/> :</td>
                        <td class="ff"><fmt:message key="LBL_COMPLAINT_SUBJECT"/>:</td>
                    </tr>
                    <tr>
                        <td>${master.tenderMaster.tblAppPackages.tblProcurementRole.procurementRole}</td>
                        <td>${master.complaintType.complaintType}</td>
                        <td>${master.complaintDetails}</td>
                    </tr>
                    <tr>
                        <td colspan="3" height="3"></td>
                    </tr>
                    <tr>
                        <th class="ff"><fmt:message key="LBL_COMPLAINT_DATE_TIME"/> :</th>
                        <th class="ff"><fmt:message key="LBL_NAME_OF_TENDERER_CONSULTANT"/>:</th>
                        <th class="ff"><fmt:message key="LBL_DATE_AND_TIME_OF_NOTIFICATION"/> :</th>
                    </tr>
                    <c:set var="creationDate" scope="session" value="${master.complaintCreationDt}" />
<c:set var="commentsdate" scope="session" value="${master.notificationDtToPe}" />

<%
Date cDate = (Date)session.getAttribute("creationDate");
Date strDate = (Date)session.getAttribute("commentsdate");
%>

               <tr>
                        <td><%=DateUtils.gridDateToStrWithoutSec(cDate)%></td>
                        <td>${master.tendererMaster.firstName}</td>
                        <td><%=DateUtils.gridDateToStrWithoutSec(strDate)%></td>
                    </tr>
                    <tr>
                        <td colspan="3" height="3"></td>
                    </tr>
                    <tr>
                        <td valign="top" class="ff">Comments from CPTU :&nbsp;<span class="mandatory">*</span></td>
                        <td colspan="2">${master.notificationRemarksToPe}</td>
                    </tr>
                </table>


            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@ include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>

    </body>
</html>
