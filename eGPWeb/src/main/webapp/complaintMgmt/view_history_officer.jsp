<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.Calendar" %>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<html >
     <%
	response.setHeader("Cache-Control","no-cache");
	response.setHeader("Cache-Control","no-store");
	response.setHeader("Pragma","no-cache");
    %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>complaint-management system</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

<%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>   
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

<script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
<%--<script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
<link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
<link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
<link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
</head>
<body onload="getQueryData('getmyprebidquery');">
    <div class="mainDiv">
            <div class="fixDiv">
                 <%@include file="../resources/common/AfterLoginTop.jsp"%>
                 
                 <div class="dashboard_div">
  <!--Dashboard Header Start-->
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
  <div class="pageHead_1"> </div>
  
  <%if(session.getAttribute("userTypeId").toString().equals("2")) {%>
          <div class="pageHead_1"><span class="c-alignment-right"><a href="TendererDashboard.htm?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back to Dashboard</a></span></div> 
  <%}else if(session.getAttribute("procurementRole").toString().equals("PE")){%>
          <div class="pageHead_1"><span class="c-alignment-right"><a href="TendererDashboard.htm?tenderId=<%=request.getParameter("tenderId")%>" class="action-button-goback">Go Back to Dashboard</a></span></div> 
 <%}else if(session.getAttribute("procurementRole").toString().equals("HOPE")){%>
          <div class="pageHead_1"><span class="c-alignment-right"><a href="complaintOfficerHope.htm" class="action-button-goback">Go Back to Dashboard</a></span></div> 
		  <%}else if(session.getAttribute("procurementRole").toString().equals("Secretary")){%>
          <div class="pageHead_1"><span class="c-alignment-right"><a href="complaintOfficerHope.htm" class="action-button-goback">Go Back to Dashboard</a></span></div> 
<%}%>
   <%
               
        String tenderId="";
        if(request.getParameter("tenderId") != null){
    	tenderId = request.getParameter("tenderId");
    	
    }
			  // Variable tenderId is defined by u on ur current page.
             pageContext.setAttribute("tenderId",tenderId);
  %>

    <%@include file="../resources/common/TenderInfoBar.jsp" %>
                <table width="970" cellspacing="0" class="tableList_3 t_space">
                    <tr>
                        <th width="44">Sl. No.</th>
                        <th width="106">Level</th>
                        <th width="697">Comments</th>
                        <th width="113">Comments Date</th>
                    </tr>
                   <c:if test="${empty listOfficer}">
                    <tr colspan="4"><td colspan="4">  No comments found</td></tr> 
					</c:if>
                   
                <c:forEach items="${listOfficer}" var="history" >
     <c:set var="i" value="1"/>
      <tr>
                        <td>${i}</td> 
                        <c:set var="i" value="${i+1}"/>
                        <td width="106">${history.complaintLevel.complaintLevelId}</td>
                        <td width="697">${history.comments}</td>
                        <td width="113">${history.commentsdt}</td>
                    </tr>



</c:forEach>
                </table>

            </div>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@ include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
    </body>


</html>
