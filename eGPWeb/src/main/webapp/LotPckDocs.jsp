<%-- 
    Document   : LotPckDocs
    Created on : Dec 10, 2010, 12:42:13 PM
    Author     : Karan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tender Documents</title>

        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="resources/js/ddlevelsmenu.js">


            /***********************************************
             * All Levels Navigational Menu- (c) Dynamic Drive DHTML code library (http://www.dynamicdrive.com)
             * This notice MUST stay intact for legal use
             * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
             ***********************************************/

        </script>
    </head>
    
    <body>

     

         <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                      <%--  <td width="25%">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                        </td>--%>
                        <td class="contentArea_1" align="left" width="100%">
                            <!--Page Content Start-->
                            <div class="pageHead_1">
                            Tender Documents
                            </div>
                             <div>&nbsp;</div>
                            <%
                                 // Variable tenderId is defined by u on ur current page.
                                  pageContext.setAttribute("tenderId", request.getParameter("tenderId"));
                            %>
                            <%@include file="resources/common/TenderInfoBar.jsp" %>

                               <%
        tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        boolean isPackage = false, isLot = false; // for docAvlMethod
        boolean isPaid = false, isFree= false;

         String tenderId="", docAvlMethod="", docFeesMode="";
            if (request.getParameter("tenderId")!=null){
                tenderId=request.getParameter("tenderId");
            }

         /* Start: Get basic Tender Information */
         List<SPTenderCommonData> lstTenderInfo = tenderCommonService.returndata("getTenderInfoForDocView", tenderId, null);
         if (!lstTenderInfo.isEmpty() && lstTenderInfo.size()>0){

              docFeesMode=lstTenderInfo.get(0).getFieldName2();

                /*
                if (docFeesMode.equalsIgnoreCase("offline")){
                    isPaid= true;
                } else if (docFeesMode.equalsIgnoreCase("bank")){
                    isPaid = true;
                } else if (docFeesMode.equalsIgnoreCase("offline/bank")) {
                    isPaid = true;
                } else {
                    isFree = true;
                }
                */

               if ("Free".equalsIgnoreCase(lstTenderInfo.get(0).getFieldName7())){
                 isFree = true;
              } else if ("Paid".equalsIgnoreCase(lstTenderInfo.get(0).getFieldName7())){
                isPaid= true;
              }

                 docAvlMethod=lstTenderInfo.get(0).getFieldName3();
                 if (docAvlMethod.equalsIgnoreCase("package")){
                     isPackage = true;
                 } else   if (docAvlMethod.equalsIgnoreCase("lot")){
                     isLot = true;
                 }                 
         }
          /* End: Get basic Tender Information */
        %>

                            
                            <%if (isPaid || isFree) {%>
                                <div class="tabPanelArea_1">
                                    <p>
                                        If you are registered user, please logon to website to access tender document.
                                    </p>
                                    <div>&nbsp;</div>
                                    <p>
                                        New user <a href="NewUserRegistrationLoginDetails.jsp" style="font-weight: bold;color: black; text-decoration: underline;">click here</a> to register.
                                    </p>
                                </div>
                            <%} else if (isFree) {%>
                             <div class="tabPanelArea_1">

                                 <% if (isPackage) {
                                    List<SPTenderCommonData> PackageList = tenderCommonService.returndata("getPackageDescriptonForDocView", tenderId, null);
                                    %>
                                    <table width="100%" cellspacing="0" class="tableList_1">
                                      <tr>
                                         <th width="15%" class="t-align-center">Package. No.</th>
                                         <th width="70%" class="t-align-center">Package Description</th>
                                         <th class="t-align-center" width="15%">Action</th>
                                     </tr>
                                     <%if (!PackageList.isEmpty() && PackageList.size()>0 ) {%>
                                        <tr>
                                            <td class="t-align-left"><%=PackageList.get(0).getFieldName1()%></td>
                                            <td class="t-align-left"><%=PackageList.get(0).getFieldName2()%></td>
                                            <td class="t-align-left">
                                                <%if (PackageList.get(0).getFieldName3().equalsIgnoreCase("N.A.")){%>
                                                 <%--<a href="../tenderer/TenderDocView.jsp?tenderId=<%=tenderId%>&porlId=">Documents</a>--%>
                                                 <a href="../tenderer/TenderDocView.jsp?tenderId=<%=tenderId%>">Documents</a>
                                                 <%}%>
                                            </td>
                                        </tr>
                                    </table>
                                    <%}%>
                                 <%} else if (isLot) {%>
                                     <table width="100%" cellspacing="0" class="tableList_1">
                                     <tr>
                                         <th width="15%" class="t-align-center">Lot. No.</th>
                                         <th width="70%" class="t-align-center">Lot Description</th>
                                         <th class="t-align-center" width="15%">Action</th>
                                     </tr>
                                     <% int j = 0;
                                         tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                                         for (SPTenderCommonData sptcd : tenderCommonService.returndata("getLotDescriptonForDocView", tenderId, null)) {
                                            j++;
                                     %>
                                     <tr
                                         <%if(Math.IEEEremainder(j,2)==0) {%>
                                            class="bgColor-Green"
                                        <%} else {%>
                                            class="bgColor-white"
                                        <%}%>
                                         >
                                         <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                                         <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                                         <td class="t-align-left">
                                              <%if (sptcd.getFieldName4().equalsIgnoreCase("N.A.")) {%>
                                             <a href="../tenderer/TenderDocView.jsp?tenderId=<%=tenderId%>&porlId=<%=sptcd.getFieldName3()%>">Documents</a>
                                             <%}%>
                                         </td>
                                     </tr>
                                     <%}%>
                                 </table>
                                 <%}%>
                                
                            </div>
                            <%}%>                             
                            <div>&nbsp;</div>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
