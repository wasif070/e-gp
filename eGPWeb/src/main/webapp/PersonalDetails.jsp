<%--
    Document   : PersonalDetails
    Created on : Oct 21, 2010, 6:13:00 PM
    Author     : taher
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblTempTendererMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="tendererMasterDtBean" class="com.cptu.egp.eps.web.databean.TendererMasterDtBean"/>
    <jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
    <jsp:setProperty property="*" name="tendererMasterDtBean"/>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <%
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Bidder Registration : Company Contact Person Details</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="resources/js/jQuery/jquery.validate.js"></script>

        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="resources/js/poshytipLib/tip-yellowsimple/tip-yellowsimple.css" rel="stylesheet" type="text/css"/>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="resources/js/poshytipLib/jquery.poshytip.js" type="text/javascript"></script>






        <script type="text/javascript">
            $(document).ready(function () {

                $('.formTxtBox_1').poshytip({
                    className: 'tip-yellowsimple',
                    showOn: 'focus',
                    alignTo: 'target',
                    alignX: 'right',
                    alignY: 'center',
                    offsetX: 5,
                    showTimeout: 100
                });

                $("#signupForm").validate({
                    rules: {
                        UserType: {required: true},
                        title: {required: true},
                        country: {required: true},
                        state: {required: true},
                        tinDocName: {required: true},
                        tinNo: {maxlength: 100},
                        firstName: {required: true, FirstLastName: true, maxlength: 50},
                        middleName: {maxlength: 50, FirstLastName: true},
                        lasatName: { /*required: true,*/FirstLastName: true, maxlength: 50},
                        nationalIdNo: {required: true, maxlength: 25, alphaNationalId: true},
                        address1: {required: true, maxlength: 250},
                        address2: {/*required: true,*/ maxlength: 250},
                        city: { /*required: true,*/maxlength: 50, DigAndNum: true},
                        upJilla: {maxlength: 50},//, alphaNationalId: true},
                        postcode: { /*required: true,*/minlength: 4, number: true},
                        phoneNo: {number: true, maxlength: 20},
                        phoneSTD: {number: true, maxlength: 10},
                        faxNo: {number: true, maxlength: 20},
                        faxSTD: {number: true, maxlength: 10},
                        mobileNo: {required: true, number: true, minlength: 8, maxlength: 8},
                        emailAddress: {required: true, email: true},
                        designation: {required: true, alphaNationalId: true, maxlength: 30},
                        department: { /*required: true,*/maxlength: 30, alphaNationalId: true},
                        //specialization: {required: true},
                        comments: {required: true, maxlength: 100}
                    },
                    messages: {
                        UserType: {required: "<div class='reqF_1'>Please select User Type</div>"},
                        title: {required: "<div class='reqF_1'>Please select Title</div>"},
                        country: {required: "<div class='reqF_1'>Please select Country</div>"},
                        state: {required: "<div class='reqF_1'>Please enter Dzongkhag / District Name</div>"},
                        tinDocName: {required: "<div class='reqF_1'>Please select Document type</div>"},
                        tinNo: {
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed</div>"
                        },
                        firstName: {
                            required: "<div class='reqF_1'>Please enter  First Name</div>",
                            FirstLastName: "<div class='reqF_1'>Please enter Characters(A-Z, a-z) also allows dot (.) and Space</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>"
                        },
                        middleName: {
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>",
                            FirstLastName: "<div class='reqF_1'>Please enter Characters(A-Z, a-z) also allows dot (.) and Space</div>"
                        },
                        lasatName: {
                            FirstLastName: "<div class='reqF_1'>Please enter Characters(A-Z, a-z) also allows dot (.) and Space</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>"
                        },
                        nationalIdNo: {
                            required: "<div class='reqF_1'>Please enter National ID / Passport No. / Driving License No.</div>",
                            alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>"
                        },
                        address1: {
                            required: "<div class='reqF_1'>Please enter Address</div>",
                            maxlength: "<div class='reqF_1'>Maximum 250 characters are allowed</div>"
                        },
                        address2: {
                            //required: "<div class='reqF_1'>Please enter Address</div>",
                            maxlength: "<div class='reqF_1'>Maximum 250 characters are allowed</div>"
                        },
                        city: {
                            required: "<div class='reqF_1'>Please enter City / Town</div>",
                            maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>",
                            DigAndNum: "<div class='reqF_1'>Allows characters and Special characters (, . - '&) only. But only Special Characters are not allowed.</div>"
                        },
                        upJilla: {
                            //maxlength: "<br/>Maximum 50 characters are allowed",
                            //alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>"
                        },
                        postcode: {
                            /*required: "<div class='reqF_1'>Please enter Postcode / Zip Code</div>",*/
                            number: "<div class='reqF_1'>Only numbers (0-9) are allowed.</div>",
                            minlength: "<div class='reqF_1'>Minimum 4 characters are required</div>"
                        },
                        phoneNo: {
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 20 characters are allowed</div>"
                        },
                        phoneSTD: {
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 10 characters are allowed</div>"
                        },
                        faxNo: {
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 20 characters are allowed"
                        },
                        faxSTD: {
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 10 characters are allowed"
                        },
                        mobileNo: {
                            required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                            number: "<div class='reqF_1'>Please enter Numbers only</div>",
                            minlength: "<div class='reqF_1'>Exactly 8 digits required</div>",
                            maxlength: "<div class='reqF_1'>Exactly 8 digits required</div>"
                        },
                        emailAddress: {
                            required: "<div class='reqF_1'>Please enter  Email Address</div>",
                            email: "<div class='reqF_1'>Please enter valid e-mail ID</div>"
                        },
                        designation: {
                            required: "<div class='reqF_1'>Please enter Designation</div>",
                            alphaNationalId: "<div class='reqF_1'> (a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>",
                            maxlength: "<div class='reqF_1'>Max 30 characters are allowed</div>"
                        },
                        department: {
                            required: "<div class='reqF_1'>Please enter Department</div>",
                            maxlength: "<div class='reqF_1'>Max 30 Characters are allowed</div>",
                            alphaNationalId: "<div class='reqF_1'>Characters(a-z), Numbers(0-9) and Special Characters(#,(,),/,!,-,.,&,*,@,\\,[,]) are allowed. But only Special Characters are not allowed.</div>"
                        },
//                        specialization: {
//                            required: "<div class='reqF_1'>Please select Specialization </div>"
//                        },
                        comments: {
                            required: "<div class='reqF_1'>Please enter  Comment</div>",
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed</div>"
                        }
                    },
                    errorPlacement: function (error, element) {
                        if (element.attr("name") == "faxSTD") {
                            error.insertAfter("#txtFax");
                        } else if (element.attr("name") == "phoneSTD") {
                            error.insertAfter("#txtPhone");
                        } else {
                            error.insertAfter(element);
                        }
                    }
                });
            });
        </script>
        <script type="text/javascript">

            function numbersOnly(value) {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
            }

//            $(function () {// Automatic SubDistrict & Area code selection by Emtaz 21/May/2016
//                $('#cmbState').change(function () {
//                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue'}, function (j) {
//                        $('#cmbSubDistrict').html(j);
//                    });
//                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
//                       $('#phoneCode2').val(j);
//                       $('#faxCode2').val(j);
//                    });
//                });
//            });
            function SubDistrictANDAreaCodeLoad()
            {// Automatic SubDistrict & Area code selection by Emtaz 22/May/2016
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue'}, function (j) {
                    $('#cmbSubDistrict').html(j);
                });
                if($('#cmbSubDistrict').val()=="Phuentsholing")
                {
                    $('#phoneCode2').val('05');
                    $('#faxCode2').val('05');
                }
                else
                {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
                        $('#phoneCode2').val(j);
                        $('#faxCode2').val(j);
                    });
                }
            }
            $(function () {
                $('#cmbCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId: $('#cmbCountry').val(), funName: 'countryCode'}, function (j) {
                        $('#cntCode').val(j.toString());
                    });
                });
                
                $('#cmbState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtThana').html(j);
                    });
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                        $('#cmbSubDistrict').html(j);
                    });
                    
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
                        $('#phoneCode2').val(j);
                        $('#faxCode2').val(j);
                    });
                });
            });
            $(function () {
                $('#cmbCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCountry').val(), funName: 'stateComboValue'}, function (j) {
                        $("select#cmbState").html(j);
                        //Code by Proshanto
                        if ($('#cmbCountry').val() == "150") {//136

                            $('#trBangla').css("display", "table-row")
                            $('#trRthana').css("display", "table-row")
                            $('#trRSubdistrict').css("display", "table-row")
                            
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
                                $('#phoneCode2').val(j);
                                $('#faxCode2').val(j);
                            });
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                                $('#txtThana').html(j);
                            });
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('select#cmbState').val(), funName: 'getSubDistrictValueOffSubDistrict'}, function (j) {
                                $('#cmbSubDistrict').html(j);
                            });
                        } else {
                            $('#trBangla').css("display", "none")
                            $('#trRthana').css("display", "none")
                            $('#trRSubdistrict').css("display", "none")
                        }
                    });
                    $('#txtCity').val(null);
                    $('#txtPost').val(null);
                    $('#txtMob').val(null);
                    $('#txtPhone').val(null);
                    $('#txtFax').val(null);
                    $('#phoneCode2').val(null);
                    $('#faxCode2').val(null);
                });
                $('#cmbSubDistrict').change(function () {
                        //alert($('#cmbCorpSubdistrict').val());
                        if($('#cmbSubDistrict').val()=='Phuentsholing')
                            {$('#phoneCode2').val('05');
                            $('#faxCode2').val('05');}
                        else
                        {
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'AreaCodeSelection'}, function (j) {
                            $('#phoneCode2').val(j);
                            $('#faxCode2').val(j);
                    });
                        }
                    });
            });

            /*$(function() {
             $('#txtNationalId').blur(function() {                    
             if($.trim(('#txtNationalId').val()!="")){
             if(alphaForPassword($('#txtNationalId').val())){
             $('span.#natMsg').css("color","red");
             $('span.#natMsg').html("Checking for Unique National Id...");
             $.post("/CommonServlet", {nationalId:$('#txtNationalId').val(),funName:'verifyNationId'},
             function(j){                            
             if(j.toString().indexOf("N", 0)!=-1){
             $('span.#natMsg').css("color","red");
             }
             $('span.#natMsg').html(j);
             });
             }
             }
             });
             });*/

            function alphaForPassword(value) {
                return /^(?![a-zA-Z]+$)(?![0-9]+$)[a-zA-Z 0-9]+$/.test(value);

            }

            /*$(function() {
             $('#txtMob').blur(function() {                    
             if($.trim($('#txtMob').val())!=""){
             if($.trim($('#txtMob').val()).length>=10 && $.trim($('#txtMob').val()).length<=15){
             if(numbersOnly($('#txtMob').val())){
             $('span.#mobMsg').css("color","red");
             $('span.#mobMsg').html("Checking for Unique Mobile Number...");
             $.post("<-%//=request.getContextPath()%>/CommonServlet", {mobileNo:$('#txtMob').val(),funName:'verifyMobile'},
             function(j){                            
             if(j.toString().indexOf("M", 0)!=-1){
             $('span.#mobMsg').css("color","red");
             $('span.#mobMsg').html(j);
             }else{
             $('span.#mobMsg').css("color","green");
             $('span.#mobMsg').html("Ok");
             }
             
             });
             }
             }
             else{
             $('span.#mobMsg').html(null);
             }
             }
             });
             });*/

//            $(function() {
//                $('#txtThana').blur(function() {
//                    if($.trim($('#txtThana').val())==""){
//                            $('#upErr').html("Please enter Thana / UpaZilla");
//                            return false;
//                        }else{
//                            $('#upErr').html(null);
//                   }
//                });
//            });

            $(function () {
                $('#signupForm').submit(function () {
                    //Code by Proshanto
                    if ($('#cmbCountry').val() == "150") {//136
//                        if($.trim($('#txtThana').val())==""){
//                            $('#upErr').html("Please enter Thana / UpaZilla");
//                            return false;
//                        }else{
//                            $('#upErr').html(null);
//                        }
                    }
                    //if(($('span.#mobMsg').html()=="Mobile no. already exist")||($('span.#natMsg').html()=="National Id./Passport No./Driving License No. already exist")){
                    if ($('span.#mobMsg').html() == "Mobile no. already exist") {
                        return false;
                    }
                    if (document.getElementById("ValidityCheck").value == '0')
                    {
                        return false;
                    }
                    if ($('#signupForm').valid()) {
                        ChangeTitleValue();
                        if ($('#btnSave') != null) {
                            $('#btnSave').attr("disabled", "true");
                            $('#hdnbutton').val("Save");
                        }
                        if ($('#btnUpdate') != null) {
                            $('#btnUpdate').attr("disabled", "true");
                            $('#hdnedit').val("Update");
                        }
                    }
                });
            });
            <%--For emptying thana msg on blur--%>
            $(function () {
                $('#txtThana').blur(function () {
                    if ($.trim($('#txtThana').val()) != "") {
                        //if ($('#upErr').html() == "Please enter Dungkhag / Sub-district") {
                        //   $('#upErr').html(null);
                        //}
                    }
                });
            });
            $(function () {
                $('#cmbCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/CommonServlet", {countryId: $('#cmbCountry').val(), funName: 'countryCode'}, function (j) {
                        $('#phoneCode').val(j.toString());
                        $('#faxCode').val(j.toString());
                    });
                });
            });

            $(function () {
                $('#cmbState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtThana').html(j);
                    });
                });
            });
            $(function () {
                $('#cmbSubDistrict').change(function () {
                    if ($('#cmbSubDistrict').val() == "--Select Dungkhag--")
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                            $('#txtThana').html(j);
                        });
                    } else
                    {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbSubDistrict').val(), funName: 'subDistrictComboValue4'}, function (j) {
                            $('#txtThana').html(j);
                        });
                    }
                });
            });
            <%--end here--%>

            //nishith 10th may 2016
            $(function () {
                // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
                $("#dialog:ui-dialog").dialog("destroy");
                $("#dialog-form").dialog({
                    autoOpen: false,
                    resizable: false,
                    draggable: true,
                    height: 600,
                    width: 500,
                    modal: true,
                    close: function () {
                    }
                });

                $("#btnPreview").click(function () {
                    $("#dialog-form").dialog("open");

                    $("#prevUserType").text($("#cmbUserType").val() == "" ? "N/A" : $("#cmbUserType option:selected").text());
                    $("#prevTitle").text($("#cmbTitle").val() == "" ? "N/A" : ($("#cmbTitle").val() == "Other") ? $("#OtherTitleValueInput").val() : $("#cmbTitle option:selected").text());
                    $("#prevFirstName").text($("#txtFirstName").val() == "" ? "N/A" : $("#txtFirstName").val());
                    $("#prevMiddleName").text($("#txtMiddleName").val() == "" ? "N/A" : $("#txtMiddleName").val());
                    $("#prevLastName").text($("#txtLastName").val() == "" ? "N/A" : $("#txtLastName").val());
//                    $("#prevNameInDzongkha").text($("#txtNameBangla").val() == "" ? "N/A" : $("#txtNameBangla").val());
                    $("#prevCIDNo").text($("#txtNationalId").val() == "" ? "N/A" : $("#txtNationalId").val());
                    $("#prevDesignation").text($("#txtDesig").val() == "" ? "N/A" : $("#txtDesig").val());
                    $("#prevDepartment").text($("#txtDept").val() == "" ? "N/A" : $("#txtDept").val());
                    $("#prevAddress1").text($("#txtaAddr1").val() == "" ? "N/A" : $("#txtaAddr1").val());
                    $("#prevAddress2").text($("#txtaAddr2").val() == "" ? "N/A" : $("#txtaAddr2").val());
                    $("#prevCountry").text($("#cmbCountry").val() == "" ? "N/A" : $("#cmbCountry option:selected").text());
                    $("#prevDistrict").text($("#cmbState").val() == "" ? "N/A" : $("#cmbState option:selected").text());
                    $("#prevSubDistrict").text($("#cmbSubDistrict").val() == "--Select Dungkhag--" ? "N/A" : $("#cmbSubDistrict option:selected").text());
                    $("#prevThana").text($("#txtThana").val() == "--Select Gewog--" ? "N/A" : $("#txtThana option:selected").val());
                    $("#prevCity").text($("#txtCity").val() == "" ? "N/A" : $("#txtCity").val());
                    $("#prevPostCode").text($("#txtPost").val() == "" ? "N/A" : $("#txtPost").val());
                    $("#prevPhoneNo").text($("#txtPhone").val() == "" ? "N/A" : $("#txtPhone").val());
                    $("#prevFaxNo").text($("#txtFax").val() == "" ? "N/A" : $("#txtFax").val());
                    $("#prevMobNo").text($("#txtMob").val() == "" ? "N/A" : $("#txtMob").val());
                    $("#prevEmail").text($("#txtEmail").val() == "" ? "N/A" : $("#txtEmail").val());
                });
            });


        </script>
    </head>
    <%
        tendererMasterSrBean.setLogUserId(session.getAttribute("userId").toString());
        boolean ifEdit = false;
        TblTempTendererMaster dtBean = tendererMasterSrBean.getTendererDetails(Integer.parseInt(session.getAttribute("userId").toString()));//Integer.parseInt(session.getAttribute("userId").toString())
        if (dtBean != null) {
            ifEdit = true;
        }
        String msg = null;
    %>
    <body onload="SubDistrictANDAreaCodeLoad();<%if (ifEdit) {%>TitleChange('<%=dtBean.getTitle()%>') <% } else { %>TitleChange('') <%}%>">
        <%

            if ("Save".equals(request.getParameter("hdnbutton"))) {
                tendererMasterDtBean.setUserType(Integer.parseInt(request.getParameter("UserType")));
                if (null != request.getParameter("cmpId")) {
                    tendererMasterDtBean.setCmpId(Integer.parseInt(request.getParameter("cmpId")));
                }
                /*if(tendererMasterDtBean.getMobileNo()!=null && tendererMasterDtBean.getNationalIdNo()!=null){
                            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                            if(userRegisterService.checkCountByHql("TblTendererMaster tttm", "tttm.mobileNo='"+tendererMasterDtBean.getMobileNo()+"'")!=0){
                                msg="Mobile no. already exist";
                            }else{
                                if(false){
                                //(Server side nationalId validation)if(userRegisterService.checkCountByHql("TblTendererMaster tttm", "tttm.nationalIdNo='"+tendererMasterDtBean.getNationalIdNo()+"'")!=0){
                                    msg="National Id./Passport No./Driving License No. already exist";
                                }else{*/
                //tendererMasterDtBean.setNationalIdNo("");
                if ("--Select Dungkhag--".equalsIgnoreCase(request.getParameter("subDistrict"))) {
                    tendererMasterDtBean.setSubDistrict("");
                }
                if ("--Select Gewog--".equalsIgnoreCase(request.getParameter("upJilla"))) {
                    tendererMasterDtBean.setUpJilla("");
                }
                String pageName = tendererMasterSrBean.registerPersonalDetail(tendererMasterDtBean, Integer.parseInt(session.getAttribute("userId").toString()));//Integer.parseInt(session.getAttribute("userId").toString())
                String redirectPage = pageName + ".jsp";
                out.write("<script type=\"text/javascript\">window.location = \"" + redirectPage + "\"</script>");
                /*}
                            }
                        }*/
            }
            if ("Update".equals(request.getParameter("hdnedit"))) {
                /*if(tendererMasterDtBean.getMobileNo()!=null && tendererMasterDtBean.getNationalIdNo()!=null){
                            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                            if(userRegisterService.checkCountByHql("TblTendererMaster tttm", "tttm.mobileNo='"+tendererMasterDtBean.getMobileNo()+"'")!=0){
                                msg="Mobile no. already exist";
                            }else{
                                if(false){
                                //if(userRegisterService.checkCountByHql("TblTendererMaster tttm", "tttm.nationalIdNo='"+tendererMasterDtBean.getNationalIdNo()+"'")!=0){
                                    msg="National Id./Passport No./Driving License No. already exist";
                                }else{*/
                if ("--Select Dungkhag--".equalsIgnoreCase(request.getParameter("subDistrict"))) {
                    tendererMasterDtBean.setSubDistrict("");
                }
                if ("--Select Gewog--".equalsIgnoreCase(request.getParameter("upJilla"))) {
                    tendererMasterDtBean.setUpJilla("");
                }
                tendererMasterDtBean.setTendererId(dtBean.getTendererId());
                tendererMasterDtBean.setCmpId(dtBean.getTblTempCompanyMaster().getCompanyId());
                tendererMasterDtBean.setUserId(dtBean.getTblLoginMaster().getUserId());
                tendererMasterDtBean.setIsAdmin(dtBean.getIsAdmin());
                tendererMasterDtBean.setUserType(Integer.parseInt(request.getParameter("UserType")));
                //tendererMasterDtBean.setNationalIdNo("");
                System.out.print(request.getParameter("UserType"));
                if (tendererMasterSrBean.updateIndividualClient(tendererMasterDtBean, false)) {
                    //response.sendRedirect("PersonalDetails.jsp?msg=s");

                    out.write("<script type=\"text/javascript\">window.location = \"PersonalDetails.jsp?msg=s\"</script>");

                } else {
                    msg = "Error in Updating PersonalDetails";
                }
                /*}
                            }
                        }*/
            }
        %>


        <!--nishith 10th may 2016 -->
        <div style="display: none" id="dialog-form" title="Electronic Procurement System (e-GP) - Personal Information">


            <!--Page Content Start-->
            <div class="pageHead_1">Company Contact Person Details Preview</div>

            <div style='padding:15px;'>
                <table border='0' cellspacing='0' cellpadding='0' class='formStyle_1 c_t_space' width='100%'>


                    <tr>
                        <td class='ff' width='35%'>User Type : </td>
                        <td id='prevUserType' width='65%'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff' width='35%'>Title : </td>
                        <td id='prevTitle' width='65%'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>First Name : </td>
                        <td id='prevFirstName'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Middle Name : </td>
                        <td  id='prevMiddleName'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Last Name : </td>
                        <td id='prevLastName'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Designation : </td>
                        <td id='prevDesignation'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class="ff">Department : </td>
                        <td id='prevDepartment'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Address : </td>
                        <td id='prevAddress1'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Country : </td>
                        <td id='prevCountry'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Dzongkhag / District : </td>
                        <td id='prevDistrict'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>

                    <tr id="trPSubdistrict">
                        <td class='ff'>Dungkhag / Sub-district : </td>
                        <td id='prevSubDistrict'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr id="trPthana">
                        <td class='ff'>Gewog : </td>
                        <td id='prevThana'></td>
                    </tr> 
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>City / Town : </td>
                        <td id='prevCity'></td>
                    </tr>

                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>

                    <tr>
                        <td class='ff'>Post Code : <!--span>*</span--></td>
                        <td id='prevPostCode'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>                         
                    <tr>
                        <td class='ff'>Email Address : </td>
                        <td id='prevEmail'></td>
                    </tr>

                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Mobile No. : </td>
                        <td id='prevMobNo'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr> 
                    <tr>
                        <td class='ff'>Phone No. : </td>
                        <td id='prevPhoneNo'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>
                    <tr>
                        <td class='ff'>Fax No. : </td>
                        <td id='prevFaxNo'></td>
                    </tr>
                    <tr>
                        <td colspan='2' height='7'></td>
                    </tr>                                    

                </table>

                <!--Page Content End-->
            </div>          

        </div>

        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                    <!--Middle Content Table Start-->                
                    <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">

                            <td class="contentArea_1">
                                <!--Page Content Start-->
                                <div class="pageHead_1">Bidder Registration - Company Contact Person Details</div>
                            <jsp:include page="EditNavigation.jsp" ></jsp:include>
                            <%if ("s".equals(request.getParameter("msg"))) {%><br/><div id="succMsg" class="responseMsg successMsg"  >Information Successfully Updated.</div><%}%>
                            <%if (msg != null) {%><br/><div class="responseMsg errorMsg"><%=msg%>.</div><%}%>
                            <form id="signupForm" name="signupForm" method="post" action="PersonalDetails.jsp">
                                <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1 c_t_space" width="100%">
                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="" align="right">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" height="30">User Type : <span>*</span></td>
                                        <td><select name="UserType" class="formTxtBox_1" id="cmbUserType" >
                                                <%--<%if (!ifEdit) {%>
                                                    <option  value="0">Select User Type</option>
                                                <%}%>--%>
                                                <option  value="1" <%if (ifEdit) {
                                                                 if (dtBean.getUserType()==1) {%>selected<%}
                                                                     }%>>Owner</option>
                                                <option value="2" <%if (ifEdit) {
                                                                if (dtBean.getUserType()==2) {%>selected<%}
                                                                    }%>>Authorized User</option>
                                               
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="22%">Title : <span>*</span></td>
                                        <td width="78%"><select name="title" class="formTxtBox_1" id="cmbTitle" style="width: 215px;" <%if (ifEdit) {%>onchange="TitleChange('<%=dtBean.getTitle()%>')" <% } else { %>onchange="TitleChange('')" <%}%> >
                                                <%if (!ifEdit) {%><option value="">Select</option><%}%>
                                                        <option value="Mr." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Mr.")) {%>selected<%}
                                                                            }%>>Mr.</option>
                                                        <option value="Ms." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Ms.")) {%>selected<%}
                                                                            }%>>Ms.</option>
                                                        <option value="Mrs." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Mrs.")) {%>selected<%}
                                                                            }%>>Mrs.</option>
                                                        <option value="Dasho" <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Dasho")) {%>selected<%}
                                                                            }%>>Dasho</option>
                                                                            <option value="Dr." <%if (ifEdit) {
                                                                if (dtBean.getTitle().equals("Dr.")) {%>selected<%}
                                                                            }%>>Dr.</option>
                                                        <option id="OtherTitle" value="Other" <%if (ifEdit) {
                                                                if (!dtBean.getTitle().equals("Dasho") && !dtBean.getTitle().equals("Dr.") && !dtBean.getTitle().equals("Mrs.") && !dtBean.getTitle().equals("Ms.") && !dtBean.getTitle().equals("Mr.")) {%>selected<%}
                                                                            }%>>Other</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <!--By Emtaz for showing Other Titles on 24/May/2016-->
                                    <tr>
                                        <td class="ff"><label id="OtherTitleName"></label></td>
                                        <td>
                                            <label id="OtherTitleValue"></label>
                                            <span id="OtherTitleMsg" style="color: red;">&nbsp;</span>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">First Name : <span>*</span></td>
                                        <td><input name="firstName" type="text" class="formTxtBox_1" maxlength="50" id="txtFirstName" style="width:200px;" title="Name should not be prefixed with any title such as Mr., Ms., Mrs. , Dr., Others. <br/>Individual applicants should provide full/abbreviated name as in the Citizenship Identity Card (CID). Only the First Name is mandatory Name, if abbreviated, should necessarily contain the last name." onblur="ToUpperCase(this)" value="<%if (ifEdit) {
                                                out.print(dtBean.getFirstName());
                                            }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Middle Name : </td>
                                        <td><input name="middleName" type="text" class="formTxtBox_1" maxlength="50" id="txtMiddleName" style="width:200px;" onblur="ToUpperCase(this)"  value="<%if (ifEdit) {
                                                out.print(dtBean.getMiddleName());
                                            }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Last Name : <!--<span>*</span>--></td>
                                        <td><input name="lasatName" type="text" class="formTxtBox_1" maxlength="50" id="txtLastName" style="width:200px;" onblur="ToUpperCase(this)"  value="<%if (ifEdit) {
                                                out.print(dtBean.getLasatName());
                                            }%>"/></td>
                                    </tr>
<!--                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>-->
                                    <!--<tr id="trBangla">-->
                                    <!--<td class="ff">Name in Dzongkha :</td>-->
                                    <input name="banglaName" type="hidden" class="formTxtBox_1" id="txtNameBangla" maxlength="300" onblur="ToUpperCase(this)" value="<%if (ifEdit) {
                                            if (dtBean.getFullNameInBangla() != null) {
                                                out.print(BanglaNameUtils.getUTFString(dtBean.getFullNameInBangla()));
                                            }
                                        }%>"/>
                                    <!--<div class="formNoteTxt">(If Bhutanese)</div></td>-->
                                    <!--</tr>-->
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <!--                                    <tr>
                                                                            <td class="ff">CID No. : <span>*</span></td>
                                                                            <td><input name="nationalIdNo" type="text" class="formTxtBox_1" id="txtNationalId" style="width:200px;"  maxlength="25" value="<%if (ifEdit) {
                                                                                    out.print(dtBean.getNationalIdNo());
                                                                                }%>"/>
                                                                                <span id="natMsg" style="color: red;">&nbsp;</span>
                                                                            </td>
                                                                        </tr>-->
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Designation : <span>*</span></td>
                                        <td><input name="designation" type="text" class="formTxtBox_1" maxlength="30" id="txtDesig" style="width:200px;"   value="<%if (ifEdit) {
                                                out.print(dtBean.getDesignation());
                                            }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Department : <!--<span>*</span>--></td>
                                        <td><input name="department" type="text" class="formTxtBox_1" maxlength="30" id="txtDept" style="width:200px;"   value="<%if (ifEdit) {
                                                out.print(dtBean.getDepartment());
                                            }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Address : <span>*</span></td>
                                        <td><textarea name="address1" rows="4" class="formTxtBox_1" id="txtaAddr1" maxlength="250" style="width:400px;"><%if (ifEdit) {
                                                out.print(dtBean.getAddress1());
                                            }%></textarea></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <!--                                    <tr>
                                                                            <td class="ff">Address Line 2 : </td>
                                                                            <td><textarea name="address2" rows="2" class="formTxtBox_1" id="txtaAddr2" maxlength="250" style="width:400px;"><%if (ifEdit) {
                                                                                    out.print(dtBean.getAddress2());
                                                                                }%></textarea></td>
                                                                        </tr>-->
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Country : <span>*</span></td>
                                        <td><select name="country" class="formTxtBox_1" id="cmbCountry">
                                                <%
                                                    String countryName = "Bhutan";
                                                    if (ifEdit) {
                                                        countryName = dtBean.getCountry();
                                                    }
                                                    for (SelectItem country : tendererMasterSrBean.getCountryList()) {
                                                %>
                                                <option  value="<%=country.getObjectId()%>" <%if (country.getObjectValue().equals(countryName)) {%>selected<%}%>><%=country.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Dzongkhag / District : <span>*</span></td>
                                        <td><select name="state" class="formTxtBox_1" id="cmbState" style="width:218px;">
                                                <%String cntName = "";
                                                    if (ifEdit) {
                                                        cntName = dtBean.getCountry();
                                                    } else {
                                                        cntName = "Bhutan";
                                                    }
                                                    for (SelectItem state : tendererMasterSrBean.getStateList(cntName)) {%>
                                                <option value="<%=state.getObjectValue()%>" <%if (ifEdit) {
                                                        if (state.getObjectValue().equals(dtBean.getState())) {%>selected<%}
                                                        } else //Change Dhaka to Thimphu,Proshanto
                                                            if (state.getObjectValue().equals("Thimphu")) {%>selected<%}%>><%=state.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>

                                    <tr id="trRSubdistrict">
                                        <td class="ff">Dungkhag / Sub-district : </td>
                                        <td><select name="subDistrict" class="formTxtBox_1" id="cmbSubDistrict" style="width:218px;">

                                                <%String regsubdistrictName = "";
                                                    if (ifEdit) {
                                                        regsubdistrictName = dtBean.getState();
                                                    } else {
                                                        regsubdistrictName = "Thimphu";
                                                    }
                                                    for (SelectItem subdistrict : tendererMasterSrBean.getSubdistrictList(regsubdistrictName)) {%>
                                                <option value="<%=subdistrict.getObjectValue()%>" <%if (ifEdit) {
                                                        if (subdistrict.getObjectValue().equals(dtBean.getSubDistrict())) {
                                                        %>selected<%
                                                            }
                                                        } else if (subdistrict.getObjectValue().equals("Phuentsholing")) {%>
                                                        selected<%
                                                            }%>><%=subdistrict.getObjectValue()%></option>
                                                <%}%>
                                            </select></td>

                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr id="trRthana">
                                        <td class="ff" height="30">Gewog : </td>
                                        <td><select name="upJilla" class="formTxtBox_1" id="txtThana" style="width:218px;">
                                                <%String state = "Thimphu";
                                                    String subdistrict = "";
                                                    if (ifEdit) {
                                                        state = dtBean.getState();
                                                        subdistrict = dtBean.getSubDistrict();
                                                    }
                                                    if (!state.isEmpty() && !subdistrict.isEmpty()) {
                                                        for (SelectItem gewoglist : tendererMasterSrBean.getGewogList2(subdistrict)) {%>
                                                <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                        if (gewoglist.getObjectValue().equals(dtBean.getUpJilla())) {
                                                        %>selected<%
                                                            }
                                                        } else if (gewoglist.getObjectValue().equals("")) {%>
                                                        selected<%
                                                            }%>><%=gewoglist.getObjectValue()%></option>
                                                <%}
                                                } else {
                                                    for (SelectItem gewoglist : tendererMasterSrBean.getGewogList(state)) {%>
                                                <option value="<%=gewoglist.getObjectValue()%>" <%if (ifEdit) {
                                                        if (gewoglist.getObjectValue().equals(dtBean.getUpJilla())) {
                                                        %>selected<%
                                                                }
                                                            }%>

                                                        ><%=gewoglist.getObjectValue()%></option>
                                                <%}
                                                    }
                                                %>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>

                                    <tr>
                                        <td class="ff">City / Town : <!--<span>*</span>--></td>
                                        <td><input name="city" type="text" class="formTxtBox_1" maxlength="50" id="txtCity" style="width:200px;"  value="<%if (ifEdit) {
                                                out.print(dtBean.getCity());
                                            }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <%--<tr id="trRthana">
                                        <td class="ff">Gewog : <!--<span>*</span>--></td>
                                        <td><input name="upJilla" type="text" class="formTxtBox_1" maxlength="50" id="txtThana" style="width:200px;"  value="<%if (ifEdit) {
                                                out.print(dtBean.getUpJilla());
                                            }%>"/><div id="upErr" style="color: red;"></div></td>
                                    </tr>--%>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <%if (ifEdit) {
                                            if (!dtBean.getCountry().equalsIgnoreCase("Bhutan")) {%>
                                    <script type="text/javascript">
                                        $('#trBangla').css("display", "none");
                                        $('#trRthana').css("display", "none");
                                        $('#trRSubdistrict').css("display", "none");
                                    </script>
                                    <%}
                                        }%>
                                    <tr>
                                        <td class="ff">Post Code : <!--span>*</span--></td>
                                        <td><input name="postcode" type="text" class="formTxtBox_1" maxlength="10" id="txtPost" style="width:200px;"  value="<%if (ifEdit) {
                                                out.print(dtBean.getPostcode());
                                            }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Email Address : <span>*</span></td>
                                        <td><input name="emailAddress" type="text" class="formTxtBox_1" maxlength="30" id="txtEmail" style="width:200px;"   value="<%if (ifEdit) {
                                                out.print(dtBean.getEmailAddress());
                                            }%>"/></td>
                                    </tr>
                                    <%--<tr>
                                        <td class="ff">Website : </td>
                                        <td><input name="website" type="text" class="formTxtBox_1" maxlength="100" id="txtWebsite" style="width:200px;"  value="<%if (ifEdit) {
                                                        out.print(dtBean.getWebsite());
                                                    }%>"/></td>
                                    </tr> --%>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Mobile No. : <span>*</span></td>
                                        <td>
                                            <input class="formTxtBox_1" style="width: 30px" id="cntCode" value="<%CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                if (ifEdit) {
                                                    out.print(commonService.countryCode(dtBean.getCountry(), false));
                                                } else {
                                                    out.print(commonService.countryCode("Bhutan", false));
                                                }%>" readonly/>-<input name="mobileNo" type="text" class="formTxtBox_1" maxlength="16" id="txtMob" style="width:160px;"  value="<%if (ifEdit) {
                                                        out.print(dtBean.getMobileNo());
                                                    }%>"/>
                                            <span id="mobMsg" style="color: red;">&nbsp;</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>  
                                    <tr>
                                        <td class="ff">Phone No. : </td>
                                        <td><input class="formTxtBox_1" style="width: 30px" id="phoneCode" value="<%
                                            if (ifEdit) {
                                                out.print(commonService.countryCode(dtBean.getCountry(), false));
                                            } else {
                                                out.print(commonService.countryCode("Bhutan", false));
                                            }%>" readonly/>-<input type="text" id="phoneCode2" value="<%if (ifEdit) {
                                                    if (!"".equals(dtBean.getPhoneNo())) {
                                                                                                           out.print(dtBean.getPhoneNo().split("-")[0]);
                                                                                                       }

                                                                                                   }%>" name="phoneSTD" class="formTxtBox_1" style="width:50px;"  />-<input name="phoneNo" type="text" class="formTxtBox_1" maxlength="20" id="txtPhone" style="width:100px;"  value="<%if (ifEdit) {
                                                                                                if (!"".equals(dtBean.getPhoneNo())) {
                                                                                                    out.print(dtBean.getPhoneNo().split("-")[1]);
                                                                                                }
                                                                                            }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Fax No. : </td>
                                        <td><input class="formTxtBox_1" style="width: 30px" id="faxCode" value="<%
                                            if (ifEdit) {
                                                out.print(commonService.countryCode(dtBean.getCountry(), false));
                                            } else {
                                                out.print(commonService.countryCode("Bhutan", false));
                                            }%>" readonly/>-<input type="text" id="faxCode2" value="<%if (ifEdit) {
                                                    if (!"".equals(dtBean.getFaxNo())) {
                                                                                                           out.print(dtBean.getFaxNo().split("-")[0]);
                                                                                                       }
                                                                                                   }%>" name="faxSTD" class="formTxtBox_1" style="width:50px;"  />-<input name="faxNo" type="text" class="formTxtBox_1" maxlength="20" id="txtFax" style="width:100px;"  value="<%if (ifEdit) {
                                                                                           if (!"".equals(dtBean.getFaxNo())) {
                                                                                               out.print(dtBean.getFaxNo().split("-")[1]);
                                                                                           }
                                                                                       }%>"/></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" height="7"></td>
                                    </tr>                                    


                                    <tr>
                                        <td>&nbsp;<input name="comments" type="hidden" class="formTxtBox_1" maxlength="100" id="txtComment" style="width:200px;"  value="user"/></td>
                                        <td>                                            
                                            <%if (ifEdit) {%>
                                            <label class="formBtn_1">
                                                <input type="button" name="preview" id="btnPreview" value="Preview"/>

                                            </label>
                                            <label class="formBtn_1">
                                                <input type="submit" name="edit" id="btnUpdate" value="Update" />
                                                <input type="hidden" name="hdnedit" id="hdnedit" value=""/>
                                            </label>
                                            <%} else {%>
                                            <label class="formBtn_1">
                                                <input type="button" name="preview" id="btnPreview" value="Preview"/>

                                            </label>
                                            <label class="formBtn_1">
                                                <input type="submit" name="button" id="btnSave" value="Save"/>
                                                <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                                            </label>
                                            <%}%>
                                        </td>
                                    </tr>
                                    <input type="hidden" id="ValidityCheck" value="1" />
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                        <!--                            <td width="266" class="td-background">
                        <%--<jsp:include page="resources/common/Left.jsp" ></jsp:include>--%>
                        </td>-->
                    </tr>
                </table>                
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script>
        //Emtaz 24/May/2016.
        function ToUpperCase(obj)
        {
            obj.value = obj.value.toUpperCase();
        }
        function TitleChange(TitleValue)
        {
            if (document.getElementById("cmbTitle").value == 'Other')
            {
                document.getElementById('OtherTitleName').innerHTML = 'Other Title :<span>*</span>';
                document.getElementById('OtherTitleValue').innerHTML = '<input class="formTxtBox_1" maxlength="50" type="text" id="OtherTitleValueInput" name="OtherTitleValueInput" onblur="TitleValidity()" value="' + TitleValue + '" />';
            } else
            {
                document.getElementById('OtherTitleName').innerHTML = '';
                document.getElementById('OtherTitleValue').innerHTML = '';
                $('#OtherTitleMsg').html("");
                document.getElementById("ValidityCheck").value = "1";
            }
        }
        function ChangeTitleValue()
        {
            if (document.getElementById("cmbTitle").value == 'Other')
            {
                var OtherTitleValue = document.getElementById("OtherTitleValueInput").value;
                document.getElementById("OtherTitle").value = OtherTitleValue;
            }
        }
        function TitleValidity()
        {
            if ($("#OtherTitleValueInput").val().length > 5)
            {
                $('#OtherTitleMsg').html("Maximum 5 characters are allowed.");
                document.getElementById("ValidityCheck").value = "0";
            } else if ($("#OtherTitleValueInput").val() == "")
            {
                $('#OtherTitleMsg').html("Enter Title.");
                document.getElementById("ValidityCheck").value = "0";
            } else
            {
                $('#OtherTitleMsg').html("");
                document.getElementById("ValidityCheck").value = "1";
            }
        }
        //Emtaz
    </script>
</html>
