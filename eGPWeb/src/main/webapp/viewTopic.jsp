
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.model.table.TblPublicProcureForum"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.PublicForumPostService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Forum Topic</title>
        <link href="resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <body>
        <%
                    
                    String ppfid1 = request.getParameter("ppfid");
                    String strUserTypeId = "";
                    Object objUserId = session.getAttribute("userId");
                    Object objUName = session.getAttribute("userName");
                    boolean isLoggedIn = false;
                    boolean fromHome = false;
                    if (objUserId != null) {
                        strUserTypeId = session.getAttribute("userTypeId").toString();
                    }
                    if (objUName != null) {
                        isLoggedIn = true;
                    }
                    if(request.getParameter("h") != null)
                        fromHome = true;
        %>

        <%
                    PublicForumPostService publicForumPostService = (PublicForumPostService) AppContext.getSpringBean("PublicForumPostService");
                    String ppfid = request.getParameter("ppfid");
                    String parentPPFId = "";
                    String subject = "";
                    String description = "";
                    Date date = null;
                    String user = "";
                   String viewType=request.getParameter("ViewType");
                    
                      // Coad added by Dipal for Audit Trail Log.
                    if(session.getAttribute("userTypeId")!= null){
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        String idType="userId";
                        int auditId=Integer.parseInt(session.getAttribute("userId").toString());
                        String auditAction="View Topic";
                        if(request.getParameter("ViewType") !=null && request.getParameter("ViewType").equalsIgnoreCase("MYTOPIC"))
                        {
                            auditAction="View Topic";
                        }
                        else if (request.getParameter("ViewType") !=null && request.getParameter("ViewType").equalsIgnoreCase("MYREPLIED"))
                        {
                            auditAction="View Reply";
                        }
                        if(!session.getAttribute("userTypeId").toString().equalsIgnoreCase("2"))
                            auditAction=auditAction+" by Officer";
                        else
                            auditAction=auditAction+" by Tenderer";
                        
                        String moduleName=EgpModule.Public_Procurement_Forum.getName(); 
                        String remarks="";
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    }
                    List<TblPublicProcureForum> colTopicDetail = publicForumPostService.getAllForums(ppfid);
                    for (TblPublicProcureForum topicProcureForum : colTopicDetail) {
                        parentPPFId = topicProcureForum.getPpfParentId().toString();
                        //out.println("parentPPFId  : "+parentPPFId);
                        if (parentPPFId.equals("0")) {
                            subject = topicProcureForum.getSubject();
                            description = topicProcureForum.getDescription();
                            date = topicProcureForum.getPostDate();
                            user = topicProcureForum.getFullName();
                        } else {
                            List<TblPublicProcureForum> colReplyDetail = publicForumPostService.getAllForums(parentPPFId);
                            for (TblPublicProcureForum replyProcureForum : colReplyDetail) {
                                subject = replyProcureForum.getSubject();
                                description = replyProcureForum.getDescription();
                                date = replyProcureForum.getPostDate();
                                user = replyProcureForum.getFullName();
                                ppfid = parentPPFId;
                            }
                        }

        %>

        <script type="text/javascript">
            var replyLink = "PostReply.jsp?ppfid=<%=ppfid%>&ViewType=<%=viewType%>";
            //alert("replyLink : "+replyLink);
            var isLoggedIn = 'true';
            //alert("isLoggedIn"+isLoggedIn);
            function checkLogin()
            {
                if(isLoggedIn == "true")
                {
                    window.location=replyLink;
                    return true;
                }
                else
                {
                    jAlert("If you are registered user, please logon to Post Reply.","Post Reply",function(RetVal) {
                    });
                    return false;
                }
            }
        </script>

        <div class="dashboard_div">            
                <div class="fixDiv">
                    <jsp:include page="resources/common/Top.jsp" ></jsp:include> 

                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1">View Forum Topic<%if (strUserTypeId.equalsIgnoreCase("8") && !fromHome) {%>
                            <span class="c-alignment-right"><a href="ViewAllTopic.jsp" class="action-button-goback">Go Back</a></span> </div>
                            <%} else {%>
                        <span class="c-alignment-right"><a href="procForum.jsp?verificationRequired=no" class="action-button-goback">Go Back</a></span> </div>
                        <%}%>
                        <%if ("y".equals(request.getParameter("succMsg"))) {%>
                    <div id="succMsg" class="responseMsg successMsg">Reply given successfully. Reply will be displayed to the other users only if the reply content is verified and accepted by the Content Admin.</div>
                    <%}%>

                    <div class="t_space b_space t-align-right">
                        <a href="PostReply.jsp?ppfid=<%=ppfid%>&ViewType=<%=viewType%>" class="action-button-add">Post Reply</a>
                    </div>

                    <table width="100%" cellspacing="0" class="tableList_1">



                        <tr>
                            <th>
                                <%=subject%> <span style="font-size:11px; font-weight:normal;">(Posted by: <label id="author" class="dynamicFont"><%=user%></label>
                                    on <label id="date" class="dynamicFont"><%=DateUtils.gridDateToStr(date)%></label> of Posting)</span>
                            </th>
                        </tr>

                        <tr>
                            <td>
                                <%=description%>
                            </td>
                        </tr>
                    </table>
                    <%}%>
                    <table width="100%" cellspacing="0" class="tableList_1">

                        <%


                                    List<TblPublicProcureForum> colResult1 = publicForumPostService.getAllReply(ppfid);
                                    int i = 0;
                                    for (TblPublicProcureForum procureForum : colResult1) {

                                        String descriptionreply = procureForum.getDescription();
                                        Date datereply = procureForum.getPostDate();
                                        String userreply = procureForum.getFullName();
                                        String status = procureForum.getStatus();
                                        String uName = "";
                                        
                                        if (status.equalsIgnoreCase("accepted")) {
                                            if (i % 2 == 0) {
                                                out.println("<tr><td class='bgColor-Green'>");
                                            } else {
                                                out.println("<td class='bgColor-white'>");
                                            }
                                            i++;

                        %>
                        <tr>
                          <td>
                        <div class=" ff" id="reply">
                            <%=descriptionreply%> <span style="font-size:11px; font-weight:normal;">(Replied by: <label id="user" class="dynamicFont"><%= userreply%></label> on <label id="date" class="dynamicFont"><%=DateUtils.gridDateToStr(datereply)%></label>)</span>
                            <br />
                            <br />
                            <br />
                        </div>
                        </td>
                        </tr>
                        <%
                                                                    } else if (userreply.equalsIgnoreCase(uName)) {
                                                                        if (i % 2 == 0) {
                                                                            out.println("<tr><td class='bgColor-Green'>");
                                                                        } else {
                                                                            out.println("<td class='bgColor-white'>");
                                                                        }
                                                                        i++;

                        %>
                        <tr>
                         <td>
                        <div class=" ff" id="reply">
                            <%=descriptionreply%> <span style="font-size:11px; font-weight:normal;">(Replied by: <label id="user" class="dynamicFont"><%= userreply%></label> on <label id="date" class="dynamicFont"><%=DateUtils.gridDateToStr(datereply)%></label>)</span>
                            <br />
                            <br />
                            <br />
                        </div>
                        </td>
                        </tr>

                        <% } else {
                           continue;
                       }

                   }%>
                    </table>
                    <div class="t_space b_space t-align-right">
                        <a href="PostReply.jsp?ppfid=<%=ppfid%>&ViewType=<%=viewType%>"  class="action-button-add">Post Reply</a>
                    </div>

                    <div>&nbsp;</div>

                    <!--Dashboard Content Part End-->


                </div>
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
    </body>
</html>
