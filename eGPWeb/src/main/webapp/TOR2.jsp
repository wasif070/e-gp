<%-- 
    Document   : TOR2
    Created on : Apr 9, 2011, 11:58:05 AM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ReportCreationService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="java.util.List"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    CommonService commonService = (CommonService)AppContext.getSpringBean("CommonService");
                    String repLabel=null;
                    String eventType = commonService.getEventType(request.getParameter("tenderid")).toString();
                    if(commonService.getProcNature(request.getParameter("tenderid")).toString().equals("Services")){
                        repLabel = "Proposal";
                    }else{
                        repLabel = "Tender";
                    }
                    boolean bol_flag= true;
                    if(request.getParameter("isPDF")!=null && "true".equalsIgnoreCase(request.getParameter("isPDF"))){
                        bol_flag = false;
                    }
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=repLabel%> Opening Report 2</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
         <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        
        <!--
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        -->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <body>
        <%
                    String tenderid = request.getParameter("tenderid");
                    String lotId = request.getParameter("lotId");
                    ReportCreationService creationService = (ReportCreationService) AppContext.getSpringBean("ReportCreationService");
                    List<SPCommonSearchDataMore> tendOpeningRpt = creationService.getOpeningReportData("getTenderOpeningRpt", tenderid, lotId,null);//lotid
                    List<SPCommonSearchDataMore> tendererList = creationService.getOpeningReportData("getTOR2BidderList", tenderid, lotId,null);//lotid
                    boolean isSecurityAvail = false;
                    List<SPCommonSearchDataMore> tendDocument = creationService.getOpeningReportData("getTenderDocument", tenderid, lotId,null);//lotid
                    List<SPCommonSearchDataMore> TOCMembers = creationService.getOpeningReportData("getTOCMembers", tenderid, lotId,"TOR2"); //lotid
                    List<SPCommonSearchDataMore> TOCCP = creationService.getOpeningReportData("getOpeningCommitteeCPId", tenderid, "","");
                    String TOCCPUserID = "0";
                    if(TOCCP!=null || !TOCCP.isEmpty()){
                        TOCCPUserID = TOCCP.get(0).getFieldName1();
                    }
                    boolean list1 = true;
                    boolean list2 = true;
                    boolean list4 = true;
                    if (tendOpeningRpt.isEmpty()) {
                        list1 = false;
                    }
                    if (tendererList.isEmpty()) {
                        list2 = false;
                    } else {
                        if (tendererList.get(0).getFieldName1().equals("1")) {
                            isSecurityAvail = true;
                        }
                    }
                    if (tendDocument.isEmpty()) {
                        list4 = false;
                    }
                    boolean is2Env = false;
                    CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService)AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> envDataMores = commonSearchDataMoreService.geteGPData("GetTenderEnvCount", tenderid);
                    if(envDataMores!=null && (!envDataMores.isEmpty())){
                        if(envDataMores.get(0).getFieldName1().equals("2")){
                            is2Env = true;
                        }
                    }
        %>
        <div class="contentArea_1">
            <%if(bol_flag){%>
            <span style="float: right;" style="margin-top: 15px;">
                <%if(request.getParameter("goback")==null || request.getParameter("goback")==""){%>
                &nbsp;&nbsp;
                <a class="action-button-goback" href="<%if("eval".equals(request.getParameter("frm"))){out.print(request.getHeader("referer"));}else{out.print("OpenComm.jsp?tenderid="+tenderid);}%>">Go Back to Dashboard</a>
                <%}%>
            </span>
            <%}%>
            <%    if("y".equals(request.getParameter("isT"))){
                        out.print("<span style='float: right;' style='margin-top: 15px;'><a href='javascript:void(0);' id='print' class='action-button-view'>Print</a>&nbsp;&nbsp;<a class='action-button-savepdf' href='"+request.getContextPath()+"/TorRptServlet?tenderId="+tenderid+"&lotId="+lotId+"&action=TOR2'>Save As PDF</a></span>");
                    }
            %>
            <div  id="print_area">
            <div class="pageHead_1"><%=repLabel%> Opening Report 2
            </div>
            <%
                        pageContext.setAttribute("tenderId", request.getParameter("tenderid"));
                        pageContext.setAttribute("tab", "6");
                        pageContext.setAttribute("isPDF", request.getParameter("isPDF"));
                        pageContext.setAttribute("userId", 0);
                        if(request.getParameter("msg")!=null){out.print("<br/><div class='responseMsg successMsg'>TOR2 Signed Successfully</div>");}
            %>
            
            <div class="tableHead_1 t_space"><%if(repLabel.equals("Tender")){%>Tender Opening Report 2<%}else{%>PROPOSAL OPENING<%}%></div>
            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 b_space" width="100%">
                <tr>
                    <td width="24%" class="ff">Ministry Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName1());
                                }%></td>
                    <td width="16%" class="ff">Division Name :</td>
                    <td width="30%"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName2());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff">Organization/Agency Name :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName3());
                                }%></td>
                    <td class="ff">Procuring Entity :</td>
                    <td><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName4());
                                }%></td>
                </tr>
                <tr>
                    <td class="ff"><%=repLabel%> Package No. and Description :</td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName5()+" & "+tendOpeningRpt.get(0).getFieldName6());
                                }%></td>
                </tr>
                <%if(!lotId.equals("0")){%>
                <tr>
                    <td class="ff">Lot No. and Description : </td>
                    <td colspan="3"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName12());
                                    if(!tendOpeningRpt.get(0).getFieldName12().equals("")){
                                        out.print(" & ");
                                    }
                                    out.print(tendOpeningRpt.get(0).getFieldName13());
                                }%></td>
                </tr>
                <%}%>
            </table>            
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <%
                    int[] tdWidth = null;
                    int colspan=0;
                    if(isSecurityAvail){
                        colspan = 5;
                        tdWidth = new int[5];
                        tdWidth[0] = 20;
                        tdWidth[1] = 20;
                        tdWidth[2] = 20;
                        tdWidth[3] = 20;
                        tdWidth[4] = 20;
                    }else{
                        colspan = 3;
                        tdWidth = new int[3];
                        tdWidth[0] = 33;
                        tdWidth[1] = 33;
                        tdWidth[2] = 34;
                    }
                %>
                <tr>
                    <td class="tableHead_1" colspan="<%=colspan%>" ><%=repLabel%> Date and Time</td>
                </tr>
                <tr>
                    <th width="<%=tdWidth[0]%>%" >Date and Time of Publishing </th>
                    <th width="<%=tdWidth[1]%>%">Date and Time of Closing</th>
                    <th width="<%=tdWidth[2]%>%">Date and Time of Opening</th>
                    <%if(isSecurityAvail){%>
                    <th width="<%=tdWidth[3]%>%"><%=repLabel%> Validity Date</th>
                    <th width="<%=tdWidth[4]%>%"><%=repLabel%> Security Validity Date</th>
                    <%}%>
                </tr>
                <tr>
                    <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName10());
                                }%></td>
                    <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName11());
                                }%></td>
                    <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName16());
                                }%></td>
                    <%if(isSecurityAvail){%>
                    <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName17());
                                }%></td>
                    <td class="t-align-center"><%if (list1) {
                                    out.print(tendOpeningRpt.get(0).getFieldName18());
                                }%></td>
                    <%}%>
                </tr>
            </table>

            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="4" ><%=repLabel%> Document</td>
                </tr>
                <tr>
                    <th width="25%" >Documents Sold</th>
                    <th width="25%">Nos. of Submissions</th>
                    <th width="25%">Nos. Withdrawn</th>
                    <th width="25%">Nos. Substituted / Modified</th>
                </tr>
                <tr>
                    <td class="t-align-center"><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName3());
                                }%></td>
                    <td class="t-align-center"><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName1());
                                }%></td>
                    <td class="t-align-center"><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName2());
                                }%></td>
                    <td class="t-align-center"><%if (list4) {
                                    out.print(tendDocument.get(0).getFieldName4());
                                }%></td>
                </tr>
            </table>

            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <%
                        tdWidth = null;
                        if(isSecurityAvail){
                            tdWidth = new int[7];
                            tdWidth[0] = 15;
                            tdWidth[1] = 12;
                            tdWidth[2] = 13;
                            tdWidth[3] = 12;
                            tdWidth[4] = 18;
                            tdWidth[5] = 12;
                            tdWidth[6] = 10;
                        }else{
                            tdWidth = new int[2];
                            tdWidth[0] = 46;
                            tdWidth[1] = 46;
                        }
                    %>
                    <th width="8%"><!--%=repLabel%-->Sl. No.</th>
                    <th width="<%=tdWidth[0]%>%">Name of Bidder / Consultant</th>
                    <th width="<%=tdWidth[1]%>%">Date and Time of Submission</th>
                    <%if (isSecurityAvail) {%>
                    <th width="<%=tdWidth[2]%>%"><%=repLabel%> Security Type</th>
                    <th width="<%=tdWidth[3]%>%"><%=repLabel%> Security Amount</th>
                    <th width="<%=tdWidth[4]%>%">Name of Financial Institute & Branch,<br/>Date of Issue</th>
                    <th width="<%=tdWidth[5]%>%"><%=repLabel%> Security Valid upto (Date)</th>
                    <th width="<%=tdWidth[6]%>%"><%=repLabel%> Validity upto (Date)</th>
                    <%}%>
                </tr>
                <%
                        tdWidth=null;
                        int cnt_no = 1;
                        for (SPCommonSearchDataMore tendererData : tendererList) {%>
                <tr>
                    <td class="t-align-center"><%=cnt_no%></td>
                    <%
                        //14 userId,15 tendererId,16 companyId
                        String[] jvSubData = creationService.jvSubContractChk(tenderid, tendererData.getFieldName14());
                        StringBuilder name_link = new StringBuilder();
                        name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewRegistrationDetail.jsp?uId="+tendererData.getFieldName14()+"&tId="+tendererData.getFieldName15()+"&cId="+tendererData.getFieldName16()+"&top=no' target='_blank'>"+tendererData.getFieldName4()+"</a>");
                        if(jvSubData[0]!=null && jvSubData[0].equals("jvyes")){
                            name_link.delete(0, name_link.length());
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewJVCADetails.jsp?uId="+tendererData.getFieldName14()+"' target='_blank'>"+tendererData.getFieldName4()+"</a>");
                            name_link.append("<br/>");
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewJVCADetails.jsp?uId="+tendererData.getFieldName14()+"' target='_blank' style='color:red'>(JVCA - View Details)</a>");
                        }
                        if(jvSubData[1]!=null && jvSubData[1].equals("subyes")){
                            name_link.append("<br/>");
                            name_link.append("<a href='"+request.getContextPath()+"/tenderer/ViewTenderSubContractor.jsp?uId="+tendererData.getFieldName14()+"&tenderId="+tenderid+"&tId="+tendererData.getFieldName15()+"&cId="+tendererData.getFieldName16()+"' target='_blank' style='color:red'>(Sub Contractor/Consultant - View Details)</a>");
                        }
                    %>
                    <td><%if(bol_flag){out.print(name_link.toString());}else{out.print(tendererData.getFieldName4());}%></td>
                    <td class="t-align-center"><%=tendererData.getFieldName5()%></td>
                    <%if (isSecurityAvail) {%>
                    <td class="t-align-center"><%=tendererData.getFieldName7()%></td>
                    <td style="text-align: right;"><%=tendererData.getFieldName8()%></td>
                    <td><%out.print("<b>Bank:</b> "+tendererData.getFieldName9() + "<br/><b>Branch:</b> " + tendererData.getFieldName10() + "<br/>" + tendererData.getFieldName11());%></td>
                    <td class="t-align-center"><%=tendererData.getFieldName12()%></td>
                    <td class="t-align-center"><%=tendererData.getFieldName13()%></td>
                    <%}%>
                </tr>
                <%
                                cnt_no++;
                            }
                            if (!list2) {
                                out.print("<tr><td colspan='");
                                if (isSecurityAvail) {
                                    out.print("8");
                                } else {
                                    out.print("3");
                                }
                                out.print("' style='color:red;' class='t-align-center'>No Records Found</td></tr>");
                            }
                %>
            </table>
            <%
              if(!(eventType.equals("REOI") || eventType.equals("PQ") || eventType.equals("1 stage-TSTM")) && !is2Env){
                List<SPCommonSearchDataMore> getreportID = null;
                String repId = null;
                getreportID = commonSearchDataMoreService.geteGPData("isCRFormulaMadeTORTER", tenderid,lotId, "TOR");
                boolean isCRFormulaMadeTOR = false;
                if(getreportID!=null && (!getreportID.isEmpty()) && getreportID.get(0).getFieldName1().equals("1")){
                    isCRFormulaMadeTOR = true;
                    getreportID = commonSearchDataMoreService.geteGPData("getTORReportId", tenderid,lotId, null);
                    if(getreportID!=null && (!getreportID.isEmpty())){
                        repId = getreportID.get(0).getFieldName1();
                    }
                }
                if(isCRFormulaMadeTOR){
                    if(repId!=null){                       
            %>
            <jsp:include page="/officer/TenderRepInclude.jsp">
                <jsp:param name="tenderid"  value="<%=tenderid%>"/>
                <jsp:param name="repId"  value="<%=repId%>"/>
                <jsp:param name="isEval"  value="true"/>
                <jsp:param name="istos"  value="true"/>
                <jsp:param name="tendrepUserId"  value="<%=TOCCPUserID%>"/>
                <jsp:param name="isTOR"  value="y"/>
            </jsp:include>
            <%
                    }else{out.print("<br/><div class='responseMsg noticeMsg'>Please Prepare a Price Comparison Report first</div>");}
                }else{out.print("<br/><div class='responseMsg noticeMsg'>Report Formula not created.</div>");
            }}%>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <td class="tableHead_1" colspan="5" ><%=repLabel.charAt(0)%>OC Members</td>
                </tr>
                <tr>
                    <th width="20%" ><span id="signLabel">Committee Members</span></th>                    
                    <%
                                int cnt_tor2=1;
                                int svpdftor2 = 0;
                                for (SPCommonSearchDataMore dataMore : TOCMembers) {
                                    out.print("<td>" + dataMore.getFieldName1() + "</td>");
                                    cnt_tor2++;
                                }
                %>
                </tr>
                <tr>
                    <th width="20%">Committee Role</th>
                    <%for (SPCommonSearchDataMore dataMore : TOCMembers) {
                                    if (dataMore.getFieldName2().equals("cp")) {
                                        out.print("<td>Chairperson</td>");
                                    } else if (dataMore.getFieldName2().equals("ms")) {
                                        out.print("<td>Member Secretary</td>");
                                    } else if (dataMore.getFieldName2().equals("m")) {
                                        out.print("<td>Member</td>");
                                    }
                                }%>
                </tr>
                <tr>
                    <th width="20%">Designation</th>
                    <%for (SPCommonSearchDataMore dataMore : TOCMembers) {
                                    out.print("<td>" + dataMore.getFieldName3() + "</td>");
                                }%>
                </tr>
                <tr>
                    <th width="20%">PA Office</th>
                    <%for (SPCommonSearchDataMore dataMore : TOCMembers) {
                                    out.print("<td>" + dataMore.getFieldName4() + "</td>");
                                }%>
                </tr>
                <tr>
                    <th width="20%">Electronically Signed <%=repLabel.charAt(0)%>OR On</th>
                    <%for (SPCommonSearchDataMore dataMore : TOCMembers) {
                        if(dataMore.getFieldName6().equals("-")){
                            svpdftor2++;
                        }
                                    out.print("<td>" + dataMore.getFieldName6() + "</td>");
                                }%>
                </tr>
                <tr>
                    <th width="20%">Comments</th>
                    <%for(SPCommonSearchDataMore dataMore : TOCMembers) {out.print("<td>"+dataMore.getFieldName8()+"</td>");}%>
                </tr>
            </table>
                 <%if(svpdftor2!=0){%>
                    <script type="text/javascript">
                        $('#svpdftor2').hide();
                    </script>
                <%}%>
        </div>
        </div>
    </body>
    <%
                tendOpeningRpt = null;
                tendDocument = null;
                TOCMembers = null;
                tendererList = null;
    %>
    <script type="text/javascript">
    $(document).ready(function() {

        $("#print").click(function() {
            //alert('sa');
            printElem({ leaveOpen: true, printMode: 'popup' });
        });

    });
    function printElem(options){
        //alert(options);

        $('#print_area').printElement(options);
        //$('#trLast').hide();
    }

</script>
</html>
<%if(request.getParameter("goback")!=null && request.getParameter("goback")!=""){%>
<script type="text/javascript">
    var headSel_Obj = document.getElementById("headTabEval");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
</script>
<%}else{%>
<script type="text/javascript">
    var headSel_Obj = document.getElementById("headTabTender");
    if(headSel_Obj != null){
        headSel_Obj.setAttribute("class", "selected");
    }
</script>
<%}%>

