<%-- 
    Document   : JREInstallation
    Created on : Jun 9, 2011, 6:16:01 AM
    Author     : rikin.p
--%>


<%@page  import="com.cptu.egp.eps.web.utility.LogoutUtils"%>

<%
    HttpSession hs = request.getSession();
    if(hs!=null)
        {
            hs.removeAttribute("userId");
            Object objUserType = hs.getAttribute("userTypeId");
            if (objUserType != null && objUserType.equals("1")) {
                hs.removeAttribute("userType");
            }
            hs.removeAttribute("userTypeId");
            hs.removeAttribute("userName");
            LogoutUtils logoutUtils = new LogoutUtils();
            logoutUtils.setLogUserId("0");
            Object objSessionid = hs.getAttribute("sessionId");
            if (objSessionid != null) {
                logoutUtils.updateSessionMaster(Integer.parseInt(objSessionid.toString()));
                hs.removeAttribute("sessionId");
            }

            //hs.invalidate();
           
             String id="0";
             hs.setAttribute("userId",id);
    }
    else
        {   
            hs = request.getSession();
            String id="0";
            hs.setAttribute("userId",id);
        }
    %>
<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 Transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Java needs to be installed</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
       
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Header Table-->
                <!--Middle Content Table-->
                <div  class="contentArea_1" align="center">
                    <table border=0 cellSpacing=7 cellPadding=0
                           width="90%" class="instructionTable" >
                        <tr>
                            <td align="center">
                                <div><strong>   Electronic Government Procurement (e-GP) System of the Royal Government of Bhutan</strong></div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center"><a href="Index.jsp"><img src="<%=request.getContextPath()%>/resources/images/cptuEgpLogo.gif" alt="" width="160" height="71"></a>
                        </tr>
                        <tr>
                            <td align="center">
                                <div>   <strong> Government Procurement and Property Management Division (GPPMD)</strong></div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div>    Department of National Properties, Ministry of Finance, Royal Government of Bhutan</div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div style="color:#FF9326;font-size: medium;">    <strong>Java needs to be installed</strong></div>
                                <table width="100%" cellspacing="0" id="header" class="t_space">
                                    <tr><td width="50%" align="right">
                                            <a class="action-button-home" style="vertical-align: middle;" href="Index.jsp" >Go To e-GP Home Page</a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="atxt_1 c_t_space" vAlign=top>
                                <div class="atxt_1">
                                    Dear User,<br/><br/>
                                    You need to download <strong>Java Runtime Environment (JRE)</strong> in your computer. This is required to generate
                                    e-Signature, to encrypt and decrypt tender data (for security reason).
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="atxt_1 c_t_space" vAlign=top>
                                <div class="atxt_1">
                                    <strong style="height:15px; line-height:15px; color:#78A951; font-size: 12px; ">  <a href="javascript:void(0)" onclick="location.href='../../help/manuals/Java Installation without screenshot bangla.pdf'" class="" target="_blank">Download:System Configuration Document </a></strong>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="atxt_1 c_t_space" vAlign=top>
                                <div class="atxt_1">
                                    Please <a href="javascript:void(0)" onclick="location.href='../../help/java/jre-6u21-windows-i586.zip'" class="">click here</a>  to download this software.
                                  
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="atxt_1 c_t_space" vAlign=top>
                                <div class="atxt_1">
                                    You also need to have below mentioned JRE settings in your computer:
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="atxt_1 c_t_space" vAlign=top style="padding-left: 20px;">
                                <ol>
                                    <li>Go to Control Panel</li>
                                    <li>Double click on Java menu to open Java Control Panel</li>
                                    <li>Click on 'Advanced' tab</li>
                                    <li>Click on 'Default Java for Browsers' and then tick below mentioned options:
                                        <ol type="a" style="padding-left: 15px;">
                                            <li>Microsoft Internet Explorer</li>
                                            <li>Mozilla family</li>
                                        </ol>
                                    </li>
                                </ol>
                                 </div>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="color:#FF9326;font-size: medium;"><br/><strong>Thank You for your support.</strong><br/><br/></td></tr>
                        <tr><td align="center"><img alt="" src="<%=request.getContextPath()%>/resources/images/cptuLogo.jpg" width="435" height="37"></td>
                        </tr>
                        <tr><td align="center">Box 116, Thimphu, Bhutan<br/><br/>
                                <b>Phone</b>: +975-02-336962 |   <b>Fax</b>: +975-02-336961 | <b>Email</b>: <a href="mailto:info.bhutan@dohatec.com.bd" >info.bhutan@dohatec.com.bd</a>  | <b>Web</b>: <a href="http://www.pppd.gov.bt/" >www.pppd.gov.bt</a>
                            </td></tr>
                        <tr>
                            <td align="center" style="color:#FF9326;font-size: medium;"><br/><strong>e-GP Support Contact Details </strong><br/></td></tr>
                        <tr>
                            <td class="atxt_1 c_t_space" vAlign=top>
                                <div class="atxt_1" style="text-align: center;">
                                    <b>Phone</b>: +880-2-9144 252/53 | <b>Email</b>: <a href="mailto:helpdesk@eprocure.gov.bd" >helpdesk@eprocure.gov.bd</a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
