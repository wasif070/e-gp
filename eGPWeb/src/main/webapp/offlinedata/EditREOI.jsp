<%-- 
    Document   : EditREOI
    Created on : 06-Aug-2012, 15:48:06
    Author     : Ahsan
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.TenderDashboardOfflineSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotPhasingOffline"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetailsOffline"%>
<%@page import="com.cptu.egp.eps.web.utility.CommonUtils"%>
<%@page import="com.cptu.egp.eps.web.offlinedata.EOIExcellBean"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblStateMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.model.table.TblDepartmentMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ManageEmployeeGridSrBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<jsp:useBean id="offlineDataSrBean" class="com.cptu.egp.eps.web.offlinedata.OfflineDataSrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Request for Expression Of Interest (REOI)</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
         <script type="text/javascript" src="../ckeditor/config.js"></script>
        <link href="../ckeditor/editor.css" type="text/css" rel="stylesheet">

        <!--jquery validator max length css change - Dohatec-->
       <style type="text/css">
           label.error{color:red}
       </style>

        <script type="text/javascript">

            $(document).ready(function(){

                // Sorting Dropdown
                $("select").each(function() {

                    // Keep track of the selected option.
                    var selectedValue = $(this).val();

                    // Sort all the options by text. I could easily sort these by val.
                    $(this).html($("option", $(this)).sort(function(a, b) {
                        return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
                    }));

                    // Select one option.
                    $(this).val(selectedValue);
                });

                $('#spanassoForiegnFirm').hide();
                
                $('#reoiSelection').change(function(){
                    if($('#reoiSelection').val() == 'Individual Consultant')
                    {
                        $('#assoForiegnFirm').hide();
                        $('#spanassoForiegnFirm').show();
                    }
                    else
                    {
                        $('#spanassoForiegnFirm').hide();
                        $('#assoForiegnFirm').show();
                    }
                });

                //Form Validation
                $("#frmEditREOI").validate({
                    rules: {
                        //textbox
                        peName: {required: true, maxlength: 150},
                        invitationRefNo: {required: true, maxlength: 50},
                        issuedate: {required: true, date:true},
                        txteoiCloseDate: {required: true, date:true},
                        preQualDocPrice: {required: true, number: true},
                        briefDescAssignment:{required: true},
                        expRequired:{required: true},
                        nameOfficial:{required: true, maxlength: 200},
                        designation:{required: true, maxlength: 200},
                        contractddress:{required: true, maxlength: 5000},
                        
                        refNo_0:{maxlength: 150},
                        phasingService_0:{required: true, maxlength: 2000},
                        locationRefNo_0:{maxlength: 100},
                        indicativeStartDate_0:{maxlength: 100},
                        indicativeComplDate_0:{maxlength: 100},

                        refNo_1:{maxlength: 150},
                        phasingService_1:{maxlength: 2000},
                        locationRefNo_1:{maxlength: 100},
                        indicativeStartDate_1:{maxlength: 100},
                        indicativeComplDate_1:{maxlength: 100},

                        refNo_2:{maxlength: 150},
                        phasingService_2:{maxlength: 2000},
                        locationRefNo_2:{maxlength: 100},
                        indicativeStartDate_2:{maxlength: 100},
                        indicativeComplDate_2:{maxlength: 100},

                        refNo_3:{maxlength: 150},
                        phasingService_3:{maxlength: 2000},
                        locationRefNo_3:{maxlength: 100},
                        indicativeStartDate_3:{maxlength: 100},
                        indicativeComplDate_3:{maxlength: 100},

                        refNo_4:{maxlength: 150},
                        phasingService_4:{maxlength: 2000},
                        locationRefNo_4:{maxlength: 100},
                        indicativeStartDate_4:{maxlength: 100},
                        indicativeComplDate_4:{maxlength: 100},

                        refNo_5:{maxlength: 150},
                        phasingService_5:{maxlength: 2000},
                        locationRefNo_5:{maxlength: 100},
                        indicativeStartDate_5:{maxlength: 100},
                        indicativeComplDate_5:{maxlength: 100},

                        refNo_6:{maxlength: 150},
                        phasingService_6:{maxlength: 2000},
                        locationRefNo_6:{maxlength: 100},
                        indicativeStartDate_6:{maxlength: 100},
                        indicativeComplDate_6:{maxlength: 100},

                        refNo_7:{maxlength: 150},
                        phasingService_7:{maxlength: 2000},
                        locationRefNo_7:{maxlength: 100},
                        indicativeStartDate_7:{maxlength: 100},
                        indicativeComplDate_7:{maxlength: 100},

                        refNo_8:{maxlength: 150},
                        phasingService_8:{maxlength: 2000},
                        locationRefNo_9:{maxlength: 100},
                        indicativeStartDate_8:{maxlength: 100},
                        indicativeComplDate_8:{maxlength: 100},

                        refNo_9:{maxlength: 150},
                        phasingService_9:{maxlength: 2000},
                        locationRefNo_9:{maxlength: 100},
                        indicativeStartDate_9:{maxlength: 100},
                        indicativeComplDate_9:{maxlength: 100},

                        devPartners:{maxlength: 300},
                        peCode:{maxlength: 15},
                        projectCode:{maxlength: 150},
                        projectName:{maxlength: 150},
                        contactDetails:{maxlength: 1000},

                        //Dropdown
                        Ministry:{selectNone: true},
                        organization: {selectNone: true},
                        district:{selectNone: true},
                        contractType:{selectNone: true},
                        procSubMethod:{selectNone: true},
                        reoiSelection:{selectNone: true},
                        assoForiegnFirm:{selectNone: true},
                        budgetType:{selectNone: true},
                        sourceFunds:{selectNone: true},

                        //Phasing Dates
                        indicativeStartDate_0:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeComplDate_0:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeStartDate_1:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeComplDate_1:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeStartDate_2:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeComplDate_2:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeStartDate_3:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeComplDate_3:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeStartDate_4:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeComplDate_4:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeStartDate_5:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeComplDate_5:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeStartDate_6:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeComplDate_6:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeStartDate_7:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeComplDate_7:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeStartDate_8:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeComplDate_8:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeStartDate_9:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/},
                        indicativeComplDate_9:{regex: /^(0[1-9]|1[012])[/](19|20)\d\d$/}

                    },
                    messages: {
                        //Textbox
                        peName: { required: "<div class='reqF_1'>Please enter Procuring Entity Name.</div>"},
                        invitationRefNo: { required: "<div class='reqF_1'>Please enter REOI No.</div>"},
                        issuedate: { required: "<div class='reqF_1'>Please select Date.</div>", date: "<div class='reqF_1'>Invalid Date Format</div>"},
                        txteoiCloseDate: { required: "<div class='reqF_1'>Please select Closing Date.</div>", date: "<div class='reqF_1'>Invalid Date Format</div>"},
                        preQualDocPrice: {required: "<div class='reqF_1'>Please enter doc price.</div>", number: "<div class='reqF_1'>Please enter nemuric only.</div>"},
                        briefDescAssignment:{ required: "<div class='reqF_1'>Please enter Brief Description of Assignment.</div>"},
                        expRequired:{ required: "<div class='reqF_1'>Please enter Description of Experience, Resources and Delivery Capacity.</div>"},
                        nameOfficial :{ required: "<div class='reqF_1'>Please enter Name.</div>"},
                        designation:{ required: "<div class='reqF_1'>Please enter Designation.</div>"},
                        contractddress:{required: "<div class='reqF_1'>Please enter Address.</div>"},
                        phasingService_0:{required: "<div class='reqF_1'>Please enter Phasing of Service.</div>"},
                        

                        //Dropdown
                        Ministry:{ selectNone: "<div class='reqF_1'>Please select Ministry.</div>"},
                        organization:{ selectNone: "<div class='reqF_1'>Please select Organization.</div>"},
                        district:{ selectNone: "<div class='reqF_1'>Please select Dzongkhag / District.</div>"},
                        contractType:{ selectNone: "<div class='reqF_1'>Please select Contract Type.</div>"},
                        procSubMethod:{ selectNone: "<div class='reqF_1'>Please select Procurement Sub-Method.</div>"},
                        reoiSelection:{ selectNone: "<div class='reqF_1'>Please select Request for Expression of Interest.</div>"},
                        assoForiegnFirm:{ selectNone: "<div class='reqF_1'>Please select Association with Foreign  Firm.</div>"},
                        budgetType:{ selectNone: "<div class='reqF_1'>Please select Budget Type.</div>"},
                        sourceFunds:{ selectNone: "<div class='reqF_1'>Please select Source of Fund.</div>"}

                    }
                });

                ////The following code has been used to adding validation of dropdown list in basic validation plugin
                $.validator.addMethod('selectNone', function (value, element){
                        if ($(element).is(":hidden")){ 
                            return true;
                        }
                        else{
                            /*
                             *  Here if the value of selected item of dropdown is '0' or '' or 'select' method will return false
                             *  If there is no value like '<option>Item</option>' it will check 'value.indexOf('--')'. Because
                             *  Items are coming from Database and 1st item (default selected item) is '-- Please Select *** --' or '-- Select *** --'
                             *  and other item does not contains '--'.
                             *  That's why 'value.indexOf('--')' is used for checking condition
                             */
                            if (value == 0 || value =='' || value =='select' || value =='<Select>' || value =='<select>' || value.indexOf('--') != -1)
                            {
                                //alert("not ok");
                                return false;
                            }
                            else
                            {
                                //alert("ok")
                                return true;
                            }
                        }
                    }
                );  //End Form Validation

                ////The following code has been added by Salahuddin on October 02 to validate the phasing of services date fields
                $.validator.addMethod(
                    "regex",
                    function(value, element, regexp) {
                        var check = false;
                        return this.optional(element) || regexp.test(value);
                    },
                    "Invalid Format"
                 );//End Form Validation

            }); //  End Document.Ready



             function setOrgval(id){

                var orgObj =document.getElementById(id);
                var orgval = orgObj.options[orgObj.selectedIndex].text;
               // alert('orgvalue >> '+orgval);
                document.getElementById("hidorg").value = orgval;
            }

        </script>

    </head>
     <%
            System.out.println("submit value  >> "+request.getParameter("submit"));
            EOIExcellBean eOIExcellBean = null;
            int tenolId = 0;
            String action = "create";
            String agencyCombo = "";
            String type = "";
            if(!request.getParameterMap().containsKey("Edit"))
               type="/offlinedata/CreateREOI.jsp";
        if( request.getParameter("submit") != null && "Submit".equals(request.getParameter("submit"))){
            try{
                System.out.println("in condition ");


                String ministry = CommonUtils.checkNull(request.getParameter("hidministry"));
                String agency = CommonUtils.checkNull(request.getParameter("hidorg"));
                String peName = CommonUtils.checkNull(request.getParameter("peName"));
                String peCode = CommonUtils.checkNull(request.getParameter("peCode"));
                String district = CommonUtils.checkNull(request.getParameter("district"));
                String contractType = CommonUtils.checkNull(request.getParameter("contractType"));
                String invitationRefNo = CommonUtils.checkNull(request.getParameter("invitationRefNo"));
                String reoiSelection = CommonUtils.checkNull(request.getParameter("reoiSelection"));
                String issuedate  = CommonUtils.checkNull(request.getParameter("issuedate"));
                String procSubMethod = CommonUtils.checkNull(request.getParameter("procSubMethod"));
                String budgetType = CommonUtils.checkNull(request.getParameter("budgetType"));
                String sourceFunds = CommonUtils.checkNull(request.getParameter("sourceFunds"));
                String devPartners = CommonUtils.checkNull(request.getParameter("devPartners"));
                String projectCode = CommonUtils.checkNull(request.getParameter("projectCode"));
                String projectName = CommonUtils.checkNull(request.getParameter("projectName"));
                String eoiCloseDate = CommonUtils.checkNull(request.getParameter("txteoiCloseDate"));
                String briefDescAssignment = CommonUtils.checkNull(request.getParameter("briefDescAssignment"));
                String expRequired = CommonUtils.checkNull(request.getParameter("expRequired"));
                String otherDetails = CommonUtils.checkNull(request.getParameter("otherDetails"));
                String assoForiegnFirm = CommonUtils.checkNull(request.getParameter("assoForiegnFirm"));
                String nameOfficial = CommonUtils.checkNull(request.getParameter("nameOfficial"));
                String designation = CommonUtils.checkNull(request.getParameter("designation"));
                String contractddress = CommonUtils.checkNull(request.getParameter("contractddress"));
                String contactDetails = CommonUtils.checkNull(request.getParameter("contactDetails"));
              
                TblTenderDetailsOffline tenderDetailsOffline = null;
                List<TblTenderLotPhasingOffline> lots = null ;
                if( request.getParameter("action") != null && request.getParameter("tenderOLId") != null && "Edit".equals(request.getParameter("action"))
                        &&  Integer.parseInt(request.getParameter("tenderOLId")) != 0){
                    
                        tenolId = Integer.parseInt(request.getParameter("tenderOLId"));
                        tenderDetailsOffline = offlineDataSrBean.getTblTenderDetailOfflineDataById(tenolId);

                        int i = 0;
                        if(tenderDetailsOffline.getTenderLotsAndPhases() != null && tenderDetailsOffline.getTenderLotsAndPhases().size() > 0){
                            while(i < tenderDetailsOffline.getTenderLotsAndPhases().size()){
                                tenderDetailsOffline.getTenderLotsAndPhases().remove(i);
                            }
                        }
                        lots = tenderDetailsOffline.getTenderLotsAndPhases();

                 }else{
                        tenderDetailsOffline = new TblTenderDetailsOffline();
                        lots  = new ArrayList<TblTenderLotPhasingOffline>();
                    }
                    for(int i=0;i<10;i++){

                        if((request.getParameter("refNo_"+i) != null || request.getParameter("phasingService_"+i) != null
                                || request.getParameter("locationRefNo_"+i) != null || request.getParameter("indicativeStartDate_"+i) != null
                                || request.getParameter("indicativeComplDate_"+i) != null ) &&
                                (!"".equals(request.getParameter("refNo_"+i)) || !"".equals(request.getParameter("phasingService_"+i))
                                || !"".equals(request.getParameter("locationRefNo_"+i)) || !"".equals(request.getParameter("indicativeStartDate_"+i))
                                ||!"".equals(request.getParameter("indicativeComplDate_"+i)) ))
                                {
                            
                                TblTenderLotPhasingOffline lotPhasingOffline = new TblTenderLotPhasingOffline();
                                lotPhasingOffline.setLotOrRefNo(CommonUtils.checkNull(request.getParameter("refNo_"+i)));
                                lotPhasingOffline.setLotIdentOrPhasingServ(CommonUtils.checkNull(request.getParameter("phasingService_"+i)));
                                lotPhasingOffline.setLocation(CommonUtils.checkNull(request.getParameter("locationRefNo_"+i)));
                                lotPhasingOffline.setStartDateTime(CommonUtils.checkNull(request.getParameter("indicativeStartDate_"+i)));
                                lotPhasingOffline.setCompletionDateTime(CommonUtils.checkNull(request.getParameter("indicativeComplDate_"+i)));
                                lotPhasingOffline.setTenderDetailsOffline(tenderDetailsOffline);
                                lotPhasingOffline.setTenderSecurityAmt(new BigDecimal(0));
                                lots.add(lotPhasingOffline);
                        }
                       
                    }
                     


                String userid = "";
                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") != null) {
                    userid = hs.getAttribute("userId").toString();
                    offlineDataSrBean.setLogUserId(userid);
                }
               // tenderDetailsOffline = new TblTenderDetailsOffline();

                tenderDetailsOffline.setMinistryOrDivision(ministry);
                tenderDetailsOffline.setAgency(agency);
                tenderDetailsOffline.setPeName(peName);
                tenderDetailsOffline.setPeCode(peCode);
                tenderDetailsOffline.setPeDistrict(district);
                // contract type lum, time based missed in database table column
                tenderDetailsOffline.setEventType("REOI");
                tenderDetailsOffline.setReoiRfpRefNo(invitationRefNo);
                tenderDetailsOffline.setReoiRfpFor(reoiSelection);
                tenderDetailsOffline.setProcurementMethod(procSubMethod);
                tenderDetailsOffline.setBudgetType(budgetType);
                tenderDetailsOffline.setSourceOfFund(sourceFunds);
                tenderDetailsOffline.setDevPartners(devPartners);
                tenderDetailsOffline.setProjectCode(projectCode);
                tenderDetailsOffline.setProjectName(projectName);

                if(!"".equals(issuedate)){
                    Date issDate = DateUtils.formatStdString(issuedate);
                    tenderDetailsOffline.setIssueDate(issDate);
                }
                if(!"".equals(eoiCloseDate)){
                    Date eoiclsDate = DateUtils.convertDateToStr(eoiCloseDate);
                    tenderDetailsOffline.setClosingDate(eoiclsDate);
                }
                tenderDetailsOffline.setBriefDescription(briefDescAssignment);
                tenderDetailsOffline.setRelServicesOrDeliverables(expRequired);
                tenderDetailsOffline.setOtherDetails(otherDetails);
                tenderDetailsOffline.setForeignFirm(assoForiegnFirm);
                tenderDetailsOffline.setPeOfficeName(nameOfficial);
                tenderDetailsOffline.setPeDesignation(designation);
                tenderDetailsOffline.setPeAddress(contractddress);
                tenderDetailsOffline.setPeContactDetails(contactDetails);

                tenderDetailsOffline.setTenderLotsAndPhases(lots);
                tenderDetailsOffline.setProcurementNature("Service");
                tenderDetailsOffline.setProcurementType(" ");
                tenderDetailsOffline.setPackageNo(" ");
                tenderDetailsOffline.setInvitationFor(contractType);
                System.out.println("contract type::::::::::::::::::::::::::::::::::: "+contractType);
                //tenderDetailsOffline.setTenderStatus("Pending");

                System.out.println("before create ");

             if( request.getParameter("action") != null && request.getParameter("tenderOLId") != null && "Edit".equals(request.getParameter("action"))
                    &&  Integer.parseInt(request.getParameter("tenderOLId")) != 0){
               // type="/offlinedata/EditREOI.jsp?action=Edit&tenderOLId="+tenolId;
                System.out.println("tenolId >>>>>>>>>>> "+tenolId);
                tenderDetailsOffline.setTenderOfflineId(tenolId);
                offlineDataSrBean.updateTenderDetailsOfflineData(tenderDetailsOffline,tenolId);

                %>
                    <script type="text/javascript">
                      alert("Information Updated Successfully");
                    </script>
                <%
               response.sendRedirect(request.getContextPath() + "/offlinedata/TenderDashboardOfflineApproval.jsp");
              }
              else
              {
                offlineDataSrBean.createOfflineData(tenderDetailsOffline);
                %>
                    <script type="text/javascript">
                      alert("Information Saved Successfully");
                    </script>
                <%
              }

            }catch(Exception ex){
                ex.printStackTrace();
            }
            }
            //else
            {

                if(request.getParameter("action") != null && "Edit".equals(request.getParameter("action"))){
                    if(request.getParameter("tenderOLId") != null ){
                     String tenderOLId = request.getParameter("tenderOLId");
                     tenolId = Integer.parseInt(tenderOLId);
                     type="/offlinedata/EditREOI.jsp?action=Edit&tenderOLId="+tenolId;
                     }
                    action = "Edit";
                   
                    eOIExcellBean = (EOIExcellBean) offlineDataSrBean.editEOIForm(tenolId);
                    System.out.println("eOIExcellBean in jsp >> "+eOIExcellBean);
                    System.out.println("based > "+eOIExcellBean.getInterestForSelectionOfBased());
                    agencyCombo = offlineDataSrBean.getOrganizationByMinistry(eOIExcellBean.getMinistryName(), eOIExcellBean.getAgency());
                 }else{
                    eOIExcellBean = (EOIExcellBean)request.getAttribute("EOIDataBean");
                    agencyCombo = offlineDataSrBean.getOrganizationByMinistry(eOIExcellBean.getMinistryName(), eOIExcellBean.getAgency());
                 }

        %>
    
    <body>

        <div class="mainDiv">
            <div class="fixDiv">
                 <%@include file="../resources/common/AfterLoginTop.jsp" %>
                 <!--<table width="100%" border="0" cellspacing="0" cellpadding="0">-->
                    <!--<tr valign="top">-->
                        <div class="contentArea_1">
                            <form id="frmEditREOI" name="frmEditREOI" method="POST" action="<%=type %>">
                              <div class="t-align-right" style="vertical-align: middle;"><a class="action-button-goback" href="<%=request.getContextPath()%>/offlinedata/TenderDashboardOfflineApproval.jsp">Go back</a></div>
                              <div class="pageHead_1">Edit REOI for Services</div>
                             
                              <div class="tableHead_22 t_space">PROCURING ENTITY (PE) INFORMATION</div>
                                <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                                    <tbody>
                                    <tr>
                                        <td style="font-style: italic" colspan="4" class="ff t-align-left" align="left">
                                            Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="25%">Ministry/Division : <span>*</span></td>
                                        <%
                                          //  ManageEmployeeGridSrBean manageEmployeeGridSrBean = new ManageEmployeeGridSrBean();
                                          //  List<TblDepartmentMaster> departmentMasterList = null;
                                          //  departmentMasterList = manageEmployeeGridSrBean.getDepartmentMasterList("Ministry");

                                TenderDashboardOfflineSrBean awardedContractOffline = new TenderDashboardOfflineSrBean();
                                List<Object[]> ministryListOffline = new ArrayList<Object[]>();
                                List<Object[]> orgListOffline = new ArrayList<Object[]>();
                                ministryListOffline = awardedContractOffline.getMinistryForTenderOffline();
                                orgListOffline = awardedContractOffline.getOfflineTenderOrganizationByMinistry(eOIExcellBean.getMinistryName());

                                        %>
                                        <td width="25%">
                                            <select name="Ministry" class="formTxtBox_1" id="Ministry" style="width: 200px;" onchange="loadOrganization();">
                                                <option value="select" selected="selected">--- Please Select ---</option>
                                                <%
                                                /*boolean flag = false;
                                                    for (int i = 0; i < departmentMasterList.size(); i++)  {
                                                         if(eOIExcellBean.getMinistryName().equals(CommonUtils.checkNull(departmentMasterList.get(i).getDepartmentName()))){
                                                           out.println("<option selected=\"selected\" value='" + departmentMasterList.get(i).getDepartmentId() + "'>" + departmentMasterList.get(i).getDepartmentName() + "</option>");
                                                           flag = true;
                                                      }else
                                                           out.println("<option value='" + departmentMasterList.get(i).getDepartmentId() + "'>" + departmentMasterList.get(i).getDepartmentName() + "</option>");
                                                    }
                                                 if(flag == false)
                                                          out.println("<option selected=\"selected\" value='" + eOIExcellBean.getMinistryName() + "'>" + eOIExcellBean.getMinistryName() + "</option>");
*/
                                                 boolean flag = false;
                                                    for (int i = 0; i < ministryListOffline.size(); i++)  {
                                                           if(CommonUtils.checkNull(eOIExcellBean.getMinistryName()).equals(String.valueOf(ministryListOffline.get(i)))){
                                                                  out.println("<option selected=\"selected\" value='" + String.valueOf(ministryListOffline.get(i)) + "'>" + String.valueOf(ministryListOffline.get(i)) + "</option>");
                                                                  flag = true;
                                                              }else
                                                                 out.println("<option value='" +String.valueOf(ministryListOffline.get(i)) + "'>" + String.valueOf(ministryListOffline.get(i)) + "</option>");
                                                    }
                                                        if(flag == false && eOIExcellBean.getMinistryName() != null)
                                                                  out.println("<option selected=\"selected\" value='" + eOIExcellBean.getMinistryName() + "'>" + eOIExcellBean.getMinistryName() + "</option>");

                                                %>
                                            </select>
                                            <input id="hidministry"  name="hidministry" type="hidden" value="<%=eOIExcellBean.getMinistryName()%>" />
                                        </td>

                                        <td class="ff" width="25%">Organization : <span>*</span></td>
                                        <td>
                                            <select name="organization" class="formTxtBox_1" id="cmbOrganization" onchange="setOrgval(this.id);" style="width: 200px;">
                                               
                                                 <%
                                                   /* if(eOIExcellBean.getAgency() != null && !"".equals(eOIExcellBean.getAgency()) && agencyCombo.equals(eOIExcellBean.getAgency())){
                                                         out.println(CommonUtils.checkNull(agencyCombo));
                                                    }else{
                                                     out.println("<option selected=\"selected\"  value='" + eOIExcellBean.getAgency() + "'>"+ eOIExcellBean.getAgency() +"</option>");
                                                }*/
                                                  boolean flagOrg = false;
                                                    for (int i = 0; i < orgListOffline.size(); i++)  {
                                                           if(CommonUtils.checkNull(eOIExcellBean.getAgency()).equals(String.valueOf(orgListOffline.get(i)))){
                                                                  out.println("<option selected=\"selected\" value='" + String.valueOf(orgListOffline.get(i)) + "'>" + String.valueOf(orgListOffline.get(i)) + "</option>");
                                                                  flagOrg = true;
                                                              }else
                                                                 out.println("<option value='" +String.valueOf(orgListOffline.get(i)) + "'>" + String.valueOf(orgListOffline.get(i)) + "</option>");
                                                    }
                                                        if(flagOrg == false && eOIExcellBean.getAgency() != null)
                                                                  out.println("<option selected=\"selected\" value='" + eOIExcellBean.getAgency() + "'>" + eOIExcellBean.getAgency() + "</option>");

                                                %>  
                                             </select>
                                             <input type="hidden" name="hidorg"  id="hidorg" value="<%= CommonUtils.checkNull(eOIExcellBean.getAgency()) %>" />
                                        </td>
                                            
                                    </tr>
                                    <tr>
                                        
                                        <td class="ff">Procuring Entity Name : <span>*</span></td>
                                        <td>
                                            <input name="peName" class="formTxtBox_1" id="txtPEName" style="width: 280px;" type="text"  value="<%= CommonUtils.checkNull(eOIExcellBean.getProcuringEntityName()) %>" />
                                        </td>

                                        <td class="ff">Procuring Entity Code :</td>
                                        <td>
                                            <%
                                            if(eOIExcellBean.getProcuringEntityCode() != null && !eOIExcellBean.getProcuringEntityCode().equals("Not used at present")){
                                            %>
                                            <input name="peCode" class="formTxtBox_1" id="txtPECode" style="width: 280px;" type="text" value="<%=CommonUtils.checkNull(eOIExcellBean.getProcuringEntityCode()) %>" />
                                            <% }else{ %>
                                            <input name="peCode" class="formTxtBox_1" id="txtPECode" style="width: 280px;" type="text" value="" />
                                            <% } %>
                                        </td>
                                    </tr>
                                    <tr>                                        
                                        <td class="ff">Procuring Entity Dzongkhag / District : <span>*</span></td>
                                        <%
                                            CommonService cservice = (CommonService) AppContext.getSpringBean("CommonService");
                                            //Code by Proshanto
                                            short countryId = 150;//136
                                            List<TblStateMaster> liststate = cservice.getState(countryId);
                                        %>
                                        <td>
                                            <select name="district" class="formTxtBox_1" id="district" style="width: 200px;">
                                                <option value="select" selected="selected">--- Please Select ---</option>
                                                 <%

                                                        for (TblStateMaster state : liststate) {
                                                             if(state.getStateName().equals(CommonUtils.checkNull(eOIExcellBean.getProcuringEntityDistrict()))){
                                                                 out.println("<option selected=\"selected\" value='" + state.getStateName() + "'>" + state.getStateName() + "</option>");
                                                             }else
                                                                 out.println("<option value='" + state.getStateName() + "'>" + state.getStateName() + "</option>");
                                                            }
                                               %>
                                             </select>
                                        </td>
                                        
                                        <td class="ff">Contract Type : <span>*</span></td>
                                        <td>
                                            <select name="contractType" class="formTxtBox_1" id="contractType" style="width: 200px;">
                                                 <%
                                               if(CommonUtils.checkNull(eOIExcellBean.getInterestForSelectionOfBased()).equals("Time - based")){
                                                    out.println("<option selected=\"selected\" value=\"Time - based\">Time - based</option>");
                                                    out.println("<option value=\"Lump – sum\">Lump – sum</option>");
                                               }else{
                                                    out.println("<option  value=\"Time - based\">Time - based</option>");
                                                    out.println("<option selected=\"selected\" value=\"Lump – sum\">Lump – sum</option>");
                                               }

                                                %>
                                               <!--  <option value="select" selected="selected">--- Please Select ---</option> -->
                                              
                                            </select>
                                            <span id="spancontractType" style="color: red;"></span>
                                        </td>
                                    </tr>
                                    <tr>  
                                        <td class="ff">Event Type :</td>
                                        <td><label>REOI</label></td>

                                        <td class="ff">REOI No. : <span>*</span></td>
                                        <td colspan="3"><input name="invitationRefNo" value="<%=CommonUtils.checkNull(eOIExcellBean.getEoiRefNo()) %>" class="formTxtBox_1" id="txtinvitationRefNo" style="width: 280px;" type="text">
                                            <span class="reqF_1" id="spantxtinvitationRefNo"></span>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Request for Expression of Interest for <br>Selection of : <span>*</span> </td>
                                        <td><select name="reoiSelection" class="formTxtBox_1" id="reoiSelection" style="width: 200px;">
                                               <!--  <option value="select" selected="selected">--- Please Select ---</option> -->
                                                <%
                                                 if(CommonUtils.checkNull(eOIExcellBean.getInterestForSelectionOfConsult()).equals("Consulting Firm")){
                                                    out.println("<option selected=\"selected\" value=\"Consulting Firm\">Consulting Firm</option>");
                                                    out.println("<option value=\"Individual Consultant\">Individual Consultant</option>");
                                               }else{
                                                    out.println("<option  value=\"Consulting Firm\">Consulting Firm</option>");
                                                    out.println("<option selected=\"selected\" value=\"Individual Consultant\">Individual Consultant</option>");
                                               }

                                                %>
                                               
                                            </select>
                                        </td>

                                        <td class="ff">Date : <span>*</span></td>
                                        <td class="formStyle_1"><input name="issuedate" value="<%= CommonUtils.checkNull(eOIExcellBean.getEoiDate())  %>" class="formTxtBox_1" id="txtissuedate" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtissuedate','txtissuedate');"  type="text">
                                            <img id="imgtxtissuedate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtissuedate','imgtxtissuedate');" border="0">
                                            <span id="span1"></span>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                                </table>
                                <div class="tableHead_22 ">Key Information and Funding Information :</div>
                                <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                                    <tbody>
                                    <tr>
                                        <td class="ff" width="25%">Procurement Sub-Method : <span>*</span> </td>
                                        <td width="25%">
                                            <select name="procSubMethod" class="formTxtBox_1" id="procSubMethod" style="width: 200px;">
                                               <!--  <option value="select" selected="selected">--- Please Select ---</option> -->
                                                <%

                                        if(CommonUtils.checkNull(eOIExcellBean.getProcurementSubmethod()).contains("QCBS")){
                                       
                                            out.println("<option selected=\"selected\" value=\"Quality Cost Based Selection(QCBS)\">Quality Cost Based Selection(QCBS)</option>");
                                            out.println("<option value=\"Design Contest(DC)\">Design Contest(DC)</option>");
                                            out.println("<option value=\"Individual Consultant(IC)\">Individual Consultant(IC)</option>");
                                            out.println("<option value=\"Selection under a Fixed Budget(SFB)\">Selection under a Fixed Budget(SFB)</option>");
                                            out.println("<option value=\"Least Cost Selection(LCS)\">Least Cost Selection(LCS)</option>");
                                            out.println("<option value=\"Selection Community Service Organisation(SCSO)\">Selection Community Service Organisation(SCSO)</option>");
                                            out.println("<option value=\"Single Source Selection(SSS)\">Single Source Selection(SSS)</option>");
                                            out.println("<option value=\"Selection of Individual Consultant(SIS)\">Selection of Individual Consultant(SIS)</option>");
                                       }
                                       else if(CommonUtils.checkNull(eOIExcellBean.getProcurementSubmethod()).contains("SFB")){
                                      
                                            out.println("<option value=\"Quality Cost Based Selection(QCBS)\">Quality Cost Based Selection(QCBS)</option>");
                                            out.println("<option value=\"Design Contest(DC)\">Design Contest(DC)</option>");
                                            out.println("<option value=\"Individual Consultant(IC)\">Individual Consultant(IC)</option>");
                                            out.println("<option selected=\"selected\" value=\"Selection under a Fixed Budget(SFB)\">Selection under a Fixed Budget(SFB)</option>");
                                            out.println("<option value=\"Least Cost Selection(LCS)\">Least Cost Selection(LCS)</option>");
                                            out.println("<option value=\"Selection Community Service Organisation(SCSO)\">Selection Community Service Organisation(SCSO)</option>");
                                            out.println("<option value=\"Single Source Selection(SSS)\">Single Source Selection(SSS)</option>");
                                            out.println("<option value=\"Selection of Individual Consultant(SIS)\">Selection of Individual Consultant(SIS)</option>");
                                       }
                                       else if(CommonUtils.checkNull(eOIExcellBean.getProcurementSubmethod()).contains("LCS")){
                                       
                                            out.println("<option value=\"Quality Cost Based Selection(QCBS)\">Quality Cost Based Selection(QCBS)</option>");
                                            out.println("<option value=\"Design Contest(DC)\">Design Contest(DC)</option>");
                                            out.println("<option value=\"Individual Consultant(IC)\">Individual Consultant(IC)</option>");
                                            out.println("<option value=\"Selection under a Fixed Budget(SFB)\">Selection under a Fixed Budget(SFB)</option>");
                                            out.println("<option selected=\"selected\" value=\"Least Cost Selection(LCS)\">Least Cost Selection(LCS)</option>");
                                            out.println("<option value=\"Selection Community Service Organisation(SCSO)\">Selection Community Service Organisation(SCSO)</option>");
                                            out.println("<option value=\"Single Source Selection(SSS)\">Single Source Selection(SSS)</option>");
                                            out.println("<option value=\"Selection of Individual Consultant(SIS)\">Selection of Individual Consultant(SIS)</option>");
                                       }
                                       else if(CommonUtils.checkNull(eOIExcellBean.getProcurementSubmethod()).contains("SCSO")){
                              
                                            out.println("<option value=\"Quality Cost Based Selection(QCBS)\">Quality Cost Based Selection(QCBS)</option>");
                                            out.println("<option value=\"Design Contest(DC)\">Design Contest(DC)</option>");
                                            out.println("<option value=\"Individual Consultant(IC)\">Individual Consultant(IC)</option>");
                                            out.println("<option value=\"Selection under a Fixed Budget(SFB)\">Selection under a Fixed Budget(SFB)</option>");
                                            out.println("<option value=\"Least Cost Selection(LCS)\">Least Cost Selection(LCS)</option>");
                                            out.println("<option selected=\"selected\" value=\"Selection Community Service Organisation(SCSO)\">Selection Community Service Organisation(SCSO)</option>");
                                            out.println("<option value=\"Single Source Selection(SSS)\">Single Source Selection(SSS)</option>");
                                            out.println("<option value=\"Selection of Individual Consultant(SIS)\">Selection of Individual Consultant(SIS)</option>");
                                       }
                                       else if(CommonUtils.checkNull(eOIExcellBean.getProcurementSubmethod()).contains("SSS")){
              
                                            out.println("<option value=\"Quality Cost Based Selection(QCBS)\">Quality Cost Based Selection(QCBS)</option>");
                                            out.println("<option value=\"Design Contest(DC)\">Design Contest(DC)</option>");
                                            out.println("<option value=\"Individual Consultant(IC)\">Individual Consultant(IC)</option>");
                                            out.println("<option value=\"Selection under a Fixed Budget(SFB)\">Selection under a Fixed Budget(SFB)</option>");
                                            out.println("<option value=\"Least Cost Selection(LCS)\">Least Cost Selection(LCS)</option>");
                                            out.println("<option value=\"Selection Community Service Organisation(SCSO)\">Selection Community Service Organisation(SCSO)</option>");
                                            out.println("<option selected=\"selected\" value=\"Single Source Selection(SSS)\">Single Source Selection(SSS)</option>");
                                            out.println("<option value=\"Selection of Individual Consultant(SIS)\">Selection of Individual Consultant(SIS)</option>");
                                       }

                                        else if(CommonUtils.checkNull(eOIExcellBean.getProcurementSubmethod()).contains("SIS")){
                                            out.println("<option value=\"Quality Cost Based Selection(QCBS)\">Quality Cost Based Selection(QCBS)</option>");
                                            out.println("<option value=\"Design Contest(DC)\">Design Contest(DC)</option>");
                                            out.println("<option value=\"Individual Consultant(IC)\">Individual Consultant(IC)</option>");
                                            out.println("<option value=\"Selection under a Fixed Budget(SFB)\">Selection under a Fixed Budget(SFB)</option>");
                                            out.println("<option value=\"Least Cost Selection(LCS)\">Least Cost Selection(LCS)</option>");
                                            out.println("<option value=\"Selection Community Service Organisation(SCSO)\">Selection Community Service Organisation(SCSO)</option>");
                                            out.println("<option value=\"Single Source Selection(SSS)\">Single Source Selection(SSS)</option>");
                                            out.println("<option selected=\"selected\" value=\"Selection of Individual Consultant(SIS)\">Selection of Individual Consultant(SIS)</option>");
                                       }

                                       else if(CommonUtils.checkNull(eOIExcellBean.getProcurementSubmethod()).contains("DC")){
                                            out.println("<option value=\"Quality Cost Based Selection(QCBS)\">Quality Cost Based Selection(QCBS)</option>");
                                            out.println("<option selected=\"selected\" value=\"Design Contest(DC)\">Design Contest(DC)</option>");
                                            out.println("<option value=\"Individual Consultant(IC)\">Individual Consultant(IC)</option>");
                                            out.println("<option value=\"Selection under a Fixed Budget(SFB)\">Selection under a Fixed Budget(SFB)</option>");
                                            out.println("<option value=\"Least Cost Selection(LCS)\">Least Cost Selection(LCS)</option>");
                                            out.println("<option value=\"Selection Community Service Organisation(SCSO)\">Selection Community Service Organisation(SCSO)</option>");
                                            out.println("<option value=\"Single Source Selection(SSS)\">Single Source Selection(SSS)</option>");
                                            out.println("<option value=\"Selection of Individual Consultant(SIS)\">Selection of Individual Consultant(SIS)</option>");
                                       }
                                       else {
                                            out.println("<option value=\"Quality Cost Based Selection(QCBS)\">Quality Cost Based Selection(QCBS)</option>");
                                            out.println("<option value=\"Design Contest(DC)\">Design Contest(DC)</option>");
                                            out.println("<option selected=\"selected\" value=\"Individual Consultant(IC)\">Individual Consultant(IC)</option>");
                                            out.println("<option value=\"Selection under a Fixed Budget(SFB)\">Selection under a Fixed Budget(SFB)</option>");
                                            out.println("<option value=\"Least Cost Selection(LCS)\">Least Cost Selection(LCS)</option>");
                                            out.println("<option value=\"Selection Community Service Organisation(SCSO)\">Selection Community Service Organisation(SCSO)</option>");
                                            out.println("<option value=\"Single Source Selection(SSS)\">Single Source Selection(SSS)</option>");
                                            out.println("<option value=\"Selection of Individual Consultant(SIS)\">Selection of Individual Consultant(SIS)</option>");
                                       }

                                        %>


                                               <%
                                                /*String[] arr = {"Community Service Organisation","Design Contest","Individual Consultant","Least Cost Selection","Quality Cost Based Selection","Under a Fixed Budget","Based on Consultants Qualification","Single Source Selection" };
                                                Map methods = new HashMap();
                                                for(String submethod : arr){
                                                         if(CommonUtils.checkNull(eOIExcellBean.getProcurementSubmethod()).equals(submethod)){
                                                             out.println("<option selected=\"selected\"  value=\""+submethod+"\">"+submethod+"</option>");
                                                          }else{
                                                             out.println("<option  value=\""+submethod+"\">"+submethod+"</option>");
                                                          }
                                                    }*/




                                                %>
                                               <!--  <option value="CSO">Community Service Organisation</option>
                                                <option value="DC">Design Contest</option>
                                                <option value="IC">Individual Consultant</option>
                                                <option value="LCS">Least Cost Selection</option>
                                                <option value="QCBS">Quality Cost Based Selection</option>
                                                <option value="UFB">Under a Fixed Budget</option>
                                                <option value="BCQ">Based on Consultants Qualification</option>
                                                <option value="SSS">Single Source Selection</option> -->
                                            </select>
                                        </td>
                                        <td class="ff">Budget Type : <span>*</span></td>
                                        <td colspan="3">
                                            <select name="budgetType" class="formTxtBox_1" id="Budget" style="width: 200px;">
                                           <!--  <option value="select" selected="selected">--- Please Select ---</option> -->
                                             <%
                                                if(CommonUtils.checkNull(eOIExcellBean.getBudgetFund()).contains("Revenue Budget")){
                                                  out.println("<option selected=\"selected\"  value=\"Revenue Budget\">Revenue Budget</option>");
                                                  out.println("<option  value=\"Development Budget\">Development Budget</option>");
                                                  }else{
                                                  out.println("<option  value=\"Revenue Budget\">Revenue Budget</option>");
                                                  out.println("<option  selected=\"selected\" value=\"Development Budget\">Development Budget</option>");
                                                  }

                                              %>
                                                       
                                             </select>
                                        </td>                                        

                                    </tr>
                                    <tr>
                                        <td class="ff" width="25%">Source of Funds : <span>*</span></td>
                                        <td width="25%"><select name="sourceFunds" class="formTxtBox_1" id="Funds" style="width: 200px;">
                                                <!--  <option value="select" selected="selected">--- Please Select ---</option> -->
                                              <%
                                               if(CommonUtils.checkNull(eOIExcellBean.getSourceFund()).equals("Government")){
                                                   out.println("<option selected=\"selected\" value=\"Government\">Government</option>");
                                                   out.println("<option value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                   out.println("<option value=\"Own Funds\">Own Funds</option>");
                                               }else if(CommonUtils.checkNull(eOIExcellBean.getSourceFund()).contains("Aid Grant")){
                                                   out.println("<option  value=\"Government\">Government</option>");
                                                   out.println("<option selected=\"selected\" value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                   out.println("<option value=\"Own Funds\">Own Funds</option>");
                                               }else{
                                                   out.println("<option  value=\"Government\">Government</option>");
                                                   out.println("<option value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                   out.println("<option selected=\"selected\" value=\"Own Funds\">Own Funds</option>");
                                               }
                                                %>
                                              
                                             </select>
                                        </td>
                                        <td class="ff" width="25%">Development Partners : </td>
                                        <td width="25%">
                                            <input name="devPartners" class="formTxtBox_1" id="txtDevPartners" value="<%=CommonUtils.checkNull(eOIExcellBean.getDevelopmentPartner()) %>" style="width: 280px;" type="text">
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="tableHead_22 ">Particular Information :</div>
                                
                                <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                                    <tbody>
                                    <tr>
                                        <td class="ff" width="25%">Project Code : </td>
                                        <td width="25%">
                                            <input name="projectCode" class="formTxtBox_1" id="txtProjectCode" value="<%=CommonUtils.checkNull(eOIExcellBean.getProjectOrProgramCode()) %>" style="width: 280px;" type="text">
                                        </td>
                                        <td class="ff" width="25%">Project Name : </td>
                                        <td width="25%">
                                            <input name="projectName" class="formTxtBox_1" id="txtProjectName" value="<%=CommonUtils.checkNull(eOIExcellBean.getProjectOrProgrammeName()) %>" style="width: 280px;" type="text">
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="ff">EOI Closing<br>Date and Time : <span>*</span></td>
                                        <td width="25%">
                                            <input name="txteoiCloseDate" class="formTxtBox_1" id="txteoiCloseDate" value="<%=CommonUtils.checkNull(eOIExcellBean.getEoiClosingDateandtime())%>" style="width: 100px;" readonly="true" onfocus="GetCal('txteoiCloseDate','txteoiCloseDate');" type="text">
                                            <img id="txteoiCloseDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCal('txteoiCloseDate','txteoiCloseDateimg');" border="0">
                                            <span id="spantxteoiCloseDate"></span>
                                        </td>
                                        <td class="ff"></td>
                                        <td class="ff">

                                        </td>
                                    </tr>
                                    </tbody>
                                </table>


                                <div class="tableHead_22 ">Information for Bidder/Consultant :</div>
                                <table class="formStyle_1 " width="100%" border="0" cellpadding="0" cellspacing="10">
                                    <tbody>                                    
                                    <tr>
                                        <td class="ff" width="25%">Brief Description of Assignment : <span>*</span></td>
                                        <td><input value="Services" id="briefValMsg" type="hidden">
                                            <textarea  style="width: 280px;"  cols="100" rows="3" id="txtabriefDescAssignment" name="briefDescAssignment" class="ckeditor"><%=CommonUtils.checkNull(eOIExcellBean.getBriefDescriptionoftheAssignment()) %></textarea>
                                            
                                            <span id="spantxtabriefDescAssignment"></span>
                                        </td><td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Experience, Resources and<br>
                                            Delivery Capacity Required : <span>*</span></td>
                                        <td><textarea style="width: 280px;" cols="100" rows="3" id="txtaexpRequired" name="expRequired" class="ckeditor"><%=CommonUtils.checkNull(eOIExcellBean.getExpResDeliverCapacityRequired()) %></textarea>
                                        
                                        </td><td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Other Details (if  applicable) :</td>
                                        <td><textarea style="width: 280px;" cols="100" rows="3" id="txtaotherDetails" name="otherDetails" class="ckeditor"><%=CommonUtils.checkNull(eOIExcellBean.getOtherDetails()) %></textarea>                                       
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Association with Foreign  Firm : <span>*</span></td>
                                        <td>
                                            <select name="assoForiegnFirm" class="formTxtBox_1" id="assoForiegnFirm" style="width: 200px;">
                                                       <!-- <option value="select" selected="selected">--- Please Select ---</option> -->
                                                        <%
                                                        if(CommonUtils.checkNull(eOIExcellBean.getAssociationwithforeignfirms()).contains("Encouraged")){
                                                          out.println("<option selected=\"selected\"  value=\"Encouraged\">Encouraged</option>");
                                                          out.println("<option  value=\"Not Encouraged\">Not Encouraged</option>");
                                                          }else{
                                                          out.println("<option  value=\"Encouraged\">Encouraged</option>");
                                                          out.println("<option  selected=\"selected\" value=\"Not Encouraged\">Not Encouraged</option>");
                                                          }

                                                      %>
                                                      
                                             </select>
                                            <span id="spanassoForiegnFirm" name="spanassoForiegnFirm" style="color: red;">Not Applicable</span>
                                            
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                   
                                    </tbody>
                                </table>
                               
                                <table id="dataTable" class="tableList_1 t_space" width="100%" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <!--<th class="t-align-center" width="5%">Select</th>-->
                                            <th class="t-align-center" width="15%">Ref. No. </th>
                                            <th class="t-align-center" width="30%">Phasing of Service <span Style="color:red">*</span></th>
                                            <th class="t-align-center" width="20%">Location </th>
                                            <th class="t-align-center" width="10%">Indicative Start<br> Date (Month/Year) </th>
                                            <th class="t-align-center" width="10%">Indicative Completion<br> Date (Month/Year)</th>
                                        </tr>
                                        <tr>
                                            <!--<td class="t-align-center"><input class="formTxtBox_1" name="chk_0" id="chk0" type="checkbox"></td>-->
                                            <td class="t-align-center">
                                                <input name="refNo_0" class="formTxtBox_1" value="<%= CommonUtils.checkNull(eOIExcellBean.getRefNo_1()) %>" id="txtrefNo_0"  style="width: 95%;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                <textarea class="formTxtBox_1" style="width: 280px;" style="width: 98%;" rows="3"  id="txtaphasingService_0" name="phasingService_0" class="formTxtBox_1"><%= CommonUtils.checkNull(eOIExcellBean.getPhasingOfServices_1()) %></textarea>
                                            </td>
                                            <td class="t-align-center">
                                                <input name="locationRefNo_0"  value="<%= CommonUtils.checkNull(eOIExcellBean.getLocation_1()) %>" class="formTxtBox_1" id="txtlocationRefNo_0" style="width: 95%;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                <input  name="indicativeStartDate_0"  value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeStartDateMonOrYer_1()) %>" class="formTxtBox_1" id="txtindicativeStartDate_0" style="width: 100px;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                <input  name="indicativeComplDate_0"  value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeCompletionDateMonOrYer_1()) %>"  class="formTxtBox_1" id="txtindicativeComplDate_0" style="width: 100px;" type="text">
                                            </td>
                                        </tr>

                                        <tr id="lot_1" >
                                            <td class="t-align-center">
                                                <input name="refNo_1" class="formTxtBox_1" value="<%= CommonUtils.checkNull(eOIExcellBean.getRefNo_2()) %>" id="txtrefNo_1"  style="width: 95%;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                <textarea class="formTxtBox_1" style="width: 280px;" style="width: 98%;" rows="3"   id="txtaphasingService_1" name="phasingService_1" class="formTxtBox_1"><%= CommonUtils.checkNull(eOIExcellBean.getPhasingOfServices_2()) %></textarea>
                                            </td>
                                            <td class="t-align-center">
                                                <input name="locationRefNo_1" value="<%= CommonUtils.checkNull(eOIExcellBean.getLocation_2()) %>" class="formTxtBox_1" id="txtlocationRefNo_1" style="width: 95%;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                <input  name="indicativeStartDate_1" value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeStartDateMonOrYer_2()) %>" class="formTxtBox_1" id="txtindicativeStartDate_1" style="width: 100px;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                <input  name="indicativeComplDate_1"  value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeCompletionDateMonOrYer_2()) %>" class="formTxtBox_1" id="txtindicativeComplDate_1" style="width: 100px;" type="text">
                                            </td>
                                        </tr>

                                        <tr id="lot_2" >
                                            <!--<td class="t-align-center"><input class="formTxtBox_1" name="chk_2" id="chk0" type="checkbox"></td>-->
                                            <td class="t-align-center">
                                                    <input name="refNo_2" value="<%= CommonUtils.checkNull(eOIExcellBean.getRefNo_3()) %>" class="formTxtBox_1" id="txtrefNo_2"  style="width: 95%;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <textarea class="formTxtBox_1" style="width: 280px;" style="width: 98%;"  rows="3" id="txtaphasingService_2" name="phasingService_2" class="formTxtBox_1"><%= CommonUtils.checkNull(eOIExcellBean.getPhasingOfServices_3()) %></textarea>
                                            </td>
                                            <td class="t-align-center">
                                                    <input name="locationRefNo_2" value="<%= CommonUtils.checkNull(eOIExcellBean.getLocation_3()) %>" class="formTxtBox_1" id="txtlocationRefNo_2" style="width: 95%;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <input  name="indicativeStartDate_2" value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeStartDateMonOrYer_3()) %>" class="formTxtBox_1" id="txtindicativeStartDate_2" style="width: 100px;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <input  name="indicativeComplDate_2"  value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeCompletionDateMonOrYer_3()) %>" class="formTxtBox_1" id="txtindicativeComplDate_2" style="width: 100px;" type="text">
                                            </td>
                                        </tr>

                                       <tr id="lot_3" >
                                            <!--<td class="t-align-center"><input class="formTxtBox_1" name="chk_3" id="chk0" type="checkbox"></td>-->
                                            <td class="t-align-center">
                                                    <input name="refNo_3" value="<%= CommonUtils.checkNull(eOIExcellBean.getRefNo_4()) %>" class="formTxtBox_1" id="txtrefNo_3"  style="width: 95%;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <textarea class="formTxtBox_1" style="width: 280px;" style="width: 98%;"  rows="3" id="txtaphasingService_3" name="phasingService_3" class="formTxtBox_1"><%= CommonUtils.checkNull(eOIExcellBean.getPhasingOfServices_4()) %></textarea>
                                            </td>
                                            <td class="t-align-center">
                                                    <input name="locationRefNo_3" class="formTxtBox_1" value="<%= CommonUtils.checkNull(eOIExcellBean.getLocation_4()) %>" id="txtlocationRefNo_3" style="width: 95%;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <input  name="indicativeStartDate_3"  value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeStartDateMonOrYer_4()) %>" class="formTxtBox_1" id="txtindicativeStartDate_3" style="width: 100px;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <input  name="indicativeComplDate_3"  value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeCompletionDateMonOrYer_4()) %>"  class="formTxtBox_1" id="txtindicativeComplDate_3" style="width: 100px;" type="text">
                                            </td>
                                        </tr>

                                        <tr id="lot_4" >
                                            <!--<td class="t-align-center"><input class="formTxtBox_1" name="chk_4" id="chk0" type="checkbox"></td>-->
                                            <td class="t-align-center">
                                                    <input name="refNo_4" class="formTxtBox_1" value="<%= CommonUtils.checkNull(eOIExcellBean.getRefNo_5()) %>" id="txtrefNo_4"  style="width: 95%;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <textarea class="formTxtBox_1" style="width: 280px;" style="width: 98%;" rows="3"  id="txtaphasingService_4" name="phasingService_4" class="formTxtBox_1"><%= CommonUtils.checkNull(eOIExcellBean.getPhasingOfServices_5()) %></textarea>
                                            </td>
                                            <td class="t-align-center">
                                                    <input name="locationRefNo_4" value="<%= CommonUtils.checkNull(eOIExcellBean.getLocation_5()) %>" class="formTxtBox_1" id="txtlocationRefNo_4" style="width: 95%;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <input  name="indicativeStartDate_4"  value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeStartDateMonOrYer_5()) %>" class="formTxtBox_1" id="txtindicativeStartDate_4" style="width: 100px;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <input  name="indicativeComplDate_4" value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeCompletionDateMonOrYer_5()) %>" class="formTxtBox_1" id="txtindicativeComplDate_4" style="width: 100px;" type="text">
                                            </td>
                                        </tr>

                                        <tr id="lot_5" >
                                            <!--<td class="t-align-center"><input class="formTxtBox_1" name="chk_5" id="chk0" type="checkbox"></td>-->
                                            <td class="t-align-center">
                                                    <input name="refNo_5" class="formTxtBox_1" value="<%= CommonUtils.checkNull(eOIExcellBean.getRefNo_6()) %>" id="txtrefNo_5"  style="width: 95%;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <textarea class="formTxtBox_1" style="width: 280px;" style="width: 98%;" rows="3"  id="txtaphasingService_5" name="phasingService_5" class="formTxtBox_1"><%= CommonUtils.checkNull(eOIExcellBean.getPhasingOfServices_6()) %></textarea>
                                            </td>
                                            <td class="t-align-center">
                                                    <input name="locationRefNo_5" value="<%= CommonUtils.checkNull(eOIExcellBean.getLocation_6()) %>" class="formTxtBox_1" id="txtlocationRefNo_5" style="width: 95%;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <input  name="indicativeStartDate_5"  value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeStartDateMonOrYer_6()) %>" class="formTxtBox_1" id="txtindicativeStartDate_5" style="width: 100px;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <input  name="indicativeComplDate_5" value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeCompletionDateMonOrYer_6()) %>" class="formTxtBox_1" id="txtindicativeComplDate_5" style="width: 100px;" type="text">
                                            </td>
                                        </tr>

                                        <tr id="lot_6" >
                                            <!--<td class="t-align-center"><input class="formTxtBox_1" name="chk_6" id="chk0" type="checkbox"></td>-->
                                            <td class="t-align-center">
                                                    <input name="refNo_6" class="formTxtBox_1" value="<%= CommonUtils.checkNull(eOIExcellBean.getRefNo_7()) %>" id="txtrefNo_6"  style="width: 95%;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <textarea class="formTxtBox_1" style="width: 280px;" style="width: 98%;"  rows="3" id="txtaphasingService_6" name="phasingService_6" class="formTxtBox_1"><%= CommonUtils.checkNull(eOIExcellBean.getPhasingOfServices_7()) %></textarea>
                                            </td>
                                            <td class="t-align-center">
                                                    <input name="locationRefNo_6"value="<%= CommonUtils.checkNull(eOIExcellBean.getLocation_7()) %>"  class="formTxtBox_1" id="txtlocationRefNo_6" style="width: 95%;" type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <input  name="indicativeStartDate_6" value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeStartDateMonOrYer_7()) %>" class="formTxtBox_1" id="txtindicativeStartDate_6" style="width: 100px;"  type="text">
                                            </td>
                                            <td class="t-align-center">
                                                    <input  name="indicativeComplDate_6"  value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeCompletionDateMonOrYer_7()) %>" class="formTxtBox_1" id="txtindicativeComplDate_6" style="width: 100px;" type="text">
                                            </td>
                                        </tr>

                                        <tr id="lot_7" >
                                                <!--<td class="t-align-center"><input class="formTxtBox_1" name="chk_7" id="chk0" type="checkbox"></td>-->
                                                <td class="t-align-center">
                                                        <input name="refNo_7" value="<%= CommonUtils.checkNull(eOIExcellBean.getRefNo_8() )%>" class="formTxtBox_1" id="txtrefNo_7"  style="width: 95%;" type="text">
                                                </td>
                                                <td class="t-align-center">
                                                        <textarea class="formTxtBox_1" style="width: 280px;" style="width: 98%;" rows="3" id="txtaphasingService_7" name="phasingService_7" class="formTxtBox_1"><%= CommonUtils.checkNull(eOIExcellBean.getPhasingOfServices_8())%></textarea>
                                                </td>
                                                <td class="t-align-center">
                                                        <input name="locationRefNo_7" value="<%= CommonUtils.checkNull(eOIExcellBean.getLocation_8()) %>" class="formTxtBox_1" id="txtlocationRefNo_7" style="width: 95%;" type="text">
                                                </td>
                                                <td class="t-align-center">
                                                        <input  name="indicativeStartDate_7"  value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeStartDateMonOrYer_8()) %>" class="formTxtBox_1" id="txtindicativeStartDate_7" style="width: 100px;" type="text">
                                                </td>
                                                <td class="t-align-center">
                                                        <input  name="indicativeComplDate_7" value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeCompletionDateMonOrYer_8()) %>" class="formTxtBox_1" id="txtindicativeComplDate_7" style="width: 100px;" type="text">
                                                </td>
                                        </tr>

                                        <tr id="lot_8" >
                                                <!--<td class="t-align-center"><input class="formTxtBox_1" name="chk_8" id="chk0" type="checkbox"></td>-->
                                                <td class="t-align-center">
                                                        <input name="refNo_8" class="formTxtBox_1" value="<%= CommonUtils.checkNull(eOIExcellBean.getRefNo_9()) %>" id="txtrefNo_8"  style="width: 95%;" type="text">
                                                </td>
                                                <td class="t-align-center">
                                                        <textarea class="formTxtBox_1" style="width: 280px;" style="width: 98%;" rows="3" id="txtaphasingService_8" name="phasingService_8" class="formTxtBox_1"><%= CommonUtils.checkNull(eOIExcellBean.getPhasingOfServices_9()) %></textarea>
                                                </td>
                                                <td class="t-align-center">
                                                        <input name="locationRefNo_8" value="<%=CommonUtils.checkNull( eOIExcellBean.getLocation_9()) %>"  class="formTxtBox_1" id="txtlocationRefNo_8" style="width: 95%;" type="text">
                                                </td>
                                                <td class="t-align-center">
                                                        <input  name="indicativeStartDate_8" value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeStartDateMonOrYer_9()) %>"  class="formTxtBox_1" id="txtindicativeStartDate_8" style="width: 100px;"  type="text">
                                                </td>
                                                <td class="t-align-center">
                                                        <input  name="indicativeComplDate_8" value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeCompletionDateMonOrYer_9()) %>" class="formTxtBox_1" id="txtindicativeComplDate_8" style="width: 100px;" type="text">
                                                </td>
                                        </tr>

                                        <tr id="lot_9" >
                                                <!--<td class="t-align-center"><input class="formTxtBox_1" name="chk_9" id="chk0" type="checkbox"></td>-->
                                                <td class="t-align-center">
                                                        <input name="refNo_9" value="<%= CommonUtils.checkNull(eOIExcellBean.getRefNo_10()) %>" class="formTxtBox_1" id="txtrefNo_9"  style="width: 95%;" type="text">
                                                </td>
                                                <td class="t-align-center">
                                                        <textarea class="formTxtBox_1" style="width: 280px;" style="width: 98%;"  rows="3" id="txtaphasingService_9" name="phasingService_9" class="formTxtBox_1"><%= CommonUtils.checkNull(eOIExcellBean.getPhasingOfServices_10()) %></textarea>
                                                </td>
                                                <td class="t-align-center">
                                                        <input name="locationRefNo_9" value="<%= CommonUtils.checkNull(eOIExcellBean.getLocation_10()) %>" class="formTxtBox_1" id="txtlocationRefNo_9" style="width: 95%;" type="text">
                                                </td>
                                                <td class="t-align-center">
                                                        <input  name="indicativeStartDate_9"  value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeStartDateMonOrYer_10()) %>" class="formTxtBox_1" id="txtindicativeStartDate_9" style="width: 100px;" type="text">
                                                </td>
                                                <td class="t-align-center">
                                                        <input  name="indicativeComplDate_9" value="<%= CommonUtils.checkNull(eOIExcellBean.getIndicativeCompletionDateMonOrYer_10()) %>"class="formTxtBox_1" id="txtindicativeComplDate_9" style="width: 100px;"  type="text">
                                                </td>
                                        </tr>

                                    </tbody>
                                </table>
                                <div class="tableHead_22 t_space">Procuring Entity Details :</div>
                                <table class="formStyle_1 t_space" width="100%" border="0" cellpadding="0" cellspacing="10">
                                    <tbody>
                                        <tr>
                                            <td class="ff" width="25%">Name of Official Inviting  REOI : <span>*</span></td>
                                            <td width="25%"><input name="nameOfficial" class="formTxtBox_1" value="<%= CommonUtils.checkNull(eOIExcellBean.getNameoftheOfficialInvitingEOI()) %>" id="txtNameOfficial" style="width: 280px;" type="text"></td>
                                            <td class="ff" width="26%"> Designation of Official Inviting  REOI : <span>*</span></td>
                                            <td width="25%"><input name="designation" value="<%= CommonUtils.checkNull(eOIExcellBean.getDesignationoftheOfficialInvitingEOI()) %>" class="formTxtBox_1" id="txtDesignation" style="width: 280px;" type="text"></td>
                                        </tr>
                                        <tr>
                                            <td class="ff">Address of Official Inviting  REOI : <span>*</span> </td>
                                            <td>
                                                <textarea class="formTxtBox_1" style="width: 280px;" name="contractddress" rows="3" cols="49" id="txtContractAddress"><%= CommonUtils.checkNull(eOIExcellBean.getAddressoftheOfficialInvitingEOI()) %></textarea>
                                            </td>
                                            <% String contacts="";
                                            if(request.getParameterMap().containsKey("action")){
                                                 contacts = CommonUtils.checkNull(eOIExcellBean.getContactDetailsoftheOfficialInvitingEOI());

                                            } else {

                                                 contacts = "Phone:"+CommonUtils.checkNull(eOIExcellBean.getContactDetailsoftheOfficialInvitingEOI())+" Fax:"+CommonUtils.checkNull(eOIExcellBean.getFax())+" Mail:"+CommonUtils.checkNull(eOIExcellBean.getEmail());
                                            }
                                            %>
                                                    <td class="ff">Contact details of Official Inviting  REOI :</td>
                                                <td><textarea class="formTxtBox_1" style="width: 280px;" name="contactDetails" rows="3" cols="49" id="txtcontactDetails"><%=contacts%></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="ff mandatory">The procuring entity reserves the right to accept or reject all Tenders/Proposals / Pre-Qualifications / EOIs</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="formStyle_1 t_space" width="100%" border="0" cellpadding="0" cellspacing="10">
                                    <tbody>
                                        <tr>
                                            <td colspan="4" align="center">
                                                <label class="formBtn_1">
                                                    <input name="submit" id="btnsubmit" value="Submit" type="submit" />
                                                    <input name="hdnbutton" id="hdnbutton" value="" type="hidden">
                                                </label>&nbsp;&nbsp;

                                                <input value="1" id="txtcounter" name="txtcounter" type="hidden">
                                                <input id="combineRefNo" name="combineRefNo" type="hidden">
                                                <input id="combinePOS" name="combinePOS" type="hidden">
                                                <input id="combineLocation" name="combineLocation" type="hidden">
                                                <input id="combineIndicativeStartDate" name="combineIndicativeStartDate" type="hidden">
                                                <input id="combineIndicativeComplDate" name="combineIndicativeComplDate" type="hidden">

                                                <input id="combineLocationLot" name="combineLocationLot" type="hidden">
                                                <input id="combineTenderSecurityAmount" name="combineTenderSecurityAmount" type="hidden">
                                                <input id="combineDocFeesLot" name="combineDocFeesLot" type="hidden">
                                                <input id="combineComplTimeLotNo" name="combineComplTimeLotNo" type="hidden">
                                                <input id="combineStartTimeLotNo" name="combineStartTimeLotNo" type="hidden">
                                                <input id="combinetenderLotSecId" name="combinetenderLotSecId" type="hidden">
                                                <input id="hdndoc" name="hdndoc" type="hidden">
                                                <input id="hdndocprice" name="hdndocprice" value="" type="hidden">
                                                <input id="tenderId" value="658" name="tenderId" type="hidden">
                                                <input id="chkSecAmt" name="chkSecAmt" value="" type="hidden">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div>&nbsp;</div>
                            </form>
    <%
    }

    %>
                                 <script type="text/javascript">
                                    function loadOrganization(){
                                        //var deptId= 0;
                                       // var districtId = $('#cmbDistrict').val();
                                       //if($('#cmbDivision').val()>0){
                                        //   deptId=$('#cmbDivision').val();
                                       //}
                                       //else{deptId= $('#cmbMinistry').val(); }
                                        var deptId= $('#Ministry').val();
                                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: deptId, funName:'offlineTenderOrgCombo'},  function(j){
                                        $('#cmbOrganization').children().remove().end()
                                        $("select#cmbOrganization").html(j);
                                        
                                        var orgObj =document.getElementById("Ministry");
                                         var orgval = orgObj.options[orgObj.selectedIndex].text;
                                         document.getElementById("hidministry").value = orgval;
                                    });
                                    }
                                </script>

                            <script type="text/javascript">
                                function GetCal(txtname,controlname)
                                {
                                    new Calendar({
                                        inputField: txtname,
                                        trigger: controlname,
                                        showTime: 24,
                                        onSelect: function() {
                                            var date = Calendar.intToDate(this.selection.get());
                                            LEFT_CAL.args.min = date;
                                            LEFT_CAL.redraw();
                                            this.hide();
                                            document.getElementById(txtname).focus();
                                        }
                                    });

                                    var LEFT_CAL = Calendar.setup({
                                        weekNumbers: false
                                    })
                                }

                                function GetCalWithouTime(txtname,controlname)
                                {
                                    new Calendar({
                                        inputField: txtname,
                                        trigger: controlname,
                                        showTime: false,
                                        dateFormat:"%d/%m/%Y",
                                        onSelect: function() {
                                            var date = Calendar.intToDate(this.selection.get());
                                            LEFT_CAL.args.min = date;
                                            LEFT_CAL.redraw();
                                            this.hide();
                                            document.getElementById(txtname).focus();
                                        }
                                    });

                                    var LEFT_CAL = Calendar.setup({
                                        weekNumbers: false
                                    })
                                }
                            </script>                        
                       
              </div>
                    
            </div>
                 <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>
                 

    </body>
</html>

