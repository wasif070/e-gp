<%--
    Document   : CreateTenderWithoutPQ
    Created on : 07-Aug-2012, 10:23:34
    Author     : salahuddin
--%>

<%@page import="com.cptu.egp.eps.web.servicebean.TenderDashboardOfflineSrBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.AwardedContractOfflineSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotPhasingOffline"%>
<%@page import="com.cptu.egp.eps.web.offlinedata.OfflineDataSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetailsOffline"%>
<%@page import="com.cptu.egp.eps.web.utility.CommonUtils"%>
<%@page import="com.cptu.egp.eps.web.offlinedata.TenderFormExcellBean"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblStateMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.model.table.TblDepartmentMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ManageEmployeeGridSrBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Create Tender without Pre-qualification (PQ)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>

        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>

    <script src="resources/config.js" type="text/javascript"></script>
    <link href="resources/editor.css" type="text/css" rel="stylesheet"/>
    <script src="resources/en.js" type="text/javascript"></script>
       <!--

        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>

        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />

       -->
       <!--jquery validator max length css change - Dohatec-->
       <style type="text/css">
           label.error{color:red}
       </style>
       
       <script type="text/javascript">


           $(document).ready(function(){

                 var frank_param = getParam( 'alert' );
                if((frank_param) == 'true')
                {
                    alert("Please select correct excel file");
                }
                
                // Sorting Dropdown
                $("select").each(function() {

                    // Keep track of the selected option.
                    var selectedValue = $(this).val();

                    // Sort all the options by text. I could easily sort these by val.
                    $(this).html($("option", $(this)).sort(function(a, b) {
                        return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
                    }));

                    // Select one option.
                    $(this).val(selectedValue);
                });

                    //Form Validation
                $("#frmUploadOfflineDate").validate({
                    rules: {
                        //textbox
                        fileUploadControl: {required: true}
                         },
                    messages: {
                        //Textbox
                        fileUploadControl: { required: "<div class='reqF_1'>Please select a file.</div>"}
                    }
                });
                $("#frmCreateTender").validate({
                    rules: {
                        //textbox
                        peName: {required: true, maxlength: 150},
                        invitationRefNo: {required: true, maxlength: 50},
                        issuedate: {required: true, date:true},
                        packageNo: {required: true, maxlength: 50},
                        packageName: {required: true, maxlength: 150},
                        tenderpublicationDate: {required: true, date:true},
                        tenderCloseDate: {required: true, date:true},
                        tenderOpenDate: {required: true, date:true},
                        tenderLastSellDate: {required: true, date:true},
                        tenderDocPrice: {required: true, number: true, maxlength: 15},
                        LotNo: {required: true},
                        DescriptionForPQ: {required: true},
                        locationlot_0: {required: true},
                        startLotNo_0: {required: true},
                        complTimeLotNo_0: {required: true},
                        nameOfficial: {required: true, maxlength: 200},
                        designationOfficial: {required: true, maxlength: 200},
                        eligibilityofTenderer:{required: true},
                        briefDescGoods:{required: true},
                        briefDescRelServices:{required: true},
                        receivingTenDoc :{required: true, maxlength: 2000},
                        openTenderDoc :{required: true, maxlength: 2000},
                        sellingDocPrinciple:{required: true, maxlength: 2000},

                        peCode:{maxlength: 15},
                        devPartner:{maxlength: 300},
                        projectCode:{maxlength: 150},
                        projectName:{maxlength: 150},
                        sellingDocOthers:{maxlength: 2000},
                        placeofPQMeeting:{maxlength: 2000},
                        addressOfOfficial:{maxlength: 5000},
                        contactDetail:{maxlength: 1000},
                        
                        lotNo_1:{maxlength: 150},
                        identification_1:{required: true, maxlength: 2000},
                        locationlot_1:{maxlength: 100},
                        tenderSecurity_1:{maxlength: 15},
                        complTimeLotNo_1:{maxlength: 100},
                        lotNo_2:{maxlength: 150},
                        identification_2:{maxlength: 2000},
                        locationlot_2:{maxlength: 100},
                        tenderSecurity_2:{maxlength: 15},
                        complTimeLotNo_2:{maxlength: 100},
                        lotNo_3:{maxlength: 150},
                        identification_3:{maxlength: 2000},
                        locationlot_3:{maxlength: 100},
                        tenderSecurity_3:{maxlength: 15},
                        complTimeLotNo_3:{maxlength: 100},
                        lotNo_4:{maxlength: 150},
                        identification_4:{maxlength: 2000},
                        locationlot_4:{maxlength: 100},
                        tenderSecurity_4:{maxlength: 15},
                        complTimeLotNo_4:{maxlength: 100},


                        //Dropdown
                        Ministry:{selectNone: true},
                        Organization: {selectNone: true},
                        district:{selectNone: true},
                        nature:{selectNone: true},
                        procType:{selectNone: true},
                        invitationFor:{selectNone: true},
                        procureMethod:{selectNone: true},
                        budgetType:{selectNone: true},
                        sourceFunds:{selectNone: true},
                        eventType:{selectNone: true}

                    },
                    messages: {
                        //Textbox
                        peName: { required: "<div class='reqF_1'>Please enter PE Name.</div>"},
                        invitationRefNo: { required: "<div class='reqF_1'>Please enter Reference No.</div>"},
                        issuedate: { required: "<div class='reqF_1'>Please select Issue Date.</div>",date: "<div class='reqF_1'>Invalid Date Format</div>"},
                        packageNo: { required: "<div class='reqF_1'>Please enter Package No.</div>"},
                        packageName: { required: "<div class='reqF_1'>Please enter Package Name.</div>"},
                        tenderpublicationDate: { required: "<div class='reqF_1'>Please select Publication Date.</div>",date: "<div class='reqF_1'>Invalid Date Format</div>"},
                        tenderCloseDate: { required: "<div class='reqF_1'>Please select Tender Closing Date.</div>",date: "<div class='reqF_1'>Invalid Date Format</div>"},
                        tenderOpenDate: { required: "<div class='reqF_1'>Please select Tender Opening Date.</div>",date: "<div class='reqF_1'>Invalid Date Format</div>"},
                        tenderLastSellDate: { required: "<div class='reqF_1'>Please select Tender Last Selling Date.</div>",date: "<div class='reqF_1'>Invalid Date Format</div>"},
                        tenderDocPrice: { required: "<div class='reqF_1'>Please enter Tender Document Price.</div>", number: "<div class='reqF_1'>Incorrect Format</div>"},
                        LotNo: { required: "<div class='reqF_1'>Please enter Lot Number.</div>"},
                        DescriptionForPQ: { required: "<div class='reqF_1'>Please enter Lot Description.</div>"},
                        locationlot_0: { required: "<div class='reqF_1'>Please enter Lot Location.</div>"},
                        startLotNo_0: { required: "<div class='reqF_1'>Please select Staring Date.</div>"},
                        complTimeLotNo_0: { required: "<div class='reqF_1'>Please select Completion Date.</div>"},
                        nameOfficial: { required: "<div class='reqF_1'>Please enter the Name.</div>"},
                        designationOfficial: { required: "<div class='reqF_1'>Please enter Designation.</div>"},
                        eligibilityofTenderer:{ required: "<div class='reqF_1'>Please enter Eligibility of Tenderer.</div>"},
                        briefDescGoods:{ required: "<div class='reqF_1'>Please enter Description of Goods or Works.</div>"},
                        briefDescRelServices:{ required: "<div class='reqF_1'>Please enter Description of Related Services.</div>"},
                        receivingTenDoc :{ required: "<div class='reqF_1'>Please enter Receiving Tender.</div>"},
                        openTenderDoc :{ required: "<div class='reqF_1'>Please enter Opening Tender.</div>"},
                        sellingDocPrinciple:{ required: "<div class='reqF_1'>Please enter Selling Tender Document.</div>"},
                        identification_1:{required: "<div class='reqF_1'>Please enter Identification of Lot.</div>"},

                        //Dropdown
                        Ministry:{ selectNone: "<div class='reqF_1'>Please select Ministry.</div>"},
                        Organization:{ selectNone: "<div class='reqF_1'>Please select Organization.</div>"},
                        district:{ selectNone: "<div class='reqF_1'>Please select Dzongkhag / District.</div>"},
                        nature:{ selectNone: "<div class='reqF_1'>Please select Procurement Category.</div>"},
                        procType:{ selectNone: "<div class='reqF_1'>Please select Procurement Type.</div>"},
                        invitationFor:{ selectNone: "<div class='reqF_1'>Please select Lot Type.</div>"},
                        procureMethod:{ selectNone: "<div class='reqF_1'>Please select Procurement Method.</div>"},
                        budgetType:{ selectNone: "<div class='reqF_1'>Please select Budget Type.</div>"},
                        sourceFunds:{ selectNone: "<div class='reqF_1'>Please select Source of Fund.</div>"},
                        eventType:{ selectNone: "<div class='reqF_1'>Please select Event Type.</div>"}

                    }
                });

                ////The following code has been used to adding validation of dropdown list in basic validation plugin
                $.validator.addMethod('selectNone',
                    function (value, element)
                    {
                        if (value == 0 || value =='' || value =='select' || value.indexOf('--') != -1)
                        {
                            //alert("not ok");
                            return false;
                        }
                        else
                        {
                            //alert("ok")
                            return true;
                        }
                    }
                );
                //End Form Validation

            }); // End Document.Ready

            function getParam( name )
            {
             name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
             var regexS = "[\\?&]"+name+"=([^&#]*)";
             var regex = new RegExp( regexS );
             var results = regex.exec( window.location.href );
             if( results == null )
              return "";
            else
             return results[1];
            }
            
           function AddRow(lotType) {
                if (lotType == "Multiple Lot") {
                    var ptable = document.getElementById('tblLotDescription');
                    var lastElement = ptable.rows.length;
                    var index = lastElement;
                    var row = ptable.insertRow(lastElement);

                    var newTxt = "";
                    for(var i = 2; i<=5; i++){

                        newTxt += "<tr class='trExtra' id='tr" + (row + i) +"'>" +
                                "<td class='t-align-center'><input name='lotNo_" + (row + i) +"' class='formTxtBox_1' id='txtLotNo_" + (row + i) +"' type='text'/></td>" +
                                "<td class='t-align-center'><textarea class='formTxtBox_1' style='width: 280px;' name='identification_" + (row + i) +"' id='txtLotID_" + (row + i) +"' rows='2' cols='40'></textarea></td>" +
                                "<td class='t-align-center'><input name='locationlot_" + (row + i) +"' style='width: 180px;' class='formTxtBox_1' id='txtLocationlot_" + (row + i) +"' onblur='chkLocLotBlank(this);' type='text'></td>" +
                                "<td class='t-align-center'><input name='tenderSecurity_" + (row + i) +"'  class='formTxtBox_1 ff' id='txtTenderSecurity_" + (row + i) +"' onblur='chkAmountLotBlank(this);' type='text'/></td>"+
                                "<td class='t-align-center'><input name='complTimeLotNo_" + (row + i) +"'  class='formTxtBox_1' id='txtComplTime_" + (row + i) +"' style='width: 100px;' type='text'/></td></tr>"
                    
                    }

                    $("#tblLotDescription").append(newTxt);
                }
                else {
                    $('.trExtra').each(function () {
                        $(this).remove();
                    });
                }
            }

            function dataTable(selVal){
                //alert(' >> '+selVal);
                    if(selVal == "Single Lot"){
                       
                        //alert(document.getElementById("lot_1").style.display);
                        document.getElementById("lot_1").style.display = "none";
                        document.getElementById("lot_2").style.display = "none";
                        document.getElementById("lot_3").style.display = "none";
                    }else {
                        document.getElementById("lot_1").style.display = "table-row";
                        document.getElementById("lot_2").style.display = "table-row";
                        document.getElementById("lot_3").style.display = "table-row";
                        document.getElementById("txtLotNo_2").value ='';
                        document.getElementById("txtLotID_2").value = '';
                        document.getElementById("txtLocationlot_2").value = '';
                        document.getElementById("txtTenderSecurity_2").value = '';
                        document.getElementById("txtComplTime_2").value='';
                        document.getElementById("txtLotNo_3").value ='';
                        document.getElementById("txtLotID_3").value = '';
                        document.getElementById("txtLocationlot_3").value = '';
                        document.getElementById("txtTenderSecurity_3").value = '';
                        document.getElementById("txtComplTime_3").value='';
                        document.getElementById("txtLotNo_4").value ='';
                        document.getElementById("txtLotID_4").value = '';
                        document.getElementById("txtLocationlot_4").value = '';
                        document.getElementById("txtTenderSecurity_4").value = '';
                        document.getElementById("txtComplTime_4").value='';
                    }

            }

            function setOrgval(id){

                var orgObj =document.getElementById(id);
                var orgval = orgObj.options[orgObj.selectedIndex].text;
               
                document.getElementById("hidorg").value = orgval;
            }
            
          /* function Validation()
           {

               var flag = true;

               var ministry = document.getElementById("cmbMinistry").value;
               var organization = document.getElementById("cmbOrganization").value;
               var district = document.getElementById("cmbdistrict").value;
               var peName = document.getElementById("txtPEName").value;
               var procureNature = document.getElementById("cmbProcureNature").value;
               var procureType = document.getElementById("cmbProcureType").value;
               var eventType = document.getElementById("cmbEventType").value;
               var invFor = document.getElementById("cmbInvitationFor").value;
               var invRefNo = document.getElementById("txtinvitationRefNo").value;
               var issuedate = document.getElementById("txtissuedate").value;
               var pMethod = document.getElementById("cmbProcureMethod").value;

               var bType = document.getElementById("cmbBudgetType").value;
               var sFund = document.getElementById("cmbSourceFunds").value;
               var pkgNo = document.getElementById("txtPackageNo").value;
               var pkgName = document.getElementById("txtPackageName").value;
               //alert("ok");
               var pubDate = document.getElementById("txtTenderpublicationDate").value;
               var lastSellDate = document.getElementById("txtTenderLastSellDate").value;

               var closeDate = document.getElementById("txtpreQualCloseDate").value;
               var openDate = document.getElementById("txtpreQualOpenDate").value;
               var sellTenderDoc = document.getElementById("txtSellDocP").value;
               var rcvTenderDoc = document.getElementById("txtRcvTenderDoc").value;
               var openTenderDoc = document.getElementById("txtOpenTenderDoc").value;
               var eligibility = document.getElementById("txtEligibility").value;
               var goodsDes = document.getElementById("txtabriefDescGoods").value;
               var serviceDes = document.getElementById("txtbriefDesServices").value;
               var docPrice = document.getElementById("txtDocPrice").value;
               var tenderSecurity = document.getElementById("txtTenderSecurity").value;
               var officialName = document.getElementById("txtNameOfficial").value;
               var designation = document.getElementById("txtDesignation").value;




               document.getElementById("msgMinistry").innerHTML = "";
               document.getElementById("msgOrganisation").innerHTML = "";
               document.getElementById("msgDistrict").innerHTML = "";
               document.getElementById("msgPEName").innerHTML = "";
               document.getElementById("msgPNature").innerHTML = "";
               document.getElementById("msgProcureType").innerHTML = "";
               document.getElementById("msgEventType").innerHTML = "";
               document.getElementById("msgInvitationFor").innerHTML = "";
               document.getElementById("msgInvitationRefNo").innerHTML = "";
               document.getElementById("msgIssueDate").innerHTML = "";
               document.getElementById("msgProcureMethod").innerHTML = "";
               document.getElementById("msgBudgetType").innerHTML = "";
               document.getElementById("msgSourceOfFunds").innerHTML = "";
               document.getElementById("msgPackageNo").innerHTML = "";
               document.getElementById("msgPackageName").innerHTML = "";
               document.getElementById("msgPublicationDate").innerHTML = "";
               document.getElementById("msgLastSelDate").innerHTML = "";

               document.getElementById("msgClosingDate").innerHTML = "";
               document.getElementById("msgOpeningDate").innerHTML = "";
               document.getElementById("msgSellDocP").innerHTML = "";
               document.getElementById("msgRcvTenderDoc").innerHTML = "";
               document.getElementById("msgOpenTenderDoc").innerHTML = "";
               document.getElementById("msgEligibility").innerHTML = "";
               document.getElementById("msgGoodsDescription").innerHTML = "";
               document.getElementById("msgServiceDescription").innerHTML = "";
               document.getElementById("msgDocPrice").innerHTML = "";
               document.getElementById("msgTenderSecurity").innerHTML = "";
               document.getElementById("msgOfficialName").innerHTML = "";
               document.getElementById("msgOfficialDesignation").innerHTML = "";

               if (ministry == "") {
                   document.getElementById("msgMinistry").innerHTML = "Please select Ministry.";
                   flag = false;
               }
               if (ministry != "" && organization=="") {
                   document.getElementById("msgOrganisation").innerHTML = "Please select Organisation.";
                   flag = false;
               }
               if (district == "") {
                   document.getElementById("msgDistrict").innerHTML = "Please select Organisation.";
                   flag = false;
               }
               if (peName == "") {
                   document.getElementById("msgPEName").innerHTML = "Please enter PE name.";
                   flag = false;
               }
               if (procureNature == "") {
                   document.getElementById("msgPNature").innerHTML = "Please select procurement Category.";
                   flag = false;
               }
               if (procureType == "") {
                   document.getElementById("msgProcureType").innerHTML = "Please select procurement type.";
                   flag = false;
               }
               if (eventType == "") {
                   document.getElementById("msgEventType").innerHTML = "Please select event type.";
                   flag = false;
               }
               if (invFor == "") {
                   document.getElementById("msgInvitationFor").innerHTML = "Please select invitation for.";
                   flag = false;
               }
               if (invRefNo == "") {
                   document.getElementById("msgInvitationRefNo").innerHTML = "Please select invitation reference number.";
                   flag = false;
               }
               if (issuedate == "") {
                   document.getElementById("msgIssueDate").innerHTML = "Please select issue date.";
                   flag = false;
               }
               if (pMethod == "") {
                   document.getElementById("msgProcureMethod").innerHTML = "Please select procurement method.";
                   flag = false;
               }
               if (bType == "") {
                   document.getElementById("msgBudgetType").innerHTML = "Please select budget type.";
                   flag = false;
               }
               if (sFund == "") {
                   document.getElementById("msgSourceOfFunds").innerHTML = "Please select source of funds.";
                   flag = false;
               }
               if (pkgNo == "") {
                   document.getElementById("msgPackageNo").innerHTML = "Please enter package number.";
                   flag = false;
               }
               if (pkgName == "") {
                   document.getElementById("msgPackageName").innerHTML = "Please enter package name.";
                   flag = false;
               }
               if (pubDate == "") {
                   document.getElementById("msgPublicationDate").innerHTML = "Please select tender publication date.";
                   flag = false;
               }
               if (lastSellDate == "") {
                   document.getElementById("msgLastSelDate").innerHTML = "Please select tender last selling date.";
                   flag = false;
               }
               if (closeDate == "") {
                   document.getElementById("msgClosingDate").innerHTML = "Please enter closing date.";
                   flag = false;
               }
               if (openDate == "") {
                   document.getElementById("msgOpeningDate").innerHTML = "Please enter opening date.";
                   flag = false;
               }
               if (sellTenderDoc == "") {
                   document.getElementById("msgSellDocP").innerHTML = "Please enter selling tender document(principal).";
                   flag = false;
               }
               if (rcvTenderDoc == "") {
                   document.getElementById("msgRcvTenderDoc").innerHTML = "Please enter receiving tender document.";
                   flag = false;
               }
               if (openTenderDoc == "") {
                   document.getElementById("msgOpenTenderDoc").innerHTML = "Please enter opening tender document.";
                   flag = false;
               }
               if (eligibility == "") {
                   document.getElementById("msgEligibility").innerHTML = "Please enter tenderer eligibility.";
                   flag = false;
               }

                if (goodsDes == "") {
                   document.getElementById("msgGoodsDescription").innerHTML = "Please enter brief description of goods or works.";
                   flag = false;
               }

                if (serviceDes == "") {
                   document.getElementById("msgServiceDescription").innerHTML = "Please enter brief description related services.";
                   flag = false;
               }

                if (docPrice == "") {
                   document.getElementById("msgDocPrice").innerHTML = "Please enter tender document price.";
                   flag = false;
               }

                if (tenderSecurity == "") {
                   document.getElementById("msgTenderSecurity").innerHTML = "Please enter tender security.";
                   flag = false;
               }

                if (officialName == "") {
                   document.getElementById("msgOfficialName").innerHTML = "Please enter name of official inviting the tender.";
                   flag = false;
               }

                if (designation == "") {
                   document.getElementById("msgOfficialDesignation").innerHTML = "Please enter designation of official inviting the tender.";
                   flag = false;
               }



               return flag;

           }*/
       </script>

        <script type="text/javascript">
            var holiArray = new Array();
        </script>
      </head>
          <%
           
            OfflineDataSrBean offlineDataSrBean = new OfflineDataSrBean();
            String userid = "";
            HttpSession hs = request.getSession();
            if (hs.getAttribute("userId") != null) {
                userid = hs.getAttribute("userId").toString();
                offlineDataSrBean.setLogUserId(userid);
                 hs.setAttribute("formType", "TenderWithoutPQ");
            }
            System.out.println("submit val >> >> "+request.getParameter("submit"));
         if( request.getParameter("submit") != null && "Submit".equals(request.getParameter("submit"))){
            try{
                System.out.println("in condition ");
                String ministry = CommonUtils.checkNull(request.getParameter("hidministry"));
                String agency = CommonUtils.checkNull(request.getParameter("hidorg"));
                String peName = CommonUtils.checkNull(request.getParameter("peName"));
                String peCode = CommonUtils.checkNull(request.getParameter("peCode"));
                String district = CommonUtils.checkNull(request.getParameter("district"));
                String nature = CommonUtils.checkNull(request.getParameter("nature"));
                String procType = CommonUtils.checkNull(request.getParameter("procType"));
                String eventType = CommonUtils.checkNull(request.getParameter("eventType"));
                String invitationFor = CommonUtils.checkNull(request.getParameter("invitationFor"));
                String invitationRefNo = CommonUtils.checkNull(request.getParameter("invitationRefNo"));
                String issuedate = CommonUtils.checkNull(request.getParameter("issuedate"));
                String procureMethod = CommonUtils.checkNull(request.getParameter("procureMethod"));
                String budget = CommonUtils.checkNull(request.getParameter("budgetType"));
                String funds = CommonUtils.checkNull(request.getParameter("sourceFunds"));
                String devPartner = CommonUtils.checkNull(request.getParameter("devPartner"));
                String projectCode = CommonUtils.checkNull(request.getParameter("projectCode"));
                String ProjectName = CommonUtils.checkNull(request.getParameter("projectName"));
                String packageNo = CommonUtils.checkNull(request.getParameter("packageNo"));
                String packageName = CommonUtils.checkNull(request.getParameter("packageName"));
                String tenderpublicationDate = CommonUtils.checkNull(request.getParameter("tenderpublicationDate"));
                String tenderLastSellDate = CommonUtils.checkNull(request.getParameter("tenderLastSellDate"));
                String tenderCloseDate = CommonUtils.checkNull(request.getParameter("tenderCloseDate"));
                String tenderOpenDate = CommonUtils.checkNull(request.getParameter("tenderOpenDate"));
                String placeofPQMeeting = CommonUtils.checkNull(request.getParameter("placeofPQMeeting"));
                String preTenderMeetStartDate = CommonUtils.checkNull(request.getParameter("preTenderMeetStartDate"));
                String sellingDocPrinciple = CommonUtils.checkNull(request.getParameter("sellingDocPrinciple"));
                String sellingDocOthers = CommonUtils.checkNull(request.getParameter("sellingDocOthers"));
                String receivingTenDoc = CommonUtils.checkNull(request.getParameter("receivingTenDoc"));
                String openTenderDoc = CommonUtils.checkNull(request.getParameter("openTenderDoc"));
                String eligibilityofTenderer = CommonUtils.checkNull(request.getParameter("eligibilityofTenderer"));
                String briefDescGoods = CommonUtils.checkNull(request.getParameter("briefDescGoods"));
                String briefDescRelServices = CommonUtils.checkNull(request.getParameter("briefDescRelServices"));
                String tenderDocPrice = CommonUtils.checkNull(request.getParameter("tenderDocPrice"));
                String lotNo_1 = CommonUtils.checkNull(request.getParameter("lotNo_1"));
                String identification_1 = CommonUtils.checkNull(request.getParameter("identification_1"));
                String locationlot_1 = CommonUtils.checkNull(request.getParameter("locationlot_1"));
                String tenderSecurity_1 = CommonUtils.checkNull(request.getParameter("tenderSecurity_1"));
                String complTimeLotNo_1 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_1"));
                String lotNo_2 = CommonUtils.checkNull(request.getParameter("lotNo_2"));
                String identification_2 = CommonUtils.checkNull(request.getParameter("identification_2"));
                String locationlot_2 = CommonUtils.checkNull(request.getParameter("locationlot_2"));
                String tenderSecurity_2 = CommonUtils.checkNull(request.getParameter("tenderSecurity_2"));
                String complTimeLotNo_2 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_2"));
                String lotNo_3 = CommonUtils.checkNull(request.getParameter("lotNo_3"));
                String identification_3 = CommonUtils.checkNull(request.getParameter("identification_3"));
                String locationlot_3 = CommonUtils.checkNull(request.getParameter("locationlot_3"));
                String tenderSecurity_3 = CommonUtils.checkNull(request.getParameter("tenderSecurity_3"));
                String complTimeLotNo_3 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_3"));
                String lotNo_4 = CommonUtils.checkNull(request.getParameter("lotNo_4"));
                String identification_4 = CommonUtils.checkNull(request.getParameter("identification_4"));
                String locationlot_4 = CommonUtils.checkNull(request.getParameter("locationlot_4"));
                String tenderSecurity_4 = CommonUtils.checkNull(request.getParameter("tenderSecurity_4"));
                String complTimeLotNo_4 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_4"));
                String nameOfficial = CommonUtils.checkNull(request.getParameter("nameOfficial"));
                String designationOfficial = CommonUtils.checkNull(request.getParameter("designationOfficial"));
                String addressOfOfficial = CommonUtils.checkNull(request.getParameter("addressOfOfficial"));
                String contactDetail = CommonUtils.checkNull(request.getParameter("contactDetail"));

            TblTenderDetailsOffline tenderDetailsOffline = new TblTenderDetailsOffline();
            List<TblTenderLotPhasingOffline> lots = new ArrayList<TblTenderLotPhasingOffline>();
            
            //tenderDetailsOffline = new TblTenderDetailsOffline();

            tenderDetailsOffline.setMinistryOrDivision(ministry);
            tenderDetailsOffline.setAgency(agency);
            tenderDetailsOffline.setPeName(peName);
            tenderDetailsOffline.setPeCode(peCode);
            tenderDetailsOffline.setPeDistrict(district);
            tenderDetailsOffline.setProcurementNature(nature);
            tenderDetailsOffline.setProcurementType(procType);
            tenderDetailsOffline.setInvitationFor(invitationFor);
            tenderDetailsOffline.setReoiRfpRefNo(invitationRefNo);
           // System.out.println("issuedate >> "+issuedate);
            Date issDate = DateUtils.formatStdString(issuedate);
            tenderDetailsOffline.setIssueDate(issDate);
            tenderDetailsOffline.setProcurementMethod(procureMethod);
            tenderDetailsOffline.setBudgetType(budget);
            tenderDetailsOffline.setSourceOfFund(funds);
            tenderDetailsOffline.setDevPartners(devPartner);
            tenderDetailsOffline.setProjectCode(projectCode);
            tenderDetailsOffline.setProjectName(ProjectName);
            tenderDetailsOffline.setPackageNo(packageNo);
            tenderDetailsOffline.setPackageName(packageName);
            tenderDetailsOffline.setTenderStatus("Pending");
           // System.out.println("tenderpublicationDate >> "+tenderpublicationDate);
            if(!"".equals(tenderpublicationDate)){
                Date pubDate = DateUtils.formatStdString(tenderpublicationDate);
                tenderDetailsOffline.setTenderPubDate(pubDate);
            }

            if(!"".equals(tenderCloseDate)){
                Date closeDate = DateUtils.convertDateToStr(tenderCloseDate);
                tenderDetailsOffline.setClosingDate(closeDate);
            }
             if(!"".equals(tenderLastSellDate)){
                 Date lastSellingDate = DateUtils.formatStdString(tenderLastSellDate);
                 tenderDetailsOffline.setLastSellingDate(lastSellingDate);
              }

             if(!"".equals(tenderOpenDate)){
                Date tenderOpeningDate = DateUtils.convertDateToStr(tenderOpenDate);
                tenderDetailsOffline.setOpeningDate(tenderOpeningDate);
              }
            
            tenderDetailsOffline.setPreTenderReoiplace(placeofPQMeeting);
            Date meetingDate = DateUtils.convertDateToStr(preTenderMeetStartDate);
            tenderDetailsOffline.setPreTenderReoidate(meetingDate);
            tenderDetailsOffline.setSellingAddPrinciple(sellingDocPrinciple);
            tenderDetailsOffline.setSellingAddOthers(sellingDocOthers);
            tenderDetailsOffline.setReceivingAdd(receivingTenDoc);
            tenderDetailsOffline.setOpeningAdd(openTenderDoc);
            tenderDetailsOffline.setEligibilityCriteria(eligibilityofTenderer);
            tenderDetailsOffline.setBriefDescription(briefDescGoods);
            tenderDetailsOffline.setRelServicesOrDeliverables(briefDescRelServices);
            tenderDetailsOffline.setEventType(eventType);
            tenderDetailsOffline.setUserId(Integer.parseInt(userid));
            float docPrice = 0;
            if (tenderDocPrice != null && !"".equals(tenderDocPrice)) {
                docPrice = Float.parseFloat(tenderDocPrice);
            }
            tenderDetailsOffline.setDocumentPrice(new BigDecimal(docPrice).setScale(2, 0));
            tenderDetailsOffline.setPeOfficeName(nameOfficial);
            tenderDetailsOffline.setPeDesignation(designationOfficial);
            tenderDetailsOffline.setPeAddress(addressOfOfficial);
            tenderDetailsOffline.setPeContactDetails(contactDetail);
           // List<TblTenderLotPhasingOffline> lots = new ArrayList<TblTenderLotPhasingOffline>();
           // System.out.println("invitationFor >> "+invitationFor);
            if(invitationFor.equals("Single Lot")){
                System.out.println("single >> ");
                TblTenderLotPhasingOffline lotPhasingOffline = new TblTenderLotPhasingOffline();
                lotPhasingOffline.setLotOrRefNo(lotNo_1);
                lotPhasingOffline.setLocation(locationlot_1);
                lotPhasingOffline.setLotIdentOrPhasingServ(identification_1);
                float tenSecurityamt = 0;
                if (tenderSecurity_1 != null && !"".equals(tenderSecurity_1)) {
                    tenSecurityamt = Float.parseFloat(tenderSecurity_1);
                }

                lotPhasingOffline.setTenderSecurityAmt(new BigDecimal(tenSecurityamt).setScale(2, 0));
                lotPhasingOffline.setCompletionDateTime(complTimeLotNo_1);
                lotPhasingOffline.setTenderDetailsOffline(tenderDetailsOffline);
                lots.add(lotPhasingOffline);
            }else if(invitationFor.equals("Multiple Lot")){
                System.out.println("multi lot ");
                for(int i=1;i<=4;i++){

                if((request.getParameter("lotNo_"+i) != null || request.getParameter("identification_"+i) != null
                        || request.getParameter("locationlot_"+i) != null || request.getParameter("complTimeLotNo_"+i) != null || request.getParameter("tenderSecurity_"+i) != null)
                        && (!"".equals(request.getParameter("lotNo_"+i)) || !"".equals(request.getParameter("identification_"+i)) || !"".equals(request.getParameter("locationlot_"+i))
                        || !"".equals(request.getParameter("complTimeLotNo_"+i))  || !"".equals(request.getParameter("tenderSecurity_"+i)) ) ){

                        TblTenderLotPhasingOffline lotPhasingOffline = new TblTenderLotPhasingOffline();
                        lotPhasingOffline.setLotOrRefNo(request.getParameter("lotNo_"+i));
                        lotPhasingOffline.setLocation(request.getParameter("locationlot_"+i));
                        lotPhasingOffline.setLotIdentOrPhasingServ(request.getParameter("identification_"+i));
                        lotPhasingOffline.setCompletionDateTime(request.getParameter("complTimeLotNo_"+i));
                        
                         float tenSecurityamt = 0;
                        if (request.getParameter("tenderSecurity_"+i) != null && !"".equals(request.getParameter("tenderSecurity_"+i))) {
                            tenSecurityamt = Float.parseFloat(request.getParameter("tenderSecurity_"+i));
                        }
                        lotPhasingOffline.setTenderSecurityAmt(new BigDecimal(tenSecurityamt).setScale(2, 0));
                        lotPhasingOffline.setTenderDetailsOffline(tenderDetailsOffline);
                        lots.add(lotPhasingOffline);
                }

               }
 
            }
            tenderDetailsOffline.setTenderLotsAndPhases(lots);
            System.out.println("before create ");

            offlineDataSrBean.createOfflineData(tenderDetailsOffline);
            %>
                <script type="text/javascript">
                  alert("Information Saved Successfully");
                </script>
            <%
            
            }catch(Exception ex){
                ex.printStackTrace();
            }
            }

            //else
            {
               

        %>

        <% TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        List<SPTenderCommonData> holidayList = tenderCommonService.returndata("getHolidayDatesBD", null, null);
        out.print("<script type='text/javascript'>");
        for(SPTenderCommonData holidays : holidayList){
            out.print("holiArray.push('"+holidays.getFieldName1()+"');");
        }
        out.print("</script>");
        %>



<body onload="documentAvailable();" >
      
         <input id="boolcheck" value="true" type="hidden"/>
        <div class="mainDiv">
            <div class="fixDiv">

         <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->

                <div class="contentArea_1">                
                                       
                    <div class="t-align-right" style="vertical-align: middle;"><a class="action-button-goback" href="<%=request.getContextPath()%>/offlinedata/TenderDashboardOfflineApproval.jsp">Go back</a></div>
                    <form  id="frmUploadOfflineDate" name="frmUploadOfflineDate" method="POST" enctype="multipart/form-data" action="<%=request.getContextPath()%>/ExcelUploadAndParseServlet" >
                    <div class="pageHead_1">Create Tender without Pre-qualification (PQ)</div>
                    <div class="tableHead_22 t_space">FILE UPLOAD</div>
                    <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                        <tbody>
                        <tr>
                            <td class="ff" width="25%">Select File:</td>
                            <td width="25%">
                                <input name="fileUploadControl" id="fileUploadControl" type="file"/>
                            </td>
                            <td class="ff" width="25%"></td>
                            <td width="25%"></td>
                        </tr>

                        <tr>
                            <td class="ff" width="25%"></td>
                            <td width="25%">
                                <!--<button type="button" value="Upload">Upload</button>-->
                                <label class="formBtn_1">
                                    <input type="submit" name="upload" value="Upload"/>
                                </label>
                            </td>
                            <td class="ff" width="25%"></td>
                            <td width="25%"></td>
                        </tr>
                        </tbody>
                    </table>
                    </form>                    

                    <div class="tableHead_22 t_space">PROCURING ENTITY (PE) INFORMATION</div>
                    <form id="frmCreateTender" name="frmCreateTender" method="POST" action="/offlinedata/CreateTenderWithoutPQ.jsp">
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">

                            <tbody><tr>
                                <td style="font-style: italic" colspan="4" class="ff t-align-left" align="left">

                                    Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Ministry/Division :<span>&nbsp;*</span></td>
                                <%
                                 //   ManageEmployeeGridSrBean manageEmployeeGridSrBean = new ManageEmployeeGridSrBean();
                                 //   List<TblDepartmentMaster> departmentMasterList = null;
                                 //   departmentMasterList = manageEmployeeGridSrBean.getDepartmentMasterList("Ministry");
                                    
                                TenderDashboardOfflineSrBean tenderOffline = new TenderDashboardOfflineSrBean();
                                List<Object[]> ministryListOffline = new ArrayList<Object[]>();
                                ministryListOffline = tenderOffline.getMinistryForTenderOffline();

                                %>
                                <td width="25%"><select name="Ministry" class="formTxtBox_1" id="cmbMinistry" style="width: 200px;" onchange="loadOrganization();">
                                                <option value="" selected="selected">--- Please Select ---</option>
                                                <%
                                                      /*  for (int i = 0; i < departmentMasterList.size(); i++)  {
                                                              out.println("<option value='" + departmentMasterList.get(i).getDepartmentId() + "'>" + departmentMasterList.get(i).getDepartmentName() + "</option>");
                                                        }*/
                                                    for (int i = 0; i < ministryListOffline.size(); i++)  {
                                                          out.println("<option value='" + String.valueOf(ministryListOffline.get(i)) + "'>" + String.valueOf(ministryListOffline.get(i)) + "</option>");
                                                    }
                                                
                                               %>
                                     </select>
                                     <br />
                                     <span style="color: red;" id="msgMinistry"></span>
                                    <input id="hidministry"  name="hidministry" type="hidden"/>
                                </td>
                               <!-- <td class="ff" width="25%">Division :</td>

                                <td width="25%"><select name="Division" class="formTxtBox_1" id="cmbDivision" style="width: 200px;">
                                                <option value="" selected="selected">--- Please Select ---</option>

                                     </select></td> -->
                               <td class="ff" width="25%"></td>
                               <td width="25%"></td>
                            </tr>
                            <tr>
                                <td class="ff">Organization :<span>&nbsp;*</span></td>
                                <td><select name="Organization" class="formTxtBox_1" id="cmbOrganization" style="width: 200px;" onchange="setOrgval(this.id);"  >
                                                <option value="" selected="selected">--- Please Select ---</option>

                                     </select>
                                     <input type="hidden" name="hidorg"  id="hidorg"  />
                                     <br />
                                     <span style="color: red;" id="msgOrganisation"></span>
                                </td>
                                <td class="ff">Procuring Entity Dzongkhag / District :<span>&nbsp;*</span></td>
                                <%
                                                            CommonService cservice = (CommonService) AppContext.getSpringBean("CommonService");
                                                            //Code by Proshanto
                                                            short countryId = 150;//136
                                                            List<TblStateMaster> liststate = cservice.getState(countryId);
                                %>
                               <td><select name="district" class="formTxtBox_1" id="cmbdistrict" style="width: 200px;">
                                                <option value="" selected="selected">--- Please Select ---</option>
                                               <%
                                                        for (TblStateMaster state : liststate) {
                                                              out.println("<option value='" + state.getStateName() + "'>" + state.getStateName() + "</option>");
                                                        }
                                               %>
                                     </select>
                                     <br />
                                            <span style="color: red;" id="msgDistrict"></span>
                               </td>
                            </tr>
                            <tr>
                                <td class="ff">Procuring Entity Name :<span>&nbsp;*</span></td>
                                <td><input name="peName" class="formTxtBox_1" id="txtPEName" style="width: 280px;" type="text"/>
                                    <br />
                                            <span style="color: red;" id="msgPEName"></span>
                                </td>

                                <td class="ff">Procuring Entity Code :</td>
                                <td><input name="peCode" class="formTxtBox_1" id="txtPECode" style="width: 280px;" type="text"/></td>

                            </tr>
                            <tr>
                                <td class="ff">Procurement Category :<span>&nbsp;*</span></td>
                                <td><select name="nature" class="formTxtBox_1" id="cmbProcureNature" style="width: 200px;" onChange="setSerType(this,true);">
                                                <option value="" selected="selected">---Select Procurement Category---</option>
                                                <option value="Goods" >Goods</option>
                                                <option value="Works" >Works</option>

                                    </select>
                                    <br />
                                            <span style="color: red;" id="msgPNature"></span>
                                </td>
                                <td class="ff">Procurement Type :<span>*&nbsp;</span></td>
                                <td><select name="procType" class="formTxtBox_1" id="cmbProcureType" style="width: 200px;">
                                                <option value="" selected="selected" >--- Please Select ---</option>
                                                <option value="NCT">NCB</option>
                                                <option value="ICT">ICB</option>
                                     </select>
                                     <br />
                                     <span style="color: red;" id="msgProcureType"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Event Type :<span>&nbsp;*</span></td>
                                <td>
                                    <select name="eventType" class="formTxtBox_1" id="cmbEventType" style="width: 200px;">
                                                <option value="" selected="selected" >--- Please Select ---</option>
                                                <option value="Tender">Tender</option>
                                                <option value="Re-Tender">Re-Tender</option>

                                     </select>
                                     <br />
                                     <span style="color: red;" id="msgEventType"></span>
                                </td>
                                 <td class="ff">Invitation for :<span>&nbsp;*</span></td>
                                <td>
                                     <select name="invitationFor" class="formTxtBox_1" id="cmbInvitationFor" style="width: 200px;" onchange="dataTable(this.value)">
                                        <option value="" selected="selected">--- Please Select ---</option>
                                        <option value="Single Lot">Single Lot</option>
                                        <option value="Multiple Lot">Multiple Lot</option>
                                    </select>
                                    <br />
                                    <span style="color: red;" id="msgInvitationFor"></span>
                                    </td>
                            </tr>
                            <tr>
                                <td class="ff">Invitation Reference No. : <span>&nbsp;*</span></td>
                                <td><input name="invitationRefNo" class="formTxtBox_1" id="txtinvitationRefNo" style="width: 280px;" type="text"/>
                                    <br />
                                    <span style="color: red;" id="msgInvitationRefNo"></span>
                                    <input id="hdnmsgTender" name="hdnmsgTenderName" value="Tender" type="hidden"/>
                                </td>
                                <td class="ff">Date : <span>&nbsp;*</span></td>
                                <td class="formStyle_1"><input name="issuedate" class="formTxtBox_1" id="txtissuedate" style="width: 100px;" onfocus="GetCalWithouTime('txtissuedate','txtissuedate');" onblur="findHoliday(this,0);" type="text"/>
                                    <img id="imgtxtissuedate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtissuedate','imgtxtissuedate');" border="0"/>
                                    <br />
                                    <span style="color: red;" id="msgIssueDate"></span>
                                    <span id="span10"></span>
                                </td>
                            </tr>
                        </tbody></table>

                                <div class="tableHead_22 ">Key Information and Funding Information :</div>
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td class="ff" width="25%">Procurement Method : <span>*</span></td>
                               <td width="25%"><select name="procureMethod" class="formTxtBox_1" id="cmbProcureMethod" style="width: 200px;">
                                                <option value="">---Select Procurement Method---</option>
                                                <option value="2">Open Tendering Method</option>
                                                <option value="5">Two Stage Tendering Method</option>
                                     </select>
                                     <input type="hidden" name="hdnPM" id="hdnPM" value=""/>
                                            <span class="reqF_2" id="msgProcureMethod1"></span>
                                    <br />
                                    <span style="color: red;" id="msgProcureMethod"></span>

                                </td>
                                <td class="ff" width="25%">Budget Type :<span>&nbsp;*</span></td>
                                 <td width="25%"><select name="budgetType" class="formTxtBox_1" id="cmbBudgetType" style="width: 200px;">
                                                <option value="" selected="selected">---Select Budget Type ---</option>
                                                <option value="Development Budget">Development Budget</option>
                                                <option value=">Revenue Budget">Revenue Budget</option>
                                                <option value="Own Fund">Own Fund</option>

                                     </select>
                                    <br />
                                    <span style="color: red;" id="msgBudgetType"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Source of Funds :<span>&nbsp;*</span></td>
                               <td width="25%"><select name="sourceFunds" class="formTxtBox_1" id="cmbSourceFunds" style="width: 200px;">
                                                <option value="" selected="selected">--- Please Select ---</option>
                                                <option value="Government" >Government</option>
                                                <option value="Aid or Grant" >Aid or Grant / Credit</option>
                                                <option value="Own fund">Own fund</option>
                                     </select>
                                    <br />
                                    <span style="color: red;" id="msgSourceOfFunds"></span>
                               </td>
                                <td class="ff" width="25%">Development Partner : </td>

                                <td width="25%">
                                    <input id="txtDevPartner" class="formTxtBox_1" type="text" style="width: 280px;" name="devPartner"  />
                                </td>
                            </tr>


                        </tbody></table>
                        <div class="tableHead_22 ">Particular Information :</div>
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td class="ff" width="25%">Project Code : </td>
                                <td width="25%"><input name="projectCode" class="formTxtBox_1" id="txtProjectCode" style="width: 280px;" type="text"/></td>
                                <td class="ff" width="25%">Project Name : </td>
                                 <td width="25%"><input name="projectName" class="formTxtBox_1" id="txtProjectName" style="width: 280px;" type="text"/></td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Tender Package No. :<span>&nbsp;*</span></td>
                                <td>
                                    <input name="packageNo" class="formTxtBox_1" id="txtPackageNo" style="width: 280px;" type="text"/>
                                    <br />
                                    <span style="color: red;" id="msgPackageNo"></span>
                                </td>
                                <td class="ff" width="25%">Tender Package Name :<span>&nbsp;*</span></td>
                                <td>
                                    <input name="packageName" class="formTxtBox_1" id="txtPackageName" style="width: 280px;" type="text"/>
                                    <br />
                                    <span style="color: red;" id="msgPackageName"></span>
                                </td>
                            </tr>
                            <tr>

                                <td class="ff">Tender Publication Date : <span>&nbsp;*</span></td>
                                <td class="formStyle_1"><input name="tenderpublicationDate" class="formTxtBox_1" id="txtTenderpublicationDate" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtTenderpublicationDate','txtTenderpublicationDate');" onblur="findHoliday(this,0);" type="text"/>
                                    <img id="txttenderpublicationDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtTenderpublicationDate','txttenderpublicationDateimg');" border="0"/>
                                    <br />
                                    <span style="color: red;" id="msgPublicationDate"></span>

                                </td>

                                <td class="ff">Tender Last Selling Date : <span>&nbsp;*</span></td>
                                <td class="formStyle_1"><input name="tenderLastSellDate" class="formTxtBox_1" id="txtTenderLastSellDate" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtTenderLastSellDate','txtTenderLastSellDate');" onblur="findHoliday(this,1);" type="text"/>
                                    <img id="txttenderLastSellDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtTenderLastSellDate','txttenderLastSellDateimg');" border="0"/>
                                    <br />
                                    <span style="color: red;" id="msgLastSelDate"></span>

                                </td>


                            </tr>

                            <tr>
                                <td class="ff">Tender Closing Date and Time : <span>&nbsp;*</span></td>
                                <td class="formStyle_1"><input name="tenderCloseDate" class="formTxtBox_1" id="txtpreQualCloseDate" style="width: 100px;" readonly="true" onfocus="GetCal('txtpreQualCloseDate','txtpreQualCloseDate');" onblur="findHoliday(this,4);" type="text"/>
                                    <img id="txtpreQualCloseDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCal('txtpreQualCloseDate','txtpreQualCloseDateimg');" border="0"/>
                                    <br />
                                    <span style="color: red;" id="msgClosingDate"></span>

                                </td>
                                <td class="ff">Tender Opening Date and Time : <span>&nbsp;*</span></td>
                                <td class="formStyle_1"><input name="tenderOpenDate" class="formTxtBox_1" id="txtpreQualOpenDate" style="width: 100px;" readonly="true" onfocus="GetCal('txtpreQualOpenDate','txtpreQualOpenDate');" onblur="findHoliday(this,5);" type="text"/>
                                    <img id="txtpreQualOpenDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCal('txtpreQualOpenDate','txtpreQualOpenDateimg');" border="0"/>
                                    <br />
                                    <span style="color: red;" id="msgOpeningDate"></span>

                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Selling Tender Document(Principal) :<span>&nbsp;*</span></td>
                                <td>
                                    <input name="sellingDocPrinciple" class="formTxtBox_1" id="txtSellDocP" style="width: 280px;" type="text"/>
                                    <br />
                                    <span style="color: red;" id="msgSellDocP"></span>
                                </td>
                                <td class="ff" width="25%">Opening Tender Document:<span>&nbsp;*</span></td>
                                <td>
                                    <input name="openTenderDoc" class="formTxtBox_1" id="txtOpenTenderDoc" style="width: 280px;" type="text"/>
                                    <br />
                                    <span style="color: red;" id="msgOpenTenderDoc"></span>
                                </td>
                                
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Receiving Tender Document :<span>&nbsp;*</span></td>
                                <td>
                                   <textarea class="formTxtBox_1" style="width: 280px;" name="receivingTenDoc" rows="2" cols="49" id="txtRcvTenderDoc"></textarea>

                                      <br />
                                    <span style="color: red;" id="msgRcvTenderDoc"></span>
                                </td>
                              <td class="ff" width="25%">Selling Tender Document(Others):</td>
                                <td>
                                   <textarea class="formTxtBox_1" style="width: 280px;" name="sellingDocOthers" rows="2" cols="49" id="txtSellDocOther"></textarea>
                                </td>
                            </tr>
                            <tr>
                                    <td class="ff">Place of Pre-Tender Meeting :</td>
                                <td class="formStyle_1">
                                    <textarea class="formTxtBox_1" style="width: 280px;" name="placeofPQMeeting" rows="2" cols="49" id="txtPlaceofPQMeeting"></textarea>
                                </td>
                                    <td class="ff">Pre-Tender Meeting Date and Time : <span></span>
                                    <input id="hdncheck" value="Yes" type="hidden"/>
                                </td>
                                <td class="formStyle_1"><input name="preTenderMeetStartDate" class="formTxtBox_1" id="txtpreTenderMeetStartDate" style="width: 100px;" readonly="true" onfocus="GetCal('txtpreTenderMeetStartDate','txtpreTenderMeetStartDate');" onblur="findHoliday(this,2);" type="text"/>
                                    <img id="txtpreTenderMeetStartDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCal('txtpreTenderMeetStartDate','txtpreTenderMeetStartDateimg');" border="0"/>
                                </td>
                            </tr>

                        </tbody></table>

                        <div class="tableHead_22 ">Information for Bidder/Consultant :</div>

                        <table class="formStyle_1 " width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td class="ff" ><input id="hdnevenyType" value="Works" type="hidden"/>Eligibility of Bidder/Consultant : <span>&nbsp;*</span></td>
                                <td><textarea class="formTxtBox_1" style="width: 280px;" style="display: none;" cols="100" rows="5" id="txtEligibility" name="eligibilityofTenderer" class="formTxtBox_1"></textarea>
                                <script type="text/javascript">
                                    //<![CDATA[
                                    CKEDITOR.replace( 'eligibilityofTenderer',
                                    {
                                        toolbar : "egpToolbar"

                                    });
                                    //]]>
                                </script>
                                    <br />
                                    <span style="color: red;" id="msgEligibility"></span>
                                </td>

                            </tr>
                            <tr>
                                <td class="ff">Brief Description of Goods or Works : <span>&nbsp;*</span></td>
                                <td><input value="Works" id="briefValMsg" type="hidden"/>
                                    <textarea class="formTxtBox_1" style="width: 280px;" style="display: none;" cols="100" rows="5" id="txtabriefDescGoods" name="briefDescGoods" class="formTxtBox_1"></textarea>
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    CKEDITOR.replace( 'briefDescGoods',
                                    {
                                        toolbar : "egpToolbar"

                                    });
                                    //]]>
                                    </script>
                                    <br />
                                    <span style="color: red;" id="msgGoodsDescription"></span>
                                </td><td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="ff">Brief Description of Services : <span>&nbsp;*</span></td>
                                <td><input value="Works" id="briefValMsg" type="hidden"/>
                                    <textarea class="formTxtBox_1" style="width: 280px;" style="display: none;" cols="100" rows="5" id="txtbriefDesServices" name="briefDescRelServices" class="formTxtBox_1"></textarea>
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    CKEDITOR.replace( 'briefDescRelServices',
                                    {
                                        toolbar : "egpToolbar"

                                    });
                                    //]]>
                                    </script>
                                    <br />
                                    <span style="color: red;" id="msgServiceDescription"></span>
                                </td><td>&nbsp;</td>
                            </tr>

                            <tr style="display: table-row;" id="docsprice">
                                    <td class="ff">Tender Document Price (In Nu.)<span>&nbsp;*</span></td>
                                <td><input name="tenderDocPrice" onblur="documentPrice(this);" class="formTxtBox_1" id="txtDocPrice" style="width: 200px;" type="text"/><br />

                                    <br />
                                    <span style="color: red;" id="msgDocPrice"></span>
                                </td>
                                <td>&nbsp;</td>
                            </tr>

                        </tbody></table>


                        <table class="tableList_1 t_space" width="100%" cellspacing="0" id="tblLotDescription">
                            <tbody><tr>
                                <th class="t-align-left" width="6%">Lot No.</th>

                                <th class="t-align-left" width="44%">Identification of Lot <span style="color:red">*</span></th>

                                <th class="t-align-center" width="10%">Location </th>

                                <th class="t-align-center" width="10%">Tender Security (Amount in Nu.)</th>

                                <th class="t-align-center" width="10%">Completion Time in Weeks/Months</th>
                            </tr>

                            <tr>
                                <td class="t-align-center"><input name="lotNo_1"  class="formTxtBox_1" id="txtLotNo_1" onblur="chkRefNoBlank(this);" style="width: 95%;" type="text"/> </td>
                                <td class="t-align-center">
                                    <textarea class="formTxtBox_1" style="width: 280px;" name="identification_1" rows="2" cols="40" id="txtLotID_1" onblur="chkRefNoBlank(this);"></textarea>
                                </td>
                                <td class="t-align-center"><input name="locationlot_1"  style="width: 180px;" class="formTxtBox_1" id="txtLocationlot_1" onblur="chkLocLotBlank(this);" type="text"/><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center"><input name="tenderSecurity_1"  class="formTxtBox_1 ff" id="txtTenderSecurity_1" onblur="chkAmountLotBlank(this);" type="text"/><br></br><span style="color: red;" id="msgTenderSecurity"></span><span id="amountLot_0" style="color: red;"></span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_1" class="formTxtBox_1" id="txtComplTime_1" style="width: 100px;" onfocus="GetCalWithouTime('complTimeLotNo_0','complTimeLotNo_0');" onblur="chkCompTimeLotBlank(this);findHoliday(this,8);" type="text"/><span id="compLot_0" style="color: red;">&nbsp;</span>
                                </td>
                            </tr>
                               <tr id="lot_1" style="display:none;" >
                                <td class="t-align-center"><input name="lotNo_2"  class="formTxtBox_1" id="txtLotNo_2" onblur="chkRefNoBlank(this);" style="width: 95%;" type="text"/> </td>
                                <td class="t-align-center">
                                    <textarea class="formTxtBox_1" style="width: 280px;" name="identification_2" rows="2" cols="40" id="txtLotID_2" onblur="chkRefNoBlank(this);"></textarea>
                                </td>
                                <td class="t-align-center"><input name="locationlot_2"  style="width: 180px;" class="formTxtBox_1" id="txtLocationlot_2" onblur="chkLocLotBlank(this);" type="text"/><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center"><input name="tenderSecurity_2"   class="formTxtBox_1 ff" id="txtTenderSecurity_2" onblur="chkAmountLotBlank(this);" type="text"/><br></br><span style="color: red;" id="msgTenderSecurity"></span><span id="amountLot_0" style="color: red;"></span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_2" class="formTxtBox_1" id="txtComplTime_2" style="width: 100px;" onfocus="GetCalWithouTime('complTimeLotNo_0','complTimeLotNo_0');" onblur="chkCompTimeLotBlank(this);findHoliday(this,8);" type="text"/><span id="compLot_0" style="color: red;">&nbsp;</span>
                                </td>
                            </tr>
                             <tr id="lot_2" style="display:none;" >
                                <td class="t-align-center"><input name="lotNo_3"  class="formTxtBox_1" id="txtLotNo_3" onblur="chkRefNoBlank(this);" style="width: 95%;" type="text"/> </td>
                                <td class="t-align-center">
                                    <textarea class="formTxtBox_1" style="width: 280px;" name="identification_3" rows="2" cols="40" id="txtLotID_3" onblur="chkRefNoBlank(this);"></textarea>
                                </td>
                                <td class="t-align-center"><input name="locationlot_3"  style="width: 180px;" class="formTxtBox_1" id="txtLocationlot_3" onblur="chkLocLotBlank(this);" type="text"/><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center"><input name="tenderSecurity_3"   class="formTxtBox_1 ff" id="txtTenderSecurity_3" onblur="chkAmountLotBlank(this);" type="text"/><br></br><span style="color: red;" id="msgTenderSecurity"></span><span id="amountLot_0" style="color: red;"></span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_3"  class="formTxtBox_1" id="txtComplTime_3" style="width: 100px;" onfocus="GetCalWithouTime('complTimeLotNo_0','complTimeLotNo_0');" onblur="chkCompTimeLotBlank(this);findHoliday(this,8);" type="text"/><span id="compLot_0" style="color: red;">&nbsp;</span>
                                </td>
                            </tr>
                            <tr id="lot_3" style="display:none;" >
                               <td class="t-align-center"><input name="lotNo_4"  class="formTxtBox_1" id="txtLotNo_4" onblur="chkRefNoBlank(this);" style="width: 95%;" type="text"/> </td>
                                <td class="t-align-center">
                                    <textarea class="formTxtBox_1" style="width: 280px;" name="identification_4" rows="2" cols="40" id="txtLotID_4" onblur="chkRefNoBlank(this);"></textarea>
                                </td>
                                <td class="t-align-center"><input name="locationlot_4"  style="width: 180px;" class="formTxtBox_1" id="txtLocationlot_4" onblur="chkLocLotBlank(this);" type="text"/><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center"><input name="tenderSecurity_4"   class="formTxtBox_1 ff" id="txtTenderSecurity_4" onblur="chkAmountLotBlank(this);" type="text"/><br></br><span style="color: red;" id="msgTenderSecurity"></span><span id="amountLot_0" style="color: red;"></span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_4"  class="formTxtBox_1" id="txtComplTime_4" style="width: 100px;" onfocus="GetCalWithouTime('complTimeLotNo_0','complTimeLotNo_0');" onblur="chkCompTimeLotBlank(this);findHoliday(this,8);" type="text"/><span id="compLot_0" style="color: red;">&nbsp;</span>
                                </td>
                            </tr>

                        </tbody></table>


                        <div class="tableHead_22 t_space">Procuring Entity Details :</div>
                        <table class="formStyle_1 t_space" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                    <td class="ff" width="25%">Name of Official Inviting  Tender : <span>*</span></td>
                               <td width="25%"><input name="nameOfficial" class="formTxtBox_1" id="txtNameOfficial" style="width: 280px;" type="text"/>
                               <br />
                                    <span style="color: red;" id="msgOfficialName"></span>
                               </td>
                                    <td class="ff" width="26%"> Designation of Official Inviting  Tender : <span>*</span></td>
                               <td width="25%"><input name="designationOfficial" class="formTxtBox_1" id="txtDesignation" style="width: 280px;" type="text"/>
                               <br />
                                    <span style="color: red;" id="msgOfficialDesignation"></span>
                               </td>
                            </tr>

                            <tr>
                                    <td class="ff">Address of Official Inviting  Tender : </td>
                            <td><textarea class="formTxtBox_1" style="width: 280px;" name="addressOfOfficial" rows="2" cols="49" id="BodyContent_txtContractDescription"></textarea>
						    </td>
                                    <td class="ff">Contact details of Official Inviting  Tender :</td>
                                <td><textarea class="formTxtBox_1" style="width: 280px;" name="contactDetail" rows="2" cols="49" id="Textarea2"></textarea>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4" class="ff mandatory">The procuring entity reserves the right to accept or reject all Tenders / Pre-Qualifications / EOIs</td>
                            </tr>
                        </tbody></table>

                        <table class="formStyle_1 t_space" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td colspan="4" align="center">
                                    <label class="formBtn_1">
                                        <input name="submit" id="btnsubmit" value="Submit" type="submit" />
                                        <input name="hdnbutton" id="hdnbutton" value="" type="hidden"/>
                                    </label>&nbsp;&nbsp;
                              </td>
                            </tr>
                        </tbody></table>
                        <div>&nbsp;</div>
                    </form>
      <% } %>
                    

                    <!--Dashboard Content Part End
                    <script type="text/javascript">//rishita - button disable
                        $(function() {
                            $('#frmCreateTender').submit(function() {
                                //alert($('#frmCreateTender').valid());
                                if($('#frmCreateTender').valid()){
                                    if($('#btnsubmit')!=null){
                                        $('#btnsubmit').attr("disabled","true");
                                        $('#hdnbutton').val("Submit");
                                    }
                                }
                            });
                        });
                    </script>-->
                    <script type="text/javascript">
                    function loadDivision() {
                        var deptId= $('#cmbMinistry').val();
                        //var districtId = $('#cmbDistrict').val();

                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: deptId, funName:'offlineTenderOrgCombo'},  function(j){
                            $('#cmbDivision').children().remove().end()
                            $("select#cmbDivision").html(j);
                        });
                    }
                    </script>
                    <script type="text/javascript">
                    function loadOrganization() {
                        var deptId= 0;
                       // var districtId = $('#cmbDistrict').val();
                            deptId= $('#cmbMinistry').val();

                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: deptId, funName:'offlineTenderOrgCombo'},  function(j){
                            $('#cmbOrganization').children().remove().end()
                            $("select#cmbOrganization").html(j);
                             var orgObj =document.getElementById("cmbMinistry");
                             var orgval = orgObj.options[orgObj.selectedIndex].text;
                             document.getElementById("hidministry").value = orgval;
                             
                        });
                    }
                    </script>

                    <script language="javascript" type="text/javascript">
                    function setSerType(obj,flag){
                        if(obj.options[obj.selectedIndex].text== "Goods" || obj.options[obj.selectedIndex].text== "Works" )
                        {
                            $.post("<%=request.getContextPath()%>/APPServlet", {projectId:'No',param4:$('#hdnPM').val(),procNature:$('#cmbProcureNature').val(),funName:'getPM'},  function(j){
                                                $("select#cmbProcureMethod").html(j);
                                            });
                                            //$('#trLotBtn').show();

                        }

                    }

                    </script>

                   

                    <script type="text/javascript">
                        function GetCal(txtname,controlname)
                        {
                            new Calendar({
                                inputField: txtname,
                                trigger: controlname,
                                showTime: 24,
                                onSelect: function() {
                                    var date = Calendar.intToDate(this.selection.get());
                                    LEFT_CAL.args.min = date;
                                    LEFT_CAL.redraw();
                                    this.hide();
                                    document.getElementById(txtname).focus();
                                }
                            });

                            var LEFT_CAL = Calendar.setup({
                                weekNumbers: false
                            })
                        }
                        function GetCalWithouTime(txtname,controlname)
                        {
                            new Calendar({
                                inputField: txtname,
                                trigger: controlname,
                                showTime: false,
                                dateFormat:"%d/%m/%Y",
                                onSelect: function() {
                                    var date = Calendar.intToDate(this.selection.get());
                                    LEFT_CAL.args.min = date;
                                    LEFT_CAL.redraw();
                                    this.hide();
                                    document.getElementById(txtname).focus();
                                }
                            });

                            var LEFT_CAL = Calendar.setup({
                                weekNumbers: false
                            })
                        }

                    </script>


                    <script type="text/javascript">

                        //Function for Required
                        function required(controlid)
                        {
                            var temp=controlid.length;
                            if(temp <= 0 ){
                                return false;
                            }else{
                                return true;
                            }
                        }

                        //Function for MaxLength
                        function Maxlenght(controlid,maxlenght)
                        {
                            var temp=controlid.length;
                            if(temp>=maxlenght){
                                return false;
                            }else
                                return true;
                        }

                        //Function for digits
                        function digits(control) {
                            return /^\d+$/.test(control);
                        }

                        function CompareToForEqual(value,params)
                        {

                            var mdy = value.split('/')  //Date and month split
                            var mdyhr=mdy[2].split(' ');  //Year and time split
                            var mdyp = params.split('/')
                            var mdyphr=mdyp[2].split(' ');
                            var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                            return Date.parse(date) == Date.parse(datep);
                        }

                        //Function for CompareToForToday
                        function CompareToForToday(first)
                        {
                            var mdy = first.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var mdyhrtime=mdyhr[1].split(':');
                            if(mdyhrtime[1] == undefined){
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                            }else
                            {
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                            }

                            var d = new Date();
                            if(mdyhrtime[1] == undefined){
                                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                            }
                            else
                            {
                                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(),d.getHours(),d.getMinutes());
                            }
                            return Date.parse(valuedate) > Date.parse(todaydate);
                        }

                        //Function for CompareToForGreater
                        function CompareToForGreater(value,params)
                        {
                            if(value!='' && params!=''){

                                var mdy = value.split('/')  //Date and month split
                                var mdyhr=mdy[2].split(' ');  //Year and time split
                                var mdyp = params.split('/')
                                var mdyphr=mdyp[2].split(' ');


                                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                                {
                                    //alert('Both Date');
                                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                                }
                                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                                {
                                    //alert('Both DateTime');
                                    var mdyhrsec=mdyhr[1].split(':');
                                    var date =  new Date( mdyhr[0], parseFloat(mdy[1])-1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var mdyphrsec=mdyphr[1].split(':');

                                    var datep =  new Date(mdyphr[0], parseFloat(mdyp[1])-1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                            }
                                else
                                {
                                    //alert('one Date and One DateTime');
                                    var a = mdyhr[1];  //time
                                    var b = mdyphr[1]; // time

                                    if(a == undefined && b != undefined)
                                    {
                                        //alert('First Date');
                                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                                        var mdyphrsec=mdyphr[1].split(':');
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                    }
                                    else
                                    {
                                        //alert('Second Date');
                                        var mdyhrsec=mdyhr[1].split(':');
                                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                                    }
                                }
                                return Date.parse(date) > Date.parse(datep);
                            }
                            else
                            {
                                return false;
                            }
                        }

                        //Function for CompareToForGreater
                        function CompareToForSmaller(value,params)
                        {
                            if(value!='' && params!=''){

                                var mdy = value.split('/')  //Date and month split
                                var mdyhr=mdy[2].split(' ');  //Year and time split
                                var mdyp = params.split('/')
                                var mdyphr=mdyp[2].split(' ');


                                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                                {
                                    //alert('Both Date');
                                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                                }
                                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                                {
                                    //alert('Both DateTime');
                                    var mdyhrsec=mdyhr[1].split(':');
                                    var date =  new Date( mdyhr[0], parseFloat(mdy[1])-1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var mdyphrsec=mdyphr[1].split(':');
                                    var datep =  new Date(mdyphr[0], parseFloat(mdyp[1])-1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                }
                                else
                                {
                                    //alert('one Date and One DateTime');
                                    var a = mdyhr[1];  //time
                                    var b = mdyphr[1]; // time

                                    if(a == undefined && b != undefined)
                                    {
                                        //alert('First Date');
                                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                                        var mdyphrsec=mdyphr[1].split(':');
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                    }
                                    else
                                    {
                                        //alert('Second Date');
                                        var mdyhrsec=mdyhr[1].split(':');
                                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                                    }
                                }

                                return Date.parse(date) < Date.parse(datep);
                            }
                            else
                            {
                                return false;
                            }
                        }

                        //Function for CompareToWithoutEqual
                        function CompareToWithoutEqual(value,params)
                        {
                            var mdy = value.split('/')  //Date and month split
                            var mdyhr=mdy[2].split(' ');  //Year and time split
                            var mdyp = params.split('/')
                            var mdyphr=mdyp[2].split(' ');


                            if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                            {
                                //alert('Both Date');
                                var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                                var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                            }
                            else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                            {
                                //alert('Both DateTime');
                                var mdyhrsec=mdyhr[1].split(':');
                                var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                var mdyphrsec=mdyphr[1].split(':');
                                var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                            }
                            else
                            {
                                //alert('one Date and One DateTime');
                                var a = mdyhr[1];  //time
                                var b = mdyphr[1]; // time

                                if(a == undefined && b != undefined)
                                {
                                    //alert('First Date');
                                    var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                                    var mdyphrsec=mdyphr[1].split(':');
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                }
                                else
                                {
                                    //alert('Second Date');
                                    var mdyhrsec=mdyhr[1].split(':');
                                    var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                                }
                            }
                            return Date.parse(date) > Date.parse(datep);
                        }
                        //

                    </script>


                    <script type="text/javascript">

                        function numeric(value) {
                            return /^\d+$/.test(value);
                        }

                        function chkRefNoBlank(obj){
                            var boolcheck1='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(obj.value!='' && obj.value.charAt(0) != ' '){
                                if(obj.value.length <= 50)
                                {
                                    document.getElementById("refno_"+i).innerHTML="";
                                    boolcheck1='true';
                                }
                                else
                                {
                                    document.getElementById("refno_"+i).innerHTML="<br/>Maximum 50 characters are allowed";
                                    boolcheck1=false;
                                }
                            }else{
                                document.getElementById("refno_"+i).innerHTML = "<br/>Please enter Ref. No";
                                boolcheck1=false;
                            }

                            if(boolcheck1==false){
                                document.getElementById("boolcheck").value=boolcheck1;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkPhaseSerBlank(obj){
                            var boolcheck2='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(obj.value!='' && obj.value.charAt(0) != ' ')
                            {
                                if(obj.value.length <= 500)
                                {
                                    document.getElementById("phaseSer_"+i).innerHTML="";
                                    boolcheck2='true';
                                }
                                else
                                {
                                    document.getElementById("phaseSer_"+i).innerHTML="<br/>Maximum 500 characters are allowed";
                                    boolcheck2=false;
                                }
                            }else{
                                document.getElementById("phaseSer_"+i).innerHTML = "<br/>Please enter Phasing of service";
                                boolcheck2=false;
                            }
                            if(boolcheck2==false){
                                document.getElementById("boolcheck").value=boolcheck2;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkLocRefBlank(obj){
                            var boolcheck4='true';

                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(obj.value!=''){
                                if(obj.value.length <= 100)
                                {
                                    document.getElementById("locRef_"+i).innerHTML="";
                                    boolcheck4='true';
                                }
                                else
                                {
                                    document.getElementById("locRef_"+i).innerHTML="<br/>Maximum 100 characters are allowed";
                                    boolcheck4=false;
                                }
                            }else{
                                document.getElementById("locRef_"+i).innerHTML = "<br/>Please enter Location";
                                boolcheck4=false;
                            }
                            if(boolcheck4==false){
                                document.getElementById("boolcheck").value=boolcheck4;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkIndStartBlank(obj){
			    vbooltemp=true;
                            var boolcheck5='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            var closeDate=document.getElementById("txtpreQualCloseDate").value;
                            var year=parseInt(closeDate.split('/')[2]);
                            var month=(closeDate.split('/')[1]);
                            var day=(closeDate.split('/')[0]);
                            var date =  new Date(year, (month-1), day);

                            var objdate=obj.value;
                            var yearobj=parseInt(objdate.split('/')[2]);
                            var monthobj=(objdate.split('/')[1]);
                            var dayobj=(objdate.split('/')[0]);
                            var dateobj =  new Date(yearobj, (monthobj-1), dayobj);

                            if(obj.value!=''){
                                if(Date.parse(dateobj) > Date.parse(date))
                                {
                                    document.getElementById("indStart_"+i).innerHTML="";
                                    boolcheck5='true';
                                }
                                else
                                {
                                    document.getElementById("indStart_"+i).innerHTML="<br/>Indicative Start Date must be greater than EOI Closing Date and Time";
                                    boolcheck5=false;
                                }
                            }else{
                                document.getElementById("indStart_"+i).innerHTML = "<br/>Please enter Indicative Start Date";
                                boolcheck5=false;
                            }
                            if(boolcheck5==false){
                                document.getElementById("boolcheck").value=boolcheck5;
				    vbooltemp=false;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
				    vbooltemp=true;
                            }
                        }
                        function chkIndCompBlank(obj){
                            var boolcheck6='true';
				vbooltemp=true;
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));

                            if(obj.value!=''){
                                if(CompareToForGreater(obj.value,document.getElementById("txtindicativeStartDate_"+i).value))
                                {
                                    document.getElementById("indComp_"+i).innerHTML="";
                                    boolcheck6='true';
                                }
                                else
                                {
                                    document.getElementById("indComp_"+i).innerHTML="<br/>Indicative Completion Date must be greater than Indicative Start Date";
                                    boolcheck6=false;
                                }
                            }else{
                                document.getElementById("indComp_"+i).innerHTML = "<br/>Please enter Indicative Completion Date";
                                boolcheck6=false;
                            }

                            if(boolcheck6==false){
                                document.getElementById("boolcheck").value=boolcheck6;
				    vbooltemp=false;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
				    vbooltemp=true;
                            }

                        }
                        function chkLocLotBlank(obj){
                            var boolcheck7='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(obj.value!=''){
                                if(obj.value.length <= 100)
                                {
                                    document.getElementById("locLot_"+i).innerHTML="";
                                    boolcheck7='true';
                                }
                                else
                                {
                                    document.getElementById("locLot_"+i).innerHTML="<br/>Maximum 100 characters are allowed";
                                    boolcheck7=false;
                                }
                            }else{
                                document.getElementById("locLot_"+i).innerHTML = "<br/>Please enter Location";
                                boolcheck7=false;
                            }
                            if(boolcheck7==false){
                                document.getElementById("boolcheck").value=boolcheck7;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkAmountLotBlank(obj){
                            var boolcheck8='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(obj.value!='')
                            {
                                if(numeric(obj.value))
                                {
                                    document.getElementById("amountLot_"+i).innerHTML="";
                                    boolcheck8='true';
                                    tenderSecAmt(obj);
                                }
                                else
                                {
                                    document.getElementById("amountLot_"+i).innerHTML="<br/>Please enter Numeric Data";
                                    var temp = obj.id.split("_");
                                    var countTSA = temp[1];
                                    $(".tenderSecAmtInWords_" + countTSA).remove();
                                    var temp = obj.id.split("_");
                                    var countTSA = temp[1];
                                    $(".tenderSecAmtInWords_" + countTSA).remove();
                                    boolcheck8=false;
                                }

                            }
                            else{
                                document.getElementById("amountLot_"+i).innerHTML = "<br/>Please enter Tender Security Amount";
                                var temp = obj.id.split("_");
                                var countTSA = temp[1];
                                $(".tenderSecAmtInWords_" + countTSA).remove();
                                boolcheck8=false;
                            }
                            if(boolcheck8==false){
                                document.getElementById("boolcheck").value=boolcheck8;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkAmountSecurity(msg){
                            var boolcheck8='true';
                            var obj = document.getElementById("txtsecurityAmountService_0");
                            //var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(document.getElementById("txtsecurityAmountService_0").value!='')
                            {
                                if(document.getElementById("txtsecurityAmountService_0").value> 0){
                                if(numeric(document.getElementById("txtsecurityAmountService_0").value))
                                {
                                    document.getElementById("amountLotService").innerHTML="";
                                    boolcheck8='true';
                                    tenderSecAmt(document.getElementById("txtsecurityAmountService_0"));
                                }
                                else
                                {
                                    document.getElementById("amountLotService").innerHTML="<br/>Please enter Numeric Data";
                                    //var temp = obj.id.split("_");
                                    //var countTSA = temp[1];
                                    $(".tenderSecAmtInWords_0").remove();
                                    $(".tenderSecAmtInWords_0").remove();
                                    boolcheck8=false;
                                }
                            }else{
                                document.getElementById('amountLotService').innerHTML='<div class="reqF_1">Only 0 value is not allowed</div>';
                                var temp = obj.id.split("_");
                                var countTSA = temp[1];
                                $(".tenderSecAmtInWords_" + countTSA).remove();
                                boolcheck8 = false;
                            }
                            }else{
                                document.getElementById("amountLotService").innerHTML = "<br/>"+msg;
                                var temp = obj.id.split("_");
                                var countTSA = temp[1];
                                $(".tenderSecAmtInWords_" + countTSA).remove();
                                boolcheck8=false;
                            }
                            if(boolcheck8==false){
                                document.getElementById('combineTenderSecurityAmount').value ='';
                                document.getElementById("boolcheck").value=boolcheck8;
                            }else
                            {
                                document.getElementById('combineTenderSecurityAmount').value = obj.value;
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkDocFeesLotBlank(obj){
                            var boolcheck9='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(!regForSpace(obj.value) && obj.value!='')
                            {
                                if(numeric(obj.value))
                                {
                                    document.getElementById("docFees_"+i).innerHTML="";
                                    boolcheck9='true';
                                    docFeesWord(obj);
                                }
                                else
                                {
                                    document.getElementById("docFees_"+i).innerHTML="<br/>Please Enter Numeric Data";
                                    var temp = obj.id.split("_");
                                    var countDF = temp[1];
                                    $(".docFeesInWords_" + countDF).remove();
                                    var temp = obj.id.split("_");
                                    var countDF = temp[1];
                                    $(".docFeesInWords_" + countDF).remove();
                                    boolcheck9=false;
                                }

                            }else{
                                document.getElementById("docFees_"+i).innerHTML = "<br/>Please enter Document fees amount in Nu.";
                                var temp = obj.id.split("_");
                                var countDF = temp[1];
                                $(".docFeesInWords_" + countDF).remove();
                                boolcheck9=false;
                            }
                            if(boolcheck9==false){
                                document.getElementById("boolcheck").value=boolcheck9;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkCompTimeLotBlank(obj){
                            var boolcheck10='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(obj.value!=''){
                                if(obj.value.length <= 100)
                                {
                                    document.getElementById('compLot_'+i).innerHTML='';
                                    boolcheck10='true';
                                }
                                else
                                {
                                    document.getElementById('compLot_'+i).innerHTML='<br/>Maximum 100 characters are allowed';
                                    boolcheck10=false;
                                }
                            }else{
                                //document.getElementById('compLot_'+i).innerHTML = '<br/>Please enter Completion Date';
                                //boolcheck10=false;
                            }
                            if(boolcheck10==false){
                                document.getElementById('boolcheck').value=boolcheck10;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkStartTimeLotBlank(obj){
                            var boolcheck11='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(obj.value!=''){
                                if(obj.value.length <= 100)
                                {
                                    document.getElementById('startLot_'+i).innerHTML='';
                                    boolcheck11='true';
                                }
                                else
                                {
                                    document.getElementById('startLot_'+i).innerHTML='<br/>Maximum 100 characters are allowed';
                                    boolcheck11=false;
                                }
                            }else{
                                //document.getElementById('startLot_'+i).innerHTML = '<br/>Please enter Start Date';
                                //boolcheck11=false;
                            }
                            if(boolcheck11==false){
                                document.getElementById('boolcheck').value=boolcheck11;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                            return boolcheck11;
                        }

                        //end
                    </script>

                </div>
            </div>


        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>

    </body>

     <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabOfflineData");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>