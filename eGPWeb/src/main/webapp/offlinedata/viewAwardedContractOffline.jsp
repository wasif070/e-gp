<%-- 
    Document   : viewAwardedContractOffline
    Created on : 30-Aug-2012, 12:56:02
    Author     : Ahsan
--%>

<%@page import="com.cptu.egp.eps.web.utility.CommonUtils"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id ="ContractAward" class="com.cptu.egp.eps.web.servicebean.AwardedContractOfflineSrBean"/>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.ContractAwardOfflineDetails" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>

         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Contract Award Details</title>
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../../resources/js/ddlevelsmenu.js"></script>
        <script src="../../resources/js/form/CommonValidation.js"type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>

        <!--jalert -->
        <%if (request.getParameter("print") == null) {%>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <%}%>

        <script type="text/javascript">

            $(document).ready(function(){
                var frank_param = getParam( 'approve' );
                if(frank_param=='false')
                    {
                        $("#divApprove").hide();
                        $("#tblApprove").hide();
                    }
               else{
                   $("#divApprove").show();
                   $("#tblApprove").show();
                   $("#cmntReq").hide();
               }

              /* $("#frmViewAwardedContract").validate({
                    rules: {

                       txtComment: {required: true}
                    },
                    messages: {

                        txtComment: { required: "<div class='reqF_1'>Required</div>"}
                    }
                });*/
                $("#btnsubmit").click(function(){
                    if($("#txtComment").val() == "")
                    {
                        $("#cmntReq").show();
                        return false;
                    }
                });
            });

            function getParam( name )
            {
             name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
             var regexS = "[\\?&]"+name+"=([^&#]*)";
             var regex = new RegExp( regexS );
             var results = regex.exec( window.location.href );
             if( results == null )
              return "";
            else
             return results[1];
            }
        </script>
    </head>
    <%
     System.out.println("editcontract submit"+request.getParameter("submit"));
   
        if( request.getParameter("submit") != null && "Submit".equals(request.getParameter("submit"))){
        try{
            ContractAward.approveAwardedContract(request.getParameter("hidRefNo"), request.getParameter("txtComment"));
            response.sendRedirect("SearchAwardedContractOffline.jsp");
        }
        catch(Exception ex){
        ex.printStackTrace();
        }
    }
   
    %>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                 <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="contentArea_1">
                    <form id="frmViewAwardedContract" name="frmViewAwardedContract" method="POST" action="viewAwardedContractOffline.jsp" >
                     <div class="t-align-right" style="vertical-align: middle;"><a class="action-button-goback" href="SearchAwardedContractOffline.jsp?">Go back</a></div>
                        <div class="pageHead_1">Contract Awards Details</div>
                   

                <%
                            String tenderInfoStatus = "";
                            String contractNo = request.getParameter("contractNo");
                            //refNo = "12345";
                            for (ContractAwardOfflineDetails ContractAwardOfflineDetails : ContractAward.getContractAwardedOfflineDetails(contractNo)) {
                               // List<Object[]> obj = ContractAward.getContractAwardOfflineData("false","Pending","","","12345","","","");
                               // tenderStatus = commonTenderDetails.getTenderStatus();
                               // tenderInfoStatus = commonTenderDetails.getTenderevalstatus();
                              // List<SPCommonSearchDataMore> isTenderSecAmt = tenderSrBean.isAvalTenSecAmt(id);
                %>


                    <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                        <tbody><tr>
                            <td class="ff" width="30%">Ministry/Division:</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getMinistry())%></td>
                        </tr>
                        <tr>
                            <td class="ff">Agency:</td>
                            <td><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getAgency())%> </td>
                        </tr>
                        <tr>
                            <td class="ff">Procuring Entity Name:</td>
                            <td><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getPeOfficeName())%></td>
                        </tr>
                        <tr>
                            <td class="ff">Procuring Entity Code:</td>
                            <td><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getPeCode())%></td>
                        </tr>
                        <tr>
                            <td class="ff">Procuring Entity Dzongkhag / District:</td>
                            <td><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getPeDistrict())%></td>
                        </tr>
                        <tr>
                            <td class="ff">Contract Award for:</td>
                            <td><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getAwardForPNature()) %></td>
                        </tr>
                        <tr>
                            <td class="ff">Invitation/Proposal Reference No.:</td>
                            <td><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getRefNo()) %>
                                <input id="hidRefNo"  name="hidRefNo" type="hidden" value="<%=ContractAwardOfflineDetails.getContarctAIdOL()%>" />
                            </td>
                        </tr>
                    </tbody></table>
                    <div class="tableHead_22 t_space">KEY INFORMATION</div>
                    <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                        <tbody><tr>
                            <td class="ff" width="30%">Procurement Method:</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getProcurementMethod()) %></td>
                        </tr>
                    </tbody></table>
                    <div class="tableHead_22 t_space">FUNDING INFORMATION</div>
                    <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                        <tbody><tr>
                            <td class="ff" width="30%">Budget and Source of Funds:</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getBudgetType()) %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Development Partner (if applicable):</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getDevPartners()) %></td>
                        </tr>
                    </tbody></table>
                    <div class="tableHead_22 t_space">PARTICULAR INFORMATION</div>
                    <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                        <tbody><tr>
                            <td class="ff" width="30%">Project/Programme Name (if applicable):</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getProjectName()) %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Tender Package No.:</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getPackageNo()) %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Tender Package Name:</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getPackageName()) %> </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Date of Advertisement:</td>
                            <td width="70%"><%=DateUtils.customDateFormate(ContractAwardOfflineDetails.getDateofAdvertisement()).toString() %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Date of Letter of Acceptance:</td>
                            <td width="70%"><%=DateUtils.customDateFormate(ContractAwardOfflineDetails.getDateofNOA()) %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Date of Contract Signing:</td>
                            <td width="70%"><%=DateUtils.customDateFormate(ContractAwardOfflineDetails.getDateofContractSign()) %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Proposed Date of Contract Completion:</td>
                            <td width="70%"><%=DateUtils.customDateFormate(ContractAwardOfflineDetails.getDateofPCCompletion()) %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">No. of Tenders/Proposals Sold:</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getSold()) %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">No. of Tenders/Proposals Received:</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getReceived()) %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Tenders/Proposals Responsive:</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getResponse()) %></td>
                        </tr>
                    </tbody></table>
                    <div class="tableHead_22 t_space">INFORMATION ON AWARD</div>
                    <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                        <tbody><tr>
                            <td class="ff" width="30%">Brief Description of Contract:</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getDescriptionofContract()) %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Contract Value (Million Nu.):</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getContractValue().toString()) %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Name of Supplier/Contractor/Consultant:</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getNameofTenderer()) %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Location of Supplier/Contractor/Consultant:</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getAddressofTenderer()) %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Location of Delivery/Works/Consultancy:</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getDeliveryPlace()) %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Was the Performance Security provided in due time?</td>
                           <%
                            if(ContractAwardOfflineDetails.getIsPSecurityDueTime().equals(1))
                            { %> <td width="70%">Yes</td> <%}
                            else %><td width="70%">No</td>

                        </tr>
                        <tr>
                            <td class="ff" width="30%">Was the Contract Singed in due time?</td>
                            <%
                            if(ContractAwardOfflineDetails.getIsSignedDueTime().equals(1))
                            { %> <td width="70%">Yes</td> <%}
                            else %><td width="70%">No</td>
                          
                        </tr>
                    </tbody></table>
                    <div class="tableHead_22 t_space">ENTITY DETAILS</div>
                    <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                        <tbody><tr>
                            <td class="ff" width="30%">Name of Authorised Officer:</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getOfficerName()) %></td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Designation of Authorised Officer:</td>
                            <td width="70%"><%=CommonUtils.checkNull(ContractAwardOfflineDetails.getOfficerDesignation()) %></td>
                        </tr>
                    </tbody></table>
                         <%}%>
                         <div class="tableHead_22 t_space" id="divApprove">Approving Information</div>
                         <table id="tblApprove" class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                             <tbody>
                                 <tr>
                                     <td class="ff" width="30%">Action:</td>
                                     <td width="70%">Approve</td>
                                 </tr>
                                 <tr>
                                     <td class="ff" width="30%">Comment: <span style="color: red">*</span></td>
                                     <td width="70%"><textarea id="txtComment" name="txtComment" cols="50" rows="3"></textarea> <div class='reqF_1' id="cmntReq"> Required</div> </td>
                                 </tr>
                                 <tr>
                                     <td class="ff" width="30%"></td>
                                     <td width="70%"> 
                                         <label class="formBtn_1">
                                             <input name="submit" id="btnsubmit" value="Submit" type="submit" />
                                         </label>
                                     </td>
                                 </tr>
                             </tbody>
                         </table>
                       </form>
                </div>
            </div>
        </div>
    </body>
</html>
