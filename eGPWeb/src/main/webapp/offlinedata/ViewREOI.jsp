<%-- 
    Document   : ViewREOI
    Created on : Sep 5, 2012, 12:35:13 PM
    Author     : Istiak
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<jsp:useBean id ="ViewTender" class="com.cptu.egp.eps.web.servicebean.TenderDashboardOfflineSrBean"/>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.TenderDashboardOfflineDetails" %>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<%@page import="com.cptu.egp.eps.web.utility.CommonUtils"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View REOI for Services</title>

        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>

        <!-- jQuery Datatable -->
        <script type="text/javascript" src="../resources/js/jquery.dataTables.js"></script>
        <link href="../resources/css/demo_table.css" type="text/css" rel="stylesheet">

        <script type="text/javascript">

            $(document).ready(function(){

                var frank_param = getParam('approve');

                if(frank_param=='false')
                {
                    $("#divApprove").remove();
                    $("#tblApprove").remove();
                    $("#btnGoBack").remove();
                    $("#pgAfterLoginTion").remove();
                }
                else
                {
                   $("#divApprove").show();
                   $("#tblApprove").show();
                   $("#btnGoBack").show();
                   $("#dvBr").remove();

                   if(frank_param=='trueCOR')
                   {
                       $("#btnsubmitCOR").show();
                       $("#btnsubmitTender").hide();
                   }
                   else
                   {
                       $("#btnsubmitCOR").hide();
                       $("#btnsubmitTender").show();
                   }
                }

                $("#frmViewREOI").validate({
                    rules: {
                       txtComment: {required: true}
                    },
                    messages: {
                        txtComment: { required: "<div class='reqF_1'>Required</div>"}
                    }
                });

            }); // End Document.Ready

                function getParam( name )
                {
                     name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
                     var regexS = "[\\?&]"+name+"=([^&#]*)";
                     var regex = new RegExp( regexS );
                     var results = regex.exec( window.location.href );
                     if( results == null )
                        return "";
                     else
                        return results[1];
                }

        </script>
        
    </head>

    <%
        if( request.getParameter("submit") != null && "Submit".equals(request.getParameter("submit"))){
            try{
                if(ViewTender.approveTenderOffline(request.getParameter("tenderOfflineId"), request.getParameter("txtComment")))
                {
                    %>
                        <script type="text/javascript">
                          alert("Tender Approved Successfully");
                        </script>
                    <%
                    response.sendRedirect("TenderDashboardOfflineApproval.jsp");
                }
                else
                {
                    %>
                        <script type="text/javascript">
                          alert("An Error Occurred");
                        </script>
                    <%
                }
                    
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }
        else if( request.getParameter("cor") != null && "Submit".equals(request.getParameter("cor"))){
            try{
                if(ViewTender.approveCOROffline(request.getParameter("tenderOfflineId"), request.getParameter("txtComment")))
                {                    
                    %>
                        <script type="text/javascript">
                          alert("Corrigendum Approved Successfully");
                        </script>
                    <%
                   response.sendRedirect("TenderDashboardOfflineApproval.jsp");
                }
                else
                {
                    %>
                        <script type="text/javascript">
                          alert("An Error Occurred");
                        </script>
                    <%
                }
            }
            catch(Exception ex){
                ex.printStackTrace();
            }
        }
    %>

    <body>        

        <div class="mainDiv">
            <div class="dashboard_div">

                <div id="pgAfterLoginTion">
                    <%@include file="../resources/common/AfterLoginTop.jsp" %>
                </div>
                <div id="dvBr">
                    <br/>
                </div>
                
                    <div class="contentArea_1">
                        <div class="t-align-right" style="vertical-align: middle;" id="btnGoBack"><a class="action-button-goback" href="<%=request.getContextPath()%>/offlinedata/TenderDashboardOfflineApproval.jsp">Go back</a></div>
                        <form id="frmViewREOI" name="frmViewREOI" method="POST" action="">

                        <div class="pageHead_1">View REOI for Services</div>

                        <%
                            String id = request.getParameter("ID");
                            for (TenderDashboardOfflineDetails tenderDashboardOfflineDetails : ViewTender.getTenderDashboardOfflineDetails(id)) {

                        %>

                        <div class="tableHead_22 t_space">PROCURING ENTITY (PE) INFORMATION</div>

                            <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                                <tbody>
                                    <tr>
                                        <td colspan="4" class="ff t-align-left" align="left"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="25%">Ministry/Division :  </td>
                                        <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getMinistryOrDivision())%></td>

                                        <td class="ff" width="25%">Agency :  </td>
                                        <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getAgency())%></td>
                                    </tr>
                                    <tr>

                                        <td class="ff">Procuring Entity Name :  </td>
                                        <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPeName())%></td>

                                        <td class="ff">Procuring Entity Code :</td>
                                        <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPeCode())%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Procuring Entity Dzongkhag / District :  </td>
                                        <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPeDistrict())%></td>

                                        <td class="ff">Contract Type :  </td>
                                        <td>
                                            <%
                                                String contractType = CommonUtils.checkNull(tenderDashboardOfflineDetails.getInvitationFor());
                                                if(contractType.indexOf("Lump") > -1)
                                                {
                                                    contractType = "Lump – sum";
                                                }
                                            %>
                                            <%=contractType%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Event Type :</td>
                                        <!--<td><label>REOI</label></td>-->
                                        <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getEventType())%></td>

                                        <td class="ff">REOI No. :  </td>
                                        <td colspan="3">
                                            <%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getReoiRfpRefNo())%>
                                            <input type="hidden" id="tenderOfflineId" name="tenderOfflineId" value="<%=id%>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Request for Expression of Interest for <br>Selection of :   </td>
                                        <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getReoiRfpFor())%></td>

                                        <td class="ff">Date :  </td>
                                        <td class="formStyle_1">
                                            <%
                                                String date = "";
                                                if(tenderDashboardOfflineDetails.getIssueDate() != null)
                                                {
                                                    date = DateUtils.customDateFormate(tenderDashboardOfflineDetails.getIssueDate());
                                                }
                                            %>
                                            <%=date%>

                                        </td>
                                    </tr>

                                </tbody>
                            </table>

                            <div class="tableHead_22 ">Key Information and Funding Information :</div>
                            <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                                <tbody>
                                <tr>
                                    <td class="ff" width="25%">Procurement Sub-Method :   </td>
                                    <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getProcurementMethod())%></td>
                                    <td class="ff">Budget Type :  </td>
                                    <td colspan="3"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getBudgetType())%></td>

                                </tr>
                                <tr>
                                    <td class="ff" width="25%">Source of Funds :  </td>
                                    <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getSourceOfFund())%></td>
                                    <td class="ff" width="25%">Development Partners : </td>
                                    <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getDevPartners())%></td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="tableHead_22 ">Particular Information :</div>

                            <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                                <tbody>
                                    <tr>
                                        <td class="ff" width="25%">Project Code : </td>
                                        <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getProjectCode())%></td>
                                        <td class="ff" width="25%">Project Name : </td>
                                        <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getProjectName())%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">EOI Closing<br>Date and Time :  </td>
                                        <td width="25%">
                                            <%
                                                String closingDate = "";
                                                if(tenderDashboardOfflineDetails.getClosingDate() != null)
                                                {
                                                    closingDate = DateUtils.gridDateToStrWithoutSec(tenderDashboardOfflineDetails.getClosingDate());
                                                }
                                            %>
                                            <%=closingDate%>
                                        </td>
                                        <td class="ff"></td>
                                        <td class="ff"></td>
                                    </tr>
                                </tbody>
                            </table>


                            <div class="tableHead_22 ">Information for Bidder/Consultant :</div>
                            <table class="formStyle_1 " width="100%" border="0" cellpadding="0" cellspacing="10">
                                <tbody>
                                    <tr>
                                        <td class="ff" width="25%">Brief Description of Assignment :  </td>
                                        <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getBriefDescription())%> </td><td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Experience, Resources and<br>Delivery Capacity Required :  </td>
                                        <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getRelServicesOrDeliverables())%></td><td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Other Details (if  applicable) :</td>
                                        <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getOtherDetails())%></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Association with Foreign  Firm :  </td>
                                        <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getForeignFirm())%></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>

                        <%
                            String  lotTable = ViewTender.lotInfo(id, "reoi");
                        %>

                        <%=lotTable%>   <!--Load Lot Table-->

                            <div class="tableHead_22 t_space">Procuring Entity Details :</div>
                            <table class="formStyle_1 t_space" width="100%" border="0" cellpadding="0" cellspacing="10">
                                <tbody>
                                    <tr>
                                        <td class="ff" width="25%">Name of Official Inviting  REOI :  </td>
                                        <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPeOfficeName())%></td>
                                        <td class="ff" width="26%"> Designation of Official Inviting  REOI :  </td>
                                        <td width="25%"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPeDesignation())%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Address of Official Inviting  REOI :   </td>
                                        <td><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPeAddress())%></td>
                                        <td class="ff">Contact details of Official Inviting REOI : </td>
                                        <td class="ff"><%=CommonUtils.checkNull(tenderDashboardOfflineDetails.getPeContactDetails())%></td>
                                    </tr>
                                </tbody>
                            </table>

                       <%}%>                    

                        <div>&nbsp;</div>

                        <label class="ff mandatory">The procuring entity reserves the right to accept or reject all Tenders/Proposals / Pre-Qualifications / EOIs</label>

                        <%
                            String  corTable = ViewTender.corInfo(id, "reoi");
                        %>

                        <%=corTable%>   <!--Load Corrigendum Table-->


                        <div class="tableHead_22 t_space" id="divApprove">Approving Information</div>
                         <table id="tblApprove" class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                             <tbody>
                                 <tr>
                                     <td class="ff" width="30%">Action :</td>
                                     <td width="70%">Approve</td>
                                 </tr>
                                 <tr>
                                     <td class="ff" width="30%">Comment : <span class="mandatory">*</span></td>
                                     <td width="70%"><textarea id="txtComment" name="txtComment" cols="50" rows="3"></textarea> </td>
                                 </tr>
                                 <tr>
                                     <td class="ff" width="30%"></td>
                                     <td width="70%">
                                         <label class="formBtn_1">
                                             <input name="submit" id="btnsubmitTender" value="Submit" type="submit" />
                                             <input name="cor" id="btnsubmitCOR" value="Submit" type="submit" />
                                         </label>
                                     </td>
                                 </tr>
                             </tbody>
                         </table>
                        
                    </form>

                </div>

            </div>
                 <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>

    </body>
</html>
