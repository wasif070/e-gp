<%-- 
    Document   : CorrigendumTenderWithPQ
    Created on : 02-Sep-2012, 10:58:23
    Author     : salahuddin
--%>


<%@page import="com.cptu.egp.eps.web.servlet.ComboServlet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblCorrigendumDetailOffline"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotPhasingOffline"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetailsOffline"%>
<%@page import="com.cptu.egp.eps.web.utility.CommonUtils"%>
<%@page import="com.cptu.egp.eps.web.offlinedata.PrequalificationExcellBean"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblStateMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.model.table.TblDepartmentMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ManageEmployeeGridSrBean"%>
<%@page import="java.util.Calendar"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
 <jsp:useBean id="offlineDataSrBean" class="com.cptu.egp.eps.web.offlinedata.OfflineDataSrBean"/>
 <jsp:useBean id="corrigendumTenderWithPQBean" class="com.cptu.egp.eps.web.offlinedata.CorrigendumTenderWithPQBean"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Corrigendum of Tender with Pre-qualification (PQ)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>

        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <link href="../ckeditor/editor.css" type="text/css" rel="stylesheet"/>

        <!--
        <script src="resources/config.js" type="text/javascript"></script>
        <script src="resources/en.js" type="text/javascript"></script>
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
       -->

       <!--jquery validator max length css change -->
       <style type="text/css">
           label.error{color:red}
       </style>
        <script type="text/javascript">
            var holiArray = new Array();
        </script>

       <script type="text/javascript">        // Script for Datatable

           function dataTable(selVal){
                    if(selVal == "Single Lot"){
                        document.getElementById("lot_1").style.display = "none";
                        document.getElementById("lot_2").style.display = "none";
                        document.getElementById("lot_3").style.display = "none";
                    }else {
                        document.getElementById("lot_1").style.display = "table-row";
                        document.getElementById("lot_2").style.display = "table-row";
                        document.getElementById("lot_3").style.display = "table-row";


                        /*document.getElementById("txtLotNo_2").value ='';
                        document.getElementById("txtidentification_2").value = '';
                        document.getElementById("txtLocationlot_2").value = '';
                        document.getElementById("txtcomplTimeLotNo_2").value='';

                        document.getElementById("txtLotNo_3").value ='';
                        document.getElementById("txtidentification_3").value = '';
                        document.getElementById("txtLocationlot_3").value = '';
                        document.getElementById("txtcomplTimeLotNo_3").value='';

                        document.getElementById("txtLotNo_4").value ='';
                        document.getElementById("txtidentification_4").value = '';
                        document.getElementById("txtLocationlot_4").value = '';
                        document.getElementById("txtcomplTimeLotNo_4").value='';*/
                    }

            }

           function AddRow(lotType) {
                if (lotType == "Multiple Lot") {
                    var ptable = document.getElementById('tblLotDescription');
                    var lastElement = ptable.rows.length;
                    var index = lastElement;
                    var row = ptable.insertRow(lastElement);

                    var newTxt = "";
                    for(var i = 2; i<=4; i++){

                        newTxt += "<tr class='trExtra' id='tr" + (row + i) +"'>" +  //<td class='t-align-center'><input type='checkbox' name='chkLot'/></td>" +
                                "<td class='t-align-center'><input name='lotNo_" + i +"' class='formTxtBox_1' value='' id='txtLotNo" + i +"' type='text'/></td>" +
                                "<td class='t-align-center'><textarea name='identification_" + i +"' id='txtDescriptionForPQ" + i +"' rows='2' cols='40'></textarea></td>" +
                                "<td class='t-align-center'><input name='locationlot_" + i +"' style='width: 180px;' class='formTxtBox_1' id='locationlot_" + i +"' type='text'></td>" +
                                "<td class='t-align-center'>" +
                                    "<input name='complTimeLotNo_" + i +"' class='formTxtBox_1' id='complTimeLotNo_" + i +"' style='width: 100px;' type='text'>" +
                                    "</td></tr>";

                    }

                    $("#tblLotDescription").append(newTxt);
                }
                else {
                    $('.trExtra').each(function () {
                        $(this).remove();
                    });

                }
            }

            

            function setOrgval(id){

                var orgObj =document.getElementById(id);
                var orgval = orgObj.options[orgObj.selectedIndex].text;
                //alert('orgvalue >> '+orgval);
                document.getElementById("hidorg").value = orgval;
            }

            $(document).ready(function(){

                //$('#addRemoveLot').hide();

                //Form Validation
                $("#frmCorrigendumTenderWithPQ").validate({
                    rules: {
                        //textbox
                        invitationRefNo: {required: true,maxlength:150},
                        issuedate: {required: true},
                        packageNo: {required: true,maxlength:50},
                        packageName: {required: true, maxlength:150},
                        tenderpublicationDate: {required: true},
                        preQualCloseDate: {required: true},
                        preQualDocPrice: {required: true, number:true,maxlength:15},
                        
                        nameOfficial: {required: true,maxlength:200},
                        designationOfficial: {required: true,maxlength:200},
                        eligibilityofTenderer:{required: true}, 
                        briefDescGoods:{required: true}, 
                        briefDescRelServices:{required: true}, 
                        receivingPQ :{required: true,maxlength:2000},
                        sellingDocPrinciple:{required: true,maxlength:2000},
                        identification_1:{required: true,maxlength:2000},
                        addressOfOfficial:{maxlength:5000},
                        contactDetail:{maxlength:1000},
                        sellingDocOthers:{maxlength:1000}, 
                        placeofPQMeeting:{maxlength:2000},
                        devPartner:{maxlength:300},
                        projectCode:{maxlength:150},
                        projectName:{maxlength:150},
                        
                        lotNo_1:{maxlength:10},
                        locationlot_1:{maxlength:100},
                        complTimeLotNo_1:{maxlength:100},
                        identification_2:{maxlength:2000},
                        lotNo_2:{maxlength:10},
                        locationlot_2:{maxlength:100},
                        complTimeLotNo_2:{maxlength:100},
                        identification_3:{maxlength:2000},
                        lotNo_3:{maxlength:10},
                        locationlot_3:{maxlength:100},
                        complTimeLotNo_3:{maxlength:100},
                        identification_4:{maxlength:2000},
                        lotNo_4:{maxlength:10},
                        locationlot_4:{maxlength:100},
                        complTimeLotNo_4:{maxlength:100},

                        //Dropdown
                        nature:{selectNone: true},
                        procType:{selectNone: true},
                        invitationFor:{selectNone: true},
                        procureMethod:{selectNone: true},
                        budgetType:{selectNone: true},
                        sourceFunds:{selectNone: true}

                    },
                    messages: {
                        //Textbox
                        invitationRefNo: { required: "<div class='reqF_1'>Please enter Reference No.</div>"},
                        issuedate: { required: "<div class='reqF_1'>Please select Issue Date.</div>"},
                        packageNo: { required: "<div class='reqF_1'>Please enter Package No.</div>"},
                        packageName: { required: "<div class='reqF_1'>Please enter Package Name.</div>"},
                        tenderpublicationDate: { required: "<div class='reqF_1'>Please select Pre-Qualification Publication Date .</div>"},
                        preQualCloseDate: { required: "<div class='reqF_1'>Please select Pre-Qualification Closing Date and Time.</div>"},
                        preQualDocPrice: { required: "<div class='reqF_1'>Please enter Pre-Qualification Document Price.</div>", number: "<div class='reqF_1'>Incorrect Format</div>"},
                        
                        nameOfficial: { required: "<div class='reqF_1'>Please enter the Name.</div>"},
                        designationOfficial: { required: "<div class='reqF_1'>Please enter Designation.</div>"},
                        eligibilityofTenderer:{ required: "<div class='reqF_1'>Please enter Eligibility of Tenderer.</div>"},
                        briefDescGoods:{ required: "<div class='reqF_1'>Please enter Description of Goods or Works.</div>"},
                        briefDescRelServices:{ required: "<div class='reqF_1'>Please enter Description of Related Services.</div>"},
                        receivingPQ :{ required: "<div class='reqF_1'>Please enter Receiving Tender.</div>"},
                        sellingDocPrinciple:{ required: "<div class='reqF_1'>Please enter Selling Tender Document.</div>"},
                        identification_1:{required: "<div class='reqF_1'>Please enter Identification of Lot.</div>"},

                        //Dropdown
                        
                        nature:{ selectNone: "<div class='reqF_1'>Please select Procurement Category.</div>"},
                        procType:{ selectNone: "<div class='reqF_1'>Please select Procurement Type.</div>"},
                        invitationFor:{ selectNone: "<div class='reqF_1'>Please select Lot Type.</div>"},
                        procureMethod:{ selectNone: "<div class='reqF_1'>Please select Procurement Method.</div>"},
                        budget:{ selectNone: "<div class='reqF_1'>Please select Budget Type.</div>"},
                        funds:{ selectNone: "<div class='reqF_1'>Please select Source of Fund.</div>"}
                    }
                });

                ////The following code has been used to adding validation of dropdown list in basic validation plugin
                $.validator.addMethod('selectNone',
                    function (value, element)
                    {
                        if (value == 0 || value =='' || value =='select' || value.indexOf('--') != -1)
                        {
                            //alert("not ok");
                            return false;
                        }
                        else
                        {
                            //alert("ok")
                            return true;
                        }
                    }
                );
                //End Form Validation

            }); // End Document.Ready

    </script>

    </head>

    <%

    PrequalificationExcellBean preqExcellBean = null;
    int tenolId = 0;
    int corrigendumNo=0;
    String action = "create";
    String agencyCombo = "";
    boolean isEdit = false;
    TblTenderDetailsOffline tenderDetailsOffline = null;
    List<TblCorrigendumDetailOffline> corrigendumDetailOffline = null;
    List<Object[]> corrigendumDetails = null;

         if( request.getParameter("submit") != null && "Submit".equals(request.getParameter("submit"))){
         try{
             System.out.println("condition ok");
            String ministry = CommonUtils.checkNull(request.getParameter("hidministry"));
            String agency = CommonUtils.checkNull(request.getParameter("hidorg"));
            String peName = CommonUtils.checkNull(request.getParameter("peName"));
            String peCode = CommonUtils.checkNull(request.getParameter("peCode"));
            String district = CommonUtils.checkNull(request.getParameter("district"));
            String nature = CommonUtils.checkNull(request.getParameter("nature"));
            String procType = CommonUtils.checkNull(request.getParameter("procType"));
            String invitationFor = CommonUtils.checkNull(request.getParameter("invitationFor"));
            String invitationRefNo = CommonUtils.checkNull(request.getParameter("invitationRefNo"));
            String issuedate = CommonUtils.checkNull(request.getParameter("issuedate"));
            String procureMethod = CommonUtils.checkNull(request.getParameter("procureMethod"));
            String budget = CommonUtils.checkNull(request.getParameter("budget"));
            String funds = CommonUtils.checkNull(request.getParameter("funds"));
            String devPartner = CommonUtils.checkNull(request.getParameter("devPartner"));
            String projectCode = CommonUtils.checkNull(request.getParameter("projectCode"));
            String ProjectName = CommonUtils.checkNull(request.getParameter("projectName"));
            String packageNo = CommonUtils.checkNull(request.getParameter("packageNo"));
            String packageName = CommonUtils.checkNull(request.getParameter("packageName"));
            String tenderpublicationDate = CommonUtils.checkNull(request.getParameter("tenderpublicationDate"));
            String preQualCloseDate = CommonUtils.checkNull(request.getParameter("preQualCloseDate"));
            String placeofPQMeeting = CommonUtils.checkNull(request.getParameter("placeofPQMeeting"));
            String preTenderMeetStartDate = CommonUtils.checkNull(request.getParameter("preTenderMeetStartDate"));
            String sellingDocPrinciple = CommonUtils.checkNull(request.getParameter("sellingDocPrinciple"));
            String sellingDocOthers = CommonUtils.checkNull(request.getParameter("sellingDocOthers"));
            String receivingPQ = CommonUtils.checkNull(request.getParameter("receivingPQ"));
            String eligibilityofTenderer = CommonUtils.checkNull(request.getParameter("eligibilityofTenderer"));
            String briefDescGoods = CommonUtils.checkNull(request.getParameter("briefDescGoods"));
            String briefDescRelServices = CommonUtils.checkNull(request.getParameter("briefDescRelServices"));
            String preQualDocPrice = CommonUtils.checkNull(request.getParameter("preQualDocPrice"));
            String lotNo_1 = CommonUtils.checkNull(request.getParameter("lotNo_1"));
            String identification_1 = CommonUtils.checkNull(request.getParameter("identification_1"));
            String locationlot_1 = CommonUtils.checkNull(request.getParameter("locationlot_1"));
            String complTimeLotNo_1 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_1"));
            String lotNo_2 = CommonUtils.checkNull(request.getParameter("lotNo_2"));
            String identification_2 = CommonUtils.checkNull(request.getParameter("identification_2"));
            String locationlot_2 = CommonUtils.checkNull(request.getParameter("locationlot_2"));
            String complTimeLotNo_2 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_2"));
            String lotNo_3 = CommonUtils.checkNull(request.getParameter("lotNo_3"));
            String identification_3 = CommonUtils.checkNull(request.getParameter("identification_3"));
            String locationlot_3 = CommonUtils.checkNull(request.getParameter("locationlot_3"));
            String complTimeLotNo_3 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_3"));
            String lotNo_4 = CommonUtils.checkNull(request.getParameter("lotNo_4"));
            String identification_4 = CommonUtils.checkNull(request.getParameter("identification_4"));
            String locationlot_4 = CommonUtils.checkNull(request.getParameter("locationlot_4"));
            String complTimeLotNo_4 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_4"));
            String nameOfficial = CommonUtils.checkNull(request.getParameter("nameOfficial"));
            String designationOfficial = CommonUtils.checkNull(request.getParameter("designationOfficial"));
            String addressOfOfficial = CommonUtils.checkNull(request.getParameter("addressOfOfficial"));
            String contactDetail = CommonUtils.checkNull(request.getParameter("contactDetail"));

            //TblTenderDetailsOffline tenderDetailsOffline = null;
            List<TblTenderLotPhasingOffline> lots = null ;
            
            if( request.getParameter("action") != null && request.getParameter("tenid") != null && "Create".equals(request.getParameter("action"))
                    &&  Integer.parseInt(request.getParameter("tenid")) != 0){
                    tenolId = Integer.parseInt(request.getParameter("tenid"));

                    tenderDetailsOffline = offlineDataSrBean.getTblTenderDetailOfflineDataById(tenolId);

                    int i = 0;
                    //System.out.println(""+tenderDetailsOffline.getTenderLotsAndPhases().size());
                    //System.out.println(""+tenderDetailsOffline.getTenderLotsAndPhases());

                    while(i < tenderDetailsOffline.getTenderLotsAndPhases().size()){
                        tenderDetailsOffline.getTenderLotsAndPhases().remove(i);
                    }
                    lots = tenderDetailsOffline.getTenderLotsAndPhases();

            }else{
                    tenderDetailsOffline = new TblTenderDetailsOffline();
                    lots  = new ArrayList<TblTenderLotPhasingOffline>();
                }

            String userid = "";
            HttpSession hs = request.getSession();
            if (hs.getAttribute("userId") != null) {
                userid = hs.getAttribute("userId").toString();
                offlineDataSrBean.setLogUserId(userid);
            }

            /*tenderDetailsOffline.setMinistryOrDivision(ministry);
            tenderDetailsOffline.setAgency(agency);
            tenderDetailsOffline.setPeName(peName);
            tenderDetailsOffline.setPeCode(peCode);
            tenderDetailsOffline.setPeDistrict(district);*/

            tenderDetailsOffline.setProcurementNature(nature);
            tenderDetailsOffline.setProcurementType(procType);
            tenderDetailsOffline.setInvitationFor(invitationFor);
            tenderDetailsOffline.setReoiRfpRefNo(invitationRefNo);
           // System.out.println("issuedate >> "+issuedate);
            Date issDate = DateUtils.formatStdString(issuedate);
            tenderDetailsOffline.setIssueDate(issDate);
            tenderDetailsOffline.setProcurementMethod(procureMethod);
            tenderDetailsOffline.setBudgetType(budget);
            tenderDetailsOffline.setSourceOfFund(funds);
            tenderDetailsOffline.setDevPartners(devPartner);
            tenderDetailsOffline.setProjectCode(projectCode);
            tenderDetailsOffline.setProjectName(ProjectName);
            tenderDetailsOffline.setPackageNo(packageNo);
            tenderDetailsOffline.setPackageName(packageName);
           // System.out.println("tenderpublicationDate >> "+tenderpublicationDate);
            Date pubDate = DateUtils.formatStdString(tenderpublicationDate);
           // System.out.println("after convert date >> "+pubDate);
            tenderDetailsOffline.setTenderPubDate(pubDate);
            Date closeDate = DateUtils.convertDateToStr(preQualCloseDate);
            tenderDetailsOffline.setClosingDate(closeDate);
            tenderDetailsOffline.setPreTenderReoiplace(placeofPQMeeting);
            Date meetingDate = DateUtils.convertDateToStr(preTenderMeetStartDate);
            tenderDetailsOffline.setPreTenderReoidate(meetingDate);
            tenderDetailsOffline.setSellingAddPrinciple(sellingDocPrinciple);
            tenderDetailsOffline.setSellingAddOthers(sellingDocOthers);
            tenderDetailsOffline.setReceivingAdd(receivingPQ);
            tenderDetailsOffline.setEligibilityCriteria(eligibilityofTenderer);
            tenderDetailsOffline.setBriefDescription(briefDescGoods);
            tenderDetailsOffline.setRelServicesOrDeliverables(briefDescRelServices);
            tenderDetailsOffline.setEventType("TenderWithPQ");
            tenderDetailsOffline.setUserId(Integer.parseInt(userid));
            float docPrice = 0;
            if (preQualDocPrice != null && !"".equals(preQualDocPrice)) {
                docPrice = Float.parseFloat(preQualDocPrice);
            }
            tenderDetailsOffline.setDocumentPrice(new BigDecimal(docPrice).setScale(2, 0));
            tenderDetailsOffline.setPeOfficeName(nameOfficial);
            tenderDetailsOffline.setPeDesignation(designationOfficial);
            tenderDetailsOffline.setPeAddress(addressOfOfficial);
            tenderDetailsOffline.setPeContactDetails(contactDetail);


           // System.out.println("invitationFor >> "+invitationFor);
            if(invitationFor.equals("Single Lot")){
                System.out.println("single >> ");
                TblTenderLotPhasingOffline lotPhasingOffline = new TblTenderLotPhasingOffline();
                lotPhasingOffline.setLotOrRefNo(lotNo_1);
                lotPhasingOffline.setLocation(locationlot_1);
                lotPhasingOffline.setLotIdentOrPhasingServ(identification_1);
                lotPhasingOffline.setCompletionDateTime(complTimeLotNo_1);
                lotPhasingOffline.setTenderSecurityAmt(new BigDecimal(0).setScale(2, 0));
                lotPhasingOffline.setTenderDetailsOffline(tenderDetailsOffline);
               // lotPhasingOffline.setTenderLotPhaId(22);
                lots.add(lotPhasingOffline);
            }else if(invitationFor.equals("Multiple Lot")){
                System.out.println("multi lot ");
                 for(int i=1;i<=4;i++){

                if((request.getParameter("lotNo_"+i) != null || request.getParameter("identification_"+i) != null
                        || request.getParameter("locationlot_"+i) != null || request.getParameter("complTimeLotNo_"+i) != null)
                        && (!"".equals(request.getParameter("lotNo_"+i)) || !"".equals(request.getParameter("identification_"+i)) || !"".equals(request.getParameter("locationlot_"+i))
                        || !"".equals(request.getParameter("complTimeLotNo_"+i))) ){

                        TblTenderLotPhasingOffline lotPhasingOffline = new TblTenderLotPhasingOffline();
                        lotPhasingOffline.setLotOrRefNo(request.getParameter("lotNo_"+i));
                        lotPhasingOffline.setLocation(request.getParameter("locationlot_"+i));
                        lotPhasingOffline.setLotIdentOrPhasingServ(request.getParameter("identification_"+i));
                        lotPhasingOffline.setCompletionDateTime(request.getParameter("complTimeLotNo_"+i));
                        lotPhasingOffline.setTenderSecurityAmt(new BigDecimal(0).setScale(2, 0));
                        lotPhasingOffline.setTenderDetailsOffline(tenderDetailsOffline);
                        lots.add(lotPhasingOffline);
                }

               }                
               }
            tenderDetailsOffline.setTenderLotsAndPhases(lots);//LOT DETAILS ARE ADDED WITH THE TENDER DETAILS HERE
            if( request.getParameter("action") != null && request.getParameter("tenid") != null
                    &&  Integer.parseInt(request.getParameter("tenid")) != 0){
                
                tenolId = Integer.parseInt(request.getParameter("tenid"));
                System.out.println("tenolId >>>>>>>>>>> "+tenolId);
                tenderDetailsOffline.setTenderOfflineId(tenolId);

                corrigendumNo = Integer.parseInt(request.getParameter("corNo"));
                //System.out.println("corrigendumNo >>>>>>>>>>> "+corrigendumNo);

                if("Edit".equals(request.getParameter("action")))
                    isEdit=true;
                
                corrigendumTenderWithPQBean.createCorrigendum(tenderDetailsOffline, tenolId, Integer.parseInt(userid), corrigendumNo, isEdit);

                 response.sendRedirect(request.getContextPath() + "/offlinedata/TenderDashboardOfflineApproval.jsp");
              }            
           }catch(Exception ex){
           ex.printStackTrace();
           }
         }else{

        /*CREATION/MODIFICATION OF CORRIGENDUM STARTS HERE*/
        if( request.getParameter("action") != null){            
            if(request.getParameter("tenderOLId") != null ){
                 String tenderOLId = request.getParameter("tenderOLId");
                 tenolId = Integer.parseInt(tenderOLId);
             }

            if(request.getParameter("corNo") != null ){
                 String strCorNo = request.getParameter("corNo");
                 corrigendumNo = Integer.parseInt(strCorNo);
             }

            String userid = "";
            HttpSession hs = request.getSession();
            if (hs.getAttribute("userId") != null) {
                userid = hs.getAttribute("userId").toString();
                offlineDataSrBean.setLogUserId(userid);
            }
             action = "Create";
             preqExcellBean = (PrequalificationExcellBean) offlineDataSrBean.editTenderWithPQForm(tenolId);            
             agencyCombo = offlineDataSrBean.getOrganizationByMinistry(preqExcellBean.getMinistryName(), preqExcellBean.getAgency());

             if("Edit".equals(request.getParameter("action"))){
                 action = "Edit";                 
                 corrigendumDetailOffline = offlineDataSrBean.corrigendumDetails(Integer.parseInt(request.getParameter("tenderOLId")));
                 //corrigendumDetails = offlineDataSrBean.getCorrogendumDetails(Integer.parseInt(request.getParameter("tenderOLId")));

                 isEdit = true;
             }
         }
                
    %>


    <% TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    List<SPTenderCommonData> holidayList = tenderCommonService.returndata("getHolidayDatesBD", null, null);
    out.print("<script type='text/javascript'>");
    for(SPTenderCommonData holidays : holidayList){
        out.print("holiArray.push('"+holidays.getFieldName1()+"');");
    }
    out.print("</script>");
    %>

    <body >



        <input id="boolcheck" value="true" type="hidden"/>
        <div class="mainDiv">
            <div class="fixDiv">

        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->

        <div class="contentArea_1">
                    <div class="t-align-right" style="vertical-align: middle;"><a class="action-button-goback" href="<%=request.getContextPath()%>/offlinedata/TenderDashboardOfflineApproval.jsp">Go back</a></div>
                    <div class="pageHead_1">Corrigendum of Tender with Pre-qualification (PQ) </div>

                    <form id="frmCorrigendumTenderWithPQ" name="frmCorrigendumTenderWithPQ" method="POST" action="/offlinedata/CorrigendumTenderWithPQ.jsp">

                        <div class="tableHead_22 t_space">PROCURING ENTITY (PE) INFORMATION</div>
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">

                            <tbody><tr>
                                <td style="font-style: italic" colspan="4" class="ff t-align-left" align="left">

                                    Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Ministry/Division :</td>
                                <td width="25%"><%= CommonUtils.checkNull(preqExcellBean.getMinistryName()) %>
                                    <input id="hidministry"  name="hidministry" type="hidden" value="<%=CommonUtils.checkNull(preqExcellBean.getMinistryName())%>" />
                                </td>
                                
                                <td class="ff" width="25%">Organization : </td>
                                <td width="25%">                                    
                                    <%= CommonUtils.checkNull(preqExcellBean.getAgency()) %>
                                    <input type="hidden" name="hidorg"  id="hidorg" value="<%= CommonUtils.checkNull(preqExcellBean.getAgency()) %>" />
                                </td>
                                
                            </tr>
                             <tr>
                                <td class="ff">Procuring Entity Name : </td>
                                <td><%= CommonUtils.checkNull(preqExcellBean.getProcuringEntityName()) %></td>
                                <td class="ff">Procuring Entity Code :</td>
                                <td>
                                    <!--    <!-- *********** Old Code *************
                                     <%
                                        //if(preqExcellBean.getProcuringEntityCode() != null && !preqExcellBean.getProcuringEntityCode().equals("Not used at present")){
                                     %>
                                        <%//=CommonUtils.checkNull(preqExcellBean.getProcuringEntityCode()) %>
                                    <% //}else{ %>
                                    <input name="peCode" class="formTxtBox_1" id="txtPECode" style="width: 280px;" type="text" value="" />
                                    <% //} %>
                                        ************************ -->

                                    <!-- PE Code cannot be changed - 01.Sep.13 -->
                                    <%=CommonUtils.checkNull(preqExcellBean.getProcuringEntityCode())%>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Procuring Entity Dzongkhag / District :</td>                                
                               <td><%=preqExcellBean.getProcuringEntityDistrict()%>
                                     
                                </td>

                                <td class="ff">Procurement Category : <span>*</span></td>
                                <td>
                                     <select name="nature" class="formTxtBox_1" id="Nature" style="width: 200px;" onChange="setSerType(this,true);">
                                               <!-- <option value="select" selected="selected">--- Please Select ---</option>-->
                                               <%
                                               int count = 0;
                                               if (isEdit) {
                                                   for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                        if (detail.getFieldName().equals("procurementNature") && detail.getCorrigendumStatus().equals("Pending")) {
                                                        if(CommonUtils.checkNull(detail.getNewValue()).equals("Goods")){
                                                            out.println("<option selected=\"selected\" value=\"Goods\">Goods</option>");
                                                            out.println("<option value=\"Works\">Works</option>");
                                                        }else{
                                                            out.println("<option  value=\"Goods\">Goods</option>");
                                                            out.println("<option selected=\"selected\" value=\"Works\">Works</option>");
                                                        }
                                                        count++;
                                                        }
                                                   }                                                   
                                               }
                                              if (count == 0) {
                                               if(CommonUtils.checkNull(preqExcellBean.getProcurementNature()).equals("Goods")){
                                                    out.println("<option selected=\"selected\" value=\"Goods\">Goods</option>");
                                                    out.println("<option value=\"Works\">Works</option>");
                                               }else{
                                                    out.println("<option  value=\"Goods\">Goods</option>");
                                                    out.println("<option selected=\"selected\" value=\"Works\">Works</option>");
                                               }
                                               }

                                                %>


                                     </select>
                                    <br />
                                    <span style="color: red;" id="msgProcNature" class="cmbMsg"></span>
                                </td>
                            </tr>
                            <tr>

                                <td class="ff">Procurement Type :<span>*</span></td>
                                <td>
                                    <select name="procType" class="formTxtBox_1" id="Type" style="width: 200px;">
                                                 <%
                                               count = 0;
                                               if (isEdit) {
                                                   for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                        if (detail.getFieldName().equals("procurementType") && detail.getCorrigendumStatus().equals("Pending")) {
                                                        if(CommonUtils.checkNull(detail.getNewValue()).equals("NCT")){
                                                            out.println("<option selected=\"selected\" value=\"NCT\">NCB</option>");
                                                            out.println("<option value=\"ICT\">ICB</option>");
                                                        }else{
                                                            out.println("<option  value=\"NCT\">NCB</option>");
                                                            out.println("<option" + " selected=\"selected\" value=\"ICT\">ICB</option>");
                                                        }
                                                        count++;
                                                        }
                                                   }
                                               }
                                               if (count == 0) {
                                                 if(CommonUtils.checkNull(preqExcellBean.getProcurementType()).equals("NCT")){
                                                    out.println("<option selected=\"selected\" value=\"NCT\">NCB</option>");
                                                    out.println("<option value=\"ICT\">ICB</option>");
                                               }else{
                                                    out.println("<option  value=\"NCT\">NCB</option>");
                                                    out.println("<option" + " selected=\"selected\" value=\"ICT\">ICB</option>");
                                               }
                                                 }

                                                %>
                                                <!--<option value="select" selected="selected">--- Please Select ---</option> -->
                                     </select>
                                    <br />
                                    <span style="color: red;" id="msgProcType" class="cmbMsg"></span>
                                </td>


                                 <td class="ff">Invitation for : <span>*</span></td>
                                <td>
                                    <select name="invitationFor" class="formTxtBox_1" id="InvitationFor" style="width: 200px;" onchange="dataTable(this.value)" >
                                       <!-- <option value="select" selected="selected">--- Please Select ---</option> -->
                                        <%
                                        count = 0;
                                               if (isEdit) {
                                                   for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                        if (detail.getFieldName().equals("invitationFor") && detail.getCorrigendumStatus().equals("Pending")) {
                                                        if(CommonUtils.checkNull(detail.getNewValue()).equals("Single Lot")){
                                                            out.println("<option selected=\"selected\" value=\"Single Lot\">Single Lot</option>");
                                                            out.println("<option value=\"Multiple Lot\">Multiple Lot</option>");
                                                        }else{
                                                            out.println("<option  value=\"Single Lot\">Single Lot</option>");
                                                            out.println("<option selected=\"selected\" value=\"Multiple Lot\">Multiple Lot</option>");
                                                        }
                                                        count++;
                                                        }
                                                   }
                                               }
                                        if (count == 0) {
                                        if(CommonUtils.checkNull(preqExcellBean.getInvitationFor()).contains("Single Lot")){
                                            out.println("<option selected=\"selected\" value=\"Single Lot\">Single Lot</option>");
                                            out.println("<option value=\"Multiple Lot\">Multiple Lot</option>");
                                        }else{
                                            out.println("<option  value=\"Single Lot\">Single Lot</option>");
                                            out.println("<option selected=\"selected\" value=\"Multiple Lot\">Multiple Lot</option>");
                                        }
                                        }
                                        %>
                                    </select>
                                    <br />
                                    <span style="color: red;" id="msgInvitationFor" class="cmbMsg"></span>
                                </td>
                            </tr>
                            <tr>

                                <td class="ff">Event Type :</td>
                                <td><label id="lblEventType">PQ</label></td>

                                <td class="ff">Invitation Reference No. : <span>*</span></td>
                                <td><input name="invitationRefNo" class="formTxtBox_1" id="txtinvitationRefNo" style="width: 280px;" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("reoiRfpRefNo") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue().trim());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getInvitationRefNo()) %><%}%>" type="text" />
                                    <span class="reqF_1" id="spantxtinvitationRefNo"></span>
                                    <input id="hdnmsgTender" name="hdnmsgTenderName" value="PQ" type="hidden">
                                </td>

                            </tr>
                            <tr>
                                <td class="ff">Date : <span>*</span></td>
                               <td class="formStyle_1"><input name="issuedate" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("IssueDate") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(preqExcellBean.getTenderissuingdate()) %><%}%>" class="formTxtBox_1" id="txtissuedate" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtissuedate','txtissuedate');" onblur="findHoliday(this,0);" type="text">
                                    <img id="imgtxtissuedate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtissuedate','imgtxtissuedate');" border="0">
                                    <span id="span1"></span>
                                </td>

                                <td class="ff"></td>
                                <td>


                                </td>
                            </tr>
                        </tbody></table>

                                <div class="tableHead_22 ">Key Information and Funding Information :</div>
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td class="ff" width="25%">Procurement Method : <span>*</span></td>
                                <td width="25%"><input value="OTM" id="hdnProcurementMethod" name="hdnProcurementMethod" type="hidden">
                                     <select name="procureMethod" class="formTxtBox_1" style="width: 200px;" id="cmbProcureMethod">
                                                <!-- <option value="select" selected="selected">--- Please Select ---</option> -->
                                                <%
                                                count = 0;
                                                if (isEdit) {
                                                    for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                        if (detail.getFieldName().equals("procurementMethod") && detail.getCorrigendumStatus().equals("Pending")) {
                                                            if(CommonUtils.checkNull(detail.getNewValue()).contains("Open")){
                                                                out.println("<option selected=\"selected\" value=\"Open Tendering Method\">Open Tendering Method</option>");
                                                                out.println("<option value=\"Two Stage Tendering Method\">Two Stage Tendering Method</option>");
                                                            }else{
                                                                out.println("<option value=\"Open Tendering Method\">Open Tendering Method</option>");
                                                                out.println("<option selected=\"selected\" value=\"Two Stage Tendering Method\">Two Stage Tendering Method</option>");
                                                        }
                                                            count++;
                                                        }
                                                    }
                                                }
                                                if (count == 0) {

                                                if(CommonUtils.checkNull(preqExcellBean.getProcurementMethod()).contains("Open")){
                                                out.println("<option selected=\"selected\" value=\"Open Tendering Method\">Open Tendering Method</option>");
                                                out.println("<option value=\"Two Stage Tendering Method\">Two Stage Tendering Method</option>");
                                                }else{
                                                out.println("<option value=\"Open Tendering Method\">Open Tendering Method</option>");
                                                out.println("<option selected=\"selected\" value=\"Two Stage Tendering Method\">Two Stage Tendering Method</option>");
                                                }
                                                }
                                                %>
                                     </select>
                                    <input type="hidden" name="hdnPM" id="hdnPM" value=""/>
                                    <br />
                                    <span style="color: red;" id="msgProcMethod" class="cmbMsg"></span>
                                </td>
                                <td class="ff" width="25%">Budget Type : <span>*</span></td>
                                <td width="25%">
                                    <select name="budget" class="formTxtBox_1" id="Budget" style="width: 200px;">
                                               <!--  <option value="select" selected="selected">--- Please Select ---</option> -->
                                               <%
                                               count = 0;
                                                if (isEdit) {
                                                    for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                        if(detail.getCorrigendumStatus().equals("Pending")){
                                                        if (detail.getFieldName().equals("budgetType")) {
                                                            if(CommonUtils.checkNull(detail.getNewValue()).equals("Revenue Budget")){
                                                                out.println("<option selected=\"selected\" value=\"Revenue Budget\">Revenue Budget</option>");
                                                                out.println("<option value=\"Development Budget\">Development Budget</option>");
                                                                out.println("<option value=\"Own Funds\">Own Funds</option>");
                                                            }else if(CommonUtils.checkNull(detail.getNewValue()).equals("Development Budget")){
                                                                out.println("<option  value=\"Revenue Budget\">Revenue Budget</option>");
                                                                out.println("<option selected=\"selected\" value=\"Development Budget\">Development Budget</option>");
                                                                out.println("<option value=\"Own Funds\">Own Funds</option>");
                                                            }
                                                            else{
                                                                    out.println("<option  value=\"Revenue Budget\">Revenue Budget</option>");
                                                                    out.println("<option  value=\"Development Budget\">Development Budget</option>");
                                                                    out.println("<option selected=\"selected\" value=\"Own Funds\">Own Funds</option>");
                                                                }
                                                            count++;
                                                        }
                                                      }
                                                    }
                                                }
                                               if (count == 0) {
                                               if(CommonUtils.checkNull(preqExcellBean.getCbobudget()).equals("Revenue Budget")){
                                                   out.println("<option selected=\"selected\" value=\"Revenue Budget\">Revenue Budget</option>");
                                                   out.println("<option value=\"Development Budget\">Development Budget</option>");
                                                   out.println("<option value=\"Own Funds\">Own Funds</option>");
                                               }else if(CommonUtils.checkNull(preqExcellBean.getCbobudget()).equals("Development Budget")){
                                                   out.println("<option  value=\"Revenue Budget\">Revenue Budget</option>");
                                                   out.println("<option selected=\"selected\" value=\"Development Budget\">Development Budget</option>");
                                                   out.println("<option value=\"Own Funds\">Own Funds</option>");
                                               }else{
                                                   out.println("<option  value=\"Revenue Budget\">Revenue Budget</option>");
                                                   out.println("<option  value=\"Development Budget\">Development Budget</option>");
                                                   out.println("<option selected=\"selected\" value=\"Own Funds\">Own Funds</option>");
                                               }
                                               }
                                                %>
                                     </select>
                                    <br />
                                    <span style="color: red;" id="msgBudgetType" class="cmbMsg"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Source of Funds : <span>*</span></td>
                                <td width="25%">
                                    <select name="funds" class="formTxtBox_1" id="Funds" style="width: 200px;">
                                             <%
                                               count = 0;
                                                if (isEdit) {
                                                    for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                        if(detail.getCorrigendumStatus().equals("Pending")){
                                                        if (detail.getFieldName().equals("sourceOfFund")) {
                                                            if(CommonUtils.checkNull(detail.getNewValue()).equals("Government")){
                                                                out.println("<option selected=\"selected\" value=\"Government\">Government</option>");
                                                                out.println("<option value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                                out.println("<option value=\"Own Funds\">Own Funds</option>");
                                                            }else if(CommonUtils.checkNull(detail.getNewValue()).contains("Aid Grant / Credit")){
                                                                out.println("<option  value=\"Government\">Government</option>");
                                                                out.println("<option selected=\"selected\" value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                                out.println("<option value=\"Own Funds\">Own Funds</option>");
                                                            }
                                                            else{
                                                                out.println("<option  value=\"Government\">Government</option>");
                                                                out.println("<option value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                                out.println("<option selected=\"selected\" value=\"Own Funds\">Own Funds</option>");
                                                                }
                                                            count++;
                                                        }
                                                        }
                                                    }
                                                }
                                               if (count == 0) {
                                               if(CommonUtils.checkNull(preqExcellBean.getCbosource()).equals("Government")){
                                                   out.println("<option selected=\"selected\" value=\"Government\">Government</option>");
                                                   out.println("<option value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                   out.println("<option value=\"Own Funds\">Own Funds</option>");
                                               }else if(CommonUtils.checkNull(preqExcellBean.getCbosource()).equals("Aid Grant / Credit")){
                                                   out.println("<option  value=\"Government\">Government</option>");
                                                   out.println("<option selected=\"selected\" value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                   out.println("<option value=\"Own Funds\">Own Funds</option>");
                                               }else{
                                                   out.println("<option  value=\"Government\">Government</option>");
                                                   out.println("<option value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                   out.println("<option selected=\"selected\" value=\"Own Funds\">Own Funds</option>");
                                               }
                                               }
                                                %>
                                        <!-- <option value="select" selected="selected">--- Please Select ---</option-->>
                                     </select>
                                    <br />
                                    <span style="color: red;" id="msgSourceOfFund" class="cmbMsg"></span>
                                </td>
                                <td class="ff" width="25%">Development Partner : </td>

                                <td width="25%">
                                    <input id="txtDevPartner" class="formTxtBox_1" type="text" style="width: 280px;" name="devPartner" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("devPartners") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(preqExcellBean.getDevelopmentPartner()) %><%}%>" />
                                </td>
                            </tr>

                        </tbody></table>
                        <div class="tableHead_22 ">Particular Information :</div>
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                 <td class="ff" width="25%">Project Code : </td>
                                <td width="25%">
                                    <input name="projectCode" class="formTxtBox_1" id="txtProjectCode" style="width: 280px;" type="text" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("projectCode") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(preqExcellBean.getProjectOrProgrammeCode()) %><%}%>" />
                                </td>
                                <td class="ff" width="25%">Project Name : </td>
                                <td width="25%">
                                    <input name="projectName" class="formTxtBox_1" id="txtProjectName" style="width: 280px;" type="text" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("projectName") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(preqExcellBean.getProjectOrProgrammeName()) %><%}%>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Tender Package No. : <span>*</span> </td>
                                <td width="25%">
                                    <input name="packageNo" class="formTxtBox_1" id="txtPackageNo" style="width: 280px;" type="text" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("packageNo") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(preqExcellBean.getTenderPackageNo()) %><%}%>" />
                                </td>
                                <td class="ff" width="25%">Tender Package Name : <span>*</span></td>
                                <td width="25%">
                                    <input name="packageName" class="formTxtBox_1" id="txtPackageName" style="width: 280px;" type="text" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("PackageName") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(preqExcellBean.getTenderPackageName()) %><%}%>" />
                                </td>
                            </tr>
                            <tr>

                                <td class="ff">Scheduled Pre-Qualification Publication<br>Date : <span>*</span></td>
                                <td class="formStyle_1"><input name="tenderpublicationDate" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("tenderPubDate") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getPrequalificationPublicationDate()) %><%}%>" class="formTxtBox_1" id="txttenderpublicationDate" style="width: 100px;" readonly="true" value="" onfocus="GetCalWithouTime('txttenderpublicationDate','txttenderpublicationDate');" onblur="findHoliday(this,0);" type="text">
                                    <img id="txttenderpublicationDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txttenderpublicationDate','txttenderpublicationDateimg');" border="0">
                                    <span id="spantxttenderpublicationDate"></span>
                                </td>

                                <td class="ff">Pre-Qualification  Closing<br>Date and Time : <span>*</span></td>
                                <td class="formStyle_1"><input name="preQualCloseDate" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("ClosingDate") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getPrequalificationClosingDateandTime()) %><%}%>" class="formTxtBox_1" id="txtpreQualCloseDate" style="width: 100px;" readonly="true" onfocus="GetCal('txtpreQualCloseDate','txtpreQualCloseDate');" onblur="findHoliday(this,4);" type="text">
                                    <img id="txtpreQualCloseDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCal('txtpreQualCloseDate','txtpreQualCloseDateimg');" border="0">
                                    <span id="spantxtpreQualCloseDate"></span>
                                </td>

                            </tr>

                            <tr>
                               <td class="ff">Place of Pre - Qualification Meeting :</td>
                                <td class="formStyle_1">
                                    <textarea name="placeofPQMeeting" rows="2" cols="20" id="txtPlaceofPQMeeting"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("PreTenderREOIPlace") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getPlace_Date_TimeofPrequalificationMeeting()) %><%}%></textarea>
                                </td>

                                <td class="ff">Pre - Qualification Meeting<br>Date and Time : <span></span>
                                    <input id="hdncheck" value="Yes" type="hidden">
                                </td>
                                <td class="formStyle_1"><input name="preTenderMeetStartDate" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("preTenderREOIDate") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull( preqExcellBean.getDateTimeofPrequalificationMeeting()) %><%}%>" class="formTxtBox_1" id="txtpreTenderMeetStartDate" style="width: 100px;" readonly="true" onfocus="GetCal('txtpreTenderMeetStartDate','txtpreTenderMeetStartDate');" onblur="findHoliday(this,2);" type="text">
                                    <img id="txtpreTenderMeetStartDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCal('txtpreTenderMeetStartDate','txtpreTenderMeetStartDateimg');" border="0">
                                    <span id="spantxtpreTenderMeetStartDate"></span>
                                </td>
                            </tr>

                            <tr>
                                <td class="ff">Selling Pre-qualification Document (Principle) : <span>*</span></td>
                                <td class="formStyle_1">
                                    <input id="txtSellingDocPrinciple" class="formTxtBox_1" type="text" style="width: 280px;" name="sellingDocPrinciple" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("SellingAddPrinciple") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getSellingPrequalificationDocumentPrincipal() )%><%}%>" />
                                </td>

                                <td class="ff">Selling Pre-qualification Document (Others) : </td>
                                <td class="formStyle_1">
                                    <input id="txtSellingDocOthers" class="formTxtBox_1" type="text" style="width: 280px;" name="sellingDocOthers" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("SellingAddOthers") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getSellingPrequalificationDocumentOthers()) %><%}%>" />
                                </td>
                            </tr>

                            <tr>
                                <td class="ff">Receiving Pre-qualification : <span>*</span></td>
                                <td class="formStyle_1">
                                    <input id="txtReceivingPQ" class="formTxtBox_1" type="text" style="width: 280px;" name="receivingPQ" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("ReceivingAdd") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull( preqExcellBean.getReceivingPrequalification()) %><%}%>" />
                                </td>

                                <td class="ff"></td>
                                <td class="formStyle_1"></td>
                            </tr>

                        </tbody></table>

                        <div class="tableHead_22 ">Information for Bidder/Consultant :</div>

                        <table class="formStyle_1 " width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td class="ff" width="25%"><input id="hdnevenyType" value="Works" type="hidden">Eligibility of Bidder/Consultant : <span>*</span></td>
                                <td colspan="2">
                                    <textarea style="display: none;" cols="100" rows="5" id="txtaeligibilityofTenderer" name="eligibilityofTenderer" class="ckeditor"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("EligibilityCriteria") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getEligibilityofApplicant()) %><%}%></textarea>
                                    
                                    <span id="spantxtaeligibilityofTenderer"></span>
                                </td>

                            </tr>
                            <tr>
                                <td class="ff">Brief Description of Goods or Works : <span>*</span></td>
                                <td><input value="Works" id="briefValMsg" type="hidden">
                                    <textarea style="display: none;" cols="100" rows="5" id="txtabriefDescGoods" name="briefDescGoods" class="ckeditor"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("BriefDescription") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getBriefDescriptionofGoodsorWorks()) %><%}%></textarea>
                                    
                                    <span id="spantxtabriefDescGoods"></span>
                                </td><td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td class="ff">Brief Description of Related Services : <span>*</span></td>
                                <td><input value="Works" id="briefValMsg" type="hidden">
                                    <textarea style="display: none;" cols="100" rows="5" id="txtBriefDescRelServices" name="briefDescRelServices" class="ckeditor"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("RelServicesOrDeliverables") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getBriefDescriptionofRelatedServices()) %><%}%></textarea>
                                    
                                    <span id="spantxtabriefDescGoods"></span>
                                </td><td>&nbsp;</td>
                            </tr>

                            <tr style="display: table-row;" id="docsprice">
                                    <td class="ff">Pre-Qualification Document Price (In Nu.)
                		        : <span>*</span></td>
                                    <td><input name="preQualDocPrice" onblur="documentPrice(this);" class="formTxtBox_1" id="txtpreQualDocPrice" style="width: 200px;" type="text" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("DocumentPrice") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getPrequalificationDocumentPrice()) %><%}%>" />
                                    <div id="preQualDocPriceInWords"></div>
                                    <span id="spantxtpreQualDocPriceman"></span>
                                </td>
                                <td>&nbsp;</td>
                            </tr>

                        </tbody></table>

                        <!--<div class="b_space" align="right" id="addRemoveLot">
                            <a id="addRow" class="action-button-add" onclick="AddRow()">Add Lot</a>
                            <a id="delRow" class="action-button-delete" onclick="RemoveRow()">Remove lot</a>
                        </div>-->

                        <table class="tableList_1 t_space" width="100%" cellspacing="0" id="tblLotDescription">
                            <tbody>
                            <tr>
                               <th class="t-align-left" width="6%">Lot No.</th>

                                <th class="t-align-left" width="54%">Identification of Lot <span class="mandatory">*</span> </th>

                                <th class="t-align-center" width="10%">Location</th>

                                <th class="t-align-center" width="10%">Completion Time in weeks/months </th>
                            </tr>
                            <tr>
                            <td class="t-align-center">

                                    <input name="lotNo_1" class="formTxtBox_1" id="txtLotNo_1" type="text" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Number") && detail.getLotorPhaseIdentity().equals("1")  && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(preqExcellBean.getLotNo_1()) %><%}%>" >
                                </td>
                                <td class="t-align-center">

                                     <textarea name="identification_1" rows="2" cols="40" id="txtidentification_1" ><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Identification") && detail.getLotorPhaseIdentity().equals("1") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(preqExcellBean.getIdentificationOfLot_1()) %><%}%></textarea>

                                </td>
                                <td class="t-align-center"><input name="locationlot_1" style="width: 180px;" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Location") && detail.getLotorPhaseIdentity().equals("1") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(preqExcellBean.getLocation_1()) %><%}%>" class="formTxtBox_1" id="txtlocationlot_1" onblur="chkLocLotBlank(this);" type="text"><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_1" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Completion Time") && detail.getLotorPhaseIdentity().equals("1") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(preqExcellBean.getCompletionTimeInWeeksORmonths_1()) %><%}%>"  class="formTxtBox_1" id="txtcomplTimeLotNo_1" style="width: 100px;"  type="text"><span id="compLot_0" style="color: red;">&nbsp;</span>

                                </td>
                            </tr>
                            <tr id="lot_1" style="<%if(isEdit){boolean lotFlag = false; String lot = "";
                            for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                         if (detail.getFieldName().equals("invitationFor"))
                                                                                lotFlag = true;
                                                                        if(lotFlag == true){
                                                                                lot = detail.getNewValue().toString();
                                                                                if (lot.equals("Multiple Lot"))
                                                                                    {%>display:table-row;<% 
                                                                                     lotFlag = false;
                                                                                }
                                                                                else{%>display:none;<%}
                                                                                }}
                                                                         if(lotFlag == false && lot.equals(""))
                                                                         { if(CommonUtils.checkNull(preqExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> display:table-row;<%}}

                                                                           
                                                                        } else {if(CommonUtils.checkNull(preqExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> display:table-row;<%}}%>" >
                                <td class="t-align-center"  >
                                    <input name="lotNo_2" class="formTxtBox_1" id="txtLotNo_2" type="text" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Number") && detail.getLotorPhaseIdentity().equals("2") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= preqExcellBean.getLotNo_2() %><%}%>" />
                                </td>
                                <td class="t-align-center">

                                     <textarea name="identification_2" rows="2" cols="40" id="txtidentification_2" ><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Identification") && detail.getLotorPhaseIdentity().equals("2") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(preqExcellBean.getIdentificationOfLot_2()) %><%}%></textarea>

                                </td>
                                <td class="t-align-center"><input name="locationlot_2" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Location") && detail.getLotorPhaseIdentity().equals("2") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getLocation_2()) %><%}%>" style="width: 180px;" class="formTxtBox_1" id="txtLocationlot_2" onblur="chkLocLotBlank(this);" type="text"><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_2" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Completion Time") && detail.getLotorPhaseIdentity().equals("2") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getCompletionTimeInWeeksORmonths_2()) %><%}%>"  class="formTxtBox_1" id="txtcomplTimeLotNo_2" style="width: 100px;"  type="text"><span id="compLot_0" style="color: red;">&nbsp;</span>

                                </td>
                            </tr>
                            <tr id="lot_2" style="<%if(isEdit){boolean lotFlag = false; String lot = "";
                            for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                         if (detail.getFieldName().equals("invitationFor"))
                                                                                lotFlag = true;
                                                                        if(lotFlag == true){
                                                                                lot = detail.getNewValue().toString();
                                                                                if (lot.equals("Multiple Lot"))
                                                                                    {%>display:table-row;<%
                                                                                     lotFlag = false;
                                                                                }
                                                                                else{%>display:none;<%}
                                                                                }}
                                                                         if(lotFlag == false && lot.equals(""))
                                                                         { if(CommonUtils.checkNull(preqExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> display:table-row;<%}}


                                                                        } else {if(CommonUtils.checkNull(preqExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> display:table-row;<%}}%>" >
               <td class="t-align-center">
                                    <input name="lotNo_3" class="formTxtBox_1" id="txtLotNo_3" type="text" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Number") && detail.getLotorPhaseIdentity().equals("3") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getLotNo_3()) %><%}%>" />
                                </td>
                                <td class="t-align-center">

                                     <textarea name="identification_3" rows="2" cols="40" id="txtidentification_3" ><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Identification") && detail.getLotorPhaseIdentity().equals("3") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(preqExcellBean.getIdentificationOfLot_3())%><%}%></textarea>

                                </td>
                                <td class="t-align-center"><input name="locationlot_3" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Location") && detail.getLotorPhaseIdentity().equals("3") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getLocation_3()) %><%}%>" style="width: 180px;" class="formTxtBox_1" id="txtLocationlot_3" onblur="chkLocLotBlank(this);" type="text"><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_3" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Completion Time") && detail.getLotorPhaseIdentity().equals("3") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getCompletionTimeInWeeksORmonths_3()) %><%}%>"  class="formTxtBox_1" id="txtcomplTimeLotNo_3" style="width: 100px;"  type="text"><span id="compLot_0" style="color: red;">&nbsp;</span>

                                </td>
                            </tr>
                            <tr id="lot_3" style="<%if(isEdit){boolean lotFlag = false; String lot = "";
                            for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                         if (detail.getFieldName().equals("invitationFor"))
                                                                                lotFlag = true;
                                                                        if(lotFlag == true){
                                                                                lot = detail.getNewValue().toString();
                                                                                if (lot.equals("Multiple Lot"))
                                                                                    {%>display:table-row;<%
                                                                                     lotFlag = false;
                                                                                }
                                                                                else{%>display:none;<%}
                                                                                }}
                                                                         if(lotFlag == false && lot.equals(""))
                                                                         { if(CommonUtils.checkNull(preqExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> display:table-row;<%}}


                                                                        } else {if(CommonUtils.checkNull(preqExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> display:table-row;<%}}%>" >
               <td class="t-align-center">
                                    <input name="lotNo_4" class="formTxtBox_1" id="txtLotNo_4" type="text" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Number") && detail.getLotorPhaseIdentity().equals("4") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull( preqExcellBean.getLotNo_4()) %><%}%>" />
                                </td>
                                <td class="t-align-center">

                                     <textarea name="identification_4" rows="2" cols="40" id="txtidentification_4"  ><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Identification") && detail.getLotorPhaseIdentity().equals("4") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getIdentificationOfLot_4()) %><%}%></textarea>

                                </td>
                                <td class="t-align-center"><input name="locationlot_4" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Location") && detail.getLotorPhaseIdentity().equals("4") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getLocation_4()) %><%}%>" style="width: 180px;" class="formTxtBox_1" id="txtLocationlot_4" onblur="chkLocLotBlank(this);" type="text"><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_4" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Completion Time") && detail.getLotorPhaseIdentity().equals("4") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getCompletionTimeInWeeksORmonths_4()) %><%}%>" class="formTxtBox_1" id="txtcomplTimeLotNo_4" style="width: 100px;"  type="text"><span id="compLot_0" style="color: red;">&nbsp;</span>

                                </td>
                            </tr>


                        </tbody></table>


                        <div class="tableHead_22 t_space">Procuring Entity Details :</div>
                        <table class="formStyle_1 t_space" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                    <td class="ff" width="25%">Name of Official Inviting  Pre-Qualification : <span>*</span></td>
                                <td width="25%">
                                    <input name="nameOfficial" class="formTxtBox_1" id="txtNameOfficial" style="width: 280px;" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("peName") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getNameofOfficialInvitingPrequalification()) %><%}%>" type="text">
                                </td>
                                    <td class="ff" width="26%"> Designation of Official Inviting  Pre-Qualification : <span>*</span></td>
                                <td width="25%">
                                    <input name="designationOfficial" class="formTxtBox_1" id="txtDesignationOfficial" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("peDesignation") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getDesignationofOfficialInvitingPrequalification()) %><%}%>" style="width: 280px;" type="text">
                                </td>
                            </tr>

                            <tr>
                                <td class="ff">Address of Official Inviting  Pre-Qualification : </td>
                                <td>
                                    <textarea name="addressOfOfficial" rows="2" cols="30" id="txtAddress"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("peAddress") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getAddressofOfficialInvitingPrequalification()) %><%}%></textarea>
                                </td>
                                <td class="ff">Contact details of Official Inviting  Pre-Qualification :</td>
                                <td>
                                    <textarea name="contactDetail" rows="2" cols="30" id="txtContact"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("peContactDetails") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(preqExcellBean.getContactdetailsofOfficialInvitingPrequalification()) %><%= CommonUtils.checkNull(preqExcellBean.getFaxNo()) %><%= CommonUtils.checkNull(preqExcellBean.getE_mail()) %><%}%></textarea>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4" class="ff mandatory">The procuring entity reserves the right to accept or reject all Tenders/Proposals / Pre-Qualifications / EOIs</td>
                            </tr>
                        </tbody></table>

                        <table class="formStyle_1 t_space" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td colspan="4" align="center">
                                    <label class="formBtn_1"><input name="submit" id="btnsubmit" value="Submit" type="submit">
                                        <input name="hdnbutton" id="hdnbutton" value="" type="hidden">
                                        <input name="action" id="action"  value="<%=action %>" type="hidden" />
                                        <input name="tenid" id="tenid"  value="<%=tenolId %>" type="hidden" />
                                        <input name="corNo" id="corNo"  value="<%=corrigendumNo %>" type="hidden" />
                                    </label>&nbsp;&nbsp;

                                </td>
                            </tr>
                        </tbody></table>
                        <div>&nbsp;</div>
                    </form>
                    <!--Dashboard Content Part End-->
<% } %>


                    <script type="text/javascript">
                        function loadOrganization() {
                            //var deptId= 0;
                           // var districtId = $('#cmbDistrict').val();
                           //if($('#cmbDivision').val()>0){
                            //   deptId=$('#cmbDivision').val();
                           //}
                           //else{deptId= $('#cmbMinistry').val(); }

                            var deptId= $('#cmbMinistry').val();
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: deptId, funName:'organizationCombo'},  function(j){
                            $('#cmbOrganization').children().remove().end()
                            $("select#cmbOrganization").html(j);
                            var orgObj =document.getElementById("cmbMinistry");
                            var orgval = orgObj.options[orgObj.selectedIndex].text;
                           document.getElementById("hidministry").value = orgval;
                        });
                    }
                    </script>

                    <script language="javascript" type="text/javascript">
                        function setSerType(obj,flag){

                            if("Works"==obj.options[obj.selectedIndex].text){
                               // $('#trSerType').hide();
                               // $('#trRFReq').hide();
                               // $('#trPQReq').show();
                               // if(flag){
                                 //   document.getElementById("cmbPQRequires").options[0].selected = "selected";
                               // }
                                $.post("<%=request.getContextPath()%>/APPServlet", {projectId:'No',param4:$('#hdnPM').val(),procNature:$('#cmbProcureNature').val(),funName:'getPM'},  function(j){
                                    $("select#cmbProcureMethod").html(j);
                                });
                                //$('#trLotBtn').show();

                            }
                            else {
                                $('#trSerType').hide();
                                $('#trRFReq').hide();
                                $('#trPQReq').hide();
                                $.post("<%=request.getContextPath()%>/APPServlet", {projectId:'No',procType:$('#cmbProcureType').val(),param4: $('#hdnPM').val(),procNature:$('#cmbProcureNature').val(),funName:'getPM'},  function(j){
                                    $("select#cmbProcureMethod").html(j);
                                });
                                //$('#trLotBtn').show();

                            }
                        }
                    </script>

                    <script type="text/javascript">
                        function GetCal(txtname,controlname)
                        {
                            new Calendar({
                                inputField: txtname,
                                trigger: controlname,
                                showTime: 24,
                                onSelect: function() {
                                    var date = Calendar.intToDate(this.selection.get());
                                    LEFT_CAL.args.min = date;
                                    LEFT_CAL.redraw();
                                    this.hide();
                                    document.getElementById(txtname).focus();
                                }
                            });

                            var LEFT_CAL = Calendar.setup({
                                weekNumbers: false
                            })
                        }

                        function GetCalWithouTime(txtname,controlname)
                        {
                            new Calendar({
                                inputField: txtname,
                                trigger: controlname,
                                showTime: false,
                                dateFormat:"%d/%m/%Y",
                                onSelect: function() {
                                    var date = Calendar.intToDate(this.selection.get());
                                    LEFT_CAL.args.min = date;
                                    LEFT_CAL.redraw();
                                    this.hide();
                                    document.getElementById(txtname).focus();
                                }
                            });

                            var LEFT_CAL = Calendar.setup({
                                weekNumbers: false
                            })
                        }

                    </script>

                    <script type="text/javascript">

                        //Function for Required
                        function required(controlid)
                        {
                            var temp=controlid.length;
                            if(temp <= 0 ){
                                return false;
                            }else{
                                return true;
                            }
                        }

                        //Function for MaxLength
                        function Maxlenght(controlid,maxlenght)
                        {
                            var temp=controlid.length;
                            if(temp>=maxlenght){
                                return false;
                            }else
                                return true;
                        }

                        //Function for digits
                        function digits(control) {
                            return /^\d+$/.test(control);
                        }

                        function CompareToForEqual(value,params)
                        {

                            var mdy = value.split('/')  //Date and month split
                            var mdyhr=mdy[2].split(' ');  //Year and time split
                            var mdyp = params.split('/')
                            var mdyphr=mdyp[2].split(' ');
                            var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                            return Date.parse(date) == Date.parse(datep);
                        }

                        //Function for CompareToForToday
                        function CompareToForToday(first)
                        {
                            var mdy = first.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var mdyhrtime=mdyhr[1].split(':');
                            if(mdyhrtime[1] == undefined){
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                            }else
                            {
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                            }

                            var d = new Date();
                            if(mdyhrtime[1] == undefined){
                                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                            }
                            else
                            {
                                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(),d.getHours(),d.getMinutes());
                            }
                            return Date.parse(valuedate) > Date.parse(todaydate);
                        }

                        //Function for CompareToForGreater
                        function CompareToForGreater(value,params)
                        {
                            if(value!='' && params!=''){

                                var mdy = value.split('/')  //Date and month split
                                var mdyhr=mdy[2].split(' ');  //Year and time split
                                var mdyp = params.split('/')
                                var mdyphr=mdyp[2].split(' ');


                                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                                {
                                    //alert('Both Date');
                                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                                }
                                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                                {
                                    //alert('Both DateTime');
                                    var mdyhrsec=mdyhr[1].split(':');
                                    var date =  new Date( mdyhr[0], parseFloat(mdy[1])-1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var mdyphrsec=mdyphr[1].split(':');

                                    var datep =  new Date(mdyphr[0], parseFloat(mdyp[1])-1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                            }
                                else
                                {
                                    //alert('one Date and One DateTime');
                                    var a = mdyhr[1];  //time
                                    var b = mdyphr[1]; // time

                                    if(a == undefined && b != undefined)
                                    {
                                        //alert('First Date');
                                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                                        var mdyphrsec=mdyphr[1].split(':');
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                    }
                                    else
                                    {
                                        //alert('Second Date');
                                        var mdyhrsec=mdyhr[1].split(':');
                                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                                    }
                                }
                                return Date.parse(date) > Date.parse(datep);
                            }
                            else
                            {
                                return false;
                            }
                        }

                        //Function for CompareToForGreater
                        function CompareToForSmaller(value,params)
                        {
                            if(value!='' && params!=''){

                                var mdy = value.split('/')  //Date and month split
                                var mdyhr=mdy[2].split(' ');  //Year and time split
                                var mdyp = params.split('/')
                                var mdyphr=mdyp[2].split(' ');


                                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                                {
                                    //alert('Both Date');
                                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                                }
                                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                                {
                                    //alert('Both DateTime');
                                    var mdyhrsec=mdyhr[1].split(':');
                                    var date =  new Date( mdyhr[0], parseFloat(mdy[1])-1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var mdyphrsec=mdyphr[1].split(':');
                                    var datep =  new Date(mdyphr[0], parseFloat(mdyp[1])-1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                }
                                else
                                {
                                    //alert('one Date and One DateTime');
                                    var a = mdyhr[1];  //time
                                    var b = mdyphr[1]; // time

                                    if(a == undefined && b != undefined)
                                    {
                                        //alert('First Date');
                                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                                        var mdyphrsec=mdyphr[1].split(':');
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                    }
                                    else
                                    {
                                        //alert('Second Date');
                                        var mdyhrsec=mdyhr[1].split(':');
                                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                                    }
                                }

                                return Date.parse(date) < Date.parse(datep);
                            }
                            else
                            {
                                return false;
                            }
                        }

                        //Function for CompareToWithoutEqual
                        function CompareToWithoutEqual(value,params)
                        {
                            var mdy = value.split('/')  //Date and month split
                            var mdyhr=mdy[2].split(' ');  //Year and time split
                            var mdyp = params.split('/')
                            var mdyphr=mdyp[2].split(' ');


                            if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                            {
                                //alert('Both Date');
                                var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                                var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                            }
                            else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                            {
                                //alert('Both DateTime');
                                var mdyhrsec=mdyhr[1].split(':');
                                var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                var mdyphrsec=mdyphr[1].split(':');
                                var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                            }
                            else
                            {
                                //alert('one Date and One DateTime');
                                var a = mdyhr[1];  //time
                                var b = mdyphr[1]; // time

                                if(a == undefined && b != undefined)
                                {
                                    //alert('First Date');
                                    var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                                    var mdyphrsec=mdyphr[1].split(':');
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                }
                                else
                                {
                                    //alert('Second Date');
                                    var mdyhrsec=mdyhr[1].split(':');
                                    var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                                }
                            }
                            return Date.parse(date) > Date.parse(datep);
                        }

                    </script>


                </div>
            </div>

            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>

    </body>

</html>
