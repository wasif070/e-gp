<%-- 
    Document   : viewAwardedContract
    Created on : 26-Aug-2012, 15:17:23
    Author     : Ahsan
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.servicebean.AwardedContractOfflineSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
            <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
          <script src="../resources/js/jQuery/jquery-1.5.1.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
         <script type="text/javascript" src="../ckeditor/config.js"></script>
        <link href="../ckeditor/editor.css" type="text/css" rel="stylesheet">

        <script type="text/javascript" src="../resources/js/jquery.dataTables.js"></script>
        <link href="../resources/css/demo_table.css" type="text/css" rel="stylesheet">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Search Awarded Contract</title>

        <script type="text/javascript">
        $(document).ready(function () {
         
           //BindGrid("false","Pending",'','','','','',''); //load datatable for alltype of category and file type.Default load.
            BindGrid("false","Pending",'','','','','','','');
           $('#btnSearch').click(function() {
           var procNature = $('#procNature').val();
           var procMethod = $('#procMethod').val();
           var refNo = $('#refNo').val();
           var value = $('#value').val();
           var advDateFrom =  $('#txtAdvDatefrom').val();
           var advDateTo =  $('#txtAdvDateto').val();
           var status = $('#status').val();
           var ministry = "";
           if(($('#cmbOrganization').val() != "-- Select Organization --") && ($('#cmbOrganization').val() != "0"))
               {
                   ministry = $('#cmbOrganization').val();
               }
           else
                ministry = $('#cmbMinistry').val();
           if(status == 'Approved')
               {
                    $("#approveTab").addClass("sMenu");
                    $("#pendingTab").removeClass("sMenu");
               }
           else if(status == 'Pending')
               {
                    $("#approveTab").removeClass("sMenu");
                    $("#pendingTab").addClass("sMenu");
               }

           var flag = CompareDate();
               
           if(flag)
                BindGrid('true',status,procNature,procMethod,refNo,value,advDateFrom,advDateTo,ministry);
          
           // BindGrid('true',status,procNature,procMethod,refNo);
           });

           $('#btnReset').click(function(){
                   // reset();
                    BindGrid("false","Pending",'','','','','','','');
                })
                
           $("#frmFileUpload").validate({
                    rules: {
                       contractValue: {number: true}
                    },
                    messages: {
                        contractValue: { number: "<div class='reqF_1'>Numeric Only</div>"}
                    }
                });
        });

         function CompareDate(){
                var pubDtfrm = document.getElementById('txtAdvDatefrom').value;
                var pubDtTo = document.getElementById('txtAdvDateto').value;
                if(pubDtfrm!=null && pubDtTo!=null){
                    if(pubDtfrm!='' && pubDtTo!=''){
                        var mdy = pubDtfrm.split('/')  //Date and month split
                        var mdyhr= mdy[2].split(' ');  //Year and time split

                        var mdy1 = pubDtTo.split('/')  //Date and month split
                        var mdyhr1= mdy1[2].split(' ');  //Year and time split

                        if(mdyhr[1] == undefined){
                            var dt_pubDateFrm = new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                        }

                        if(mdyhr1[1] == undefined){
                            var dt_pubDateTo =new Date(mdyhr1[0], mdy1[1]-1, mdy1[0]);
                        }

                        if(Date.parse(dt_pubDateFrm) < Date.parse(dt_pubDateTo)){
                            document.getElementById('valAll').innerHTML='';

                            return true;
                        }else{
                            document.getElementById('valAll').innerHTML='Advertising Date From should be less then Advertising Date To';

                            return false;
                        }
                    }
                }
                return true;
            }
            
         function alertOffline(){
             alert("Alert the message");
          //   /viewContractAwardOfflineServlet?action=Delete&refNo
         }

         function confirmation()
            {
                var del = confirm("Are you sure you want to delete the Awarded Contract?");
                return del;
            }
            
         function changeTab(tabNo){
                if(tabNo == 1){
                    $("#approveTab").removeClass("sMenu");
                    $("#pendingTab").addClass("sMenu");
                     BindGrid("false","Pending",'','','','','','','');
                }
                else if(tabNo == 2){
                    $("#approveTab").addClass("sMenu");
                    $("#pendingTab").removeClass("sMenu");
                     BindGrid("false","Approved",'','','','','','','');
                }
         }
                //bind dataTable
       function BindGrid(searchFlag,status,procNature,procMethod,refNo,value,advDateFrom,advDateTo,ministry) {
            if (typeof oTable == 'undefined') {
                oTable = $('#gridPlan').dataTable({
                    "sDom": '<rt"top"><"bottom"li><"clear">',
                    //"bPaginate": false,
                    "sPaginationType": "full_numbers",
                    "bProcessing": true,
                    "bFilter": true,
                    //"aaSorting": [],
                    "aoColumns": [null, null, null, null, null, null, null, null,null],
                    "sAjaxSource": "<%=request.getContextPath()%>/viewContractAwardOfflineServlet?action=bindGrid",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": sSource,
                            "data": { "status": status,"procNature":procNature,"procMethod":procMethod,"searchFlag":searchFlag,"refNo":refNo,"value":value,"advDateFrom":advDateFrom,"advDateTo":advDateTo,"ministry":ministry },
                            //"data": { "status": status },
                            "success": fnCallback
                        });
                    }
                });
            }
            else {
                $('#gridPlan').dataTable({
                  "sDom": '<rt"top"><"bottom"li><"clear">',
                  // "bPaginate": false,
                    "sPaginationType": "full_numbers",
                    "bProcessing": true,
                   //  "bFilter": true,
                    //"bServerSide": true,
                    "bDestroy": true,
                    "bRegex":true,
                    //"aaSorting": [],
                    "aoColumns": [null, null, null, null, null, null, null, null,null],
                    "sAjaxSource": "<%=request.getContextPath()%>/viewContractAwardOfflineServlet?action=bindGrid",
                    "fnServerData": function (sSource, aoData, fnCallback) {
                        $.ajax({
                            "dataType": 'json',
                            "type": "POST",
                            "url": sSource,
                            "data": { "status": status,"procNature":procNature,"procMethod":procMethod,"searchFlag":searchFlag,"refNo":refNo,"value":value,"advDateFrom":advDateFrom,"advDateTo":advDateTo,"ministry":ministry },
                            //"data": { "status": status },
                            "success": fnCallback
                        });
                    }
                });

            }
        } //end of Bind

        function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            function GetCalWithouTime(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            function setOrgval(id){

                var orgObj =document.getElementById(id);
                var orgval = orgObj.options[orgObj.selectedIndex].text;

                document.getElementById("hidorg").value = orgval;
            }
            function loadOrganization() {
                        var deptId= 0;
                       // var districtId = $('#cmbDistrict').val();
                       if($('#cmbDivision').val()>0){
                           deptId=$('#cmbDivision').val();
                       }
                       else{deptId= $('#cmbMinistry').val(); }

                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: deptId, funName:'offlineAwardedOrgCombo'},  function(j){
                            $('#cmbOrganization').children().remove().end()
                            $("select#cmbOrganization").html(j);
                            var orgObj =document.getElementById("cmbMinistry");
                             var orgval = orgObj.options[orgObj.selectedIndex].text;
                            // document.getElementById("hidministry").value = orgval;
                        });
                    }
        </script>
         <style type="text/css">

             .table-style a{
                text-decoration: underline;
                color: #FF9326;
                }
             .table-style
             {
                

             }
         </style>
    </head>
    <body>
            <div class="mainDiv">
            <div class="dashboard_div">
                 <%@include file="../resources/common/AfterLoginTop.jsp" %>
              
                 <!--<table width="100%" border="0" cellspacing="0" cellpadding="0">-->
                    <!--<tr valign="top">-->
                        <div class="contentArea_1">
                            <div class="pageHead_1">Search Awarded Contract</div>
                            <form id="frmFileUpload" name="frmFileUpload" method="POST" action="">
                                <div class="formBg_1">
                                    <jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>
                                    <table id="tblSearchBox" cellspacing="10" class="formStyle_1" width="100%">
                                               <tr>
                                                    <td class="ff">Ministry/Division :</td>
                                                    <%
                               // ManageEmployeeGridSrBean manageEmployeeGridSrBean = new ManageEmployeeGridSrBean();
                              //  List<TblDepartmentMaster> departmentMasterList = null;
                              //  departmentMasterList = manageEmployeeGridSrBean.getDepartmentMasterList("Ministry");
                                AwardedContractOfflineSrBean awardedContractOffline = new AwardedContractOfflineSrBean();
                                List<Object[]> ministryListOffline = new ArrayList<Object[]>();
                                ministryListOffline = awardedContractOffline.getMinistryForAwardedContractOffline();

                            %>
                                                   <!-- <td colspan="3"><input type="hidden" name="viewType" id="viewType" value="Live"/>
                                                        <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 300px;"
                                                               id="txtdepartment" onblur="showHide();checkCondition();"  readonly style="width: 200px;"/>
                                                        <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                                        <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=homeallTenders', '', 'width=350px,height=400px,scrollbars=1','');">
                                                            <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                        </a>
                                                    </td>-->
                                                   <td width="33%">
                                                <select name="cmbMinistry" id="cmbMinistry" class="formTxtBox_1" style="width:208px;" onchange="loadOrganization();">
                                                    <option value="" selected="selected">-- Select Ministry --</option>
                                                    <%
                                                    for (int i = 0; i < ministryListOffline.size(); i++)  {
                                                          out.println("<option value='" + String.valueOf(ministryListOffline.get(i)) + "'>" + String.valueOf(ministryListOffline.get(i)) + "</option>");
                                                    }

                                           %>

                                                </select>
                                            </td>
                                                </tr>
                                                <tr>
                                                  <td class="ff" width="30%">Organization : </td>
                                                <td>
                                                     <select class="formTxtBox_1" style="width: 208px;"  name="Organization" id="cmbOrganization" onchange="setOrgval(this.id);" >
                                                            <option value="0" selected="selected">-- Select Organization --</option>
                                                    </select>
                                                     <input class="formTxtBox_1" style="width: 280px;"  type="hidden" name="hidorg"  id="hidorg"  />
                                                </td>
                                                </tr>
                                        <tr>
                                            <td width="17%" class="ff">Procurement Category :</td>
                                            <td width="33%">
                                                <select name="procNature" id="procNature" class="formTxtBox_1" id="select2" style="width:208px;">
                                                    <option value="" selected="selected">-- Select Category --</option>
                                                    <option value="Goods">Goods</option>
                                                    <option value="Works">Works</option>
                                                    <option value="Services">Services</option>
                                                </select>
                                            </td>
                                           <!-- <td width="17%" class="ff">
                                                <input type="hidden" id="status" value="Pending"/>
                                                <input type="hidden" id="statusTab" value="Approved"/><!--bug id :: 1397 Live -->
                                            <!--</td>-->
                                            <td width="33%"></td>
                                        </tr>
                                        <!--<tr>
                                            <td class="ff">Procurement Type : </td>
                                            <td>
                                                <select name="procType" class="formTxtBox_1" id="cmbType" style="width:208px;">
                                                    <option value="">-- Select Type --</option>
                                                    <option value="NCT">NCT</option>
                                                    <option value="ICT">ICT</option>
                                                </select>
                                            </td>
                                            <td class="ff"></td>
                                            <td></td>
                                        </tr>-->
                                        <tr>
                                            <td class="ff">Procurement Method :</td>
                                            <td>
                                                <select name="procMethod" class="formTxtBox_1" id="procMethod" style="width:208px;">
                                                    <option value="" selected="selected">- Select Procurement Method -</option>
                                                         <option value="NCT">NCB</option>
                                                         <option value="ICT">ICB</option>
                                                        <!-- <option value="OTM">Open Tendering Method (OTM)</option>
                                                         <option value="TSTM">Two Stage Tendering Method (TSTM)</option>
                                                         <option value="RFQ">Request For Quotation (RFQ)</option>-->
                                                           <option value="QCBS">Quality Cost Based Selection(QCBS)</option>
                                                           <option value="SFB">Selection under a Fixed Budget(SFB)</option>
                                                           <option value="LCS">Least Cost Selection(LCS)</option>
                                                           <option value="SCSO">Selection Community Service Organisation(SCSO)</option>
                                                           <option value="SSS">Single Source Selection(SSS)</option>
                                                           <option value="SIS">Selection of Individual Consultant(SIS)</option>

                                                </select>
                                            </td>
                                            <td class="ff"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="ff">Value(In Nu.) :</td>
                                            <td><input type="text" name ="contractValue" class="formTxtBox_1" id="value" style="width:202px;" /></td>
                                            <td class="ff">Reference No :</td>
                                            <td><input type="text" class="formTxtBox_1" id="refNo" style="width:200px;" /></td>
                                        </tr>
                                        <tr>
                                            <td class="ff">Advertising Date From :</td>
                                             <td>
                                                <!--<input value="" name="txtNOADate" id="txtNOADate" type="text"/>-->

                                                <input name="txtAdvDatefrom" class="formTxtBox_1" id="txtAdvDatefrom" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtAdvDatefrom','txtAdvDatefrom');" type="text">
                                                <img id="imgAdvDatefroming" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtAdvDatefrom','imgAdvDatefroming');" border="0">

                                            </td>
                                            <td class="ff">Advertising Date To :</td>
                                             <td>
                                                <!--<input value="" name="txtNOADate" id="txtNOADate" type="text"/>-->
                                                <input name="txtAdvDateto" class="formTxtBox_1" id="txtAdvDateto" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtAdvDateto','txtAdvDateto');" type="text">
                                                <img id="imgAdvDateto" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtAdvDateto','imgAdvDateto');" border="0">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="16%" class="ff">Status :</td>
                                            <td width="32%">
                                                <select name="status" class="formTxtBox_1" id="status" style="width:85px;" >
                                                  
                                                    <option value="Pending" selected="selected">Pending</option>
                                                    <option value="Approved">Approved</option>
                                                </select>
                                            </td>
                                            <td width="15%" class="ff"></td>
                                            <td width="38%"></td>
                                        </tr>
                                        <tr>
                                            <td class="t-align-center" colspan="4">
                                                <div id="valAll" class="reqF_1"></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="ff">&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td class="ff">&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center"><label class="formBtn_1">
                                                    <input type="button" name="button" id="btnSearch" value="Search" />
                                                </label>
                                                &nbsp;
                                                <label class="formBtn_1">
                                                    <input type="reset" name="Reset" id="btnReset" value="Reset" />
                                                </label></td>
                                        </tr>
                                        <!--<tr>
                                            <td class="t-align-center" colspan="4">
                                                <div id="valAll" class="reqF_1"></div>
                                            </td>
                                        </tr>-->
                                    </table>
                                    <div class="tableHead_1 t_space">Awarded Contracts Search Results</div>
                                    <ul class="tabPanel_1 t_space" id="tabForApproved">
                                            <li><a href="javascript:void(0);" id="pendingTab" onclick="changeTab(1);" class="sMenu" >Pending</a></li>
                                            <li><a href="javascript:void(0);" id="approveTab" onclick="changeTab(2); ">Approved</a></li>
                                    </ul>
                                   
                                    <div id="gridContainer">
                                        <table cellpadding="0" cellspacing="0" border="1" class="display table-style" id="gridPlan">
                                            <thead>
                                                <tr>
                                                    <th width="3%">Sl. No.</th>
                                                    <th width="15%">Ministry & Division</th>
                                                    <th width="18%">Ref No.,Title & <br/> Advertising Date</th>
                                                    <th width="15%">Procuring Entity <br/> Procurement Method</th>
                                                    <th width="8%">Dzongkhag / District</th>
                                                    <th width="10%">Date of Letter of Acceptance</th>
                                                    <th width="10%">Contract award to</th>
                                                    <th width="7%">Value<br/>(Million Nu.)</th>
                                                    <th width="15%">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                               <!-- </div> -->
                            </div>
                        </form>
                   </div>
                                  <%@include file="../resources/common/Bottom.jsp" %>
            </div>
         </div>
    </body>
</html>
