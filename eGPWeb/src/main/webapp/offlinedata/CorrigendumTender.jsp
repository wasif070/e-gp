<%-- 
    Document   : CorrigendumTender
    Created on : 10-Sep-2012, 12:21:36
    Author     : salahuddin
--%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotPhasingOffline"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetailsOffline"%>
<%@page import="com.cptu.egp.eps.web.utility.CommonUtils"%>
<%@page import="com.cptu.egp.eps.web.offlinedata.TenderFormExcellBean"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblStateMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.model.table.TblDepartmentMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ManageEmployeeGridSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblCorrigendumDetailOffline"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>

<jsp:useBean id="offlineDataSrBean" class="com.cptu.egp.eps.web.offlinedata.OfflineDataSrBean"/>
<jsp:useBean id="corrigendumTenderBean" class="com.cptu.egp.eps.web.offlinedata.CorrigendumTenderBean"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Corrigendum Tender without Pre-qualification (PQ)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>

        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>

    <script src="resources/config.js" type="text/javascript"></script>
    <link href="resources/editor.css" type="text/css" rel="stylesheet"/>
    <script src="resources/en.js" type="text/javascript"></script>
       <!--

        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>

        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />

       -->

       <!--jquery validator max length css change - Dohatec-->
       <style type="text/css">
           label.error{color:red}
       </style>

       <script type="text/javascript">

            function dataTable(selVal){                
                    if(selVal == "Single Lot"){

                        //alert(document.getElementById("lot_1").style.display);
                        document.getElementById("lot_1").style.display = "none";
                        document.getElementById("lot_2").style.display = "none";
                        document.getElementById("lot_3").style.display = "none";
                    }else {
                        document.getElementById("lot_1").style.display = "table-row";
                        document.getElementById("lot_2").style.display = "table-row";
                        document.getElementById("lot_3").style.display = "table-row";

                       /* document.getElementById("txtLotNo_2").value ='';
                        document.getElementById("txtLotID_2").value = '';
                        document.getElementById("txtLocationlot_2").value = '';
                        document.getElementById("txtTenderSecurity_2").value = '';
                        document.getElementById("txtComplTime_2").value='';

                        document.getElementById("txtLotNo_3").value ='';
                        document.getElementById("txtLotID_3").value = '';
                        document.getElementById("txtLocationlot_3").value = '';
                        document.getElementById("txtTenderSecurity_3").value = '';
                        document.getElementById("txtComplTime_3").value='';
                        
                        document.getElementById("txtLotNo_4").value ='';
                        document.getElementById("txtLotID_4").value = '';
                        document.getElementById("txtLocationlot_4").value = '';
                        document.getElementById("txtTenderSecurity_4").value = '';
                        document.getElementById("txtComplTime_4").value=''; */
                    }

            }
            

            function setOrgval(id){

                var orgObj =document.getElementById(id);
                var orgval = orgObj.options[orgObj.selectedIndex].text;

                document.getElementById("hidorg").value = orgval;
            } 

           $(document).ready(function(){

                //Form Validation
                $("#frmCreateTender").validate({
                    rules: {
                        //textbox
                        peName: {required: true, maxlength: 150},
                        invitationRefNo: {required: true, maxlength: 50},
                        issuedate: {required: true},
                        packageNo: {required: true, maxlength: 50},
                        packageName: {required: true, maxlength: 150},
                        tenderpublicationDate: {required: true},
                        tenderCloseDate: {required: true},
                        tenderOpenDate: {required: true},
                        tenderLastSellDate: {required: true},
                        tenderDocPrice: {required: true, number:true, maxlength: 15},
                        LotNo: {required: true},
                        DescriptionForPQ: {required: true},
                        locationlot_0: {required: true},
                        startLotNo_0: {required: true},
                        complTimeLotNo_0: {required: true},
                        nameOfficial: {required: true, maxlength: 200},
                        designationOfficial: {required: true, maxlength: 200},
                        eligibilityofTenderer:{required: true}, //
                        briefDescGoods:{required: true}, //
                        briefDescRelServices:{required: true}, //
                        receivingTenDoc :{required: true, maxlength: 2000},
                        openTenderDoc :{required: true, maxlength: 2000},
                        sellingDocPrinciple:{required: true, maxlength: 2000},
                        
                        peCode:{maxlength: 15},
                        devPartner:{maxlength: 300},
                        projectCode:{maxlength: 150},
                        projectName:{maxlength: 150},
                        sellingDocOthers:{maxlength: 2000},
                        contactDetail:{maxlength: 1000},
                        addressOfOfficial:{maxlength: 5000},
                        placeofPQMeeting:{maxlength: 2000},

                        lotNo_1:{maxlength: 150},
                        identification_1:{required: true, maxlength: 2000},
                        locationlot_1:{maxlength: 100},
                        tenderSecurity_1:{maxlength: 15},
                        complTimeLotNo_1:{maxlength: 100},
                        lotNo_2:{maxlength: 150},
                        identification_2:{maxlength: 2000},
                        locationlot_2:{maxlength: 100},
                        tenderSecurity_2:{maxlength: 15},
                        complTimeLotNo_2:{maxlength: 100},
                        lotNo_3:{maxlength: 150},
                        identification_3:{maxlength: 2000},
                        locationlot_3:{maxlength: 100},
                        tenderSecurity_3:{maxlength: 15},
                        complTimeLotNo_3:{maxlength: 100},
                        lotNo_4:{maxlength: 150},
                        identification_4:{maxlength: 2000},
                        locationlot_4:{maxlength: 100},
                        tenderSecurity_4:{maxlength: 15},
                        complTimeLotNo_4:{maxlength: 100},

                        //Dropdown
                        eventType:{selectNone: true},
                        nature:{selectNone: true},
                        procType:{selectNone: true},
                        invitationFor:{selectNone: true},
                        procureMethod:{selectNone: true},
                        budgetType:{selectNone: true},
                        sourceFunds:{selectNone: true}

                    },
                    messages: {
                        //Textbox
                        peName: { required: "<div class='reqF_1'>Please enter PE Name.</div>"},
                        invitationRefNo: { required: "<div class='reqF_1'>Please enter Reference No.</div>"},
                        issuedate: { required: "<div class='reqF_1'>Please select Issue Date.</div>"},
                        packageNo: { required: "<div class='reqF_1'>Please enter Package No.</div>"},
                        packageName: { required: "<div class='reqF_1'>Please enter Package Name.</div>"},
                        tenderpublicationDate: { required: "<div class='reqF_1'>Please select Publication Date.</div>"},
                        tenderCloseDate: { required: "<div class='reqF_1'>Please select Tender Closing Date.</div>"},
                        tenderOpenDate: { required: "<div class='reqF_1'>Please select Tender Opening Date.</div>"},
                        tenderLastSellDate: { required: "<div class='reqF_1'>Please select Tender Last Selling Date.</div>"},
                        tenderDocPrice: { required: "<div class='reqF_1'>Please enter Tender Document Price.</div>", number: "<div class='reqF_1'>Incorrect Format</div>"},
                        LotNo: { required: "<div class='reqF_1'>Please enter Lot Number.</div>"},
                        DescriptionForPQ: { required: "<div class='reqF_1'>Please enter Lot Description.</div>"},
                        locationlot_0: { required: "<div class='reqF_1'>Please enter Lot Location.</div>"},
                        startLotNo_0: { required: "<div class='reqF_1'>Please select Staring Date.</div>"},
                        complTimeLotNo_0: { required: "<div class='reqF_1'>Please select Completion Date.</div>"},
                        nameOfficial: { required: "<div class='reqF_1'>Please enter the Name.</div>"},
                        designationOfficial: { required: "<div class='reqF_1'>Please enter Designation.</div>"},
                        eligibilityofTenderer:{ required: "<div class='reqF_1'>Please enter Eligibility of Tenderer.</div>"},
                        briefDescGoods:{ required: "<div class='reqF_1'>Please enter Description of Goods or Works.</div>"},
                        briefDescRelServices:{ required: "<div class='reqF_1'>Please enter Description of Related Services.</div>"},
                        receivingTenDoc :{ required: "<div class='reqF_1'>Please enter Receiving Tender.</div>"},
                        openTenderDoc :{ required: "<div class='reqF_1'>Please enter Opening Tender.</div>"},
                        sellingDocPrinciple:{ required: "<div class='reqF_1'>Please enter Selling Tender Document.</div>"},
                        identification_1:{required: "<div class='reqF_1'>Please enter Identification of Lot.</div>"},

                        //Dropdown
                        eventType:{ selectNone: "<div class='reqF_1'>Please select Event Type.</div>"},
                        nature:{ selectNone: "<div class='reqF_1'>Please select Procurement Category.</div>"},
                        procType:{ selectNone: "<div class='reqF_1'>Please select Procurement Type.</div>"},
                        invitationFor:{ selectNone: "<div class='reqF_1'>Please select Lot Type.</div>"},
                        procureMethod:{ selectNone: "<div class='reqF_1'>Please select Procurement Method.</div>"},
                        budgetType:{ selectNone: "<div class='reqF_1'>Please select Budget Type.</div>"},
                        sourceFunds:{ selectNone: "<div class='reqF_1'>Please select Source of Fund.</div>"}

                    }
                });

                ////The following code has been used to adding validation of dropdown list in basic validation plugin
                $.validator.addMethod('selectNone',
                    function (value, element)
                    {
                        if (value == 0 || value =='' || value =='select' || value.indexOf('--') != -1)
                        {
                            //alert("not ok");
                            return false;
                        }
                        else
                        {
                            //alert("ok")
                            return true;
                        }
                    }
                );
                //End Form Validation

            }); // End Document.Ready

       </script>

        <script type="text/javascript">
            var holiArray = new Array();
        </script>
      </head>
      <%
            //System.out.println("submit value  >> "+request.getParameter("submit"));
            TenderFormExcellBean tenderFormExcellBean = null;
            int tenolId = 0;
            int corrigendumNo=0;
            String action = "create";
            String invitationFor = "";
            String agencyCombo = "";
            boolean isEdit = false;
            List<TblCorrigendumDetailOffline> corrigendumDetailOffline = null;
        if( request.getParameter("submit") != null && "Submit".equals(request.getParameter("submit"))){
            try{
                //System.out.println("in condition ");
                String ministry = CommonUtils.checkNull(request.getParameter("hidministry"));
                String agency = CommonUtils.checkNull(request.getParameter("hidorg"));
                String peName = CommonUtils.checkNull(request.getParameter("peName"));
                String peCode = CommonUtils.checkNull(request.getParameter("peCode"));
                String district = CommonUtils.checkNull(request.getParameter("district"));
                String nature = CommonUtils.checkNull(request.getParameter("nature"));
                String procType = CommonUtils.checkNull(request.getParameter("procType"));
                String eventType = CommonUtils.checkNull(request.getParameter("eventType"));
                invitationFor = CommonUtils.checkNull(request.getParameter("invitationFor"));
                String invitationRefNo = CommonUtils.checkNull(request.getParameter("invitationRefNo"));
                String issuedate = CommonUtils.checkNull(request.getParameter("issuedate"));
                String procureMethod = CommonUtils.checkNull(request.getParameter("procureMethod"));
                String budget = CommonUtils.checkNull(request.getParameter("budgetType"));
                String funds = CommonUtils.checkNull(request.getParameter("sourceFunds"));
                String devPartner = CommonUtils.checkNull(request.getParameter("devPartner"));
                String projectCode = CommonUtils.checkNull(request.getParameter("projectCode"));
                String ProjectName = CommonUtils.checkNull(request.getParameter("projectName"));
                String packageNo = CommonUtils.checkNull(request.getParameter("packageNo"));
                String packageName = CommonUtils.checkNull(request.getParameter("packageName"));
                String tenderpublicationDate = CommonUtils.checkNull(request.getParameter("tenderpublicationDate"));
                String tenderLastSellDate = CommonUtils.checkNull(request.getParameter("tenderLastSellDate"));
                String tenderCloseDate = CommonUtils.checkNull(request.getParameter("tenderCloseDate"));
                String tenderOpenDate = CommonUtils.checkNull(request.getParameter("tenderOpenDate"));
                String placeofPQMeeting = CommonUtils.checkNull(request.getParameter("placeofPQMeeting"));
                String preTenderMeetStartDate = CommonUtils.checkNull(request.getParameter("preTenderMeetStartDate"));
                String sellingDocPrinciple = CommonUtils.checkNull(request.getParameter("sellingDocPrinciple"));
                String sellingDocOthers = CommonUtils.checkNull(request.getParameter("sellingDocOthers"));
                String receivingTenDoc = CommonUtils.checkNull(request.getParameter("receivingTenDoc"));
                String openTenderDoc = CommonUtils.checkNull(request.getParameter("openTenderDoc"));
                String eligibilityofTenderer = CommonUtils.checkNull(request.getParameter("eligibilityofTenderer"));
                String briefDescGoods = CommonUtils.checkNull(request.getParameter("briefDescGoods"));
                String briefDescRelServices = CommonUtils.checkNull(request.getParameter("briefDescRelServices"));
                String tenderDocPrice = CommonUtils.checkNull(request.getParameter("tenderDocPrice"));
                String lotNo_1 = CommonUtils.checkNull(request.getParameter("lotNo_1"));
                String identification_1 = CommonUtils.checkNull(request.getParameter("identification_1"));
                String locationlot_1 = CommonUtils.checkNull(request.getParameter("locationlot_1"));
                String tenderSecurity_1 = CommonUtils.checkNull(request.getParameter("tenderSecurity_1"));
                String complTimeLotNo_1 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_1"));
                String lotNo_2 = CommonUtils.checkNull(request.getParameter("lotNo_2"));
                String identification_2 = CommonUtils.checkNull(request.getParameter("identification_2"));
                String locationlot_2 = CommonUtils.checkNull(request.getParameter("locationlot_2"));
                String tenderSecurity_2 = CommonUtils.checkNull(request.getParameter("tenderSecurity_2"));
                String complTimeLotNo_2 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_2"));
                String lotNo_3 = CommonUtils.checkNull(request.getParameter("lotNo_3"));
                String identification_3 = CommonUtils.checkNull(request.getParameter("identification_3"));
                String locationlot_3 = CommonUtils.checkNull(request.getParameter("locationlot_3"));
                String tenderSecurity_3 = CommonUtils.checkNull(request.getParameter("tenderSecurity_3"));
                String complTimeLotNo_3 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_3"));
                String lotNo_4 = CommonUtils.checkNull(request.getParameter("lotNo_4"));
                String identification_4 = CommonUtils.checkNull(request.getParameter("identification_4"));
                String locationlot_4 = CommonUtils.checkNull(request.getParameter("locationlot_4"));
                String tenderSecurity_4 = CommonUtils.checkNull(request.getParameter("tenderSecurity_4"));
                String complTimeLotNo_4 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_4"));
                String nameOfficial = CommonUtils.checkNull(request.getParameter("nameOfficial"));
                String designationOfficial = CommonUtils.checkNull(request.getParameter("designationOfficial"));
                String addressOfOfficial = CommonUtils.checkNull(request.getParameter("addressOfOfficial"));
                String contactDetail = CommonUtils.checkNull(request.getParameter("contactDetail"));

            TblTenderDetailsOffline tenderDetailsOffline = null;
            List<TblTenderLotPhasingOffline> lots = null ;
            if( request.getParameter("action") != null && request.getParameter("tenid") != null && "Edit".equals(request.getParameter("action"))
                    &&  Integer.parseInt(request.getParameter("tenid")) != 0){
                    tenolId = Integer.parseInt(request.getParameter("tenid"));

                    tenderDetailsOffline = offlineDataSrBean.getTblTenderDetailOfflineDataById(tenolId);

                    int i = 0;
                    if(tenderDetailsOffline.getTenderLotsAndPhases() != null && tenderDetailsOffline.getTenderLotsAndPhases().size() > 0){
                        while(i < tenderDetailsOffline.getTenderLotsAndPhases().size()){
                            tenderDetailsOffline.getTenderLotsAndPhases().remove(i);
                        }
                    }
                    lots = tenderDetailsOffline.getTenderLotsAndPhases();

             }else{
                    tenderDetailsOffline = new TblTenderDetailsOffline();
                    lots  = new ArrayList<TblTenderLotPhasingOffline>();
                }

                String userid = "";
                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") != null) {
                    userid = hs.getAttribute("userId").toString();
                    offlineDataSrBean.setLogUserId(userid);
                }
            //tenderDetailsOffline = new TblTenderDetailsOffline();

            tenderDetailsOffline.setMinistryOrDivision(ministry);
            tenderDetailsOffline.setAgency(agency);
            tenderDetailsOffline.setPeName(peName);
            tenderDetailsOffline.setPeCode(peCode);
            tenderDetailsOffline.setPeDistrict(district);
            tenderDetailsOffline.setProcurementNature(nature);
            tenderDetailsOffline.setProcurementType(procType);
            tenderDetailsOffline.setInvitationFor(invitationFor);
            tenderDetailsOffline.setReoiRfpRefNo(invitationRefNo);
           // System.out.println("issuedate >> "+issuedate);
            Date issDate = DateUtils.formatStdString(issuedate);
            tenderDetailsOffline.setIssueDate(issDate);
            tenderDetailsOffline.setProcurementMethod(procureMethod);
            tenderDetailsOffline.setBudgetType(budget);
            tenderDetailsOffline.setSourceOfFund(funds);
            tenderDetailsOffline.setDevPartners(devPartner);
            tenderDetailsOffline.setProjectCode(projectCode);
            tenderDetailsOffline.setProjectName(ProjectName);
            tenderDetailsOffline.setPackageNo(packageNo);
            tenderDetailsOffline.setPackageName(packageName);
           // System.out.println("tenderpublicationDate >> "+tenderpublicationDate);
            if(!"".equals(tenderpublicationDate)){
                Date pubDate = DateUtils.formatStdString(tenderpublicationDate);
                tenderDetailsOffline.setTenderPubDate(pubDate);
            }

            if(!"".equals(tenderCloseDate)){
                Date closeDate = DateUtils.convertDateToStr(tenderCloseDate);
                tenderDetailsOffline.setClosingDate(closeDate);
            }
             if(!"".equals(tenderLastSellDate)){
                 Date lastSellingDate = DateUtils.formatStdString(tenderLastSellDate);
                 tenderDetailsOffline.setLastSellingDate(lastSellingDate);
              }

             if(!"".equals(tenderOpenDate)){
                //Date tenderOpeningDate = DateUtils.formatStdString(tenderOpenDate);
                 Date tenderOpeningDate = DateUtils.convertDateToStr(tenderOpenDate);
                System.out.println("tender opening date>>>> >> "+tenderOpeningDate);
                tenderDetailsOffline.setOpeningDate(tenderOpeningDate);
              }

            tenderDetailsOffline.setPreTenderReoiplace(placeofPQMeeting);
            Date meetingDate = DateUtils.convertDateToStr(preTenderMeetStartDate);
            tenderDetailsOffline.setPreTenderReoidate(meetingDate);
            tenderDetailsOffline.setSellingAddPrinciple(sellingDocPrinciple);
            tenderDetailsOffline.setSellingAddOthers(sellingDocOthers);
            tenderDetailsOffline.setReceivingAdd(receivingTenDoc);
            tenderDetailsOffline.setOpeningAdd(openTenderDoc);
            tenderDetailsOffline.setEligibilityCriteria(eligibilityofTenderer);
            tenderDetailsOffline.setBriefDescription(briefDescGoods);
            tenderDetailsOffline.setRelServicesOrDeliverables(briefDescRelServices);
            tenderDetailsOffline.setEventType(eventType);
            tenderDetailsOffline.setUserId(Integer.parseInt(userid));
            float docPrice = 0;
            if (tenderDocPrice != null && !"".equals(tenderDocPrice)) {
                docPrice = Float.parseFloat(tenderDocPrice);
            }
            tenderDetailsOffline.setDocumentPrice(new BigDecimal(docPrice).setScale(2, 0));
            tenderDetailsOffline.setPeOfficeName(nameOfficial);
            tenderDetailsOffline.setPeDesignation(designationOfficial);
            tenderDetailsOffline.setPeAddress(addressOfOfficial);
            tenderDetailsOffline.setPeContactDetails(contactDetail);
           // List<TblTenderLotPhasingOffline> lots = new ArrayList<TblTenderLotPhasingOffline>();
           // System.out.println("invitationFor >> "+invitationFor);
            if(invitationFor.equals("Single Lot")){
                //System.out.println("single >> ");
                TblTenderLotPhasingOffline lotPhasingOffline = new TblTenderLotPhasingOffline();
                lotPhasingOffline.setLotOrRefNo(lotNo_1);
                lotPhasingOffline.setLocation(locationlot_1);
                lotPhasingOffline.setLotIdentOrPhasingServ(identification_1);
                float tenSecurityamt = 0;
                if (tenderSecurity_1 != null && !"".equals(tenderSecurity_1)) {
                    tenSecurityamt = Float.parseFloat(tenderSecurity_1);
                }

                lotPhasingOffline.setTenderSecurityAmt(new BigDecimal(tenSecurityamt).setScale(2, 0));
                lotPhasingOffline.setCompletionDateTime(complTimeLotNo_1);
                lotPhasingOffline.setTenderDetailsOffline(tenderDetailsOffline);
                lots.add(lotPhasingOffline);
            }else if(invitationFor.equals("Multiple Lot")){
                //System.out.println("multi lot ");
                 for(int i=1;i<=4;i++){

                if((request.getParameter("lotNo_"+i) != null || request.getParameter("identification_"+i) != null
                        || request.getParameter("locationlot_"+i) != null || request.getParameter("complTimeLotNo_"+i) != null || request.getParameter("tenderSecurity_"+i) != null)
                        && (!"".equals(request.getParameter("lotNo_"+i)) || !"".equals(request.getParameter("identification_"+i)) || !"".equals(request.getParameter("locationlot_"+i))
                        || !"".equals(request.getParameter("complTimeLotNo_"+i))  || !"".equals(request.getParameter("tenderSecurity_"+i)) ) ){

                        TblTenderLotPhasingOffline lotPhasingOffline = new TblTenderLotPhasingOffline();
                        lotPhasingOffline.setLotOrRefNo(request.getParameter("lotNo_"+i));
                        lotPhasingOffline.setLocation(request.getParameter("locationlot_"+i));
                        lotPhasingOffline.setLotIdentOrPhasingServ(request.getParameter("identification_"+i));
                        lotPhasingOffline.setCompletionDateTime(request.getParameter("complTimeLotNo_"+i));

                         float tenSecurityamt = 0;
                        if (request.getParameter("tenderSecurity_"+i) != null && !"".equals(request.getParameter("tenderSecurity_"+i))) {
                            tenSecurityamt = Float.parseFloat(request.getParameter("tenderSecurity_"+i));
                        }
                        lotPhasingOffline.setTenderSecurityAmt(new BigDecimal(tenSecurityamt).setScale(2, 0));
                        lotPhasingOffline.setTenderDetailsOffline(tenderDetailsOffline);
                        lots.add(lotPhasingOffline);
                }

               }
            }
            tenderDetailsOffline.setTenderLotsAndPhases(lots);            

         /*if( request.getParameter("action") != null && request.getParameter("tenid") != null && "Create".equals(request.getParameter("action"))
                &&  Integer.parseInt(request.getParameter("tenid")) != 0){
            System.out.println("tenolId >>>>>>>>>>> "+tenolId);
            tenderDetailsOffline.setTenderOfflineId(tenolId);
            
            corrigendumTenderBean.createCorrigendum(tenderDetailsOffline, Integer.parseInt(request.getParameter("tenid")), Integer.parseInt(userid));

          }*/

          if( request.getParameter("action") != null && request.getParameter("tenid") != null
                    &&  Integer.parseInt(request.getParameter("tenid")) != 0){

                tenolId = Integer.parseInt(request.getParameter("tenid"));
                System.out.println("tenolId >>>>>>>>>>> "+tenolId);
                tenderDetailsOffline.setTenderOfflineId(tenolId);

                corrigendumNo = Integer.parseInt(request.getParameter("corNo"));

                if("Edit".equals(request.getParameter("action")))
                    isEdit=true;

                corrigendumTenderBean.createCorrigendum(tenderDetailsOffline, tenolId, Integer.parseInt(userid),corrigendumNo, isEdit);

                %>
                <script type="text/javascript">
                  alert("Information Saved Successfully");
                </script>
                <%
                response.sendRedirect(request.getContextPath() + "/offlinedata/TenderDashboardOfflineApproval.jsp");
              }

            }catch(Exception ex){
                ex.printStackTrace();}
            }else{
                /*CREATION OF CORRIGENDUM STARTS HERE*/
                if(request.getParameter("action") != null ){
                    if(request.getParameter("tenderOLId") != null ){
                     String tenderOLId = request.getParameter("tenderOLId");
                     tenolId = Integer.parseInt(tenderOLId);
                     }

                if(request.getParameter("corNo") != null ){
                 String strCorNo = request.getParameter("corNo");
                 corrigendumNo = Integer.parseInt(strCorNo);
                 }
                    action = "Create";
                    tenderFormExcellBean = (TenderFormExcellBean) offlineDataSrBean.editTenderWithoutPQForm(tenolId);
                    agencyCombo = offlineDataSrBean.getOrganizationByMinistry(tenderFormExcellBean.getMinistryName(), tenderFormExcellBean.getAgency());
                    //System.out.println("office name > "+tenderFormExcellBean.getNameofOfficialInvitingTender());

                    if("Edit".equals(request.getParameter("action"))){
                        action = "Edit";
                        corrigendumDetailOffline = offlineDataSrBean.corrigendumDetails(Integer.parseInt(request.getParameter("tenderOLId")));
                        isEdit = true;
                    }
                 }/*else{
                    tenderFormExcellBean = (TenderFormExcellBean)request.getAttribute("tenderDataBean");
                    agencyCombo = offlineDataSrBean.getOrganizationByMinistry(tenderFormExcellBean.getMinistryName(), tenderFormExcellBean.getAgency());
                 }*/

        %>

        <% TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        List<SPTenderCommonData> holidayList = tenderCommonService.returndata("getHolidayDatesBD", null, null);
        out.print("<script type='text/javascript'>");
        for(SPTenderCommonData holidays : holidayList){
            out.print("holiArray.push('"+holidays.getFieldName1()+"');");
        }
        out.print("</script>");
        %>

    <body onload="documentAvailable();" >

         <input id="boolcheck" value="true" type="hidden"/>
        <div class="mainDiv">
            <div class="fixDiv">

         <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->

                <div class="contentArea_1">
                    <div class="t-align-right" style="vertical-align: middle;"><a class="action-button-goback" href="<%=request.getContextPath()%>/offlinedata/TenderDashboardOfflineApproval.jsp">Go back</a></div>
                    <form id="frmCreateTender" name="frmCreateTender" method="POST" action="/offlinedata/CorrigendumTender.jsp">
                    <div class="pageHead_1">Corrigendum of Tender without Pre-qualification (PQ)</div>

                    <div class="tableHead_22 t_space">PROCURING ENTITY (PE) INFORMATION</div>
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">

                            <tbody><tr>
                                <td style="font-style: italic" colspan="4" class="ff t-align-left" align="left">

                                    Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Ministry :<span>&nbsp;*</span></td>
                               
                                <td width="25%"><%=CommonUtils.checkNull(tenderFormExcellBean.getMinistryName())%>
                                     <br />
                                     <span style="color: red;" id="msgMinistry"></span>
                                    <input id="hidministry"  name="hidministry" type="hidden" value="<%=CommonUtils.checkNull(tenderFormExcellBean.getMinistryName())%>" />
                                </td>
                                <td width="25%"></td>
                                <td width="25%"></td>
                            </tr>
                            <tr>
                                <td class="ff">Organization :<span>&nbsp;*</span></td>
                                <td><%= CommonUtils.checkNull(tenderFormExcellBean.getAgency()) %>
                                     <input type="hidden" name="hidorg"  id="hidorg" value="<%= CommonUtils.checkNull(tenderFormExcellBean.getAgency()) %>" />
                                     <br />
                                     <span style="color: red;" id="msgOrganisation"></span>
                                </td>
                                <td class="ff">Procuring Entity Dzongkhag / District :<span>&nbsp;*</span></td>                                
                               <td><%= CommonUtils.checkNull(tenderFormExcellBean.getProcuringEntityDistrict()) %>
                                     <br />
                                            <span style="color: red;" id="msgDistrict"></span>
                               </td>
                            </tr>
                            <tr>
                                <td class="ff">Procuring Entity Name :<span>&nbsp;*</span></td>
                                <td><input name="peName" class="formTxtBox_1" id="txtPEName" style="width: 280px;" type="text"  value="<%= CommonUtils.checkNull(tenderFormExcellBean.getProcuringEntityName()) %>" />
                                    <br />
                                            <span style="color: red;" id="msgPEName"></span>
                                </td>

                                <td class="ff">Procuring Entity Code :</td>
                                <td>
                                    <!-- *********** Old Code ************* -->
                                     <%
                                    //if(CommonUtils.checkNull(tenderFormExcellBean.getProcuringEntityCode()) != null && !CommonUtils.checkNull(tenderFormExcellBean.getProcuringEntityCode()).equals("Not used at present")){
                                    %>
                                    <!--<input name="peCode" class="formTxtBox_1" id="txtPECode" style="width: 280px;" type="text" value="<%//=CommonUtils.checkNull(tenderFormExcellBean.getProcuringEntityCode()) %>" />
                                    <% //else{ %>
                                    <input name="peCode" class="formTxtBox_1" id="txtPECode" style="width: 280px;" type="text" value="" />-->
                                    <% // %>
                                    <!-- ************************ -->

                                    <!-- PE Code cannot be changed - 01.Sep.13 -->
                                    <%=CommonUtils.checkNull(tenderFormExcellBean.getProcuringEntityCode())%>
                                </td>

                            </tr>
                            <tr>
                                <td class="ff">Procurement Category :<span>&nbsp;*</span></td>
                                <td><select name="nature" class="formTxtBox_1" id="cmbProcureNature" style="width: 200px;" onChange="setSerType(this,true);">
                                              <!--  <option value="" selected="selected">---Select Procurement Nature---</option> -->
                                                <%
                                                int count = 0;
                                               if (isEdit) {
                                                   for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                        if (detail.getFieldName().equals("procurementNature") && detail.getCorrigendumStatus().equals("Pending")) {
                                                        if(CommonUtils.checkNull(detail.getNewValue()).equals("Goods")){
                                                            out.println("<option selected=\"selected\" value=\"Goods\">Goods</option>");
                                                            out.println("<option value=\"Works\">Works</option>");
                                                        }else{
                                                            out.println("<option  value=\"Goods\">Goods</option>");
                                                            out.println("<option selected=\"selected\" value=\"Works\">Works</option>");
                                                        }
                                                        count++;
                                                        }
                                                   }
                                               }
                                               if (count == 0) {
                                               if(CommonUtils.checkNull(tenderFormExcellBean.getProcurementNature()).equals("Goods")){
                                                    out.println("<option selected=\"selected\" value=\"Goods\">Goods</option>");
                                                    out.println("<option value=\"Works\">Works</option>");
                                               }else{
                                                    out.println("<option  value=\"Goods\">Goods</option>");
                                                    out.println("<option selected=\"selected\" value=\"Works\">Works</option>");
                                               }
                                               }

                                                %>
                                    </select>
                                    <br />
                                            <span style="color: red;" id="msgPNature"></span>
                                </td>
                                <td class="ff">Procurement Type :<span>*&nbsp;</span></td>
                                <td><select name="procType" class="formTxtBox_1" id="cmbProcureType" style="width: 200px;">
                                               <!-- <option value="" selected="selected" >--- Please Select ---</option> -->
                                               <%
                                               count = 0;
                                               if (isEdit) {
                                                   for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                        if (detail.getFieldName().equals("procurementType") && detail.getCorrigendumStatus().equals("Pending")) {
                                                        if(CommonUtils.checkNull(detail.getNewValue()).equals("NCT")){
                                                            out.println("<option selected=\"selected\" value=\"NCT\">NCB</option>");
                                                            out.println("<option value=\"ICT\">ICB</option>");
                                                        }else{
                                                            out.println("<option  value=\"NCT\">NCB</option>");
                                                            out.println("<option" + " selected=\"selected\" value=\"ICT\">ICB</option>");
                                                        }
                                                        count++;
                                                        }
                                                   }
                                               }
                                               if (count == 0) {
                                               if(CommonUtils.checkNull(tenderFormExcellBean.getProcurementType()).equals("NCT")){
                                                    out.println("<option selected=\"selected\" value=\"NCT\">NCB</option>");
                                                    out.println("<option value=\"ICT\">ICB</option>");
                                               }else{
                                                    out.println("<option  value=\"NCT\">NCB</option>");
                                                    out.println("<option selected=\"selected\" value=\"ICT\">ICB</option>");
                                               }
                                               }
                                                %>

                                     </select>
                                     <br />
                                     <span style="color: red;" id="msgProcureType"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Event Type :<span>&nbsp;*</span></td>
                                <td>
                                    <select name="eventType" class="formTxtBox_1" id="cmbEventType" style="width: 200px;">
                                               <!-- <option value="" selected="selected" >--- Please Select ---</option> -->
                                                 <%
                                               count = 0;
                                               if (isEdit) {
                                                   for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                        if (detail.getFieldName().equals("eventType") && detail.getCorrigendumStatus().equals("Pending")) {
                                                        if(CommonUtils.checkNull(detail.getNewValue()).equals("Tender")){
                                                            out.println("<option selected=\"selected\" value=\"Tender\">Tender</option>");
                                                            out.println("<option value=\"Re-Tender\">Re-Tender</option>");
                                                        }else{
                                                            out.println("<option  value=\"Tender\">Tender</option>");
                                                            out.println("<option selected=\"selected\" value=\"Re-Tender\">Re-Tender</option>");
                                                        }
                                                        count++;
                                                        }
                                                   }
                                               }
                                               if (count == 0) {
                                               if(CommonUtils.checkNull(tenderFormExcellBean.getEventType()).equals("Tender")){
                                                    out.println("<option selected=\"selected\" value=\"Tender\">Tender</option>");
                                                    out.println("<option value=\"Re-Tender\">Re-Tender</option>");
                                               }else{
                                                    out.println("<option  value=\"Tender\">Tender</option>");
                                                    out.println("<option selected=\"selected\" value=\"Re-Tender\">Re-Tender</option>");
                                               }
                                               }
                                                %>


                                     </select>
                                     <br />
                                     <span style="color: red;" id="msgEventType"></span>
                                </td>
                                 <td class="ff">Invitation for :<span>&nbsp;*</span></td>
                                <td>
                                     <select name="invitationFor" class="formTxtBox_1" id="cmbInvitationFor" style="width: 200px;" onchange="dataTable(this.value)">
                                       <!-- <option value="" selected="selected">--- Please Select ---</option> -->
                                         <%
                                        count = 0;
                                               if (isEdit) {
                                                   for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                        if (detail.getFieldName().equals("invitationFor") && detail.getCorrigendumStatus().equals("Pending")) {
                                                        if(CommonUtils.checkNull(detail.getNewValue()).equals("Single Lot")){
                                                            out.println("<option selected=\"selected\" value=\"Single Lot\">Single Lot</option>");
                                                            out.println("<option value=\"Multiple Lot\">Multiple Lot</option>");
                                                        }else{
                                                            out.println("<option  value=\"Single Lot\">Single Lot</option>");
                                                            out.println("<option selected=\"selected\" value=\"Multiple Lot\">Multiple Lot</option>");
                                                        }
                                                        count++;
                                                        }
                                                   }
                                               }
                                        if (count == 0) {
                                         if(CommonUtils.checkNull(tenderFormExcellBean.getInvitationFor()).contains("Single Lot")){
                                            out.println("<option selected=\"selected\" value=\"Single Lot\">Single Lot</option>");
                                            out.println("<option value=\"Multiple Lot\">Multiple Lot</option>");
                                        }else{
                                            out.println("<option  value=\"Single Lot\">Single Lot</option>");
                                            out.println("<option selected=\"selected\" value=\"Multiple Lot\">Multiple Lot</option>");
                                        }
                                         }
                                        %>
                                    </select>
                                    <br />
                                    <span style="color: red;" id="msgInvitationFor"></span>
                                    </td>
                            </tr>
                            <tr>
                                <td class="ff">Invitation Reference No. : <span>&nbsp;*</span></td>
                                <td><input name="invitationRefNo" class="formTxtBox_1" id="txtinvitationRefNo" style="width: 280px;" type="text" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("reoiRfpRefNo") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue().trim());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getInvitationRefNo()) %><%}%>" />
                                    <br />
                                    <span style="color: red;" id="msgInvitationRefNo"></span>
                                    <input id="hdnmsgTender" name="hdnmsgTenderName" value="Tender" type="hidden"/>
                                </td>
                                <td class="ff">Date : <span>&nbsp;*</span></td>
                                <td class="formStyle_1"><input name="issuedate" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("IssueDate") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getTenderissuingdate()) %><%}%>" class="formTxtBox_1" id="txtissuedate" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtissuedate','txtissuedate');" onblur="findHoliday(this,0);" type="text"/>
                                    <img id="imgtxtissuedate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtissuedate','imgtxtissuedate');" border="0"/>
                                    <br />
                                    <span style="color: red;" id="msgIssueDate"></span>
                                    <span id="span10"></span>
                                </td>
                            </tr>
                        </tbody></table>

                                <div class="tableHead_22 ">Key Information and Funding Information :</div>
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td class="ff" width="25%">Procurement Method : <span>*</span></td>
                               <td width="25%"><select name="procureMethod" class="formTxtBox_1" id="cmbProcureMethod" style="width: 200px;">
                                               <!-- <option value="">---Select Procurement Method---</option> -->
                                               <%
                                               count = 0;
                                                if (isEdit) {
                                                    for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                        if (detail.getFieldName().equals("procurementMethod") && detail.getCorrigendumStatus().equals("Pending")) {
                                                            if(CommonUtils.checkNull(detail.getNewValue()).contains("Open")){
                                                                out.println("<option selected=\"selected\" value=\"Open Tendering Method\">Open Tendering Method</option>");
                                                                out.println("<option value=\"Two Stage Tendering Method\">Two Stage Tendering Method</option>");
                                                            }else{
                                                                out.println("<option value=\"Open Tendering Method\">Open Tendering Method</option>");
                                                                out.println("<option selected=\"selected\" value=\"Two Stage Tendering Method\">Two Stage Tendering Method</option>");
                                                        }
                                                            count++;
                                                        }
                                                    }
                                                }
                                                if (count == 0) {
                                                if(CommonUtils.checkNull(tenderFormExcellBean.getProcurementMethod()).contains("Open")){
                                                out.println("<option selected=\"selected\" value=\"Open Tendering Method\">Open Tendering Method</option>");
                                                out.println("<option value=\"Two Stage Tendering Method\">Two Stage Tendering Method</option>");
                                                }else{
                                                out.println("<option value=\"Open Tendering Method\">Open Tendering Method</option>");
                                                out.println("<option selected=\"selected\" value=\"Two Stage Tendering Method\">Two Stage Tendering Method</option>");
                                                }
                                                }
                                                %>
                                     </select>
                                     <input type="hidden" name="hdnPM" id="hdnPM" value=""/>
                                            <span class="reqF_2" id="msgProcureMethod1"></span>
                                    <br />
                                    <span style="color: red;" id="msgProcureMethod"></span>

                                </td>
                                <td class="ff" width="25%">Budget Type :<span>&nbsp;*</span></td>
                                 <td width="25%"><select name="budgetType" class="formTxtBox_1" id="cmbBudgetType" style="width: 200px;">
                                               <!-- <option value="" selected="selected">---Select Budget Type ---</option> -->
                                                <%
                                                count = 0;
                                                if (isEdit) {
                                                    for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                        if(detail.getCorrigendumStatus().equals("Pending")){
                                                        if (detail.getFieldName().equals("budgetType")) {
                                                            if(CommonUtils.checkNull(detail.getNewValue()).equals("Revenue Budget")){
                                                                out.println("<option selected=\"selected\" value=\"Revenue Budget\">Revenue Budget</option>");
                                                                out.println("<option value=\"Development Budget\">Development Budget</option>");
                                                                out.println("<option value=\"Own Funds\">Own Funds</option>");
                                                            }else if(CommonUtils.checkNull(detail.getNewValue()).equals("Development Budget")){
                                                                out.println("<option  value=\"Revenue Budget\">Revenue Budget</option>");
                                                                out.println("<option selected=\"selected\" value=\"Development Budget\">Development Budget</option>");
                                                                out.println("<option value=\"Own Funds\">Own Funds</option>");
                                                            }
                                                            else{
                                                                    out.println("<option  value=\"Revenue Budget\">Revenue Budget</option>");
                                                                    out.println("<option  value=\"Development Budget\">Development Budget</option>");
                                                                    out.println("<option selected=\"selected\" value=\"Own Funds\">Own Funds</option>");
                                                                }
                                                            count++;
                                                        }
                                                        }
                                                    }
                                                }
                                               if (count == 0) {
                                               if(CommonUtils.checkNull(tenderFormExcellBean.getCbobudget()).equals("Revenue Budget")){
                                                   out.println("<option selected=\"selected\" value=\"Revenue Budget\">Revenue Budget</option>");
                                                   out.println("<option value=\"Development Budget\">Development Budget</option>");
                                                   out.println("<option value=\"Own Funds\">Own Funds</option>");
                                               }else if(CommonUtils.checkNull(tenderFormExcellBean.getCbobudget()).equals("Development Budget")){
                                                   out.println("<option  value=\"Revenue Budget\">Revenue Budget</option>");
                                                   out.println("<option selected=\"selected\" value=\"Development Budget\">Development Budget</option>");
                                                   out.println("<option value=\"Own Funds\">Own Funds</option>");
                                               }else{
                                                   out.println("<option  value=\"Revenue Budget\">Revenue Budget</option>");
                                                   out.println("<option  value=\"Development Budget\">Development Budget</option>");
                                                   out.println("<option selected=\"selected\" value=\"Own Funds\">Own Funds</option>");
                                               }
                                               }
                                                %>


                                     </select>
                                    <br />
                                    <span style="color: red;" id="msgBudgetType"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Source of Funds :<span>&nbsp;*</span></td>
                               <td width="25%"><select name="sourceFunds" class="formTxtBox_1" id="cmbSourceFunds" style="width: 200px;">
                                               <!-- <option value="" selected="selected">--- Please Select ---</option> -->

                                                 <%
                                                 count = 0;
                                                if (isEdit) {
                                                    for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                        if(detail.getCorrigendumStatus().equals("Pending")){
                                                        if (detail.getFieldName().equals("sourceOfFund")) {
                                                            if(CommonUtils.checkNull(detail.getNewValue()).equals("Government")){
                                                                out.println("<option selected=\"selected\" value=\"Government\">Government</option>");
                                                                out.println("<option value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                                out.println("<option value=\"Own Funds\">Own Funds</option>");
                                                            }else if(CommonUtils.checkNull(detail.getNewValue()).equals("Aid Grant / Credit")){
                                                                out.println("<option  value=\"Government\">Government</option>");
                                                                out.println("<option selected=\"selected\" value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                                out.println("<option value=\"Own Funds\">Own Funds</option>");
                                                            }
                                                            else{
                                                                out.println("<option  value=\"Government\">Government</option>");
                                                                out.println("<option value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                                out.println("<option selected=\"selected\" value=\"Own Funds\">Own Funds</option>");
                                                                }
                                                            count++;
                                                            break;
                                                        }
                                                        }
                                                    }
                                                }
                                               if (count == 0) {
                                               if(CommonUtils.checkNull(tenderFormExcellBean.getCbosource()).equals("Government")){
                                                   out.println("<option selected=\"selected\" value=\"Government\">Government</option>");
                                                   out.println("<option value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                   out.println("<option value=\"Own Funds\">Own Funds</option>");
                                               }else if(CommonUtils.checkNull(tenderFormExcellBean.getCbosource()).equals("Aid Grant")){
                                                   out.println("<option  value=\"Government\">Government</option>");
                                                   out.println("<option selected=\"selected\" value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                   out.println("<option value=\"Own Funds\">Own Funds</option>");
                                               }else{
                                                   out.println("<option  value=\"Government\">Government</option>");
                                                   out.println("<option value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                   out.println("<option selected=\"selected\" value=\"Own Funds\">Own Funds</option>");
                                               }
                                               }
                                                %>
                                     </select>
                                    <br />
                                    <span style="color: red;" id="msgSourceOfFunds"></span>
                               </td>
                                <td class="ff" width="25%">Development Partner : </td>

                                <td width="25%">
                                    <input id="txtDevPartner" class="formTxtBox_1" type="text" style="width: 280px;" name="devPartner" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("devPartners") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getDevelopmentPartner()) %><%}%>" />
                                </td>
                            </tr>


                        </tbody></table>
                        <div class="tableHead_22 ">Particular Information :</div>
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td class="ff" width="25%">Project Code : </td>
                                <td width="25%"><input name="projectCode" class="formTxtBox_1" id="txtProjectCode" style="width: 280px;" type="text" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("projectCode") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getProjectOrProgrammeCode()) %><%}%>" /></td>
                                <td class="ff" width="25%">Project Name : </td>
                                 <td width="25%"><input name="projectName" class="formTxtBox_1" id="txtProjectName" style="width: 280px;" type="text" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("projectName") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getProjectOrProgrammeName()) %><%}%>" /></td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Tender Package No. :<span>&nbsp;*</span></td>
                                <td>
                                    <input name="packageNo" class="formTxtBox_1" id="txtPackageNo" style="width: 280px;" type="text" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("packageNo") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getTenderPackageNo()) %><%}%>" />
                                    <br />
                                    <span style="color: red;" id="msgPackageNo"></span>
                                </td>
                                <td class="ff" width="25%">Tender Package Name :<span>&nbsp;*</span></td>
                                <td>
                                    <input name="packageName" class="formTxtBox_1" id="txtPackageName" style="width: 280px;" type="text"value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("PackageName") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getTenderPackageName()) %><%}%>" />
                                    <br />
                                    <span style="color: red;" id="msgPackageName"></span>
                                </td>
                            </tr>
                            <tr>

                                <td class="ff">Tender Publication Date : <span>&nbsp;*</span></td>
                                <td class="formStyle_1"><input name="tenderpublicationDate" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("tenderPubDate") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getTenderPublicationDate()) %><%}%>" class="formTxtBox_1" id="txtTenderpublicationDate" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtTenderpublicationDate','txtTenderpublicationDate');" onblur="findHoliday(this,0);" type="text"/>
                                    <img id="txttenderpublicationDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtTenderpublicationDate','txttenderpublicationDateimg');" border="0"/>
                                    <br />
                                    <span style="color: red;" id="msgPublicationDate"></span>

                                </td>

                                <td class="ff">Tender Last Selling Date : <span>&nbsp;*</span></td>
                                <td class="formStyle_1"><input name="tenderLastSellDate" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("lastSellingDate") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getTenderLastSellingDate()) %><%}%>" class="formTxtBox_1" id="txtTenderLastSellDate" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtTenderLastSellDate','txtTenderLastSellDate');" onblur="findHoliday(this,1);" type="text"/>
                                    <img id="txttenderLastSellDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtTenderLastSellDate','txttenderLastSellDateimg');" border="0"/>
                                    <br />
                                    <span style="color: red;" id="msgLastSelDate"></span>

                                </td>


                            </tr>

                            <tr>
                                <td class="ff">Tender Closing Date and Time : <span>&nbsp;*</span></td>
                                <td class="formStyle_1"><input name="tenderCloseDate" class="formTxtBox_1" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("ClosingDate") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getTenderClosingDate()) %><%}%>" id="txtpreQualCloseDate" style="width: 100px;" readonly="true" onfocus="GetCal('txtpreQualCloseDate','txtpreQualCloseDate');" onblur="findHoliday(this,4);" type="text"/>
                                    <img id="txtpreQualCloseDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCal('txtpreQualCloseDate','txtpreQualCloseDateimg');" border="0"/>
                                    <br />
                                    <span style="color: red;" id="msgClosingDate"></span>

                                </td>
                                <td class="ff">Tender Opening Date and Time : <span>&nbsp;*</span></td>
                                <td class="formStyle_1"><input name="tenderOpenDate" class="formTxtBox_1" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("OpeningDate") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getTenderOpeningDate()) %><%}%>" id="txtpreQualOpenDate" style="width: 100px;" readonly="true" onfocus="GetCal('txtpreQualOpenDate','txtpreQualOpenDate');" onblur="findHoliday(this,5);" type="text"/>
                                    <img id="txtpreQualOpenDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCal('txtpreQualOpenDate','txtpreQualOpenDateimg');" border="0"/>
                                    <br />
                                    <span style="color: red;" id="msgOpeningDate"></span>

                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Selling Tender Document(Principal) :<span>&nbsp;*</span></td>
                                <td>
                                    <input name="sellingDocPrinciple" class="formTxtBox_1" id="txtSellDocP" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("SellingAddPrinciple") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getSellingTenderDocumentPrincipal()) %><%}%>" style="width: 280px;" type="text"/>
                                    <br />
                                    <span style="color: red;" id="msgSellDocP"></span>
                                </td>
                                <td class="ff" width="25%">Opening Tender Document:<span>&nbsp;*</span></td>
                                <td>
                                    <input name="openTenderDoc" class="formTxtBox_1" id="txtOpenTenderDoc" style="width: 280px;" type="text" value="<% count = 0;
                                                if (isEdit) {
                                                    for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                        if (detail.getFieldName().equals("OpeningAdd") && detail.getCorrigendumStatus().equals("Pending")) {
                                                            out.print(detail.getNewValue());
                                                            count++;
                                                        }
                                                    }
                                                }
                                                if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getOpeningTenderDocument()) %><%}%>" />
                                    <br />
                                    <span style="color: red;" id="msgOpenTenderDoc"></span>
                                </td>
                                
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Receiving Tender Document :<span>&nbsp;*</span></td>
                                <td>
                                    <textarea rows="2" cols="49" name="receivingTenDoc" class="formTxtBox_1" id="txtRcvTenderDoc" style="width: 280px;"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("ReceivingAdd") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getReceivingTenderDocument()) %><%}%></textarea>
                                    <br />
                                    <span style="color: red;" id="msgRcvTenderDoc"></span>
                                </td>
                                <td class="ff" width="25%">Selling Tender Document(Others):</td>
                                <td>
                                    <textarea name="sellingDocOthers" class="formTxtBox_1" id="txtSellDocOther" style="width: 280px;"><% count = 0;
                                            if (isEdit) {
                                                for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                    if (detail.getFieldName().equals("SellingAddOthers") && detail.getCorrigendumStatus().equals("Pending")) {
                                                        out.print(detail.getNewValue());
                                                        count++;
                                                    }
                                                }
                                            }
                                            if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getSellingTenderDocumentOthers()) %><%}%></textarea>
                                </td>
                            </tr>
                            <tr>
                                    <td class="ff">Place of Pre-Tender Meeting :</td>
                                <td class="formStyle_1">
                                    <textarea name="placeofPQMeeting" rows="2" cols="49" id="txtPlaceofPQMeeting"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("PreTenderREOIPlace") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getPlaceofTenderMeeting()) %><%}%></textarea>
                                </td>
                                    <td class="ff">Date and Time of Pre-Tender Meeting : <span></span>
                                    <input id="hdncheck" value="Yes" type="hidden"/>
                                </td>
                                <td class="formStyle_1"><input name="preTenderMeetStartDate" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("preTenderREOIDate") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getDateTimeofTenderMeeting()) %><%}%>" class="formTxtBox_1" id="txtpreTenderMeetStartDate" style="width: 100px;" readonly="true" onfocus="GetCal('txtpreTenderMeetStartDate','txtpreTenderMeetStartDate');" onblur="findHoliday(this,2);" type="text"/>
                                    <img id="txtpreTenderMeetStartDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCal('txtpreTenderMeetStartDate','txtpreTenderMeetStartDateimg');" border="0"/>
                                </td>
                            </tr>

                        </tbody></table>

                        <div class="tableHead_22 ">Information for Bidder/Consultant :</div>

                        <table class="formStyle_1 " width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td class="ff" width="25%"><input id="hdnevenyType" value="Works" type="hidden"/>Eligibility of Bidder/Consultant : <span>&nbsp;*</span></td>
                                <td colspan="2">
                                    <textarea style="display: none;" cols="100" rows="5" id="txtEligibility" name="eligibilityofTenderer" class="ckeditor"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("EligibilityCriteria") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getEligibilityofTender()) %><%}%></textarea>
                                    
                                    <span style="color: red;" id="msgEligibility"></span>
                                </td>

                            </tr>
                            <tr>
                                <td class="ff">Brief Description of Goods or Works : <span>&nbsp;*</span></td>
                                <td><input value="Works" id="briefValMsg" type="hidden"/>
                                    <textarea style="display: none;" cols="100" rows="5" id="txtabriefDescGoods" name="briefDescGoods" class="ckeditor"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("BriefDescription") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getBriefDescriptionofGoodsorWorks()) %><%}%></textarea>

                                    <span style="color: red;" id="msgGoodsDescription"></span>
                                </td><td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="ff">Brief Description of Services : <span>&nbsp;*</span></td>
                                <td><input value="Works" id="briefValMsg" type="hidden"/>
                                    <textarea style="display: none;" cols="100" rows="5" id="txtbriefDesServices" name="briefDescRelServices" class="ckeditor"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("RelServicesOrDeliverables") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getBriefDescriptionofRelatedServices()) %><%}%></textarea>
                                    
                                    <span style="color: red;" id="msgServiceDescription"></span>
                                </td><td>&nbsp;</td>
                            </tr>

                            <tr style="display: table-row;" id="docsprice">
                                    <td class="ff">Tender Document Price (In Nu.) : <span>&nbsp;*</span></td>
                                <td><input name="tenderDocPrice" onblur="documentPrice(this);" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("DocumentPrice") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getTenderDocumentPrice()) %><%}%>"  class="formTxtBox_1" id="txtDocPrice" style="width: 200px;" type="text"/><br />

                                    <br />
                                    <span style="color: red;" id="msgDocPrice"></span>
                                </td>
                                <td>&nbsp;</td>
                            </tr>

                        </tbody></table>


                        <table class="tableList_1 t_space" width="100%" cellspacing="0" id="tblLotDescription">
                            <tbody><tr>
                                <th class="t-align-left" width="6%">Lot No.</th>

                                <th class="t-align-left" width="44%">Identification of Lot <span style="color:red">*</span></th>

                                <th class="t-align-center" width="10%">Location </th>

                                <th class="t-align-center" width="10%">Tender Security (Amount in Nu.)</th>

                                <th class="t-align-center" width="10%">Completion Time in Weeks/Months</th>
                            </tr>

                            <tr>
                                <td class="t-align-center"><input name="lotNo_1" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Number") && detail.getLotorPhaseIdentity().equals("1") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getLotNo_1()) %><%}%>" class="formTxtBox_1" id="txtLotNo_1" onblur="chkRefNoBlank(this);" style="width: 95%;" type="text"/> </td>
                                <td class="t-align-center">
                                    <textarea name="identification_1" rows="2" cols="40" id="txtLotID_1" onblur="chkRefNoBlank(this);"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Identification") &&detail.getLotorPhaseIdentity().equals("1") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getIdentificationOfLot_1()) %><%}%></textarea>
                                </td>
                                <td class="t-align-center"><input name="locationlot_1" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Location") && detail.getLotorPhaseIdentity().equals("1") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getLocation_1()) %><%}%>" style="width: 180px;" class="formTxtBox_1" id="txtLocationlot_1" onblur="chkLocLotBlank(this);" type="text"/><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center"><input name="tenderSecurity_1"  value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Bid Security") && detail.getLotorPhaseIdentity().equals("1") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getTenderSecurityAmount_1()) %><%}%>" class="formTxtBox_1 ff" id="txtTenderSecurity_1" onblur="chkAmountLotBlank(this);" type="text"/><br/><span style="color: red;" id="msgTenderSecurity"></span><span id="amountLot_0" style="color: red;"></span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_1"  value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Completion Time") && detail.getLotorPhaseIdentity().equals("1") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getCompletionTimeInWeeksORmonths_1()) %><%}%>" class="formTxtBox_1" id="txtComplTime_1" style="width: 100px;" onfocus="GetCalWithouTime('complTimeLotNo_0','complTimeLotNo_0');" onblur="chkCompTimeLotBlank(this);findHoliday(this,8);" type="text"/><span id="compLot_0" style="color: red;">&nbsp;</span>
                                </td>
                            </tr>
                               <tr id="lot_1" style="<%if(isEdit){ boolean lotFlag = false; for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {

                                                                            if (detail.getFieldName().equals("invitationFor"))
                                                                                lotFlag = true;
                                                                            if(lotFlag == true)
                                                                                 if (detail.getFieldName().equals("invitationFor") && detail.getNewValue().equals("Multiple Lot") && detail.getCorrigendumStatus().equals("Pending")) {%>display:table-row;<%}else{%>display:none;<%}
                                                                            }
                                                                            if(lotFlag == false)
                                                                             { if(CommonUtils.checkNull(tenderFormExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> table-row;<%}}


                                                                            

                                                                            } else { if(CommonUtils.checkNull(tenderFormExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> table-row;<%}}%>" >
                                     <td class="t-align-center"><input name="lotNo_2" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Number") && detail.getLotorPhaseIdentity().equals("2") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getLotNo_2()) %><%}%>" class="formTxtBox_1" id="txtLotNo_2" onblur="chkRefNoBlank(this);" style="width: 95%;" type="text"/> </td>
                                <td class="t-align-center">
                                    <textarea name="identification_2" rows="2" cols="40" id="txtLotID_2" onblur="chkRefNoBlank(this);"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Identification") && detail.getLotorPhaseIdentity().equals("2") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getIdentificationOfLot_2()) %><%}%></textarea>
                                </td>
                                <td class="t-align-center"><input name="locationlot_2" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Location") && detail.getLotorPhaseIdentity().equals("2") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getLocation_2()) %><%}%>" style="width: 180px;" class="formTxtBox_1" id="txtLocationlot_2" onblur="chkLocLotBlank(this);" type="text"/><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center"><input name="tenderSecurity_2"  value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Bid Security") && detail.getLotorPhaseIdentity().equals("2") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getTenderSecurityAmount_2()) %><%}%>" class="formTxtBox_1 ff" id="txtTenderSecurity_2" onblur="chkAmountLotBlank(this);" type="text"/><br/><span style="color: red;" id="msgTenderSecurity"></span><span id="amountLot_0" style="color: red;"></span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_2"  value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Completion Time") && detail.getLotorPhaseIdentity().equals("2") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getCompletionTimeInWeeksORmonths_2()) %><%}%>" class="formTxtBox_1" id="txtComplTime_2" style="width: 100px;" onfocus="GetCalWithouTime('complTimeLotNo_0','complTimeLotNo_0');" onblur="chkCompTimeLotBlank(this);findHoliday(this,8);" type="text"/><span id="compLot_0" style="color: red;">&nbsp;</span>
                                </td>
                            </tr>
                             <tr id="lot_2" style="<%if(isEdit){boolean lotFlag = false; for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {

                                                                            if (detail.getFieldName().equals("invitationFor"))
                                                                                lotFlag = true;
                                                                            if(lotFlag == true)
                                                                                 if (detail.getFieldName().equals("invitationFor") && detail.getNewValue().equals("Multiple Lot") && detail.getCorrigendumStatus().equals("Pending")) {%>display:table-row;<%}else{%>display:none;<%}
                                                                            }
                                                                            if(lotFlag == false)
                                                                             { if(CommonUtils.checkNull(tenderFormExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> table-row;<%}}




                                                                            } else { if(CommonUtils.checkNull(tenderFormExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> table-row;<%}}%>" >
                                 <td class="t-align-center"><input name="lotNo_3" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Number") & detail.getLotorPhaseIdentity().equals("3")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getLotNo_3()) %><%}%>" class="formTxtBox_1" id="txtLotNo_3" onblur="chkRefNoBlank(this);" style="width: 95%;" type="text"/> </td>
                                <td class="t-align-center">
                                    <textarea name="identification_3" rows="2" cols="40" id="txtLotID_3" onblur="chkRefNoBlank(this);"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Identification") && detail.getLotorPhaseIdentity().equals("3") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getIdentificationOfLot_3()) %><%}%></textarea>
                                </td>
                                <td class="t-align-center"><input name="locationlot_3" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Location") && detail.getLotorPhaseIdentity().equals("3") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getLocation_3()) %><%}%>" style="width: 180px;" class="formTxtBox_1" id="txtLocationlot_3" onblur="chkLocLotBlank(this);" type="text"/><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center"><input name="tenderSecurity_3"  value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Bid Security") && detail.getLotorPhaseIdentity().equals("3") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getTenderSecurityAmount_3()) %><%}%>" class="formTxtBox_1 ff" id="txtTenderSecurity_3" onblur="chkAmountLotBlank(this);" type="text"/><br/><span style="color: red;" id="msgTenderSecurity"></span><span id="amountLot_0" style="color: red;"></span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_3"  value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Completion Time") && detail.getLotorPhaseIdentity().equals("3") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getCompletionTimeInWeeksORmonths_3()) %><%}%>" class="formTxtBox_1" id="txtComplTime_3" style="width: 100px;" onfocus="GetCalWithouTime('complTimeLotNo_0','complTimeLotNo_0');" onblur="chkCompTimeLotBlank(this);findHoliday(this,8);" type="text"/><span id="compLot_0" style="color: red;">&nbsp;</span>
                                </td>
                            </tr>
                            <tr id="lot_3" style="<%if(isEdit){boolean lotFlag = false; for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {

                                                                            if (detail.getFieldName().equals("invitationFor"))
                                                                                lotFlag = true;
                                                                            if(lotFlag == true)
                                                                                 if (detail.getFieldName().equals("invitationFor") && detail.getNewValue().equals("Multiple Lot") && detail.getCorrigendumStatus().equals("Pending")) {%>display:table-row;<%}else{%>display:none;<%}
                                                                            }
                                                                            if(lotFlag == false)
                                                                             { if(CommonUtils.checkNull(tenderFormExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> table-row;<%}}




                                                                            } else { if(CommonUtils.checkNull(tenderFormExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> table-row;<%}}%>" >
                                    <td class="t-align-center"><input name="lotNo_4" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Number") & detail.getLotorPhaseIdentity().equals("4")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getLotNo_4()) %><%}%>" class="formTxtBox_1" id="txtLotNo_4" onblur="chkRefNoBlank(this);" style="width: 95%;" type="text"/> </td>
                                <td class="t-align-center">
                                    <textarea name="identification_4" rows="2" cols="40" id="txtLotID_4" onblur="chkRefNoBlank(this);"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Lot Identification") && detail.getLotorPhaseIdentity().equals("4") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getIdentificationOfLot_4()) %><%}%></textarea>
                                </td>
                                <td class="t-align-center"><input name="locationlot_4" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Location") && detail.getLotorPhaseIdentity().equals("4") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getLocation_4()) %><%}%>" style="width: 180px;" class="formTxtBox_1" id="txtLocationlot_4" onblur="chkLocLotBlank(this);" type="text"/><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center"><input name="tenderSecurity_4"  value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Bid Security") && detail.getLotorPhaseIdentity().equals("4") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getTenderSecurityAmount_4()) %><%}%>" class="formTxtBox_1 ff" id="txtTenderSecurity_4" onblur="chkAmountLotBlank(this);" type="text"/><br/><span style="color: red;" id="msgTenderSecurity"></span><span id="amountLot_0" style="color: red;"></span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_4"  value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("Completion Time") && detail.getLotorPhaseIdentity().equals("4") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%=CommonUtils.checkNull(tenderFormExcellBean.getCompletionTimeInWeeksORmonths_4()) %><%}%>" class="formTxtBox_1" id="txtComplTime_4" style="width: 100px;" onfocus="GetCalWithouTime('complTimeLotNo_0','complTimeLotNo_0');" onblur="chkCompTimeLotBlank(this);findHoliday(this,8);" type="text"/><span id="compLot_0" style="color: red;">&nbsp;</span>
                                </td>
                            </tr>

                        </tbody></table>


                        <div class="tableHead_22 t_space">Procuring Entity Details :</div>
                        <table class="formStyle_1 t_space" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                    <td class="ff" width="25%">Name of Official Inviting  Tender : <span>*</span></td>
                               <td width="25%"><input name="nameOfficial" class="formTxtBox_1" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("peName") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getNameofOfficialInvitingTender()) %><%}%>" id="txtNameOfficial" style="width: 280px;" type="text"/>
                               <br />
                                    <span style="color: red;" id="msgOfficialName"></span>
                               </td>
                                    <td class="ff" width="26%"> Designation of Official Inviting  Tender : <span>*</span></td>
                               <td width="25%"><input name="designationOfficial" value="<% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("peDesignation") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getDesignationofOfficialInvitingTender()) %><%}%>" class="formTxtBox_1" id="txtDesignation" style="width: 280px;" type="text"/>
                               <br />
                                    <span style="color: red;" id="msgOfficialDesignation"></span>
                               </td>
                            </tr>

                            <tr>
                                    <td class="ff">Address of Official Inviting  Tender : </td>
                            <td><textarea name="addressOfOfficial" rows="2" cols="49" id="BodyContent_txtContractDescription"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("peAddress") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getAddressofOfficialInvitingTender()) %><%}%></textarea>
						    </td>
                                    <td class="ff">Contact details of Official Inviting  Tender :</td>
                                <td><textarea name="contactDetail" rows="2" cols="49" id="Textarea2"><% count = 0;
                                                                    if (isEdit) {
                                                                        for (TblCorrigendumDetailOffline detail : corrigendumDetailOffline) {
                                                                            if (detail.getFieldName().equals("peContactDetails") && detail.getCorrigendumStatus().equals("Pending")) {
                                                                                out.print(detail.getNewValue());
                                                                                count++;
                                                                            }
                                                                        }
                                                                    }
                                                                    if (count == 0) {%><%= CommonUtils.checkNull(tenderFormExcellBean.getContactdetailsofOfficialInvitingTender()) %><%}%></textarea>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4" class="ff mandatory">The procuring entity reserves the right to accept or reject all Tenders/Pre-Qualifications / EOIs</td>
                            </tr>
                        </tbody></table>

                        <table class="formStyle_1 t_space" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td colspan="4" align="center">
                                    <label class="formBtn_1">
                                        <input name="submit" id="btnsubmit" value="Submit" type="submit" />
                                        <input name="hdnbutton" id="hdnbutton" value="" type="hidden"/>
                                         <input name="action" id="action"  value="<%=action %>" type="hidden" />
                                        <input name="tenid" id="tenid"  value="<%=tenolId %>" type="hidden" />
                                        <input name="corNo" id="corNo"  value="<%=corrigendumNo %>" type="hidden" />
                                    </label>&nbsp;&nbsp;
                              </td>
                            </tr>
                        </tbody></table>
                        <div>&nbsp;</div>
                    </form>
   <% } %>


                    <!--Dashboard Content Part End-->


                    <script type="text/javascript">
                    function loadOrganization() {
                        var deptId= 0;
                       // var districtId = $('#cmbDistrict').val();
                            deptId= $('#cmbMinistry').val();
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: deptId, funName:'organizationCombo'},  function(j){
                            $('#cmbOrganization').children().remove().end()
                            $("select#cmbOrganization").html(j);
                        });
                         var orgObj =document.getElementById("cmbMinistry");
                         var orgval = orgObj.options[orgObj.selectedIndex].text;
                         document.getElementById("hidministry").value = orgval;
                    }
                    </script>

                    <script language="javascript" type="text/javascript">
                    function setSerType(obj,flag){
                        if(obj.options[obj.selectedIndex].text== "Goods" || obj.options[obj.selectedIndex].text== "Works" )
                        {
                            $.post("<%=request.getContextPath()%>/APPServlet", {projectId:'No',param4:$('#hdnPM').val(),procNature:$('#cmbProcureNature').val(),funName:'getPM'},  function(j){
                                                $("select#cmbProcureMethod").html(j);
                                            });
                                            //$('#trLotBtn').show();

                        }

                    }

                    </script>


                    <script type="text/javascript">
                        function GetCal(txtname,controlname)
                        {
                            new Calendar({
                                inputField: txtname,
                                trigger: controlname,
                                showTime: 24,
                                onSelect: function() {
                                    var date = Calendar.intToDate(this.selection.get());
                                    LEFT_CAL.args.min = date;
                                    LEFT_CAL.redraw();
                                    this.hide();
                                    document.getElementById(txtname).focus();
                                }
                            });

                            var LEFT_CAL = Calendar.setup({
                                weekNumbers: false
                            })
                        }
                        function GetCalWithouTime(txtname,controlname)
                        {
                            new Calendar({
                                inputField: txtname,
                                trigger: controlname,
                                showTime: false,
                                dateFormat:"%d/%m/%Y",
                                onSelect: function() {
                                    var date = Calendar.intToDate(this.selection.get());
                                    LEFT_CAL.args.min = date;
                                    LEFT_CAL.redraw();
                                    this.hide();
                                    document.getElementById(txtname).focus();
                                }
                            });

                            var LEFT_CAL = Calendar.setup({
                                weekNumbers: false
                            })
                        }

                    </script>


                    <script type="text/javascript">

                        //Function for Required
                        function required(controlid)
                        {
                            var temp=controlid.length;
                            if(temp <= 0 ){
                                return false;
                            }else{
                                return true;
                            }
                        }

                        //Function for MaxLength
                        function Maxlenght(controlid,maxlenght)
                        {
                            var temp=controlid.length;
                            if(temp>=maxlenght){
                                return false;
                            }else
                                return true;
                        }

                        //Function for digits
                        function digits(control) {
                            return /^\d+$/.test(control);
                        }

                        function CompareToForEqual(value,params)
                        {

                            var mdy = value.split('/')  //Date and month split
                            var mdyhr=mdy[2].split(' ');  //Year and time split
                            var mdyp = params.split('/')
                            var mdyphr=mdyp[2].split(' ');
                            var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                            return Date.parse(date) == Date.parse(datep);
                        }

                        //Function for CompareToForToday
                        function CompareToForToday(first)
                        {
                            var mdy = first.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var mdyhrtime=mdyhr[1].split(':');
                            if(mdyhrtime[1] == undefined){
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                            }else
                            {
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                            }

                            var d = new Date();
                            if(mdyhrtime[1] == undefined){
                                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                            }
                            else
                            {
                                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(),d.getHours(),d.getMinutes());
                            }
                            return Date.parse(valuedate) > Date.parse(todaydate);
                        }

                        //Function for CompareToForGreater
                        function CompareToForGreater(value,params)
                        {
                            if(value!='' && params!=''){

                                var mdy = value.split('/')  //Date and month split
                                var mdyhr=mdy[2].split(' ');  //Year and time split
                                var mdyp = params.split('/')
                                var mdyphr=mdyp[2].split(' ');


                                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                                {
                                    //alert('Both Date');
                                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                                }
                                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                                {
                                    //alert('Both DateTime');
                                    var mdyhrsec=mdyhr[1].split(':');
                                    var date =  new Date( mdyhr[0], parseFloat(mdy[1])-1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var mdyphrsec=mdyphr[1].split(':');

                                    var datep =  new Date(mdyphr[0], parseFloat(mdyp[1])-1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                            }
                                else
                                {
                                    //alert('one Date and One DateTime');
                                    var a = mdyhr[1];  //time
                                    var b = mdyphr[1]; // time

                                    if(a == undefined && b != undefined)
                                    {
                                        //alert('First Date');
                                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                                        var mdyphrsec=mdyphr[1].split(':');
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                    }
                                    else
                                    {
                                        //alert('Second Date');
                                        var mdyhrsec=mdyhr[1].split(':');
                                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                                    }
                                }
                                return Date.parse(date) > Date.parse(datep);
                            }
                            else
                            {
                                return false;
                            }
                        }

                        //Function for CompareToForGreater
                        function CompareToForSmaller(value,params)
                        {
                            if(value!='' && params!=''){

                                var mdy = value.split('/')  //Date and month split
                                var mdyhr=mdy[2].split(' ');  //Year and time split
                                var mdyp = params.split('/')
                                var mdyphr=mdyp[2].split(' ');


                                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                                {
                                    //alert('Both Date');
                                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                                }
                                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                                {
                                    //alert('Both DateTime');
                                    var mdyhrsec=mdyhr[1].split(':');
                                    var date =  new Date( mdyhr[0], parseFloat(mdy[1])-1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var mdyphrsec=mdyphr[1].split(':');
                                    var datep =  new Date(mdyphr[0], parseFloat(mdyp[1])-1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                }
                                else
                                {
                                    //alert('one Date and One DateTime');
                                    var a = mdyhr[1];  //time
                                    var b = mdyphr[1]; // time

                                    if(a == undefined && b != undefined)
                                    {
                                        //alert('First Date');
                                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                                        var mdyphrsec=mdyphr[1].split(':');
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                    }
                                    else
                                    {
                                        //alert('Second Date');
                                        var mdyhrsec=mdyhr[1].split(':');
                                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                                    }
                                }

                                return Date.parse(date) < Date.parse(datep);
                            }
                            else
                            {
                                return false;
                            }
                        }

                        //Function for CompareToWithoutEqual
                        function CompareToWithoutEqual(value,params)
                        {
                            var mdy = value.split('/')  //Date and month split
                            var mdyhr=mdy[2].split(' ');  //Year and time split
                            var mdyp = params.split('/')
                            var mdyphr=mdyp[2].split(' ');


                            if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                            {
                                //alert('Both Date');
                                var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                                var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                            }
                            else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                            {
                                //alert('Both DateTime');
                                var mdyhrsec=mdyhr[1].split(':');
                                var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                var mdyphrsec=mdyphr[1].split(':');
                                var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                            }
                            else
                            {
                                //alert('one Date and One DateTime');
                                var a = mdyhr[1];  //time
                                var b = mdyphr[1]; // time

                                if(a == undefined && b != undefined)
                                {
                                    //alert('First Date');
                                    var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                                    var mdyphrsec=mdyphr[1].split(':');
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                }
                                else
                                {
                                    //alert('Second Date');
                                    var mdyhrsec=mdyhr[1].split(':');
                                    var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                                }
                            }
                            return Date.parse(date) > Date.parse(datep);
                        }
                        //

                    </script>





                    <script type="text/javascript">

                        function numeric(value) {
                            return /^\d+$/.test(value);
                        }

                        function chkRefNoBlank(obj){
                            var boolcheck1='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(obj.value!='' && obj.value.charAt(0) != ' '){
                                if(obj.value.length <= 50)
                                {
                                    document.getElementById("refno_"+i).innerHTML="";
                                    boolcheck1='true';
                                }
                                else
                                {
                                    document.getElementById("refno_"+i).innerHTML="<br/>Maximum 50 characters are allowed";
                                    boolcheck1=false;
                                }
                            }else{
                                document.getElementById("refno_"+i).innerHTML = "<br/>Please enter Ref. No";
                                boolcheck1=false;
                            }

                            if(boolcheck1==false){
                                document.getElementById("boolcheck").value=boolcheck1;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkPhaseSerBlank(obj){
                            var boolcheck2='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(obj.value!='' && obj.value.charAt(0) != ' ')
                            {
                                if(obj.value.length <= 500)
                                {
                                    document.getElementById("phaseSer_"+i).innerHTML="";
                                    boolcheck2='true';
                                }
                                else
                                {
                                    document.getElementById("phaseSer_"+i).innerHTML="<br/>Maximum 500 characters are allowed";
                                    boolcheck2=false;
                                }
                            }else{
                                document.getElementById("phaseSer_"+i).innerHTML = "<br/>Please enter Phasing of service";
                                boolcheck2=false;
                            }
                            if(boolcheck2==false){
                                document.getElementById("boolcheck").value=boolcheck2;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkLocRefBlank(obj){
                            var boolcheck4='true';

                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(obj.value!=''){
                                if(obj.value.length <= 100)
                                {
                                    document.getElementById("locRef_"+i).innerHTML="";
                                    boolcheck4='true';
                                }
                                else
                                {
                                    document.getElementById("locRef_"+i).innerHTML="<br/>Maximum 100 characters are allowed";
                                    boolcheck4=false;
                                }
                            }else{
                                document.getElementById("locRef_"+i).innerHTML = "<br/>Please enter Location";
                                boolcheck4=false;
                            }
                            if(boolcheck4==false){
                                document.getElementById("boolcheck").value=boolcheck4;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkIndStartBlank(obj){
			    vbooltemp=true;
                            var boolcheck5='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            var closeDate=document.getElementById("txtpreQualCloseDate").value;
                            var year=parseInt(closeDate.split('/')[2]);
                            var month=(closeDate.split('/')[1]);
                            var day=(closeDate.split('/')[0]);
                            var date =  new Date(year, (month-1), day);

                            var objdate=obj.value;
                            var yearobj=parseInt(objdate.split('/')[2]);
                            var monthobj=(objdate.split('/')[1]);
                            var dayobj=(objdate.split('/')[0]);
                            var dateobj =  new Date(yearobj, (monthobj-1), dayobj);

                            if(obj.value!=''){
                                if(Date.parse(dateobj) > Date.parse(date))
                                {
                                    document.getElementById("indStart_"+i).innerHTML="";
                                    boolcheck5='true';
                                }
                                else
                                {
                                    document.getElementById("indStart_"+i).innerHTML="<br/>Indicative Start Date must be greater than EOI Closing Date and Time";
                                    boolcheck5=false;
                                }
                            }else{
                                document.getElementById("indStart_"+i).innerHTML = "<br/>Please enter Indicative Start Date";
                                boolcheck5=false;
                            }
                            if(boolcheck5==false){
                                document.getElementById("boolcheck").value=boolcheck5;
				    vbooltemp=false;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
				    vbooltemp=true;
                            }
                        }
                        function chkIndCompBlank(obj){
                            var boolcheck6='true';
				vbooltemp=true;
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));

                            if(obj.value!=''){
                                if(CompareToForGreater(obj.value,document.getElementById("txtindicativeStartDate_"+i).value))
                                {
                                    document.getElementById("indComp_"+i).innerHTML="";
                                    boolcheck6='true';
                                }
                                else
                                {
                                    document.getElementById("indComp_"+i).innerHTML="<br/>Indicative Completion Date must be greater than Indicative Start Date";
                                    boolcheck6=false;
                                }
                            }else{
                                document.getElementById("indComp_"+i).innerHTML = "<br/>Please enter Indicative Completion Date";
                                boolcheck6=false;
                            }

                            if(boolcheck6==false){
                                document.getElementById("boolcheck").value=boolcheck6;
				    vbooltemp=false;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
				    vbooltemp=true;
                            }

                        }
                        function chkLocLotBlank(obj){
                            var boolcheck7='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(obj.value!=''){
                                if(obj.value.length <= 100)
                                {
                                    document.getElementById("locLot_"+i).innerHTML="";
                                    boolcheck7='true';
                                }
                                else
                                {
                                    document.getElementById("locLot_"+i).innerHTML="<br/>Maximum 100 characters are allowed";
                                    boolcheck7=false;
                                }
                            }else{
                                document.getElementById("locLot_"+i).innerHTML = "<br/>Please enter Location";
                                boolcheck7=false;
                            }
                            if(boolcheck7==false){
                                document.getElementById("boolcheck").value=boolcheck7;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkAmountLotBlank(obj){
                            var boolcheck8='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(obj.value!='')
                            {
                                if(numeric(obj.value))
                                {
                                    document.getElementById("amountLot_"+i).innerHTML="";
                                    boolcheck8='true';
                                    tenderSecAmt(obj);
                                }
                                else
                                {
                                    document.getElementById("amountLot_"+i).innerHTML="<br/>Please enter Numeric Data";
                                    var temp = obj.id.split("_");
                                    var countTSA = temp[1];
                                    $(".tenderSecAmtInWords_" + countTSA).remove();
                                    var temp = obj.id.split("_");
                                    var countTSA = temp[1];
                                    $(".tenderSecAmtInWords_" + countTSA).remove();
                                    boolcheck8=false;
                                }

                            }
                            else{
                                document.getElementById("amountLot_"+i).innerHTML = "<br/>Please enter Tender Security Amount";
                                var temp = obj.id.split("_");
                                var countTSA = temp[1];
                                $(".tenderSecAmtInWords_" + countTSA).remove();
                                boolcheck8=false;
                            }
                            if(boolcheck8==false){
                                document.getElementById("boolcheck").value=boolcheck8;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkAmountSecurity(msg){
                            var boolcheck8='true';
                            var obj = document.getElementById("txtsecurityAmountService_0");
                            //var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(document.getElementById("txtsecurityAmountService_0").value!='')
                            {
                                if(document.getElementById("txtsecurityAmountService_0").value> 0){
                                if(numeric(document.getElementById("txtsecurityAmountService_0").value))
                                {
                                    document.getElementById("amountLotService").innerHTML="";
                                    boolcheck8='true';
                                    tenderSecAmt(document.getElementById("txtsecurityAmountService_0"));
                                }
                                else
                                {
                                    document.getElementById("amountLotService").innerHTML="<br/>Please enter Numeric Data";
                                    //var temp = obj.id.split("_");
                                    //var countTSA = temp[1];
                                    $(".tenderSecAmtInWords_0").remove();
                                    $(".tenderSecAmtInWords_0").remove();
                                    boolcheck8=false;
                                }
                            }else{
                                document.getElementById('amountLotService').innerHTML='<div class="reqF_1">Only 0 value is not allowed</div>';
                                var temp = obj.id.split("_");
                                var countTSA = temp[1];
                                $(".tenderSecAmtInWords_" + countTSA).remove();
                                boolcheck8 = false;
                            }
                            }else{
                                document.getElementById("amountLotService").innerHTML = "<br/>"+msg;
                                var temp = obj.id.split("_");
                                var countTSA = temp[1];
                                $(".tenderSecAmtInWords_" + countTSA).remove();
                                boolcheck8=false;
                            }
                            if(boolcheck8==false){
                                document.getElementById('combineTenderSecurityAmount').value ='';
                                document.getElementById("boolcheck").value=boolcheck8;
                            }else
                            {
                                document.getElementById('combineTenderSecurityAmount').value = obj.value;
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkDocFeesLotBlank(obj){
                            var boolcheck9='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(!regForSpace(obj.value) && obj.value!='')
                            {
                                if(numeric(obj.value))
                                {
                                    document.getElementById("docFees_"+i).innerHTML="";
                                    boolcheck9='true';
                                    docFeesWord(obj);
                                }
                                else
                                {
                                    document.getElementById("docFees_"+i).innerHTML="<br/>Please Enter Numeric Data";
                                    var temp = obj.id.split("_");
                                    var countDF = temp[1];
                                    $(".docFeesInWords_" + countDF).remove();
                                    var temp = obj.id.split("_");
                                    var countDF = temp[1];
                                    $(".docFeesInWords_" + countDF).remove();
                                    boolcheck9=false;
                                }

                            }else{
                                document.getElementById("docFees_"+i).innerHTML = "<br/>Please enter Document fees amount in Nu.";
                                var temp = obj.id.split("_");
                                var countDF = temp[1];
                                $(".docFeesInWords_" + countDF).remove();
                                boolcheck9=false;
                            }
                            if(boolcheck9==false){
                                document.getElementById("boolcheck").value=boolcheck9;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkCompTimeLotBlank(obj){
                            var boolcheck10='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(obj.value!=''){
                                if(obj.value.length <= 100)
                                {
                                    document.getElementById('compLot_'+i).innerHTML='';
                                    boolcheck10='true';
                                }
                                else
                                {
                                    document.getElementById('compLot_'+i).innerHTML='<br/>Maximum 100 characters are allowed';
                                    boolcheck10=false;
                                }
                            }else{
                                //document.getElementById('compLot_'+i).innerHTML = '<br/>Please enter Completion Date';
                                //boolcheck10=false;
                            }
                            if(boolcheck10==false){
                                document.getElementById('boolcheck').value=boolcheck10;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                        }
                        function chkStartTimeLotBlank(obj){
                            var boolcheck11='true';
                            var i = (obj.id.substr(obj.id.indexOf("_")+1));
                            if(obj.value!=''){
                                if(obj.value.length <= 100)
                                {
                                    document.getElementById('startLot_'+i).innerHTML='';
                                    boolcheck11='true';
                                }
                                else
                                {
                                    document.getElementById('startLot_'+i).innerHTML='<br/>Maximum 100 characters are allowed';
                                    boolcheck11=false;
                                }
                            }else{
                                //document.getElementById('startLot_'+i).innerHTML = '<br/>Please enter Start Date';
                                //boolcheck11=false;
                            }
                            if(boolcheck11==false){
                                document.getElementById('boolcheck').value=boolcheck11;
                            }else
                            {
                                document.getElementById("boolcheck").value='true';
                            }
                            return boolcheck11;
                        }

                        //end
                    </script>

                </div>
            </div>


        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>

    </body>

     <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabOfflineData");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
</html>
