<%--
    Document   : TenderDashboardOfflineApproval
    Created on : Aug 27, 2012, 11:13:52 AM
    Author     : Istiak
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Offline Tender/Proposal</title>

        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <script src="resources/config.js" type="text/javascript"></script>
        <link href="resources/editor.css" type="text/css" rel="stylesheet"/>
        <script src="resources/en.js" type="text/javascript"></script>

        <!-- jQuery Datatable -->
        <script type="text/javascript" src="../resources/js/jquery.dataTables.js"></script>
        <link href="../resources/css/demo_table.css" type="text/css" rel="stylesheet">

        <style type="text/css">

            .link{
                color: #78A951;
                text-decoration: underline;
            }
        </style>

        <script type="text/javascript">

            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            function GetCalWithouTime(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            // Load Procurement Methods
            function setSerType(obj,flag){

                if("Works"==obj.options[obj.selectedIndex].text){

                    $.post("<%=request.getContextPath()%>/APPServlet", {projectId:'No',param4: $('#hdnPM').val(),procNature:$('#procNature').val(),funName:'getPM'},  function(j){
                        $("select#cmbProcMethod").html(j);
                    });
                }
                else if("Goods" == obj.options[obj.selectedIndex].text){
                    $.post("<%=request.getContextPath()%>/APPServlet", {projectId:'No',procType:$('#cmbType').val(),param4: $('#hdnPM').val(),procNature:$('#procNature').val(),funName:'getPM'},  function(j){
                        $("select#cmbProcMethod").html(j);
                    });
                }
                else
                {
                    j = "<option value=''>-- Please Select Procurement Method --</option>" +
                        "<option value='Quality Cost Based Selection(QCBS)'>Quality Cost Based Selection(QCBS)</option>" +
                        "<option value='Design Contest'>Design Contest(DC)</option>" +
                        "<option value='Individual Consultant'>Individual Consultant(IC)</option>" +
                        "<option value='Selection under a Fixed Budget(SFB)'>Selection under a Fixed Budget(SFB)</option>" +
                        "<option value='Least Cost Selection(LCS)'>Least Cost Selection(LCS)</option>" +
                        "<option value='Selection Community Service Organisation(SCSO)'>Selection Community Service Organisation(SCSO)</option>" +
                        "<option value='Single Source Selection(SSS)'>Single Source Selection(SSS)</option>" +
                        "<option value='Selection of Individual Consultant(SIS)'>Selection of Individual Consultant(SIS)</option>";
                    $("select#cmbProcMethod").html(j);
                }
            }

            $(document).ready(function () {

                BindGrid("dashboard", "false", "", "", "", "", "Pending", "", "", ""); //load datatable for alltype of category and file type.Default load.

                $('#btnReset').click(function(){
                    reset();

                    var status = $('#cmbStatus').val();
                    BindGrid("dashboard", "false", "", "", "", "", status, "", "", "");
                })

                $('#btnSearch').click(function(){

                    var moduleFlag = "dashboard";
                    var searchFlag = 'true';
                    var procNature = $('#procNature').val();
                    var cmbType = $('#cmbType').val();

                    var cmbProcMethod = $('#cmbProcMethod').val();
                    if(cmbProcMethod != "" && cmbProcMethod != "0")
                    {
                        cmbProcMethod = $('#cmbProcMethod option:selected').text();
                    }
                    
                    var id = $('#tenderId').val();
                    var cmbStatus = $('#cmbStatus').val();
                    var refNo  = $('#refNo').val();
                    var txtpubDatefrom = $('#txtpubDatefrom').val();
                    if(txtpubDatefrom != '')
                    {
                        txtpubDatefrom = dateFormate(txtpubDatefrom);
                    }

                    var txtpubDateto = $('#txtpubDateto').val();
                    if(txtpubDateto != '')
                    {
                        txtpubDateto = dateFormate(txtpubDateto);
                    }

                    $("#approveTab").removeClass("sMenu");
                    $("#pendingTab").removeClass("sMenu");

                    var flag = CompareDate();

                    if(flag)
                    {
                        BindGrid(moduleFlag, searchFlag, procNature, cmbType, cmbProcMethod, id, cmbStatus, refNo, txtpubDatefrom, txtpubDateto);
                    }
                })

                $("#frmSearchTenderOffline").validate({
                    rules: {
                       tenderId: {number: true}
                    },
                    messages: {
                       tenderId: { number: "<div class='reqF_1'>Numeric Only</div>"}
                    }
                });

            }); // End Document.Ready

            function CompareDate(){
                var pubDtfrm = document.getElementById('txtpubDatefrom').value;
                var pubDtTo = document.getElementById('txtpubDateto').value;
                if(pubDtfrm!=null && pubDtTo!=null){
                    if(pubDtfrm!='' && pubDtTo!=''){
                        var mdy = pubDtfrm.split('/')  //Date and month split
                        var mdyhr= mdy[2].split(' ');  //Year and time split

                        var mdy1 = pubDtTo.split('/')  //Date and month split
                        var mdyhr1= mdy1[2].split(' ');  //Year and time split

                        if(mdyhr[1] == undefined){
                            var dt_pubDateFrm = new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                        }

                        if(mdyhr1[1] == undefined){
                            var dt_pubDateTo =new Date(mdyhr1[0], mdy1[1]-1, mdy1[0]);
                        }

                        if(Date.parse(dt_pubDateFrm) < Date.parse(dt_pubDateTo)){
                            document.getElementById('valAll').innerHTML='';

                            return true;
                        }else{
                            document.getElementById('valAll').innerHTML='Publishing Date From should be less then Publishing Date To';

                            return false;
                        }
                    }
                }
                return true;
            }

            function dateFormate(date)
            {
                var split = date.split('/');
                return split[2] + "-" + split[1] + "-" + split[0];
            }

            function confirmation()
            {
                var del = confirm("Are you sure you want to delete the Tender/REOI?");
                return del;
            }

            function reset()
            {
                $('#procNature').val('');
                $('#cmbType').val('');
                $('#cmbProcMethod').val('');
                $('#tenderId').val('');
                //$('#cmbStatus').val('');
                $('#txtpubDatefrom').val('');
                $('#txtpubDateto').val('');
            }

            function changeTab(tabNo){
                if(tabNo == 1){
                    $("#approveTab").removeClass("sMenu");
                    $("#pendingTab").addClass("sMenu");
                    $('#cmbStatus').val('Pending');
                    reset();                    
                    BindGrid("dashboard", "false", "", "", "", "", "Pending", "", "", "");
                }
                else if(tabNo == 2){
                    $("#approveTab").addClass("sMenu");
                    $("#pendingTab").removeClass("sMenu");
                    $('#cmbStatus').val('Approved');
                    reset();
                    BindGrid("dashboard", "false", "", "", "", "", "Approved", "", "", "");
                }
            }

            // Bind dataTable
            function BindGrid(moduleFlag, searchFlag, procNature, procType, procMethod, id, status, refNo, pubDateFrom, pubDateTo) {
                if (typeof oTable == 'undefined') {
                    oTable = $('#gridTender').dataTable({
                        "sDom": '<rt"top"><"bottom"li><"clear">',
                        "sPaginationType": "full_numbers",
                        "bProcessing": true,
                        //"bServerSide": true,
                        "bDestroy": true,
                        "bRetrieve": true,
                        "aoColumns": [null, null, null, null, null, null, null],
                        "sAjaxSource": "<%=request.getContextPath()%>/TenderDashboardOfflineServlet?action=bindGrid",
                        "fnServerData": function (sSource, aoData, fnCallback) {
                            $.ajax({
                                "dataType": 'json',
                                "type": "POST",
                                "url": sSource,
                                "data": { "moduleFlag": moduleFlag, "searchFlag" : searchFlag, "procNature" : procNature, "procType" : procType, "procMethod" : procMethod, "id" : id, "status": status, "refNo" : refNo, "pubDateFrom" : pubDateFrom, "pubDateTo" : pubDateTo },
                                "success": function (aoData) {
                                 /*if (aoData.Error != undefined) {
                                     alert(aoData.Error);
                                 }*/
                                    fnCallback(aoData);
                                }
                            });
                        }
                    });
                }
                else {
                    $('#gridTender').dataTable({
                        "sDom": '<rt"top"><"bottom"li><"clear">',
                        "sPaginationType": "full_numbers",
                        "bProcessing": true,
                        //"bServerSide": true,
                        "bDestroy": true,
                        "aoColumns": [null, null, null, null, null, null, null],
                        "sAjaxSource": "<%=request.getContextPath()%>/TenderDashboardOfflineServlet?action=bindGrid",
                        "fnServerData": function (sSource, aoData, fnCallback) {
                            $.ajax({
                                "dataType": 'json',
                                "type": "POST",
                                "url": sSource,
                                "data": { "moduleFlag": moduleFlag, "searchFlag" : searchFlag, "procNature" : procNature, "procType" : procType, "procMethod" : procMethod, "id" : id, "status": status, "refNo" : refNo, "pubDateFrom" : pubDateFrom, "pubDateTo" : pubDateTo  },
                                "success": function (aoData) {
                                 /*if (aoData.Error != undefined) {
                                     alert(aoData.Error);
                                 }*/
                                    fnCallback(aoData);
                                }
                            });
                        }
                    });

                }
            } //end of Bind

        </script>

    </head>

    <body>
        <div class="mainDiv">
          <div class="fixDiv">

              <div class="contentArea_1">

              <%@include  file="../resources/common/AfterLoginTop.jsp"%>
              <form id="frmSearchTenderOffline" name="frmSearchTenderOffline" method="POST" action="">

               <div class="formBg_1">
                    <jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>

                    <table id="tblSearchBox" cellspacing="10" class="formStyle_1" width="100%">

                        <tr>
                            <td width="17%" class="ff">Procurement Category :</td>
                            <td width="33%">
                                <select name="procNature" id="procNature" class="formTxtBox_1" id="select2" style="width:208px;" onchange="setSerType(this,true)">
                                    <option value="" selected="selected">-- Select Category --</option>
                                    <option value="Goods">Goods</option>
                                    <option value="Works">Works</option>
                                    <option value="Service">Service</option>
                                </select>
                            </td>
                            <td width="17%" class="ff">
                                <input type="hidden" id="status" value="Pending"/>
                                <input type="hidden" id="statusTab" value="Under Preparation"/><!--bug id :: 1397 Live -->
                            </td>
                            <td width="33%"></td>
                        </tr>
                        <tr>
                            <td class="ff">Procurement Type : </td>
                            <td>
                                <select name="procType" class="formTxtBox_1" id="cmbType" style="width:208px;">
                                    <option value="">-- Select Type --</option>
                                    <option value="NCT">NCB</option>
                                    <option value="ICT">ICB</option>
                                </select>
                            </td>
                            <td class="ff"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="ff">Procurement Method :</td>
                            <td>
                                <select name="procMethod" class="formTxtBox_1" id="cmbProcMethod" style="width:208px;">
                                    <option value="" selected="selected">-- Select Method --</option>
                                    <!--<c:forEach var="procMethod" items="${advAppSearchSrBean.procMethodList}">
                                        <option value="Open Tendering Method">Open Tendering Method (OTM)</option>
                                         <option value="Two Stage Tendering Method">Two Stage Tendering Method (TSTM)</option>
                                         <option value="Request For Quotation">Request For Quotation (RFQ)</option>
                                    </c:forEach>-->
                                </select>
                            </td>
                            <td class="ff"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="ff">ID : </td>
                            <td><input type="text" class="formTxtBox_1" id="tenderId" name="tenderId" style="width:202px;" /></td>
                            <td class="ff">Reference No :</td>
                            <td><input type="text" class="formTxtBox_1" id="refNo" style="width:200px;" /></td>
                        </tr>
                        <tr>
                            <td class="ff">Publishing Date From :</td>
                             <td>
                                <!--<input value="" name="txtNOADate" id="txtNOADate" type="text"/>-->

                                <input name="txtpubDatefrom" class="formTxtBox_1" id="txtpubDatefrom" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtpubDatefrom','txtpubDatefrom');" type="text">
                                <img id="txtpubDatefromimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtpubDatefrom','txtpubDatefromimg');" border="0">

                            </td>
                            <td class="ff">Publishing Date To :</td>
                             <td>

                                <input name="txtpubDateto" class="formTxtBox_1" id="txtpubDateto" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtpubDateto','txtpubDateto');" type="text">
                                <img id="imgpubDateto" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtpubDateto','imgpubDateto');" border="0">

                            </td>
                        </tr>
                        <tr>
                            <td width="16%" class="ff">Status : <span>*</span></td>
                            <td width="32%">
                                <select name="select2" class="formTxtBox_1" id="cmbStatus" style="width:85px;" >
                                    <option value="Pending" selected="selected">Pending</option>
                                    <option value="Approved">Approved</option>

                                </select>
                            </td>
                            <td width="15%" class="ff"></td>
                            <td width="38%"></td>
                        </tr>
                        <tr>
                            <td class="ff">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="ff">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="t-align-center" colspan="4">
                                <div id="valAll" class="reqF_1"></div>
                                <input type="hidden" name="hdnPM" id="hdnPM" value=""/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center"><label class="formBtn_1">
                                    <input type="button" name="btnSearch" id="btnSearch" value="Search" />
                                </label>
                                &nbsp;
                                <label class="formBtn_1">
                                    <input type="reset" name="btnReset" id="btnReset" value="Reset" />
                                </label></td>
                        </tr>
                        <tr>
                            <td class="t-align-center" colspan="4">
                                
                            </td>
                        </tr>
                    </table>

                </div>

              </form>
          <!--  Start   -->

           <div class="tableHead_1 t_space">Tender/Proposal Search Result</div>

               <ul class="tabPanel_1 t_space" id="tabForApproved">
                    <li><a href="javascript:void(0);" id="pendingTab" onclick="changeTab(1);" class="sMenu" >Pending</a></li>
                    <li><a href="javascript:void(0);" id="approveTab" onclick="changeTab(2); ">Approved</a></li>

                </ul>

                <div id="gridContainer">
                    <table cellpadding="0" cellspacing="0" border="1" class="display" id="gridTender">
                        <thead>
                            <tr style="text-align: center">
                                <th width="4%">Sl. No.</th>
                                <th width="11%">Reference No., <br />Tender/Proposal Status</th>
                                <th width="30%">Procurement Category, <br />Title</th>
                                <th width="18%">Ministry/Division, <br />Organization, <br />PA</th>
                                <th width="10%">Type, <br />Method</th>
                                <th width="12%">Publishing Date | <br />Closing Date</th>
                                <th width="15%">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

          <!--  END -->
            </div>
          </div>

            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>
    </body>
</html>
