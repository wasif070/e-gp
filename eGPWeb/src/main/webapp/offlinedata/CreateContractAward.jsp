
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblContractAwardedOffline"%>
<%@page import="com.cptu.egp.eps.web.utility.CommonUtils"%>
<%@page import="com.cptu.egp.eps.web.offlinedata.OfflineDataSrBean"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblStateMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.model.table.TblDepartmentMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ManageEmployeeGridSrBean"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.web.servicebean.AwardedContractOfflineSrBean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>

    <head>
             <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
            %>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Create Contract Awards</title>
     
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>

        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <script type="text/javascript" src="resources/ddlevelsmenu.js"></script>
        <script src="resources/ConvertToWord.js" type="text/javascript"></script>     
        <script src="resources/CommonValidation.js" type="text/javascript"></script>
        <script type="text/javascript" src="resources/ckeditor.js"></script>       
        <script type="text/javascript">
            var holiArray = new Array();
        </script>
        <script type="text/javascript">        holiArray.push('18/05/2011'); holiArray.push('05/04/2012'); holiArray.push('24/04/2012');</script>    
        <script src="resources/config.js" type="text/javascript"></script>
        <link href="resources/editor.css" type="text/css" rel="stylesheet" />
        <script src="resources/en.js" type="text/javascript"></script>
        <style type="text/css" id="Style1">.wrc_whole_window{	display: none; 	position: fixed; 	z-index: 2147483647;	background-color: rgba(40, 40, 40, 0.9);	word-spacing: normal;	margin: 0px;	padding: 0px;	border: 0px;	line-height: normal;	letter-spacing: normal;}.wrc_middle_main {	font-family: Segoe UI, Arial Unicode MS, Arial, Sans-Serif;	font-size: 14px;	width: 600px;	height: auto;	margin: 0px auto;	margin-top: 15%;    background: url("chrome://wrc/skin/png/background-body.png");	background-color: rgb(39, 53, 62);}.wrc_middle_logo {    background: url("chrome://wrc/skin/logo.jpg") no-repeat left bottom;    width: 140px;    height: 42px;    color: orange;    display: table-cell;    text-align: right;    vertical-align: middle;}.wrc_icon_warning {	margin: 20px 10px 20px 15px;	float: left;	background-color: transparent;}.wrc_middle_title {    color: #b6bec7;	height: auto;    margin: 0px auto;	font-size: 2.2em;	white-space: nowrap;	text-align: center;}.wrc_middle_hline {    height: 2px;	width: 100%;    display: block;}.wrc_middle_description {	text-align: center;	margin: 15px;	font-size: 1.4em;	padding: 20px;	height: auto;	color: white;	min-height: 3.5em;}.wrc_middle_actions_main_div {	text-align: center;	margin-bottom: 15px;}.wrc_middle_actions_blue_button {	-moz-appearance: none;	border-radius: 7px;	-moz-border-radius: 7px/7px;	border-radius: 7px/7px;	background-color: rgb(0, 173, 223) !important;	display: inline-block;	width: auto;	cursor: Pointer;	border: 2px solid #00dddd;	text-decoration: none;}.wrc_middle_actions_blue_button:hover {	background-color: rgb(0, 159, 212) !important;}.wrc_middle_actions_blue_button:active {	background-color: rgb(0, 146, 200) !important;	border: 2px solid #00aaaa;}.wrc_middle_actions_blue_button div {	display: inline-block;	width: auto;	cursor: Pointer;	margin: 3px 10px 3px 10px;	color: white !important;	font-size: 1.2em;	font-weight: bold;}.wrc_middle_action_low {	font-size: 0.9em;	white-space: nowrap;	cursor: Pointer;	color: grey !important;	margin: 10px 10px 0px 10px;	text-decoration: none;}.wrc_middle_action_low:hover {	color: #aa4400 !important;}.wrc_middle_actions_rest_div {	padding-top: 5px;	white-space: nowrap;	text-align: center;}.wrc_middle_action {	white-space: nowrap;	cursor: Pointer;	color: red !important;	font-size: 1.2em;	margin: 10px 10px 0px 10px;	text-decoration: none;}.wrc_middle_action:hover {	color: #aa4400 !important;}</style>
        

         <!--jquery validator max length css change -->
       <style type="text/css">
           label.error{color:red}
       </style>
        <script type="text/javascript">

            $(document).ready(function(){

             var frank_param = getParam( 'alert' );
                if((frank_param) == 'true')
                {
                    alert("Please select correct excel file");
                }
                
                $("select").each(function() {

                    // Keep track of the selected option.
                    var selectedValue = $(this).val();

                    // Sort all the options by text. I could easily sort these by val.
                    $(this).html($("option", $(this)).sort(function(a, b) {
                        return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
                    }));

                    // Select one option.
                    $(this).val(selectedValue);
                });

                //$('#NOAReason').attr('disabled','disabled');
                $('#trNoaReason').hide();
                $('#trNoaReason').attr('disabled','disabled');
                $('#trPSecurityReason').attr('disabled','disabled');
                $('#trSignedReason').attr('disabled','disabled');
                $('#trPSecurityReason').hide();
                $('#trSignedReason').hide();
                $('#ddlIsSamePersonNOA').change(function (){
                    if($('#ddlIsSamePersonNOA').val()=='2')
                        {
                        $('#trNoaReason').show();
                        $('#trNoaReason').removeAttr('disabled');
                        }
                    else
                        {
                        $('#trNoaReason').hide();
                        $('#trNoaReason').attr('disabled','disabled');
                        }
                     
                });

               // $('#perSecurityReason').attr('disabled','disabled');
                $('#ddlIsPSecurityDueTime').change(function (){
                    if($('#ddlIsPSecurityDueTime').val()=='2'){
                        $('#perSecurityReason').removeAttr('disabled');
                        $('#trPSecurityReason').show();
                    }
                    else{
                         $('#trPSecurityReason').hide();
                         $('#perSecurityReason').attr('disabled','disabled');
                    }
                });

              //  $('#signedReason').attr('disabled','disabled');
                $('#ddlIsSignedDueTime').change(function (){
                    if($('#ddlIsSignedDueTime').val()=='2'){

                        $('#signedReason').removeAttr('disabled');
                        $('#trSignedReason').show();
                    }
                    else{
                        $('#signedReason').attr('disabled','disabled');
                        $('#trSignedReason').hide();
                    }
                });

                //Form Validation
                $("#frmUploadOfflineDate").validate({
                    rules: {
                        //textbox
                        fileUploadControl: {required: true}
                         },
                    messages: {
                        //Textbox
                        fileUploadControl: { required: "<div class='reqF_1'>Please select a file.</div>"}
                    }
                });
                
                $("#frmContractAward").validate({
                    rules: {
                        //textbox
                        peName: {required: true,maxlength:150},
                        invitationRefNo: {required: true,maxlength:50},
                        authorisedOfficerName:{required: true,maxlength:300},
                        authorisedOfficerDesignation:{required: true,maxlength:300},
                        contractDescription: {required: true,maxlength:2000},
                        contractValue: {required: true, number: true,maxlength:15},
                        nameCompany:{required: true,maxlength:15},
                        locationCompany:{required: true,maxlength:1000},
                        locationDelWrkSer:{required: true,maxlength:1000},
                       // NOAReason:{required: true},
                       // perSecurityReason:{required: true},
                      //  signedReason:{required: true},
                        packageNo:{required: true,maxlength:100},
                        packageName:{required: true,maxlength:150},
                        advertisementDate:{required: true, date:true},
                        NOADate:{required: true, date:true},
                        contractSignDate:{required: true, date:true},
                        contractCompletionDate:{required: true,  date:true},
                        noBidSold:{required: true,maxlength:2000},
                        noBidReceived:{required: true,maxlength:2000},
                        noResponsiveTenderer:{required: true,maxlength:2000},
                        devPartner:{maxlength:300},
                        projectCode:{maxlength:150},
                        projectName:{maxlength:150},
                        peCode:{maxlength:15},
                       

                        //Dropdown
                        Ministry:{selectNone: true},
                        Organization: {selectNone: true},
                        district:{selectNone: true},
                        ddlSourceOfFund:{selectNone: true},
                        budgetType:{selectNone: true},
                        sourceOfFund:{selectNone: true},
                        procurementMethod:{selectNone: true},
                        contractAwardFor:{selectNone: true}

                    },
                    messages: {
                        //Textbox
                        peName: { required: "<div class='reqF_1'>Please enter Procuring Entity Name.</div>"},
                        invitationRefNo: { required: "<div class='reqF_1'>Please enter Referance No.</div>"},
                        authorisedOfficerName :{ required: "<div class='reqF_1'>Please enter Name.</div>"},
                        authorisedOfficerDesignation:{ required: "<div class='reqF_1'>Please enter Designation.</div>"},
                        contractDescription: { required: "<div class='reqF_1'>Please enter Contract Description.</div>"},
                        contractValue: { required: "<div class='reqF_1'>Please enter Contract Value.</div>", number: "<div class='reqF_1'>Numeric Only</div>"},
                        nameCompany:{ required: "<div class='reqF_1'>Please enter Name of Supplier/Contractor/Consultant.</div>"},
                        locationCompany:{ required: "<div class='reqF_1'>Please enter Location of Supplier/Contractor/Consultant.</div>"},
                        locationDelWrkSer:{required: "<div class='reqF_1'>Please enter Location of Delivery/Works/Consultancy.</div>"},
                       // NOAReason:{required: "<div class='reqF_1'>Please enter Reason.</div>"},
                      //  perSecurityReason:{required: "<div class='reqF_1'>Please enter Reason.</div>"},
                      //  signedReason:{required: "<div class='reqF_1'>Please enter Reason.</div>"},
                        packageNo:{required: "<div class='reqF_1'>Please enter Package No.</div>"},
                        packageName:{required: "<div class='reqF_1'>Please enter Package Name.</div>"},
                        advertisementDate:{required: "<div class='reqF_1'>Please select Advertisement Date.</div>", date: "<div class='reqF_1'>Invalid Date Format</div>"},
                        NOADate:{required: "<div class='reqF_1'>Please select Letter of Acceptance Date.</div>", date: "<div class='reqF_1'>Invalid Date Format</div>"},
                        contractSignDate:{required: "<div class='reqF_1'>Please select Contract Sigining Date.</div>", date: "<div class='reqF_1'>Invalid Date Format</div>"},
                        contractCompletionDate:{required: "<div class='reqF_1'>Please select Contract Completion Date.</div>", date: "<div class='reqF_1'>Invalid Date Format</div>"},
                        noBidSold:{required: "<div class='reqF_1'>Please enter No. of Tenders/Proposals Sold.</div>"},
                        noBidReceived:{required: "<div class='reqF_1'>Please enter No. of Tenders/Proposals Received .</div>"},
                        noResponsiveTenderer:{required: "<div class='reqF_1'>Please enter No. of Tenders Responsive.</div>"},

                        //Dropdown
                        Ministry:{ selectNone: "<div class='reqF_1'>Please select Ministry.</div>"},
                        Organization:{ selectNone: "<div class='reqF_1'>Please select Organization.</div>"},
                        district:{ selectNone: "<div class='reqF_1'>Please select Dzongkhag / District.</div>"},
                        contractAwardFor:{ selectNone: "<div class='reqF_1'>Please select Contract Award for.</div>"},
                        budgetType:{ selectNone: "<div class='reqF_1'>Please select Budget Type.</div>"},
                        sourceOfFund:{ selectNone: "<div class='reqF_1'>Please select Source of Fund.</div>"},
                        procurementMethod:{ selectNone: "<div class='reqF_1'>Please select Procurement Method.</div>"}

                    }
                });

                ////The following code has been used to adding validation of dropdown list in basic validation plugin
                $.validator.addMethod('selectNone', function (value, element){
                        if ($(element).is(":hidden")){
                            return true;
                        }
                        else{
                            /*
                             *  Here if the value of selected item of dropdown is '0' or '' or 'select' method will return false
                             *  If there is no value like '<option>Item</option>' it will check 'value.indexOf('--')'. Because
                             *  Items are coming from Database and 1st item (default selected item) is '-- Please Select *** --' or '-- Select *** --'
                             *  and other item does not contains '--'.
                             *  That's why 'value.indexOf('--')' is used for checking condition
                             */
                            if (value == 0 || value =='' || value =='select' || value.indexOf('--') != -1)
                            {
                                //alert("not ok");
                                return false;
                            }
                            else
                            {
                                //alert("ok")
                                return true;
                            }
                        }
                    }
                );  //End Form Validation

            }); //  End Document.Ready

            function getParam( name )
            {
             name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
             var regexS = "[\\?&]"+name+"=([^&#]*)";
             var regex = new RegExp( regexS );
             var results = regex.exec( window.location.href );
             if( results == null )
              return "";
            else
             return results[1];
            }
        </script>

        <script type="text/javascript">

                function setOrgval(id){

                var orgObj =document.getElementById(id);
                var orgval = orgObj.options[orgObj.selectedIndex].text;
                
                document.getElementById("hidorg").value = orgval;
            }

                    function loadOrganization() {
                        var deptId= 0;
                       // var districtId = $('#cmbDistrict').val();
                       if($('#cmbDivision').val()>0){
                           deptId=$('#cmbDivision').val();
                       }
                       else{deptId= $('#cmbMinistry').val(); }

                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: deptId, funName:'offlineAwardedOrgCombo'},  function(j){
                            $('#cmbOrganization').children().remove().end()
                            $("select#cmbOrganization").html(j);
                            var orgObj =document.getElementById("cmbMinistry");
                             var orgval = orgObj.options[orgObj.selectedIndex].text;
                             document.getElementById("hidministry").value = orgval;
                        });
                    }
        </script>

        <script type="text/javascript">

            function numeric(value) {
                return /^\d+$/.test(value);
            }

            
            //end
        </script>

        <script type="text/javascript">

            //Function for Required
            function required(controlid) {
                var temp = controlid.length;
                if (temp <= 0) {
                    return false;
                } else {
                    return true;
                }
            }

            //Function for MaxLength
            function Maxlenght(controlid, maxlenght) {
                var temp = controlid.length;
                if (temp >= maxlenght) {
                    return false;
                } else
                    return true;
            }

            //Function for digits
            function digits(control) {
                return /^\d+$/.test(control);
            }

            function CompareToForEqual(value, params) {

                var mdy = value.split('/')  //Date and month split
                var mdyhr = mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr = mdyp[2].split(' ');
                var date = new Date(mdyhr[0], mdy[1], mdy[0]);
                var datep = new Date(mdyphr[0], mdyp[1], mdyp[0]);
                return Date.parse(date) == Date.parse(datep);
            }

            //Function for CompareToForToday
            function CompareToForToday(first) {
                var mdy = first.split('/')  //Date and month split
                var mdyhr = mdy[2].split(' ');  //Year and time split
                var mdyhrtime = mdyhr[1].split(':');
                if (mdyhrtime[1] == undefined) {
                    var valuedate = new Date(mdyhr[0], mdy[1] - 1, mdy[0]);
                } else {
                    var valuedate = new Date(mdyhr[0], mdy[1] - 1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                }

                var d = new Date();
                if (mdyhrtime[1] == undefined) {
                    var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                }
                else {
                    var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes());
                }
                return Date.parse(valuedate) > Date.parse(todaydate);
            }

            //Function for CompareToForGreater
            function CompareToForGreater(value, params) {
                if (value != '' && params != '') {

                    var mdy = value.split('/')  //Date and month split
                    var mdyhr = mdy[2].split(' ');  //Year and time split
                    var mdyp = params.split('/')
                    var mdyphr = mdyp[2].split(' ');


                    if (mdyhr[1] == undefined && mdyphr[1] == undefined) {
                        //alert('Both Date');
                        var date = new Date(mdyhr[0], mdy[1], mdy[0]);
                        var datep = new Date(mdyphr[0], mdyp[1], mdyp[0]);
                    }
                    else if (mdyhr[1] != undefined && mdyphr[1] != undefined) {
                        //alert('Both DateTime');
                        var mdyhrsec = mdyhr[1].split(':');
                        var date = new Date(mdyhr[0], parseFloat(mdy[1]) - 1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var mdyphrsec = mdyphr[1].split(':');

                        var datep = new Date(mdyphr[0], parseFloat(mdyp[1]) - 1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else {
                        //alert('one Date and One DateTime');
                        var a = mdyhr[1];  //time
                        var b = mdyphr[1]; // time

                        if (a == undefined && b != undefined) {
                            //alert('First Date');
                            var date = new Date(mdyhr[0], mdy[1], mdy[0], '00', '00');
                            var mdyphrsec = mdyphr[1].split(':');
                            var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                        }
                        else {
                            //alert('Second Date');
                            var mdyhrsec = mdyhr[1].split(':');
                            var date = new Date(mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                            var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], '00', '00');
                        }
                    }
                    return Date.parse(date) > Date.parse(datep);
                }
                else {
                    return false;
                }
            }

            //Function for CompareToForGreater
            function CompareToForSmaller(value, params) {
                if (value != '' && params != '') {

                    var mdy = value.split('/')  //Date and month split
                    var mdyhr = mdy[2].split(' ');  //Year and time split
                    var mdyp = params.split('/')
                    var mdyphr = mdyp[2].split(' ');


                    if (mdyhr[1] == undefined && mdyphr[1] == undefined) {
                        //alert('Both Date');
                        var date = new Date(mdyhr[0], mdy[1], mdy[0]);
                        var datep = new Date(mdyphr[0], mdyp[1], mdyp[0]);
                    }
                    else if (mdyhr[1] != undefined && mdyphr[1] != undefined) {
                        //alert('Both DateTime');
                        var mdyhrsec = mdyhr[1].split(':');
                        var date = new Date(mdyhr[0], parseFloat(mdy[1]) - 1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var mdyphrsec = mdyphr[1].split(':');
                        var datep = new Date(mdyphr[0], parseFloat(mdyp[1]) - 1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else {
                        //alert('one Date and One DateTime');
                        var a = mdyhr[1];  //time
                        var b = mdyphr[1]; // time

                        if (a == undefined && b != undefined) {
                            //alert('First Date');
                            var date = new Date(mdyhr[0], mdy[1], mdy[0], '00', '00');
                            var mdyphrsec = mdyphr[1].split(':');
                            var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                        }
                        else {
                            //alert('Second Date');
                            var mdyhrsec = mdyhr[1].split(':');
                            var date = new Date(mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                            var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], '00', '00');
                        }
                    }

                    return Date.parse(date) < Date.parse(datep);
                }
                else {
                    return false;
                }
            }

            //Function for CompareToWithoutEqual
            function CompareToWithoutEqual(value, params) {
                var mdy = value.split('/')  //Date and month split
                var mdyhr = mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr = mdyp[2].split(' ');


                if (mdyhr[1] == undefined && mdyphr[1] == undefined) {
                    //alert('Both Date');
                    var date = new Date(mdyhr[0], mdy[1], mdy[0]);
                    var datep = new Date(mdyphr[0], mdyp[1], mdyp[0]);
                }
                else if (mdyhr[1] != undefined && mdyphr[1] != undefined) {
                    //alert('Both DateTime');
                    var mdyhrsec = mdyhr[1].split(':');
                    var date = new Date(mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                    var mdyphrsec = mdyphr[1].split(':');
                    var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                }
                else {
                    //alert('one Date and One DateTime');
                    var a = mdyhr[1];  //time
                    var b = mdyphr[1]; // time

                    if (a == undefined && b != undefined) {
                        //alert('First Date');
                        var date = new Date(mdyhr[0], mdy[1], mdy[0], '00', '00');
                        var mdyphrsec = mdyphr[1].split(':');
                        var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else {
                        //alert('Second Date');
                        var mdyhrsec = mdyhr[1].split(':');
                        var date = new Date(mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], '00', '00');
                    }
                }
                return Date.parse(date) > Date.parse(datep);
            }
            //

        </script>


        <script type="text/javascript">
            function findHoliday(comp, compi) {
                $(".err" + compi).remove();
                var compVal = comp.value;
                var cnt = 0;
                if (compVal != null && compVal != "") {
                    for (var i = 0; i < holiArray.length; i++) {
                        if (CompareToForEqual(holiArray[i], compVal)) {
                            cnt++;
                        }
                    }
                }
                if (cnt != 0) {
                    $('#' + comp.id).parent().append("<div class='err" + compi + "' style='color:red;'>Holiday!</div>");
                }
            }

            function GetCal(txtname, controlname) {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    onSelect: function () {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            function GetCalWithouTime(txtname, controlname) {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat: "%d/%m/%Y",
                    onSelect: function () {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

        </script>

    </head>
       <%
        OfflineDataSrBean offlineDataSrBean = new OfflineDataSrBean();
            String userid = "";
            HttpSession hs = request.getSession();
            if (hs.getAttribute("userId") != null) {
                userid = hs.getAttribute("userId").toString();
                offlineDataSrBean.setLogUserId(userid);
                 hs.setAttribute("formType", "Award");
            }
            System.out.println("userid >> >> "+hs.getAttribute("userId"));

             if( request.getParameter("submit") != null && "Submit".equals(request.getParameter("submit"))){
         try{
            System.out.println("in condition ");
            String ministry = CommonUtils.checkNull(request.getParameter("hidministry"));
            String agency = CommonUtils.checkNull(request.getParameter("hidorg"));
            String peName = CommonUtils.checkNull(request.getParameter("peName"));
            String peCode = CommonUtils.checkNull(request.getParameter("peCode"));
            String district = CommonUtils.checkNull(request.getParameter("district"));
            String contractAwardFor = CommonUtils.checkNull(request.getParameter("contractAwardFor"));
            String invitationRefNo = CommonUtils.checkNull(request.getParameter("invitationRefNo"));
            String procurementMethod = CommonUtils.checkNull(request.getParameter("procurementMethod"));
            String budgetType = CommonUtils.checkNull(request.getParameter("budgetType"));
            String sourceOfFund = CommonUtils.checkNull(request.getParameter("sourceOfFund"));
            String devPartner = CommonUtils.checkNull(request.getParameter("devPartner"));
            String projectCode = CommonUtils.checkNull(request.getParameter("projectCode"));
            String ProjectName = CommonUtils.checkNull(request.getParameter("projectName"));
            String packageNo = CommonUtils.checkNull(request.getParameter("packageNo"));
            String packageName = CommonUtils.checkNull(request.getParameter("packageName"));
            String advertisementDate = CommonUtils.checkNull(request.getParameter("advertisementDate"));
            String NOADate = CommonUtils.checkNull(request.getParameter("NOADate"));
            String contractSignDate = CommonUtils.checkNull(request.getParameter("contractSignDate"));

            String contractCompletionDate = CommonUtils.checkNull(request.getParameter("contractCompletionDate"));
            String noBidSold = CommonUtils.checkNull(request.getParameter("noBidSold"));
            String noBidReceived = CommonUtils.checkNull(request.getParameter("noBidReceived"));
            String noResponsiveTenderer = CommonUtils.checkNull(request.getParameter("noResponsiveTenderer"));
            String contractDescription = CommonUtils.checkNull(request.getParameter("contractDescription"));
            String contractValue = CommonUtils.checkNull(request.getParameter("contractValue"));
            String nameCompany = CommonUtils.checkNull(request.getParameter("nameCompany"));
            String locationCompany = CommonUtils.checkNull(request.getParameter("locationCompany"));
            String locationDelWrkSer = CommonUtils.checkNull(request.getParameter("locationDelWrkSer"));
            String isSamePersonNOA = CommonUtils.checkNull(request.getParameter("isSamePersonNOA"));
            String NOAReason = CommonUtils.checkNull(request.getParameter("NOAReason"));
            String isPSecurityDueTime = CommonUtils.checkNull(request.getParameter("isPSecurityDueTime"));
            String perSecurityReason = CommonUtils.checkNull(request.getParameter("perSecurityReason"));
            String isSignedDueTime = CommonUtils.checkNull(request.getParameter("isSignedDueTime"));
            String signedReason = CommonUtils.checkNull(request.getParameter("signedReason"));
            String authorisedOfficerName = CommonUtils.checkNull(request.getParameter("authorisedOfficerName"));
            String authorisedOfficerDesignation = CommonUtils.checkNull(request.getParameter("authorisedOfficerDesignation"));

            TblContractAwardedOffline contractAwardedOffline = new TblContractAwardedOffline();

            contractAwardedOffline.setMinistry(ministry);
            contractAwardedOffline.setAgency(agency);
            contractAwardedOffline.setPeOfficeName(peName);
            contractAwardedOffline.setPeCode(peCode);
            contractAwardedOffline.setPeDistrict(district);
            contractAwardedOffline.setAwardForPnature(contractAwardFor);
            contractAwardedOffline.setRefNo(invitationRefNo);
            contractAwardedOffline.setProcurementMethod(procurementMethod);
            contractAwardedOffline.setBudgetType(budgetType);
            contractAwardedOffline.setSourceOfFund(sourceOfFund);
            contractAwardedOffline.setDevPartners(devPartner);
            contractAwardedOffline.setProjectCode(projectCode);
            contractAwardedOffline.setProjectName(ProjectName);
            contractAwardedOffline.setUserId(Integer.parseInt(userid));
            contractAwardedOffline.setPackageNo(packageNo);
            contractAwardedOffline.setPackageName(packageName);

           
            System.out.println("advertisementDate >> "+advertisementDate);
            if(advertisementDate != null && !"".equals(advertisementDate)){
                Date adverDate = DateUtils.formatStdString(advertisementDate);
                contractAwardedOffline.setDateofAdvertisement(adverDate);
            }
            if(NOADate != null && !"".equals(NOADate)){
                Date noaDate = DateUtils.formatStdString(NOADate);
                contractAwardedOffline.setDateofNoa(noaDate);
            }
            if(contractSignDate != null && !"".equals(contractSignDate)){
                Date conSignDate = DateUtils.formatStdString(contractSignDate);
                contractAwardedOffline.setDateofContractSign(conSignDate);
            }
            if(contractCompletionDate != null && !"".equals(contractCompletionDate)){
            Date concompleDate = DateUtils.formatStdString(contractCompletionDate);
            contractAwardedOffline.setDateofPccompletion(concompleDate);
            }
            contractAwardedOffline.setSold(noBidSold);
            contractAwardedOffline.setReceived(noBidReceived);
            contractAwardedOffline.setResponse(noResponsiveTenderer);
            contractAwardedOffline.setDescriptionofContract(contractDescription);
            float convalue = 0;
            if (contractValue != null && !"".equals(contractValue)) {
                convalue = Float.parseFloat(contractValue);
            }
            System.out.println("contract"+convalue);
            contractAwardedOffline.setContractValue(new BigDecimal(convalue).setScale(2, 0));
            contractAwardedOffline.setNameofTenderer(nameCompany);
            contractAwardedOffline.setAddressofTenderer(locationCompany);
            contractAwardedOffline.setDeliveryPlace(locationDelWrkSer);
            short samepersonNOA = 0;
            if(isSamePersonNOA.equals("1")){
                samepersonNOA = 1;
            }
            contractAwardedOffline.setIsSamePersonNoa(samepersonNOA);
            contractAwardedOffline.setNoareason(NOAReason);
            short psecurityDueTime = 0;
            if(isPSecurityDueTime.equals("1")){
                psecurityDueTime = 1;
            }
            contractAwardedOffline.setIsPsecurityDueTime(psecurityDueTime);
            contractAwardedOffline.setPsecurityReason(perSecurityReason);
            short signedDuetime = 0;
            if(isSignedDueTime.equals("1")){
                signedDuetime = 1;
            }

            contractAwardedOffline.setIsSignedDueTime(signedDuetime);
            contractAwardedOffline.setSignedReason(signedReason);
            contractAwardedOffline.setOfficerName(authorisedOfficerName);
            contractAwardedOffline.setOfficerDesignation(authorisedOfficerDesignation);
            contractAwardedOffline.setDivision("N/A");
            contractAwardedOffline.setStatus("Pending");
            Date date = DateUtils.formatStdString(new java.util.Date().toString());
            contractAwardedOffline.setDate(date);
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>");
            
            offlineDataSrBean.createContractAwardOfflineData(contractAwardedOffline);
            %>
                <script type="text/javascript">
                  alert("Information Saved Successfully");
                </script>
            <%
           // response.sendRedirect("CreateContractAward.jsp");
           }catch(Exception ex){
           ex.printStackTrace();
           }
         }//else
         {
            
        %>
        <% TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
        List<SPTenderCommonData> holidayList = tenderCommonService.returndata("getHolidayDatesBD", null, null);
        out.print("<script type='text/javascript'>");
        for(SPTenderCommonData holidays : holidayList){
            out.print("holiArray.push('"+holidays.getFieldName1()+"');");
        }
        out.print("</script>");

        %>

   <body>
        
        
        <div class="mainDiv">
            <div class="fixDiv">

         <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->

                <div class="contentArea_1">
                    <div class="t-align-right" style="vertical-align: middle;" id="btnGoBack"><a class="action-button-goback" href="SearchAwardedContractOffline.jsp">Go back</a></div>
                    <form  id="frmUploadOfflineDate" name="frmUploadOfflineDate" method="POST" enctype="multipart/form-data" action="/ExcelUploadAndParseServlet" >
                    <div class="pageHead_1">Add New Contract Awards</div>
                     <div class="tableHead_22 t_space">FILE UPLOAD</div>
                    <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                        <tbody>
                        <tr>
                            <td class="ff" width="30%">Select File:</td>
                            <td width="70%">                                
                                    <input name="fileUploadControl" id="fileUploadControl" type="file"/>                                
                            </td>
                        </tr>

                        <tr>
                            <td class="ff" width="30%"></td>
                            <td width="70%">
                                <!--<button type="button" value="Upload">Upload</button>-->
                                <label class="formBtn_1">
                                    <input type="submit" name="upload" value="Upload"/>
                                </label>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </form>

                    <div class="tableHead_22 t_space">PROCURING ENTITY INFORMATION</div>
                    <form id="frmContractAward" name="frmContractAward" method="POST" action="/offlinedata/CreateContractAward.jsp" >
                    <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                        <tbody>
                        <tr>
                           <td class="ff" width="30%">Ministry/Division : <span>*</span></td>
                            <%
                               // ManageEmployeeGridSrBean manageEmployeeGridSrBean = new ManageEmployeeGridSrBean();
                              //  List<TblDepartmentMaster> departmentMasterList = null;
                              //  departmentMasterList = manageEmployeeGridSrBean.getDepartmentMasterList("Ministry");
                                AwardedContractOfflineSrBean awardedContractOffline = new AwardedContractOfflineSrBean();
                                List<Object[]> ministryListOffline = new ArrayList<Object[]>();
                                ministryListOffline = awardedContractOffline.getMinistryForAwardedContractOffline();

                            %>
                            <td width="70%"> <select class="formTxtBox_1" style="width: 200px;"  name="Ministry" id="cmbMinistry" onchange="loadOrganization();">
                                            <option value="0" selected="selected">--- Please Select ---</option>
                                            <%
                                                  /*  for (int i = 0; i < departmentMasterList.size(); i++)  {
                                                          out.println("<option value='" + departmentMasterList.get(i).getDepartmentId() + "'>" + departmentMasterList.get(i).getDepartmentName() + "</option>");
                                                    }*/
                                            //System.out.print("List Size of Ministry:"+ministryListOffline.size());

                                            /*if(ministryListOffline.size() > 0){
                                                int i=0;
                                                for(Object[] obj :ministryListOffline){
                                                      out.println("<option value='" + obj[i].toString() + "'>" + obj[i].toString() + "</option>");
                                                      i = i+1;
                                                    }
                                                }*/
                                                    for (int i = 0; i < ministryListOffline.size(); i++)  {
                                                          out.println("<option value='" + String.valueOf(ministryListOffline.get(i)) + "'>" + String.valueOf(ministryListOffline.get(i)) + "</option>");
                                                    }

                                           %>
                                 </select>
                                <input class="formTxtBox_1" style="width: 280px;"  id="hidministry"  name="hidministry" type="hidden"  />
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Organization : <span>*</span></td>
                            <td>
                                 <select class="formTxtBox_1" style="width: 200px;"  name="Organization" id="cmbOrganization" onchange="setOrgval(this.id);" >
	                                <option value="select" selected="selected">--- Please Select ---</option>
                                </select>
                                 <input class="formTxtBox_1" style="width: 280px;"  type="hidden" name="hidorg"  id="hidorg"  />
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Procuring Entity Name : <span>*</span></td>
                            <td>
                                <input class="formTxtBox_1" style="width: 280px;"  name="peName" class="formTxtBox_1" id="txtPEName" style="width: 280px;" type="text"   />
                            </td>
                        </tr>

                        <tr>
                            <td class="ff">Procuring Entity Code :</td>
                            <td>
                                <input class="formTxtBox_1" style="width: 280px;"   name="peCode" id="txtProcuringEntityCode" type="text" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Procuring Entity Dzongkhag / District : <span>*</span></td>
                             <%
                                CommonService cservice = (CommonService) AppContext.getSpringBean("CommonService");
                                //Code by Proshanto
                                //short countryId = 136;
                                short countryId = 150;
                                List<TblStateMaster> liststate = cservice.getState(countryId);
                            %>
                           <td> <select class="formTxtBox_1" style="width: 200px;"  name="district" class="formTxtBox_1" id="cmbdistrict" style="width: 200px;">
                                <option value="0" selected="selected">--- Please Select ---</option>
                               <%
                                        for (TblStateMaster state : liststate) {
                                              out.println("<option value='" + state.getStateName() + "'>" + state.getStateName() + "</option>");
                                        }
                               %>

                                </select>
                           </td>
                        </tr>
                        <tr>
                            <td class="ff">Contract Award for : <span>*</span></td>
                            <td>
                                 <select class="formTxtBox_1" style="width: 200px;"  name="contractAwardFor" id="ddlInvitationFor">
	                                <option value="0" selected="selected">-- Select --</option>
                                    <option value="Goods">Goods</option>
                                    <option value="Services">Services</option>
                                    <option value="Works">Works</option>
                                </select>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Invitation/Proposal Reference No. :<span>*</span></td>
                            <td>
                                <input class="formTxtBox_1" style="width: 280px;"  name="invitationRefNo" id="txtReferanceNo" type="text" />
                            </td>
                        </tr>
                    </tbody></table>
                    <div class="tableHead_22 t_space">KEY INFORMATION</div>
                    <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                        <tbody>
                        <tr>
                            <td class="ff" width="30%">Procurement Method : <span>*</span></td>
                            <td width="70%">
                                 <select class="formTxtBox_1" style="width: 200px;"  name="procurementMethod" id="ddlProcurementMethod">
	                                <option value="0" selected="selected">-- Select --</option>
                                    <option value="ICT">ICB</option>
                                    <option value="NCT">NCB</option>
                                    <option value="QCBS">Quality Cost Based Selection(QCBS)</option>
                                    <option value="SFB">Selection under a Fixed Budget(SFB)</option>
                                    <option value="LCS">Least Cost Selection(LCS)</option>
                                    <option value="SCSO">Selection Community Service Organisation(SCSO)</option>
                                    <option value="SSS">Single Source Selection(SSS)</option>
                                    <option value="SIS">Selection of Individual Consultant(SIS)</option>

                                </select>                                
                            </td>
                        </tr>
                    </tbody></table>
                    <div class="tableHead_22 t_space">FUNDING INFORMATION</div>
                    <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                        <tbody><tr>
                            <td class="ff" width="30%">Budget Type : <span>*</span></td>
                            <td width="70%">
                                 <select class="formTxtBox_1" style="width: 200px;"  name="budgetType" id="ddlBudgetTypeID">
	                                <option value="0" selected="selected">-- Select --</option>
                                    <option value="Development Budget">Development Budget</option>
                                    <option value="Revenue Budget">Revenue Budget</option>
                                     <option value="Own Funds">Own Funds</option>
                                </select>                                         
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Source of Funds : <span>*</span></td>
                            <td width="70%">
                                 <select class="formTxtBox_1" style="width: 200px;"  name="sourceOfFund" id="ddlSourceOfFund">
	                                <option value="0" selected="selected">-- Select --</option>
                                            <option  value="Government">Government</option>
                                            <option value="Aid Grant / Credit">Aid Grant / Credit</option>
                                            <option value="Own Funds">Own Funds</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Development Partner (if applicable):</td>
                            <td width="70%">
                                <input class="formTxtBox_1" style="width: 280px;"  name="devPartner" id="txtDevPartner" type="text"/>
                            </td>
                        </tr>
                    </tbody></table>
                    <div class="tableHead_22 t_space">PARTICULAR INFORMATION</div>
                    <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                        <tbody>
                             <tr>
                                 <td class="ff" width="25%">Project Code : </td>
                                <td width="25%">
                                    <input class="formTxtBox_1" style="width: 280px;"  name="projectCode" id="txtProjectCode" type="text"  />
                                </td>
                        </tr>
                       <tr>
                            <td class="ff" width="25%">Project Name : </td>
                            <td width="25%">
                                <input class="formTxtBox_1" style="width: 280px;"  name="projectName" id="txtProjectName" type="text"  />
                            </td>
                            </tr>
                        <tr>
                            <td class="ff" width="30%">Tender/Proposal Package No. : <span>*</span> </td>
                            <td width="70%">
                                <input class="formTxtBox_1" style="width: 280px;"  value="" name="packageNo" id="txtTenderPkgNo" type="text"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Tender/Proposal Package Name : <span>*</span> </td>
                            <td width="70%">
                                <input class="formTxtBox_1" style="width: 280px;"  value="" name="packageName" id="txtTenderPkgName" type="text"/>
                            </td>
                        </tr>
                       
                        <tr>
                            <td class="ff" width="30%">Date of Advertisement:<span>*</span> </td>
                            <td width="70%">
                                <!--<input class="formTxtBox_1" style="width: 280px;"  value="" name="txtAdvertisementDate" id="txtAdvertisementDate" type="text"/>-->

                                <input class="formTxtBox_1" style="width: 100px;"  name="advertisementDate" class="formTxtBox_1" id="txtAdvertisementDate" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtAdvertisementDate','txtAdvertisementDate');" onblur="findHoliday(this,0);" type="text"/>
                                <img id="imgtxtAdvertisementDate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtAdvertisementDate','imgtxtAdvertisementDate');" border="0"/>

                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Date of Letter of Acceptance : <span>*</span> </td>
                            <td width="70%">
                                <!--<input class="formTxtBox_1" style="width: 280px;"  value="" name="txtNOADate" id="txtNOADate" type="text"/>-->

                                <input class="formTxtBox_1" style="width: 100px;"  name="NOADate" class="formTxtBox_1" id="txtNOADate" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtNOADate','txtNOADate');" onblur="findHoliday(this,0);" type="text"/>
                                <img id="imgtxtNOADate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtNOADate','imgtxtNOADate');" border="0"/>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Date of Contract Signing : <span>*</span> </td>
                            <td width="70%">
                                <!--<input class="formTxtBox_1" style="width: 280px;"  value="" name="txtContractSignDate" id="txtContractSignDate" type="text"/>-->

                                <input class="formTxtBox_1" style="width: 100px;"  name="contractSignDate" class="formTxtBox_1" id="txtContractSignDate" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtContractSignDate','txtContractSignDate');" onblur="findHoliday(this,0);" type="text"/>
                                <img id="imgtxtContractSignDate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtContractSignDate','imgtxtContractSignDate');" border="0"/>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Proposed Date of Contract Completion : <span>*</span> </td>
                            <td width="70%">
                                <!--<input class="formTxtBox_1" style="width: 280px;"  value="" name="txtContractCompletionDate" id="txtContractCompletionDate" type="text"/>-->

                                <input class="formTxtBox_1" style="width: 100px;"  name="contractCompletionDate" class="formTxtBox_1" id="txtContractCompletionDate" style="width: 100px;" readonly="true" onfocus="GetCalWithouTime('txtContractCompletionDate','txtContractCompletionDate');" onblur="findHoliday(this,0);" type="text"/>
                                <img id="imgtxtContractCompletionDate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtContractCompletionDate','imgtxtContractCompletionDate');" border="0"/>
                                

                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">No. of Tenders/Proposals Sold : <span>*</span> </td>
                            <td width="70%">
                                <input class="formTxtBox_1" style="width: 280px;"  value="" name="noBidSold" id="txtNoBidSold" type="text"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">No. of Tenders/Proposals Received : <span>*</span> </td>
                            <td width="70%">
                                <input class="formTxtBox_1" style="width: 280px;"  value="" name="noBidReceived" id="txtNoBidSubmit" type="text"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Tenders/Proposals Responsive:<span>*</span> </td>
                            <td width="70%">
                                <input class="formTxtBox_1" style="width: 280px;"  value="" name="noResponsiveTenderer" id="txtNoResponsiveTenderer" type="text"/>
                            </td>
                        </tr>
                    </tbody></table>
                    <div class="tableHead_22 t_space">INFORMATION ON AWARD</div>
                    <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                        <tbody><tr>
                            <td class="ff" width="30%">Brief Description of Contract : <span>*</span> </td>
                            <td width="70%">
                                <textarea class="formTxtBox_1" style="width: 280px;" class="formTxtBox_1"  name="contractDescription" rows="2" cols="49" id="BodyContent_txtContractDescription"></textarea>
                                 
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Contract Value (Million Nu.) : <span>*</span> </td>
                            <td width="70%">
                                <input class="formTxtBox_1" style="width: 280px;"  value="" name="contractValue" id="txtContractValue" type="text"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Name of Supplier/Contractor/Consultant : <span>*</span> </td>
                            <td width="70%">
                                <input class="formTxtBox_1" style="width: 280px;"  value="" name="nameCompany" id="txtNameCompany" type="text"/>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Location of Supplier/Contractor/Consultant : <span>*</span> </td>
                            <td width="70%">
                                <textarea class="formTxtBox_1" style="width: 280px;" class="formTxtBox_1"  name="locationCompany" id="txtLocationCompany" rows="2" cols="49"></textarea>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Location of Delivery/Works/Consultancy : <span>*</span> </td>
                            <td width="70%">
                                <textarea class="formTxtBox_1" style="width: 280px;" class="formTxtBox_1"  name="locationDelWrkSer" id="txtLocationDelWrkSer" rows="2" cols="49"></textarea>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Was the contract signed with the same person stated in the Letter of Acceptance?   <span>*</span> </td>
                            <td width="70%">
                                 <select class="formTxtBox_1" style="width: 100px;"  name="isSamePersonNOA" id="ddlIsSamePersonNOA">
	                                <option selected="selected" value="1">Yes</option>
	                                <option value="2">No</option>
                                </select>
                            
                            </td>
                        </tr>
                        <tr id="trNoaReason">
                            <td class="ff" width="30%">If No give reason why:</td>
                            <td width="70%">
                                <input class="formTxtBox_1" style="width: 280px;"  name="NOAReason" id="txtNOAReason" type="text"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Was the Performance Security provided in due time?  <span>*</span> </td>
                            <td width="70%">
                                 <select class="formTxtBox_1" style="width: 100px;"  name="isPSecurityDueTime" id="ddlIsPSecurityDueTime">
	                                <option selected="selected" value="1">Yes</option>
	                                <option value="2">No</option>
                                </select>
                            
                            </td>
                        </tr>
                        <tr id="trPSecurityReason">
                            <td class="ff" width="30%">If No give reason why:</td>
                            <td width="70%">
                                <input class="formTxtBox_1" style="width: 280px;"  name="perSecurityReason" id="txtPSecurityReason" type="text"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Was the Contract Singed in due time?  <span>*</span> </td>
                            <td width="70%">
                                 <select class="formTxtBox_1" style="width: 100px;"  name="isSignedDueTime" id="ddlIsSignedDueTime">
	                                <option selected="selected" value="1">Yes</option>
	                                <option value="2">No</option>
                                </select>
                            
                            </td>
                        </tr>
                        <tr id="trSignedReason">
                            <td class="ff" width="30%">If No give reason why:</td>
                            <td width="70%">
                                <input class="formTxtBox_1" style="width: 280px;"  name="signedReason" id="txtSignedReason" type="text"/>
                            </td>
                        </tr>
                    </tbody></table>
                    <div class="tableHead_22 t_space">ENTITY DETAILS</div>
                    <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                        <tbody><tr>
                            <td class="ff" width="30%">Name of Authorised Officer : <span>*</span> </td>
                            <td width="70%">
                                <input class="formTxtBox_1" style="width: 280px;"  name="authorisedOfficerName" id="txtAuthorisedOfficerName" type="text"/>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%">Designation of Authorised Officer : <span>*</span> </td>
                            <td width="70%">
                                <input class="formTxtBox_1" style="width: 280px;"  name="authorisedOfficerDesignation" id="txtAuthorisedOfficerDesignation" type="text"/>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%"></td>
                            <td width="70%">
                            </td>
                        </tr>
                        <tr>
                            <td class="ff" width="30%"></td>
                            <td width="70%">
                                <label class="formBtn_1">
                                    <input id="btnSubmit" type="submit" value="Submit" name="submit"  />
                                </label>
                            </td>
                        </tr>
                    </tbody></table>
                    </form>
                               <%  }  %>
                 </div>
            </div>


        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>

     </body>
        </html>