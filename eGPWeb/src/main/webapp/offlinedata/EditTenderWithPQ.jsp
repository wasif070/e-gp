<%-- 
    Document   : EditTenderWithPQ
    Created on : Aug 8, 2012, 2:43:27 PM
    Author     : Istiak
--%>


<%@page import="com.cptu.egp.eps.web.servicebean.TenderDashboardOfflineSrBean"%>
<%@page import="com.cptu.egp.eps.web.servlet.ComboServlet"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderLotPhasingOffline"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderDetailsOffline"%>
<%@page import="com.cptu.egp.eps.web.utility.CommonUtils"%>
<%@page import="com.cptu.egp.eps.web.offlinedata.PrequalificationExcellBean"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.servicebean.EvalSerCertiSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblStateMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page import="com.cptu.egp.eps.model.table.TblDepartmentMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ManageEmployeeGridSrBean"%>
<%@page import="java.util.Calendar"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
 <jsp:useBean id="offlineDataSrBean" class="com.cptu.egp.eps.web.offlinedata.OfflineDataSrBean"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Edit Tender with Pre-qualification (PQ)</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>

        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <link href="../ckeditor/editor.css" type="text/css" rel="stylesheet"/>

        <!--
        <script src="resources/config.js" type="text/javascript"></script>
        <script src="resources/en.js" type="text/javascript"></script>
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
       -->

        <!--jquery validator max length css change -->
       <style type="text/css">
           label.error{color:red}
       </style>
       
        <script type="text/javascript">
            var holiArray = new Array();
        </script>

       <script type="text/javascript">        // Script for Datatable

            function dataTable(selVal){
                    if(selVal == "Single Lot"){
                        document.getElementById("lot_1").style.display = "none";
                        document.getElementById("lot_2").style.display = "none";
                        document.getElementById("lot_3").style.display = "none";
                    }else {
                        document.getElementById("lot_1").style.display = "table-row";
                        document.getElementById("lot_2").style.display = "table-row";
                        document.getElementById("lot_3").style.display = "table-row";

                        document.getElementById("txtLotNo_2").value ='';
                        document.getElementById("txtidentification_2").value = '';
                        document.getElementById("txtLocationlot_2").value = '';
                        document.getElementById("txtcomplTimeLotNo_2").value='';
                        
                        document.getElementById("txtLotNo_3").value ='';
                        document.getElementById("txtidentification_3").value = '';
                        document.getElementById("txtLocationlot_3").value = '';
                        document.getElementById("txtcomplTimeLotNo_3").value='';

                        document.getElementById("txtLotNo_4").value ='';
                        document.getElementById("txtidentification_4").value = '';
                        document.getElementById("txtLocationlot_4").value = '';
                        document.getElementById("txtcomplTimeLotNo_4").value='';
                    }

            }
            function AddRow(lotType) {
                if (lotType == "Multiple Lot") {
                    var ptable = document.getElementById('tblLotDescription');
                    var lastElement = ptable.rows.length;
                    var index = lastElement;
                    var row = ptable.insertRow(lastElement);

                    var newTxt = "";

                    for(var i = 1; i<=4; i++){

                        newTxt += "<tr class='trExtra' id='tr" + (row + i) +"'>" +  
                                "<td class='t-align-center'><input name='LotNo" + (row + i) +"' class='formTxtBox_1' id='txtLotNo" + (row + i) +"' type='text'/></td>" +
                                "<td class='t-align-center'><textarea class='formTxtBox_1' style='width: 280px;' name='DescriptionForPQ" + (row + i) +"' id='txtDescriptionForPQ" + (row + i) +"' rows='2' cols='40'></textarea></td>" +
                                "<td class='t-align-center'><input name='locationlot_" + (row + i) +"' style='width: 180px;' class='formTxtBox_1' id='locationlot_" + (row + i) +"' onblur='chkLocLotBlank(this);' type='text'><span id='locLot_0' style='color: red;'>&nbsp;</span></td>" +
                                "<td class='t-align-center'>" +
                                    "<input name='complTimeLotNo_" + (row + i) +"' readonly='true' class='formTxtBox_1' id='complTimeLotNo_" + (row + i) +"' style='width: 100px;' onfocus='GetCalWithouTime(\"complTimeLotNo_" + (row + i) +"\",\"complTimeLotNo_" + (row + i) +"\");' onblur='chkCompTimeLotBlank(this);findHoliday(this,8);' type='text'><span id='compLot_" + (row + i) +"' style='color: red;'>&nbsp;</span>" +
                                    "</td></tr>";
                    }
                    $("#tblLotDescription").append(newTxt);
                }
                else {
                    $('.trExtra').each(function () {
                        $(this).remove();
                    });
                }
            }

            /*function RemoveRow()
            {
                var checked = $('input[name:chkLot]:checked').length;

                if(checked > 0){
                    $('input[name:chkLot]:checked').each(function(){
                        var curRow = $(this).parents('tr');
                        curRow.remove();
                    });
                }
                else
                {
                    alert("Please select a row");
                }
            }

            function AddRemoveLotButton(lotType)
                {
                    if (lotType == "multiple") {
                       $('#addRemoveLot').show();
                    }
                    else {
                        $('.trExtra').each(function () {
                            $(this).remove();
                        });
                        $('#addRemoveLot').hide();
                    }
                }*/

            function setOrgval(id){

                var orgObj =document.getElementById(id);
                var orgval = orgObj.options[orgObj.selectedIndex].text;
                //alert('orgvalue >> '+orgval);
                document.getElementById("hidorg").value = orgval;
            }

            $(document).ready(function(){

                //$('#addRemoveLot').hide();

                // Sorting Dropdown
                $("select").each(function() {

                    // Keep track of the selected option.
                    var selectedValue = $(this).val();

                    // Sort all the options by text. I could easily sort these by val.
                    $(this).html($("option", $(this)).sort(function(a, b) {
                        return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
                    }));

                    // Select one option.
                    $(this).val(selectedValue);
                });

                //Form Validation
                $("#frmCreateTenderWithPQ").validate({
                    rules: {
                        //textbox
                         peName: {required: true,maxlength:150},
                        invitationRefNo: {required: true,maxlength:150},
                        issuedate: {required: true,date:true},
                        packageNo: {required: true,maxlength:50},
                        packageName: {required: true, maxlength:150},
                        tenderpublicationDate: {required: true,date:true},
                        preQualCloseDate: {required: true,date:true},
                        preQualDocPrice: {required: true, number: true},
                        LotNo: {required: true,maxlength:150},
                        DescriptionForPQ: {required: true},
                        locationlot_0: {required: true},
                        startLotNo_0: {required: true},
                        complTimeLotNo_0: {required: true},
                        nameOfficial: {required: true,maxlength:200},
                        designationOfficial: {required: true,maxlength:200},
                        eligibilityofTenderer:{required: true},
                        briefDescGoods:{required: true},
                        briefDescRelServices:{required: true},
                        receivingPQ :{required: true,maxlength:2000},
                        sellingDocPrinciple:{required: true,maxlength:2000},
                        identification_1:{required: true,maxlength:2000},
                        addressOfOfficial:{maxlength:5000},
                        contactDetail:{maxlength:1000},
                        sellingDocOthers:{maxlength:1000},
                        placeofPQMeeting:{maxlength:2000},
                        devPartner:{maxlength:300},
                        projectCode:{maxlength:150},
                        projectName:{maxlength:150},
                        peCode:{maxlength:15},
                        lotNo_1:{maxlength:10},
                        locationlot_1:{maxlength:100},
                        complTimeLotNo_1:{maxlength:100},

                        //Dropdown
                        ministry:{selectNone: true},
                        organization: {selectNone: true},
                        district:{selectNone: true},
                        nature:{selectNone: true},
                        procType:{selectNone: true},
                        invitationFor:{selectNone: true},
                        procureMethod:{selectNone: true},
                        budget:{selectNone: true},
                        funds:{selectNone: true}

                    },
                    messages: {
                        //Textbox
                        peName: { required: "<div class='reqF_1'>Please enter PE Name.</div>"},
                        invitationRefNo: { required: "<div class='reqF_1'>Please enter Reference No.</div>"},
                        issuedate: { required: "<div class='reqF_1'>Please select Issue Date.</div>", date: "<div class='reqF_1'>Invalid Date Format</div>"},
                        packageNo: { required: "<div class='reqF_1'>Please enter Package No.</div>"},
                        packageName: { required: "<div class='reqF_1'>Please enter Package Name.</div>"},
                        tenderpublicationDate: { required: "<div class='reqF_1'>Please select Publication Date.</div>", date: "<div class='reqF_1'>Invalid Date Format</div>"},
                        preQualCloseDate: { required: "<div class='reqF_1'>Please select PQ Closing Date.</div>", date: "<div class='reqF_1'>Invalid Date Format</div>"},
                        preQualDocPrice: { required: "<div class='reqF_1'>Please enter PQ Document Price.</div>",number: "<div class='reqF_1'>Please enter numeric only.</div>"},
                        lotNo: { required: "<div class='reqF_1'>Please enter Lot Number.</div>"},
                        descriptionForPQ: { required: "<div class='reqF_1'>Please enter Lot Description.</div>"},
                        locationlot_0: { required: "<div class='reqF_1'>Please enter Lot Location.</div>"},
                        startLotNo_0: { required: "<div class='reqF_1'>Please select Staring Date.</div>"},
                        complTimeLotNo_0: { required: "<div class='reqF_1'>Please select Completion Date.</div>"},
                        nameOfficial: { required: "<div class='reqF_1'>Please enter the Name.</div>"},
                        designationOfficial: { required: "<div class='reqF_1'>Please enter Designation.</div>"},
                        eligibilityofTenderer:{ required: "<div class='reqF_1'>Please enter Eligibility of Tenderer.</div>"},
                        briefDescGoods:{ required: "<div class='reqF_1'>Please enter Description of Goods or Works.</div>"},
                        briefDescRelServices:{ required: "<div class='reqF_1'>Please enter Description of Related Services.</div>"},
                        receivingPQ :{ required: "<div class='reqF_1'>Please enter Receiving Pre-qualification.</div>"},
                        sellingDocPrinciple:{ required: "<div class='reqF_1'>Please enter Selling Pre-qualification Document.</div>"},
                        identification_1:{required: "<div class='reqF_1'>Please enter Identification of Lot.</div>"},

                        //Dropdown
                        ministry:{ selectNone: "<div class='reqF_1'>Please select Ministry.</div>"},
                        organization:{ selectNone: "<div class='reqF_1'>Please select Organization.</div>"},
                        district:{ selectNone: "<div class='reqF_1'>Please select Dzongkhag / District.</div>"},
                        nature:{ selectNone: "<div class='reqF_1'>Please select Procurement Category.</div>"},
                        procType:{ selectNone: "<div class='reqF_1'>Please select Procurement Type.</div>"},
                        invitationFor:{ selectNone: "<div class='reqF_1'>Please select Lot Type.</div>"},
                        procureMethod:{ selectNone: "<div class='reqF_1'>Please select Procurement Method.</div>"},
                        budget:{ selectNone: "<div class='reqF_1'>Please select Budget Type.</div>"},
                        funds:{ selectNone: "<div class='reqF_1'>Please select Source of Fund.</div>"}

                    }
                });

                ////The following code has been used to adding validation of dropdown list in basic validation plugin
                $.validator.addMethod('selectNone',
                    function (value, element)
                    {
                        if (value == 0 || value =='' || value =='select' || value =='<Select>' || value =='<select>' || value.indexOf('--') != -1)
                        {
                            //alert("not ok");
                            return false;
                        }
                        else
                        {
                            //alert("ok")
                            return true;
                        }
                    }
                );
                //End Form Validation

            }); // End Document.Ready

    </script>

    </head>

    <%

    PrequalificationExcellBean preqExcellBean = null;
    int tenolId = 0;
    String action = "create";
    String agencyCombo = "";
    String type = "";
    if(!request.getParameterMap().containsKey("action"))
            type="/offlinedata/CreateTenderWithPQ.jsp";
         if( request.getParameter("submit") != null && "Submit".equals(request.getParameter("submit"))){
         try{
             System.out.println("condition ok");
            String ministry = CommonUtils.checkNull(request.getParameter("hidministry"));
            String agency = CommonUtils.checkNull(request.getParameter("hidorg"));
            String peName = CommonUtils.checkNull(request.getParameter("peName"));
            String peCode = CommonUtils.checkNull(request.getParameter("peCode"));
            String district = CommonUtils.checkNull(request.getParameter("district"));
            String nature = CommonUtils.checkNull(request.getParameter("nature"));
            String procType = CommonUtils.checkNull(request.getParameter("procType"));
            String invitationFor = CommonUtils.checkNull(request.getParameter("invitationFor"));
            String invitationRefNo = CommonUtils.checkNull(request.getParameter("invitationRefNo"));
            String issuedate = CommonUtils.checkNull(request.getParameter("issuedate"));
            String procureMethod = CommonUtils.checkNull(request.getParameter("procureMethod"));
            String budget = CommonUtils.checkNull(request.getParameter("budget"));
            String funds = CommonUtils.checkNull(request.getParameter("funds"));
            String devPartner = CommonUtils.checkNull(request.getParameter("devPartner"));
            String projectCode = CommonUtils.checkNull(request.getParameter("projectCode"));
            String ProjectName = CommonUtils.checkNull(request.getParameter("projectName"));
            String packageNo = CommonUtils.checkNull(request.getParameter("packageNo"));
            String packageName = CommonUtils.checkNull(request.getParameter("packageName"));
            String tenderpublicationDate = CommonUtils.checkNull(request.getParameter("tenderpublicationDate"));
            String preQualCloseDate = CommonUtils.checkNull(request.getParameter("preQualCloseDate"));
            String placeofPQMeeting = CommonUtils.checkNull(request.getParameter("placeofPQMeeting"));
            String preTenderMeetStartDate = CommonUtils.checkNull(request.getParameter("preTenderMeetStartDate"));
            String sellingDocPrinciple = CommonUtils.checkNull(request.getParameter("sellingDocPrinciple"));
            String sellingDocOthers = CommonUtils.checkNull(request.getParameter("sellingDocOthers"));
            String receivingPQ = CommonUtils.checkNull(request.getParameter("receivingPQ"));
            String eligibilityofTenderer = CommonUtils.checkNull(request.getParameter("eligibilityofTenderer"));
            String briefDescGoods = CommonUtils.checkNull(request.getParameter("briefDescGoods"));
            String briefDescRelServices = CommonUtils.checkNull(request.getParameter("briefDescRelServices"));
            String preQualDocPrice = CommonUtils.checkNull(request.getParameter("preQualDocPrice"));
            String lotNo_1 = CommonUtils.checkNull(request.getParameter("lotNo_1"));
            String identification_1 = CommonUtils.checkNull(request.getParameter("identification_1"));
            String locationlot_1 = CommonUtils.checkNull(request.getParameter("locationlot_1"));
            String complTimeLotNo_1 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_1"));
            String lotNo_2 = CommonUtils.checkNull(request.getParameter("lotNo_2"));
            String identification_2 = CommonUtils.checkNull(request.getParameter("identification_2"));
            String locationlot_2 = CommonUtils.checkNull(request.getParameter("locationlot_2"));
            String complTimeLotNo_2 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_2"));
            String lotNo_3 = CommonUtils.checkNull(request.getParameter("lotNo_3"));
            String identification_3 = CommonUtils.checkNull(request.getParameter("identification_3"));
            String locationlot_3 = CommonUtils.checkNull(request.getParameter("locationlot_3"));
            String complTimeLotNo_3 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_3"));
            String lotNo_4 = CommonUtils.checkNull(request.getParameter("lotNo_4"));
            String identification_4 = CommonUtils.checkNull(request.getParameter("identification_4"));
            String locationlot_4 = CommonUtils.checkNull(request.getParameter("locationlot_4"));
            String complTimeLotNo_4 = CommonUtils.checkNull(request.getParameter("complTimeLotNo_4"));
            String nameOfficial = CommonUtils.checkNull(request.getParameter("nameOfficial"));
            String designationOfficial = CommonUtils.checkNull(request.getParameter("designationOfficial"));
            String addressOfOfficial = CommonUtils.checkNull(request.getParameter("addressOfOfficial"));
            String contactDetail = CommonUtils.checkNull(request.getParameter("contactDetail"));
            TblTenderDetailsOffline tenderDetailsOffline = null;
            List<TblTenderLotPhasingOffline> lots = null ;
            if( request.getParameter("action") != null && request.getParameter("tenid") != null && "Edit".equals(request.getParameter("action"))
                    &&  Integer.parseInt(request.getParameter("tenid")) != 0){
                    tenolId = Integer.parseInt(request.getParameter("tenid"));
                   
                    tenderDetailsOffline = offlineDataSrBean.getTblTenderDetailOfflineDataById(tenolId);
                     
                    int i = 0;
                    while(i < tenderDetailsOffline.getTenderLotsAndPhases().size()){
                        tenderDetailsOffline.getTenderLotsAndPhases().remove(i);
                    }
                    lots = tenderDetailsOffline.getTenderLotsAndPhases();
                    
            }else{
                    tenderDetailsOffline = new TblTenderDetailsOffline();
                    lots  = new ArrayList<TblTenderLotPhasingOffline>();
                }

            String userid = "";
            HttpSession hs = request.getSession();
            if (hs.getAttribute("userId") != null) {
                userid = hs.getAttribute("userId").toString();
                offlineDataSrBean.setLogUserId(userid);
            }
            
            tenderDetailsOffline.setMinistryOrDivision(ministry);
            tenderDetailsOffline.setAgency(agency);
            tenderDetailsOffline.setPeName(peName);
            tenderDetailsOffline.setPeCode(peCode);
            tenderDetailsOffline.setPeDistrict(district);
            tenderDetailsOffline.setProcurementNature(nature);
            tenderDetailsOffline.setProcurementType(procType);
            tenderDetailsOffline.setInvitationFor(invitationFor);
            tenderDetailsOffline.setReoiRfpRefNo(invitationRefNo);
           // System.out.println("issuedate >> "+issuedate);
            Date issDate = DateUtils.formatStdString(issuedate);
            tenderDetailsOffline.setIssueDate(issDate);
            tenderDetailsOffline.setProcurementMethod(procureMethod);
            tenderDetailsOffline.setBudgetType(budget);
            tenderDetailsOffline.setSourceOfFund(funds);
            tenderDetailsOffline.setDevPartners(devPartner);
            tenderDetailsOffline.setProjectCode(projectCode);
            tenderDetailsOffline.setProjectName(ProjectName);
            tenderDetailsOffline.setPackageNo(packageNo);
            tenderDetailsOffline.setPackageName(packageName);
           // System.out.println("tenderpublicationDate >> "+tenderpublicationDate);
            Date pubDate = DateUtils.formatStdString(tenderpublicationDate);
           // System.out.println("after convert date >> "+pubDate);
            tenderDetailsOffline.setTenderPubDate(pubDate);
            Date closeDate = DateUtils.convertDateToStr(preQualCloseDate);
            tenderDetailsOffline.setClosingDate(closeDate);
            tenderDetailsOffline.setPreTenderReoiplace(placeofPQMeeting);

            Date meetingDate = null;
            if(!preTenderMeetStartDate.equalsIgnoreCase("") && preTenderMeetStartDate != null && !preTenderMeetStartDate.isEmpty()){
                meetingDate = DateUtils.convertDateToStr(preTenderMeetStartDate);
            } 

            tenderDetailsOffline.setPreTenderReoidate(meetingDate);
            tenderDetailsOffline.setSellingAddPrinciple(sellingDocPrinciple);
            tenderDetailsOffline.setSellingAddOthers(sellingDocOthers);
            tenderDetailsOffline.setReceivingAdd(receivingPQ);
            tenderDetailsOffline.setEligibilityCriteria(eligibilityofTenderer);
            tenderDetailsOffline.setBriefDescription(briefDescGoods);
            tenderDetailsOffline.setRelServicesOrDeliverables(briefDescRelServices);
            tenderDetailsOffline.setEventType("Tender With PQ");
            tenderDetailsOffline.setUserId(Integer.parseInt(userid));
            float docPrice = 0;
            if (preQualDocPrice != null && !"".equals(preQualDocPrice)) {
                docPrice = Float.parseFloat(preQualDocPrice);
            }
           // tenderDetailsOffline.setDocumentPrice(new BigDecimal(docPrice).setScale(2, 0));
            tenderDetailsOffline.setDocumentPrice(new BigDecimal(preQualDocPrice));
            tenderDetailsOffline.setPeOfficeName(nameOfficial);
            tenderDetailsOffline.setPeDesignation(designationOfficial);
            tenderDetailsOffline.setPeAddress(addressOfOfficial);
            tenderDetailsOffline.setPeContactDetails(contactDetail);
            // tenderDetailsOffline.setTenderStatus("Pending");

            
           // System.out.println("invitationFor >> "+invitationFor);
            if(invitationFor.equals("Single Lot")){
                System.out.println("single >> ");
                TblTenderLotPhasingOffline lotPhasingOffline = new TblTenderLotPhasingOffline();
                lotPhasingOffline.setLotOrRefNo(lotNo_1);
                lotPhasingOffline.setLocation(locationlot_1);
                lotPhasingOffline.setLotIdentOrPhasingServ(identification_1);
                lotPhasingOffline.setCompletionDateTime(complTimeLotNo_1);
                lotPhasingOffline.setTenderSecurityAmt(new BigDecimal(0).setScale(0, 0));
                lotPhasingOffline.setTenderDetailsOffline(tenderDetailsOffline);
               // lotPhasingOffline.setTenderLotPhaId(22);
                lots.add(lotPhasingOffline);
            }else if(invitationFor.equals("Multiple Lot")){
                System.out.println("multi lot ");
                 for(int i=1;i<=4;i++){

                if((request.getParameter("lotNo_"+i) != null || request.getParameter("identification_"+i) != null
                        || request.getParameter("locationlot_"+i) != null || request.getParameter("complTimeLotNo_"+i) != null)
                        && (!"".equals(request.getParameter("lotNo_"+i)) || !"".equals(request.getParameter("identification_"+i)) || !"".equals(request.getParameter("locationlot_"+i))
                        || !"".equals(request.getParameter("complTimeLotNo_"+i))) ){

                        TblTenderLotPhasingOffline lotPhasingOffline = new TblTenderLotPhasingOffline();
                        lotPhasingOffline.setLotOrRefNo(request.getParameter("lotNo_"+i));
                        lotPhasingOffline.setLocation(request.getParameter("locationlot_"+i));
                        lotPhasingOffline.setLotIdentOrPhasingServ(request.getParameter("identification_"+i));
                        lotPhasingOffline.setCompletionDateTime(request.getParameter("complTimeLotNo_"+i));
                        lotPhasingOffline.setTenderSecurityAmt(new BigDecimal(0).setScale(2, 0));
                        lotPhasingOffline.setTenderDetailsOffline(tenderDetailsOffline);
                        lots.add(lotPhasingOffline);
                }

               }
            }
            tenderDetailsOffline.setTenderLotsAndPhases(lots);
            if( request.getParameter("action") != null && request.getParameter("tenid") != null && "Edit".equals(request.getParameter("action"))
                    &&  Integer.parseInt(request.getParameter("tenid")) != 0){
                System.out.println("tenolId >>>>>>>>>>> "+tenolId);
                
                tenderDetailsOffline.setTenderOfflineId(tenolId);
                if(offlineDataSrBean.updateTenderDetailsOfflineData(tenderDetailsOffline,tenolId))
                    %>

             
                   <script type="text/javascript">
                      alert("Information Updated Successfully");
                    </script>
                <%
                response.sendRedirect(request.getContextPath() + "/offlinedata/TenderDashboardOfflineApproval.jsp");
              }else{
                offlineDataSrBean.createOfflineData(tenderDetailsOffline);
                %>
              <script type="text/javascript">
                  alert("Information Saved Successfully");
                 // window.location("/offlinedata/CreateContractAward.jsp");
                //   window.location.replace(window.location.host+'/offlinedata/CreateContractAward.jsp');
              </script>
              <%}
            //response.sendRedirect("/offlinedata/EditTenderWithPQ.jsp?action=Edit&tenderOLId=3");
           }catch(Exception ex){
           ex.printStackTrace();
           }
         }
    //else
    {

        if( request.getParameter("action") != null && "Edit".equals(request.getParameter("action"))){
             if(request.getParameter("tenderOLId") != null ){
                 String tenderOLId = request.getParameter("tenderOLId");
                 tenolId = Integer.parseInt(tenderOLId);
                  type="/offlinedata/EditTenderWithPQ.jsp?action=Edit&tenderOLId="+tenderOLId;
             }

            String userid = "";
            HttpSession hs = request.getSession();
            if (hs.getAttribute("userId") != null) {
                userid = hs.getAttribute("userId").toString();
                offlineDataSrBean.setLogUserId(userid);
            }
             action = "Edit";
             preqExcellBean = (PrequalificationExcellBean) offlineDataSrBean.editTenderWithPQForm(tenolId);
            System.out.println("testing : "+preqExcellBean);
             agencyCombo = offlineDataSrBean.getOrganizationByMinistry(preqExcellBean.getMinistryName(), preqExcellBean.getAgency());
         }else{
             preqExcellBean = (PrequalificationExcellBean)request.getAttribute("prequalificationDataBean");
             agencyCombo = offlineDataSrBean.getOrganizationByMinistry(preqExcellBean.getMinistryName(), preqExcellBean.getAgency());
            
             }
    %>
    
   
    <% TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
    List<SPTenderCommonData> holidayList = tenderCommonService.returndata("getHolidayDatesBD", null, null);
    out.print("<script type='text/javascript'>");
    for(SPTenderCommonData holidays : holidayList){
        out.print("holiArray.push('"+holidays.getFieldName1()+"');");
    }
    out.print("</script>");
    %>

    <body onload="documentAvailable();" >

       

        <input id="boolcheck" value="true" type="hidden"/>
        <div class="mainDiv">
            <div class="fixDiv">

        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <!--Dashboard Header End-->

        <div class="contentArea_1">
                 
                      
                <div class="t-align-right" style="vertical-align: middle;"><a class="action-button-goback" href="<%=request.getContextPath()%>/offlinedata/TenderDashboardOfflineApproval.jsp">Go back</a></div>
                    <div class="pageHead_1">Edit Tender with Pre-qualification (PQ) </div>
                    
                    <form id="frmCreateTenderWithPQ" name="frmCreateTenderWithPQ" method="POST" action="<%=type%>">
                    
                        <div class="tableHead_22 t_space">PROCURING ENTITY (PE) INFORMATION</div>
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">

                            <tbody><tr>
                                <td style="font-style: italic" colspan="4" class="ff t-align-left" align="left">

                                    Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Ministry/Division : <span>*</span></td>
                                <%
                                //    ManageEmployeeGridSrBean manageEmployeeGridSrBean = new ManageEmployeeGridSrBean();
                                //    List<TblDepartmentMaster> departmentMasterList = null;
                               //     departmentMasterList = manageEmployeeGridSrBean.getDepartmentMasterList("Ministry");


                               TenderDashboardOfflineSrBean awardedContractOffline = new TenderDashboardOfflineSrBean();
                                List<Object[]> ministryListOffline = new ArrayList<Object[]>();
                                List<Object[]> orgListOffline = new ArrayList<Object[]>();
                                ministryListOffline = awardedContractOffline.getMinistryForTenderOffline();
                                orgListOffline = awardedContractOffline.getOfflineTenderOrganizationByMinistry(preqExcellBean.getMinistryName());


                                %>
                                <td width="25%">
                                    <select name="ministry" class="formTxtBox_1" id="cmbMinistry" style="width: 200px;" onchange="loadOrganization();">
                                               <!-- <option value="0" selected="selected">--- Please Select ---</option> -->
                                                <%
                                                /* boolean flag = false;
                                                        for (int i = 0; i < departmentMasterList.size(); i++)  {
                                                              if(CommonUtils.checkNull(preqExcellBean.getMinistryName()).equals(departmentMasterList.get(i).getDepartmentName())){
                                                                  out.println("<option selected=\"selected\" value='" + departmentMasterList.get(i).getDepartmentId() + "'>" + departmentMasterList.get(i).getDepartmentName() + "</option>");
                                                                    flag = true;
                                                                  }else
                                                                 out.println("<option value='" + departmentMasterList.get(i).getDepartmentId() + "'>" + departmentMasterList.get(i).getDepartmentName() + "</option>");
                                                        }
                                                 if(flag == false)
                                                 out.println("<option selected=\"selected\" value='" + preqExcellBean.getMinistryName() + "'>" + preqExcellBean.getMinistryName() + "</option>");
                                                */
                                                 boolean flag = false;
                                                    for (int i = 0; i < ministryListOffline.size(); i++)  {
                                                           if(CommonUtils.checkNull(preqExcellBean.getMinistryName()).equals(String.valueOf(ministryListOffline.get(i)))){
                                                                  out.println("<option selected=\"selected\" value='" + String.valueOf(ministryListOffline.get(i)) + "'>" + String.valueOf(ministryListOffline.get(i)) + "</option>");
                                                                  flag = true;
                                                              }else
                                                                 out.println("<option value='" +String.valueOf(ministryListOffline.get(i)) + "'>" + String.valueOf(ministryListOffline.get(i)) + "</option>");
                                                    }
                                                        if(flag == false && preqExcellBean.getMinistryName() != null)
                                                                  out.println("<option selected=\"selected\" value='" + preqExcellBean.getMinistryName() + "'>" + preqExcellBean.getMinistryName() + "</option>");

                                                
                                               %>
                                     </select>
                                    <input id="hidministry"  name="hidministry" type="hidden" value="<%=CommonUtils.checkNull(preqExcellBean.getMinistryName())%>" />
                                    <br />
                                    <span style="color: red;" id="msgMinistry" class="cmbMsg"></span>
                                </td>
                                <td class="ff" width="25%">Organization : <span>*</span></td>
                                <td width="25%">
                                    <select name="organization" class="formTxtBox_1" id="cmbOrganization" style="width: 200px;" onchange="setOrgval(this.id);">
                                                
                                                <%
                                               /* if(preqExcellBean.getAgency() != null && !"".equals(preqExcellBean.getAgency())  && agencyCombo.equals(preqExcellBean.getAgency())){
                                                    out.println(agencyCombo);
                                                }else{
                                                    out.println("<option selected=\"selected\"  value='" + preqExcellBean.getAgency() + "'>"+ preqExcellBean.getAgency() +"</option>");

                                                    }*/
                                                 boolean flagOrg = false;
                                                    for (int i = 0; i < orgListOffline.size(); i++)  {
                                                           if(CommonUtils.checkNull(preqExcellBean.getAgency()).equals(String.valueOf(orgListOffline.get(i)))){
                                                                  out.println("<option selected=\"selected\" value='" + String.valueOf(orgListOffline.get(i)) + "'>" + String.valueOf(orgListOffline.get(i)) + "</option>");
                                                                  flagOrg = true;
                                                              }else
                                                                 out.println("<option value='" +String.valueOf(orgListOffline.get(i)) + "'>" + String.valueOf(orgListOffline.get(i)) + "</option>");
                                                    }
                                                        if(flagOrg == false && preqExcellBean.getAgency() != null)
                                                                  out.println("<option selected=\"selected\" value='" + preqExcellBean.getAgency() + "'>" + preqExcellBean.getAgency() + "</option>");

                                                %>

                                     </select>
                                                <input type="hidden" name="hidorg"  id="hidorg" value="<%= CommonUtils.checkNull(preqExcellBean.getAgency()) %>" />
                                    <br />
                                    <span style="color: red;" id="msgOgrganization" class="cmbMsg" ></span>
                                </td>
                            </tr>
                             <tr>
                                <td class="ff">Procuring Entity Name : <span>*</span></td>
                                <td>
                                    <input name="peName" class="formTxtBox_1" id="txtPEName" style="width: 280px;" type="text" value="<%= CommonUtils.checkNull(preqExcellBean.getProcuringEntityName()) %>"  />
                                </td>
                                <td class="ff">Procuring Entity Code :</td>
                                <td>
                                     <%
                                    if(preqExcellBean.getProcuringEntityCode() != null && !preqExcellBean.getProcuringEntityCode().equals("Not used at present")){
                                    %>
                                    <input name="peCode" class="formTxtBox_1" id="txtPECode" style="width: 280px;" type="text" value="<%=CommonUtils.checkNull(preqExcellBean.getProcuringEntityCode()) %>" />
                                    <% }else{ %>
                                    <input name="peCode" class="formTxtBox_1" id="txtPECode" style="width: 280px;" type="text" value="" />
                                    <% } %>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Procuring Entity Dzongkhag / District : <span>*</span></td>
                                <%
                                        CommonService cservice = (CommonService) AppContext.getSpringBean("CommonService");
                                        //Code by Proshanto
                                        short countryId = 150;//136
                                        List<TblStateMaster> liststate = cservice.getState(countryId);
                                %>
                               <td>
                                     <select name="district" class="formTxtBox_1" id="cmbdistrict" style="width: 200px;">
                                       <!-- <option value="0" selected="selected">--- Please Select ---</option> -->
                                       <%
                                            for (TblStateMaster state : liststate) {
                                                 if(CommonUtils.checkNull(preqExcellBean.getProcuringEntityDistrict()).equals(state.getStateName())){
                                                    out.println("<option selected=\"selected\" value='" + state.getStateName() + "'>" + state.getStateName() + "</option>");
                                                 }else
                                                  out.println("<option value='" + state.getStateName() + "'>" + state.getStateName() + "</option>");
                                            }
                                       %>
                                    </select>
                                    <br />
                                    <span style="color: red;" id="msgDistrict" class="cmbMsg"></span>
                                </td>

                                <td class="ff">Procurement Category : <span>*</span></td>
                                <td>
                                     <select name="nature" class="formTxtBox_1" id="Nature" style="width: 200px;" onChange="setSerType(this,true);">
                                               <!-- <option value="select" selected="selected">--- Please Select ---</option>-->
                                               <%
                                               if(CommonUtils.checkNull(preqExcellBean.getProcurementNature()).equals("Goods")){
                                                   out.println("<option value=\"0\">--- Please Select ---</option>");
                                                    out.println("<option selected=\"selected\" value=\"Goods\">Goods</option>");
                                                    out.println("<option value=\"Works\">Works</option>");
                                               }else if(CommonUtils.checkNull(preqExcellBean.getProcurementNature()).equals("Works")){
                                                   out.println("<option value=\"0\">--- Please Select ---</option>");
                                                    out.println("<option value=\"Goods\">Goods</option>");
                                                    out.println("<option selected=\"selected\" value=\"Works\">Works</option>");
                                               }
                                                else
                                                {
                                                out.println("<option selected=\"selected\" value=\"0\">--- Please Select ---</option>");
                                                out.println("<option  value=\"Goods\">Goods</option>");
                                                out.println("<option  value=\"Works\">Works</option>");
                        
                                                }


                                                %>


                                     </select>
                                    <br />
                                    <span style="color: red;" id="msgProcNature" class="cmbMsg"></span>
                                </td>
                            </tr>
                            <tr>

                                <td class="ff">Procurement Type :<span>*</span></td>
                                <td>
                                    <select name="procType" class="formTxtBox_1" id="Type" style="width: 200px;">
                                                 <%
                                               if(CommonUtils.checkNull(preqExcellBean.getProcurementType()).equals("NCT")){
                                                    out.println("<option value=\"0\">--- Please Select ---</option>");
                                                    out.println("<option selected=\"selected\" value=\"NCT\">NCB</option>");
                                                    out.println("<option value=\"ICT\">ICB</option>");
                                               }else if(CommonUtils.checkNull(preqExcellBean.getProcurementType()).equals("ICT")){
                                                    out.println("<option value=\"0\">--- Please Select ---</option>");
                                                    out.println("<option  value=\"NCT\">NCB</option>");
                                                    out.println("<option selected=\"selected\"value=\"ICT\">ICB</option>");

                                               }
                                             else
                                                {
                                                out.println("<option selected=\"selected\" value=\"0\">--- Please Select ---</option>");
                                                out.println("<option value=\"NCT\">NCB</option>");
                                                out.println("<option value=\"ICT\">ICB</option>");

                                                }

                                                %>
                                                <!--<option value="select" selected="selected">--- Please Select ---</option> -->
                                     </select>
                                    <br />
                                    <span style="color: red;" id="msgProcType" class="cmbMsg"></span>
                                </td>


                                 <td class="ff">Invitation for : <span>*</span></td>
                                <td>
                                    <select name="invitationFor" class="formTxtBox_1" id="InvitationFor" style="width: 200px;" onchange="dataTable(this.value)">
                                       <!-- <option value="select" selected="selected">--- Please Select ---</option> -->
                                        <%
                                        if(CommonUtils.checkNull(preqExcellBean.getInvitationFor()).contains("Single Lot")){
                                            out.println("<option value=\"0\">--- Please Select ---</option>");
                                            out.println("<option selected=\"selected\" value=\"Single Lot\">Single Lot</option>");
                                            out.println("<option value=\"Multiple Lot\">Multiple Lot</option>");
                                        }else if(CommonUtils.checkNull(preqExcellBean.getInvitationFor()).contains("Multiple Lot")) {
                                             out.println("<option value=\"0\">--- Please Select ---</option>");
                                            out.println("<option  value=\"Single Lot\">Single Lot</option>");
                                            out.println("<option selected=\"selected\" value=\"Multiple Lot\">Multiple Lot</option>");
                                        }
                                        else {
                                            out.println("<option selected=\"selected\" value=\"0\">--- Please Select ---</option>");
                                            out.println("<option  value=\"Single Lot\">Single Lot</option>");
                                            out.println("<option  value=\"Multiple Lot\">Multiple Lot</option>");
                                        }
                                        %>
                                    </select>
                                    <br />
                                    <span style="color: red;" id="msgInvitationFor" class="cmbMsg"></span>
                                </td>
                            </tr>
                            <tr>

                                <td class="ff">Event Type :</td>
                                <td><label id="lblEventType">PQ</label></td>

                                <td class="ff">Invitation Reference No. : <span>*</span></td>
                                <td><input name="invitationRefNo" class="formTxtBox_1" id="txtinvitationRefNo" style="width: 280px;" value="<%= CommonUtils.checkNull(preqExcellBean.getInvitationRefNo()) %>" type="text" />
                                    <span class="reqF_1" id="spantxtinvitationRefNo"></span>
                                    <input id="hdnmsgTender" name="hdnmsgTenderName" value="PQ" type="hidden">
                                </td>

                            </tr>
                            <tr>
                                <td class="ff">Date : <span>*</span></td>
                               <td class="formStyle_1"><input name="issuedate" value="<%=CommonUtils.checkNull(preqExcellBean.getTenderissuingdate()) %>" class="formTxtBox_1" id="txtissuedate" style="width: 100px;" onfocus="GetCalWithouTime('txtissuedate','txtissuedate');" onblur="findHoliday(this,0);" type="text">
                                    <img id="imgtxtissuedate" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txtissuedate','imgtxtissuedate');" border="0">
                                    <span id="span1"></span>
                                </td>

                                <td class="ff"></td>
                                <td>


                                </td>
                            </tr>
                        </tbody></table>

                                <div class="tableHead_22 ">Key Information and Funding Information :</div>
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td class="ff" width="25%">Procurement Method : <span>*</span></td>
                                <td width="25%"><input value="OTM" id="hdnProcurementMethod" name="hdnProcurementMethod" type="hidden">
                                     <select name="procureMethod" class="formTxtBox_1" style="width: 200px;" id="cmbProcureMethod">
                                                <!-- <option value="select" selected="selected">--- Please Select ---</option> -->
                                                <%
                                                if(CommonUtils.checkNull(preqExcellBean.getProcurementMethod()).contains("Open")){
                                                out.println("<option value=\"0\">--- Please Select ---</option>");
                                                out.println("<option selected=\"selected\" value=\"Open Tendering Method\">Open Tendering Method</option>");
                                                out.println("<option value=\"Two Stage Tendering Method\">Two Stage Tendering Method</option>");
                                                }else if(CommonUtils.checkNull(preqExcellBean.getProcurementMethod()).contains("Two")){
                                                out.println("<option value=\"0\">--- Please Select ---</option>");
                                                out.println("<option value=\"Open Tendering Method\">Open Tendering Method</option>");
                                                out.println("<option selected=\"selected\" value=\"Two Stage Tendering Method\">Two Stage Tendering Method</option>");
                                                }
                                            else{
                                                out.println("<option  selected=\"selected\" value=\"0\">--- Please Select ---</option>");
                                                out.println("<option value=\"Open Tendering Method\">Open Tendering Method</option>");
                                                out.println("<option value=\"Two Stage Tendering Method\">Two Stage Tendering Method</option>");
                                                }
                                                %>
                                     </select>
                                    <input type="hidden" name="hdnPM" id="hdnPM" value=""/>
                                    <br />
                                    <span style="color: red;" id="msgProcMethod" class="cmbMsg"></span>
                                </td>
                                <td class="ff" width="25%">Budget Type : <span>*</span></td>
                                <td width="25%">
                                    <select name="budget" class="formTxtBox_1" id="Budget" style="width: 200px;">
                                               <!--  <option value="select" selected="selected">--- Please Select ---</option> -->
                                               <%
                                               if(CommonUtils.checkNull(preqExcellBean.getCbobudget()).equals("Revenue Budget")){
                                                   out.println("<option value=\"0\">--- Please Select ---</option>");
                                                   out.println("<option selected=\"selected\" value=\"Revenue Budget\">Revenue Budget</option>");
                                                   out.println("<option value=\"Development Budget\">Development</option>");
                                                   out.println("<option value=\"Own Funds\">Own Funds</option>");
                                               }else if(CommonUtils.checkNull(preqExcellBean.getCbobudget()).equals("Development Budget")){
                                                   out.println("<option value=\"0\">--- Please Select ---</option>");
                                                   out.println("<option  value=\"Revenue Budget\">Revenue Budget</option>");
                                                   out.println("<option selected=\"selected\" value=\"Development Budget\">Development Budget</option>");
                                                   out.println("<option value=\"Own Funds\">Own Funds</option>");
                                               }else if(CommonUtils.checkNull(preqExcellBean.getCbobudget()).equals("Own Funds")){
                                                   out.println("<option value=\"0\">--- Please Select ---</option>");
                                                   out.println("<option  value=\"Revenue Budget\">Revenue Budget</option>");
                                                   out.println("<option  value=\"Development Budget\">Development Budget</option>");
                                                   out.println("<option selected=\"selected\" value=\"Own Funds\">Own Funds</option>");
                                               }
                                            else{
                                               out.println("<option selected=\"selected\" value=\"0\">--- Please Select ---</option>");
                                               out.println("<option  value=\"Revenue Budget\">Revenue Budget</option>");
                                               out.println("<option  value=\"Development Budget\">Development Budget</option>");
                                               out.println("<option  value=\"Own Funds\">Own Funds</option>");
                                           }
                                                %>
                                     </select>
                                    <br />
                                    <span style="color: red;" id="msgBudgetType" class="cmbMsg"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Source of Funds : <span>*</span></td>
                                <td width="25%">
                                    <select name="funds" class="formTxtBox_1" id="Funds" style="width: 200px;">
                                             <%
                                               if(CommonUtils.checkNull(preqExcellBean.getCbosource()).equals("Government")){
                                                    out.println("<option value=\"0\">--- Please Select ---</option>");
                                                   out.println("<option selected=\"selected\" value=\"Government\">Government</option>");
                                                   out.println("<option value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                   out.println("<option value=\"Own Funds\">Own Funds</option>");
                                               }else if(CommonUtils.checkNull(preqExcellBean.getCbosource()).equals("Aid Grant / Credit")){
                                                    out.println("<option value=\"0\">--- Please Select ---</option>");
                                                   out.println("<option  value=\"Government\">Government</option>");
                                                   out.println("<option selected=\"selected\" value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                   out.println("<option value=\"Own Funds\">Own Funds</option>");
                                               }else if(CommonUtils.checkNull(preqExcellBean.getCbosource()).equals("Own Funds")){
                                                    out.println("<option value=\"0\">--- Please Select ---</option>");
                                                   out.println("<option  value=\"Government\">Government</option>");
                                                   out.println("<option value=\"Aid Grant / Credit\">Aid Grant / Credit</option>");
                                                   out.println("<option selected=\"selected\" value=\"Own Funds\">Own Funds</option>");
                                               }
                                             else{
                                                   out.println("<option selected=\"selected\" value=\"0\">--- Please Select ---</option>");
                                                   out.println("<option  value=\"Government\">Government</option>");
                                                   out.println("<option value=\"Aid Grand / Credit\">Aid Grand / Credit</option>");
                                                   out.println("<option  value=\"Own Funds\">Own Funds</option>");
                                               }
                                                %>
                                        <!-- <option value="select" selected="selected">--- Please Select ---</option-->>
                                     </select>
                                    <br />
                                    <span style="color: red;" id="msgSourceOfFund" class="cmbMsg"></span>
                                </td>
                                <td class="ff" width="25%">Development Partner : </td>
                                
                                <td width="25%">
                                    <input id="txtDevPartner" class="formTxtBox_1" type="text" style="width: 280px;" name="devPartner" value="<%=CommonUtils.checkNull(preqExcellBean.getDevelopmentPartner()) %>" />
                                </td>
                            </tr>

                        </tbody></table>
                        <div class="tableHead_22 ">Particular Information :</div>
                        <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                 <td class="ff" width="25%">Project Code : </td>
                                <td width="25%">
                                    <input name="projectCode" class="formTxtBox_1" id="txtProjectCode" style="width: 280px;" type="text" value="<%=CommonUtils.checkNull(preqExcellBean.getProjectOrProgrammeCode()) %>" />
                                </td>
                                <td class="ff" width="25%">Project Name : </td>
                                <td width="25%">
                                    <input name="projectName" class="formTxtBox_1" id="txtProjectName" style="width: 280px;" type="text" value="<%=CommonUtils.checkNull(preqExcellBean.getProjectOrProgrammeName()) %>" />
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="25%">Tender Package No. : <span>*</span> </td>
                                <td width="25%">
                                    <input name="packageNo" class="formTxtBox_1" id="txtPackageNo" style="width: 280px;" type="text" value="<%=CommonUtils.checkNull(preqExcellBean.getTenderPackageNo()) %>" />
                                </td>
                                <td class="ff" width="25%">Tender Package Name : <span>*</span></td>
                                <td width="25%">
                                    <input name="packageName" class="formTxtBox_1" id="txtPackageName" style="width: 280px;" type="text" value="<%=CommonUtils.checkNull(preqExcellBean.getTenderPackageName()) %>" />
                                </td>
                            </tr>
                            <tr>

                                <td class="ff">Scheduled Pre-Qualification Publication<br>Date : <span>*</span></td>
                                <td class="formStyle_1"><input name="tenderpublicationDate" value="<%= CommonUtils.checkNull(preqExcellBean.getPrequalificationPublicationDate()) %>" class="formTxtBox_1" id="txttenderpublicationDate" style="width: 100px;"  value="" onfocus="GetCalWithouTime('txttenderpublicationDate','txttenderpublicationDate');" onblur="findHoliday(this,0);" type="text">
                                    <img id="txttenderpublicationDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCalWithouTime('txttenderpublicationDate','txttenderpublicationDateimg');" border="0">
                                    <span id="spantxttenderpublicationDate"></span>
                                </td>

                                <td class="ff">Pre-Qualification  Closing<br>Date and Time : <span>*</span></td>
                                <td class="formStyle_1"><input name="preQualCloseDate" value="<%= CommonUtils.checkNull(preqExcellBean.getPrequalificationClosingDateandTime()) %>" class="formTxtBox_1" id="txtpreQualCloseDate" style="width: 100px;"  onfocus="GetCal('txtpreQualCloseDate','txtpreQualCloseDate');" onblur="findHoliday(this,4);" type="text">
                                    <img id="txtpreQualCloseDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCal('txtpreQualCloseDate','txtpreQualCloseDateimg');" border="0">
                                    <span id="spantxtpreQualCloseDate"></span>
                                </td>

                            </tr>

                            <tr>
                               <td class="ff">Place of Pre - Qualification Meeting :</td>
                                <td class="formStyle_1">
                                    <textarea class="formTxtBox_1" style="width: 280px;" name="placeofPQMeeting" rows="2" cols="49" id="txtPlaceofPQMeeting"><%= CommonUtils.checkNull(preqExcellBean.getPlace_Date_TimeofPrequalificationMeeting()) %></textarea>
                                </td>

                                <td class="ff">Pre - Qualification Meeting<br>Date and Time : <span></span>
                                    <input id="hdncheck" value="Yes" type="hidden">
                                </td>
                                <td class="formStyle_1"><input name="preTenderMeetStartDate" value="<%=CommonUtils.checkNull( preqExcellBean.getDateTimeofPrequalificationMeeting()) %>" class="formTxtBox_1" id="txtpreTenderMeetStartDate" style="width: 100px;" onfocus="GetCal('txtpreTenderMeetStartDate','txtpreTenderMeetStartDate');" onblur="findHoliday(this,2);" type="text">
                                    <img id="txtpreTenderMeetStartDateimg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" style="vertical-align: middle;" onclick="GetCal('txtpreTenderMeetStartDate','txtpreTenderMeetStartDateimg');" border="0">
                                    <span id="spantxtpreTenderMeetStartDate"></span>
                                </td>
                            </tr>

                            <tr>
                                <td class="ff">Selling Pre-qualification Document (Principle) : <span>*</span></td>
                                <td class="formStyle_1">
                                    <input id="txtSellingDocPrinciple" class="formTxtBox_1" type="text" style="width: 280px;" name="sellingDocPrinciple" value="<%= CommonUtils.checkNull(preqExcellBean.getSellingPrequalificationDocumentPrincipal() )%>" />
                                </td>

                                <td class="ff">Selling Pre-qualification Document (Others) : </td>
                                <td class="formStyle_1">
                                    <input id="txtSellingDocOthers" class="formTxtBox_1" type="text" style="width: 280px;" name="sellingDocOthers" value="<%= CommonUtils.checkNull(preqExcellBean.getSellingPrequalificationDocumentOthers()) %>" />
                                </td>
                            </tr>

                            <tr>
                                <td class="ff">Receiving Pre-qualification : <span>*</span></td>
                                <td class="formStyle_1">
                                    <input id="txtReceivingPQ" class="formTxtBox_1" type="text" style="width: 280px;" name="receivingPQ" value="<%=CommonUtils.checkNull( preqExcellBean.getReceivingPrequalification()) %>" />
                                </td>

                                <td class="ff"></td>
                                <td class="formStyle_1"></td>
                            </tr>

                        </tbody></table>

                        <div class="tableHead_22 ">Information for Bidder/Consultant :</div>

                        <table class="formStyle_1 " width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td class="ff"><input id="hdnevenyType" value="Works" type="hidden">Eligibility of Bidder/Consultant : <span>*</span></td>
                                <td>
                                    <textarea class="formTxtBox_1" style="width: 280px;" style="display: none;" cols="100" rows="5" id="txtaeligibilityofTenderer" name="eligibilityofTenderer" class="ckeditor"><%= CommonUtils.checkNull(preqExcellBean.getEligibilityofApplicant()) %></textarea>
                                    <!--<span id="cke_txtaeligibilityofTenderer" onmousedown="return false;" class="cke_skin_kama cke_editor_txtaeligibilityofTenderer" dir="ltr" title=" " role="application" aria-labelledby="cke_txtaeligibilityofTenderer_arialbl" lang="en"><span id="cke_txtaeligibilityofTenderer_arialbl" class="cke_voice_label">Rich Text Editor</span><span class="cke_browser_gecko" role="presentation"><span class="cke_wrapper cke_ltr" role="presentation"><table class="cke_editor" role="presentation" border="0" cellpadding="0" cellspacing="0"><tbody><tr style="-moz-user-select: none;" role="presentation"><td id="cke_top_txtaeligibilityofTenderer" class="cke_top" role="presentation"><div class="cke_toolbox" role="toolbar" aria-labelledby="cke_2"><span id="cke_2" class="cke_voice_label">Toolbar</span><span id="cke_3" class="cke_toolbar" role="presentation"><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><span class="cke_button"><a id="cke_4" class="cke_off cke_button_source" title="Source" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_4_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(0, event);" onfocus="return CKEDITOR.ui.button._.focus(0, event);" onclick="CKEDITOR.tools.callFunction(3, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_4_label" class="cke_label">Source</span></a></span><span class="cke_button"><a id="cke_5" class="cke_off cke_button_cut" title="Cut" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_5_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(1, event);" onfocus="return CKEDITOR.ui.button._.focus(1, event);" onclick="CKEDITOR.tools.callFunction(4, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_5_label" class="cke_label">Cut</span></a></span><span class="cke_button"><a id="cke_6" class="cke_off cke_button_copy" title="Copy" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_6_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(2, event);" onfocus="return CKEDITOR.ui.button._.focus(2, event);" onclick="CKEDITOR.tools.callFunction(5, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_6_label" class="cke_label">Copy</span></a></span><span class="cke_button"><a id="cke_7" class="cke_off cke_button_paste" title="Paste" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_7_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(3, event);" onfocus="return CKEDITOR.ui.button._.focus(3, event);" onclick="CKEDITOR.tools.callFunction(6, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_7_label" class="cke_label">Paste</span></a></span><span class="cke_button"><a id="cke_8" class="cke_off cke_button_pastefromword" title="Paste from Word" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_8_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(4, event);" onfocus="return CKEDITOR.ui.button._.focus(4, event);" onclick="CKEDITOR.tools.callFunction(7, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_8_label" class="cke_label">Paste from Word</span></a></span></span><span class="cke_separator" role="separator"></span><span class="cke_toolgroup" role="presentation"><span class="cke_button"><a id="cke_9" class="cke_off cke_button_numberedlist" title="Insert/Remove Numbered List" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_9_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(5, event);" onfocus="return CKEDITOR.ui.button._.focus(5, event);" onclick="CKEDITOR.tools.callFunction(8, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_9_label" class="cke_label">Insert/Remove Numbered List</span></a></span><span class="cke_button"><a id="cke_10" class="cke_off cke_button_bulletedlist" title="Insert/Remove Bulleted List" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_10_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(6, event);" onfocus="return CKEDITOR.ui.button._.focus(6, event);" onclick="CKEDITOR.tools.callFunction(9, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_10_label" class="cke_label">Insert/Remove Bulleted List</span></a></span></span><span class="cke_separator" role="separator"></span><span class="cke_toolgroup" role="presentation"><span class="cke_button"><a id="cke_11" class="cke_off cke_button_print" title="Print" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_11_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(7, event);" onfocus="return CKEDITOR.ui.button._.focus(7, event);" onclick="CKEDITOR.tools.callFunction(10, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_11_label" class="cke_label">Print</span></a></span></span><span class="cke_separator" role="separator"></span><span class="cke_toolgroup" role="presentation"><span class="cke_button"><a aria-disabled="true" id="cke_12" class="cke_button_undo cke_disabled" title="Undo" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_12_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(8, event);" onfocus="return CKEDITOR.ui.button._.focus(8, event);" onclick="CKEDITOR.tools.callFunction(11, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_12_label" class="cke_label">Undo</span></a></span><span class="cke_button"><a aria-disabled="true" id="cke_13" class="cke_button_redo cke_disabled" title="Redo" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_13_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(9, event);" onfocus="return CKEDITOR.ui.button._.focus(9, event);" onclick="CKEDITOR.tools.callFunction(12, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_13_label" class="cke_label">Redo</span></a></span></span><span class="cke_separator" role="separator"></span><span class="cke_toolgroup" role="presentation"><span class="cke_button"><a id="cke_14" class="cke_off cke_button_bold" title="Bold" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_14_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(10, event);" onfocus="return CKEDITOR.ui.button._.focus(10, event);" onclick="CKEDITOR.tools.callFunction(13, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_14_label" class="cke_label">Bold</span></a></span><span class="cke_button"><a id="cke_15" class="cke_off cke_button_italic" title="Italic" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_15_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(11, event);" onfocus="return CKEDITOR.ui.button._.focus(11, event);" onclick="CKEDITOR.tools.callFunction(14, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_15_label" class="cke_label">Italic</span></a></span><span class="cke_button"><a id="cke_16" class="cke_off cke_button_underline" title="Underline" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_16_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(12, event);" onfocus="return CKEDITOR.ui.button._.focus(12, event);" onclick="CKEDITOR.tools.callFunction(15, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_16_label" class="cke_label">Underline</span></a></span></span><span class="cke_separator" role="separator"></span><span class="cke_toolgroup" role="presentation"><span class="cke_button"><a id="cke_17" class="cke_off cke_button_justifyleft" title="Left Justify" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_17_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(13, event);" onfocus="return CKEDITOR.ui.button._.focus(13, event);" onclick="CKEDITOR.tools.callFunction(16, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_17_label" class="cke_label">Left Justify</span></a></span><span class="cke_button"><a id="cke_18" class="cke_off cke_button_justifycenter" title="Center Justify" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_18_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(14, event);" onfocus="return CKEDITOR.ui.button._.focus(14, event);" onclick="CKEDITOR.tools.callFunction(17, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_18_label" class="cke_label">Center Justify</span></a></span><span class="cke_button"><a id="cke_19" class="cke_off cke_button_justifyright" title="Right Justify" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_19_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(15, event);" onfocus="return CKEDITOR.ui.button._.focus(15, event);" onclick="CKEDITOR.tools.callFunction(18, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_19_label" class="cke_label">Right Justify</span></a></span><span class="cke_button"><a id="cke_20" class="cke_off cke_button_justifyblock" title="Block Justify" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_20_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(16, event);" onfocus="return CKEDITOR.ui.button._.focus(16, event);" onclick="CKEDITOR.tools.callFunction(19, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_20_label" class="cke_label">Block Justify</span></a></span></span><span class="cke_separator" role="separator"></span><span class="cke_toolgroup" role="presentation"><span class="cke_button"><a id="cke_21" class="cke_off cke_button_link" title="Link" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_21_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(17, event);" onfocus="return CKEDITOR.ui.button._.focus(17, event);" onclick="CKEDITOR.tools.callFunction(20, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_21_label" class="cke_label">Link</span></a></span></span><span class="cke_separator" role="separator"></span><span class="cke_toolgroup" role="presentation"><span class="cke_button"><a id="cke_22" class="cke_off cke_button_image" title="Image" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_22_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(18, event);" onfocus="return CKEDITOR.ui.button._.focus(18, event);" onclick="CKEDITOR.tools.callFunction(21, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_22_label" class="cke_label">Image</span></a></span><span class="cke_button"><a id="cke_23" class="cke_off cke_button_table" title="Table" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_23_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(19, event);" onfocus="return CKEDITOR.ui.button._.focus(19, event);" onclick="CKEDITOR.tools.callFunction(22, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_23_label" class="cke_label">Table</span></a></span><span class="cke_button"><a id="cke_24" class="cke_off cke_button_horizontalrule" title="Insert Horizontal Line" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_24_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(20, event);" onfocus="return CKEDITOR.ui.button._.focus(20, event);" onclick="CKEDITOR.tools.callFunction(23, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_24_label" class="cke_label">Insert Horizontal Line</span></a></span></span><span class="cke_toolbar_end"></span></span><span id="cke_25" class="cke_toolbar" role="presentation"><span class="cke_toolbar_start"></span><span class="cke_rcombo"><span id="cke_26" class="cke_format cke_off"><span id="cke_26_label" class="cke_label">Format</span><a hidefocus="true" title="Paragraph Format" tabindex="-1" role="button" aria-labelledby="cke_26_label" aria-describedby="cke_26_text" aria-haspopup="true" onblur="this.style.cssText = this.style.cssText;" onkeydown="CKEDITOR.tools.callFunction( 25, event, this );" onclick="CKEDITOR.tools.callFunction(24, this); return false;"><span><span id="cke_26_text" class="cke_text cke_inline_label">Format</span></span><span class="cke_openbutton"></span></a></span></span><span class="cke_rcombo"><span id="cke_27" class="cke_font cke_off"><span id="cke_27_label" class="cke_label">Font</span><a hidefocus="true" title="Font Name" tabindex="-1" role="button" aria-labelledby="cke_27_label" aria-describedby="cke_27_text" aria-haspopup="true" onblur="this.style.cssText = this.style.cssText;" onkeydown="CKEDITOR.tools.callFunction( 27, event, this );" onclick="CKEDITOR.tools.callFunction(26, this); return false;"><span><span id="cke_27_text" class="cke_text cke_inline_label">Font</span></span><span class="cke_openbutton"></span></a></span></span><span class="cke_rcombo"><span id="cke_28" class="cke_fontSize cke_off"><span id="cke_28_label" class="cke_label">Size</span><a hidefocus="true" title="Font Size" tabindex="-1" role="button" aria-labelledby="cke_28_label" aria-describedby="cke_28_text" aria-haspopup="true" onblur="this.style.cssText = this.style.cssText;" onkeydown="CKEDITOR.tools.callFunction( 29, event, this );" onclick="CKEDITOR.tools.callFunction(28, this); return false;"><span><span id="cke_28_text" class="cke_text cke_inline_label">Size</span></span><span class="cke_openbutton"></span></a></span></span><span class="cke_toolgroup" role="presentation"><span class="cke_button"><a id="cke_29" class="cke_off cke_button_textcolor" title="Text Color" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_29_label" aria-haspopup="true" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(21, event);" onfocus="return CKEDITOR.ui.button._.focus(21, event);" onclick="CKEDITOR.tools.callFunction(30, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_29_label" class="cke_label">Text Color</span><span class="cke_buttonarrow">&nbsp;</span></a></span><span class="cke_button"><a id="cke_30" class="cke_off cke_button_bgcolor" title="Background Color" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_30_label" aria-haspopup="true" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(22, event);" onfocus="return CKEDITOR.ui.button._.focus(22, event);" onclick="CKEDITOR.tools.callFunction(31, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_30_label" class="cke_label">Background Color</span><span class="cke_buttonarrow">&nbsp;</span></a></span></span><span class="cke_toolbar_end"></span></span></div><a title="Collapse Toolbar" id="cke_31" tabindex="-1" class="cke_toolbox_collapser" onclick="CKEDITOR.tools.callFunction(32)"><span>▲</span></a></td></tr><tr role="presentation"><td id="cke_contents_txtaeligibilityofTenderer" class="cke_contents" style="height: 200px;" role="presentation"><iframe style="width: 100%; height: 100%;" title="Rich text editor, txtaeligibilityofTenderer, press ALT 0 for help." src="" tabindex="0" allowtransparency="true" frameborder="0"></iframe></td></tr><tr style="-moz-user-select: none;" role="presentation"><td id="cke_bottom_txtaeligibilityofTenderer" class="cke_bottom" role="presentation"><div class="cke_resizer" title="Drag to resize" onmousedown="CKEDITOR.tools.callFunction(2, event)"></div></td></tr></tbody></table><style>.cke_skin_kama{visibility:hidden;}</style></span></span><span tabindex="-1" style="position: absolute;" role="presentation"></span></span>
                                    -->
                                    <script type="text/javascript">
                                        //<![CDATA[
                                        CKEDITOR.replace( 'eligibilityofTenderer',
                                        {
                                            toolbar : "egpToolbar"

                                        });
                                        //]]>
                                    </script>
                                    <span id="spantxtaeligibilityofTenderer"></span>
                                </td>

                            </tr>
                            <tr>
                                <td class="ff">Brief Description of Goods or Works : <span>*</span></td>
                                <td><input value="Works" id="briefValMsg" type="hidden">
                                    <textarea class="formTxtBox_1" style="width: 280px;" style="display: none;" cols="100" rows="5" id="txtabriefDescGoods" name="briefDescGoods" class="ckeditor"><%= CommonUtils.checkNull(preqExcellBean.getBriefDescriptionofGoodsorWorks()) %></textarea>
                                    <!--<span id="cke_txtabriefDescGoods" onmousedown="return false;" class="cke_skin_kama cke_editor_txtabriefDescGoods" dir="ltr" title=" " role="application" aria-labelledby="cke_txtabriefDescGoods_arialbl" lang="en"><span id="cke_txtabriefDescGoods_arialbl" class="cke_voice_label">Rich Text Editor</span><span class="cke_browser_gecko" role="presentation"><span class="cke_wrapper cke_ltr" role="presentation"><table class="cke_editor" role="presentation" border="0" cellpadding="0" cellspacing="0"><tbody><tr style="-moz-user-select: none;" role="presentation"><td id="cke_top_txtabriefDescGoods" class="cke_top" role="presentation"><div class="cke_toolbox" role="toolbar" aria-labelledby="cke_39"><span id="cke_39" class="cke_voice_label">Toolbar</span><span id="cke_40" class="cke_toolbar" role="presentation"><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><span class="cke_button"><a id="cke_41" class="cke_off cke_button_source" title="Source" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_41_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(23, event);" onfocus="return CKEDITOR.ui.button._.focus(23, event);" onclick="CKEDITOR.tools.callFunction(37, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_41_label" class="cke_label">Source</span></a></span><span class="cke_button"><a id="cke_42" class="cke_off cke_button_bold" title="Bold" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_42_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(24, event);" onfocus="return CKEDITOR.ui.button._.focus(24, event);" onclick="CKEDITOR.tools.callFunction(38, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_42_label" class="cke_label">Bold</span></a></span><span class="cke_button"><a id="cke_43" class="cke_off cke_button_italic" title="Italic" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_43_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(25, event);" onfocus="return CKEDITOR.ui.button._.focus(25, event);" onclick="CKEDITOR.tools.callFunction(39, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_43_label" class="cke_label">Italic</span></a></span></span><span class="cke_separator" role="separator"></span><span class="cke_toolgroup" role="presentation"><span class="cke_button"><a id="cke_44" class="cke_off cke_button_link" title="Link" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_44_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(26, event);" onfocus="return CKEDITOR.ui.button._.focus(26, event);" onclick="CKEDITOR.tools.callFunction(40, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_44_label" class="cke_label">Link</span></a></span><span class="cke_button"><a aria-disabled="true" id="cke_45" class="cke_button_unlink cke_disabled" title="Unlink" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_45_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(27, event);" onfocus="return CKEDITOR.ui.button._.focus(27, event);" onclick="CKEDITOR.tools.callFunction(41, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_45_label" class="cke_label">Unlink</span></a></span></span><span class="cke_toolbar_end"></span></span></div><a title="Collapse Toolbar" id="cke_46" tabindex="-1" class="cke_toolbox_collapser" onclick="CKEDITOR.tools.callFunction(42)"><span>▲</span></a></td></tr><tr role="presentation"><td id="cke_contents_txtabriefDescGoods" class="cke_contents" style="height: 200px;" role="presentation"><iframe style="width: 100%; height: 100%;" title="Rich text editor, txtabriefDescGoods, press ALT 0 for help." src="" tabindex="0" allowtransparency="true" frameborder="0"></iframe></td></tr><tr style="-moz-user-select: none;" role="presentation"><td id="cke_bottom_txtabriefDescGoods" class="cke_bottom" role="presentation"><div class="cke_resizer" title="Drag to resize" onmousedown="CKEDITOR.tools.callFunction(36, event)"></div></td></tr></tbody></table><style>.cke_skin_kama{visibility:hidden;}</style></span></span><span tabindex="-1" style="position: absolute;" role="presentation"></span></span>
                                    -->
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    CKEDITOR.replace( 'briefDescGoods',
                                    {
                                        //toolbar : "Basic"
                                        toolbar : "egpToolbar"
                                    });
                                    //]]>
                                    </script>
                                    <span id="spantxtabriefDescGoods"></span>
                                </td><td>&nbsp;</td>
                            </tr>

                            <tr>
                                <td class="ff">Brief Description of Related Services : <span>*</span></td>
                                <td><input value="Works" id="briefValMsg" type="hidden">
                                    <textarea class="formTxtBox_1" style="width: 280px;" style="display: none;" cols="100" rows="5" id="txtBriefDescRelServices" name="briefDescRelServices" class="ckeditor"><%= CommonUtils.checkNull(preqExcellBean.getBriefDescriptionofRelatedServices()) %></textarea>
                                    <!--<span id="cke_txtabriefDescGoods" onmousedown="return false;" class="cke_skin_kama cke_editor_txtabriefDescGoods" dir="ltr" title=" " role="application" aria-labelledby="cke_txtabriefDescGoods_arialbl" lang="en"><span id="cke_txtabriefDescGoods_arialbl" class="cke_voice_label">Rich Text Editor</span><span class="cke_browser_gecko" role="presentation"><span class="cke_wrapper cke_ltr" role="presentation"><table class="cke_editor" role="presentation" border="0" cellpadding="0" cellspacing="0"><tbody><tr style="-moz-user-select: none;" role="presentation"><td id="cke_top_txtabriefDescGoods" class="cke_top" role="presentation"><div class="cke_toolbox" role="toolbar" aria-labelledby="cke_39"><span id="cke_39" class="cke_voice_label">Toolbar</span><span id="cke_40" class="cke_toolbar" role="presentation"><span class="cke_toolbar_start"></span><span class="cke_toolgroup" role="presentation"><span class="cke_button"><a id="cke_41" class="cke_off cke_button_source" title="Source" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_41_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(23, event);" onfocus="return CKEDITOR.ui.button._.focus(23, event);" onclick="CKEDITOR.tools.callFunction(37, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_41_label" class="cke_label">Source</span></a></span><span class="cke_button"><a id="cke_42" class="cke_off cke_button_bold" title="Bold" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_42_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(24, event);" onfocus="return CKEDITOR.ui.button._.focus(24, event);" onclick="CKEDITOR.tools.callFunction(38, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_42_label" class="cke_label">Bold</span></a></span><span class="cke_button"><a id="cke_43" class="cke_off cke_button_italic" title="Italic" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_43_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(25, event);" onfocus="return CKEDITOR.ui.button._.focus(25, event);" onclick="CKEDITOR.tools.callFunction(39, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_43_label" class="cke_label">Italic</span></a></span></span><span class="cke_separator" role="separator"></span><span class="cke_toolgroup" role="presentation"><span class="cke_button"><a id="cke_44" class="cke_off cke_button_link" title="Link" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_44_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(26, event);" onfocus="return CKEDITOR.ui.button._.focus(26, event);" onclick="CKEDITOR.tools.callFunction(40, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_44_label" class="cke_label">Link</span></a></span><span class="cke_button"><a aria-disabled="true" id="cke_45" class="cke_button_unlink cke_disabled" title="Unlink" tabindex="-1" hidefocus="true" role="button" aria-labelledby="cke_45_label" onblur="this.style.cssText = this.style.cssText;" onkeydown="return CKEDITOR.ui.button._.keydown(27, event);" onfocus="return CKEDITOR.ui.button._.focus(27, event);" onclick="CKEDITOR.tools.callFunction(41, this); return false;"><span class="cke_icon">&nbsp;</span><span id="cke_45_label" class="cke_label">Unlink</span></a></span></span><span class="cke_toolbar_end"></span></span></div><a title="Collapse Toolbar" id="cke_46" tabindex="-1" class="cke_toolbox_collapser" onclick="CKEDITOR.tools.callFunction(42)"><span>▲</span></a></td></tr><tr role="presentation"><td id="cke_contents_txtabriefDescGoods" class="cke_contents" style="height: 200px;" role="presentation"><iframe style="width: 100%; height: 100%;" title="Rich text editor, txtabriefDescGoods, press ALT 0 for help." src="" tabindex="0" allowtransparency="true" frameborder="0"></iframe></td></tr><tr style="-moz-user-select: none;" role="presentation"><td id="cke_bottom_txtabriefDescGoods" class="cke_bottom" role="presentation"><div class="cke_resizer" title="Drag to resize" onmousedown="CKEDITOR.tools.callFunction(36, event)"></div></td></tr></tbody></table><style>.cke_skin_kama{visibility:hidden;}</style></span></span><span tabindex="-1" style="position: absolute;" role="presentation"></span></span>
                                    -->
                                    <script type="text/javascript">
                                    //<![CDATA[
                                    CKEDITOR.replace( 'briefDescRelServices',
                                    {
                                        //toolbar : "Basic"
                                        toolbar : "egpToolbar"
                                    });
                                    //]]>
                                    </script>
                                    <span id="spantxtabriefDescGoods"></span>
                                </td><td>&nbsp;</td>
                            </tr>

                            <tr style="display: table-row;" id="docsprice">
                                    <td class="ff">Pre-Qualification Document Price (In Nu.)
                		        : <span>*</span></td>
                                    <td><input name="preQualDocPrice" onblur="documentPrice(this);" class="formTxtBox_1" id="txtpreQualDocPrice" style="width: 200px;" type="text" value="<%= CommonUtils.checkNull(preqExcellBean.getPrequalificationDocumentPrice()) %>" />
                                    <div id="preQualDocPriceInWords"></div>
                                    <span id="spantxtpreQualDocPriceman"></span>
                                </td>
                                <td>&nbsp;</td>
                            </tr>

                        </tbody></table>

                        <!--<div class="b_space" align="right" id="addRemoveLot">
                            <a id="addRow" class="action-button-add" onclick="AddRow()">Add Lot</a>
                            <a id="delRow" class="action-button-delete" onclick="RemoveRow()">Remove lot</a>
                        </div>-->

                        <table class="tableList_1 t_space" width="100%" cellspacing="0" id="tblLotDescription">
                            <tbody>
                            <tr>
                               <th class="t-align-left" width="6%">Lot No.</th>

                                <th class="t-align-left" width="54%">Identification of Lot <span class="mandatory">*</span></th>

                                <th class="t-align-center" width="10%">Location</th>

                                <th class="t-align-center" width="10%">Completion Time in weeks/months </th>
                            </tr>
                            <tr>
                            <td class="t-align-center">
                                    
                                    <input name="lotNo_1" class="formTxtBox_1" id="txtLotNo_1" type="text" value="<%=CommonUtils.checkNull(preqExcellBean.getLotNo_1()) %>" />
                                </td>
                                <td class="t-align-center">

                                     <textarea class="formTxtBox_1" style="width: 280px;" name="identification_1" rows="2" cols="40" id="txtidentification_1"  ><%=CommonUtils.checkNull(preqExcellBean.getIdentificationOfLot_1()) %></textarea>

                                </td>
                                <td class="t-align-center"><input name="locationlot_1" style="width: 180px;" value="<%=CommonUtils.checkNull(preqExcellBean.getLocation_1()) %>" class="formTxtBox_1" id="txtlocationlot_1" onblur="chkLocLotBlank(this);" type="text"><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_1" value="<%=CommonUtils.checkNull(preqExcellBean.getCompletionTimeInWeeksORmonths_1()) %>"  class="formTxtBox_1" id="txtcomplTimeLotNo_1" style="width: 100px;"  type="text"><span id="compLot_0" style="color: red;">&nbsp;</span>
                                   
                                </td>
                            </tr>
                            <tr id="lot_1" style="<% if(CommonUtils.checkNull(preqExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> display:table-row;<%}%>" >
                                <td class="t-align-center"  >
                                    <input name="lotNo_2" class="formTxtBox_1" id="txtLotNo_2" type="text" value="<%=CommonUtils.checkNull(preqExcellBean.getLotNo_2()) %>" />
                                </td>
                                <td class="t-align-center">

                                     <textarea class="formTxtBox_1" style="width: 280px;" name="identification_2" rows="2" cols="40" id="txtidentification_2" ><%=CommonUtils.checkNull(preqExcellBean.getIdentificationOfLot_2()) %></textarea>

                                </td>
                                <td class="t-align-center"><input name="locationlot_2" value="<%= CommonUtils.checkNull(preqExcellBean.getLocation_2()) %>" style="width: 180px;" class="formTxtBox_1" id="txtlocationlot_2" onblur="chkLocLotBlank(this);" type="text"><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_2" value="<%= CommonUtils.checkNull(preqExcellBean.getCompletionTimeInWeeksORmonths_2()) %>"  class="formTxtBox_1" id="txtcomplTimeLotNo_2" style="width: 100px;"  type="text"><span id="compLot_0" style="color: red;">&nbsp;</span>
                                    
                                </td>
                            </tr>
                             <tr id="lot_2" style="<% if(CommonUtils.checkNull(preqExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> display:table-row;<%}%>" >
                                <td class="t-align-center">
                                    <input name="lotNo_3" class="formTxtBox_1" id="txtLotNo_3" type="text" value="<%= CommonUtils.checkNull(preqExcellBean.getLotNo_3()) %>" />
                                </td>
                                <td class="t-align-center">

                                     <textarea class="formTxtBox_1" style="width: 280px;" name="identification_3" rows="2" cols="40" id="txtidentification_3" > <%=CommonUtils.checkNull(preqExcellBean.getIdentificationOfLot_3())%></textarea>

                                </td>
                                <td class="t-align-center"><input name="locationlot_3" value="<%= CommonUtils.checkNull(preqExcellBean.getLocation_3()) %>" style="width: 180px;" class="formTxtBox_1" id="txtlocationlot_2" onblur="chkLocLotBlank(this);" type="text"><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_3" value="<%= CommonUtils.checkNull(preqExcellBean.getCompletionTimeInWeeksORmonths_3()) %>"  class="formTxtBox_1" id="txtcomplTimeLotNo_3" style="width: 100px;"  type="text"><span id="compLot_0" style="color: red;">&nbsp;</span>
                                   
                                </td>
                            </tr>
                            <tr id="lot_3" style="<% if(CommonUtils.checkNull(preqExcellBean.getInvitationFor()).contains("Single Lot")){ %> display:none; <%}else{%> display:table-row;<%}%>" >
                                <td class="t-align-center">
                                    <input name="lotNo_4" class="formTxtBox_1" id="txtLotNo_4" type="text" value="<%=CommonUtils.checkNull( preqExcellBean.getLotNo_4()) %>" />
                                </td>
                                <td class="t-align-center">

                                     <textarea class="formTxtBox_1" style="width: 280px;" name="identification_4" rows="2" cols="40" id="txtidentification_4"  ><%= CommonUtils.checkNull(preqExcellBean.getIdentificationOfLot_4()) %></textarea>

                                </td>
                                <td class="t-align-center"><input name="locationlot_4" value="<%= CommonUtils.checkNull(preqExcellBean.getLocation_4()) %>" style="width: 180px;" class="formTxtBox_1" id="txtlocationlot_4" onblur="chkLocLotBlank(this);" type="text"><span id="locLot_0" style="color: red;">&nbsp;</span></td>

                                <td class="t-align-center">
                                    <input name="complTimeLotNo_4" value="<%= CommonUtils.checkNull(preqExcellBean.getCompletionTimeInWeeksORmonths_4()) %>" class="formTxtBox_1" id="txtcomplTimeLotNo_4" style="width: 100px;"  type="text"><span id="compLot_0" style="color: red;">&nbsp;</span>
                                    
                                </td>
                            </tr>


                        </tbody></table>


                        <div class="tableHead_22 t_space">Procuring Entity Details :</div>
                        <table class="formStyle_1 t_space" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                    <td class="ff" width="25%">Name of Official Inviting  Pre-Qualification : <span>*</span></td>
                                <td width="25%">
                                    <input name="nameOfficial" class="formTxtBox_1" id="txtNameOfficial" style="width: 280px;" value="<%= CommonUtils.checkNull(preqExcellBean.getNameofOfficialInvitingPrequalification()) %>" type="text">
                                </td>
                                    <td class="ff" width="26%"> Designation of Official Inviting  Pre-Qualification : <span>*</span></td>
                                <td width="25%">
                                    <input name="designationOfficial" class="formTxtBox_1" id="txtDesignationOfficial" value="<%= CommonUtils.checkNull(preqExcellBean.getDesignationofOfficialInvitingPrequalification()) %>" style="width: 280px;" type="text">
                                </td>
                            </tr>

                            <tr>
                                <td class="ff">Address of Official Inviting  Pre-Qualification : </td>
                                <td>
                                    <textarea class="formTxtBox_1" style="width: 280px;" name="addressOfOfficial" rows="2" cols="49" id="txtAddress"><%= CommonUtils.checkNull(preqExcellBean.getAddressofOfficialInvitingPrequalification()) %></textarea>
                                </td>
                                <td class="ff">Contact details of Official Inviting  Pre-Qualification :</td>
                                <td>
                                    <% String contacts="";
                                        if(request.getParameterMap().containsKey("action")){
                                             contacts = CommonUtils.checkNull(preqExcellBean.getContactdetailsofOfficialInvitingPrequalification());

                                        } else {

                                             contacts = "Phone:"+CommonUtils.checkNull(preqExcellBean.getContactdetailsofOfficialInvitingPrequalification())+" Fax:"+CommonUtils.checkNull(preqExcellBean.getFaxNo())+" Mail:"+CommonUtils.checkNull(preqExcellBean.getE_mail());
                                        }
                                        %> 
                                    <textarea class="formTxtBox_1" style="width: 280px;" name="contactDetail" rows="2" cols="49" id="txtContact"><%=contacts%>
                                        

                                    </textarea>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4" class="ff mandatory">The procuring entity reserves the right to accept or reject all Tenders/Proposals / Pre-Qualifications / EOIs</td>
                            </tr>
                        </tbody></table>

                        <table class="formStyle_1 t_space" width="100%" border="0" cellpadding="0" cellspacing="10">
                            <tbody><tr>
                                <td colspan="4" align="center">
                                    <label class="formBtn_1"><input name="submit" id="btnsubmit" value="Submit" type="submit">
                                        <input name="hdnbutton" id="hdnbutton" value="" type="hidden">
                                        <input name="action" id="action"  value="<%=action %>" type="hidden" />
                                        <input name="tenid" id="tenid"  value="<%=tenolId %>" type="hidden" />
                                    </label>&nbsp;&nbsp;
                                    
                                </td>
                            </tr>
                        </tbody></table>
                        <div>&nbsp;</div>
                    </form>
                    <!--Dashboard Content Part End-->
<% } %>
             
                   
                    <script type="text/javascript">
                        function loadOrganization() {
                            //var deptId= 0;
                           // var districtId = $('#cmbDistrict').val();
                           //if($('#cmbDivision').val()>0){
                            //   deptId=$('#cmbDivision').val();
                           //}
                           //else{deptId= $('#cmbMinistry').val(); }
                           
                            var deptId= $('#cmbMinistry').val();
                            $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: deptId, funName:'offlineTenderOrgCombo'},  function(j){
                            $('#cmbOrganization').children().remove().end()
                            $("select#cmbOrganization").html(j);
                            var orgObj =document.getElementById("cmbMinistry");
                            var orgval = orgObj.options[orgObj.selectedIndex].text;
                           document.getElementById("hidministry").value = orgval;
                        });
                    }
                    </script>

                    <script language="javascript" type="text/javascript">
                        function setSerType(obj,flag){

                            if("Works"==obj.options[obj.selectedIndex].text){
                               // $('#trSerType').hide();
                               // $('#trRFReq').hide();
                               // $('#trPQReq').show();
                               // if(flag){
                                 //   document.getElementById("cmbPQRequires").options[0].selected = "selected";
                               // }
                                $.post("<%=request.getContextPath()%>/APPServlet", {projectId:'No',param4:$('#hdnPM').val(),procNature:$('#cmbProcureNature').val(),funName:'getPM'},  function(j){
                                    $("select#cmbProcureMethod").html(j);
                                });
                                //$('#trLotBtn').show();

                            }
                            else {
                                $('#trSerType').hide();
                                $('#trRFReq').hide();
                                $('#trPQReq').hide();
                                $.post("<%=request.getContextPath()%>/APPServlet", {projectId:'No',procType:$('#cmbProcureType').val(),param4: $('#hdnPM').val(),procNature:$('#cmbProcureNature').val(),funName:'getPM'},  function(j){
                                    $("select#cmbProcureMethod").html(j);
                                });
                                //$('#trLotBtn').show();

                            }
                        }
                    </script>

                    <script type="text/javascript">
                        function GetCal(txtname,controlname)
                        {
                            new Calendar({
                                inputField: txtname,
                                trigger: controlname,
                                showTime: 24,
                                onSelect: function() {
                                    var date = Calendar.intToDate(this.selection.get());
                                    LEFT_CAL.args.min = date;
                                    LEFT_CAL.redraw();
                                    this.hide();
                                    document.getElementById(txtname).focus();
                                }
                            });

                            var LEFT_CAL = Calendar.setup({
                                weekNumbers: false
                            })
                        }

                        function GetCalWithouTime(txtname,controlname)
                        {
                            new Calendar({
                                inputField: txtname,
                                trigger: controlname,
                                showTime: false,
                                dateFormat:"%d/%m/%Y",
                                onSelect: function() {
                                    var date = Calendar.intToDate(this.selection.get());
                                    LEFT_CAL.args.min = date;
                                    LEFT_CAL.redraw();
                                    this.hide();
                                    document.getElementById(txtname).focus();
                                }
                            });

                            var LEFT_CAL = Calendar.setup({
                                weekNumbers: false
                            })
                        }

                    </script>

                    <script type="text/javascript">

                        //Function for Required
                        function required(controlid)
                        {
                            var temp=controlid.length;
                            if(temp <= 0 ){
                                return false;
                            }else{
                                return true;
                            }
                        }

                        //Function for MaxLength
                        function Maxlenght(controlid,maxlenght)
                        {
                            var temp=controlid.length;
                            if(temp>=maxlenght){
                                return false;
                            }else
                                return true;
                        }

                        //Function for digits
                        function digits(control) {
                            return /^\d+$/.test(control);
                        }

                        function CompareToForEqual(value,params)
                        {

                            var mdy = value.split('/')  //Date and month split
                            var mdyhr=mdy[2].split(' ');  //Year and time split
                            var mdyp = params.split('/')
                            var mdyphr=mdyp[2].split(' ');
                            var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                            var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                            return Date.parse(date) == Date.parse(datep);
                        }

                        //Function for CompareToForToday
                        function CompareToForToday(first)
                        {
                            var mdy = first.split('/')  //Date and month split
                            var mdyhr= mdy[2].split(' ');  //Year and time split
                            var mdyhrtime=mdyhr[1].split(':');
                            if(mdyhrtime[1] == undefined){
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                            }else
                            {
                                var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                            }

                            var d = new Date();
                            if(mdyhrtime[1] == undefined){
                                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                            }
                            else
                            {
                                var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(),d.getHours(),d.getMinutes());
                            }
                            return Date.parse(valuedate) > Date.parse(todaydate);
                        }

                        //Function for CompareToForGreater
                        function CompareToForGreater(value,params)
                        {
                            if(value!='' && params!=''){

                                var mdy = value.split('/')  //Date and month split
                                var mdyhr=mdy[2].split(' ');  //Year and time split
                                var mdyp = params.split('/')
                                var mdyphr=mdyp[2].split(' ');


                                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                                {
                                    //alert('Both Date');
                                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                                }
                                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                                {
                                    //alert('Both DateTime');
                                    var mdyhrsec=mdyhr[1].split(':');
                                    var date =  new Date( mdyhr[0], parseFloat(mdy[1])-1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var mdyphrsec=mdyphr[1].split(':');

                                    var datep =  new Date(mdyphr[0], parseFloat(mdyp[1])-1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                            }
                                else
                                {
                                    //alert('one Date and One DateTime');
                                    var a = mdyhr[1];  //time
                                    var b = mdyphr[1]; // time

                                    if(a == undefined && b != undefined)
                                    {
                                        //alert('First Date');
                                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                                        var mdyphrsec=mdyphr[1].split(':');
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                    }
                                    else
                                    {
                                        //alert('Second Date');
                                        var mdyhrsec=mdyhr[1].split(':');
                                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                                    }
                                }
                                return Date.parse(date) > Date.parse(datep);
                            }
                            else
                            {
                                return false;
                            }
                        }

                        //Function for CompareToForGreater
                        function CompareToForSmaller(value,params)
                        {
                            if(value!='' && params!=''){

                                var mdy = value.split('/')  //Date and month split
                                var mdyhr=mdy[2].split(' ');  //Year and time split
                                var mdyp = params.split('/')
                                var mdyphr=mdyp[2].split(' ');


                                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                                {
                                    //alert('Both Date');
                                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                                }
                                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                                {
                                    //alert('Both DateTime');
                                    var mdyhrsec=mdyhr[1].split(':');
                                    var date =  new Date( mdyhr[0], parseFloat(mdy[1])-1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var mdyphrsec=mdyphr[1].split(':');
                                    var datep =  new Date(mdyphr[0], parseFloat(mdyp[1])-1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                }
                                else
                                {
                                    //alert('one Date and One DateTime');
                                    var a = mdyhr[1];  //time
                                    var b = mdyphr[1]; // time

                                    if(a == undefined && b != undefined)
                                    {
                                        //alert('First Date');
                                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                                        var mdyphrsec=mdyphr[1].split(':');
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                    }
                                    else
                                    {
                                        //alert('Second Date');
                                        var mdyhrsec=mdyhr[1].split(':');
                                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                                    }
                                }

                                return Date.parse(date) < Date.parse(datep);
                            }
                            else
                            {
                                return false;
                            }
                        }

                        //Function for CompareToWithoutEqual
                        function CompareToWithoutEqual(value,params)
                        {
                            var mdy = value.split('/')  //Date and month split
                            var mdyhr=mdy[2].split(' ');  //Year and time split
                            var mdyp = params.split('/')
                            var mdyphr=mdyp[2].split(' ');


                            if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                            {
                                //alert('Both Date');
                                var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                                var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                            }
                            else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                            {
                                //alert('Both DateTime');
                                var mdyhrsec=mdyhr[1].split(':');
                                var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                var mdyphrsec=mdyphr[1].split(':');
                                var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                            }
                            else
                            {
                                //alert('one Date and One DateTime');
                                var a = mdyhr[1];  //time
                                var b = mdyphr[1]; // time

                                if(a == undefined && b != undefined)
                                {
                                    //alert('First Date');
                                    var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                                    var mdyphrsec=mdyphr[1].split(':');
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                                }
                                else
                                {
                                    //alert('Second Date');
                                    var mdyhrsec=mdyhr[1].split(':');
                                    var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                                }
                            }
                            return Date.parse(date) > Date.parse(datep);
                        }

                    </script>


                </div>
            </div>

            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>

    </body>

</html>
