<%-- 
    Document   : prr
    Created on : Jan 3, 2018, 3:56:31 PM
    Author     : Feroz
--%>

<%-- 
    Document   : eSBDs
    Created on : Jan 3, 2018, 11:58:22 AM
    Author     : Feroz
--%>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Procurement Rules and Regulations</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/simpletreemenu.js"></script>
        <style>
        div.gallery {
            margin: 5px;
            border: 1px solid #ccc;
            float: left;
            width: 180px;
        }

        div.gallery:hover {
            border: 1px solid #777;
            background-color: lightyellow;
        }

        div.gallery img {
            width: 100%;
            height: auto;
        }

        div.desc {
            padding: 15px;
            text-align: center;
        }

	#homePage{
		text-decoration:none;
		color:#FFF;
		padding-bottom:10px;
	}
</style>
    </head>
    <body>
        <div class="mainDiv" style="display:block;" id="listDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->  
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                       
                        <td class="contentArea-Blogin"><!--Page Content Start-->   
                         
                            <br><br>
                            <div class="pageHead_1" style="padding-left:0%;">Procurement Rules and Regulations
                            </div>
                            <br>
                            <span id="lTable">
                            <table   style="padding-left: 15%;width: 100%;">
                                <tr>
                                   <td align="center" style="">
                                    <div class="gallery" style="#">
                                        <a href="/PRR/pdf/Procurement Rules and Regulations.pdf" onclick="">
                                          <img src="/PRR/poster/prr.png" alt="Procurement Rules and Regulations" width="50" height="70" style="border:1px solid #021a40;">
                                          <div class="desc"><b>Procurement Rules and Regulations</div>
                                        </a>
                                      </div>
                                   </td>
                                   <td align="center" style="">
                                    <div class="gallery" style="padding-bottom: 2.3%;">
                                        <a href="/PRR/pdf/Procurement Guidelines.pdf" onclick="">
                                          <img src="/PRR/poster/procurementGuidelines.png" alt="Procurement Guidelines" width="50" height="70" style="border:1px solid #021a40;">
                                          <div class="desc"><b>Procurement Guidelines</div>
                                        </a>
                                      </div>
                                   </td>
                                   <td align="center" style="">
                                    <div class="gallery" style="padding-bottom: 2.3%;">
                                        <a href="/PRR/pdf/Evaluation Guideline for procurement of Works.pdf" onclick="">
                                          <img src="/PRR/poster/evaluationGuidelines.png" alt="Evaluation Guideline for Procurement of Works" width="50" height="70" style="border:1px solid #021a40;">
                                          <div class="desc"><b>Evaluation Guideline for Procurement of Works</div>
                                        </a>
                                      </div>
                                   </td>
                                   
                                </tr>
                                <tr>
                                   <td align="center" style="">
                                    <div class="gallery" style="#">
                                        <a href="/PRR/pdf/SampleEvaluationReport.pdf" onclick="">
                                          <img src="/PRR/poster/evaluationReport.png" alt="Sample Evaluation Report" width="50" height="70" style="border:1px solid #021a40;">
                                          <div class="desc"><b>Sample Evaluation Report</div>
                                        </a>
                                      </div>
                                   </td>
                                   
                                </tr>
                                
                            </table>
                            </span>
                                
                                
                        </td>
                    </tr>
                </table>
                
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
           
        
            


    </body>
</html>


