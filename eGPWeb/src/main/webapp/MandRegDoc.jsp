<%--
    Document   : Mandatory Registration Documents
    Created on : Jan 21, 2010, 12:54:06 PM
    Author     : Rikin
--%>

<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Mandatory Registration Documents</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(){

                var pauseA = 2500;
                var pause = 3500;

                function newstickerA()
                {
                    // algorithm:
                    // get last element, remove it from the list,
                    // add to first position with hidden style
                    // slideDown the new first element
                    // continue
                    last = $('ul#listtickerA li:last').hide().remove();
                    $('ul#listtickerA').prepend(last);
                    $('ul#listtickerA li:first').slideDown("slow");
                }

                interval = setInterval(newstickerA, pauseA);

                function newsticker()
                {
                    // algorithm:
                    // get last element, remove it from the list,
                    // add to first position with hidden style
                    // slideDown the new first element
                    // continue
                    last = $('ul#listticker li:last').hide().remove();
                    $('ul#listticker').prepend(last);
                    $('ul#listticker li:first').slideDown("slow");
                }

                interval = setInterval(newsticker, pause);
            });
        </script>
    </head>
    <body>
        <%
            String lang = null, contentmandregdoc = null;

            if(request.getParameter("lang")!=null && request.getParameter("lang")!=""){
                lang = request.getParameter("lang");
            }else{
                lang = "en_US";
            }

            MultiLingualService multiLingualService = (MultiLingualService)AppContext.getSpringBean("MultiLingualService");
            List<TblMultiLangContent> langContentList = multiLingualService.findContent(lang,"mandatoryregdocs");

//            if(!langContentList.isEmpty())
//            {
//                for(TblMultiLangContent tblMultiLangContent:langContentList)
//                {
//                    if(tblMultiLangContent.getSubTitle().equals("content_mandatoryregdocs")){
//                        if("bn_IN".equals(lang)){
//                           //contentmandregdoc = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
//                        }else{
//                           //contentmandregdoc = new String(tblMultiLangContent.getValue());
//                        }
//                    }
//                }
//            }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                       
                        <td class="contentArea">
<!--                            <div class="pageHead_1">
                                Mandatory Registration Documents
                            </div>-->
                                            <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1 c_t_space" width="100%">
                                                <tr>
                                                    <td colspan="2" class="ff" align="right">&nbsp; </td>
                                                </tr>
                                            </table>
                                             <h2><span style="color:#DAA520;">Bidder / Consultant </span></h2>
                                                <hr/>

                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <th class="t-align-center" width="50%">Mandatory Documents</th>
                                            <th class="t-align-center" width="50%">Optional Documents</th>
                                        </tr>
                                        <tr class="bgColor-white">
                                            <td class="t-align-left"><b>Self Declaration </b><span style="color:red">*</span><i> (For Company Owner)</i></td>
                                            <td class="t-align-left"><b>Company Registration No</b></td>
                                        </tr>
                                        <tr class="bgColor-white">
                                            <td class="t-align-left"><b>Citizenship Identity Card</b> <span style="color:red">*</span><i> (For Company Contact Person)</i></td>
                                            <td class="t-align-left"><b>Tax Payment No. (TPN)</b></td>
                                        </tr>
                                        <tr class="bgColor-white">
                                            <td class="t-align-left"><b>Power Of Attorney</b><span style="color:red">*</span><i> (For Company's Authorized User)</i></td>
                                            <td class="t-align-left"><b>Statutory Certificate No</b></td>
                                        </tr>
                                        <tr class="bgColor-white">
                                            <td class="t-align-left"><b>Trade License</b><span style="color:red">*</span><i> (For Goods, Works and Service)</i></td>
                                            <td class="t-align-left"><b>Others/More</b></td>
                                        </tr>
                                        <tr class="bgColor-white">
                                            <td class="t-align-left"><b>Construction Development Board (CDB) Registration Certificate</b><span style="color:red">*</span><i> (For Works Only)</i></td>
                                            <td class="t-align-left"><b></b></td>
                                        </tr>

                                    </table>

                            
                        </td>
                         <td style="width:266px;">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include></td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
