/****** Object:  StoredProcedure [dbo].[p_get_promisereport]    Script Date: 04/11/2012 14:00:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Shreyansh Shah>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[p_get_promisereport]
	-- Add the parameters for the stored procedure here
	@yearname varchar(15),
	@ptype varchar(20),
	@deptid varchar(6),
	@officeid varchar(8),
	@PEId varchar(10),
	@noofpage varchar(10),
	@records varchar(4),
	@v_Page_inInt int =1,
@v_RecordPerPage_inInt int =10
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from

	declare @column varchar(50)
    declare @fnlQry varchar(max)
    declare @officeQry varchar(max)
	declare @deptQry varchar(max)
	declare @deptType varchar(max)
	declare @ptypeQry varchar(max)
	declare @paginationQry varchar(max)
	declare @v_StartAt_Int int, @v_StopAt_Int int
declare @publishtender varchar(max)
declare @fyearQry varchar(max)


/* START CODE: TO CALCULATE ACTUAL ROW POSITIONS TO FETCH FROM TOTAL RECORDS */
set @v_Page_inInt = CONVERT(INT, @noofpage)
set @v_RecordPerPage_inInt = CONVERT(INT, @records)
Set @v_StartAt_Int = ((@v_Page_inInt-1) * @v_RecordPerPage_inInt) + 1
Set @v_StopAt_Int= (@v_Page_inInt * @v_RecordPerPage_inInt)

	select @deptType = departmentType  from tbl_DepartmentMaster where departmentId = @deptid
	Select @column = Case @deptType
	When 'Ministry' Then 'Ministry_ID'
	When 'Division' Then 'Division_ID'
	When 'Organization' Then 'Org_ID'
	else 'Ministry_Id'
	End
	set @officeQry = ''
	set @deptQry = ''
	set @ptypeQry = ''
	set @fyearQry = ''

	-- if office not selected
	if(@officeid != 0)
	begin
		set @officeQry = ' and pi.officeId ='+@officeid
	end
	-- if departments are not selected

	if(@deptid != 0)
	begin
		set @deptQry = ' where ' + @column + ' = '  + @deptid
	end
	-- If procurement types are not mentioned
	if(@ptype != '-')
	begin
		set @ptypeQry = ' and pi.tenderId in (select tenderId from tbl_TenderDetails where procurementType = ''' + @ptype + ''') '
	end

	-- Check whether user has selected financial year
	if(@yearname != '-')
	begin
		--set @fyearQry = ''
		set @fyearQry = ' inner join (select tenderId, financialYear from tbl_TenderMaster where financialYear like ''' + @yearname + ''') yr on yr.tenderId = pi.tenderId'
	end
	-- variable for pagination
	set @paginationQry = ' ) select * from virtualtable where RowNum BETWEEN ' + CONVERT(varchar(50), @v_StartAt_Int) + ' And ' + CONVERT(varchar(50), @v_StopAt_Int)

	-- Count Total Published Tender, Total Tender
	set @publishtender = 'count(case when PublishNewspaper != 0 or PublishCPTUWebsite != 0  then PublishNewspaper end)';


	if(@PEId = '1')
	Begin

		set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, '+@publishtender+' ) as FieldValue3,
		convert(varchar , count(case when PublishNewspaper=1
		then PublishNewspaper end)) as FieldValue4,
		case when ' + @publishtender  + ' = 0 then ''0'' else
		 convert(varchar,round(cast((cast(count(case when PublishNewspaper=1 then PublishNewspaper end)as float) / cast( '+@publishtender+' as float))  * 100 as float),0)) end as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '
	end

	else if(@PEId = '2')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, '+@publishtender+' ) as FieldValue3,
		convert(varchar , count(case when PublishCPTUWebsite=1
		then PublishCPTUWebsite end)) as FieldValue4,
		case when ' + @publishtender  + ' = 0 then ''0'' else
		 convert(varchar,round(cast((cast(count(case when PublishCPTUWebsite=1 then PublishCPTUWebsite end)as float) / cast( '+@publishtender+' as float))  * 100 as float),0)) end as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End

	-- To Calculate Tender following procurement rule (FieldValue3) either dprule or gobprocrules are 1
	else if(@PEId = '3')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, count(case when GoBProcRules != 0 or DPRules != 0  then GoBProcRules else 0 end)) as FieldValue3,
		convert(varchar , count(case when GoBProcRules=1
		then GoBProcRules end)) as FieldValue4,
		 convert(varchar,round(cast((cast(count(case when GoBProcRules=1 then GoBProcRules end)as float) / cast(count(case when GoBProcRules != 0 or DPRules != 0  then GoBProcRules else 0 end) as float)) * 100 as float), 0 )) as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	-- To Calculate Tender following procurement rule (FieldValue3) either dprule or gobprocrules are 1
	else if(@PEId = '4')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, count(case when GoBProcRules != 0 or DPRules != 0  then GoBProcRules else 0 end)) as FieldValue3,
		convert(varchar , count(case when DPRules=1
		then DPRules end)) as FieldValue4,
		convert(varchar,round(cast((cast(count(case when DPRules=1 then DPRules end)as float) / cast(count(case when GoBProcRules != 0 or DPRules != 0  then GoBProcRules else 0 end) as float)) * 100 as float), 0 )) as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End

	else if(@PEId = '5')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, '+@publishtender+' ) as FieldValue3,
		convert(varchar , count(case when MultiLocSubmission=1
		then MultiLocSubmission end)) as FieldValue4,
		 convert(varchar,round(cast((cast(count(case when MultiLocSubmission=1 then MultiLocSubmission end)as float) / cast('+ @publishtender +' as float)) as float) * 100 ,2)) as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		+ @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '6')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
	convert(varchar, '+@publishtender+' ) as FieldValue3,
		convert(varchar,SUM(convert(INT,DaysBetTenPubTenderClosing))) as FieldValue4,
		case when SUM(convert(float,DaysBetTenPubTenderClosing)) = 0 or '+@publishtender + ' = 0  then ''0.00'' else
		 convert(varchar,cast(round(cast((cast(SUM(convert(INT,DaysBetTenPubTenderClosing))as float) / cast( '+@publishtender+' as float))  as float),2) as decimal(20,2))) end as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '7')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when SuffTenderSubTime=1
		then SuffTenderSubTime end)) as FieldValue4,
		case when count(case when SuffTenderSubTime=1 then SuffTenderSubTime end) = 0 or '+@publishtender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when SuffTenderSubTime=1 then SuffTenderSubTime end)as float) / cast(' + @publishTender + ' as float)) as float) * 100 ,0)) end as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '8')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , sum(convert(INT,TenderDocSold))) as FieldValue4,
		 case when sum(convert(INT,TenderDocSold)) = 0 or '+ @publishtender +' = 0  then ''0.00'' else convert(varchar ,cast(round(sum(convert(INT,TenderDocSold)) / cast(' + @publishTender + ' as float),2) as decimal(20,2)))  end as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '9')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , sum(convert(INT,TenderersPartCount))) as FieldValue4,
		 case when sum(convert(INT,TenderersPartCount)) = 0 or '+@publishtender +' = 0 then ''0.00'' else convert(varchar , cast(round(sum(convert(INT,TenderersPartCount)) / cast(' + @publishTender + ' as float),2) as decimal(20,2)))  end as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '10')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar,SUM(Convert(INT,SUBSTRING(TenderSubVSDocSold, CHARINDEX('':'', TenderSubVSDocSold) + 1, LEN(TenderSubVSDocSold))))) AS FieldValue3,
	convert(varchar,SUM(Convert(INT,SUBSTRING(TenderSubVSDocSold, 1, CHARINDEX('':'', TenderSubVSDocSold) - 1)))) AS FieldValue4,
		 CAST(SUM(Convert(INT,SUBSTRING(TenderSubVSDocSold, 1, CHARINDEX('':'', TenderSubVSDocSold) - 1))) as varchar(10))+'':''+CAST(SUM(Convert(INT,SUBSTRING(TenderSubVSDocSold, CHARINDEX('':'', TenderSubVSDocSold) + 1, LEN(TenderSubVSDocSold)))) as varchar(10) ) as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '


	End
	else if(@PEId = '11')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when TOCMemberFromTEC=1
		then TOCMemberFromTEC end)) as FieldValue4,
		case when count(case when TOCMemberFromTEC=1 then TOCMemberFromTEC end) = 0 or '+@publishtender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when TOCMemberFromTEC=1 then TOCMemberFromTEC end)as float) / cast(' + @publishTender + ' as float)) as float) * 100 ,0)) end as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '12')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when TECFormedByAA=1
		then TECFormedByAA end)) as FieldValue4,
		case when count(case when TECFormedByAA=1 then TECFormedByAA end) = 0 or '+@publishtender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when TECFormedByAA=1 then TECFormedByAA end)as float) / cast(' + @publishTender + ' as float)) as float) * 100,0)) end as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '13')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when TECExternalMembers=1
		then TECExternalMembers end)) as FieldValue4,
		case when count(case when TECExternalMembers=1 then TECExternalMembers end) = 0 or '+@publishtender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when TECExternalMembers=1 then TECExternalMembers end)as float) / cast(' + @publishTender + ' as float)) as float) * 100 ,0)) end as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '14')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
				convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , isnull(sum(case when EvaluationTime != ''NA'' and EvaluationTime != 0 then convert(INT,EvaluationTime) end),0)) as FieldValue4,
		case when SUM(case when EvaluationTime != ''NA'' and EvaluationTime != ''0'' then convert(INT,EvaluationTime) else ''0'' end) = 0 or '+@publishtender + ' = 0  then ''0.00'' else
		convert(varchar ,cast(round(cast(cast(SUM(case when EvaluationTime != ''NA'' and EvaluationTime != ''0'' then convert(INT,EvaluationTime)  end) as float)/ cast(' + @publishTender + ' as float) as float),2) as decimal(20,2))) end as FieldValue5,
		ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '15')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when EvalCompletedInTime=1
		then EvalCompletedInTime end)) as FieldValue4,
		case when count(case when EvalCompletedInTime=1 then EvalCompletedInTime end) = 0 or '+@publishtender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when EvalCompletedInTime=1 then EvalCompletedInTime end)as float) / cast(count(EvalCompletedInTime) as float)) as float) * 100,0)) end as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	-- Need Clarifcation
	else if(@PEId = '16')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
				convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , sum(convert(INT,TenderersPartCount))) as FieldValue4,
		convert(varchar ,SUM(case when ResponsiveTenderersCount != ''NA'' then convert(INT,ResponsiveTenderersCount) else 0 end)) as FieldValue5,
		convert(varchar,cast(round(cast((cast(sum(convert(INT,TenderersPartCount)) as float) / cast(' + @publishTender + ' as float)) as float) ,2) as decimal(20,2)))  as FieldValue6,
		convert(varchar,cast(round(cast((cast(SUM(case when ResponsiveTenderersCount != ''NA'' then convert(INT,ResponsiveTenderersCount) else 0 end) as float) / cast(' + @publishTender + ' as float)) as float) ,2)as decimal(20,2)))  as FieldValue7,
		ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '17')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when IsReTendered=1
		then IsReTendered end)) as FieldValue4,
		case when count(case when IsReTendered=1 then IsReTendered end) = 0 or '+@publishtender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when IsReTendered=1 then IsReTendered end)as float) / cast(' + @publishTender + ' as float)) as float) * 100 ,0)) end as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '18')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when IsTenderCancelled=1
		then IsTenderCancelled end)) as FieldValue4,
		case when count(case when IsTenderCancelled=1 then IsTenderCancelled end) = 0 or '+@publishtender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when IsTenderCancelled=1 then IsTenderCancelled end)as float) / cast(' + @publishTender + ' as float)) as float) * 100 ,0)) end as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '19')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
				convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when TenderersPartCount != ''NA'' and TenderersPartCount != ''0'' then TenderersPartCount  end)) as FieldValue4,
		convert(varchar ,SUM(case when DaysBtwnTenEvalTenApp != ''NA'' then convert(INT,DaysBtwnTenEvalTenApp) else 0 end)) as FieldValue5,
		convert(varchar ,case when count(case when TenderersPartCount != ''NA'' and TenderersPartCount != ''0'' then TenderersPartCount  end) = 0 then ''0'' else convert(varchar , isnull(sum(convert(INT,case when DaysBtwnTenEvalTenApp != ''NA'' then convert(INT,DaysBtwnTenEvalTenApp) end)),0) /  cast(count(case when TenderersPartCount != ''NA'' and TenderersPartCount != ''0'' then TenderersPartCount  end) as float))  end) as FieldValue6,
		ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '20')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when IsApprovedByProperAA=1
		then IsApprovedByProperAA end)) as FieldValue4,
		case when count(case when IsApprovedByProperAA=1 then IsApprovedByProperAA end) = 0 or '+@publishtender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when IsApprovedByProperAA=1 then IsApprovedByProperAA end)as float) / cast(' + @publishTender + ' as float)) as float) * 100 ,0)) end as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '21')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, count(SubofEvalReportAA)) as FieldValue3,
		convert(varchar , count(case when SubofEvalReportAA=1
		then SubofEvalReportAA end)) as FieldValue4,
		case when count(case when SubofEvalReportAA=1 then SubofEvalReportAA end) = 0 or '+@publishtender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when SubofEvalReportAA=1 then SubofEvalReportAA end)as float) / cast(count(SubofEvalReportAA) as float)) as float) * 100 ,0)) end as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '22')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when AppofContractByAAInTimr=1
		then AppofContractByAAInTimr end)) as FieldValue4,
		case when count(case when AppofContractByAAInTimr=1 then AppofContractByAAInTimr end) = 0 or '+@publishtender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when AppofContractByAAInTimr=1 then AppofContractByAAInTimr end)as float) / cast(' + @publishTender + ' as float)) as float) * 100 ,0)) end as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '23')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, count(SubofEvalReportAA)) as FieldValue3,
		convert(varchar , count(case when TERReviewByOther=1
		then TERReviewByOther end)) as FieldValue4,
		case when count(case when TERReviewByOther=1 then TERReviewByOther end) = 0 or '+@publishtender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when TERReviewByOther=1 then TERReviewByOther end)as float) / cast(count(SubofEvalReportAA) as float)) as float) * 100 ,0)) end as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '24')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when ContractAppByHighAuth=1
		then ContractAppByHighAuth end)) as FieldValue4,
		case when count(case when ContractAppByHighAuth=1 then ContractAppByHighAuth end) = 0 or '+@publishtender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when ContractAppByHighAuth=1 then ContractAppByHighAuth end)as float) / cast(' + @publishTender + ' as float)) as float) * 100 ,0)) end as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '25')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(IsApprovedByProperAA)) as FieldValue4,
		convert(varchar ,SUM(case when DaysBtwnContappNOA != ''NA'' then convert(INT,DaysBtwnContappNOA) else 0 end)) as FieldValue5,
		convert(varchar ,case when sum(convert(INT,case when DaysBtwnContappNOA != ''NA'' then convert(INT,DaysBtwnContappNOA) end)) = 0
		or count(case when DaysBtwnContappNOA != ''NA'' then DaysBtwnContappNOA  end) = 0
		then ''0.00'' else convert(varchar , cast(round(sum(convert(INT,case when DaysBtwnContappNOA != ''NA'' then convert(INT,DaysBtwnContappNOA) else 0 end)) / cast(count(IsApprovedByProperAA) as float),2) as decimal(20,2))) end) as FieldValue6,
		ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '26')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
				convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when DaysBtwnContappNOA != ''NA'' and DaysBtwnContappNOA != ''0'' then DaysBtwnTenOpenNOA  end)) as FieldValue4,
		convert(varchar ,SUM(case when DaysBtwnTenOpenNOA != ''NA'' then convert(INT,DaysBtwnTenOpenNOA) else 0 end)) as FieldValue5,
		convert(varchar ,case when count(case when DaysBtwnContappNOA != ''NA'' and DaysBtwnContappNOA != ''0'' then DaysBtwnContappNOA  end) = 0
		or SUM(case when DaysBtwnTenOpenNOA != ''NA'' then convert(INT,DaysBtwnTenOpenNOA) end) = 0
		then ''0.00'' else convert(varchar ,cast(round(sum(convert(INT,case when DaysBtwnTenOpenNOA != ''NA'' then convert(INT,DaysBtwnTenOpenNOA) end)) / cast(count(case when DaysBtwnContappNOA != ''NA'' and DaysBtwnContappNOA != ''0'' then DaysBtwnContappNOA  end) as float),2) as decimal(20,2)))  end) as FieldValue6,
		ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '27')
	Begin
			set @fnlQry =

		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when DaysBtwnContappNOA != ''NA'' and DaysBtwnContappNOA != ''0'' then DaysBtwnTenOpenNOA  end)) as FieldValue4,
		convert(varchar ,SUM(case when DaysBtwnIFTNOA != ''NA'' then convert(INT,DaysBtwnIFTNOA) else 0 end)) as FieldValue5,
		convert(varchar ,case when sum(convert(INT,case when DaysBtwnIFTNOA != ''NA'' then convert(INT,DaysBtwnIFTNOA) else 0 end)) = 0
		or count(case when DaysBtwnContappNOA != ''NA'' and DaysBtwnContappNOA != ''0'' then DaysBtwnContappNOA  end) = 0
		then ''0.00'' else convert(varchar , cast(round(sum(convert(INT,case when DaysBtwnIFTNOA != ''NA'' then convert(INT,DaysBtwnIFTNOA) end)) / cast(count(case when DaysBtwnContappNOA != ''NA'' and DaysBtwnContappNOA != ''0'' then DaysBtwnContappNOA  end) as float),2) as decimal(20,2)))  end) as FieldValue6,
		ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '28')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, count(AwardPubCPTUWebsite)) as FieldValue3,
		convert(varchar , count(case when AwardPubCPTUWebsite=1
		then AwardPubCPTUWebsite end)) as FieldValue4,
		case when count(case when AwardPubCPTUWebsite=1 then AwardPubCPTUWebsite end) = 0 or count(AwardPubCPTUWebsite) = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when AwardPubCPTUWebsite=1 then AwardPubCPTUWebsite end)as float) / cast(count(AwardPubCPTUWebsite) as float)) as float) * 100,0)) end as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '29')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, count(AwardPubCPTUWebsite)) as FieldValue3,
		convert(varchar , count(case when AwardedInTenValidityPeriod=1
		then AwardedInTenValidityPeriod end)) as FieldValue4,
		case when count(case when AwardedInTenValidityPeriod=1 then AwardedInTenValidityPeriod end) = 0 or count(AwardPubCPTUWebsite) = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when AwardedInTenValidityPeriod=1 then AwardedInTenValidityPeriod end)as float) / cast(count(AwardPubCPTUWebsite) as float)) as float) * 100 ,0)) end as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '30')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when DeliveredInTime=1
		then DeliveredInTime end)) as FieldValue4,
		case when count(case when DeliveredInTime=1 then DeliveredInTime end) = 0 or ' + @publishTender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when DeliveredInTime=1 then DeliveredInTime end)as float) / cast(' + @publishTender + ' as float)) as float) * 100 ,0)) end as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '31')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when LiquidatedDamage=1
		then LiquidatedDamage end)) as FieldValue4,
		case when count(case when LiquidatedDamage=1 then LiquidatedDamage end) = 0 or ' + @publishTender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when LiquidatedDamage=1 then LiquidatedDamage end)as float) / cast(' + @publishTender + ' as float)) as float) * 100 ,0)) end as FieldValue5,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '32')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, count(AwardPubCPTUWebsite)) as FieldValue3,
		convert(varchar , count(case when FullyCompleted=1
		then FullyCompleted end)) as FieldValue4,
		case when count(case when FullyCompleted=1 then FullyCompleted end) = 0 or count(AwardPubCPTUWebsite) = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when FullyCompleted=1 then FullyCompleted end)as float) / cast(count(AwardPubCPTUWebsite) as float)) as float) * 100 ,0)) end as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '33')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar , count(case when DaysReleasePayment != ''NA'' AND DaysReleasePayment!= ''0''
		then DaysReleasePayment end)) as FieldValue3,
		 convert(varchar,cast(AVG(cast(case when DaysReleasePayment != ''NA'' AND DaysReleasePayment!= ''0''
		then round(DaysReleasePayment,2)  else 0 end as int)) as decimal(20,2))) as FieldValue4  ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '34')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar , count(case when DaysReleasePayment != ''NA'' AND DaysReleasePayment!= ''0''
		then DaysReleasePayment end)) as FieldValue3,
		convert(varchar , count(case when LatePayment=1
		then LatePayment end)) as FieldValue4,
		case when count(case when LatePayment=1 then LatePayment end) = 0 or count(AwardPubCPTUWebsite) = 0  or count(case when DaysReleasePayment != ''NA'' AND DaysReleasePayment!= ''0'' then DaysReleasePayment end) = 0 then ''0'' else
		 convert(varchar,round(cast((cast(count(case when LatePayment=1 then LatePayment end)as float) / cast(count(case when DaysReleasePayment != ''NA'' AND DaysReleasePayment!= ''0''
		then DaysReleasePayment end) as float)) as float) * 100 ,0)) end as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '35')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, count(case when DaysReleasePayment != ''NA'' AND DaysReleasePayment!= ''0''
		then DaysReleasePayment end)) as FieldValue3,
		convert(varchar , count(case when InterestPaid=1
		then InterestPaid end)) as FieldValue4,
		case when count(case when InterestPaid=1 then InterestPaid end) = 0 or count(AwardPubCPTUWebsite) = 0  or count(case when DaysReleasePayment != ''NA'' AND DaysReleasePayment!= ''0'' then DaysReleasePayment end) = 0 then ''0'' else
		 convert(varchar,round(cast((cast(count(case when InterestPaid=1 then InterestPaid end)as float) / cast(count(case when DaysReleasePayment != ''NA'' AND DaysReleasePayment!= ''0''
		then DaysReleasePayment end) as float)) as float) * 100,0)) end as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '36')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishTender + ') as FieldValue3,
		convert(varchar , count(case when ComplaintReceived=1
		then ComplaintReceived end)) as FieldValue4,
		case when count(case when ComplaintReceived=1 then ComplaintReceived end) = 0 or ' + @publishTender + ' = 0  then ''0'' else
		 convert(varchar,round(cast((cast(count(case when ComplaintReceived=1 then ComplaintReceived end)as float) / cast(' + @publishTender + ' as float)) as float) * 100 ,0)) end as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '37')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, count(case when ComplaintReceived=1 then ComplaintReceived end)) as FieldValue3,
		convert(varchar , count(case when ComplaintResolved=1
		then ComplaintResolved end)) as FieldValue4,
		convert(varchar , count(case when ResolAwardModification=1
		then ResolAwardModification end)) as FieldValue5,
		case when count(case when ResolAwardModification=1 then ResolAwardModification end) = 0 or count(case when ComplaintResolved=1 then ComplaintResolved end) = 0  then ''0.00'' else
		 convert(varchar,cast(round(cast((cast(count(case when ResolAwardModification=1 then ResolAwardModification end)as float) / cast(count(case when ComplaintResolved=1
		then ComplaintResolved end) as float)) as float) * 100 ,2) as decimal(20,2))) end as FieldValue6 ,
		ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '38')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, count(case when ComplaintReceived=1 then ComplaintReceived end)) as FieldValue3,
		convert(varchar , count(case when ComplaintResolved=1 then ComplaintResolved end)) as FieldValue4,
		case when count(case when ComplaintReceived=1 then ComplaintReceived end) = 0 or count(case when ComplaintResolved=1 then ComplaintResolved end) = 0 then ''0'' else
		 convert(varchar,round(cast((cast(count(case when ComplaintResolved=1 then ComplaintResolved end)as float) / cast(count(case when ComplaintReceived=1 then ComplaintReceived end) as float)) as float) * 100 ,0)) end as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '39')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, count(case when ComplaintReceived=1 then ComplaintReceived end)) as FieldValue3,
		convert(varchar , count(case when ComplaintResolved=1 then ComplaintResolved end)) as FieldValue4,
		convert(varchar , count(case when RevPanelDecUpheld=1 then RevPanelDecUpheld end)) as FieldValue5,
		case when count(case when ComplaintReceived=1 then ComplaintReceived end) = 0 then ''0.00'' else
		 convert(varchar,cast(round(cast((cast(count(case when RevPanelDecUpheld=1 then RevPanelDecUpheld end)as float) / cast(count(case when ComplaintResolved=1 then ComplaintResolved end) as float)) as float) * 100 ,2) as decimal(20,2))) end as FieldValue6 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '40')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar, ' + @publishtender + ') as FieldValue3,
		convert(varchar , count(case when AwardPubCPTUWebsite = 1 then AwardPubCPTUWebsite end)) as FieldValue4,
		convert(varchar , count(case when IsContAmended=1 then IsContAmended end)) as FieldValue5,
		case when count(case when AwardPubCPTUWebsite=1 then AwardPubCPTUWebsite end) = 0 then ''0.00'' else
		 convert(varchar,cast(round(cast((cast(count(case when IsContAmended=1 then IsContAmended end)as float) / cast(count(case when AwardPubCPTUWebsite=1 then AwardPubCPTUWebsite end) as float)) as float) * 100 ,2) as decimal(20,2))) end as FieldValue6 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '41')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar , count(case when AwardPubCPTUWebsite = 1 then AwardPubCPTUWebsite end)) as FieldValue3,
		convert(varchar , count(case when ContUnresolvDispute=1 then ContUnresolvDispute end)) as FieldValue4,
		case when count(case when ContUnresolvDispute =1 then ContUnresolvDispute end) = 0 or count(case when AwardPubCPTUWebsite=1 then AwardPubCPTUWebsite end) = 0 then ''0'' else
		 convert(varchar,round(cast((cast(count(case when ContUnresolvDispute =1 then ContUnresolvDispute  end)as float) / cast(count(case when AwardPubCPTUWebsite=1 then AwardPubCPTUWebsite end) as float)) as float) * 100 ,0)) end as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
	else if(@PEId = '42')
	Begin
			set @fnlQry =
		'select convert(varchar, '+@column+') as FieldValue1,
		di.departmentName as FieldValue2,
		convert(varchar , '+ @publishtender +') as FieldValue3,
		convert(varchar , count(case when DetFraudCorruption=1 then DetFraudCorruption end)) as FieldValue4,
		case when count(case when DetFraudCorruption =1 then DetFraudCorruption end) = 0  or '+ @publishtender +' = 0 then ''0'' else
		 convert(varchar,round(cast((cast(count(case when DetFraudCorruption =1 then DetFraudCorruption  end)as float) / cast('+ @publishtender +' as float)) as float) * 100 ,0)) end as FieldValue5 ,
		 ROW_NUMBER() OVER (ORDER BY departmentName DESC) AS RowNum
		 from Tbl_PromisIndicator pi
		 inner join tbl_DepartmentMaster di on pi.'+@column+' = di.departmentId'
		 + @deptQry + @officeQry + @ptypeQry + @fyearQry +
		 ' group by '+@column+',di.departmentName '

	End
			if(@yearname != '-')
		begin
			set @fnlQry = @fnlQry + ',yr.financialYear '
		end
	set @fnlQry = 'with virtualtable as ( ' + @fnlQry + @paginationQry
		 print (@fnlQry)
		 exec (@fnlQry)
END
