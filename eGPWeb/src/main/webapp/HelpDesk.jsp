<%--
    Document   : Help Desk
    Created on : 20th Jan 2010
    Author     : yanki
--%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>GPPMD Office </title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
    </head>
    <body>
        <%
                    String lang = null, contenthelpdesk = null;

                    if (request.getParameter("lang") != null && request.getParameter("lang") != "") {
                        lang = request.getParameter("lang");
                    } else {
                        lang = "en_US";
                    }

                    MultiLingualService multiLingualService = (MultiLingualService) AppContext.getSpringBean("MultiLingualService");
                    List<TblMultiLangContent> langContentList = multiLingualService.findContent(lang, "helpdesk");

                    if (!langContentList.isEmpty()) {
                        for (TblMultiLangContent tblMultiLangContent : langContentList) {
                            if (tblMultiLangContent.getSubTitle().equals("content_helpdesk")) {
                                if ("bn_IN".equals(lang)) {
                                    contenthelpdesk = BanglaNameUtils.getUTFString(tblMultiLangContent.getValue());
                                } else {
                                    contenthelpdesk = new String(tblMultiLangContent.getValue());
                                }
                            }
                        }
                    }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td width="266">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include></td>
                        <td class="contentArea-Blogin">
<!--                            <div class="pageHead_1">
                                Help Desk
                            </div>-->

                             <%=contenthelpdesk%>

                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>