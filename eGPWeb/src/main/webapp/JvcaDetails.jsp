<%--
    Document   : JvcaDetails
    Created on : Oct 21, 2010, 6:13:00 PM
    Author     : taher
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
    <%@page import="com.cptu.egp.eps.web.databean.JvcaCompListDtBean"%>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP New User Registration : JVCA Details</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />        
        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="resources/js/jQuery/jquery.validate.js"></script>
        <!--<script type="text/javascript" src="resources/js/jQuery/jquery.regexpCommon-0.3.0.js"></script>-->
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            $(function() {
                $('#btnSearch').click(function() {

                    $('#dis').css("display", "none");
                    if($('#cmbSearchBy').val()=='emailId'){

                        txt = $('#txtSearchVal').val();
                        if($('#txtSearchVal').val()==""){
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('Please enter e-mail ID.')
                            return false;
                        }

                        var SearchByval=$('#cmbSearchBy').val();
                        var val = EmailCheck(txt)

                        if (val == true) {
                            $('#SearchValError').html('')
                            $('#dis').css("display", "none");
                            //return true;
                        }
                        else {
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('<div class="reqF_1"> Please enter valid e-mail ID.</div>')
                            return false;
                        }
                    }

                    else if($('#cmbSearchBy').val()=='companyName'){

                        if($('#txtSearchVal').val()==""){
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('Please enter Company Name.')
                            return false;
                        }
                    }

                    else if($('#cmbSearchBy').val()=='companyRegNumber'){
                        if($('#txtSearchVal').val()==""){
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('Please enter Registration no.')
                            return false;
                        }
                    }

                    $.post("<%=request.getContextPath()%>/CommonServlet", {searchBy:$('#cmbSearchBy').val(),searchVal:$('#txtSearchVal').val(),funName:'companySearch'},  function(j){
                        if(j.toString().indexOf("<", 0)!=-1){
                            $('span.#company').html(null);
                            $('select.#cmbCompany').html(j);
                        }else{
                            $('span.#company').html(j);
                            $('select.#cmbCompany').html(null);
                        }
                    });
                });
            });

            function EmailCheck(value) {
                //alert('in');
                //alert(value);
                var c = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);
                //alert(c);
                return c;

            }

            $(function() {
                $('#frmJvcaDetails').submit(function() {                    
                    if($('#cmbCompany').val()==null){
                        $('#company').html("Please select company");
                        return false;
                    }else{
                         $('#company').html(null);                        
                    }
                    if($('#frmJvcaDetails').valid()){
                        $('#btnAdd').attr("disabled","true");
                        $('#hdnbtnAdd').val("Add Company");
                    }
                });
            });            
        </script>
    </head>        
    <body>
    <%
            tendererMasterSrBean.setLogUserId(session.getAttribute("userId").toString());
            boolean load = true;
            int leadCnt = 0;
            int otherCnt = 0;
            java.util.List<JvcaCompListDtBean> compListDtBeans = new java.util.ArrayList<JvcaCompListDtBean>();
            compListDtBeans = tendererMasterSrBean.getCompanyJointVentures(Integer.parseInt(request.getSession().getAttribute("userId").toString()));
            if (request.getParameter("vId") != null) {
                tendererMasterSrBean.delteJvcaDetail(Integer.parseInt(request.getParameter("vId")));
                load = false;
                response.sendRedirect("JvcaDetails.jsp?del=y");
            }
            if (("Previous".equals(request.getParameter("btnPrev"))) || ("Next".equals(request.getParameter("btnNext")))) {
                for (JvcaCompListDtBean jvcaCompListDtBean : compListDtBeans) {
                    if (jvcaCompListDtBean.getJvcaRole().equals("lead")) {
                        leadCnt++;
                    } else {
                        otherCnt++;
                    }
                }
                if (!(leadCnt == 1 && otherCnt >= 1)) {
                    load = true;
            %>
        <%--<div class="responseMsg errorMsg">To Form a JVCA at least 1 Lead and Secondary partner should be available</div>--%>
            <%
                } else {
                    tendererMasterSrBean.updateNextScreenDetail(Integer.parseInt(request.getSession().getAttribute("userId").toString()), "SupportingDocuments");
                    load = false;
                    response.sendRedirect("SupportingDocuments.jsp");
                }
            }
    %>

    <%
        if(load){
    %>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>                
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <!--Page Content Start-->
                        <td class="contentArea_1"><div class="pageHead_1">New User Registration - JVCA Details</div>
                            <jsp:include page="EditNavigation.jsp" ></jsp:include>
                            <%                            
                            if ("Add Company".equals(request.getParameter("hdnbtnAdd"))) {
                                String msg = tendererMasterSrBean.addJvcaDetail(request.getParameter("jvcaRole"), Integer.parseInt(request.getParameter("company")), Integer.parseInt(request.getSession().getAttribute("userId").toString()));//seesionid
                                char tmp;
                                if (msg.contains("0")) {
                                    tmp=msg.substring(0, msg.length() - 1).charAt(0);
                            
                                } else {
                                    tmp=msg.substring(0, msg.length() - 1).charAt(0);
                                 }
                                response.sendRedirect("JvcaDetails.jsp?msg="+tmp);
                            }
                            if (request.getParameter("del") != null) {
                            %>
                            <br/><div class="responseMsg successMsg">Company removed successfully</div>
                            <%}%>
                            <%
                                if("L".equals(request.getParameter("msg"))){
                                    out.print("<br/><div class=\"responseMsg errorMsg\">Lead partner already exists</div>");
                                }
                                if("C".equals(request.getParameter("msg"))){
                                    out.print("<br/><div class=\"responseMsg successMsg\">Company added successfully</div>");
                                }
                                if("T".equals(request.getParameter("msg"))){
                                    out.print("<br/><div class=\"responseMsg errorMsg\">This company already exists</div>");
                                }
                            %>
                            <table border="0" cellspacing="0" cellpadding="0" class="formStyle_1 t_space" width="100%">
                                <tr>
                                    <td style="font-style: italic" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                </tr>
                            </table>
                            <div  class="formBg_1 t_space">
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td class="ff" width="10%">Search by   : <span>*</span></td>
                                    <td width="90%"><select name="select4" class="formTxtBox_1" id="cmbSearchBy" style="width: 200px;">
                                            <option value="companyRegNumber">Registration No</option>
                                            <option value="companyName">Company Name</option>
                                            <option value="emailId" selected>e-mail ID</option>
                                        </select>&nbsp;
                                        <input name="textfield5" type="text" class="formTxtBox_1" id="txtSearchVal" style="width:200px;"/>&nbsp;
                                        <label class="formBtn_1">
                                            <input type="button" name="btnSearch" id="btnSearch" value="Search" />
                                        </label>
                                    </td>                                   
                                </tr>
                                <tr id="dis" style="display: none;">
                                    <td width="10%">&nbsp;</td>
                                    <td class="t-align-left"><span id="SearchValError" class="reqF_1"></span></td>
                                </tr>
                            </table>
                                </div>
                            <form id="frmJvcaDetails" method="post" action="JvcaDetails.jsp">
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td class="ff" width="10%">Company Name : </td>
                                        <td width="90%">
                                            <select name="company" class="formTxtBox_1" id="cmbCompany">
                                            </select>
                                            <span id="company" style="color: red;">&nbsp;</span>
                                            <span id="comptag" style="color: red;">&nbsp;</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">JVCA Role : <span>*</span></td>
                                        <td><select name="jvcaRole" class="formTxtBox_1" id="cmbJVCARole" style="width: 80px">
                                                <option value="lead">Lead</option>
                                                <option value="other">Secondary</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <label class="formBtn_1">
                                                <input type="submit" name="btnAdd" id="btnAdd" value="Add Company" />
                                                <input type="hidden" name="hdnbtnAdd" id="hdnbtnAdd" value="" />
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </form>                            
                            <%
                                if (("Previous".equals(request.getParameter("btnPrev"))) || ("Next".equals(request.getParameter("btnNext")))) {
                                    if (!(leadCnt == 1 && otherCnt >= 1)) {
                            %>
                                <div class="responseMsg errorMsg">To Form a JVCA at least 1 Lead and Secondary partner should be available</div>
                            <%
                                    }
                                }
                            %>
                            <form action="JvcaDetails.jsp" method="post">
                                <table width="100%" align="center" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <th style="width:4%;">Sl. No.</th>
                                        <th>Company Name</th>
                                        <th>Company Registration No</th>
                                        <th>JVCA Role</th>
                                        <th style="text-align:center; width:10%;">Remove</th>
                                    </tr>
                                    <%
                                                int count = 1;
                                                for (JvcaCompListDtBean jvcaCompListDtBean : compListDtBeans) {
                                    %>
                                    <tr>
                                        <td><%=count%></td>

                                        <td><%=jvcaCompListDtBean.getCompanyName()%></td>
                                        <td><%=jvcaCompListDtBean.getCompanyRegNumber()%></td>
                                        <td><%if(jvcaCompListDtBean.getJvcaRole().equalsIgnoreCase("lead")){out.print("Lead");}else{out.print("Secondary");}%></td>
                                        <td style="text-align:center;">
                                            <a href="JvcaDetails.jsp?vId=<%=jvcaCompListDtBean.getVentureId()%>" title="Delete" name="delJvca"><img src="resources/images/Dashboard/delIcn.png" alt="Delete" /></a>
                                        </td>
                                    </tr>
                                    <%
                                                    count++;
                                                }
                                    %>
                                </table>
                                <%if (count != 1) {%>
                                <table align="center" border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>                                        
                                        <td align="center">
                                            <label class="formBtn_1">
                                                <input type="submit" name="btnNext" id="btnNext" value="Next" />
                                            </label></td>
                                    </tr>
                                </table>
                                <%}%>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <%
        }
        %>
    </body>
</html>
