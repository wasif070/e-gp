<%-- 
    Document   : eGpGuidelines
    Created on : Jun 8, 2017, 1:32:33 PM
    Author     : 
--%>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Guidelines</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/simpletreemenu.js"></script>
        <style>
            div.gallery {
                margin: 5px;
                border: 1px solid #ccc;
                float: left;
                width: 180px;
            }

            div.gallery:hover {
                border: 1px solid #777;
                background-color: lightyellow;
            }

            div.gallery img {
                width: 100%;
                height: auto;
            }

            div.desc {
                padding: 15px;
                text-align: center;
            }

            #homePage{
                    text-decoration:none;
                    color:#FFF;
                    padding-bottom:10px;
            }
    </style>
    </head>
    <body>
        <div class="mainDiv" style="display:block;" id="listDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->  
                <table class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                       
                        <td class="contentArea-Blogin"><!--Page Content Start-->   
                         
                            <br><br>
                            <div class="pageHead_1" style="padding-left:0%;">e-GP Guidelines
                                <span style="float:right;"><a href="eLearning.jsp" class="action-button-goback">Go Back To e-Learning</a></span>
                            </div>
                            <br>
                            <span id="lTable">
                            <table   style="padding-left: 10%;width: 100%;">
                                <tr>
                                    <td align="center" style="padding-left: 38%;">
                                    <div class="gallery" style="#">
                                        <a href="javascript:void();" onclick="downloadFile('e-GP Guidelines.pdf')">
                                            <img src="/manuals/poster/Guidelines.png" alt="e-GP Guidelines" width="300" height="200" style="border:1px solid #021a40;">
                                          <div class="desc"><b>e-GP Guidelines</div>
                                        </a>
                                      </div>
                                    </td>
                                </tr>
                            </table>
                            </span>
                                
                                
                        </td>
                    </tr>
                </table>
                
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
           
        
            


    </body>
</html>
<form id="form2" method="post">
</form>
<script type="text/javascript">

function downloadFile(fileName){
        document.getElementById("form2").action="<%=request.getContextPath()%>/DocumentBriefcaseSrBean?work=downloadManuals&fileName="+fileName;
        document.getElementById("form2").submit();
    }

</script>