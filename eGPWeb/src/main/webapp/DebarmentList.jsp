<%-- 
    Document   : DebarmentList
    Created on : Jan 11, 2011, 12:05:20 PM
    Author     : Chalapathi.Bavisetti
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Debarment List</title>
             <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
         <link type="text/css" rel="stylesheet" href="resources/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="resources/css/border-radius.css" />
        <script type="text/javascript" src="resources/js/jscal2.js"></script>
        <script type="text/javascript" src="resources/js/lang/en.js"></script>
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
         <script type="text/javascript">
            $(function() {
                $('#btnSearch').click(function() {
                    $("#pageNo").val("1");
                   
                    $.post("<%=request.getContextPath()%>/CommonSearchServiceSrBean", {funName: "GetDebaredData",cmpName: $("#cmpName").val(),dstName: $("#dstName").val(),debfrm: $("#debfrm").val(),debto: $("#debto").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                        $('#resultTable').find("tr:gt(0)").remove();
                        $('#resultTable tr:last').after(j);
                        $("#pageTot").html($("#totalPages").val());
                    });
                });
        });
        </script>

         <script type="text/javascript">
            $(function() {
                $('#btnReset').click(function() {
                    $("#pageNo").val("1");
                    $("#cmpName").val('')
                   $("#dstName").val('')
                   $("#debfrm").val('')
                   $("#debto").val('')
                    $.post("<%=request.getContextPath()%>/CommonSearchServiceSrBean", {funName: "GetDebaredData",cmpName: '',dstName: '',debfrm: '',debto:'',pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                        $('#resultTable').find("tr:gt(0)").remove();
                        $('#resultTable tr:last').after(j);
                        $("#pageTot").html($("#totalPages").val());
                    });
                });
        });
        </script>
        <script type="text/javascript">
         function loadDebarList()
        {
            
            $.post("<%=request.getContextPath()%>/CommonSearchServiceSrBean", {funName: "GetDebaredData",cmpName: $("#cmpName").val(),dstName: $("#dstName").val(),debfrm: $("#debfrm").val(),debto: $("#debto").val(),pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);
                $("#pageTot").html($("#totalPages").val());
            });
            
        }
        </script>
         <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadDebarList();
                        $('#dispPage').val("1");
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        loadDebarList();
                        $('#dispPage').val(totalPages);
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                    }
                });
            });
        </script>
        <script type="text/javascript">
    $(function() {
        $('#btnNext').click(function() {
            var pageNo=parseInt($('#pageNo').val(),10);
            var totalPages=parseInt($('#totalPages').val(),10);

            if(pageNo <= totalPages) {
                $('#pageNo').val(Number(pageNo)+1);
                    loadDebarList();
                $('#dispPage').val(Number(pageNo)+1);
                $('#btnPrevious').removeAttr("disabled");
            }
        });
    });

        </script>
        <script type="text/javascript">
    $(function() {
        $('#btnPrevious').click(function() {
            var pageNo=$('#pageNo').val();

            if(parseInt(pageNo, 10) > 1)
            {
                $('#pageNo').val(Number(pageNo) - 1);
                loadDebarList();
                $('#dispPage').val(Number(pageNo) - 1);
                if(parseInt($('#pageNo').val(), 10) == 1)
                    $('#btnPrevious').attr("disabled", "true")
            }
        });
    });
        </script>
        <script type="text/javascript">
    $(function() {
        $('#btnGoto').click(function() {
            var pageNo=parseInt($('#dispPage').val(),10);
            var totalPages=parseInt($('#totalPages').val(),10);
            if(pageNo > 0)
            {
                if(pageNo <= totalPages) {
                    $('#pageNo').val(Number(pageNo));
                    loadDebarList();
                    $('#dispPage').val(Number(pageNo));
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                    if(parseInt($('#pageNo').val(), 10) > 1)
                        $('#btnPrevious').removeAttr("disabled");
                }
            }
        });
    });
        </script>
    </head>
    <body onload="loadDebarList()">
        <div class="dashboard_div">
             <jsp:include page="resources/common/Top.jsp" ></jsp:include>
           
            <div class="contentArea_1">
                 <div class="pageHead_1">Debarment List</div>
                  <div>&nbsp;</div>
                  <div class="formBg_1">
                      <table cellspacing="10" class="formStyle_1" width="100%">
                            <tr>
                             <td class="ff">Search Company:</td>
                             <td></td>
                             <td></td>
                            </tr>
                            <tr>
                               <td  class="ff">Company Name:</td>
                               <td><input type="text" class="formTxtBox_1" id="cmpName" name="cmpName" style="width:202px;" /></td>
                               <td></td>
                            </tr>
                           <tr>
                               <td  class="ff">Dzongkhag / District:</td>
                               <td>
                                <input type="text" class="formTxtBox_1" id="dstName" name="dstName" style="width:202px;" />
                               </td>
                               <td></td>
                            </tr>

                             <tr>
                        <td class="ff">Debar Period:</td>
                        <td><input name="debfrm" id="debfrm" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" onfocus="GetCal('debfrm','debfrmImg');"/>&nbsp;
                            <a  href="javascript:void(0);" title="Calender"><img id="debfrmImg" src="resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('debfrm','debfrmImg');"/></a></td>
                      
                        <td><input name="debto" id="debto" type="text" class="formTxtBox_1" style="width:100px;" readonly="readonly" />&nbsp;
                            <a  href="javascript:void(0);" title="Calender"><img id="debtoImg" src="resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('debto','debtoImg');"/></a></td>
                    </tr>
                   
                    <tr>
                        <td colspan="4" align="center"><label class="formBtn_1">
                                <input type="submit" name="button" id="btnSearch" value="Search" />
                            </label>
                            &nbsp;
                            <label class="formBtn_1">
                                <input type="reset" name="Reset" id="btnReset" value="Reset" />
                            </label></td>
                    </tr>
                      </table>
                  </div>
                  <div>&nbsp;</div>
                      <div class="tabPanelArea_1">
                          <table width="100%" cellspacing="0" class="tableList_1" id="resultTable">
                            <tr>
                                <th width="4%" class="t-align-left">Sl. No.</th>
                                <th width="25%" class="t-align-left">Firm/Company</th>
                                <th width="21%" class="t-align-left">Dzongkhag / District</th>
                                <th width="15%" class="t-align-left">Debar Period</th>
                                <th width="15%" class="t-align-left">Debared By</th>
                                <th width="15%" class="t-align-left">Resons</th>
                            </tr>
                          </table>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pagging_1">
                                        <tr>
                                            <td align="left">Page 1 - <span id="pageTot">10</span></td>
                                            <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                &nbsp;
                                                <label class="formBtn_1">
                                                    <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                                </label></td>
                                            <td align="right"><ul>
                                                    <li>&laquo; <a href="javascript:void(0)" id="btnFirst">First</a></li>
                                                    <li>&#8249; <a href="javascript:void(0)" id="btnPrevious">Prev.</a></li>
                                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> &#8250;</li>
                                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> &raquo;</li>
                                                </ul></td>
                                        </tr>
                                    </table>
                           <div align="center">
                                        <input type="hidden" id="pageNo" value="1"/>
                                        <input type="hidden" name="size" id="size" value="10"/>
                              </div>
                      </div>
                  
            </div>
            
        </div>
    </body>
</html>
