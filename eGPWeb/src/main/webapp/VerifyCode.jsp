<%-- 
    Document   : VerifyCode
    Created on : Oct 28, 2010, 7:07:00 PM
    Author     : Malhar
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Verify Code</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />

       
       
    </head>
    <body>
        <%
            String msg = request.getParameter("msg");
            if (msg != null && msg.equalsIgnoreCase("success")) {
                session.removeAttribute("emailId");
        %>
            <form name="frmVerifyCd" id="frmVerifyCd" method="post"></form>
            <script type="text/javascript">
                jAlert('Code Verified Successful','Verify Code', function(RetVal) {
                    if(r){
                        document.getElementById("frmVerifyCd").action = "<%=request.getContextPath()%>/Index.jsp";
                        document.getElementById("frmVerifyCd").submit();
                    }
                });
                
            </script>
        <%
            } else if(msg != null && (msg.equalsIgnoreCase("send") ||  msg.equalsIgnoreCase("verify")) && session.getAttribute("email")==null){
             %>
            <form name="frmVC" id="frmVC" method="post"></form>
            <script type="text/javascript">

                        document.getElementById("frmVC").action = "<%=request.getContextPath()%>/Index.jsp";
                        document.getElementById("frmVC").submit();


            </script>
        <%
          }else{
            String url = "ResetPasswordSrBean";
             //String email="";
             //if(session.getAttribute("emailId")!=null)
             //    email = session.getAttribute("emailId").toString();
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
                <script src="resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
                <link href="resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
                <script src="resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1">
                                        <div class="pageHead_1">Verify Code</div>
                                        <%
                                            if (msg != null && msg.equalsIgnoreCase("fail")) {
                                        %>
                                                <div class="t_space"><div class="responseMsg errorMsg">Code verification failed</div></div>
                                        <%
                                            } else if (msg != null && msg.equalsIgnoreCase("failSend")) {
                                             url = "LoginSrBean";
                                        %>
                                                <div class="t_space"><div class="responseMsg errorMsg">Code verification failed</div></div>
                                        <%
                                            } else if (msg != null && msg.equalsIgnoreCase("send") && session.getAttribute("email")!=null) {
                                              url = "LoginSrBean";
                                         %>
                                                 <div class="t_space"><div class="responseMsg successMsg">Verification Code has been sent to your email</div></div>
                                        <% }else if(msg != null && msg.equalsIgnoreCase("verify") && session.getAttribute("email")!=null){
                                              url = "ResetPasswordSrBean";
                                         %>
                                                <div class="t_space"><div class="responseMsg successMsg">Verification Code has been sent to your email</div></div>
                                         <%}%>
                                        <form id="frmReset" name="frmReset" method="POST" action="<%=request.getContextPath()%>/<%=url%>">
                                       
                                       <!-- <form id="frmReset" name="frmReset" method="POST" action="<%=request.getContextPath()%>/ResetPasswordSrBean">-->
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td class="ff">e-mail ID : <span>*</span></td>
                                                    <td>
                                                        <% if(msg != null && (msg.equalsIgnoreCase("send") || msg.equalsIgnoreCase("failSend"))){  %>
                                                             <input type="hidden" name="funName" value="verifyCodeLogin"/>
                                                             <input type="hidden" name="action" value="checkLogin"/>
                                                             <input type="hidden" name="password" value="<%=session.getAttribute("pass")%>"/>
                                                             <input class="formTxtBox_1" readonly type="text" id="txtEmailId" name="emailId" style="width: 200px" value="<%=session.getAttribute("email")%>"/>
                                                        <% 
                                                        session.removeAttribute("pass");
                                                        session.removeAttribute("email");
                                                        //session.invalidate();
                                                        } else if(msg != null && msg.equalsIgnoreCase("verify")){%>
                                                        <input class="formTxtBox_1" type="text" id="txtEmailId" name="emailId" style="width: 200px" value="<%=session.getAttribute("email")%>" readonly/>
                                                        <input type="hidden" name="funName" value="verifyCode"/>
                                                        <% session.removeAttribute("email");
                                                        //session.invalidate();
                                                        }else if(msg != null && msg.equalsIgnoreCase("fail")){%>
                                                          <input class="formTxtBox_1" type="text" id="txtEmailId" name="emailId" style="width: 200px" value="<%=session.getAttribute("email")%>" readonly/>
                                                          <input type="hidden" name="funName" value="verifyCode"/>
                                                        <% session.removeAttribute("email");}
                                                         else{%>
                                                         <input class="formTxtBox_1" type="text" id="txtEmailId" name="emailId" style="width: 200px"/>
                                                         <input type="hidden" name="funName" value="verifyCode"/>
                                                          <% } %>
                                                        <span id="errorMsg" style="color: red;font-weight: bold"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Verification Code : <span>*</span></td>
                                                    <td>
                                                        <input class="formTxtBox_1" type="text" id="txtVerificationCode" name="verificationCode" style="width: 200px" maxlength="7"/>
                                                    </td>
                                                    <td>
                                                        <label class="formBtn_1" id="lblResend">
                                                            <input type="button" name="button" id="btnResend" value="Resend Code"/>
                                                            
                                                        </label>
                                                    </td>
                                                    <td>
                                                         <span id="smsMsg" style="font-weight: bold;display: none" ></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="submit" id="btnSubmit" value="Submit" disabled/>
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form> 
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <%
            }
        %>
    </body>
<script type="text/javascript">
$(document).ready(function() {
    $("#btnSubmit").attr('disabled', false);
    $("#frmReset").validate({
            rules: {
                emailId:{required: true,email:true},
                verificationCode:{required:true}
            },
            messages: {
                emailId:{ required: "<div class='reqF_1'>Please enter valid e-mail ID.</div>",
                    email:"<div class='reqF_1'>Please enter valid e-mail ID.</div>"},
                verificationCode:{required:"<div class='reqF_1'>Please enter Verification Code.</div>"}
            }
    });
        $('#btnResend').click(function() {
        
        if($j('#txtEmailId').val()=="")
        {
            $j('span.#errorMsg').css("display", "block");
            $j('span.#errorMsg').html("Please enter valid e-mail ID.");
        }
        else
        {
        $("#errorMsg").hide();
        jQuery.ajax({
            url:    '<%=request.getContextPath()%>/LoginSrBean'
                + '?emailId='+ $j('#txtEmailId').val()
                //+ '&type='+ 'reset'
                + '&funName=' + 'resendCode',
            type: 'POST',
            success: function(result) {
                    $j('div.successMsg').css("display", "none");
                    $j('span.#smsMsg').css("display", "block");
                    if(result=='ok'){
                        $j('span.#smsMsg').html("Verification Code has been sent to your email.");
                        $j('span.#smsMsg').css("color", "green");
                    }
                    //else if(result=='over'){
                    //     $j('span.#smsMsg').css("color", "red");
                    //     $j('span.#smsMsg').html("Maximum limit of requesting verification code has been exceeded.");
                   // }
                    else{
                        $j('span.#smsMsg').css("color", "red");
                        $j('span.#smsMsg').html("Error Occured.");
                    }
            }
        });
        }
   });
});
</script>
</html>
