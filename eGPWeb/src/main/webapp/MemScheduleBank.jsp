<%--
    Document   : contactUs
    Created on : Dec 12, 2010, 12:54:06 PM
    Author     : parag
--%>

<%@page import="com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MultiLingualService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.List"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Member Schedule Banks</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(){

                var pauseA = 2500;
                var pause = 3500;

                function newstickerA()
                {
                    // algorithm:
                    // get last element, remove it from the list,
                    // add to first position with hidden style
                    // slideDown the new first element
                    // continue
                    last = $('ul#listtickerA li:last').hide().remove();
                    $('ul#listtickerA').prepend(last);
                    $('ul#listtickerA li:first').slideDown("slow");
                }

                interval = setInterval(newstickerA, pauseA);

                function newsticker()
                {
                    // algorithm:
                    // get last element, remove it from the list,
                    // add to first position with hidden style
                    // slideDown the new first element
                    // continue
                    last = $('ul#listticker li:last').hide().remove();
                    $('ul#listticker').prepend(last);
                    $('ul#listticker li:first').slideDown("slow");
                }

                interval = setInterval(newsticker, pause);
            });
        </script>
    </head>
    <body>

        <%
                    ScBankDevpartnerService scbankdevpartner = (ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
                    List<TblScBankDevPartnerMaster> bankname = scbankdevpartner.getBankName();
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td style="width:266px;">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include></td>
                        <td>&nbsp;&nbsp;&nbsp;</td>
                        <td class="contentArea">                  
                            <div class="pageHead_1">
                                Member Scheduled Banks
                            </div>
                            <div class="atxt_1 c_t_space">
                                <table>
                                    <tr style="padding-top:  5px;">
                                        <td class="ff t-align-right">Please click on Financial Institute Name to view its Branch contact details</td>
                                    </tr>
                                    <tbody>
                                        <tr>
                                            <td style="padding-right: 5px;">                                                
                                                <ul style="padding-top: 10px; margin:10px 10px 10px 0px; list-style:none; line-height:18px;">
                                                <%
                                                            if (!bankname.isEmpty()) {
                                                                for (int i = 0; i < bankname.size(); i++) {
                                                %>
                                                
                                                    <li style="margin-bottom: 5px; color:#333; background:url(resources/images/bul_3.gif) 0px 5px no-repeat; padding-left:12px;">
                                                        <a href="BranchDetail.jsp?sbankdevelopid=<%=bankname.get(i).getSbankDevelopId()%>&name=<%=bankname.get(i).getSbDevelopName()%>"><%=bankname.get(i).getSbDevelopName()%></a>
                                                    </li>
                                                
                                                <%
                                                                }
                                                            }
                                                %>
                                                </ul>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>

                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
