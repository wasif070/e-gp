<%-- 
    Document   : ViewNews
    Created on : Apr 8, 2018, 5:20:55 PM
    Author     : Aprojit
--%>

<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page  import="com.cptu.egp.eps.web.utility.LogoutUtils"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <%
            HttpSession hs = request.getSession();
    if(hs!=null)
        {
            hs.removeAttribute("userId");
            Object objUserType = hs.getAttribute("userTypeId");
            if (objUserType != null && objUserType.equals("1")) {
                hs.removeAttribute("userType");
            }
            hs.removeAttribute("userTypeId");
            hs.removeAttribute("userName");
            LogoutUtils logoutUtils = new LogoutUtils();
            logoutUtils.setLogUserId("0");
            Object objSessionid = hs.getAttribute("sessionId");
            if (objSessionid != null) {
                logoutUtils.updateSessionMaster(Integer.parseInt(objSessionid.toString()));
                hs.removeAttribute("sessionId");
            }

            //hs.invalidate();
           
             String id="0";
             hs.setAttribute("userId",id);
    }
    else
        {   
            hs = request.getSession();
        }
            String Type = "";
            
            if(request.getParameter("newsType").equalsIgnoreCase("N"))
            {
                Type = "News/Advertisement";
            }
            else if(request.getParameter("newsType").equalsIgnoreCase("C"))
            {
                Type = "Circular";
            }
            else if(request.getParameter("newsType").equalsIgnoreCase("A"))
            {
                Type = "Amendment";
            }
            else if(request.getParameter("newsType").equalsIgnoreCase("I"))
            {
                Type = "Notification";
            }
        %>
        <title><%=Type%> Details </title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="resources/js/ddlevelsmenu.js">
            

            /***********************************************
             * All Levels Navigational Menu- (c) Dynamic Drive DHTML code library (http://www.dynamicdrive.com)
             * This notice MUST stay intact for legal use
             * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
             ***********************************************/

        </script>
    </head>
    <body>

        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table  class="content-table-all" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        
                        <td class="contentArea-Blogin" align="left">
                            <!--Page Content Start-->
                            
                            <div class="pageHead_1">
                                  
                                  <%=Type%> Details
                            </div>
                            <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%" style="color:#333; text-align: justify;">
                                
                                <%
                                    String newsId = "";
                                    String newsType = "";
                                    if (!request.getParameter("newsType").equals("")) {
                                        newsId = request.getParameter("nId");
                                        newsType = request.getParameter("newsType");
                                    }                                    
                                    
                                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                    List<SPTenderCommonData> list = tenderCommonService.returndata("getnewsbyid", newsId, newsType);
                                    String Time = null;
                                    String FileName = null;

                                    if(list.get(0).getFieldName6()!=null && !list.get(0).getFieldName6().equals(""))
                                    {
                                        Time = DateUtils.SqlStringDatetoApplicationStringDate(list.get(0).getFieldName6());
                                    }
                                    if(list.get(0).getFieldName8()!=null && !list.get(0).getFieldName8().equals(""))
                                    {
                                        FileName = list.get(0).getFieldName8();
                                    }
                                     if (!list.isEmpty() && list.size()>0) {                                    
                                %>

                                <tr>
                                    <td width="22%" class="t-align-left ff">
                                        <%=Type%> Brief :
                                    </td>

                                    <td width="78%" style="padding-left:20px;text-align: justify;"><%= list.get(0).getFieldName4()%></td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">
                                        <%=Type%> Details :
                                    </td>
                                    <td style="padding-left:20px;text-align: justify;"><%=list.get(0).getFieldName5()%></td>
                                </tr> 
                                <% if(!request.getParameter("newsType").equalsIgnoreCase("N")) { %>
                                <tr>
                                    <td class="t-align-left ff">
                                        Uploaded file :
                                    </td>
                                    <td style="padding-left:20px;text-align: justify;">
                                        <% if(FileName != null) {%>
                                                <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?funName=downloadNewDocuments&FileName=<%=FileName%>" title="Download">View Current file<a>
                                        <% } %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">
                                        Letter Number :
                                    </td>
                                    <td style="padding-left:20px;text-align: justify;">
                                        <%  
                                            if(list.get(0).getFieldName7()!=null) 
                                            {
                                                out.print(list.get(0).getFieldName7()); 
                                            }
                                        %>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="t-align-left ff">
                                        Time :
                                    </td>
                                    <td style="padding-left:20px;text-align: justify;">
                                        <% 
                                            if(Time!=null) 
                                            { 
                                                out.print(Time);
                                            }
                                        %>
                                    </td>

                                </tr>
                                <% } %>
                                <%}%>
                            </table>
                            <div>&nbsp;</div>
                            <!--Page Content End-->
                        </td>
                            <td width="266" class="td-background">
                            <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>


    </body>
</html>
