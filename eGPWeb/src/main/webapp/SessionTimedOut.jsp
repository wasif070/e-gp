<%-- 
    Document   : SessionTimedOut
    Created on : 22-Nov-2010, 6:58:19 PM
    Author     : yagnesh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Session Expired</title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <%
                    session.invalidate();
                    if (request.getParameter("rs") != null) {
                        HttpSession hs = request.getSession();
                        if (hs != null) {
                            hs.invalidate();
                        }
                    }
        %>
        <div class="dashboard_div">
            <jsp:include page="resources/common/Top.jsp" ></jsp:include>
            <!--Middle Content Table Start-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr valign="top">
                    <td class="contentArea_1">
                        <!--Page Content Start-->
                        <fieldset class="sessionTimeOut">
                            <legend class="heading-text"><img src="<%= request.getContextPath()%>/resources/images/Dashboard/Warning.png" width="16" height="16" /> &nbsp;Dear User, Your session has been expired.
                            </legend>
                            <div class="reason">Your session is expired due to any of the following reasons :</div>
                            <ul>
                                <li>You might have kept your browser window idle for more than 30 minutes.</li>
                                <li>Change in URL.</li>
                            </ul>
                            <div class="t_space">
                                <a href="Index.jsp">Go to login page.</a>
                            </div>
                        </fieldset>

                        <!--Page Content End-->
                    </td>
                </tr>
            </table>
            <%--<div>&nbsp;</div>
            <div class="responseMsg errorMsg">Session Timed Out</div>--%>
            <!--Middle Content Table End-->
            <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
</html>
