<%-- 
    Document   : ManageProject
    Created on : Nov 17, 2010, 2:17:38 PM
    Author     : parag
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="projGridSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectGridSrBean" />
<jsp:useBean id="projSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectSrBean" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page  import="java.util.List,com.cptu.egp.eps.dao.storedprocedure.CommonAppData" %>
<%
            boolean flag=false;
            String action="";
            String msg="";

            if(request.getParameter("flag") != null){
                 if("true".equalsIgnoreCase(request.getParameter("flag"))){
                     flag=true;
                 }
            }
            if(request.getParameter("action") != null){
                action = request.getParameter("action");
            }

            /*if(flag && "createGovtUser".equalsIgnoreCase(action)){
                msg = " Government User Created Successfully";
            }
            if(flag && "createGovtUser".equalsIgnoreCase(action)){
                msg = " Government User Created Successfully";
            }
            else if(flag && "editGovtUser".equalsIgnoreCase(action)){
                msg = " Government User Updated Successfully";
            }*/

            String type = "";
            String title = "";
            String userType = "";
            int userTypeId=5;

            int govUserId=0;

            if (request.getAttribute("userId") != null) {
                govUserId = Integer.parseInt(request.getAttribute("userId").toString());
            }

            govSrUser.setUserId(171);
            int deptId=govSrUser.getUserTypeWiseDepartmentId(govUserId,userTypeId);
            

            type = "GovtUser";
            title = "Government Users";
            userType = "gov";

           // List<CommonAppData> govData = govGridSrUser.getGovtUserDetail(3);
           
            String link = "GovtUserGridSrBean?action=viewGovData&govUserType";
           
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage <%=title%> Admin</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />



    </head>
    <%

    %>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>


                        <td>
                            <div >
                        <%if(!" ".equalsIgnoreCase(msg) || flag){ %>
                        <div id="successMsg" class="responseMsg successMsg" style="display:none"><%=msg%></div>
                        <%}else if(!" ".equalsIgnoreCase(msg)){%>
                            <div id="errMsg" class="responseMsg errorMsg" style="display:none"><%=msg%></div>
                         <%}%>
                         </div>

                            <div align="center">

                                <table id="list"></table>
                                <div id="page">

                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
 <script type="text/javascript">
             //{name:'emailId',index:'emailId', width:100},
                        //{name:'<%=type%>',index:'<%=type%>', width:100},
                        //{name:'view',index:'view', width:100}

            jQuery().ready(function (){
                //alert("url:" + < %=link%>);
                jQuery("#list").jqGrid({
                    url: "../GovtUserGridSrBean?action=viewGovData",
                    datatype: "xml",
                    height: 250,
                    colNames:['Full Name','Mobile No','Date','Status','Edit','View','Final Submission'],
                    colModel:[
                        {name:'fullName',index:'fullName', width:90},
                        {name:'mobNo',index:'mobNo', width:100},
                        {name:'sdate',index:'sdate', width:100},
                        {name:'status',index:'status', width:100},
                        {name:'edit',index:'edit', width:100},
                        {name:'view',index:'view', width:100},
                        {name:'finalSubmission',index:'finalSubmission', width:100},
                    ],
                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    caption: "<%=title%> ",
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{edit:false,add:false,del:false});
            });
        </script>
