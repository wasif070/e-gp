<%-- 
    Document   : PEOfficeDetailsPage
    Created on : Mar 22, 2011, 6:59:40 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PE Office Details</title>
<script type="text/javascript" src="resources/js/ddlevelsmenu.js"></script>
<link href="resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
</head>
<body>
<div class="mainDiv">
    <div class="fixDiv">
        <!-- include Top Panel -->
         <jsp:include page="resources/common/Top.jsp" ></jsp:include>
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top">
                <td width="266">
                    <!-- include Left Panel -->
                    <jsp:include page="resources/common/Left.jsp" ></jsp:include>
                </td>
                <td class="contentArea-Blogin">
                    <div class="DashboardContainer">
                        <%
                            String strType = "";
                            if(request.getParameter("type")!=null && !"".equalsIgnoreCase(request.getParameter("type"))){
                                strType = request.getParameter("type");
                            }

                            // Get PE Office Details Ministry, Division and Organization wise.
                            CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                            List<SPCommonSearchDataMore> reportData = commonSearchDataMoreService.getCommonSearchData("GetPEOfficeInfo", strType, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                        %>
                    <div class="pageHead_1"><%=strType%> wise PE office details page
                        <span style="float: right; text-align: right;">
                            <a class="action-button-goback" href="RegistrationDetails.jsp" title="Go Back">Go Back</a>
                        </span>
                    </div>
                    <div class="DashboardContainer">
                        <table width="100%" cellspacing="0" class="tableList_3 t_space">
                            <tr>
                                <th width="75%">Name of <%=strType%></th>
                                <th width="25%">No. of PE offices</th>
                            </tr>
                            <!-- Display PE Office Details -->
                            <%  for(SPCommonSearchDataMore data:reportData){ %>
                            <tr>
                              <td class="t-align-left"><%=data.getFieldName2()%></td>
                              <td class="t-align-center"><% if("0".equals(data.getFieldName3())){ %>No PE office available<% } else{ out.println(data.getFieldName3()); } %></td>
                            </tr>
                            <% } %>
                        </table>
                  </div>
                  </div>
                </td>
            </tr>
         </table>
          <!-- include Bottom Panel -->
         <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
    </div>
</div>
</body>
</html>
