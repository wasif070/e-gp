<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.PublicForumPostService" %>
<%@page import="net.tanesha.recaptcha.ReCaptchaFactory"%>
<%@page import="net.tanesha.recaptcha.ReCaptcha"%>
<%@page import="com.cptu.egp.eps.model.table.TblPublicProcureForum" %>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster" %>
<%@page import="java.util.Date" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Give Reply to Forum Topic</title>
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
        <!--
        <script src="ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script type="text/javascript" src="resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="resources/js/form/CommonValidation.js"></script>

        <script type="text/javascript">
            $(function() {
                $('#recaptcha_response_field').blur(function() {
            if($('#recaptcha_response_field').val()!=""){
                        $.post("<%=request.getContextPath()%>/CommonServlet", {challenge:$('#recaptcha_challenge_field').val(),userInput:$('#recaptcha_response_field').val(),funName:'captchaValid'},
                        function(j){
                            if(j.toString()=="OK"){
                                $('span.#vericode').css("color","green");
                            }
                            else{
                                javascript:Recaptcha.reload();
                                $('span.#vericode').css("color","red");
                            }
                            $('span.#vericode').html(j);
                        });
                    }else{
                        $('span.#vericode').css("color","red");
                        $('#vericode').html("Please enter Verification Code");
                    }
                });
            });
            function validate()
            {
                //alert("Ketan Prajapati : "+CKEDITOR.instances.txtarea_replydetails.getData()+"length : "+CKEDITOR.instances.txtarea_replydetails.getData()+"trim : "+$.trim(CKEDITOR.instances.txtarea_replydetails.getData()));
                $(".err").remove();
                var valid = true;

                if($.trim($("#txtuname").val()) == '') {
                    $("#txtuname").parent().append("<div class='err' style='color:red;'>Please enter User's Name</div>");
                    valid = false;
                }
                if(isCKEditorFieldBlank($.trim(CKEDITOR.instances.txtarea_replydetails.getData().replace(/<[^>]*>|\s/g, '')))){
                    $("#txtarea_replydetails").parent().append("<div class='err' style='color:red;'>Please enter Detail</div>");
                    CKEDITOR.instances.txtarea_replydetails.setData("");
                    valid=false;
                }
                else if($.trim(CKEDITOR.instances.txtarea_replydetails.getData().replace(/<[^>]*>|\s/g, '')).length > 2000) {
                    $("#txtarea_replydetails").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters are allowed</div>");
                    valid=false;
                }
                if($('#recaptcha_response_field').val() != undefined){
                    if($('#recaptcha_response_field').val()==""){
                        $('span.#vericode').css("color","red");
                        $('#vericode').html("Please enter Verification Code");
                        valid =  false;
                    }else{
                        if($('#vericode').html()!="OK"){
                            javascript:Recaptcha.reload();
                            valid =  false;
                        }
                    }
                }
                if(!valid){
                    return false;
                }
            }
        </script>
    </head>
    <body>

        <div class="dashboard_div">
            <jsp:include page="resources/common/Top.jsp"/>

            <%
                        String topicSubject = "";
                        String topicDetail = "";
                        String ppfid = "";


                        if (request.getParameter("ppfid") != null) {
                            PublicForumPostService publicForumPostService = (PublicForumPostService) AppContext.getSpringBean("PublicForumPostService");
                            ppfid = request.getParameter("ppfid");
                            //out.println("Request to PostReply.jsp file : PPF id : "+ppfid);

                            List<TblPublicProcureForum> colResult = publicForumPostService.getAllForums(ppfid);
                            for (TblPublicProcureForum procureForum : colResult) {
                                topicSubject = procureForum.getSubject();
                                topicDetail = procureForum.getDescription();
                            }
                        }
            %>
            <script type="text/javascript">
                var goBackLink = "viewTopic.jsp?ppfid="+"<%=ppfid%>";;
                //alert("replyLink : "+goBackLink);
            </script>
            <!--Dashboard Header Start-->

            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1">Give Reply to Forum Topic <span class="c-alignment-right"><a href="viewTopic.jsp?ppfid=<%=ppfid%>&ViewType=<%=request.getParameter("ViewType")%>" class="action-button-goback">Go Back</a></span> </div>
                <form id="frmPublicProcForum" name="frmPublicProcForum" method="post" action="<%=request.getContextPath()%>/PublicProcForumServlet">
                    <div style="font-style: italic" class="formStyle_1 ff t_space">Fields marked with (<span class="mandatory">*</span>) are mandatory</div>
                    <table width="100%" cellspacing="5" class="formStyle_1">
                        <tr>
                            <td width="10%" class="ff">User's Name</td>
                            <td width="90%">
                                <input type="text" name="txtuname" class="formTxtBox_1" id="txtuname" style="width:200px;" maxlength="100" value="<%=session.getAttribute("userId") != null ? session.getAttribute("userName") : ""%>" <%=session.getAttribute("userId") != null ? " readonly " : ""%> />
                            </td>
                        </tr>
                        <tr>
                            <td class="ff"> Topic : </td>
                            <td><%=topicSubject%></td>
                        </tr>
                        <tr>
                            <td class="ff">
   	  Detail : </td>
                            <td><%=topicDetail%></td>
                        </tr>
                        <tr>
                            <td class="ff">Reply Detail: <span class="mandatory">*</span></td>
                            <td>
                                <textarea cols="80" id="txtarea_replydetails" name="txtarea_replydetails"  rows="10"></textarea>
                                <script type="text/javascript">
                                    CKEDITOR.replace( 'txtarea_replydetails',
                                    {
                                        fullPage : false
                                    });
                                </script>
                            </td>
                        </tr>
                        <%if (session.getAttribute("userId") == null) {%>
                    <tr>
                        <td class="ff">Verification Code : <span>*</span></td>
                        <td>
                            <%
                                String pubKey = "6Lfw4r0SAAAAAO2WEc7snoFlSixnCQ2IGaO7k_iO";
                                String privKey = "6Lfw4r0SAAAAAN_Pr3YGPA39kRIyNsuMCDJIpUsS";
                                StringBuffer reqURL = request.getRequestURL();
                                if (reqURL.toString().substring(reqURL.toString().indexOf("//") + 2, reqURL.toString().lastIndexOf("/")).equals("www.eprocure.gov.bd")) {
                                    pubKey = "6Lc0ssISAAAAAKh369J1vrGKmSfpEGhbr07sl1gS";
                                    privKey = "6Lc0ssISAAAAAJf6jPcfSBElxYSRk1LB46MBsWUk";
                                }
                                ReCaptcha c = ReCaptchaFactory.newReCaptcha(pubKey, privKey, false);
                                out.print(c.createRecaptchaHtml(null, null));
                                pubKey = null;
                                privKey = null;
                                reqURL = null;
                            %>
                            <span class="formNoteTxt">(If you cannot read the text, you may get new Verification Code by clicking <img alt="refresh"  src="resources/images/refresh.gif"/>, and if you want to hear the Verification Code, please click <img alt="sound"  src="resources/images/sound.gif"/> )</span>
                            <span class="reqF_1" id="vericode"></span>
                        </td>
                    </tr>
                    <%}%>
                        <input type="hidden" name="ppfid" value="<%=ppfid%>" />
                        <tr>
                            <td class="ff">&nbsp;</td>
                            <td><label class="formBtn_1"><input name="button" type="submit" id="button" value="Submit" onclick="return validate()"/></label></td>
                        </tr>
                        <tr>
                            <td colspan="2" class="t-align-left">
                                <font color="red"><strong>Important Note:</strong> Please note, posting of any issue which is not related to Procurement or use of abusive language can lead to debarment & deactivation of your account with or without penalty.</font>
                            </td>
                        </tr>
                    </table>
                    <input type="hidden" value="postReply" name="action" id="action" />
                    <input type="hidden" value='<%=request.getParameter("ViewType")%>' name="ViewType" id="ViewType"/>
                    <input type="hidden" value="<%=topicSubject%>" name="topicSubject" id="topicSubject" />
                    <input type="hidden" value="<%=ppfid%>" name="ppfid" id="ppfid" />
                </form>
                <div>&nbsp;</div>

                <!--Dashboard Content Part End-->

            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>

    </body>
</html>
