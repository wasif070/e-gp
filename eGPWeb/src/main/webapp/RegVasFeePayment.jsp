<%--
    Document    : RegVasFeePayment
    Created on  : March 07, 2012, 11:47:42 AM
    Author     : dixit
--%>

<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
            String strTrnsID = "";
            if (request.getParameter("tansId") != null) {
                strTrnsID = request.getParameter("tansId");
            }

            CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
            List<SPCommonSearchDataMore> listData = csdms.geteGPDataMore("getRegVASPaymentData", strTrnsID);
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Online Registration fee payment for Value Added Services </title>
        <link href="resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

        <script src="resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <%
                    if (listData == null || listData.isEmpty()) {
        %>
        <script type="text/javascript">
            function regForNumber(value)
            {
                return /^(0|(\+)?[1-9]{1}[0-9]{0,8}|(\+)?[1-3]{1}[0-9]{1,9}|(\+)?[4]{1}([0-1]{1}[0-9]{8}|[2]{1}([0-8]{1}[0-9]{7}|[9]{1}([0-3]{1}[0-9]{6}|[4]{1}([0-8]{1}[0-9]{5}|[9]{1}([0-5]{1}[0-9]{4}|[6]{1}([0-6]{1}[0-9]{3}|[7]{1}([0-1]{1}[0-9]{2}|[2]{1}([0-8]{1}[0-9]{1}|[9]{1}[0-5]{1})))))))))$/.test(value);

            }
            function generateTotal()
            {
                if(document.getElementById("txtAmountid").value!="")
                {
                    if(!regForNumber(document.getElementById("txtAmountid").value))
                    {
                        document.getElementById("txtAmountspan").innerHTML="Please enter numeric value";
                    }else{
                        document.getElementById("txtAmountspan").innerHTML="";
                        var PercentageAmount = (document.getElementById("txtAmountid").value*document.getElementById("serviceChargeid").value)/100;
                        var Amount = document.getElementById("txtAmountid").value;
                        var totalAmount = eval(Amount) + eval(PercentageAmount);
                        document.getElementById("ServicechargeAmount").innerHTML = PercentageAmount.toFixed(3)+" (In Nu.)";
                        document.getElementById("totalAmount").innerHTML = totalAmount.toFixed(3);
                        document.getElementById("totalAmountid").value = totalAmount.toFixed(3);
                    }
                }
            }

            function checkValue(objId,errorSpanID)
            {
                if(document.getElementById(objId).value != "")
                {
                    document.getElementById(errorSpanID).innerHTML = "";
                }
                //validate();
            }

            function validate()
            {
                var flag = true;
                if(document.getElementById("txtPayeeNameid").value == "")
                {
                    flag = false;
                    document.getElementById("txtPayeeNamespan").innerHTML = "Please enter Payee Name";
                }
                else{
                    document.getElementById("txtPayeeNamespan").innerHTML = "";
                }
                if(document.getElementById("txtPayeeAddressid").value == "")
                {
                    flag = false;
                    document.getElementById("txtPayeeAddressSpan").innerHTML = "Please enter Payee Address";
                }else{
                    document.getElementById("txtPayeeAddressSpan").innerHTML ="";
                }
                if(document.getElementById("txtDescofPayid").value == "")
                {
                    flag = false;
                    document.getElementById("txtDescofPayspan").innerHTML = "Please enter Description of Payment ";
                }
                else{
                    document.getElementById("txtDescofPayspan").innerHTML = "";
                }
                if(document.getElementById("emailId").value == "")
                {
                    flag = false;
                    document.getElementById("txtemailIdspan").innerHTML = "Please enter e-mail ID";
                }else{
                    document.getElementById("txtemailIdspan").innerHTML = "";
                    if($.trim($("#emailId").val()).length!=0) {
                        spaceTest = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;
                        if(!spaceTest.test($('#emailId').val())){
                            document.getElementById("txtemailIdspan").innerHTML = "Please enter valid e-mail ID";
                            flag = false;
                        }
                    }
                }
                if(document.getElementById("txtAmountid").value == "")
                {
                    flag = false;
                    document.getElementById("txtAmountspan").innerHTML = "Please enter Amount";
                }else{
                    document.getElementById("txtAmountspan").innerHTML ="";
                    if(!regForNumber(document.getElementById("txtAmountid").value))
                    {
                        document.getElementById("txtAmountspan").innerHTML="Please enter numeric value";
                        flag = false;
                    }
                }
                return flag;
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#emailId').blur(function() {
                    if($('#emailId').val()==""){
                        $('span.#txtemailIdspan').css("color","red");
                        $('span.#txtemailIdspan').html("Please enter e-mail ID");
                    }else{
                        $('span.#txtemailIdspan').css("color","red");
                        $('#txtemailIdspan').html("Checking for unique e-mail ID...");
                        $.post("<%=request.getContextPath()%>/OnlinePaymentServlet", {mailId:$('#emailId').val(),action:'verifyMail'},
                        function(j){
                            if(j.toString().indexOf("OK", 0)!=-1){
                                $('span.#txtemailIdspan').css("color","green");
                            }
                            else{
                                $('span.#txtemailIdspan').css("color","red");
                            }
                            $('span.#txtemailIdspan').html(j);
                        });
                    }
                });
            });
        </script>
        <%}%>
    </head>

    <body>
        <div class="dashboard_div">
            <div class="mainDiv">
                <div class="fixDiv">
                    <jsp:include page="resources/common/Top.jsp" ></jsp:include>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr valign="top">
                            <td class="contentArea-Blogin">
                                <%-- @include  file="resources/common/Top.jsp" --%>
                                <div class="contentArea_1">
                                    <div class="pageHead_1">Online Value Added Service Registration Fee</div>
                                    <div>&nbsp;</div>
                                    <%
                                                if (listData != null && !listData.isEmpty() && ((listData.get(0).getFieldName10() != null && listData.get(0).getFieldName10().indexOf("000") > 0 && listData.get(0).getFieldName10().indexOf("OK") > 0) || listData.get(0).getFieldName10().indexOf("<txn_status>ACCEPTED</txn_status>") > 0)) {
                                    %>
                                    <jsp:include page="OnlinePaymentVASSuc.jsp" ></jsp:include>
                                    <%} else {%>
                                    <jsp:include page="OnlinePmtForVAS.jsp" ></jsp:include>
                                    <%}%>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <%@include file="resources/common/Bottom.jsp" %>
            </div>
        </div>
    </body>
</html>
