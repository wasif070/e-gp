<%-- 
    Document   : editModule
    Created on : Jun 7, 2017, 1:03:04 PM
    Author     : feroz
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.QuestionModuleService"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuestionModule"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuizAnswer"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuizQuestion"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuizAnswerService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuizQuestionService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.BhutanDebarmentCommitteeService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.util.Calendar"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Edit Module</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
            
                $("#setQ").validate({
                    rules: {
                        moduleName:
                        {
                            required:true,
                            maxlength: 450
                        }

                    },
                    messages: {
                        moduleName:
                        { required: "<div class='reqF_1'> Please Enter Module Name.</div>",
                          maxlength: "<div class='reqF_1'> Maximum 450 characters are allowed.</div>"
                        }
                    }
                }
            );
            });

        </script>
        <%
                    QuestionModuleService questionModuleService = (QuestionModuleService) AppContext.getSpringBean("QuestionModuleService");
                    int createdBy = Integer.parseInt(session.getAttribute("userId").toString());
                    if ("Update".equals(request.getParameter("button"))) 
                    {
                        boolean doneFlag = false;
                        String action = "";
                        String moduleId = request.getParameter("moduleId");
                        String moduleName = request.getParameter("moduleName");
                        TblQuestionModule tblQuestionModuleThis = new TblQuestionModule();
                        try{
                                tblQuestionModuleThis.setModuleId(Integer.parseInt(moduleId));
                                tblQuestionModuleThis.setModuleName(moduleName);
                                questionModuleService.updateTblQuestionModule(tblQuestionModuleThis);
                                
                            }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                            }finally{
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                                action=null;
                            }
                        response.sendRedirect("QuestionModule.jsp?msg=edtS");
                    }
         %>
        
    </head>
    <body>
        <%
            if(request.getParameter("moduleId")!=null)
            {
                    List<TblQuestionModule> tblQuestionModule = questionModuleService.findTblQuestionModule(Integer.parseInt(request.getParameter("moduleId")));
                    
                    
                    

         %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
       
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea"><div class="pageHead_1">Edit Question Module</div>
                        <form id="setQ" name="setQ" action="editModule.jsp" method="post">
                        <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                            <tr>
                                <td style="font-style: italic" class="t-align-right" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <tr>
                                <td class="ff">Module Name : <span>*</span></td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="text" id="moduleName" name="moduleName" maxlength="450" value="<%=tblQuestionModule.get(0).getModuleName()%>">
                                    <input style="width:300px;" class="formTxtBox_1" type="hidden" id="moduleId" name="moduleId" maxlength="450" value="<%=tblQuestionModule.get(0).getModuleId()%>">
                                </td>
                            </tr>
                            </tbody>
                        </table>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" cellpadding="0" >
                                <tr>
                                    <td class="t-align-center" style="padding-right:110px;"><span class="formBtn_1"><input type="submit" name="button" id="button" value="Update" onclick="return validate();"/></span></td>
                                </tr>
                            </table>
                        </form>
                </td>
            </tr>
        </table>
                                    <%}%>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
</html>