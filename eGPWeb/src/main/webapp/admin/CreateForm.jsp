<%--
    Document   : CreateForm
    Created on : 24-Oct-2010, 4:49:09 PM
    Author     : yanki
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsSrvBoqMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TemplateSectionForm"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TemplateMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TemplateMasterImpl"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsTemplateSrvBoqDetail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TemplateSectionFormImpl"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.TemplateSection"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateSections"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="templateFrmSrBean" class="com.cptu.egp.eps.web.servicebean.TemplateFormSrBean" />
<%@page import="java.util.List" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    if (session.getAttribute("userId") != null) {
                        templateFrmSrBean.setLogUserId(session.getAttribute("userId").toString());
                    }
                    int formId = 0;
                    String strTitle = "Create Form";
                    boolean isEdit = false;
                    if (request.getParameter("formId") != null) {
                        formId = Integer.parseInt(request.getParameter("formId"));
                        isEdit = true;
                        strTitle = "Edit Form";
                    }

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=strTitle%></title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>


        <script type="text/javascript">
            $(document).ready(function(){

                $("#frmCreateForm").validate({

                    rules:{
                        formName:{required:true,maxlength:200},
                        noOfTables:{required:true,digits:true,range:[1,99]},
                        multipleTimeFill:{required:true},
                        boqForm:{required:true},
                        mandatoryForm:{required:true},
                        typeOfForm:{required:true}
                    },

                    messages:{
                        formName:{required:"<div class='reqF_1'>Please enter Form Name.</div>",
                        maxlength:"<div class='reqF_1'>Maximum 50 characters are allowed.</div>"},
                        noOfTables:{required:"<div class='reqF_1'>Please enter No of Tables.</div>",
                        range:"<div class='reqF_1'>Please enter value between 1 and 99.</div>",
                        digits:"<div class='reqF_1'>Please enter numeric value between (1 - 99).</div>"},
                        multipleTimeFill:{required:"<div class='reqF_1'>Please select Form to be filled multiple time or not.</div>"},
                        boqForm:{required:"<div class='reqF_1'>Please select Is Price Schedule / BOQ Form.</div>"},
                        mandatoryForm:{required:"<div class='reqF_1'>Please select Is Form Mandatory.</div>"},
                        typeOfForm:{required:"<div class='reqF_1'>Please select Form Type.</div>"}
                    }
                });
            });
        </script>


    </head>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="contentArea_1">
                <!--Middle Content Table Start-->
                <%
                            TemplateSectionFormImpl templateForm = (TemplateSectionFormImpl) AppContext.getSpringBean("AddFormService");
                            short templateId = 0;
                            int sectionId = 0;

                            if (request.getParameter("templateId") != null) {
                                templateId = Short.parseShort(request.getParameter("templateId"));
                            }
                            if (request.getParameter("sectionId") != null) {
                                sectionId = Integer.parseInt(request.getParameter("sectionId"));
                            }

                            int srvBoqDetailId = 0;
                            String srvBoqId = "";
                            String srvBoqType = "";
                            String frmName = "";
                            StringBuffer frmHeader = new StringBuffer();
                            StringBuffer frmFooter = new StringBuffer();
                            byte noOfTable = 0;
                            String isMultipleFill = "";
                            String isBOQ = "";
                            String isMandatoryFrm = "";
                            String typeOfForm = "";

                            if (isEdit) {
                                List<com.cptu.egp.eps.model.table.TblTemplateSectionForm> frm = templateFrmSrBean.getFormDetail(formId);
                                if (frm != null) {
                                    if (frm.size() > 0) {
                                        frmName = frm.get(0).getFormName();
                                        frmHeader.append(frm.get(0).getFormHeader());
                                        frmFooter.append(frm.get(0).getFormFooter());
                                        noOfTable = frm.get(0).getNoOfTables();
                                        isMultipleFill = frm.get(0).getIsMultipleFilling();
                                        isBOQ = frm.get(0).getIsPriceBid();
                                        isMandatoryFrm = frm.get(0).getIsMandatory();
                                        typeOfForm = frm.get(0).getFormType();
                                    }
                                    frm = null;
                                }
                                List<TblCmsTemplateSrvBoqDetail> list = templateForm.getCmsTemplateSrvBoqDetail(formId);
                                if(!list.isEmpty())
                                {
                                    srvBoqDetailId = list.get(0).getSrvBoqDtlId();
                                    srvBoqId = Integer.toString(list.get(0).getTblCmsSrvBoqMaster().getSrvBoqId());
                                    List<TblCmsSrvBoqMaster> srvmasterdata = templateForm.getSrvBoqMaster(Integer.parseInt(srvBoqId));
                                    srvBoqType = srvmasterdata.get(0).getSrvBoqType();
                                }
                            }
                %>
                <form action="<%=request.getContextPath()%>/CreateSTDForm?action=formCreation" method="post" id="frmCreateForm">
                    <input type="hidden" name="templateId" id="templateId" value="<%= templateId%>" />
                    <input type="hidden" name="sectionId" id="sectionId" value="<%= sectionId%>" />
                    <%if (isEdit) {%>
                    <input type="hidden" name="formId" id="formId" value="<%= formId%>" />
                    <%}%>
                    <div class="pageHead_1">
                        <%=strTitle%>
                        <span style="float: right; text-align: right;">
                            <a href="DefineSTDInDtl.jsp?templateId=<%= templateId%>" title="SBD Dashboard" class="action-button-goback">SBD Dashboard</a>
                        </span>
                    </div>
                    <table width="100%" cellspacing="0" class="tableView_1 t_space">
                        <tr>
                            <td style="font-style: italic" class="" align="right">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                        </tr>
                    </table>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1 t_space" width="100%">
                        <tr>
                            <td width="20%" class="ff">Form Name : <span>*</span></td>
                            <%if (isEdit) {%>
                            <td width="80%" ><textarea name="formName" rows="3" class="formTxtBox_1" id="formName" style="width:500px;"><%= frmName.replace("<br/>", "\n") %></textarea></td>
                            <%} else {%>
                            <td width="80%" ><textarea name="formName" rows="3" class="formTxtBox_1" id="formName" style="width:500px;"></textarea></td>
                            <%}%>
                        </tr>
                        <tr>
                            <td class="ff">Form Header :</td>
                            <td>
                                <%if (isEdit) {%>
                                <textarea cols="80" id="formHeader" name="formHeader" rows="10"><%= frmHeader%></textarea>
                                <%} else {%>
                                <textarea cols="80" id="formHeader" name="formHeader" rows="10"></textarea>
                                <%}%>
                                <script type="text/javascript">
                                    CKEDITOR.replace( 'formHeader',
                                    {
                                        fullPage : false
                                    });
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Form Footer : </td>
                            <td>
                                <%if (isEdit) {%>
                                <textarea cols="80" id="formFooter" name="formFooter" rows="10"><%= frmFooter%></textarea>
                                <%} else {%>
                                <textarea cols="80" id="formFooter" name="formFooter" rows="10"></textarea>
                                <%}%>
                                <script type="text/javascript">
                                    CKEDITOR.replace( 'formFooter',
                                    {
                                        fullPage : false
                                    });
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">No of Tables : <%if (!isEdit) {%><span>*</span><%}%></td>
                            <td>
                                <%if (isEdit) {%>
                                <%= noOfTable%>
                                <input name="noOfTables" type="hidden" class="formTxtBox_1" id="noOfTables" style="width:200px;" value="<%= noOfTable%>" />
                                <%} else {%>
                                <input name="noOfTables" type="text" class="formTxtBox_1" id="noOfTables" style="width:200px;" value="" />
                                <%}%>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Form to be filled multiple time : <span>*</span></td>
                            <td>
                                <select name="multipleTimeFill" class="formTxtBox_1" id="multipleTimeFill" style="width:200px;">
                                    <option value="">Select</option>
                                    <option value="yes" <%if ("yes".equals(isMultipleFill)) {
                                                    out.print("selected");
                                                }%>>Yes</option>
                                    <option value="no" <%if ("no".equals(isMultipleFill)) {
                                                    out.print("selected");
                                                }%>>No</option
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Is Price Schedule / BOQ Form? : <span>*</span></td>
                            <td>
                                <select name="boqForm" class="formTxtBox_1" id="boqForm" style="width:200px;">
                                    <option value="">Select</option>
                                    <option value="yes" <%if ("yes".equals(isBOQ)) {
                                                    out.print("selected");
                                                }%>>Yes</option>
                                    <option value="no" <%if ("no".equals(isBOQ)) {
                                                    out.print("selected");
                                                }%>>No</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Is Form Mandatory? : <span>*</span></td>
                            <td>
                                <select name="mandatoryForm" class="formTxtBox_1" id="mandatoryForm" style="width:200px;">
                                    <option value="">Select</option>
                                    <option value="yes"
                                            <%if ("yes".equalsIgnoreCase(isMandatoryFrm)) {
                                                            out.print("selected");
                                                        }%>
                                            >Yes</option>
                                    <option value="no"
                                            <%if ("no".equalsIgnoreCase(isMandatoryFrm)) {
                                                        out.print("selected");
                                                    }%>
                                            >No</option>
                                </select>
                            </td>
                        </tr>
                         <%
                        TemplateMaster templateMaster = (TemplateMaster) AppContext.getSpringBean("TemplateMasterService");
                        TemplateSection templateSectionService = (TemplateSection) AppContext.getSpringBean("TemplateSectionService");
                        TemplateSectionForm TemplateSectionForm = (TemplateSectionForm) AppContext.getSpringBean("AddFormService");
                        List<TblTemplateMaster> listt = templateMaster.getTemplateDtl(templateId);
                        List<TblTemplateSections> list = templateSectionService.getSectionType(sectionId);
                        /*      Added By Dohatec for checking goods type procurement Start */
                       if(!listt.isEmpty()){
                         //   if(("GOODS".equalsIgnoreCase(listt.get(0).getProcType()))){
                        %>
                        <tr>
                            <td class="ff">Type of Form : <span>*</span></td>
                            <td width="50%">
                                <%if(("GOODS".equalsIgnoreCase(listt.get(0).getProcType()))){%>
                                <input type ="radio" class="formTxtBox_1" name="typeOfForm" id="typeOfForm1" value="manufactured"<%if ("manufactured".equalsIgnoreCase(typeOfForm)) {
                                                            out.print("checked");
                                                        }%> >Manufactured in Bhutan</input> &nbsp
                                <input type ="radio" class="formTxtBox_1" name="typeOfForm" id="typeOfForm2" value="imported" <%if ("imported".equalsIgnoreCase(typeOfForm)) {
                                                            out.print("checked");
                                                        }%>>To be imported or already imported</input> &nbsp
                                                        <%}%>
                                <input type ="radio" class="formTxtBox_1" name="typeOfForm" id="typeOfForm3" value="BOQsalvage" <%if ("BOQsalvage".equalsIgnoreCase(typeOfForm)) {
                                                            out.print("checked");
                                                        }%>>Discount Form</input> &nbsp
                                <input type ="radio" class="formTxtBox_1" name="typeOfForm" id="typeOfForm4" value="na" <%if ("na".equalsIgnoreCase(typeOfForm)) {
                                                            out.print("checked");
                                                        }%>>N/A</input>

                            </td>
                        </tr>
                        <%
                       // }
                       //  else {
                        %>  
                        <%--   <input type ="hidden" class="formTxtBox_1" name="typeOfForm" id="typeOfForm4" value="na"/> --%>
                        <%
                         //   }
                         }
                         /*      Added By Dohatec for checking goods type procurement End */
                            if(!listt.isEmpty())
                            {
                                if("srvcmp".equalsIgnoreCase(listt.get(0).getProcType()) ||"srvindi".equalsIgnoreCase(listt.get(0).getProcType()) ||"srvnoncon".equalsIgnoreCase(listt.get(0).getProcType()))
                                {
                                    if(!list.isEmpty())
                                    {
                                        if("TOR".equalsIgnoreCase(list.get(0).getContentType()) || "Form".equalsIgnoreCase(list.get(0).getContentType()))
                                        {
                                             List<Object[]> getSrvBoqMasterdata = TemplateSectionForm.getCmsSrvBoqMasterdata(sectionId);
                                             if(!getSrvBoqMasterdata.isEmpty())
                                             {
                        %>
                                             <tr>
                                             <td class="ff">Type of Form : <span>*</span></td>
                                             <td>
                        <%                      if(isEdit)
                                                {
                        %>
                                                    <input type="radio" name="rad" value="<%=srvBoqId%>" checked="checked"/> <%=srvBoqType%>
                        <%
                                                }
                                                else
                                                {
                                                    for(int i=0; i<getSrvBoqMasterdata.size(); i++)
                                                    {
                                                        Object[] obj = getSrvBoqMasterdata.get(i);
                        %>
                                                    <input type="radio" name="rad" value="<%=obj[0].toString()%>" /> <%=obj[1].toString()%>&nbsp;
                        <%                          }
                        %>
                                                 <input type="radio" name="rad" value="17" checked/> Others
                                              <%}%>
                                             </td>
                                             </tr>
                        <%
                                            }else{
                                                 if(isEdit){
                        %>
                                                <tr>
                                                    <td class="ff">Type of Form : <span>*</span></td>
                                                    <td><input type="radio" name="rad" value="<%=srvBoqId%>" checked="checked"/> <%=srvBoqType%></td>
                                                </tr>
                        <%
                                                 }else{
                        %>
                                             <tr>
                                             <td class="ff">Type of Form : <span>*</span></td>
                                             <td><input type="radio" name="rad" value="17" checked/> Others</td>
                                             </tr>
                        <%
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        %>
                        <tr>
                            <td class="ff">&nbsp;</td>
                            <td><label class="formBtn_1">
                                    <%if (isEdit) {%>
                                    <input type="submit" name="btnCreateEdit" id="btnCreateEdit" value="Edit" />
                                    <%} else {%>
                                    <input type="submit" name="btnCreateEdit" id="btnCreateEdit" value="Create" />
                                    <%}%>
                                </label></td>
                        </tr>
                    </table>
                    <div>&nbsp;</div>
                </form>
            </div>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <script type="text/javascript" >
            var headSel_Obj = document.getElementById("headTabSTD");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
<%
            if (templateFrmSrBean != null) {
                templateFrmSrBean = null;
            }
%>
