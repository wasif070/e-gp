<%-- 
    Document   : ViewGovtUserFinancePower
    Created on : Nov 13, 2010, 3:10:05 PM
    Author     : parag
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="govSrUser" class="com.cptu.egp.eps.web.servicebean.GovtUserSrBean" />
<jsp:useBean id="projSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectSrBean" />
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod,java.util.List,com.cptu.egp.eps.model.view.VwEmpfinancePower" %>
<%
            //govUser.getRole();
        List<TblProcurementMethod>  procurementMethod = projSrUser.getProcurementMethods();
            String str = request.getContextPath();
            int empId = 0;
            if (request.getParameter("empId") != null) {
                empId = Integer.parseInt(request.getParameter("empId"));
                govSrUser.setEmpId(empId);
            }
            

            String finPowerBy="";
            String roleId="";
            String roleName="";
            String empRoleId="";
            short deptId=0;

            if(request.getParameter("finPowerBy") != null){
                finPowerBy = request.getParameter("finPowerBy");
            }
            if(request.getParameter("roleId") != null){
                roleId = request.getParameter("roleId");
            }
            if(request.getParameter("roleName") != null){
                roleName = request.getParameter("roleName");
            }
            if(request.getParameter("empRoleId") != null){
                empRoleId = request.getParameter("empRoleId");
            }

            if(request.getParameter("deptId") != null){
                deptId = Short.parseShort(request.getParameter("deptId"));
                govSrUser.setDepartmentId(deptId);
            }

            List<VwEmpfinancePower> vwFinancePower=govSrUser.getGovtFinanceRole();
            
%>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP View Government Financial Power</title>
         <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
         <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%--<jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>--%>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1"><div class="pageHead_1">View Financial Power</div>
                                        <%-- <div class="pageHead_1">Create User</div>--%>
                                        <%--<div class="pageHead_1">Assign Financial Power</div>--%>

                                        <div id="successMsg" class="responseMsg successMsg" style="display:none"></div>
                                        <div id="errMsg" class="responseMsg errorMsg" style="display:none"></div>
                                        <%-- <div class="responseMsg noticeMsg">Notification Msg <a href="#">Link</a></div>--%>

                                        <form name="frmFinance" id="frmFinance"  method="post" action="../GovtUserSrBean?action=createGovtFinanceRole">                                            
                                            <div>&nbsp;&nbsp;</div>
                                            <table align="" id="table0" width="80%" cellspacing="0" class="tableList_1 t_space">
                                                <tr>
                                                    <%--<th class="t-align-center" width="5">S. No.</th>--%>
                                                    <th class="t-align-left">Procurement Category </th>
                                                    <th class="t-align-left">Procurement Method  </th>
                                                    <th class="t-align-left">Budget Type  </th>
                                                    <th class="t-align-left">Operator  </th>
                                                    <th class="t-align-left">Amount (In Million Nu.)  </th>
                                                </tr>
                                                <tbody id="tbody0"  align="">
                                                    <% int count = 0;
                                                    short tmpDeptId=0;
                                                    int tmpEmpRoleId=0;
                                                    //java.text.DecimalFormat fmt = new java.text.DecimalFormat("#.##");
                                                    if(vwFinancePower != null){
                                                    if(vwFinancePower.size()>0){
                                                    for (VwEmpfinancePower financePower : vwFinancePower) {
                                                        count++;
                                                        int cnt = -1;

                                                        if(count == 1){
                                                            tmpEmpRoleId = financePower.getId().getEmployeeRoleId();
                                                        }
                                                        if(tmpEmpRoleId == financePower.getId().getEmployeeRoleId()){
                                                    %>
                                                    <tr  id="tr0" >
                                                        <%--<td ><%=count%>
                                                        </td>--%>
                                                        <td id="td" >
                                                            <label ><%=financePower.getId().getProcurementNature()%></label>                                                          
                                                        </td>
                                                        <td id="td" >
                                                            <label ><%=financePower.getId().getProcurementMethod()%></label>                                                          
                                                        </td>
                                                        <td id="td" >
                                                            <label ><%=financePower.getId().getBudgetType()%></label>                                                          
                                                        </td>
                                                        <td id="td" >
                                                            <label ><%=financePower.getId().getOperator()%></label>                                                          
                                                        </td>
                                                        <td id="td" >
                                                            <label ><%=String.format("%.2f",financePower.getId().getAmount())%></label>
                                                        </td>
                                                    </tr>
                                                    <%
                                                            }
                                                        }
                                                       }
                                                       else{
                                                        out.print("<tr><td colspan='6'>Record Not Found.</td></tr>");
                                                       }
                                                      }
                                                      else{
                                                        out.print("<tr><td colspan='6'>Record Not Found.</td></tr>");
                                                      }
                                                    %>
                                                </tbody>
                                            </table>
                                            <!-- Buttons -->
                                            <table align="center" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                   
                                                </tr>
                                            </table>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <div>&nbsp;</div>

                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
                <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<%
            govSrUser = null;
%>

