<%-- 
    Document   : EditGovtUserCreateRoleOff
    Created on : Oct 30, 2010, 3:37:30 PM
    Author     : parag
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="govSrUser" class="com.cptu.egp.eps.web.servicebean.GovtUserSrBean" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.List,com.cptu.egp.eps.model.table.TblEmployeeRoles,com.cptu.egp.eps.model.table.TblDepartmentMaster" %>
<%@page import="com.cptu.egp.eps.model.table.TblEmployeeOffices" %>
<%
            String str = request.getContextPath();
            int empId = 0;
            int userId = 0;
            short departmentId = 0;
            int designationId = 0;

            if (request.getParameter("empId") != null) {
                empId = Integer.parseInt(request.getParameter("empId"));
                govSrUser.setEmpId(empId);
            }

            if (request.getParameter("userId") != null) {
                userId = Integer.parseInt(request.getParameter("userId"));
            }
            //govSrUser.setEmpId(empId);

            List<TblEmployeeRoles> empRoles = govSrUser.getEmployeeRoles();
            
            departmentId = empRoles.get(0).getTblDepartmentMaster().getDepartmentId();
            govSrUser.setDepartmentId(departmentId);

            TblDepartmentMaster departmentMaster = govSrUser.getDepartmentMasterData();
            govSrUser.setUserId(userId);
            
            Object[] object = govSrUser.getGovtUserData();
            designationId = (Integer) object[6];

            
            int j = 0;
            //int k=0;
%>
<script>
    var rolesArray = new Array('<%=empRoles.size()%>');
    var len=0;
</script>
<%
            for (int k = 0; k < (empRoles.size()); k++) {
%>
<script>             
    rolesArray.push('<%=empRoles.get(k).getTblProcurementRole().getProcurementRoleId()%>');
    len++;
</script>
<%
            }
%>
<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - Govt User Role Creation</title>
         <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
         <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmRole").validate({
                    rules: {
                        txtdepartment : { required: true },
                        office: { required: true }
                    },
                    messages: {
                        txtdepartment: { required: "<div class='reqF_1'>Please Select Department</div>"},
                        office: { required: "<div class='reqF_1'>Please Select Office</div>"}
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "office")
                            error.insertAfter("#office");
                        else
                            error.insertAfter(element);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            <%-- $(function() {
                 if(document.getElementById("txtdepartmentid") != ""){
                 $('#txtdepartmentid').blur(function() {
                     $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId:$('#txtdepartmentid').val(),funName:'Office'}, function(j){
                         $("select#cmbOffice").html(j);
                     });
                 });
                 }
             });--%>


                 $(document).ready(function() {
                     $.post("<%=request.getContextPath()%>/GovtUserSrBean",{objectId:<%=departmentId%>,employeeId:<%=empId%>,funName:'SOffice'}, function(j){
                         $("td#tdOffice").html(j);
                         //alert(j);
                     });
                 });

                 $(document).ready(function() {
                     $.post("<%=request.getContextPath()%>/GovtUserSrBean",{objectId:<%=departmentId%>,designationId:<%=designationId%>,funNameforSecond:'SDesignation'}, function(j){
                         $("select#cmbDesig").html(j);
                         //alert(j);
                     });
                 });

                 $(document).ready(function() {
  
                     if(rolesArray.length > 0){
                         for(var j=0;j < rolesArray.length;j++){

                             var value =rolesArray.pop();
                            // alert('value is :::'+value);
                       
                             if($('input:radio[id=rdCCGP]').val() == value ){
                                 document.getElementById('trCcgp').style.display='block';
                                 $('input:radio[id=ccgp]').attr('checked',true);
                             }
                             if($('input:radio[id=rdProcRoleGrOneA]').val() == value ){
                                 document.getElementById('trProcRoleGrOne').style.display='block';
                                 $('input:radio[id=rdProcRoleGrOneA]').attr('checked',true);
                             }
                             if($('input:radio[id=rdProcRoleGrOneB]').val() == value ){
                                 document.getElementById('trProcRoleGrOne').style.display='block';
                                 $('input:radio[id=rdProcRoleGrOneB]').attr('checked',true);
                             }
                             if($('input:radio[id=rdProcRoleGrOneB]').val() == value ){
                                 document.getElementById('trProcRoleGrOne').style.display='block';
                                 $('input:radio[id=rdProcRoleGrOneB]').attr('checked',true);
                             }

                             if($('input:radio[id=rdProcRoleGrTwoA]').val() == value ){
                                 document.getElementById('trProcRoleGrTwo').style.display='block';
                                 $('input:radio[id=rdProcRoleGrTwoA]').attr('checked',true);
                             }
                             if($('input:radio[id=rdProcRoleGrTwoB]').val() == value ){
                                 document.getElementById('trProcRoleGrTwo').style.display='block';
                                 $('input:radio[id=rdProcRoleGrTwoB]').attr('checked',true);
                             }
                             if($('input:radio[id=rdProcRoleGrThreeA]').val() == value ){
                                 document.getElementById('trProcRoleGrThree').style.display='block';
                                 $('input:radio[id=rdProcRoleGrThreeA]').attr('checked',true);
                             }
                             if($('input:radio[id=rdProcRoleGrThreeB]').val() == value ){
                                 document.getElementById('trProcRoleGrThree').style.display='block';
                                 $('input:radio[id=rdProcRoleGrThreeB]').attr('checked',true);
                             }
                         }
                     }else{
                         if($('input:radio[name=ccgp]').val() == value ){
                             document.getElementById('trCcgp').style.display='block';
                             $('input:radio[name=ccgp]').attr('checked',false);
                         }
                     }
                 });

                 $(function() {;
                     document.getElementById("tdOffice").innerHTML="";                      
                     $('#txtdepartment').blur(function() {
                        
                         if($('#txtdepartmentid').val() != <%=departmentId%> ){
                         $.post("<%=request.getContextPath()%>/GovtUserSrBean",{objectId:$('#txtdepartmentid').val(),funName:'Office'}, function(j){
                             $("td#tdOffice").html(j);
                         });
                         }
                         else{
                            // alert(' asdfdf =====>>');
                         }
                     });                      
                 });

                 $(function() {;
                     document.getElementById("tdOffice").innerHTML="";
                     $('#txtdepartment').blur(function() {
                         $.post("<%=request.getContextPath()%>/GovtUserSrBean",{objectId:$('#txtdepartmentid').val(),funName:'Office'}, function(j){
                             $("td#tdOffice").html(j);
                         });
                     });
                     // }
                 });

                 $(function() {
                     document.getElementById("cmbDesig").options.length=0;
                     if(document.getElementById("txtdepartmentid") != ""){
                         $('#txtdepartment').blur(function() {
                             //if($('#txtdepartmentid').val() != <%=departmentId%> ){
                                 $('input:radio[name=ccgp]').attr('checked',false);
                                 $('input:radio[name=procRoleGrOne]').attr('checked',false);
                                 $('input:radio[name=procRoleGrTwo]').attr('checked',false);
                                 $('input:radio[name=procRoleGrThree]').attr('checked',false);
                             //}
                         });
                     }
                 });

                 function checkCondition(){
            
                     if(document.getElementById("txtDepartmentType").value == "CCGP"){
                         document.getElementById("trCcgp").style.display="block";
                         //for none
                         //document.getElementById("rdCCGP").checked = false;
                         document.getElementById("trProcRoleGrOne").style.display="none";
                         document.getElementById("trProcRoleGrTwo").style.display="none";
                         document.getElementById("trProcRoleGrThree").style.display="none";
                     }
                     else if(document.getElementById("txtDepartmentType").value == "CabinetDivision"){
                         document.getElementById("trProcRoleGrOne").style.display="block";
                         document.getElementById("tdGrOneA").style.display="none";
                         document.getElementById("tdGrOneC").style.display="none";
                         //for none
                         document.getElementById("trCcgp").style.display="none";
                         document.getElementById("trProcRoleGrTwo").style.display="none";
                         document.getElementById("trProcRoleGrThree").style.display="none";
                     }
                     else if(document.getElementById("txtDepartmentType").value == "Ministry"){
                         document.getElementById("trCcgp").style.display="block";
                         document.getElementById("trProcRoleGrOne").style.display="block";
                         document.getElementById("trProcRoleGrTwo").style.display="block";
                         document.getElementById("trProcRoleGrThree").style.display="block";
                         document.getElementById("tdGrOneC").style.display="none";
                         alert(document.getElementsById("rdCCGP").checked);
                         document.getElementsById("rdCCGP").checked = false;
                     }
                     else if(document.getElementById("txtDepartmentType").value == "Division"){
                         document.getElementById("trProcRoleGrOne").style.display="block";
                         document.getElementById("tdGrOneA").style.display="none";
                         document.getElementById("tdGrOneC").style.display="none";
                         document.getElementById("trProcRoleGrTwo").style.display="block";
                         document.getElementById("trProcRoleGrThree").style.display="block";
                         //for none
                         document.getElementById("trCcgp").style.display="none";
                     }
                     else if(document.getElementById("txtDepartmentType").value == "Organization"){
                         document.getElementById("trProcRoleGrOne").style.display="block";
                         document.getElementById("tdGrOneA").style.display="none";
                         document.getElementById("tdGrOneB").style.display="none";
                         //for none
                         document.getElementById("trCcgp").style.display="none";
                        // document.getElementById("trProcRoleGrTwo").style.display="none";
                        // document.getElementById("trProcRoleGrThree").style.display="none";
                     }
                     // document.getElementById("trProcRoleGrOne").style.visibility="visible";
                 }

                 function forMinistry(){
                     if(document.getElementById("rdProcRoleGrOneA").checked == true ){
                         document.getElementById("trProcRoleGrTwo").style.display="none";
                         document.getElementById("trProcRoleGrThree").style.display="none";
                     }
                     else if(document.getElementById("rdProcRoleGrOneB").checked == true ){
                         document.getElementById("trProcRoleGrTwo").style.display="block";
                         document.getElementById("trProcRoleGrThree").style.display="block";
                     }

                 }

                 function checkRole(){
                     var count=0;
                     if(document.getElementById("rdCCGP").checked == true){
                         count++;
                     }
                     if(document.getElementById("rdProcRoleGrOneA").checked == true){
                         count++;
                     }
                     if(document.getElementById("rdProcRoleGrOneB").checked == true){
                         count++;
                     }
                     if(document.getElementById("rdProcRoleGrOneC").checked == true){
                         count++;
                     }
                     if(document.getElementById("rdProcRoleGrTwoA").checked == true){
                         count++;
                     }
                     if(document.getElementById("rdProcRoleGrTwoB").checked == true){
                         count++;
                     }
                     if(document.getElementById("rdProcRoleGrThreeA").checked == true){
                         count++;
                     }
                     if(document.getElementById("rdProcRoleGrThreeB").checked == true){
                         count++;
                     }
                     if(count <= 1){
                         document.getElementById("rdAssFinPowerU").checked = true;
                         document.getElementById("lbAssFinPower").innerHTML = "(Please Select Multipal Procurement Role To Assign Role Specific Financial Power.)";
                         document.getElementById("lbAssFinPower").style.color='red';
                     }
                 }

                 function validateData(){

                     if( document.getElementById("rdCCGP").checked == false &&
                         document.getElementById("rdProcRoleGrOneA").checked == false &&
                         document.getElementById("rdProcRoleGrOneB").checked == false &&
                         document.getElementById("rdProcRoleGrOneC").checked == false &&
                         document.getElementById("rdProcRoleGrTwoA").checked == false &&
                         document.getElementById("rdProcRoleGrTwoB").checked == false &&
                         document.getElementById("rdProcRoleGrThreeA").checked == false &&
                         document.getElementById("rdProcRoleGrThreeB").checked == false
                 ){
                         document.getElementById("errMsg").innerHTML="";
                         document.getElementById("errMsg").style.display='block';
                         document.getElementById("errMsg").innerHTML ='Please Select Procurement Role.';
                         return false;
                     }else{
                         document.getElementById("errMsg").innerHTML="";
                         document.getElementById("errMsg").style.display='none';
                         //document.forms[0].action ="";
                         //document.forms[0].submit();
                         return true;
                     }

                 }


        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1"><div class="pageHead_1">Create User</div>
                                        <%--<div class="pageHead_1">Create User</div>--%>
                                        <div id="successMsg" class="responseMsg successMsg" style="display:none"></div>
                                        <div id="errMsg" class="responseMsg errorMsg" style="display:none"></div>
                                        <form name="frmRole" id="frmRole" action="../GovtUserSrBean?action=editGovtUserRole" method="POST"  >
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td width="125" class="ff">Select Office Level: <span>*</span></td>
                                                    <td>
                                                        <input class="formTxtBox_1" name="txtDepartmentType" type="text" id="txtDepartmentType"
                                                               value="<%out.print(departmentMaster.getDepartmentType());%>"
                                                               readonly style="width: 200px;"/>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="ff" width="200">Select Department<span>*</span></td>
                                                    <td><input class="formTxtBox_1" name="txtdepartment" type="text" id="txtdepartment"
                                                               value="<%out.print(departmentMaster.getDepartmentName());%>"
                                                               onblur="checkCondition();"  readonly style="width: 200px;"  />
                                                        <input class="formTxtBox_1" name="txtdepartmentid" type="text"
                                                               value="<%out.print(departmentMaster.getDepartmentId());%>"
                                                               id="txtdepartmentid"  style="width: 200px;"/>
                                                        <input type="button" value="New Window!" onClick="window.open('DepartmentSelection.jsp','window','width=400,height=200')">
                                                    </td>
                                                </tr>

                                                <tr id="trOffice">
                                                    <td class="ff">Select Office : <span>*</span></td>
                                                    <td id="tdOffice">
                                                    </td>

                                                </tr>

                                                <tr id="trDesignataion">
                                                    <td class="ff">Select Designataion : <span>*</span></td>
                                                    <td><select name="designation"  id="cmbDesig" class="formTxtBox_1">
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>

                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">

                                                <td class="ff" width="125"> Procurement Role :<span>*</span> </td>
                                                <td>
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="radioList_1">
                                                        <tr id="trCcgp" style="display:none" >
                                                            <td id="tdccgp" ><input type="radio" name="ccgp" id="rdCCGP" value="7"
                                                                                    />
                                                                CCGP</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr id="trProcRoleGrOne" style="display:none">
                                                            <td id="tdGrOneA"><input type="radio" name="procRoleGrOne" id="rdProcRoleGrOneA" value="3" onclick="forMinistry();" />
                                                                Minister</td>
                                                            <td id="tdGrOneB"><input type="radio" name="procRoleGrOne" id="rdProcRoleGrOneB" value="2" onclick="forMinistry();"/>
                                                                Secretary</td>
                                                            <td id="tdGrOneC"><input type="radio" name="procRoleGrOne" id="rdProcRoleGrOneC" value="4" />
                                                                BoD</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr id="trProcRoleGrTwo" style="display:none">
                                                            <td id="tdGrTwoA"><input type="radio" name="procRoleGrTwo" id="rdProcRoleGrTwoA" value="6" />
                                                                HOPA</td>
                                                            <td id="tdGrTwoB"><input type="radio" name="procRoleGrTwo" id="rdProcRoleGrTwoB" value="5" />
                                                                Authorized Officer </td>
                                                        </tr>
                                                        <tr id="trProcRoleGrThree" style="display:none">
                                                            <td id="tdGrThreeA"><input type="radio" name="procRoleGrThree" id="rdProcRoleGrThreeA" value="1" />
                                                                PE</td>
                                                            <td id="tdGrThreeA"><input type="radio" name="procRoleGrThree" id="rdProcRoleGrThreeB" value="5" />
                                                                Authorized User </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                            </table>
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <td class="ff" width="125"> Assign Financial power :<span>*</span> </td>
                                                <td>
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="radioList_1">
                                                        <tr id="trAssFinPower">
                                                            <td id="tdAssFinPowerU"><input type="radio" name="assFinPower" id="rdAssFinPowerU" value="user" checked />
                                                                User specific</td>
                                                            <td id="tdAssFinPowerR"><input type="radio" name="assFinPower" id="rdAssFinPowerR" value="role" onclick="return checkRole();" />
                                                                Role specific </td>
                                                            <td id="tdGrThreeA"><label name="lbAssFinPower" id="lbAssFinPower" style="color:red" > (&nbsp;Please Select Multipal Procurement Role To Assign Role Specific Financial Power.) </label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </table>
                                            <!-- Buttons -->
                                            <table align="center" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td><label class="formBtn_1">
                                                            <input type="submit" name="btnAdd" id="btnAdd" value="Add Procuremnet Role" onclick="return validateData();" />
                                                        </label>
                                                        &nbsp;
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnRemove" id="btnRemove" value="Remove Procurement Role" />
                                                        </label>
                                                        &nbsp;
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnNext" id="btnNext" value="Next" />
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <input type="hidden" name="empId" id="empId" value="<%=govSrUser.getEmpId()%>" />
                                                        <input type="hidden" name="designationId" id="designationId" value="<%=designationId%>" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <div>&nbsp;</div>

                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
                <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
 <script type="text/javascript">
 $(document).ready(function() {
        $('#txtdepartment').focus();
    });
 </script>
