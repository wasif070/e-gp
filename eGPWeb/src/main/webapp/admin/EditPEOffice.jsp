<%-- 
    Document   : EditPEOffice
    Created on : Oct 28, 2010, 10:57:45 PM
    Author     : rishita
--%>


<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.model.table.TblOfficeMaster" %>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PA Office details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <jsp:useBean id="peOfficeCreationSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.PEOfficeCreationSrBean"/>
        <jsp:useBean id="peOfficeCreationDtBean" scope="request" class="com.cptu.egp.eps.web.databean.PEOfficeCreationDtBean"/>
        <jsp:setProperty name="peOfficeCreationDtBean" property="*"/>
        <%
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        peOfficeCreationSrBean.setLogUserId(session.getAttribute("userId").toString());
                    }
        %>

        <script type="text/javascript">
            $(document).ready(function() {
                $.validator.addMethod(
                        "regex",
                        function (value, element, regexp) {
                            var check = false;
                            return this.optional(element) || regexp.test(value);
                        },
                        "Please check your input."
                        );
                $("#frmEditPEOffice").validate({
                    rules: {
                        txtdepartment: {required: true},
                        //officeName: { required: true,maxlength:150,alNumRegex: true },
                        deptBanglaString: { maxlength:100 },
                        peCode:{maxlength:50},
                        address:{required:true,maxlength:1000},
                        //countryId:{required:true},
                        State:{required:true},
                        //city: { requiredWithoutSpace: true,spacevalidate:true,alphaCity: true,maxlength:100},
                        //upjilla: { requiredWithoutSpace: true,spacevalidate:true,alphaCity: true,maxlength:100 },
                        //postCode: {required:true,alphanumeric:true,maxlength:10 },
                        postCode:{number: true, minlength:4},
                        phoneNo: {required: true, regex: /^[0-9]+-[0-9,]*[0-9]$/},
                        faxNo: {regex: /^[0-9]+-[0-9,]*[0-9]$/}
                    },
                    messages: {
                        txtdepartment: { required: "<div  class='reqF_1'>Please select Department.</div>"},
                        //officeName: { required: "<div class='reqF_1'>Please enter Office Name.</div>",
                        //  alNumRegex: "<div class='reqF_1' id='oName'  name='oName'>Allows characters (a to z),Special characters (.-/ &) only</div>",
                        // maxlength:"<div class='reqF_1'>Maximum 150 characters are allowed.</div>"},
                        deptBanglaString: { maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"},
                        peCode:{ maxlength:"<div class='reqF_1'>Maximum 50 characters are allowed.</div>"
                        },
                        address:{required: "<div class='reqF_1'>Please  enter Address.</div>",
                            maxlength:"<div class='reqF_1'>Only 1000 characters are allowed.</div>"
                        },
                        //countryId:{required: "<div class='reqF_1'>Please select Country.</div>"
                        //},
                        State:{required:"<div class='reqF_1'>Please enter Dzongkhag / District Name.</div>"
                        },
//                        city: { requiredWithoutSpace: "<div class='reqF_1'>Please enter City / Town Name</div>",
//                            alphaCity:"<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , ' \" } { - . _) Only</div>",
//                            maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>",
//                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>"},
//                        upjilla: { requiredWithoutSpace: "<div class='reqF_1'>Please enter Thana / UpaZilla</div>",
//                            alphaCity: "<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , ' \" } { - . _) Only</div>",
//                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>",
//                            maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>"},
                        postCode:{ number:"<div class='reqF_1'>Please enter digits (0-9) only</div>",
                                    maxlength:"<div class='reqF_1'>Maximum 4 digits are allowed</div>",
                                    minlength:"<div class='reqF_1'>Minimum 4 digits required</div>"},
                        
                        phoneNo: {required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            regex: "<div class='reqF_1'>Area code should contain minimum 2 digits and maximum 5 digits.</br>Phone no. should contain minimum 3 digits and maximum 10 digits.</br>In case of more than one Phone No. use comma.</br>Do not repeat Area Code.</div>"
                        },
                        faxNo: {regex: "<div class='reqF_1'>Area code should contain minimum 2 digits and maximum 5 digits.</br>Fax no. should contain minimum 3 digits and maximum 10 digits.</br>In case of more than one Fax No. use comma.</br>Do not repeat Area Code.</div>"
                        }
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "txtdepartment")
                            error.insertAfter("#btnnewbutton");
                        else if(element.attr("name")=="phoneNo")
                        {
                            error.insertAfter("#phno");
                        }
                        else if(element.attr("name")=="faxNo")
                        {
                            error.insertAfter("#fxno");
                        }
                        else
                            error.insertAfter(element);
                    }

                });
            });

            var bvalidationPostCode = true;
            var bvalidation;
            function clrMsg(){
                bvalidation = true;
                if($('#txtOfficeName').val()==''){
                    $('#errMsg').html('Please enter Office Name');
                    bvalidation = false;
                }
                
            }
            
          function chkregPostCode(value)
        {
            return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

        }
        </script>

    </head>
    <script type="text/javascript">
        $(function() {
            $('#cmbCountry').change(function() {
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbCountry').val(),funName:'stateCombo'},  function(j){
                    $("#cmbState").html(j);
                });
            });
        });
        $(function () {
                $('#cmbState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue2'}, function (j) {
                        $('#txtCity').html(j);
                    });
                });
            });
            $(function () {
                $('#cmbState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtupJilla').html(j);
                    });
                });
            });
            $(function () {
                $('#txtCity').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#txtCity').val(), funName: 'subDistrictComboValue4'}, function (j) {
                        $('#txtupJilla').html(j);
                    });
                });
            });
    </script>
    <!--                    <script type="text/javascript">
                $(function() {
                    $('#txtOfficeName').change(function() {

                        $.post("<-%=request.getContextPath()%>/CommonServlet", {officeName:$('#txtOfficeName').val(),deptId:$('#txtchk').val(),funName:'verifyOfficeName'},
                        function(j)
                        {
                            if(j.toString().indexOf("OK", 0)!=-1){
                                $('span.#Msg').css("color","green");
                            }
                            else if(j.toString().indexOf("Office", 0)!=-1){
                                $('span.#Msg').css("color","red");

                            }
                            $('span.#Msg').html(j);
                        });
                    });
                });
            </script>-->
    <!--    <script type="text/javascript">
            $(function() {
                $('#frmEditPEOffice').submit(function() {
                    if(($('span.#Msg').html()=="OK") || ($('span.#Msg').html()=="Ok")){
                        return true;
                    }else{
                        return false;
                    }
                });
            });
        </script>-->
    <body>
        <%
                    int officeid = 0;
                    if (!request.getParameter("officeid").equals("")) {
                        officeid = Integer.parseInt(request.getParameter("officeid"));
                    }

                    String departmentType = "";
                    if (!request.getParameter("deptType").equals("")) {
                        departmentType = request.getParameter("deptType");
                    }

                    StringBuilder userType = new StringBuilder();
                    if (request.getParameter("hdnUserType") != null) {
                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                            userType.append(request.getParameter("hdnUserType"));
                        } else {
                            userType.append("org");
                        }
                    } else {
                        userType.append("org");
                    }
                    if ("Update".equals(request.getParameter("Update"))) {
                        peOfficeCreationDtBean.setOfficeName(peOfficeCreationDtBean.getOfficeName().trim());
                        peOfficeCreationSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                        String parseMe = request.getParameter("stateId");
                        peOfficeCreationDtBean.setStateId(parseMe);
                        if(peOfficeCreationSrBean.updateOfficeMaster(peOfficeCreationDtBean)){
                        response.sendRedirect("PEOfficeView.jsp?officeid=" + officeid + "&departmentType=" + departmentType + "&msg=success");
                        }else{
                            response.sendRedirect("EditPEOffice.jsp?officeid=" + officeid + "&deptType=" + departmentType + "&msg=updateFail");
                        }
                    } else {
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1">
                            <div class="pageHead_1">Edit PA Office Details</div>
                            <!--Page Content Start-->
                            <%if (request.getParameter("msg") != null && request.getParameter("msg").equals("updateFail")) {%>
                            <div class='responseMsg errorMsg'><%=appMessage.peOfficeUpdate %></div>
                            <%}%>
                            <form id="frmEditPEOffice" name="frmEditPEOffice" action="" method="post">

                                <table width="100%" cellspacing="10" class="formStyle_1" border="0" cellpadding="0">
                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                    </tr>
                                    <%

                                                            for (TblOfficeMaster tom : peOfficeCreationSrBean.getFindUser(officeid)) {
                                    %>
                                    <tr>
                                        <td class="ff" width="200"><% if(departmentType.equalsIgnoreCase("Organization")){out.print("Division");}else if(departmentType.equalsIgnoreCase("Division")){out.print("Department");}else {out.print(departmentType);} %> Name :</td>
                                        <td><label><%= peOfficeCreationSrBean.getDefaultDept(tom.getDepartmentId()).get(0).getDepartmentName()%></label>
                                            <input class="formTxtBox_1" value="<%= peOfficeCreationSrBean.getDefaultDept(tom.getDepartmentId())%>" name="txtdepartment" type="hidden" id="txtdepartment" style="width: 200px;"/>
                                            <input class="formTxtBox_1" name="txtdepartmentid" type="hidden" value="<%= tom.getDepartmentId()%>" id="txtdepartmentid" style="width: 200px;"/>
                                            <%--<input id="btnnewbutton" name="btnnewbutton" type="submit" value="New Window!" onClick="window.open('DepartmentSelection.jsp','window','width=400,height=200')">--%>
                                            <input type="hidden" id="txtchk" name="txtchk" value="<%=tom.getDepartmentId()%>"/>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Office Name : <span>*</span></td>
                                        <td>
                                            <input type="text" name="officeName" id="txtOfficeName" style="width: 400px;" class="formTxtBox_1" value="<%= tom.getOfficeName()%>" onblur="chkUnique();" maxlength="101"/>
                                            <div id="errMsg" style="color: red;"></div>
                                            <input type="hidden" name="hiddenofficeName" value="<%=tom.getOfficeName()%>" id="hiddenofficeName"/>
                                            <div id="Msg" style="color: red; font-weight: bold"></div>
                                            <input type="hidden" name="strCreatedDate" value="<%= tom.getCreationDate()%>" id="txtcreatedDate"/>
                                            <input type="hidden" name="officeId" value="<%= officeid%>" id="t"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Office Name in Dzongkha :</td>
                                        <td>
                                            <%                                                      String str = "";
                                                                                                            //String value = new String(tdm.getDeptNameInBangla());
                                                                                                            if (tom.getOfficeNameInBangla() == null) {
                                                                                                            } else {
                                                                                                                str = new String(tom.getOfficeNameInBangla(), "UTF-8");
                                                                                                                //request.setCharacterEncoding("UTF-8");
                                                                                                            }
                                            %>
                                            <input type="text" name="deptBanglaString" id="txtOfficeNameBangla" style="width: 400px;" class="formTxtBox_1" value="<%=str%>" maxlength="101"/>
                                        </td>
                                    </tr>
                                    <%if (departmentType.equals("Organization") && peOfficeCreationSrBean.getDefaultDept(tom.getDepartmentId()).get(0).getOrganizationType().equals("no")){ %>
                                    <tr>
                                        <td class="ff" width="200">Office Type :<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbOfcType" name="officeType" style="width: 205px" >
                                                <%
                                                String[] ofcType = {"Division","District","Upazila","Circle","Others"};
                                                for(String ofcItem:ofcType){
                                                    if(ofcItem.contains(tom.getOfficeType())){
                                                         // if(tom.getOfficeType().equals("Others")){%>
                                                <option value=<%=tom.getOfficeType()%> selected><%=tom.getOfficeType()%> Level </option>
                                                <%}else{ %>
                                                <option value=<%=ofcItem%>><%=ofcItem%> Level </option>
                                                <% }} %>
                                            </select>
                                        </td>
                                    </tr>
                                    <%}else{%>
                                    <select class="formTxtBox_1" id="cmbOfcType" name="officeType" style="width: 205px;display: none" >
                                        <option value="Others" selected> Others</option>
                                    </select>
                                    <%}%>

                                    <tr>
                                        <td class="ff" width="200">PA Code :</td>
                                        <td>
                                            <input type="text" name="peCode" id="txtPECode" style="width: 200px;" class="formTxtBox_1" <% if (tom.getPeCode() != null) {%> value="<%= tom.getPeCode()%>" <% }%>/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Address : <span>*</span></td>
                                        <td>
                                            <textarea rows="5" id="txtAddress" name="address" style="width: 400px;" class="formTxtBox_1" onkeypress="return imposeMaxLength(this, 1001, event);"><%= tom.getAddress()%></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Country : </td>
                                        <td>Bhutan
                                            <select name="countryId" class="formTxtBox_1" id="cmbCountry" style="width: 205px;display:none">
                                                <%
                                                                                                                for (SelectItem country : peOfficeCreationSrBean.getCountryList()) {
                                                                                                                    if (country.getObjectValue().equals(tom.getTblCountryMaster().getCountryName())) {
                                                %>
                                                <option  value="<%=country.getObjectId()%>" selected="true"><%=country.getObjectValue()%></option>
                                                <%                                                                                                    } else {
                                                %>
<!--                                                <option  value="<%=country.getObjectId()%>"><%=country.getObjectValue()%></option>-->
                                                <%                                                                }
                                                                                                                }
                                                %>
                                            </select>
                                        </td>
                                    <tr>
                                        <td class="ff" width="200">Dzongkhag : <span>*</span></td>
                                        <td><select name="stateId" class="formTxtBox_1" id="cmbState">
                                                <%for (SelectItem state : peOfficeCreationSrBean.getStateList()) {
                                                
                                                    if(state.getObjectValue().equalsIgnoreCase(tom.getTblStateMaster().getStateName())){%>
                                                    <option value="<%=state.getObjectValue()%>,<%=state.getObjectId()%>" selected="selected"><%=state.getObjectValue()%></option>
                                                    <%}else{%>
                                                    <option value="<%=state.getObjectValue()%>,<%=state.getObjectId()%>"><%=state.getObjectValue()%></option>
                                                <%}}%>
                                            </select></td>
                                       
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Dungkhag : </td>
                                        <td>
                                            <select class="formTxtBox_1" id="txtCity" name="city">
                                                <option  value="<%= tom.getCity()%>"><%= tom.getCity()%></option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Gewog : <span>*</span></td>
                                        <td>
                                            <select class="formTxtBox_1" id="txtupJilla" name="upjilla">
                                                <option  value="<%= tom.getUpjilla()%>"><%= tom.getUpjilla()%></option> 
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Post Code : </td>
                                        <td>
                                            <input type="text" name="postCode" id="txtPostCode" style="width: 200px;" class="formTxtBox_1" value="<%= tom.getPostCode()%>" maxlength="19" />
                                            <div id="SearchValErrorPostCode" class="reqF_1"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Phone No. : <span>*</span></td>
                                        <td>
                                            <input type="text" name="phoneNo" id="txtPhoneNumber" style="width: 200px;" class="formTxtBox_1" value="<%= tom.getPhoneNo()%>"/>
                                            <span id="phno" style="color: grey;">AreaCode-Phone No. i.e. 02-336962</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Fax No. :</td>
                                        <td>
                                            <input type="text" name="faxNo" id="txtFaxNumber" style="width: 200px;" class="formTxtBox_1" value="<%= tom.getFaxNo()%>"/>
                                            <span id="fxno" style="color: grey;">AreaCode-Fax No. i.e. 02-336961</span>
                                        </td>
                                    </tr>
                                    <tr><td> &nbsp; </td>
                                        <td align="left"><label class="formBtn_1"> <input type="submit" value="Update" name="Update" onclick="clrMsg();"/></label>
                                        </td>
                                    </tr>

                                    <% }%>
                                </table>
                            </form>

                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <% }%>
        <script type="text/javascript">

            function alphanumregex(value)
            {
                return /^([\a-zA-Z]+([\sa-zA-Z]+[\d+\s-./&]+)?)+$/.test(value);
            }
            function ChkMaxLength(value)
            {
                var ValueChk=value;
                if(ValueChk.length>150)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            var bvalidationName;
            function chkreg(value)
            {
                return /^[a-zA-Z 0-9](?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9]+$)+$/.test(value);

            }
            
            function chkUnique(){

                var txtOfficeName = document.getElementById('txtOfficeName').value;
                var hiddenofficeName = document.getElementById('hiddenofficeName').value;
                if(hiddenofficeName == txtOfficeName)
                {
                    $('#Msg').html('');
                }
                else
                {
                    bvalidationName = true;
                    if($('#txtOfficeName').val() == ''){
                        $('#Msg').html("");
                        $('#errMsg').html('Please enter Office Name');
                        bvalidationName = false;
                    }else if(document.getElementById('txtOfficeName').value.charAt(0) == " "){
                        $('#Msg').html("");
                        $('#errMsg').html('Only Space is not allowed');
                        bvalidationName = false;
                    }else if(!chkreg($('#txtOfficeName').val())){
                        $('#Msg').html("");
                        $('#errMsg').html('Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' " } { - . _) Only');
                        bvalidationName = false;
                    }else if(document.getElementById('txtOfficeName').value.length > 100){
                        $('#Msg').html("");
                        $('#errMsg').html('Allows maximum 100 characters only');
                        bvalidationName = false;
                    }else{
                        $('#errMsg').html('');
                        $('#Msg').html(" ");
                        $('#Msg').css("color","red");
                        $('#Msg').html("Checking for unique Office Name...");
                        $.post("<%=request.getContextPath()%>/CommonServlet", {officeName:$.trim($('#txtOfficeName').val()),deptId:$('#txtchk').val(),funName:'verifyOfficeName'},
                        function(j)
                        {
                            if(j.toString().indexOf("OK", 0)!=-1){
                                $('#Msg').css("color","green");
                            }
                            else if(j.toString().indexOf("Office", 0)!=-1){
                                $('#Msg').css("color","red");
                            }
                            //$('span.#Msg').html(j);
                            if(document.getElementById('oName') == null){
                                $('#Msg').html(j);
                            }else{
                                document.getElementById('oName').removeAttribute('id');
                                $('#Msg').html('');
                            }
                        });
                    }
                }

            }

        </script>
        <script type="text/javascript">
            $(function() {
                $('#frmEditPEOffice').submit(function() {
                        if((bvalidation == undefined || bvalidation == true) && (bvalidationName == undefined || bvalidationName == true) && (bvalidationPostCode == undefined || bvalidationPostCode == true)){
                        if($('span.#Msg').html().length > 0 ){
                            if(($('span.#Msg').html()=="OK") || ($('span.#Msg').html()=="Ok")){
                                return true;
                            }else{
                                return false;
                            }
                        }
                        else{
                            return true;
                        }
                    }else {
                        return false;
                    }
                });
            });
        
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
            </script>
    </body>
    <%
                peOfficeCreationSrBean = null;
                peOfficeCreationDtBean = null;
    %>
</html>
