<%-- 
    Document   : ViewPEAdmin
    Created on : Nov 17, 2010, 2:23:04 PM
    Author     : rishita
--%>

<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblUserActivationHistory"%>
 <!--Code added by Palash, Dohatec-->
<%@page import="com.cptu.egp.eps.model.table.TblAdminTransfer"%>
 <!--Code End by Palash, Dohatec-->
<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.databean.ManageAdminDtBean;
        import com.cptu.egp.eps.web.servicebean.PEAdminSrBean" %>
<%@page import="com.cptu.egp.eps.model.table.TblOfficeMaster" %>
<%@page import="java.util.List" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View PA Admin</title>
        <jsp:useBean id="manageAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.UpdateAdminSrBean"/>
        <!--Code added by Palash, Dohatec-->
        <jsp:useBean id="AdminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AdminMasterDtBean"/>
        <!--Code End by Palash, Dohatec-->
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/common.js" type="text/javascript"></script>
                        <%
                    String status = request.getParameter("status");
                    String ValueButton = "";
                    String dispstatus = "";
                    String var = "";
                    if ("activate".equalsIgnoreCase(status)) {
                        ValueButton = "Approved";
                        dispstatus = "Deactivate";
                        var = "deactive";
                        status = "Approved";
                    } else {
                        ValueButton = "Deactivate";
                        dispstatus = "Approved";
                    }

                    int uTypeID = 0;
                    if (session.getAttribute("userTypeId") != null) {
                        uTypeID = Integer.parseInt(session.getAttribute("userTypeId").toString());
                    }
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        manageAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                    }
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("hdnUserType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                            userType.append(request.getParameter("hdnUserType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }
                                    byte userTypeid = 0;
                                    if (request.getParameter("userTypeid") != null) {
                                        userTypeid = Byte.parseByte(request.getParameter("userTypeid"));
                                    }
                    int userId = 0;
                    if (request.getParameter("userId") != null) {
                        userId = Integer.parseInt(request.getParameter("userId"));
                    }
                    
                   if(request.getParameter("action") != null && request.getParameter("action").equalsIgnoreCase("view") && request.getParameter("ok") == null)
                   {
                         // Coad added by Dipal for Audit Trail Log.
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        String idType="userId";
                        int auditId=Integer.parseInt(request.getParameter("userId"));
                        String auditAction="View Profile of PA Admin";
                        String moduleName=EgpModule.Manage_Users.getName();
                        String remarks="";
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                   }

                        %>
        <script type="text/javascript">
            function actionSubmit(){
                var comments = document.getElementById("comments").value;;
                if(comments.length == 0){
                    $('#passMsg').html('Please enter Comments');
                    $('#passMsg').show();
                    return false;
                }
                else if($('#comments').val().length>2000){
                    $('#passMsg').html('Allows maximum 2000 characters only');
                    $('#passMsg').show();
                    return false;
                }
                else{
                    jConfirm('Are you sure you want to <%=ValueButton%> this user?', 'PA Admin user', function (ans) {
                        if (ans)
                            document.frmViewPEAdmin.submit();
                    });
                }
            }
            function transfer()
            {
                //window.location="PEAdminGrid.jsp?userTypeid=4";
                dynamicFromSubmit("PEAdminGrid.jsp?userTypeid=4");
                    
            }
            function transferedit()
            {
                //window.location = "ManagePEAdmin.jsp?userId=" + <%=userId%> + "&userTypeid=4";
                dynamicFromSubmit("ManagePEAdmin.jsp?userId=" + <%=userId%> + "&userTypeid=4");

            }
            function transferhistory(){

                //window.location = "ViewPEAdminAccHistory.jsp?userId=" + <%=userId%> + "&userTypeid=4";
                dynamicFromSubmit("ViewPEAdminAccHistory.jsp?userId=" + <%=userId%> + "&userTypeid=4");
//                document.getElementById('frmViewPEAdminTemp').action = "ViewPEAdminAccHistory.jsp?userId=" + <%=userId%> + "&userTypeid=4";
//                document.getElementById('frmViewPEAdminTemp').submit();
            }
              // added by Palash for PE Admin Transfer history.
             function transferPEAdminhistory(){

                dynamicFromSubmit("ViewPEAdminTransHistory.jsp?userId=" + <%=userId%> + "&userTypeid=4");
            }
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                        <%if (Integer.parseInt(session.getAttribute("userTypeId").toString()) == 1 || Integer.parseInt(session.getAttribute("userTypeId").toString()) == 5) {%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <%}%>
                        <td class="contentArea_1">
                            <%if (request.getParameter("from") != null && uTypeID == 1) {%>
                            <div align="left" id="sucMsg" class="responseMsg successMsg">PA admin updated successfully</div>
                            <div>&nbsp;</div>
                            <%} else if (request.getParameter("from") != null) {%>
                            <div align="left" id="sucMsg" class="responseMsg successMsg">Profile updated successfully</div>
                            <div>&nbsp;</div>
                            <% }
                                        if (request.getParameter("msg") != null) {%>
                            <div align="left" id="sucMsg" class="responseMsg successMsg">PA admin created successfully</div>
                            <div>&nbsp;</div>
                            <%}%>
                            <%if (!"".equalsIgnoreCase(status) && status != null) {%>
                            <div class="pageHead_1">PA Admin Account Status</div>
                            <%} else {
                                if (Integer.parseInt(session.getAttribute("userTypeId").toString()) == 1 || Integer.parseInt(session.getAttribute("userTypeId").toString()) == 5) {%>
                                <div class="pageHead_1">PA Admin Details</div>
                            <%} else {%>
                            <div class="pageHead_1">View Profile</div>
                            <%}
                                        }%>
                            <!--Page Content Start-->

                            <form method="POST" name ="frmViewPEAdmin" id="frmViewPEAdmin" action="<%=request.getContextPath()%>/AdminActDeactServlet">
                                <table class="formStyle_1" border="0" width="100%" cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td class="ff" width="200">Hierarchy Node :</td>
                                        <%
                                        if(!"1".equals(session.getAttribute("userTypeId").toString())){
                                           manageAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                        }else{
                                            manageAdminSrBean.setAuditTrail(null);
                                        }
                                                     List<ManageAdminDtBean> officeListDetails = manageAdminSrBean.getOfficeList(userId);
                                                     String deptName = "";
                                                    TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
                                                    List<TblUserActivationHistory> list = transferEmployeeServiceImpl.viewAccountStatusHistory(userId + "");
                                               //  <!--Code added by Palash, Dohatec-->
                                                    List<TblAdminTransfer> list_1 = transferEmployeeServiceImpl.viewPEAdminTransferHistory(String.valueOf(userId));
                                                      boolean flag = false;
                                                     String strReplacedBy = "";
                                                        for (int i = 0; i < list_1.size(); i++) {
                                                            if (!"".equalsIgnoreCase(list_1.get(i).getReplacedBy()) && list_1.get(i).getReplacedBy() != null) {
                                                                strReplacedBy = list_1.get(i).getReplacedBy();
                                                                flag = true;
                                                            }
                                                        }
                                                     // End, Palash
                                            for (ManageAdminDtBean officelist : officeListDetails) {
                                                deptName = officelist.getDepartmentName();
                                            }                                                                                              
                                            %>                                   
                                        <td><label><%=deptName%></label></td>
                                    </tr>
                                    <%
                                    for (ManageAdminDtBean peAdminDetails : manageAdminSrBean.getManagePEDetail(userTypeid, userId)) {
                                    %>
                                    <input type="hidden" name="action" value="changePEAdminStatus" />
                                    <input type="hidden" name="status" value="<%=status%>" />
                                    <input type="hidden" name="userid" value="<%=request.getParameter("userId")%>" />
                                    <input type="hidden" name="userTypeId" value="<%=request.getParameter("userTypeid")%>" />
                                    <input type="hidden" name="partnerType" value="<%=request.getParameter("partnerType")%>" />
                                    <input type="hidden" name="emailId" value="<%= peAdminDetails.getEmailId()%>" />
                                    <input type="hidden" name="fullName" value="<%=peAdminDetails.getFullName()%>" />
                                    <%--<tr>
                                        <td class="ff" width="200">Dzongkhag / District :</td>
                                        <td>
                                            <label id="lblDistName"><%=peAdminDetails.getStateName()%></label>
                                        </td>
</tr>--%>
                                    <tr>
                                        <td class="ff" width="200">Office :</td>
                                        <td>

                                            <label id="lblOfficeName"><%=officeListDetails.get(0).getOfficeName()%><br/></label>
                                            <%--<option value="<%= oName.getOfficeId()%>" <%for(Object object : officeList){if(object.equals(oName.getOfficeName())){%>selected<%}}%>><%= oName.getOfficeName()%></option>--%>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">e-mail ID :</td>
                                        <td>
                                            <input type="hidden" name="adminId" value="<%= peAdminDetails.getAdminId()%>"/>
                                            <input type="hidden" name="nationalId" value="<%= peAdminDetails.getNationalId()%>"/>
                                            <input type="hidden" name="userId" value="<%= peAdminDetails.getUserId()%>"/>
                                            <label id="lblEmailId"><%= peAdminDetails.getEmailId()%></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff" width="200">Full Name :</td>
                                        <td><label id="lblFullName"><%=peAdminDetails.getFullName()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <!--<td class="ff" width="200"> National ID :</td>-->
                                        <td class="ff" width="200"> CID No. :</td>
                                        <td>
                                            <label id="lblNationalId"><%= peAdminDetails.getNationalId()%></label>
                                        </td>
                                    </tr>
                                    <!--<tr>
                                        <td class="ff" width="200">Phone No. :</td>
                                        <td><label id="lblPhoneNumber"><%=peAdminDetails.getPhoneNo()%></label>
                                        </td>
                                    </tr>-->
                                    <tr>
                                        <td class="ff" width="200">Mobile No. :</td>
                                        <td><label id="lblMobileNumber"><%=peAdminDetails.getMobileNo()%></label>
                                        </td>
                                    </tr>
                                    <%}
                                    %>
                                    <%if (!"".equalsIgnoreCase(status) && status != null) {%>
                                    <tr>
                                        <td class="ff" width="200">Status :</td>
                                        <td><label id="lblMobileNumber"><%=dispstatus%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Comments : <span>*</span></td>
                                        <td colspan="3">
                                            <textarea name="comments" cols=""  rows="5" style="width:50%;" class="formTxtBox_1" id="comments" ></textarea>
                                            <span class="reqF_1" id="passMsg" style="display: none;"></span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff">&nbsp;</td>
                                        <td>
                                            <label class="formBtn_1">
                                                <input type="button" name="btnSubmit" id="btnSubmit" value="<%=ValueButton%>" onclick="return actionSubmit();"/>
                                            </label>
                                        </td>
                                    </tr>
                                    <% } else {
                                        if (Integer.parseInt(session.getAttribute("userTypeId").toString()) == 1 || Integer.parseInt(session.getAttribute("userTypeId").toString()) == 5) {%>
                                    <tr align="left">
                                        <td>&nbsp;</td>
                                        <td width="80%" class="t-align-left">
                                            <label class="formBtn_1"><input id="ok" name="ok" value="OK" type="button" onclick="transfer();"/></label>
                                            <label class="formBtn_1"><input id="edit" name="edit" value="Edit" type="button" onclick="transferedit();" /></label>
    <!--Code added by Palash, Dohatec-->
                                            <%if (flag){%>
                                            <label class="formBtn_1"><input id="acchistory" name="edit" value="View Transfer History" type="button" onclick="transferPEAdminhistory();" /></label>
                                           <%}%>
     <!--Code End by Palash, Dohatec-->
                                            <%if(list.size()>0){%>
                                            <label class="formBtn_1"><input id="acchistory" name="edit" value="View Account History" type="button" onclick="transferhistory();" /></label>
                                             <%}%>
                                        </td>
                                    </tr>
                                    <%}
                                                }%>
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script type="text/javascript">
//            var headSel_Obj = document.getElementById("headTabMngUser");
//            if(headSel_Obj != null){
//                headSel_Obj.setAttribute("class", "selected");
//            }
                var uTypeId = '<%=uTypeID%>';
                var headSel_Obj;
                if(uTypeId == 1){
                    headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
                }else{
                    headSel_Obj = document.getElementById("headTabMyAcc");
                    if(headSel_Obj != null){
                        headSel_Obj.setAttribute("class", "selected");
                    }
                }
            </script>
    </body>
    <%
                manageAdminSrBean = null;
    %>
</html>
