<%-- 
    Document   : ViewManageRoleRights
    Created on : Mar 9, 2012, 4:05:09 PM
    Author     : dipal.shah
--%>


<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.ManageUserRights"%>
<%@page import="com.cptu.egp.eps.model.table.TblMenuMaster"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ManageUserRightsImpl"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Role Wise Rights</title>
        <%String contextPath = request.getContextPath();%>
        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/deployJava.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/CommonValidation.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <body>
          <div class="mainDiv">
            <div class="dashboard_div">
                <div class="fixDiv">

                 <!--Middle Content Table Start-->
<%
                ManageUserRightsImpl objManageUserRights1 = (ManageUserRightsImpl)AppContext.getSpringBean("ManageUserRigths");

                int userId = 0;
                if(session.getAttribute("userId") != null) {
                    if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                    }
                }

                int roleId = 0;
                if (request.getParameter("roleId") != null) {
                    roleId = Integer.parseInt(request.getParameter("roleId"));
                }

                int loguserId = 0;
                if (session.getAttribute("userId") != null) {
                    loguserId = Integer.parseInt(session.getAttribute("userId").toString());
                }

                String action = "";
                if(request.getParameter("action") != null && !"".equalsIgnoreCase(request.getParameter("action")))
                {
                     action = request.getParameter("action");
                }

                List<TblMenuMaster> lstMenuMaster=objManageUserRights1.getMenuList(0);

                List<Object[]> objUserDetails=objManageUserRights1.getRoleDetails(roleId);
                String roleName="";
                if(objUserDetails != null)
                {
                    for(Object[] objArray : objUserDetails)
                    {
                        if(objArray!=null && objArray[0]!=null)
                        {
                            roleName=objArray[0].toString();
                        }
                    }
                }

                ArrayList objRoleRights = objManageUserRights1.getRoleMenuRightsList(roleId);
                Map<Integer,String> objUserRightsMap1= null;
                objUserRightsMap1=objManageUserRights1.getUserMenuRightsMap(loguserId);
                //out.println(objUserRights.size());

                %>

                    <div class="contentArea_1">
                        <div class="t_space">
                                <div class="pageHead_1">
                                    Role Wise Rights Details
                                </div>
                        </div>

                  <form id="frmSubmit" name="frmSubmit" method="post">
                        <div class="tableHead_1 t_space">Role Details</div>
                        <table width="100%"  cellspacing="10" class="tableView_1 infoBarBorder">
                        <tr>
                          <td width="10%" class="ff">Role Name :</td>
                          <td width="25%" align="left"><%= roleName%></td>
                        </tr>
                       </table>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" align="center">
                        <th>
                            Module / Menu
                        </th>
                        <th>
                            Functionality / Sub Menu
                        </th>
                        <%

                        for(TblMenuMaster objMenu : lstMenuMaster)
                        {
                                %>
                                <tr valign="top">
                                    <td>
                                        <%= objMenu.getMenuName()%>
                                    </td>
                                    <td>
                                        <%
                                        for(TblMenuMaster objSubMenu : objManageUserRights1.getMenuList(objMenu.getMenuId()))
                                        {
                                            %>
                                            <input disabled type="checkbox" id="chk_sub_<%= objMenu.getMenuId() %>" name="chk" onclick="checkParent(document.frmSubmit.chk_sub_<%= objMenu.getMenuId() %>,'<%=objMenu.getMenuId()%>');" value="<%= objSubMenu.getMenuId() %>"
                                            <%
                                            if(objRoleRights!=null && objRoleRights.contains(objSubMenu.getMenuId()))
                                            { out.println("checked");}
                                            %>
                                            >
                                            <%=objSubMenu.getMenuName()%><BR>
                                            <%
                                        }
                                        %>

                                    </td>
                                </tr>
                               <%

                        }
                        %>
                    </table>
                </form>
                </div>
            </div>
        </div>
        </div>
    </body>
</html>
