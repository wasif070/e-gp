<%-- 
    Document   : TenPaymentConfiguration
    Created on : Apr 19, 2011, 3:28:17 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenPaymentConfigService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.model.table.TblConfigDocFees"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "No-store, No-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "No-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Document Fees, Tender Security and Performance Security, Tender Validity business rule configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            function changeReq(data1,data2){
                var cnt = document.getElementById(data2).options.length;
                if(data1.value=='No'){
                    for(var i=0;i<cnt;i++){
                        if(document.getElementById(data2).options[i].value=="Yes"){
                            document.getElementById(data2).options[i].disabled=true;
                        }else{
                            document.getElementById(data2).options[i].selected=true;
                        }
                    }
                }
                if(data1.value=='Yes'){
                    for(var i=0;i<cnt;i++){
                        if(document.getElementById(data2).options[i].value=="Yes"){
                            document.getElementById(data2).options[i].disabled=false;
                        }
                    }
                }
            }
             $(function() {
                $('#remRule').click(function() {
                    var counter=$('#tbodyData').children().length-1;
                    var tmpCnt = 0;
                    for(var i=1;i<=counter;i++){
                        if(document.getElementById("chk_"+i) != null && document.getElementById("chk_"+i).checked){
                            tmpCnt++;
                        }
                    }
                    if(tmpCnt>0){jConfirm('Are You Sure You want to Delete.','Procurement Method Business Rule Configuration', function(ans) {if(ans){
                     $(":checkbox[checked='true']").each(function(){
                        var len=$('#tbodyData').children().length-1;
                         var curRow = $(this).closest('tr');
                         if(len==1){
                            jAlert("Atleast one configuration is required.","Tender Payment Configuration", function(RetVal) {
                            });
                         }else{
                            var id=$(this).attr("id").substring(4,$(this).attr("id").length);
                            for(var i=id;i<len;i++){
                                $('#procType_'+eval(eval(i)+eval(1))).attr("id","procType_"+(i));
                                $('#tendType_'+eval(eval(i)+eval(1))).attr("id","tendType_"+(i));
                                $('#procMethod_'+eval(eval(i)+eval(1))).attr("id","procMethod_"+(i));
                                $('#isDocFeesReq_'+eval(eval(i)+eval(1))).attr("id","isDocFeesReq_"+(i));
                                $('#isTenderSecReq_'+eval(eval(i)+eval(1))).attr("id","isTenderSecReq_"+(i));
                                $('#isPerSecFeesReq_'+eval(eval(i)+eval(1))).attr("id","isPerSecFeesReq_"+(i));
                                $('#isTenderValReq_'+eval(eval(i)+eval(1))).attr("id","isTenderValReq_"+(i));
                                $('#chk_'+eval(eval(i)+eval(1))).attr("id","chk_"+(i));
                            }
                            curRow.remove();
                          }
                     });
                   }});}else{jAlert("please Select checkbox first","Procurement Method Business Rule Configuration", function(RetVal) {});}
                });
            });

            $(function() {
                $('#addRule').click(function() {
                    var len=$('#tbodyData').children().length-1;
                    $('#tbodyData').append("<tr>"+
                        "<td class='t-align-center'>"+
                        "<input type='checkbox' id='chk_"+len+"' />"+
                        "</td>"+
                        "<td class='t-align-center'>"+
                        "<select name='tendType' class='formTxtBox_1' id='tendType_"+len+"'>"+$('#cmbTend').val()+"</select>"+
                        "</td>"+
                        "<td class='t-align-center'>"+
                        "<select name='procMethod' class='formTxtBox_1' id='procMethod_"+len+"'>"+$('#cmbMethod').val()+"</select>"+
                        "</td>"+
                        "<td class='t-align-center'>"+
                        "<select name='procType' class='formTxtBox_1' id='procType_"+len+"'>"+$('#cmdType').val()+"</select>"+
                        "</td>"+                        
                        "<td class='t-align-center'>"+
                        "<select name='isDocFeesReq' class='formTxtBox_1' id='isDocFeesReq_"+len+"'>"+$('#cmbCommon').val()+"</select>"+
                        "</td>"+
                        "<td class='t-align-center'>"+
                        "<select name='isTenderValReq' class='formTxtBox_1' id='isTenderValReq_"+len+"' onchange=\"changeReq(this,'isTenderSecReq_"+len+"')\">"+$('#cmbCommon').val()+"</select>"+
                        "</td>"+                        
                        "<td class='t-align-center'>"+
                        "<select name='isTenderSecReq' class='formTxtBox_1' id='isTenderSecReq_"+len+"'>"+$('#cmbCommon').val()+"</select>"+
                        "</td>"+
                        "<td class='t-align-center'>"+
                        "<select name='isPerSecFeesReq' class='formTxtBox_1' id='isPerSecFeesReq_"+len+"'>"+$('#cmbCommon').val()+"</select>"+
                        "</td>"+                        
                        "</tr>");
                });
            });
            function delRow(row){
                var curRow = $(row).closest('tr');
                var len=$('#tbodyData').children().length-1;
                if(len==1){
                    jAlert("Atleast one configuration is required.","Tender Payment Configuration", function(RetVal) {
                    });
                }else{
                    var id=$(row).attr("id").substring(8,$(row).attr("id").length);
                    for(var i=id;i<len;i++){
                        $('#procType_'+eval(eval(i)+eval(1))).attr("id","procType_"+(i));
                        $('#tendType_'+eval(eval(i)+eval(1))).attr("id","tendType_"+(i));
                        $('#procMethod_'+eval(eval(i)+eval(1))).attr("id","procMethod_"+(i));
                        $('#isDocFeesReq_'+eval(eval(i)+eval(1))).attr("id","isDocFeesReq_"+(i));
                        $('#isTenderSecReq_'+eval(eval(i)+eval(1))).attr("id","isTenderSecReq_"+(i));
                        $('#isPerSecFeesReq_'+eval(eval(i)+eval(1))).attr("id","isPerSecFeesReq_"+(i));
                        $('#isTenderValReq_'+eval(eval(i)+eval(1))).attr("id","isTenderValReq_"+(i));
                        $('#remRule_'+eval(eval(i)+eval(1))).attr("id","remRule_"+(i));
                    }
                    curRow.remove();
                }
            }

            function validate(){
                $(".err").remove();
                var tbol = true;
                var len=$('#tbodyData').children().length-1;
                var cnt=0;
                var cnt2=0;
                /*for(var i=0;i<len;i++){
                    if($.trim($("#NoEnv_"+i).val()) == "") {
                        $("#NoEnv_"+i).parent().append("<div class='err' style='color:red;'>Please enter No. of Envelopes</div>");
                        cnt++;
                    }else{
                        if(!($("#NoEnv_"+i).val() == "1" || $("#NoEnv_"+i).val() == "2")) {
                            $("#NoEnv_"+i).parent().append("<div class='err' style='color:red;'>Please enter either 1 or 2</div>");
                            cnt2++;
                        }
                    }
                }*/
                if(cnt!=0){
                    tbol = false;
                }
                if(cnt2!=0){
                    tbol = false;
                }
                var chkCount=0;
                for(var i=0;i<len;i++){
                    var pN = $("#procType_"+i).val();
                    var tT = $("#tendType_"+i).val();
                    var pM = $("#procMethod_"+i).val();
                    for(var j=i+1;j<len;j++){
                        var pN2 = $("#procType_"+j).val();
                        var tT2 = $("#tendType_"+j).val();
                        var pM2 = $("#procMethod_"+j).val();
                        if(pN==pN2 && tT==tT2 && pM==pM2){
                            chkCount++;
                        }
                    }
                }
                if(chkCount!=0){
                    jAlert("Duplicate record found. Please enter unique record.","Tender Payment Configuration", function(RetVal) {
                    });
                    tbol=false;
                }
                return tbol;
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <%
                        TenPaymentConfigService configService = (TenPaymentConfigService) AppContext.getSpringBean("TenPaymentConfigService");
                        configService.setLogUserId(session.getAttribute("userId").toString());
                        List<Object[]> procurementType = configService.getProcuremenTypes();
                        List<Object[]> tenderTypes = configService.getTenderTypes();
                        List<Object[]> procurementMethod = configService.getProcurementMethod();
                        StringBuilder cmb1 = new StringBuilder();
                        for (Object[] proc : procurementType) {
                            cmb1.append("<option value='" + proc[0] + "'>" + proc[1] + "</option>");
                        }
                        StringBuilder cmb2 = new StringBuilder();
                        for (Object[] proc : tenderTypes) {
                            cmb2.append("<option value='" + proc[0] + "'>" + proc[1] + "</option>");
                        }
                        StringBuilder cmb3 = new StringBuilder();
                        for (Object[] proc : procurementMethod) {
                            cmb3.append("<option value='" + proc[0] + "'>" + proc[1] + "</option>");
                        }
                        StringBuilder cmb4 = new StringBuilder();
                        cmb4.append("<option value='Yes'>Yes</option>");
                        cmb4.append(" <option value='No'>No</option>");
                        List<TblConfigDocFees> getEvalMethods = new ArrayList<TblConfigDocFees>();
                        boolean isEdit = false;
                        if ("y".equals(request.getParameter("isedit"))) {
                            isEdit = true;
                            getEvalMethods = configService.getAllTblConfigDocFees();
                        }
            %>
            <input type="hidden" value="<%=cmb1%>" id="cmdType"/>
            <input type="hidden" value="<%=cmb2%>" id="cmbTend"/>
            <input type="hidden" value="<%=cmb3%>" id="cmbMethod">
            <input type="hidden" value="<%=cmb4%>" id="cmbCommon">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea">
                        <div class="pageHead_1">Document Fees, Tender Security and Performance Security, Tender Validity business rule configuration</div>
                        <div style="font-style: italic" class="t-align-left t_space"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>

                        <div align="right" class="t_space b_space">
                            <a href="javascript:void(0);" class="action-button-add" id="addRule">Add Rule</a>                        
                            <a href="javascript:void(0);" class="action-button-delete" id="remRule">Remove Rule</a>
                        </div>
                        <div>&nbsp;</div>

                        <form action="<%=request.getContextPath()%>/TenPaymentConfigServlet?action=<%if (isEdit) {
                                        out.print("evalUpdate");
                                    } else {
                                        out.print("evalInsert");
                                    }%>" method="post" >


                            <%if ("fail".equals(request.getParameter("msg"))) {
                                            out.print("<div class='responseMsg errorMsg'>Error in inserting Database</div>");
                                        }%>




                            <table class="tableList_1" cellspacing="0" width="100%" id="members">
                                <tbody id="tbodyData">
                                    <tr>
                                        <!--                        <th class="t-align-center">Select</th>-->
                                        <th class="t-align-center">Select</th>
                                        <th class="t-align-center">Tender Type<br>(<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center">Procurement Method<br>(<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center">Procurement Type<br>(<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center">Requires Document Fees<br>(<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center">Requires Tender Validity<br>(<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center">Requires Tender Security <br>(<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center">Requires Performance Security<br>(<span class="mandatory" style="color: red;">*</span>)</th>
                                    </tr>
                                    <%
                                                int cnt = 0;
                                                do {
                                    %>
                                    <tr>
                                        <td class="t-align-center">
                                            <input type="checkbox" id="chk_<%=cnt%>" />
                                        </td>
                                        <td class="t-align-center">
                                            <select name="tendType" class="formTxtBox_1" id="tendType_<%=cnt%>">
                                                <% for (Object[] proc : tenderTypes) {%>
                                                <option value="<%=proc[0]%>" <%if (isEdit) {
                                                         if (getEvalMethods.get(cnt).getTenderTypeId() == (Byte) proc[0]) {
                                                             out.print("selected");
                                                         }
                                                     }%>><%=proc[1]%></option>
                                                <%}%>
                                            </select>
                                        </td>
                                        <td class="t-align-center">
                                            <select name="procMethod" class="formTxtBox_1" id="procMethod_<%=cnt%>">
                                                <%for (Object[] proc : procurementMethod) {
                                                if("OTM".equalsIgnoreCase((String)proc[1]) ||
                                                           "LTM".equalsIgnoreCase((String)proc[1]) ||
                                                           "DP".equalsIgnoreCase((String)proc[1]) ||
                                                          // "LEQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                         //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                           "RFQ".equalsIgnoreCase((String)proc[1]) ||
                                                          // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                         //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                           "FC".equalsIgnoreCase((String)proc[1]) ||
                                                          // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                                                           "DPM".equalsIgnoreCase((String)proc[1])||
                                                            "QCBS".equalsIgnoreCase((String)proc[1]) ||
                                                            "LCS".equalsIgnoreCase((String)proc[1]) ||
                                                            "SFB".equalsIgnoreCase((String)proc[1]) ||
                                                            "SBCQ".equalsIgnoreCase((String)proc[1]) ||
                                                            "SSS".equalsIgnoreCase((String)proc[1]) ||
                                                            "IC".equalsIgnoreCase((String)proc[1]))
                                                            {
                                                                if("RFQ".equalsIgnoreCase((String)proc[1]))
                                                                {%>
                                                                    <option value="<%=proc[0]%>" <%if (isEdit) {
                                                                    if (getEvalMethods.get(cnt).getProcurementMethodId() == (Byte) proc[0]) {
                                                                    out.print("selected");
                                                                    }
                                                                   }%>>LEM</option>
                                                                  <%}
                                                                  else if("DPM".equalsIgnoreCase((String)proc[1]))
                                                                  {%>
                                                                  <option value="<%=proc[0]%>" <%if (isEdit) {
                                                                    if (getEvalMethods.get(cnt).getProcurementMethodId() == (Byte) proc[0]) {
                                                                    out.print("selected");
                                                                    }
                                                                   }%>>DCM</option>
                                                                  <%}
                                                                  else
                                                                  {%>
                                                                  <option value="<%=proc[0]%>" <%if (isEdit) {
                                                                    if (getEvalMethods.get(cnt).getProcurementMethodId() == (Byte) proc[0]) {
                                                                    out.print("selected");
                                                                    }
                                                                   }%>><%=proc[1]%></option>
                                                            <%}}}%>
                                            </select>
                                        </td>
                                        <td class="t-align-center">
                                            <select name="procType" class="formTxtBox_1" id="procType_<%=cnt%>">
                                                <%for (Object[] proc : procurementType) {%>
                                                <option value="<%=proc[0]%>" <%if (isEdit) {
                                                        if (getEvalMethods.get(cnt).getProcurementTypeId() == (Byte) proc[0]) {
                                                            out.print("selected");
                                                        }
                                                    }%>><%
                                                        //ICT->ICB & NCT->NCB by Emtaz on 19/April/2016
                                                           if(proc[1].toString().equalsIgnoreCase("ICT"))
                                                           {out.print("ICB");}
                                                           else if(proc[1].toString().equalsIgnoreCase("NCT"))
                                                           {out.print("NCB");}
                                                    %></option>
                                                <%}%>
                                            </select>
                                        </td>                                        
                                        <td class="t-align-center">
                                            <select name="isDocFeesReq" class="formTxtBox_1" id="isDocFeesReq_<%=cnt%>">
                                                <option value="Yes" <%if (isEdit) {if(getEvalMethods.get(cnt).getIsDocFeesReq().equals("Yes")) {out.print("selected");}}%>>Yes</option>
                                                <option value="No" <%if (isEdit) {if(getEvalMethods.get(cnt).getIsDocFeesReq().equals("No")) {out.print("selected");}}%>>No</option>
                                            </select>
                                        </td>
                                        <td class="t-align-center">
                                            <select name="isTenderValReq" class="formTxtBox_1" id="isTenderValReq_<%=cnt%>" onchange="changeReq(this,'isTenderSecReq_<%=cnt%>')">
                                                <option value="Yes"<%if (isEdit) {if(getEvalMethods.get(cnt).getIsTenderValReq().equals("Yes")) {out.print("selected");}}%>>Yes</option>
                                                <option value="No"<%if (isEdit) {if(getEvalMethods.get(cnt).getIsTenderValReq().equals("No")) {out.print("selected");}}%>>No</option>
                                            </select>
                                        </td>
                                        <td class="t-align-center">
                                            <select name="isTenderSecReq" class="formTxtBox_1" id="isTenderSecReq_<%=cnt%>">
                                                <option value="Yes" <%if (isEdit) {if(getEvalMethods.get(cnt).getIsTenderSecReq().equals("Yes")) {out.print("selected");}}%> <%if (isEdit) {if(getEvalMethods.get(cnt).getIsTenderValReq().equals("No")) {out.print("disabled");}}%>>Yes</option>
                                                <option value="No" <%if (isEdit) {if(getEvalMethods.get(cnt).getIsTenderSecReq().equals("No")) {out.print("selected");}}%>>No</option>
                                            </select>
                                        </td>                                        
                                        <td class="t-align-center">
                                            <select name="isPerSecFeesReq" class="formTxtBox_1" id="isPerSecFeesReq_<%=cnt%>">
                                                <option value="Yes" <%if (isEdit) {if(getEvalMethods.get(cnt).getIsPerSecFeesReq().equals("Yes")) {out.print("selected");}}%>>Yes</option>
                                                <option value="No" <%if (isEdit) {if(getEvalMethods.get(cnt).getIsPerSecFeesReq().equals("No")) {out.print("selected");}}%>>No</option>
                                            </select>
                                        </td>                                        
                                    </tr>
                                    <%cnt++;
                                                } while (cnt < getEvalMethods.size());%>
                                </tbody>

<!--                                <tr>
                                    <td colspan="8" class="t-align-center">
                                        
                                    </td>
                                </tr>                                        -->
                            </table><div>&nbsp;</div>
                            <div align="center"><span class="formBtn_1" >
                                    <input name="button2" align="center"id="button2" value="Submit" type="submit" onclick="return validate();">
                                </span></div>
                            <div>&nbsp;</div>
                        </form>
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <%if (isEdit) {%>
        <script type="text/javascript" >
            var obj = document.getElementById('lblTenPayEdit');
            if(obj != null){
                if(obj.innerHTML == 'Edit'){
                    obj.setAttribute('class', 'selected');
                }
            }

        </script>

        <%} else {%>
        <script>
            var obj = document.getElementById('lblTenPayAdd');
            if(obj != null){
                if(obj.innerHTML == 'Add'){
                    obj.setAttribute('class', 'selected');
                }
            }

        </script>

        <% }%>

        <script type="text/javascript">
            var headSel_Obj = document.getElementById("headTabConfig");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
</html>
