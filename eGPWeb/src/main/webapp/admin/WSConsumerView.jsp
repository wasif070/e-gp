<%--
    Document   : GovtUserCreation
    Created on : Oct 23, 2010, 5:28:28 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.web.utility.WebServiceUtil"%>
<%@page import="com.cptu.egp.eps.model.table.TblWsOrgMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.WsOrgMasterService"%>
<%@page import="com.cptu.egp.eps.model.table.TblWsMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.WsMasterService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Web-Service Consumer Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <%
                    int webServiceOrgId = Integer.parseInt(request.getParameter("wsOrgId"));
                    WsOrgMasterService wsOrgMasterService = (WsOrgMasterService) AppContext.getSpringBean("WsOrgMasterService");
                    TblWsOrgMaster wsOrgMaster = wsOrgMasterService.getTblWsOrgMaster(webServiceOrgId);
                    String wsRightsString = wsOrgMaster.getWsRights();
                    String[] wsRightsArray = WebServiceUtil.getStringArray(wsRightsString);
                    String statusString = "";
                    if ("N".equalsIgnoreCase(wsOrgMaster.getIsDeleted())) {
                        statusString = "Active";
                    } else {
                        statusString = "Deactivate";
                    }
                    //getting webservices list
                    WsMasterService wsMasterService = (WsMasterService) AppContext.getSpringBean("WsMasterService");
                    List<TblWsMaster> webServiceList = new ArrayList<TblWsMaster>();
                    webServiceList = wsMasterService.getAllWsMaster();

        %>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1">
                                        <div class="pageHead_1">
                                            View Web-Service Consumer Details
                                            <span style="float: right; text-align: right;">
                                                <a class="action-button-goback" href="WSConsumersList.jsp?mode=view">Go back</a>
                                            </span>
                                        </div>
                                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" >
                                            <tr>
                                                <td class="ff" width="20%" >e-Mail ID : </td>
                                                <td width="80%">
                                                    <%=wsOrgMaster.getEmailId()%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Organization Name : </td>
                                                <td><%=wsOrgMaster.getOrgNameEn()%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Organization Name in Dzongkha : </td>
                                                <td><%=wsOrgMaster.getOrgNameBg()%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Organization Address/Detail : </td>
                                                <td> <%=wsOrgMaster.getOrgDetail()%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Contact Person : </td>
                                                <td><%=wsOrgMaster.getContactPerson()%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Contact Person Mobile No : </td>
                                                <td><%=wsOrgMaster.getMobileNo()%>
                                            </tr>
                                            <tr>
                                                <td class="ff">Status : </td>
                                                <td class="t-align-left">
                                                    <%
                                                                out.print(statusString);
                                                    %>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Assigned Web-Service For Accessing : </td>
                                                <td>
                                                    <%  for (TblWsMaster tblWsMaster : webServiceList) {
                                                                    String wsIdString = "" + tblWsMaster.getWsId();
                                                                    for (int i = 0; i < wsRightsArray.length; i++) {
                                                                        if (wsIdString.equalsIgnoreCase(wsRightsArray[i])) {
                                                                            out.println(tblWsMaster.getWsName());
                                                                            out.println("<br>");
                                                                        }//if close
                                                                    }//inner for loop
                                                                }//outer for loop
%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td class="t-align-left">
                                                    <label class="formBtn_1">
                                                        <input type="button" name="buttonWSConsumer" value="Edit" onClick="window.location.href='WSConsumerEdit.jsp?wsOrgId=<%=wsOrgMaster.getOrgId()%>&action=edit&parentPage=viewPage';"/>
                                                    </label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
</html>