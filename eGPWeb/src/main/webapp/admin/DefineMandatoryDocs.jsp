<%-- 
    Document   : DefineMandatoryDocs
    Created on : Apr 25, 2011, 2:22:23 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TemplateSectionFormImpl"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Define Required Documents for a form</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function() {
                $('#addRule').click(function() {
                    var len=$('#tbodyData').children().length-1;
                    $('#tbodyData').append("<tr>"+
                        "<td class='t-align-center'>"+
                        "<input type='checkbox' id='chk_"+len+"' />"+
                        "</td>"+
                        "<td class='t-align-center'><label id='cnt_"+len+"'>"+(eval(len+1))+"</label></td>"+
                        "<td class='t-align-center'>"+
                        "<textarea cols='10' rows='5'  id='doc_"+len+"' name='docName' style='width: 400px;' class='formTxtBox_1'></textarea>"+
                        "</td>"+
                        "</tr>");
                });
            });
            
            var cnt = 0;
            $(function() {
                $('#remRule').click(function() {
                    $(":checkbox[checked='true']").each(function(){
                        var len=$('#tbodyData').children().length-1;
                        var curRow = $(this).closest('tr');
                        if(len==1){
                            jAlert("Atleast one configuration is required.","Service Configuration", function(RetVal) {
                            });
                        }else{
                            var id=$(this).attr("id").substring(4,$(this).attr("id").length);
                            for(var i=id;i<len;i++){
                                $('#chk_'+eval(eval(i)+eval(1))).attr("id","chk_"+(i));
                                var temp =$('#cnt_'+eval(eval(i)+eval(1))).html();
                                $('#cnt_'+eval(eval(i)+eval(1))).html(temp-1);
                                $('#cnt_'+eval(eval(i)+eval(1))).attr("id","cnt_"+(i));
                                $('#doc_'+eval(eval(i)+eval(1))).attr("id","doc_"+(i));
                            }
                            curRow.remove();
                             cnt++;
                        }
                    });
                    if(cnt==0){
                        jAlert("Please select atleast one Document.","Remove Document", function(RetVal) {
                            });
                    }
                });
            });
            function validate(){
                $(".err").remove();
                var bool=true;
                var cnt=0;
                $("textarea").each(function(){
                    if($.trim($(this).val())==""){
                        $(this).parent().append("<div class='err' style='color:red;'>Please enter Document Name.</div>");
                        cnt++;
                    }else{
                        if($.trim($(this).val()).length>500){
                            $(this).parent().append("<div class='err' style='color:red;'>Max 500 Character allowed.</div>");
                            cnt++;
                        }
                    }
                })
                if(cnt!=0){
                   bool=false;
                }
                return bool;
            }
        </script>
    </head>
    <body>
        <%
            TemplateSectionFormImpl templateForm = (TemplateSectionFormImpl) AppContext.getSpringBean("AddFormService");
            List<Object[]> mandDocList = templateForm.getSTDMandDocs(request.getParameter("tId"), request.getParameter("sId"), request.getParameter("fId"));
        %>
        <div class="contentArea_1">
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <div class="pageHead_1">Define Required Documents for a form<span style="float: right;"><a class="action-button-goback" href="DefineSTDInDtl.jsp?templateId=<%=request.getParameter("tId")%>">Go Back to Dashboard</a></span></div>
            <div style="font-style: italic" class="t-align-left t_space"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>
            <div align="right" class="t_space b_space">
                <a href="javascript:void(0);" class="action-button-add" id="addRule">Add Document</a>
                <a href="javascript:void(0);" class="action-button-delete" id="remRule">Remove Document</a>
            </div>
            <form action="<%=request.getContextPath()%>/CreateSTDForm?action=saveSTDMandDocs" method="post">
                <input type="hidden" value="<%=request.getParameter("tId")%>" name="tId"/>
                <input type="hidden" value="<%=request.getParameter("fId")%>" name="fId"/>
                <input type="hidden" value="<%=request.getParameter("sId")%>" name="sId"/>
                <table class="tableList_1" cellspacing="0" width="100%" id="members">
                    <tbody id="tbodyData">
                        <tr>
                            <th class="t-align-center" width="6%">Select</th>
                            <th class="t-align-center" width="4%">Sl. No.</th>
                            <th class="t-align-center" width="90%">Name of Document<span class="mandatory">*</span></th>
                        </tr>
                        <tr>
                            <td class="t-align-center">
                                <input type="checkbox" id="chk_0"/>
                            </td>
                            <td class="t-align-center"><label id="cnt_0">1</label></td>
                            <td class="t-align-center">
                                <textarea cols="10" rows="5"  id="doc_0" name="docName" style="width: 400px;" class="formTxtBox_1"></textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="t-align-center t_space">
                    <label class="formBtn_1">
                        <input type="submit" name="submit" value="Submit" onclick="return validate();"/>
                    </label>
                </div>
            </form>
             <table class="tableList_1 t_space" cellspacing="0" width="100%" id="members">
                    <tbody id="tbodyData">
                        <tr>
                            <th class="t-align-center" width="4%">Sl. <br/> No.</th>
                            <th class="t-align-center" width="66%">Name of Document</th>
                            <th class="t-align-center" width="10%">Action</th>
                        </tr>
                        <%
                            int man_cnt=1;
                            for(Object[] data : mandDocList){
                        %>
                        <tr>
                            <td class="t-align-center" width="4%"><%=man_cnt%></td>
                            <td><%=data[1]%></td>
                            <td class="t-align-center">
                                <a href="EditMandatoryDocs.jsp?mId=<%=data[0]%>&pg=1&tId=<%=request.getParameter("tId")%>&fId=<%=request.getParameter("fId")%>&sId=<%=request.getParameter("sId")%>">Edit</a>&nbsp;|&nbsp;
                                <a href="<%=request.getContextPath()%>/CreateSTDForm?action=delSTDMandDocs&mId=<%=data[0]%>">Remove</a>
                            </td>
                        </tr>
                        <%man_cnt++;}%>
                    </tbody>
             </table>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
