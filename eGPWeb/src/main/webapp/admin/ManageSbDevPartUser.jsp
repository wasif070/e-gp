<%--
    Document   : ManageSbDevPartUser.jsp
    Created on : Apr 16, 2010, 11:57 AM
    Author     : Ketan Prajapati
--%>

<%@page import="com.cptu.egp.eps.model.table.TblPartnerAdminTransfer"%>
<%@page import="com.cptu.egp.eps.model.table.TblUserActivationHistory"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <jsp:useBean id="scBankCreationSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean"/>
    <jsp:useBean id="scBankPartnerAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ScBankPartnerAdminSrBean"/>
    <jsp:useBean id="partnerAdminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.PartnerAdminMasterDtBean"/>
    <jsp:setProperty name="partnerAdminMasterDtBean" property="*"/>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <%
                    String logUserId = "0";
                    if (session.getAttribute("userId") != null) {
                        logUserId = session.getAttribute("userId").toString();
                    }
                    String strUsername = "";
                    if (session.getAttribute("userName") != null) {
                        strUsername = session.getAttribute("userName").toString();
                    }
                    scBankCreationSrBean.setLogUserId(logUserId);
                    scBankPartnerAdminSrBean.setLogUserId(logUserId);
                    byte usrTypeId = 0;
                    if (!request.getParameter("userTypeId").equals("")) {
                        usrTypeId = Byte.parseByte(request.getParameter("userTypeId"));
                    }
                    String usrId = request.getParameter("userId");

                    boolean bole_fromWhere = false;
                    if (usrId.equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        bole_fromWhere = true;
                    }
                    if (usrId != null && !usrId.equals("")) {
                        partnerAdminMasterDtBean.setUserId(Integer.parseInt(usrId));
                        partnerAdminMasterDtBean.setUserTypeId(usrTypeId);
                        partnerAdminMasterDtBean.populateInfo();
                    }

                    String partnerType = request.getParameter("partnerType");

                    String strPartnerId = request.getParameter("partnerId");
                    String strFullName = request.getParameter("fullName");



                    String type = "";
                    String title = "";
                    String title1 = "";
                    String userType = "";
                    String title2 = "";

                    String strOldStatus = request.getParameter("userOldStatus");
                    String strOldStatusCaption = "";
                    if ("deactive".equalsIgnoreCase(strOldStatus)) {
                        strOldStatusCaption = "Deactivated";
                    } else {
                        strOldStatusCaption = "Approved";
                    }

                    String strNewStatus = "";
                    String strCaption = "";
                    if ("approved".equalsIgnoreCase(strOldStatus)) {
                        title2 = "Deactivate User";
                        strNewStatus = "deactive";
                        strCaption = "Deactivate";
                    } else {
                        title2 = "Activate User";
                        strNewStatus = "approved";
                        strCaption = "Activate";
                    }
                    System.out.println("strNewStatus  :"+strNewStatus);
                    String from = request.getParameter("from");

                    if (from == null || from.equals("")) {
                        from = "Operation";
                    }
                    if (partnerType != null && partnerType.equals("Development")) {
                        type = "Development";
                        title = "Development Partner";
                        title1 = "Development Partner";
                        //title2 = "Development Partner";
                        userType = "dev";
                    } else {
                        type = "ScheduleBank";
                        title = "Financial Institute Bank";
                        title1 = "Scheduled Bank";
                        //title2 = "Scheduled Bank Branch";
                        userType = "sb";
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <%if (bole_fromWhere) {%>
        <title>View Profile</title>
        <%} else {%>
        <title><%=title%> - <%=title2%> </title>
        <%}%>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
    </head>
    <script type="text/javascript">
        //  $(document).ready(function(){
        /*   $("#btnChangeStatus").click(function(){
                $(".err").remove();
                if($.trim($("#txtarea_comments").val())=="" ){
                    $("#txtarea_comments").parent().append("<div class='err' style='color:red;'>Please enter Comments</div>");
                }
                else if($.trim($("#txtarea_comments").val()).length > 2000){
                    $("#txtarea_comments").parent().append("<div class='err' style='color:red;'>Maximum 2000 Characters are allowed</div>");
                }
                else
                    {
                        $("#frmSBAdmin").submit();
                       // document.ge
                    }
            });

            $("#txtarea_comments").change(function()
            {
                    $(".err").remove();
            });*/

        function validate()
        {
            var vNewStatus =document.frmSBAdmin.status.value;// document.getElementById("status").value;
            var vPartnerType = $("#partnerType").val();
            var vConfirmMsg;
            var vConfirmTitle;
            $(".err").remove();
            if($.trim($("#txtarea_comments").val())=="" ){
                $("#txtarea_comments").parent().append("<div class='err' style='color:red;'>Please enter Comments</div>");
                return false;
            }
            else if($.trim($("#txtarea_comments").val()).length > 2000){
                $("#txtarea_comments").parent().append("<div class='err' style='color:red;'>Allows maximum 2000 characters only</div>");
                return false;
            }
            else
            {
                if(vNewStatus == 'deactive')
                {
                    vConfirmMsg = 'Are you sure you want to deactivate this user?';
                    vConfirmTitle1 = 'Deactivate';
                }
                else
                {
                    vConfirmMsg = 'Are you sure you want to activate this user?';
                    vConfirmTitle1 = 'Activate';
                    
                }
                if(vPartnerType == 'Development')
                {
                    vConfirmTitle = vConfirmTitle1+ ' Development Partner User';
                }
                else
                {
                    vConfirmTitle = vConfirmTitle1+ ' Scheduled Bank User';
                }
                jConfirm(vConfirmMsg, vConfirmTitle, function(RetVal) {
                    if (RetVal) {
                        document.forms["frmSBAdmin"].submit();
                    }
                });
            }
        }
                
        



       

    </script>


    <body>


        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <%if (!bole_fromWhere) {%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                        <%}%>
                        <td class="contentArea_1">
                            <!--Page Content Start-->

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <!-- Success failure -->
                                    <!-- Result Display End-->
                                    <td>
                                        <div class="pageHead_1">
                                            <%if (bole_fromWhere) {%>
                                            View Profile
                                            <%} else {%>
                                            <%=title%> - <%=title2%>
                                            <%}%>
                                        </div>
                                        <div class="t_space">
                                        </div>
                                        <form id="frmSBAdmin" name="frmSBAdmin" method="post" action="<%=request.getContextPath()%>/AdminActDeactServlet" >
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <%if (!"Development".equalsIgnoreCase(type)) {%>

                                                <tr>
                                                    <td class="ff" width="15%">
                                                        Role :
                                                    </td>
                                                    <td>
                                                        <%/*if(partnerAdminMasterDtBean.getIsMakerChecker().equalsIgnoreCase("BankChecker")){*/%>
                                                        <!--                                                            <input type="radio" name="userRole" id="bankCheckerRole" value="BankChecker" checked="cheked"/> Bank Checker &nbsp;-->
                                                        <%--<label>Bank Checker</label>--%>
                                                        <%/*}else*/ if (partnerAdminMasterDtBean.getIsMakerChecker().equalsIgnoreCase("BranchChecker")) {%>
                                                        <!--                                                            <input type="radio" name="userRole" id="branchCheckerRole" value="BranchChecker" checked="cheked"/> Branch Checker &nbsp;-->
                                                        <label> Branch Checker</label>
                                                        <%} else {%>
                                                        <!--                                                            <input type="radio" name="userRole" id="branchMakerRole" value="maker" checked="cheked"/> Branch Maker-->
                                                        <label> Branch Maker</label>

                                                        <%}%>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <%
                                                            scBankCreationSrBean.getBankForAdmin((partnerAdminMasterDtBean.getSbankDevelopId()));
                                                            if (scBankCreationSrBean.getsBankDevelopId() != 0) {
                                                %>
                                                <tr>
                                                    <td width="15%" class="ff"><%=title1%> : </td>
                                                    <td width="85%">
                                                        <label id="bankName"><%=scBankCreationSrBean.getSbDevelopName()%></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%" class="ff"><%=title1%> Branch: </td>
                                                    <td width="85%">
                                                        <label id="bankName">${partnerAdminMasterDtBean.sbDevelopName}</label>
                                                    </td>
                                                </tr>
                                                <%} else {%>
                                                <tr>
                                                    <td width="15%" class="ff"><%=title1%> : </td>
                                                    <td width="85%">
                                                        <label id="bankName">${partnerAdminMasterDtBean.sbDevelopName}</label>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <tr>
                                                    <td class="ff">e-mail ID : </td>
                                                    <td>
                                                        <label id="txtMail">${partnerAdminMasterDtBean.emailId}</label>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td class="ff">Full Name :</td>
                                                    <td><label id="txtName">${partnerAdminMasterDtBean.fullName}</label>
                                                    </td>
                                                </tr>
                                                <%if (!"Development".equalsIgnoreCase(type)) {
                                                %>
                                                <tr>
                                                    <td class="ff">CID No. : </td>
                                                    <td>
                                                        <label id="txtNationalId" >${partnerAdminMasterDtBean.nationalId}</label>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <tr>
                                                    <td class="ff">Designation :</td>
                                                    <td>
                                                        <label id="txtDesignation">${partnerAdminMasterDtBean.designation}</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Mobile No :</td>
                                                    <%if (!partnerAdminMasterDtBean.getMobileNo().equalsIgnoreCase("")) {%>
                                                    <td>
                                                        <label id="txtMobileNo">${partnerAdminMasterDtBean.mobileNo}</label>
                                                    </td>
                                                    <%}%>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Status :</td>

                                                    <td>
                                                        <label id="txtStatus"><%=strOldStatusCaption%></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Comments :<span class='mendatory'>&nbsp;*</span></td>

                                                    <td colspan="3"><span class="t-align-left">
                                                            <textarea cols="" id="txtarea_comments" name="txtarea_comments" rows="5" class="formTxtBox_1" style="width:50%;"></textarea>
                                                        </span></td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">&nbsp;</td>
                                                    <td><label class="formBtn_1"><input name="btnChangeStatus" type="button" id="btnChangeStatus" value="<%=strCaption%>" onclick="return validate()"  /></label></td>
                                                </tr>
                                            </table>
                                            <input type="hidden" name="action" id="action" value="changeStatusSbDevUser" />
                                            <input type="hidden" name="userTypeId" id="userTypeId" value="<%=usrTypeId%>" />
                                            <input type="hidden" name="userid" id="userId" value="<%=usrId%>" />
                                            <input type="hidden" name="status" id="status" value="<%=strNewStatus%>" />
                                            <input type="hidden" name="partnerType" id="partnerType" value="<%=partnerType%>" />
                                            <input type="hidden" name="partnerId" id="partnerId" value="<%=strPartnerId%>" />
                                            <input type="hidden" name="fullName" id="fullName" value="<%=strFullName%>" />
                                            <input type="hidden" name="emailId" id="emailId"  value="<%=partnerAdminMasterDtBean.getEmailId()%>"/>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <%if (bole_fromWhere) {%>
        <script>
            var headSel_Obj = document.getElementById("headTabMyAcc");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
        <%} else {%>
        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
        <%}%>
</html>
