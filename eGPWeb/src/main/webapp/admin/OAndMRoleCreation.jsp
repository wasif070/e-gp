<%-- 
    Document   : OAndMRoleCreation
    Created on : Mar 9, 2012, 3:27:03 PM
    Author     : shreyansh.shah
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.ManageUserRights"%>
<%@page import="com.cptu.egp.eps.model.table.TblMenuMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ManageUserRightsImpl"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>


<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Manage Role Rights</title>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>

        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js" type="text/javascript"></script>

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
<%
                int roleId = 0;
                if (request.getParameter("roleId") != null) {
                    roleId = Integer.parseInt(request.getParameter("roleId"));
                }

%>

                $(function() {

                       $('#txtrole').blur(function() {
                            var varRoleName  = $.trim(document.getElementById("txtrole").value);
                            if($.trim(varRoleName)==""){
                                $('span.#rolemsg').html("Please enter Role Name.");
                                return false;
                            }
                            else
                            {
                               // alert(varRoleName);
                                 $('span.#rolemsg').html("");
                                 $('span.#rolemsg').html("Checking for unique Role Name...");
                                 $.post("<%=request.getContextPath()%>/ManageUserRights", {roleName:varRoleName,action:'checkRoleNameExist',roleId:'<%=roleId%>'},  function(j)
                                 {
                                     //alert(j.toString());
                                    if(j.toString().indexOf("false", 0)!=-1){
                                        $('span.#rolemsg').css("color","green");
                                        $('span.#rolemsg').html("");
                                        return true;
                                    }
                                    else if(j.toString().indexOf("true", 0)!=-1){
                                        $('span.#rolemsg').css("color","red");
                                        $('span.#rolemsg').html("Role Name already exists. Please enter unique Role Name.");
                                        return  false;
                                    }
                                    $('span.#mailMsg').html(j);
                                 });
                            }
                        });
                });
            
            function chkValidate()
            {
                if(document.getElementById("txtrole")!= null &&  $.trim(document.getElementById("txtrole").value) == '' )
                {
                    $('span.#rolemsg').html("Please enter Role Name.");
                    return false;
                }
                
                if(document.getElementById("rolemsg").innerHTML != '')
                {
                    return false;
                }
                var obj=document.frmSubmit.chk;
                var flg=false;
                for (i = 0; i < obj.length; i++)
                    {
                        if(obj[i].checked == true)
                            {
                                flg=true;
                                return;
                            }
                    }

                if(!flg)
                {
                    alert("Select at least one checkbox to assign rights.");
                    return false;
                }
                else
                    return true;
            }

            function checkParent(lstControl,varMainMenuId)
            {
               // alert(lstControl.length);
                var flg=false;
                for (i = 0; i < lstControl.length; i++)
                {
                    if(lstControl[i].checked == true)
                        {
                            flg=true;
                        }
                }
                document.getElementById("chk_"+varMainMenuId).checked=flg;
            }


          </script>
        
        <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");

                ManageUserRightsImpl objManageUserRights1 = (ManageUserRightsImpl)AppContext.getSpringBean("ManageUserRigths");
                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");

                objManageUserRights1.setAuditTrail(objAuditTrail);
                objManageUserRights1.setMakeAuditTrailService(makeAuditTrailService);

                int userId = 0;
                if(session.getAttribute("userId") != null) {
                    if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                    }
                }
                objManageUserRights1.setLogUserId(userId+"");
                
                int logUserTypeid = 0;
                if (session.getAttribute("userTypeId") != null) {
                    logUserTypeid = Integer.parseInt(session.getAttribute("userTypeId").toString());
                }

                String action = "";
                if(request.getParameter("action") != null && !"".equalsIgnoreCase(request.getParameter("action")))
                {
                     action = request.getParameter("action");
                }

                List<TblMenuMaster> lstMenuMaster=objManageUserRights1.getMenuList(0);

                String roleName="";
                if(action != null && action.equalsIgnoreCase("Update Role"))
                      roleName=objManageUserRights1.getRoleDetails(roleId);
                
                ArrayList objRoleRights = objManageUserRights1.getRoleMenuRightsList(roleId);
                Map<Integer,String> objUserRightsMap1= null;
                objUserRightsMap1=objManageUserRights1.getUserMenuRightsMap(Integer.parseInt(session.getAttribute("userId").toString()));
                //out.println(objUserRights.size());

        %>

       

    </head>
    <body>
          <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle O & M Table Start-->
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("hdnUserType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                    userType.append(request.getParameter("hdnUserType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1" width="100%">
                            <div>&nbsp;</div>
                            <div class="pageHead_1"><%= action %></div>
                            <div>&nbsp;</div>
                            <!--Page O & M Start-->
                             <form id="frmSubmit" name="frmSubmit" method="post" action="<%=request.getContextPath()%>/ManageUserRights">
                             <input type="hidden" name="roleId" id="roleId" value="<%=roleId%>"/>
                             <input type="hidden" name="action" id="action" value="<%=action%>"/>
                                <div class="tableHead_1 t_space" style="width: 95%;">Role Details</div>
                                <table width="98%" cellspacing="10" class="tableView_1 infoBarBorder">
                                <tr>
                                  <td  class="ff">Role Name :  &nbsp;<span style="color: red; font-weight: bold">*</span>
                                    <input style="width:195px;" class="formTxtBox_1" type="text" id="txtrole" name="role" value="<%=roleName%>" maxlength="100"/>
                                    <span id="rolemsg" style="color: red; font-weight: bold"></span>
                                  </td>
                                </tr>
                               </table>
                                <div>&nbsp;</div>
                               <div class="tableHead_1" style="width: 95%;">Role Rights Details</div>
                               <table width="98%" cellspacing="10" class="tableView_1 infoBarBorder">
                                    <tr>
                                    <td class="ff" >
                                         <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" align="center">
                                            <th>
                                                Module / Menu
                                            </th>
                                            <th>
                                                Functionality / Sub Menu
                                            </th>
                                            <%

                                            for(TblMenuMaster objMenu : lstMenuMaster)
                                            {
                                                if(logUserTypeid == 19 && objUserRightsMap1!=null && !objUserRightsMap1.containsKey(objMenu.getMenuId()))
                                                {
                                                    %>
                                                    <tr valign="top" style="display: none;">
                                                    <%
                                                }
                                                else
                                                {
                                                    %>
                                                    <tr valign="top">
                                                    <%
                                                }
                                                   %>
                                                        <td>
                                                            <%= objMenu.getMenuName()%>
                                                            <input style="display: none;" type="checkbox" id="chk_<%= objMenu.getMenuId()%>" name="chk" value="<%= objMenu.getMenuId() %>"
                                                                   <%
                                                                    if(objRoleRights!=null && objRoleRights.contains(objMenu.getMenuId()))
                                                                    { out.println("checked");}
                                                                   %>
                                                            />
                                                        </td>
                                                        <td>
                                                            <%
                                                            for(TblMenuMaster objSubMenu : objManageUserRights1.getMenuList(objMenu.getMenuId()))
                                                            {

                                                                    if(logUserTypeid == 19 && objUserRightsMap1!=null && !objUserRightsMap1.containsKey(objSubMenu.getMenuId()))
                                                                    {
                                                                       %>
                                                                       <input  style="display: none;"  type="checkbox" id="chk_sub_<%= objMenu.getMenuId() %>" name="chk" onclick="checkParent(document.frmSubmit.chk_sub_<%= objMenu.getMenuId() %>,'<%=objMenu.getMenuId()%>');" value="<%= objSubMenu.getMenuId() %>"
                                                                       <%
                                                                       if(objRoleRights!=null && objRoleRights.contains(objSubMenu.getMenuId()))
                                                                          { out.println("checked");}
                                                                        %>
                                                                      />
                                                                       <%
                                                                    }
                                                                    else
                                                                    {
                                                                       %>
                                                                         <input type="checkbox" id="chk_sub_<%= objMenu.getMenuId() %>" name="chk" onclick="checkParent(document.frmSubmit.chk_sub_<%= objMenu.getMenuId() %>,'<%=objMenu.getMenuId()%>');" value="<%= objSubMenu.getMenuId() %>"
                                                                       <%
                                                                       if(objRoleRights!=null && objRoleRights.contains(objSubMenu.getMenuId()))
                                                                          { out.println("checked");}
                                                                        %>
                                                                       />
                                                                        <%=objSubMenu.getMenuName()%><BR>
                                                                        <%
                                                                    }
                                                            }
                                                            %>

                                                        </td>
                                                    </tr>
                                                   <%

                                            }
                                            %>
                                            
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                  <td colspan="2">
                                        <center>
                                            <label class="formBtn_1" id="lblSave">
                                                <input type="submit" name="save" id="save" value="Save" onclick="return chkValidate(this);"/>
                                            </label>
                                        </center>
                                    </td>
                                </tr>
                               </table>
                            </form>
                            
                            <!--Page O & M End-->
                        </td>
                    </tr>
                </table>
                <!--Middle O & M Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        
    </body>
    
</html>

