<%--
    Document   : ManageAdminStatus.jsp
    Created on : April 20, 2010, 4:49:50 PM
    Author     : Ketan Prajapati
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.databean.ManageAdminDtBean" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

                    byte userTypeid = 0;
                    if (request.getParameter("userTypeid") != null) {
                        userTypeid = Byte.parseByte(request.getParameter("userTypeid"));
                    }
                    String strOldStatus = "";
                    String strOldStatusCaption = "";
                    String strNewStatus = "";
                    String title = "";
                    String adminId = "";
                    if (request.getParameter("userOldStatus") != null) {
                        strOldStatus = request.getParameter("userOldStatus");
                    }
                    if ("deactive".equalsIgnoreCase(strOldStatus)) {
                        title = "Activate";
                        strOldStatusCaption = "Deactivated";
                        strNewStatus = "approved";
                    } else {
                        title = "Deactivate";
                        strOldStatusCaption = "Approved";
                        strNewStatus = "deactive";
                    }
                    if (request.getParameter("adminId") != null) {
                        adminId = request.getParameter("adminId");
                    }

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <% if (userTypeid == 12) {%>
        <title><%=title%> Procurement Expert User</title>
        <% } else if (userTypeid == 5) {%>
        <title><%=title%> Organization Admin</title>
        <% } else if (userTypeid == 19) {%>
        <title><%=title%> O & M Admin</title>
        <% } else if (userTypeid == 20) {%>
        <title><%=title%> O & M User</title>
        <% }
             else if (userTypeid == 21) {%>
        <title><%=title%> Performance Monitoring User</title>
        <% }
                                       else {%>
        <title><%=title%> Content Admin</title>
        <% }%>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    </head>
    <jsp:useBean id="manageAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.UpdateAdminSrBean"/>
    <jsp:useBean id="adminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AdminMasterDtBean"/>
    <body>
        <script type="text/javascript">
            function validate()
            {
                var vNewStatus = document.frmManageAdmin.status.value;
                var vUserType = document.frmManageAdmin.userTypeId.value;
                var vConfirmMsg;
                var vConfirmTitle;
                $(".err").remove();
                if($.trim($("#txtarea_comments").val())=="" ){
                    $("#txtarea_comments").parent().append("<div class='err' style='color:red;'>Please enter Comments</div>");
                    $("#txtarea_comments").val("");
                    return false;
                }
                else if($.trim($("#txtarea_comments").val()).length > 2000){
                    $("#txtarea_comments").parent().append("<div class='err' style='color:red;'>Allows maximum 2000 characters only</div>");
                    return false;
                }
                else
                {
                    if(vNewStatus == 'deactive')
                    {
                        vConfirmMsg = 'Are you sure you want to deactivate this user?';
                        vConfirmTitle = 'Deactivate';
                    }
                    else
                    {
                        vConfirmMsg = 'Are you sure you want to activate this user?';
                        vConfirmTitle = 'Activate';

                    }
                    if(vUserType == "5"){
                        vConfirmTitle = vConfirmTitle + " Organization Admin";
                    }
                    else if(vUserType == "8"){
                        vConfirmTitle = vConfirmTitle + " Content Admin";
                    }
                    else if(vUserType == "19"){
                        vConfirmTitle = vConfirmTitle + " O & M Admin";
                    }
                    else if(vUserType == "20"){
                        vConfirmTitle = vConfirmTitle + " O & M User";
                    }
                     else if(vUserType == "21"){
                        vConfirmTitle = vConfirmTitle + " Performance Monitoring User";
                    }
                    else{
                        vConfirmTitle = vConfirmTitle + " Procurement Expert User";
                    }
                    jConfirm(vConfirmMsg, vConfirmTitle, function(RetVal) {
                        if (RetVal) {
                            document.forms["frmManageAdmin"].submit();
                        }
                    });
                }
            }
        </script>
        <div class="mainDiv">
            <div class="fixDiv">
                <%
                            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                manageAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                            }
                            int userId = 0;
                            if (request.getParameter("userId") != null) {
                                userId = Integer.parseInt(request.getParameter("userId"));
                            }
                %>
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("hdnUserType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                            userType.append(request.getParameter("hdnUserType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }
                                    int uTypeId = 0;
                                    sn = request.getSession();
                                    if (sn.getAttribute("userTypeId") != null) {
                                        uTypeId = Integer.parseInt(sn.getAttribute("userTypeId").toString());
                                    }
                                    

                        %>
                        <% if (Integer.parseInt(session.getAttribute("userTypeId").toString()) == 1 || Integer.parseInt(session.getAttribute("userTypeId").toString()) == 19 || Integer.parseInt(session.getAttribute("userTypeId").toString()) == 20) {%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <% }%>
                        <td class="contentArea_1">
                           <%if (userTypeid == 12) {%>
                            <div class="pageHead_1"><%=title%> Procurement Expert User</div>
                            <div>&nbsp;</div>
                            <%} else if (userTypeid == 8) {%>
                            <div class="pageHead_1"><%=title%> Content Admin</div>
                            <div>&nbsp;</div>
                            <%} else if (userTypeid == 5) {%>
                            <div class="pageHead_1"><%=title%> Organization Admin </div>
                            <div>&nbsp;</div>
                            <%} else if (userTypeid == 19) {%>
                            <div class="pageHead_1"><%=title%> O & M Admin </div>
                            <div>&nbsp;</div>
                            <%} else if (userTypeid == 20) {%>
                            <div class="pageHead_1"><%=title%> O & M User </div>
                            <div>&nbsp;</div>
                            <%}
        else if (userTypeid == 21) {%>
                            <div class="pageHead_1"><%=title%> Performance Monitoring User </div>
                            <div>&nbsp;</div>
                            <%}
        else {%>
                            <div class="pageHead_1"><%=title%> Organization Admin</div>
                            <div>&nbsp;</div>
                            <% }%>
                            <!--Page Content Start-->
                            <form id="frmManageAdmin" name="frmManageAdmin" method="post" action="<%=request.getContextPath()%>/AdminActDeactServlet">
                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10" width="100%">
                                    <%   for (ManageAdminDtBean manageAdminDtBean : manageAdminSrBean.getManageDetail(userTypeid, userId)) {
                                                    if (userTypeid == 5) {
                                    %>
                                    <tr>
                                        <td class="ff" width="200">Organization :</td>
                                        <td>
                                            <label id="lblOrgaization"><%= manageAdminDtBean.getDepartmentName()%></label>
                                        </td>
                                    </tr>
                                    <% }%>
                                    <tr>
                                        <td class="ff" width="200">e-mail ID :</td>
                                        <td>
                                            <label id="lblEmailId"><%= manageAdminDtBean.getEmailId()%></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff" width="200">Full Name :</td>
                                        <td>
                                            <label id="lblFullName"><%= manageAdminDtBean.getFullName()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">CID No.:</td>
                                        <td>
                                            <label id="lblNationalId"><%=manageAdminDtBean.getNationalId()%></label>
                                            <input class="formTxtBox_1" name="nationalId" value="<%=manageAdminDtBean.getNationalId()%>" id="txtNationalID"  type="hidden" style="width: 200px;"/>
                                        </td>
                                    </tr>

<!--                                    <tr>
                                        <td class="ff" width="200">Phone No. :</td>
                                        <td>
                                            <label id="lblPhoneNo"><%=manageAdminDtBean.getPhoneNo()%></label>
                                        </td>
                                    </tr>-->

                                    <tr>
                                        <td class="ff" width="200">Mobile No. :</td>
                                        <td>
                                            <label id="lblMobileNo"><%=manageAdminDtBean.getMobileNo()%></label>
                                        </td>
                                    </tr>
                                    <%if (Integer.parseInt(session.getAttribute("userTypeId").toString()) == 1 || Integer.parseInt(session.getAttribute("userTypeId").toString()) == 19 || Integer.parseInt(session.getAttribute("userTypeId").toString()) == 20) {%>
                                    <tr>
                                        <td class="ff">Status :</td>

                                        <td class="t-align-left">
                                            <label id="txtStatus"><%=strOldStatusCaption%></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff">Comments :<span class='mendatory'>&nbsp;*</span></td>

                                        <td colspan="3"><span class="t-align-left">
                                                <textarea cols="5" rows="5" id="txtarea_comments" name="txtarea_comments"  class="formTxtBox_1" style="width:50%;"></textarea>
                                            </span></td>
                                    </tr>

                                    <tr>
                                        <td class="ff">&nbsp;</td>
                                        <td><label class="formBtn_1"><input name="btnChangeStatus" type="button" id="btnChangeStatus" value="<%=title%>" onclick="return validate()"  /></label></td>
                                    </tr>
                                    <%}%>
                                </table>
                                <input type="hidden" name="action" id="action" value="changeStatusAdminUser" />
                                <input type="hidden" name="userTypeId" id="userTypeId" value="<%=String.valueOf(userTypeid)%>" />
                                <input type="hidden" name="userid" id="userId" value="<%=String.valueOf(userId)%>" />
                                <input type="hidden" name="status" id="status" value="<%=strNewStatus%>" />
                                <input type="hidden" name="fullName" id="fullName" value="<%=manageAdminDtBean.getFullName()%>" />
                                <input type="hidden" name="emailId" id="emailId" value="<%=manageAdminDtBean.getEmailId()%>" />
                                <input type="hidden" name="adminId" id="adminId" value="<%=adminId%>" />
                            <% }%>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
    <%
                manageAdminSrBean = null;
                adminMasterDtBean = null;
    %>
</html>
