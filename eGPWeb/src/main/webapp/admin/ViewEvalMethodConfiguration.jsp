<%-- 
    Document   : ViewEvalMethodConfiguration
    Created on : Mar 16, 2011, 3:54:03 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigEvalMethod"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvalMethodConfigService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Evaluation Method Business Rules Configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>        <script type="text/javascript">
            $(document).ready(function() {
                sortTable();
            });
        </script>
        <script type="text/javascript">
            function confirmDelete(){
                var len=$('#tbodyData').children().length-1;                 
                if(len==2){
                    jAlert("Atleast one configuration is required.","Service Configuration", function(RetVal) {
                    });
                    return false;
                }else{
                if(window.confirm("Do you really want to delete the configuration Rule?")){
                    return true;
                }
                else{
                    return false;
            }
            }
         }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <%
                    EvalMethodConfigService configService = (EvalMethodConfigService) AppContext.getSpringBean("EvalMethodConfigService");
                        configService.setLogUserId(session.getAttribute("userId").toString());
                    List<Object[]> procurementNature = configService.getProcurementNature();
                    List<Object[]> tenderTypes = configService.getTenderTypes();
                    List<Object[]> procurementMethod = configService.getProcurementMethod();
                    List<TblConfigEvalMethod> getEvalMethods = configService.getAllTblConfigEvalMethod();
            %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea">
                        <div class="pageHead_1">Evaluation Method Business Rules Configuration
                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('5');">Save as PDF</a></span>
                        </div>
                        <table class="tableList_1 t_space" cellspacing="0" width="100%" id="resultTable" cols="@5">
                            <tbody id="tbodyData">&nbsp;
                                <%
                                            String message = "";
                                            if (!"".equalsIgnoreCase(request.getParameter("msg"))) {
                                                message = request.getParameter("msg");
                                                if ("updsucc".equalsIgnoreCase(message)) {
                                %>
                            <div class="responseMsg successMsg t_space"><span>Evaluation Method Business Rule Configured Successfully</span></div><br/>
                            <%                                                } else if ("editsucc".equalsIgnoreCase(message)) {
                            %>
                            <div class="responseMsg successMsg t_space"><span>Evaluation Method updated successfully</span></div><br/>
                            <%                                    }
                            else if ("delsucc".equalsIgnoreCase(message)) {
                                                        %>
                                                        <div class="responseMsg successMsg t_space"><span>Evaluation Method deleted successfully</span></div><br/>
                                                        <%                                    }
                                        }
                            %>
                            <tr>
                                <th class="t-align-center">Procurement Category</th>
                                <th class="t-align-center">Tender Type</th>
                                <th class="t-align-center">Procurement Method</th>
                                <th class="t-align-center">No Of Envelopes</th>
                                <th class="t-align-center">Evaluation Method</th>
                                <th class="t-align-center">Action</th>
                            </tr>
                            <%
                                        int cnt = 0;
                                        do {
                            %>
                             <%if(cnt%2==0){%>
                            <tr>
          <%}else{%>
          <tr style='background-color:#E4FAD0;'>
          <%}%>
                                <td class="t-align-center">

                                    <%for (Object[] proc : procurementNature) {
                                                                            if (getEvalMethods.get(cnt).getProcurementNatureId() == (Byte) proc[0]) {
                                                                                out.print(proc[1]);
                                                                            }%>
                                    <%}%>
                                </td>
                                <td class="t-align-center">
                                    <% for (Object[] proc : tenderTypes) {
                                                                            if (getEvalMethods.get(cnt).getTenderTypeId() == (Byte) proc[0]) {
                                                                                out.print(proc[1]);
                                                                            }%>
                                    <%}%>
                                </td>
                                <td class="t-align-center">
                                    <%for (Object[] proc : procurementMethod) {
                                                                            if (getEvalMethods.get(cnt).getProcurementMethodId() == (Byte) proc[0]) {
                                                                                if("RFQ".equalsIgnoreCase((String)proc[1]))
                                                                                {
                                                                                    out.print("LEM");
                                                                                }
                                                                                else if("DPM".equalsIgnoreCase((String)proc[1]))
                                                                                {
                                                                                    out.print("DCM");
                                                                                }
                                                                                else
                                                                                {
                                                                                    out.print(proc[1]);
                                                                                }
                                                                            }%>
                                    <%}%>
                                </td>
                                <td class="t-align-center">
                                    <%out.print(getEvalMethods.get(cnt).getNoOfEnvelops());%>
                                </td>
                                <td class="t-align-center">

                                    <%
                                    if (getEvalMethods.get(cnt).getEvalMethod() == 1) {
                                            out.print("T1");
                                    }
                                    if (getEvalMethods.get(cnt).getEvalMethod() == 2) {
                                            out.print("L1");
                                    }
                                    if (getEvalMethods.get(cnt).getEvalMethod() == 3) {
                                            out.print("T1L1");
                                    }
                                    /*if (getEvalMethods.get(cnt).getEvalMethod() == 4) {
                                                                            out.print("T1L1A");
                                                                        }*/
                                    %>
                                </td>
                                <td class="t-align-center">
                                    <a href="EditEvalMethodConfig.jsp?evalId=<%=getEvalMethods.get(cnt).getEvalMethodConfId()%>">Edit</a> &nbsp;|&nbsp; <a href="<%=request.getContextPath()%>/EvalMethodConfigServlet?action=delSingleEval&evalId=<%=getEvalMethods.get(cnt).getEvalMethodConfId()%>" onclick="return confirmDelete()" >Delete</a>
                                </td>
                            </tr>
                            <%cnt++;
                                } while (cnt < getEvalMethods.size());%>
                            </tbody>
                        </table>
                </td>
            </tr>
        </table>

        <form id="formstyle" action="" method="post" name="formstyle">

           <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
           <%
             SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
             String appenddate = dateFormat1.format(new Date());
           %>
           <input type="hidden" name="fileName" id="fileName" value="EvaluationRule_<%=appenddate%>" />
            <input type="hidden" name="id" id="id" value="EvaluationRule" />
        </form>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script>
        var obj = document.getElementById('lblEvalMethodView');
        if(obj != null){
            if(obj.innerHTML == 'View'){
                obj.setAttribute('class', 'selected');
            }
        }

    </script>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabConfig");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
