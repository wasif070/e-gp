<%-- 
    Document   : NOABusiRule
    Created on : Feb 6, 2011, 2:12:16 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Letter of Acceptance (LOA) Business Rule Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <table width="100%" cellspacing="0">
                    <tr valign="top">
                        <td><div id="ddtopmenubar" class="topMenu_1">
                                <!--<div class="dash_menu_1">-->
                                <ul>
                                    <li><a href="#"><img src="../resources/images/Dashboard/msgBoxIcn.png" />Message Box</a></li>
                                    <li><a href="#" rel="ddsubmenu1"><img src="../resources/images/Dashboard/configIcn.png" />Configuration</a></li>
                                    <li><a href="#" rel="ddsubmenu2"><img src="../resources/images/Dashboard/tenderIcn.png" />SBD</a></li>
                                    <li><a href="#"><img src="../resources/images/Dashboard/docLibIcn.png" />Content</a></li>
                                    <li><a href="#" rel="ddsubmenu3"><img src="../resources/images/Dashboard/committeeIcn.png" />Manage Users</a></li>
                                    <li><a href="#"><img src="../resources/images/Dashboard/reportIcn.png" />Report</a></li>
                                    <li><a href="#"><img src="../resources/images/Dashboard/myAccountIcn.png" />Profile</a></li>
                                </ul>
                            </div>
                            <script type="text/javascript">
                                ddlevelsmenu.setup("ddtopmenubar") //ddlevelsmenu.setup("mainmenuid", "topbar|sidebar")
                            </script>
                            <!--Multilevel Menu 1 (rel="ddsubmenu1")-->
                            <ul id="ddsubmenu1" class="ddsubmenustyle">
                                <li><a href="#">Financial Year Details </a>
                                    <ul>
                                        <li><a href="#">Sub Menu 1</a></li>
                                        <li><a href="#">Sub Menu 2</a></li>
                                        <li><a href="#">Sub Menu 3</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Configure calendar</a></li>
                                <li><a href="#">Item 3</a></li>
                            </ul>


                            <!--Multilevel Menu 2 (rel="ddsubmenu2")-->
                            <ul id="ddsubmenu2" class="ddsubmenustyle">
                                <li><a href="#">Item 1</a>
                                    <ul>
                                        <li><a href="#">Sub Menu 1</a></li>
                                        <li><a href="#">Sub Menu 2</a></li>
                                        <li><a href="#">Sub Menu 3</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">Item 2</a></li>
                                <li><a href="#">Item 3</a></li>
                            </ul>

                            <!--Multilevel Menu 3 (rel="ddsubmenu3")-->
                            <ul id="ddsubmenu3" class="ddsubmenustyle">
                                <li><a href="#">Menu 1</a></li>
                                <li><a href="#">Menu 2</a></li>
                                <li><a href="#">Menu 3</a></li>
                                <li><a href="#">Menu 4</a></li>
                            </ul>

                            <table width="100%" cellspacing="6" class="loginInfoBar">
                                <tr>
                                    <td align="left"><b>Friday 27/08/2010 21:45</b></td>
                                    <td align="center"><b>Last Login :</b> Friday 27/08/2010 21:45</td>
                                    <td align="right"><img src="../resources/images/Dashboard/userIcn.png" class="linkIcon_1" /><b>Welcome,</b> User   &nbsp;|&nbsp; <img src="../resources/images/Dashboard/logoutIcn.png" class="linkIcon_1" alt="Logout" /><a href="#" title="Logout">Logout</a></td>
                                </tr>
                            </table></td>
                        <td width="141"><img src="../resources/images/Dashboard/e-GP.gif" width="141" height="64" alt="e-GP" /></td>
                    </tr>
                </table>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <form action="" id="frmNOABusiRule" method="post">
                <div class="pageHead_1">Letter of Acceptance (LOA) Business Rule Configuration</div>

                <div style="font-style: italic" class="t-align-left t_space"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>

                <div align="right" class="t_space b_space"><a href="#" class="action-button-add">Add Rule</a> <a href="#" class="action-button-delete">Remove Rule</a></div>

                <table width="100%" cellspacing="0" class="tableList_1">
                    <tr>
                        <th width="10%" class="t-align-left">Procurement
                            Category
                        </th>
                        <th width="10%" class="t-align-left">Procurement
                            Method
                        </th>
                        <th width="10%" class="t-align-left">Procurement
                            Type
                        </th>
                        <th width="14%" class="t-align-left">Minimum Value in
                            Million (In Million)
                        </th>
                        <th width="14%" class="t-align-left">Maximum Value in
                            Million (In Million)
                        </th>
                        <th width="14%" class="t-align-left">No. of days for Letter of Acceptance (LOA) Acceptance</th>
                        <th width="14%" class="t-align-left">No. of Days for Performance
                            Security Submission
                        </th>
                        <th width="14%" class="t-align-left">No. of days for signing of contract from date of issuance of Letter of Acceptance (LOA)</th>
                    </tr>
                    <tr>
                        <td class="t-align-left"><select name="select" class="formTxtBox_1" id="select">
                                <option>- Select -</option>
                            </select></td>
                        <td class="t-align-left"><select name="select3" class="formTxtBox_1" id="select3">
                                <option>- Select -</option>
                            </select></td>
                        <td class="t-align-left"><select name="select2" class="formTxtBox_1" id="select2">
                                <option>- Select -</option>
                            </select></td>
                        <td class="t-align-left"><input name="textfield2" type="text" class="formTxtBox_1" id="textfield2" style="width:90%;" /></td>
                        <td class="t-align-left"><input name="textfield2" type="text" class="formTxtBox_1" id="textfield2" style="width:90%;" /></td>
                        <td class="t-align-left"><input name="textfield2" type="text" class="formTxtBox_1" id="textfield2" style="width:90%;" /></td>
                        <td class="t-align-left"><input name="textfield2" type="text" class="formTxtBox_1" id="textfield2" style="width:90%;" /></td>
                        <td class="t-align-left"><input name="textfield2" type="text" class="formTxtBox_1" id="textfield2" style="width:90%;" /></td>
                    </tr>
                </table>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->

            </form>
            <!--Dashboard Footer Start-->
            <table width="100%" cellspacing="0" class="footerCss t_space">
                <tr>
                    <td align="left">e-GP &copy; All Rights Reserved
                        <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
                    <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a></td>
                </tr>
            </table>
            <!--Dashboard Footer End-->
        </div>

        <script>
                var headSel_Obj = document.getElementById("hheadTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
