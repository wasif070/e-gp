<%-- 
    Document   : EditDebarMember
    Created on : Apr 30, 2017, 3:43:31 PM
    Author     : feroz
--%>
<%-- 
    Document   : CreateDebarMember
    Created on : Apr 30, 2017, 12:12:22 PM
    Author     : feroz
--%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.BhutanDebarmentCommitteeService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.util.Calendar"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Edit Debarment Committee</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript">
            function CompareToForGreater(value, params)
            {
                if (value != '' && params != '') {

                    var mdy = value.split('/')  //Date and month split
                    var mdyhr = mdy[2].split(' ');  //Year and time split
                    var mdyp = params.split('/')
                    var mdyphr = mdyp[2].split(' ');


                    if (mdyhr[1] == undefined && mdyphr[1] == undefined)
                    {
                        //alert('Both Date');
                        var date = new Date(mdyhr[0], mdy[1], mdy[0]);
                        var datep = new Date(mdyphr[0], mdyp[1], mdyp[0]);
                    } else if (mdyhr[1] != undefined && mdyphr[1] != undefined)
                    {
                        //alert('Both DateTime');
                        var mdyhrsec = mdyhr[1].split(':');
                        var date = new Date(mdyhr[0], parseFloat(mdy[1]) - 1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var mdyphrsec = mdyphr[1].split(':');

                        var datep = new Date(mdyphr[0], parseFloat(mdyp[1]) - 1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    } else
                    {
                        //alert('one Date and One DateTime');
                        var a = mdyhr[1];  //time
                        var b = mdyphr[1]; // time

                        if (a == undefined && b != undefined)
                        {
                            //alert('First Date');
                            var date = new Date(mdyhr[0], mdy[1], mdy[0], '00', '00');
                            var mdyphrsec = mdyphr[1].split(':');
                            var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                        } else
                        {
                            //alert('Second Date');
                            var mdyhrsec = mdyhr[1].split(':');
                            var date = new Date(mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                            var datep = new Date(mdyphr[0], mdyp[1], mdyp[0], '00', '00');
                        }
                    }
                    return Date.parse(date) > Date.parse(datep);
                } else
                {
                    return false;
                }
            }
            $(document).ready(function() {
                $.validator.addMethod(
                    "currentDate",
                    function (value, element) {
                        var currentDate = document.getElementById("currentDate").value;
                        return CompareToForGreater(value, currentDate);
                    },
                    "Please check your input."
                );
                $.validator.addMethod(
                    "greaterDate",
                    function (value, element) {
                        var compareDate = document.getElementById("fromDate").value;
                        return CompareToForGreater(value, compareDate);
                    },
                    "Please check your input."
                );
                $("#debarCom").validate({
                    rules: {
                        uName:{required: true},
                        uDesig:{required:true},
                        uRole:{required:true},
                        uMail:{
                            required:true,
                            email:true
                        },
                        uMob:{
                            required:true,
                            digits: true,
                            minlength: 8
                        },
                        uCid:{
                            required:true,
                            digits:true,
                            minlength: 11
                        },
                        uAddress:{required:true},
                        fromDate:{
                            required:true
                        },
                        toDate:{
                            required:true,
                            currentDate:true,
                            greaterDate:true
                        },
                        isActive:{required:true},
                        remarks:{required:true}

                    },
                    messages: {
                        uName:{ required: "<div class='reqF_1'> Please Enter Full Name.</div>"},

                        uDesig:{ required: "<div class='reqF_1'> Please Enter Designation.</div>"},
                        
                        uRole:{ required: "<div class='reqF_1'> Please Select Role.</div>"},
                        
                        uMail:{ 
                            required: "<div class='reqF_1'> Please Enter Email Id.</div>",
                            email: "<div class='reqF_1'> Please enter a valid email address.</div>"
                        },
                        
                        uMob:{ 
                            required: "<div class='reqF_1'> Please Enter Mobile No.</div>",
                            digits: "<div class='reqF_1'> Please enter only digits.</div>",
                            minlength: "<div class='reqF_1'> Minimum 8 digits are required.</div>"
                        },
                        
                        uCid:{ 
                            required: "<div class='reqF_1'> Please Enter CID No.</div>",
                            digits: "<div class='reqF_1'> Please enter only digits.</div>",
                            minlength: "<div class='reqF_1'> Minimum 11 digits are required.</div>"
                        },
                        
                        uAddress:{ required: "<div class='reqF_1'> Please Enter Address.</div>"},
                        
                        fromDate:{ 
                            required: "<div class='reqF_1'> Please Enter From Date.</div>"
                        },
                        
                        toDate:{ 
                            required: "<div class='reqF_1'> Please Enter To Date.</div>",
                            currentDate: "<div class='reqF_1'>Input must be greater than current date and time.</div>",
                            greaterDate: "<div class='reqF_1'>To Date must be greater than From Date.</div>"
                        },
                        
                        isActive:{ required: "<div class='reqF_1'> Please Select Status.</div>"},
                        
                        remarks:{ required: "<div class='reqF_1'> Please Enter Remarks.</div>"}
                    }
                }
            );
            });

        </script>
        <%
                    BhutanDebarmentCommitteeService bhutanDebarmentCommitteeService = (BhutanDebarmentCommitteeService) AppContext.getSpringBean("BhutanDebarmentCommitteeService");
                    TblBhutanDebarmentCommittee tblBhutanDebarmentCommitteeThis = new TblBhutanDebarmentCommittee();

                    if ("Update".equals(request.getParameter("button"))) 
                    {
                        boolean chairCheck = true;
                        String doneFlag = "";
                        String action = "";
                        DateFormat dF = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        String uName = request.getParameter("uName");
                        String uDesig = request.getParameter("uDesig");
                        String uRole = request.getParameter("uRole");
                        String uMail = request.getParameter("uMail");
                        String uMob = request.getParameter("uMob");
                        String uCid = request.getParameter("uCid");
                        String uAddress = request.getParameter("uAddress");
                        String fromDate = request.getParameter("fromDate");
                        String toDate = request.getParameter("toDate");
                        String createdDate = request.getParameter("createdDate");
                        String isActive = request.getParameter("isActive");
                        if(uRole.equals("Chairman") && isActive.equals("1"))
                        {
                            List<TblBhutanDebarmentCommittee> tblBhutanDebarmentCommitteeCheck = bhutanDebarmentCommitteeService.getAllTblBhutanDebarmentCommittee();
                            for(TblBhutanDebarmentCommittee tblBhutanDebarmentCommitteeMatch : tblBhutanDebarmentCommitteeCheck)
                            {
                                if(Integer.parseInt(request.getParameter("memberId"))!=tblBhutanDebarmentCommitteeMatch.getId() && tblBhutanDebarmentCommitteeMatch.getRole().equals("Chairman") && tblBhutanDebarmentCommitteeMatch.isIsActive())
                                {
                                    chairCheck = false;
                                }
                            }
                        }
                        String remarks = request.getParameter("remarks");
                        int createdBy = Integer.parseInt(request.getParameter("createdBy"));
                        if(chairCheck)
                        {
                            try{
                                tblBhutanDebarmentCommitteeThis.setId(Integer.parseInt(request.getParameter("memberId")));
                                tblBhutanDebarmentCommitteeThis.setMemberName(uName);
                                tblBhutanDebarmentCommitteeThis.setDesignation(uDesig);
                                tblBhutanDebarmentCommitteeThis.setRole(uRole);
                                tblBhutanDebarmentCommitteeThis.setEmailId(uMail);
                                tblBhutanDebarmentCommitteeThis.setMobileNo(uMob);
                                tblBhutanDebarmentCommitteeThis.setNationalId(uCid);
                                tblBhutanDebarmentCommitteeThis.setContactAddress(uAddress);
                                tblBhutanDebarmentCommitteeThis.setFromDate(DateUtils.convertDateToStr(fromDate));
                                tblBhutanDebarmentCommitteeThis.setToDate(DateUtils.convertDateToStr(toDate));
                                tblBhutanDebarmentCommitteeThis.setCreateDate(DateUtils.convertDateToStr(createdDate));
                                if(isActive.equals("1"))
                                {
                                    tblBhutanDebarmentCommitteeThis.setIsActive(true);
                                }
                                else
                                {
                                    tblBhutanDebarmentCommitteeThis.setIsActive(false);
                                }
                                tblBhutanDebarmentCommitteeThis.setCreatedBy(createdBy);
                                tblBhutanDebarmentCommitteeThis.setRemarks(remarks);
                                doneFlag = bhutanDebarmentCommitteeService.updateTblBhutanDebarmentCommittee(tblBhutanDebarmentCommitteeThis);
                            }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                            }finally{
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                                action=null;
                            }
                            if(uRole.equals("Chairman"))
                            {
                                if(doneFlag.equals("editSucc"))
                                {
                                    response.sendRedirect("DebarComChair.jsp?msg=edtS");
                                }
                                else
                                {
                                    response.sendRedirect("DebarComChair.jsp?msg=edtF");
                                }

                            }else
                            {
                                if(doneFlag.equals("editSucc"))
                                {
                                    response.sendRedirect("ActiveMembers.jsp?msg=edtS");
                                }
                                else
                                {
                                    response.sendRedirect("ActiveMembers.jsp?msg=edtF");
                                }
                            }
                        }
                        else
                        {
                            response.sendRedirect("ActiveMembers.jsp?msg=chF");
                        }
                    }
         %>
        
    </head>
    <body>
        <%
            if(request.getParameter("memberId")!=null)
            {
                List<TblBhutanDebarmentCommittee> tblBhutanDebarmentCommitteeEdit = bhutanDebarmentCommitteeService.findTblBhutanDebarmentCommittee(Integer.parseInt(request.getParameter("memberId")));
            
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
       
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea"> <div class="pageHead_1">Edit Debarment Committee Member</div>
                        <form id="debarCom" name="debarCom" action="EditDebarMember.jsp" method="post">
                        <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                            <tr>
                                <td style="font-style: italic" class="t-align-right" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <tr>
                                <td class="ff">Full Name : <span>*</span></td>
                                <td><input style="width:195px;" class="formTxtBox_1" type="text" id="uName" name="uName" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getMemberName()%>" maxlength="150"></td>
                            </tr>
                            <tr>
                                <td class="ff">Designation : <span>*</span></td>
                                <td><input style="width:195px;" class="formTxtBox_1" type="text" id="uDesig" name="uDesig" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getDesignation()%>" maxlength="150"></td>
                            </tr>
                            <tr>
                                <td class="ff">Select Role : <span>*</span></td>
                                <td>
                                    <select style="width:210px;" class="formTxtBox_1" id="uRole" name="uRole">
                                        <option  value="Member" <%if(tblBhutanDebarmentCommitteeEdit.get(0).getRole().equals("Member")){out.print("selected");}%>>Member</option>
                                        <option  value="Member Secretary" <%if(tblBhutanDebarmentCommitteeEdit.get(0).getRole().equals("Member Secretary")){out.print("selected");}%>>Member Secretary</option>
                                        <option  value="Chairman" <%if(tblBhutanDebarmentCommitteeEdit.get(0).getRole().equals("Chairman")){out.print("selected");}%>>Chairman</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Email Id : <span>*</span></td>
                                <td><input style="width:195px;" class="formTxtBox_1" type="text" id="uMail" name="uMail" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getEmailId()%>" maxlength="150"></td>
                            </tr>
                            <tr>
                                <td class="ff">Mobile No. : <span>*</span></td>
                                <td><input style="width:195px;" class="formTxtBox_1" type="text" id="uMob" name="uMob" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getMobileNo()%>" maxlength="8"></td>
                            </tr>
                            <tr>
                                <td class="ff">CID No. : <span>*</span></td>
                                <td><input style="width:195px;" class="formTxtBox_1" type="text" id="uCid" name="uCid" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getNationalId()%>" maxlength="11"></td>
                            </tr>
                            <tr>
                                <td class="ff">Contact Address : <span>*</span></td>
                                <td><textarea rows="5" class="formTxtBox_1" style="width:195px;" id="uAddress" name="uAddress" maxlength="500"><%=tblBhutanDebarmentCommitteeEdit.get(0).getContactAddress()%></textarea></td>
                            </tr>
                            <tr>
                                <td class="ff">From Date : <span>*</span></td>
                                <td>
                                    <input name="fromDate" type="text" class="formTxtBox_1" id="fromDate" style="width:195px;" readonly="true"  onfocus="GetCal('fromDate', 'fromDate');" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getFromDate()%>"/>
                                    <img id="fromDateImg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('fromDate', 'fromDateImg');"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">To Date : <span>*</span></td>
                                <td>
                                    <input name="toDate" type="text" class="formTxtBox_1" id="toDate" style="width:195px;" readonly="true"  onfocus="GetCal('toDate', 'toDate');" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getToDate()%>"/>
                                    <img id="toDateImg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('toDate', 'toDateImg');"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Remarks : <span>*</span></td>
                                <td><textarea rows="3" class="formTxtBox_1" style="width:195px;" id="remarks" name="remarks"><%=tblBhutanDebarmentCommitteeEdit.get(0).getRemarks()%></textarea></td>
                            </tr>
                            <%if(tblBhutanDebarmentCommitteeEdit.get(0).isIsActive()){%>
                                <input type="hidden" name="isActive" id="isActive" value="1">
                            <%}else{%>
                                <input type="hidden" name="isActive" id="isActive" value="0">
                            <%}%>
                            <input type="hidden" name="memberId" id="memberId" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getId()%>">
                            <input type="hidden" name="createdDate" id="createdDate" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getCreateDate()%>">
                            <input type="hidden" name="createdBy" id="createdBy" value="<%=tblBhutanDebarmentCommitteeEdit.get(0).getCreatedBy()%>">
                            <%
                                Date currentDate = new Date();
                                SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy hh:mm");
                            %>
                            <input type="hidden" id="currentDate" value="<%=dt.format(currentDate)%>">          
                            </tbody>
                        </table>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" cellpadding="0" >
                                <tr>
                                    <td class="t-align-center" style="padding-right:110px;"><span class="formBtn_1"><input type="submit" name="button" id="button" value="Update" onclick=""/></span></td>
                                </tr>
                            </table>
                        </form>
                </td>
            </tr>
        </table>
                            <%}%>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        
    </body>
    <script>
        var obj = document.getElementById('lblConfigPaThreshold');
        if(obj != null){
            if(obj.innerHTML == 'View'){
                obj.setAttribute('class', 'selected');
            }
        }

    </script>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabDebar");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        function GetCal(txtname, controlname)
        {
            new Calendar({
                inputField: txtname,
                trigger: controlname,
                showTime: 24,
                onSelect: function () {
                    var date = Calendar.intToDate(this.selection.get());
                    LEFT_CAL.args.min = date;
                    LEFT_CAL.redraw();
                    this.hide();
                    document.getElementById(txtname).focus();
                    
                }
            });

            var LEFT_CAL = Calendar.setup({
                weekNumbers: false
            })

        }
    </script>
</html>