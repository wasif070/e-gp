<%--
    Document   : EditAdvtConfig
    Created on : jan 25, 2013, 6:13:00 PM
    Author     : sambasiva.sugguna
--%>

<%@page import="com.cptu.egp.eps.model.table.TblAdvtConfigurationMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblAdvertisement"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigureCalendarSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblHolidayMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!--jsp:useBean id="configureCalendarSrBean" class="com.cptu.egp.eps.web.servicebean.ConfigureCalendarSrBean" /-->
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Advertisement Configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function() {
                $('#frmUserReg').submit(function() {
                    var msg=true;
                    if($('#txtlocation').val()==""){
                        $('span.#txtlocationspan').css("color","red");
                        $('span.#txtlocationspan').html("Please enter location");
                        msg= false;
                    }
                    else{
                        $('span.#txtlocationspan').html("");
                    }
                    if($('#txtperiod').val()==""){
                        $('span.#txtperiodspan').css("color","red");
                        $('span.#txtperiodspan').html("Please enter dimension");
                        msg= false;
                    }
                    else{
                        $('span.#txtperiodspan').html("");
                    }
                    if(!(($('#txtprice').val()).match("^\\d+\\.\\d{3}?$"))||($('#txtprice').val()==0))
                    {
                        jAlert("- Please enter Positive Numbers Greater than Zero.<br /> - 3 Numbers after decimal (.) are required.<br /> - For example 1001.123","Report Alert");
                        $('span.#txtpricespan').css("color","red");
                        msg=false;
                    }
                    else{
                        $('span.#txtpricespan').html("");
                    }
                    if($('#txtsize').val()==""){
                        $('span.#txtsizespan').css("color","red");
                        $('span.#txtsizespan').html("Please enter size");
                        msg= false;
                    }else
                        if(!digits(document.getElementById("txtsize").value)){
                            $('span.#txtsizespan').html("only numbers allowed");
                            msg= false;
                        }
                    else{
                        $('span.#txtsizespan').html("");
                    }
                    if($('#txtdimension').val()==""){
                        $('span.#txtdimensionspan').css("color","red");
                        $('span.#txtdimensionspan').html("Please enter dimensions");
                        msg= false;
                    }
                    else{
                        $('span.#txtdimensionspan').html("");
                    }
                    if(msg==false){
                        return false;
                    }
                });
            });
            function digits(value) {
                return /^[0-9]+$/.test(value);
            }
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" ></jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <form id="frmUserReg" name="frmUserReg" action="/AdvertisementConfig?funct=submitEdit&adConfigId=<%=request.getParameter("adConfigId")%>" method="post">
                                <div class="pageHead_1"> Advertisement Price Configuration</div>
                                <div>&nbsp;</div>
                                <div><span id="remmsg" ></span></div>
                                <table width="50%" cellspacing="1" class="tableList_1 t_space" id ="tableListDetail">
                                    <% ArrayList<TblAdvtConfigurationMaster> cont = (ArrayList) request.getAttribute("Advertisement");
                                                for (TblAdvtConfigurationMaster tbl : cont) {
                                    %>
                                    <tr>
                                        <td>Location : <span class="mandatory">*</span></td>
                                        <td><input type="text" name="txtlocation" id="txtlocation" size="20" maxlength="25" value="<%= tbl.getLocation()%>" readonly>
                                            <span id="txtlocationspan" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> Period : <span class="mandatory">*</span></td>
                                        <td><input type="text" name="txtperiod" id="txtperiod" size="20" maxlength="25" value="<%= tbl.getDuration()%>" readonly>
                                            <span id="txtperiodspan" class="reqF_1"></span>                                              </td>
                                    </tr>
                                    <tr>
                                        <td>Size (in KB) : <span class="mandatory">*</span></td>
                                        <td>
                                            <input type="text" name="txtsize" id="txtsize" size="20" maxlength="25" value="<%= tbl.getBannerSize()%>" readonly>
                                            <span id="txtsizespan" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Dimension : <span class="mandatory">*</span></td>
                                        <td>
                                            <input type="text" name="txtdimension" id="txtdimension" size="20"maxlength="25" value="<%= tbl.getDimensions()%>" readonly>
                                            <span id="txtdimensionspan" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Price (in Nu.) : <span class="mandatory">*</span></td>
                                        <td>
                                            <input type="text" name="txtprice" id="txtprice" size="20" maxlength="25" value="<%= tbl.getFee()%>" onblur="change();">
                                            <span id="txtpricespan" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center"></td>
                                        <td>
                                            <input name="button1" id="button1" type="submit" value="submit"/>
                                    </tr>
                                </table>
                                <%}%>
                            </form>
                        </td>
                    </tr>
                </table>
                <div>&nbsp;</div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
