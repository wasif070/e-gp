<%--
    Document   : ViewGovtUserDetail
    Created on : Nov 13, 2010, 2:31:37 PM
    Author     : parag
--%>

<%@page import="com.cptu.egp.eps.model.table.TblPartnerAdminTransfer"%>
<%@page import="com.cptu.egp.eps.model.table.TblEmployeeTrasfer"%>
<%@page import="com.cptu.egp.eps.model.table.TblUserActivationHistory"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="govSrUser" class="com.cptu.egp.eps.web.servicebean.GovtUserSrBean" />
<jsp:useBean id="projSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectSrBean" />
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod,java.util.List,com.cptu.egp.eps.model.view.VwEmpfinancePower" %>
<%@page import="java.util.List,com.cptu.egp.eps.model.table.TblEmployeeRoles,com.cptu.egp.eps.model.table.TblDepartmentMaster" %>
<%@page import="com.cptu.egp.eps.model.table.TblEmployeeOffices,com.cptu.egp.eps.dao.storedprocedure.SPGovtEmpRolesReturn" %>
<%
            String str = request.getContextPath();

            //createGovtUser
            //editGovtUser
            String action = "createGovtUser";
            int userId = 0;
            int iUserTypeId = 0;
            int empId = 0;
            String mode = "";
            String fromWhere = "";

            String logUserId = "0";
            if (session.getAttribute("userId") != null) {
                logUserId = session.getAttribute("userId").toString();
            }
            String strUsername = "";
            if (session.getAttribute("userName") != null) {
                strUsername = session.getAttribute("userName").toString();
            }
            govSrUser.setLogUserId(logUserId);
            projSrUser.setLogUserId(logUserId);

            if (request.getParameter("empId") != null) {
                empId = Integer.parseInt(request.getParameter("empId").toString());
            }


            if (request.getParameter("mode") != null) {
                mode = request.getParameter("mode").toString();
            }
            if (request.getParameter("userId") != null) {
                userId = Integer.parseInt(request.getParameter("userId"));
                govSrUser.setUserId(userId);
            } else {
                if (empId != 0) {
                    userId = govSrUser.takeUserIdFromEmpId(empId);
                    govSrUser.setUserId(userId);
                }
            }

            if (request.getParameter("userTypeId") != null) {
                iUserTypeId = Integer.parseInt(request.getParameter("userTypeId"));
            }

            if (request.getParameter("fromWhere") != null) {
                fromWhere = request.getParameter("fromWhere");
            }


            if ("SearchUser".equalsIgnoreCase(fromWhere) || "ViewProfile".equalsIgnoreCase(fromWhere)) {
                if ("ViewProfile".equalsIgnoreCase(fromWhere)) {
                    userId = Integer.parseInt(session.getAttribute("userId").toString());
                }
                try {
                    empId = govSrUser.takeEmpIdFromUserId(userId);
                } catch (Exception e) {
                }
            }
            govSrUser.setEmpId(empId);
            govSrUser.setUserId(userId);

            Object[] object = null;
            object = govSrUser.getGovtUserData();


            List<SPGovtEmpRolesReturn> empRolesReturn = govSrUser.getEmpRoleses(empId);

            List<VwEmpfinancePower> vwFinancePower = govSrUser.getGovtFinanceRole();

            String strOldStatus = request.getParameter("userOldStatus");
            String strOldStatusCaption = "";
            if ("deactive".equalsIgnoreCase(strOldStatus)) {
                strOldStatusCaption = "Deactivated";
            } else {
                strOldStatusCaption = "Approved";
            }

            String strNewStatus = "";
            String strCaption = "";
            String title = "";
            if ("approved".equalsIgnoreCase(strOldStatus)) {
                title = "Deactivate User";
                strNewStatus = "deactive";
                strCaption = "Deactivate";
            } else {
                title = "Activate User";
                strNewStatus = "approved";
                strCaption = "Activate";
            }

%>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Government User - <%=title%> </title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript">
            //  $(document).ready(function(){
            /*   $("#btnChangeStatus").click(function(){
                $(".err").remove();
                if($.trim($("#txtarea_comments").val())=="" ){
                    $("#txtarea_comments").parent().append("<div class='err' style='color:red;'>Please enter Comments</div>");
                }
                else if($.trim($("#txtarea_comments").val()).length > 2000){
                    $("#txtarea_comments").parent().append("<div class='err' style='color:red;'>Maximum 2000 Characters are allowed</div>");
                }
                else
                    {
                        $("#frmSBAdmin").submit();
                       // document.ge
                    }
            });

            $("#txtarea_comments").change(function()
            {
                    $(".err").remove();
            });*/

            function validate()
            {
                var vNewStatus = document.frmGovUser.status.value;
                var vConfirmMsg;
                var vConfirmTitle;
                var vConfirmTitle1;
                $(".err").remove();
                if($.trim($("#txtarea_comments").val())=="" ){
                    $("#txtarea_comments").parent().append("<div class='err' style='color:red;'>Please enter Comments</div>");
                    $("#txtarea_comments").val("");
                    return false;
                }
                else if($.trim($("#txtarea_comments").val()).length > 2000){
                    $("#txtarea_comments").parent().append("<div class='err' style='color:red;'>Allows maximum 2000 characters only</div>");
                    return false;
                }
                else
                {
                    if(vNewStatus == 'deactive')
                    {
                        vConfirmMsg = 'Are you sure you want to deactivate this user?';
                        vConfirmTitle1 = 'Deactivate';
                    }
                    else
                    {
                        vConfirmMsg = 'Are you sure you want to activate this user?';
                        vConfirmTitle1 = 'Activate';

                    }
                    vConfirmTitle = vConfirmTitle1+ ' Government User';
                    jConfirm(vConfirmMsg, vConfirmTitle, function(RetVal) {
                        if (RetVal) {
                            document.forms["frmGovUser"].submit();
                        }
                    });
                }
            }
        </script>

    </head>
    <body>

        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <%
                %>
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%if (!"ViewProfile".equalsIgnoreCase(fromWhere)) {%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>
                        <% }%>
                        <td class="contentArea_1">

                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1"><div class="pageHead_1">Government User - <%=title%></div>
                                        <%if (!"SearchUser".equalsIgnoreCase(fromWhere)) {%>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space" >
                                            <tr>
                                                <%--<th class="t-align-left" width="5%" align="center">Sr No. </th>--%>
                                                <th class="t-align-left" >Employee Name </th>
                                                <th class="t-align-left" >Department Name </th>
                                                <th class="t-align-left" >Office</th>
                                                <th class="t-align-left" >Designation </th>
                                                <th class="t-align-left" >Procurement Role </th>
                                                <!--                                                    <th class="t-align-left" >Financial Power</th>-->
                                            </tr>

                                            <%
                                                /*List<TblEmployeeRoles> empRoles = govSrUser.getEmployeeRoles();
                                                System.out.println(" empRoles :: "+empRoles.size());
                                                String rEmp = "";
                                                for (TblEmployeeRoles roles : empRoles) {
                                                rEmp += roles.getTblProcurementRole().getProcurementRole() + ",";
                                                }

                                                departmentId = empRoles.get(0).getTblDepartmentMaster().getDepartmentId();
                                                govSrUser.setDepartmentId(departmentId);

                                                TblDepartmentMaster departmentMaster = govSrUser.getDepartmentMasterData();
                                                govSrUser.setUserId(userId);
                                                System.out.println(" userid is  ::" + govSrUser.getUserId());
                                                Object[] object = govSrUser.getGovtUserData();
                                                designationId = (Integer) object[6];
                                                String employeeName = (String) object[2];
                                                String designationName = govSrUser.takeDesignationName(designationId);*/

                                                for (SPGovtEmpRolesReturn govtEmpRolesReturn : empRolesReturn) {
                                                    String roleId = govtEmpRolesReturn.getProcurementRoleId();
                                                    String roleName = govtEmpRolesReturn.getProcurementRole();
                                                    String finPowerBy = govtEmpRolesReturn.getFinPowerBy();
                                                    int deptId = govtEmpRolesReturn.getDepartmentId();
                                                    String empRoleId = govtEmpRolesReturn.getEmployeeRoleId();
                                                    String officeId = govtEmpRolesReturn.getOfficeId();
                                                    //govtEmpRolesReturn.
                                            %>
                                            <tr>
                                                <td class="t-align-left"><%=govtEmpRolesReturn.getEmployeeName()%></td>
                                                <td class="t-align-left"><%=govtEmpRolesReturn.getDepartmentname()%></td>
                                                <td class="t-align-left"><%=govtEmpRolesReturn.getOfficeName()%></td>
                                                <td class="t-align-left"><%=govtEmpRolesReturn.getDesignationName()%></td>
                                                <td class="t-align-center">
                                                <%
                                                    String ReplaceHOPE = govtEmpRolesReturn.getProcurementRole();
//                                                    while(ReplaceHOPE.charAt(0)==' ')
//                                                    {
//                                                        ReplaceHOPE = ReplaceHOPE.substring(1,ReplaceHOPE.length());
//                                                    }
                                                    if(ReplaceHOPE.contains("HOPE"))
                                                    {
                                                        ReplaceHOPE = ReplaceHOPE.replaceAll("HOPE", "HOPA");
                                                        //out.print("<td width=\"10%\" class=\"t-align-center\">" + ReplaceHOPE + "</td>");
                                                    }
                                                    if(ReplaceHOPE.contains("PE"))
                                                    {
                                                        int LocationOfPE = ReplaceHOPE.indexOf("PE");
                                                        char[] ReplaceHOPEChars = ReplaceHOPE.toCharArray();
                                                        if(LocationOfPE==0)
                                                        {

                                                            ReplaceHOPEChars[LocationOfPE+1] = 'A';
                                                            ReplaceHOPE = String.valueOf(ReplaceHOPEChars);

                                                        }
                                                        else
                                                        {
                                                            if(ReplaceHOPE.charAt(LocationOfPE-1)==',' || ReplaceHOPE.charAt(LocationOfPE-1)==' ')
                                                            {
                                                                ReplaceHOPEChars[LocationOfPE+1] = 'A';
                                                                ReplaceHOPE = String.valueOf(ReplaceHOPEChars);
                                                            }

                                                        }
                                                    }
                                                    //out.print(ReplaceHOPE);
                                                %>
                                                <%=ReplaceHOPE%>
                                                    
                                                </td>
                                                <!--                                                    <td class="t-align-center">
                                                <%
                                                                                                    if (!"Authorized User".equalsIgnoreCase(govtEmpRolesReturn.getProcurementRole().trim()) && !"AU".equalsIgnoreCase(govtEmpRolesReturn.getProcurementRole().trim())) {
                                                %>
                                                <a href="#" onclick="window.open('ViewGovtUserFinancePower.jsp?userId=<%=userId%>&empId=<%=empId%>&deptId=<%=deptId%>','window','')" title="View">View</a>
                                                <%  } else {
                                                                                                        out.print(" - ");
                                                                                                    }%>
                                            </td>-->
                                            </tr>
                                            <%
                                                }
                                            %>

                                        </table>
                                        <%-- <div class="pageHead_1">Create User</div>--%>
                                        <form method="POST" id="frmGovUser" name="frmGovUser" action="<%=request.getContextPath()%>/AdminActDeactServlet" >

                                            <table width=100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">

                                                <tr>
                                                    <td class="ff" width="20%">e-mail ID : </td>
                                                    <td><% out.print((String) object[0]);%>


                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Full Name : </td>
                                                    <td class="t-align-left"><% out.print((String) object[2]);%>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td class="ff">Name in Dzongkha : </td>
                                                    <%if (object[3] != null) {
                                                            if (!"".equalsIgnoreCase(new String(((byte[]) object[3]), "UTF-8"))) {%>
                                                    <td class="t-align-left"><% out.print("" + new String(((byte[]) object[3]), "UTF-8"));%>
                                                    </td>
                                                    <% }
                                                        }%>
                                                </tr>
                                                <tr>
                                                    <td class="ff">CID No. : </td>
                                                    <%if (!"".equalsIgnoreCase((String) object[5])) {%>
                                                    <td class="t-align-left"><%out.print((String) object[5]);%>
                                                    </td>
                                                    <% }%>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Mobile No : </td>
                                                    <%if (!"".equalsIgnoreCase((String) object[4])) {%>
                                                    <td class="t-align-left"><%out.print((String) object[4]);%>
                                                    </td>
                                                    <% }%>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Status :</td>

                                                    <td class="t-align-left">
                                                        <label id="txtStatus"><%=strOldStatusCaption%></label>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="ff">Comments :<span class='mendatory'>&nbsp;*</span></td>

                                                    <td colspan="3"><span class="t-align-left">
                                                            <textarea cols="5" rows="5" id="txtarea_comments" name="txtarea_comments"  class="formTxtBox_1" style="width:50%;"></textarea>
                                                        </span></td>
                                                </tr>

                                                <tr>
                                                    <td class="ff">&nbsp;</td>
                                                    <td><label class="formBtn_1"><input name="btnChangeStatus" type="button" id="btnChangeStatus" value="<%=strCaption%>" onclick="return validate()"  /></label></td>
                                                </tr>
                                            </table>



                                            <%if ("finalSubmission".equalsIgnoreCase(mode)) {%>
                                            <table border="0" cellspacing="10" cellpadding="0" width="100%" >
                                                <tr class="t-align-center">
                                                    <td class="t-align-center">&nbsp;</td>
                                                    <td class="t-align-center">
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnSubmission" id="btnSubmission" value="Complete Registration" onclick="finalSubmission();" />
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <%}%>
                                            <%}%>
                                            <input type="hidden" name="action" id="action" value="changeStatusGovtUser" />
                                            <input type="hidden" name="userTypeId" id="userTypeId" value="<%=iUserTypeId%>" />
                                            <input type="hidden" name="userid" id="userId" value="<%=String.valueOf(userId)%>" />
                                            <input type="hidden" name="status" id="status" value="<%=strNewStatus%>" />
                                            <input type="hidden" name="fullName" id="fullName" value="<%=object[2].toString()%>" />
                                            <input type="hidden" name="emailId" value="<%=object[0].toString()%>" />
                                            <input type="hidden" name="empId" id="empId" value="<%=String.valueOf(empId)%>" />

                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
<form id="form1" method="post">
    <input type="hidden" name="hidEmailId" id="hidEmailId" value="<%=(String) object[0]%>" />
    <input type="hidden" name="hidPassword" id="hidPassword" value="<%=(String) object[1]%>" />
    <input type="hidden" name="hidMobNo" id="hidMobNo" value="<%=(String) object[4]%>" />
</form>
<script>
    function finalSubmission(){
        var flag=false;
        //$.post("<%=request.getContextPath()%>/GovtUserSrBean", {userId:<%//userId%>,empId:<%=empId%>,action:'finalSubmission'}, function(j){
        //       alert(j);
        //       flag=true;
        // });
        document.getElementById("form1").action="<%=request.getContextPath()%>/GovtUserSrBean?userId=<%=userId%>&empId=<%=empId%>&role=<%=empRolesReturn.get(0).getProcurementRoleId()%>&roleName=<%=empRolesReturn.get(0).getProcurementRole()%>&action=finalSubmission";
        document.getElementById("form1").submit();
    }

</script>
<%
            govSrUser = null;
            projSrUser = null;
%>