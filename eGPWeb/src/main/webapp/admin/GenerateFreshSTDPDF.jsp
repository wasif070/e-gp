<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TemplateSectionImpl"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateSections"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"%>

<%@page import="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd"%>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />


<%


        try {
            TemplateSectionImpl templateSectionImpl = (TemplateSectionImpl) AppContext.getSpringBean("TemplateSectionService");
             GenreatePdfCmd pdfCmd = new GenreatePdfCmd();
                int sectionId=0;
                int parentSecId = 0;
                int templateId=0;

                        String sectionType="";
                        String sectionName = "";
                        String reqURL = request.getRequestURL().toString();
                        sectionName = URLDecoder.decode(request.getParameter("pdfName"), "UTF-8");
                        String folderName = pdfConstant.STDPDF;
                        int ittId = -1, gccId = -1;
                        sectionType = request.getParameter("contentType");
                        sectionId = Integer.parseInt(request.getParameter("sectionId"));
                        templateId = Integer.parseInt(request.getParameter("templateId"));
                        String reqQuery = "sectionId="+sectionId+"&templateId="+templateId;
                        List<TblTemplateSections> list = new ArrayList<TblTemplateSections>();
                      
                            if("ITT".equalsIgnoreCase(sectionType)){
                                ittId = sectionId;
                            }else if ("GCC".equalsIgnoreCase(sectionType)){
                                gccId = sectionId;
                            }
                            if(sectionType.equalsIgnoreCase("TDS")){
                                 list = templateSectionImpl.getSecName(Short.valueOf(String.valueOf(templateId)), "ITT");
                                parentSecId = list.get(0).getSectionId();
                                reqQuery = "sectionId="+parentSecId+"&templateId="+templateId;
                            }else if (sectionType.equalsIgnoreCase("PCC")){
                                list = templateSectionImpl.getSecName(Short.valueOf(String.valueOf(templateId)), "GCC");
                                parentSecId = list.get(0).getSectionId();
                                reqQuery = "sectionId="+parentSecId+"&templateId="+templateId;
                            }
                            if("ITT".equalsIgnoreCase(sectionType) || "GCC".equalsIgnoreCase(sectionType)){
                                reqURL =  reqURL.replace("GenerateFreshSTDPDF","ITTView") ;
                                String genId=templateId+"_"+sectionId;
                                pdfCmd.genrateSTDpdf(reqURL, reqQuery, folderName,genId,sectionName);
                            }
                            if("TDS".equalsIgnoreCase(sectionType) || "PCC".equalsIgnoreCase(sectionType)){
                                reqURL =  reqURL.replace("GenerateFreshSTDPDF","TDSView") ;
                                String genId=templateId+"_"+sectionId;
                                pdfCmd.genrateSTDpdf(reqURL, reqQuery, folderName,genId,sectionName);
                            }
                      /*      if("Form".equalsIgnoreCase(sectionType)){
                                String strForAllFormPDF = "";
                                DefineSTDInDtlSrBean defSTDInDtl = new DefineSTDInDtlSrBean();
                                java.util.List<com.cptu.egp.eps.model.table.TblTemplateSectionForm> tblTemplateForms = defSTDInDtl.getTemplateSectionForm((short)templateId, sectionId);
                                String genId=templateId+"_"+sectionId;
                                int FormId=0;
                                reqURL =  reqURL.replace("GenerateFreshSTDPDF","ViewCompleteForm") ;
                                if(tblTemplateForms != null){
                                    if(!tblTemplateForms.isEmpty()){
                                        for(int j=0;j<tblTemplateForms.size();j++){
                                            FormId = tblTemplateForms.get(j).getFormId();
                                            reqQuery = "templateId="+templateId+"&sectionId="+sectionId+"&formId="+FormId;
                                                strForAllFormPDF += "\"" + reqURL + "?" + reqQuery + "&isPDF=true" + "\" ";
                                        }
                                        pdfCmd.genrateSTDForm(strForAllFormPDF, "", folderName,genId,sectionName);
                                    }
                                    tblTemplateForms = null;
                                }
                            }*/
                      
                        response.sendRedirect("DefineSTDInDtl.jsp?templateId="+templateId);
                        }                       
         catch (Exception e) {
           e.printStackTrace();
        }
       
       
%>
