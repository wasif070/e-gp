<%--
    Document   : CreateTemplate
    Created on : 24-Oct-2010, 1:03:35 AM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBean" />
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />
<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster" %>
<%@page import="java.util.List" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        if(session.getAttribute("userId") != null){
            createSubSectionSrBean.setLogUserId(session.getAttribute("userId").toString());
            defineSTDInDtlSrBean.setLogUserId(session.getAttribute("userId").toString());
        }
%>
        <%
            int sectionId = Integer.parseInt(request.getParameter("sectionId"));
            //int templateId = Integer.parseInt(request.getParameter("templateId"));
            List<com.cptu.egp.eps.model.table.TblIttHeader> subSectionDetail = null;
            boolean edit = false;
            if(request.getParameter("edit") != null){
                if(request.getParameter("edit").equalsIgnoreCase("true")){
                    edit = true;
                }
            }
            int hdNoOfSubSection = 0;
            if(edit){
                subSectionDetail = createSubSectionSrBean.getSubSectionDetail(sectionId);
                if(subSectionDetail != null){
                    hdNoOfSubSection = subSectionDetail.size();
                }
            }

            int templateId = Integer.parseInt(request.getParameter("templateId"));
            String procType = "";
            String procTypefull = "";
            String STDName = "";
            
            List<TblTemplateMaster> templateMasterLst = defineSTDInDtlSrBean.getTemplateMaster((short) templateId);
            if(templateMasterLst != null){
                if(templateMasterLst.size() > 0){
                    procType = templateMasterLst.get(0).getProcType();
                    if("goods".equalsIgnoreCase(procType)){
                        procTypefull = "Goods";
                    }else if("works".equalsIgnoreCase(procType)){
                        procTypefull = "Works";
                    }else if("srvcmp".equalsIgnoreCase(procType)){
                        procTypefull = "Services - Consulting Firms";
                    }else if("srvindi".equalsIgnoreCase(procType)){
                        procTypefull = "Services - Individual Consultant";
                }
                    else if("srvnoncon".equalsIgnoreCase(procType)){
                        procTypefull = "Services - Non consulting services";
                    }
                    STDName = templateMasterLst.get(0).getTemplateName();
                }
                templateMasterLst = null;
            }
            String contentType = createSubSectionSrBean.getContectType(sectionId);
            String sectionName = "";
            sectionName = createSubSectionSrBean.getSectionName(sectionId);
            /*if("srvcmp".equals(procType)){
                if("ITT".equals(contentType)){
                    contentType = contentType.replace("ITT", "ITC");
                }else if("TDS".equals(contentType)){
                    contentType = contentType.replace("TDS", "PDS");
                }
            }else if("srvindi".equals(procType)){
                if("ITT".equals(contentType)){
                    contentType = contentType.replace("ITT", "ITA");
                }
            }*/
            if ("ITT".equals(contentType)) {  contentType="ITB";} 
            else if("TDS".equals(contentType)){contentType="BDS";} 
            else if("PCC".equals(contentType)){contentType="SCC";} 
            
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=contentType%> Sections</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

        <script>
            var alertMsg = "Prepare "+'<%=contentType%>';
            function replaceAll(dataValue, replace, with_this) {
                return dataValue.replace(new RegExp(replace, 'g'),with_this);
            }
            
            $(document).ready(function(){
                $("#addSubSection").click(function(){
                    var counter = ($("#subSectionTable tr:last-child").attr("id").split("_")[1]*1);
                    //var last_id = ($("#createSubSection tr:last-child").attr("id").split("_")[1]*1);
                    <%
                    if(edit){
                    %>
                    var htmlEle = "<tr id=\"tr_" + (counter+1) + "\"><td class='t-align-center'>" + (counter+1) + "</td><td>" +
                                    "<textarea rows=\"3\" class=\"formTxtBox_1\" name=\"txtSubSecName" + (counter+1) + "\" " +
                                    "id=\"txtSecName" + (counter+1) + "\" style=\"width:400px;\"></textarea>" +
                                    "<a href='javascript:void(0);' id='href" + (counter+1) + "' onclick=\"addHeader('" + (counter+1) + "')\">Save Sub Section</a> " +
                                    "</td></tr>";
                    <%
                    }else{
                    %>
                    var htmlEle = "<tr id=\"tr_" + (counter+1) + "\"><td class='t-align-center'>" + (counter+1) + "</td><td>" +
                                    "<textarea rows=\"3\" class=\"formTxtBox_1\" name=\"txtSubSecName" + (counter+1) + "\" " +
                                    "id=\"txtSecName" + (counter+1) + "\" style=\"width:400px;\"></textarea></td></tr>";
                    <%
                    }
                    %>

                    $("#subSectionTable").append(htmlEle);
                });
                $("#removeSubSection").click(function(){
                    var last_id = ($("#subSectionTable tr:last-child").attr("id").split("_")[1]*1);
                    if(last_id >1){
                        $("#tr_"+(last_id)).remove();
                    }else{
                        jAlert("At least one section is needed", alertMsg);
                    }

                });
            });
            function edit(counter){
                var subSectionDetails = $.trim($("#txtSecName"+counter).val());
                var subSectionId = $("#txtSecId"+counter).val();
                /*if(subSectionDetails == ""){
                    jAlert("Please enter Sub Section name", "Alert Message");
                }else */
                    if(subSectionDetails.length > 200){
                    jAlert("Please enter 200 characters only", alertMsg);
                }else{
                    $.ajax({
                        url: "<%=request.getContextPath()%>/CreateSubSectionSrBean",
                        data: "action=update&headerId=" + subSectionId + "&subSectionDetail=" + replaceAll(subSectionDetails, "&", "%26") + "&sectionId=<%=sectionId%>&",
                        context: document.body,
                        type: 'POST',
                        success: function(msg){
                            if(msg == "true"){
                                jAlert("Sub section content updated successfully", "Update Successful");
                            }else{
                                jAlert("error:" + msg, "Error");
                            }
                        }
                    });
                }
            }
            
            function deleteHeader(counter){
                var subSectionCnt = $('#hdNoOfSection').val();
                if(parseInt(subSectionCnt) == 1){
                    jAlert("Atleast one subsection should be Present", alertMsg, function(res){
                        if(res){

                        }
                    });
                }else{
                    jConfirm("Are you sure you want to remove the Sub-Section?", "Remove Sub-Section", function(result){
                        if(result) {
                            var subSectionDetails = $("#txtSecName"+counter).val();
                            var subSectionId = $("#txtSecId"+counter).val();
                            $.ajax({
                                url: "<%=request.getContextPath()%>/CreateSubSectionSrBean",
                                data: "action=delete&headerId=" + subSectionId + "&subSectionDetail=" + subSectionDetails + "&sectionId=<%=sectionId%>&",
                                context: document.body,
                                type: 'POST',
                                success: function(msg){
                                    if(msg == "true"){
                                        $('#hdNoOfSection').val(parseInt(subSectionCnt) - 1);
                                        jAlert("Sub-Section removed successfully", "Removed Successfully", function(){
                                            $("#tr_"+counter).remove();
                                            var coun = 1;
                                            $("tr:[id^=tr_]").each(function(){
                                                if($(this).attr("id") != "tr_0"){
                                                    $("#" + $(this).attr("id") + " td:first-child").html(coun++);
                                                }
                                            });
                                        });
                                    }else{
                                        jAlert("error:" + msg, "Error");
                                    }
                                }
                            });
                        }
                    });
                }
            }
            function addHeader(counter){
                var subSectionCnt = $('#hdNoOfSection').val();
                var subSectionDetails = $.trim($("#txtSecName"+counter).val());
                /*if(subSectionDetails == ""){
                    jAlert("Please enter Sub Section name", "Alert Message");
                }else */
                 if(subSectionDetails.length > 200){
                    jAlert("Please enter 200 characters only", alertMsg);
                }else{
                    $("#href"+counter).remove();
                    var subSectionId = 0;
                    $.ajax({
                        url: "<%=request.getContextPath()%>/CreateSubSectionSrBean",
                        data: "action=add&subSectionDetail=" + subSectionDetails + "&sectionId=<%=sectionId%>",
                        context: document.body,
                        type: 'POST',
                        success: function(msg){
                            if(msg.split(",")[0] == "true"){
                                $('#hdNoOfSection').val(parseInt(subSectionCnt) + 1);
                                jAlert("Sub Section saved successfully","Success");
                                subSectionId = msg.split(",")[1];
                                var ele = "<input type=\"hidden\" name=\"txtSecId" + counter + "\" id=\"txtSecId" + counter + "\"" +
                                            "value=\"" + subSectionId + "\" /><a onclick=\"edit('" + counter + "')\" href=\"javascript:void(0);\">Edit Content</a> - " +
                                            "<a onclick=\"deleteHeader('" + counter + "')\" href=\"javascript:void(0);\">Remove Sub-Section</a>";
                                
                                $("#txtSecName"+counter).parent().append(ele);

                            }else{
                                jAlert("error:" + msg,"Error");
                            }
                        }
                    });
                }
            }

            function validate(){
                var flag = true;
                $(".error").remove();
                $("textarea:[name^=txtSubSecName]").each(function(){
                    if($(this).val() == ""){
                      /*  $(this).parent().append("<span class='error' style='color:red;'>Please enter Sub Section Name.</span>");
                        flag = false;*/
                    }else if($(this).val().length > 200){
                        $(this).parent().append("<span class='error' style='color:red;'>Please enter 200 characters only.</span>");
                        flag = false;
                    }
                });
                return flag;
            }
        </script>

    </head>
    
    <body>
        <div class="mainDiv">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="fixDiv">
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1">Prepare <%=contentType%></div>
                            </div>
                            
                                <form action="<%=request.getContextPath()%>/CreateSubSectionSrBean" method="POST" onsubmit="return validate();">

                                    <!--CreateSectionClause.jsp-->
                                    <input type="hidden" id="hdNoOfSection" name="hdNoOfSection" <%if(edit){%>value="<%= hdNoOfSubSection %>"<%}else{%>value="1"<%}%> />
                                    <input type="hidden" name="hdTemplateId" value="<%=templateId%>" />
                                    <input type="hidden" name="hdSectionId" value="<%=sectionId%>" />
                                    <input type="hidden" name="sectionType" value="<%=contentType%>" />


                                    <table width="100%" cellspacing="0" id="header" class="t_space">
                                        <tr><td width="50%" align="left">
                                                <a class="action-button-goback" href="DefineSTDInDtl.jsp?templateId=<%=templateId%>" >Go back to SBD Dashboard</a>
                                        </td>
                                        </tr>
                                    </table>
                                        
                                    <table width="100%" cellspacing="0" id="header" class="t_space">
                                        <tr><td width="50%" align="left">
                                        <span class="action-button-add" id="addSubSection">Add Sub Section</span>
                                        </td>
                                            <td width="50%" class="t-align-right">
                                        <%
                                            if(!edit){
                                        %>
                                        <a href="#" class="action-button-delete" id="removeSubSection">Remove Sub-Section</a>
                                        <%
                                            }
                                        %>
                                        </td>
                                        </tr>
                                    </table>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                        <tr>
                                            <td width="30%" align="center"><b>Name of SBD :</b></td>
                                            <td width="70%"><%=STDName%></td>
                                        </tr>
                                        <tr>
                                            <td width="30%" align="center"><b>Procurement Type :</b></td>
                                            <td width="70%"><%=procTypefull%></td>
                                        </tr>
                                        <tr>
                                            <td width="30%" align="center"><b>Section :</b></td>
                                            <td width="70%"><%=sectionName%></td>
                                        </tr>
                                    </table>
                                        <table width="100%" class="t_space">
                                            <tr>
                                                <td width="100%" class="t-align-left formNoteTxt"></td>
                                            </tr>
                                        </table>
                                    <%
                                    if(edit){
                                    %>
                                        <table width="100%" cellspacing="0" id="subSectionTable" class="tableList_1 t_space">
                                            <tr>
                                                <td class="formNoteTxt t-align-center">Instruction : </td>
                                                <td class="formNoteTxt">Sub Section name cannot be more than 200 characters</td>
                                            </tr>
                                            <tr id="tr_0">
                                                <th width="11%"">Sub Section No.</th>
                                                <th class="ff">Sub Section Name</th>
                                            </tr>
                                            <%
                                            for(int i=0;i<subSectionDetail.size(); i++){
                                            %>
                                            <tr id="tr_<%=(i+1)%>">
                                                <td class="t-align-center"><%=(i+1)%></td>
                                                <td>
                                                    <textarea rows="3" class="formTxtBox_1" name="txtSecName<%=(i+1)%>"  id="txtSecName<%=(i+1)%>" style="width:400px;"><%=subSectionDetail.get(i).getIttHeaderName()%></textarea>
                                                    <input type="hidden" name="txtSecId<%=(i+1)%>" id="txtSecId<%=(i+1)%>" value="<%=subSectionDetail.get(i).getIttHeaderId()%>" />
                                                    <a onclick="edit('<%=(i+1)%>')" href="javascript:void(0);">Edit content</a> -
                                                    <a onclick="deleteHeader('<%=(i+1)%>')" href="javascript:void(0);">Remove Sub-Section</a>
                                                </td>
                                            </tr>
                                            <%
                                            }
                                            %>
                                        </table>
                                    <%
                                    }else{
                                    %>
                                        <table width="100%" cellspacing="0" id="subSectionTable" class="tableList_1 t_space">
                                            <tr id="tr_0">
                                                <th width="11%" class="t-align-center" >Sub Section No.</th>
                                                <th class="ff">Sub Section Name</th>
                                            </tr>
                                            <tr id="tr_1">
                                                <td class="t-align-center">1</td>
                                                <td>
                                                    <textarea rows="3" class="formTxtBox_1" name="txtSubSecName1"  id="txtSecName1" style="width:400px;"></textarea>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" class="t_space" ><tr><td align="center">
                                            <label class="formBtn_1" >
                                            <input type="submit" name="submit" value="Submit" class="btnAdd" />
                                            </label>
                                                </td></tr>
                                            
                                        </table>
                                    <%
                                    }
                                    %>
                                </form>
                            
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
             <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<%
    if(defineSTDInDtlSrBean != null){
        defineSTDInDtlSrBean = null;
    }
%>
