<%-- 
    Document   : ManageGovtUser.jsp
    Created on : Nov 14, 2010, 5:10:25 PM
    Author     : parag
--%>
<%-- 
    Search formation changed by Emtaz on 11/May/2016. JqGrid is removed.
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="govGridSrUser" class="com.cptu.egp.eps.web.servicebean.GovtUserGridSrBean" />
<jsp:useBean id="govSrUser" class="com.cptu.egp.eps.web.servicebean.GovtUserSrBean" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page  import="java.util.List,com.cptu.egp.eps.dao.storedprocedure.CommonAppData" %>
<%
            boolean flag = false;
            String action = "";
            String msg = "";

            if (request.getParameter("flag") != null) {
                if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
                    flag = true;
                 }
            }
            if (request.getParameter("action") != null) {
                action = request.getParameter("action");
            }

            /*if(flag && "createGovtUser".equalsIgnoreCase(action)){
                msg = " Government User Created Successfully";
            }
            else if(flag && "editGovtUser".equalsIgnoreCase(action)){
                msg = " Government User Updated Successfully";
            }*/

            String type = "";
            String title = "";
            String userType = "";
            int suserTypeId = 0;

            int govUserId = 0;

            if (request.getAttribute("userId") != null) {
                govUserId = Integer.parseInt(request.getAttribute("userId").toString());
            }

            govSrUser.setUserId(govUserId);

            int deptId = govSrUser.getUserTypeWiseDepartmentId(govUserId, suserTypeId);
            type = "GovtUser";
            title = "Government User Details";
            userType = "gov";


           // List<CommonAppData> govData = govGridSrUser.getGovtUserDetail(3);
            String link = "GovtUserGridSrBean?action=viewGovData&govUserType";
            
%>
<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage <%=title%></title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <!--        <script src="< %=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js" type="text/javascript"></script>-->
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
    </head>
        <%
                    if (flag) {
                        if (request.getParameter("msg") != null) {
                            msg = request.getParameter("msg");
                            if (msg.equalsIgnoreCase("true")) {
                        msg = "Government User updated successfully";
                            } else {
                     msg = "Government User created successfully";
                        }
                     }
                }
        %>
        <script type="text/javascript">
            function chkdisble(pageNo){
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }
                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", true)
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(totalPages>0 && $('#pageNo').val()!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo)+1);
                        $('#btnPrevious').removeAttr("disabled");
                    }
                });
            }); 

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();

                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
        </script>
        
        <script type="text/javascript">
            $(function() {
                $('#btnSearch').click(function() {
                    count=0;
                    if(document.getElementById('FullName') !=null && document.getElementById('FullName').value !=''){
                        count = 1;
                    }else if(document.getElementById('txtdepartment') !=null && document.getElementById('txtdepartment').value !=''){
                        count = 1;
                    }else if(document.getElementById('Role') !=null && document.getElementById('Role').value !=''){
                        count = 1;
                    }else if(document.getElementById('EmailID') !=null && document.getElementById('EmailID').value !=''){
                        count = 1;
                    }else if(document.getElementById('Status') !=null && document.getElementById('Status').value !=''){
                        count = 1;
                    }
                   
                    if(count == 0){
                        $('#valMsg').html('Please enter at least One search criteria');
                    }else{
                        $('#valMsg').html('');
                        $('#pageNo').val('1');
                        loadTable();
                    }
                });
                $('#btnReset').click(function() {
                   $("#pageNo").val("1");
                    $("#FullName").val('');
                    $("#txtdepartment").val('');
                    $("#Role").val('');
                    $("#EmailID").val('');
                    $("#Status").val('');
                    loadTable();
                });
            });
            function loadTable()
            {
                $.post("<%=request.getContextPath()%>/GovtUserGridSrBean", {
                        action: "viewGovData",
                        FullName: $("#FullName").val(),
                        OrganizationName: $("#txtdepartment").val(),
                        Role: $("#Role").val(),
                        EmailID: $("#EmailID").val(), 
                        Status: $("#Status").val(),
                        pageNo: $("#pageNo").val(),
                        size: $("#size").val()
                    },  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    sortTable();
                    //sortTable();
                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    chkdisble($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                });
            }
        </script>      
    
    <body onload="loadTable();hide();">
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                        <td class="contentArea">
                            <div>
                                <div class="pageHead_1">View Government Users
                                    <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('7');">Save as PDF</a></span>
                                </div>
                            </div>
                            <%if ("y".equals(request.getParameter("succFlag"))) {
                                            if ("approved".equalsIgnoreCase(request.getParameter("userStatus"))) {%>
                            <div id="succMsg" class="responseMsg successMsg">User's account Activated Successfully.</div>
                            <%} else {%>
                            <div id="succMsg" class="responseMsg successMsg">User's account Deactivated Successfully.</div>
                            <%}
                                        }%>

                            <div >
                                <%
                                            if ("pass".equals(request.getParameter("msg"))) {
                                                out.print("<div class='responseMsg successMsg'>User replaced successfully</div>");
                                            }
                                %>
                                <%if (flag) {%>
                                    <div id="successMsg" class="responseMsg successMsg" style="display:block; margin-top: 10px;"><%=msg%></div>
                                <%} else {%>
                                    <div id="errMsg" class="responseMsg errorMsg" style="display:none; margin-top: 10px;"><%=msg%></div>
                                <%}%>
                            </div>
                            <div class="t-align-left ff formStyle_1">To sort click on the relevant column header</div>
                           
                            <div class="formBg_1">
                                <jsp:useBean id="GovtUserGridSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.GovtUserGridSrBean"/>
                                <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                                <table id="tblSearchBox" cellspacing="10" class="formStyle_1" width="100%">

                                    <tr>
                                        <td class="ff">Full Name : </td>
                                        <td><input type="text" class="formTxtBox_1" id="FullName" style="width:202px;" /></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Hierarchy Node : </td>
                                        <!--<td><input type="text" class="formTxtBox_1" id="OrganizationName" style="width:202px;" /></td>-->
                                        <td colspan="3">
                                            <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 202px;" tabindex="1"
                                                   id="txtdepartment" onblur="showHide();checkCondition();"  readonly/>
                                            <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                            <a href="javascript:void(0);" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?aLvlOff=3&operation=SearchUser', '', 'width=350px,height=400px,scrollbars=1','');">
                                                <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="17%" class="ff">Role :</td>
                                        <td width="33%">
                                            <select name="Role" id="Role" class="formTxtBox_1" id="select2" style="width:215px;">
                                                <option value="" selected="selected">-- Select Role --</option>
                                                <option value="HOPA">HOPA</option>
                                                <option value="PE">PA</option>
                                                <%--<option value="AO">AO</option>--%>
                                                <option value="AU">AU</option>
                                                <option value="TOC/POC">TOC</option>
                                                <option value="TEC/PEC">TEC</option>
                                                <option value="TC">TC</option>
                                                <%--  <option value="Secretary">Secretary</option> 
                                                <option value="PMC">PMC</option>
                                                <option value="Minister">Minister</option>
                                                <option value="BOD">BOD</option>
                                                <option value="CCGP">CCGP</option>--%>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Email ID : </td>
                                        <td><input type="text" class="formTxtBox_1" id="EmailID" style="width:202px;" /></td>
                                    </tr>

                                    <tr>
                                        <td width="16%" class="ff">Status :</td>
                                        <td width="32%">
                                            <select name="Status" class="formTxtBox_1" id="Status" style="width:120px;" >
                                                <option value="">--Select--</option>
                                                <option value="approved">Approved</option>
                                                <option value="pending">Pending</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td class="ff">&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="t-align-center">
                                            <label class="formBtn_1"><input type="button" tabindex="6" name="search" id="btnSearch" value="Search"/>
                                            </label>&nbsp;&nbsp;
                                            <label class="formBtn_1"><input type="button" tabindex="7" name="reset" id="btnReset" value="Reset"/>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="t-align-center">
                                            <div class="reqF_1" id="valMsg"></div>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                    
                            <div class="tableHead_1 t_space">
                                Search Result
                            </div>
                    
                            <div class="tabPanelArea_1">
                                <table width="100%" cellspacing="0" class="tableList_1" id="resultTable" cols="@0,7">
                                    <tr>
                                        <th width="4%" class="t-align-center">
                                            Sl. <br/>
                                            No.
                                        </th>
                                        <th width="11%" class="t-align-center">
                                            Full <br />
                                            Name                                
                                        </th>
                                        <th width="20%" class="t-align-center">
                                            Hierarchy Node
                                        </th>
                                        <th width="10%" class="t-align-center">
                                            Role
                                        </th>
                                        <th width="25%" class="t-align-center">
                                            Email <br />
                                            ID
                                        </th>
                                        <th width="10%" class="t-align-center">
                                            Date
                                        </th>
                                        <th width="5%" class="t-align-center">
                                            Status
                                        </th>
                                        <th width="15%" class="t-align-center">
                                            Action
                                        </th>
                                    </tr>
                                </table>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                                    <tr>
                                        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                        <td align="center"><input name="textfield3" onkeypress="checkKeyGoTo(event);" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input   type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                        </label></td>
                                        <td  class="prevNext-container">
                                            <ul>
                                                <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                                <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                                <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <div align="center">

                                    <input type="hidden" id="pageNo" value="1"/>
                                    <input type="hidden" name="size" id="size" value="10"/>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <!--For Generate PDF  Starts-->
                <form id="formstyle" action="" method="post" name="formstyle">
                    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                    <%
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                        String appenddate = dateFormat1.format(new Date());
                    %>
                    <input type="hidden" name="fileName" id="fileName" value="GovernmentUser_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="GovernmentUser" />
                </form>
                <!--For Generate PDF  Ends-->
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        
    </body>
    
    <script type="text/javascript">
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
            function showHide()
            {
                if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
                    document.getElementById('tblSearchBox').style.display = 'table';
                    document.getElementById('collExp').innerHTML = '- Advanced Search';
                }else{
                    document.getElementById('tblSearchBox').style.display = 'none';
                    document.getElementById('collExp').innerHTML = '+ Advanced Search';
                }
            }
            function hide()
            {
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Advanced Search';
            }
    </script>

    <script type="text/javascript">
                    var obj = document.getElementById('lblGovUsersView');
                    if(obj != null){
                        if(obj.innerHTML == 'View Government Users'){
                            obj.setAttribute('class', 'selected');
                        }
                    }

        //{name:'emailId',index:'emailId', width:100},
                            //{name:'view',index:'view', width:100}

        /*jQuery().ready(function (){
                    //alert("url:" + < %=link%>);
                    var grid = $("#list"),
                    prmSearch = {multipleSearch:false,overlay:false};
                    grid.jqGrid({
                        url: "../GovtUserGridSrBean?action=viewGovData",
                        datatype: "xml",
                        height: 250,
                        colNames:['S. No.','Full Name','Organization Name','Role','e-mail ID','Date','Status','Action'],
                        colModel:[
                            {name:'srno',index:'srno', width:20, search: false,sortable:false,align:'center'},
                            {name:'employeeName',index:'employeeName', width:70,sortable:true, searchoptions: { sopt: ['eq', 'cn'] }},
                            {name:'organizationName',index:'organizationName', width:110,sortable:true, searchoptions: { sopt: ['eq', 'cn'] }},
                            {name:'role',index:'role', width:60,sortable:true,align:'center', searchoptions: { sopt: ['eq', 'cn'] }},
                            {name:'emailid',index:'emailid', width:120,sortable:true, searchoptions: { sopt: ['eq', 'cn'] }},
                            {name:'sdate',index:'sdate', width:60,sortable:true,search: false,align:'center'},
                            {name:'status',index:'status', width:60,sortable:true,align:'center', searchoptions: { sopt: ['eq'] }},
                            {name:'action',index:'action', width:120,sortable:false, search: false,align:'center'},
                        ],
                        autowidth: true,
                        multiselect: false,
                        paging: true,
                        rowNum:10,
                        rowList:[10,20,30],
                        pager: $("#page"),
                        caption: "<%=title%> ",
                        gridComplete: function(){
                        $("#list tr:nth-child(even)").css("background-color", "#fff");
                    }
                    }).navGrid('#page',{edit:false,add:false,del:false},{},{},{},prmSearch);

                    grid.searchGrid(prmSearch);

                    // find the div which contain the searching dialog
                    var searchDialog = $("#fbox_"+grid[0].id);

                    // make the searching dialog non-popup
                    searchDialog.css({position:"relative", "z-index":"auto"});
                });*/
        /*jQuery().ready(function (){
                    //alert("url:" + < %=link%>);
                    jQuery("#list").jqGrid({
                        url: "../GovtUserGridSrBean?action=viewGovData",
                        datatype: "xml",
                        height: 250,
                        colNames:['S. No.','Full Name','Organization Name','Role','e-mail ID','Date','Status','Action'],
                        colModel:[
                            {name:'srno',index:'srno', width:20, search: false,sortable:false,align:'center'},
                            {name:'employeeName',index:'employeeName', width:70,sortable:true, searchoptions: { sopt: ['eq', 'cn'] }},
                            {name:'organizationName',index:'organizationName', width:110,sortable:true, searchoptions: { sopt: ['eq', 'cn'] }},
                            {name:'role',index:'role', width:60,sortable:true,align:'center', searchoptions: { sopt: ['eq', 'cn'] }},
                            {name:'emailid',index:'emailid', width:120,sortable:true, searchoptions: { sopt: ['eq', 'cn'] }},
                            {name:'sdate',index:'sdate', width:60,sortable:true,search: false,align:'center'},
                            {name:'status',index:'status', width:60,sortable:true,align:'center', searchoptions: { sopt: ['eq'] }},
                            {name:'action',index:'action', width:120,sortable:false, search: false,align:'center'},
                        ],
                        autowidth: true,
                        multiselect: false,
                        paging: true,
                        rowNum:10,
                        rowList:[10,20,30],
                        pager: $("#page"),
                        caption: "<%=title%> ",
                        gridComplete: function(){
                        $("#list tr:nth-child(even)").css("background-color", "#fff");
                    }
                    }).navGrid('#page',{edit:false,add:false,del:false});
                });*/
    </script>

    <script type="text/javascript"> 
        
    var Interval;
    clearInterval(Interval);
    Interval = setInterval(function(){
        var PageNo = document.getElementById('pageNo').value;
        var Size = document.getElementById('size').value;
        CorrectSerialNumber(PageNo-1,Size); 
    }, 100);
        
    function checkKey(e)
    {
        var keyValue = (window.event)? e.keyCode : e.which;
        if(keyValue == 13){
            //Validate();
            $('#pageNo').val('1')
            $('#btnSearch').click();
            //loadTable();
        }
    }
        
        
    function checkKeyGoTo(e)
    {
        var keyValue = (window.event)? e.keyCode : e.which;
        if(keyValue == 13){
            //Validate();
            $(function() {
            //$('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            //loadTable();
                            $('#btnGoto').click();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            //});
        }
    }
    </script>
    <%
        govSrUser = null;
        govGridSrUser = null;
    %>

</html>