<%--
Document   : PEAdminRegister
Created on : Oct 23, 2010, 6:13:00 PM
Author     : Sanjay,rishita
--%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblStateMaster"%>
<%@page import="com.cptu.egp.eps.web.databean.OfficeAdminDtBean"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="java.util.List"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create PA Admin</title>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js" type="text/javascript"></script>

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
//            function myFunction() {
//            alert("The input value has changed. The new value is: ");
//        }
            $(document).ready(function () {
                var validCheckMail = true;
                var validCheckNid = true;
                var validCheckMno = true;
                $("#frmPEAdmin").validate({
                    rules: {
                        txtdepartment: {required: true},
                        office: {required: true},
                        emailId: {required: true},
                        district: {required: true},
                        password: {spacevalidate: true, requiredWithoutSpace: true, maxlength: 25, minlength: 8, alphaForPassword: true},
                        //, spacevalidate: true
                        confPassword: {required: true, equalTo: "#txtPassword"},
                        //, spacevalidate: true
                        //fullName: { required: true, alphaName:true,maxlength: 100 },
                        //nationalId: { alphaNationalId:true,maxlength: 25},
                        nationalId: {required: true, number: true, minlength: 11, maxlength: 11},
                        phoneNo: {required: true, numberWithHyphen: true, PhoneFax: true},
                        mobileNo: {required: true, number: true, minlength: 8, maxlength: 8}
                    },
                    messages: {
                        txtdepartment: {required: "<div class='reqF_1'>Please select Organization</div>"},
                        office: {required: "<div class='reqF_1'>Please select Office</div>"},
                        emailId: {required: "<div class='reqF_1'>Please enter e-mail ID</div>"
                        },
                        district: {required: "<div class='reqF_1'>Please select Dzongkhag / District</div>"
                        },
                        password: {spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace: "<div class='reqF_1'>Please enter Password</div>",
                            alphaForPassword: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>",
                            //spacevalidate: "<div class='reqF_1'>Space is not allowed.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>",
                            minlength: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>"
                        },
                        confPassword: {required: "<div class='reqF_1'>Please re-type Password</div>",
                            equalTo: "<div class='reqF_1'>Password does not match. Please try again</div>"
                                    //spacevalidate: "<div class='reqF_1'>Space is not allowed.</div>"
                        },
                        /*fullName: { required: "<div class='reqF_1'>Please enter Full Name</div>",
                         alphaName:"<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , ' \" } { - . _) Only</div>",
                         maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>"
                         },*/

                        /*nationalId: {
                         alphaNationalId: "<div class='reqF_1'>Only special characters not allowed</div>",
                         maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>"},*/
                        nationalId: {
                            required: "<div class='reqF_1'>Please enter CID No.</div>",
                            number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                            minlength: "<div class='reqF_1'>CID should comprise of minimum 11 digits</div>",
                            maxlength: "<div class='reqF_1'>CID should comprise of maximum 11 digits</div>"},
                        phoneNo: {required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            numberWithHyphen: "<div class='reqF_1'>Allows numbers (0-9) and hyphen (-) only</div>",
                            PhoneFax: "<div class='reqF_1'>Please enter valid Phone No. Area code should contain minimum 2 digits and maximum 5 digits. Phone no. should contain minimum 3 digits and maximum 10 digits</div>"},
                        mobileNo: {
                            required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                            number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                            minlength: "<div class='reqF_1'>Minimum 8 digits are required</div>",
                            maxlength: "<div class='reqF_1'>Allows maximum 8 digits only</div>"
                        }
                    },
                    errorPlacement: function (error, element)
                    {
                        if (element.attr("name") == "txtdepartment")
                        {
                            error.insertAfter("#ancdeptTreeIcn");
                        } else if (element.attr("name") == "password")
                        {
                            error.insertAfter("#tipPassword");
                        } else if (element.attr("name") == "phoneNo")
                        {
                            error.insertAfter("#fxno")
                        } else
                            error.insertAfter(element);
                    },
                    invalidHandler: function (e, validator) {
                        var errors = validator.numberOfInvalids();
                        document.getElementById("btnAdd").disabled = false;
                    }
                }
                );
            });
            function chkregFullName(value)
            {
                return /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/.test(value);


            }
            function whiteSpace(value) {
                //return /^[ \t]+|[ \t]+$ /.test(value);
                return /^\s+|\s+$/g.test(value);
                //return /[ \t]/.test(value);

            }
            var bvalidationFullName;
            function validationFullname() {
                bvalidationFullName = true;
                if ($('#txtFullName').val() == '') {
                    $('#SearchValErrorFullName').html('Please enter Full Name');
                    bvalidationFullName = false;
                }/*else if(document.getElementById('txtFullName').value.charAt(0) == " "){
                 $('#SearchValErrorFullName').html('Only Space is not allowed');
                 bvalidationFullName = false;
                 }*/else if (whiteSpace($('#txtFullName').val())) {
                    $('#SearchValErrorFullName').html('Only Space is not allowed');
                    bvalidationFullName = false;
                } else if (!chkregFullName($('#txtFullName').val())) {
                    $('#SearchValErrorFullName').html('Allows Characters (A to Z) & Special Characters (& , \' " } { - . _) Only');
                    bvalidationFullName = false;
                } else if ($('#txtFullName').val().length > 100) {
                    $('#SearchValErrorFullName').html('Allows maximum 100 characters only');
                    bvalidationFullName = false;
                } else {
                    $('#SearchValErrorFullName').html('');
                }
            }

            function checkctrlkey() {
                //if(e.keyCode==17 || e.keyCode==93){
                jAlert("Copy Paste not allowed.", "PE Admin Register", function (RetVal) {
                });
                return false;
                //}
            }

            function checkrightclick(e) {
                $(e).bind("contextmenu", function (e) {
                    return false;
                });
            }
        </script>
    </head>
    <jsp:useBean id="peAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.PEAdminSrBean"/>
    <jsp:useBean id="adminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AdminMasterDtBean"/>
    <!--Code added by Palash, Dohatec-->
    <jsp:useBean id="adminTransferDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AdminTransferDtBean"/>
    <jsp:useBean id="loginMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.LoginMasterDtBean"/>
    <jsp:useBean id="officeAdminDtBean" scope="request" class="com.cptu.egp.eps.web.databean.OfficeAdminDtBean"/>
    <jsp:setProperty name="adminMasterDtBean" property="*"/>
    <!--Code added by Palash, Dohatec-->
    <jsp:setProperty name="adminTransferDtBean" property="*"/>
    <jsp:setProperty name="loginMasterDtBean" property="*"/>
    <jsp:setProperty name="officeAdminDtBean" property="*"/>
    <body>
        <%
            if ("Submit".equals(request.getParameter("add"))) {
                String msg = "";
                int strM = -1; // Default
                String mailId = loginMasterDtBean.getEmailId();
                //  String nationalId = adminMasterDtBean.getNationalId();
                String mobileNo = "";

                if (adminMasterDtBean.getMobileNo() != null) {
                    mobileNo = adminMasterDtBean.getMobileNo();
                }

                if (!mailId.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-\\-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                    strM = 1; //Not Valid EmailId
                }
                if (strM == -1) {
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    msg = commonService.verifyMail(mailId.trim());
                    if (msg.length() > 2) {
                        strM = 2; //EmailId Already Exist
                    }
                    /* if (strM == -1) {
                                msg = commonService.verifyNationalId(nationalId.trim());
                                if (msg.length() > 2) {
                                    strM = 3; //NationalId Already Exist
                                }
                            }*/

 /*if (strM == -1) {
                                msg = commonService.verifyMobileNo(mobileNo.trim());
                                if (msg.length() > 2) {
                                    strM = 4; //Mobile No Already Exist
                                }
                            }*/
                }
                if (strM == -1) {

                    if (adminMasterDtBean.getMobileNo() == null) {
                        adminMasterDtBean.setMobileNo("");
                    }
                    if (adminMasterDtBean.getNationalId() == null) {
                        adminMasterDtBean.setNationalId("");
                    }

                    peAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
// <!--Code added by Palash, Dohatec-->
                    int peId = peAdminSrBean.peAdminReg(adminMasterDtBean, adminTransferDtBean, loginMasterDtBean, request.getParameterValues("office"), request.getParameter("password"));
                    // End
                    if (peId > 0) {
                        response.sendRedirect("ViewPEAdmin.jsp?userId=" + peId + "&userTypeid=4&msg=create");
                    } else {
                        response.sendRedirect("PEAdminRegister.jsp?msg=adminCreateFail");
                    }
                } else {
                    response.sendRedirect("PEAdminRegister.jsp?strM=" + strM);
                }
            } else {
        %>

        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("hdnUserType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                    userType.append(request.getParameter("hdnUserType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }

                            //int userTypeId = 0;
                            //if(session.getAttribute("userTypeId")!=null && !"".equalsIgnoreCase(session.getAttribute("userTypeId").toString())){
                            //   userTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
                            //}
                            int userId = 0;
                            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                userId = Integer.parseInt(session.getAttribute("userId").toString());
                                peAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                            }

                            if (userTypeId == 5) {
                                peAdminSrBean.getUserTypeWiseDepartmentId(userId, userTypeId);
                            }

                            List<OfficeAdminDtBean> allOfficeListDetails = peAdminSrBean.getOfficeList();
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                            <td class="contentArea_1">
                            <%
                                int strM = -1;
                                String msg = "";
                                if (request.getParameter("strM") != null) {
                                    try {
                                        strM = Integer.parseInt(request.getParameter("strM"));
                                    } catch (Exception ex) {
                                        strM = 5;
                                    }
                                    switch (strM) {
                                        case (1):
                                            msg = "Please enter valid e-mail ID.";
                                            break;
                                        case (2):
                                            msg = "e-mail ID already exists, Please enter another e-mail ID.";
                                            break;
                                        case (3):
                                            msg = "CID already exists.";
                                            break;
                                        case (4):
                                            msg = "Mobile No already exists.";
                                            break;
                                    }
                            %>
                            <div class="responseMsg errorMsg" style="margin-left:7px; margin-right: 2px; margin-top: 15px;">
                                <%= msg%>
                            </div>
                            <%
                                    msg = null;
                                }
                            %>
                            <div>&nbsp;</div>
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1"><div class="pageHead_1">Create Procuring Agency (PA) Admin</div>
                                        <% if (request.getParameter("msg") != null && "adminCreateFail".equals(request.getParameter("msg"))) {%>
                                        <div class='responseMsg errorMsg'><%=appMessage.peAdminReg%></div>
                                        <% } %>
                                        <form id="frmPEAdmin" name="frmPEAdmin" id="frmPEAdmin" method="post">
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td style="font-style: italic" class="t-align-right" colspan="2"><normal>Fields marked with (<span class="mandatory">*</span>) are mandatory</normal></td>
                                                </tr>
                                                <%if (userTypeId == 1) {%>
                                                <tr>
                                                    <td class="ff" width="200">Select Hierarchy Node : <span>*</span></td>
                                                    <td>
                                                        <input class="formTxtBox_1"  name="txtdepartment" type="text" style="width: 400px;"
                                                               id="txtdepartment" style="width: 400px;"/>
                                                        <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />
                                                        <a id="ancdeptTreeIcn" href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=peadmin', '', 'width=350px,height=400px,scrollbars=1', '');">
                                                            <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                        </a>
                                                    </td>
                                                </tr>
                                                <%
                                                } else {
                                                    short deptId = 0;
                                                    deptId = peAdminSrBean.getDepartmentIdByUserId(userId);
                                                %>
                                                <input type="hidden" name="txtdepartmentid" id="txtdepartmentid" value="<%=deptId%>"/>
                                                <%}%>
                                                <%--<tr>
                                                    <td class="ff" width="200">Dzongkhag / District : <span>*</span></td>
                                                    <td>
                                                        <%
                                                            short countryId = 150;
                                                            List<TblStateMaster> stateMasters = peAdminSrBean.getBanglaStates(countryId);
                                                            request.setAttribute("stateMasters", stateMasters);
                                                        %>
                                                        <select style="width:200px;" class="formTxtBox_1" name="district" id="cmbdistrict" onchange="getOfficesByState();">
                                                            <option value="">-- Select Dzongkhag / District --</option>
                                                            <c:forEach var="slist" items="${stateMasters}">                                                     
                                                                <option  value="${slist.stateId}">${slist.stateName}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </td>
</tr>--%>

                                                <tr>
                                                    <td class="ff">Select Office : <span>*</span></td>
                                                    <td>
                                                        <select style="width:200px;" class="formTxtBox_1" name="office" id="cmbOffice" onfocus="getDepartements();">
                                                            <option  value="">-- Select Office --</option>
                                                            <%//if (userTypeId == 5) {%>
                                                            <!--<-c:forEach var="cList" items="-{peAdminSrBean.officeList}">
                                                                <option  value="-{cList.officeId}">-{cList.officeName}</option>
                                                            <-/c:forEach>-->
                                                            <%//}%>
                                                        </select>
                                                        <span id="officeMsg" style="color: red; font-weight: bold"></span>
                                                        <div id="readonlyMsg" class="reqF_1"></div>
                                                        <div id="errMsg" style="color: red;"></div>
                                                        <div id="Msg" style="color: red; font-weight: bold"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">e-mail ID: <span>*</span></td>
                                                    <td>
                                                        <input style="width:195px;" class="formTxtBox_1" type="text" id="txtMail" name="emailId" maxlength="100"/>
                                                        <span id="mailMsg" style="color: red; font-weight: bold"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Password : <span>*</span></td>
                                                    <td>
                                                        <input style="width:195px;" class="formTxtBox_1" type="password" id="txtPassword" name="password" maxlength="25" autocomplete="off" /><br/><span id="tipPassword" style="color: grey;" > ( Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. 
                                                <br/>Special characters may be added) </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Confirm Password : <span>*</span></td>
                                                    <td>
                                                        <input style="width:195px;" class="formTxtBox_1" type="password" id="txtConfPassword" name="confPassword" value="" onpaste="return checkctrlkey();" autocomplete="off" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Full Name : <span>*</span></td>
                                                    <td>
                                                        <input style="width:195px;" class="formTxtBox_1" type="text" onblur="validationFullname();" id="txtFullName" name="fullName" maxlength="101"/>
                                                        <div id="SearchValErrorFullName" class="reqF_1"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">CID No: <span>*</span></td>

                                                    <td><input style="width:195px;" class="formTxtBox_1" maxlength="26" type="text" id="txtNationalId" name="nationalId"/>
                                                        <span id="errMsg" style="color: red; font-weight: bold"></span>
                                                    </td>
                                                </tr>
                                                <!--<tr>
                                                    <td class="ff">Phone No. : <span>*</span></td>
                                                    <td>
                                                        <input type="hidden" style="width:195px;" class="formTxtBox_1" type="text" name="phoneNo" id="txtPhoneNo" maxlength="17" value="11-11111111"/>
                                                        <span id="fxno" style="color: grey;"> ( Area Code - Phone No. e.g. 02-324425) </span>
                                                    </td>
                                                </tr>-->
                                                <tr>
                                                    <td class="ff">Mobile No. : <span>*</span></td>
                                                    <td>
                                                        <input style="width:195px;" class="formTxtBox_1" type="text" name="mobileNo" id="txtMobileNo" maxlength="16"/><span id="mNo" style="color: grey;"> (Mobile No. format should be e.g 12345678)</span>
                                                        <span id="mobMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td align="left">
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="add" id="btnAdd" value="Submit" onClick="return validate(this);"/>
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                </div>
            </div>
            <script>
                var obj = document.getElementById('lblPEAdminCreation');
                if (obj != null) {
                    if (obj.innerHTML == 'Create PA Admin') {
                        obj.setAttribute('class', 'selected');
                    }
                }

                var headSel_Obj = document.getElementById("headTabMngUser");
                if (headSel_Obj != null) {
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
        </body>
    <%
        }
    %>
    <script type="text/javascript">
        var flag;
        $(function () {
            $('#txtMail').blur(function () {
                var varEmailId = trim(document.getElementById("txtMail").value);
                if (varEmailId != "") {
                    $('span.#mailMsg').html("Checking for unique Mail Id...");
                    $.post("<%=request.getContextPath()%>/CommonServlet", {mailId: varEmailId, funName: 'verifyMail'}, function (j) {
                        if (j.toString().indexOf("OK", 0) != -1) {
                            $('span.#mailMsg').css("color", "green");
                            validCheckMail = true;
                        } else if (j.toString().indexOf("Mail", 0) != -1) {
                            $('span.#mailMsg').css("color", "red");
                            validCheckMail = false;
                        }
                        $('span.#mailMsg').html(j);
                    });
                } else {
                    $('span.#mailMsg').html("");
                }
            });
            
            $('#cmbOffice').blur(function () {
                //alert("The input value has changed. The new value is: ");
                flag=true;
                $('#Msg').css("color", "red");
                $('#Msg').html("Checking for unique Office Admin...");
                $.post("<%=request.getContextPath()%>/CommonServlet", {officeId: $('#cmbOffice').val(), funName: 'verifyOffice'},
                        function (j)
                        {
                            if (j.toString().indexOf("OK", 0) != -1) {
                                $('#Msg').css("color", "green");
                                $('#errMsg').html('');
                            } else if (j.toString().indexOf("Admin", 0) != -1) {
                                flag=false;
                                $('#Msg').css("color", "red");
                                $('#errMsg').html('');

                            }
                            //$('span.#Msg').html(j);

                            //alert(document.getElementById('oName'));
                            if (document.getElementById('oName') == null) {
                                $('#Msg').html(j);
                                $('#errMsg').html('');
                            } else {
                                document.getElementById('oName').removeAttribute('id');
                                $('#Msg').html('');
                                $('#errMsg').html('');
                            }
                        });
                
                
            });
        });
        


    </script>
    <script type="text/javascript">
        $(function () {
            $('#frmPEAdmin').submit(function () {
                if (flag == true) {
                    if ($('#frmPEAdmin').valid()) {
                        if($('#Submit')!=null){
                         $('#Submit').attr("disabled","true");
                         $('#hdnbutton').val("Submit");
                         }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            });
        });
        function getDepartements() {
            $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId: $('#txtdepartmentid').val(), funName: 'Office'}, function (j) {
                $("select#cmbOffice").html(j);
            });
        }

        $(function () {
            $('#txtMobileNo').blur(function () {
                var varMobileNo = trim(document.getElementById("txtMobileNo").value);
                if (varMobileNo != "") {
                    if ((document.getElementById("txtMobileNo").value.length >= 10) && (document.getElementById("txtMobileNo").value.length <= 15) && (/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(document.getElementById("txtMobileNo").value))) // max min number
                    {
                        /*$('span.#mobMsg').html("Checking for unique Mobile No...");
                         $.post("<%=request.getContextPath()%>/CommonServlet", {mobileNo:varMobileNo,funName:'verifyMobileNo'},  function(j){
                         
                         if(j.toString().indexOf("OK", 0)==0){
                         $('span.#mobMsg').css("color","green");
                         validCheckMno = true;
                         }
                         else if(j.toString().indexOf("Mobile", 0)!=-1){
                         $('span.#mobMsg').css("color","red");
                         validCheckMno = false;
                         }
                         $('span.#mobMsg').html(j);
                         });*/
                    }
                } else {
                    $('span.#mobMsg').html("");
                }
            });
        });

        /* $(function() {
         $('#txtNationalId').blur(function() {
         var varNationalId = trim(document.getElementById("txtNationalId").value);
         if(varNationalId!=""){
         if($('#frmPEAdmin').valid())
         {
         // $('span.#errMsg').html("Checking for unique National Id...");
         $.post("<%//request.getContextPath()%>/CommonServlet", {nationalId:varNationalId,funName:'verifyNationalId'},  function(j){
         if(j.toString().indexOf("OK", 0)!=-1){
         $('span.#errMsg').css("color","white");
         validCheckNid = true;
         }
         else if(j.toString().indexOf("National", 0)!=-1){
         $('span.#errMsg').css("color","red");
         validCheckNid = false;
         }
         $('span.#errMsg').html(j);
         });
         }
         }else{
         $('span.#errMsg').html("");
         }
         });
         });*/
        $(function () {
            /*if (document.getElementById("txtdepartmentid") != "") {
             $('#txtdepartment').blur(function () {
             $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId: $('#txtdepartmentid').val(), funName: 'Office'}, function (j) {
             $("select#cmbOffice").html(j);
             });
             });
             }*/
        });

        /*function check(){
         if(document.getElementById("cmbOffice").value==0){
         document.getElementById("officeMsg").innerHTML = "<br/>Please select Office.";
         return false;
         }else{
         document.getElementById("officeMsg").innerHTML = "";
         return true;
         }
         }*/
        function validate(obj) {
            if (document.getElementById("mailMsg") != null && document.getElementById("mailMsg").innerHTML == "e-mail ID already exists, Please enter another e-mail ID.") {
                return false;
            }
            if (document.getElementById("errMsg") != null && document.getElementById("errMsg").innerHTML == "CID already exists.") {
                return false;
            }
            if (document.getElementById("mobMsg") != null && document.getElementById("mobMsg").innerHTML == "Mobile No. already exists.") {
                return false;
            }
            /*if(document.getElementById("cmbOffice").value=="0"){
             document.getElementById("officeMsg").innerHTML = "<br/>Please select Office.";
             return false;
             }*/
            bvalidation = true;
            if ($('#txtFullName').val() == '') {
                $('#SearchValErrorFullName').html('Please enter Full Name');
                bvalidation = false;
            }
            if (validCheckMail && validCheckNid && validCheckMno) {
                if ($('#frmPEAdmin').valid()) {
                    //$('#btnAdd').attr("disabled", true);
                    $('#btnAdd').hide();
                    //document.getElementById("frmPEAdmin").action="PEAdminRegister.jsp?Submit=add";
                    //document.getElementById("frmPEAdmin").submit();
                    return true;
                }
            } else {
                $('#btnAdd').show();
                return false;
            }
        }


        function getOfficesByState() {

            $.post("<%=request.getContextPath()%>/CommonServlet", {deptId: $('#txtdepartmentid').val(), stateId: $('#cmbdistrict').val(), funName: 'officeListByStateId'}, function (j) {
                $("select#cmbOffice").html(j);
            });
        }

    </script>
</html>
