<%-- 
    Document   : ViewNewsDetails
    Created on : Nov 19, 2010, 3:11:26 PM
    Author     : Karan
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <%
            String Type = "";
            
            if(request.getParameter("newsType").equalsIgnoreCase("N"))
            {
                Type = "News/Advertisement";
            }
            else if(request.getParameter("newsType").equalsIgnoreCase("C"))
            {
                Type = "Circular";
            }
            else if(request.getParameter("newsType").equalsIgnoreCase("A"))
            {
                Type = "Amendment";
            }
            else if(request.getParameter("newsType").equalsIgnoreCase("I"))
            {
                Type = "Notification";
            }
        %>
        <title><%=Type%> Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js">

            /***********************************************
             * All Levels Navigational Menu- (c) Dynamic Drive DHTML code library (http://www.dynamicdrive.com)
             * This notice MUST stay intact for legal use
             * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
             ***********************************************/

        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
                        <!-- Yr Code -->
                         <!--Dashboard Content Part Start-->
            <div class="pageHead_1">
                         <%
                            String referer = "";
                            if (request.getHeader("referer") != null) {
                                referer = request.getHeader("referer");
                            }
                        %>
                        <%=Type%> Details <span style="float:right;"><a href="<%=referer%>" class="action-button-goback">Go Back</a></span></div>
        
              <table border="0" cellspacing="10" cellpadding="0" class="tableList_1 t_space" width="100%" style="color:#333; text-align: justify;">

                
                <%
                            String newsId = "";
                            String newsType = "";
                            if (!request.getParameter("newsType").equals("")) {
                                newsId = request.getParameter("nId");
                                newsType = request.getParameter("newsType");
                            }

                            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                            String userID="0";
                            if(session.getAttribute("userId")!=null){
                            userID=session.getAttribute("userId").toString();
                            }
                            tenderCommonService.setLogUserId(userID);
                            String action = "";
                            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                            try{
                                action = "View News";
                                List<SPTenderCommonData> list = tenderCommonService.returndata("getnewsbyid", newsId, newsType);
                                String Time = null;
                                String FileName = null;

                                if(list.get(0).getFieldName6()!=null && !list.get(0).getFieldName6().equals(""))
                                {
                                    Time = DateUtils.SqlStringDatetoApplicationStringDate(list.get(0).getFieldName6());
                                }
                                if(list.get(0).getFieldName8()!=null && !list.get(0).getFieldName8().equals(""))
                                {
                                    FileName = list.get(0).getFieldName8();
                                }
                                if (!list.isEmpty()) {

                %>

                <tr>
                    <td width="20%" class="t-align-left">
                        <%=Type%> Brief :
                    </td>

                    <td style="padding-left:20px;text-align: justify;"><%= list.get(0).getFieldName4()%></td>
                </tr>
                
                <tr>
                    <td class="t-align-left" width="20%">
                        <%=Type%> Details :
                    </td>
                    <td style="padding-left:20px;text-align: justify;"><%=list.get(0).getFieldName5()%></td>
                </tr>
                <% if(!request.getParameter("newsType").equalsIgnoreCase("N")) { %>
                    <tr>
                        <td class="t-align-left" width="20%">
                            Uploaded file :
                        </td>
                        <td style="padding-left:20px;text-align: justify;">
                            <% if(FileName != null) {%>
                                    <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?funName=downloadNewDocuments&FileName=<%=FileName%>" title="Download" target="_blank">View Current file<a>
                            <% } %>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left" width="20%">
                            Letter Number :
                        </td>
                        <td style="padding-left:20px;text-align: justify;">
                            <%  
                                if(list.get(0).getFieldName7()!=null) 
                                {
                                    out.print(list.get(0).getFieldName7()); 
                                }
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td class="t-align-left" width="20%">
                            Time :
                        </td>
                        <td style="padding-left:20px;text-align: justify;">
                            <% 
                                if(Time!=null) 
                                { 
                                    out.print(Time);
                                }
                            %>
                        </td>
                        
                    </tr>
                    <% } %>
<!--                <tr>
                    <td class="ff">Importance : </td>
                    <td style="text-align: justify;"></td>
                </tr>-->
                <%}
               }catch(Exception e){
                   action = "Error in "+action+" "+e.getMessage();
               }finally{
                   makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()), Integer.valueOf(session.getAttribute("userId").toString()), "userId", EgpModule.Content.getName(), action, "");
               }
            %>
            </table>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
                    </td>
                </tr>
</table>

           
            <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabContent");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>

