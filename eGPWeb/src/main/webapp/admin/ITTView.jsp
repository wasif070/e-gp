<%--
    Document   : CreateSectionClause
    Created on : 24-Oct-2010, 1:03:35 AM
    Author     : yanki
--%>
<jsp:useBean id="sectionClauseSrBean" class="com.cptu.egp.eps.web.servicebean.SectionClauseSrBean" />
<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBean" />
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />

<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster" %>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.model.table.TblIttClause" %>
<%@page import="com.cptu.egp.eps.model.table.TblIttSubClause" %>
<%@page import="com.cptu.egp.eps.model.table.TblIttHeader" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");

        if(session.getAttribute("userId") != null){
            sectionClauseSrBean.setLogUserId(session.getAttribute("userId").toString());
            createSubSectionSrBean.setLogUserId(session.getAttribute("userId").toString());
            defineSTDInDtlSrBean.setLogUserId(session.getAttribute("userId").toString());
        }
%>
         <%
            int templateId = 0;
            if(request.getParameter("templateId")!=null){
                templateId = Integer.parseInt(request.getParameter("templateId"));
            }
            String procType = "";
             String procTypefull = "";
            String STDName = "";
            
            List<TblTemplateMaster> templateMasterLst = defineSTDInDtlSrBean.getTemplateMaster((short) templateId);
            if(templateMasterLst != null){
                if(templateMasterLst.size() > 0){
                    procType = templateMasterLst.get(0).getProcType();
                    if("goods".equalsIgnoreCase(procType)){
                        procTypefull = "Goods";
                    }else if("works".equalsIgnoreCase(procType)){
                        procTypefull = "Works";
                    }else if("srvcmp".equalsIgnoreCase(procType)){
                        procTypefull = "Services - Companies";
                    }else if("srvindi".equalsIgnoreCase(procType)){
                        procTypefull = "Services - Individual";
                }
                    else if("srvnoncon".equalsIgnoreCase(procType)){
                        procTypefull = "Services - Non Consultant";
                    }
                    STDName = templateMasterLst.get(0).getTemplateName();
                }
                templateMasterLst = null;
            }
            if(request.getParameter("sectionId") == null){
                return;
            }
            
            int sectionId = Integer.parseInt(request.getParameter("sectionId"));
             String isPDF= "abc";
                            if(request.getParameter("isPDF") != null)
                                    isPDF= request.getParameter("isPDF");
             
            List<TblIttHeader> headerInfo = createSubSectionSrBean.getSubSectionDetail(sectionId);
            String contentType = createSubSectionSrBean.getContectType(sectionId);
            String sectionName = "";
            sectionName = createSubSectionSrBean.getSectionName(sectionId);
            
            if("srvcmp".equals(procType)){
                if("ITT".equals(contentType)){
                    contentType = contentType.replace("ITT", "ITC");
                }else if("TDS".equals(contentType)){
                    contentType = contentType.replace("TDS", "PDS");
                }
            }else if("srvindi".equals(procType)){
                if("ITT".equals(contentType)){
                    contentType = contentType.replace("ITT", "ITA");
                }
            }else{
                 if("ITT".equals(contentType)){
                    contentType = contentType.replace("ITT", "ITB");
                }else if("TDS".equals(contentType)){
                    contentType = contentType.replace("TDS", "BDS");
                }
            }
            String contentType1 = "";
            if(contentType.equalsIgnoreCase("ITT")){
                contentType1 = "BDS";
            }else if(contentType.equalsIgnoreCase("ITC")){
                contentType1 = "PDS";
            }else if(contentType.equalsIgnoreCase("GCC")){
                contentType1 = "SCC";
            }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View <%=contentType%></title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <%--<style type="text/css">
            ul li {margin-left: 20px;}
        </style>--%>
    </head>
    <body>

        <div class="mainDiv">
            <% if(!(isPDF.equalsIgnoreCase("true")) ){  %>
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <% } %>
            <div class="fixDiv">
                <!--Middle Content Table Start-->
                <div class="contentArea_1">
                    <div class="pageHead_1">View <%=contentType%></div>
                        <form action="<%=request.getContextPath()%>/SectionClauseSrBean" method="post"  onsubmit="return validate();">
                            <%--/PrepareTDS.jsp--%>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td width="30%" align="center"><b>Name of SBD :</b></td>
                                    <td width="70%"><%=STDName%></td>
                                </tr>
                                <tr>
                                    <td width="30%" align="center"><b>Procurement Type :</b></td>
                                    <td width="70%"><%=procTypefull%></td>
                                </tr>
                                <tr>
                                    <td width="30%" align="center"><b>Section :</b></td>
                                    <td width="70%"><%=sectionName%></td>
                                </tr>
                            </table>
                            <table id="clauseData" width="100%" cellspacing="0" class="tableList_1 t_space">
                            <%
                                for(int k=0; k< headerInfo.size(); k++){
                                    String subSectionName = headerInfo.get(k).getIttHeaderName();
                                    int ittHeaderId = headerInfo.get(k).getIttHeaderId();
                                %>
                                    <tr id="tr_0">
                                        <td colspan="2" class="t-align-center"><b><%=subSectionName%></b></td>
                                    </tr>
                                    <%
                                    List<TblIttClause> tblIttClause = sectionClauseSrBean.getClauseDetail(ittHeaderId);
                                    int ittClauseId = -1;
                                    for(int i=0; i<tblIttClause.size(); i++){
                                        int clauseId = tblIttClause.get(i).getIttClauseId();
                                    %>
                                    <tr id="tr_<%=(i+1)%>_1" >
                                        <td width="30%" style="line-height: 1.75">
                                            <%
                                            if(ittClauseId != tblIttClause.get(i).getIttClauseId()){
                                                ittClauseId = tblIttClause.get(i).getIttClauseId();
                                                out.print(tblIttClause.get(i).getIttClauseName());
                                            } else {
                                                out.print("&nbsp;");
                                            }
                                            %>
                                        </td>
                                    <%
                                        List<TblIttSubClause> tblIttSubClause = sectionClauseSrBean.getSubClauseDetail(clauseId);
                                        int j = 0;
                                        for(j=0;j<tblIttSubClause.size();j++){
                                            tblIttSubClause.get(j).getIttSubClauseId();
                                            tblIttSubClause.get(j).getIttSubClauseName();
                                            if(j != 0){
                                        %>
                                            <tr id="tr_<%=(i+1)%>_<%=(j+1)*2%>">
                                                <td>&nbsp;</td>
                                        <%
                                            }
                                        %>
                                                <td width="70%" style="line-height: 1.75">
                                                    <%=tblIttSubClause.get(j).getIttSubClauseName()%>
                                                </td>
                                            </tr>

                                        <%
                                        }
                                    }
                                }
                                %>
                                </table>
                            </form>
                                </div>
                        <!--Page Content End-->
                <!--Middle Content Table End-->
                <% if(! (isPDF.equalsIgnoreCase("true")) ){  %>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <% } %>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<%
createSubSectionSrBean = null;
sectionClauseSrBean = null;
%>
<%
    if(defineSTDInDtlSrBean != null){
        defineSTDInDtlSrBean = null;
    }
%>