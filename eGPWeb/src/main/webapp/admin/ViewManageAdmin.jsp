<%-- 
    Document   : viewManageAdmin
    Created on : Nov 16, 2010, 4:49:50 PM
    Author     : rishita
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.databean.ManageAdminDtBean" %>
<%@page import="com.cptu.egp.eps.model.table.TblUserActivationHistory"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

                    byte userTypeid = 0;
                    int iSessionUserTypeId = 0;
                    boolean onmViewProfile = false;
                    if (request.getParameter("userTypeid") != null) {
                        userTypeid = Byte.parseByte(request.getParameter("userTypeid"));
                    }

                    if(session.getAttribute("userTypeId") != null){
                        iSessionUserTypeId = Integer.parseInt(((Object)session.getAttribute("userTypeId")).toString());
                    }

                    if(request.getParameter("frm")!=null && request.getParameter("frm").equalsIgnoreCase("vp") && (iSessionUserTypeId == 19 || iSessionUserTypeId == 20 || iSessionUserTypeId == 21))
                    {
                        onmViewProfile = true;
                    }
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <% if (userTypeid == 12) {%>
        <title>View Procurement Expert</title>
        <% } else if (userTypeid == 5) {%>
        <title>View Profile</title>
        <% } else if (userTypeid == 19) {%>
        <title>O&M Admin</title>
        <% } else {%>
        <title>View Admin</title>
        <% }%>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
    </head>
    <jsp:useBean id="manageAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.UpdateAdminSrBean"/>
    <jsp:useBean id="adminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AdminMasterDtBean"/>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%
                            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                manageAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                            }
                            int userId = 0;
                            if (request.getParameter("userId") != null) {
                                userId = Integer.parseInt(request.getParameter("userId"));
                            }

                            if ("Edit".equals(request.getParameter("editOandMUser"))) {
                                response.sendRedirect("OANDMRegister.jsp?uId=" + userId + "&userTypeid=" + userTypeid + "&isUser=Yes&action=Edit");
                            }
                            if ("Edit".equals(request.getParameter("edit"))) {
                                response.sendRedirect("ManageAdmin.jsp?userId=" + userId + "&userTypeid=" + userTypeid);
                            }
                            if ("OK".equals(request.getParameter("ok"))) {
                                if (userTypeid == 5) {
                                    response.sendRedirect("OrganizationAdminGrid.jsp?userTypeid=" + userTypeid);
                                } else if (userTypeid == 8) {
                                   // response.sendRedirect("ContentAdminGrid.jsp?userTypeid=" + userTypeid);
                                   response.sendRedirect("../resources/common/Dashboard.jsp");
                                } else if (userTypeid == 12) {
                                    response.sendRedirect("ProcExpertAdminGrid.jsp?userTypeid=" + userTypeid);
                                } else if (userTypeid == 19) {
                                    response.sendRedirect("OAndMAdminGrid.jsp?userTypeid=" + userTypeid);
                                }
                                else if (userTypeid == 20) {
                                    response.sendRedirect("OAndMAdminGrid.jsp?userTypeid=" + userTypeid);
                                }
                                else if (userTypeid == 21) {
                                    response.sendRedirect("OAndMAdminGrid.jsp?userTypeid=" + userTypeid);
                                }


                            }

                            if (request.getParameter("action") != null && request.getParameter("action").equalsIgnoreCase("view") && request.getParameter("ok") == null) {
                                // Coad added by Dipal for Audit Trail Log.
                                AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                                String idType = "userId";
                                int auditId = Integer.parseInt(request.getParameter("userId"));
                                String auditAction = "View Profile of Organization Admin";
                                if (request.getParameter("userTypeid") != null && request.getParameter("userTypeid").equalsIgnoreCase("8")) {
                                    auditAction = "View Profile of Content Admin";
                                } else if (request.getParameter("userTypeid") != null && request.getParameter("userTypeid").equalsIgnoreCase("12")) {
                                    auditAction = "View Profile of Procurement Expert User";
                                } else if (request.getParameter("userTypeid") != null && request.getParameter("userTypeid").equalsIgnoreCase("19")) {
                                    auditAction = "View O & M Admin";
                                }
                                else if (request.getParameter("userTypeid") != null && request.getParameter("userTypeid").equalsIgnoreCase("20")) {
                                    auditAction = "View O & M User";
                                }
                                else if (request.getParameter("userTypeid") != null && request.getParameter("userTypeid").equalsIgnoreCase("21")) {
                                    auditAction = "View Performance Monitoring User";
                                }


                                if ("View Account History".equals(request.getParameter("accountHistory"))) {
                                    auditAction = "View Account History of Organization Admin";
                                    if (request.getParameter("userTypeid") != null && request.getParameter("userTypeid").equalsIgnoreCase("8")) {
                                        auditAction = "View Account History of Content Admin";
                                    } else if (request.getParameter("userTypeid") != null && request.getParameter("userTypeid").equalsIgnoreCase("12")) {
                                        auditAction = "View Account History of Procurement Expert User";
                                    } else if (request.getParameter("userTypeid") != null && request.getParameter("userTypeid").equalsIgnoreCase("19")) {
                                        auditAction = "View Account History of O & M Admin";
                                    } else if (request.getParameter("userTypeid") != null && request.getParameter("userTypeid").equalsIgnoreCase("20")) {
                                        auditAction = "View Account History of O & M User";
                                    } else if (request.getParameter("userTypeid") != null && request.getParameter("userTypeid").equalsIgnoreCase("21")) {
                                        auditAction = "View Account History of Performance Monitoring User";
                                    }
                                }


                                String moduleName = EgpModule.Manage_Users.getName();
                                String remarks = "";
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
                            }

                            if ("View Account History".equals(request.getParameter("accountHistory"))) {
                                response.sendRedirect("ViewAdminAccountHistory.jsp?userTypeid=" + userTypeid + "&userId=" + userId);
                            }

                            TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
                            List<TblUserActivationHistory> list = transferEmployeeServiceImpl.viewAccountStatusHistory(userId + "");




                %>
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("hdnUserType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                            userType.append(request.getParameter("hdnUserType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }
                                    int uTypeId = 0;
                                    sn = request.getSession();
                                    if (sn.getAttribute("userTypeId") != null) {
                                        uTypeId = Integer.parseInt(sn.getAttribute("userTypeId").toString());
                                    }


                        %>
                        <% if (Integer.parseInt(session.getAttribute("userTypeId").toString()) == 1) {%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <% }%>
                        <td class="contentArea_1">
                            <%if (request.getParameter("msg") != null) {
                                            if (request.getParameter("msg").equals("success")) {
                                                if (Integer.parseInt(session.getAttribute("userTypeId").toString()) == 8 && userTypeid == 8) {%>
                            <div class="responseMsg successMsg">Profile updated successfully</div>
                            <div>&nbsp;</div>
                            <%} else if (userTypeid == 8) {%>
                            <div class="responseMsg successMsg">Content Admin updated successfully</div>
                            <div>&nbsp;</div>
                            <% } else if (userTypeid == 5) {
                                                                                if (uTypeId == 1) {%>
                            <div class="responseMsg successMsg">Organization admin updated successfully</div>
                            <div>&nbsp;</div>
                            <% } else {%>
                            <div class="responseMsg successMsg">Profile updated successfully</div>
                            <div>&nbsp;</div>
                            <%                                                                }
                                                                            } else if (userTypeid == 12) {%>
                            <div class="responseMsg successMsg">Procurement Expert User updated successfully</div>
                            <div>&nbsp;</div>
                            <%} else if (userTypeid == 19) {%>
                            <div class="responseMsg successMsg">O & M Admin updated successfully</div>
                            <div>&nbsp;</div>
                            <%} else if (userTypeid == 20) {%>
                            <div class="responseMsg successMsg">O & M User updated successfully</div>
                            <div>&nbsp;</div>
                            <%}
                            else if (userTypeid == 21) {%>
                            <div class="responseMsg successMsg">Performance Monitoring User updated successfully</div>
                            <div>&nbsp;</div>
                            <%}

                                                                        } else if (request.getParameter("msg").equals("create")) {
                                                                            if (Integer.parseInt(session.getAttribute("userTypeId").toString()) == 8 && userTypeid == 8) {%>
                            <div class="responseMsg successMsg">Profile created successfully</div>
                            <div>&nbsp;</div>
                            <%} else if (userTypeid == 8) {%>
                            <div class="responseMsg successMsg">Content Admin created successfully</div>
                            <div>&nbsp;</div>
                            <% } else if (userTypeid == 5) {
                            %>
                            <div class="responseMsg successMsg">Admin User created successfully</div>
                            <div>&nbsp;</div>
                            <%                                                            } else if (userTypeid == 12) {
                            %>
                            <div class="responseMsg successMsg">Procurement Expert User created successfully</div>
                            <div>&nbsp;</div>
                            <%                                            } else if (userTypeid == 19) {
                            %>
                            <div class="responseMsg successMsg">O & M Admin created successfully</div>
                            <div>&nbsp;</div>
                            <%                                            }
                                                                            else if (userTypeid == 20) {
                            %>
                            <div class="responseMsg successMsg">O & M User created successfully</div>
                            <div>&nbsp;</div>
                            <%                                            }
                                                                            else if (userTypeid == 21) {
                            %>
                            <div class="responseMsg successMsg">Performance Monitoring User created successfully</div>
                            <div>&nbsp;</div>
                            <%                                            }
                                            }
                                        }
                                        if (userTypeid == 12) {%>
                            <div class="pageHead_1">View Procurement Expert</div>
                            <div>&nbsp;</div>
                            <%} else if (Integer.parseInt(session.getAttribute("userTypeId").toString()) == 8 && userTypeid == 8) {%>
                            <div class="pageHead_1">View Profile</div>
                            <div>&nbsp;</div>
                            <%} else if (userTypeid == 8) {%>
                            <div class="pageHead_1">View Content Admin</div>
                            <div>&nbsp;</div>
                            <%} else if (userTypeid == 5) {%>
                            <div class="pageHead_1">View Organization Admin</div>
                            <div>&nbsp;</div>
                            <%} else if (userTypeid == 19 && iSessionUserTypeId != 19) {%>
                            <div class="pageHead_1">View O & M Admin</div>
                            <div>&nbsp;</div>
                            <%} else if (userTypeid == 20 && iSessionUserTypeId != 20) {%>
                            <div class="pageHead_1">View O & M User</div>
                            <div>&nbsp;</div>
                            <% } else if (userTypeid == 21 && iSessionUserTypeId != 21) {%>
                            <div class="pageHead_1">View Performance Monitoring User</div>
                            <div>&nbsp;</div>
                            <% } else {%>
                            <div class="pageHead_1">View Profile</div>
                            <div>&nbsp;</div>
                            <%}%>
                            <!--Page Content Start-->
                            <form id="frmManageAdmin" name="frmManageAdmin" method="post" action="">
                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10">
                                    <%
                                                if (!"1".equals(session.getAttribute("userTypeId").toString())) {
                                                    manageAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                                } else {
                                                    manageAdminSrBean.setAuditTrail(null);
                                                }
                                                for (ManageAdminDtBean manageAdminDtBean : manageAdminSrBean.getManageDetail(userTypeid, userId)) {
                                                    if (userTypeid == 5) {
                                    %>
                                    <tr>
                                        <td class="ff" width="200">Organization :</td>
                                        <td>
                                            <label id="lblOrgaization"><%= manageAdminDtBean.getDepartmentName()%></label>
                                        </td>
                                    </tr>
                                    <% }%>
                                    <tr>
                                        <td class="ff" width="200">e-mail ID :</td>
                                        <td>
                                            <label id="lblEmailId"><%= manageAdminDtBean.getEmailId()%></label>
                                        </td>
                                    </tr>
<%
                                    if(request.getParameter("userTypeid")!=null && request.getParameter("userTypeid").equalsIgnoreCase("20"))
                                    {
                                    %>
                                    <tr>
                                        <td class="ff" width="200">Role Type :</td>
                                        <td>
                                            <label id="lblEmailId"><%=(manageAdminDtBean.getRollId() >0?"Role Wise":"Individual") %></label>
                                        </td>
                                    </tr>
                                    <%
                                    }
                                    %>

                                    <tr>
                                        <td class="ff" width="200">Full Name :</td> 
                                        <td>
                                            <label id="lblFullName"><%= manageAdminDtBean.getFullName()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">CID No. :</td>
                                        <td>
                                            <label id="lblNationalId"><%=manageAdminDtBean.getNationalId()%></label>
                                            <input class="formTxtBox_1" name="nationalId" value="<%=manageAdminDtBean.getNationalId()%>" id="txtNationalID"  type="hidden" style="width: 200px;"/>
                                        </td>
                                    </tr>

                                   <!-- <tr>
                                        <td class="ff" width="200">Phone No. :</td>
                                        <td>
                                            <label id="lblPhoneNo"><%=manageAdminDtBean.getPhoneNo()%></label>
                                        </td>
                                    </tr>-->

                                    <tr>
                                        <td class="ff" width="200">Mobile No. :</td>
                                        <td>
                                            <label id="lblMobileNo"><%=manageAdminDtBean.getMobileNo()%></label>
                                        </td>
                                    </tr>
                                    <% }%>
                                    <%if (Integer.parseInt(session.getAttribute("userTypeId").toString()) == 1 || onmViewProfile == false) {%>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td align="left">
                                            <label class="formBtn_1"><input id="ok" name="ok" value="OK" type="submit"/></label>
                                            <%
                                            if(request.getParameter("userTypeid")!=null && !request.getParameter("userTypeid").equalsIgnoreCase("20"))
                                            {
                                            %>
                                            <label class="formBtn_1">
                                                
                                                    <input id="edit" name="edit" value="Edit" type="submit"/></label>
                                            <%
                                            }
                                            else
                                            {
                                                %>
                                            <label class="formBtn_1">

                                                    <input id="edit" name="editOandMUser" value="Edit" type="submit"/></label>
                                            <%
                                            }
                                            %>
                                                <% if (list.size() > 0) {%>
                                            <label class="formBtn_1"><input id="accountHistory" name="accountHistory" value="View Account History" type="submit"/></label>
                                                <%}%>
                                        </td>
                                    </tr>
                                    <%}%>
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
    <%
                manageAdminSrBean = null;
                adminMasterDtBean = null;
    %>
</html>
