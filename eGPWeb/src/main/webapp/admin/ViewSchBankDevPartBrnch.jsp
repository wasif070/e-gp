<%-- 
    Document   : ViewSchBankDevPartBrnch
    Created on : Nov 17, 2010, 5:07:15 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="scBankCreationDtBean" class="com.cptu.egp.eps.web.databean.ScBankCreationDtBean" scope="request"/>
<jsp:useBean id="scBankCreationSrBean" class="com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean" scope="request"/>
<%
            String logUserId = "0";
            if(session.getAttribute("userId")!=null){
                    logUserId = session.getAttribute("userId").toString();
                }
            scBankCreationSrBean.setLogUserId(logUserId);
            // scBankCreationSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
            String partnerType = request.getParameter("partnerType");
            String type = "";
            String title = "";
            String userType = "";
            String strOffice = request.getParameter("officeId");
            String branch="Branch";
            String from=request.getParameter("from");
            String headOffice="Head Office";
            if(from==null)
                from="Operation";
            int officeId = 0;
            if (strOffice != null && !strOffice.equalsIgnoreCase("")) {
                officeId = Integer.parseInt(strOffice);
            }
            if (partnerType != null && partnerType.equals("Development")) {
                type = "Development";
                title = "Development Partner";
                userType = "dev";
                branch="Regional/Country Office";
            }
            else {
                type = "ScheduleBank";
                title = "Financial Institution";
                userType = "sb";
                headOffice="Financial Institute Name";
            }

            if (officeId != 0) {
                scBankCreationDtBean.setOfficeId(officeId);
                scBankCreationDtBean.populateInfo();
                ScBankDevpartnerService scBankDevpartnerService=(ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
                if(request.getParameter("msg")== null && request.getParameter("mode")!= null && request.getParameter("mode").equalsIgnoreCase("view"))
                {
                    scBankDevpartnerService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                }
                else
                {
                    scBankDevpartnerService.setAuditTrail(null);
                }
            }
            String gridURL = "ManageSbDpBranchOffice.jsp?partnerType=" + type;
            String editURL = "EditSchBankDevPartnerBranch.jsp?officeId=" + officeId + "&partnerType=" + type;

%>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=title%> <%=branch%> : View</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%--<jsp:setProperty name="scBankCreationDtBean" property="*" />--%>

                <!--Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1">
                            <!-- Success failure -->
                            
                            <!-- Result Display End-->
                            <div class="pageHead_1">View <%=title%> <%=branch%> Details</div>
                            <!--Page Content Start-->
                            <%
                                        String msg = request.getParameter("msg");
                                        if (msg != null && msg.equals("success")) {%>
                                        <div class="responseMsg successMsg" style="margin-top: 10px;"><%=title%> <%=branch%> <%=from%> successfully</div>
                            <%}%>
                            <form name="frmBankSchedule" action="" method="POST" id ="frmBankSchedule">
                                <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td class="ff" width="20%"><%=headOffice%> :</td>
                                        <td width="80%">
                                            <label id="branchName">${scBankCreationDtBean.headOfficeName}</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff"><%=title%> <%=branch%> :</td>
                                        <td><label id="branchName">${scBankCreationDtBean.bankName}</label></td>
                                    </tr>
                                    <% if (type.equals("ScheduleBank")) { %>
                                    <tr>
                                        <td class="ff" >Swift Code : </td>
                                        <td align="left">${scBankCreationDtBean.swiftCode}</td>
                                    </tr>
                                   <% }  %>
                                    <tr>
                                        <td class="ff"> Address :</td>
                                        <td align="left" id="address"><label>${scBankCreationDtBean.officeAddress}</label></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">City / Town :</td>
                                        <td><label id="counrty">${scBankCreationDtBean.city}</label></td>
                                    </tr>
                                    
                                    <%
                                                if (type.equals("ScheduleBank")) {
                                                    String country = scBankCreationDtBean.getCountry();
                                                    String display = country.equals("Bangladesh") ? "table-row" : "none";
                                    %>
                                    <tr id="upjilla" style="display: <%=display%>">
                                        <td class="ff">Gewog :</td>
                                        <td>
                                            <label id="lblUpZilla">${scBankCreationDtBean.thanaUpzilla}</label>
                                        </td>
                                    </tr>
                                    <%}%>
                                    <tr>
                                        <td class="ff">Dzongkhag / District :</td>
                                        <td><label id="counrty">${scBankCreationDtBean.state}</label></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Country :</td>
                                        <td><label id="counrty">${scBankCreationDtBean.country}</label></td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="ff">Post Code :</td>
                                        <td>
                                            <label id="lblUpZilla">${scBankCreationDtBean.postCode}</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Phone Number :</td>
                                        <td>
                                            <label id="lblUpZilla">${scBankCreationDtBean.phoneNo}</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Fax No : </td>
                                        <%if(!scBankCreationDtBean.getFaxNo().equalsIgnoreCase("")){%>
                                        <td><label id="lblUpZilla">${scBankCreationDtBean.faxNo}</label></td>
                                        <%}%>
                                    </tr>
                                    <%--<tr>
                                        <td class="ff" width="200">Website</td>
                                        <td>
                                            <label id="lblUpZilla">${scBankCreationDtBean.webSite}</label>
                                        </td>
                                    </tr>--%>
                                    <%--<tr>
                                        <td align="center" colspan="2">
                                            <label class="formBtn_1">
                                                <input type="button" name="edit" id="btnEdit" value="Update" />
                                            </label>
                                        </td>
                                    </tr>--%>
                                </table>
                                <table width="100%" cellspacing="10" cellpadding="0" border="0">
                                    <tr>
                                        <td width="20%">&nbsp;</td>
                                        <td width="80%" class="t-align-left" >
                                            <a href="<%=gridURL%>" class="anchorLink">Ok</a>&nbsp;<a href="<%= editURL %>" class="anchorLink">Edit</a>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
