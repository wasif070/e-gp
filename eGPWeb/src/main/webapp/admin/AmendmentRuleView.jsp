<%-- 
    Document   : AmendmentRuleView
    Created on : Nov 25, 2010, 11:40:46 AM
    Author     : Naresh.Akurathi
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigAmendment"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
     <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Amendment Business Rule</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

<script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
<script type="text/javascript"
src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
   <script type="text/javascript">
            $(document).ready(function() {
                sortTable();
            });
        </script>

<script type="text/javascript">
    function conform()
    {
        if (confirm("Do you want to delete this business rule?"))
            return true;
        else
            return false;
    }
</script>


<%!
  ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();
  
 %>

</head>

<body>
    <%
            String msg = "";
            String delMsg= "";
            String mesg= request.getParameter("msg");
             if("delete".equals(request.getParameter("action"))){
                String action = "Remove Amendment Rules";
                try{
                    int amndId = Integer.parseInt(request.getParameter("id"));
                    TblConfigAmendment configAmnd = new TblConfigAmendment();
                    configAmnd.setConfigAmendmentId(amndId);
                    configAmnd.setAmendMinDays(Short.parseShort("1"));
                    configAmnd.setAmendPercent(new BigDecimal(Byte.parseByte("1")));
                    configAmnd.setTblProcurementMethod(new TblProcurementMethod(Byte.parseByte("1")));
                    configAmnd.setTblProcurementTypes(new TblProcurementTypes(Byte.parseByte("1")));
                    configPreTenderRuleSrBean.delConfingAmendment(configAmnd);
                    delMsg="Amendment Business Rule Deleted successfully";
                }catch(Exception e){
                    System.out.println(e);
                    action = "Error in : "+action +" : "+ e;
                }finally{
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                    action=null;
                }
           }            
    %>

<div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="pageHead_1">Amendment Business Rule Configuration
  <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('4');">Save as PDF</a></span>
  </div>
  <div>&nbsp;</div>
  <%if("delete".equals(request.getParameter("action"))){%>
  <div class="responseMsg successMsg"><%=delMsg%></div>
  <%}%>
  
  <%if(mesg != null){%>
  <div class="responseMsg successMsg"><%=mesg%></div>
  <%}%>
  <div>&nbsp;</div>
  <input type="hidden" name="temp" value="val"/>    
 <table width="100%" cellspacing="0" class="tableList_1" name="resultTable" id="resultTable"  cols="@4">
     <tr>      
       <th>Procurement Type <br /></th>
       <th>Procurement Method <br /></th>
       <th>Time remaining in % of Total Tender Preparation time (Tender Closing Date - Tender Publication Date)<br /></th>
       <th>Specify minimum no. of days by which Submission Date should be extended<br /></th>
       <th>Action</th>
    </tr>

     <%
            List<TblConfigAmendment> lst = configPreTenderRuleSrBean.getConfigAmendment();
            
            String temp="val";
            String sty= "style='background-color:#E4FAD0;'";
            int i=0;
            if(!lst.isEmpty() ||lst != null){
                TblConfigAmendment configamd = new TblConfigAmendment();
                Iterator it = lst.iterator();
                while(it.hasNext()){i++;
                        configamd = (TblConfigAmendment)it.next();
                       
     %>     

     <%if(i%2==0){%>
      <tr style='background-color:#E4FAD0;'>
          <%}else{%>
          <tr>
          <%}%>
          <td class="t-align-center"><%if(configamd.getTblProcurementTypes().getProcurementType().equalsIgnoreCase("NCT")){out.print("NCB");} else if(configamd.getTblProcurementTypes().getProcurementType().equalsIgnoreCase("ICT")){out.print("ICB");} %></td>
          <td class="t-align-center"><%if(configamd.getTblProcurementMethod().getProcurementMethod().equalsIgnoreCase("RFQ")){out.print("LEM");}else if(configamd.getTblProcurementMethod().getProcurementMethod().equalsIgnoreCase("DPM")){out.print("DCM");}else{out.print(configamd.getTblProcurementMethod().getProcurementMethod());}%></td>
          <td class="t-align-center" ><%=configamd.getAmendPercent()%></td>
          <td class="t-align-center"><%=configamd.getAmendMinDays()%></td>
          <td class="t-align-center"><a href="AmendmentRuleDetails.jsp?action=edit&id=<%=configamd.getConfigAmendmentId()%>">Edit</a>&nbsp;|&nbsp;<a href="AmendmentRuleView.jsp?action=delete&id=<%=configamd.getConfigAmendmentId()%>" onclick="return conform();">Delete</a></td>
          </tr></tr>
      
          <%
        }
 }
            else
                {
                    msg = "No Record Found";
                }

%>

 </table>  
<div align="center"> <%=msg%></div>

                    </td>
                </tr>
            </table>
        <form id="formstyle" action="" method="post" name="formstyle">

           <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
           <%
             SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
             String appenddate = dateFormat1.format(new Date());
           %>
           <input type="hidden" name="fileName" id="fileName" value="AmendmentRule_<%=appenddate%>" />
            <input type="hidden" name="id" id="id" value="AmendmentRule" />
        </form>
  <div>&nbsp;</div>
                    
         
   <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>  
</div>
 <script>
                var obj = document.getElementById('lblAmendmentRuleView');
                if(obj != null){
                    if(obj.innerHTML == 'View'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
        <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
</body>
</html>


