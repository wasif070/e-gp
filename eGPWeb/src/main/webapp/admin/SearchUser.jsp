<%-- 
    Document   : SearchUser
    Created on : Nov 2, 2010, 2:26:33 PM
    Author     : parag
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="projSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectSrBean" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.List,com.cptu.egp.eps.dao.storedprocedure.CommonAppData,com.cptu.egp.eps.web.utility.SelectItem" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
    %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Search &amp; select user for project</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
   <script type="text/javascript">
            $(document).ready(function() {
                $("#frmSearchUser").validate({
                    rules: {
                        cmbOrganization:{required:true},
                        cmbOffice:{required:true}
                    },
                    messages: {
                        cmbOrganization:{required:"<div class='reqF_1'>Please Select Organization.</div>"
                        },

                        cmbOffice:{required:"<div class='reqF_1'>Please Select Office.</div>"
                        }
                    }
                }
            );
            });

        </script>
<script type="text/javascript">

function getOfficeData() {
     if(document.getElementById("deptName") != ""){
         // $('#cmbOrganization').change(function() {
             $.post("<%=request.getContextPath()%>/ProjectSrBean", {deptId:$('#cmbOrganization').val(),funName:'Office'}, function(j){
                 $("select#cmbOffice").html(j);
             });
        // });
     }
 }

</script>
</head>
<body>
    <div class="mainDiv">
            <div class="fixDiv">
    <%
      String emailId="";
      int organization=0;
      String deptName = "";
      int officeId=0;
      boolean isSearch=false;
      String proleType="";
      List<CommonAppData> commonAppDatas = null;

      if("Search".equalsIgnoreCase(request.getParameter("btnSearch") ) ){
      
      isSearch = true;
        if(request.getParameter("txtEmailId") != null){
            emailId = request.getParameter("txtEmailId");
            projSrUser.setEmailId(emailId);
        }
        else{
            projSrUser.setEmailId("0");
        }

        if(request.getParameter("cmbOrganization") != null){
                organization = Integer.parseInt(request.getParameter("cmbOrganization"));
        }
        if(request.getParameter("deptName") != null){
            deptName = request.getParameter("deptName");
        }

      
        if(request.getParameter("cmbOffice") != null){
            officeId = Integer.parseInt(request.getParameter("cmbOffice"));
            projSrUser.setOfficeId(officeId);
        }else{
            projSrUser.setOfficeId(0);
        }

        if(request.getParameter("proleId") != null){
            proleType = request.getParameter("proleId");
        }

        
        
        commonAppDatas = projSrUser.getSearchList(proleType);
        
      }

      /*if("Add User".equalsIgnoreCase(request.getParameter("btnAddUser"))){
          
          
          String selUser= request.getParameter("SelUserId");
          int selUserId = Integer.parseInt(selUser.substring(0,selUser.indexOf("_")));
          String selUserValue = selUser.substring(selUser.indexOf("_")+1,selUser.length());
          //String selUser= request.getParameter("SelUserId");
     }*/

     List<SelectItem> deptList=projSrUser.getDepartmentList();
     
     List<CommonAppData> list=null;
    if(isSearch)
    {
        list = projSrUser.getSelectOfficeData(organization);
        
    }
    %>

<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <div class="contentArea_1">
  <div class="topHeader">
  </div>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="pageHead_1">Search &amp; select  user for project</div>
  <div>&nbsp;</div>
  <form method="post" id="frmSearchUser" >
  <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="70%">
    <tr>
      <td style="font-style: italic" class="ff t-align-left" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
    </tr>
	<%--<tr>
      <td class="ff" width="22%">e-mail ID:   <span>*</span></td>
      <td width="78%"><input name="txtEmailId" type="text" class="formTxtBox_1" id="txtEmailId"  style="width:200px;" /></td>
	</tr>--%>
	<tr>
      <td class="ff">Select Organization :    <span>*</span></td>
      <td>
          <%--<select name="cmbOrganization" class="formTxtBox_1" id="cmbOrganization" style="width:300px;"
                 onchange="getOfficeData();" >
              <option value="" > Select Organization</option>
             <%for(int i=0;i<(deptList.size()) ;i++){%>
             <option value="<%=deptList.get(i).getObjectId()%>"
                <%if(Integer.parseInt(deptList.get(i).getObjectId().toString()) == organization){%> selected<%}%> >
            <%=deptList.get(i).getObjectValue()%></option>
            <%}%>
            </select>--%>
          <input type="text" name="cmbOrganization" id="cmbOrganization" class="formTxtBox_1" readonly style="display: none" value="<%= organization %>"/>
          <input type="text" name="deptName" id="deptName" class="formTxtBox_1" readonly style="width: 400px;" value="<%= deptName %>"/>
            <a id="imgTree" href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=searchUsrPrj', '', 'width=350px,height=400px,scrollbars=1','');">
                <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
            </a>
      </td>
	</tr>
	<tr>
      <td class="ff">Select Office :    <span>*</span></td>
      <td id="tdOffice">
         <%if(!isSearch){%>
          <select name="cmbOffice" class="formTxtBox_1" id="cmbOffice" style="width:300px;">
          </select>
         <%}else{%>
          <select name="cmbOffice" class="formTxtBox_1" id="cmbOffice" style="width:300px;">
              <%
              
              for(int i=0;i<(list.size()) ;i++){%>
             <option value="<%=list.get(i).getFieldName1()%>"
   <%if(Integer.parseInt(list.get(i).getFieldName1().toString()) == officeId){ out.print("selected"); }%> >
            <%=list.get(i).getFieldName2()%></option>
            <%}%>
          </select>
         <%}%>
      </td>
	</tr>
    <tr>
      <td>&nbsp;</td>
      <td><label class="formBtn_1">
              <input type="submit" name="btnSearch" id="btnSearch" value="Search" onclick="" />
        </label>       </td>
    </tr>
  </table>
      </form>
  <form method="post" >
<table width="70%" cellspacing="0" class="tableList_1 t_space">

      <tr>
        <th class="t-align-center"><strong>Select</strong></th>
        <th class="t-align-center">            
            Official Name (Department - Procurement Role - Designation)</th>
        <th class="t-align-center" style="display:none"><strong>Details</strong></th>
      </tr>
      <%
      
      int count=2;
      if(commonAppDatas != null){
          
         for(CommonAppData appData : commonAppDatas){
             count++;
      %>
      <tr>
        <td class="t-align-center"><input type="radio" name="SelUserId" id="rdSelUserId<%=count%>"
               value="<%if(isSearch){out.print(appData.getFieldName2()+'_'+appData.getFieldName1());}%>" /></td>
        <td class="t-align-center"><%if(isSearch){out.print(appData.getFieldName1());}%>
            <input type="hidden" id="txtSelUserId<%=count%>" name="txtSelUserId<%=count%>"
               value="<%if(isSearch){out.print(appData.getFieldName1());}%>"    />
        </td>
        <td class="t-align-center" style="display:none">&nbsp;
            <%if(isSearch){%> <a href="#" onclick="window.open('ViewGovtUserDetail.jsp?userId=<%=appData.getFieldName2()%>&fromWhere=SearchUser','window','width=900,height=600')" >View</a><%}%>
        </td>
      </tr>
      <%
           }
        }
        else{
      
         }
      %>

      <tr>
        <td colspan="3" class="t-align-center"><span class="formBtn_1">
        
        <input type="hidden" id="hidCountId" name="hidCountId" value="<%=count%>" />
        <input type="submit" name="btnAddUser" id="btnAddUser" value="Add User" onclick="return getData();" />
        <input type="hidden" id="proleId" name="proleId" value="" />
        </span></td>
      </tr>
      <%--<tr>
          <input type="text" id="hidOfficeId" name="hidOfficeId" value="" />
        <input type="text" id="hidOfficeValue" name="hidOfficeValue" value="" />
        <input type="text" id="hidUserId" name="hidUserId" value="" />
         <input type="text" id="hidOfficialName" name="hidOfficialName" value="" />
      </tr>--%>
    </table>
       </form>
  <p>&nbsp;</p>
    <div>&nbsp;</div>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
  <table width="100%" cellspacing="0" class="footerCss">
    <tr>
      <td align="left">e-GP &copy; All Rights Reserved
        <div class="msg">Best viewed in 1024x768 &amp; above resolution</div></td>
      <td align="right"><a href="#">About e-GP</a> &nbsp;|&nbsp; <a href="#">Contact Us</a> &nbsp;|&nbsp; <a href="#">RSS Feed</a> &nbsp;|&nbsp; <a href="#">Terms &amp; Conditions</a> &nbsp;|&nbsp; <a href="#">Privacy Policy</a></td>
    </tr>
  </table>
  <!--Dashboard Footer End-->
</div>
</div></div></div>
</body>
</html>

<script type="text/javascript">
    <%--function searchvalidation()
    {
         jAlert("Please select Organization ", function(RetVal) {
         });
    }--%>
    <%--$(document).ready(function() {
     $.post("<%=request.getContextPath()%>/ProjectSrBean", {deptId:$('#cmbOrganization').val(),funName:'Office'}, function(j){
             $("select#cmbOffice").html(j);     
      });
    });--%>
    
    function getData(){
                    
        var officeId = $('#cmbOffice').val();
        //alert(officeId);
       // var officeValue = $('#cmbOffice').select().text();        
       var officeValue = $.trim(document.getElementById("cmbOffice").options[document.getElementById("cmbOffice").selectedIndex].innerHTML);
        try{
            var selUser =$("input:radio[name='SelUserId']:checked").val();
            var selUserId = selUser.substring(0,selUser.indexOf("_",0));
            var selUserValue = selUser.substring(selUser.indexOf("_",0) + 1,selUser.length);           
        }catch(e){
            jAlert("Please select atleast one user."," Search User ", function(RetVal) {
            });
            return false;
        }

        window.opener.document.getElementById("txtProjOffice").value= officeValue;
        window.opener.document.getElementById("hidProjOffice").value = officeId;
        window.opener.document.getElementById("txtUser").value= selUserValue;
        window.opener.document.getElementById("hidUser").value = selUserId;
        //alert(' selUserId :: '+selUserId);
        window.close();
        window.opener.checkPE(selUserId);
        
       
        <%--$('#hidOfficeId').val(officeId);
        $('#hidOfficeValue').val(officeValue);
        $('#hidUserId').val(selUserId);
        $('#hidOfficialName').val(selUserValue);--%>

        return false;
    }

    $(document).ready(function() {
        if(window.opener.document.getElementById('chkPD').checked || window.opener.document.getElementById('chkPE').checked){
            document.getElementById("proleId").value=1;
        }
         if(window.opener.document.getElementById('chkAU').checked){
            document.getElementById("proleId").value=2;
        }
    });
</script>

<%
    projSrUser = null;
%>