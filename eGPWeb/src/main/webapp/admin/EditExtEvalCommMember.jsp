<%--
    Document   : EditExtEvalCommMember
    Created on : Feb 19, 2011, 3:09:01 PM
    Author     : rishita
--%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.web.servlet.CommonServlet"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register External Committee Member</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    String useId = "";
                    if (request.getParameter("uId") != null) {
                        useId = request.getParameter("uId");
                    }
                    /*String createdBy = "";
                    if (!session.getAttribute("userId").equals("") && session.getAttribute("userId") != null) {
                    createdBy = session.getAttribute("userId").toString();
                    }*/

        %>
    </head>
    <jsp:useBean id="regExtEvalCommMemberSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.RegExtEvalCommMemberSrBean"/>
    <jsp:useBean id="regExtEvalCommMemberDtBean" scope="request" class="com.cptu.egp.eps.web.databean.RegExtEvalCommMemberDtBean"/>
    <jsp:setProperty name="regExtEvalCommMemberDtBean" property="*"/>
    <body>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmregExtEvalCommMember").validate({
                    rules: {
                        //emailId: { required: true}
                        fullName: { requiredWithoutSpace: true , alphaName:true, maxlength:100 },
                        nameBanglaString: {maxlength:100},
                        phoneNo: { numberWithHyphen:true,PhoneFax: true},
                        mobileNo: { number: true, minlength: 10, maxlength:15 },
                        nationalId:{number: true,minlength: 13,maxlength: 25}
                    },
                    messages: {
                        fullName: { //spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace: "<div class='reqF_1'>Please enter Full Name</div>",
                            alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , ' \" } { - . _) Only </div>",
                            maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"
                        },
                        phoneNo: { numberWithHyphen: "<div class='reqF_1'>Allows numbers (0-9) and hyphen (-) only</div>",
                            PhoneFax:"<div class='reqF_1'>Please enter valid Phone No. Area code should contain minimum 2 digits and maximum 5 digits. Phone no. should contain minimum 3 digits and maximum 10 digits</div>"
                        },
                        nationalId: {
                            number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                            minlength: "<div class='reqF_1'>National ID should comprise of minimum 13 digits</div>",
                            maxlength: "<div class='reqF_1'>National ID should comprise of maximum 25 digits</div>"
                        },
                        nameBanglaString: {maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"
                        },
                        mobileNo: {
                            number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" ,
                            minlength:"<div class='reqF_1'>Minimum 10 digits are required</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 15 digits only</div>"
                        }
                    },
                    errorPlacement:function(error ,element)
                    {
                        if(element.attr("name")=="mobileNo")
                        {
                            error.insertAfter("#mNo");
                        }else if(element.attr("name")=="phoneNo")
                        {
                            error.insertAfter("#phno");
                        }
                        else
                        {
                            error.insertAfter(element);

                        }
                    }
                });
            });
        </script>
        <%
                    if ("Update".equals(request.getParameter("Update"))) {
                        regExtEvalCommMemberDtBean.setFullName(regExtEvalCommMemberDtBean.getFullName().trim());
                        regExtEvalCommMemberSrBean.setAuditTrail(null);
                        
                        // Coad added by Dipal for Audit Trail Log.
                        String idType="userId";
                        int auditId=Integer.parseInt(request.getParameter("uId"));
                        String auditAction="Edit External Evaluation Commitee Memeber User";
                        String moduleName=EgpModule.Manage_Users.getName();
                        String remarks="";
                        AuditTrail objAuditTrail =null;
                        
                        if(!"1".equals(session.getAttribute("userTypeId").toString()))
                        {
                           regExtEvalCommMemberSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                        }
                        else
                        {
                            regExtEvalCommMemberSrBean.setAuditTrail(null);
                            objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        }
                        
                       MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        
                        if (regExtEvalCommMemberSrBean.updateExtComm(regExtEvalCommMemberDtBean)) 
                        {
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                            response.sendRedirect("ViewRegExtEvalCommMember.jsp?uId=" + useId + "&msg=updated");
                        }
                        else 
                        {
                            auditAction= " Error in "+auditAction;
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                            response.sendRedirect("EditExtEvalCommMember.jsp?uId=" + useId + "&msg=failure");
                        }
                    }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }

                            int uTypeId = 0;
                            if (session.getAttribute("userTypeId") != null) {
                                uTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%if (uTypeId == 1 || uTypeId == 4 || uTypeId == 5) {%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>
                        <% }%>
                        <td class="contentArea_1">
                            <%if (uTypeId == 1 || uTypeId == 4 || uTypeId == 5) {%>
                            <div class="pageHead_1">Update External Evaluation Committee Member</div>
                            <%} else {%>
                            <div class="pageHead_1">Update Profile</div>
                            <%}%>
                            <%if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("no")) {
                            %>
                            <div align="left" id="sucMsg" class="responseMsg errorMsg ">e-mail ID already exist, Please enter another e-mail ID</div>
                            <% }%>
                            <%if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("failure")) {
                            %>
                            <div align="left" id="sucMsg" class="responseMsg errorMsg ">External Committee Member is not created successfully</div>
                            <% }%>
                            <form action="" method="post" id="frmregExtEvalCommMember" name="frmregExtEvalCommMember">
                                <%
                                            regExtEvalCommMemberSrBean.setAuditTrail(null);
                                            for (Object[] listing : regExtEvalCommMemberSrBean.getExtCommListing(Integer.parseInt(useId))) {

                                %>
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td style="font-style: italic" class="ff t-align-left" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="15%">e-mail ID :</td>
                                        <td width="85%"><%=listing[9]%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Full Name : <span>*</span></td>
                                        <td><input name="fullName" type="text" value="<%=listing[2]%>" class="formTxtBox_1" id="txtFullName" style="width:200px;" maxlength="101"/>
                                            <input type="hidden" value="<%=listing[0]%>" name="memberId"/>
                                            <input type="hidden" value="<%=listing[7]%>" name="createdBy"/>
                                            <input type="hidden" value="<%=listing[8]%>" name="creationDtString"/>
                                            <input type="hidden" value="<%=listing[10]%>" name="userId"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Name in Dzongkha : </td>
                                        <td><input name="nameBanglaString" <%if (listing[3] != null) {%> value="<%=BanglaNameUtils.getUTFString((byte[]) listing[3])%>" <% }%>type="text" class="formTxtBox_1" id="txtNameBanglaString" style="width:200px;" maxlength="101"/></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">CID No. : </td>
                                        <td><input name="nationalId" <% if (listing[4] != null && !listing[4].toString().trim().equals("")) {%> value="<%=listing[4]%>" <% }%> type="text" class="formTxtBox_1" id="txtNationalId" style="width:200px;" maxlength="26"/></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Phone No. :</td>
                                        <td><input name="phoneNo" type="text" <% if (listing[5] != null && !listing[5].toString().trim().equals("")) {%> value="<%=listing[5]%>" <% }%> class="formTxtBox_1" id="txtPhoneNo" style="width:200px;" maxlength="17" />
                                            <span id="phno" style="color: grey;">(Area Code - Phone No. e.g. 2-324425)</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Mobile No. : </td>
                                        <td><input name="mobileNo" type="text" maxlength="16" class="formTxtBox_1" id="txtMobileNo" style="width:200px;" <% if (listing[6] != null && !listing[6].toString().trim().equals("")) {%> value="<%=listing[6]%>" <% }%> /><span id="mNo" style="color: grey;"> (Mobile No. format should be e.g 12345678)</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td class="t-align-left">
                                            <label class="formBtn_1"><input type="submit" name="Update" id="Update" value="Update" /></label>
                                        </td>
                                    </tr>
                                </table>
                                <% }%>
                            </form>
                            <!--Middle Content Table End-->
                        </td>
                        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>
<%
            regExtEvalCommMemberDtBean = null;
            regExtEvalCommMemberSrBean = null;
%>
<script type="text/javascript">
    var uTypeId = '<%=uTypeId%>';

    if(uTypeId == 1){
        if(document.getElementById("headTabMngUser") != null){
            document.getElementById("headTabMngUser").setAttribute("class", "selected");
        }
    }else{
        if(document.getElementById("headTabMyAccExt") != null){
            document.getElementById("headTabMyAccExt").setAttribute("class", "selected");
        }
    }
    $(function() {
        $('#frmregExtEvalCommMember').submit(function() {
            $(".err").remove();
            if($('#txtFullName').val().length !=0 && $.trim($('#txtFullName').val()).length == 0){
                $("#txtFullName").parent().append("<div class='err' style='color:red;'>Only space is not allowed</div>");
                return false;
            }
        });
        $('#txtFullName').blur(function() {
            $(".err").remove();
            if($('#txtFullName').val().length !=0 && $.trim($('#txtFullName').val()).length == 0){
                $("#txtFullName").parent().append("<div class='err' style='color:red;'>Only space is not allowed</div>");
                //               valid = false;
            }
        });
    });
</script>