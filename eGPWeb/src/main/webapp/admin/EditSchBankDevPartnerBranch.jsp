<%-- 
    Document   : EditSchBankDevPartnerBranch
    Created on : Oct 30, 2010, 1:04:49 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="scBankCreationDtBean" class="com.cptu.egp.eps.web.databean.ScBankCreationDtBean" scope="request"/>
<jsp:useBean id="scBankCreationSrBean" class="com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean" scope="request"/>
<%
            String logUserId = "0";
            if(session.getAttribute("userId")!=null){
                    logUserId = session.getAttribute("userId").toString();
                }
            scBankCreationSrBean.setLogUserId(logUserId);
            String partnerType = request.getParameter("partnerType");
            String type = "";
            String title = "";
            String userType = "";
            String branch = "Branch";
            String headOffice="Head Office";
            String branch1 = "Branch";
            int officeId = Integer.parseInt(request.getParameter("officeId"));
            String mainOffice = request.getParameter("officeId");
            if (partnerType != null && partnerType.equals("Development")) {
                type = "Development";
                title = "Development Partner";
                userType = "dev";
                branch = "Regional Office";
                branch1 = "Regional/Country office";
            }
            else {
                type = "ScheduleBank";
                title = "Financial Institution";
                userType = "sb";
                headOffice="Bank Name";
            }
            scBankCreationSrBean.setAuditTrail(null);
            scBankCreationDtBean.setOfficeId(officeId);
            scBankCreationDtBean.populateInfo();
            
%>
<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit <%=title%> <%=branch%> Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $.validator.addMethod(
                "regex",
                 function(value, element, regexp) {
                 var check = false;
                 return this.optional(element) || regexp.test(value);
                 },
                 "Please check your input."
                 );
                
                $('#btnUpdate').attr("disabled", false);
                $("#frmBankSchedule").validate({
                    rules: {
                        officeAddress:{required:true,maxlength:300},
                        postCode:{number:true,minlength:4},
                        city:{//requiredWithoutSpace: true,
                            spacevalidate:true,alphaCity: true,maxlength:100},
                        thanaUpzilla:{requiredWithoutSpace: true,spacevalidate:true,maxlength:100,alphaCity: true },
                        phoneNo: {required: true, regex: /^[0-9]+-[0-9,]*[0-9]$/},
                        faxNo: {regex: /^[0-9]+-[0-9,]*[0-9]$/}
                    },
                    messages: {
                        officeAddress:{ required: "<div class='reqF_1'> Please enter Head Office Address.</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 300 characters only</div>"
                        },
                        city:{//requiredWithoutSpace: "<div class='reqF_1'>Please enter City / Town Name</div>",
                            alphaCity:"<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , ' \" } { - . _) Only</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>",
                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>"
                        },
                        postCode:{//required:"<div class='reqF_1'>Please enter Post code.</div>",
                            number: "<div class='reqF_1'>Please enter numbers only</div>",
                            maxlength:"<div class='reqF_1'>Maximum 4 Characters are allowed.</div>",
                            minlength:"<div class='reqF_1'>Minimum 4 Characters are required.</div>"
                        },
                        thanaUpzilla:{requiredWithoutSpace: "<div class='reqF_1'>Please enter Thana / UpaZilla</div>",
                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>",
                            alphaCity: "<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , ' \" } { - . _) Only</div>"
                        },
                        phoneNo: {required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            regex: "<div class='reqF_1'>Give dash (-) after Area Code.</br>Use comma to separate alternative Phone no.</br>Do not repeat Area Code.</div>",
                            },
                        faxNo: {regex: "<div class='reqF_1'>Give dash (-) after Area Code.</br>Use comma to separate alternative Fax no.</br>Do not repeat Area Code.</div>",
                            }
                    },

                    errorPlacement:function(error ,element)
                    {
                       if(element.attr("name")=="phoneNo")
                        {
                            error.insertAfter("#phno");
                        }
                        else if(element.attr("name")=="faxNo")
                        {
                            error.insertAfter("#fxno");
                        }
                        else
                            error.insertAfter(element);
                    }
                });
            });

            function chkregPostCode(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function chkreg(value)
            {
                return /^[a-zA-Z 0-9](?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9]+$)+$/.test(value);

            }
            var bvalidationPostCode = true;
           function ValPost()
            {
//                
//                var partnerType = '<%= type%>';
//                    if($('#txtPostCode').val() != '')
//                {
//                    if(partnerType == 'Development')
//                    {
//                        //alert("development condition");
//                        if(alphanumeric($('#txtPostCode').val()))
//                        {
//                            if(!ChkMaxLength($('#txtPostCode').val()))
//                            {
//                                $('span.#postCodeMsg').html("<br/>Postcode should comprise of 10 digits only");
//                               // flag=false;
//                                bvalidationPostCode = false;
//                            }
//                            else
//                            {
//                                bvalidationPostCode = true;
//                                $('span.#postCodeMsg').html("");
//                            }
//                        }
//                        else
//                        {   bvalidationPostCode = false;
//                            $('span.#postCodeMsg').html("<br/>Please enter Only Alphanumeric Value");
//                        }
//                    }
//                    else
//                    {
//                        //alert("sc bank. condition");
//                        if(numeric($('#txtPostCode').val()))
//                        {
//                            if(!ChkMaxLength($('#txtPostCode').val()))
//                            {
//                                $('span.#postCodeMsg').html("<br/>Postcode should comprise of 4 digits only");
//                                bvalidationPostCode=false;
//                            }
//                            else
//                            {
//                                bvalidationPostCode = true;
//                                $('span.#postCodeMsg').html("");
//                            }
//                        }
//                        else
//                        {
//                            bvalidationPostCode = false;
//                            $('span.#postCodeMsg').html("<br/>Please enter Numbers Only");
//                        }
//                    }
//                }
//                else
//                {
//                    bvalidationPostCode = false;
//                    $('span.#postCodeMsg').html("<br/>Please enter Post code.");
//                }
                return bvalidationPostCode;
            }
            /* $(function() {
                $('#txtPostCode').blur(function() {
                    bvalidationPostCode = true;
                    if($('#txtPostCode').val()==''){
                        $('#postCodeMsg').html('Please enter Postcode');
                        bvalidationPostCode = false;
                    }else if(!chkregPostCode($('#txtPostCode').val())){
                        $('#postCodeMsg').html('Please enter digits (0-9) only');
                        bvalidationPostCode = false;
                    }else if($('#txtPostCode').val().length > 4 || $('#txtPostCode').val().length < 4){
                        $('#postCodeMsg').html('Postcode should comprise of 4 digits only');
                        bvalidationPostCode = false;
                    }else{
                        $('#postCodeMsg').html('');
                    }
                });
            });*/

            function alphanumeric(value)
                {
                    return /^[\sa-zA-Z0-9\'\-]+$/.test(value);
                }
                function numeric(value)
                {
                    return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
                }
                function ChkMaxLength(value)
                {
                    var lenth = 4;
                    var partnerType = '<%= type%>';
                    if(partnerType == 'Development'){
                        lenth = 10;
                    }
                    var ValueChk=value;
                    if(ValueChk.length>lenth)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                
            $(function() {
                var bvalidationName;
                $('#txtBranchName').blur(function() {
                    if($('#oldVal').val()!=$('#txtBranchName').val()){
                        bvalidationName = true;
                        var flag=document.getElementById('txtBranchName').value;
                        if($('#txtBranchName').val() == ''){
                            $('#officeMsg').html("");
                            $('#branchValidation').html('Please enter <%=branch%> Name');
                            bvalidationName = false;
                        }else if(flag.charAt(0) == " "){
                            $('#officeMsg').html("");
                            $('#branchValidation').html('Only Space is not allowed');
                            bvalidationName = false;
                        }else if(!chkreg($('#txtBranchName').val())){
                            $('#officeMsg').html("");
                            $('#branchValidation').html('Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' " } { - . _) Only');
                            bvalidationName = false;
                        }else if(flag.length > 100){
                            $('#officeMsg').html("");
                            $('#branchValidation').html('Allows maximum 100 characters only');
                            bvalidationName = false;
                        }else{
                            $('#branchValidation').html('');
                            $.post("<%=request.getContextPath()%>/ScBankServlet", {officeName:$('#txtBranchName').val(),isBranch: 'Yes',title: "<%=title%>",type: "<%=type%>",funName:'verifyOffice',mainOffice: "<%=mainOffice%>"},
                            function(j){
                                if(j.toString().indexOf("OK", 0)!=-1){
                                    $('#officeMsg').css("color","green");
                                }
                                else{
                                    $('#officeMsg').css("color","red");
                                }
                                $('#officeMsg').show();
                                $('#officeMsg').html(j);
                            
                            });
                        }
                    }
                });
            });
            
             $(function() {
             var bvalidationName;
                $('#frmBankSchedule').submit(function() {
                    if(bvalidationPostCode == false){
                        return false;
                    }
                    else if(bvalidationName == false){
                        return false;
                    }
                    else if(($('#officeMsg').html().length > 0)) {
                        if(($('#officeMsg').html()=="OK")) {
                            return true;//if(bvalidationPostCode == true && bvalidationName == true){
                        }
                        else{
                            return false;
                        }
                    }
                    else
                    {
                         return true;
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#cmbCountry').change(function() {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbCountry').val(),funName:'stateCombo'},  function(j){
                        $('#cmbState').children().remove().end().append('<option selected value="0">-- Select --</option>') ;
                        $("select#cmbState").html(j);
                        toggleUpzillaView();
                    });
                    
                });
            });
        </script>
        <script type="text/javascript">
            function toggleUpzillaView()
            {
                if($('#cmbCountry option:selected').text() == "Bangladesh" && $('#partnerType').val() == "ScheduleBank")
                    $('#upjilla').show()
                else
                    $('#upjilla').hide();
            }
        </script>
      <script type="text/javascript">
          
           <%-- $(function() {
                $('#frmBankSchedule').submit(function() {
                    if(($('span.#officeMsg').html().toString().length > 0) && ($('span.#officeMsg').html()=="OK")){
                        return true;
                    }else{
                        return false;
                    }
                });
            });--%>
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%--<jsp:setProperty name="scBankCreationDtBean" property="*" />--%>

                <!--Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1">
                            <div class="pageHead_1">Edit <%=title%> <%=branch1%> Details</div>
                            <!--Page Content Start-->
                            <!-- Success failure -->
                            <%
                                        String msg = request.getParameter("msg");
                                        if (msg != null && msg.equals("fail")) {%>
                            <div class="responseMsg errorMsg"><%=branch%> Updation Failed.</div>
                            <%}
                                                                    else if (msg != null && msg.equals("success")) {%>
                            <div class="responseMsg successMsg"><%=branch%> Updated Successfully</div>
                            <%}%>
                            <!-- Result Display End-->
                       
                           <form name="frmBankSchedule" action="<%=request.getContextPath()%>/ScBankServlet" method="POST" id ="frmBankSchedule">
                               <table class="formStyle_1" width="100%" border="0" cellpadding="0" cellspacing="10">
                                    <tr><td class="ff" width="20%"><%=headOffice%> : </td>
                                        <td width="80%">
                                            <label>${scBankCreationDtBean.headOfficeName}</label>
                                            <input id="txtHeadOffice" name="headOffice" style="width: 200px;" type="hidden" value="${scBankCreationDtBean.headOfficeName}"/>
                                            <input type="hidden" name="branchHead" value="${scBankCreationDtBean.sbankDevelHeadId}">
                                        </td>
                                    </tr>
                                    <tr><td class="ff" >Name of <%=branch1%> : <span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtBranchName" name="bankName" style="width: 200px;" type="text" value="${scBankCreationDtBean.bankName}" maxlength="101"/>
                                            <div id="officeMsg"  style="font-weight: bold;display: none;" >OK</div>
                                            <input type="hidden" id="oldVal" name="oldVal" value="${scBankCreationDtBean.bankName}"/>
                                            <input type="hidden" id="partnerType" name="partnerType" value="<%=type%>"/>
                                            <input type="hidden" name="isBranch" value="Yes"/>
                                            
                                            <input type="hidden" name="createdBy" value="${scBankCreationDtBean.createdBy}"/>
                                            <input type="hidden" name="createdDate" value="${scBankCreationDtBean.createdDate}"/>
                                            <input type="hidden" name="officeId" value="${scBankCreationDtBean.officeId}"/>
                                            <%--<div id="infoOffNameMsg" style="color: grey;">
                                                Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (&/-.) Only
                                            </div>--%>
                                            <div id="branchValidation" class="reqF_1"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" >Address : <span>*</span></td>
                                        <td align="left"><textarea cols="20" rows="3" class="formTxtBox_1"  id="txtaAddress" name="officeAddress" style="width: 200px" onkeypress="return imposeMaxLength(this, 301, event);">${scBankCreationDtBean.officeAddress}</textarea></td>
                                    </tr>
                                   <% if (type.equals("ScheduleBank")) { %>
                                    <tr>
                                        <td class="ff" >Swift Code : </td>
                                        <td align="left">
                                            <input id="txtSwiftCode" class="formTxtBox_1" name="swiftCode" style="width: 200px" type="text" maxlength="50"
                                                                value="${scBankCreationDtBean.swiftCode}"/></td>
                                    </tr>
                                   <% }  %>
                                    <tr>
                                        <td class="ff" >Country : </td>
                                        <td>
                                            <% String country = scBankCreationDtBean.getCountry();
                                                    if(!type.equals("ScheduleBank")){
                                             %>
                                            <select class="formTxtBox_1" id="cmbCountry" name="country" style="width: 208px">
                                                <%
                                                            for (SelectItem countryItem : scBankCreationSrBean.getCountryList()) {
                                                                if (countryItem.getObjectValue().equalsIgnoreCase(country)) {
                                                %>
                                                <option  value="<%=countryItem.getObjectId()%>" selected="selected"><%=countryItem.getObjectValue()%></option>
                                                <%
                                                                                                                }
                                                                                                                else {
                                                %>
                                                <option  value="<%=countryItem.getObjectId()%>"><%=countryItem.getObjectValue()%></option>
                                                <%  }
                                                            }%>
                                            </select>
                                            <%}else{
                                                               
                                                               
                                                            for (SelectItem countryItem : scBankCreationSrBean.getCountryList()) {
                                                                if (countryItem.getObjectValue().equalsIgnoreCase(country)) {
                                                %>
                                                    <label   id="cmbCountry" name="country" value="<%=countryItem.getObjectId()%>" ><%=countryItem.getObjectValue()%></label>
                                                    <input type="hidden" name="country" value="<%=countryItem.getObjectId()%>" />
                                            <%}}}%>
                                            <input type="hidden" name="action" value="update"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" >Dzongkhag / District : <span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbState" name="state" style="width: 208px">
                                                <%
                                                            String state = scBankCreationDtBean.getState();
                                                            for (SelectItem stateItem : scBankCreationSrBean.getStateList(scBankCreationDtBean.getCountryId())) {
                                                                if (stateItem.getObjectValue().equalsIgnoreCase(state)) {
                                                %>
                                                <option  value="<%=stateItem.getObjectId()%>" selected="selected"><%=stateItem.getObjectValue()%></option>
                                                <%
                                                        }
                                                        else {
                                                %>
                                                <option  value="<%=stateItem.getObjectId()%>"><%=stateItem.getObjectValue()%></option>
                                                <%  }
                                                            }%>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" >City / Town : </td>
                                        <td><input class="formTxtBox_1" id="txtCity" value="${scBankCreationDtBean.city}" name="city" style="width: 200px;" type="text" maxlength="101"/>
                                        </td>
                                    </tr>
                                    <%
                                    String maxLen = "maxlength='11'";
                                    if (type.equals("ScheduleBank")) {                                        
                                        maxLen = "maxlength='5'";
                                        String display =  "table-row" ;
                                    %>
<!--                                    <tr id="upjilla" style="display: <%=display%>">
                                        <td class="ff" >Thana / UpaZilla : <span>*</span></td>
                                        <td><input id="txtUpJilla" class="formTxtBox_1" name="thanaUpzilla" style="width: 200px;" type="text" maxlength="101"
                                                   value="${scBankCreationDtBean.thanaUpzilla}"/>
                                        </td>
                                    </tr>-->
                                    <%}%>
                                    <tr>
                                        <td class="ff" >Post Code : </td>
                                        <td><input id="txtPostCode" name="postCode" class="formTxtBox_1" style="width: 200px;" type="text" maxlength="19"
                                                   value="${scBankCreationDtBean.postCode}" onblur="return ValPost();"/>
                                        <span id="postCodeMsg" style="color: red;"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" >Phone No. : <span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtPhoneNo" style="width: 200px;" name="phoneNo" type="text" maxlength="245"
                                                   value="${scBankCreationDtBean.phoneNo}"/>
                                             <%if(type.equals("ScheduleBank")){%>
                                        <span id="phno" style="color: grey;"> (Area Code - Phone No. e.g. 02-336962)</span>
                                        <%}else{%>
                                        <span id="phno" style="color: grey;"> (Area Code - Phone No. e.g. 02-336962)</span>
                                        <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" >Fax No. : </td>
                                        <td><input class="formTxtBox_1" id="txtFaxNo" style="width: 200px;" name="faxNo" type="text" maxlength="245"
                                                   value="${scBankCreationDtBean.faxNo}"/>
                                             <%if(type.equals("ScheduleBank")){%>
                                        <span id="fxno" style="color: grey;"> (Area Code - Fax No. e.g. 02-336961)</span>
                                        <%}else{%>
                                        <span id="fxno" style="color: grey;"> (Area Code - Fax No. e.g. 02-336961)</span>
                                        <%}%>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td align="left" >
                                            <label class="formBtn_1"><input id="btnUpdate" name="update" value="Update" type="submit" onclick="return ValPost();" disabled/></label>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>