<%-- 
    Document   : EditEmployee
    Created on : Oct 27, 2010, 7:03:45 PM
    Author     : rishita
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.model.table.TblDepartmentMaster" %>
<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        String departmentType = "";
        if (!request.getParameter("division").equals("")) {
            departmentType = request.getParameter("division");
        }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Update <% if(departmentType.equalsIgnoreCase("Organization")) { out.print("Division"); }else if(departmentType.equalsIgnoreCase("Division")) { out.print("Department"); } else {out.print(departmentType);} %> details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $.validator.addMethod(
                        "regex",
                        function (value, element, regexp) {
                            var check = false;
                            return this.optional(element) || regexp.test(value);
                        },
                        "Please check your input."
                        );
                $("#frmEditEmployee") .validate({

                    rules:{
                        deptBanglaString:{maxlength:100},
                        address:{required:true,maxlength:1000},
                        city:{spacevalidate:true,alphaCity: true,maxlength:100},
                        upJilla:{requiredWithoutSpace: true,spacevalidate:true,maxlength:100 },
                        postCode:{number: true,minlength:4},
                        phoneNo: {required: true, regex: /^[0-9]+-[0-9,]*[0-9]$/},
                        faxNo: {regex: /^[0-9]+-[0-9,]*[0-9]$/},
                        website:{url:true,maxlength: 50}

                    },

                    messages:{
                        
                        deptBanglaString:{maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>"},

                        address:{required:"<div class='reqF_1'>Please enter Address</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 1000 characters only</div>"},

                        city:{alphaCity:"<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , ' \" } { - . _) Only</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>",
                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>"},

                        upJilla:{requiredWithoutSpace: "<div class='reqF_1'>Please enter Gewog</div>",
                            alphaCity: "<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , ' \" } { - . _) Only</div>",
                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>"},
                        postCode:{ number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" ,
                            maxlength:"<div class='reqF_1'>Maximum 4 digits are allowed</div>",
                            minlength:"<div class='reqF_1'>Minimum 4 digits required</div>"},
                        phoneNo: {required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            regex: "<div class='reqF_1'>Area code should contain minimum 2 digits and maximum 5 digits.</br>Phone no. should contain minimum 3 digits and maximum 10 digits.</br>In case of more than one Phone No. use comma.</br>Do not repeat Area Code.</div>"
                        },
                        faxNo: {regex: "<div class='reqF_1'>Area code should contain minimum 2 digits and maximum 5 digits.</br>Fax no. should contain minimum 3 digits and maximum 10 digits.</br>In case of more than one Fax No. use comma.</br>Do not repeat Area Code.</div>"
                        },  

                        website:{url:"<div class='reqF_1'>Please enter website in www.xyz.gov.bt format</div>",
                            maxlength:"<div class='reqF_1'>Maximum 20 digits are allowed</div>"
                        }

                    },
                    errorPlacement:function(error ,element)
                    {

                        if(element.attr("name")=="phoneNo")
                        {
                            error.insertAfter("#phno");
                        }
                        else if(element.attr("name")=="faxNo")
                        {
                            error.insertAfter("#fxno");
                        }else if(element.attr("name")=="website")
                        {
                            error.insertAfter("#websiteToolTip");
                        }
                        else
                            error.insertAfter(element);
                    }

                });

             
                // alert($('#cmdDepartmentType').val());
                $('#messageid').html("Please enter " + $('#cmdDepartmentType').val())
               
            });
            
        </script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <jsp:useBean id="manageEmployeeSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ManageEmployeeGridSrBean"/>
        <jsp:useBean id="deptMasterSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.DepartmentCreationSrBean"/>
        <jsp:useBean id="deptDataBean" scope="request" class="com.cptu.egp.eps.web.databean.DepartmentCreationDtBean"/>
        <jsp:setProperty name="deptDataBean" property="*"/>


    </head>
    <%
                short deptid = 0;
                if (!request.getParameter("deptid").equals("")) {
                    deptid = Short.parseShort(request.getParameter("deptid"));
                }
    %>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#cmdDepartmentType').change(function() {
                $('#departmentid').val($('#cmdDepartmentType').val())
                //$('#messageid').html("Please enter " + $('#cmdDepartmentType').val())
                if ($('#cmdDepartmentType').val()=='Ministry'){
                    $('#cmbParentType').val('2');
                    $('#cmbParentType').attr('disabled', 'disabled');
                }
                else{
                    $('#cmbParentType').attr('disabled', '');
                }
            });
        });
    </script>
    <!--    <script type="text/javascript">
            $(function() {
                $('#txtDeptName').blur(function() {
                    alert('rishita');
                    $.post("<-%=request.getContextPath()%>/CommonServlet", {depName:$('#txtDeptName').val(),funName:'verifyDeptName'},
                    function(j)
                    {
                        if(j.toString().indexOf("e-GP", 0)!=-1){
                            $('span.#Msg').css("color","green");
                        }
                        else if(j.toString().indexOf("Mail", 0)!=-1){
                            $('span.#Msg').css("color","red");
                        }
                        $('span.#Msg').html(j);
                    });
                });
            });
        </script>-->
    <script type="text/javascript">
        $(function() {
            $('#cmdDepartmentType').change(function() {
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmdDepartmentType').val(),funName:'ministryC'},  function(j){
                    $("select#cmbParentType").html(j);
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#cmbCountry').change(function() {
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbCountry').val(),funName:'stateCombo'},  function(j){
                    $("#cmbState").html(j);
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#frmEditEmployee').submit(function() {
                if(bvalidation == true){
                    if(bvalidationPostCode == undefined || bvalidationPostCode == true){
                    if($('#frmEditEmployee').valid()){
                        if($('#btnUpdate')!=null){
                            $('#btnUpdate').attr("disabled","true");
                            $('#hdnbutton').val("Update");
                        }
                     }
                    }else {
                        return false;
                    }
                }else{
                    return false;
                }
            });
        });
    </script>
    <body>
        <div class="mainDiv">
            <%
                        int userid = 0;
                        HttpSession hs = request.getSession();
                        if (hs.getAttribute("userId") != null) {
                            userid = Integer.parseInt(hs.getAttribute("userId").toString());
                            deptMasterSrBean.setLogUserId(hs.getAttribute("userId").toString());
                            manageEmployeeSrBean.setLogUserId(hs.getAttribute("userId").toString());
                        }
            %>
            <div class="fixDiv">
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("hdnUserType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                    userType.append(request.getParameter("hdnUserType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }

                            if ("Update".equals(request.getParameter("hdnbutton"))) {
                                deptDataBean.setDepartmentName(deptDataBean.getDepartmentName().trim());
                                manageEmployeeSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                if(manageEmployeeSrBean.updateDepartmentMaster(deptDataBean)){
                                response.sendRedirect("ViewEmployee.jsp?deptid=" + deptid + "&division=" + departmentType + "&msg=success");
                                }else{
                                    response.sendRedirect("EditEmployee.jsp?deptid=" + deptid + "&division=" + departmentType+"&msg=deptUpdate");
                                }
                                //response.sendRedirect("ManageEmployee.jsp?deptType=" + departmentType);
                            }
                %>
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1">
                            <div class="pageHead_1">Update <%if(departmentType.equalsIgnoreCase("Organization")) { out.print("Division"); }else if(departmentType.equalsIgnoreCase("Division")) { out.print("Department"); } else {out.print(departmentType);}%> Details</div>
                            <%if(request.getParameter("msg") != null && request.getParameter("msg").equals("deptUpdate")){
                            if(departmentType.equalsIgnoreCase("Ministry")){%>
                            <div class='responseMsg errorMsg'><%= appMessage.departmentUpdateM %></div>
                            <% } else if(departmentType.equalsIgnoreCase("Division")){%>
                            <div class='responseMsg errorMsg'><%= appMessage.departmentUpdateD%></div>
                            <% } else if(departmentType.equalsIgnoreCase("Organization")){%>
                            <div class='responseMsg errorMsg'><%= appMessage.departmentUpdateO %></div>
                            <% } } %>
                            <form id="frmEditEmployee" name="frmEditEmployee" method="post" action="">
                                <input type="hidden" id="departmentiddfd"/>
                                <table width="100%" class="formStyle_1" border="0" cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="ff t-align-left">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                    </tr>
                                    <%
                                                String pDeptName = "";
                                                for (TblDepartmentMaster tdm : manageEmployeeSrBean.getFindUser(deptid)) {
                                                    if (!departmentType.equals("Ministry")) {
                                                        if (departmentType.equals("Division")) {
                                                            pDeptName = "Ministry";
                                                        }
                                                        else if (departmentType.equals("Organization")) {
                                                            pDeptName = "Department";
                                                        }else {
                                                            pDeptName = "Division";
                                                        }
                                    %>
                                    <tr>
                                        <td class="ff" width="200">Parent <%=pDeptName%> :</td>
                                        <td width="200"><label><%=manageEmployeeSrBean.getParentDeptName()%></label></td>
                                    </tr>
                                    <% }%>
                                    <tr>
                                        <td class="ff" width="200"><label id="lblName" style="width: 400px" >Name of <% if(tdm.getDepartmentType().equalsIgnoreCase("Organization")) { out.print("Division"); } else if(tdm.getDepartmentType().equalsIgnoreCase("Division")) { out.print("Department"); }  else { out.print(tdm.getDepartmentType()); } %> :</label> <span>*</span></td>
                                        <td>
                                            <input type="text" name="departmentName" id="txtDeptName" style="width: 400px;" class="formTxtBox_1" value="<%=tdm.getDepartmentName()%>" maxlength="101" onblur="chkUnique();"/>
                                            <div id="Msg" style="color: red; font-weight: bold"></div>
                                            <div id="SearchValError" class="reqF_1"></div>
                                            <input type="hidden" name="hiddenDeptName" value="<%= tdm.getDepartmentName()%>" id="txtHiddenDeptName"/>
                                            <input type="hidden" name="strCreatedDate" value="<%= tdm.getCreatedDate()%>" id="txtcreatedDate"/>
                                            <input type="hidden" name="departmentType" value="<%= tdm.getDepartmentType()%>" id="txtDepartmentType" />
                                            <input type="hidden" id="departmentId" name="departmentId" value="<%= deptid%>"/>
                                            <input type="hidden" id="deptParentId" name="deptParentId" value="<%= tdm.getParentDepartmentId()%>"/>
                                            <input type="hidden" id="deptHirchy" name="deptHirchy" value="<%= tdm.getDepartmentHirarchy()%>"/>
                                        </td>
                                    </tr>
<!--         //                            <tr>
                                        <td class="ff" width="200"><label id="lblNameBangla">Name of <%if(tdm.getDepartmentType().equalsIgnoreCase("Organization")) { out.print("Organization"); } else {out.print(tdm.getDepartmentType());}%> in Dzongkha :</label></td>
                                        <td>
                                            <%
                                                                                                String str = "";
                                                                                                if (tdm.getDeptNameInBangla() != null) {
                                                                                                    str = new String(tdm.getDeptNameInBangla(), "UTF-8");

                                                                                                }
                                            %>
                                            <input type="text" name="deptBanglaString" style="width: 400px;" class="formTxtBox_1" value="<%=str%>"/>
                                        </td>
                                    </tr>-->
                                    <tr>
                                        <td class="ff" width="200">Address : <span>*</span></td>
                                        <td>
                                            <textarea class="formTxtBox_1" id="address" rows="5" name="address" style="width: 400px" onkeypress="return imposeMaxLength(this, 1001, event);"><%=tdm.getAddress()%></textarea>
                                            <input type="hidden" value="<%=userid%>" name="approvingAuthId" id="approvingAuthId"/>
                                            <%--<input type="text" name="address" style="width: 200px;" class="formTxtBox_1" />--%>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Country : </td>

                                        <td> Bhutan
                                            <select name="countryId" class="formTxtBox_1" id="cmbCountry" style="width: 205px;display:none">
                                                <%
                                                                                                    for (SelectItem country : deptMasterSrBean.getCountryMaster()) {
                                                                                                        if (country.getObjectValue().equals(tdm.getTblCountryMaster().getCountryName())) {
                                                %>
                                                <option  value="<%=country.getObjectId()%>" selected="true"><%=country.getObjectValue()%></option>
                                                <%                                                                                                    } else {
                                                %>
<!--                                                <option  value="<%=country.getObjectId()%>"><%=country.getObjectValue()%></option>-->
                                                <%                                                                }
                                                                                                    }
                                                %>

                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Dzongkhag / District : <span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbState" name="stateId" style="width: 205px">
                                                <%
                                                                                                    for (SelectItem stateItem : manageEmployeeSrBean.getStateList()) {
                                                                                                        if (stateItem.getObjectValue().equalsIgnoreCase(tdm.getTblStateMaster().getStateName())) {
                                                %>
                                                <option  value="<%=stateItem.getObjectId()%>" selected="selected"><%=stateItem.getObjectValue()%></option>
                                                <%
                                                                                                                                                        } else {
                                                %>
                                                <option  value="<%=stateItem.getObjectId()%>"><%=stateItem.getObjectValue()%></option>
                                                <%  }
                                                                                                    }%>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">City / Town : </td>
                                        <td>
                                            <input type="text" name="city" style="width: 200px;" class="formTxtBox_1" value="<%=tdm.getCity()%>"/>
                                        </td>
                                    </tr>
                                    <tr>
<!--                                        <td class="ff" width="200">Thana / Upazilla : <span>*</span></td>-->
                                        <td>
                                            <input type="hidden" name="upJilla" style="width: 200px;" class="formTxtBox_1" value="<%=tdm.getUpJilla()%>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Post Code : </td>
                                        <td>
                                            <input type="text" name="postCode" style="width: 200px;" class="formTxtBox_1" id="txtPostCode" maxlength="19" value="<%=tdm.getPostCode()%>"/>
                                            <div id="SearchValErrorPostCode" class="reqF_1"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff"  width="200">Phone No. : </td>
                                        <td>
                                            <input type="text" name="phoneNo" style="width: 200px;" class="formTxtBox_1" <% if (!tdm.getPhoneNo().equals("")) {%>value="<%=tdm.getPhoneNo()%>" <% }%>/>
                                            <span id="phno" style="color: grey;">AreaCode-Phone No. i.e. 02-336962</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Fax No. :</td>
                                        <td>
                                            <input type="text" name="faxNo" style="width: 200px;" class="formTxtBox_1" <% if (!tdm.getFaxNo().equals(" ")) {%>value="<%=tdm.getFaxNo()%>" <% }%>/>
                                            <span id="fxno" style="color: grey;">AreaCode-Fax No. i.e. 02-336961</span>
                                        </td>
                                    </tr>

                                    <!--                                    <tr>
                                                                            <td class="ff" width="200">Mobile Number</td>
                                                                            <td>
                                                                                <input type="text" name="mobileNo" style="width: 200px;" class="formTxtBox_1" <% //if (tdm.getMobileNo() != null) {%>value="<%//dm.getMobileNo()%>" <% //}%> />
                                                                            </td>
                                                                        </tr>-->
                                    <tr>
                                        <td class="ff" width="200">Website :</td>
                                        <td>
                                            <input type="text" name="website" style="width: 200px;" class="formTxtBox_1" <% if (tdm.getWebsite() != null) {%>value="<%=tdm.getWebsite()%>" <% }%>/>
                                            <div id="websiteToolTip" style="color: grey;">(Enter the Website name without 'http://' e.g. pppd.gov.bt)</div>
                                            <span id="webMsg" style="color: red; ">&nbsp;</span>
                                        </td>
                                    </tr>
                                    <%-- Nitish Start 11/06/2017 
                                    Only if condition will execute when egp Admin create Divisions 
                                    otherwise it will take else condition like 
                                            create Antonomus Body, Ministry etc--%>
                                    
                                    <% if(departmentType.equalsIgnoreCase("Organization"))
                                    {%>
                                   <tr>
                                        <td class="ff" width="200">Corporation Type :</td>
                                        <td><input class="formTxtBox_1" <% if(tdm.getOrganizationType().equalsIgnoreCase("yes"))
                                                                        {%>
                                                                        checked="checked"
                                                                        <% } %> type="radio" name="organizationType" value="yes" id="organizationType1"/> Yes
                                            <input class="formTxtBox_1" <% if(tdm.getOrganizationType().equalsIgnoreCase("no"))
                                                                        {%>checked="checked"<% } %> 
                                                                        type="radio" name="organizationType" value="no" id="organizationType2"/> No
                                        </td>
                                    </tr>
                                    <% }else{ %>
                                    <tr>
<!--                                        <td class="ff" width="200">Thana / Upazilla : <span>*</span></td>-->
                                        <td>
                                            <input type="hidden" name="organizationType" style="width: 200px;" class="formTxtBox_1" value="<%=tdm.getOrganizationType()%>"/>
                                        </td>
                                    </tr>
                                    <% }}%>
                                    <%--Nitish END --%>
                                    <tr><td>&nbsp;</td>
                                        <td align="left"><label class="formBtn_1"><input id="btnUpdate" name="Update" value="Update" type="submit" onclick="return chkUniqueDept()" /></label>
                                            <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script type="text/javascript">
            function chkregPostCode(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function chkreg(value)
            {
                return /^[a-zA-Z 0-9](?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9]+$)+$/.test(value);

            }
            var bvalidationPostCode = true;
//            $('#txtPostCode').blur(function() {
//                bvalidationPostCode = true;
//                if($('#txtPostCode').val()==''){
//                    $('#SearchValErrorPostCode').html('Please enter Postcode');
//                    bvalidationPostCode = false;
//                }else if(!chkregPostCode($('#txtPostCode').val())){
//                    $('#SearchValErrorPostCode').html('Please enter digits (0-9) only');
//                    bvalidationPostCode = false;
//                }else if($('#txtPostCode').val().length > 4 || $('#txtPostCode').val().length < 4){
//                    $('#SearchValErrorPostCode').html('Postcode should comprise of 4 digits only');
//                    bvalidationPostCode = false;
//                }else{
//                    $('#SearchValErrorPostCode').html('');
//                }
//            });
            var bvalidation;
            $(function() {
                $('#btnUpdate').click(function() {
                    bvalidation = true;
                    if($('#txtDeptName').val()==''){
                        $('#SearchValError').html('Please enter name of <% if(departmentType.equalsIgnoreCase("Organization")) { out.print("Organizatioin/PA"); } else {out.print(departmentType);} %>');
                        bvalidation = false;
                    }
//                    if($('#txtPostCode').val()==''){
//                        $('#SearchValErrorPostCode').html('Please enter Postcode');
//                        bvalidation = false;
//                    }
                });
            });
            var bvalidationName;
            function chkUnique(){
                if(document.getElementById('txtDeptName') != null && document.getElementById('txtDeptName').value != null){
                    if(document.getElementById('txtHiddenDeptName') != null && document.getElementById('txtHiddenDeptName').value != null){
                        var txtDeptName = document.getElementById('txtDeptName').value;
                        var txtHiddenDeptName = document.getElementById('txtHiddenDeptName').value;
                        if(txtHiddenDeptName == txtDeptName){
                            //$('span.#Msg').html('');
                        }else{
                            bvalidationName = true;
                            if($('#txtDeptName').val()!=''){
                                var flag=document.getElementById('txtDeptName').value;
                                if(flag.charAt(0) == " "){
                                    $('#Msg').html("");
                                    $('#SearchValError').html('Only Space is not allowed');
                                    bvalidationName = false;
                                }else if(!chkreg($('#txtDeptName').val())){
                                    $('#Msg').html("");
                                    $('#SearchValError').html('Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' " } { - . _) Only');
                                    bvalidationName = false;
                                }else if(flag.length > 100){
                                    $('#Msg').html("");
                                    $('#SearchValError').html('Allows maximum 100 characters only');
                                    bvalidationName = false;
                                }else{
                                    $('#SearchValError').html('');
                            //$('#Msg').html(" ");
                                    $('#Msg').css("color","red");
                                    $('#Msg').html("Checking for unique Name of <% if(departmentType.equalsIgnoreCase("Organization")) { out.print("Organization"); }  else {out.print(departmentType);} %>...");
                                    $.post("<%=request.getContextPath()%>/CommonServlet", {depName:$.trim($('#txtDeptName').val()),depType:'<%=departmentType%>',funName:'verifyDeptName'},
                                    function(j)
                                    {
                                        if(j.toString().indexOf("OK", 0)!=-1){
                                            $('#Msg').css("color","green");

                                        }
                                        else if(j.toString().indexOf("<%=departmentType%>", 0)!=-1){
                                            $('#Msg').css("color","red");

                                        }
                                        $('#Msg').html(j);
                                    });
                                }
                            }
                        }
                    }
                }
                
            }
            function chkUniqueDept(){
                if(document.getElementById('Msg').innerHTML == 'Checking for unique Name of <%=departmentType %>...' || document.getElementById('Msg').innerHTML == '<%=departmentType %> Name Already Exists'){
                    return false;
                }
                else{
                    //return true;
                }
            }
        </script>
<!--        <script type="text/javascript">
            $(function() {
                $('#frmEditEmployee').submit(function() {
                    if($('span.#Msg').html().length > 0 ){
                        if(($('span.#Msg').html()=="OK") || ($('span.#Msg').html()=="Ok")){
                            return true;
                        }else{
                            return false;
                        }
                    }
                    else{
                        return true;
                    }
                });
            });
        </script>-->
        <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>

    <%
                manageEmployeeSrBean = null;
                deptMasterSrBean = null;
                deptDataBean = null;
    %>
</html>
