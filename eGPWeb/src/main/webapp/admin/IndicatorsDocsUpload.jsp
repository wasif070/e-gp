<%--
    Document   : IndicatorsDocsUpload
    Created on : July, 2015, 3:29:18 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />
<%@page  import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Admin Docs Upload</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">

            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:100}
                    },
                    messages: {
                        uploadDocFile: { required: "<div class='reqF_1'>Please select Document</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please enter Description</div>",
                            maxlength: "<div class='reqF_1'>Maximum 100 characters are allowed.</div>"}
                    }
                });
            });

            $(function() {
            $('#frmUploadDoc').submit(function() {
                if($('#frmUploadDoc').valid()){
                    $('.err').remove();
                    var count = 0;
                    var browserName=""
                    var maxSize = parseInt($('#fileSize').val())*1024*1024;
                    var actSize = 0;
                    var fileName = "";
                    jQuery.each(jQuery.browser, function(i, val) {
                         browserName+=i;
                    });
                    $(":input[type='file']").each(function(){
                        if(browserName.indexOf("mozilla", 0)!=-1){
                            actSize = this.files[0].size;
                            fileName = this.files[0].name;
                        }else{
                            var file = this;
                            var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                            var filepath = file.value;
                            var thefile = myFSO.getFile(filepath);
                            actSize = thefile.size;
                            fileName = thefile.name;
                        }
                        if(parseInt(actSize)==0){
                            $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                            count++;
                        }
                        if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                            $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                            count++;
                        }
                        if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                            $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                            count++;
                        }
                    });
                    if(count==0){
                        $('#button').attr("disabled", "disabled");
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            });
        });
        </script>

        <style type="text/css">
            .hideGoBackButton {
              display: none;
             }

        </style>
    </head>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                <tr>
                    <%--<%@include file="../resources/common/AfterLoginLeft.jsp" %>--%>
                    <jsp:include page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td>
                         <div class="contentArea_1">
                <%

                              String tenderPaymentId = "";
                            if (session.getAttribute("userId") == null) {
                                response.sendRedirect("SessionTimedOut.jsp");
                            }
                           
                            String queryString = "";
                            if (request.getQueryString() != null) {
                                queryString = request.getQueryString();
                            }
                            String pageName = "";
                            String uri = "";
                            if (request.getRequestURI() != null) {
                                uri = request.getRequestURI();
                            }
                            String url = "";
                            if (request.getRequestURL() != null) {
                                url = request.getRequestURL().toString();
                            }
                           
                            if (request.getParameter("page") == null) {
                                String temp = url.substring(0, url.indexOf(uri));
                                pageName = request.getHeader("referer");
                                pageName = (pageName.replace(temp, ""));
                             
                            }
                         
                            if (request.getParameter("page") != null) {
                                pageName = (request.getParameter("page"));
                            }

                %>
               
                <form id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/AdminReportUploadServlet" enctype="multipart/form-data" name="frmUploadDoc">
                    <input type="hidden" name="pageName" value="<%=pageName%>"/>                    
                    <div class="pageHead_1">Performance Indicators Report
                        <%if (request.getParameter("page") != null) { %>
                        <span style="float:right;"><a href="<%=pageName%>" class="action-button-goback hideGoBackButton">Go Back</a></span>
                        <%} else {
                              String PageRef = "";                                                 
                              PageRef = request.getHeader("referer");
                                
                         %>
                        <!-- <span style="float:right;"><a href="<%=request.getHeader("referer")%>" class="action-button-goback">Go Back</a></span> -->
                         <span style="float:right;"><a href="<%=PageRef%>" class="action-button-goback hideGoBackButton">Go Back</a></span>
                        <%}%>
                    </div>            
                    <%
                                if (request.getParameter("fq") != null) {
                                    if (request.getParameter("fq").equals("Removed") || request.getParameter("fq").equals("Uploaded")|| request.getParameter("fq").equals("Update")) {
                    %>
                    <!--<div class="responseMsg successMsg" style="margin-top: 10px;">File <%=request.getParameter("fq")%> Successfully</div> -->
                    <div class="responseMsg successMsg" style="margin-top: 10px;">File Update Successfully</div>
                    <%  } else {
                    %>
                    <div class="responseMsg errorMsg t_space"><%=request.getParameter("fq")%></div>
                    <%
                                                        }}
                                                        if (request.getParameter("fs") != null) {
                    %>
                    <div class="responseMsg errorMsg t_space">
                        Max FileSize <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                    </div>
                    <%
                                    }
                               // }  Modified By dohatec for document upload checking
                    %>
                    <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>

                            <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                        </tr>
                        <tr>
                            <td width="10%" class="ff t-align-left">Document   : <span class="mandatory">*</span></td>
                            <td width="90%" class="t-align-left"><input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:200px; background:none;"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Description : <span>*</span></td>
                            <td>
                                <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="documentBrief" style="width:200px;" />
                                <div id="dvDescpErMsg" class='reqF_1'></div>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <%--<label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Upload" onclick="return checkfile()" /></label>--%>
                                <label class="formBtn_1"><input type="submit" name="btnUpld" id="button" value="Upload" /></label>
                            </td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th  class="t-align-left">Instructions</th>
                        </tr>
                        <tr>
                            <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("egpadmin");%>
                            <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.

                            <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">Acceptable File Types <span class="mandatory"><%out.print(tblConfigurationMaster.getAllowedExtension().replace(",", ",  "));%></span></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                        </tr>
                         
                    </table>
                       
                </form>
                   <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <th width="4%" class="t-align-center">Sl. No.</th>
                        <th class="t-align-center" width="23%">File Name</th>
                        <th class="t-align-center" width="32%">File Description</th>
                        <th class="t-align-center" width="7%">File Size <br />
                            (in KB)</th>
                        <th class="t-align-center" width="18%">Action</th>
                        <th class="t-align-center" width="18%">File Status in Homepage</th>
                    </tr>
                    <%
                                int docCnt = 0;
                                TenderCommonService tenderCS = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                if (session.getAttribute("userId") != null) {
                                    tenderCS.setLogUserId(session.getAttribute("userId").toString());
                                 }
                                for (SPTenderCommonData sptcd : tenderCS.returndata("ReportDocs", "", "")) {
                                    docCnt++;

                    %>
                    <tr>
                        <td class="t-align-center"><%=docCnt%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName1()%></td>
                        <td class="t-align-left"><%=sptcd.getFieldName2()%></td>
                        <td class="t-align-center"><%=(Long.parseLong(sptcd.getFieldName3()) / 1024)%></td>
                        <td class="t-align-center">
                            <a href="<%=request.getContextPath()%>/AdminReportUploadServlet?docName=<%=sptcd.getFieldName1()%>&docSize=<%=sptcd.getFieldName3()%>&funName=download" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                            &nbsp;
                            <a href="<%=request.getContextPath()%>/AdminReportUploadServlet?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&pageName=<%=pageName%>&funName=remove"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a>
                        </td>
                        <td class="t-align-center">
                            <% if(sptcd.getFieldName5().equalsIgnoreCase("yes")) { %>
                            <a href="<%=request.getContextPath()%>/AdminReportUploadServlet?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&status=<%=sptcd.getFieldName5()%>&funName=update">Show</a>


                            <% } else { %>
                            <a href="<%=request.getContextPath()%>/AdminReportUploadServlet?&docName=<%=sptcd.getFieldName1()%>&docId=<%=sptcd.getFieldName4()%>&status=<%=sptcd.getFieldName5()%>&funName=update">Hide</a>
                            <% } %>
                        </td>
                    </tr>

                    <%   if (sptcd != null) {
                                        sptcd = null;
                                    }
                                }%>
                    <% if (docCnt == 0) {%>
                    <tr>
                        <td colspan="6" class="t-align-center">No records found.</td>
                    </tr>
                    <%}%>
                </table>
            </div>
                    </td>
                </tr>
            </table>


           
            <div>&nbsp;</div>
            <%@include file="../resources/common/Bottom.jsp" %>
        </div>
    </body>


</html>
