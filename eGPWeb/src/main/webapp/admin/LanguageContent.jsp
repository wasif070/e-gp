<%--
    Document   : LanguageContent
    Created on : Dec 18, 2010, 5:25:01 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblMultiLangContent"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblHelpManual"%>
<%@page import="com.cptu.egp.eps.web.servicebean.HelpManualSrBean"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="languageContentSrBean" class="com.cptu.egp.eps.web.servicebean.LanguageContentSrBean"/>
<%
        TblMultiLangContent tblMultiLangContent = new TblMultiLangContent();

        int contentId = 0;
        String title = "";
        String subtitle = "";
        String displayTitle = "";
        byte[] value = null;
        String language = "";
        String lang_code = "";
        String langContent = "";
        int lenlangContent = 0;

        /* *********** Update Language Content ************** */
        if("update".equalsIgnoreCase(request.getParameter("hidaction"))){
            contentId = Integer.parseInt(request.getParameter("langContentId").toString());
            title = request.getParameter("namelocation");
            subtitle = request.getParameter("namesubtitle");
            displayTitle = request.getParameter("nametitle");
            language = request.getParameter("namelanguage");
            langContent = request.getParameter("langContent");
            lenlangContent = langContent.length();

            // add for the corrupted content problem.
            if(lenlangContent%2!=0){
                langContent = langContent + " ";
            }

            //add Bangla UTF String for Bangla content
            if(language=="bn_IN"){
                value = BanglaNameUtils.getBytes(langContent);
            }else{
                value = langContent.getBytes();
            }

            // Call update function
            languageContentSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
            String msg = languageContentSrBean.updatelangContent(title, subtitle, language, displayTitle, value, contentId);
            if(msg.equals("Value updated")){
                response.sendRedirect("LanguageListing.jsp");
            }
        }

        /* *********** Edit and View Language Content ************** */
        if("Edit".equals(request.getParameter("action")) || "View".equals(request.getParameter("action"))){
            int id = Integer.parseInt(request.getParameter("id").toString());
            Iterator t = languageContentSrBean.getData(id).iterator();

            while(t.hasNext()){
               tblMultiLangContent = (TblMultiLangContent) t.next();
               contentId = tblMultiLangContent.getContentId();
               title = tblMultiLangContent.getTitle();
               displayTitle = tblMultiLangContent.getDisplayTitle();
               subtitle = tblMultiLangContent.getSubTitle();
               value = tblMultiLangContent.getValue();
               lang_code = tblMultiLangContent.getLanguage();
               if(lang_code.equals("en_US"))
                   language = "English";
               else
                   language = "Dzongkha";
            }
        }
    %>
<html>
    <head>
         <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - Programme Multilingual Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script type="text/javascript">
           
            /* *********** Validate Edit Language Content ************** */
            function checkData(){
                var flag = 1;
                    <% if(subtitle.equals("content_home") || subtitle.equals("content_aboutegp") || subtitle.equals("content_contactus") || subtitle.equals("content_faq") || subtitle.equals("content_helpdesk") || subtitle.equals("content_egpguidelines") || subtitle.equals("content_mandatoryregdocs") || subtitle.equals("content_memberschbanks") || subtitle.equals("content_termsncond") || subtitle.equals("content_privacypolicy") || subtitle.equals("content_impmsgdetails") || subtitle.equals("content_nursteps")|| subtitle.equals("content_requirement")){ %>
                if(CKEDITOR.instances.idlangContent.getData() == 0){
                    document.getElementById("idlangContentMsg").innerHTML = "Please Enter the Help Content";
                    flag = 0;
                }
                <% }else{ %>
                if(document.getElementById("idlangContent").value==""){
                    document.getElementById("idlangContentMsg").innerHTML = "Please Enter the Language Content";
                    flag = 0;
                }
                <% } %>
                if(flag=="1"){
                    return true;
                }else{
                    return false;
                }                    
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <%--<span class="c-alignment-right"><a href="HelpListing.jsp" class="action-button-goback">Go Back To Dashboard</a></span>--%>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                <tr valign="top">
                    <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <td class="contentArea" style="vertical-align: top;" align="center" valign="middle">
                            <div class="pageHead_1 t-align-left">Multilingual
                                <span class="c-alignment-right">
                                    <a href="LanguageListing.jsp" class="action-button-goback">Go Back</a>
                                </span>
                            </div>
                        <form action="LanguageContent.jsp"  name="frmlanguagecontent" id="idfrmlanguagecontent" method="post">
                        <input type="hidden" name="langContentId" id="idlangContentId" value="<%=contentId%>">
                        <% if("Edit".equals(request.getParameter("action"))){ %>
                            <input type="hidden" name="hidaction" id="idaction" value="update">
                        <% } %>
                        <table width="100%" cellspacing="10" class="formStyle_1 t_space">
                        <tr>
                            <td width="8%" class="t-align-left ff">Location:</td>
                            <td width="92%" class="t-align-left">
                                <input type="hidden" name="namelocation" id="idlocation" value="<%=title%>">
                                <%=title%>
                            </td>
                        </tr>
                        <tr>
                            <td width="8%" class="t-align-left ff">Title:</td>
                            <td width="92%" class="t-align-left">
                                <input type="hidden" name="nametitle" id="idtitle" value="<%=displayTitle %>">
                                <input type="hidden" name="namesubtitle" id="idsubtitle" value="<%=subtitle%>">
                                <%=displayTitle%>
                            </td>
                        </tr>
                        <tr>
                            <td width="8%" class="t-align-left ff">Language:</td>
                            <td width="92%" class="t-align-left">
                                <input type="hidden" name="namelanguage" id="idlanguage" value="<%=lang_code%>">
                                <%=language%>
                            </td>
                        </tr>
                        <tr>
                            <td class="t-align-left ff">Content:</td>
                            <td class="t-align-left">
                                <!-- put FCK Editor for the content field -->
                                <% if( "Edit".equals(request.getParameter("action"))){ %>
                                <textarea rows="5" id="idlangContent" name="langContent" class="formTxtBox_1" style="width:40%;"><%=BanglaNameUtils.getUTFString(value)%></textarea>
                                <span id="idlangContentMsg" style="color: red; font-weight: bold"></span>
                                <% if(subtitle.equals("content_home") || subtitle.equals("content_aboutegp") || subtitle.equals("content_contactus") || subtitle.equals("content_faq") || subtitle.equals("other_links") || subtitle.equals("content_helpdesk") || subtitle.equals("content_egpguidelines") || subtitle.equals("content_mandatoryregdocs") || subtitle.equals("content_memberschbanks") || subtitle.equals("content_termsncond") || subtitle.equals("content_privacypolicy") || subtitle.equals("content_impmsgdetails") || subtitle.equals("content_nursteps") || subtitle.equals("content_requirement") || subtitle.equals("content_newFeature")|| subtitle.equals("content_serviceLevel")){ %>
                                    <script type="text/javascript">
                                      CKEDITOR.replace( 'idlangContent',{
                                           toolbar : "egpToolbar"
                                      });
                                    </script>
                                <% } %>
                                <% }else{ %>
                                <%=BanglaNameUtils.getUTFString(value)%>
                                <% } %>
                            </td>
                        </tr>
                         <% if( "Edit".equals(request.getParameter("action"))){ %>
                        <tr>
                            <td class="t-align-left ff">&nbsp;</td>
                            <td class="t-align-left">
                                <label class="formBtn_1">
                                    <input name="btnsubmit" type="submit" value="Submit" onclick="return checkData();" />
                                </label>
                            </td>
                        </tr>
                        <% } %>
                        </table>
                    </form>
                    </td>
                </tr>
            </table>
            </div>
            <jsp:include page="../resources/common/Bottom.jsp"></jsp:include>
            <script>
                var headSel_Obj = document.getElementById("headTabContent");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
     </body>
</html>