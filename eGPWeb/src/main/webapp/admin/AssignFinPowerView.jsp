<%-- 
    Document   : AssignFinPowerView
    Created on : Jan 13, 2011, 11:33:37 AM
    Author     : Naresh.Akurathi
--%>


<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementRole"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="com.cptu.egp.eps.model.table.TblFinancialYear"%>
<%@page import="com.cptu.egp.eps.model.table.TblBudgetType"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblFinancialPowerByRole"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Assign Financial Power View</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <script type="text/javascript">
            function conform()
            {
                if (confirm("Do you want to delete this business rule?"))
                    return true;
                else
                    return false;
            }
        </script>
    </head>
    <body>
        <%
            Logger LOGGER = Logger.getLogger("AssignFinPowerView.jsp");
        %>
        <%                
                ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();
                String mesg= request.getParameter("msg");
                String delMsg= "";

                 if("delete".equals(request.getParameter("action"))){
                int finId = Integer.parseInt(request.getParameter("id"));
                TblFinancialPowerByRole finPow = new TblFinancialPowerByRole();
                finPow.setFinancialPowerByRoleId(finId);
                finPow.setFpAmount(new BigDecimal(Byte.parseByte("1")));
                finPow.setOperator(">");
                finPow.setTblBudgetType(new TblBudgetType(Byte.parseByte("1")));
                finPow.setTblFinancialYear(new TblFinancialYear(Byte.parseByte("1")));
                finPow.setTblProcurementMethod(new TblProcurementMethod(Byte.parseByte("1")));
                finPow.setTblProcurementNature(new TblProcurementNature(Byte.parseByte("1")));
                finPow.setTblProcurementRole(new TblProcurementRole(Byte.parseByte("1")));
                configPreTenderRuleSrBean.delFinancialPowerByRole(finPow);                
                delMsg = "Amendment Business Rule Deleted Successfully";
                }

        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%--<jsp:include page="../resources/common/AfterLoginTop.jsp" ></jsp:include>--%>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 250px; display: block;">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                            </tr>
                        </table>
                    </td>
                    <td class="contentArea">
                        <!--Dashboard Content Part Start-->
                        <div class="pageHead_1">Assign Financial Power Details</div>

                          <%if("delete".equals(request.getParameter("action"))){%>
                          <div class="responseMsg successMsg"><%=delMsg%></div>
                          <%}%>

                          <%if(mesg != null){%>
                          <div class="responseMsg successMsg"><%=mesg%></div>
                          <%}%>
                        <div>&nbsp;</div>
                        <div class="overFlowContainer t-align-right">

                                <table width="100%" cellspacing="0" class="tableList_1" id="tblrule" name="tblrule">
                                    <tr >
                                        <th >Financial Year</th>
                                        <th >Budget Type</th>
                                        <th >Procurement Role</th>
                                        <th >Procurement Category</th>
                                        <th >Procurement Method</th>
                                        <th >Operator</th>
                                        <th >Amount</th>
                                        <th >Action</th>
                                     </tr>
                                    <%
                                    try{
                                           
                                            String msg = "";
                                            List<TblFinancialPowerByRole> lst = configPreTenderRuleSrBean.getAllFinancialPowerByRole();
                                            if(!lst.isEmpty()){
                                                Iterator it = lst.iterator();
                                                int i = 0;
                                                while(it.hasNext()){
                                                    TblFinancialPowerByRole tblFinancialPowerByRole = (TblFinancialPowerByRole)it.next();
                                                    i++;
                                                    
                                    %>
                                    <%if(i%2==0){%>
                                      <tr style='background-color:#E4FAD0;'>
                                          <%}else{%>
                                          <tr>
                                          <%}%>
                                        <td class="t-align-center"><%=tblFinancialPowerByRole.getTblFinancialYear().getFinancialYear()%></td>
                                        <td class="t-align-center"><%=tblFinancialPowerByRole.getTblBudgetType().getBudgetType()%></td>
                                        <td class="t-align-center"><%=tblFinancialPowerByRole.getTblProcurementRole().getProcurementRole()%></td>
                                        <td class="t-align-center"><%=tblFinancialPowerByRole.getTblProcurementNature().getProcurementNature()%></td>
                                        <td class="t-align-center"><%=tblFinancialPowerByRole.getTblProcurementMethod().getProcurementMethod()%></td>
                                        <td class="t-align-center"><%=tblFinancialPowerByRole.getOperator()%></td>
                                        <td class="t-align-center"><%=tblFinancialPowerByRole.getFpAmount()%></td>
                                        <td class="t-align-center"><a href="AssignFinPowerDetails.jsp?action=edit&id=<%=tblFinancialPowerByRole.getFinancialPowerByRoleId()%>"><font color="black"/>Edit</font></a>&nbsp;|&nbsp;<a href="AssignFinPowerView.jsp?action=delete&id=<%=tblFinancialPowerByRole.getFinancialPowerByRoleId()%>" onclick="return conform();"><font color="black"/>Delete</font></a></td>
                                          </tr></tr>
                                    <%
                                                }
                                            }else
                                                msg = "No Record Found";
                                            }
                                    catch(Exception e){
                                            LOGGER.debug("processRequest :Starts");
                                        }

                                    %>

                                </table>
                                <div>&nbsp;</div>
                        </div>
                        <!--Dashboard Content Part Start-->
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <script>
                var headSel_Obj = document.getElementById("headTabMngUser ");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>

