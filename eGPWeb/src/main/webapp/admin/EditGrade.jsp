<%-- 
    Document   : EditGrade
    Created on : Nov 23, 2010, 11:58:41 AM
    Author     : Malhar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Class Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript">
           <%-- $(function() {
                $('#txtGrade').change(function() {                    
                    if($.trim($('#txtGrade').val()) != $('#oldVal').val()) {
                        var spacetest=/^([a-zA-Z][a-zA-Z ]*[a-zA-Z])$/;
                        if($.trim($('#txtGrade').val()) != ''){
                            $('span.#gradeMsg').html("");
                            $('#txtGrade').val($.trim($('#txtGrade').val()));
                                $('#gradeMsg').html(" ");
                                $.post("<%=request.getContextPath()%>/GradeMasterSrBean", {grade:$('#txtGrade').val(),funName:'verifyGrade'},  function(j){
                                    if(j.toString().indexOf("Ok", 0)!=-1){
                                        $('#gradeMsg').css("color","green");
                                    }
                                    else{
                                        $('#gradeMsg').css("color","red");
                                    }
                                    $('span.#gradeMsg').html(j);
                                });                           
                        }else{
                            $('#gradeMsg').css("color","red");
                            $('span.#gradeMsg').html("Please Enter Class.");
                            $('#txtGrade').val($.trim($('#txtGrade').val()));                          
                        }
                    }
                    else
                    {
                        $('span.#gradeMsg').html("");
                        $('#txtGrade').val($.trim($('#txtGrade').val()));
                    }
                });
            });--%>
                <%-- $(function() {
                     $('#txtGrade').change(function() {
                        if($.trim($('#txtGrade').val()) == ''){
                            $('#gradeMsg').css("color","red");
                            $('span.#gradeMsg').html("Please Enter Class.");
                            $('#txtGrade').val($.trim($('#txtGrade').val()));
                        }else{
                            $('span.#gradeMsg').html("");
                            $('#txtGrade').val($.trim($('#txtGrade').val()));
                        }
                     });
                });--%>
        </script>
        <script type="text/javascript">            
                function checkGrade(){
                    if(document.getElementById("gradeMsg").innerHTML != "" && document.getElementById("gradeMsg").innerHTML != "Ok"){
                        return false;
                    }else{
                        return true;
                    }
                }               
        </script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#frmCreateGrade").validate({
                    rules:{
                        grade:{required:true},
                        gradeLevel:{required:true}
                    },

                    messages:{
                        grade:{required:"<div class='reqF_1'>Please select Class.</div>"},
                        gradeLevel:{required:"<div class='reqF_1'>Please select Grade.</div>"}
                    }

                });

            });

        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" ></jsp:include>
                        <td class="contentArea_1">
                            <jsp:useBean id="gradeDtBean" scope="page" class="com.cptu.egp.eps.web.databean.GradeDtBean"/>
                            <!--Page Content Start-->
                            <!-- Success failure -->
                            <%
                                        String msg = request.getParameter("msg");
                                        if (msg != null && msg.equals("success")) {%>
                            <div class="responseMsg errorMsg">Class Updation Failed</div>
                            <%}%>
                            <!-- Result Display End-->
                            <div class="t_space">
                                <div class="pageHead_1">Edit Class Details</div>
                                <div class="txt_1 c_t_space"><br />
                                </div>
                            </div>
                            <%
                                        String gradeId = request.getParameter("gradeId");
                                        if (gradeId != null && !gradeId.isEmpty()) {
                                            gradeDtBean.populateInfo(Short.parseShort(gradeId));


                                            if (gradeDtBean.isDataExists()) {%>
                            <form id="frmCreateGrade" action="<%=request.getContextPath()%>/GradeMasterSrBean" method="POST">
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                    <tr>
                                        <td class="ff">Class : <span>*</span></td>
                                        <td><!--input name="grade" type="text" value="${gradeDtBean.gradeName}" class="formTxtBox_1" id="txtGrade" style="width:200px;" maxlength="50"/-->

                                            <select name="grade" class="formTxtBox_1" id="txtGrade" style="width:208px;">
                                                <option value="">-- Select Class --</option>
                                                <%
                                                  for (int i = 1; i <= 4; i++) {
                                                %>
                                                <option <%if(("Class "+i).equals(gradeDtBean.getGradeName())){%>selected<%}%>>Class
                                                    <%=i%>
                                                </option>
                                                <%}%>
                                            </select>

                                            <input type="hidden" name="oldVal" id="oldVal" value="${gradeDtBean.gradeName}"/>
                                            <span id="gradeMsg" style="font-weight: bold"></span>
                                            <input type="hidden" name="gradeId" value="${gradeDtBean.gradeId}"/>
                                            <input type="hidden" name="action" value="update"></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Grade : <span>*</span></td>
                                        <td>
                                            <select name="gradeLevel" class="formTxtBox_1" id="cmbGradeLevel" style="width:208px;">
                                                <option value="">-- Select Grade --</option>
                                                <%
                                                  for (int i = 1; i <= 10; i++) {
                                                %>
                                                <option <%if(i==gradeDtBean.getGradeLevel()){%>selected<%}%> value="<%=i%>">Grade
                                                    <%=i%>
                                                </option>
                                                <%}%>
                                            </select>                                            
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <label class="formBtn_1">
                                                <input type="submit" name="gradeUpdate" id="btnUpdate" value="Update" onclick="return checkGrade();"/>
                                            </label>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <%}
                                                                        else {%>
                            <div class="responseMsg errorMsg">Data Not Found for Class: <%=gradeId%></div>
                            <%}
                                                                    }
                                                                    else {%>
                            <div class="responseMsg errorMsg">Plz provide Valid Class-Id</div>
                            <%}%>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
