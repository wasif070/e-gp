<%-- 
    Document   : EditCompanyDetails
    Created on : Oct 23, 2010, 6:43:14 PM
    Author     : parag
--%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Company Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="Include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="../resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1"><div class="pageHead_1">Edit User Registration - Company Details</div>
                            <!--Page Content Start-->
                            <form id="frmCompanyDetails" method="post">
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                <tr>
                                    <td class="ff">Company Reg. Number : <span>*</span></td>
                                    <td><input name="textfield52" type="text" class="formTxtBox_1" id="txtComRegNumber" style="width:200px;" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Company Name : <span>*</span></td>
                                    <td><input name="textfield5" type="text" class="formTxtBox_1" id="txtComName" style="width:200px;" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td class="ff">Company Name in Bangla : <span>*</span></td>
                                    <td><input name="textfield6" type="text" class="formTxtBox_1" id="txtComNameInBangla" style="width:200px;" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td class="ff">Company's Legal Status : <span>*</span></td>
                                    <td><input name="textfield61" type="text" class="formTxtBox_1" id="txtCompanyStatus" style="width:200px;" readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td class="ff">Company's<br />
                                        Establishment Year : <span>*</span></td>
                                    <td><input name="textfield62" type="text" class="formTxtBox_1" id="txtComExtYear" style="width:200px;" readonly="true" /></td>
                                </tr>
                                <%--<tr>
                                    <td class="ff">Trade License Issue Date : <span>*</span></td>
                                    <td><input name="textfield7" type="text" class="formTxtBox_1" id="txtIssueDate" style="width:100px;" readonly="readonly" />
                                        <a href="javascript:void(0);" onclick="" title="Calender"><img src="Images/Dashboard/calendarIcn.png" style="vertical-align:middle;" border="0" /></a></td>
                                </tr>
                                <tr>
                                    <td class="ff">Trade License Expiry Date : <span>*</span></td>
                                    <td><input name="textfield7" type="text" class="formTxtBox_1" id="txtExpiryDate" style="width:100px;" readonly="readonly" />
                                        <a href="javascript:void(0);" onclick="" title="Calender"><img src="Images/Dashboard/calendarIcn.png" style="vertical-align:middle;" border="0" /></a></td>
                                </tr>
                                <tr>
                                    <td class="ff">Tax Payment Number : <span>*</span></td>
                                    <td>
                                        <div class="t_space"> <input type="text" name="tinno" id="txtTaxIdNumber" value="123" style="width:200px;"/>
                                        </div>
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td class="ff">Nature of Business : <span>*</span></td>
                                    <td><textarea name="textarea" rows="3" class="formTxtBox_1" id="txtANatureOfBus" style="width:400px;"></textarea></td>
                                </tr>
                                <tr>
                                    <td class="ff">Registered Address : <span>*</span></td>
                                    <td><textarea name="textarea3" rows="3" class="formTxtBox_1" id="txtARegAddress"  style="width:400px;"></textarea></td>
                                </tr>
                                <tr>
                                    <td class="ff">Registered Office Country : <span>*</span></td>
                                    <td><select name="select2" class="formTxtBox_1" id="cmbRegCountry">
                                            <option>Select Country</option>
                                            <option>India</option>
                                            <option>Bhutan</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td class="ff">Registered Dzongkhag / District : <span>*</span></td>
                                    <td><select name="select2" class="formTxtBox_1" id="cmbRegSD"  >
                                            <option>Select Dzongkhag / District</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td class="ff">Registered City / Town : <span>*</span></td>
                                    <td><input name="textfield63" type="text" class="formTxtBox_1" id="txtRegCT" style="width:200px;" maxlength="50" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Registered Gewogs : <span>*</span></td>
                                    <td><input name="textfield63" type="text" class="formTxtBox_1" id="txtRegTU" style="width:200px;" maxlength="50" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Registered Post Code / Zip Code : <span>*</span></td>
                                    <td><input name="textfield63" type="text" class="formTxtBox_1" id="txtRegPZ" style="width:200px;" maxlength="15" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Registered Phone No : <span>*</span></td>
                                    <td><input name="textfield63" type="text" class="formTxtBox_1" id="txtRegPhoneNo" style="width:200px;" maxlength="20" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Registered Fax No : <span>*</span></td>
                                    <td><input name="textfield63" type="text" class="formTxtBox_1" id="txtRegFaxNo" style="width:200px;" maxlength="20" /></td>
                                </tr>

                                <tr>
                                    <td class="ff">Trade License Issue Date : <span>*</span></td>
                                    <td><input name="textfield7" type="text" class="formTxtBox_1" id="txtIssueDate" style="width:100px;" readonly="readonly" />
                                        <a href="javascript:void(0);" onclick="" title="Calender"><img src="Images/Dashboard/calendarIcn.png" style="vertical-align:middle;" border="0" /></a></td>
                                </tr>
                                <tr>
                                    <td class="ff">Trade License Expiry Date : <span>*</span></td>
                                    <td><input name="textfield7" type="text" class="formTxtBox_1" id="txtExpiryDate" style="width:100px;" readonly="readonly" />
                                        <a href="javascript:void(0);" onclick="" title="Calender"><img src="Images/Dashboard/calendarIcn.png" style="vertical-align:middle;" border="0" /></a></td>
                                </tr>
                                <tr>
                                    <td class="ff">Tax Payment Number or Other Similar Identification Number : <span>*</span></td>
                                    <td>
                                        <div class="t_space"> <input type="text" name="tinno" id="txtTaxIdNumber" value="123" style="width:200px;"/>
                                        </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="ff">Name of Other Document similar to TIN : <span>*</span></td>
                                    <td><input name="otherDoc1TIN" type="text" class="formTxtBox_1" maxlength="20" id="txtOtherDoc1TIN" style="width:200px;" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td class="ff"> <span></span></td>
                                    <td><input name="otherDoc2TIN" type="text" class="formTxtBox_1" maxlength="20" id="txtOtherDoc2TIN" style="width:200px;" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">As same as registered office address <span></span></td>
                                    <td><input name="regOfficeAddress" type="checkbox" class="formTxtBox_1"  id="txtRegOfficeAddress" style="width:200px;" /></td>
                                </tr>

                                <tr>
                                    <td class="ff">Corporate / Head office<br />
                                        Address : <span>*</span></td>
                                    <td><textarea name="textarea4" rows="3" class="formTxtBox_1" id="txtRegCorOffice" style="width:400px;"></textarea></td>
                                </tr>
                                <tr>
                                    <td class="ff">Corporate Country : <span>*</span></td>
                                    <td><select name="select2" class="formTxtBox_1" id="cmbCorCountry">
                                            <option>Select Country</option>
                                            <option>India</option>
                                            <option>Bhutan</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td class="ff">Corporate Dzongkhag / District : <span>*</span></td>
                                    <td><select name="select2" class="formTxtBox_1" id="cmbCorSD">
                                            <option>Select Dzongkhag / District</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td class="ff">Corporate City / Town : <span>*</span></td>
                                    <td><input name="textfield63" type="text" class="formTxtBox_1" id="txtCorCT" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Corporate Gewog : <span>*</span></td>
                                    <td><input name="textfield63" type="text" class="formTxtBox_1" id="txtCorTU" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Corporate Post Code / Zip Code : <span>*</span></td>
                                    <td><input name="textfield63" type="text" class="formTxtBox_1" id="txtCorPZ" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Corporate Phone No : <span>*</span></td>
                                    <td><input name="textfield63" type="text" class="formTxtBox_1" id="txtCorPhoneNo" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Corporate Fax No : <span>*</span></td>
                                    <td><input name="textfield63" type="text" class="formTxtBox_1" id="txtCorFaxNo" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Company's website : <span>*</span></td>
                                    <td><input name="textfield63" type="text" class="formTxtBox_1" id="txtCorWebsite" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><label class="formBtn_1">
                                            <input type="submit" name="btnSubmit" id="btnSubmit" value="Previous" />
                                        </label>
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input type="submit" name="btnSave" id="btnSave" value="Save" />
                                        </label>
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input type="submit" name="btnNext" id="btnNext" value="Next" />
                                        </label></td>
                                </tr>
                            </table>
                           </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
