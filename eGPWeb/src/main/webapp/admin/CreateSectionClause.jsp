<%--
    Document   : CreateSectionClause
    Created on : 24-Oct-2010, 1:03:35 AM
    Author     : yanki
--%>
<jsp:useBean id="sectionClauseSrBean" class="com.cptu.egp.eps.web.servicebean.SectionClauseSrBean" />
<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBean" />
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />
<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster" %>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.model.table.TblIttClause" %>
<%@page import="com.cptu.egp.eps.model.table.TblIttSubClause" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");


            if(session.getAttribute("userId") != null){
                sectionClauseSrBean.setLogUserId(session.getAttribute("userId").toString());
                createSubSectionSrBean.setLogUserId(session.getAttribute("userId").toString());
                defineSTDInDtlSrBean.setLogUserId(session.getAttribute("userId").toString());
            }
            int templateId = 0;
            if(request.getParameter("templateId") != null){
                templateId = Integer.parseInt(request.getParameter("templateId"));
            }
            String procType = "";
            List<TblTemplateMaster> templateMasterLst = defineSTDInDtlSrBean.getTemplateMaster((short) templateId);
            if(templateMasterLst != null){
                if(templateMasterLst.size() > 0){
                    procType = templateMasterLst.get(0).getProcType();
                }
                templateMasterLst = null;
            }
            if(request.getParameter("ittHeaderId") == null || request.getParameter("sectionId") == null){
                return;
            }
            int ittHeaderId = Integer.parseInt(request.getParameter("ittHeaderId"));
            int sectionId = Integer.parseInt(request.getParameter("sectionId"));
            String subSectionName = sectionClauseSrBean.getSubSectionName(ittHeaderId);

            String contentType = createSubSectionSrBean.getContectType(sectionId);
            if ("ITT".equals(contentType)) {  contentType="ITB";} 
            else if("TDS".equals(contentType)){contentType="BDS";} 
            else if("PCC".equals(contentType)){contentType="SCC";} 
            String contentType1 = "";
            if(contentType.equalsIgnoreCase("ITT")){
                contentType1 = "BDS";
            }else if(contentType.equalsIgnoreCase("ITC")){
                contentType1 = "PDS";
            }else if(contentType.equalsIgnoreCase("GCC")){
                contentType1 = "SCC";
            }

            boolean edit = false;
            if(request.getParameter("edit") != null){
                if(request.getParameter("edit").equalsIgnoreCase("true")){
                    edit = true;
                }
            }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Prepare <%=contentType%></title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.blockUI.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js"></script>
        <%--<script src="<%=request.getContextPath()%>/resources/js/form/CommonValidation.js"></script>--%>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="../ckeditor/adapters/jquery.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script type="text/javascript">
            var $jq = jQuery.noConflict();
            var procType = '<%= procType %>';
            var clauseCounterNo;

            function replaceAll(dataValue, replace, with_this) {
                return dataValue.replace(new RegExp(replace, 'g'),with_this);
                }

            function validate(){
                <%if(!edit){%>
                    //alert('validate');
                    var counter = 0;
                    $jq(".dynamicError").remove();
                    $jq('textArea[id^=subSectionClause_]').each(function(){

                        if($jq.trim($jq(this).val()) == ""){
                            $jq(this).parent().append("<span class='dynamicError' style='color:red'>Please enter Clause No. and Clause Description</span>");
                            $jq(this).focus();
                            counter++;
                        }
                    });
                    $jq('textArea[id^=subSectionSubClause_]').each(function(){
                        //if($jq(this).val().trim() == ""){
                        if($jq.trim(CKEDITOR.instances[$jq(this).attr("id")].getData()) == ""){
                            $jq(this).parent().append("<span class='dynamicError' style='color:red'>Please enter Sub Clause Details</span>");
                            $jq(this).focus();
                            counter++;
                        }
                    });
                    if(counter > 0){
                        return false;
                    }
                <%}%>
                }

            function removeSubClause(counter1,counter2){
                //alert('removeSubClause');
                if($jq("#subClauseCount_"+counter1).val() > 1){
                    var rowId1 = "tr_"+counter1+"_"+(counter2*2);
                    var rowId2 = "tr_"+counter1+"_"+((counter2*2)+1);

                    //subSectionSubClause
                    var id1 = $jq("#subSectionSubClause_"+counter1+"_"+counter2).attr("id");
                    var instances = CKEDITOR.instances[id1];
                    if(instances)
                    {
                        instances.destroy();
                        //CKEDITOR.remove(instances);
                    }
                    //CKEDITOR.replace(id1);

                    $jq("#"+rowId1).remove();
                    $jq("#"+rowId2).remove();
                    $jq("#subClauseCount_"+counter1).val($jq("#subClauseCount_"+counter1).val()-1);
                }else{
                    jAlert("At least one sub clause is required", "Error");
                }
            }

            function removeClause(counter){
                //alert('removeClause')
                if($jq("#clauseCount").val() > 1){
                    $jq('tr[id^=tr_'+counter+'_]').each(function(){
                        var id = $jq(this).attr("id");
                        var counter_Last=id.replace("tr_","");
                        if($jq("#subSectionSubClause_"+counter_Last) != undefined){
                            var id1 = $jq("#subSectionSubClause_"+counter_Last).attr("id");
                            var instance = CKEDITOR.instances[id1];
                            if(instance)
                            {
                                CKEDITOR.remove(instance);
                            }
                        }
                        $jq(this).remove();
                    });
                    $jq("#clauseCount").val($jq("#clauseCount").val()-1);
                }else{
                    jAlert("At least one clause is required", "Error");
                }
            }

            function addSubClause(eleObj){
                //alert('addSubClause');
                var eleId = eleObj.id;
                <%//if(!edit){ while edit it should also ask for this%>
                jPrompt("Please enter no of sub clauses required", "1", "Sub Clause", function(val){
                    if(isNaN(val)){
                        jAlert("Please enter numbers only", "Error");
                        return;
                    } else {
                        if(val.split(".").length > 1) {
                            jAlert("Please enter numbers only", "Error");
                            return;
                        }
                    }
                <%// }
                  // if(!edit){%>
                    for(var loop=0;loop < val; loop++){
                        //counter = counter + loop;
                        subClauseCount = subClauseCount + loop;
                <%//}%>
                        var counter = eleId.split("_")[1]*1;
                        var subClauseCount = (document.getElementById("subClauseCount_"+counter).value)*1;
                        document.getElementById("subClauseCount_"+counter).value = subClauseCount+1;
                        var totalSubClauseCount = (document.getElementById("totalSubClauseCount_"+counter).value)*1;
                        document.getElementById("totalSubClauseCount_"+counter).value = totalSubClauseCount+1;

                        var c = ((subClauseCount)*2)+1;
                        //alert("c:" + c);
                        var eleSubClause = "";
                        var td1_2="";
                        var td1_3="";
                    <%if(edit){%>
                            if($jq("#hdClauseId_"+counter).val() != undefined){
                                eleSubClause = "&nbsp;&nbsp;<a href=\"javascript:void(0);\" class=\"action-button-edit1\" onclick=\"saveSubClause('" + counter + "_" + (subClauseCount+1) + "')\">Save Sub Clause</a>";
                                eleSubClause = eleSubClause + "&nbsp;&nbsp;<a href=\"javascript:void(0);\" class=\"action-button-delete1\" onclick=\"deleteSubClause('" + counter + "_" + (subClauseCount+1) + "')\">Remove Sub Clause</a>";
                            }
                    <%}else{%>
                            td1_2="<td style=\"text-align: center\">" +
                                            "<input type=\"checkbox\" name=\"chkSelSubClause_"+counter+"_"+(subClauseCount+1)+
                                            "\" id=\"chkSelSubClause_"+counter+"_"+(subClauseCount+1)+"\" value=\"0\" /></td>";
                            td1_3="<td style=\"text-align: center;\">&nbsp;</td>";
                     <%}%>

                        var subClauseTr = "<tr id=\"tr_" + counter + "_"+ (c+1) +"\">" + td1_2 + "<td>" +
                                            "Sub Clause No. and Sub Clause Description</td><td>" +
                                            "<textarea name=\"subSectionSubClause_" + counter + "_" +(subClauseCount+1)+
                                            "\" id=\"subSectionSubClause_" + counter + "_"+(subClauseCount+1)+"\" " +
                                            "cols=\"100\" rows=\"6\" ></textarea></td></tr><tr id=\"tr_" + counter + "_" + (c+2) + "\">" +
                                            td1_3 + "<td><%=contentType1%> Applicable</td><td><input type=\"checkbox\" " +
                                            "name=\"chkTDCApplicable_" + counter + "_"+(subClauseCount+1)+"\" id=\"chkTDCApplicable_" +
                                            counter + "_"+(subClauseCount+1)+"\" value=\"0\" />" + eleSubClause + "</td></tr>";
                        if(procType == 'srvindi'){
                            subClauseTr = "";
                            subClauseTr = "<tr id=\"tr_" + counter + "_"+ (c+1) +"\">" + td1_2 + "<td>" +
                                            "Sub Clause No. and Sub Clause Description</td><td>" +
                                            "<textarea name=\"subSectionSubClause_" + counter + "_" +(subClauseCount+1)+
                                            "\" id=\"subSectionSubClause_" + counter + "_"+(subClauseCount+1)+"\" " +
                                            "cols=\"100\" rows=\"6\" ></textarea></td></tr><tr id=\"tr_" + counter + "_" + (c+2) + "\" >" +
                                            td1_3 + "<td><label style=\"display:none\" ><%=contentType1%> Applicable</label></td><td><input type=\"checkbox\" style=\"display:none\" " +
                                            "name=\"chkTDCApplicable_" + counter + "_"+(subClauseCount+1)+"\" id=\"chkTDCApplicable_" +
                                            counter + "_"+(subClauseCount+1)+"\" value=\"0\" />" + eleSubClause + "</td></tr>";
                        }

                        $jq("#tr_" + (counter) + "_" + c).after(subClauseTr);
                        //alert(">"+ "subSectionSubClause_" + counter + "_"+(subClauseCount+1));
                        //alert(">>" + $jq("#subSectionSubClause_" + counter + "_"+(subClauseCount+1)).attr("id"));
                        CKEDITOR.replace($jq("#subSectionSubClause_" + counter + "_"+(subClauseCount+1)).attr("id"),
                        {
                            toolbar : 'egpToolbar'
                        });
                        eleSubClause = "";
                    <%//if(!edit){%>
                    }
                    <%//}
                      //if(!edit){%>
                });
                <%//}%>
                //alert('addSubClause end');
                }

            function saveClause(counter){
                //alert('saveClause');
                $jq("#subSectionClause_"+counter).val($jq.trim($jq("#subSectionClause_"+counter).val()));
                var clauseName = $jq("#subSectionClause_"+counter).val();
                var cntr = 0;
                $jq(".dynamicError").remove();
                if(clauseName == ""){
                    $jq("#subSectionClause_" + counter).parent().append("<span class='dynamicError' style='color:red'>Please enter Clause Details</span>");
                    $jq("#subSectionClause_" + counter).focus();
                    //return false;
                    cntr = 1;
                }

                var c = 0;
                var subClauseName = new Array();
                var isTDSAppl = new Array();

                $jq('textarea[id^=subSectionSubClause_'+counter+'_]').each(function(){
                    //if($jq(this).val() == ""){
                    if($jq.trim(CKEDITOR.instances[$jq(this).attr("id")].getData()) == ""){
                        $jq(this).parent().append("<span class='dynamicError' style='color:red'>Please enter  Sub Clause No. and Sub Clause Description</span>");
                        cntr++;
                    }
                    subClauseName[c] = $jq.trim(CKEDITOR.instances[$jq(this).attr("id")].getData());
                    var isTDSApplId = "chkTDCApplicable_" + $jq(this).attr("id").split("_")[1] + "_" + $jq(this).attr("id").split("_")[2];
                    isTDSAppl[c++] = $jq("#" + isTDSApplId).attr("checked");
                });
                if(cntr > 0){
                    return false;
                }
                //data = new Array()
                var data = "";//new Array()

                var cnt = 0;
                //data[cnt++] = "'action':'saveClause'";
                data = "action=saveClause&";
                data = data + "ittHeaderId=<%=ittHeaderId%>&";
                //cnt++;

                //data[cnt++] = "'clauseName':'" + clauseName + "'";
                data = data + "clauseName=" + clauseName + "&";
                //cnt++;

                //data = "{'action': 'saveClause', 'clauseName':'"+ clauseName + "',";

                for(var i=0; i < subClauseName.length; i++){
                    data = data + "subClauseName_" + (i+1) + "=" + replaceAll(subClauseName[i], "&", "%26") + "&";

                    data = data + "isTDSAppl_" + (i+1) + "=" + isTDSAppl[i] + "&";
                }

                //alert("----->"+data+"<-----");

                $jq.ajax({
                   url: "<%=request.getContextPath()%>/SectionClauseSrBean",
                   data: data,
                   context: document.body,
                   type: 'GET',
                   success: function(msg){
                       //alert(msg);
                        if(msg.split(",")[0] == "true"){
                            jAlert("Clause saved successfully", "Saved Successfully",function(){
                                $jq.blockUI({ message: '<h1><img src="<%=request.getContextPath()%>/resources/images/loading.gif"/> Just a moment...</h1>' });
                                window.location.reload();
                            });
                        }else{
                            jAlert("error:" + msg, "ERROR");
                        }
                   }
                });
            }

            function updateClause(counter){
                //alert('updateClause');
                var clauseInfo = $jq("#subSectionClause_" + counter).val();

                $jq(".dynamicError").remove();
                if(clauseInfo == ""){
                    $jq("#subSectionClause_" + counter).parent().append("<span class='dynamicError' style='color:red'>Please enter Clause Details</span>");
                    $jq("#subSectionClause_" + counter).focus();
                    return false;
                }

                var clauseId = $jq("#hdClauseId_" + counter).val();

                $jq.ajax({
                    url: "<%=request.getContextPath()%>/SectionClauseSrBean",
                    data: { "action":"updateClause",
                            "headerId": <%=ittHeaderId%>,
                            "clauseInfo": clauseInfo,
                            "clauseId": clauseId},
                    context: document.body,
                    type: 'POST',
                    success: function(msg){
                        if(msg == "true"){
                            jAlert("Clause updated successfully", "Success");
                        }else{
                            jAlert("error:" + msg, "ERROR");
                        }
                    }
                });
            }

            function deleteSubClause(counter){
                //alert('deleteSubClause');
                                var id_SubClause = "";
//                $("tr[id=row_" + customerId + "]").remove();

                                $jq("tr:[id^=tr_"+counter.split("_")[0]+"_]").each(function(){
                                    id_SubClause = $jq(this).attr("id");
                                });

                //alert(">>" + id_SubClause);
                                if(id_SubClause.split("_")[2] == 3){
                                    jAlert("At least 1 Sub Clause is required.", "Error");
                                    return false;
                }

                jConfirm("Do you really want to remove this sub Clause?", "Delete Sub Clause", function(result){
                    if(result){
                        try{
                            if($jq("#hdClauseId_" + counter.split("_")[0]).val() == undefined){
                                //clause is not saved yet.
                                // remove last subclause
                                // Commented below code for the bug #1456 And move to top of the function
//                                var id_SubClause = "";
//                                $jq("tr:[id^=tr_"+counter.split("_")[0]+"_]").each(function(){
//                                    id_SubClause = $jq(this).attr("id");
//                                });
//                                //alert(">>" + id_SubClause);
//                                if(id_SubClause.split("_")[2] == 3){
//                                    jAlert("At least 1 Sub Clause is required.", "Error");
//                                    return false;
//                                } else {
                                    var subRowId = id_SubClause.split("_")[2];
                                    subRowId = (subRowId*1)-1;
                                    var instanceTblH = CKEDITOR.instances["subSectionSubClause_"+counter];
                                    if(instanceTblH)
                                    {
                                        CKEDITOR.remove(instanceTblH);
                                    }
                                    $jq("#"+id_SubClause.split("_")[0]+"_"+id_SubClause.split("_")[1]+"_"+subRowId).remove();
                                    $jq("#"+id_SubClause).remove();
                                    
                                    jAlert("Sub Clause removed successfully ", "Removed");
                                    var subClauseCount = (document.getElementById("subClauseCount_"+counter.split("_")[0]).value)*1;
                                    document.getElementById("subClauseCount_"+counter.split("_")[0]).value = subClauseCount-1;
//                                }
                                return false;
                            }
                        }catch(err){
                            // DO NOTHING
                        }

                        if($jq("#hdSubClauseId_"+counter).val() == undefined){
                            var id = $jq("#subSectionSubClause_"+counter).parent().parent().next().attr("id");
                            var instanceTblH = CKEDITOR.instances["subSectionSubClause_"+counter];
                            if(instanceTblH)
                            {
                                CKEDITOR.remove(instanceTblH);
                            }
                            $jq("#"+id).remove();
                            $jq("#"+id.split("_")[0]+"_"+id.split("_")[1]+"_"+((id.split("_")[2]*1)-1)).remove();
                            jAlert("Sub Clause removed successfully ", "Removed");

                            var subClauseCount = (document.getElementById("subClauseCount_"+counter.split("_")[0]).value)*1;
                            document.getElementById("subClauseCount_"+counter.split("_")[0]).value = subClauseCount-1;

                            return false;
                        }
                        var eleCheck = counter.split("_")[1];
                        if(eleCheck == "1"){
                            if($jq("#hdSubClauseId_"+counter.split("_")[0] + "_2").val() == undefined){
                                jAlert("At least 1 Sub Clause is required", "Error");
                                return false;
                            }
                        }

                        var subClauseId = $jq("#hdSubClauseId_"+counter).val();
                        var clauseId = $jq("#hdClauseId_"+counter.split("_")[0]).val();
                        $jq.ajax({
                            url: "<%=request.getContextPath()%>/SectionClauseSrBean",
                            data: {"action": "removeSubClause",
                                    "clauseId" : clauseId,
                                    "ittSubClauseId": subClauseId
                            },
                            context: document.body,
                            type: 'POST',
                            success: function(msg){
                                if(msg == "true"){
                                    //alert(document.getElementById(id_SubClause).nodeName);
                                    //alert(document.getElementById(id_SubClause).rowIndex);
                                    if(document.getElementById(id_SubClause).nodeName == 'TR'){
                                        var currentRowIdx = document.getElementById(id_SubClause).rowIndex;
                                        tableObj = document.getElementById(id_SubClause).parentNode;
                                        tableObj.deleteRow(currentRowIdx);
                                        tableObj.deleteRow(currentRowIdx-1);
                                    }
                                    jAlert("Sub Clause removed successfully ", "Removed",function(){
                                        //$jq.blockUI({ message: '<h1><img src="<%=request.getContextPath()%>/resources/images/loading.gif"/> Just a moment...</h1>' });
                                        //window.location.reload();
                                    });
                                }else{
                                    jAlert("error:" + msg, "ERROR");
                                }
                            }
                        });
                    }
                });
            }

            function updateSubClause(counter){
            //alert('updateSubClause');
                var subClauseId = $jq("#hdSubClauseId_"+counter).val();
                //var subClauseName = $("#subSectionSubClause_"+counter).val();
                var subClauseName = $jq.trim(CKEDITOR.instances["subSectionSubClause_"+counter].getData());
                $jq(".dynamicError").remove();
                if(subClauseName == ""){
                    $jq("#subSectionSubClause_"+counter).parent().append("<span class='dynamicError' style='color:red'>Please enter Sub Clause Details.</span>");
                    $jq("#subSectionSubClause_"+counter).focus();
                    return false;
                }
                var isTdsApplicable = $jq("#chkTDCApplicable_"+counter).attr("checked")
                var clauseId = $jq("#hdClauseId_"+counter.split("_")[0]).val();

                $jq.ajax({
                    url: "<%=request.getContextPath()%>/SectionClauseSrBean",
                    data: {"action": "updateSubClause",
                            "clauseId": clauseId,
                            "subclauseName": subClauseName,
                            "subClauseId": subClauseId,
                            "isTdsApplicable": isTdsApplicable
                    },
                    context: document.body,
                    type: 'POST',
                    success: function(msg){
                        if(msg == "true"){
                            jAlert("Sub Clause updated successfully", "Success");
                        }else{
                            jAlert("error:" + msg, "ERROR");
                        }
                    }
                });
            }

            function saveSubClause(counter){
                //alert('saveSubClause');
                $jq(".dynamicError").remove();
                //var subClauseName = $("#subSectionSubClause_"+counter).val();
                var subClauseName = $jq.trim(CKEDITOR.instances["subSectionSubClause_"+counter].getData());
                if(subClauseName == ""){
                    $jq("#subSectionSubClause_"+counter).parent().append("<span class='dynamicError' style='color:red'>Please enter Sub Clause Details</span>");
                    $jq("#subSectionSubClause_"+counter).focus();
                    return false;
                }
                var isTdsApplicable = $jq("#chkTDCApplicable_"+counter).attr("checked");
                var clauseId = $jq("#hdClauseId_"+counter.split("_")[0]).val();
                $jq.ajax({
                    url: "<%=request.getContextPath()%>/SectionClauseSrBean",
                    data: {"action": "saveSubClause",
                            "clauseId": clauseId,
                            "subclauseName": subClauseName,
                            "isTdsApplicable": isTdsApplicable
                    },
                    context: document.body,
                    type: 'POST',
                    success: function(msg){
                        if((msg.split(",")[0]) == "true"){
                            jAlert("Sub Clause saved successfully", "Saved");
                            /*jAlert("Sub Clause saved successfully", "Saved",function(){
                                $jq.blockUI({ message: '<h1><img src="<%=request.getContextPath()%>/resources/images/loading.gif"/> Just a moment...</h1>' });
                                window.location.reload();
                            });
                            */
                            var subClauseId = msg.split(",")[1];
                            var updateCellData = ' <input value="0" name="chkTDCApplicable_'+counter+'" id="chkTDCApplicable_'+counter+'" type="checkbox">'+
                            ' <a href="javascript:void(0);" class="action-button-edit1" onclick="updateSubClause(\''+counter+'\')">Update Sub Clause Information</a>' +
                            ' <a href="javascript:void(0);" class="action-button-delete1" onclick="deleteSubClause(\''+counter+'\')">Remove Sub Clause</a>'+
                            ' <input name="hdSubClauseId_'+counter+'" id="hdSubClauseId_'+counter+'" value="'+subClauseId+'" type="hidden">';

                            if(document.getElementById("chkTDCApplicable_"+counter).parentNode.nodeName == 'TD')
                            {
                                document.getElementById("chkTDCApplicable_"+counter).parentNode.innerHTML = updateCellData;
                            }
                        }else{
                            jAlert("error:" + msg, "ERROR");
                        }
                    }
                });
            }

            function deleteClause(counter){
                //alert('deleteClause');
                jConfirm("Do you really want to remove this clause?", "Confirm Delete", function(result){
                    if(result){
                        if($jq("#hdClauseId_" + counter).val() == undefined){
                        //clause is not saved yet.
                            //alert('length '+$jq("tr:[id^=tr_"+counter+"_]").length);
                            var currentClauseTRLength = $jq("tr:[id^=tr_"+counter+"_]").length;
                            if( currentClauseTRLength > 0)
                            {
                                for(var iCnt = 1;iCnt <= currentClauseTRLength;iCnt++)
                                {
                                    if($jq("#subSectionSubClause_"+counter+"_"+iCnt).attr("id"))
                                    {
                                        var id = $jq("#subSectionSubClause_"+counter+"_"+iCnt).attr("id");
                                        var instances = CKEDITOR.instances[id];
                                        if(instances)
                                        {
                                            instances.destroy();
                                            //CKEDITOR.remove(instances);
                                        }
                                    }
                                }
                            }
                        $jq("tr:[id^=tr_"+counter+"_]").remove();
                        return false;
                    }
                    var clauseId = $jq("#hdClauseId_" + counter).val();
                    $jq.ajax({
                        url: "<%=request.getContextPath()%>/SectionClauseSrBean",
                        data: {"action": "removeClause",
                                "clauseId": clauseId,
                                "ittHeaderId": <%=ittHeaderId%>
                        },
                        context: document.body,
                        type: 'POST',
                        success: function(msg){
                            if(msg == "true"){
                                    var currentClauseTRLength = $jq("tr:[id^=tr_"+counter+"_]").length;
                                    if( currentClauseTRLength > 0)
                                    {
                                        for(var iCnt = 1;iCnt <= currentClauseTRLength;iCnt++)
                                        {
                                            if($jq("#subSectionSubClause_"+counter+"_"+iCnt).attr("id"))
                                            {
                                                var id = $jq("#subSectionSubClause_"+counter+"_"+iCnt).attr("id");
                                                var instances = CKEDITOR.instances[id];
                                                if(instances)
                                                {
                                                    instances.destroy();
                                                    //CKEDITOR.remove(instances);
                                                }
                                            }
                                        }
                                    }
                                $jq("tr:[id^=tr_"+counter+"_]").remove();

                                jAlert("Clause removed successfully", "Removed",function(){
                                    //$jq.blockUI({ message: '<h1><img src="<%=request.getContextPath()%>/resources/images/loading.gif"/> Just a moment...</h1>' });
                                    //window.location.reload();
                                });
                            }else{
                                jAlert("error:" + msg, "ERROR");
                            }
                        }
                    });
                    }
                });
            }

            $jq(document).ready(function(){
                $jq("#addClause").bind("click", function(){
                    section_addClause();
                });
                $jq("#addClause1").bind("click", function(){
                    section_addClause();
                });
                $jq("#remove").bind("click", function(){
                    //alert('#remove');
                    section_remove();
                });
                $jq("#remove1").bind("click", function(){
                   // alert('#remove1');
                    section_remove();
                });

                /*$jq('textarea[name^="subSectionSubClause_"]').each(function(){
                    //$("#"+$(this).attr("id")).ckeditor(config);
                    CKEDITOR.replace($jq(this).attr("id"),
                    {
                        toolbar : 'egpToolbar'
                    });
                });*/
            });

            function section_addClause(){
                //alert('section_addClause');
                var lastTrId = "tr_1_1";
                $jq('tr[id^="tr_"]').each(function(){
                    lastTrId = $jq(this).attr("id");
                });

                //var lastTrId = $jq("#clauseData tr:last-child").attr("id");
                //alert("lastTrId:" + lastTrId)
                var clauseNo = ((lastTrId.split("_")[1]) * 1) + 1;
                //alert("clauseNo:" + clauseNo);
                //var subClauseNo = lastTrId.split("_")[2];

                var eleClause = "";
                var eleSubClause = ""
                var td1_1 = "";
                var td1_2 = "";
                var td1_3 = "";
                <%

                if(edit){
                    %>
                    eleClause = "&nbsp;&nbsp;<a href=\"javascript:void(0);\" class=\"action-button-edit1\" onclick=\"saveClause('" + clauseNo + "')\">Save Clause</a>";
                    eleClause = eleClause + "&nbsp;&nbsp;<a class=\"action-button-delete1\" href=\"javascript:void(0);\" onclick=\"deleteClause('" + clauseNo + "')\">Remove Clause</a>";
                    eleClause = eleClause + "&nbsp;&nbsp;<a class=\"action-button-delete1\" href=\"javascript:void(0);\" onclick=\"deleteSubClause('" + clauseNo + "_1')\">Remove Sub Clause</a>";

                    //eleSubClause = "&nbsp;&nbsp;<a href=\"javascript:void(0);\" class=\"action-button-edit1\" onclick=\"saveSubClause('" + clauseNo + "_1')\">Save Sub Clause</a>";
                    //eleSubClause = eleSubClause + "&nbsp;&nbsp;<a href=\"javascript:void(0);\" class=\"action-button-delete\" onclick=\"removeSubClause('" + clauseNo + "_1')\">Remove Sub Clause</a>";
                    <%
                }else{
                    %>
                    td1_1="<td style=\"text-align: center;\">" +
                                "<input type=\"checkbox\" name=\"chkSelClause_"+clauseNo+"\" id=\"chkSelClause_"+clauseNo+"\" /> " +
                                "</td>";
                    td1_2="<td style=\"text-align: center\">" +
                                "<input type=\"checkbox\" name=\"chkSelSubClause_"+clauseNo+"_1\" id=\"chkSelSubClause_"+clauseNo+"_1\" value=\"0\" /></td>";
                    td1_3="<td style=\"text-align: center;\">" +
                                "&nbsp;</td>";
                    <%
                }
                %>

                var clauseTr = "<tr id=\"tr_" + clauseNo + "_1\">" + td1_1 + "<td>Clause No. and Clause Description</td><td valign=\"top\">" +
                                "<div><textarea name=\"subSectionClause_" + clauseNo + "\" id=\"subSectionClause_" + clauseNo + "\" cols=\"100\" >"+
                                "</textarea><div><a id=\"addSubClause_" + clauseNo + "\" class='action-button-add1' " +
                                "onclick=\"addSubClause(this)\" href=\"javascript:void(0);\">Add Sub Clause</a>" + eleClause + "</div>" +
                                "<input type=\"hidden\" name=\"subClauseCount_" + clauseNo + "\" id=\"subClauseCount_" + clauseNo + "\" value=\"1\" />" +
                                "<input type=\"hidden\" name=\"totalSubClauseCount_" + clauseNo + "\" id=\"totalSubClauseCount_" + clauseNo + "\" value=\"1\" /></td></tr> " +
                                "<tr id=\"tr_" + clauseNo + "_2\">" + td1_2 + "<td>" +
                                "Sub Clause No. and Sub Clause Description</td><td>" +
                                "<textarea name=\"subSectionSubClause_" + clauseNo + "_1\" id=\"subSectionSubClause_" + clauseNo + "_1\" " +
                                "cols=\"100\" rows=\"6\" ></textarea></td></tr><tr id=\"tr_" + clauseNo + "_3\">" + td1_3 +
                                "<td><%=contentType1%> Applicable</td><td><input type=\"checkbox\" " +
                                "name=\"chkTDCApplicable_" + clauseNo + "_1\" value=\"0\" id=\"chkTDCApplicable_" + clauseNo + "_1\" />" + eleSubClause + "</td></tr>";

               if(procType == 'srvindi'){
                    clauseTr = "";
                    clauseTr = "<tr id=\"tr_" + clauseNo + "_1\">" + td1_1 + "<td>Clause No. and Clause Description</td><td valign=\"top\">" +
                            "<div><textarea name=\"subSectionClause_" + clauseNo + "\" id=\"subSectionClause_" + clauseNo + "\" cols=\"100\" >"+
                            "</textarea><div><a id=\"addSubClause_" + clauseNo + "\" class='action-button-add1' " +
                            "onclick=\"addSubClause(this)\" href=\"javascript:void(0);\">Add Sub Clause</a>" + eleClause + "</div>" +
                            "<input type=\"hidden\" name=\"subClauseCount_" + clauseNo + "\" id=\"subClauseCount_" + clauseNo + "\" value=\"1\" />" +
                            "<input type=\"hidden\" name=\"totalSubClauseCount_" + clauseNo + "\" id=\"totalSubClauseCount_" + clauseNo + "\" value=\"1\" /></td></tr> " +
                            "<tr id=\"tr_" + clauseNo + "_2\">" + td1_2 + "<td>" +
                            "Sub Clause No. and Sub Clause Description</td><td>" +
                            "<textarea name=\"subSectionSubClause_" + clauseNo + "_1\" id=\"subSectionSubClause_" + clauseNo + "_1\" " +
                            "cols=\"100\" rows=\"6\" ></textarea></td></tr><tr id=\"tr_" + clauseNo + "_3\" style=\"display:none\" >" + td1_3 +
                            "<td><%=contentType1%> Applicable</td><td><input type=\"checkbox\" " +
                            "name=\"chkTDCApplicable_" + clauseNo + "_1\" value=\"0\" id=\"chkTDCApplicable_" + clauseNo + "_1\" />" + eleSubClause + "</td></tr>";
               }

               $jq("#clauseData").append(clauseTr);
               CKEDITOR.replace($jq("#subSectionSubClause_" + clauseNo + "_1").attr("id"),
               {
                   toolbar : 'egpToolbar'
               });
               $jq("#clauseCount").val(($jq("#clauseCount").val()*1)+1);
               $jq("#totalClauseCount").val(($jq("#totalClauseCount").val()*1)+1);
               eleClause = "";
               eleSubClause = "";
            }

            function section_remove(){
                //alert('section_remove');
                clauseCounterNo = new Array($jq("#clauseCount").val());
                var i=0;
                $jq('[type=checkbox]').each(function(){
                    var id = $jq(this).attr("id");
                    if(id.indexOf("chkTDCApplicable") < 0){
                        if(id.indexOf("chkSelClause") >= 0){
                            if($jq(this).attr("checked") == true){
                                //alert("Remove Clause with Id===" + id.split("_")[1]);
                                var c1 = id.split("_")[1];
                                clauseCounterNo[i]=c1;
                                removeClause(c1);
                                //alert('remove clause');
                            }
                        }else if(id.indexOf("chkSelSubClause") >= 0){
                            if($jq(this).attr("checked") == true){
                                //alert("Remove Clause with Id===" + id.split("_")[1]);
                                var c1 = id.split("_")[1];
                                var c2 = id.split("_")[2];
                                clauseCounterNo[i]=c1;
                                //alert('remove sub clause');
                                removeSubClause(c1,c2);
                        }
                            }
                    }
                    i++;
                });
                //renameClause(clauseCounterNo);
            }
        </script>
    </head>
    <body>

        <div class="mainDiv">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="fixDiv">
                <!--Middle Content Table Start-->

                <table width="100%" border="0" cellspacing="0" cellpadding="0" >
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1">Prepare <%=contentType%></div>
                            </div>
                            <table width="100%"  border="0" cellspacing="0" cellpadding="0"style="color: #333;font-weight: bold;" class="t_space t-align-right">
                                <tr>
                                    <td align="left">
                                        <a  class="action-button-goback"  href="subSectionDashBoard.jsp?templateId=<%=request.getParameter("templateId")%>&sectionId=<%=request.getParameter("sectionId") %>">
                                Go back to <%=contentType%> Dashboard</a>

                                    </td>
                                    <td align="left">
                                    </td>
                                </tr>
                            </table>
                  <table width="100%" border="0" cellspacing="0" class="tableView_1 t_space">
                    <tr>
                        <td align="left">
                             <a class="action-button-goback" href="DefineSTDInDtl.jsp?templateId=<%=request.getParameter("templateId")%>">
                                Go back to SBD Dashboard</a>
                        </td>
                        <td align="right">
                            <a id="addClause"  class="action-button-add" href="javascript:void(0);">Add Clause</a>
                            <%
                            if(!edit){
                            %>
                            <a id="remove" class="action-button-delete" href="javascript:void(0);">Remove</a>
                            <%
                            }else{
                            %>
                            &nbsp;&nbsp;
                            <%
                            }
                            %>
                        </td>
                    </tr>
                        <tr><td colspan="2">&nbsp;</td></tr>
                        <tr>
                            <td style="font-style: italic" colspan="2" class="t-align-left formNoteTxt">Fields marked with (<span style="color:red;">*</span>) are mandatory</td>
                        </tr>
                </table>
                            <form action="<%=request.getContextPath()%>/SectionClauseSrBean" method="post"  onsubmit="return validate();">
                                <%--/PrepareTDS.jsp--%>
                                <input type="hidden" name="clauseCount" id="clauseCount" value="1" />
                                <input type="hidden" name="totalClauseCount" id="totalClauseCount" value="1" />
                                <input type="hidden" name="ittHeaderId" id="ittHeaderId" value="<%=ittHeaderId%>" />
                                <input type="hidden" name="sectionId" id="sectionId" value="<%=sectionId%>" />
                                <input type="hidden" name="templateId" id="templateId" value="<%=request.getParameter("templateId")%>" />

                                <%
                                if(!edit){
                                %>
                                <input type="hidden" name="subClauseCount_1" id="subClauseCount_1" value="1" />
                                <input type="hidden" name="totalSubClauseCount_1" id="totalSubClauseCount_1" value="1" />
                                <table id="clauseData" width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr id="tr_0">
                                        <th>Select</th>
                                        <th>Sub Section Name</th>
                                        <th><%=subSectionName%><span style="color:red;">*</span></th>
                                    </tr>
                                    <tr id="tr_1_1">
                                        <td style="text-align: center;">
                                            <input type="checkbox" name="chkSelClause_1" id="chkSelClause_1" />
                                        </td>
                                        <td>
                                            Clause No. and Clause Description
                                        </td>
                                        <td valign="top">
                                            <textarea name="subSectionClause_1" id="subSectionClause_1" cols="100" ></textarea>
                                            <div>
                                            <a href="javascript:void(0);"  class="action-button-add1" onclick="addSubClause(this)" id="addSubClause_1">Add Sub Clause</a>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id="tr_1_2">
                                        <td style="text-align: center">
                                            <input type="checkbox" value="0" name="chkSelSubClause_1_1" id="chkSelSubClause_1_1" />
                                        </td>
                                        <td>
                                            Sub Clause No. and Sub Clause Description
                                        </td>
                                        <td>
                                            <textarea name="subSectionSubClause_1_1" id="subSectionSubClause_1_1" cols="100" rows="6" ></textarea>
                                             <script type="text/javascript">
                                                CKEDITOR.replace('subSectionSubClause_1_1',
                                                {
                                                    toolbar : 'egpToolbar'
                                                });
                                            </script>
                                        </td>
                                    </tr>
                                        <tr id="tr_1_3" <%if("srvindi".equals(procType)){out.print(" style=\"display:none\" ");}else{}%> >
                                        <td style="text-align: center;">
                                            &nbsp;
                                        </td>
                                        <td>
                                            <%=contentType1%> Applicable

                                        </td>
                                        <td>
                                            <input type="checkbox" value="0" name="chkTDCApplicable_1_1" id="chkTDCApplicable_1_1" />
                                        </td>
                                    </tr>
                                </table>
                                            <div class="t-align-right t_space">
                                            <a id="addClause1"  class="action-button-add" href="javascript:void(0);">Add Clause</a>
                                            <%
                                            if(!edit){
                                            %>
                                            <a id="remove1" class="action-button-delete" href="javascript:void(0);">Remove</a>
                                            <%
                                            }else{
                                            %>
                                            &nbsp;&nbsp;
                                            <%
                                            }
                                            %>
                                            </div>
                                <div align="center">
                                    <label class="formBtn_1">
                                        <input type="submit" name="button" id="button" value="Submit" /></label>
                                </div>
                                <%
                                }else{
                                %>
                                <table id="clauseData" width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr id="tr_0">
                                        <th>Sub Section Name</th>
                                        <th><%=subSectionName%></th>
                                    </tr>
                                    <%
                                    List<TblIttClause> tblIttClause = sectionClauseSrBean.getClauseDetail(ittHeaderId);
                                    for(int i=0; i<tblIttClause.size(); i++){
                                        int clauseId = tblIttClause.get(i).getIttClauseId();
                                    %>
                                    <tr id="tr_<%=(i+1)%>_1">
                                        <td>
                                            Clause No. and Clause Description
                                        </td>
                                        <td valign="top">
                                            <textarea name="subSectionClause_<%=(i+1)%>" id="subSectionClause_<%=(i+1)%>" cols="100" ><%=tblIttClause.get(i).getIttClauseName()%></textarea>
                                            <div>
                                            <a href="javascript:void(0);" class="action-button-add1" onclick="addSubClause(this)" id="addSubClause_<%=(i+1)%>">Add Sub Clause</a>

                                            &nbsp;&nbsp;
                                            <a href="javascript:void(0);" class="action-button-edit1" onclick="updateClause('<%=(i+1)%>')">Update Clause Information</a>
                                            &nbsp;&nbsp;

                                            <a href="javascript:void(0);" class="action-button-delete1" onclick="deleteClause('<%=(i+1)%>')">Remove Clause</a>

                                            <input type="hidden" name="hdClauseId_<%=(i+1)%>" id="hdClauseId_<%=(i+1)%>" value="<%=tblIttClause.get(i).getIttClauseId()%>" />
                                            </div>

                                        </td>
                                    </tr>
                                    <%
                                        List<TblIttSubClause> tblIttSubClause = sectionClauseSrBean.getSubClauseDetail(clauseId);
                                        int j = 0;
                                        for(j=0;j<tblIttSubClause.size();j++){
                                            tblIttSubClause.get(j).getIttSubClauseId();
                                            tblIttSubClause.get(j).getIttSubClauseName();
                                        %>
                                            <tr id="tr_<%=(i+1)%>_<%=(j+1)*2%>">
                                                <td>
                                                    Sub Clause No. and Sub Clause Description
                                                </td>
                                                <td>
                                                    <textarea name="subSectionSubClause_<%=(i+1)%>_<%=(j+1)%>" id="subSectionSubClause_<%=(i+1)%>_<%=(j+1)%>" cols="100" rows="6" ><%=tblIttSubClause.get(j).getIttSubClauseName()%></textarea>
                                                    <script type="text/javascript">
                                                        CKEDITOR.replace('subSectionSubClause_<%=(i+1)%>_<%=(j+1)%>',
                                                        {
                                                            toolbar : 'egpToolbar'
                                                        });
                                                    </script>
                                                </td>
                                            </tr>

                                            <tr id="tr_<%=(i+1)%>_<%=((j+1)*2)+1%>" <%if("srvindi".equals(procType)){out.print(" style=\"display:none\" ");}else{}%> >
                                                <td>
                                                    <%=contentType1%> Applicable
                                                </td>
                                                <td>
                                                    <input type="checkbox" value="0" name="chkTDCApplicable_<%=(i+1)%>_<%=(j+1)%>" id="chkTDCApplicable_<%=(i+1)%>_<%=(j+1)%>" <%
                                                        if(tblIttSubClause.get(j).getIsTdsApplicable().equalsIgnoreCase("yes")){
                                                            %>checked="ckecked"<%
                                                        }
                                                    %> />
                                                    <a href="javascript:void(0);" class="action-button-edit1" onclick="updateSubClause('<%=(i+1)%>_<%=(j+1)%>')">Update Sub Clause Information</a>
                                                    <a href="javascript:void(0);" class="action-button-delete1" onclick="deleteSubClause('<%=(i+1)%>_<%=(j+1)%>')">Remove Sub Clause</a>
                                                    <input type="hidden" name="hdSubClauseId_<%=(i+1)%>_<%=(j+1)%>" id="hdSubClauseId_<%=(i+1)%>_<%=(j+1)%>" value="<%=tblIttSubClause.get(j).getIttSubClauseId()%>" />

                                                <%
                                                if(tblIttSubClause.size()-1 != j){
                                                    out.print("</td></tr>");
                                                }
                                                %>
                                        <%
                                        }
                                        %>
                                        <input type="hidden" name="subClauseCount_<%=(i+1)%>" id="subClauseCount_<%=(i+1)%>" value="<%=j%>" />
                                        <input type="hidden" name="totalSubClauseCount_<%=(i+1)%>" id="totalSubClauseCount_<%=(i+1)%>" value="<%=j%>" />
                                    </td></tr>
                                        <%
                                    }
                                    %>
                                </table>
                                        <div class="t-align-right t_space">
                                            <a id="addClause1"  class="action-button-add" href="javascript:void(0);">Add Clause</a>
                                            <%
                                            if(!edit){
                                            %>
                                            <a id="remove1" class="action-button-delete" href="javascript:void(0);">Remove</a>
                                            <%
                                            }else{
                                            %>
                                            &nbsp;&nbsp;
                                            <%
                                            }
                                            %>
                                        </div>
                                <%
                                }
                                %>
                            </form>
                      </td>
                    </tr>
                  </table>
                            <!--Page Content End-->

                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<%
createSubSectionSrBean = null;
sectionClauseSrBean = null;
%>
<%
    if(defineSTDInDtlSrBean != null){
        defineSTDInDtlSrBean = null;
    }
%>