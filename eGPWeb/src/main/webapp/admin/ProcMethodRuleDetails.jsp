<%-- 
    Document   : ProcMethodRuleDetails
    Created on : Nov 23, 2010, 11:44:51 AM
    Author     : Naresh.Akurathi
--%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%
            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            Object objUName = session.getAttribute("userName");
            boolean isLoggedIn = false;
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userId").toString();
            }
            if (objUName != null) {
                isLoggedIn = true;
            }
%>
<%@page import="java.util.List"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigProcurement"%>
<%@page import="com.cptu.egp.eps.model.table.TblBudgetType"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTypes"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Procurement Method Business Rule</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <%!    ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();

        %>

        <%
                    StringBuilder budgetType = new StringBuilder();
                    StringBuilder tenderType = new StringBuilder();
                    StringBuilder procNature = new StringBuilder();
                    StringBuilder procType = new StringBuilder();
                    StringBuilder procMethod = new StringBuilder();
                    
                    TblBudgetType budgetTypes2 = new TblBudgetType();
                    Iterator bdit2 = configPreTenderRuleSrBean.getBudgetType().iterator();
                    while (bdit2.hasNext()) {
                        budgetTypes2 = (TblBudgetType) bdit2.next();
                        budgetType.append("<option value='" + budgetTypes2.getBudgetTypeId() + "'>" + budgetTypes2.getBudgetType() + "</option>");
                    }

                    TblTenderTypes tenderTypes2 = new TblTenderTypes();
                    Iterator trit2 = configPreTenderRuleSrBean.getTenderNames().iterator();
                    while (trit2.hasNext()) {
                        tenderTypes2 = (TblTenderTypes) trit2.next();
                        tenderType.append("<option value='" + tenderTypes2.getTenderTypeId() + "'>" + tenderTypes2.getTenderType() + "</option>");
                    }
                    TblProcurementNature tblProcureNature2 = new TblProcurementNature();
                    Iterator pnit2 = configPreTenderRuleSrBean.getProcurementNature().iterator();
                    while (pnit2.hasNext()) {
                        tblProcureNature2 = (TblProcurementNature) pnit2.next();
                        procNature.append("<option value='" + tblProcureNature2.getProcurementNatureId() + "'>" + tblProcureNature2.getProcurementNature() + "</option>");
                    }

                    TblProcurementMethod tblProcurementMethod2 = new TblProcurementMethod();
                    Iterator pmit2 = configPreTenderRuleSrBean.getProcurementMethod().iterator();
                    while (pmit2.hasNext()) {
                        tblProcurementMethod2 = (TblProcurementMethod) pmit2.next();
                        procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>" + tblProcurementMethod2.getProcurementMethod() + "</option>");
                    }

                    TblProcurementTypes tblProcurementTypes2 = new TblProcurementTypes();
                    Iterator ptit2 = configPreTenderRuleSrBean.getProcurementTypes().iterator();
                    while (ptit2.hasNext()) {
                        tblProcurementTypes2 = (TblProcurementTypes) ptit2.next();
                        procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + tblProcurementTypes2.getProcurementType() + "</option>");
                    }
                    String msg = "";
                    configPreTenderRuleSrBean.setLogUserId(strUserTypeId);

                    if ("Submit".equals(request.getParameter("button")) || "Update".equals(request.getParameter("button"))) {

                        int row = Integer.parseInt(request.getParameter("TotRule"));

                        if ("edit".equals(request.getParameter("action"))) {
                            msg = "Not Deleted";
                        } else {
                            //msg = configPreTenderRuleSrBean.delAllConfigProcurement();
                        }

                        if (msg.equals("Deleted")) {
                             //Add/Edit/Remove Procurement Method Rules
                            String action = "Add Procurement Method Rules";
                            try{
                                for (int i = 1; i <= row; i++) {
                                    if (request.getParameter("budgetType" + i) != null) {
                                        byte btype = Byte.parseByte(request.getParameter("budgetType" + i));
                                        byte ttype = Byte.parseByte(request.getParameter("tenderType" + i));
                                        byte pnature = Byte.parseByte(request.getParameter("procNature" + i));
                                        byte pmethod = Byte.parseByte(request.getParameter("procMethod" + i));
                                        byte ptypes = Byte.parseByte(request.getParameter("procType" + i));
                                        BigDecimal minvalue = new BigDecimal(request.getParameter("minValue" + i));
                                        BigDecimal maxvalue = new BigDecimal(request.getParameter("maxValue" + i));
                                        short minsubdays = Short.parseShort(request.getParameter("minNoDays" + i));
                                        short maxsubdays = Short.parseShort(request.getParameter("maxNoDays" + i));

                                        TblConfigProcurement configProcurement = new TblConfigProcurement();
                                        configProcurement.setTblBudgetType(new TblBudgetType(btype));
                                        configProcurement.setTblTenderTypes(new TblTenderTypes(ttype));
                                        configProcurement.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                        configProcurement.setTblProcurementTypes(new TblProcurementTypes(ptypes));
                                        configProcurement.setTblProcurementNature(new TblProcurementNature(pnature));
                                        configProcurement.setIsNationalDisaster("Normal");
                                        configProcurement.setIsUrgency(request.getParameter("urgency" + i));
                                        configProcurement.setMinValue(minvalue);
                                        configProcurement.setMaxValue(maxvalue);
                                        configProcurement.setMinSubDays(minsubdays);
                                        configProcurement.setMaxSubDays(maxsubdays);
                                        
                                        msg = configPreTenderRuleSrBean.addConfigProcurement(configProcurement);
                                    }
                                }
                            }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                            }finally{
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                            }   action=null;
                        } else {
                            
                            String action = "Edit Procurement Method Rules";
                            try{
                                for (int i = 1; i <= row; i++) {
                                    if (request.getParameter("budgetType" + i) != null) {
                                        byte btype = Byte.parseByte(request.getParameter("budgetType" + i));
                                        byte ttype = Byte.parseByte(request.getParameter("tenderType" + i));
                                        byte pnature = Byte.parseByte(request.getParameter("procNature" + i));
                                        byte pmethod = Byte.parseByte(request.getParameter("procMethod" + i));
                                        byte ptypes = Byte.parseByte(request.getParameter("procType" + i));
                                        BigDecimal minvalue = new BigDecimal(request.getParameter("minValue" + i));
                                        BigDecimal maxvalue = new BigDecimal(request.getParameter("maxValue" + i));
                                        short minsubdays = Short.parseShort(request.getParameter("minNoDays" + i));
                                        short maxsubdays = Short.parseShort(request.getParameter("maxNoDays" + i));
                                        
                                        TblConfigProcurement configProcurement = new TblConfigProcurement();
                                        configProcurement.setConfigProcurementId(Integer.parseInt(request.getParameter("id")));
                                        configProcurement.setTblBudgetType(new TblBudgetType(btype));
                                        configProcurement.setTblTenderTypes(new TblTenderTypes(ttype));
                                        configProcurement.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                        configProcurement.setTblProcurementTypes(new TblProcurementTypes(ptypes));
                                        configProcurement.setTblProcurementNature(new TblProcurementNature(pnature));
                                        configProcurement.setIsNationalDisaster("Normal");
                                        configProcurement.setIsUrgency(request.getParameter("urgency" + i));
                                        configProcurement.setMinValue(minvalue);
                                        configProcurement.setMaxValue(maxvalue);
                                        configProcurement.setMinSubDays(minsubdays);
                                        configProcurement.setMaxSubDays(maxsubdays);
                                        
                                        msg = configPreTenderRuleSrBean.updateConfigProcurement(configProcurement);
                                    }
                                 }
                            }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                            }finally{
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                                action=null;
                            }
                         }


                        if (msg.equals("Values Added")) {
                            msg = "Procurement Method Business Rule Configured successfully";
                            response.sendRedirect("ProcMethodRuleView.jsp?msg=" + msg);
                        }

                        if (msg.equals("Updated")) {
                            msg = "Procurement Method Business Rule Updated successfully";
                            response.sendRedirect("ProcMethodRuleView.jsp?msg=" + msg);
                        }

                    }

        %>


        <script type="text/javascript">

            var counter = 1;
            var delCnt = 0;
            $(function() {
                $('#linkAddRule').click(function() {
                    counter = eval(document.getElementById("TotRule").value);
                    delCnt = eval(document.getElementById("delrow").value);

                    $('span.#lotMsg').css("visibility","collapse");
                    var htmlEle = "<tr id='trrule_"+ (counter+1) +"'>"+
                        "<td class='t-align-center'><input type='checkbox' name='checkbox"+(counter+1)+"' id='checkbox_"+(counter+1)+"' value='"+(counter+1)+"' style='width:90%;'/></td>"+
                        "<td class='t-align-center'><select name='budgetType"+(counter+1)+"' class='formTxtBox_1' id='budgetType_"+(counter+1)    +"'><%=budgetType%></select></td>"+
                        "<td class='t-align-center'><select name='tenderType"+(counter+1)+"' class='formTxtBox_1' id='tenderType_"+(counter+1)    +"'><%=tenderType%></select></td>"+
                        "<td class='t-align-center'><select name='procMethod"+(counter+1)+"' class='formTxtBox_1' id='procMethod_"+(counter+1)    +"'><%=procMethod%></select></td>"+
                        "<td class='t-align-center'><select name='procType"+(counter+1)+"' class='formTxtBox_1' id='procType_"+(counter+    1)+"'><%=procType%></select></td>"+
                        "<td class='t-align-center'><select name='procNature"+(counter+1)+"' class='formTxtBox_1' id='procNature_"+(counter+1)+"'><%=procNature%></select></td>"+
                        "<input type='hidden' name='urgency"+(counter+1)+"' class='formTxtBox_1' id='urgency_"+(counter+1)+"' value='Yes'/>"+
                        "<td class='t-align-center'><input name='minValue"+(counter+1)+"' type='text' class='formTxtBox_1' maxlength='18' id='minValue_"+(counter+1)+"' style='width:90%;' onblur='return chkMinValueBlank(this);'/><span id='minv_"+(counter+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><span id='minvwords_"+(counter+1)+"'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><input name='maxValue"+(counter+1)+"' type='text' class='formTxtBox_1' maxlength='18' id='maxValue_"+(counter+1)+"' style='width:90%;' onblur='return chkMaxValueBlank(this);'/><span id='maxv_"+(counter+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><span id='maxvwords_"+(counter+1)+"'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><input name='minNoDays"+(counter+1)+"' type='text' class='formTxtBox_1' id='minNoDays_"+(counter+1)+"' style='width:90%;' onblur='return chkMinDaysBlank(this);'/><span id='mind_"+(counter+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><input name='maxNoDays"+(counter+1)+"' type='text' class='formTxtBox_1' id='maxNoDays_"+(counter+1)+"' style='width:90%;' onblur='return chkMaxDaysBlank(this);'/><span id='maxd_"+(counter+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "</tr>";
                    $("#tblrule").append(htmlEle);
                    document.getElementById("TotRule").value = (counter+1);

                });
            });

            $(function() {
                $('#linkDelRule').click(function() {
                    counter = eval(document.getElementById("TotRule").value);
                    delCnt = eval(document.getElementById("delrow").value);

                    var tmpCnt = 0;
                    for(var i=1;i<=counter;i++){
                        if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                            tmpCnt++;
                        }
                    }

                    if(tmpCnt==(eval(counter-delCnt))){
                        $('span.#lotMsg').css("visibility","visible");
                        $('span.#lotMsg').css("color","red");
                        $('span.#lotMsg').html('Minimum 1 record is needed!');
                    }else{
                        if(tmpCnt>0){jConfirm('Are You Sure You want to Delete.','Procurement Method Business Rule Configuration', function(ans) {if(ans){
                                for(var i=1;i<=counter;i++){
                                    if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                                                $("tr[id='trrule_"+i+"']").remove();
                                                $('span.#lotMsg').css("visibility","collapse");
                                                delCnt++;
                                    }
                                }
                            document.getElementById("delrow").value = delCnt;
                        }});}else{jAlert("please Select checkbox first","Procurement Method Business Rule Configuration", function(RetVal) {});}
                    }
                });
            });
        </script>
        <script type="text/javascript">            
            function validate()
            {
                var flag = true;
                var counter = eval(document.getElementById("TotRule").value);
                if(document.getElementById("hiddenaction"))
                {
                    if("edit"==document.getElementById("hiddenaction").value)
                    {
                    $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/BusinessRuleConfigurationServlet",
                        data:"configProcurementId="+$('#hidden').val()+"&"+"budgetType="+$('#budgetType_1').val()+"&"+"tenderType="+$('#tenderType_1').val()+"&"+"procMethod="+$('#procMethod_1').val()+"&"+"procType="+$('#procType_1').val()+"&"+"procNature="+$('#procNature_1').val()+"&"+"typeofImergency="+$('#nationalDisaster_1').val()+"&"+"minvalue="+$('#minValue_1').val()+"&"+"maxvalue="+$('#maxValue_1').val()+"&"+"funName=ProcMethodRuleForEdit",
                        async: false,
                        success: function(j){

                            if(j.toString()!="0"){
                                flag=false;
                                jAlert("Combination of 'BudgetType', 'TenderType', 'ProcurementMethod','ProcurementType', and 'ProcurementNature' already exist.","Procurement Method Business Rule Configuration", function(RetVal) {
                                });
                            }
                        }
                    });
                    }
                }
                for(var k=1;k<=counter;k++)
                {
                    
                    //alert(document.getElementById("minValue_"+k).value);
                    if(document.getElementById("minValue_"+k)!=null)
                    {
                        if(document.getElementById("minValue_"+k).value==""){
                            document.getElementById("minv_"+k).innerHTML="<br/>Please Enter Minimum Value in Nu.";
                            flag = false;
                        }
                        else
                        {
                            if(decimal(document.getElementById("minValue_"+k).value))
                            {
                                if(!chkMax(document.getElementById("minValue_"+k).value))
                                {
                                    document.getElementById("minv_"+k).innerHTML="Maximum 12 digits are allowed.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("minv_"+k).innerHTML=""; 
                                }
                            }
                            else
                            {
                                document.getElementById("minv_"+k).innerHTML="<br/>Please Enter Numbers and 2 Digits After Decimal";
                                flag= false;
                            }
                        }
                    }  

                    if(document.getElementById("maxValue_"+k)!=null)
                    {
                        if(document.getElementById("maxValue_"+k).value=="")
                        {
                            document.getElementById("maxv_"+k).innerHTML="<br/>Please Enter Maximum Value in Nu.";
                            flag = false;
                        }
                        else
                        {   
                            if(decimal(document.getElementById("maxValue_"+k).value))
                            {   //checking for non-zero
                                //alert(document.getElementById("maxValue_"+k).value);
                                
                                if((document.getElementById("maxValue_"+k).value)==0)
                                {
                                    //alert('compared with zero');
                                   
                                    document.getElementById("maxv_"+k).innerHTML="Zero value is not allowed.";
                                    //alert(document.getElementById("maxv_"+k).innerHTML);
                                    flag=false;
                                }
                                else
                                { //for max value comparision
                                    //alert(parseFloat(document.getElementById("minValue_"+k).value));
                                    //alert(parseFloat(document.getElementById("maxValue_"+k).value));
                                    if((parseFloat(document.getElementById("maxValue_"+k).value)) > (parseFloat(document.getElementById("minValue_"+k).value)))
                                    {

                                        if(!chkMax(document.getElementById("maxValue_"+k).value))
                                        {
                                            document.getElementById("maxv_"+k).innerHTML="Maximum 12 digits are allowed.";
                                            flag=false;
                                        }
                                        else
                                        {
                                            document.getElementById("maxv_"+k).innerHTML="";

                                        }
                                        
                                        //document.getElementById("maxv_"+k).innerHTML="Maximum value field must be greater than the minimum value in Lakhs.";
                                        //flag=false;
                                    }
                                    else
                                    {
                                        document.getElementById("maxv_"+k).innerHTML="Maximum value field must be greater than the Minimum value in Nu.";
                                        flag=false;

                                    }
                                }
                            }
                            else
                            {
                                document.getElementById("maxv_"+k).innerHTML="<br/>Please Enter Numbers and 2 Digits After Decimal";
                                flag=false;
                            }
                        }
                    }

                    if(document.getElementById("minNoDays_"+k)!=null)
                    {
                        if(document.getElementById("minNoDays_"+k).value==""){
                            document.getElementById("mind_"+k).innerHTML="<br/>Please Enter Minimum No.of Days for New-Tender/New-Proposal Submission(In days)";
                            flag = false;
                        }
                        else
                        {
                            if(numeric(document.getElementById("minNoDays_"+k).value))
                            {
                                if(!chkMaxLength(document.getElementById("minNoDays_"+k).value))
                                {
                                    document.getElementById("mind_"+k).innerHTML="<br/>Maximum 3 digits are allowed.";
                                    flag= false;
                                }
                                else
                                {
                                    document.getElementById("mind_"+k).innerHTML="";
                                }
                            }
                            else
                            {
                                document.getElementById("mind_"+k).innerHTML="<br/>Please Enter numbers only.";
                                flag= false;
                            }
                        }
                    }

                    if(document.getElementById("maxNoDays_"+k)!=null)
                    {
                        if(document.getElementById("maxNoDays_"+k).value==""){
                            document.getElementById("maxd_"+k).innerHTML="<br/>Please Enter Maximum No.of Days for Re-Tender/Re-Proposal Submission(In days)";
                            flag = false;
                        }
                        else
                        {
                            if(numeric(document.getElementById("maxNoDays_"+k).value))
                            {
                                if(!chkMaxLength(document.getElementById("maxNoDays_"+k).value))
                                {
                                    document.getElementById("maxd_"+k).innerHTML="<br/>Maximum 3 digits are allowed.";
                                    flag= false;
                                }
                                else
                                {
                                    document.getElementById("maxd_"+k).innerHTML="";
                                }
                            }
                            else
                            {
                                document.getElementById("maxd_"+k).innerHTML="<br/>Please Enter numbers only.";
                                flag= false;
                            }
                        }

                    }
                    
                }
               
                // Start OF--Checking for Unique Rows

                if(document.getElementById("TotRule")!=null){
                    var totalcount = eval(document.getElementById("TotRule").value);} //Total Count After Adding Rows
                
                var chk=true;
                var i=0;
                var j=0;
                for(i=1; i<=totalcount; i++) //Loop For Newly Added Rows
                {
                    if(document.getElementById("minValue_"+i)!=null)
                    if($.trim(document.getElementById("minValue_"+i).value)!='' && $.trim(document.getElementById("maxValue_"+i).value) !='' && $.trim(document.getElementById("minNoDays_"+i).value)!='' && $.trim(document.getElementById("maxNoDays_"+i).value)!='') // If Condition for When all Data are filled thiscode is Running
                    {

                        for(j=1; j<=totalcount && j!=i; j++) // Loop for Total Count but Not same as (i) value
                        {
                            chk=true;
                            //If Condition for Check Duplicate Rows are there or not.If Columns are diff then chk variable set to false
                            //IF Row is same Give alert message.
                            if($.trim(document.getElementById("budgetType_"+i).value) != $.trim(document.getElementById("budgetType_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("tenderType_"+i).value) != $.trim(document.getElementById("tenderType_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("procMethod_"+i).value) != $.trim(document.getElementById("procMethod_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("procType_"+i).value) != $.trim(document.getElementById("procType_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("procNature_"+i).value) != $.trim(document.getElementById("procNature_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("nationalDisaster_"+i).value) != $.trim(document.getElementById("nationalDisaster_"+j).value))
                            {
                                chk=false;
                            }
                            
                            var minvi=document.getElementById("minValue_"+i).value;
                            var minvj=document.getElementById("minValue_"+j).value;
                            if(minvi.indexOf(".")==-1)
                            {
                                minvi=minvi+".00";
                            }
                            if(minvj.indexOf(".")==-1)
                            {
                                minvj=minvj+".00";
                            }
                            if($.trim(minvi) != $.trim(minvj))
                            {                               
                                chk=false;
                            }
                            var maxvi=document.getElementById("maxValue_"+i).value;
                            var maxvj=document.getElementById("maxValue_"+j).value;
                            if(maxvi.indexOf(".")==-1)
                            {
                                maxvi=maxvi+".00";
                            }
                            if(maxvj.indexOf(".")==-1)
                            {
                                maxvj=maxvj+".00";
                            }
                            if($.trim(maxvi) != $.trim(maxvj))
                            {                                
                                chk=false;
                            }  
//                            if($.trim(document.getElementById("minNoDays_"+i).value) != $.trim(document.getElementById("minNoDays_"+j).value))
//                            {
//                                chk=false;
//                            }
//                            if($.trim(document.getElementById("maxNoDays_"+i).value) != $.trim(document.getElementById("maxNoDays_"+j).value))
//                            {
//                                chk=false;
//                            }
                            if(flag){
                           if(chk==true) //If Row is same then give alert message
                            {       
                                    jAlert("Duplicate record found. Please enter unique record","Procurement Method Business Rule Configuration",function(RetVal) {
                                    });
                                return false;
                            }
                        }
                    }
                }
                }
                // End OF--Checking for Unique Rows
                
                if (flag==false){
                    return false;
                }
            }

            function chkMax(value)
            {
                var chkVal=value;
                var ValSplit=chkVal.split('.');
                //alert(chkVal);
                //alert(ValSplit[0].length);
                if(ValSplit[0].length>12)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            function chkMaxLength(value)
            {
                var ValueChk=value;
                //alert(ValueChk);

                if(ValueChk.length>3)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            function chkMinValueBlank(obj){
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                //document.getElementById("minvwords_"+i).innerHTML = DoIt(document.getElementById("minValue_"+i).value);
                document.getElementById("minvwords_"+i).innerHTML = CurrencyConverter(document.getElementById("minValue_"+i).value);
                var chk=true;
                if(obj.value!='')
                {
                    if(decimal(obj.value))
                    {
                        if(!chkMax(obj.value))
                        {
                            document.getElementById("minv_"+i).innerHTML="Maximum 12 digits are allowed.";
                            chk=false;
                        }
                        else
                        {
                            document.getElementById("minv_"+i).innerHTML="";
                        }
                        //document.getElementById("minv_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("minv_"+i).innerHTML="<br/>Please Enter Numbers and 2 Digits After Decimal";
                        chk=false;
                    }
                }
                else
                {
                    document.getElementById("minv_"+i).innerHTML = "<br/>Please Enter Minimum Value in Nu.";
                    chk=false;
                }
                if(chk==false)
                {
                    return false;
                }
            }
            function chkMaxValueBlank(obj){
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                //document.getElementById("maxvwords_"+i).innerHTML = DoIt(document.getElementById("maxValue_"+i).value);
                document.getElementById("maxvwords_"+i).innerHTML = CurrencyConverter(document.getElementById("maxValue_"+i).value);
                var chk1=true;
                
                if(obj.value!='')
                {
                    if(decimal(obj.value))
                    {   //checking for non-zero

                        if((obj.value)==0)
                        {
                            document.getElementById("maxv_"+i).innerHTML="Zero value is not allowed.";
                            chk1=false;
                        }
                        else
                        { //for max value comparision
                            //alert(document.getElementById("minValue_"+i).value);
                            if((parseFloat(obj.value)) > (parseFloat(document.getElementById("minValue_"+i).value)))
                            {
                                if(!chkMax(obj.value))
                                {
                                    document.getElementById("maxv_"+i).innerHTML="Maximum 12 digits are allowed.";
                                    chk1=false;
                                }
                                else
                                {
                                    document.getElementById("maxv_"+i).innerHTML="";

                                }
                            }
                            else
                            {
                                document.getElementById("maxv_"+i).innerHTML="Maximum value must be greater than the Minimum value in Nu.";
                                chk1=false;
                            }
                        }
                    }
                    else
                    {
                        document.getElementById("maxv_"+i).innerHTML="<br/>Please Enter Numbers and 2 Digits After Decimal.";
                        chk1=false;
                    }
                }
                else
                {
                    document.getElementById("maxv_"+i).innerHTML = "<br/>Please Enter Maximum Value in Nu.";
                    chk1=false;
                }
                if(chk1==false)
                {
                    return false;
                }
            }

            function chkMinDaysBlank(obj){
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk2=true;
                if(obj.value!='')
                {
                    if(numeric(obj.value))
                    {
                        if(!chkMaxLength(obj.value))
                        {
                            document.getElementById("mind_"+i).innerHTML="Maximum 3 digits are allowed.";
                            chk2=false;
                        }
                        else
                        {
                            document.getElementById("mind_"+i).innerHTML="";
                        }
                        //document.getElementById("mind_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("mind_"+i).innerHTML="<br/>Please Enter numbers only.";
                        chk2=false;
                    }
                }
                else
                {
                    document.getElementById("mind_"+i).innerHTML = "<br/>Please Enter Minimum No. of Days for New-Tender/New-Proposal Submission(In days)";
                    chk2=false;
                }
                if(chk2==false)
                {
                    return false;
                }
            }
            function chkMaxDaysBlank(obj){
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk3=true;
                if(obj.value!='')
                {
                    if(numeric(obj.value))
                    {
                        if(!chkMaxLength(obj.value))
                        {
                            document.getElementById("maxd_"+i).innerHTML="Maximum 3 digits are allowed.";
                            chk3=false;
                        }
                        else
                        {
                            document.getElementById("maxd_"+i).innerHTML="";
                        }

                        //document.getElementById("maxd_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("maxd_"+i).innerHTML="<br/>Please Enter numbers only.";
                        chk3= false;
                    }
                }
                else
                {
                    document.getElementById("maxd_"+i).innerHTML = "<br/>Please Enter Maximum No. of Days for Re-Tender/Re-Proposal Submission(In days)";
                    chk3=false;
                }
                if(chk3==false)
                {
                    return false;
                }
            }

            function numeric(value) {
                return /^\d+$/.test(value);
            }

            function decimal(value) {
                return /^(\d+(\.\d{2})?)$/.test(value);
            }
            function ConvertNewAdded()
            {

            }
        </script>

    </head>
    <body onload="convert();">        
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
                        <div class="pageHead_1">Procurement Method Business Rule Configuration</div>

                        <div style="font-style: italic" class="ff t-align-left">Fields marked with (<span class="mandatory">*</span>) are mandatory</div>
                        <%
                                    if (request.getParameter("id") == null) {
                        %>

                        <div align="right" class="t_space">
                            <span id="lotMsg" style="color: red; font-weight: bold; float: left; visibility: collapse">&nbsp;</span>
<!--                            <a id="linkAddRule"class="action-button-add">Add Rule</a> -->
                            <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a>

                        </div>
                        <%}%>
                        <div>&nbsp;</div>
                        <%--<div class="pageHead_1" align="center"> <%=msg%></div>--%>
                        <form action="ProcMethodRuleDetails.jsp" method="post">
                            <table cellspacing="0" class="tableList_1 t_space" id="tblrule" name="tblrule">
                                <tr>
                                    <th >Select</th>
                                    <th >Budget  Type <br />(<span class="mandatory">*</span>)</th>
                                    <th >Tender  Type<br />(<span class="mandatory">*</span>)</th>
                                    <th >Procurement  Method<br />(<span class="mandatory">*</span>)</th>
                                    <th >Procurement  Type<br />(<span class="mandatory">*</span>)</th>
                                    <th >Procurement  Category<br />(<span class="mandatory">*</span>)</th>
                                   <%-- <th >Type Of Emergency<br />(<span class="mandatory">*</span>)</th>--%>
                                    <th >Minimum Value in <br />(In Nu.)<br />(<span class="mandatory">*</span>)</th>
                                    <th >Minimum Value in <br />(In Nu. words)<br /><span class="mandatory"></span></th>
                                    <th >Maximum  Value in <br />(In Nu.)<br />(<span class="mandatory">*</span>) </th>
                                    <th >Maximum  Value in <br />(In Nu. words)<br /><span class="mandatory"></span> </th>
                                    <th >No. of days for <br/>New-Tender/New-Proposal Submission    <br />(<span class="mandatory">*</span>) </th>
                                    <th >No. of days for <br/>Re-Tender/Re-Proposal Submission<br />(<span class="mandatory">*</span>) </th>
                                </tr>

                                <input type="hidden" name="delrow" value="0" id="delrow"/>
                                <input type="hidden" name="introw" value="" id="introw"/>

                                <%
                                            List l = null;
                                            if (request.getParameter("id") != null && "edit".equals(request.getParameter("action"))) {
                                                String action = request.getParameter("action");
                                                int id = Integer.parseInt(request.getParameter("id"));
                                                %>
                                                <input type="hidden" id="hiddenaction"name="action" value="<%=action%>"/>
                                                <input type="hidden" id="hidden" name="id" value="<%=id%>"/>
                                                <%

                                                l = configPreTenderRuleSrBean.getConfigProcurement(id);
                                            } else
                                                    l = configPreTenderRuleSrBean.getConfigProcurement();
                                            int i = 0;
                                            int length = 0;
                                            if (!l.isEmpty() || l != null) {
                                                Iterator it = l.iterator();
                                                TblConfigProcurement configProcurement = new TblConfigProcurement();
                                                List getBudgetType = configPreTenderRuleSrBean.getBudgetType();
                                                List getTenderType = configPreTenderRuleSrBean.getTenderNames();
                                                List getProcNature = configPreTenderRuleSrBean.getProcurementNature();
                                                List getProcMethod = configPreTenderRuleSrBean.getProcurementMethod();
                                                List getProcTypes = configPreTenderRuleSrBean.getProcurementTypes();
                                                while (it.hasNext()) {
                                                    configProcurement = (TblConfigProcurement) it.next();
                                                    i++;
                                                    length++;
                                                    StringBuilder tType = new StringBuilder();
                                                    StringBuilder bType = new StringBuilder();
                                                    StringBuilder pNature = new StringBuilder();
                                                    StringBuilder pType = new StringBuilder();
                                                    StringBuilder pMethod = new StringBuilder();
                                                    StringBuilder natDisaster = new StringBuilder();
                                                    StringBuilder urgency = new StringBuilder();
                                                    String minValue = new String();
                                                    String maxValue = new String();
                                                    String minDays = new String();
                                                    String maxDays = new String();

                                                    TblBudgetType tblbudgetType = new TblBudgetType();
                                                    Iterator btit = getBudgetType.iterator();
                                                    String budType="";
                                                    while (btit.hasNext()) {
                                                        tblbudgetType = (TblBudgetType) btit.next();
                                                        if(tblbudgetType.getBudgetType().equalsIgnoreCase("Development"))
                                                            budType="Capital";
                                                        else if(tblbudgetType.getBudgetType().equalsIgnoreCase("Revenue"))
                                                            budType="Recurrent";
                                                        else
                                                            budType= budgetTypes2.getBudgetType();
                                                        bType.append("<option value='");
                                                        if (tblbudgetType.getBudgetTypeId() == configProcurement.getTblBudgetType().getBudgetTypeId()) {
                                                            bType.append(tblbudgetType.getBudgetTypeId() + "' selected='true'>" + budType);
                                                        } else {
                                                            bType.append(tblbudgetType.getBudgetTypeId() + "'>" + budType);
                                                        }
                                                        bType.append("</option>");
                                                    }

                                                    TblTenderTypes tenderTypes = new TblTenderTypes();
                                                    Iterator trit = getTenderType.iterator();
                                                    while (trit.hasNext()) {
                                                        tenderTypes = (TblTenderTypes) trit.next();
                                                        if(!tenderTypes.getTenderType().equals("PQ") && !tenderTypes.getTenderType().equals("RFA") && !tenderTypes.getTenderType().equals("2 Stage-PQ"))
                                                        {
                                                            tType.append("<option value='");
                                                            if (tenderTypes.getTenderTypeId() == configProcurement.getTblTenderTypes().getTenderTypeId()) {
                                                                tType.append(tenderTypes.getTenderTypeId() + "' selected='true'>" + tenderTypes.getTenderType());
                                                            } else {
                                                                tType.append(tenderTypes.getTenderTypeId() + "'>" + tenderTypes.getTenderType());
                                                            }
                                                            tType.append("</option>");
                                                         }
                                                    }

                                                    TblProcurementNature tblProcureNature = new TblProcurementNature();
                                                    Iterator pnit = getProcNature.iterator();
                                                    while (pnit.hasNext()) {
                                                        tblProcureNature = (TblProcurementNature) pnit.next();                                                          
                                                        
                                                                pNature.append("<option value='");
                                                                if (tblProcureNature.getProcurementNatureId() == configProcurement.getTblProcurementNature().getProcurementNatureId()) {
                                                                    pNature.append(tblProcureNature.getProcurementNatureId() + "' selected='true'>" + tblProcureNature.getProcurementNature());
                                                                } else {
                                                                    pNature.append(tblProcureNature.getProcurementNatureId() + "'>" + tblProcureNature.getProcurementNature());
                                                                }
                                                                pNature.append("</option>");
                                                             
                                                        }

                                                    TblProcurementMethod tblProcurementMethod = new TblProcurementMethod();
                                                    Iterator pmit = getProcMethod.iterator();
                                                    while (pmit.hasNext()) {
                                                        tblProcurementMethod = (TblProcurementMethod) pmit.next();
                                                        if("OTM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                           "LTM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                           "DP".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                          // "LEQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                         //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                           "RFQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                          // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                         //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                           "FC".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                          // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                                                           "DPM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())||
                                                            "QCBS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                            "LCS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                            "SFB".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                            "SBCQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                            "SSS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                            "IC".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()))
                                                        {
                                                            pMethod.append("<option value='");
                                                            if (tblProcurementMethod.getProcurementMethodId() == configProcurement.getTblProcurementMethod().getProcurementMethodId()) {
                                                                if(tblProcurementMethod.getProcurementMethod().equals("RFQ"))
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>LEM");

                                                                }
                                                                else if(tblProcurementMethod.getProcurementMethod().equals("DPM"))
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>DCM");

                                                                }
                                                                else
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>" + tblProcurementMethod.getProcurementMethod());

                                                                }
                                                            } else {
                                                                
                                                                if(tblProcurementMethod.getProcurementMethod().equals("RFQ"))
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>LEM");

                                                                }
                                                                else if(tblProcurementMethod.getProcurementMethod().equals("DPM"))
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>DCM");

                                                                }
                                                                else
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>" + tblProcurementMethod.getProcurementMethod());

                                                                }
                                                            }
                                                            pMethod.append("</option>");
                                                        }
                                                    }
                                                    TblProcurementTypes tblProcurementTypes = new TblProcurementTypes();
                                                    Iterator ptit = getProcTypes.iterator();
                                                    String procureType="";
                                                    while (ptit.hasNext()) {
                                                        tblProcurementTypes = (TblProcurementTypes) ptit.next();
                                                        if(tblProcurementTypes.getProcurementType().equalsIgnoreCase("NCT"))
                                                            procureType="NCB";
                                                        else if(tblProcurementTypes.getProcurementType().equalsIgnoreCase("ICT"))
                                                            procureType="ICB";
                                                        pType.append("<option value='");
                                                        if (tblProcurementTypes.getProcurementTypeId() == configProcurement.getTblProcurementTypes().getProcurementTypeId()) {
                                                            pType.append(tblProcurementTypes.getProcurementTypeId() + "' selected='true'>" + procureType);
                                                        } else {
                                                            pType.append(tblProcurementTypes.getProcurementTypeId() + "'>" + procureType);
                                                        }
                                                        pType.append("</option>");

                                                    }

                                                    String[] opt = {"Normal", "National Disaster", "Urgent(Catastrophe)"};
                                                    int a = opt.length;

                                                    for (int j = 0; j < a; j++) {
                                                        natDisaster.append("<option ");
                                                        if (opt[j].equals(configProcurement.getIsNationalDisaster())) {
                                                            natDisaster.append(" selected='true' value='"+opt[j]+"'>" + opt[j]);
                                                        } else {
                                                            natDisaster.append(">" + opt[j]);
                                                        }
                                                        natDisaster.append("</option>");
                                                    }

                                                    /* if ("Yes".equals(configProcurement.getIsNationalDisaster())) {
                                                    natDisaster.append("<option value='Yes' selected='true'>Yes</option><option>No</option>");
                                                    } else {
                                                    natDisaster.append("<option>Yes</option><option value='No' selected='true'>No</option>");
                                                    }

                                                    if ("Yes".equals(configProcurement.getIsUrgency())) {
                                                    urgency.append("<option value='Yes' selected='true'>Yes</option><option>No</option>");
                                                    } else {
                                                    urgency.append("<option>Yes</option><option value='No' selected='true'>No</option>");
                                                    }*/

                                %>


                                <tr id="trrule_<%=i%>">
                                    <td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_<%=i%>" value="" /></td>
                                    <td class="t-align-center"><select name="budgetType<%=i%>" class="formTxtBox_1" id="budgetType_<%=i%>">
                                            <%=bType%></select>
                                    </td>
                                    <td class="t-align-center"><select name="tenderType<%=i%>" class="formTxtBox_1" id="tenderType_<%=i%>">
                                            <%=tType%></select>
                                    </td>
                                    <td class="t-align-center"><select name="procMethod<%=i%>" class="formTxtBox_1" id="procMethod_<%=i%>">
                                            <%=pMethod%></select>
                                    </td>
                                    <td class="t-align-center"><select name="procType<%=i%>" class="formTxtBox_1" id="procType_<%=i%>">
                                            <%=pType%></select>
                                    </td>
                                    <td class="t-align-center"><select name="procNature<%=i%>" class="formTxtBox_1" id="procNature_<%=i%>">
                                            <%=pNature%></select>
                                    </td>
                                   <%--  <td class="t-align-center"><select name="nationalDisaster<%=i%>" class="formTxtBox_1" id="nationalDisaster_<%=i%>">
                                            <%=natDisaster%></select>
                                    </td> --%>
                                   <input type="hidden" name="nationalDisaster1" class="formTxtBox_1" id="nationalDisaster_1" value="Normal">
                                        
                                    <input name="urgency<%=i%>" type="hidden" value="Yes" class="formTxtBox_1" id="urgency_<%=i%>"/>

                                    <td class="t-align-center"><input name="minValue<%=i%>" type="text" class="formTxtBox_1" maxlength="18" style="width:90%;" id="minValue_<%=i%>" value="<%=configProcurement.getMinValue()%>" onblur="return chkMinValueBlank(this);"/><span id="minv_<%=i%>" style="color: red;">&nbsp;</span></td>
                                    <td class="t-align-center"><span id="minvwords_<%=i%>" value="<%=i%>">&nbsp;</span></td>
                                    <td class="t-align-center"><input name="maxValue<%=i%>" type="text" class="formTxtBox_1" maxlength="18"  style="width:90%;" id="maxValue_<%=i%>" value="<%=configProcurement.getMaxValue()%>" onblur="return chkMaxValueBlank(this);"/><span id="maxv_<%=i%>" style="color: red;">&nbsp;</span></td>
                                    <td class="t-align-center"><span id="maxvwords_<%=i%>" >&nbsp;</span></td>
                                    <td class="t-align-center"><input name="minNoDays<%=i%>" type="text" class="formTxtBox_1" style="width:90%;" id="minNoDays_<%=i%>" value="<%=configProcurement.getMinSubDays()%>" onblur="return chkMinDaysBlank(this);"/><span id="mind_<%=i%>" style="color: red;">&nbsp;</span></td>
                                    <td class="t-align-center"><input name="maxNoDays<%=i%>" type="text" class="formTxtBox_1"  style="width:90%;" id="maxNoDays_<%=i%>" value="<%=configProcurement.getMaxSubDays()%>" onblur="return chkMaxDaysBlank(this);"/><span id="maxd_<%=i%>" style="color: red;">&nbsp;</span></td>
                                </tr>

                                <%
                                                }
                                            } else {
                                                msg = "No Record Found";
                                            }

                                %>


                            </table>
                            <div>&nbsp;</div>
                            <input type="hidden" name="TotRule" id="TotRule" value="<%=i%>"/>
                            <table width="100%" cellspacing="0" cellpadding="0" >
                                <tr>
                                    <%if ("edit".equals(request.getParameter("action"))) {%>
                                    <td class="t-align-center">
                                        <span class="formBtn_1"><input type="submit" name="button" id="button" value="Update" onclick="return validate();" /></span>
                                    </td>
                                    <%} else {%>
                                    <td class="t-align-center">
                                        <span class="formBtn_1"><input type="submit" name="button" id="button" value="Submit" onclick="return validate();" /></span>
                                    </td>
                                    <%}%>
                                </tr>
                            </table>
                            <%--<div align="center"> <%=msg%></div>--%>
                        </form>

                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>
             <script>
                var obj = document.getElementById('lblProcMethodEdit');
                if(obj != null){
                    if(obj.innerHTML == 'Edit'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            <script type="text/javascript">
                function convert()
                {
                for(var k=1; k<=<%= length%>; k++)
                    {
                        //document.getElementById("minvwords_"+k).innerHTML = DoIt(document.getElementById("minValue_"+k).value);
                        //document.getElementById("maxvwords_"+k).innerHTML = DoIt(document.getElementById("maxValue_"+k).value);
                        document.getElementById("minvwords_"+k).innerHTML = CurrencyConverter(document.getElementById("minValue_"+k).value);
                        document.getElementById("maxvwords_"+k).innerHTML = CurrencyConverter(document.getElementById("maxValue_"+k).value);
                    }
                }
            </script>
    </body>
</html>


