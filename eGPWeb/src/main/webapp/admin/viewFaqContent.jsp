<%-- 
    Document   : viewFaqContent
    Created on : May 30, 2017, 11:30:59 AM
    Author     : Nitish Oritro
--%>


<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblFaqBhutan"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.FaqBhutanService"%>
<%@page import="com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.BhutanDebarmentCommitteeService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Faq</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>        <script type="text/javascript">
            $(document).ready(function () {
                sortTable();
            });
        </script>
        <script type="text/javascript">
            function conform()
            {
                if (confirm("Do you want to delete this FAQ?"))
                    return true;
                else
                    return false;
            }
        </script>
            
 
    </head>
    <body>



        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <%            FaqBhutanService FaqBhutanService = (FaqBhutanService) AppContext.getSpringBean("FaqBhutanService");
                //List<TblFaqBhutan> tblFaqBhutan = FaqBhutanService.getAllTblFaqBhutan();
            %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                        <td class="contentArea">

                        <%
                            String message = "";
                            if (!"".equalsIgnoreCase(request.getParameter("msg"))) {
                                message = request.getParameter("msg");
                                if ("actS".equalsIgnoreCase(message)) {
                        %>
                        <div class="responseMsg successMsg t_space"><span>Debarment Committee Member is activated successfully</span></div><br/>
                        <%} else if ("actF".equalsIgnoreCase(message)) {
                        %>
                        <div class="responseMsg errorMsg t_space"><span>Error occurred while activating Debarment Committee Member!</span></div><br/>
                        <%} else if ("faqCreatedSuccessfully".equalsIgnoreCase(message)) {%>
                        <div class="responseMsg successMsg t_space"><span>FAQ Created Successfully.</span></div><br/>
                        <%} else if ("edtF".equalsIgnoreCase(message)) {%>
                        <div class="responseMsg successMsg t_space"><span>FAQ Updated Successfully.</span></div><br/>
                        <%} else if ("edtF".equalsIgnoreCase(message)) {%>
                        <div class="responseMsg errorMsg t_space"><span>Error occurred while updating!</span></div><br/>
                        <%} else if ("chF".equalsIgnoreCase(message)) {%>
                        <div class="responseMsg errorMsg t_space"><span>There is already an active Chairman!</span></div><br/>
                        <%} else if ("crS".equalsIgnoreCase(message)) {%>
                        <div class="responseMsg successMsg t_space"><span>FAQ Deleted successfully.</span></div><br/>
                        <%}
                            }
                            boolean doneFlag = false;
                            if ("delete".equals(request.getParameter("action"))) {
                                //String doneFlag = "";
                                String action = "Remove FAQ Content";
                                try {
                                    //int faqId = Integer.parseInt(request.getParameter("faqId"));
                                    TblFaqBhutan tblFaqBhutanThis = new TblFaqBhutan();
                                    tblFaqBhutanThis.setFaqId(Integer.parseInt(request.getParameter("faqId")));

                                    FaqBhutanService.deleteTblFaqBhutan(tblFaqBhutanThis);
                                    //delMsg="Notification of Award (NOA) Business Rule Deleted Successfully";
                                    doneFlag = true;
                                } catch (Exception e) {
                                    System.out.println(e);
                                    action = "Error in : " + action + " : " + e;
                                } finally {
                                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                                    action = null;
                                }
                            }
                            if (doneFlag) {
                        %>
                        <div class="responseMsg successMsg t_space"><span>FAQ Deleted Successfully</span></div>
                        <%
                            }

                            List<TblFaqBhutan> tblFaqBhutan = FaqBhutanService.getAllTblFaqBhutan();

                        %>
                        <div class="pageHead_1">View Frequently Asked Questions

                            <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('3');">Save as PDF</a></span>
                        </div>

                        <br>


                        <div class="tabPanelArea_1">

                            <table class="tableList_1 t_space" cellspacing="0" width="100%" id="resultTable" >
                                <tbody id="tbodyData">&nbsp;

                                    <tr>
                                        <th class="t-align-center" >Question No.</th>
                                        <th class="t-align-center">Question Name</th>
                                        <th class="t-align-center">Answers</th>

                                        <th class="t-align-center" width="15%">Action</th>
                                    </tr>
                                    <%                                    int cnt = 0;
                                        for (TblFaqBhutan tblFaqBhutanThis : tblFaqBhutan) {

                                            cnt++;
                                            if (cnt % 2 == 0) {%>
                                    <tr>
                                        <%} else {%>
                                    <tr style='background-color:#E4FAD0;'>
                                        <%}%>
                                        <td class="t-align-center">
                                            <%out.print(cnt);%>
                                        </td>
                                        <td class="t-align-center">
                                            <%out.print(tblFaqBhutanThis.getQuestion());%>
                                        </td>
                                        <td class="t-align-center">
                                            <%out.print(tblFaqBhutanThis.getAnswer());%>
                                        </td>

                                        <td class="t-align-center">
                                            <a class="btn" href="viewFaqContent.jsp?action=delete&faqId=<%=tblFaqBhutanThis.getFaqId()%>" onclick="return conform();">Delete</a>&nbsp;|
                                            <a href="EditFaqContent.jsp?faqId=<%=tblFaqBhutanThis.getFaqId()%>">Edit</a>&nbsp;
                                            <%--<a href="EditFaqContent.jsp?faqId=<%=tblFaqBhutanThis.getFaqId()%>">Delete</a>&nbsp;--%>
                                            
                                        </td>
                                    </tr>
                                    <% }%>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
<%-- Nitish Start :--- 
        PDF generator in viewfaqContent Page --%>
            <form id="formstyle" action="" method="post" name="formstyle">

                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                    String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="ViewFAQContent_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="EvaluationRule" />
            </form>
<%-- Nitish END :--- 
        PDF generator in viewfaqContent Page --%>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabContent");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }
        var leftObj = document.getElementById("lblFaqView");
        if (leftObj != null) {
            leftObj.setAttribute("class", "selected");
        }
        
        $(document).ready(function(){
           $('.btn').on('click',function(){
               bootbox.alert("Hello There");
           }) 
        });
        
        
    </script>
</html>
