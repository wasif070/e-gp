<%--
    Document   : CreateFormTable
    Created on : 24-Oct-2010, 4:49:09 PM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.List" %>
<jsp:useBean id="templateTblSrBean" class="com.cptu.egp.eps.web.servicebean.TemplateTableSrBean" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    if (session.getAttribute("userId") != null) {
                        templateTblSrBean.setLogUserId(session.getAttribute("userId").toString());
                    }
                    String strTitle = "Create form's table";
                    boolean isEdit = false;
                    int tableId = 0;
                    if (request.getParameter("tableId") != null) {
                        isEdit = true;
                        tableId = Integer.parseInt(request.getParameter("tableId"));
                        strTitle = "Edit form's table";
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=strTitle%></title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
        <script type="text/javascript" src="../resources/js/form/Add.js"></script>
        <script type="text/javascript" src="../resources/js/form/CommonValidation.js"></script>
        <script type="text/javascript" src="../resources/js/form/CreateTable.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script>
            var $jq = jQuery.noConflict();

            function validate(form,isBoQ){
                var flag = true;
                $jq('textArea[id^=TableHeader]').each(function(){
                    if($jq.trim(CKEDITOR.instances[$jq(this).attr("id")].getData()).length > 2000){
                        jAlert("only 2000 characters are allowed for Table Header ." , 'Alert');
                        flag = false;
                    }

                   
                });
                if(flag == true){
                    $jq('textArea[id^=TableFooter]').each(function(){
                        if($jq.trim(CKEDITOR.instances[$jq(this).attr("id")].getData()).length > 2000){
                            jAlert("only 2000 characters are allowed for Table Footer ." , 'Alert');
                            flag = false;
                        }
                   
                    });
                }
                if(flag == true){
                    if(!checkTableEntryOk(form, isBoQ)){
                        flag = false;
                    }
                    else{
                        flag = true;
                    }
                }

                return flag;
            }

/*            window.onkeypress =function (e)
            {
                var keyValue = (window.event)? e.keyCode : e.which;
                alert(keyValue);
            }

            window.onunload = function (e)
            {
                var keyValue = (window.event)? e.keyCode : e.which;
                alert(keyValue);
                alert('on unload <%=request.getHeader("referer")%>');
                alert(document.referrer);
                alert(top.document.referrer);
               // checkbeforeunload();
            }
*/
        </script>

    </head>
    <%
                short templateId = 0;
                int sectionId = 0;
                int formId = 0;
                short noOfTable = 0;



                if (request.getParameter("templateId") != null) {
                    templateId = Short.parseShort(request.getParameter("templateId"));
                }
                if (request.getParameter("sectionId") != null) {
                    sectionId = Integer.parseInt(request.getParameter("sectionId"));
                }
                if (request.getParameter("formId") != null) {
                    formId = Integer.parseInt(request.getParameter("formId"));
                }

                if (request.getParameter("noOfTables") != null) {
                    noOfTable = Short.parseShort(request.getParameter("noOfTables"));
                } else {
                    noOfTable = 1;
                }
                String tblName = "";
                StringBuffer tblHeader = new StringBuffer();
                StringBuffer tblFooter = new StringBuffer();
                String isMultipleFilling = "";
                short noOfRows = 0;
                short noOfCols = 0;
                if (isEdit) {
                    List<com.cptu.egp.eps.model.table.TblTemplateTables> tblDtl = templateTblSrBean.getTemplateTablesDetail(tableId);
                    if (tblDtl != null) {
                        if (tblDtl.size() > 0) {
                            tblName = tblDtl.get(0).getTableName();
                            tblHeader.append(tblDtl.get(0).getTableHeader());
                            tblFooter.append(tblDtl.get(0).getTableFooter());
                            isMultipleFilling = tblDtl.get(0).getIsMultipleFilling();
                            noOfCols = tblDtl.get(0).getNoOfCols();
                            noOfRows = tblDtl.get(0).getNoOfRows();
                        }
                        tblDtl = null;
                    }
                }

                boolean isBOQForm = templateTblSrBean.isPriceBidForm(formId);
    %>
    <body>
        <div class="dashboard_div">
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <div class="contentArea_1">
                <!--Middle Content Table Start-->
                <form action="<%=request.getContextPath()%>/CreateSTDForm?action=formTableCreation" name="frmFormTable" id="frmFormTable" action="CreateFormMatrix.jsp" method="post">
                    <div class="pageHead_1">
                        <%=strTitle%>
                        <span style="float: right; text-align: right;">
                            <a class="action-button-goback" href="TableDashboard.jsp?templateId=<%= templateId%>&sectionId=<%= sectionId%>&formId=<%= formId%>" title="Form Dashboard">Form Dashboard</a>
                        </span>
                    </div>
                    <table width="100%" cellspacing="10" class="tableView_1">
                        <tr>
                            <tr>
                                <td style="font-style: italic" class="" align="right">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                            </tr>
                        </tr>
                    </table>
                    <%if (!isEdit) {%>
                    <table width="100%" cellspacing="10" class="tableView_1">
                        <tr>
                            <td align="left">
                                <a href="#" class="action-button-add" onclick="AddNewTable('<%= isBOQForm%>');" title="Add Table">Add Table</a>
                                <a href="#" class="action-button-delete" onclick="DelTable(document.getElementById('frmFormTable'));" title="Delete Table">Delete Table</a>
                            </td>
                        </tr>
                    </table>
                    <%} else {%>
                    <input type="hidden" value="<%= tableId%>" name="tableId" id="tableId" />
                    <%}%>
                    <input type="hidden" name="TableCount" id="TableCount" value="<%=noOfTable%>" />
                    <input type="hidden" value="0" name="noOfDelTables" id="noOfDelTables" />
                    <input type="hidden" value="" name="delTableArr" id="delTableArr" />

                    <input type="hidden" value="<%= templateId%>" name="templateId" id="templateId" />
                    <input type="hidden" value="<%= sectionId%>" name="sectionId" id="sectionId" />
                    <input type="hidden" value="<%= formId%>" name="formId" id="formId" />

                    <table width="100%" cellspacing="10" class="tableView_1">
                        <tr id="TableRow">
                            <%
                                        for (short i = 1; i <= noOfTable; i++) {
                            %>
                            <td id="tdTable_<%= i%>" valign="top">
                                <%if (isEdit) {%>
                                <div class="formSubHead_1">Edit Table Details</div>
                                <%} else {%>
                                <div class="formSubHead_1">Enter Table Details
                                    <input type="checkbox" name="delTable" id="delTable_<%= i%>" />
                                </div>
                                <%}%>
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                    <tr>
                                        <td width="20%" class="ff">Table Name : </td>
                                                <td width="80%"><textarea name="TableName<%=i%>" rows="3" class="formTxtBox_1" id="TableName<%=i%>" style="width:200px;"><%if (isEdit) {
                                                                                    out.print(tblName.replace("<br/>", "\n"));
                                                                                }%></textarea></td>
                                    </tr>
                                    <tr>
                                        <td width="125" class="ff">Header : </td>
                                        <td>
                                                <textarea cols="80" id="TableHeader<%=i%>" name="TableHeader<%=i%>" rows="10"><%if (isEdit) {
                                                                                        out.print(tblHeader);
                                                                                    }%></textarea>
                                            <script type="text/javascript">
                                                //<![CDATA[

                                                CKEDITOR.replace( 'TableHeader<%=i%>',
                                                {
                                                    fullPage : false
                                                });

                                                //]]>
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="125" class="ff">Footer : </td>
                                        <td>
                                                <textarea cols="80" id="TableFooter<%=i%>" name="TableFooter<%=i%>" rows="10"><%if (isEdit) {
                                                                                        out.print(tblFooter);
                                                                                    }%></textarea>
                                            <script type="text/javascript">
                                                //<![CDATA[

                                                CKEDITOR.replace( 'TableFooter<%=i%>',
                                                {
                                                    fullPage : false
                                                });

                                                //]]>
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="125" class="ff">To be filled Multiple Times : <%if (!isEdit) {%><span>*</span><%}%></td>
                                        <td>
                                            <%if (!isEdit) {%>
                                            <input type="radio" name="MultipleFilling<%=i%>" id="MultipleFillingY<%=i%>" value="yes" />Yes
                                            <input type="radio" name="MultipleFilling<%=i%>" id="MultipleFillingN<%=i%>" value="no" checked />No
                                            <%} else {%>
                                            <input type="radio" name="MultipleFilling<%=i%>" id="MultipleFillingY<%=i%>" value="yes" <%if ("yes".equals(isMultipleFilling)) {
                                                    out.print("checked");
                                                }%> />Yes
                                            <input type="radio" name="MultipleFilling<%=i%>" id="MultipleFillingN<%=i%>" value="no" <%if ("no".equals(isMultipleFilling)) {
                                                    out.print("checked");
                                                }%> />No
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="125" class="ff">No of Columns required : <%if (!isEdit) {%><span>*</span><%}%></td>
                                        <td>
                                            <%if (isEdit) {%>
                                            <%= noOfCols%><input name="NoOfCols<%=i%>" type="hidden" class="formTxtBox_1" id="NoOfCols<%=i%>" value="<%= noOfCols%>" maxlength="2" style="width:50px;" />
                                            <%} else {%>
                                            <input name="NoOfCols<%=i%>" type="text" class="formTxtBox_1" id="NoOfCols<%=i%>"  maxlength="2" style="width:50px;" />
                                            <%}%>
                                        </td>
                                    </tr>
                                        <tr <%if (isBOQForm) {
                                                                                out.print(" style='display:none' ");
                                                                            }%> >
                                        <td width="125" class="ff">No of Rows required :<%if (!isEdit) {%><span>*</span><%}%></td>
                                        <td>
                                            <%if (isEdit) {%>
                                            <%= noOfRows%><input name="NoOfRows<%=i%>" type="hidden" class="formTxtBox_1" id="NoOfRows<%=i%>" value="<%= noOfRows%>" maxlength="2" style="width:50px;" />
                                            <%} else {%>
                                            <input name="NoOfRows<%=i%>" type="text" class="formTxtBox_1" id="NoOfRows<%=i%>" <%if (isBOQForm) {
                                                    out.print(" value='0' ");
                                                }%> maxlength="2" style="width:50px;" />
                                            <%}%>
                                        </td>
                                        <td colspan="2" style="display: none;">
                                            Save : <input type="checkbox" name="Check" value="<%=i%>" id="Check_<%=i%>" checked />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <%
                                        }
                            %>
                        </tr></table>
                    <div align="center">
                        <label class="formBtn_1">
                            <input type="submit" name="btnCreateEdit" id="btnCreateEdit"
                                   <%if (isEdit) {%>
                                   value="Edit"
                                   <%} else {%>
                                   value="Next Step"
                                   <%}%>
                                   onclick="return validate(this.form, '<%= isBOQForm%>');" />
                        </label>
                    </div>
                </form>
            </div>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <script>
            var headSel_Obj = document.getElementById("headTabSTD");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
<%
            if (tblHeader != null) {
                tblHeader = null;
            }
            if (tblFooter != null) {
                tblFooter = null;
            }
            if (templateTblSrBean != null) {
                templateTblSrBean = null;
            }
%>
