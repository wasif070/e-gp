<%-- 
    Document   : ViewRegExtEvalCommMember
    Created on : Feb 19, 2011, 12:56:30 PM
    Author     : rishita
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register External Committee Member</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
    </head>
    <jsp:useBean id="regExtEvalCommMemberSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.RegExtEvalCommMemberSrBean"/>
    <body>
        <%
                    String useId = "";
                    if (request.getParameter("uId") != null) {
                        useId = request.getParameter("uId");
                    }
                    if ("Edit".equals(request.getParameter("edit"))) {
                        response.sendRedirect("EditExtEvalCommMember.jsp?uId=" + useId + "&msg=success");
                    }
                    if ("Ok".equals(request.getParameter("ok"))) {
                        response.sendRedirect("ViewExtEvalCommMember.jsp");
                    }
                    
                    if(request.getParameter("action") != null && request.getParameter("action").equalsIgnoreCase("view") && request.getParameter("ok") == null)
                   {
                         // Coad added by Dipal for Audit Trail Log.
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        String idType="userId";
                        int auditId=Integer.parseInt(request.getParameter("uId"));
                        String auditAction="View External Evaluation Committee Member User";
                        String moduleName=EgpModule.Manage_Users.getName();
                        String remarks="";
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                   }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                            int uTypeId = 0;
                            if(session.getAttribute("userTypeId") != null){
                                uTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%if(uTypeId == 1||uTypeId == 4||uTypeId == 5){%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>
                        <% } %>
                        <td class="contentArea_1">
                            <%if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("success")) {
                            %>
                            <div align="left" id="sucMsg" class="responseMsg successMsg ">External Committee Member created successfully</div>
                            <% }%>
                            <%if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("updated")) {
                                if(uTypeId == 1||uTypeId == 4||uTypeId == 5){
                            %>
                            <div align="left" id="sucMsg" class="responseMsg successMsg ">External Committee Member updated successfully</div>
                            <% }else { %>
                            <div class="responseMsg successMsg">Profile updated successfully</div>
                            <%} } if(uTypeId == 1||uTypeId == 4||uTypeId == 5){ %>
                            <div class="pageHead_1 t_space">View External Committee Member</div>
                            <% } else {%>
                            <div class="pageHead_1 t_space">View Profile</div>
                            <% } %>
                            <form action="" method="post" id="frmregExtEvalCommMember" name="frmregExtEvalCommMember">
                                <%
                                            if(!"1".equals(session.getAttribute("userTypeId").toString())){
                                               regExtEvalCommMemberSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                            }else{
                                                regExtEvalCommMemberSrBean.setAuditTrail(null);
                                            }
                                            for (Object[] listing : regExtEvalCommMemberSrBean.getExtCommListing(Integer.parseInt(useId))) {
                                %>
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td class="ff" width="15%">e-mail ID :</td>
                                        <td width="85%"><%=listing[9]%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Full  Name :</td>
                                        <td width="85%"><%=listing[2]%></td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="ff">Name  in Dzongkha : </td>
                                        <%if (listing[3] != null) {%>
                                        <td width="85%"><%=BanglaNameUtils.getUTFString((byte[]) listing[3])%></td>
                                        <% } %>
                                    </tr>
                                    <tr>
                                        <td class="ff">CID No. : </td>
                                        <% if (listing[4] != null && !listing[4].toString().trim().equals("")) {%>
                                        <td width="85%"><%=listing[4]%></td>
                                        <% } %>
                                    </tr>
                                    <tr>
                                        <td class="ff">Phone  No. :</td>
                                        <%  if (listing[5] != null && !listing[5].toString().trim().equals("")) {%>
                                        <td width="85%"><%=listing[5]%></td>
                                        <% } %>
                                    </tr>
                                    <tr>
                                        <td class="ff">Mobile  No. : </td>
                                        <%  if (listing[6] != null && !listing[6].toString().trim().equals("")) {%>
                                        <td width="85%"><%=listing[6]%></td>
                                        <% }%>
                                    </tr>
                                     <%if(uTypeId == 1||uTypeId == 4||uTypeId == 5){%>
                                    <tr align="center">
                                        <td>&nbsp;</td>
                                        <td  align="left"><label class="formBtn_1"><input id="ok" name="ok" value="Ok" type="submit"/></label>&nbsp;<label class="formBtn_1">
                                                <input id="edit" name="edit" value="Edit" type="submit"/></label></td>
                                    </tr>
                                    <%}%>
                                </table>
                                <% }%>
                            </form>
                            <!--Middle Content Table End-->
                        </td>
                        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>
<%
            regExtEvalCommMemberSrBean = null;
%>
<script type="text/javascript">
    var uTypeId = '<%=uTypeId%>';

    if(uTypeId == 1){
        if(document.getElementById("headTabMngUser") != null){
            document.getElementById("headTabMngUser").setAttribute("class", "selected");
        }
    }else{
        if(document.getElementById("headTabMyAccExt") != null){
            document.getElementById("headTabMyAccExt").setAttribute("class", "selected");
        }
    }
</script>