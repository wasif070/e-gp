<%--
    Document   : CreateSectionClause
    Created on : 24-Oct-2010, 1:03:35 AM
    Author     : yanki
--%>
<jsp:useBean id="sectionClauseSrBean" class="com.cptu.egp.eps.web.servicebean.SectionClauseSrBean" />
<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBean" />
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />

<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster" %>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.model.table.TblIttClause" %>
<%@page import="com.cptu.egp.eps.model.table.TblIttSubClause" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        
            if(session.getAttribute("userId") != null){
                sectionClauseSrBean.setLogUserId(session.getAttribute("userId").toString());
                createSubSectionSrBean.setLogUserId(session.getAttribute("userId").toString());
                defineSTDInDtlSrBean.setLogUserId(session.getAttribute("userId").toString());
            }

            int templateId = 0;
            if(request.getParameter("templateId")!=null){
                templateId = Integer.parseInt(request.getParameter("templateId"));
            }
            String procType = "";
            List<TblTemplateMaster> templateMasterLst = defineSTDInDtlSrBean.getTemplateMaster((short) templateId);
            if(templateMasterLst != null){
                if(templateMasterLst.size() > 0){
                    procType = templateMasterLst.get(0).getProcType();
                }
                templateMasterLst = null;
            }
            
            if(request.getParameter("ittHeaderId") == null || request.getParameter("sectionId") == null){
                return;
            }
            int ittHeaderId = Integer.parseInt(request.getParameter("ittHeaderId"));
            int sectionId = Integer.parseInt(request.getParameter("sectionId"));
            String subSectionName = sectionClauseSrBean.getSubSectionName(ittHeaderId);

            String contentType = createSubSectionSrBean.getContectType(sectionId);            
            if ("ITT".equals(contentType)) {  contentType="ITB";} 
            else if("TDS".equals(contentType)){contentType="BDS";} 
            else if("PCC".equals(contentType)){contentType="SCC";} 
            String contentType1 = "";
            if(contentType.equalsIgnoreCase("ITT")){
                contentType1 = "BDS";
            }else if(contentType.equalsIgnoreCase("ITC")){
                contentType1 = "PDS";
            }else if(contentType.equalsIgnoreCase("GCC")){
                contentType1 = "SCC";
            }

            
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View <%=contentType%></title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            ul li {margin-left: 20px;}
        </style>
    </head>
    <body>

        <div class="mainDiv">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="fixDiv">
                <!--Middle Content Table Start-->
                <div class="contentArea_1">
                <div class="pageHead_1 t_space">Prepare <%=contentType%></div>
                
                            <!--Page Content Start-->

                <table width="100%" cellspacing="10" >
                    <tr>
                        <td align="left" >
                        <a class="action-button-goback" href="DefineSTDInDtl.jsp?templateId=<%=request.getParameter("templateId")%>">
                                Go back to SBD dashboard</a></td>
                        <td align="right">
                        <a class="action-button-goback" href="subSectionDashBoard.jsp?templateId=<%=request.getParameter("templateId")%>&sectionId=<%=request.getParameter("sectionId") %>">
                                Go back to <%=contentType%> dashboard</a></td>
                    </tr>
                </table>
                    <form action="<%=request.getContextPath()%>/SectionClauseSrBean" method="post"  onsubmit="return validate();">
                        <%--/PrepareTDS.jsp--%>
                        <input type="hidden" name="clauseCount" id="clauseCount" value="1" />
                        <input type="hidden" name="totalClauseCount" id="totalClauseCount" value="1" />
                        <input type="hidden" name="ittHeaderId" id="ittHeaderId" value="<%=ittHeaderId%>" />
                        <input type="hidden" name="sectionId" id="sectionId" value="<%=sectionId%>" />
                        <input type="hidden" name="templateId" id="templateId" value="<%=request.getParameter("templateId")%>" />


                        <table id="clauseData" width="100%" cellspacing="0" class="tableList_1">
                            <tr id="tr_0">
                                <th colspan="2" align="center"><%=subSectionName%></th>
                            </tr>
                            <%
                            List<TblIttClause> tblIttClause = sectionClauseSrBean.getClauseDetail(ittHeaderId);
                            int ittClauseId = -1;
                            for(int i=0; i<tblIttClause.size(); i++){
                                int clauseId = tblIttClause.get(i).getIttClauseId();
                            %>
                            <tr id="tr_<%=(i+1)%>_1">
                                <td style="line-height: 1.75">
                                    <%
                                    if(ittClauseId != tblIttClause.get(i).getIttClauseId()){
                                        ittClauseId = tblIttClause.get(i).getIttClauseId();
                                        out.print(tblIttClause.get(i).getIttClauseName());
                                    } else {
                                        out.print("&nbsp;");
                                    }
                                    %>
                               </td>
                            <%
                                List<TblIttSubClause> tblIttSubClause = sectionClauseSrBean.getSubClauseDetail(clauseId);
                                int j = 0;
                                for(j=0;j<tblIttSubClause.size();j++){
                                    tblIttSubClause.get(j).getIttSubClauseId();
                                    tblIttSubClause.get(j).getIttSubClauseName();
                                    if(j != 0){
                                %>
                                    <tr id="tr_<%=(i+1)%>_<%=(j+1)*2%>">
                                        <td>&nbsp;</td>
                                <%
                                    }
                                %>
                                        <td style="line-height: 1.75">
                                            <%=tblIttSubClause.get(j).getIttSubClauseName()%>
                                        </td>
                                    </tr>

                                <%
                                }
                            }
                            %>
                        </table>
                    </form>
                </div>
                            <!--Page Content End-->
                        
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<%
createSubSectionSrBean = null;
sectionClauseSrBean = null;
%>
<%
    if(defineSTDInDtlSrBean != null){
        defineSTDInDtlSrBean = null;
    }
%>