<%--
    Document   : HelpListing
    Created on : Dec 18, 2010, 5:25:01 PM
    Author     : Rikin
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblHelpManual"%>
<%@page import="com.cptu.egp.eps.web.servicebean.HelpManualSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="helpManualSrBean" class="com.cptu.egp.eps.web.servicebean.HelpManualSrBean"/>
<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Publish Page Help Content</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script type="text/javascript">

            /* validate Add , Edit Help Content */
            function checkData(){
                var flag = 1;

                if(document.getElementById("idhelpUrl").value==""){
                    document.getElementById("idhelpMsg").innerHTML = "<br />Please enter Page Name";
                    flag = 0;
                }
                if(CKEDITOR.instances.idhelpContent.getData() == 0){
                    document.getElementById("idhelpContentMsg").innerHTML = "Please enter Content";
                    flag = 0;
                }
                //alert(document.getElementById("mobMsg").innerHTML.length);
                //alert($.trim(document.getElementById("mobMsg").innerHTML.value)).length);
                if(document.getElementById("mobMsg").innerHTML.length>5){
                    flag = 0;
                }
                if(flag=="1"){
                    return true;
                }else{
                    return false;
                }
            }
            /* validation for page Name checking */
            function checkPageName(pagename){
                try{
                    document.getElementById("idhelpuniqMsg").innerHTML = "";
                    if(pagename!=""){
                        $.ajax({
                            url: "<%=request.getContextPath()%>/HelpGrid?action=checkPage&pageName="+pagename,
                            method: 'POST',
                            async: false,
                            success: function(j) {
                                if(j=="notavail"){
                                    document.getElementById("idhelpuniqMsg").innerHTML = '<span style="color: red; font-weight: bold;" id="mobMsg">Page name already exist</span>';
                                }
                                if(j=="avail"){
                                    document.getElementById("idhelpuniqMsg").innerHTML = '<span style="color: green; font-weight: bold;" id="mobMsg">OK</span>';
                                }
                            }
                        });
                    }
                    return false;
                }catch(e){
                }
            }
        </script>
    </head>
    <body>
        <%
               String userID="0";
               if(session.getAttribute("userId")!=null){
               userID=session.getAttribute("userId").toString();
               }
               helpManualSrBean.setLogUserId(userID);
        %>
        <%
            TblHelpManual tblHelpManual = new TblHelpManual();

            int helpManualId = 0;
            String helpUrl = "";
            String helpContent = "";

            /* Add Help Content */
            if("save".equalsIgnoreCase(request.getParameter("hidaction"))){
                
                helpManualSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                String msg = helpManualSrBean.addHelpManual(request.getParameter("helpUrl"),request.getParameter("helpContent"));

                if(msg.equals("Values Added")){
                    response.sendRedirect("HelpListing.jsp");
                }
            }
            /* Update Help Content */
            if("update".equalsIgnoreCase(request.getParameter("hidaction"))){
                
                helpManualSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                helpUrl = request.getParameter("helpUrl");
                helpContent = request.getParameter("helpContent");
                helpManualId = Integer.parseInt(request.getParameter("helpManualId").toString());
                String msg = helpManualSrBean.updateManual(helpUrl, helpContent, helpManualId);

                if(msg.equals("Value Updated")){
                    response.sendRedirect("HelpListing.jsp?msg=succ");
                }
            }
            helpManualSrBean.setAuditTrail(null);
            /* Edit and View Help Content */
            if("Edit".equals(request.getParameter("action")) || "View".equals(request.getParameter("action"))){
                int id = Integer.parseInt(request.getParameter("id").toString());
                Iterator t = helpManualSrBean.getData(id).iterator();

                while(t.hasNext()){
                   tblHelpManual = (TblHelpManual)t.next();
                   helpManualId = tblHelpManual.getHelpManualId();
                   helpUrl = tblHelpManual.getHelpUrl();
                   helpContent = tblHelpManual.getHelpContent();
                }
            }
        %>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%--<span class="c-alignment-right"><a href="HelpListing.jsp" class="action-button-goback">Go Back To Dashboard</a></span>--%>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                <tr valign="top">
                    <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <%--<jsp:param name="userType" value="<%=userType.toString()%>"/>
                    </jsp:include>--%>
                        <td class="contentArea" style="vertical-align: top;" align="center" valign="middle">
                            <div class="pageHead_1 t-align-left">Publish Page Help Content
                                <span class="c-alignment-right">
<!--                                    <a href="HelpListing.jsp" class="action-button-goback">Go Back to Dashboard</a>-->
                                </span>
                            </div>
                        <form action="HelpContent.jsp"  name="frmhelpcontent" id="idfrmhelpcontent" method="post">
                        <input type="hidden" name="helpManualId" id="idhelpManual" value="<%=helpManualId%>">
                        <% if("Edit".equals(request.getParameter("action"))){ %>
                            <input type="hidden" name="hidaction" id="idaction" value="update"> <!-- set update for action in edit case -->
                        <% }else{ %>
                            <input type="hidden" name="hidaction" id="idaction" value="save"> <!-- set save for action in edit case -->
                        <% } %>
                <table width="100%" cellspacing="10" class="formStyle_1 t_space">
                    <% if(!"View".equals(request.getParameter("action"))){ %>
                    <tr>
                        <td style="font-style: italic" colspan="2" class="t-align-left ff">Fields marked with (*) are Mandatory</td>
                    </tr>
                    <% } %>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <!-- Page Name field -->
                <tr>
                    <td width="15%" class="t-align-left ff">Page Name: <% if(!"View".equals(request.getParameter("action"))){ %><span class="mandatory">*</span><%}%> </td>
                    <td width="85%" class="t-align-left">
                        <% if(!"View".equals(request.getParameter("action"))){ %>
                            <input name="helpUrl" type="text" class="formTxtBox_1" id="idhelpUrl" style="width:60%;" value="<%=helpUrl%>" onblur="checkPageName(this.value)" />
                        <% }else{ %>
                            <%=helpUrl%>
                        <% } %>
                        <span id="idhelpMsg" class="reqF_2" >&nbsp;</span>
                        <span id="idhelpuniqMsg"></span>
                    </td>
                </tr>
                <!-- Page Content field -->
                <tr>
                    <td class="t-align-left ff">Content: <% if(!"View".equals(request.getParameter("action"))){ %><span class="mandatory">*</span><%}%>
                    <td class="t-align-left">
                        <% if(!"View".equals(request.getParameter("action"))){ %>
                        <textarea  rows="5" id="idhelpContent" name="helpContent" class="formTxtBox_1" style="width:40%;"><%=helpContent%></textarea>
                        <span id="idhelpContentMsg" class="reqF_2">&nbsp;</span>
                        <script type="text/javascript">
                          CKEDITOR.replace( 'idhelpContent',{
                               toolbar : "egpToolbar"
                          });
                        </script>
                        <% }else{ %>
                            <%=helpContent%>
                        <% } %>
                    </td>
                </tr>
                <tr>
                    <td class="t-align-left ff">&nbsp;</td>
                    <td width="85" class="t-align-left">
                        <% if("Edit".equals(request.getParameter("action"))){ %>
                        <label class="formBtn_1">
                            <input name="btnsubmit" type="submit" value="Update" onclick="return checkData();" />
                        </label>
                        <% }else if("View".equals(request.getParameter("action"))){ %>
                            <a href="HelpContent.jsp?action=Edit&id=<%=helpManualId%>" class="anchorLink" style="text-decoration: none; color: #fff;">Edit</a>
                        <%}else{%>
                        <label class="formBtn_1">
                            <input name="btnsubmit" type="submit" value="Submit" onclick="return checkData();" />
                        </label>
                        <%}%>
                    </td>
                </tr>
                </table>
            </form>
                    </td>
                </tr>
            </table>            
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <script>
                var obj = document.getElementById('lblHelpAddContent');
                if(obj != null){
                    if(obj.innerHTML == 'Create'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
                <script>
                var headSel_Obj = document.getElementById("headTabContent");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
     </body>
</html>
