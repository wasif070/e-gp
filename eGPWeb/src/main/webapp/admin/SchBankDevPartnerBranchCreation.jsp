<%-- 
    Document   : SchedulebankBranchCreation
    Created on : Oct 23, 2010, 7:37:24 PM
    Author     : rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem" %>
<jsp:useBean id="scBankCreationSrBean" class="com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean" scope="request"/>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%

            String partnerType = request.getParameter("partnerType");
            String type = "";
            String title = "";
            String userType = "";
            String branch = "Branch";
            String headOffice = "Head Office";
            String state="Dzongkhag / District";
            String branch1 = "Branch";
            String headOffice1 = "";
            String mainOfc = "";
            String str_validationMsg = "";
            if (partnerType != null && partnerType.equals("Development")) {
                type = "Development";
                title = "Development Partner";
                userType = "dev";
                branch = "Regional Office";
                branch1 = "Regional/Country Office";
                headOffice1 = "Head Office";
                str_validationMsg = "Regional/Country Office";
            } else {
                type = "ScheduleBank";
                title = "Financial Institution";
                userType = "sb";
                headOffice = "Financial Institution";
                headOffice1="Financial Institution";
                state="Dzongkhag / District";
                str_validationMsg = "Financial Institution Branch";
            }
 
%>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=title%> <%=branch%> Creation</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $.validator.addMethod(
                "regex",
                 function(value, element, regexp) {
                 var check = false;
                 return this.optional(element) || regexp.test(value);
                 },
                 "Please check your input."
                 );
                $('#btnSubmit').attr("disabled", false);
                $("#frmbankScheduleBranch").validate({
                    rules: {
                        branchHead: { required: true},
                        swiftCode:{maxlength:20},
                        officeAddress:{required:true,maxlength:300},
                        city:{spacevalidate:true,alphaCity: true,maxlength:100},
                        thanaUpzilla:{requiredWithoutSpace: true,spacevalidate:true,maxlength:100,alphaCity: true },
                        phoneNo: {required: true, regex: /^[0-9]+-[0-9,]*[0-9]$/},
                        faxNo: {regex: /^[0-9]+-[0-9,]*[0-9]$/},
                        postCode:{number:true,minlength:4}
                       
                    },
                    messages: {
                        branchHead: { required: "<div class='reqF_1'>Please select <%=headOffice1%></div>"},
                        swiftCode:{maxlength:"<div class='reqF_1'>Max 20 characters are allowed</div>"
                        },

                        officeAddress:{required:"<div class='reqF_1'>Please enter Address</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 300 characters only</div>"
                        },                        
                        city:{//requiredWithoutSpace: "<div class='reqF_1'>Please enter City / Town Name</div>",
                            alphaCity:"<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , ' \" } { - . _) Only</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>",
                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>"
                        },
                    
                        thanaUpzilla:{requiredWithoutSpace: "<div class='reqF_1'>Please enter Thana / UpaZilla</div>",
                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>",
                            alphaCity: "<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , ' \" } { - . _) Only</div>"
                        },
                        phoneNo: {required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            regex: "<div class='reqF_1'>Give dash (-) after Area Code.</br>Use comma to separate alternative Phone no.</br>Do not repeat Area Code.</div>",
                            },
                        faxNo: {regex: "<div class='reqF_1'>Give dash (-) after Area Code.</br>Use comma to separate alternative Fax no.</br>Do not repeat Area Code.</div>",
                            },
                        postCode:{//required:"<div class='reqF_1'>Please enter Post code.</div>",
                            number: "<div class='reqF_1'>Please enter numbers only</div>",
                            maxlength:"<div class='reqF_1'>Maximum 4 Characters are allowed.</div>",
                            minlength:"<div class='reqF_1'>Minimum 4 digits are required.</div>"
                        }
                    },

                    errorPlacement:function(error ,element)
                    {
                       if(element.attr("name")=="phoneNo")
                        {
                            error.insertAfter("#phno");
                        }
                        else if(element.attr("name")=="faxNo")
                        {
                            error.insertAfter("#fxno");
                        }
                        else
                            error.insertAfter(element);
                    }
                });
            });
        </script>

        <script type="text/javascript">
            $(function() {
                $('#cmbcountry').change(function() {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbcountry').val(),funName:'stateCombo'},  function(j){
                        $('#cmbState').children().remove().end().append('<option selected value="0">-- Select --</option>') ;
                        $("select#cmbState").html(j);
                    });
                    if($('#cmbCountry option:selected').text() == "Bhutan" && $('#partnerType').val() == "ScheduleBank")
                        $('#upjilla').css("display", "table-row");
                    else
                        $('#upjilla').css("display", "none");
                });
            });
        </script>
        <script type="text/javascript">
            function alphanumeric(value)
                {
                    return /^[\sa-zA-Z0-9\'\-]+$/.test(value);
                }
            function numeric(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
            }
            function ChkMaxLength(value)
            {
                lenth = 4;
                if(partnerType == 'Development'){
                lenth = 10;
                }
                var ValueChk=value;
                if(ValueChk.length>lenth)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        function ValPost()
            {
                var flag = true;
//                var partnerType = '<%= type%>';
//                if($('#txtpostCode').val() != '')
//                {
//                    if(partnerType == 'Development')
//                    {
//                        //alert("development condition");
//                        if(alphanumeric($('#txtpostCode').val()))
//                        {
//                            if(!ChkMaxLength($('#txtpostCode').val()))
//                            {
//                                $('span.#postCodeMsg').html("<br/>Postcode should comprise of 10 digits only");
//                                flag=false;
//                            }
//                            else
//                            {
//                                $('span.#postCodeMsg').html("");
//                            }
//                        }
//                        else
//                        {
//                            $('span.#postCodeMsg').html("<br/>Please enter Only Alphanumeric Value");
//                            flag=false;
//                        }
//                    }
//                    else
//                    {
//                        //alert("sc bank. condition");
//                        if(numeric($('#txtpostCode').val()))
//                        {
//                            if(!ChkMaxLength($('#txtpostCode').val()))
//                            {
//                                $('span.#postCodeMsg').html("<br/>Postcode should comprise of 4 digits only");
//                                flag=false;
//                            }
//                            else
//                            {
//                                $('span.#postCodeMsg').html("");
//                            }
//                        }
//                        else
//                        {
//                            $('span.#postCodeMsg').html("<br/>Please enter Numbers Only");
//                            flag=false;
//                        }
//                    }
//                }
//                else
//                {
//                    $('span.#postCodeMsg').html("<br/>Please enter Post code.");
//                    flag=false;
//                }
                return flag;
            }
            function chkregPostCode(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function chkreg(value)
            {
                //return /^[a-zA-Z 0-9](?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9]+$)+$/.test(value);
                
                //FINAL,DOESN'T ALLOW SINGLE CHARACTER,SINGLE DIGIT,SINGLE SPECIAL CHARACTERS
                //ALLOWS CHARACTER+DIGIT,CHARACTER+SPECIAL CHARACTERS
                return /^([a-zA-Z 0-9]([a-zA-Z 0-9\s\[\]\(\)\-\.\&\"\'\{\}\_\)])+$)/.test(value);
                

            }
         //   var bvalidationName;
            /*var bvalidationPostCode;
            $(function() {
                $('#txtpostCode').blur(function() {
                    bvalidationPostCode = true;
                    if($('#txtpostCode').val()==''){
                        $('#postCodeMsg').html('Please enter Postcode');
                        bvalidationPostCode = false;
                    }else if(!chkregPostCode($('#txtpostCode').val())){
                        $('#postCodeMsg').html('Please enter digits (0-9) only');
                        bvalidationPostCode = false;
                    }else if($('#txtpostCode').val().length > 4 || $('#txtpostCode').val().length < 4){
                        $('#postCodeMsg').html('Postcode should comprise of 4 digits only');
                        bvalidationPostCode = false;
                    }else{
                        $('#postCodeMsg').html('');
                    }
                });
            });*/
          /*  $(function() {
                $('#txtBranchName').blur(function() {
                    bvalidationName = true;
                    var flag=document.getElementById('txtBranchName').value;
                    if($('#txtBranchName').val() == ''){
                        $('#officeMsg').html("");
                        $('#branchValidation').html('Please enter <%=str_validationMsg%>');
                        bvalidationName = false;
                    }else if(flag.charAt(0) == " "){
                        $('#officeMsg').html("");
                        $('#branchValidation').html('Only Space is not allowed');
                        bvalidationName = false;
                    }else if(!chkreg($('#txtBranchName').val())){
                        $('#officeMsg').html("");
                        $('#branchValidation').html('Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' " } { - . _) Only');
                        bvalidationName = false;
                    }else if(flag.length > 100){
                        $('#officeMsg').html("");
                        $('#branchValidation').html('Allows maximum 100 characters only');
                        bvalidationName = false;
                    }else{
                        $('#branchValidation').html('');
                        $.post("<%=request.getContextPath()%>/ScBankServlet", {title: "<%=title%>",type: "<%=type%>",headOffice: $("#cmdBank option:selected").val(),officeName:$('#txtBranchName').val(),isBranch: 'Yes',funName:'verifyOffice',mainOffice: "<%=mainOfc%>"},
                        function(j){
                            if(j.toString().indexOf("OK", 0)!=-1){
                                $('#officeMsg').css("color","green");
                            }
                            else{
                                $('#officeMsg').css("color","red");
                            }
                            $('#officeMsg').html(j);
                        });
                    }
                });
            });*/
        </script>
        <script type="text/javascript">
            $(function() {
                $('#cmbCountry').change(function() {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbCountry').val(),funName:'stateCombo'},  function(j){
                        $('#cmbState').children().remove().end().append('<option selected value="0">-- Select --</option>') ;
                        $("select#cmbState").html(j);
                    });
                    if($('#cmbCountry option:selected').text() == "Bhutan" && $('#partnerType').val() == "ScheduleBank")
                        $('#upjilla').show();
                    else
                        $('#upjilla').hide();
                });
            });
        </script>
        <script type="text/javascript">
                var bValidation;
            $(function() {
                $('#btnSubmit').click(function() {
                    bValidation = true;
                    if($('#txtBranchName').val()==''){
                        $('#branchValidation').html('Please enter <%=str_validationMsg%>');
                        bValidation = false;
                    }
//                    if($('#txtpostCode').val()==''){
//                        $('#postCodeMsg').html('Please enter Postcode');
//                        bValidation = false;
//                    }
                });
            });
            $(function() {
                $('#frmbankScheduleBranch').submit(function() {
                    if($('#officeMsg').html()=="OK" && bValidation == true && bvalidationName == true && $('#postCodeMsg').html().length==0 && $('#frmbankScheduleBranch').valid()){
                        
                         $('#btnSubmit').attr("disabled", true);
                        return true;
                       
                    }else{
                        return false;
                    }
                });
            });
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%
                        String logUserId = "0";
                        if(session.getAttribute("userId")!=null){
                                logUserId = session.getAttribute("userId").toString();
                            }
                        scBankCreationSrBean.setLogUserId(logUserId);
                %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1">
                            <div class="pageHead_1">Create <%=title%> <%=branch1%></div>
                            <!--Page Content Start-->
                            <!-- Success failure -->
                            <%
                                        String msg = request.getParameter("msg");
                                        if (msg != null && msg.equals("fail")) {%>
                            <div class="responseMsg errorMsg"><%=branch%> Creation Failed.</div>
                            <%} else if (msg != null && msg.equals("success")) {%>
                            <div class="responseMsg successMsg"><%=branch%> Created Successfully</div>
                            <%}%>
                            <!-- Result Display End-->
                            <form id="frmbankScheduleBranch" name="frmbankScheduleBranch" action="<%=request.getContextPath()%>/ScBankServlet" method="POST">
                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10" width="100%">
                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="ff" align="left" >Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                        
                                    </tr>
                                    <tr>
                                        <%
                                                    if (userTypeId != 1) {
                                                        int userId = Integer.parseInt(session.getAttribute("userId").toString());
                                                        scBankCreationSrBean.getBankName(userId, (byte) userTypeId);
                                                        mainOfc = String.valueOf(scBankCreationSrBean.getsBankDevelopId());
                                        %>
                                        <td class="ff" width="200"><%=headOffice%>:</td>
                                        <td width="200"><%=scBankCreationSrBean.getSbDevelopName()%>
                                            <input type="hidden" name="branchHead" id="cmdBank" value="${scBankCreationSrBean.sBankDevelopId}"/>
                                            <%} else {%>
                                        <td class="ff"  width="200">Select <%=headOffice%> : <span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmdBank" name="branchHead" style="width: 200px">
                                                <option value="" selected="selected">-- Select <%=headOffice%> --</option>
                                                <%
                                                   for (SelectItem officeItem : scBankCreationSrBean.getBankPartnerList(type)) {
                                                %>
                                                <option value="<%=officeItem.getObjectId()%>"><%=officeItem.getObjectValue()%></option>
                                                <%}%>
                                            </select> <%}%>
                                            <input type="hidden" name="partnerType" value="<%=type%>"/>
                                            <input type="hidden" name="isBranch" value="Yes"/>
                                            <input type="hidden" name="action" value="create"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Name of <%=branch1%> : <span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtBranchName" name="bankName" style="width: 200px;" type="text" maxlength="101"/>
                                            <div id="officeMsg" style="font-weight: bold"></div>
                                            <div id="branchValidation" class="reqF_1"></div>
                                        </td>
                                    </tr>
                                    <%
                                        if (type.equals("ScheduleBank")) {
                                    %>
                                    <tr>
                                        <td class="ff" width="200">Swift Code : </td>
                                        <td><input class="formTxtBox_1" id="txtswiftCode" name="swiftCode" style="width: 200px;" type="text" maxlength="21"/>
                                        </td>
                                    </tr>
                                    <%}%>
                                    <tr>
                                        <td class="ff" width="200">Address : <span>*</span></td>
                                        <td><textarea class="formTxtBox_1" cols="20" rows="3" id="taaddress" name="officeAddress" style="width: 200px" onkeypress="return imposeMaxLength(this, 301, event);"></textarea>
                                        </td>
                                    </tr>
                                     
                                    <tr>
                                        <%if(type.equals("ScheduleBank")){%>
                                        <td class="ff" width="200">Country : </td>
                                        <%}else{%>
                                        <td class="ff" width="200">Country : <span>*</span></td>
                                        <%}%>
                                        <td>
                                            <%if(!type.equals("ScheduleBank")){%>
                                            <select class="formTxtBox_1" id="cmbCountry" name="country" style="width: 208px">
                                                <%
                                                            for (SelectItem countryItem : scBankCreationSrBean.getCountryList()) {
                                                                if (countryItem.getObjectValue().equalsIgnoreCase("Bhutan")) {
                                                %>
                                                <option  value="<%=countryItem.getObjectId()%>" selected="selected"><%=countryItem.getObjectValue()%></option>
                                                <%
                                                                                                    } else {
                                                %>
                                                <option  value="<%=countryItem.getObjectId()%>"><%=countryItem.getObjectValue()%></option>
                                                <%  }
                                                            }%>
                                            </select>
                                            <%}else{
                                            for (SelectItem countryItem : scBankCreationSrBean.getCountryList()) {
                                                                if (countryItem.getObjectValue().equalsIgnoreCase("Bhutan")) {
                                                %>
                                                <label id="cmbCountry" name="country" ><%=countryItem.getObjectValue()%></label>
                                                <input type="hidden"  name="country" value="<%=countryItem.getObjectId()%>" />
                                            <%}}}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200"><%=state%> : <span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbState" name="state" style="width: 208px">
                                                <%

                                                            for (SelectItem stateItem : scBankCreationSrBean.getStateList((short) 150)) {
                                                                if (stateItem.getObjectValue().equalsIgnoreCase("Thimphu")) {
                                                %>
                                                <option  value="<%=stateItem.getObjectId()%>" selected="selected"><%=stateItem.getObjectValue()%></option>
                                                <%
                                                                                                                } else {
                                                %>
                                                <option  value="<%=stateItem.getObjectId()%>"><%=stateItem.getObjectValue()%></option>
                                                <%  }
                                                            }%>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">City / Town : </td>
                                        <td><input class="formTxtBox_1" id="txtCity" name="city" style="width: 200px;" type="text" maxlength="101"/>
                                        </td>
                                    </tr>
                                    
                                    <%
                                                if (type.equals("ScheduleBank")) {
                                    %>
<!--                                    <tr id="upjilla">
                                        <td class="ff" width="200">Thana / UpaZilla : <span>*</span></td>
                                        <td><input id="txtUpJilla" class="formTxtBox_1" name="thanaUpzilla" style="width: 200px;" type="text" maxlength="101"/>
                                        </td>
                                    </tr>-->
                                    <%}%>
                                    <tr>
                                        <%String maxLen = "maxlength='11'";
                                        if(type.equals("ScheduleBank")){
                                            maxLen = "maxlength='5'";
                                            }%>


                                        <td class="ff" width="200">Postcode : </td>
                                        <td><input id="txtpostCode" class="formTxtBox_1" name="postCode" style="width: 200px;" type="text" maxlength="19"/>
                                            <span id="postCodeMsg" class="reqF_1"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Phone No. : <span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtPhoneNo" maxlength="245" style="width: 200px;" name="phoneNo" type="text"/>&nbsp;
                                            <%if (type.equals("ScheduleBank")) {%>
                                            <span id="phno" style="color: grey;"> (Area Code-Phone No. e.g. 2-336962,336963)</span>
                                            <%} else {%>
                                            <span id="phno" style="color: grey;"> (Area Code-Phone No. e.g. 2-336962,336963)</span>
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Fax No. :</td>
                                        <td><input class="formTxtBox_1" id="txtFaxNo" style="width: 200px;" name="faxNo" type="text" maxlength="245"/>&nbsp;
                                            <%if (type.equals("ScheduleBank")) {%>
                                            <span id="fxno" style="color: grey;"> (Area Code-Fax No. e.g. 02-336961,336963)</span>
                                            <%} else {%>
                                            <span id="fxno" style="color: grey;"> (Area Code-Fax No. e.g. 02-336961,336963)</span>
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <label class="formBtn_1"><input id="btnSubmit" name="Submit" value="Submit" type="submit" onclick="return ValPost();" disabled/>
                                            </label></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                     <script>
                        var partnerType ='<%=partnerType %>';
                       
                            if(partnerType == 'Development'){
                            var obj = document.getElementById('lblDevPartnerSchBankCreation');
                            
                            if(obj != null){
                                
                               // if(obj.innerHTML == 'Create'){
                                    obj.setAttribute('class', 'selected');
                                //}
                            }
                        }else{
                             var obj1 = document.getElementById('lblSchBankDevCreation');
                             
                                if(obj1 != null){
                                   // if(obj1.innerHTML == 'Create'){
                                        obj1.setAttribute('class', 'selected');
                                   // }
                                }
                        }
                                 var bvalidationName;
            /*var bvalidationPostCode;
            $(function() {
                $('#txtpostCode').blur(function() {
                    bvalidationPostCode = true;
                    if($('#txtpostCode').val()==''){
                        $('#postCodeMsg').html('Please enter Postcode');
                        bvalidationPostCode = false;
                    }else if(!chkregPostCode($('#txtpostCode').val())){
                        $('#postCodeMsg').html('Please enter digits (0-9) only');
                        bvalidationPostCode = false;
                    }else if($('#txtpostCode').val().length > 4 || $('#txtpostCode').val().length < 4){
                        $('#postCodeMsg').html('Postcode should comprise of 4 digits only');
                        bvalidationPostCode = false;
                    }else{
                        $('#postCodeMsg').html('');
                    }
                });
            });*/
            $(function() {
                $('#txtBranchName').blur(function() {
                    bvalidationName = true;
                    var flag=document.getElementById('txtBranchName').value;
                    if($('#txtBranchName').val() == ''){
                        $('#officeMsg').html("");
                        $('#branchValidation').html('Please enter <%=str_validationMsg%>');
                        bvalidationName = false;
                    }else if(flag.charAt(0) == " "){
                        $('#officeMsg').html("");
                        $('#branchValidation').html('Only Space is not allowed');
                        bvalidationName = false;
                    }else if(!chkreg($('#txtBranchName').val())){
                        $('#officeMsg').html("");
                        $('#branchValidation').html('Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' " } { - . _) Only');
                        bvalidationName = false;
                    }else if(flag.length > 100){
                        $('#officeMsg').html("");
                        $('#branchValidation').html('Allows maximum 100 characters only');
                        bvalidationName = false;
                    }else{
                        $('#branchValidation').html('');
                        $.post("<%=request.getContextPath()%>/ScBankServlet", {title: "<%=title%>",type: "<%=type%>",headOffice:$('#cmdBank').val(),officeName:$('#txtBranchName').val(),isBranch: 'Yes',funName:'verifyOffice',mainOffice: "0"},
                        function(j){
                            if(j.toString().indexOf("OK", 0)!=-1){
                                $('#officeMsg').css("color","green");
                            }
                            else{
                                $('#officeMsg').css("color","red");
                            }
                            $('#officeMsg').html(j);
                        });
                    }
                });
            });
                    </script>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var obj = document.getElementById('lblRegionalOfficeCreation');
                
                if(obj != null){
                    //if(obj.innerHTML == 'Create Regional/Country Office'){
                        obj.setAttribute('class', 'selected');
                   // }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
