<%--
    Document   : ViewPEAdminA
    Created on : Nov 17, 2010, 2:23:04 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<%@page import="com.cptu.egp.eps.model.table.TblUserActivationHistory"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.databean.ManageAdminDtBean" %>
<%@page import="com.cptu.egp.eps.model.table.TblOfficeMaster" %>
<%@page import="java.util.List" %>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View PE Admin</title>
        <jsp:useBean id="manageAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.UpdateAdminSrBean"/>
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
       
        <%

                    int uTypeID = 0;
                    if (session.getAttribute("userTypeId") != null) {
                        uTypeID = Integer.parseInt(session.getAttribute("userTypeId").toString());
                    }
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        manageAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                    }
                    StringBuilder userType = new StringBuilder();
                    if (request.getParameter("hdnUserType") != null) {
                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                            userType.append(request.getParameter("hdnUserType"));
                        } else {
                            userType.append("org");
                        }
                    } else {
                        userType.append("org");
                    }
                    byte userTypeid = 0;
                    if (request.getParameter("userTypeid") != null) {
                        userTypeid = Byte.parseByte(request.getParameter("userTypeid"));
                    }
                    int userId = 0;
                    if (request.getParameter("userId") != null) {
                        userId = Integer.parseInt(request.getParameter("userId"));
                    }
                    
                    // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    String idType="userId";
                    int auditId=Integer.parseInt(request.getParameter("userId"));
                    String auditAction="View Account History of PE Admin";
                    if(request.getParameter("userTypeid") !=null && request.getParameter("userTypeid").equalsIgnoreCase("4"))
                        auditAction="View Account History of PE Admin";
                    else if(request.getParameter("userTypeid") !=null && request.getParameter("userTypeid").equalsIgnoreCase("12"))
                        auditAction="View Account History of Expert User";
                    
                    String moduleName=EgpModule.Manage_Users.getName();
                    String remarks="";
                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
           

        %>
        
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                         <jsp:include page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                        <td class="contentArea_1">
                            <div class="pageHead_1">PE Admin Account Status</div>
                            <form method="POST" name ="frmViewPEAdmin" id="frmViewPEAdmin">
                                <table class="formStyle_1" border="0" width="100%" cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td class="ff" width="200">Organization :</td>
                                        <%
                                                    List<ManageAdminDtBean> officeListDetails = manageAdminSrBean.getOfficeList(userId);
                                                    String deptName = "";
                                                    for (ManageAdminDtBean officelist : officeListDetails) {
                                                        deptName = officelist.getDepartmentName();
                                                    }
                                        %>

        <%
                    TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
                    List<TblUserActivationHistory> list = transferEmployeeServiceImpl.viewAccountStatusHistory(userId+"");
                    boolean flag = false;
                    int j = 1;
        %>
                                        <td><label><%=deptName%></label></td>
                                    </tr>
                                    <%
                                                for (ManageAdminDtBean peAdminDetails : manageAdminSrBean.getManagePEDetail(userTypeid, userId)) {
                                    %>
                                    <tr>
                                        <td class="ff" width="200">Dzongkhag / District :</td>
                                        <td>
                                            <label id="lblDistName"><%=peAdminDetails.getStateName()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Office :</td>
                                        <td>

                                            <label id="lblOfficeName"><%=officeListDetails.get(0).getOfficeName()%><br/></label>
                                                <%--<option value="<%= oName.getOfficeId()%>" <%for(Object object : officeList){if(object.equals(oName.getOfficeName())){%>selected<%}}%>><%= oName.getOfficeName()%></option>--%>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">e-mail ID :</td>
                                        <td>
                                            <label id="lblEmailId"><%= peAdminDetails.getEmailId()%></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff" width="200">Full Name :</td>
                                        <td><label id="lblFullName"><%=peAdminDetails.getFullName()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200"> National ID :</td>
                                        <td>
                                            <label id="lblNationalId"><%= peAdminDetails.getNationalId()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Phone No. :</td>
                                        <td><label id="lblPhoneNumber"><%=peAdminDetails.getPhoneNo()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Mobile No. :</td>
                                        <td><label id="lblMobileNumber"><%=peAdminDetails.getMobileNo()%></label>
                                        </td>
                                    </tr><%}%>
                                </table>
                                         <table class="tableList_1 t_space" cellspacing="0" width="100%">
                                            <tr>
                                                <th class="t-align-center">Sl. No.</th>
                                                <th class="t-align-center">Full Name</th>
                                                <th class="t-align-center">Status</th>
                                                <th class="t-align-center">Action By</th>
                                                <th class="t-align-center">Action Date</th>
                                                <th class="t-align-center">Reason</th>
                                            </tr>
                                            <%

                                                        for (int i = 0; i < list.size(); i++) {
                                                            String strFullName = "-";
                                                            String straction = "-";
                                                            String strComments = "-";
                                                            String strdt = "-";

                                                            if (!"".equalsIgnoreCase(list.get(i).getAction())) {
                                                                straction= list.get(i).getAction();
                                                                flag = true;
                                                                    strFullName = list.get(i).getFullName();
                                                                if (!"".equalsIgnoreCase(list.get(i).getComments())) {
                                                                    strComments = list.get(i).getComments();
                                                                }
                                                                if (!"".equalsIgnoreCase(list.get(i).getAction())) {
                                                                    straction = list.get(i).getAction();
                                                                }
                                            %>
                                            <tr
                                                <%if (i % 2 == 0) {%>
                                                class="bgColor-white"
                                                <%} else {%>
                                                class="bgColor-Green"
                                                <%   }%>
                                                >
                                                <td class="t-align-center"> <%=j%>
                                                    <%j++;%>
                                                </td>
                                                <td class="t-align-center"><%=strFullName%></td>
                                                <td class="t-align-center"><%=straction%></td>
                                                <td class="t-align-center"><%=list.get(i).getActionByName()%></td>
                                                <td class="t-align-center"><%=DateUtils.gridDateToStr(list.get(i).getActionDt())%></td>
                                                 <td class="t-align-center"><%=strComments%></td>

                                            </tr>
                                            <% }
                                                        }
                                                        if (!flag) {
                                            %>
                                            <td class="t-align-center" colspan="6" style="color: red;font-weight: bold">No Record Found</td>
                                            <%}%>


                                        </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script type="text/javascript">
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
    <%
                manageAdminSrBean = null;
    %>
</html>
