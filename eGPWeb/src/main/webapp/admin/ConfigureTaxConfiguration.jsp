<%-- 
    Document   : ConfigureTaxConfiguration
    Created on : Jul 7, 2011, 3:16:58 PM
    Author     : Sreenu.Durga
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsTaxConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsTaxConfigBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <title>Tax Configuration Page</title>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
    </head>
    <%
                CmsTaxConfigBean cmsTaxConfigBean = new CmsTaxConfigBean();
                if (session.getAttribute("userId") != null) {
                    cmsTaxConfigBean.setLogUserId(session.getAttribute("userId").toString());
                }

                final int GOODS_PROCUREMENT_NATURE_ID = 1;
                final int WORKS_PROCUREMENT_NATURE_ID = 2;
                final int SERVICES_PROCUREMENT_NATURE_ID = 3;

                boolean isEdit = false;
                final String YES = "yes";
                final String NO = "no";

                TblCmsTaxConfig currentTblCmsTaxConfig = null;

                StringBuilder userType = new StringBuilder();
                if (request.getParameter("userType") != null) {
                    if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                        userType.append(request.getParameter("userType"));
                    } else {
                        userType.append("org");
                    }
                } else {
                    userType.append("org");
                }

                int natureId = 0;
                if (request.getParameter("natureType") != null) {
                    natureId = Integer.parseInt(request.getParameter("natureType"));
                    currentTblCmsTaxConfig = cmsTaxConfigBean.getLatestCmsTaxConfiguration(natureId);
                }
                int taxConfigId = 0;
                if (request.getParameter("taxConfigId") != null) {
                    taxConfigId = Integer.parseInt(request.getParameter("taxConfigId"));
                    isEdit = true;
                }

    %>
    <script Language="JavaScript" >
        function checkPercentageRange(control) {
            var enteredValue=control.value;
            var flag=false;
            if((enteredValue <0) || (enteredValue>100)){
                jAlert('Percentage should be 0 to 100 only', 'Warning!');
                control.value = "";
                control.focus();
                flag = false;                
            }else{
                flag = true;
            }
            return flag;
        }
        function checkPercentageWithRetention(control) {
            var enteredValue=control.value;
            var flag=false;
            if((enteredValue <0) || (enteredValue>100)){
                jAlert('Percentage should be 0 to 100 only', 'Warning!');
                control.value = "";
                control.focus();
                flag = false;
            }else{
                var isSelectedRetention =  document.getElementById('selectRetentionMoney').value;
                if(isSelectedRetention == "yes"){
                    var valRetentionFirst = parseInt(document.getElementById('txtRetentionMoneyPerFirst').value);
                    var valPgFirst = parseInt(document.getElementById('txtPgApplicablePerFirst').value);
                    var sum = valRetentionFirst+valPgFirst;
                    if(sum >25 ){
                        jAlert("The Sum of Retention Money Applicable  percentage and Performance Security applicable percentage  should be less than or equal to 25","Warning!");
                        control.value = "";
                        control.focus();
                        flag = false;
                    }else{
                        flag = true;
                    }
                }                
            }
            return flag;
        }
        function setTextBoxProperties(control) {
            var selectedValue=control.value;
            var selectedControlName = control.name.toString().substr(6,control.name.toString().length);
            //6 is 'select' length
            if(control.name == "selectVatApplicable"){
                if(selectedValue == "no"){
                    document.getElementById('txt'+selectedControlName+'PerFirst').style.display = 'none';
                    document.getElementById('txt'+selectedControlName+'PerFirst').value = 0;
                } else if(selectedValue=="yes"){
                    document.getElementById('txt'+selectedControlName+'PerFirst').style.display = 'block';
                    document.getElementById('txt'+selectedControlName+'PerFirst').value ='';
                }
            }else{
                if(selectedValue == "no"){
                    document.getElementById('txt'+selectedControlName+'PerFirst').style.display = 'none';
                    document.getElementById('txt'+selectedControlName+'PerSecond').style.display =  'none';
                    document.getElementById('txt'+selectedControlName+'PerFirst').value = 0;
                    document.getElementById('txt'+selectedControlName+'PerSecond').value = 0;                    
                }else if(selectedValue=="yes"){
                    document.getElementById('txt'+selectedControlName+'PerFirst').style.display = 'block';
                    document.getElementById('txt'+selectedControlName+'PerSecond').style.display =  'block';
                    document.getElementById('txt'+selectedControlName+'PerFirst').value ='';
                    document.getElementById('txt'+selectedControlName+'PerSecond').value =  '';
                }
            }            
            document.getElementById("formEditTaxConfiguration").submit();
            return (true);
        }
    </script>

    <script type="text/javascript">      
        $(document).ready(function() {
            $("#formEditTaxConfiguration").validate({
                rules: {
                    selectAdvanceApplicable:{required: true,alphanumeric:true},
                    txtAdvanceApplicablePerFirst:{ required: true,requiredWithoutSpace: true , number:true},
                    txtAdvanceApplicablePerSecond:{ required: true,requiredWithoutSpace: true , number:true},

                    selectRetentionMoney:{required: true,alphanumeric:true},
                    txtRetentionMoneyPerFirst:{ required: true,requiredWithoutSpace: true , number:true},
                    txtRetentionMoneyPerSecond:{ required: true,requiredWithoutSpace: true , number:true},

                    selectPgApplicable:{required: true,alphanumeric:true},
                    txtPgApplicablePerFirst:{ required: true,requiredWithoutSpace: true , number:true},
                    txtPgApplicablePerSecond:{ required: true,requiredWithoutSpace: true , number:true},

                    selectVatApplicable:{required: true,alphanumeric:true},
                    txtVatApplicablePerFirst:{ required: true,requiredWithoutSpace: true , number:true},

                    selectBonusApplicable:{required: true,alphanumeric:true},
                    txtBonusApplicablePerFirst:{ required: true,requiredWithoutSpace: true , number:true},
                    txtBonusApplicablePerSecond:{ required: true,requiredWithoutSpace: true , number:true},

                    selectLdApplicable:{required: true,alphanumeric:true},
                    txtLdApplicablePerFirst:{ required: true,requiredWithoutSpace: true , number:true},
                    txtLdApplicablePerSecond:{ required: true,requiredWithoutSpace: true , number:true},
                    
                    selectPenaltyApplicable:{required: true,alphanumeric:true},
                    txtPenaltyApplicablePerFirst:{ required: true,requiredWithoutSpace: true , number:true},
                    txtPenaltyApplicablePerSecond:{ required: true,requiredWithoutSpace: true , number:true}
                },
                messages: {
                    selectAdvanceApplicable: { required: "<div class='reqF_1'>Please Select a value</div>",
                        alphanumeric:"<div class='reqF_1'>Please Select a value</div>"
                    },
                    txtAdvanceApplicablePerFirst:{
                        required: "<div class='reqF_1'>Please Enter Percentage</div>",
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" 
                    },
                    txtAdvanceApplicablePerSecond:{
                        required: "<div class='reqF_1'>Please Enter Percentage</div>",
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>"
                    },

                    selectRetentionMoney: { required: "<div class='reqF_1'>Please Select a value</div>",
                        alphanumeric:"<div class='reqF_1'>Please Select a value</div>"
                    },
                    txtRetentionMoneyPerFirst:{
                        required: "<div class='reqF_1'>Please Enter Percentage</div>",
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>"
                    },
                    txtRetentionMoneyPerSecond:{
                        required: "<div class='reqF_1'>Please Enter Percentage</div>",
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>"
                    },

                    selectPgApplicable: { required: "<div class='reqF_1'>Please Select a value</div>",
                        alphanumeric:"<div class='reqF_1'>Please Select a value</div>"
                    },
                    txtPgApplicablePerFirst:{
                        required: "<div class='reqF_1'>Please Enter Percentage</div>",
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>"
                    },
                    txtPgApplicablePerSecond:{
                        required: "<div class='reqF_1'>Please Enter Percentage</div>",
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>"
                    },

                    selectVatApplicable: { required: "<div class='reqF_1'>Please Select a value</div>",
                        alphanumeric:"<div class='reqF_1'>Please Select a value</div>"
                    },
                    txtVatApplicablePerFirst:{
                        required: "<div class='reqF_1'>Please Enter Percentage</div>",
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>"
                    },

                    selectBonusApplicable: { required: "<div class='reqF_1'>Please Select a value</div>",
                        alphanumeric:"<div class='reqF_1'>Please Select a value</div>"
                    },
                    txtBonusApplicablePerFirst:{
                        required: "<div class='reqF_1'>Please Enter Percentage</div>",
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>"
                    },
                    txtBonusApplicablePerSecond:{
                        required: "<div class='reqF_1'>Please Enter Percentage</div>",
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>"
                    },

                    selectLdApplicable: { required: "<div class='reqF_1'>Please Select a value</div>",
                        alphanumeric:"<div class='reqF_1'>Please Select a value</div>"
                    },
                    txtLdApplicablePerFirst:{
                        required: "<div class='reqF_1'>Please Enter Percentage</div>",
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>"
                    },
                    txtLdApplicablePerSecond:{
                        required: "<div class='reqF_1'>Please Enter Percentage</div>",
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>"
                    },

                    selectPenaltyApplicable: { required: "<div class='reqF_1'>Please Select a value</div>",
                        alphanumeric:"<div class='reqF_1'>Please Select a value</div>"
                    },
                    txtPenaltyApplicablePerFirst:{
                        required: "<div class='reqF_1'>Please Enter Percentage</div>",
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>"
                    },
                    txtPenaltyApplicablePerSecond:{
                        required: "<div class='reqF_1'>Please Enter Percentage</div>",
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>"
                    }                   
                },
                errorPlacement:function(error ,element){
                    error.insertAfter(element);
                }
            });
        });
    </script>
    <script type="text/javascript">
        function numeric(value) {
            return /^\d+$/.test(value);
        }
        function checkData(){
            if($('#formEditTaxConfiguration').valid()){
                document.getElementById("formEditTaxConfiguration").action="<%=request.getContextPath()%>/TaxConfigServlet";
                document.getElementById("formEditTaxConfiguration").submit();                            
            }
        }
    </script>

    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%--Dashboard Header Start--%>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%--Dashboard Header End--%>
                <%--Dashboard Body Start--%>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType.toString()%>" ></jsp:include>
                        <td class="contentArea">
                            <div>
                                <div class="pageHead_1">
                                    Tax Configure
                                    <span style="float: right; text-align: right;">
                                        <a class='action-button-goback' href='ViewTaxConfiguration.jsp'>Go back</a>
                                    </span>
                                </div>
                            </div>                           
                            <form id="formEditTaxConfiguration" name="formEditTaxConfiguration" method="post" action='<%=request.getContextPath()%>/TaxConfigServlet' >
                                <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_3 t_space">
                                    <tr>
                                        <th width="87%" valign="top" class="ff">Procurement Type
                                            <input type="hidden" name="natureType" id="natureType" value='<%=natureId%>' />
                                            <input type="hidden" name="taxConfigId" id="taxConfigId" value='<%=taxConfigId%>'/>
                                            <%
                                                        if (GOODS_PROCUREMENT_NATURE_ID == natureId) {
                                                            out.println("<th width='13%'>Goods</th>");
                                                        } else if (WORKS_PROCUREMENT_NATURE_ID == natureId) {
                                                            out.println("<th width='13%'>Works</th>");
                                                        } else {
                                                            out.println("<th width='13%'>Services</th>");
                                                        }
                                            %>
                                        </th>
                                    </tr>
                                    <tr>
                                        <td class="ff red table-left" valign="top" style="color: red">
                                            Is Advance Applicable
                                        </td>
                                        <td>
                                            <center>
                                                <select name="selectAdvanceApplicable" class="formTxtBox_1" id="selectAdvanceApplicable" style="width:auto;" onchange="return setTextBoxProperties(this);" >
                                                    <%
                                                                if (isEdit) {
                                                                    String value = currentTblCmsTaxConfig.getIsAdvanceApplicable();
                                                                    if (YES.equalsIgnoreCase(value)) {
                                                                        out.println("<option value='yes' selected='selected' >Yes</option>");
                                                                        out.println("<option value='no'>No</option>");
                                                                    } else {
                                                                        out.println("<option value='yes'>Yes</option>");
                                                                        out.println("<option value='no' selected='selected' >No</option>");
                                                                    }
                                                                } else {
                                                                    out.println("<option selected='selected' value='^'>- Select -</option>");
                                                                    out.println("<option value='yes'>Yes</option>");
                                                                    out.println("<option value='no'>No</option>");
                                                                }
                                                    %>
                                                </select>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff table-left" valign="top">
                                            Percentage of Advance that can be paid as part of Contract value
                                        </td>
                                        <td>
                                            <center>
                                                <%
                                                            if (isEdit) {
                                                                String selectedValue = currentTblCmsTaxConfig.getIsAdvanceApplicable();
                                                                if (YES.equalsIgnoreCase(selectedValue)) {
                                                                    String val = currentTblCmsTaxConfig.getAdvancePercent() + "";
                                                                    out.print("<input name='txtAdvanceApplicablePerFirst' type='text' size='8' class='formTxtBox_1' value="
                                                                            + val + " style='display:block' id='txtAdvanceApplicablePerFirst'  onblur='return checkPercentageRange(this);'  />");
                                                                } else {
                                                                    out.print("<input name='txtAdvanceApplicablePerFirst' type='text' size='8' class='formTxtBox_1' "
                                                                            + " style='display:none' value='0' id='txtAdvanceApplicablePerFirst'  onblur='return checkPercentageRange(this);' />");
                                                                }
                                                            } else {
                                                                out.print("<input name='txtAdvanceApplicablePerFirst' type='text' size='8' class='formTxtBox_1' "
                                                                        + " style='display:none' value='0' id='txtAdvanceApplicablePerFirst'  onblur='return checkPercentageRange(this);' />");
                                                            }
                                                %>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff table-left" valign="top">
                                            If advance is paid, in what % of Bills it should be adjusted e.g. adjust 100% advance when 60% of bills are cleared, specify 0 if to be proportionately adjusted in all bills
                                        </td>
                                        <td>
                                            <center>
                                                <%
                                                            if (isEdit) {
                                                                String selectedValue = currentTblCmsTaxConfig.getIsAdvanceApplicable();
                                                                if (YES.equalsIgnoreCase(selectedValue)) {
                                                                    String val = currentTblCmsTaxConfig.getAdvanceAdjustment() + "";
                                                                    out.print("<input name='txtAdvanceApplicablePerSecond' type='text' size='8' class='formTxtBox_1' value= "
                                                                            + val + " style='display:block' id='txtAdvanceApplicablePerSecond' onblur='return checkPercentageRange(this);' />");
                                                                } else {
                                                                    out.print("<input name='txtAdvanceApplicablePerSecond' type='text' size='8' class='formTxtBox_1' "
                                                                            + "style='display:none' value='0' id='txtAdvanceApplicablePerSecond' onblur='return checkPercentageRange(this);' />");
                                                                }
                                                            } else {
                                                                out.print("<input name='txtAdvanceApplicablePerSecond' type='text' size='8' class='formTxtBox_1' "
                                                                        + "style='display:none' value='0' id='txtAdvanceApplicablePerSecond' onblur='return checkPercentageRange(this);' />");
                                                            }
                                                %>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff red table-left" valign="top" style="color: red">
                                            Is Retention Money Applicable
                                        </td>
                                        <td>
                                            <center>
                                                <select name='selectRetentionMoney' class='formTxtBox_1' id='selectRetentionMoney' style='width:auto;' onchange='return setTextBoxProperties(this);' >
                                                    <%
                                                                if (isEdit) {
                                                                    String value = currentTblCmsTaxConfig.getIsRetentionApplicable();
                                                                    if (YES.equalsIgnoreCase(value)) {
                                                                        out.println("<option value='yes' selected='selected' >Yes</option>");
                                                                        out.println("<option value='no'>No</option>");
                                                                    } else {
                                                                        out.println("<option value='yes'>Yes</option>");
                                                                        out.println("<option value='no' selected='selected' >No</option>");
                                                                    }
                                                                } else {
                                                                    out.println("<option selected='selected' value='^'>- Select -</option>");
                                                                    out.println("<option value='yes'>Yes</option>");
                                                                    out.println("<option value='no'>No</option>");
                                                                }
                                                    %>
                                                </select>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff table-left" valign="top" >
                                            Retention Money to be deducted as % of contract value
                                        </td>
                                        <td>
                                            <center>
                                                <%
                                                            if (isEdit) {
                                                                String selectedValue = currentTblCmsTaxConfig.getIsRetentionApplicable();
                                                                if (YES.equalsIgnoreCase(selectedValue)) {
                                                                    String val = currentTblCmsTaxConfig.getRetentionPercent() + "";
                                                                    out.print("<input name='txtRetentionMoneyPerFirst' type='text' size='8' class='formTxtBox_1' value=" + val
                                                                            + " id='txtRetentionMoneyPerFirst' style='display:block' onblur='return checkPercentageRange(this);' />");
                                                                } else {
                                                                    out.print("<input name='txtRetentionMoneyPerFirst' type='text' size='8' class='formTxtBox_1' "
                                                                            + " id='txtRetentionMoneyPerFirst' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                                }
                                                            } else {
                                                                out.print("<input name='txtRetentionMoneyPerFirst' type='text' size='8' class='formTxtBox_1' "
                                                                        + " id='txtRetentionMoneyPerFirst' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                            }
                                                %>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff table-left" valign="top">
                                            When should Retention Money be released as % of contract, specify 0 if not applicable
                                        </td>
                                        <td>
                                            <center>
                                                <%
                                                            if (isEdit) {
                                                                String selectedValue = currentTblCmsTaxConfig.getIsRetentionApplicable();
                                                                if (YES.equalsIgnoreCase(selectedValue)) {
                                                                    String val = currentTblCmsTaxConfig.getRetentionAdjustment() + "";
                                                                    out.print("<input name='txtRetentionMoneyPerSecond' type='text' size='8' class='formTxtBox_1' value=" + val
                                                                            + " id='txtRetentionMoneyPerSecond' style='display:block' onblur='return checkPercentageRange(this);' />");
                                                                } else {
                                                                    out.print("<input name='txtRetentionMoneyPerSecond' type='text' size='8' class='formTxtBox_1' "
                                                                            + " id='txtRetentionMoneyPerSecond' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                                }
                                                            } else {
                                                                out.print("<input name='txtRetentionMoneyPerSecond' type='text' size='8' class='formTxtBox_1' "
                                                                        + "id='txtRetentionMoneyPerSecond' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                            }
                                                %>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff red table-left" valign="top" style="color: red">
                                            Is PG Applicable
                                        </td>
                                        <td>
                                            <center>
                                                <select name="selectPgApplicable" class="formTxtBox_1" id="selectPgApplicable" style="width:auto;" onchange="return setTextBoxProperties(this);">
                                                    <%
                                                                if (isEdit) {
                                                                    String value = currentTblCmsTaxConfig.getIsPgapplicable();
                                                                    if (YES.equalsIgnoreCase(value)) {
                                                                        out.println("<option value='yes' selected>Yes</option>");
                                                                        out.println("<option value='no'>No</option>");
                                                                    } else {
                                                                        out.println("<option value='yes'>Yes</option>");
                                                                        out.println("<option value='no' selected>No</option>");
                                                                    }
                                                                } else {
                                                                    out.println("<option selected='selected' value='^'>- Select -</option>");
                                                                    out.println("<option value='yes'>Yes</option>");
                                                                    out.println("<option value='no'>No</option>");
                                                                }
                                                    %>
                                                </select>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff table-left" valign="top">
                                            Performance Security applicable as % of contract value, add 0 if not applicable
                                        </td>
                                        <td>
                                            <center>
                                                <%
                                                            if (isEdit) {
                                                                String selectedValue = currentTblCmsTaxConfig.getIsPgapplicable();
                                                                if (YES.equalsIgnoreCase(selectedValue)) {
                                                                    String val = currentTblCmsTaxConfig.getPgPercent() + "";
                                                                    out.print("<input name='txtPgApplicablePerFirst' type='text' size='8' class='formTxtBox_1' value=" + val
                                                                            + " id='txtPgApplicablePerFirst'   style='display:block' onblur='return checkPercentageWithRetention(this);' />");
                                                                } else {
                                                                    out.print("<input name='txtPgApplicablePerFirst' type='text' size='8' class='formTxtBox_1' "
                                                                            + "id='txtPgApplicablePerFirst' value='0' style='display:none' onblur='return checkPercentageWithRetention(this);' />");
                                                                }
                                                            } else {
                                                                out.print("<input name='txtPgApplicablePerFirst' type='text' size='8' class='formTxtBox_1' "
                                                                        + "id='txtPgApplicablePerFirst' value='0' style='display:none' onblur='return checkPercentageWithRetention(this);' />");
                                                            }
                                                %>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff table-left" valign="top">
                                            What % of contract should be completed to release Performance Security (PG)
                                        </td>
                                        <td>
                                            <center>
                                                <%
                                                            if (isEdit) {
                                                                String selectedValue = currentTblCmsTaxConfig.getIsPgapplicable();
                                                                if (YES.equalsIgnoreCase(selectedValue)) {
                                                                    String val = currentTblCmsTaxConfig.getPgAdjustment() + "";
                                                                    out.print("<input name='txtPgApplicablePerSecond' type='text' size='8' class='formTxtBox_1' value=" + val
                                                                            + " id='txtPgApplicablePerSecond'  style='display:block' onblur='return checkPercentageRange(this);'  />");
                                                                } else {
                                                                    out.print("<input name='txtPgApplicablePerSecond' type='text' size='8' class='formTxtBox_1' "
                                                                            + "id='txtPgApplicablePerSecond' value='0' style='display:none' onblur='return checkPercentageRange(this);'  />");
                                                                }
                                                            } else {
                                                                out.print("<input name='txtPgApplicablePerSecond' type='text' size='8' class='formTxtBox_1' "
                                                                        + "id='txtPgApplicablePerSecond' value='0' style='display:none' onblur='return checkPercentageRange(this);'  />");
                                                            }
                                                %>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff red table-left" valign="top" style="color: red">
                                            Is VAT applicable
                                        </td>
                                        <td>
                                            <center>
                                                <select name="selectVatApplicable" class="formTxtBox_1" id="selectVatApplicable" style="width:auto;" onchange="return setTextBoxProperties(this);">
                                                    <%
                                                                if (isEdit) {
                                                                    String value = currentTblCmsTaxConfig.getIsVatapplicable();
                                                                    if (YES.equalsIgnoreCase(value)) {
                                                                        out.println("<option value='yes' selected>Yes</option>");
                                                                        out.println("<option value='no'>No</option>");
                                                                    } else {
                                                                        out.println("<option value='yes'>Yes</option>");
                                                                        out.println("<option value='no' selected>No</option>");
                                                                    }
                                                                } else {
                                                                    out.println("<option selected='selected' value='^'>- Select -</option>");
                                                                    out.println("<option value='yes'>Yes</option>");
                                                                    out.println("<option value='no'>No</option>");
                                                                }
                                                    %>
                                                </select>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff table-left" valign="top">
                                            Percentage of VAT applicable
                                        </td>
                                        <td>
                                            <center>
                                                <%
                                                            if (isEdit) {
                                                                String selectedValue = currentTblCmsTaxConfig.getIsVatapplicable();
                                                                if (YES.equalsIgnoreCase(selectedValue)) {
                                                                    String val = currentTblCmsTaxConfig.getVatPercent() + "";
                                                                    out.print("<input name='txtVatApplicablePerFirst' type='text' size='8' class='formTxtBox_1' value=" + val
                                                                            + " id='txtVatApplicablePerFirst'  style='display:block' onblur='return checkPercentageRange(this);' />");

                                                                } else {
                                                                    out.print("<input name='txtVatApplicablePerFirst' type='text' size='8' class='formTxtBox_1' "
                                                                            + "id='txtVatApplicablePerFirst' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                                }
                                                            } else {
                                                                out.print("<input name='txtVatApplicablePerFirst' type='text' size='8' class='formTxtBox_1' "
                                                                        + "id='txtVatApplicablePerFirst' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                            }
                                                %>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff red table-left" valign="top" style="color: red">
                                            Is Bonus Applicable
                                        </td>
                                        <td>
                                            <center>
                                                <select name="selectBonusApplicable" class="formTxtBox_1" id="selectBonusApplicable" style="width:auto;" onchange="return setTextBoxProperties(this);">
                                                    <%
                                                                if (isEdit) {
                                                                    String value = currentTblCmsTaxConfig.getIsBonusApplicable();
                                                                    if (YES.equalsIgnoreCase(value)) {
                                                                        out.println("<option value='yes' selected>Yes</option>");
                                                                        out.println("<option value='no'>No</option>");
                                                                    } else {
                                                                        out.println("<option value='yes'>Yes</option>");
                                                                        out.println("<option value='no' selected>No</option>");
                                                                    }
                                                                } else {
                                                                    out.println("<option selected='selected' value='^'>- Select -</option>");
                                                                    out.println("<option value='yes'>Yes</option>");
                                                                    out.println("<option value='no'>No</option>");
                                                                }
                                                    %>
                                                </select>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff table-left" valign="top">
                                            What %  of contract value can be given as Bonus
                                        </td>
                                        <td>
                                            <center>
                                                <%
                                                            if (isEdit) {
                                                                String selectedValue = currentTblCmsTaxConfig.getIsBonusApplicable();
                                                                if (YES.equalsIgnoreCase(selectedValue)) {
                                                                    String val = currentTblCmsTaxConfig.getBonusPercent() + "";
                                                                    out.print("<input name='txtBonusApplicablePerFirst' type='text' size='8' class='formTxtBox_1' value=" + val
                                                                            + " id='txtBonusApplicablePerFirst'  style='display:block' onblur='return checkPercentageRange(this);'/>");
                                                                } else {
                                                                    out.print("<input name='txtBonusApplicablePerFirst' type='text' size='8' class='formTxtBox_1' "
                                                                            + "id='txtBonusApplicablePerFirst' value='0' style='display:none' onblur='return checkPercentageRange(this);'/>");
                                                                }

                                                            } else {
                                                                out.print("<input name='txtBonusApplicablePerFirst' type='text' size='8' class='formTxtBox_1' "
                                                                        + "id='txtBonusApplicablePerFirst' value='0' style='display:none' onblur='return checkPercentageRange(this);'/>");
                                                            }
                                                %>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff table-left" valign="top">
                                            What % of contract should be completed when Bonus can be paid
                                        </td>
                                        <td>
                                            <center>
                                                <%
                                                            if (isEdit) {
                                                                String selectedValue = currentTblCmsTaxConfig.getIsBonusApplicable();
                                                                if (YES.equalsIgnoreCase(selectedValue)) {
                                                                    String val = currentTblCmsTaxConfig.getBonusAfter() + "";
                                                                    out.print("<input name='txtBonusApplicablePerSecond' type='text' size='8' class='formTxtBox_1' value=" + val
                                                                            + " id='txtBonusApplicablePerSecond'  style='display:block' onblur='return checkPercentageRange(this);' />");
                                                                } else {
                                                                    out.print("<input name='txtBonusApplicablePerSecond' type='text' size='8' class='formTxtBox_1' "
                                                                            + "id='txtBonusApplicablePerSecond' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                                }
                                                            } else {
                                                                out.print("<input name='txtBonusApplicablePerSecond' type='text' size='8' class='formTxtBox_1' "
                                                                        + "id='txtBonusApplicablePerSecond' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                            }
                                                %>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff red table-left" valign="top" style="color: red">
                                            Is LD Applicable
                                        </td>
                                        <td>
                                            <center>
                                                <select name="selectLdApplicable" class="formTxtBox_1" id="selectLdApplicable" style="width:auto;" onchange="return setTextBoxProperties(this);">
                                                    <%
                                                                if (isEdit) {
                                                                    String value = currentTblCmsTaxConfig.getIsLdapplicable();
                                                                    if (YES.equalsIgnoreCase(value)) {
                                                                        out.println("<option value='yes' selected>Yes</option>");
                                                                        out.println("<option value='no'>No</option>");
                                                                    } else {
                                                                        out.println("<option value='yes'>Yes</option>");
                                                                        out.println("<option value='no' selected>No</option>");
                                                                    }
                                                                } else {
                                                                    out.println("<option selected='selected' value='^'>- Select -</option>");
                                                                    out.println("<option value='yes'>Yes</option>");
                                                                    out.println("<option value='no'>No</option>");
                                                                }
                                                    %>
                                                </select>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff table-left" valign="top">
                                            What % of Contract value can be levieed as Liquidated damages
                                        </td>
                                        <td>
                                            <center>
                                                <%
                                                            if (isEdit) {
                                                                String selectedValue = currentTblCmsTaxConfig.getIsLdapplicable();
                                                                if (YES.equalsIgnoreCase(selectedValue)) {
                                                                    String val = currentTblCmsTaxConfig.getLdPercent() + "";
                                                                    out.print("<input name='txtLdApplicablePerFirst' type='text' size='8' class='formTxtBox_1' value=" + val
                                                                            + " id='txtLdApplicablePerFirst'  style='display:block' onblur='return checkPercentageRange(this);' />");
                                                                } else {
                                                                    out.print("<input name='txtLdApplicablePerFirst' type='text' size='8' class='formTxtBox_1' "
                                                                            + "id='txtLdApplicablePerFirst' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                                }
                                                            } else {
                                                                out.print("<input name='txtLdApplicablePerFirst' type='text' size='8' class='formTxtBox_1' "
                                                                        + "id='txtLdApplicablePerFirst' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                            }
                                                %>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff table-left" valign="top">
                                            On completion of what % of contract LD damages can be levied
                                        </td>
                                        <td>
                                            <center>
                                                <%
                                                            if (isEdit) {
                                                                String selectedValue = currentTblCmsTaxConfig.getIsLdapplicable();
                                                                if (YES.equalsIgnoreCase(selectedValue)) {
                                                                    String val = currentTblCmsTaxConfig.getLdAfter() + "";
                                                                    out.print("<input name='txtLdApplicablePerSecond' type='text' size='8' class='formTxtBox_1' value=" + val
                                                                            + " id='txtLdApplicablePerSecond'  style='display:block' onblur='return checkPercentageRange(this);' />");
                                                                } else {
                                                                    out.print("<input name='txtLdApplicablePerSecond' type='text' size='8' class='formTxtBox_1' "
                                                                            + "id='txtLdApplicablePerSecond' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                                }
                                                            } else {
                                                                out.print("<input name='txtLdApplicablePerSecond' type='text' size='8' class='formTxtBox_1' "
                                                                        + "id='txtLdApplicablePerSecond' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                            }
                                                %>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff red table-left" valign="top" style="color: red">
                                            Is Penalty Applicable
                                        </td>
                                        <td>
                                            <center>
                                                <select name="selectPenaltyApplicable" class="formTxtBox_1" id="selectPenaltyApplicable" style="width:auto;" onchange="return setTextBoxProperties(this);">
                                                    <%
                                                                if (isEdit) {
                                                                    String value = currentTblCmsTaxConfig.getIsPenaltyApplicable();
                                                                    if (YES.equalsIgnoreCase(value)) {
                                                                        out.println("<option value='yes' selected>Yes</option>");
                                                                        out.println("<option value='no'>No</option>");
                                                                    } else {
                                                                        out.println("<option value='yes'>Yes</option>");
                                                                        out.println("<option value='no' selected>No</option>");
                                                                    }
                                                                } else {
                                                                    out.println("<option selected='selected' value='^'>- Select -</option>");
                                                                    out.println("<option value='yes'>Yes</option>");
                                                                    out.println("<option value='no'>No</option>");
                                                                }
                                                    %>
                                                </select>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff table-left" valign="top">
                                            What % of contract value can be levied as penalty
                                        </td>
                                        <td>
                                            <center>
                                                <%
                                                            if (isEdit) {
                                                                String selectedValue = currentTblCmsTaxConfig.getIsPenaltyApplicable();
                                                                if (YES.equalsIgnoreCase(selectedValue)) {
                                                                    String val = currentTblCmsTaxConfig.getPenaltyPercent() + "";
                                                                    out.print("<input name='txtPenaltyApplicablePerFirst' type='text' size='8' class='formTxtBox_1' value=" + val
                                                                            + " id='txtPenaltyApplicablePerFirst'  style='display:block' onblur='return checkPercentageRange(this);' />");
                                                                } else {
                                                                    out.print("<input name='txtPenaltyApplicablePerFirst' type='text' size='8' class='formTxtBox_1' "
                                                                            + "id='txtPenaltyApplicablePerFirst' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                                }
                                                            } else {
                                                                out.print("<input name='txtPenaltyApplicablePerFirst' type='text' size='8' class='formTxtBox_1' "
                                                                        + "id='txtPenaltyApplicablePerFirst' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                            }
                                                %>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff table-left" valign="top">
                                            On completion on what % of contract, can Penalty be levied
                                        </td>
                                        <td>
                                            <center>
                                                <%
                                                            if (isEdit) {
                                                                String selectedValue = currentTblCmsTaxConfig.getIsPenaltyApplicable();
                                                                if (YES.equalsIgnoreCase(selectedValue)) {
                                                                    String val = currentTblCmsTaxConfig.getPenaltyAfter() + "";
                                                                    out.print(" <input name='txtPenaltyApplicablePerSecond' type='text' size='8' class='formTxtBox_1' value=" + val
                                                                            + " id='txtPenaltyApplicablePerSecond'   style='display:block' onblur='return checkPercentageRange(this);' />");
                                                                } else {
                                                                    out.print(" <input name='txtPenaltyApplicablePerSecond' type='text' size='8' class='formTxtBox_1' "
                                                                            + "id='txtPenaltyApplicablePerSecond' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                                }
                                                            } else {
                                                                out.print(" <input name='txtPenaltyApplicablePerSecond' type='text' size='8' class='formTxtBox_1' "
                                                                        + "id='txtPenaltyApplicablePerSecond' value='0' style='display:none' onblur='return checkPercentageRange(this);' />");
                                                            }
                                                %>
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>                                        
                                        <td class="t-align-center" colspan="2">
                                            <label class="formBtn_1">
                                                <input type="submit" align="middle" name="submit" id="submit" value="Submit"/>
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </form>                            
                        </td>
                    </tr>
                </table>               
                <%--Dashboard Body End--%>
                <%--Dashboard Footer Start--%>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <%--Dashboard Footer End--%>
            </div><%--end dashboard_div--%>
        </div><%--end mainDiv--%>
    </body>
</html>

