<%-- 
    Document   : OAndMAdminGrid
    Created on : Mar 5, 2012, 2:55:10 PM
    Author     : shreyansh.shah
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        String OMTitle = "";
        String RoleType = "";
        if(request.getParameter("isUser") != null && request.getParameter("isUser") != "null" && request.getParameter("isUser") != ""){
            if(request.getParameter("isUser").equals("DLI") || request.getParameter("isUser").equals("DLISuccess") )
                             OMTitle = "Performance Monitoring User";
                        else
                             OMTitle = "O & M User";
        }else {
                if(request.getParameter("userTypeid").equals("21"))
                    OMTitle = "Performance Monitoring User";
                else
                    OMTitle = "O & M Admin";
            }
        if(request.getParameter("RoleType") != null && request.getParameter("RoleType") != "null" && request.getParameter("RoleType") != "")
            {
                RoleType = request.getParameter("RoleType");
            }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=OMTitle%></title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
    </head>
    <%
                byte userTypeIdC = 0;
                String strColHeaders="'Sl. No.','e-mail ID','Full Name','Action'";
                if (!request.getParameter("userTypeid").equals("")) {
                    userTypeIdC = Byte.parseByte(request.getParameter("userTypeid"));
                }
                if(userTypeIdC == 20)
                {
                    strColHeaders="'Sl. No.','e-mail ID','Full Name','Assigned Role','Action'";
                }
               // System.out.print("userTypeIdC"+userTypeIdC);
    %>
    <script type="text/javascript">
        jQuery().ready(function (){
            jQuery("#list").jqGrid({
                url:'<%=request.getContextPath()%>/OrganizationAdminGrid?q=1&action=fetchData&userTypeId=<%=userTypeIdC%>&RoleType=<%=RoleType%>',
                datatype: "xml",
                height: 250,
                colNames:[<%=strColHeaders%>],
                colModel:[
                    {name:'srno',index:'srno', width:5,sortable:false,align:'center',search: false},
                    {name:'tlm.emailId',index:'tlm.emailId', width:35,sortable:true, searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'tam.fullName',index:'tam.fullName', width:35,sortable:true, searchoptions: { sopt: ['eq', 'cn'] }},
                    <%if(userTypeIdC == 20){%>
                    {name:'Rolewise', index:'tam.rollId',width:25,sortable:true,align:'center'},
                    <%}%>
                    {name:'Operation',index:'Operation', width:25,sortable:false, search: false,align:'center'}
                    
                ],
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum:10,
                rowList:[10,20,30],
                pager: $("#page"),
                <% if (userTypeIdC == 21){ %>
                caption: "Performance Monitoring User",
                <% }else{ %>
                caption: "O & M Admin Users",
                <%}%>
                gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
            }).navGrid('#page',{edit:false,add:false,del:false});
        });
    </script>
    <jsp:useBean id="manageAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.UpdateAdminSrBean"/>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("userType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                            userType.append(request.getParameter("userType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                        <td class="contentArea">
                            <div class="pageHead_1">View <%=OMTitle%>s
                            <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="
                                <%if(request.getParameter("isUser") != null && request.getParameter("isUser") != ""){%>
                                exportToPDF('4');
                                <%}else {%>
                                    exportToPDF('3');
                                    <%}%>
                                ">Save as PDF</a></span>
                            </div>
                            <%if ("y".equals(request.getParameter("succFlag"))) {
                                            if ("approved".equalsIgnoreCase(request.getParameter("userStatus"))) {%>
                            <div id="succMsg" class="responseMsg successMsg"><%=OMTitle%>'s account Activated Successfully.</div>
                            <%} else {%>
                            <div id="succMsg" class="responseMsg successMsg"><%=OMTitle%>'s account Deactivated Successfully.</div>
                            <%}
                            }
                            if(request.getParameter("userRightsmsg")!= null && request.getParameter("userRightsmsg").equalsIgnoreCase("success"))
                            {
                            %>
                            <div id="succMsg" class="responseMsg successMsg">Rights assigned Successfully.</div>
                            <%
                            }
                            else if(request.getParameter("userRightsmsg")!= null && request.getParameter("userRightsmsg").equalsIgnoreCase("fail"))
                            {
                            %>
                            <div id="succMsg" class="responseMsg successMsg">Some Problem occured during rights assign.</div>
                            <%
                            }
                            %>
                            <div class="t-align-left ff formStyle_1">To sort click on the relevant column header</div>
                            <%if(request.getParameter("isUser") != null && request.getParameter("isUser") != "null" && request.getParameter("isUser") != "" && request.getParameter("isUser").equals("DLISuccess")){
                            %>
                            <div id="succMsg" class="responseMsg successMsg">User created successfully.</div>
                            <%
                            } else if(request.getParameter("isUser") != null && request.getParameter("isUser") != "null" && request.getParameter("isUser") != "" && !request.getParameter("isUser").equals("DLI")){
                            %>
                                <div class="mandatory">Note : *only individual rights can be changed/updated from here.</div>

                              
                            <%}%>
                            <div class="t_space">
                                <%if (request.getParameter("from") != null) {%>
                                <div align="left" id="sucMsg" class="responseMsg successMsg"><%=OMTitle%> created successfully</div>
                                <%}%>
                                <table id="list"></table>
                                <div id="page">

                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <!--For Generate PDF  Starts-->
            <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                    String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="<%if(request.getParameter("isUser") != null && request.getParameter("isUser") != ""){%>OAndMUser<%}else{%>OAndMAdmin<%}%>_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="<%if(request.getParameter("isUser") != null && request.getParameter("isUser") != ""){%>OAndMUser<%}else{%>OAndMAdmin<%}%>" />
            </form>
            <!--For Generate PDF  Ends-->
                <!--Middle O & M Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
  <script>
                var obj = document.getElementById('lblOAndMAdminView');
                if(obj != null){
                    if(obj.innerHTML == 'View <%=OMTitle%>s'){
                        obj.setAttribute('class', 'selected');
                }
            }

        </script>
        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
    <%
                manageAdminSrBean = null;
    %>
</html>

