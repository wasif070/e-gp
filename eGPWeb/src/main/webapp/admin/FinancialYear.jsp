<%-- 
    Document   : FinancialYear
    Created on : Nov 1, 2010, 4:47:38 PM
    Author     : Naresh.Akurathi
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblFinancialYear"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="financialDtBean" class="com.cptu.egp.eps.web.databean.FinancialYearDtBean"/>
<jsp:useBean id="financialSrBean" class="com.cptu.egp.eps.web.servicebean.FinancialYearSrBean"/>
<jsp:setProperty property="*" name="financialDtBean"/>


<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - Financial Year</title>

        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        
<script type="text/javascript">
function valid(){
        var flag = true;
        if(document.getElementById("stYear")!=null)
        {
            if(document.getElementById("stYear").value=='1')
            {
                document.getElementById("spfin").innerHTML="Please Select Start Financial Year.";
                flag = false;
            }
        }
    return flag;
}
</script>

        <script type="text/javascript">
        $(function() {
                $('#stYear').change(function() {
                    //alert($('#stYear').val());
                    if($('#stYear').val() == '1'){
                        $('#spfin').html('Please Select Start Financial Year');
                        $('#edYear').val('');
                    }else{
                        $('#spfin').html('');
                        //var st = "1-July-"+$('#stYear').val();
                        var st = $('#stYear').val();
                        var v = $('#stYear').val();
                        var c =++v;
                        //var ed = "30-June-"+c;
                        var ed = c;
                        $('#edYear').val(ed);
                        var fy =st+"-"+ed;
                         $('#financialYear').val(fy);
                    }
                });
        });
        </script>
    </head>
    <body>
        <%
                financialSrBean.setLogUserId(session.getAttribute("userId").toString());
        %>

        <%
                    String msg = "";
                    String y = "checked='true'";
                    String n = "";
                    String financialYear = "";
                    int financialId = 0;
                    int id = 0;
                    String stYear = "";
                    String edYear = "";
                    StringBuffer sb = new StringBuffer();

                    if ("Submit".equals(request.getParameter("button"))) {
                       
                        id = Integer.parseInt(request.getParameter("id"));
                        if (id != 0) {
                            //int fid = Integer.parseInt(request.getParameter("id"));
                            financialSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                            msg = financialSrBean.updateFinancialYear(financialDtBean, id);
                            if (msg.equals("Value Updated")) {
                                response.sendRedirect("FinancialYearDetails.jsp");
                            }
                        } else {
                            financialSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                            msg = financialSrBean.addFinancialYearDetails(financialDtBean); //calling SrBean method.
                            if (msg.equals("Values Added")) {
                                msg = "Financial year Configured Successfully";
                                response.sendRedirect("FinancialYearDetails.jsp?msg=" + msg);
                            } else {
                                msg = "Not Added";
                            }
                        }


                    } else if (request.getParameter("id") != null) {

                        id = Integer.parseInt(request.getParameter("id"));
                        Iterator it = financialSrBean.getFinancialYearData(id).iterator();
                        TblFinancialYear tblFinancialYear = new TblFinancialYear();
                        
                        if(it.hasNext()) {
                            tblFinancialYear = (TblFinancialYear) it.next();
                            financialYear = tblFinancialYear.getFinancialYear();
                            financialId = tblFinancialYear.getFinancialYearId();
                            
                            String st[]=financialYear.split("-");
                            //stYear = st[2].toString();
                            //edYear = st[3]+"-"+st[4]+"-"+st[5];
                            stYear = st[0].toString();
                            edYear = st[1].toString();
                            sb.append("<select name='stYear' class='formTxtBox_1' id='stYear'>");
                            sb.append("<option value='1'>--Select Year--</option>");
                           
                            for(int i=2010;i<=2050;i++){
                                sb.append("<option ");
                                if(i==Integer.parseInt(stYear))
                                    sb.append("selected='true'>"+i);
                                else
                                    sb.append(">"+i);
                                sb.append("</option>");
                            }
                            sb.append("</select>");
                            
                            if ("Yes".equals(tblFinancialYear.getIsCurrent())) {
                                y = "checked='true'";
                            }
                            if ("No".equals(tblFinancialYear.getIsCurrent())) {
                                n = "checked='true'";
                                y = "";
                            }
                        }
                    }

        %>

        <%
        StringBuffer startYear = new StringBuffer();
        startYear.append("<select name='stYear' class='formTxtBox_1' id='stYear'>");
        startYear.append("<option value='1'>--Select Year--</option>");
        for(int year=2010;year<=2050;year++){            
            startYear.append("<option>"+year+"</option>");            
            }
        startYear.append("</select>");
       
        %>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <!--Dashboard Header End-->

            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include page="../resources/common/AfterLoginLeft.jsp" />

                    <td class="contentArea" valign="top">
                        <div class="pageHead_1">Financial Year</div>
                        <!--Dashboard Content Part Start-->
                        <!--Dashboard Content Part End-->
                        <form id="frmFinancialYear" action="FinancialYear.jsp" method="post">

                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1 t_space">
                                <tr>
                                    <td  class="ff" width="10%">Financial Start Year Date: <span class="mandatory">*</span></td>
                                    <td width="40%"><%--<input name="stDate" id="stDate" type="text" size="2" class="formTxtBox_1"  value="1-July-"  readonly="true" />--%>
                                           <%-- <select name="stYear" class="formTxtBox_1" id="stYear">
                                            <option>--Select Year--</option>--%>
                                            <%if (request.getParameter("id") != null){%>
                                            <%=sb%>
                                               <%}else{%>
                                                    <%=startYear%>
                                                    <%}%>                                         
                                           
                                      <%-- </select>--%>
                                        <span id="spfin" style="color: red;"></span>                                        
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td  class="ff" width="10%">Financial End Year Date: </td>
                                    <td widht="40%"><input name="edYear" id="edYear" type="text" class="formTxtBox_1"  value="<%=edYear%>"  readonly="true"  "/>
                                       
                                       
                                    </td>
                                </tr>
                                <tr>
                                    <td  class="ff"><input type="hidden" name="financialYear" id="financialYear" value="<%=financialYear%>"/></td>
                                <!-- <td ><span id="spfin" style="color: red;"></span></td> -->
                                    </tr>
                                <tr>
                                    <td class="ff" width="10%">Default Financial Year : <span class="mandatory">*</span></td>
                                    <td><input name="isCurrent" id="txtYes" type="radio" <%=y%> value="Yes">Yes
                                        &nbsp;&nbsp;<input name="isCurrent" id="txtNo" type="radio" <%=n%> value="No">No
                                    </td>
                                </tr>                                
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><b><font color="green"><%=msg%></font></b></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <label class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Submit"  onclick="return valid();"/>
                                        </label>
                                    </td>
                                </tr>
                            </table>
                            <input type="hidden" name="id" value="<%=financialId%>"/>
                        </form>
                    </td>
                </tr>
            </table>
            <br><br>
            <!--Dashboard Footer Start-->
            <%@include file="../resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
             <script>
                var obj = document.getElementById('lblFinancialYear');
                if(obj != null){
                    if(obj.innerHTML == 'Configure'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>





