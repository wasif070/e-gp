<%--
    Document   : ViewSchBankDevPartner
    Created on : Nov 11, 2010, 12:00:00 PM
    Author     : Malhar
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.ScBankDevpartnerService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="scBankCreationDtBean" class="com.cptu.egp.eps.web.databean.ScBankCreationDtBean" scope="request"/>
<jsp:useBean id="scBankCreationSrBean" class="com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean" scope="request"/>
<%
            String logUserId = "0";
            if(session.getAttribute("userId")!=null){
                    logUserId = session.getAttribute("userId").toString();
                }
            scBankCreationSrBean.setLogUserId(logUserId);
            String partnerType = request.getParameter("partnerType");
            String type = "";
            String title = "";
            String userType = "";
            String strOffice = request.getParameter("officeId");
            int officeId = 0;
            String from=request.getParameter("from");
            if(from == null)
                from="Operation";
            if (strOffice != null && !strOffice.equalsIgnoreCase("")) {
                officeId = Integer.parseInt(strOffice);
            }
            if (partnerType != null && partnerType.equals("Development")) {
                type = "Development";
                title = "Development Partner";
                userType = "dev";
            }
            else {
                type = "ScheduleBank";
                title = "Financial Institution";
                userType = "sb";
            }

            if (officeId != 0) {
                scBankCreationDtBean.setOfficeId(officeId);
                ScBankDevpartnerService scBankDevpartnerService=(ScBankDevpartnerService) AppContext.getSpringBean("ScBankDevpartnerService");
                
                if(request.getParameter("mode")!= null && request.getParameter("mode").equalsIgnoreCase("view"))
                {
                    scBankDevpartnerService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                }
                else
                {
                    scBankDevpartnerService.setAuditTrail(null);
                }
                
                scBankCreationDtBean.populateInfo();
            }
            String gridURL = "ManageSbDpOffice.jsp?partnerType=" + type;
            String editURL = "EditSchBankDevPartner.jsp?officeId=" + officeId + "&partnerType=" + type;

%>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=title%> Details : View</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>


                <!--Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">

                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1">
                            <!-- Success failure -->

                            <!-- Result Display End-->
                            <div class="pageHead_1">View <%=title%> Details</div>
                            <!--Page Content Start-->
                            <div class="t_space">
                            <%
                                        String msg = request.getParameter("msg");
                                        if (msg != null && msg.equals("success")) {%>
                            <div class="responseMsg successMsg"><%=title%> <%=from%> successfully</div>
                            <%}%>
                            </div>
                            <form name="frmBankSchedule" action="" method="POST" id ="frmBankSchedule">
                                <table width="100%" class="formStyle_1" border="0" cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td class="ff" width="20%">Name of <%=title%> :</td>
                                        <td width="80%">
                                            <label id="bankName">${scBankCreationDtBean.bankName}</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff"> Address :</td>
                                        <td align="left">
                                            <label id="lblAddress">${scBankCreationDtBean.officeAddress}</label>
                                        </td>                                    
                                    </tr>
                                    <tr>
                                        <td class="ff">City / Town :</td>
                                        <td>
                                            <label id="lblCity">${scBankCreationDtBean.city}</label>
                                        </td>
                                    </tr>
                                    <%
                                                if (type.equals("ScheduleBank")) {
                                                    String country = scBankCreationDtBean.getCountry();
                                                    String display = country.equals("Bangladesh") ? "table-row" : "none";
                                    %>
                                    <tr id="upjilla" style="display: <%=display%>">
                                        <td class="ff">Gewog :</td>
                                        <td>
                                            <label id="lblUpZilla">${scBankCreationDtBean.thanaUpzilla}</label>
                                        </td>
                                    </tr>
                                    <%}%>
                                    <tr>
                                        <td class="ff">Dzongkhag / District :</td>
                                        <td>
                                            <label id="lblState">${scBankCreationDtBean.state}</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Country :</td>
                                        <td>
                                            <label id="lblCountry">${scBankCreationDtBean.country}</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Post Code :</td>
                                        <td>
                                            <label id="lblUpZilla">${scBankCreationDtBean.postCode}</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Phone No. :</td>
                                        <td>
                                            <label id="lblPhoneNo">${scBankCreationDtBean.phoneNo}</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Fax No. :</td>
                                        <%if(!scBankCreationDtBean.getFaxNo().equals("")){%>
                                        <td><label id="lblFax">${scBankCreationDtBean.faxNo}</label></td>
                                        <% } %>
                                    </tr>
                                    <tr>
                                        <td class="ff">Website :</td>
                                        <%if(!scBankCreationDtBean.getWebSite().equals("")){%>
                                        <td><label id="lblFax">${scBankCreationDtBean.webSite}</label></td>
                                        <% } %>
                                    </tr>
                                   <%-- <tr>
                                        <td align="center" colspan="2">
                                            <label class="formBtn_1">
                                                <input type="button" name="edit" id="btnEdit" value="Update"/>
                                            </label>
                                        </td>
                                    </tr>--%>
                                </table>
                                            <table width="100%" cellspacing="10" cellpadding="0" border="0">
                                                <tr>
                                                    <td width="20%">
                                                    &nbsp;
                                                    </td>
                                                    <td width="80%" class="t-align-left" >
                                                        <a href="<%=gridURL%>" class="anchorLink">Ok</a>&nbsp;  <a href="<%=editURL%>" class="anchorLink">Edit</a>
                                                    </td>
                                                </tr>
                                            </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
