<%-- 
    Document   : ViewTaxConfiguration
    Created on : Jul 7, 2011, 11:45:51 AM
    Author     : Sreenu.Durga
--%>

<%@page import="com.cptu.egp.eps.model.table.TblCmsTaxConfig"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsTaxConfigBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Tax Configuration Page</title>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
    </head>
    <%
                CmsTaxConfigBean cmsTaxConfigBean = new CmsTaxConfigBean();
                if (session.getAttribute("userId") != null) {
                    cmsTaxConfigBean.setLogUserId(session.getAttribute("userId").toString());
                }
                StringBuilder userType = new StringBuilder();
                if (request.getParameter("userType") != null) {
                    if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                        userType.append(request.getParameter("userType"));
                    } else {
                        userType.append("org");
                    }
                } else {
                    userType.append("org");
                }

                boolean isGoodsCurrent = false;
                boolean isWorksCurrent = false;
                boolean isServicesCurrent = false;

                final String SELECT = "SELECT";
                final String SPACE = "&nbsp;";
                final String YES = "yes";
                final String NO = "no";

                final int GOODS_PROCUREMENT_NATURE_ID = 1;
                final int WORKS_PROCUREMENT_NATURE_ID = 2;
                final int SERVICES_PROCUREMENT_NATURE_ID = 3;

                TblCmsTaxConfig currentGoodsTblCmsTaxConfig =
                        cmsTaxConfigBean.getLatestCmsTaxConfiguration(GOODS_PROCUREMENT_NATURE_ID);
                TblCmsTaxConfig currentWorksTblCmsTaxConfig =
                        cmsTaxConfigBean.getLatestCmsTaxConfiguration(WORKS_PROCUREMENT_NATURE_ID);
                TblCmsTaxConfig currentServicesTblCmsTaxConfig =
                        cmsTaxConfigBean.getLatestCmsTaxConfiguration(SERVICES_PROCUREMENT_NATURE_ID);

                if (currentGoodsTblCmsTaxConfig != null) {
                    isGoodsCurrent = true;
                }
                if (currentWorksTblCmsTaxConfig != null) {
                    isWorksCurrent = true;
                }
                if (currentServicesTblCmsTaxConfig != null) {
                    isServicesCurrent = true;
                }
                boolean flag = false;
                String message = "";
                if (request.getParameter("flag") != null) {
                    if (flag) {
                        message = "changed successfully";
                    } else {
                        message = "updated successfully";
                    }
                }

    %>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%--Dashboard Header Start--%>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%--Dashboard Header End--%>
                <%--Dashboard Body Start--%>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType.toString()%>" ></jsp:include>
                        <td class="contentArea">
                            <div>
                                <div class="pageHead_1"> View Tax Configure</div>
                            </div>
                            <%
                                        if (message.length() != 0) {
                                            out.print("<div id='successMsg' class='responseMsg successMsg' style='display:block; margin-top: 10px;'>");
                                            out.print(message);
                                            out.print("</div>");
                                        }
                            %>
                            <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_3 t_space">
                                <tr>
                                    <th width="64%" valign="top" class="ff">Procurement Type</th>
                                    <th width="12%">Goods</th>
                                    <th width="12%">Work</th>
                                    <th width="12%">Services</th>
                                </tr>
                                <tr>
                                    <td align="center"  class="ff"> <center>Action</center></td>
                                    <td>
                                        <center>
                                            <%
                                                        if (isGoodsCurrent == false) {
                                                            out.print("<a href='ConfigureTaxConfiguration.jsp?natureType="
                                                                    + GOODS_PROCUREMENT_NATURE_ID + "'>Create</a>");
                                                        } else {
                                                            out.print("<a href='ConfigureTaxConfiguration.jsp?natureType="
                                                                    + GOODS_PROCUREMENT_NATURE_ID
                                                                    + "&taxConfigId=" + currentGoodsTblCmsTaxConfig.getTaxConfigId()
                                                                    + "'>Edit</a>");
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (isWorksCurrent == false) {
                                                            out.print("<a href='ConfigureTaxConfiguration.jsp?natureType="
                                                                    + WORKS_PROCUREMENT_NATURE_ID + "'>Create</a>");
                                                        } else {
                                                            out.print("<a href='ConfigureTaxConfiguration.jsp?natureType="
                                                                    + WORKS_PROCUREMENT_NATURE_ID
                                                                    + "&taxConfigId=" + currentWorksTblCmsTaxConfig.getTaxConfigId()
                                                                    + "'>Edit</a>");
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (isServicesCurrent == false) {
                                                            out.print("<a href='ConfigureTaxConfiguration.jsp?natureType="
                                                                    + SERVICES_PROCUREMENT_NATURE_ID + "'>Create</a>");
                                                        } else {
                                                            out.print("<a href='ConfigureTaxConfiguration.jsp?natureType="
                                                                    + SERVICES_PROCUREMENT_NATURE_ID
                                                                    + "&taxConfigId=" + currentServicesTblCmsTaxConfig.getTaxConfigId()
                                                                    + "'>Edit</a>");
                                                        }
                                            %>
                                        </center>
                                    </td>
                                </tr>                                
                                <tr>                                   
                                    <td class="ff red table-left" valign="top" style="color: red">Is Advance Applicable </td>                                    
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentGoodsTblCmsTaxConfig != null) {
                                                                out.print(currentGoodsTblCmsTaxConfig.getIsAdvanceApplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentWorksTblCmsTaxConfig != null) {
                                                                out.print(currentWorksTblCmsTaxConfig.getIsAdvanceApplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentServicesTblCmsTaxConfig != null) {
                                                                out.print(currentServicesTblCmsTaxConfig.getIsAdvanceApplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>                                        
                                    </td>
                                </tr>                                
                                <tr>
                                    <td class="ff table-left" valign="top" >Percentage of Advance that can be paid as part of Contract value</td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentGoodsTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentGoodsTblCmsTaxConfig.getIsAdvanceApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentGoodsTblCmsTaxConfig.getAdvancePercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentWorksTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentWorksTblCmsTaxConfig.getIsAdvanceApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentWorksTblCmsTaxConfig.getAdvancePercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentServicesTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentServicesTblCmsTaxConfig.getIsAdvanceApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentServicesTblCmsTaxConfig.getAdvancePercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                </tr>                               
                                <tr>
                                    <td class="ff table-left" valign="top" >If advance is paid, in what % of Bills it should be adjusted e.g. adjust 100% advance when 60% of bills are cleared, specify 0 if to be proportionately adjusted in all bills</td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentGoodsTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentGoodsTblCmsTaxConfig.getIsAdvanceApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentGoodsTblCmsTaxConfig.getAdvanceAdjustment());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentWorksTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentWorksTblCmsTaxConfig.getIsAdvanceApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentWorksTblCmsTaxConfig.getAdvanceAdjustment());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentServicesTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentServicesTblCmsTaxConfig.getIsAdvanceApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentServicesTblCmsTaxConfig.getAdvanceAdjustment());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                </tr>                                 
                                <tr>
                                    <td class="ff red table-left" valign="top" style="color: red">Is Retention Money Applicable</td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentGoodsTblCmsTaxConfig != null) {
                                                                out.print(currentGoodsTblCmsTaxConfig.getIsRetentionApplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentWorksTblCmsTaxConfig != null) {
                                                                out.print(currentWorksTblCmsTaxConfig.getIsRetentionApplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentServicesTblCmsTaxConfig != null) {
                                                                out.print(currentServicesTblCmsTaxConfig.getIsRetentionApplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td class="ff table-left" valign="top" >Retention Money to be deducted as % of contract value</td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentGoodsTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentGoodsTblCmsTaxConfig.getIsRetentionApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentGoodsTblCmsTaxConfig.getRetentionPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentWorksTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentWorksTblCmsTaxConfig.getIsRetentionApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentWorksTblCmsTaxConfig.getRetentionPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentServicesTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentServicesTblCmsTaxConfig.getIsRetentionApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentServicesTblCmsTaxConfig.getRetentionPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff table-left" valign="top" >When should Retention Money be released as % of contract, specify 0 if not applicable</td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentGoodsTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentGoodsTblCmsTaxConfig.getIsRetentionApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentGoodsTblCmsTaxConfig.getRetentionAdjustment());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentWorksTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentWorksTblCmsTaxConfig.getIsRetentionApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentWorksTblCmsTaxConfig.getRetentionAdjustment());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentServicesTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentServicesTblCmsTaxConfig.getIsRetentionApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentServicesTblCmsTaxConfig.getRetentionAdjustment());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                </tr>                               
                                <tr>
                                    <td class="ff red table-left" valign="top" style="color: red">Is PG Applicable</td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentGoodsTblCmsTaxConfig != null) {
                                                                out.print(currentGoodsTblCmsTaxConfig.getIsPgapplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentWorksTblCmsTaxConfig != null) {
                                                                out.print(currentWorksTblCmsTaxConfig.getIsPgapplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentServicesTblCmsTaxConfig != null) {
                                                                out.print(currentServicesTblCmsTaxConfig.getIsPgapplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff table-left" valign="top">Performance Security applicable as % of contract value, add 0 if not applicable</td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentGoodsTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentGoodsTblCmsTaxConfig.getIsPgapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentGoodsTblCmsTaxConfig.getPgPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentWorksTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentWorksTblCmsTaxConfig.getIsPgapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentWorksTblCmsTaxConfig.getPgPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentServicesTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentServicesTblCmsTaxConfig.getIsPgapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentServicesTblCmsTaxConfig.getPgPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff table-left" valign="top">What % of contract should be completed to release Performance Security (PG)</td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentGoodsTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentGoodsTblCmsTaxConfig.getIsPgapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentGoodsTblCmsTaxConfig.getPgAdjustment());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentWorksTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentWorksTblCmsTaxConfig.getIsPgapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentWorksTblCmsTaxConfig.getPgAdjustment());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentServicesTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentServicesTblCmsTaxConfig.getIsPgapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentServicesTblCmsTaxConfig.getPgAdjustment());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                </tr>                               
                                <tr>
                                    <td class="ff red table-left" valign="top" style="color: red">Is VAT applicable</td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentGoodsTblCmsTaxConfig != null) {
                                                                out.print(currentGoodsTblCmsTaxConfig.getIsVatapplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentWorksTblCmsTaxConfig != null) {
                                                                out.print(currentWorksTblCmsTaxConfig.getIsVatapplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentServicesTblCmsTaxConfig != null) {
                                                                out.print(currentServicesTblCmsTaxConfig.getIsVatapplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff table-left" valign="top">Percentage of VAT applicable</td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentGoodsTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentGoodsTblCmsTaxConfig.getIsVatapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentGoodsTblCmsTaxConfig.getVatPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentWorksTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentWorksTblCmsTaxConfig.getIsVatapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentWorksTblCmsTaxConfig.getVatPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentServicesTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentServicesTblCmsTaxConfig.getIsVatapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentServicesTblCmsTaxConfig.getVatPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                </tr> 
                                <tr>
                                    <td class="ff red table-left" valign="top" style="color: red">Is Bonus Applicable</td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentGoodsTblCmsTaxConfig != null) {
                                                                out.print(currentGoodsTblCmsTaxConfig.getIsBonusApplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentWorksTblCmsTaxConfig != null) {
                                                                out.print(currentWorksTblCmsTaxConfig.getIsBonusApplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentServicesTblCmsTaxConfig != null) {
                                                                out.print(currentServicesTblCmsTaxConfig.getIsBonusApplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff table-left" valign="top" >What %  of contract value can be given as Bonus </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentGoodsTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentGoodsTblCmsTaxConfig.getIsBonusApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentGoodsTblCmsTaxConfig.getBonusPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentWorksTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentWorksTblCmsTaxConfig.getIsBonusApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentWorksTblCmsTaxConfig.getBonusPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentServicesTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentServicesTblCmsTaxConfig.getIsBonusApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentServicesTblCmsTaxConfig.getBonusPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff table-left" valign="top" >What % of contract should be completed when Bonus can be paid</td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentGoodsTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentGoodsTblCmsTaxConfig.getIsBonusApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentGoodsTblCmsTaxConfig.getBonusAfter());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentWorksTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentWorksTblCmsTaxConfig.getIsBonusApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentWorksTblCmsTaxConfig.getBonusAfter());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentServicesTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentServicesTblCmsTaxConfig.getIsBonusApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentServicesTblCmsTaxConfig.getBonusAfter());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                </tr>    
                                <tr>
                                    <td class="ff red table-left" valign="top" style="color: red">Is LD Applicable</td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentGoodsTblCmsTaxConfig != null) {
                                                                out.print(currentGoodsTblCmsTaxConfig.getIsLdapplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentWorksTblCmsTaxConfig != null) {
                                                                out.print(currentWorksTblCmsTaxConfig.getIsLdapplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentServicesTblCmsTaxConfig != null) {
                                                                out.print(currentServicesTblCmsTaxConfig.getIsLdapplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff table-left" valign="top">What % of Contract value can be levieed as Liquidated damages</td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentGoodsTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentGoodsTblCmsTaxConfig.getIsLdapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentGoodsTblCmsTaxConfig.getLdPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentWorksTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentWorksTblCmsTaxConfig.getIsLdapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentWorksTblCmsTaxConfig.getLdPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentServicesTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentServicesTblCmsTaxConfig.getIsLdapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentServicesTblCmsTaxConfig.getLdPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff table-left" valign="top" >On completion of what % of contract LD damages can be levied</td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentGoodsTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentGoodsTblCmsTaxConfig.getIsLdapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentGoodsTblCmsTaxConfig.getLdAfter());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentWorksTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentWorksTblCmsTaxConfig.getIsLdapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentWorksTblCmsTaxConfig.getLdAfter());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentServicesTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentServicesTblCmsTaxConfig.getIsLdapplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentServicesTblCmsTaxConfig.getLdAfter());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff red table-left" valign="top" style="color: red">Is Penalty Applicable</td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentGoodsTblCmsTaxConfig != null) {
                                                                out.print(currentGoodsTblCmsTaxConfig.getIsPenaltyApplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentWorksTblCmsTaxConfig != null) {
                                                                out.print(currentWorksTblCmsTaxConfig.getIsPenaltyApplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <b>
                                                <%
                                                            if (currentServicesTblCmsTaxConfig != null) {
                                                                out.print(currentServicesTblCmsTaxConfig.getIsPenaltyApplicable().toUpperCase());
                                                            } else {
                                                                out.print(SELECT);
                                                            }
                                                %>
                                            </b>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff table-left" valign="top" >What % of contract value can be levied as penalty </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentGoodsTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentGoodsTblCmsTaxConfig.getIsPenaltyApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentGoodsTblCmsTaxConfig.getPenaltyPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentWorksTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentWorksTblCmsTaxConfig.getIsPenaltyApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentWorksTblCmsTaxConfig.getPenaltyPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentServicesTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentServicesTblCmsTaxConfig.getIsPenaltyApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentServicesTblCmsTaxConfig.getPenaltyPercent());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff table-left" valign="top" >On completion on what % of contract, can Penalty be levied</td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentGoodsTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentGoodsTblCmsTaxConfig.getIsPenaltyApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentGoodsTblCmsTaxConfig.getPenaltyAfter());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentWorksTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentWorksTblCmsTaxConfig.getIsPenaltyApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentWorksTblCmsTaxConfig.getPenaltyAfter());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <%
                                                        if (currentServicesTblCmsTaxConfig != null) {
                                                            if (NO.equalsIgnoreCase(currentServicesTblCmsTaxConfig.getIsPenaltyApplicable())) {
                                                                out.print(SPACE);
                                                            } else {
                                                                out.print(currentServicesTblCmsTaxConfig.getPenaltyAfter());
                                                            }
                                                        } else {
                                                            out.print(SPACE);
                                                        }
                                            %>
                                        </center>
                                    </td>                                
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table> 
                <%--Dashboard Body End--%>
                <%--Dashboard Footer Start--%>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <%--Dashboard Footer End--%>
            </div><%--end dashboard_div--%>
        </div><%--end mainDiv--%>
    </body>
</html>
