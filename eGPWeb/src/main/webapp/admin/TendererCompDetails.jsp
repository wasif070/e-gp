<%--
    Document   : CompanyVerification
    Created on : Nov 5, 2010, 11:40 PM
    Author     : taher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblCompanyMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblBiddingPermission"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum" %>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Verification</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />

    </head>       
    <body>
        <%
            ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
            contentAdminService.setAuditTrail(null);
            TblCompanyMaster tblCompanyMaster = contentAdminService.findCompanyMaster("companyId", Operation_enum.EQ, Integer.parseInt(request.getParameter("cId"))).get(0);
            List<TblBiddingPermission> tempBiddingPermisionList = null;
            List<TblBiddingPermission> approvedBiddingPermisionList = null;
            if ("pending".equalsIgnoreCase(request.getParameter("status"))) {
                tempBiddingPermisionList = contentAdminService.findBiddingpermission("companyId", Operation_enum.EQ, Integer.parseInt(request.getParameter("cId").toString()), "isReapplied", Operation_enum.EQ, 0);
            } else {
                tempBiddingPermisionList = contentAdminService.findBiddingpermission("companyId", Operation_enum.EQ, Integer.parseInt(request.getParameter("cId").toString()), "isReapplied", Operation_enum.EQ, 1);
                approvedBiddingPermisionList = contentAdminService.findBiddingpermission("companyId", Operation_enum.EQ, Integer.parseInt(request.getParameter("cId").toString()), "isReapplied", Operation_enum.EQ, 0);
            }
            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");

            // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType = "userId";
            int auditId = Integer.parseInt(session.getAttribute("userId").toString());
            String auditAction = "View Company Verification";
            String moduleName = EgpModule.New_User_Registration.getName();
            String remarks = "";
            makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);

        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="tableHead_1 t_space">Bidder/Consultant Company Details</div>
                <jsp:include page="EditAdminNavigation.jsp" ></jsp:include>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" id="tb2" width="100%">                        
                    <tr>
                        <td class="ff" width="18%">Company Name : </td>
                        <td width="82%"><%=tblCompanyMaster.getCompanyName()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Company's Legal Status : </td>
                        <td>
                            <%if (tblCompanyMaster.getLegalStatus().equals("public")) {%>Public Limited Company.<%}%>
                            <%if (tblCompanyMaster.getLegalStatus().equals("private")) {%>Private Limited Company<%}%>
                            <%--<%if (tblCompanyMaster.getLegalStatus().equals("partnership")) {%>Partnership.<%}%>--%>
                            <%if (tblCompanyMaster.getLegalStatus().equals("proprietor")) {%>Sole Proprietorship.<%}%>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Company's
                            Establishment Year : </td>
                        <td><%=tblCompanyMaster.getEstablishmentYear()%></td>
                    </tr>
                    <tr class="ff"> 
                        <td>Procurement Category 
                            <%if(request.getParameter("status")!=null){
                                if("reapply".equalsIgnoreCase(request.getParameter("status").toString())){%>(Pending)<%}}%>:</td>
                        <td style="text-align: left">
                            <% if (tempBiddingPermisionList.size() != 0) 
                                {
                                    for (int i = 0; i < tempBiddingPermisionList.size(); i++) {                                        
                                        out.println(tempBiddingPermisionList.get(i).getProcurementCategory().toString());
                                        if (tempBiddingPermisionList.get(i).getWorkType() != null && !tempBiddingPermisionList.get(i).getWorkType().isEmpty()) {
                                            out.print(", " + tempBiddingPermisionList.get(i).getWorkType().toString());
                                        }
                                        if (tempBiddingPermisionList.get(i).getWorkCategroy() != null && !tempBiddingPermisionList.get(i).getWorkCategroy().isEmpty()) {
                                            out.print(", " + tempBiddingPermisionList.get(i).getWorkCategroy().toString());
                                        }
                                        out.print("<br/>");
                                    }
                                }
                            %>
                        </td>
                    </tr>
                    <%if(approvedBiddingPermisionList!=null){%>
                    <tr class="ff"> 
                        <td>Procurement Category (Approved):</td>
                        <td style="text-align: left">
                            <% if (approvedBiddingPermisionList.size() != 0) 
                                {
                                    for (int i = 0; i < approvedBiddingPermisionList.size(); i++) {                                        
                                        out.println(approvedBiddingPermisionList.get(i).getProcurementCategory().toString());
                                        if (approvedBiddingPermisionList.get(i).getWorkType() != null && !approvedBiddingPermisionList.get(i).getWorkType().isEmpty()) {
                                            out.print(", " + approvedBiddingPermisionList.get(i).getWorkType().toString());
                                        }
                                        if (approvedBiddingPermisionList.get(i).getWorkCategroy() != null && !approvedBiddingPermisionList.get(i).getWorkCategroy().isEmpty()) {
                                            out.print(", " + approvedBiddingPermisionList.get(i).getWorkCategroy().toString());
                                        }
                                        out.print("<br/>");
                                    }
                                }
                            %>
                        </td>
                    </tr>
                    <%}%>
                    <tr style="display: none">
                        <td class="ff">Nature of Business : </td>
                        <td><%=tblCompanyMaster.getSpecialization()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Registered Address : </td>
                        <td><%=tblCompanyMaster.getRegOffAddress()%></td>
                    </tr>
                    <tr>
                        <td class="ff"> Country of Origin : </td>
                        <td><%if (tblCompanyMaster.getOriginCountry() != null) {
                                out.print(tblCompanyMaster.getOriginCountry());
                            }%></td>
                    </tr>
                    <tr>
                        <td class="ff">Country : </td>
                        <td><%=tblCompanyMaster.getRegOffCountry()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Dzongkhag / District : </td>
                        <td><%=tblCompanyMaster.getRegOffState()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Dungkhag / Sub-District : </td>
                        <td><%if (tblCompanyMaster.getRegOffSubDistrict() != null) {
                                    out.print(tblCompanyMaster.getRegOffSubDistrict());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">City / Town : </td>
                        <td><%if (tblCompanyMaster.getRegOffCity() != null) {
                                out.print(tblCompanyMaster.getRegOffCity());}%></td>
                    </tr>
                    <tr id="trRthana">
                        <td class="ff">Gewog : </td>
                        <td><%if (tblCompanyMaster.getRegOffUpjilla() != null) {
                                    out.print(tblCompanyMaster.getRegOffUpjilla());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Post Code : </td>
                        <td><%if (tblCompanyMaster.getRegOffPostcode() != null) {
                                    out.print(tblCompanyMaster.getRegOffPostcode());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Mobile No. : </td>
                        <td><%if (tblCompanyMaster.getRegOffMobileNo() != null) {
                                out.print(tblCompanyMaster.getRegOffMobileNo());
                            }%></td>
                    </tr>
                    <tr>
                        <td class="ff">Phone No. : </td>
                        <td><%if (tblCompanyMaster.getRegOffPhoneNo() != null) {
                                out.print(commonService.countryCode(tblCompanyMaster.getRegOffCountry(), false) + "-" + tblCompanyMaster.getRegOffPhoneNo());
                            }%></td>
                    </tr>

                    <tr>
                        <td class="ff">Corporate / Head office<br />
                            Address : </td>
                        <td><%=tblCompanyMaster.getCorpOffAddress()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Country : </td>
                        <td><%=tblCompanyMaster.getCorpOffCountry()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Dzongkhag / District : </td>
                        <td><%if (tblCompanyMaster.getCorpOffState() != null) {
                                    out.print(tblCompanyMaster.getCorpOffState());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Dungkhag  / Sub-District : </td>
                        <td><%if (tblCompanyMaster.getCorpOffSubDistrict() != null) {
                                    out.print(tblCompanyMaster.getCorpOffSubDistrict());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">City / Town : </td>
                        <td><%if (tblCompanyMaster.getCorpOffCity() != null) {
                                    out.print(tblCompanyMaster.getCorpOffCity());}%></td>
                    </tr>
                    <tr id="trCthana">
                        <td class="ff">Gewog : </td>
                        <td><%if (tblCompanyMaster.getCorpOffUpjilla() != null) {
                                    out.print(tblCompanyMaster.getCorpOffUpjilla());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Post Code : </td>
                        <td><%if (tblCompanyMaster.getCorpOffPostcode() != null) {
                                    out.print(tblCompanyMaster.getCorpOffPostcode());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Mobile No. : </td>
                        <td><%if (tblCompanyMaster.getCorpOffMobMobileNo() != null) {
                                    out.print(tblCompanyMaster.getCorpOffMobMobileNo());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Phone No. : </td>
                        <td><%if (tblCompanyMaster.getCorpOffPhoneno() != null) {
                                out.print(commonService.countryCode(tblCompanyMaster.getCorpOffCountry(), false) + "-" + tblCompanyMaster.getCorpOffPhoneno());
                            }%></td>
                    </tr>

                    <tr>
                        <td class="ff">Company's Website : </td>
                        <td><%if (tblCompanyMaster.getWebsite()!=null)
                             out.print(tblCompanyMaster.getWebsite());%></td>
                    </tr>                    
                </table>
                <table border="0" cellspacing="10" cellpadding="0" width="100%">
                    <tr>
                        <td width="18%">&nbsp;</td>
                        <td width="82%" align="left">
                            <%if (request.getParameter("status") != null) {
                                    if ("reapply".equalsIgnoreCase(request.getParameter("status").toString())) {%>
                            <a href="TendererDocs.jsp?s=<%=request.getParameter("s")%>&payId=<%=request.getParameter("payId")%>&tId=<%=request.getParameter("tId")%>&uId=<%=request.getParameter("uId")%>&cId=<%=request.getParameter("cId")%>&jv=<%=request.getParameter("jv")%>&status=<%=request.getParameter("status")%>"  class="anchorLink">Next</a>
                            <% }
                             else {%>
                            <a href="TendererDetails.jsp?s=<%=request.getParameter("s")%>&payId=<%=request.getParameter("payId")%>&tId=<%=request.getParameter("tId")%>&uId=<%=request.getParameter("uId")%>&cId=<%=request.getParameter("cId")%>&jv=<%=request.getParameter("jv")%>&status=<%=request.getParameter("status")%>"  class="anchorLink">Next</a>
                            <% }}%>
                        </td>
                    </tr>
                </table>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script>
            var headSel_Obj = document.getElementById("headTabCompVerify");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
