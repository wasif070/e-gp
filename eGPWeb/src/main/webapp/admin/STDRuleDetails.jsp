
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%-- 
    Document   : STDRuleDetails
    Created on : Nov 16, 2010, 12:56:44 PM
    Author     : Naresh.Akurathi
--%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenPaymentConfigService"%>
<%
            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            Object objUName = session.getAttribute("userName");
            boolean isLoggedIn = false;
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userId").toString();
            }
            if (objUName != null) {
                isLoggedIn = true;
            }
%>
<%@page import="java.util.List"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigStd"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigPreTender"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
                    TenPaymentConfigService configService = (TenPaymentConfigService) AppContext.getSpringBean("TenPaymentConfigService");
                    List<Object[]> tenderTypes = configService.getTenderTypes();
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Standard Bidding Document Selection Business Rule</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <%!    ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();

        %>


        <%
                    StringBuilder tenderType = new StringBuilder();
                    StringBuilder procNature = new StringBuilder();
                    StringBuilder procType = new StringBuilder();
                    StringBuilder procMethod = new StringBuilder();
                    StringBuilder templateMaster = new StringBuilder();

                    configPreTenderRuleSrBean.setLogUserId(strUserTypeId);
                    TblProcurementNature tblProcureNature2 = new TblProcurementNature();
                    Iterator pnit2 = configPreTenderRuleSrBean.getProcurementNature().iterator();
                    while (pnit2.hasNext()) {
                        tblProcureNature2 = (TblProcurementNature) pnit2.next();
                        procNature.append("<option value='" + tblProcureNature2.getProcurementNatureId() + "'>" + tblProcureNature2.getProcurementNature() + "</option>");
                    }

                    TblProcurementMethod tblProcurementMethod2 = new TblProcurementMethod();
                    Iterator pmit2 = configPreTenderRuleSrBean.getProcurementMethod().iterator();
                    while (pmit2.hasNext()) {
                        tblProcurementMethod2 = (TblProcurementMethod) pmit2.next();
                        if("OTM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                            "LTM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                            "DP".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                           // "LEQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                          //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                            "RFQ".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                           // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                          //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                            "FC".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                           // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                            "DPM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())||
                             "QCBS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "LCS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "SFB".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "SBCQ".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "SSS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "IC".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()))
                         {
                                if(tblProcurementMethod2.getProcurementMethod().equalsIgnoreCase("RFQ"))
                                {
                                    procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>LEM</option>");
                                }
                                else if(tblProcurementMethod2.getProcurementMethod().equalsIgnoreCase("DPM"))
                                {
                                    procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>DCM</option>");
                                }
                                else
                                {
                                    procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>" + tblProcurementMethod2.getProcurementMethod() + "</option>");
                                }
                         }
                        
                     }

                    TblProcurementTypes tblProcurementTypes2 = new TblProcurementTypes();
                    Iterator ptit2 = configPreTenderRuleSrBean.getProcurementTypes().iterator();
                    while (ptit2.hasNext()) {
                        tblProcurementTypes2 = (TblProcurementTypes) ptit2.next();
                        if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("NCT"))
                        {
                            procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>NCB</option>");
                        }
                        else if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("ICT"))
                        {
                            procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>ICB</option>");
                        }
                        else
                        {
                            procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + tblProcurementTypes2.getProcurementType() + "</option>");
                        }
                    }
                    

                    TblTemplateMaster tblTemplateMaster2 = new TblTemplateMaster();
                    Iterator tmit = configPreTenderRuleSrBean.getTemplateMaster().iterator();
                    while (tmit.hasNext()) {
                        tblTemplateMaster2 = (TblTemplateMaster) tmit.next();
                        templateMaster.append("<option value='" + tblTemplateMaster2.getTemplateId() + "'>" + tblTemplateMaster2.getTemplateName() + "</option>");
                    }

        %>


        
    </head>
   <body onload="convert()">

        <%

                    TblConfigStd configStd = new TblConfigStd();
                    String msg = "";
                    if ("Submit".equals(request.getParameter("button")) || "Update".equals(request.getParameter("button"))) {

                        int row = Integer.parseInt(request.getParameter("TotRule"));

                        if ("edit".equals(request.getParameter("action"))) {
                            msg = "Not Deleted";
                        } else {
                            msg = configPreTenderRuleSrBean.delAllConfigStd();
                        }

                        if (msg.equals("Deleted")) {
                            String action = "Add STD Section Rules";
                            try{
                                for (int i = 1; i <= row; i++) {
                                    if (request.getParameter("pNature" + i) != null) {

                                        byte pnature = Byte.parseByte(request.getParameter("pNature" + i));
                                        byte pmethod = Byte.parseByte(request.getParameter("pMethod" + i));
                                        byte ptypes = Byte.parseByte(request.getParameter("pType" + i));
                                        float tvalues = Float.parseFloat(request.getParameter("values" + i));
                                        short stdName = Short.parseShort(request.getParameter("stdName" + i));
                                        int tendertype = Integer.parseInt(request.getParameter("tenderType" + i));
                                        TblConfigStd tblConfigStd = new TblConfigStd();
                                        tblConfigStd.setTblProcurementNature(new TblProcurementNature(pnature));
                                        tblConfigStd.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                        tblConfigStd.setTblProcurementTypes(new TblProcurementTypes(ptypes));
                                        tblConfigStd.setOperator(request.getParameter("operator" + i));
                                        tblConfigStd.setTenderValue(new BigDecimal(request.getParameter("values" + i)));
                                        tblConfigStd.setTenderTypeId(tendertype);
                                        tblConfigStd.setTblTemplateMaster(new TblTemplateMaster(stdName));

                                        msg = configPreTenderRuleSrBean.addConfigStd(tblConfigStd);


                                    }
                             }
                          }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                           }finally{
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                                action=null;
                           }
                        } else {
                            String action = "Edit STD Section Rules";
                            try{
                                for (int i = 1; i <= row; i++) {
                                    if (request.getParameter("pNature" + i) != null) {

                                        byte pnature = Byte.parseByte(request.getParameter("pNature" + i));
                                        byte pmethod = Byte.parseByte(request.getParameter("pMethod" + i));
                                        byte ptypes = Byte.parseByte(request.getParameter("pType" + i));
                                        float tvalues = Float.parseFloat(request.getParameter("values" + i));
                                        short stdName = Short.parseShort(request.getParameter("stdName" + i));
                                        int tendertype = Integer.parseInt(request.getParameter("tenderType" + i));
                                        TblConfigStd tblConfigStd = new TblConfigStd();
                                        tblConfigStd.setConfigStdId(Integer.parseInt(request.getParameter("id")));
                                        tblConfigStd.setTblProcurementNature(new TblProcurementNature(pnature));
                                        tblConfigStd.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                        tblConfigStd.setTblProcurementTypes(new TblProcurementTypes(ptypes));
                                        tblConfigStd.setOperator(request.getParameter("operator" + i));
                                        tblConfigStd.setTenderValue(new BigDecimal(request.getParameter("values" + i)));
                                        tblConfigStd.setTenderTypeId(tendertype);
                                        tblConfigStd.setTblTemplateMaster(new TblTemplateMaster(stdName));

                                        msg = configPreTenderRuleSrBean.updateConfigStd(tblConfigStd);

                                    }
                                }
                               }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                               }finally{
                                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                                    action=null;
                               }
                            }
                        if (msg.equals("Values Added")) {
                            msg = "SBD Business Rule Configured Successfully";
                            response.sendRedirect("STDRuleView.jsp?msg=" + msg);
                        }
                        if (msg.equals("Updated")) {
                            msg = "SBD Business Rule Updated Successfully";
                            response.sendRedirect("STDRuleView.jsp?msg=" + msg);
                        }

                    }


        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
                        <!--Dashboard Header End-->
                        <!--Dashboard Content Part Start-->
                        <div class="pageHead_1">Standard Bidding Document Selection Business Rule</div>

                        <div style="font-style: italic;" class="t-align-right"><normal>Fields marked with (<span class="mandatory">*</span>) are mandatory</normal></div>

                        <%
                                    if (request.getParameter("id") == null) {
                        %>

                        <div align="right" class="t_space b_space">
                            <span id="lotMsg" style="color: red; font-weight: bold; float: left; visibility: collapse;">&nbsp;</span>
                            <a id="linkAddRule" class="action-button-add">Add Rule</a> <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a>
                        </div>
                        <%}%>
                        <div>&nbsp;</div>

                        <%--<div align="center"> <%=msg%></div>--%>
                        <form action="STDRuleDetails.jsp" method="post">
                            <table width="100%" cellspacing="0" class="tableList_1" id="tblrule" name="tblstd">
                                <tr>
                                    <th nowrap >Select</th>
                                    <th nowrap>Tender Type<br />(<span class="mandatory">*</span>)</th>
                                    <th nowrap>Procurement<br />Category<br />(<span class="mandatory">*</span>)</th>
                                    <th nowrap>Procurement<br />Method<br />(<span class="mandatory">*</span>)</th>
                                    <th nowrap>Procurement<br />Type<br />(<span class="mandatory">*</span>)</th>
                                    <th nowrap>Operator<br />(<span class="mandatory">*</span>)</th>
                                    <th nowrap>Value (in Nu.)<br />(<span class="mandatory">*</span>)</th>
                                    <th nowrap>Value<br/>(in Words (Nu.))<span class="mandatory"></span></th>
                                    <th nowrap>SBD Name<br />(<span class="mandatory">*</span>)</th>
                                </tr>
                                <input type="hidden" name="delrow" value="0" id="delrow"/>


                                <%

                                            List l = null;

                                            if (request.getParameter("id") != null && "edit".equals(request.getParameter("action"))) {

                                                String action = request.getParameter("action");
                                                int id = Integer.parseInt(request.getParameter("id"));
                                                %>
                                                <input type="hidden" id="hiddenaction" name="action" value="<%=action%>"/>
                                                <input type="hidden" id="hiddenid" name="id" value="<%=id%>"/>
                                                <%
                                                l = configPreTenderRuleSrBean.getConfigStd(id);
                                            } else
                                                    l = configPreTenderRuleSrBean.getAllConfigStd();



                                            int i = 0;
                                            int length = 0;
                                            if (!l.isEmpty() || l != null) {

                                                List getProcNature = configPreTenderRuleSrBean.getProcurementNature();
                                                List getProcMethod = configPreTenderRuleSrBean.getProcurementMethod();
                                                List getProcTypes = configPreTenderRuleSrBean.getProcurementTypes();
                                                //List getTempMaster = configPreTenderRuleSrBean.getTemplateMaster();
                                                List getTempMaster = configPreTenderRuleSrBean.getAcceptTemplateMaster();
                                                Iterator it = l.iterator();


                                                while (it.hasNext()) {
                                                    configStd = (TblConfigStd) it.next();
                                                    i++;
                                                    length++;
                                                    StringBuilder pNature = new StringBuilder();
                                                    StringBuilder pType = new StringBuilder();
                                                    StringBuilder pMethod = new StringBuilder();
                                                    StringBuilder stName = new StringBuilder();
                                                    StringBuilder operator = new StringBuilder();





                                                    TblProcurementNature tblProcureNature = new TblProcurementNature();
                                                    Iterator pnit = getProcNature.iterator();
                                                    while (pnit.hasNext()) {
                                                        tblProcureNature = (TblProcurementNature) pnit.next();
                                                        pNature.append("<option value='");
                                                        if (tblProcureNature.getProcurementNatureId() == configStd.getTblProcurementNature().getProcurementNatureId()) {

                                                            pNature.append(tblProcureNature.getProcurementNatureId() + "' selected='true'>" + tblProcureNature.getProcurementNature());
                                                        } else {
                                                            pNature.append(tblProcureNature.getProcurementNatureId() + "'>" + tblProcureNature.getProcurementNature());
                                                        }
                                                        pNature.append("</option>");
                                                    }

                                                    TblProcurementMethod tblProcurementMethod = new TblProcurementMethod();
                                                    Iterator pmit = getProcMethod.iterator();
                                                    while (pmit.hasNext()) {
                                                        tblProcurementMethod = (TblProcurementMethod) pmit.next();
                                                        if("OTM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                "LTM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                "DP".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                "RFQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                "FC".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                "DPM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())||
                                                                 "QCBS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                 "LCS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                 "SFB".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                 "SBCQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                 "SSS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                 "IC".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()))
                                                          {
                                                                pMethod.append("<option value='");
                                                                if (tblProcurementMethod.getProcurementMethodId() == configStd.getTblProcurementMethod().getProcurementMethodId()) {
                                                                    if(tblProcurementMethod.getProcurementMethod().equalsIgnoreCase("RFQ"))
                                                                    {
                                                                        pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>LEM");
                                                                    }
                                                                    else if(tblProcurementMethod.getProcurementMethod().equalsIgnoreCase("DPM"))
                                                                    {
                                                                        pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>DCM");
                                                                    }
                                                                    else
                                                                    {
                                                                        pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>" + tblProcurementMethod.getProcurementMethod());
                                                                    }

                                                                } else {

                                                                    if(tblProcurementMethod.getProcurementMethod().equalsIgnoreCase("RFQ"))
                                                                    {
                                                                        pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>LEM");
                                                                    }
                                                                    else if(tblProcurementMethod.getProcurementMethod().equalsIgnoreCase("DPM"))
                                                                    {
                                                                        pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>DCM");
                                                                    }
                                                                    else
                                                                    {
                                                                        pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>" + tblProcurementMethod.getProcurementMethod());
                                                                    }  

                                                                }
                                                                    pMethod.append("</option>");
                                                          }
                                                        
                                                    }

                                                    TblProcurementTypes tblProcurementTypes = new TblProcurementTypes();
                                                    Iterator ptit = getProcTypes.iterator();
                                                    String procureType="";
                                                    while (ptit.hasNext()) {
                                                        tblProcurementTypes = (TblProcurementTypes) ptit.next();
                                                        if(tblProcurementTypes.getProcurementType().equalsIgnoreCase("NCT"))
                                                                procureType="NCB";
                                                        else if(tblProcurementTypes.getProcurementType().equalsIgnoreCase("ICT"))
                                                                procureType="ICB";
                                                        pType.append("<option value='");
                                                        if (tblProcurementTypes.getProcurementTypeId() == configStd.getTblProcurementTypes().getProcurementTypeId()) {
                                                            pType.append(tblProcurementTypes.getProcurementTypeId() + "' selected='true'>" + procureType);
                                                        } else {
                                                            pType.append(tblProcurementTypes.getProcurementTypeId() + "'>" + procureType);
                                                        }
                                                        pType.append("</option>");

                                                    }

                                                    TblTemplateMaster tblTemplateMaster = new TblTemplateMaster();
                                                    Iterator tmpit = getTempMaster.iterator();
                                                    while (tmpit.hasNext()) {
                                                        tblTemplateMaster = (TblTemplateMaster) tmpit.next();


                                                        stName.append("<option value='");
                                                        if (tblTemplateMaster.getTemplateId() == configStd.getTblTemplateMaster().getTemplateId()) {
                                                            stName.append(tblTemplateMaster.getTemplateId() + "' selected='true'>" + tblTemplateMaster.getTemplateName());
                                                        } else {
                                                            stName.append(tblTemplateMaster.getTemplateId() + "'>" + tblTemplateMaster.getTemplateName());
                                                        }
                                                        stName.append("</option>");



                                                    }

                                                    String[] opt = {">", ">=", "<", "<=", "="};
                                                    int a = opt.length;

                                                    for (int j = 0; j < a; j++) {

                                                        operator.append("<option ");
                                                        if (opt[j].equals(configStd.getOperator())) {
                                                            operator.append(" selected='true'>" + opt[j]);
                                                        } else {
                                                            operator.append(">" + opt[j]);
                                                        }
                                                        operator.append("</option>");

                                                    }


                                %>

                                <tr id="trrule_<%=i%>">
                                    <td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_<%=i%>" value="" /></td>
                                    <td class="t-align-center">
                                        <select name="tenderType<%=i%>" class="formTxtBox_1" id="tenderType_<%=i%>">
                                            <% for (Object[] proc : tenderTypes) {
                                            if(!proc[1].equals("PQ") && !proc[1].equals("RFA") && !proc[1].equals("2 Stage-PQ")){    
                                            %>
                                            <option value="<%=proc[0]%>" <%if (Integer.parseInt(proc[0].toString()) == configStd.getTenderTypeId()) {
                                                     out.print(" selected ");
                                                 }%>><%=proc[1]%></option>
                                            <%}}%>
                                        </select>
                                    </td>
                                    <td class="t-align-center"><select name="pNature<%=i%>" class="formTxtBox_1" id="pNature_<%=i%>">
                                            <%=pNature%>
                                        </select></td>
                                    <td class="t-align-center"><select name="pMethod<%=i%>" class="formTxtBox_1" id="pMethod_<%=i%>">
                                            <%=pMethod%>
                                        </select></td>
                                    <td class="t-align-center"><select name="pType<%=i%>" class="formTxtBox_1" id="pType_<%=i%>">
                                            <%=pType%>
                                        </select></td>
                                    <td class="t-align-center"><select name="operator<%=i%>" class="formTxtBox_1" id="operator_<%=i%>">
                                            <%=operator%>
                                        </select></td>
                                    <td class="t-align-center"><input name="values<%=i%>" type="text" class="formTxtBox_1" maxlength="18" id="values_<%=i%>" value="<%=configStd.getTenderValue()%>" onblur="return chkValueBlank(this);"/><span id="val_<%=i%>" style="color: red;">&nbsp;</span></td>
                                    <td class="t-align-center"><span id="wordval_<%=i%>" >&nbsp;</span></td>
                                    <td class="t-align-center"><select name="stdName<%=i%>" class="formTxtBox_1" id="stdName_<%=i%>">
                                            <%=stName%></select>
                                    </td>
                                </tr>

                                <%
                                              /*  }
                                              }
                                            }*/
                                          }
                                        } else {

                                                msg = "No Record Found";
                                         }

                                %>

                            </table>
                            <input type="hidden" name="TotRule" id="TotRule" value="<%=i%>"/>
                            <input type="hidden" name="introw" value="" id="introw"/>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" >
                                <tr>
                                    <%if ("edit".equals(request.getParameter("action"))) {%>
                                    <td align="right" colspan="9" class="t-align-center">
                                        <span class="formBtn_1"><input type="submit" name="button" id="button" value="Update" onclick="return validate();"/></span>
                                    </td>
                                    <%} else {%>
                                    <td align="right" colspan="9" class="t-align-center">
                                        <span class="formBtn_1"><input type="submit" name="button" id="button" value="Submit" onclick="return validate();"/></span>
                                    </td>
                                    <%}%>
                                </tr>
                            </table>
                            <%--<div align="center"> <%=msg%></div>--%>

                        </form>
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <script>
                var obj = document.getElementById('lblSTDRuleEdit');
                if(obj != null){
                    if(obj.innerHTML == 'Edit'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
        <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
        <script type="text/javascript">
                function convert()
                {
                for(var k=1; k<=<%= length%>; k++)
                    {
                        //document.getElementById("wordval_"+k).innerHTML = DoIt(document.getElementById("values_"+k).value);
                        document.getElementById("wordval_"+k).innerHTML = CurrencyConverter(document.getElementById("values_"+k).value);
                    }
                }
        </script>
        <script type="text/javascript">

            var counter = 1;
            var delCnt = 0;
            $(function() {
                $('#linkAddRule').click(function() {
                    counter = eval(document.getElementById("TotRule").value);
                    delCnt = eval(document.getElementById("delrow").value);

                    $('span.#lotMsg').css("visibility","collapse");
                    var htmlEle = "<tr id='trrule_"+ (counter+1) +"'>"+
                        "<td class='t-align-center'><input type='checkbox' name='checkbox"+(counter+1)+"' id='checkbox_"+(counter+1)+"' value='"+(counter+1)+"' /></td>"+
                        "<td class='t-align-center'><select name='tenderType"+(counter+1)+"' class='formTxtBox_1' id='tenderType_"+(counter+1)+"'><% for (Object[] procc : tenderTypes) {if(!procc[1].equals("PQ") && !procc[1].equals("RFA") && !procc[1].equals("2 Stage-PQ")){%> <option value='<%=procc[0]%>'><%=procc[1]%></option> <%}}%></select></td>"+
                        "<td class='t-align-center'><select name='pNature"+(counter+1)+"' class='formTxtBox_1' id='pNature_"+(counter+1)+"'><%=procNature%></select></td>"+
                        "<td class='t-align-center'><select name='pMethod"+(counter+1)+"' class='formTxtBox_1' id='pMethod_"+(counter+1)+"'><%=procMethod%></select></td>"+
                        "<td class='t-align-center'><select name='pType"+(counter+1)+"' class='formTxtBox_1' id='pType_"+(counter+1)+"'><%=procType%></select></td>"+
                        "<td class='t-align-center'><select name='operator"+(counter+1)+"' class='formTxtBox_1' id='operator_"+(counter+1)+"'><option><</option><option><=</option><option>></option><option>>=</option><option>=</option></</select></td>"+
                        "<td  class='t-align-center'><input type='text' name='values"+(counter+1)+"' class='formTxtBox_1' maxlength='18' id='values_"+(counter+1)+"' onblur='return chkValueBlank(this);'/><span id='val_"+(counter+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td  class='t-align-center'><span id='wordval_"+(counter+1)+"' >&nbsp;</span></td>"+
                        "<td class='t-align-center'><select name='stdName"+(counter+1)+"' class='formTxtBox_1' id='stdName_"+(counter+1)+"'><%=templateMaster%></select></td>"+
                        "</tr>";
                    $("#tblrule").append(htmlEle);
                    document.getElementById("TotRule").value = (counter+1);

                });
            });

            $(function() {
                $('#linkDelRule').click(function() {
                    counter = eval(document.getElementById("TotRule").value);
                    delCnt = eval(document.getElementById("delrow").value);

                    var tmpCnt = 0;
                    for(var i=1;i<=counter;i++){
                        if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                            tmpCnt++;
                        }
                    }

                    if(tmpCnt==(eval(counter-delCnt))){
                        $('span.#lotMsg').css("visibility","visible");
                        $('span.#lotMsg').css("color","red");
                        $('span.#lotMsg').html('Minimum 1 record is needed!');
                    }else{
                        if(tmpCnt>0){jConfirm('Are You Sure You want to Delete.','Procurement Method Business Rule Configuration', function(ans) {if(ans){
                            for(var i=1;i<=counter;i++){
                                if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                                    $("tr[id='trrule_"+i+"']").remove();
                                    $('span.#lotMsg').css("visibility","collapse");
                                    delCnt++;
                                }
                            }
                            document.getElementById("delrow").value = delCnt;
                        }});}else{jAlert("please Select checkbox first","Procurement Method Business Rule Configuration", function(RetVal) {});}
                    }

                });
            });
        </script>
        <script type="text/javascript">
            function validate()
            {
                //alert('dhruti');
                var flag = true;
                var counter = eval(document.getElementById("TotRule").value);
                if(document.getElementById("hiddenaction"))
                {
                    if("edit"==document.getElementById("hiddenaction").value)
                    {
                    $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/BusinessRuleConfigurationServlet",
                        data:"configStdId="+$('#hiddenid').val()+"&"+"tenderType="+$('#tenderType_1').val()+"&"+"procNature="+$('#pNature_1').val()+"&"+"procMethod="+$('#pMethod_1').val()+"&"+"procType="+$('#pType_1').val()+"&"+"operator="+$('#operator_1').val()+"&"+"valueinbdtaka="+$('#values_1').val()+"&"+"stdname="+$('#stdName_1').val()+"&"+"funName=STDSelectionRule",
                        async: false,
                        success: function(j){

                            if(j.toString()!="0"){
                                flag=false;
                                jAlert("Combination of 'TenderType' , 'ProcurementNature' , 'ProcurementMethod' , 'ProcurementType' , 'Operator'and value (in Nu.) already exist.","Standard Bidding Document Selection Business Rule", function(RetVal) {
                                });
                            }
                        }
                    });
                    }
                }
                for(var k=1;k<= counter;k++)
                {
                    if(document.getElementById("values_"+k)!=null)
                    {
                        if(document.getElementById("values_"+k).value==""){
                            document.getElementById("val_"+k).innerHTML="<br/>Please Enter Value";
                            flag = false;
                        }
                        else
                        {
                            if(decimal(document.getElementById("values_"+k).value))
                            {
                                if(chkMax(document.getElementById("values_"+k).value))
                                {
                                        document.getElementById("val_"+k).innerHTML="";
                                }
                                else
                                {
                                    document.getElementById("val_"+k).innerHTML="<br/>Maximum 12 digits are allowed.";
                                    flag=false;
                                }
                            }
                            else
                            {
                                document.getElementById("val_"+k).innerHTML="<br/>Please Enter numbers and 2 Digits After Decimal";
                                return false;
                            }
                        }
                    }
                }

                // Start OF Dhruti--Checking for Unique Rows

              if(document.getElementById("TotRule")!=null){
                var totalcount = eval(document.getElementById("TotRule").value); }//Total Count After Adding Rows
                //alert(totalcount);
                var chk=true;
                var i=0;
                var j=0;
                for(i=1; i<=totalcount; i++) //Loop For Newly Added Rows
                {
                    if(document.getElementById("values_"+i)!=null)
                    if($.trim(document.getElementById("values_"+i).value)!='') // If Condition for When all Data are filled thiscode is Running
                    {

                        for(j=1; j<=totalcount && j!=i; j++) // Loop for Total Count but Not same as (i) value
                        {
                            chk=true;
                            //If Condition for Check Duplicate Rows are there or not.If Columns are diff then chk variable set to false
                            //IF Row is same Give alert message.
                            if($.trim(document.getElementById("pNature_"+i).value) != $.trim(document.getElementById("pNature_"+j).value))
                            {
                                chk=false;
                                //alert('bt');
                            }
                            if($.trim(document.getElementById("tenderType_"+i).value) != $.trim(document.getElementById("tenderType_"+j).value))
                            {
                                chk=false;
                                //alert('bt');
                            }

                            if($.trim(document.getElementById("pMethod_"+i).value) != $.trim(document.getElementById("pMethod_"+j).value))
                            {
                                chk=false;
                                //alert('tt');
                            }
                            if($.trim(document.getElementById("pType_"+i).value) != $.trim(document.getElementById("pType_"+j).value))
                            {
                                chk=false;
                                //alert('pm');
                            }
                            if($.trim(document.getElementById("operator_"+i).value) != $.trim(document.getElementById("operator_"+j).value))
                            {
                                chk=false;
                                //alert('pt');
                            }
                            var vi=document.getElementById("values_"+i).value;
                            var vj=document.getElementById("values_"+j).value;
                            if(vi.indexOf(".")==-1)
                            {
                                vi=vi+".00";
                                //alert(vi);
                            }
                            if(vj.indexOf(".")==-1)
                            {
                                vj=vj+".00";
                                //alert(vj);
                            }
                            if($.trim(vi) != $.trim(vj))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("stdName_"+i).value) != $.trim(document.getElementById("stdName_"+j).value))
                            {
                                chk=false;
                                //alert('pt');
                            }
                            /*     if($.trim(document.getElementById("stdName_"+i).value) != $.trim(document.getElementById("stdName_"+j).value))
                        {
                            chk=false;
                            //alert('pt');
                        }
                             */
                        //alert(j);
                        //alert("chk" +chk);
                            if(flag){
                        if(chk==true) //If Row is same then give alert message
                        {
                                    jAlert("Duplicate record found. Please enter unique record","SBD Selection Business Rule Configuration",function(RetVal) {
                                    });
                            return false;
                        }
                    }
                        }

                }
            }
            // End OF Dhruti--Checking for Unique Rows

            if (flag == false){
                return false;
            }
        }
        function decimal(value) {
            return /^(\d+(\.\d{2})?)$/.test(value);
        }
        function chkMax(value)
            {
                var chkVal=value;
                var ValSplit=chkVal.split('.');

                if(ValSplit[0].length>12)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        function chkValueBlank(obj){
            var i = (obj.id.substr(obj.id.indexOf("_")+1));
            //document.getElementById("wordval_"+i).innerHTML = DoIt(document.getElementById("values_"+i).value);
            document.getElementById("wordval_"+i).innerHTML = CurrencyConverter(document.getElementById("values_"+i).value);
            if(obj.value!='')
            {
                if(decimal(obj.value))
                    {
                        if(chkMax(obj.value))
                        {
                            document.getElementById("val_"+i).innerHTML="";
                        }
                        else
                        {
                            document.getElementById("val_"+i).innerHTML="<br/>Maximum 12 digits are allowed.";
                            flag=false;
                        }
                    }
                    else
                    {
                        document.getElementById("val_"+i).innerHTML="<br/>Please Enter numbers and 2 Digits After Decimal";
                        return false;
                    }
            }
            else{
                document.getElementById("val_"+i).innerHTML = "<br/>Please Enter Value";
                return false;
            }
        }
        </script>
    </body>
</html>

