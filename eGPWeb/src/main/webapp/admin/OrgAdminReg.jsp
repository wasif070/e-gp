<%--
Document   : OrganizationAdminRegister
Created on : Oct 23, 2010, 6:13:00 PM
Author     : Sanjay,rishita
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Register Organization Admin</title>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                var validCheckMail = true;
                var validCheckNid = true;
                var validCheckMno = true;
                $("#frmOrganizationAdmin").validate({
                    rules: {
                        organization: {required: true},
                        emailId: {required: true  /*, email: true*/},
                        password: {spacevalidate: true, requiredWithoutSpace: true, maxlength: 25, minlength: 8, alphaForPassword: true},
                        confPassword: {required: true, equalTo: "#txtPassword"},
                        //, spacevalidate: true 
                        fullName: {spacevalidate: true, requiredWithoutSpace: true, alphaName: true, maxlength: 100},
                        nationalId: {required: true, number: true, maxlength: 11, minlength: 11},
                        phoneNo: {required: true, numberWithHyphen: true, PhoneFax: true},
                        mobileNo: {required: true, number: true, minlength: 8, maxlength: 8}
                    },
                    messages: {
                        organization: {required: "<div class='reqF_1'>Please select Organization</div>"},
                        emailId: {required: "<div class='reqF_1'>Please enter e-mail ID</div>"
                                    //, email: "<div class='reqF_1'>e-mail ID must be in xyz@abc.com format</div>"
                        },
                        password: {spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace: "<div class='reqF_1'>Please enter Password</div>",
                            alphaForPassword: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>",
                            //spacevalidate: "<div class='reqF_1'>Space is not allowed</div>",
                            maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>",
                            minlength: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>"},
                        confPassword: {
                            required: "<div class='reqF_1'>Please re-type Password</div>",
                            equalTo: "<div class='reqF_1'>Password does not match. Please try again</div>"
                        },
                        //spacevalidate: "<div class='reqF_1'>Space is not allowed</div>"},

                        fullName: {
                            spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace: "<div class='reqF_1'>Please enter Full Name</div>",
                            alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only </div>",
                            maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"},
                        nationalId: {
                            required: "<div class='reqF_1'>Please enter CID No.</div>",
                            number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                            maxlength: "<div class='reqF_1'>CID should comprise of maximum 11 digits</div>",
                            minlength: "<div class='reqF_1'>CID should comprise of minimum 11 digits</div>"
                        },
                        phoneNo: {required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            numberWithHyphen: "<div class='reqF_1'>Allows numbers (0-9) and hyphen (-) only</div>",
                            PhoneFax: "<div class='reqF_1'>Please enter valid Phone No. Area code should contain minimum 2 digits and maximum 5 digits. Phone no. should contain minimum 3 digits and maximum 10 digits</div>"},
                        mobileNo: {
                            required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                            number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                            minlength: "<div class='reqF_1'>Minimum 8 digits are required</div>",
                            maxlength: "<div class='reqF_1'>Allows maximum 8 digits only</div>"
                        }
                    },
                    errorPlacement: function (error, element)
                    {
                        if (element.attr("name") == "phoneNo")
                        {
                            error.insertAfter("#fxno");
                        } else if (element.attr("name") == "password")
                        {
                            error.insertAfter("#tipPassword");
                        } else
                        {
                            error.insertAfter(element);

                        }
                    }
                });

            });

            function checkctrlkey() {
                //if(e.keyCode==17 || e.keyCode==93){
                jAlert("Copy Paste not allowed.", "Organization Admin", function (RetVal) {
                });
                //alert('Copy Paste not allowed.');
                return false;
                //}
            }

            function checkrightclick(e) {
                $(e).bind("contextmenu", function (e) {
                    return false;
                });
            }
        </script>

    </head>
    <jsp:useBean id="orgAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.OrgAdminSrBean"/>
    <jsp:useBean id="adminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AdminMasterDtBean"/>
    <jsp:useBean id="loginMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.LoginMasterDtBean"/>
    <jsp:setProperty name="adminMasterDtBean" property="*"/>
    <jsp:setProperty name="loginMasterDtBean" property="*"/>
    <body>
        <%
            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                orgAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
            }
            if ("Submit".equals(request.getParameter("add"))) {
                String msg = "";
                int strM = -1; // Default
                String mailId = loginMasterDtBean.getEmailId();
                // String nationalId = adminMasterDtBean.getNationalId();
                //String mobileNo = adminMasterDtBean.getMobileNo();  - Unique mobile no. check removed
                if (!mailId.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-\\-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                    strM = 1; //Not Valid EmailId
                }
                if (strM == -1) {
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    msg = commonService.verifyMail(mailId.trim());
                    if (msg.length() > 2) {
                        strM = 2; //EmailId Already Exist
                    }
                    /*  if (strM == -1) {
                                msg = commonService.verifyNationalId(nationalId.trim());
                                if (msg.length() > 2) {
                                    strM = 3; //NationalId Already Exist
                                }
                            }*/
 /*if (strM == -1) { - Unique mobile no. check removed 
                                msg = commonService.verifyMobileNo(mobileNo.trim());
                                if (msg.length() > 2) {
                                    strM = 4; //MobileNo Already Exist
                                }
                            }*/
                }
                if (strM == -1) {
                    orgAdminSrBean.setOfficeId(Integer.parseInt(request.getParameter("organization")));

                    if (adminMasterDtBean.getMobileNo() == null) {
                        adminMasterDtBean.setMobileNo("");
                    }
                    if (adminMasterDtBean.getNationalId() == null) {
                        adminMasterDtBean.setNationalId("");
                    }

                    orgAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                    int userId = orgAdminSrBean.orgAdminReg(adminMasterDtBean, loginMasterDtBean, request.getParameter("password"));
                    if (userId > 0) {
                        response.sendRedirect("ViewManageAdmin.jsp?userId=" + userId + "&userTypeid=5&msg=create");
                    } else {
                        response.sendRedirect("OrgAdminReg.jsp?msg=orgAdminCreateFail");
                    }
                } else {
                    response.sendRedirect("OrgAdminReg.jsp?strM=" + strM);
                }
            } else {
        %>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("hdnUserType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                    userType.append(request.getParameter("hdnUserType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp">
                            <jsp:param name="hdnUserType" value="<%=userType%>"/>
                        </jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <%
                                int strM = -1;
                                String msg = "";
                                if (request.getParameter("strM") != null) {
                                    try {
                                        strM = Integer.parseInt(request.getParameter("strM"));
                                    } catch (Exception ex) {
                                        strM = 5;
                                    }
                                    switch (strM) {
                                        case (1):
                                            msg = "Please enter valid e-mail ID.";
                                            break;
                                        case (2):
                                            msg = "e-mail ID already exists, Please enter another e-mail ID.";
                                            break;
                                        case (3):
                                            msg = "CID already exists.";
                                            break;
                                        case (4):
                                            msg = "Mobile No already exists.";
                                            break;
                                    }
                            %>
                            <div class="responseMsg errorMsg" style="margin-left:7px; margin-right: 2px; margin-top: 15px;">
                                <%= msg%>
                            </div>
                            <%
                                    msg = null;
                                }
                            %>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1">
                                        <div class="pageHead_1">Create Organization Admin</div>
                                        <% if (request.getParameter("msg") != null && "orgAdminCreateFail".equals(request.getParameter("msg"))) {%>
                                        <div class='responseMsg errorMsg'><%=appMessage.orgAdminReg%></div>
                                        <% } %>
                                        <form id="frmOrganizationAdmin" name="frmOrganizationAdmin" id="frmOrganizationAdmin" method="post">
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td style="font-style: italic" class="ff t-align-left" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Select Organization : <span>*</span></td>
                                                    <td>
                                                        <select  style="width:400px;" class="formTxtBox_1" name="organization" id="cmbOrganization">
                                                            <option  value="">-- Select Organization --</option>
                                                            <c:forEach var="cList" items="${orgAdminSrBean.organizationList}">
                                                                <option  value="${cList.objectId}">${cList.objectValue}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">e-mail ID: <span>*</span></td>
                                                    <td>
                                                        <input style="width:195px;" class="formTxtBox_1" type="text" id="txtMail" name="emailId" maxlength="100"/>
                                                        <span id="mailMsg" style="color: red; font-weight: bold"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Password : <span>*</span></td>
                                                    <td>
                                                        <input  style="width:195px;" class="formTxtBox_1" type="password" id="txtPassword" name="password" maxlength="25" autocomplete="off" /><br/><span id="tipPassword" style="color: grey;"> ( Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. 
                                                <br/>Special characters may be added) </span>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="ff">Confirm Password : <span>*</span></td>
                                                    <td><input style="width:195px;" class="formTxtBox_1" type="password" id="txtConfPassword" name="confPassword" value="" onpaste="return checkctrlkey();" autocomplete="off" />
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td class="ff">Full Name : <span>*</span></td>

                                                    <td><input style="width:195px;" class="formTxtBox_1" type="text" id="txtFullName" name="fullName" maxlength="101"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">CID No. : <span>*</span></td>

                                                    <td><input class="formTxtBox_1" style="width:195px;" type="text" id="txtNationalId" name="nationalId" maxlength="26"/>
                                                        <span id="errMsg" style="color: red; font-weight: bold"></span>
                                                    </td>
                                                </tr>
                                               <!-- <tr>
                                                    <td class="ff">Phone No. : <span>*</span></td>
                                                    <td><input type="hidden" style="width:195px;" class="formTxtBox_1" type="text" name="phoneNo" id="txtPhoneNo" maxlength="17" value="11-11111111"/>
                                                        <span id="fxno" name="fxno" style="color: grey;"> (Area Code - Phone No. e.g. 2-324425)</span>
                                                    </td>
                                                </tr>-->
                                                <tr>
                                                    <td class="ff">Mobile No. : <span>*</span></td>
                                                    <td><input style="width:195px;" class="formTxtBox_1" type="text" name="mobileNo" id="txtMobileNo" maxlength="16"/><span id="mNo" style="color: grey;"> (Mobile No. format should be e.g 12345678)</span>
                                                        <span id="mobMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td align="left">
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="add" id="btnAdd" value="Submit" onClick="return validate();"/>
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                </div>
            </div>
        <%
            }
        %>
        <script>
            var obj = document.getElementById('lblOrgAdminCreation');
            if (obj != null) {
                if (obj.innerHTML == 'Create Organization Admin') {
                    obj.setAttribute('class', 'selected');
                }
            }

        </script>
        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
    <script type="text/javascript">
        $(function () {
            $('#txtMail').blur(function () {
                var varEmailId = trim(document.getElementById("txtMail").value);
                if (varEmailId != "")
                {
                    $('span.#mailMsg').html("Checking for unique Mail Id...");
                    $.post("<%=request.getContextPath()%>/CommonServlet", {mailId: varEmailId, funName: 'verifyMail'}, function (j) {
                        if (j.toString().indexOf("OK", 0) != -1) {
                            $('span.#mailMsg').css("color", "green");
                            validCheckMail = true;
                        } else if (j.toString().indexOf("Mail", 0) != -1) {
                            $('span.#mailMsg').css("color", "red");
                            validCheckMail = false;
                        }
                        $('span.#mailMsg').html(j);
                    });
                } else
                {
                    $('span.#mailMsg').html("");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $('#txtNationalId').blur(function () {
                var varNationalId = trim(document.getElementById("txtNationalId").value);
                if (varNationalId != "") {
                    //   $('span.#errMsg').html("Checking for unique National Id...");
                    $.post("<%=request.getContextPath()%>/CommonServlet", {nationalId: varNationalId, funName: 'verifyNationalId'}, function (j) {

                        if (j.toString().indexOf("OK", 0) == 0) {
                            $('span.#errMsg').css("color", "white");
                            validCheckNid = true;
                        } else if (j.toString().indexOf("National", 0) != -1) {
                            $('span.#errMsg').css("color", "red");
                            validCheckNid = false;
                        }
                        $('span.#errMsg').html(j);
                    });
                } else {
                    $('span.#errMsg').html("");
                }
            });
        });

//            $(function() { - Unique mobile no. check removed
//                $('#txtMobileNo').blur(function() {
//                    var varMobileNo = trim(document.getElementById("txtMobileNo").value);
//                    if(varMobileNo!=""){
//                        if((document.getElementById("txtMobileNo").value.length>=10) && (document.getElementById("txtMobileNo").value.length<=15) && (/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(document.getElementById("txtMobileNo").value))) // max min number
//                        {
//                            $('span.#mobMsg').html("Checking for unique Mobile No...");
//                            $.post("<%//request.getContextPath()%>/CommonServlet", {mobileNo:varMobileNo,funName:'verifyMobileNo'},  function(j){
//
//                                if(j.toString().indexOf("OK", 0)==0){
//                                    $('span.#mobMsg').css("color","green");
//                                    validCheckMno = true;
//                                }
//                                else if(j.toString().indexOf("Mobile", 0)!=-1){
//                                    $('span.#mobMsg').css("color","red");
//                                    validCheckMno = false;
//                                }
//                                $('span.#mobMsg').html(j);
//                            });
//                        }
//                    }
//                    else{
//                        $('span.#mobMsg').html("");
//                    }
//                });
//            });

        function validate() {
            if (document.getElementById("mailMsg") != null && document.getElementById("mailMsg").innerHTML == "e-mail ID already exists, Please enter another e-mail ID.") {
                return false;
            }
            if (document.getElementById("errMsg") != null && document.getElementById("errMsg").innerHTML == "CID already exists.") {
                return false;
            }
            if (document.getElementById("mobMsg") != null && document.getElementById("mobMsg").innerHTML == "Mobile No. already exists.") {
                return false;
            }
            //if(validCheckMail && validCheckNid && validCheckMno){
            if (validCheckMail && validCheckNid) {

            } else {
                return false;
            }
        }
    </script>
</html>
