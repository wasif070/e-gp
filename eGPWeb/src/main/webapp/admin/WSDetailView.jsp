<%--
    Document   : WSDetail
    Created on : Jun 6, 2011, 5:14:26 AM
    Author     : visitor
--%>

<%@page import="com.cptu.egp.eps.model.table.TblWsMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.WsMasterService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Detail View Of Web Service</title>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <%
                    StringBuilder userType = new StringBuilder();
                    if (request.getParameter("userType") != null) {
                        if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                            userType.append(request.getParameter("userType"));
                        } else {
                            userType.append("org");
                        }
                    } else {
                        userType.append("org");
                    }
                    int webServiceId = Integer.parseInt(request.getParameter("webServiceId"));
                    WsMasterService wsMasterService = (WsMasterService) AppContext.getSpringBean("WsMasterService");
                    TblWsMaster wsMaster = wsMasterService.getTblWsMaster(webServiceId);
                    String statusString = "";
                    String authString = "";
                    if ("Y".equalsIgnoreCase(wsMaster.getIsActive())) {
                        statusString = "Active";
                    } else {
                        statusString = "Deactivate";
                    }
                    if ("Y".equalsIgnoreCase(wsMaster.getIsAuthenticate())) {
                        authString = "Required";
                    } else {
                        authString = "Not Required";
                    }
        %>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType.toString()%>" ></jsp:include>
                        <td class="contentArea">
                            <div>
                                <div class="pageHead_1">
                                    Details of a Web-Service
                                    <span style="float: right; text-align: right;">
                                        <a class="action-button-goback" href="WSList.jsp?mode=view">Go back</a>
                                    </span>
                                </div>
                            </div>
                            <form id="formAddWebSerice" method="post" action="/WebServiceControlServlet?action=view">
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td class="ff" width="20%">
                                            Web-Service Name : 
                                        </td>
                                        <td width="80%">
                                            <%=wsMaster.getWsName()%>
                                            <br/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">
                                            Web-Service Description :
                                        </td>
                                        <td width="80%">
                                            <%=wsMaster.getWsDesc()%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">
                                            Status :
                                        </td>
                                        <td>
                                            <%=statusString%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">
                                            Authentication :
                                        </td>
                                        <td>
                                            <%=authString%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="t-align-left">
                                            <label class="formBtn_1">
                                                <input type="button" name="buttonWebSerivce" value="Edit" onClick="window.location.href='WSDetailEdit.jsp?webServiceId=<%=wsMaster.getWsId()%>&action=edit&parentPage=viewPage';"/>
                                            </label>
                                        </td>
                                    </tr>
                                </table>   
                                <%--
                                         <label class="formBtn_1">
                                            <input type="button" name="buttonWebSerivce" value="View All Services" onClick="window.location.href='WSList.jsp';"/>
                                        </label>
                                --%>
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
    <jsp:include page="../resources/common/Bottom.jsp"></jsp:include>
</html>
