<%-- 
    Document   : NewsManagement
    Created on : Nov 19, 2010, 12:42:12 PM
    Author     : Karan
--%>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.web.servlet.BOQServlet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            BSR List
        </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <script type="text/javascript" >
            function ConfirmDelete(id) {
                jConfirm('Do you want to delete this BSR?', 'BoQ', function (ans) {
                    if (ans) {
                        $.post("<%=request.getContextPath()%>/BOQServlet", {funName: "delete", id: id}, function (j) 
                        {
                            if (j) 
                            {
                                window.location="<%=request.getContextPath()%>/admin/BOQList.jsp?msgId=deleted";
                            }
                            else
                            {
                                alert('Unable to delete BSR');
                            }
                        });
                    }
                });
            }
        </script>
        <%
            String newsType = "N";
//            int counterId = 0;
//
//            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

//            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
//            List<SPCommonSearchData> data = commonSearchService.searchData("BOQList", null, null, null, null, null, null, null, null, null);

            String userID = "0";
            if (session.getAttribute("userId") != null) {
                userID = session.getAttribute("userId").toString();
            }
//            commonSearchService.setLogUserId(userID);

        %>
        <script type="text/javascript">
            function chkdisble(pageNo) {
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if (parseInt($('#pageNo').val(), 10) != 1) {
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if (parseInt($('#pageNo').val(), 10) == 1) {
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if (parseInt($('#pageNo').val(), 10) == 1) {
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if (parseInt($('#pageNo').val(), 10) > 1) {
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if (parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())) {
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                } else {
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if (parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())) {
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                } else {
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            function loadTable()
            {
                $.post("<%=request.getContextPath()%>/BOQServlet", {funName: "BOQList", pageNo: $("#pageNo").val(), size: $("#size").val()}, function (j) {
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    sortTable();
                    if ($('#noRecordFound').attr('value') == "noRecordFound") {
                        $('#pagination').hide();
                    } else {
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if ($("#totalPages").val() == 1) {
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    } else {
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            $(function () {
                $('#btnFirst').click(function () {
                    var totalPages = parseInt($('#totalPages').val(), 10);
                    var pageNo = $('#pageNo').val();
                    if (totalPages > 0 && pageNo != "1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#btnLast').click(function () {
                    var totalPages = parseInt($('#totalPages').val(), 10);
                    if (totalPages > 0)
                    {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#btnNext').click(function () {
                    var pageNo = parseInt($('#pageNo').val(), 10);
                    var totalPages = parseInt($('#totalPages').val(), 10);

                    if (pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo) + 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) + 1);

                        $('#dispPage').val(Number(pageNo) + 1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function () {
                $('#btnPrevious').click(function () {
                    var pageNo = $('#pageNo').val();
                    if (parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#btnGoto').click(function () {
                    var pageNo = parseInt($('#dispPage').val(), 10);
                    var totalPages = parseInt($('#totalPages').val(), 10);
                    if (pageNo > 0)
                    {
                        if (pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
        </script>

    </head>
    <body onload="loadTable();">
        <form name="frmsubmit" id="frmsubmit" method="post"></form>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                <tr>
                    <%--<%@include file="../resources/common/AfterLoginLeft.jsp" %>--%>
                    <jsp:include page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                        <td class="contentArea">
                            <!--Dashboard Content Part Start-->
                            <div class="pageHead_1">View BSR
                                <span style="float: right;">
<!--                                    <a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('6');">Save as PDF</a>-->
                                </span>
                            </div>
                            <div class="t-align-right t_space b_space">
                                <a href="AddBOQ.jsp" class="action-button-add" >Add BSR</a>

                            </div>

                        <%if (request.getParameter("msgId") != null) {
                                String msgId = "", msgTxt = "";
                                msgId = request.getParameter("msgId");
                                if (!msgId.equalsIgnoreCase("")) {
                                    if (msgId.equalsIgnoreCase("added")) {
                                        msgTxt = "BSR added successfully";
                                    }else if (msgId.equalsIgnoreCase("edited")) {
                                        msgTxt = "BSR edited successfully";
                                    }else if (msgId.equalsIgnoreCase("deleted")) {
                                        msgTxt = "BSR deleted successfully";                              
                                    }
                        %> 
                        <div class="responseMsg successMsg" ><%=msgTxt%></div>
                        <% }
                    }%>
                        <table width="100%" cellspacing="0" id="resultTable" class="tableList_3 t_space" cols="@0,1,2,3,4,5,6">
                            <tr>
                                <th width="4%" class="t-align-center">Sl. No.</th>
                                <th width="15%" class="t-align-center">Category </th>
                                <th width="15%" class="t-align-center">Sub Category</th>
                                <th width="15%" class="t-align-center">Code</th>
                                <th width="30%" class="t-align-center">Description</th>
                                <th width="15%" class="t-align-center">Unit</th>
                                <th width="6%" class="t-align-center">Action</th>
                            </tr>
                        </table>
                        <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                            <tr>
                                <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">20</span></td>
                                <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                    &nbsp;
                                    <label class="formBtn_1">
                                        <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                    </label></td>
                                <td align="right" class="prevNext-container"><ul>
                                        <li><font size="3">&laquo;</font> <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                        <li><font size="3">&#8249;</font> <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                        <li><a href="javascript:void(0)" id="btnNext">Next</a><font size="3"> &#8250;</font></li>
                                        <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                    </ul></td>
                            </tr>
                        </table>
                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" id="size" value="10"/>

                        <!--Dashboard Content Part End-->
                    </td>
                </tr>
            </table>

            <form id="formstyle" action="" method="post" name="formstyle">

                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                    String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="NewsEvent_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="NewsEvent" />
            </form>
            <!--Dashboard Footer Start-->
            <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
        </div>
        <script>
            var newsType = '<%=newsType%>';
            var obj = document.getElementById('lblBOQList');
            if (obj != null) {
                if (obj.innerHTML == 'View') {
                    obj.setAttribute('class', 'selected');
                }
            }

        </script>
        <script>
            var headSel_Obj = document.getElementById("headTabContent");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
