<%-- 
    Document   : ViewTenPaymentConfig
    Created on : Apr 19, 2011, 6:40:28 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.model.table.TblConfigDocFees"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenPaymentConfigService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Document Fees, Tender Security and Performance Security, Tender Validity business rule configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                sortTable();
            });
        </script>
        <script type="text/javascript">
            function confirmDelete(){
                var len=$('#tenCount').val();
                if(len==1){
                    jAlert("Atleast one configuration is required.","Service Configuration", function(RetVal) {
                    });
                    return false;
                }else{
                    if(window.confirm("Do you really want to delete the configuration Rule?")){
                        return true;
                    }
                    else{
                        return false;
                    }
                }
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <%
                        TenPaymentConfigService configService = (TenPaymentConfigService) AppContext.getSpringBean("TenPaymentConfigService");
                        configService.setLogUserId(session.getAttribute("userId").toString());
                        List<Object[]> procurementType = configService.getProcuremenTypes();
                        List<Object[]> tenderTypes = configService.getTenderTypes();
                        List<Object[]> procurementMethod = configService.getProcurementMethod();
                        List<TblConfigDocFees> getEvalMethods = configService.getAllTblConfigDocFees();
            %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea">
                        <div class="pageHead_1">Document Fees, Tender Security and Performance Security, Tender Validity business rule configuration
                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('7');">Save as PDF</a></span>
                        </div>
                        <%
                                            String message = "";
                                            if (!"".equalsIgnoreCase(request.getParameter("msg"))) {
                                                message = request.getParameter("msg");
                                                if ("updsucc".equalsIgnoreCase(message)) {
                                %>
                            <div class="responseMsg successMsg t_space"><span>Tender Payment business rule configured successfully</span></div><br/>
                            <%                                                } else if ("editsucc".equalsIgnoreCase(message)) {
                            %>
                            <div class="responseMsg successMsg t_space"><span>Tender Payment business rule updated successfully</span></div><br/>
                            <%                                    }
                                        }
                            %>
                        <table class="tableList_1 t_space" cellspacing="0" width="100%" id="resultTable" cols="@7">
                            <tbody id="tbodyData">&nbsp;                                
                            <tr>
                                <th class="t-align-center" width="15%">Tender Type<!--<br>(<span class="mandatory" style="color: red;">*</span>)</th>-->
                                <th class="t-align-center" width="15%">Procurement Method<!--<br>(<span class="mandatory" style="color: red;">*</span>)--></th>
                                <th class="t-align-center" width="15%">Procurement Type<!--<br>(<span class="mandatory" style="color: red;">*</span>)--></th>
                                <th class="t-align-center" width="10%">Requires Document Fees<!--<br>(<span class="mandatory" style="color: red;">*</span>)--></th>
                                <th class="t-align-center" width="10%">Requires Tender Validity<!--<br>(<span class="mandatory" style="color: red;">*</span>)--></th>
                                <th class="t-align-center" width="10%">Requires Tender Security <!--<br>(<span class="mandatory" style="color: red;">*</span>)--></th>
                                <th class="t-align-center">Requires Performance Security<!--<br>(<span class="mandatory" style="color: red;">*</span>)--></th>
                                <th class="t-align-center" width="15%">Action</th>
                            </tr>
                            <%
                                        int cnt = 0;
                                        do {
                            %>
                             <%if(cnt%2==0){%>
                             <tr>
          <%}else{%>
          <tr style='background-color:#E4FAD0;'>
          <%}%>                                
                                <td class="t-align-center">
                                    <% for (Object[] proc : tenderTypes) {
                                                                                    if (getEvalMethods.get(cnt).getTenderTypeId() == (Byte) proc[0]) {
                                                                                        out.print(proc[1]);
                                                                                    }%>
                                    <%}%>
                                </td>
                                <td class="t-align-center">
                                    <%for (Object[] proc : procurementMethod) {
                                                                            if (getEvalMethods.get(cnt).getProcurementMethodId() == (Byte) proc[0]) {
                                                                                if("RFQ".equalsIgnoreCase((String)proc[1]))
                                                                                {
                                                                                    out.print("LEM");
                                                                                }
                                                                                else if("DPM".equalsIgnoreCase((String)proc[1]))
                                                                                {
                                                                                    out.print("DCM");
                                                                                }
                                                                                else
                                                                                {
                                                                                    out.print(proc[1]);
                                                                                }
                                                                            }%>
                                    <%}%>
                                </td>
                                <td class="t-align-center">

                                    <%for (Object[] proc : procurementType) {
                                                                                    if (getEvalMethods.get(cnt).getProcurementTypeId() == (Byte) proc[0]) {
                                                                                        //ICT->ICB & NCT->NCB by Emtaz on 19/April/2016
                                                                                        if(proc[1].toString().equalsIgnoreCase("ICT"))
                                                                                        {out.print("ICB");}
                                                                                        else if(proc[1].toString().equalsIgnoreCase("NCT"))
                                                                                        {out.print("NCB");}
                                                                                    }%>
                                    <%}%>
                                </td>
                                <td class="t-align-center">
                                    <%out.print(getEvalMethods.get(cnt).getIsDocFeesReq());%>
                                </td>
                                <td class="t-align-center">
                                    <%out.print(getEvalMethods.get(cnt).getIsTenderValReq());%>
                                </td>
                                <td class="t-align-center">
                                    <%out.print(getEvalMethods.get(cnt).getIsTenderSecReq());%>
                                </td>                                                                
                                <td class="t-align-center">
                                    <%out.print(getEvalMethods.get(cnt).getIsPerSecFeesReq());%>
                                </td>
                                <td class="t-align-center">
                                    <a href="EditTenPaymentConfig.jsp?docFeesId=<%=getEvalMethods.get(cnt).getDocFeesId()%>">Edit</a> &nbsp;|&nbsp; <a href="<%=request.getContextPath()%>/TenPaymentConfigServlet?action=delSingleEval&docFeesId=<%=getEvalMethods.get(cnt).getDocFeesId()%>" onclick="return confirmDelete()" >Delete</a>
                                </td>
                            </tr>
                            <%cnt++;
                                        } while (cnt < getEvalMethods.size());%>
                            </tbody>
                            <input type="hidden" value="<%=cnt%>" id="tenCount">
                        </table>
                    </td>
                </tr>
            </table>
           <form id="formstyle" action="" method="post" name="formstyle">

               <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
               <%
                 SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                 String appenddate = dateFormat1.format(new Date());
               %>
               <input type="hidden" name="fileName" id="fileName" value="TenderPayRule_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="TenderPayRule" />
            </form>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script>
        var obj = document.getElementById('lblTenPayView');
        if(obj != null){
            if(obj.innerHTML == 'View'){
                obj.setAttribute('class', 'selected');
            }
        }

    </script>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabConfig");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
