<%-- 
    Document   : ConfigPreTenderMettingRuleDetails
    Created on : Nov 10, 2010, 8:10:29 PM
    Author     : Naresh.Akurathi
--%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%
            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            Object objUName = session.getAttribute("userName");
            boolean isLoggedIn = false;
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userId").toString();
            }
            if (objUName != null) {
                isLoggedIn = true;
            }
%>

<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigPreTender"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<%@page import="java.util.Date"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Pre-Bid Meeting Business Rules Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />        
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        
        <%
                    StringBuilder tenderType = new StringBuilder();
                    StringBuilder procNature = new StringBuilder();
                    StringBuilder procType = new StringBuilder();
                    StringBuilder procMethod = new StringBuilder();
                    configPreTenderRuleSrBean.setLogUserId(strUserTypeId);

                    TblTenderTypes tenderTypes2 = new TblTenderTypes();
                    Iterator trit2 = configPreTenderRuleSrBean.getTenderNames().iterator();
                    while (trit2.hasNext()) {
                        tenderTypes2 = (TblTenderTypes) trit2.next();
                        if(!tenderTypes2.getTenderType().equals("PQ") && !tenderTypes2.getTenderType().equals("RFA") && !tenderTypes2.getTenderType().equals("2 Stage-PQ"))
                        {
                            tenderType.append("<option value='" + tenderTypes2.getTenderTypeId() + "'>" + tenderTypes2.getTenderType() + "</option>");
                        }

                    }

                    TblProcurementNature tblProcureNature2 = new TblProcurementNature();
                    Iterator pnit2 = configPreTenderRuleSrBean.getProcurementNature().iterator();
                    while (pnit2.hasNext()) {
                        tblProcureNature2 = (TblProcurementNature) pnit2.next();
                        procNature.append("<option value='" + tblProcureNature2.getProcurementNatureId() + "'>" + tblProcureNature2.getProcurementNature() + "</option>");
                    }

                    TblProcurementMethod tblProcurementMethod2 = new TblProcurementMethod();
                    Iterator pmit2 = configPreTenderRuleSrBean.getProcurementMethod().iterator();
                    while (pmit2.hasNext()) {
                        tblProcurementMethod2 = (TblProcurementMethod) pmit2.next();
                        if("OTM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                            "LTM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                            "DP".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                           // "LEQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                          //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                            "RFQ".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                           // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                          //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                            "FC".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                           // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                            "DPM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())||
                             "QCBS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "LCS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "SFB".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "SBCQ".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "SSS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "IC".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()))
                            {
                                if(tblProcurementMethod2.getProcurementMethod().equals("RFQ"))
                                {
                                    procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>LEM</option>");

                                }
                                else if(tblProcurementMethod2.getProcurementMethod().equals("DPM"))
                                {
                                    procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>DCM</option>");

                                }
                                else
                                {
                                    procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>" + tblProcurementMethod2.getProcurementMethod() + "</option>");

                                }
                            }
                    }

                    TblProcurementTypes tblProcurementTypes2 = new TblProcurementTypes();
                    Iterator ptit2 = configPreTenderRuleSrBean.getProcurementTypes().iterator();
                    while (ptit2.hasNext()) {
                        tblProcurementTypes2 = (TblProcurementTypes) ptit2.next();
                        if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("NCT"))
                        {
                            procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>NCB</option>");
                        }
                        else if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("ICT"))
                        {
                            procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>ICB</option>");
                        }
                        else
                        {
                            procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + tblProcurementTypes2.getProcurementType() + "</option>");
                        }
                    }

        %>        

    </head>
    <body>
        <%!        ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();


        %>

        <%
                    String msg = "";
                    if ("Submit".equals(request.getParameter("button")) || "Update".equals(request.getParameter("button"))) {
                        int row = Integer.parseInt(request.getParameter("TotRule"));

                        if ("edit".equals(request.getParameter("action"))) {
                            msg = "Not Deleted";
                        } else {
                            msg = configPreTenderRuleSrBean.deleteAllConfigPreTenderRule();
                        }

                        if (msg.equals("Deleted")) {
                            //Add/Edit/Remove Pre Tender Meeting Rules
                            String action = "Add Pre Tender Meeting Rules";
                            try{
                                for (int i = 1; i <= row; i++) {
                                    if (request.getParameter("tenderType" + i) != null) {
                                        byte ttype = Byte.parseByte(request.getParameter("tenderType" + i));
                                        byte pnature = Byte.parseByte("1");
                                        byte pmethod = Byte.parseByte(request.getParameter("procurementMethod" + i));
                                        byte ptypes = Byte.parseByte(request.getParameter("procurementType" + i));
                                        TblConfigPreTender master = new TblConfigPreTender();

                                        master.setTblTenderTypes(new TblTenderTypes(ttype));
                                        master.setTblProcurementNature(new TblProcurementNature(pnature));
                                        master.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                        master.setTblProcurementTypes(new TblProcurementTypes(ptypes));

                                        master.setIsPreTenderMeetingfReq("Yes");
                                        master.setPubBidDays(Short.parseShort(request.getParameter("daysforPublication" + i)));
                                        master.setPostQueDays(Short.valueOf("0"));
                                        master.setUploadedTenderDays(Short.parseShort(request.getParameter("daysforAllowed" + i)));

                                        msg = configPreTenderRuleSrBean.addConfigPreTenderRule(master);

                                    }
                                }
                           }catch(Exception e){
                             System.out.println(e);
                            action = "Error in : "+action +" : "+ e;
                           }finally{
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                                action=null;
                            }
                        } else {
                            String action = "Edit Pre Tender Meeting Rules";
                            try{
                                for (int i = 1; i <= row; i++) {
                                    if (request.getParameter("tenderType" + i) != null) {
                                        byte ttype = Byte.parseByte(request.getParameter("tenderType" + i));
                                        byte pnature = Byte.parseByte("1");
                                        byte pmethod = Byte.parseByte(request.getParameter("procurementMethod" + i));
                                        byte ptypes = Byte.parseByte(request.getParameter("procurementType" + i));
                                        TblConfigPreTender master = new TblConfigPreTender();
                                        master.setPreTenderMeetId(Integer.parseInt(request.getParameter("id")));
                                        master.setTblTenderTypes(new TblTenderTypes(ttype));
                                        master.setTblProcurementNature(new TblProcurementNature(pnature));
                                        master.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                        master.setTblProcurementTypes(new TblProcurementTypes(ptypes));
                                        master.setIsPreTenderMeetingfReq("Yes");
                                        master.setPubBidDays(Short.parseShort(request.getParameter("daysforPublication" + i)));
                                        //master.setPostQueDays(Short.parseShort(request.getParameter("postingofQuestions" + i)));
                                        master.setPostQueDays(Short.valueOf("0"));
                                        master.setUploadedTenderDays(Short.parseShort(request.getParameter("daysforAllowed" + i)));

                                        msg = configPreTenderRuleSrBean.updateConfigPreTenderRule(master);
                                    }
                                  }
                            }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                            }finally{
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                                action=null;
                            }
                            }
                        if (msg.equals("Values Added")) {
                            msg = "Pre-Bid Meeting Business Rule Configured successfully";
                            response.sendRedirect("ConfigPreTenderMettingRuleView.jsp?msg=" + msg);
                        }
                        if (msg.equals("Updated")) {
                            msg = "Pre-Bid Meeting Business Rule Updated successfully";
                            response.sendRedirect("ConfigPreTenderMettingRuleView.jsp?msg=" + msg);
                        }
                    }

        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%--<jsp:include page="../resources/common/AfterLoginTop.jsp" ></jsp:include>--%>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>

            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">

                        <div class="pageHead_1">Pre-Bid Meeting Business Rules Configuration</div>

                        <div style="font-style: italic;" class="t-align-right"><normal>Fields marked with (<span class="mandatory">*</span>) are mandatory</normal></div>

                        <%
                                    if (request.getParameter("id") == null) {
                        %>

                        <div align="right" class="t_space">
                            <span id="lotMsg" style="color: red; font-weight: bold; float: left; visibility: collapse">&nbsp;</span>
                            <a id="linkAddRule"class="action-button-add">Add Rule</a> <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a>

                        </div>
                        <%}%>
                       <div>&nbsp;</div>

                            <form action="ConfigPreTenderMettingRuleDetails.jsp" method="post">
                                <%--<div class="pageHead_1" align="center"> <%=msg%></div>--%>
                                <table width="100%" cellspacing="0" class="tableList_1" id="tblrule">
                                    <tr  >
                                    <th style="width:12%;">Select</th>
                                    <th style="width:16%;">Tender Type<br />(<span class="mandatory">*</span>)</th>
                                    <!--<th >Procurement Nature<br />(<span class="mandatory">*</span>)</th>-->
                                    <th style="width:16%;">Procurement Method<br />(<span class="mandatory">*</span>)</th>
                                    <th style="width:16%;">Procurement Type<br />(<span class="mandatory">*</span>)</th>
                                    <!--<th >Pre Tender Meeting Required<br />(<span class="mandatory">*</span>)</th>-->
                                    <th style="width:25%;">Specify as how many days, from date of Tender Publication, Pre-Bid Meeting should start
                                        <br />(<span class="mandatory">*</span>)</th>
                                    <!--<th >Posting of questions allowed before no. of days from  tender closing date<br />(<span class="mandatory">*</span>)</th>-->
                                    <th style="width:25%;">Days allowed for uploading Pre-Bid MOM, after Pre-Bid meeting is over<br />(<span class="mandatory">*</span>)</th>
                                    </tr>


                                    <%
                                                List l = null;
                                            if (request.getParameter("id") != null && "edit".equals(request.getParameter("action"))) {
                                                String action = request.getParameter("action");
                                                int id = Integer.parseInt(request.getParameter("id"));
                                                %>
                                                <input type="hidden" id="hiddenaction" name="action" value="<%=action%>"/>
                                                <input type="hidden" id="hiddenid" name="id" value="<%=id%>"/>
                                                <%

                                                l = configPreTenderRuleSrBean.getConfigPreTenderRule(id);
                                            } else
                                                    l = configPreTenderRuleSrBean.getConfigPreTenderRuleDetails();

                                                int i = 0;
                                                if (!l.isEmpty() || l != null) {
                                                    Iterator it = l.iterator();
                                                    TblConfigPreTender tblcon = new TblConfigPreTender();
                                                    while (it.hasNext()) {
                                                        tblcon = (TblConfigPreTender) it.next();
                                                        i++;
                                                        tblcon.getPreTenderMeetId();
                                                        StringBuilder tType = new StringBuilder();
                                                        StringBuilder pNature = new StringBuilder();
                                                        StringBuilder pType = new StringBuilder();
                                                        StringBuilder pMethod = new StringBuilder();
                                                        StringBuilder tMeet = new StringBuilder();
                                                        String pQues = new String();
                                                        String bDays = new String();
                                                        String uTender = new String();

                                                        List getProcNature = configPreTenderRuleSrBean.getProcurementNature();
                                                        List getProcMethod = configPreTenderRuleSrBean.getProcurementMethod();
                                                        List getProcType = configPreTenderRuleSrBean.getProcurementTypes();

                                                        TblTenderTypes tenderTypes = new TblTenderTypes();
                                                        Iterator trit = configPreTenderRuleSrBean.getTenderNames().iterator();
                                                        while (trit.hasNext()) {
                                                            tenderTypes = (TblTenderTypes) trit.next();
                                                            if(!tenderTypes.getTenderType().equals("PQ") && !tenderTypes.getTenderType().equals("RFA") && !tenderTypes.getTenderType().equals("2 Stage-PQ"))
                                                            {
                                                                tType.append("<option value='");
                                                                if (tenderTypes.getTenderTypeId() == tblcon.getTblTenderTypes().getTenderTypeId()) {
                                                                    tType.append(tenderTypes.getTenderTypeId() + "' selected='true'>" + tenderTypes.getTenderType());
                                                                } else {
                                                                    tType.append(tenderTypes.getTenderTypeId() + "'>" + tenderTypes.getTenderType());
                                                                }
                                                                tType.append("</option>");
                                                            }
                                                        }

                                                        TblProcurementNature tblProcureNature = new TblProcurementNature();
                                                        Iterator pnit = getProcNature.iterator();
                                                        while (pnit.hasNext()) {
                                                            tblProcureNature = (TblProcurementNature) pnit.next();
                                                            pNature.append("<option value='");
                                                            if (tblProcureNature.getProcurementNatureId() == tblcon.getTblProcurementNature().getProcurementNatureId()) {
                                                                pNature.append(tblProcureNature.getProcurementNatureId() + "' selected='true'>" + tblProcureNature.getProcurementNature());
                                                            } else {
                                                                pNature.append(tblProcureNature.getProcurementNatureId() + "'>" + tblProcureNature.getProcurementNature());
                                                            }
                                                            pNature.append("</option>");
                                                        }

                                                        TblProcurementMethod tblProcurementMethod = new TblProcurementMethod();
                                                        Iterator pmit = getProcMethod.iterator();
                                                        while (pmit.hasNext()) {
                                                            tblProcurementMethod = (TblProcurementMethod) pmit.next();
                                                            if("OTM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                "LTM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                "DP".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                               // "LEQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                              //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                                "RFQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                               // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                              //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                                "FC".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                               // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                                                                "DPM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())||
                                                                 "QCBS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                 "LCS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                 "SFB".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                 "SBCQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                 "SSS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                                 "IC".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()))
                                                             {
                                                            pMethod.append("<option value='");
                                                            if (tblProcurementMethod.getProcurementMethodId() == tblcon.getTblProcurementMethod().getProcurementMethodId()) {
                                                                if(tblProcurementMethod.getProcurementMethod().equals("RFQ"))
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>LEM");

                                                                }
                                                                else if(tblProcurementMethod.getProcurementMethod().equals("DPM"))
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>DCM");

                                                                }
                                                                else
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>" + tblProcurementMethod.getProcurementMethod());

                                                                }
                                                            } else {
                                                                if(tblProcurementMethod.getProcurementMethod().equals("RFQ"))
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>LEM");

                                                                }
                                                                else if(tblProcurementMethod.getProcurementMethod().equals("DPM"))
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>DCM");

                                                                }
                                                                else
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>" + tblProcurementMethod.getProcurementMethod());

                                                                }
                                                               
                                                            }
                                                            pMethod.append("</option>");
                                                        }
                                                    }

                                                        TblProcurementTypes tblProcurementTypes = new TblProcurementTypes();
                                                        Iterator ptit = getProcType.iterator();
                                                        String procureType="";
                                                        while (ptit.hasNext()) {
                                                            tblProcurementTypes = (TblProcurementTypes) ptit.next();
                                                            if(tblProcurementTypes.getProcurementType().equalsIgnoreCase("NCT"))
                                                                procureType="NCB";
                                                            else if(tblProcurementTypes.getProcurementType().equalsIgnoreCase("ICT"))
                                                                procureType="ICB";
                                                            pType.append("<option value='");
                                                            if (tblProcurementTypes.getProcurementTypeId() == tblcon.getTblProcurementTypes().getProcurementTypeId()) {
                                                                pType.append(tblProcurementTypes.getProcurementTypeId() + "' selected='true'>" + procureType);
                                                            } else {
                                                                pType.append(tblProcurementTypes.getProcurementTypeId() + "'>" + procureType);
                                                            }
                                                            pType.append("</option>");
                                                        }

                                                    /*       if ("Yes".equals(tblcon.getIsPreTenderMeetingfReq())) {
                                                            tMeet.append("<option value='Yes' selected='true'>Yes</option><option>No</option>");
                                                        } else {
                                                            tMeet.append("<option>Yes</option><option value='No' selected='true'>No</option>");
                                                        }
                                                     */
                                                    tMeet.append("<option value='Yes' selected='true'>Yes</option><option>No</option>");

                                    %>
                                    <input type="hidden" name="delrow" value="0" id="delrow"/>
                                    <input type="hidden" name="introw" value="" id="introw"/>
                                    <tr id="trrule_<%=i%>">
                                        <td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_<%=i%>" value="" /></td>

                                        <td class="t-align-center"><select name="tenderType<%=i%>" class="formTxtBox_1" id="tenderType_<%=i%>">
                                                <%=tType%></select>
                                        </td>
                                    <!--<td class="t-align-center"><select name="procurementNature<%=i%>" class="formTxtBox_1" id="procurementNature_<%=i%>">
                                                <%=pNature%></select>
                            </td>-->
                                        <td class="t-align-center"><select name="procurementMethod<%=i%>" class="formTxtBox_1" id="procurementMethod_<%=i%>">
                                                <%=pMethod%></select>
                                        </td>
                                        <td class="t-align-center"><select name="procurementType<%=i%>" class="formTxtBox_1" id="procurementType_<%=i%>">
                                                <%=pType%></select>
                                        </td>
                                    <!--<td class="t-align-center"><select name="preTenderMettingRequired<%=i%>" class="formTxtBox_1" id="preTenderMettingRequired_<%=i%>">
                                                <%=tMeet%></select>
                            </td>-->
                                        <td class="t-align-center"><input name="daysforPublication<%=i%>" type="text" class="formTxtBox_1" id="daysforPublication_<%=i%>" value="<%=tblcon.getPubBidDays()%>" onBlur="return chkDaysFromPub(this);" /><span id="DaysforPublication_<%=i%>" style="color:red;"></span></td>
                                    <!--<td class="t-align-center"><input name="postingofQuestions<%=i%>" type="text" class="formTxtBox_1" id="postingofQuestions_<%=i%>" value="<%=tblcon.getPostQueDays()%>" onBlur="return chkpostingofQue(this);" /><span id="PostingofQuestions_<%=i%>" style="color:red;"></span></td>-->
                                        <td class="t-align-center"><input name="daysforAllowed<%=i%>" type="text" class="formTxtBox_1" id="daysforAllowed_<%=i%>" value="<%=tblcon.getUploadedTenderDays()%>" onBlur="return chkdaysforAllowed(this);" /><span id="DaysforAllowed_<%=i%>" style="color:red;"></span></td>

                                    </tr>
                                    <%

                                                    }
                                                } else {
                                                    msg = "No Record Found";
                                                }
                                    %>

                                    <tbody id="trbody">

                                    </tbody>
                                </table>
                                <input type="hidden" name="TotRule" id="TotRule" value="<%=i%>"/>
                                <div>&nbsp;</div>
                                <table width="100%" cellspacing="0" >
                                    <tr>
                                    <%if ("edit".equals(request.getParameter("action"))) {%>
                                         <td align="right" colspan="9" class="t-align-center"><span class="formBtn_1">
                                                <input type="submit" name="button" id="button" value="Update" onclick="return check();"/>
                                            </span></td>
                                        <%} else {%>
                                        <td align="right" colspan="9" class="t-align-center"><span class="formBtn_1">
                                                <input type="submit" name="button" id="button" value="Submit" onclick="return check();"/>
                                            </span></td>
                                        <%}%>
                                    </tr>
                                </table>
                                <%--<div class="pageHead_1" align="center"> <%=msg%></div>--%>
                            </form>


                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>
             <script>
                var obj = document.getElementById('lblConfigPreTenderMettingRuleEdit');
                if(obj != null){
                    if(obj.innerHTML == 'Edit'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            <script type="text/javascript">


            function check()
            {
                var count=document.getElementById("TotRule").value;
                var flag=true;
                if(document.getElementById("hiddenaction"))
                {
                    if("edit"==document.getElementById("hiddenaction").value)
                    {
                    $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/BusinessRuleConfigurationServlet",
                        data:"preTenderMeetId="+$('#hiddenid').val()+"&"+"tenderType="+$('#tenderType_1').val()+"&"+"procMethod="+$('#procurementMethod_1').val()+"&"+"procType="+$('#procurementType_1').val()+"&"+"funName=PreTenderMettingRule",
                        async: false,
                        success: function(j){

                            if(j.toString()!="0"){
                                flag=false;
                                jAlert("Combination of 'Tender Type','Procurement Method' and 'Procurement Type', already exist.","Pre-Bid meeting business rule configuration", function(RetVal) {
                                });
                            }
                        }
                    });
                    }
                }
                for(var k=1;k<=count;k++)
                {
                    if(document.getElementById("daysforPublication_"+k)!=null)
                    {
                        if(document.getElementById("daysforPublication_"+k).value=="")
                        {
                            document.getElementById("DaysforPublication_"+k).innerHTML="<br/>Please specify as how many days, from date of Tender Publication, Pre-Bid Meeting should start.";
                            flag=false;
                        }
                        else
                        {
                            if(digits(document.getElementById("daysforPublication_"+k).value))
                            {
                                if(!chkMaxLength(document.getElementById("daysforPublication_"+k).value))
                                {
                                    document.getElementById("DaysforPublication_"+k).innerHTML="</br>Maximum 3 numbers  without decimal are allowed.";
                                    flag=false;
                                }

                                else
                                {
                                    document.getElementById("DaysforPublication_"+k).innerHTML="";
                                }

                            }
                            else
                            {
                                document.getElementById("DaysforPublication_"+k).innerHTML="</br>Please enter digits only.";
                                flag=false;
                            }

                        }
                    }

                    /*       if(document.getElementById("postingofQuestions_"+k)!=null)
                    {
                        if(document.getElementById("postingofQuestions_"+k).value=="")
                        {
                            document.getElementById("PostingofQuestions_"+k).innerHTML="<br/>Please enter posting of questions allowed before no. of days from tender closing date.";
                            flag=false;
                        }
                        else
                        {
                            if(digits(document.getElementById("postingofQuestions_"+k).value))
                            {
                                if(!chkMaxLength(document.getElementById("postingofQuestions_"+k).value))
                                {
                                    document.getElementById("PostingofQuestions_"+k).innerHTML="</br>Maximum 3 numbers  without decimal are allowed.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("PostingofQuestions_"+k).innerHTML="";
                                }
                            }
                            else
                            {
                                document.getElementById("PostingofQuestions_"+k).innerHTML="</br>Please enter digits only.";
                                flag=false;
                            }
                        }
                    }
                     */

                    if(document.getElementById("daysforAllowed_"+k)!=null)
                    {
                        if(document.getElementById("daysforAllowed_"+k).value=="")
                        {
                            document.getElementById("DaysforAllowed_"+k).innerHTML="<br/>Please Specify no. of days allowed for uploading MoM of Pre-Bid Meeting, after Pre-Bid Meeting is over.";
                            flag=false;
                        }
                        else
                        {
                            if(digits(document.getElementById("daysforAllowed_"+k).value))
                            {
                                if(!chkMaxLength(document.getElementById("daysforAllowed_"+k).value))
                                {
                                    document.getElementById("DaysforAllowed_"+k).innerHTML="</br>Maximum 3 numbers  without decimal are allowed.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("DaysforAllowed_"+k).innerHTML="";
                                }
                            }
                            else
                            {
                                document.getElementById("DaysforAllowed_"+k).innerHTML="</br>Please enter digits only.";
                                flag=false;
                            }

                        }
                    }
                }

                // Start OF--Checking for Unique Rows

                if(document.getElementById("TotRule")!=null){
                    var totalcount = eval(document.getElementById("TotRule").value); //Total Count After Adding Rows
                }
                var chk=true;
                var i=0;
                var j=0;
                for(i=1; i<=totalcount; i++) //Loop For Newly Added Rows
                {
                    if(document.getElementById("daysforPublication_"+i)!=null)
                        if($.trim(document.getElementById("daysforPublication_"+i).value)!='' && $.trim(document.getElementById("daysforAllowed_"+i).value)!='') // If Condition for When all Data are filled thiscode is Running
                    {
                        for(j=1; j<=totalcount && j!=i; j++) // Loop for Total Count but Not same as (i) value
                        {
                            chk=true;
                            //If Condition for Check Duplicate Rows are there or not.If Columns are diff then chk variable set to false
                            //IF Row is same Give alert message.

                            if($.trim(document.getElementById("tenderType_"+i).value) != $.trim(document.getElementById("tenderType_"+j).value))
                            {
                                chk=false;
                            }
                            /*  if($.trim(document.getElementById("procurementNature_"+i).value) != $.trim(document.getElementById("procurementNature_"+j).value))
                            {
                                chk=false;
                            }
                             */
                            if($.trim(document.getElementById("procurementMethod_"+i).value) != $.trim(document.getElementById("procurementMethod_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("procurementType_"+i).value) != $.trim(document.getElementById("procurementType_"+j).value))
                            {
                                chk=false;
                            }
                            /*   if($.trim(document.getElementById("preTenderMettingRequired_"+i).value) != $.trim(document.getElementById("preTenderMettingRequired_"+j).value))
                            {
                                chk=false;
                            }

                            if($.trim(document.getElementById("daysforPublication_"+i).value) != $.trim(document.getElementById("daysforPublication_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("postingofQuestions_"+i).value) != $.trim(document.getElementById("postingofQuestions_"+j).value))
                            {
                                chk=false;
                            }

                            if($.trim(document.getElementById("daysforAllowed_"+i).value) != $.trim(document.getElementById("daysforAllowed_"+j).value))
                            {
                                chk=false;
                            }
                             */
                            if(flag){
                            if(chk==true) //If Row is same then give alert message
                            {
                                    jAlert("Duplicate record found. Please enter unique record","Pre-Bid Meeting Business Rules Configuration",function(RetVal) {
                                    });
                                return false;
                            }
                        }
                    }
                }
                }
                // End OF--Checking for Unique Rows
                if (flag==false)
                {
                    return false;
                }
            };

            function chkMaxLength(value)
            {
                var ValueChk=value;
                //alert(ValueChk);

                if(ValueChk.length>3)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            function chkDaysFromPub(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk1=true;

                if(obj.value!='')
                {
                    if(digits(obj.value))
                    {
                        if(!chkMaxLength(obj.value))
                        {
                            document.getElementById("DaysforPublication_"+i).innerHTML="</br>Maximum 3 numbers  without decimal are allowed.";
                            chk1=false;
                        }
                        else
                        {
                            document.getElementById("DaysforPublication_"+i).innerHTML="";
                        }
                        //document.getElementById("DaysforPublication_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("DaysforPublication_"+i).innerHTML="</br>Please enter digits only.";
                        chk1=false;
                    }
                }
                else
                {
                    document.getElementById("DaysforPublication_"+i).innerHTML="</br>Please specify as how many days, from date of Tender Publication, Pre-Bid Meeting should start.";
                    chk1=false;
                }
                if(chk1==false)
                {
                    return false;
                }
            }

            /*   function chkpostingofQue(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk2=true;

                if(obj.value!='')
                {
                    if(digits(obj.value))
                    {
                        if(!chkMaxLength(obj.value))
                        {
                            document.getElementById("PostingofQuestions_"+i).innerHTML="</br>Maximum 3 numbers  without decimal are allowed.";
                            chk2=false;
                        }
                        else
                        {
                            document.getElementById("PostingofQuestions_"+i).innerHTML="";
                        }
                        //document.getElementById("PostingofQuestions_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("PostingofQuestions_"+i).innerHTML="</br>Please enter digits only.";
                        chk2=false;
                    }
                }
                else
                {
                    document.getElementById("PostingofQuestions_"+i).innerHTML="</br>Please enter posting of questions allowed before no. of days from tender closing date.";
                    chk2=false;
                }
                if(chk2==false)
                {
                    return false;
                }
            }*/
            function chkdaysforAllowed(obj)
            {  var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk3=true;

                if(obj.value!='')
                {
                    if(digits(obj.value))
                    {
                        if(!chkMaxLength(obj.value))
                        {
                            document.getElementById("DaysforAllowed_"+i).innerHTML="</br>Maximum 3 numbers  without decimal are allowed.";
                            chk3=false;
                        }
                        else
                        {
                            document.getElementById("DaysforAllowed_"+i).innerHTML="";
                        }
                        //document.getElementById("DaysforAllowed_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("DaysforAllowed_"+i).innerHTML="</br>Please enter digits only.";
                        chk3=false;
                    }
                }
                else
                {
                    document.getElementById("DaysforAllowed_"+i).innerHTML="</br>Please Specify no. of days allowed for uploading MoM of Pre-Bid Meeting, after Pre-Bid Meeting is over.";
                    chk3=false;
                }
                if(chk3==false)
                {
                    return false;
                }
            }

            function digits(value)
            {
                return /^\d+$/.test(value);
            }
        </script>
        <script type="text/javascript">

            var counter = 1;
            var delCnt = 0;
            $(function() {
                $('#linkAddRule').click(function() {
                    counter = eval(document.getElementById("TotRule").value);
                    if(document.getElementById("delrow")!=null){
                        delCnt = eval(document.getElementById("delrow").value);
                    }
                    $('span.#lotMsg').css("visibility","collapse");
                    var htmlEle = "<tr id='trrule_"+ (counter+1) +"'>"+
                        "<td class='t-align-center'><input type='checkbox' name='checkbox"+(counter+1)+"' id='checkbox_"+(counter+1)+"' value='"+(counter+1)+"' /></td>"+
                        "<td class='t-align-center'><select name='tenderType"+(counter+1)+"' class='formTxtBox_1' id='tenderType_"+(counter+1)+"'><%=tenderType%></select></td>"+
                        //"<td class='t-align-center'><select name='procurementNature"+(counter+1)+"' class='formTxtBox_1' id='procurementNature_"+(counter+1)+"'><%=procNature%></select></td>"+
                        "<td class='t-align-center'><select name='procurementMethod"+(counter+1)+"' class='formTxtBox_1' id='procurementMethod_"+(counter+1)+"'><%=procMethod%></select></td>"+
                        "<td class='t-align-center'><select name='procurementType"+(counter+1)+"' class='formTxtBox_1' id='procurementType_"+(counter+1)+"'><%=procType%></select></td>"+
                        //"<!--<td  class='t-align-center'><select name='preTenderMettingRequired"+(counter+1)+"' class='formTxtBox_1' id='preTenderMettingRequired_"+(counter+1)+"'><option value='Yes' selected='true'>Yes</option><option>No</option></td>-->"+
                        "<td class='t-align-center'><input name='daysforPublication"+(counter+1)+"' type='text' class='formTxtBox_1' id='daysforPublication_"+(counter+1)+"' onBlur='return chkDaysFromPub(this);'/><span id='DaysforPublication_"+ (counter+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        //"<td class='t-align-center'><input name='postingofQuestions"+(counter+1)+"' type='text' class='formTxtBox_1' id='postingofQuestions_"+(counter+1)+"' onBlur='return chkpostingofQue(this);'/><span id='PostingofQuestions_"+ (counter+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><input name='daysforAllowed"+(counter+1)+"' type='text' class='formTxtBox_1' id='daysforAllowed_"+(counter+1)+"' onBlur='return chkdaysforAllowed(this);'/><span id='DaysforAllowed_"+ (counter+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "</tr>";
                    $("#tblrule").append(htmlEle);
                    document.getElementById("TotRule").value = (counter+1);

                });
            });

            $(function() {
                $('#linkDelRule').click(function() {
                    counter = eval(document.getElementById("TotRule").value);
                    delCnt = eval(document.getElementById("delrow").value);
                    var tmpCnt = 0;
                    for(var i=1;i<=counter;i++){
                        if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                            tmpCnt++;
                        }
                    }

                    if(tmpCnt==(eval(counter-delCnt))){
                        $('span.#lotMsg').css("visibility","visible");
                        $('span.#lotMsg').css("color","red");
                        $('span.#lotMsg').html('Minimum 1 record is needed!');
                    }else{
                        if(tmpCnt>0){jConfirm('Are You Sure You want to Delete.','Procurement Method Business Rule Configuration', function(ans) {if(ans){
                        for(var i=1;i<=counter;i++){
                            if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                                $("tr[id='trrule_"+i+"']").remove();
                                $('span.#lotMsg').css("visibility","collapse");
                                delCnt++;
                            }
                        }
                        document.getElementById("delrow").value = delCnt;
                        }});}else{jAlert("please Select checkbox first","Procurement Method Business Rule Configuration", function(RetVal) {});}
                    }
                });
            });
        </script>

    </body>
</html>
