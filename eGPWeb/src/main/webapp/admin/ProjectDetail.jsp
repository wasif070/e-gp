<%-- 
    Document   : ProjectDetail
    Created on : Nov 1, 2010, 9:26:55 PM
    Author     : parag
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="projGridSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectGridSrBean" />
<jsp:useBean id="projSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectSrBean" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page  import="java.util.List,com.cptu.egp.eps.dao.storedprocedure.CommonAppData" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Projects Information</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
    </head>
    <body>
        <%
            String userID="0";
            if(session.getAttribute("userId")!=null){
            userID=session.getAttribute("userId").toString();
            }
            projGridSrUser.setLogUserId(userID);
            projSrUser.setLogUserId(userID);
        %>
        <%
                    boolean flag = false;
                    String action = "";
                    String msg = "";

                    if (request.getParameter("flag") != null) {
                        if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
                            flag = true;
                        }
                    }
                    if (request.getParameter("action") != null) {
                        action = request.getParameter("action");
                    }

                    
                    if (flag) {
                        msg = "Project approved successfully";
                    }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <%
                     int usertypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
                            StringBuilder userType = new StringBuilder();
                            String type = "";
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                            if(usertypeId==4){
                             type = "pending";
                            }else{
                                type="approved";
                                }
                            String title = "Project Detail";
                            String str_QueryAsc = "Order by projectId desc";
                            List<CommonAppData> projData = projGridSrUser.getProjectData("pending",str_QueryAsc,session.getAttribute("userId").toString()+"");
                            
                            String link = "GovtUserGridSrBean?action=viewGovData&govUserType";
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <%--<%@include file="/resources/common/AfterLoginLeft.jsp" %>--%>

                        <td class="contentArea">
                            <div class="pageHead_1">Projects Information
                            <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('6');">Save as PDF</a></span>
                            </div>
                            <table width="100%" border="0" class="t_space" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td>
                                        <div >
                                            <%if (flag) {%>
                                            <div id="successMsg" class="responseMsg successMsg" style="display:block"><%=msg%></div>
                                            <%} else {%>
                                            <div id="errMsg" class="responseMsg errorMsg" style="display:none"><%=msg%></div>
                                            <%}%>
                                        </div>
                                        <!--Dashboard Header End-->
                                        <!--Dashboard Content Part Start-->
                                        
                                        <%
                                            
                                             if(usertypeId==4){
                                        %>
                                        <div class="t-align-right t_space">

                                            <a href="CreateEditProject.jsp" class="action-button-add">Add New Project</a>

                                        </div>
                                        <ul class="tabPanel_1">
                                            <li><a href="javascript:void(0);" id="hrefPending" onclick="checkSatus('pending');" class="sMenu" >Pending</a></li>
                                            <li><a href="javascript:void(0);" id="hrefApprove" onclick="checkSatus('approve');"  >Approved</a></li>
                                        </ul>
                                        <%}else{%>
                                      
                                        <%}%>
                                        <div class="tabPanelArea_1 t_space" >
                                            <table width="100%" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <%--<div >
                                                            <%if (!" ".equalsIgnoreCase(msg) || flag) {%>
                                                            <div id="successMsg" class="responseMsg successMsg" style="display:none"><%=msg%></div>
                                                            <%} else if (!" ".equalsIgnoreCase(msg)) {%>
                                                            <div id="errMsg" class="responseMsg errorMsg" style="display:none"><%=msg%></div>
                                                            <%}%>
                                                        </div>--%>

                                                        <div align="center" id="jqGrid">
                                                        </div>
                                                        <%--<div align="center" id="divApprove" style="display:none">
                                                            <table id="lista"></table>
                                                            <div id="page">
                                                            </div>
                                                        </div>--%>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>




                                    </td>

                                </tr>
                            </table>
                            <!--For Generate PDF  Starts-->
                            <form id="formstyle" action="" method="post" name="formstyle">
                                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                                <%
                                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                                    String appenddate = dateFormat1.format(new Date());
                                %>
                                <input type="hidden" name="fileName" id="fileName" value="Projects_<%=appenddate%>" />
                                <input type="hidden" name="id" id="id" value="Projects" />
                            </form>
                            <!--For Generate PDF  Ends-->
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End--><%--<%@include file="/resources/common/Bottom.jsp" %>--%>
            </div>
        </div>
            <script>
                var obj = document.getElementById('lblProjInfoView');
                if(obj != null){
                    if(obj.innerHTML == 'View Projects'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabContent");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>

<script type="text/javascript">
    //{name:'emailId',index:'emailId', width:100},
    //{name:'<%=type%>',index:'<%=type%>', width:100},
    //{name:'view',index:'view', width:100}

    function forPending(){
        $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
        jQuery("#list").jqGrid({
            url: "<%=request.getContextPath()%>/ProjectGridSrBean?action=viewProjectData&status=<%=type%>",
            datatype: "xml",
            height: 250,
            colNames:['Sl. No.','Project Code','Project Name','Project Cost(in Nu.)','Start Date','End Date','Action'],
            colModel:[
                {name:'srno',index:'srno', width:5, search: false,sortable:false,align:'center'},
                {name:'projectCode',index:'projectCode', width:25,sortable:true,searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'projectName',index:'projectName', width:30,sortable:true,searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'projectCost',index:'projectCost', width:25,sortable:true,searchoptions: { sopt: ['eq'] }},
                {name:'sdate',index:'projectStartDate', width:20,sortable:true,align:'center',searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'edate',index:'projectEndDate', width:20,sortable:true,align:'center',searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'action',index:'action', width:30,sortable:false,search: false, align:'center'},
            ],
            autowidth: true,
            multiselect: false,
            paging: true,
            rowNum:10,
            rowList:[10,20,30],
            pager: $("#page"),
            caption: "<%=title%> ",
            gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
        }).navGrid('#page',{edit:false,add:false,del:false});
    }

    function forApprove(){
        $("#jqGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
        jQuery("#list").jqGrid({
            url: "<%=request.getContextPath()%>/ProjectGridSrBean?action=viewProjectData&status=approve",
            datatype: "xml",
            height: 250,
            colNames:['Sl. No.','Project Code','Project Name','Cost (in Nu.)','Start Date','End Date','Action'],
            colModel:[
                {name:'srno',index:'srno', width:15,sortable:false,search: false,align:'center'},
                {name:'projcode',index:'projectCode', width:90,sortable:true,searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'projname',index:'projectName', width:90,sortable:true,searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'cost',index:'projectCost', width:90,sortable:true,searchoptions: { sopt: ['eq', 'cn'] }},
                {name:'sdate',index:'projectStartDate', width:90,sortable:true,searchoptions: { sopt: ['eq', 'cn'] },align:'center'},
                {name:'edate',index:'projectEndDate', width:90,sortable:true,searchoptions: { sopt: ['eq', 'cn'] },align:'center'},
                {name:'action',index:'action', width:100,sortable:false,search: false, align:'center'},
            ],
            autowidth: true,
            multiselect: false,
            paging: true,
            rowNum:10,
            rowList:[10,20,30],
            pager: $("#page"),
            caption: "<%=title%> ",
            gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
        }).navGrid('#page',{edit:false,add:false,del:false});
    }
     
    function checkSatus(status){
        if("pending" == status ){
            document.getElementById("hrefPending").className = "sMenu";
            document.getElementById("hrefApprove").className="";
            forPending();
        }
        else{
            document.getElementById("hrefPending").className = "";
            document.getElementById("hrefApprove").className = "sMenu";
            forApprove();
        }
    }

    jQuery().ready(function (){
        forPending();
    });
</script>
<%
            projGridSrUser = null;
            projSrUser = null;
%>