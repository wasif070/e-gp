<%-- 
    Document   : ManageDesignation
    Created on : Nov 27, 2010, 3:20:07 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Designation</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery().ready(function (){
                //alert("url:" + < %=link%>);
                jQuery("#list").jqGrid({
                    url: "../ManageDesigServlet?q=1&action=fetchData",
                    datatype: "xml",
                    height: 250,
                    colNames:['Sl. No.','Designation','Department',/*"Grade",*/"Action"],
                    colModel:[
                        {name:'Sl. No.',index:'Sl. No.', width: 20,sortable: false,align:'center'},
                        {name:'designation',index:'designation', width: 50,sortable: true},
                        {name:'department',index:'department', width:50,sortable: true},
                       /* {name:'grade',index:'grade', width:50,sortable: true,align:'center'},*/
                        {name:'action',index:'action', width:50,sortable: false,align:'center'}
                    ],
                    autowidth: true,
                    multiselect: false,
                    sortable: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    caption: "Manage Designation Data",
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{edit:false,add:false,del:false,search: false,refresh: false});
            });
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td><jsp:include page="../resources/common/AfterLoginLeft.jsp" ></jsp:include></td>
                        <td class="contentArea">

                             <div>
                                <div class="pageHead_1">View Designations
                                <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('3');">Save as PDF</a></span>
                                </div>
                            </div>

                            <div class="t-align-left ff formStyle_1">To sort click on the relevant column header</div>
                            <div class="t_space">
                                <table id="list"></table>
                                <div id="page">

                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <!--For Generate PDF  Starts-->
                <form id="formstyle" action="" method="post" name="formstyle">
                    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                    <%
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                        String appenddate = dateFormat1.format(new Date());
                    %>
                    <input type="hidden" name="fileName" id="fileName" value="Designation_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="Designation" />
                </form>
                <!--For Generate PDF  Ends-->
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var obj = document.getElementById('lblDesignationView');
                if(obj != null){
                    if(obj.innerHTML == 'View Designations'){
                        obj.setAttribute('class', 'selected');
                    }
                }
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
