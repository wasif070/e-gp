<%-- 
    Document   : EditDesignation
    Created on : Nov 26, 2010, 10:40:35 AM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Designation Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <%--<script type="text/javascript" src="Include/pngFix.js"></script>--%>

        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#frmCreateDesignation").validate({
                    rules:{
                        designation:{spacevalidate: true, requiredWithoutSpace: true,maxlength:250,alphaName:true}
                    },

                    messages:{
                        designation:{
                            requiredWithoutSpace:"<div class='reqF_1' id='desigdiv'>Please enter Designation.</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 250 characters only</div>",
                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>",
                            alphaName:"<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' \" } { - . _) Only </div>"
                        }
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#txtDesignation').change(function() {
                    if($('#txtDesignation').val()!=$('#oldValue').val()){
                         var spacetest=/^[\sa-zA-Z!,&.\'\-]+$/;
                        if(spacetest.test($('#txtDesignation').val())) {
                           $('span.#desigMsg').html("Checking for unique Designation...");
                            $.post("<%=request.getContextPath()%>/DesignationServlet", {department: $('#txtdepartmentid').val(),designation:$('#txtDesignation').val(),funName:'verifyDesignation'},  function(j){
                                if(j.toString().indexOf("OK", 0)!=-1){
                                    $('#desigMsg').css("color","green");
                                }
                                else{
                                    $('#desigMsg').css("color","red");
                                }
                                $('span.#desigMsg').html(j);
                            });
                        }
                        else
                        {
                            $('#desigMsg').css("color","red");
                            $('span.#desigMsg').html("Please Enter Valid Designation");
                        }
                    }
                    else
                    {
                        $('span.#desigMsg').html("");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#frmEditDesignation').submit(function() {
                    if($.trim($('#txtDesignation').val())==""){
                        $('span.#desigMsg').css("color","red");
                        $('span.#desigMsg').html('Please enter Designation');
                        $('#tbodyHide').show();
                        $('span.#redirect').css("display","none");
                        return false;
                    }else{
                    if($('#desigMsg').html().toString().length > 0 ) {
                        if($('#desigMsg').html().indexOf("OK", 0) == -1)
                            return false;
                    }}
                    return true;
                });
            });
        </script>
    </head>
</head>
<body>
    <div class="mainDiv">
        <div class="dashboard_div">
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
            <jsp:useBean id="designationSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.DesignationSrBean"/>
            <jsp:useBean id="designationDtBean" scope="request" class="com.cptu.egp.eps.web.databean.DesignationDtBean"/>
            <!--Middle Content Table Start-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr valign="top">
                    <jsp:include page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea_1">
                        <!--Page Content Start-->
                        <%
                                    String logUserId = "0";
                                    if(session.getAttribute("userId")!=null){
                                            logUserId = session.getAttribute("userId").toString();
                                        }
                                    designationSrBean.setUserId(Integer.parseInt(logUserId));
                                    String strDesigId = request.getParameter("designationId");
                                    if (strDesigId != null && !strDesigId.isEmpty()) {

                        %>
                        <!-- Success failure -->
                        <%
                                                                String msg = request.getParameter("msg");
                                                                if (msg != null && msg.equals("success")) {
                        %>
                        <div class="responseMsg successMsg">Designation updation failed</div>
                        <%}%>
                        <!-- Result Display End-->
                        <div class="t_space">
                            <div class="pageHead_1">Edit Designation Details</div>
                        </div>
                        <%
                                                                int designationId = Integer.parseInt(strDesigId);
                                                                designationDtBean.populateInfo(designationId);
                                                                if (designationDtBean.isDataExists()) {
                        %>
                        <form id="frmEditDesignation" action="<%=request.getContextPath()%>/DesignationServlet" method="POST">
                            <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                                <tr>
                                     <td style="font-style: italic" colspan="2" class="ff t-align-left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                </tr>
                                <tr>
                                    <td class="ff" width="12%"><% //Emtaz on 24/April/2016 Organization->Organization/PA
                                                                    
                                                                    if(designationDtBean.getDeptType().equalsIgnoreCase("Organization"))
                                                                    {
                                                                        out.print("Organization");
                                                                    }
                                                                    else
                                                                    {
                                                                        out.print(designationDtBean.getDeptType());
                                                                    } %> : 
                                    </td>
                                    <td width="88%"><label>${designationDtBean.departmentName}</label>
                                        <input type="hidden" id="txtdepartmentid" name="txtdepartmentid" value="${designationDtBean.departmentId}"/>
                                        <input type="hidden" name="action" value="update"/>
                                        <input type="hidden" name="createdBy" value="${designationDtBean.createdBy}"/>
                                        <input type="hidden" name="createdDate" value="${designationDtBean.createdDate}"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Designation : <span>*</span></td>
                                    <td>
                                        <input name="designation" type="text" value="${designationDtBean.designationName}" class="formTxtBox_1" id="txtDesignation" style="width:200px;" maxlength="51"/>
                                        <span id="desigMsg" style="font-weight: bold"></span>
                                        <input name="designationId" type="hidden" value="${designationDtBean.designationId}"/>
                                        <input type="hidden" value="${designationDtBean.designationName}" id="oldValue" name="oldValue"/>
                                    </td>
                                </tr>
                                <tr Style = "display: none;">
                                    <td class="ff">Class-Grade : <span>*</span></td>
                                    <td>
                                            <select name="grade" class="formTxtBox_1" id="grade" style="width: 205px">
                                                <%
                                                    String chk = String.valueOf(designationDtBean.getGradeId());
                                                                                                    for (SelectItem grade : designationSrBean.getGradeList()) {
                                                                                                        
                                                                                                        //if (grade.getObjectId().equals(designationDtBean.getGradeId())) {
                                                                                                        if (grade.getObjectId().toString().equals(chk.trim())) {
                                                %>
                                                <option  value="<%=grade.getObjectId()%>" selected="selected">${designationDtBean.gradeName}-${designationDtBean.gradeLevel}</option>
                                                <%                                                                                                    } else {
                                                                                                            
                                                %>
                                                <option  value="<%=grade.getObjectId()%>"><%=grade.getObjectValue()%></option>
                                                <%                                                                }
                                                                                                    if(grade!=null){
                                                                                                        grade=null;
                                                                                                    }}
                                                %>
                                            </select>
                                        </td>
                                    <%--<td>
                                        <label>${designationDtBean.gradeName}</label>
                                        <input type="hidden" name="grade" value="${designationDtBean.gradeId}"/>
                                    </td>--%>
                                    
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><label class="formBtn_1">
                                            <input type="submit" name="designationSubmit" id="btnDesignationSubmit" value="Update" />
                                        </label>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <%
                                                                                        }
                                                                                        else {%>
                        <div class="responseMsg errorMsg">Data NOT Found for Designation Id: <%=designationId%></div>
                        <%           }
                                                            }
                                                            else {%>
                        <div class="responseMsg errorMsg">Plz Provide Designation Id</div>
                        <%}%>
                        <!--Page Content End-->
                    </td>
                </tr>
            </table>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </div>
        <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
</body>
</html>
