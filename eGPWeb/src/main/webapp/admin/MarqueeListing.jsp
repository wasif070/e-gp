<%-- 
    Document   : MarqueeListing
    Created on : Nov 18, 2010, 11:53:17 AM
    Author     : Kinjal Shah
--%>

<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.model.table.TblTempCompanyMaster,com.cptu.egp.eps.web.utility.BanglaNameUtils,com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Marquee List</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>

<link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
<script type="text/javascript">
       function Delete(obj)
       {
           jConfirm('Do you want to delete the Marquee?', 'Marquee', function (ans){
                if(ans){
                    document.getElementById("frmsubmit").action = "MarqueeListing.jsp?mid="+obj+"&op=delete";
                    document.getElementById("frmsubmit").submit();
                }
           });
       }

    </script>
<script type="text/javascript">
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 10) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 10) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 10) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            function loadTable()
            {                    
                $.post("<%=request.getContextPath()%>/MarqueeServlet", {funName : "viewMarqueelist",pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    var pageNo =$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo)+1);

                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
        </script>
</head>
<body onload="loadTable();">
    <%
        CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
        String userID="0";
        if(session.getAttribute("userId")!=null){
        userID=session.getAttribute("userId").toString();
        }
        commonXMLSPService.setLogUserId(userID);

        if (request.getParameter("mid") != null && request.getParameter("op") != null && request.getParameter("op").equals("delete")) {
            String strCond = "marqueeId=" + request.getParameter("mid");
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
            CommonMsgChk commonMsgChk = new CommonMsgChk();
            String action = "Delete Marquee";
            try{
                commonMsgChk = commonXMLSPService.insertDataBySP("delete", "tbl_MarqueeMaster", "", strCond).get(0);
            }catch(Exception e){
                action = "Error in "+action+" "+e.getMessage();
            }finally{
                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(session.getAttribute("userId").toString()), "userId", EgpModule.Content.getName(), action, "");
                action = null;
            }
            
            if(commonMsgChk.getFlag() ==true){
            response.sendRedirect("MarqueeListing.jsp?msg=trued");
            }else
            {
                response.sendRedirect("MarqueeListing.jsp?msg=false");
            }
        }
    %>
<div class="dashboard_div">
  <!--Dashboard Header Start-->

<div class="topHeader">
      <%@include  file="../resources/common/AfterLoginTop.jsp"%>
  </div>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->

  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                    
                        <td class="contentArea" valign="top">
   <div class="pageHead_1">View Marquee List</div>
   <%
                        // Variable tenderId is defined by u on ur current page.
                                            String msg ="";
                                             msg = request.getParameter("msg");
                            %>
                            <div>&nbsp;</div>
                            <% if (msg != null && msg.contains("truei")) {%>
                                <div class="responseMsg successMsg t_space">Marquee created successfully</div>
                            <%  }else if(msg != null && msg.contains("trueu")){%>
                                <div class="responseMsg successMsg t_space">Marquee updated successfully</div>
                                <%} else if(msg != null && msg.contains("trued")){%>
                                <div class="responseMsg successMsg t_space">Marquee deleted successfully</div>
                                <%} else if(msg != null && msg.contains("false")){%>
                                <div class="responseMsg errorMsg t_space">Operation failed, Please try again later.</div>
                                <%}%>  
  <div id="action-tray" class="t-align-right">
  	<ul>
    	<li><a href="Marquee.jsp?mid=0&op=" class="action-button-add">Create New Marquee</a></li>
    </ul>
  </div>
  <form id="frmsubmit" action="" method="post">
  <table width="100%" cellspacing="0" id="resultTable" class="tableList_3 t_space">
        <tr>
          <th width="4%" class="t-align-center">Sl. No.</th>
          <th width="26%" class="t-align-center">Marquee Text</th>
          <th width="40%" class="t-align-center">Display Location</th>
          <th width="15%" class="t-align-center">Action</th>
     </tr>
 </table>
 <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
    <tr>
        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
        <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
            &nbsp;
            <label class="formBtn_1">
                <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
            </label></td>
        <td align="right" class="prevNext-container"><ul>
                <li><font size="3">&laquo;</font> <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                <li><font size="3">&#8249;</font> <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                <li><a href="javascript:void(0)" id="btnNext">Next</a><font size="3"> &#8250;</font></li>
                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
            </ul></td>
        </tr>
    </table>
        <input type="hidden" id="pageNo" value="1"/>
        <input type="hidden" id="size" value="10"/>
</form>
  <div>&nbsp;</div>
 </td>
           </tr>
</table>



  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
  <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
  <!--Dashboard Footer End-->
</div>
  <script>
                var obj = document.getElementById('lblMarqueeView');
                if(obj != null){
                    if(obj.innerHTML == 'View'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
  <script>
                var headSel_Obj = document.getElementById("headTabContent");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
</body>
</html>

