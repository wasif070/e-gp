<%@page import="sun.misc.BASE64Decoder"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="java.util.Set"%>
<%@page import="com.cptu.egp.eps.web.utility.EncryptDecryptUtils"%>
<%@page import="java.util.Collection"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%! String searchCriteria = "";%>
<%
    response.setHeader("Expires", "-1");
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
    response.setHeader("Pragma", "no-cache");
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bidder Registration and Payment Report</title>
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
<link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
<link type="text/css" rel="stylesheet" href="../resources/js/jQuery/tablesorter.css" />
<script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
<script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
<!--        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>-->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>

        <script type="text/javascript">
            /* Call Print function */
            $(document).ready(function() {
                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });
            });
            function printElem(options){
                $('#titleDiv').show();
                $('#print_area').printElement(options);
                $('#titleDiv').hide();
            }
        </script>

<script type="text/javascript">
    /* Call Calendar function */
    function GetCal(txtname,controlname)
    {
        new Calendar({
            inputField: txtname,
            trigger: controlname,
            showTime: true,
            dateFormat:"%d/%m/%Y",
            onSelect: function() {
                var date = Calendar.intToDate(this.selection.get());
                LEFT_CAL.args.min = date;
                LEFT_CAL.redraw();
                this.hide();
            }
        });

        var LEFT_CAL = Calendar.setup({
            weekNumbers: false
        })
    }
</script>
<script type="text/javascript">
    /* check if pageNO is disable or not */
    function chkdisble(pageNo){
        //alert(pageNo);
        $('#dispPage').val(Number(pageNo));
        if(parseInt($('#pageNo').val(), 10) != 1){
            $('#btnFirst').removeAttr("disabled");
            $('#btnFirst').css('color', '#333');
        }

        if(parseInt($('#pageNo').val(), 10) == 1){
            $('#btnFirst').attr("disabled", "true");
            $('#btnFirst').css('color', 'gray');
        }


        if(parseInt($('#pageNo').val(), 10) == 1){
            $('#btnPrevious').attr("disabled", "true")
            $('#btnPrevious').css('color', 'gray');
        }

        if(parseInt($('#pageNo').val(), 10) > 1){
            $('#btnPrevious').removeAttr("disabled");
            $('#btnPrevious').css('color', '#333');
        }

        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
            $('#btnLast').attr("disabled", "true");
            $('#btnLast').css('color', 'gray');
        }

        else{
            $('#btnLast').removeAttr("disabled");
            $('#btnLast').css('color', '#333');
        }

        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
            $('#btnNext').attr("disabled", "true")
            $('#btnNext').css('color', 'gray');
        }
        else{
            $('#btnNext').removeAttr("disabled");
            $('#btnNext').css('color', '#333');
        }
    }
</script>
        <%
        String searchCriteriaList[] = {"","","","","","","","","","","",""};
        if(request.getParameter("searchCriteria") != null && !request.getParameter("searchCriteria").equals("")) {
            searchCriteria = new String(new BASE64Decoder().decodeBuffer(request.getParameter("searchCriteria")));
            searchCriteriaList = searchCriteria.split(":");
        }

        %>
<script type="text/javascript">
    var sarchData = '<%=searchCriteria%>';
    /*  load Grid for the User Registration Report */
    function loadTable()
    {
        if($("#keyWord").val() == undefined)
            $("#keyWord").val('');
            //alert($('#idregStatus').val());
            var bankname  = "";
            var branchname  = "";
                        if($('#bankName').val()!=""){
                var cmbBank = document.getElementById('bankName');
                bankname  =cmbBank.options[cmbBank.selectedIndex].text;
            }
            if($('#cmbDescmbBankignation').attr('disabled')==false && $('#cmbDescmbBankignation').val()!=''){
                var cmbBank = document.getElementById('cmbDescmbBankignation');
                branchname  =cmbBank.options[cmbBank.selectedIndex].text;
            }
            // For Search Criteria
            var searchPaymetMode = '';
            var paymentStatus = '';
            var searchPaymentFrom = '';
            var searchPaymentTo = '';
            var searchEmailId = '';
            var sortEmailSearch = '';
            var company = '';
            var sortCompany = '';
            var searchBank = '';
            var searchBranch = '';
            var searPageNo = '';
            var searchSize = '';
            if(sarchData == '') {
                searchPaymetMode = $('#cmbPaymentFor').val();
                paymentStatus = $('#idregStatus').val();
                searchPaymentFrom = $("#txtpaymentDateFrom").val();
                searchPaymentTo = $("#txtpaymentDateTo").val();
                searchEmailId = $("#txtemail").val();
                sortEmailSearch =$("#idemailSearchOption").val();
                company = $("#txtCompanyName").val();
                sortCompany = $("#idcmpSearchOption").val();
                searchBank = bankname;
                searchBranch = branchname;
                searPageNo= $("#pageNo").val();
                searchSize= $("#size").val();
            } else {
                searchPaymetMode = "<%=searchCriteriaList[0]%>";
                paymentStatus = "<%=searchCriteriaList[1]%>";
                searchPaymentFrom = "<%=searchCriteriaList[2]%>";
                searchPaymentTo = "<%=searchCriteriaList[3]%>";
                searchEmailId = "<%=searchCriteriaList[4]%>";
                sortEmailSearch = "<%=searchCriteriaList[5]%>";
                company = "<%=searchCriteriaList[6]%>";
                sortCompany = "<%=searchCriteriaList[7]%>";
                searchBank = "<%=searchCriteriaList[8]%>";
                searchBranch = "<%=searchCriteriaList[9]%>";
                searPageNo= "<%=searchCriteriaList[10]%>";
                searchSize= "<%=searchCriteriaList[11]%>";
                sarchData = "";
            }
            //payId=485&uId=2871&strPageNo=1&strOffset=10&status=&paymentFrom=2011-04-07&paymentTo=2013-04-25&emailId=yagnesh@localmail.com&companyName=&bankName=&branchName=&sortEmail==&sortCmp=
            $.post("<%=request.getContextPath()%>/UserRegReportServlet", {paymetMode : searchPaymetMode,status : paymentStatus ,paymentFrom: searchPaymentFrom, paymentTo: searchPaymentTo, emailId: searchEmailId ,companyName: company,action:'Search',size: searchSize,pageNo: searPageNo,sortEmail: sortEmailSearch ,sortCmp: sortCompany,bankName:searchBank,branchName:searchBranch},  function(j){
            $('#resultTable').find("tr:gt(0)").remove();
            $('#resultTable tr:last').after(j);

            sortTable();
            
            if($('#noRecordFound').attr('value') == "noRecordFound"){
                $('#pagination').hide();
            }else{
                $('#pagination').show();
            }
            chkdisble($("#pageNo").val());
            if($("#totalPages").val() == 1){
                $('#btnNext').attr("disabled", "true");
                $('#btnLast').attr("disabled", "true");
            }else{
                $('#btnNext').removeAttr("disabled");
                $('#btnLast').removeAttr("disabled");
            }
            $("#pageNoTot").html($("#pageNo").val());
            $("#pageTot").html($("#totalPages").val());
            $('#resultDiv').show();

            loadCriteriaTable();
        });
    }
</script>
<script type="text/javascript">
    var sarchData = '<%=searchCriteria%>';
    /*  load Criteria totals for the User Registration Report */
    function loadCriteriaTable()
    {
        if($("#keyWord").val() == undefined)
            $("#keyWord").val('');
            //alert($('#idregStatus').val());
            var bankname  = "";
            var branchname  = "";
                        if($('#bankName').val()!=""){
                var cmbBank = document.getElementById('bankName');
                bankname  =cmbBank.options[cmbBank.selectedIndex].text;
            }
            if($('#cmbDescmbBankignation').attr('disabled')==false && $('#cmbDescmbBankignation').val()!=''){
                var cmbBank = document.getElementById('cmbDescmbBankignation');
                branchname  =cmbBank.options[cmbBank.selectedIndex].text;
            }
            // For Search Criteria
            var searchPaymetMode = '';
            var paymentStatus = '';
            var searchPaymentFrom = '';
            var searchPaymentTo = '';
            var searchEmailId = '';
            var sortEmailSearch = '';
            var company = '';
            var sortCompany = '';
            var searchBank = '';
            var searchBranch = '';
            var searPageNo = '';
            var searchSize = '';
            if(sarchData == '') {
                searchPaymetMode = $('#cmbPaymentFor').val();
                paymentStatus = $('#idregStatus').val();
                searchPaymentFrom = $("#txtpaymentDateFrom").val();
                searchPaymentTo = $("#txtpaymentDateTo").val();
                searchEmailId = $("#txtemail").val();
                sortEmailSearch =$("#idemailSearchOption").val();
                company = $("#txtCompanyName").val();
                sortCompany = $("#idcmpSearchOption").val();
                searchBank = bankname;
                searchBranch = branchname;
                searPageNo= $("#pageNo").val();
                searchSize= $("#size").val();
            } else {
                searchPaymetMode = "<%=searchCriteriaList[0]%>";
                paymentStatus = "<%=searchCriteriaList[1]%>";
                searchPaymentFrom = "<%=searchCriteriaList[2]%>";
                searchPaymentTo = "<%=searchCriteriaList[3]%>";
                searchEmailId = "<%=searchCriteriaList[4]%>";
                sortEmailSearch = "<%=searchCriteriaList[5]%>";
                company = "<%=searchCriteriaList[6]%>";
                sortCompany = "<%=searchCriteriaList[7]%>";
                searchBank = "<%=searchCriteriaList[8]%>";
                searchBranch = "<%=searchCriteriaList[9]%>";
                searPageNo= "<%=searchCriteriaList[10]%>";
                searchSize= "<%=searchCriteriaList[11]%>";
                sarchData = "";
            }
            //payId=485&uId=2871&strPageNo=1&strOffset=10&status=&paymentFrom=2011-04-07&paymentTo=2013-04-25&emailId=yagnesh@localmail.com&companyName=&bankName=&branchName=&sortEmail==&sortCmp=
            $.post("<%=request.getContextPath()%>/UserRegReportServlet", {paymetMode : searchPaymetMode,status : paymentStatus ,paymentFrom: searchPaymentFrom, paymentTo: searchPaymentTo, emailId: searchEmailId ,companyName: company,action:'SearchCriteria',size: searchSize,pageNo: searPageNo,sortEmail: sortEmailSearch ,sortCmp: sortCompany,bankName:searchBank,branchName:searchBranch},  function(j){
            $('#searchcritres').find("tr:gt(0)").remove();
            $('#searchcritres tr:last').after(j);

             sortTable();

            if($('#noRecordFound').attr('value') == "noRecordFound"){
                $('#pagination').hide();
            }else{
                $('#pagination').show();
            }
            chkdisble($("#pageNo").val());
            if($("#totalPages").val() == 1){
                $('#btnNext').attr("disabled", "true");
                $('#btnLast').attr("disabled", "true");
            }else{
                $('#btnNext').removeAttr("disabled");
                $('#btnLast').removeAttr("disabled");
            }
            $("#pageNoTot").html($("#pageNo").val());
            $("#pageTot").html($("#totalPages").val());
            $('#resultDiv').show();


        });
    }
</script>
<script type="text/javascript">
    /*  Handle First click event */
    $(function() {
        $('#btnFirst').click(function() {
            var totalPages=parseInt($('#totalPages').val(),10);
            var pageNo =$('#pageNo').val();
            if(totalPages>0 && pageNo!="1")
            {
                $('#pageNo').val("1");
                loadTable();
                $('#dispPage').val("1");
            }
        });
    });
</script>
<script type="text/javascript">
    /*  Handle Last Button click event */
    $(function() {
        $('#btnLast').click(function() {
            var totalPages=parseInt($('#totalPages').val(),10);
            if(totalPages>0){
                $('#pageNo').val(totalPages);
                loadTable();
                $('#dispPage').val(totalPages);
            }
        });
    });
</script>
<script type="text/javascript">
    /*  Handle Next Button click event */
    $(function() {
        $('#btnNext').click(function() {
            var pageNo=parseInt($('#pageNo').val(),10);
            var totalPages=parseInt($('#totalPages').val(),10);

            if(pageNo < totalPages) {
                $('#pageNo').val(Number(pageNo)+1);
                loadTable();
                $('#dispPage').val(Number(pageNo)+1);

                $('#dispPage').val(Number(pageNo)+1);
            }
        });
    });

</script>
<script type="text/javascript">
    /*  Handle Previous click event */
    $(function() {
        $('#btnPrevious').click(function() {
            var pageNo=$('#pageNo').val();
            if(parseInt(pageNo, 10) > 1)
            {
                $('#pageNo').val(Number(pageNo) - 1);
                loadTable();
                $('#dispPage').val(Number(pageNo) - 1);
            }
        });
    });
</script>
<script type="text/javascript">
    /*  Handle Go To Button event */
    $(function() {
        $('#btnGoto').click(function() {
            var pageNo=parseInt($('#dispPage').val(),10);
            var totalPages=parseInt($('#totalPages').val(),10);
            if(pageNo > 0)
            {
                if(pageNo <= totalPages) {
                    $('#pageNo').val(Number(pageNo));
                    loadTable();
                    $('#dispPage').val(Number(pageNo));
                }
            }
        });
    });
    <%--function restVal(){
        $('#idregStatus').val('');
        $('#txtpaymentDateFrom').val('');
        $('#txtemail').val('');
        $('#txtpaymentDateTo').val('');
        $('#txtCompanyName').val('');
        $('#cmbDescmbBankignation').attr('disabled',true);
        loadTable();
    }--%>
</script>
<script type="text/javascript">
    function checkExpiry(stausValue){
        if(stausValue=="Expired"){
            document.getElementById("paymentRow").style.display = "none";
        }else{
            document.getElementById("paymentRow").style.display = "table-row";
        }
    }
function setBranch(){
    if($('#bankName').val()!= "" && $('#bankName').val()!= 'Dutch Bangla Bank(DBBL)' && $('#bankName').val()!= 'BRAC Bank'){
        $.post("<%=request.getContextPath()%>/ComboServlet", {type:'Scheduled Bank',objectId:$('#bankName').val(),funName:'branchCombo'}, function(j){
            $('#cmbDescmbBankignation').removeAttr("disabled");
            $("select#cmbDescmbBankignation").html(j);
         });
    }else{
        $('#cmbDescmbBankignation').attr('disabled',true);
    }
}
function chkBank(){
    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbPaymentFor').val(),funName:'paymentType'}, function(j){
        $("select#bankName").html(j);
     });
//  if($('#cmbPaymentFor').val()== "Online"){
//      $('#bankName').attr('disabled',true);
//      $('#cmbDescmbBankignation').attr('disabled',true);
//  }else{
//      $('#bankName').removeAttr("disabled");
//      $('#cmbDescmbBankignation').removeAttr("disabled");
//  }
}
</script>
<%
                int userid = 0;
                int usertypeid = 0;
                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") != null) {
                    userid = Integer.parseInt(hs.getAttribute("userId").toString());
                }
                if (hs.getAttribute("userTypeId") != null) {
                    usertypeid = Integer.parseInt(hs.getAttribute("userTypeId").toString());
                }
                CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> list = csdms.geteGPData("getMainPaymentBanksList", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
                List<SPCommonSearchDataMore> spGetTotal = csdms.geteGPData("getPaymentReportTotal");
                List<SPCommonSearchDataMore> spGetTotalPaid = csdms.geteGPData("getPaymentReportTotal","paid");
                List<SPCommonSearchDataMore> spGetTotalVerified = csdms.geteGPData("getPaymentReportTotal","verified");
System.out.println("size is"+spGetTotalPaid.size());
        System.out.println("size is"+spGetTotalVerified.size());
                /**
                Online Bank Name added from List
                **/
                Collection<String> bankval = null;
                bankval = XMLReader.getBnkMsg().values();
                Object[] valArr = bankval.toArray();

                for (int i = 0; i < valArr.length; i++) {
                    if (i % 2 == 1) {
                        SPCommonSearchDataMore onlineBank = new SPCommonSearchDataMore();
                        onlineBank.setFieldName1(valArr[i].toString());
                        onlineBank.setFieldName2(valArr[i].toString());
                        list.add(onlineBank);
                    }
                }
    %>
</head>
<body onload="loadTable()">
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <%@include file="../resources/common/AfterLoginTop.jsp" %>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
  <div class="pageHead_1">Bidder Registration and Payment Report</div>
  <div class="formBg_1 t_space">
      <form name="frmpayment" id="frmpayment" action="UserRegReport.jsp?fromWhere=MyAccount" method="post">
  <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
    <tr>
        <td width="13%" class="ff">Payment Type :</td>
      <td width="32%">
          <select name="cmbPaymentFor" class="formTxtBox_1" id="cmbPaymentFor" style="width:80px;" onchange="chkBank();">
              <option value="All">All</option>
              <option value="Online">Online</option>
              <option value="Offline">Offline</option>
          </select>
      </td>
      <td class="ff" width="15%">Status :</td>
      <td width="40%">
          <select name="txtregStatus" class="formTxtBox_1" id="idregStatus" style="width:80px;" onchange="checkExpiry(this.value);">
              <option value="">--Select--</option>
              <option value="New">New</option>
              <option value="Renew">Renew</option>
              <option value="Expired">Expired</option>
              <option value="Disabled">Disabled</option>
          </select>
      </td>

      </tr>
      <tr style="display:table-row;" id="paymentRow">
          <td class="ff">Payment Date From :</td>
<!--          <td>
             <input name="txtpaymentDateFrom" type="text" class="formTxtBox_1" id="txtpaymentDateFrom" style="width:100px;" readonly="true" onClick="GetCal('txtpaymentDateFrom','imgCal');" />
                  &nbsp;<a href="javascript:void(0);" onclick ="GetCal('txtpaymentDateFrom','imgCal');" title="Calender">
                      <img id="imgCal" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"/>
                  </a>
          </td>-->
          <td>
             <input name="txtpaymentDateFrom" type="text" class="formTxtBox_1" id="txtpaymentDateFrom" style="width:100px;" readonly="true" onClick="GetCal('txtpaymentDateFrom','imgCal');" />
              &nbsp;
              <a href="javascript:void(0);" onclick ="GetCal('txtpaymentDateFrom','imgCal');" title="Calender">
                  <img id="imgCal" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"/>
              </a>
          </td>
         <td class="ff">Payment Date To :</td>
          <td>
             <input name="txtpaymentDateTo" type="text" class="formTxtBox_1" id="txtpaymentDateTo" style="width:100px;" readonly="true" onClick="GetCal('txtpaymentDateTo','imgCal1');" />
              &nbsp;
              <a href="javascript:void(0);" onclick ="GetCal('txtpaymentDateTo','imgCal1');" title="Calender">
                  <img id="imgCal1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"/>
              </a>
          </td>
       </tr>
      <tr>
      <td class="ff">e-mail ID :</td>
      <td>
          <select name="txtemailSearchOption" class="formTxtBox_1" id="idemailSearchOption" style="width:80px;" >
              <option value="=" selected>Equals</option>
              <option value="Like">Contains</option>
          </select>
          <input name="txtemail" type="text" class="formTxtBox_1" id="txtemail" style="width:200px;" />
      </td>
      <td class="ff">Company Name :</td>
      <td><select name="txtcmpSearchOption" class="formTxtBox_1" id="idcmpSearchOption" style="width:80px;" >
              <option value="=" selected>Equals</option>
              <option value="Like">Contains</option>
          </select>
          <input name="txtCompanyName" type="text" class="formTxtBox_1" id="txtCompanyName" style="width:200px;" />
      </td>
    </tr>
    <tr>
        <td  class="ff">Financial Institution Name:</td>
        <td><select name="bankName" class="formTxtBox_1" id="bankName" style="width:250px;" onchange="setBranch();">
                <option value="">--Select Financial Institution--</option>
                <%if (!list.isEmpty() && list != null) {
                                for (int i = 0; i < list.size(); i++) {
                %>
                <option value=<%=list.get(i).getFieldName1()%>><%=list.get(i).getFieldName2()%></option>
                <%}
                            }%>
            </select></td>
            <td class="ff">Financial Institution Branch:</td>
                <td width="21%">
                <select name="branch"  id="cmbDescmbBankignation" class="formTxtBox_1" style="width: 200px" disabled>
                    <option selected="selected" value="">-- Financial Institution Branch --</option>
                </select>
            </td>

    </tr>
    <tr>
      <td colspan="2" class="t-align-center">
          <label class="formBtn_1">
              <input type="button" name="button" id="button" value="Search" onclick="loadTable();loadCriteriaTable();" />
          </label>
          <label class="formBtn_1">
              <input type="submit" name="btnReset" id="btnReset" value="Reset" />
          </label>
      </td>
      <td colspan="2" class="t-align-right">
        <%
           String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
           String folderName = pdfConstant.PAYMENT;
           String genId = cdate + "_" + userid;
           if(usertypeid == 1 || usertypeid == 8) {
        %>
           <a class="action-button-savepdf"  href="<%=request.getContextPath()%>/GeneratePdf?reqURL=&reqQuery=&folderName=<%=folderName%>&id=<%=genId%>" >Save As PDF </a>
                                           &nbsp;
        <%} else {%>
            <a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('11');">Save as PDF</a>
                                           &nbsp;
        <%}%>
       <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
      </td>
    </tr>
  </table>
  </form>
  </div>
<div id="resultDiv" style="display: block;">
    <div  id="print_area">
         <div class="formStyle_1 t_space ff" id="searchCriteria">
                            <h3>
                               <%-- Search Criteria :
                                <%
                                String commaSepr = "";
                                if(searchCriteriaList != null) {
                                                                    if (searchCriteriaList[0] != null && !searchCriteriaList[0].equals("")) {
                                                                        out.print("Payment Type : " + searchCriteriaList[0]);
                                                                        commaSepr = ",";
                                                                    }
                                                                    if (searchCriteriaList[1] != null && !searchCriteriaList[1].equals("")) {
                                                                        out.print(commaSepr + "Status : " + searchCriteriaList[1]);
                                                                        commaSepr = ",";
                                                                    }
                                                                    if (searchCriteriaList[2] != null && !searchCriteriaList[2].equals("")) {
                                                                        out.print(commaSepr + "Payment Date From : " + searchCriteriaList[2]);
                                                                        commaSepr = ",";
                                                                    }
                                                                    if (searchCriteriaList[3] != null && !searchCriteriaList[3].equals("")) {
                                                                        out.print(commaSepr + "Payment Date To : " + searchCriteriaList[3]);
                                                                        commaSepr = ",";
                                                                    }
                                                                    if (searchCriteriaList[4] != null && !searchCriteriaList[4].equals("")) {
                                                                        out.print(commaSepr + "e-mail ID : " + searchCriteriaList[4]);
                                                                        commaSepr = ",";
                                                                    }
                                                                    if (searchCriteriaList[5] != null && !searchCriteriaList[5].equals("")) {
                                                                        out.print(commaSepr + "Company Name : " + searchCriteriaList[5]);
                                                                        commaSepr = ",";
                                                                    }
                                                                    if (searchCriteriaList[6] != null && !searchCriteriaList[6].equals("")) {
                                                                        out.print(commaSepr + "Bank Name : " + searchCriteriaList[6]);
                                                                        commaSepr = ",";
                                                                    }
                                                                    if (searchCriteriaList[7] != null && !searchCriteriaList[7].equals("")) {
                                                                        out.print("Bank Branch : " + searchCriteriaList[7]);
                                                                    }
                                                                    }
                                %>

                               --%>
                            </h3>
                        </div>
<div id='titleDiv' class="pageHead_1" style="display: none;">Bidder Registration and Payment Report</div>
<b>NA = Not Available</b>
<table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable" cols="@0,11">
      <tr>
        <th width="3%" class="t-align-center">Sl. No.</th>
        <th width="12%" class="t-align-center">	Registration Type</th>
        <th width="9%" class="t-align-center">Registration Status</th>
        <th width="15%" class="t-align-center">Company Name</th>
        <th width="14%" class="t-align-center">e-mail ID</th>
        <th width="8%" class="t-align-center">Country</th>
        <th width="5%" class="t-align-center">Dzongkhag/ District </th>
        <th width="5%" class="t-align-center">Payment Status</th>
        <th width="5%" class="t-align-center">Payment Type</th>
        <th width="13%" class="t-align-center">Payment Date & Time</th>
        <th width="10%" class="t-align-center">Amount</th>
        <th width="12%" class="t-align-center">Action</th>
    </tr>
</table>
    </div>

<table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
    <tr>
        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Total Records : <input type="text" name="size" id="size" value="10" style="width:70px;"/>
        </td>
        <td></td>
        <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
            &nbsp;
            <label class="formBtn_1">
                <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
            </label></td>
        <td  class="prevNext-container">
            <ul>
                <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
            </ul>
       </td>
    </tr>
</table>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="pagging_1" id="searchcritres">
                                <tr><td  width="70%" align='right' ></td><td width="30%"align='left' ></td></tr>

                            </table>
      
<input type="hidden" id="pageNo" value="1"/>
<!--input type="hidden" id="size" value="20"/-->
</div>
  <p>&nbsp;</p>
    <div>&nbsp;</div>
</div> <!--For Generate PDF  Starts-->
                <form id="formstyle" action="" method="post" name="formstyle">
                    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                    <%
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                        String appenddate = dateFormat1.format(new Date());
                    %>
                    <input type="hidden" name="fileName" id="fileName" value="PaymentReport_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="PaymentReport" />
                </form>
                <!--For Generate PDF  Ends-->
    <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
 <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
  <!--Dashboard Footer End-->
</div>
</body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabReport");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
