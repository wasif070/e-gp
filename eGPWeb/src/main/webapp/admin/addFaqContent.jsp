<%-- 
    Document   : addFaqContent
    Created on : May 30, 2017, 11:29:11 AM
    Author     : Nitish Oritro
--%>


<%@page import="com.cptu.egp.eps.model.table.TblFaqBhutan"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.FaqBhutanService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.BhutanDebarmentCommitteeService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.util.Calendar"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Add FAQ</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <script type="text/javascript">

            $(document).ready(function () 
            {    
                $("#faqContent").validate({
                    rules: {
                        questions: {required: true}
                    },
                    messages: {
                        questions: {required: "<div class='reqF_1'> Please Enter Questions.</div>"}
                    }
                });
                
                $('#questions').blur(function()
                {  
                    var questions = $("#questions").val();
                    
                    if(questions !="")
                    {
                        $('#questionsMsg').html('');
                    }
                    else
                    {
                        $('#questionsMsg').html('Please Enter Questions.'); 
                    }
                });
            });

        </script>
        <%
            int createdBy = Integer.parseInt(session.getAttribute("userId").toString());
            if ("Create".equals(request.getParameter("button"))) {
                boolean doneFlag = false;
                String action = "";
                
                String questions = request.getParameter("questions");
                
                String answers = request.getParameter("answers");
                FaqBhutanService FaqBhutanService = (FaqBhutanService) AppContext.getSpringBean("FaqBhutanService");
                TblFaqBhutan tblFaqBhutanThis = new TblFaqBhutan();
                try {

                    tblFaqBhutanThis.setQuestion(questions);

                    tblFaqBhutanThis.setAnswer(answers);
                    doneFlag = FaqBhutanService.addTblFaqBhutan(tblFaqBhutanThis);
                } catch (Exception e) {
                    System.out.println(e);
                    action = "Error in : " + action + " : " + e;
                } finally {
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                    action = null;
                }
                response.sendRedirect("viewFaqContent.jsp?msg=faqCreatedSuccessfully");
            }
        %>

    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                        <td class="contentArea" ><div class="pageHead_1">Add a Frequently Asked Questions</div>
                            <form id="faqContent" name="faqContent" action="addFaqContent.jsp" method="post">
                                <table border="0" width="100%" cellspacing="10" cellpadding="0"  class="formStyle_1">
                                    <tr>
                                        <td style="font-style: italic" class="t-align-left" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                    </tr>

                                    <tr>
                                        <td class="ff">Questions : <span>*</span></td>
                                        <td>
                                            <span id="questionsMsg" style="color: red; font-weight: bold"></span>
                                            <textarea rows="2" class="formTxtBox_1" style="width:700px;" id="questions" name="questions"></textarea>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff">Answers : <span>*</span></td>
                                        <td>
                                            <textarea rows="6" class="formTxtBox_1" style="width:700px;" id="answers" name="answers"></textarea>
                                            <span id="answersMsg" style="color: red; font-weight: bold"></span>
                                            <script type="text/javascript">
                                                CKEDITOR.replace('answers', {
                                                    toolbar: "egpToolbar"
                                                });
                                            </script>
                                        </td>
                                    </tr>
                                <%                                //Date currentDate = new Date();
                                //SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy hh:mm");
%>
                                <%--<input type="hidden" id="currentDate" value="<%=dt.format(currentDate)%>">--%>
                                </tbody>
                            </table>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" cellpadding="0" >
                                <tr>
                                    <td class="t-align-center" style="padding-right:110px;"><span class="formBtn_1"><input type="submit" name="button" id="button" value="Create" onclick="return validate();"/></span></td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>

        <script type="text/javascript">
            var headSel_Obj = document.getElementById("headTabContent");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }
            var leftObj = document.getElementById("lblFaqAddContent");
            if (leftObj != null) {
                leftObj.setAttribute("class", "selected");
            }
            
            function validate(){
                var flag = 1;
                var questions = document.getElementById("questions");
                //value = CKEDITOR.instances[answers].getData();
                if(CKEDITOR.instances.answers.getData() == 0)
                {
                    //value = CKEDITOR.instances[elementId].getData();
                    document.getElementById("answersMsg").innerHTML = "Please Enter the FAQ answer";
                    alert("Please Enter the FAQ answer");
                    flag = 0;
                    return false;
                }
                if(questions != null)
                {
                    return true;
                }
                else
                {
                    $('#questionsMsg').html('Please Enter Questions.'); 
                }
                return true;
            }
            
        </script>
    </body>
</html>