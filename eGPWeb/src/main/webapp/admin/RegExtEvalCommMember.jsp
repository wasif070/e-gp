<%-- 
    Document   : RegExtEvalCommMember
    Created on : Feb 17, 2011, 5:03:40 PM
    Author     : rishita
--%>

<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.servlet.CommonServlet"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register External Committee Member</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    String createdBy = "";
                    if (!session.getAttribute("userId").equals("") && session.getAttribute("userId") != null) {
                        createdBy = session.getAttribute("userId").toString();
                    }

                    String msg = "";
        %>
    </head>
    <jsp:useBean id="regExtEvalCommMemberSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.RegExtEvalCommMemberSrBean"/>
    <jsp:useBean id="regExtEvalCommMemberDtBean" scope="request" class="com.cptu.egp.eps.web.databean.RegExtEvalCommMemberDtBean"/>
    <jsp:setProperty name="regExtEvalCommMemberDtBean" property="*"/>
    <body>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmregExtEvalCommMember").validate({
                    rules: {
                        emailId: { required: true},
                        password: { required: true,minlength: 8, maxlength:25, alphaForPassword:true},
                        confirmPassword: { required: true,equalTo: "#txtPassword"},
                        fullName: { requiredWithoutSpace: true , alphaName:true, maxlength:100 },
                        nameBanglaString: {maxlength:100},
                        phoneNo: { numberWithHyphen:true,PhoneFax: true},
                        mobileNo: { number: true, minlength: 8, maxlength:8 },
                        nationalId:{number: true,minlength: 13,maxlength: 25}
                    },
                    messages: {
                        emailId: { required: "<div class='reqF_1'>Please enter e-mail ID</div>"},
                        password: { required: "<div class='reqF_1'>Please enter Password</div>",
                            minlength: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>",
                            maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>",
                            alphaForPassword: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>"
                        },
                        confirmPassword: { required: "<div class='reqF_1'>Please retype Password</div>",
                            equalTo: "<div class='reqF_1' id='msgPwdNotMatch'>Password doesnot match. Please try again.</div>"
                        },
                        fullName: { //spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace: "<div class='reqF_1'>Please enter Full Name</div>",
                            alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , ' \" } { - . _) Only </div>",
                            maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"
                        },
                        nameBanglaString: {maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"
                        },
                        phoneNo: { numberWithHyphen: "<div class='reqF_1'>Allows numbers (0-9) and hyphen (-) only</div>",
                            PhoneFax:"<div class='reqF_1'>Please enter valid Phone No. Area code should contain minimum 2 digits and maximum 5 digits. Phone no. should contain minimum 3 digits and maximum 10 digits</div>"
                        },
                        mobileNo: { number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" ,
                            minlength:"<div class='reqF_1'>Minimum 8 digits are required</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 8 digits only</div>"
                        },
                        nationalId: {
                            number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                            minlength: "<div class='reqF_1'>National ID should comprise of minimum 13 digits</div>",
                            maxlength: "<div class='reqF_1'>National ID should comprise of maximum 25 digits</div>"
                        }
                    },
                    errorPlacement:function(error ,element)
                    {
                        if(element.attr("name")=="password")
                        {
                            error.insertAfter("#tipPassword");
                        }
                        else if(element.attr("name")=="phoneNo")
                        {
                            error.insertAfter("#phno");
                        }
                        else if(element.attr("name")=="mobileNo")
                        {
                            error.insertAfter("#mNo");
                        }
                        else
                        {
                            error.insertAfter(element);

                        }
                    }
                });
            });
            function copyPaste(){
                jAlert("Copy and Paste not allowed.","External User Registration", function(RetVal) {
                });
                return false;
            }
        </script>
        <%
                    if ("Submit".equals(request.getParameter("hdnbutton"))) {
                        String chkUniqueEmailID = regExtEvalCommMemberSrBean.verifyMail(regExtEvalCommMemberDtBean.getEmailId().trim());
                        if (chkUniqueEmailID.length() > 2) {
                            response.sendRedirect("RegExtEvalCommMember.jsp?msg=no");
                        } 
                        else
                         {
                            regExtEvalCommMemberDtBean.setFullName(regExtEvalCommMemberDtBean.getFullName().trim());
                            if (regExtEvalCommMemberSrBean.insertInToTblExternalMemInfo(regExtEvalCommMemberDtBean, request.getParameter("password"))) 
                            {
                                // Coad added by Dipal for Audit Trail Log.
                                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                                String idType="userId";
                                int auditId=regExtEvalCommMemberSrBean.getUserId();
                                String auditAction="Create External Evaluation Commitee Memeber User";
                                String moduleName=EgpModule.Manage_Users.getName();
                                String remarks="";
                                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                   
                                response.sendRedirect("ViewRegExtEvalCommMember.jsp?uId=" + regExtEvalCommMemberSrBean.getUserId() + "&msg=success");
                            }
                            else 
                            {
                                response.sendRedirect("RegExtEvalCommMember.jsp?msg=failure");
                                // Coad added by Dipal for Audit Trail Log.
                                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                                String idType="userId";
                                int auditId=Integer.parseInt(session.getAttribute("userId").toString());
                                String auditAction="Error in Create External Evaluation Commitee Memeber User";
                                String moduleName=EgpModule.Manage_Users.getName();
                                String remarks="";
                                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                            }
                        }
                    }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>
                        <td class="contentArea_1">
                            <div class="pageHead_1">Create External Evaluation Committee Member</div>
                            <%if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("no")) {
                            %>
                            <div align="left" id="sucMsg" class="responseMsg errorMsg ">e-mail ID already exist, Please enter another e-mail ID</div>
                            <% }%>
                            <%if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("failure")) {
                            %>
                            <div align="left" id="sucMsg" class="responseMsg errorMsg ">External Committee Member is not created successfully</div>
                            <% }%>
                            <form action="" method="post" id="frmregExtEvalCommMember" name="frmregExtEvalCommMember">
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td style="font-style: italic" class="ff t-align-left" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="15%">e-mail ID:   <span>*</span></td>
                                        <td width="85%"><input name="emailId" type="text" class="formTxtBox_1" id="txtEmailId" style="width:200px;"/>
                                            <div id="mailMsg" style="color: red; "></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Password :<span>*</span></td>
                                        <td><input name="password" type="password" class="formTxtBox_1" id="txtPassword" onblur="chkPasswordMatches();" style="width:200px;" autocomplete="off" /><br/><span id="tipPassword" style="color: grey;"> (Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. 
                                                <br/>Special characters may be added) </span>
<!--                                            <span id="passnote" class="formNoteTxt">-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Confirm Password:    <span>*</span></td>
                                        <td><input name="confirmPassword" type="password" class="formTxtBox_1" onblur="chkPasswordMatches();" id="txtConfirmPassword" onpaste="return copyPaste();" style="width:200px;" autocomplete="off" />
                                            <div style="color: green" id="pwdMatchMsg"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Full Name : <span>*</span></td>

                                        <td><input name="fullName" type="text" class="formTxtBox_1" maxlength="101" id="txtFullName" style="width:200px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Name  in Dzongkha : </td>
                                        <td><input name="nameBanglaString" type="text" class="formTxtBox_1" id="txtNameBanglaString" maxlength="101" style="width:200px;" /></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">CID No. : </td>

                                        <td><input name="nationalId" type="text" class="formTxtBox_1" id="txtNationalId" style="width:200px;" maxlength="26" /></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Phone  No. :</td>
                                        <td><input name="phoneNo" type="text" class="formTxtBox_1" id="txtPhoneNo" style="width:200px;" maxlength="17"/>
                                            <span id="phno" style="color: grey;">(Area Code - Phone No. e.g. 2-324425)</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Mobile  No. : </td>
                                        <td><input name="mobileNo" maxlength="16" type="text" class="formTxtBox_1" id="txtMobileNo" style="width:200px;" /><span id="mNo" style="color: grey;"> (Mobile No. format should be e.g 12345678)</span>
                                            <input name="createdBy" type="hidden" value="<%=createdBy%>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td class="t-align-left">
                                            <label class="formBtn_1"><input type="submit" name="Submit" id="Submit" value="Submit" /></label>
                                            <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <!--Middle Content Table End-->
                        </td>
                        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>
        <script>
                var obj = document.getElementById('lblExtEvalCommCreation');
                if(obj != null){
                    if(obj.innerHTML == 'Create User'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
        <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
<script type="text/javascript">
    $(function() {
        $('#txtFullName').blur(function() {
            $(".err").remove();
            if($('#txtFullName').val().length !=0 && $.trim($('#txtFullName').val()).length == 0){
               $("#txtFullName").parent().append("<div class='err' style='color:red;'>Only space is not allowed</div>");
//               valid = false;
            }
        });
        $('#frmregExtEvalCommMember').submit(function() {
            $(".err").remove();
            if($('#txtFullName').val().length !=0 && $.trim($('#txtFullName').val()).length == 0){
               $("#txtFullName").parent().append("<div class='err' style='color:red;'>Only space is not allowed</div>");
               return false;
            }else if($('#frmregExtEvalCommMember').valid()){
                if($('#mailMsg').html().toUpperCase() == 'OK'){
                    if($('#Submit')!=null){
                        $('#Submit').attr("disabled","true");
                        $('#hdnbutton').val("Submit");
                    }
                } else{
                    return false;
                }
            }
        });
    });

    $(function() {
        $('#txtEmailId').blur(function() {
            if($.trim($('#txtEmailId').val()) != '') {
                $('#mailMsg').css("color","red");
                $('#mailMsg').html("Checking for unique e-mail ID...");
                $.post("<%=request.getContextPath()%>/CommonServlet", {mailId:$.trim($('#txtEmailId').val()),funName:'verifyMail'},
                function(j){
                    if($('#uniqueValEmail').val() != undefined){
                        $('#uniqueValEmail').remove();
                    }
                    if(j.toString().indexOf("OK", 0)!=-1){
                        $('#mailMsg').css("color","green");
                    }
                    else{
                        $('#mailMsg').css("color","red");
                    }
                    $('#mailMsg').html(j);
                });
            }else{
                $('#mailMsg').html("");
            }
        });
    });


    function chkPasswordMatches(){
        var objPwd = document.getElementById("txtPassword");
        var objConfirmPwd = document.getElementById("txtConfirmPassword");
        if(objPwd != null && objConfirmPwd != null){
            if($.trim(objPwd.value) == "" || $.trim(objConfirmPwd.value) == ""){}else{
                var msgPwdMatchObjDiv = document.getElementById("pwdMatchMsg");
                if(objPwd.value == objConfirmPwd.value){
                    var msgPwdNotMatch = document.getElementById("msgPwdNotMatch");
                    if(msgPwdNotMatch!=null){
                        if(msgPwdNotMatch.innerHTML == "Password does not match. Please try again"){
                            msgPwdNotMatch.style.diplay = "none";
                            msgPwdNotMatch.innerHTML = "";
                        }
                    }
                    msgPwdMatchObjDiv.innerHTML = "Password Matches";
                }else{
                    msgPwdMatchObjDiv.innerHTML = "";
                }
            }
        }
    }
</script>
<%
            regExtEvalCommMemberDtBean = null;
            regExtEvalCommMemberSrBean = null;
%>