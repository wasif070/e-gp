<%-- 
    Document   : PromisOverallReportPDF
    Created on : Mar 31, 2012, 11:19:41 AM
    Author     : shreyansh.shah
--%>


<%@page import="com.cptu.egp.eps.model.table.TblFinancialYear"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.FinancialYearService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    FinancialYearService financialYearService = (FinancialYearService) AppContext.getSpringBean("financialYearService");
                                        int userid = 0;

                    HttpSession hs = request.getSession();

                    if(hs != null)
                        {
                    if (hs.getAttribute("userId") != null) {
                        userid = Integer.parseInt(hs.getAttribute("userId").toString());
                    }
                    }
                    else{
                        userid = Integer.parseInt(request.getParameter("userId").toString());
                        }

                    boolean isPDF = false;
                    if (request.getParameter("isPDF") != null) {
                        isPDF = true;
                    }
                    String Fyear = request.getParameter("fyear");
                    String PType = request.getParameter("ptype");
                    String DeptId = request.getParameter("deptid");
                    String Officeid = request.getParameter("officeid");
                    String PEID = "1";
                    String PageNo = request.getParameter("pageNo");
                    String Size = request.getParameter("size");
                    String districtId = request.getParameter("districtId");
                    String indiCategory = request.getParameter("indiCategory");
                    String quater = request.getParameter("quater");
                    String projectId = request.getParameter("projectId");
        %>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Overall Procurement Performance Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/lang/en.js"></script>
             <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script type="text/javascript">
            /* Call Print function */

            $(document).ready(function() {
                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });
            function printElem(options){
                //$('#titleDiv').show();
                $('#print_div').printElement(options);
                //$('#titleDiv').hide();
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
             <%if(!isPDF){%>
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <%}%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
               
                        <div id="print_div">
                <div class="tableHead_1 t_space" id="getSubHeading">
                  Overall Procurement Performance Report
                </div>
                <div class="tabPanelArea_1">
                    <table width="100%" cellspacing="0" class="tableList_1" id="resultTable">
                        <tr id="subtableheading">

                        </tr>

                    </table>
                    <div align="center">
                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" name="size" id="size" value="1000"/>
                    </div>
                </div>
            </div>
                <div>&nbsp;</div>
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <%if(!isPDF){%>
            <%@include file="../resources/common/Bottom.jsp" %>
            <%}%>
            <!--Dashboard Footer End-->

        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
   
    
    <script type="text/javascript">
        function changeTab(tabNo){
            loadTenderTableone();
        }

    </script>
  
    <!-- AJAX Grid Functions Start -->
    <script type="text/javascript">
        $(function() {
            var count;
            $('#btnSearch').click(function() {
                loadTenderTable();
              });
        });
    </script>
   

    <script type="text/javascript">
        function getTableHeading()
        {
                tableheadings = '<th width=\"10%\" class=\"t-align-center\">SN</th>'+
                    '<th width=\"25%\" class=\"t-align-center\">Indicator Category</th>'+
                    '<th width=\"30%\" class=\"t-align-center\">Process Indicator</th>'+
                    '<th width=\"40%\" class=\"t-align-center\">Description</th>'+
                    '<th width=\"20%\" class=\"t-align-center\">Result</th>'+
                    '<th width=\"30%\" class=\"t-align-center\">Total Tender</th>'
                return tableheadings;

        }
        function loadTenderTableone()
        {
            $.post("<%=request.getContextPath()%>/PromiseReportServlet", {
                fyear :$("#statusTab").val(),
                ptype: "Ministry",
                deptid:$("#statusTab").val(),
                officeid: $("#status").val(),
                //PEId : $("#PEId").val(),
                pageNo: $("#pageNo").val(),
                size: $("#size").val(),
                isPDF:'<%=isPDF%>',
                districtId:'<%=districtId%>',
                ctgIndicator:'<%=indiCategory%>'
            },
            function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);


                if($('#noRecordFound').attr('value') == "noRecordFound"){
                    $('#pagination').hide();
                }else{
                    $('#pagination').show();
                }
                chkdisble($("#pageNo").val());
                if($("#totalPages").val() == 1){
                    $('#btnNext').attr("disabled", "true");
                    $('#btnLast').attr("disabled", "true");
                }else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnLast').removeAttr("disabled");
                }
                $("#pageTot").html($("#totalPages").val());
                $("#pageNoTot").html($("#pageNo").val());
                $('#resultDiv').show();

                var counter = $('#cntTenBrief').val();
                for(var i=0;i<counter;i++){
                    if($('#tenderBrief_'+i).html() != null){
                        //alert($('#tenderBrief_'+i).html());
                        //alert($('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, ''));
                        var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                        var temp1 = $('#tenderBrief_'+i).html();
                        if(temp.length > 250){
                            temp = temp1.substr(0, 250);
                            $('#tenderBrief_'+i).html(temp+'...');
                        }
                    }
                }
            });
        }

        function loadTenderTable()
        {
            //alert("Indicatror id :"+);
         //   var Heading = getMainHeading();
            var deptid = 0;
            if($("#txtdepartmentid").val() != "")
            {
                deptid = $("#txtdepartmentid").val();
            }
             var subtableheading = getTableHeading();
            $("#subtableheading").html(subtableheading);
            $.post("<%=request.getContextPath()%>/PromiseReportServlet", {
                //                fyear :$("#statusTab").val(),
                //                ptype: "Ministry",
                //                deptid:$("#statusTab").val(),
                //                officeid: $("#status").val(),
                //                pageNo: $("#pageNo").val(),
                //                size: $("#size").val()
                funName : "OverallPromiseReport",
                fyear :'<%=Fyear%>',
                ptype: '<%=PType%>',
                deptid: '<%=DeptId%>',
                officeid:'<%=Officeid%>',
                PEId : '<%=PEID%>',
                pageNo:'<%=PageNo%>',
                size:'<%=Size%>',
                isPDF:'<%=isPDF%>',
                districtId:'<%=districtId%>',
                ctgIndicator:'<%=indiCategory%>',
                quater:'<%=quater%>',
                projectId:'<%=projectId%>'
            },
            function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);
                //                if($('#noRecordFound').attr('value') == "noRecordFound"){
                //                    $('#pagination').hide();
                //                }else{
                //                    $('#pagination').show();
                //                }
                //                chkdisble($("#pageNo").val());
                //                if($("#totalPages").val() == 1){
                //                    $('#btnNext').attr("disabled", "true");
                //                    $('#btnLast').attr("disabled", "true");
                //                }else{
                //                    $('#btnNext').removeAttr("disabled");
                //                    $('#btnLast').removeAttr("disabled");
                //                }
                //                $("#pageTot").html($("#totalPages").val());
                //                $("#pageNoTot").html($("#pageNo").val());
                //                $('#resultDiv').show();

                //                var counter = $('#cntTenBrief').val();
                //                for(var i=0;i<counter;i++){
                //                    var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                //                    var temp1 = $('#tenderBrief_'+i).html();
                //                    if(temp.length > 250){
                //                        temp = temp1.substr(0, 250);
                //                        $('#tenderBrief_'+i).html(temp+'...');
                //                    }
                //                    //$('#tenderBrief_'+i).attr('style', 'width: 20px;');
                //                }
            });
        }
    </script>
    <%--<script type="text/javascript">
function loadListingTable()
{
    $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {funName: "MyTenders",action: "defualt",pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
        $('#resultTable').find("tr:gt(0)").remove();
        $('#resultTable tr:last').after(j);
    });
}

        </script>--%>
    <script type="text/javascript">
        $(function() {
            $('#btnFirst').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);

                if(totalPages>0 && $('#pageNo').val()!="1")
                {
                    $('#pageNo').val("1");
                    loadTenderTable();
                    $('#dispPage').val("1");
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnLast').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);
                if(totalPages>0)
                {
                    $('#pageNo').val(totalPages);
                    loadTenderTable();
                    $('#dispPage').val(totalPages);
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnNext').click(function() {
                var pageNo=parseInt($('#pageNo').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo <= totalPages) {
                    $('#pageNo').val(Number(pageNo)+1);
                    loadTenderTable();
                    $('#dispPage').val(Number(pageNo)+1);
                    $('#btnPrevious').removeAttr("disabled");
                }
            });
        });

    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnPrevious').click(function() {
                var pageNo=$('#pageNo').val();

                if(parseInt(pageNo, 10) > 1)
                {
                    $('#pageNo').val(Number(pageNo) - 1);
                    loadTenderTable();
                    $('#dispPage').val(Number(pageNo) - 1);
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnGoto').click(function() {
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTenderTable();
                        $('#dispPage').val(Number(pageNo));
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                        if(parseInt($('#pageNo').val(), 10) > 1)
                            $('#btnPrevious').removeAttr("disabled");
                    }
                }
                loadTenderTable();
            });
        });
    </script>
    <!-- AJAX Grid Finish-->
    <script type="text/javascript">
        loadTenderTable();
    </script>
    <script type="text/javascript">
        function showHide()
        {
            if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='- Collapse'){
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Expand';
            }else{
                document.getElementById('tblSearchBox').style.display = 'table';
                document.getElementById('collExp').innerHTML = '- Collapse';
            }
        }

        function checkKeyGoTo(e)
        {
            var keyValue = (window.event)? e.keyCode : e.which;
            if(keyValue == 13){
                //Validate();
                $(function() {
                    //$('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            //loadTable();
                            $('#btnGoto').click();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
                //});
            }
        }
    </script>
</html>