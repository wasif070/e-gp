<%-- 
    Document   : ViewProjectUserFP
    Created on : Nov 15, 2010, 7:34:30 PM
    Author     : parag
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="projSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectSrBean" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.List,com.cptu.egp.eps.dao.storedprocedure.CommonAppData" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPProjectRolesReturn,com.cptu.egp.eps.model.table.TblProcurementMethod" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPProjectFPReturn"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPProjectDetailReturn,java.util.StringTokenizer" %>
<%
           
    int projectId=1;
    int userId=0;

    List<TblProcurementMethod>  procurementMethod = projSrUser.getProcurementMethods();

    if(request.getParameter("userId") != null){
        userId= Integer.parseInt(request.getParameter("userId"));
    }
    if(request.getParameter("projectId") != null){
        projectId= Integer.parseInt(request.getParameter("projectId"));
    }

    SPProjectDetailReturn pProjectDetailReturns =null;
    SPProjectRolesReturn userWiseList = null;

    pProjectDetailReturns = projSrUser.getpProjectDetailReturns(projectId).get(0);

    
    userWiseList = projSrUser.getUserRolesReturns(projectId,userId).get(0);

    List<SPProjectFPReturn> pProjectFPReturns = projSrUser.getpProjectFPReturns(userId,projectId);
%>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - Govt User Financial Role Creation</title>
        <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%--<jsp:include page="../resources/common/AfterLoginTop.jsp" ></jsp:include>--%>
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%--<jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>--%>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1"><div class="pageHead_1">View Financial Power</div>
                                        <%-- <div class="pageHead_1">Create User</div>--%>
                                        <%--<div class="pageHead_1">Assign Financial Power</div>--%>

                                        <div id="successMsg" class="responseMsg successMsg" style="display:none"></div>
                                        <div id="errMsg" class="responseMsg errorMsg" style="display:none"></div>
                                        <%-- <div class="responseMsg noticeMsg">Notification Msg <a href="#">Link</a></div>--%>

                                        <form name="frmFinance" id="frmFinance"  method="post" action="../GovtUserSrBean?action=createGovtFinanceRole">
                                            <div>&nbsp;&nbsp;</div>
                                            <table width="100%" id="table0" cellspacing="0" class="tableList_1 t_space">
                                                <tr id="tr">
                                                    <th class="t-align-left">Procurement Method</th>
                                                    <th class="t-align-left">Operator</th>
                                                    <th class="t-align-left">Amount In Million (In Nu. )</th>
                                                </tr>
                                                <%
                                                    int count = 0;
                                                    if (pProjectFPReturns != null){
                                                        if (pProjectFPReturns.size() > 0) {
                                                            for (int iP = 0; iP < pProjectFPReturns.size(); iP++) {
                                                                
                                                                count++;
                                                %>
                                                <tr>                                                    
                                                    <td id="td<%=count%>_1" class="t-align-left">
                                                        <%out.print(pProjectFPReturns.get(iP).getProcurementmethodid());%>
                                                    </td>
                                                    <td  id="td<%=count%>_2" class="t-align-left">
                                                        <%String operation="";
                                                            if("lt".equalsIgnoreCase(pProjectFPReturns.get(iP).getOperation())){
                                                                operation = "<";
                                                             }
                                                             else if("gt".equalsIgnoreCase(pProjectFPReturns.get(iP).getOperation())){
                                                                 operation = ">";
                                                             }
                                                             else if("lt=".equalsIgnoreCase(pProjectFPReturns.get(iP).getOperation())){
                                                                 operation = "<=";
                                                             }
                                                             else if("gt=".equalsIgnoreCase(pProjectFPReturns.get(iP).getOperation())){
                                                                 operation = ">=";
                                                             }
                                                            else if("=".equalsIgnoreCase(pProjectFPReturns.get(iP).getOperation())){
                                                                operation = "=";
                                                             }
                                                             out.print(""+operation);
                                                         %>
                                                    </td>
                                                    <td id="td<%=count%>_3" class="t-align-left">
                                                        <%=String.format("%.2f",pProjectFPReturns.get(iP).getAmount())%>
                                                    </td>
                                                </tr>

                                                <%     }
                                                        } else {
                                                            out.print("<tr> <td class='t-align-left' colspan='3' > No Records Found. </td></tr>");
                                                        }
                                                        } else {
                                                            out.print("<tr> <td class='t-align-left' colspan='3' > No Records Found. </td></tr>");
                                                        }
                                                %>
                                                </table>
                                                <!-- Buttons -->
                                                <table align="center" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                    <tr>

                                                    </tr>
                                                </table>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <div>&nbsp;</div>

                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                 
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
</html>



