<%-- 
    Document   : SecurityConfig
    Created on : Dec 1, 2010, 2:46:54 PM
    Author     : Naresh.Akurathi
--%>

<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigMasterSrBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
    %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Security Configuration</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="../resources/js/ddlevelsmenu.js">

</script>
</head>
<body>
    <%!
     ConfigMasterSrBean configMasterSrBean = new ConfigMasterSrBean();
     String msg="";
    %>

    <%
            if("Submit".equals(request.getParameter("button"))){

                        int floginatmpt = Integer.parseInt(request.getParameter("faillogin"));
                        String pkiReq = request.getParameter("pkireq");

                        msg = configMasterSrBean.updateConfigMaster(floginatmpt, pkiReq);
                }

    %>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <div class="topHeader">
  <div class="topHeader">
      <%@include file="../resources/common/AfterLoginTop.jsp" %>
   
  </div>
  </div>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="pageHead_1">Security Configuration</div>
  <form action="SecurityConfig.jsp" method="post">
  <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
    <tr>
      <td style="font-style: italic" class="ff t-align-left" colspan="2">Fields Marked with (<span class="mandatory">*</span>) are Mandatory</td>
    </tr>
        <%
         
          TblConfigurationMaster tblConfigurationMaster = new TblConfigurationMaster();
          List l=configMasterSrBean.getConfigMaster();
         
          if(!l.isEmpty() && l!=null){
          Iterator it = l.iterator();
          if(it.hasNext())
              {                    
                    String y="";
                    String n="";
                    tblConfigurationMaster=(TblConfigurationMaster)it.next();
                    if("Yes".equals(tblConfigurationMaster.getIsPkiRequired()))
                        y="checked='true'";
                    if("No".equals(tblConfigurationMaster.getIsPkiRequired()))
                        n="checked='true'";

        %>
	<tr>
      <td class="ff" width="15%">Failed Login attempts  Count : <span class="mandatory">*</span></td>
      <td width="85%"><input name="faillogin" type="text" class="formTxtBox_1" id="textfield" style="width:195px;" value="<%=tblConfigurationMaster.getFailedLoginAttempt()%>"/></td>
    </tr>
      
	<tr>
      <td class="ff">PKI Requires :</td>
      <td>
        <input name="pkireq" type="radio" value="Yes" <%=y%>/> Yes &nbsp;&nbsp;
        <input name="pkireq" type="radio" value="No" <%=n%>/> No
      </td>
    </tr>
        <%
            }

                }
          else
              msg="No Data in ConfigMaster";
        %>
	<tr>
      <td></td>
      <td class="t-align-left">
      	<label class="formBtn_1">
        <input type="submit" name="button" id="button" value="Submit" />
        </label>
      </td>
    </tr>
  </table>
  </form>
        
  <div>&nbsp;</div>
  <div class="pageHead_1" align="center"> <%=msg%></div>
  <!--Dashboard Content Part End-->
  <div class="topHeader">
    <%@include file="../resources/common/Bottom.jsp" %>
  </div>
  
</div>
  <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
</body>
</html>
