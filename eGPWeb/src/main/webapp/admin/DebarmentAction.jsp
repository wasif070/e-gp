<%-- 
    Document   : ReinstateDebarment
    Created on : Aug 9, 2016, 1:56:48 PM
    Author     : NAHID
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentTypes"%>
<%@page import="com.cptu.egp.eps.web.servicebean.InitDebarmentSrBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.dao.storedprocedure.UserApprovalBean"%>
<%@page import="java.util.List,java.util.Calendar,com.cptu.egp.eps.service.serviceimpl.ContentAdminService" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Debarment Process</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript">
            function GetCal(txtname, controlname) {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat: "%d/%m/%Y",
                    onSelect: function () {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            function ReinstateValidate() {

                $(".err").remove();

                var valid = true;
                if ($("#grounds").val() == "") {
                    $("#grounds").parent().append("<div class='err' style='color:red;'>Please enter Reinstate Reasons</div>");
                    valid = false;
                } else {
                    if ($("#grounds").val().length > 2000) {
                        $("#grounds").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters allowed</div>");
                        valid = false;
                    }
                }

                if (!valid) {
                    return false;
                }
            }
            
            function UpdateValidate(){
                $(".err").remove();

                var valid = true;
                var dateCheck = true;

                if ($("#textfield_2").val() == "") {
                    $("#textfield_2").parent().append("<div class='err' style='color:red;'>Please select Debarment From date</div>");
                    dateCheck = false;
                    valid = false;
                }
                else if(!CompareToForGreater($('#hdnCurrDate').val(),$('#textfield_2').val())){
                    $("#textfield_2").parent().append("<div class='err' style='color:red;'>From Date must not be greater than Today</div>");
                    dateCheck = false;
                    valid = false;
                }

                if ($("#textfield_1").val() == "") {
                    $("#textfield_1").parent().append("<div class='err' style='color:red;'>Please select Debarment To date</div>");
                    dateCheck = false;
                    valid = false;
                }else if(!CompareToForGreater($('#textfield_1').val(),$('#hdnCurrDate').val())){
                    $("#textfield_1").parent().append("<div class='err' style='color:red;'>To Date must not be less than Today</div>");
                    dateCheck = false;
                    valid = false;
                }
                
                if (dateCheck) {
                    if(!CompareToForGreater($('#textfield_1').val(),$('#textfield_2').val())){
                        $("#textfield_1").parent().append("<div class='err' style='color:red;'>To Date must be greater than From Date.</div>");
                        return false;
                    }
                }
                
                if (!valid) {
                    return false;
                }
            }
            
            function CompareToForGreater(value,params)
            {

                var mdy = value.split('/')  //Date and month split
                var mdyhr=mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr=mdyp[2].split(' ');


                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                {
                    //alert('Both Date');
                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                }
                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                {
                    //alert('Both DateTime');
                    var mdyhrsec=mdyhr[1].split(':');
                    var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                    var mdyphrsec=mdyphr[1].split(':');
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                }
                else
                {
                    //alert('one Date and One DateTime');
                    var a = mdyhr[1];  //time
                    var b = mdyphr[1]; // time

                    if(a == undefined && b != undefined)
                    {
                        //alert('First Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('Second Date');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                    }
                }

                return Date.parse(date) >= Date.parse(datep);
            }

        </script>
    </head>
    <body>
        <input type="hidden" value="<%=DateUtils.formatStdDate(new java.util.Date())%>" name="currDate" id="hdnCurrDate"/>
        <div class="mainDiv">
            <div class="fixDiv">
                <div class="contentArea_1">

                    <!--Dashboard Header Start-->
                    <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->

                    <%                        
                        String action = request.getParameter("action");
                        String userId = request.getParameter("userId");
                        String category = request.getParameter("category");
                        InitDebarmentSrBean debarmentSrBean = new InitDebarmentSrBean();
                        java.util.List<SPCommonSearchDataMore> list1 = new java.util.ArrayList<SPCommonSearchDataMore>();
                        list1 = debarmentSrBean.getDebaredBiddingPermissionInfo(userId, category);
                    %>
                    <!--            <div class="pageHead_1">Initiate Debarment Process<span style="float:right;"> <a class="action-button-goback" href="DebarmentListing.jsp">Go back</a> </span></div><br/>-->

                    <% 
                        if (request.getParameter("flag") != null) {
                            String flag = request.getParameter("flag");
                            if (flag.equalsIgnoreCase("fail")) {
                                out.println("<div class='responseMsg errorMsg'>Error Initiating Debarment Process</div><br/>");
                            }
                            if (flag.equalsIgnoreCase("success")) {
                                out.println("<div class='responseMsg successMsg'>Debarment Process Successful</div><br/>");
                            }
                        }
                    %>
                    <form action="<%=request.getContextPath()%>/InitDebarment" method="post">
                        <input type="hidden" value="<%
                                   if (action.equalsIgnoreCase("reinstate")) {
                                       out.print("ReinstateDebarment");
                                   } else if (action.equalsIgnoreCase("update")) {
                                       out.print("UpdateDebarment");
                                   }
                               %>" name="action" id="action" />
                        <input type="hidden" value="<%=userId%>" name="userId" id="userId" />
                        <input type="hidden" value="<%=category%>" name="category" id="category" />
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <%
                                if (!list1.isEmpty()) {%>
                            <tr>
                                <td width="16%" class="t-align-left ff">
                                    <%
                                        if (list1.get(0).getFieldName2().equalsIgnoreCase("1")) {
                                            out.print("Individual Name :");
                                        } else {
                                            out.print("Company Name :");
                                        }
                                    %><span class="mandatory">*</span></td>
                                <td width="84%" class="t-align-left">
                                    <%
                                        String name = "";
                                        if (list1.get(0).getFieldName2().equalsIgnoreCase("1")) {
                                            name = list1.get(0).getFieldName11();
                                            if (list1.get(0).getFieldName12() != null) {
                                                name = name + " " + list1.get(0).getFieldName12();
                                            }
                                            if (list1.get(0).getFieldName13() != null) {
                                                name = name + " " + list1.get(0).getFieldName13();
                                            }
                                        } else {
                                            name = list1.get(0).getFieldName10();
                                        }

                                        out.print(name);
                                    %>

                                </td>
                            </tr>   
                            <tr>
                                <td class="t-align-left ff">Email :</td>
                                <td class="t-align-left">
                                    <%
                                        out.print(list1.get(0).getFieldName9());
                                    %>
                                    <input type="hidden" value="<%=list1.get(0).getFieldName9()%>" name="emailId" id="emailId" />
                                </td>
                            </tr>

                            <tr>
                                <td class="t-align-left ff">Debarred Category :</td>
                                <td class="t-align-left">
                                    <%
                                        out.print(list1.get(0).getFieldName1());
                                    %>
                                </td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Debarment Reason :</td>
                                <td class="t-align-left">
                                    <%
                                        out.print(list1.get(0).getFieldName8());
                                    %>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="t-align-left ff">Grounds :</td>
                                <td width="84%" class="t-align-left">
                                    <%
                                        out.print(list1.get(0).getFieldName6());
                                    %>
                                </td>
                            </tr>
                            <%
                                 if (action.equalsIgnoreCase("update")) {
                            %>
                            <tr>
                                <td class="t-align-left ff">Debarred From :<span class="mandatory">*</span></td>
                                <td class="t-align-left">
                                    <input type="text" onblur="return DateBlank(this);" value="<%=DateUtils.formatStdDate(DateUtils.convertStringtoDate(list1.get(0).getFieldName4(), "MMM dd yyyy h:mma"))%>" readonly="readonly" style="width:100px;" id="textfield_2" class="formTxtBox_1" name="textfield_2" onclick="GetCal('textfield_2', 'textfield_2');"/>
                                    <a  title="Calender" href="javascript:void(0);">
                                        <img border="0" onclick="GetCal('textfield_2', 'imgfield_2');" style="vertical-align:middle;" id="imgfield_2" alt="add Date" src="../resources/images/Dashboard/calendarIcn.png">
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Debarred To :<span class="mandatory">*</span></td>
                                <td class="t-align-left">
                                    <input type="text" onblur="return DateBlank(this);" value="<%=DateUtils.formatStdDate(DateUtils.convertStringtoDate(list1.get(0).getFieldName5(), "MMM dd yyyy h:mma"))%>" readonly="readonly" style="width:100px;" id="textfield_1" class="formTxtBox_1" name="textfield_1" onclick="GetCal('textfield_1', 'textfield_1');" />
                                    <a title="Calender" href="javascript:void(0);">
                                        <img border="0" onclick="GetCal('textfield_1', 'imgfield_1');" style="vertical-align:middle;" id="imgfield_1" alt="add Date" src="../resources/images/Dashboard/calendarIcn.png">
                                    </a>
                                </td>
                            </tr>
                            <%}else{%>
                            <tr>
                                <td class="t-align-left ff">Debarred From :</td>
                                <td class="t-align-left">
                                    <%
                                        out.print(DateUtils.formatStdDate(DateUtils.convertStringtoDate(list1.get(0).getFieldName4(), "MMM dd yyyy h:mma")));
                                    %>
                                </td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Debarred To :</td>
                                <td class="t-align-left">
                                    <%
                                        out.print(DateUtils.formatStdDate(DateUtils.convertStringtoDate(list1.get(0).getFieldName5(), "MMM dd yyyy h:mma")));
                                    %>
                                </td>
                            </tr>
                            <%
                                }
                                if (action.equalsIgnoreCase("reinstate")) {
                            %>
                            <tr>
                                <td valign="top" class="t-align-left ff">Reinstatement Reason :<span class="mandatory">*</span></td>
                                <td width="84%" class="t-align-left">
                                    <textarea rows="5" id="grounds" name="grounds" class="formTxtBox_1" style="width:99%;"></textarea>
                                </td>
                            </tr>
                            <%}%>
                            <%}%>

                        </table>

                        <div class="t-align-center t_space">
                            <%
                                if (action.equalsIgnoreCase("reinstate")) {
                            %>
                            <label class="formBtn_1"><input type="submit" name="button3" id="button3" value="Reinstate" onclick="return ReinstateValidate();"  /></label>
                            <%} else if (action.equalsIgnoreCase("update")) {
                            %>
                            <label class="formBtn_1"><input type="submit" name="button4" id="button4" value="Update" onclick="return UpdateValidate();"  /></label>
                            <%}%>
                        </div>
                    </form>
                </div>
                <%@include file="../resources/common/Bottom.jsp" %>
            </div>
            <!--Dashboard Footer End-->
        </div>

    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabDebar");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

