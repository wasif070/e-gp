<%-- 
    Document   : WSDetail
    Created on : Jun 6, 2011, 5:14:26 AM
    Author     : visitor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New Web Service</title>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>        
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <script type="text/javascript">
             $(document).ready(function() {
                  $("#formAddWebSerice").validate({
                       rules: {
                           txtWebServiceName: {spacevalidate: true, requiredWithoutSpace: true , alphaName:true, maxlength:100 }
                       },
                        messages: {
                             txtWebServiceName: { spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                                    requiredWithoutSpace: "<div class='reqF_1'>Please Enter Web Service Name</div>",
                                    alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only </div>",
                                    maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"}
                        },
                        errorPlacement:function(error ,element){
                                error.insertAfter(element);
                        }
                  });
             });
              function numeric(value) {
                return /^\d+$/.test(value);
            }
            function checkData(){
                //var isMail=false;
                var retval;
                var isForEdit = 'false';
                if(isForEdit == "false"){
                    if(document.getElementById("txtWebServiceName").value !="" && document.getElementById("txtWebServiceName").innerHTML != "OK"){
                        return false;
                    }
                }
                if($('#formAddWebSerice').valid()){
                    //$('#btnSubmit').attr("disabled", true);
                    document.getElementById("formAddWebSerice").action="/WebServiceControlServlet?action=add";
                    document.getElementById("formAddWebSerice").submit();
                    //return true;
                }
            }
        </script>
        <script type="text/javascript">
            function passMsg(){
                jAlert("Empty Values not allowed.","Web Service Creation", function(RetVal) {
                });
                return false;
            }
        </script>
        <% //String userType = "config";%>
        <%
                    StringBuilder userType = new StringBuilder();
                    if (request.getParameter("userType") != null) {
                        if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                            userType.append(request.getParameter("userType"));
                        } else {
                            userType.append("org");
                        }
                    } else {
                        userType.append("org");
                    }
        %>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType.toString()%>" ></jsp:include>
                        <td class="contentArea">
                            <div>
                                <div class="pageHead_1">
                                    Adding a New Web-Service
                                    <span style="float: right; text-align: right;">
                                        <a class="action-button-goback" href="WSList.jsp?mode=view">Go back</a>
                                    </span>
                                </div>
                            </div>
                            <form id="formAddWebSerice" method="post" action="/WebServiceControlServlet?action=add" >
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr></tr>
                                    <tr>
                                        <td class="ff" width="20%">
                                            Web-Service Name : <span>*</span>
                                        </td>
                                        <td width="80%">
                                            <input name="txtWebServiceName" type="text" class="formTxtBox_1" id="txtWebServiceName" style="width:300px;" />
                                            <br/>
                                            <span id="WSname_warning" style="color:red"></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">
                                            Web-Service Description :
                                        </td>
                                        <td width="80%">
                                            <textarea name="txtWebServiceDesc" rows="3" class="formTxtBox_1" id="txtWebServiceDesc" style="width:300px;" ></textarea>
                                            <br/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">
                                            Status :
                                        </td>
                                        <td>
                                            <select name="selectWsStatus" class="formTxtBox_1" id="selectWsStatus" style="width:auto;">
                                                <option value="Y" selected>Active</option>
                                                <option value="N">DeActivate</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">
                                            Authentication :
                                        </td>
                                        <td>
                                            <select name="selectWsAuthentication" class="formTxtBox_1" id="selectWsAuthentication" style="width:auto;">
                                                <option value="Y" selected>Yes</option>
                                                <option value="N">No</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="t-align-left">
                                            <label class="formBtn_1">
                                                <input type="submit" name="submitWebSerivce" id="submitWebSerivce" value="Submit" />
                                            </label>
                                        </td>
                                    </tr>
                                </table>                                                             
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
    <jsp:include page="../resources/common/Bottom.jsp"></jsp:include>
</html>
