<%-- 
    Document   : DelegationDesignationRank
    Created on : Jul 8, 2015, 1:34:19 PM
    Author     : Istiak (Dohatec)
--%>

<jsp:useBean id="oFinancialDelegationSrBean" class="com.cptu.egp.eps.web.servicebean.FinancialDelegationSrBean"/>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "No-store, No-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "No-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Delegation Designation Ranks Configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <%
            String flag = request.getParameter("f");

            String showTable = "";
            if (!flag.equalsIgnoreCase("a")) {
                showTable = oFinancialDelegationSrBean.showDelegationRanks(flag);
            }

            String procRole = "";
            if (!flag.equalsIgnoreCase("v")) {
                procRole = oFinancialDelegationSrBean.getProcurementRoleDropDown();
            }

        %>

        <script type="text/javascript">
            $(document).ready(function () {
            <%if (flag.equalsIgnoreCase("a")) {%>
                AddRows();
            <%} else {%>
                ShowTable();
            <%}%>
            });

            function ShowTable() {
                $('#tbodyData').append($('#showTable').val());
            }

            <%if (!flag.equalsIgnoreCase("v")) {%>

            function AddRows() {
                var latestRank = Math.abs($('input[type="hidden"][name="Count"]:last').val());
                if (isNaN(latestRank))
                    latestRank = 0;

                var lastId = Math.abs($('input[type="checkbox"][name="RanksId"]:last').val());
                var newId = 0;
                if (isNaN(lastId))
                    newId = 1;
                else
                    newId = lastId + 1;

                $('#tbodyData').append("<tr>" +
                        "<td class='t-align-center' width='5%'>" +
                        "<input id='chk_" + newId + "' name='ChkRanksId' type='checkbox' value='n' />" +
                        "<input type='hidden' name='RanksId' id='RanksId_" + (latestRank + 1) + "' value='" + (latestRank + 1) + "'>" +
                        "<input type='hidden' name='NewOldEdit' id='NewOldEdit_" + newId + "' value='New'>" +
                        "</td>" +
                        "<td class='t-align-center' width='10%'>" + (latestRank + 1) +
                        "<input type='hidden' name='Count' id='Count_" + (latestRank + 1) + "' value='" + (latestRank + 1) + "'>" +
                        "</td>" +
                        "<td class='t-align-center' width='10%'>" +
                        "<select name='Designation' class='formTxtBox_1' id='Designation_" + newId + "' style='width:100px;'>" +
                        //"<option value='' selected='selected'>-- Select --</option>" +
                        $('#procRole').val() +
                        "</select>" +
                        "</td>" +
                        "</tr>");
            }

            <%if (flag.equalsIgnoreCase("e")) {%>
            function DeleteRows() {

                var checkedCount = $('input[type="checkbox"][name="ChkRanksId"]:checked').length;

                if (checkedCount > 0) {
                    var id = "";
                    $('input[type="checkbox"][name="ChkRanksId"]:checked').each(function () {
                        if ($(this).val() != 'n')
                            id = $(this).val() + "," + id;
                    });
                    id = id.substr(0, (id.length - 1));

                    if (confirm("Are You Sure You want to Delete?")) {
                        if (id.length > 0) {
                            $.post("<%=request.getContextPath()%>/FinancialDelegationServlet", {action: "DeleteRanks", rowNo: id}, function (j) {
                                if (j == "false") {
                                    alert("Approving Authority delete failed!!");
                                } else {
                                    $('input[type="checkbox"][name="ChkRanksId"]:checked').each(function () {
                                        $(this).closest('tr').remove();
                                    });

                                    alert("Approving Authority delete successfully.");
                                }
                            });
                        } else if (id.length == 0 && checkedCount > 0) {
                            $('input[type="checkbox"][name="ChkRanksId"]:checked').each(function () {
                                $(this).closest('tr').remove();
                            });

                            alert("Approving Authority delete successfully.");
                        }
                    }
                } else {
                    alert("Please Select checkbox first!!!");
                }
            }
            <%} else {%>
            function DeleteRows() {

                var checkedCount = $('input[type="checkbox"][name="ChkRanksId"]:checked').length;

                if (checkedCount > 0) {
                    if (confirm("Are You Sure You want to Delete?")) {
                        $('input[type="checkbox"][name="ChkRanksId"]:checked').each(function () {
                            $(this).closest('tr').remove();
                        });
                    }
                } else {
                    alert("Please Select checkbox first!!!");
                }
            }
            <%}%>

            function NewOrEdit(id) {
                if ($('#Designation_' + id).val() != $('#Old_' + id).val()) {
                    if ($('#NewOldEdit_' + id).val() == "Old")
                        $('#NewOldEdit_' + id).val("Edit");
                }
            }

            function Validate() {
                var submit = true;
                var designation = [];
                var duplicate = false;

                if (submit) {
                    $('select[name="Designation"]').each(function (index, val) {
                        for (var i = 0; i < designation.length; i++) {
                            if (designation[i] == $(val).val()) {
                                duplicate = true;
                                alert("You cannot select same Approving Authority more than one.");
                                break;
                            }
                        }

                        if (duplicate) {
                            submit = false;
                            return false;
                        } else {
                            designation[index] = $(val).val();
                        }
                    });
                }

                if (submit) {
                    $('input[name="NewOldEdit"]').each(function () {
                        if ($(this).val() == 'Old') {
                            submit = false;
                        } else if ($(this).val() == 'New' || $(this).val() == 'Edit') {
                            submit = true;
                            return false;
                        }
                    });

                    if (!submit) {
                        alert('You did not change or add any Approving Authority .');
                    }
                }

                return submit;
            }
            <%}%>
        </script>

    </head>

    <body>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                        <td class="contentArea">
                            <div class="pageHead_1">Delegation Designation Ranks</div>

                        <%if (!flag.equalsIgnoreCase("v")) {%>
                        <div style="font-style: italic" class="t-align-left t_space"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>
                        <div align="right" class="t_space b_space">
                            <a href="javascript:void(0);" class="action-button-add" id="addRule" onclick="AddRows()">Add Rule</a>
                            <a href="javascript:void(0);" class="action-button-delete" id="removeRule" onclick="DeleteRows()">Remove Rule</a>
                        </div>
                        <%}%>

                        <br/>
                        <div >&nbsp;
                            <%if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("y")) {%>
                            <div class='responseMsg successMsg'>Data save successfully</div>
                            <%}
                                if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("n")) {%>
                            <div class='responseMsg errorMsg'>Data save failed</div>
                            <%}%>
                        </div>
                        <br/>

                        <form action="<%=request.getContextPath()%>/FinancialDelegationServlet?action=RanksAddUpdate" method="post">

                            <table class="tableList_1" cellspacing="0" width="100%" id="resultTable">
                                <tbody id="tbodyData">
                                    <tr>
                                        <%if (!flag.equalsIgnoreCase("v")) {%>
                                        <th class="t-align-center" width="5%">Select</th>
                                            <%}%>
                                        <th class="t-align-center" width="10%">Rank</th>
                                        <th class="t-align-center" width="10%">Approving Authority <%if (!flag.equalsIgnoreCase("v")) {%>(<span class="mandatory" style="color: red;">*</span>)<%}%></th>
                                    </tr>
                                </tbody>
                            </table><div>&nbsp;</div>

                            <%if (!flag.equalsIgnoreCase("v")) {%>
                            <div align="center">
                                <span class="formBtn_1" >
                                    <input name="btnSubmit" align="center" id="btnSubmit" value="Submit" type="submit" onclick="return Validate();">
                                </span>
                            </div>
                            <%}%>
                            <div>&nbsp;</div>
                        </form>

                        <input type="hidden" id="showTable" value="<%=showTable%>">
                        <input type="hidden" id="procRole" value="<%=procRole%>">

                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
            <script>
            <%if (flag.equalsIgnoreCase("v")) {%>
                var obj = document.getElementById('designationRankView');
                if (obj != null) {
                    if (obj.innerHTML == 'View') {
                        obj.setAttribute('class', 'selected');
                    }
                }
            <%} else {%>
                var obj = document.getElementById('designationRankEdit');
                if (obj != null) {
                    if (obj.innerHTML == 'Edit') {
                        obj.setAttribute('class', 'selected');
                    }
                }
            <%}%>

                var headSel_Obj = document.getElementById("headTabConfig");
                if (headSel_Obj != null) {
                    headSel_Obj.setAttribute("class", "selected");
                }
        </script>
    </body>
</html>
