<%-- 
    Document   : EditNavigation
    Created on : Nov 4, 2010, 12:26:18 PM
    Author     : Taher
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page  import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="java.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
     <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        String strParamUID = (request.getParameter("uId") != null?request.getParameter("uId"):"");
        String strParamTID = (request.getParameter("tId") != null?request.getParameter("tId"):"");
        String strParamS = (request.getParameter("s") != null?request.getParameter("s"):"");
        String strParamSta = (request.getParameter("status") != null?request.getParameter("status"):"");
        String status = "";
        if(!strParamSta.isEmpty())
        {
            status = strParamSta;
        }
%>
    <body>
        <div class="stepWiz_1 t_space">
            <ul>
                <%
                            UserRegisterService userRegisterService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
                            List<String> pageList = userRegisterService.pageNavigationList(Integer.parseInt(strParamUID));//
                            String pageName=request.getRequestURI().toString().split("/")[request.getRequestURI().toString().split("/").length-1];                            
                            String payId="0";
                            TenderCommonService service = (TenderCommonService)AppContext.getSpringBean("TenderCommonService");
                            List<SPTenderCommonData> peBUEmail = service.returndata("getEmailIdfromUserId",strParamUID,null);
                            List<SPTenderCommonData> list = service.returndata("SearchEmailForContentAdmin",peBUEmail.get(0).getFieldName1(), null);
                            if(!list.isEmpty()){
                                if(!list.get(0).getFieldName1().equals("0") && false){//to eliminate payment page for bhutan egp
                                     payId=list.get(0).getFieldName1();                                     
                                 %>
                                    <li <%if(pageName.equals("TendererPaymentDetail.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererPaymentDetail.jsp")){%><a href="TendererPaymentDetail.jsp?uId=<%=strParamUID%>&tId=<%=strParamTID%>&cId=<%=request.getParameter("cId")%>&jv=no&payId=<%=payId%>&s=<%=strParamS%>"><%}%>Bidder/Consultant Payment Details<%if(!pageName.equals("TendererPaymentDetail.jsp")){%></a><%}%></li>
                                 <%
                                }
                            }     
                            if (pageList.size() == 3) {
                %>
                <li <%if(pageName.equals("TendererRegDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererRegDetails.jsp")){%><a href="TendererRegDetails.jsp?uId=<%=strParamUID%>&tId=<%=strParamTID%>&cId=<%=request.getParameter("cId")%>&jv=no&payId=<%=payId%>&s=<%=strParamS%>"><%}%>Individual Consultant Registration Details<%if(!pageName.equals("TendererRegDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererDetails.jsp")){%><a href="TendererDetails.jsp?uId=<%=strParamUID%>&tId=<%=strParamTID%>&cId=<%=request.getParameter("cId")%>&jv=no&payId=<%=payId%>&s=<%=strParamS%>"><%}%>Individual Consultant Details<%if(!pageName.equals("TendererDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererDocs.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererDocs.jsp")){%><a href="TendererDocs.jsp?uId=<%=strParamUID%>&tId=<%=strParamTID%>&cId=<%=request.getParameter("cId")%>&jv=no&payId=<%=payId%>&s=<%=strParamS%>"><%}%>Individual Consultant Documents<%if(!pageName.equals("TendererDocs.jsp")){%></a><%}%></li>
                <%} else if (pageList.size() == 4) {
                %>
                <li <%if(pageName.equals("TendererRegDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererRegDetails.jsp")){%><a href="TendererRegDetails.jsp?uId=<%=strParamUID%>&tId=<%=strParamTID%>&cId=<%=request.getParameter("cId")%>&jv=n&payId=<%=payId%>&s=<%=strParamS%>&status=<%=status%>"><%}%>Bidder/Consultant Registration Details<%if(!pageName.equals("TendererRegDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererCompDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererCompDetails.jsp")){%><a href="TendererCompDetails.jsp?uId=<%=strParamUID%>&tId=<%=strParamTID%>&cId=<%=request.getParameter("cId")%>&jv=n&payId=<%=payId%>&s=<%=strParamS%>&status=<%=status%>"><%}%>Bidder/Consultant Company Details<%if(!pageName.equals("TendererCompDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererDetails.jsp")){%><a href="TendererDetails.jsp?uId=<%=strParamUID%>&tId=<%=strParamTID%>&cId=<%=request.getParameter("cId")%>&jv=n&payId=<%=payId%>&s=<%=strParamS%>&status=<%=status%>"><%}%>Bidder/Consultant Details<%if(!pageName.equals("TendererDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererDocs.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererDocs.jsp")){%><a href="TendererDocs.jsp?uId=<%=strParamUID%>&tId=<%=strParamTID%>&cId=<%=request.getParameter("cId")%>&jv=n&payId=<%=payId%>&s=<%=strParamS%>&status=<%=status%>"><%}%>Bidder/Consultant Documents<%if(!pageName.equals("TendererDocs.jsp")){%></a><%}%></li>
                <%} else if (pageList.size() == 5) {
                %>
                <li <%if(pageName.equals("TendererRegDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererRegDetails.jsp")){%><a href="TendererRegDetails.jsp?uId=<%=strParamUID%>&tId=<%=strParamTID%>&cId=<%=request.getParameter("cId")%>&jv=y&payId=<%=payId%>&s=<%=strParamS%>"><%}%>Bidder/Consultant Registration Details<%if(!pageName.equals("TendererRegDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererCompDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererCompDetails.jsp")){%><a href="TendererCompDetails.jsp?uId=<%=strParamUID%>&tId=<%=strParamTID%>&cId=<%=request.getParameter("cId")%>&jv=y&payId=<%=payId%>&s=<%=strParamS%>"><%}%>Bidder/Consultant Company Details<%if(!pageName.equals("TendererCompDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererDetails.jsp")){%><a href="TendererDetails.jsp?uId=<%=strParamUID%>&tId=<%=strParamTID%>&cId=<%=request.getParameter("cId")%>&jv=y&payId=<%=payId%>&s=<%=strParamS%>"><%}%>Bidder/Consultant Details<%if(!pageName.equals("TendererDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererJVDetails.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererJVDetails.jsp")){%><a href="TendererJVDetails.jsp?uId=<%=strParamUID%>&tId=<%=strParamTID%>&cId=<%=request.getParameter("cId")%>&jv=y&payId=<%=payId%>&s=<%=strParamS%>"><%}%>Bidder/Consultant JVCA Details<%if(!pageName.equals("TendererJVDetails.jsp")){%></a><%}%></li>
                <li <%if(pageName.equals("TendererDocs.jsp")){%>class="sMenu"<%}%>><%if(!pageName.equals("TendererDocs.jsp")){%><a href="TendererDocs.jsp?uId=<%=strParamUID%>&tId=<%=strParamTID%>&cId=<%=request.getParameter("cId")%>&jv=y&payId=<%=payId%>&s=<%=strParamS%>"><%}%>Bidder/Consultant Documents<%if(!pageName.equals("TendererDocs.jsp")){%></a><%}%></li>
                <%}
                %>
            </ul>
        </div>        
    </body>
</html>
