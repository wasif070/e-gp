<%--
    Document   : Admin
    Created on : Oct 21, 2010, 6:13:00 PM
    Author     : rishita
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Admin</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<!--        <script type="text/javascript" src="Include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="/resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <div class="pageHead_1">Create Organization</div>
                            <!--Page Content Start-->
                            <form name="frmOrganizationAdmin" method="post">
                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td class="ff" width="200">PA</td>
                                        <td><input class="formTxtBox_1" id="txtOrganization" name="Organization" style="width: 200px;" type="text" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Unique e-mail ID</td>
                                        <td><input class="formTxtBox_1" id="txtEmailId" name="EmailId" style="width: 200px;" type="text" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Full Name<span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtFullName" name="FullName" style="width: 200px;" type="text" maxlength="200"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">National Id<span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtNationalId" name="NationalId" style="width: 200px;" type="text" readonly="true"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Phone Number</td>
                                        <td><input class="formTxtBox_1" id="txtPhoneNumber" name="PhoneNumber" style="width: 200px;" type="text" maxlength="20"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Mobile Number<span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtMobileNumber" style="width: 200px;" name="MobileNumber" type="text" maxlength="20"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2" class="formBtn_1" align="center"><input id="btnUpdate" name="Update" value="Update" type="button"/></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
