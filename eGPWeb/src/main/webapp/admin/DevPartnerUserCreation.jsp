<%-- 
    Document   : DevPartnerUserCreation
    Created on : Oct 23, 2010, 7:14:33 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - Development Partner User Role Creation</title>
        <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
       <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%@include  file="/resources/common/Top.jsp"%>
            <!--Dashboard Header End-->

            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Create Development Partner User</div>
            <form method="POST" id="frmDevUser" action="">
                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                    <tr>
                        <td class="ff">Select Development Partner : <span>*</span></td>
                        <td><select name="office"  id="cmbOffice" class="formTxtBox_1" style="width: 208px;">
                                <option selected="selected">-- Select Partner --</option>
                                <option>World Bank</option>
                                <option>ADB</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td class="ff">Unique e-mail ID : <span>*</span></td>
                        <td><input name="mailId" id="txtMail" type="text" class="formTxtBox_1"  style="width:200px;" /></td>
                    </tr>
                    <tr>
                        <td class="ff">Password : <span>*</span></td>
                        <td><input name="password" id="txtPass" type="text" class="formTxtBox_1" style="width:200px;" maxlength="25"/></td>
                    </tr>
                    <tr>
                        <td class="ff">Confirm Password : <span>*</span></td>
                        <td><input name="confPassword" id="txtConfPass" type="text" class="formTxtBox_1" style="width:200px;"/></td>
                    </tr>

                    <tr>
                        <td class="ff">Full Name : <span>*</span></td>
                        <td><input name="fullName" id="txtaFullName" type="text" class="formTxtBox_1" style="width:200px;" /></td>
                    </tr>
                    <tr>
                        <td class="ff">Name in Bangla : </td>
                        <td><input name="bngName" id="txtBngName" type="text" class="formTxtBox_1" style="width:200px;" /></td>
                    </tr>
                    <tr>
                        <td class="ff">National Id : <span>*</span></td>
                        <td><input name="ntnlId" id="txtNtnlId" type="text" class="formTxtBox_1" style="width:200px;" maxlength="13"/></td>
                    </tr>
                    <tr>
                        <td class="ff">Select Designataion : <span>*</span></td>
                        <td><select name="designation"  id="cmbDesignation" class="formTxtBox_1" style="width: 208px">
                                <option selected="selected">-- Select Designation --</option>
                                <option>Admin</option>
                                <option>Officer</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td class="ff">Phone No : <span>*</span></td>
                        <td><input name="phoneNo" id="txtPhone" type="text" class="formTxtBox_1" style="width:200px;" /></td>
                    </tr>
                    <tr>
                        <td class="ff">Mobile No : <span>*</span></td>
                        <td><input name="mobileNo" id="txtMob" type="text" class="formTxtBox_1" style="width:200px;" maxlength="16"/></td>
                    </tr>
                    <tr>
                        <td class="ff">fax No : <span>*</span></td>
                        <td><input name="faxNo" type="text" class="formTxtBox_1" id="txtFax" style="width:200px;" /></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><label class="formBtn_1">
                                <input type="submit" name="btnSubmit" id="btnSubmit" value="Submit" />
                            </label>
                        </td>
                    </tr>
                </table>
            </form>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@include file="/resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
