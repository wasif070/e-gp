<%-- 
    Document   : CalendarDetail
    Created on : Oct 30, 2010, 7:52:19 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Register Organization Admin</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("hdnUserType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                            userType.append(request.getParameter("hdnUserType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1">
                                        <div id="configCalendar" class="pageHead_1">Configure calendar</div>
                                        <div>&nbsp;</div>
                                        <div id="action-tray">
                                            <ul>
                                                <li><a href="ConfigureCalendar.jsp" class="action-button-configure">Configure Calendar</a></li>
                                            </ul>
                                        </div>
                                        <div>&nbsp;</div>
                                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                            <tr>
                                                <td class="ff">Weekend (Fix throughout a year) : </td>
                                                <td>Friday, Saturday</td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Festival holidays : </td>
                                                <td class="txt_2"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                        <tr>
                                                            <th class="t-align-center">Sl. No.</th>
                                                            <th class="t-align-center">Date</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="t-align-center">1</td>
                                                            <td class="t-align-center">5/11/2010</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="t-align-center">2</td>
                                                            <td class="t-align-center">25/12/2010</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <div>&nbsp;</div>
                                    <!--Page Content End-->
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>

