<%-- 
    Document   : ProjectRoles
    Created on : Nov 1, 2010, 8:52:20 PM
    Author     : parag
--%>


<%@page import="java.util.ArrayList"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="projSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectSrBean" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.List,com.cptu.egp.eps.dao.storedprocedure.CommonAppData" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPProjectRolesReturn" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Project Details: Users</title>
        <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#frmProjectRoles").validate({
                    rules: {
                        chkProcurementRole: {required: true},
                        txtProjOffice: {required: true},
                        txtUser: {required: true}
                    },
                    messages: {
                        chkProcurementRole: {required: "<div class='reqF_1'>Please Select Procurement Role.</div>"},
                        txtProjOffice: {required: "<div class='reqF_1'>Please Select Project Office.</div>"},
                        txtUser: {required: "<div class='reqF_1'>Please Select Project Director.</div>"
                        }
                    }/*,
                     errorPlacement: function(error, element) {
                     if (element.attr("name") == "txtUser")
                     error.insertAfter("#btnSearchUser");
                     else if (element.attr("name") == "chkProcurementRole")
                     error.insertAfter("#chkAU");
                     else
                     error.insertAfter(element);
                     }*/
                }
                );
            });

        </script>
    </head>
    <%
        Logger LOGGER = Logger.getLogger("ProjectRoles.jsp");
        boolean isEdit = false;
        int projectId = 0;
        int userId = 0;
        int peAdminUserId = 0;
        if (session.getAttribute("userId") != null) {
            peAdminUserId = Integer.parseInt(session.getAttribute("userId").toString());
        }
        String action = "createProjectRoles";
        List<SPProjectRolesReturn> projectRolesReturns = null;
        byte suserTypeId = 0;

        if (session.getAttribute("userTypeId") != null) {
            suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
        }

        if (request.getParameter("hidProjectId") != null) {
            projectId = Integer.parseInt(request.getParameter("hidProjectId").toString());

        }
        if (request.getParameter("projectId") != null) {
            projectId = Integer.parseInt(request.getParameter("projectId").toString());

        }

        try {
            projectRolesReturns = projSrUser.getRolesReturns(projectId);
        } catch (Exception e) {
            LOGGER.error(" projectRolesReturns ::: " + e);
        }

        SPProjectRolesReturn userWiseList = null;
        //String[] roles=new String[2];
        if ("Edit".equalsIgnoreCase(request.getParameter("Edit"))) {
            userId = Integer.parseInt(request.getParameter("userId"));

            userWiseList = projSrUser.getUserRolesReturns(projectId, userId).get(0);
            isEdit = true;
            action = "updateProjRoles";

        }

    %>
    <body >
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <%                    StringBuilder userType = new StringBuilder();
                    if (request.getParameter("userType") != null) {
                        if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                            userType.append(request.getParameter("userType"));
                        } else {
                            userType.append("org");
                        }
                    } else {
                        userType.append("org");
                    }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <%--<%@include file="/resources/common/AfterLoginLeft.jsp" %>--%>

                        <td class="contentArea_1">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td>
                                        <!--Dashboard Header End-->
                                        <!--Dashboard Content Part Start-->
                                        <div class="pageHead_1">Project Details: Add Users</div>

                                        <%--<div id="action-tray">
                                            <ul>
                                                <li><a href="CreateEditProject.jsp?Edit=Edit&projectId=<%=projectId%>" class="action-button-goback">Go Back</a></li>
                                            </ul>
                                        </div>--%>

                                        <div id="successMsg" class="responseMsg successMsg t_space" style="display:none; margin-top: 12px; "></div>
                                        <div id="errMsg" class="responseMsg errorMsg"  style="display:none; margin-top: 12px;"></div>
                                        <div class="stepWiz_1 t_space">
                                            <ul>
                                                <li> <%
                                                    long pjid = 0;
                                                    pjid = projSrUser.checkIsProjectExist(projectId);

                                                    if (pjid != 0) {
                                                    %>
                                                    <a style="text-decoration: underline;" href="CreateEditProject.jsp?projectId=<%=projectId%>&Edit=Edit" >Key Fields Information</a>
                                                    <%  } else {%>
                                                    Key Fields Information
                                                    <%  }%></li>
                                                <li class="sMenu">&gt;&gt;&nbsp;&nbsp;Add Users </li>
                                                    <%--<li>&gt;&gt;&nbsp;&nbsp; -->
                                                        <%
                                                                int peuserid = 0;
                                                                int pduserid = 0;

                                                            projectRolesReturns = projSrUser.getRolesReturns(projectId);
                                                            
                                                            if (projectRolesReturns.size() > 0) {
                                                                for (int i = 0; i < projectRolesReturns.size(); i++) {
                                                                    
                                                                    if (projectRolesReturns.get(i).getRolesName().trim().contains("PD")) {
                                                                        pduserid = projectRolesReturns.get(i).getUserId();
                                                                    } else if (projectRolesReturns.get(i).getRolesName().trim().contains("PE")) {
                                                                        peuserid = projectRolesReturns.get(i).getUserId();
                                                                    }
                                                                }
                                                                if (pduserid == peuserid && pduserid != 0 && peuserid != 0) {

                                                    %>
<!--                                                    <a href="ProjectUserFP.jsp?Edit=Edit&userId=<%=pduserid%>&projectId=<%=projectId%>" style="text-decoration: underline;"> User Financial Power </a>-->
                                                    <%} else if (pduserid != peuserid && pduserid != 0 && peuserid != 0) {

                                                    %>
<!--                                                    <a href="ProjectUserFP.jsp?Edit=Edit&userId=<%=pduserid%>&projectId=<%=projectId%>" style="text-decoration: underline;"> User Financial Power(PD) </a>-->
                                                    &nbsp;|&nbsp
<!--                                                    <a href="ProjectUserFP.jsp?Edit=Edit&userId=<%=peuserid%>&projectId=<%=projectId%>" style="text-decoration: underline;"> User Financial Power(PE) </a>-->
                                                    <%
                                                                                                            } else if (pduserid != 0) {
                                                    %>
<!--                                                    <a href="ProjectUserFP.jsp?Edit=Edit&userId=<%=pduserid%>&projectId=<%=projectId%>" style="text-decoration: underline;"> User Financial Power </a>-->
                                                    <%
                                                                                                            } else if (peuserid != 0) {
                                                    %>
<!--                                                    <a href="ProjectUserFP.jsp?Edit=Edit&userId=<%=peuserid%>&projectId=<%=projectId%>" style="text-decoration: underline;"> User Financial Power </a>-->
                                                    <%
                                                                                                                } else {%>
<!--                                                    User Financial Power-->
                                                    <%}
                                                                                                                if (projectRolesReturns.isEmpty()) {
                                                                                                                    //out.print("User Financial Power");
                                                                                                                }

                                                                                                            } else {%>
<!--                                                    User Financial Power-->
                                                    <% }%>
<!--                                            </li>-->--%>
                                            </ul>
                                        </div>

                                        <form method="post" id="frmProjectRoles" action="<%=request.getContextPath()%>/ProjectSrBean?action=<%=action%>">
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                                <tr>
                                                    <td style="font-style: italic" class="t-align-right" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                                </tr>
                                                <tr>
                                                    <td class="ff" width="20%">Project Code : </td>
                                                    <td width="80%"><%=projSrUser.getProjectCode(projectId)%>
                                                        <%--<input name="txtProjCode" type="text" class="formTxtBox_1" id="txtProjCode"
                                                                           value=""  readonly="true" style="width:200px;" />--%>

                                                    </td>
                                                </tr>
                                                <%--<%
                                                            String pdorPeName = "";
                                                            String officeName = "";
                                                            int pdorPeId = 0;
                                                            int officeId = 0;
                                                            CommonAppData getName = null;

                                                            List<CommonAppData> getAuNames = null;

                                                            getName = projSrUser.getPdOrPeForOffice(1, peAdminUserId);
                                                            if (getName != null) {
                                                                pdorPeName = getName.getFieldName2();
                                                                pdorPeId = Integer.parseInt(getName.getFieldName1());
                                                                getName = null;
                                                            }

                                                            getAuNames = projSrUser.getAuUserForOffice(5, peAdminUserId);
                                                            if(getAuNames!=null && !getAuNames.isEmpty() && projectRolesReturns!=null && !projectRolesReturns.isEmpty()){
                                                                        for(int a=0;a<getAuNames.size();a++){
                                                                                for(int b=0;b<projectRolesReturns.size();b++){
                                                                                        if(Integer.toString(projectRolesReturns.get(b).getUserId()).equalsIgnoreCase(getAuNames.get(a).getFieldName1())){
                                                                                                getAuNames.remove(a);
                                                                                            }
                                                                                    }
                                                                            }
                                                                }
                                                %>--%>
                                                <tr>
                                                    <td class="ff">Procurement Role :   <span>*</span></td>
                                                    <td>
                                                        <table cellspacing="2" cellpadding="0">
                                                            <tr>
                                                                <td width="20"><label id="lblPD" > PD</label></td>
                                                                <td>
                                                                    <input type="checkbox" name="chkProcurementRole" id="chkPD"
                                                                           value="19"
                                                                           <%if (isEdit && userWiseList.getRolesName().contains("PD")) {%> checked  <%}%>
                                                                           onclick="showOffice();"  />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label id="lblPE" > PA</label>
                                                                </td>
                                                                <td>
                                                                    <input type="checkbox" name="chkProcurementRole" id="chkPE" 
                                                                           value="1"
                                                                           <%if (isEdit && userWiseList.getRolesName().contains("PE")) {%> checked  <%}%>
                                                                           onclick="showOffice();" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label id="lblAU" > AU</label></td><td><input type="checkbox" name="chkProcurementRole" id="chkAU"
                                                                                                              value="5"
                                                                                                              <%if (isEdit && userWiseList.getRolesName().contains("Authorized User")) {%> checked  <%}%>
                                                                                                              onclick="showOffice();" />
                                                                    <span id="procureRoleMsg"  style="color: red; font-weight: bold"></span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <span id="procureRoleMsg"  style="color: red; font-weight: bold">&nbsp;</span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>

                                                <tr id="hidProjOfficeTr" <%if (!isEdit) {%>style="display: none;"<%}%>>
                                                    <%
                                                        List<CommonAppData> officeList = new ArrayList<CommonAppData>();
                                                        officeList = projSrUser.getProjectOfficeOrg(peAdminUserId);
                                                    %>
                                                    <td class="ff">Project Office : </td>
                                                    <td><%if (officeList != null && !officeList.isEmpty()) {%>
                                                        <select class="formTxtBox_1" name="hidProjOffice" id="hidProjOffice" style="width: 200px" onchange="forOfficeEmp();">
                                                            <option value="" >-- Select Office --</option>
                                                            <%

                                                    for (CommonAppData commonAppData : officeList) {
                                                        if (isEdit) {
                                                            if (userWiseList.getOfficeId() == (Integer.parseInt(commonAppData.getFieldName1()))) {%>
                                                            <option value="<%=commonAppData.getFieldName1()%>" selected="selected"><%=commonAppData.getFieldName2()%></option>  
                                                            <%} else {%>
                                                            <option value="<%=commonAppData.getFieldName1()%>" ><%=commonAppData.getFieldName2()%></option>  
                                                            <%}
                                                                        } else {%>
                                                            <option value="<%=commonAppData.getFieldName1()%>" ><%=commonAppData.getFieldName2()%></option>  
                                                            <%}
                                                                       }%>
                                                        </select>
                                                        <%} else {%>
                                                        <input type="hidden" name="hidProjOffice" id="hidProjOffice" value=""/>
                                                        <label>No office(s) found in this Organization</label>
                                                        <%}%>

                                                    </td> 
                                                </tr>
                                                <tr id="addUserTr" style="display: none;">
                                                    <td class="ff">Add User :   </td>
                                                    <!--                                                    <td><input name="txtUser" type="text" class="formTxtBox_1" id="txtUser"
                                                                                                                   value="<%if (isEdit) {
                                                                                                                           out.print(userWiseList.getEmpName());
                                                                                                                       }%>"
                                                                                                                   readonly="true"
                                                                                                                   style="width:200px;" />
                                                                                                            <input name="hidUser" type="hidden" class="formTxtBox_1"
                                                                                                                   value="<%if (isEdit) {
                                                                                                                           out.print(userWiseList.getUserId());
                                                                                                                       }%>"
                                                                                                                   id="hidUser" value="" />
                                                                                                            &nbsp;&nbsp;<a href="#" id="btnSearchUser" onclick="selectUser();" class="action-button-add">Select user</a>
                                                    <%-- <a href="#" class="action-button-delete">Remove User</a>--%></td>-->
                                                    <%--<td>
                                                        <%if(pdorPeName!="" || !getAuNames.isEmpty()){%>
                                                        <label id="lblPdOrPeName" style="display: none;"><%= pdorPeName%></label>


                                                        <select class="formTxtBox_1" id="cmbAuName" style="display: none;" name="cmbAuName" style="width: 208px" onchange="onChageOfAuName(this);">
                                                            <option  value="-1">---Select----</option>
                                                            <%
                                                                        if (getAuNames != null) {
                                                                            if (!getAuNames.isEmpty()) {

                                                                                for (int i = 0; i < getAuNames.size(); i++) {
                                                            %>
                                                            <option  value="<%=getAuNames.get(i).getFieldName1()%>"><%=getAuNames.get(i).getFieldName2()%></option>
                                                            <%
                                                                                }
                                                                            }
                                                                        }
                                                            %>
                                                        </select>
                                                        <input type="hidden" name="hidUser" id="hidUser" value="" />
                                                        <input type="hidden" name="hidPePdUser" id="hidPePdUser" value="<%=pdorPeId%>" />
                                                        <!--                                            <a href="#" id="btnSearchUser" onclick="selectUser();" class="action-button-add">Select user</a>-->
                                                        <%}else{%>
                                                        <lable class="formTxtBox_1">No user(s) found in this Office</lable>
                                                        <%}%>
                                                    </td>
                                                </tr> --%>

                                                    <%--<tr>
                                                        <td class="ff">Project Office : </td>

                                                    <!--                                                    <td><input name="txtProjOffice" type="text" class="formTxtBox_1" id="txtProjOffice"
                                                                                                                   value="<%if (isEdit) {
                                                                               out.print(userWiseList.getOfficeName());
                                                                           }%>"
                                                                                                                   readonly="true" style="width:200px;" />
                                                                                                            <input name="hidProjOffice" type="hidden" class="formTxtBox_1" id="hidProjOffice"
                                                                                                                   value="<%if (isEdit) {
                                                                               out.print(userWiseList.getOfficeId());
                                                                           }%>" />
                                                                                                        </td>-->
                                                    <td>
                                                        <%
                                                                    getName = projSrUser.getProjectOffice(peAdminUserId);
                                                                    if (getName != null) {
                                                                        officeName = getName.getFieldName2();
                                                                        officeId = Integer.parseInt(getName.getFieldName1());
                                                                        getName = null;
                                                                    }
                                                        %>

                                                        <label id="lblOfficeName"><%=officeName%></label>
                                                        <input type="hidden" name="hidProjOffice" id="hidProjOffice" value="<%=officeId%>">
                                                    </td>
                                                </tr>--%>
                                                    <td id="addUserTd">

                                                    </td>
                                                    <%if (!isEdit) {%>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td align="left"><label class="formBtn_1"><input type="submit" name="button" id="button" value="Add User" onclick="return buttonDisabled()" /></label>&nbsp;
                                                        <label class="formBtn_1"><input type="reset" name="buttonReset" id="buttonReset" value="Reset" onclick="return resetButton();" /></label>
                                                    </td>

                                                </tr>
                                                <%} else {%>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td align="left"><label class="formBtn_1"><input type="submit" name="button" id="button" value="Update User" onclick="return buttonDisabled()"/></label>
                                                        <label class="formBtn_1"><input type="reset" name="buttonReset" id="buttonReset" value="Reset" onclick="return resetButton();" /></label>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <tr>
                                                    <td><label >
                                                            <input type="hidden" name="hidProjectId" id="hidProjectId" value="<%=projectId%>" />
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" cellspacing="0" class="tableList_1" >
                                                <tr>
                                                    <th class="t-align-left" width="5%" >S No. </th>
                                                    <th class="t-align-left" width="25%">User Name </th>
                                                    <th class="t-align-left" width="15%">Procurement Role </th>
                                                    <th class="t-align-left" width="40%">Office </th>
                                                    <!--                                                    <th class="t-align-left" >Edit </th>-->
                                                    <!--                                                    <th class="t-align-left" >Assign Financial Power </th>-->
                                                    <th class="t-align-left" width="15%">Remove User Roles </th>
                                                </tr>
                                                <%if (projectRolesReturns != null) {
                                                        if (projectRolesReturns.size() > 0) {
                                                            for (int i = 0; i < projectRolesReturns.size(); i++) {%>
                                                <tr>
                                                    <td class="t-align-center" width="7%" ><%=(i + 1)%></td>
                                                    <td class="t-align-left"><%=projectRolesReturns.get(i).getEmpName()%></td>
                                                    <td class="t-align-left">
                                                        <%
                                                            if(projectRolesReturns.get(i).getRolesName().contains("PE"))
                                                            {
                                                                out.print("PA");
                                                            }
                                                            else
                                                            {
                                                                out.print(projectRolesReturns.get(i).getRolesName());
                                                            }
                                                        %>
                                                    </td>
                                                    <td class="t-align-left"><%=projectRolesReturns.get(i).getOfficeName()%>
                                                        <input type="hidden" id="pePdAuUserId" value="<%=projectRolesReturns.get(i).getUserId()%>" />
                                                        <input type="hidden" id="pePdAuUserRole" value="<%=projectRolesReturns.get(i).getRolesId()%>" />
                                                    </td>

<!--                                                    <td class="t-align-left"><a href="ProjectRoles.jsp?Edit=Edit&userId=<%=projectRolesReturns.get(i).getUserId()%>&hidProjectId=<%=projectId%>" > Edit </a></td>-->
                                                    <!--                                                    <td class="t-align-left" >-->
                                                    <%//if (projectRolesReturns.get(i).getRolesName().trim().contains("PD")) {%>
<!--                                                        <a href="ProjectUserFP.jsp?Edit=Edit&userId=<%=projectRolesReturns.get(i).getUserId()%>&projectId=<%=projectId%>"> Assign</a> </td>-->
                                                    <%//} else {%>
                                                    <!--                                                    --->
                                                    <%//}%>

                                                    <td class="t-align-center">
                                                        <a href="../ProjectSrBean?action=Remove&userId=<%=projectRolesReturns.get(i).getUserId()%>&projectId=<%=projectId%>&hidProjectId=<%=projectId%> " title="Remove" ><img  src="../resources/images/Dashboard/delIcn.png" alt="Remove" width="16" height="16" onclick="return confirm('Do you really want to remove the user?');" /></a>
                                                    </td>
                                                </tr>
                                                <%}

                                                        } else {
                                                            out.print("<tr> <td class='t-align-center' colspan='7' > No Records Found. </td></tr>");
                                                        }
                                                    }

                                                %>
                                            </table>
                                            <table width="100%" id="table1" class="t_space" cellspacing="0" border="0" >
                                                <tr>
                                                    <td class="t-align-center"><label class="formBtn_1">
                                                            <a href="ProjectDetail.jsp" class="anchorLink">Submit</a>
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form></td>


                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <%@include file="../resources/common/Bottom.jsp" %>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->                
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
</html>
<script type ="text/javascript">
    function resetButton() {
        document.getElementById("addUserTr").style.display = 'none';
        $('#hidProjOfficeTr').hide();
        $('#addUserTr').hide();
    }

    function checkPD() {
        $("#procureRoleMsg").html("");
        $.ajax({
            url: "<%=request.getContextPath()%>/ProjectSrBean?projectId=<%=projectId%>&funName=checkPD&",
            method: 'POST',
            async: false,
            success: function (j) {
                if (j == "true") {
                    document.getElementById("chkPD").selected = false;

                }
            }
        });
    }

    <%if (!isEdit) {%>
    checkPD();
    forOfficeselect();
    <%} else if (!userWiseList.getRolesName().contains("PD") && !userWiseList.getRolesName().contains("PE")) {%>
    checkPD();
    forOfficeselect();
    <%}%>

    function checkPE(userId) {
        if (document.getElementById("hidUser").value != document.getElementById("pePdAuUserId").value) {
            $("#procureRoleMsg").html("");
            var flag = false;
            $.ajax({
                url: "<%=request.getContextPath()%>/ProjectSrBean?projectId=<%=projectId%>&userId=" + userId + "&funName=checkPE&",
                method: 'POST',
                async: false,
                success: function (j) {
                    if (j == "true") {
                        document.getElementById("chkPE").selected = false;
                        document.getElementById("chkAU").selected = false;
                        flag = j;
                        //   doucment.getElementById("procureRoleMsg").innerHTML = "<div class='reqF_1'>You have already select PE role.</div>";
                        $("#procureRoleMsg").html('PE role is already selected for this user.');
                        $("#button").attr('disabled', 'disabled');
                    }
                }
            });
            if (!flag) {
                $("#procureRoleMsg").html('');
                checkAU(userId);
            }

        } else {
            $('#procureRoleMsg').show();
            $("#procureRoleMsg").html('<br />PE Or PD role is already selected for this user.');
            flag = false;
        }
        return flag;
    }

    function checkAU(userId) {

        if (document.getElementById("hidUser").value != document.getElementById("pePdAuUserId").value) {
            $("#procureRoleMsg").html("");
            var flag = false;
            $.ajax({
                url: "<%=request.getContextPath()%>/ProjectSrBean?projectId=<%=projectId%>&userId=" + userId + "&funName=checkAU&",
                method: 'POST',
                async: false,
                success: function (j) {

                    if (j == "true") {
                        document.getElementById("chkPE").selected = false;
                        document.getElementById("chkAU").selected = false;
                        $("#procureRoleMsg").html('AU role is already selected for this user.');
                        flag = j;
                        $("#button").attr('disabled', 'disabled');
                    }
                }

            });
            if (!flag) {
                $('#chkAU').show();
                $('#lblAU').show();
            }
            return flag;
        } else {
            $("#procureRoleMsg").html('<br />AU role is already selected for this user.');
        }
    }


    function forOfficeselect() {
        $.ajax({
            url: "<%=request.getContextPath()%>/ProjectSrBean?projectId=<%=projectId%>&funName=checkProjectPE&",
            method: 'POST',
            async: true,
            success: function (j) {
                if (j == "true") {
                    document.getElementById("chkPE").selected = false;
                }
            }
        });
    }



    function selectUser() {
        document.getElementById("lblPdOrPeName").style.display = "none";
        if (!document.getElementById("chkPD").checked && !document.getElementById("chkPE").checked && !document.getElementById("chkAU").checked) {
            jAlert("Please Select Procurement Role.", "Procurement Role", function (RetVal) {
                return false;
            });
        } else {
            window.open('SearchUser.jsp', 'window', 'width=900,height=600,resizable=yes,scrollbars=1')
        }
    }

    /* function hidValue(){
     if(!document.getElementById("chkPD").checked && !document.getElementById("chkPE").checked && !document.getElementById("chkAU").checked ){
     document.getElementById("hidUser").value="";
     //hidPePdUser
     if(!document.getElementById("chkPD").checked){
     document.getElementById("hidUser").value="";
     }
     if(!document.getElementById("chkPE").checked){
     document.getElementById("hidUser").value="";
     }
     if(!document.getElementById("chkAU").checked){
     document.getElementById("hidUser").value="";
     }
     }
     }*/

</script>
<script type="text/javascript">
    var headSel_Obj = document.getElementById("headTabContent");
    if (headSel_Obj != null) {
        headSel_Obj.setAttribute("class", "selected");
    }
</script>
<%
    projSrUser = null;
%><script type="text/javascript">
    function showOffice() {
        var flag = true;
        if ($('#chkPE').attr("checked") == true && $('#chkAU').attr("checked") == true) {
            $('#chkAU').removeAttr("checked");
            $('#chkPE').removeAttr("checked");

            flag = false;

        }
        if (flag) {
            if ($('#chkPE').attr("checked") == true || $('#chkPD').attr("checked") == true || $('#chkAU').attr("checked") == true) {
                $('#hidProjOfficeTr').show();
                if ($('#hidProjOffice').val() != '') {
                    forOfficeEmp();
                }
            } else {
                $('#hidUser').children().remove();

                $('#addUserTr').hide();
                jAlert('Please Select Role', "Error", function (RetVal) {
                    return false;
                });

            }
        } else {
            $('#hidUser').children().remove();
            $('#addUserTr').hide();
            jAlert('PE can not be AU', "Error", function (RetVal) {
                return false;
            });
        }

    }
    function forOfficeEmp() {
        var flag = true;
        var val = "";
        var msg = "";
        var officeVal = "";
        if ($('#chkPD').attr("checked") == true) {
            val = 'PD'
        }
        if ($('#chkPE').attr("checked") == true || ($('#chkPE').attr("checked") == true && $('#chkPD').attr("checked") == true)) {
            val = 'PE';
        }

        if ($('#chkAU').attr("checked") == true || ($('#chkAU').attr("checked") == true && $('#chkPD').attr("checked") == true)) {
            val = 'AU';
        }
        if (val != '') {
            officeVal = $('#hidProjOffice').val();
            if (officeVal == "") {
                $("select#hidUser").html('');
                msg = "Please select Office";
                flag = false;
            }
        } else {
            $("select#hidUser").html('');
            flag = false;
            msg = "Please select Procurment Role";
        }
        if (flag) {
            $.ajax({
                url: "<%=request.getContextPath()%>/ProjectSrBean?role=" + val + "&officeId=" + officeVal + "&funName=getEmprole",
                method: 'POST',
                async: true,
                success: function (j) {
                    //$("addUserTd").html(j.toString());
                    document.getElementById("addUserTd").innerHTML = j;
                    $('#addUserTr').show();
                }
            });
        } else {
            jAlert(msg, "Error", function (RetVal) {
                return false;
            });
        }
    }
    var isError = true;
    function cheksameUser() {
        $(":checkbox[checked='true']").each(function () {


            if ($(this).val() == 5 && $('#pePdAuUserRole').val() == 1) {
                if ($('#pePdAuUserId').val() == $('#hidUser').val()) {
                    isError = false;
                    jAlert('PE and AU can not be same', "Error", function (RetVal) {
                        return false;
                    });
                }
            }
        });

    }
    function buttonDisabled() {
        var flag = true;
        cheksameUser();
        var i = 0;
        $(":checkbox[checked='true']").each(function () {
            i++;
        });
        if (i == 0) {
            jAlert('Please Select Procurement Role', "Error", function (RetVal) {
                return false;
            });
        }
        if ($('#hidUser').val() == '') {
            flag = false;
            jAlert('Please Select Employee', "Error", function (RetVal) {
                return false;
            });
        }
        if ($('#hidProjOffice').val() == '') {
            flag = false;
            jAlert('Please Select Project Office', "Error", function (RetVal) {
                return false;
            });
        }

        if (isError && flag) {
        } else {
            return false;
        }
    }

</script>

