<%-- 
    Document   : NewsManagement
    Created on : Nov 19, 2010, 12:42:12 PM
    Author     : Karan
--%>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.web.servlet.BOQServlet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            BSR List
        </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <script type="text/javascript" >
            
        </script>
        <%
//            int counterId = 0;
//
//            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

//            CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
//            List<SPCommonSearchData> data = commonSearchService.searchData("BOQList", null, null, null, null, null, null, null, null, null);

            String userID = "0";
            if (session.getAttribute("userId") != null) {
                userID = session.getAttribute("userId").toString();
            }
//            commonSearchService.setLogUserId(userID);

        %>
        <script type="text/javascript">
            function chkdisble(pageNo) {
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if (parseInt($('#pageNo').val(), 10) != 1) {
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if (parseInt($('#pageNo').val(), 10) == 1) {
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if (parseInt($('#pageNo').val(), 10) == 1) {
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if (parseInt($('#pageNo').val(), 10) > 1) {
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if (parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())) {
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                } else {
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if (parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())) {
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                } else {
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            function categoryCombo()
            {                
                $.post("<%=request.getContextPath()%>/BOQServlet", {funName: "SelectBOQCategory"}, function (j) {
                    $("#cmbCategory").append(j);
                });
            }
            
            function loadTable()
            {               
                $("#progressBar").show();
                $("#progressBar").css("visibility","visible");
                $("#btnGo").attr('disabled','disabled');
                                                
                $.post("<%=request.getContextPath()%>/BOQServlet", {funName: "SelectBOQ", c: $('#cmbCategory').val()}, function (j) {
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    sortTable();
                    if ($('#noRecordFound').attr('value') == "noRecordFound") {
                        $('#pagination').hide();
                    } else {
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if ($("#totalPages").val() == 1) {
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    } else {
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                    
                    $("#progressBar").hide();
                    $("#btnGo").removeAttr('disabled');
                });
            }
        </script>
        <script type="text/javascript">
            $(function () {
                categoryCombo();
                $('#cmbCategory').change(function () 
                {
                    loadTable();
                    
                    var cat = $('#cmbCategory').val();
                    
                    if (cat != '') 
                    {
                        $('#cat').hide();
                    }
                    else
                    {
                        $('#cat').hide();
                        $('#cat').toggle();
                    }                    
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#btnLast').click(function () {
                    var totalPages = parseInt($('#totalPages').val(), 10);
                    if (totalPages > 0)
                    {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#btnNext').click(function () {
                    var pageNo = parseInt($('#pageNo').val(), 10);
                    var totalPages = parseInt($('#totalPages').val(), 10);

                    if (pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo) + 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) + 1);

                        $('#dispPage').val(Number(pageNo) + 1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function () {
                $('#btnPrevious').click(function () {
                    var pageNo = $('#pageNo').val();
                    if (parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function () {                
                $('#btnGo').click(function () {
                    
                    //var ids = [];
                    var count = 0;
                    $( "input:checkbox[id^=chk_]:checked" ).each(function()
                    {
                        ++count;
                        var tr=this.closest('tr');
                        tr.id = "tr" + count;
                        
                        var cat = $('#cmbCategory').val();
                        var r = 2;
                    
                        if (cat.length == 0) 
                        {
                            r+=1;
                        }
                        
                        var code = $('#' + tr.id).children('td')[r].innerHTML;
                        var desc = $('#' + tr.id).children('td')[r+1].innerHTML;  
                        
                        var unit = $('#' + tr.id).children('td')[r+2].innerHTML;
                        
                        var idd = this.id.replace('chk_','');
                        //ids.push(idd);
                        window.opener.addRowToTable(idd,code, desc, unit);
                    });
                    
                    window.close();
                });
            });
        </script>

    </head>
    <body onload="loadTable();">
        <form name="frmsubmit" id="frmsubmit" method="post"></form>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-left: 15px" class="t_space">
                
                <tr>
                        <td>
                            <!--Dashboard Content Part Start-->
                            <div class="pageHead_1">Select BSR                             
                            </div>       
                            <table>
                                <tr>
                    <td width="15%" class="t-align-left ff">Category: </td>
                    <td width="85%" class="t-align-left">
                        <select style="width:200px;" id="cmbCategory" class="formTxtBox_1" name="cmbCategory">
                                <option value="">- Select Category -</option>                                
                            </select>                     
                    </td>
                </tr>
                            </table>
                        <table width="40%" cellspacing="0" id="resultTable" class="tableList_3 t_space" cols="@0,1,2,3,4,5,6">                            
                            <div id="progressBar" align="center" style="visibility:hidden; width: 40%"  >
                                                <img id="theImg" alt="progress" src="<%=request.getContextPath()%>/resources/images/loading.gif"/>
                                            </div>
                            <tr>
                                <th width="5%" class="t-align-center">Select</th>
                                <th id="cat" width="10%" class="t-align-center">Category</th>
                                <th width="10%" class="t-align-center">Sub Category</th>
                                <th width="5%" class="t-align-center">Code</th>
                                <th width="15%" class="t-align-center">Description</th>
                                <th width="5%" class="t-align-center">Unit</th>
                            </tr>
                        </table>
                        <!--Dashboard Content Part End-->
                        <br/>
                        <div align="center" style="width: 100%; padding: 10px 0px; position: fixed; bottom: 0px; left: 0px; background-color: #f0e68c">
                            <label class="formBtn_1">
                                <input type="submit" name="btnGo" id="btnGo" id="button" value="Submit" />
                            </label>
                        </div>
                    </td>
                </tr>
            </table>
            <!--Dashboard Footer Start-->
            <div align="center"></div>
            <!--Dashboard Footer End-->
        </div>
        <script>
            var obj = document.getElementById('lblBOQList');
            if (obj != null) {
                if (obj.innerHTML == 'View') {
                    obj.setAttribute('class', 'selected');
                }
            }

        </script>
        <script>
            var headSel_Obj = document.getElementById("headTabContent");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
