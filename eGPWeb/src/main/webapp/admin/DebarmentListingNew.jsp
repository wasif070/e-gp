<%--
    Document   : InitiateDebarmentProcessNew
    Created on : Sep 03, 2016, 3:33:25 PM
    Author     : nafiul
--%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentTypes"%>
<%@page import="com.cptu.egp.eps.web.servicebean.InitDebarmentSrBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.dao.storedprocedure.UserApprovalBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.Calendar,com.cptu.egp.eps.service.serviceimpl.ContentAdminService" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Initiate Debarment Process</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript">
            /* Call Calendar function */
            function GetCal(txtname, controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: true,
                    dateFormat: "%d/%m/%Y",
                    onSelect: function () {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            /*Check the disable*/
            function chkdisble(pageNo) {
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if (parseInt($('#pageNo').val(), 10) != 1) {
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if (parseInt($('#pageNo').val(), 10) == 1) {
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if (parseInt($('#pageNo').val(), 10) == 1) {
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if (parseInt($('#pageNo').val(), 10) > 1) {
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if (parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())) {
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                } else {
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if (parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())) {
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                } else {
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
            /*Load The Table*/
            function loadTable()
            {
                if ($("#keyWord").val() == undefined)
                    $("#keyWord").val('');
                $.post("<%=request.getContextPath()%>/InitDebarment", {pageNo: $("#pageNo").val(), size: $("#size").val(), searchFor: $('#idsearchFor').val(), cmpName: $('#txtcmpName').val(), dtFrom: $('#txtDateFrom').val(), dtTo: $('#txtDateTo').val(), action: 'getDebarDataListing'}, function (j) {
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);

                    if ($('#noRecordFound').attr('value') == "noRecordFound") {
                        $('#pagination').hide();
                    } else {
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if ($("#totalPages").val() == 1) {
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    } else {
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                    // Dynamically shortened text with ?Read More? Link
                    var showChar = 150;  // How many characters are shown by default
                    var ellipsestext = "...";
                    var moretext = "more";
                    var lesstext = "less";

                    $('.more').each(function () {
                        var content = $(this).html();

                        if (content.length > showChar) {

                            var c = content.substr(0, showChar);
                            var h = content.substr(showChar, content.length - showChar);
                            var html = c + '<span class="moreellipses">' + ellipsestext + '</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';

                            $(this).html(html);
                        } else
                        {
                            var c = content + '</br>';
                            var html = c;
                            $(this).html(html);
                        }

                    });

                    $(".morelink").click(function () {
                        if ($(this).hasClass("less")) {
                            $(this).removeClass("less");
                            $(this).html(moretext);
                        } else {
                            $(this).addClass("less");
                            $(this).html(lesstext);
                        }
                        $(this).parent().prev().toggle();
                        $(this).prev().toggle();
                        return false;
                    });
                });
            }
            /*  Handle country Combo Event*/
            $(function () {
                $('#cmbCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCountry').val().toString().split('_', 100)[0], funName: 'stateComboValue'}, function (j) {
                        //alert(j.toSource());
                        $('#cmbState').children().remove().end().append('<option value="">--Select Dzongkhag / District --</option>');
                        $("select#cmbState").append(j.toString().replace('selected', ''));
                        //toggleUpzillaView();
                    });
                });
            });

            $(function () {
                $('#btnGoto').click(function () {
                    var pageNo = parseInt($('#dispPage').val(), 10);
                    var totalPages = parseInt($('#totalPages').val(), 10);
                    if (pageNo > 0)
                    {
                        if (pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
            $(function () {
                $('#btnPrevious').click(function () {
                    var pageNo = $('#pageNo').val();
                    if (parseInt(pageNo, 10) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
            $(function () {
                $('#btnNext').click(function () {
                    var pageNo = parseInt($('#pageNo').val(), 10);
                    var totalPages = parseInt($('#totalPages').val(), 10);

                    if (pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo) + 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) + 1);

                        $('#dispPage').val(Number(pageNo) + 1);
                    }
                });
            });
            $(function () {
                $('#btnLast').click(function () {
                    var totalPages = parseInt($('#totalPages').val(), 10);
                    if (totalPages > 0) {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
            $(function () {
                $('#btnFirst').click(function () {
                    var totalPages = parseInt($('#totalPages').val(), 10);
                    var pageNo = $('#pageNo').val();
                    if (totalPages > 0 && pageNo != "1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
            $(function () {
                $('#idsearchFor').change(function () {
                    if ($('#idsearchFor').val() == 'company')
                    {
                        $('#lvlCompanyName').html('Company Name :');
                    } else if ($('#idsearchFor').val() == 'individual')
                    {
                        $('#lvlCompanyName').html('Consultant Name :');
                    }
                });
            });
        </script>
    </head>
    <body onload="loadTable(); hide();">
        <input type="hidden" value="<%=DateUtils.formatStdDate(new java.util.Date())%>" name="currDate" id="hdnCurrDate"/>
        <div class="mainDiv">
            <div class="fixDiv">
                <div class="contentArea_1">

                    <!--Dashboard Header Start-->
                    <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->
                    <div class="contentArea_1">
                        <div class="pageHead_1">Debarment List</div>
                        <div class="formBg_1 t_space">
                        <%                            
                            if (request.getParameter("flag") != null) {
                                String flag = request.getParameter("flag");
                                if (flag.equalsIgnoreCase("fail")) {
                                    out.println("<div class='responseMsg errorMsg'>Error Initiating Debarment Process</div><br/>");
                                }
                                if (flag.equalsIgnoreCase("success")) {
                                    out.println("<div class='responseMsg successMsg'>Reinstate Process Successful</div><br/>");
                                }
                            }
                        %>
                        <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" id="tblSearchBox">
                                <tr>
                                    <td class="ff" width="15%" >Search For : </td>
                                    <td width="35%">
                                        <select name="idsearchFor" class="formTxtBox_1" id="idsearchFor" style="width: auto;">
                                            <option value="company" selected>Company</option>
                                            <option value="individual">Individual Consultant</option>
                                        </select> 
                                    </td>
                                    <td width="15%"></td>
                                    <td width="35%">&nbsp;</td>
                                </tr>
                                <tr id="companyTr">
                                    <td class="ff" width="15%" id = 'lvlCompanyName'>Company Name : </td>
                                    <td width="35%">
                                        <input name="txtcmpName" type="text" class="formTxtBox_1" id="txtcmpName" style="width:200px;" />
                                    </td>
                                    <td width="15%"></td>
                                    <td width="35%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="t-align-center">
                                        <label class="formBtn_1">
                                            <input type="button" name="button" id="button" value="Search" onclick="javascript:{
                                                        document.getElementById('pageNo').value = 1;
                                                        loadTable();
                                                    }" />
                                        </label>
                                        <label class="formBtn_1">
                                            <input type="submit" name="btnReset" id="btnReset" value="Reset" onclick="window.location.reload();"/>
                                        </label>
                                    </td>
                                    <td class="t-align-right">

                                    </td>
                                </tr>
                            </table>
                        </div>
                        <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
                            <tr>
                                <th width="4%" class="t-align-center">Sl. <br/> No.</th>
                                <th width="30%" class="t-align-center">Consultant/Company</th>
                                <!--<th width="12%" class="t-align-center">Dzongkhag/District</th>-->
                                <th width="10%" class="t-align-center">Debarred Category</th>
                                <th width="10%" class="t-align-center">Debarred From</th>
                                <th width="10%" class="t-align-center">Debarred To</th>
                                <th width="16%" class="t-align-center">Reason</th>
                                <th width="20%" class="t-align-center">Action</th>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                            <tr>
                                <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    Total Records : <input type="text" name="size" id="size" value="10" style="width:70px;"/>
                                </td>
                                <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                    &nbsp;
                                    <label class="formBtn_1">
                                        <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                    </label></td>
                                <td  class="prevNext-container">
                                    <ul>
                                        <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                        <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                        <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                        <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                        <input type="hidden" id="pageNo" value="1"/>
                    </div>
                </div>
                <%@include file="../resources/common/Bottom.jsp" %>
            </div>
            <!--Dashboard Footer End-->
        </div>

    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabDebar");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }
        
        function showHide()
        {
            if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
                document.getElementById('tblSearchBox').style.display = 'table';
                document.getElementById('collExp').innerHTML = '- Advanced Search';
            }else{
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Advanced Search';
            }
        }
        function hide()
        {
            document.getElementById('tblSearchBox').style.display = 'none';
            document.getElementById('collExp').innerHTML = '+ Advanced Search';
        }

    </script>
</html>
