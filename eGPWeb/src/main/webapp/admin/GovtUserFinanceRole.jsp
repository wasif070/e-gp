<%--
    Document   : GovtuserFinanceRole
    Created on : Oct 23, 2010, 6:42:15 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="govSrUser" class="com.cptu.egp.eps.web.servicebean.GovtUserSrBean" />
<jsp:useBean id="projSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectSrBean" />
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod,java.util.List,com.cptu.egp.eps.model.view.VwEmpfinancePower" %>
<%
            //govUser.getRole();
        List<TblProcurementMethod>  procurementMethod = projSrUser.getProcurementMethods();
            String str = request.getContextPath();
            int empId = 0;
            if (request.getParameter("empId") != null) {
                empId = Integer.parseInt(request.getParameter("empId"));
                govSrUser.setEmpId(empId);
            }            

            String finPowerBy="";
            String roleId="";
            String roleName="";
            String empRoleId="";
            short deptId=0;
            int userId=0;

            if(request.getParameter("finPowerBy") != null){
                finPowerBy = request.getParameter("finPowerBy");
            }
            if(request.getParameter("roleId") != null){
                roleId = request.getParameter("roleId");
            }
            if(request.getParameter("roleName") != null){
                roleName = request.getParameter("roleName");
            }
            if(request.getParameter("empRoleId") != null){
                empRoleId = request.getParameter("empRoleId");
            }
            if(request.getParameter("userId") != null){
                userId = Integer.parseInt(request.getParameter("userId"));
            }

            govSrUser.setUserId(userId);
            govSrUser.setEmpId(empId);            
            Object[] object = null;
            object = govSrUser.getGovtUserData();


            if(request.getParameter("deptId") != null){
                deptId = Short.parseShort(request.getParameter("deptId"));
                govSrUser.setDepartmentId(deptId);
            }

            List<VwEmpfinancePower> vwFinancePower=govSrUser.getGovtFinanceRole();            
            String action="createGovtFinanceRole";

            if(vwFinancePower.size() > 0){
                    action = "editGovtFinanceRole";
            }            
%>
<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - Govt User Assign Financial Power</title>
         <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
         <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            <%--$(document).ready(function() {
               $("#frmFinance").validate({
                   rules: {
                       powerType: { required: true },
                       nature: { required: true },
                       method: { required: true },
                       budgetType: { required: true },
                       operator: { required: true },
                       amount:  { required: true , number: true }
                 },
                   messages: {
                       powerType: { required: "<div class='reqF_1'>Please Select Financial Type</div>"
                       },
                        nature: { required: "<div class='reqF_1'>Please Select Nature</div>"
                       },
                       method: { required: "<div class='reqF_1'>Please Select Method</div>"
                       },
                       budgetType: { required: "<div class='reqF_1'>Please Select Type</div>" },

                operator: { required: "<div class='reqF_1'>Please Select Operator</div>" },

                amount: { required: "<div class='reqF_1'>Please Enter Amount</div>",
                number: "<div class='reqF_1'>Please Enter Only Numbers</div>"}
                }
        }
    );
    });--%>

        function numericDE(value) {
                return  /^(\d+(\.\d{1,2})?)$/.test(value);
          }

        var count=0;

        var tempCount=0;

        function addComponent(){
            count=$('#txtCount').val();
            //alert('sanjay start');
            //alert(count);
            var nature="";
            var procurementMethod="";
            var budgetType="";
            var operator="";
            var str="";


            if(!checkValidData()){
                return false;
            }

            count++;

            if(count > 1){
                if(checkData()){
                    document.getElementById("errMsg").innerHTML="";
                    document.getElementById("errMsg").style.display='block';
                    document.getElementById("errMsg").innerHTML ='Selected Value Already Exists.';
                    return false;
                }
            }

            //alert(count);


            var tbody0=document.getElementById("tbody0");
            var table0 = document.getElementById("table0");

            var tr0=document.createElement("tr");
            tr0.id="tr0_"+count;

            var cnt=-1;
            //alert(count +" == "+cnt );
            var td0=document.createElement("td");
            td0.id="td"+count+"_"+(++cnt);
            td0.innerHTML ="<input type='checkbox' id='checkbox"+count+"_"+cnt+"' name='checkbox' />";
            tr0.appendChild(td0);


            <%--var td10=document.createElement("td");
            td10.id="td"+count+"_"+(cnt++);
            td10.innerHTML ="<lable id='lbRole"+count+"_"+(cnt)+"' >"+document.getElementById("cmbRole").options[document.getElementById("cmbRole").selectedIndex].innerHTML+"</label> \n\
         <input type='text' name='cmbRole1_1' id='cmbRole1_1' value='"+document.getElementById('cmbRole').value+"'  />";
            tr0.appendChild(td10);--%>

            var td1=document.createElement("td");
            td1.id="td"+count+"_"+(cnt++);
            nature = " <label>"+document.getElementById("cmbnature0_1").options[document.getElementById("cmbnature0_1").selectedIndex].innerHTML+"</label> \n\
        <input type='hidden' name='cmbnature"+count+"_"+(cnt)+"'  id='cmbnature"+count+"_"+(cnt)+"' value='"+document.getElementById('cmbnature0_1').value+"'  />";
            td1.innerHTML = nature;
            tr0.appendChild(td1);

            var pmethod;
            if(document.getElementById("cmbMethod0_2").options[document.getElementById("cmbMethod0_2").selectedIndex].innerHTML  =="-- Select --"){
                pmethod = "-";
            }else{
                pmethod = document.getElementById("cmbMethod0_2").options[document.getElementById("cmbMethod0_2").selectedIndex].innerHTML ;
            }
            var td2=document.createElement("td");
            td2.id="td"+count+"_"+(++cnt);
            procurementMethod = " <label>"+pmethod+"</label> \n\
        <input type='hidden' name='cmbMethod"+count+"_"+(cnt)+"'  id='cmbMethod"+count+"_"+(cnt)+"' \n\
        value='"+document.getElementById('cmbMethod0_2').value+"'  />";
            td2.innerHTML = procurementMethod;
            tr0.appendChild(td2);


            var td3=document.createElement("td");
            td3.id="td"+count+"_"+(++cnt);
            budgetType = " <label>"+document.getElementById("cmbBudgetType0_3").options[document.getElementById("cmbBudgetType0_3").selectedIndex].innerHTML+"</label>\n\
        <input type='hidden' name='cmbBudgetType"+count+"_"+(cnt)+"'  id='cmbBudgetType"+count+"_"+(cnt)+"' \n\
        value='"+document.getElementById('cmbBudgetType0_3').value+"'  />";
            td3.innerHTML = budgetType;
            tr0.appendChild(td3);

            var td4=document.createElement("td");
            td4.id="td"+count+"_"+(++cnt);
            operator = "<lable>"+document.getElementById("cmbOperator0_4").options[document.getElementById("cmbOperator0_4").selectedIndex].innerHTML+"</label> \n\
        <input type='hidden' name='cmbOperator"+count+"_"+(cnt)+"'  id='cmbOperator"+count+"_"+(cnt)+"' \n\
        value='"+document.getElementById('cmbOperator0_4').value+"'  />";
            td4.innerHTML = operator;
            tr0.appendChild(td4);


            var td5=document.createElement("td");
            td5.id="td"+count+"_"+(++cnt);
            td5.innerHTML ="<label>"+document.getElementById('txtAmount0_5').value+"</label><input type='hidden' name='txtAmount"+count+"_"+cnt+"' id='txtAmount"+count+"_"+cnt+"' \n\
        value='"+document.getElementById('txtAmount0_5').value+"'  />";
            tr0.appendChild(td5);


            document.getElementById("txtCount").value=count;
            tempCount++;
            document.getElementById("errMsg").style.display='none';
            document.getElementById("errMsg").innerHTML="";

            tbody0.appendChild(tr0);
            //tbody0.appendChild(table0);


        }

        function removeComponent(){
            var tbody0=document.getElementById("tbody0");
            count=document.getElementById("txtCount").value;
            for(var i=1;i<=count;i++){
                if(document.getElementById("checkbox"+i+"_"+0) != null){
                    if(document.getElementById("checkbox"+i+"_"+0).checked == true){
                        //alert(document.getElementById("tr0_"+i));
                        tbody0.removeChild(document.getElementById("tr0_"+i));
                    }
                }
            }
        }

      /*  function checkFinancialPower(){
            if(document.getElementById("cmbRole").options[document.getElementById("cmbRole").selectedIndex].innerHTML != "CCGP"
                &&  document.getElementById("cmbRole").options[document.getElementById("cmbRole").selectedIndex].innerHTML != "Minister"
                &&  document.getElementById("cmbRole").options[document.getElementById("cmbRole").selectedIndex].innerHTML != "Secretary"
                && document.getElementById("cmbRole").options[document.getElementById("cmbRole").selectedIndex].innerHTML != "BOD"
                && document.getElementById("cmbRole").options[document.getElementById("cmbRole").selectedIndex].innerHTML != "HOPE"
        ){
                if(document.getElementById("cmbBudgetType0_3").options[document.getElementById("cmbBudgetType0_3").selectedIndex].innerHTML == "Development"){
                    document.getElementById("errMsg").innerHTML="";
                    document.getElementById("errMsg").style.display='block';
                    document.getElementById("errMsg").innerHTML ='Please Select Budget Type .';
                    document.getElementById("cmbBudgetType0_3").value = "0";
                }
                else{
                    document.getElementById("errMsg").innerHTML="";
                    document.getElementById("errMsg").style.display='none';
                }
            }
            else{
                document.getElementById("errMsg").innerHTML="";
                document.getElementById("errMsg").style.display='none';
            }
        }*/

        function checkData(){
            // alert(document.getElementById("cmbnature0_1").value);
            var selectbox=document.getElementById("cmbnature0_1");
            //alert(selectbox.options[selectbox.selectedIndex].value);
            // alert(count);

           // count=document.getElementById("txtCount").value;
            var flag=false;
            var flagRole=false;
            var flagnature=false;
            var flagMethod=false;
            var flagBudgetType=false;
            var flagOperator=false;
            var flagAmount=false;
            var i=0;
            for( i=1;i<=count;i++){
                if(document.getElementById("cmbnature"+i+"_"+1) != null){
                    //alert(document.getElementById("cmbRole").options[document.getElementById("cmbRole").selectedIndex].value);
                    //alert(document.getElementById("cmbRole"+i+"_"+1).value);
                    <%--if(document.getElementById("cmbRole").options[document.getElementById("cmbRole").selectedIndex].value == document.getElementById("cmbRole"+i+"_"+1).value ){
                        flagRole=true;
                    }
                    else{
                        flagRole=false;
                    }--%>

                    if(document.getElementById("cmbnature0_1").options[document.getElementById("cmbnature0_1").selectedIndex].value == document.getElementById("cmbnature"+i+"_"+1).value ){
                        flagnature=true;
                    }
                    else{
                        flagnature=false;
                    }
                    if(document.getElementById("cmbMethod0_2").options[document.getElementById("cmbMethod0_2").selectedIndex].value == document.getElementById("cmbMethod"+i+"_"+2).value){
                        flagMethod=true;
                    }
                    else{
                        flagMethod=false;
                        // break;
                    }
                    if(document.getElementById("cmbBudgetType0_3").options[document.getElementById("cmbBudgetType0_3").selectedIndex].value == document.getElementById("cmbBudgetType"+i+"_"+3).value){
                        flagBudgetType=true;
                    }else{
                        flagBudgetType=false;
                        // break;
                    }
                    if(document.getElementById("cmbOperator0_4").options[document.getElementById("cmbOperator0_4").selectedIndex].value == document.getElementById("cmbOperator"+i+"_"+4).value){
                        flagOperator=true;
                    }else{
                        flagOperator=false;
                        // break;
                    }
                    if(document.getElementById("txtAmount0_5").value == document.getElementById("txtAmount"+i+"_"+5).value){
                        flagAmount=true;

                    }else{
                        flagAmount=false;
                        // break;
                    }
                }
                //alert(" asdfasdf  ::::"+flagOperator);
                if(flagnature && flagMethod && flagBudgetType && flagOperator && flagAmount){
                    flag=true;
                    break;
                }
            }
            return flag;
        }

        function checkValidData(){

            //alert(' value is ::'+document.getElementById("cmbOperator0_4").options[document.getElementById("cmbOperator0_4").selectedIndex].value);

           // var role=document.getElementById("cmbRole");
            <%--if(document.getElementById("cmbRole").options[document.getElementById("cmbRole").selectedIndex].value == 0){
                document.getElementById("errMsg").innerHTML="";
                document.getElementById("errMsg").style.display='block';
                document.getElementById("errMsg").innerHTML ='Please Select Role.';
                return false;
            }
             else if(document.getElementById("cmbMethod0_2").options[document.getElementById("cmbMethod0_2").selectedIndex].value == 0){
                document.getElementById("errMsg").innerHTML="";
                document.getElementById("errMsg").style.display='block';
                document.getElementById("errMsg").innerHTML ='Please Select Procurement Method.';
                return false;
            }--%>
            if(document.getElementById("cmbnature0_1").options[document.getElementById("cmbnature0_1").selectedIndex].value == 0){
                document.getElementById("errMsg").innerHTML="";
                document.getElementById("errMsg").style.display='block';
                document.getElementById("errMsg").innerHTML ='Please Select Category Of Procurement.';
                return false;
            }           
            else if(document.getElementById("cmbBudgetType0_3").options[document.getElementById("cmbBudgetType0_3").selectedIndex].value == 0){
                document.getElementById("errMsg").innerHTML="";
                document.getElementById("errMsg").style.display='block';
                document.getElementById("errMsg").innerHTML ='Please Select Budget Type.';
                return false;
            }
            else if(document.getElementById("cmbOperator0_4").options[document.getElementById("cmbOperator0_4").selectedIndex].value == 0){
                document.getElementById("errMsg").innerHTML="";
                document.getElementById("errMsg").style.display='block';
                document.getElementById("errMsg").innerHTML ='Please Select Operator.';
                return false;
            }
            else if(document.getElementById("txtAmount0_5").value == ""){
                document.getElementById("errMsg").innerHTML="";
                document.getElementById("errMsg").style.display='block';
                document.getElementById("errMsg").innerHTML ='Please Enter Amount.';
                return false;
            }
            else if(!checkAmount()){
                return false;
            }
            else{
                //alet()
                return true;
            }
        }

        function submitValidation(){
            //alert(tempCount);

           // alert('asdf');
           // alert(""+document.getElementById("txtCount").value);
            var j=0;
            //alert(count);
            var i=0;
            <%--for(i=1;i<=count;i++){
                 alert(i);
                 alert(i+"  == "+document.getElementById("cmbnature"+i+"_"+1).value);
                 alert(document.getElementById("cmbMethod"+i+"_"+2).value);
                 alert(document.getElementById("cmbBudgetType"+i+"_"+3).value);
                 alert(document.getElementById("cmbOperator"+i+"_"+4).value);
                 alert(document.getElementById("txtAmount"+i+"_"+5).value);
            }--%>

            if(tempCount < 1){
                document.getElementById("errMsg").innerHTML="";
                document.getElementById("errMsg").style.display='block';
                document.getElementById("errMsg").innerHTML ='Please Select Data for Financial Power.';
                return false;
            }
            else{
                if($('#frmFinance').valid()){
                    $('#btnSubmit').attr("disabled", true);
                    document.getElementById("frmFinance").action="<%=request.getContextPath()%>/GovtUserSrBean?action=<%=action%>";
                    document.getElementById("frmFinance").submit();
                    return true;
                }
            }
       }

       function checkAmount(){
           var flag=false;
           var len;

           if(document.getElementById('txtAmount0_5').value == ""){
               $('#amountMsg').html('');
               flag=false;
           }
           else{
               if($('#txtAmount0_5').val().length > 10){
                   $('#amountMsg').html('<br/>Maximum 10 digit is allowed.');
                   flag=false;
               }else{
                flag = numericDE($('#txtAmount0_5').val());
                if(!flag){
                   $('#amountMsg').html('<br/>Please enter numbers with Two decimal only.');
                }else{
                   $('#amountMsg').html('');
                }
               }
           }           
           return flag;
       }
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1"><div class="pageHead_1">Assign Financial Power</div>
                                        <%-- <div class="pageHead_1">Create User</div>--%>
                                        <%--<div class="pageHead_1">Assign Financial Power</div>--%>

                                        <div id="successMsg" class="responseMsg successMsg" style="display:none"></div>
                                        <div id="errMsg" class="responseMsg errorMsg" style="display:none"></div>
                                        <%-- <div class="responseMsg noticeMsg">Notification Msg <a href="#">Link</a></div>--%>
                                        <div class="stepWiz_1 t_space">
                                            <ul>
                                                <li>
                                                    <% long empys = 0;

                                                    empys = govSrUser.countForQuery("TblEmployeeMaster", "employeeId = "+empId);                                                    
                                                    if(empys != 0){%>
                                                    <a style="text-decoration: underline;" href="GovtUserCreation.jsp?empId=<%=empId%>&Edit=Edit" >Employee Information</a>
                                                    <%  }else{ %>
                                                    Employee Information
                                                    <% }  %>
                                                </li>
                                                <li>&gt;&gt;&nbsp;&nbsp;
                                                    <% long contOffices = 0;

                                                    contOffices = govSrUser.countForQuery("TblEmployeeOffices", "employeeId = "+empId);                                                    

                                                     if(contOffices != 0){
                                                    %>
                                                    <a style="text-decoration: underline;" href="GovtUserCreateRoleOff.jsp?empId=<%=empId%>" > Assign Procurement Roles</a>
                                                    <%  }else{ %>
                                                    Assign Procurement Roles <%  } %> </li>
<!--                                                <li class="sMenu">&gt;&gt;&nbsp;&nbsp;Assign Financial Power</li>-->
                                            </ul>
                                        </div>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="10" class="formStyle_1 t_space">
                                            <tr>
                                                <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                            </tr>
                                            <tr>
                                               <%-- <td width="5" ></td>--%>
                                                <td width="15%" class="ff"><br />Assign Financial Power : </td>
                                                <td width="85%"><br /><%=roleName%>
                                                    <input type="hidden" id="hidRoleId"  name="hidRoleId" value="<%=roleId%>"/>
                                                    <input type="hidden" id="hidFinPowerBy"  name="hidFinPowerBy" value="<%=finPowerBy%>"/>
                                                    <input type="hidden" id="hidEmpRoleId"  name="hidEmpRoleId" value="<%=empRoleId%>"/>
                                                </td>
                                            </tr>

                                        </table>
                                        <form name="frmFinance" id="frmFinance"  method="post" action="">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="10" class="formStyle_1 t_space">
                                                <tr>
                                                    <%--<td width="5" ></td>--%>
                                                    <%--<td width="10" class="ff">Assign Financial Power</td>--%>
                                                    <td height="30" class="ff">Procurement Category&nbsp; (<span>*</span>)</td>
                                                    <td class="ff">Procurement Method &nbsp;  </td>
                                                    <td class="ff">Budget Type &nbsp; (<span>*</span>) </td>
                                                    <td class="ff">Operator  &nbsp; (<span>*</span>)</td>
                                                    <td class="ff">Amount (In Million. Nu.) &nbsp; (<span>*</span>) </td>
                                                </tr>

                                                <tr if="tr0_0">
                                                    <%-- <td width="125" class="ff">Financial Power Type : <span>*</span></td>
                                                     <td><select name="powerType"  id="cmbPowerType" class="formTxtBox_1" style="width: 200px">
                                                             <option selected="selected" value="">-- Select Financial Type --</option>
                                                             <option>User specific</option>
                                                             <option>Role specific</option>
                                                         </select>
                                                     </td>--%>
                                                    <%--<td width="5" id="td0_0"></td>--%>

                                                   <%-- <td><select name="cmbRole"  id="cmbRole" class="formTxtBox_1" style="width: 200px">
                                                            <option selected="selected" value="0">-- Select Role --</option>
                                                            <c:forEach items="${govSrUser.role}" var="role" >
                                                                <option value="${role.objectId}">${role.objectValue}</option>
                                                            </c:forEach>
                                                        </select>
                                                        <%=roleName%>


                                                    </td>--%>
                                                    <td id="td0_1">
                                                         <input type="hidden" id="checkbox0_0" name="checkbox" />
                                                        <select name="nature"  id="cmbnature0_1" class="formTxtBox_1" style="width: 150px">
                                                            <option selected="selected" value="0">-- Select --</option>
                                                            <option value="1" >Goods</option>
                                                            <option value="2">Services</option>
                                                            <option value="3">Works</option>
                                                        </select>
                                                    </td>
                                                    <td id="td0_2"><select name="method"  id="cmbMethod0_2" class="formTxtBox_1" style="width: 150px">
                                                            <option selected="selected" value="0">-- Select --</option>
                                                                 <option value="1">RFQ</option>                                                                 
                                                        </select></td>
                                                        <%//for(int i=0;i<procurementMethod.size();i++){%>
                                                                 <%//procurementMethod.get(i).getProcurementMethodId()%>
                                                                 <%//procurementMethod.get(i).getProcurementMethod()%>
                                                           
                                                    <td id="td0_3"><select name="budgetType"  id="cmbBudgetType0_3" class="formTxtBox_1" style="width: 150px"  >
                                                            <option selected="selected" value="0">-- Select --</option>
                                                            <c:forEach items="${govSrUser.budgetTypes}" var="budget" >
                                                                <option value="${budget.objectId}">${budget.objectValue}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </td>
                                                    <td id="td0_4"><select name="operator"  id="cmbOperator0_4" class="formTxtBox_1" style="width: 150px">
                                                            <option selected="selected" value="0">-- Select --</option>
                                                            <option value="<" >&lt;</option>
                                                            <option value=">" >&gt;</option>
                                                            <option value="<=" >&lt;=</option>
                                                            <option value=">=" >&gt;=</option>
                                                            <option value="=" >=</option>
                                                        </select></td>
                                                        <td id="td0_5"><input type="text" name="amount" id="txtAmount0_5" onkeyup="return checkAmount();" style="width: 150px;">
                                                            <span id="amountMsg"  style="color: red; font-weight: bold">&nbsp;</span>
                                                        </td>
                                                </tr>

                                                <%--<tr><td width="5" ></td>
                                                    <td class="ff">Operator  </td>
                                                    <td class="ff">Amount (In Mn. Taka)  </td>
                                                </tr>


                                                <tr>
                                                    <td width="5" ></td>
                                                    <td id="td0_4"><select name="operator"  id="cmbOperator0_4" class="formTxtBox_1" style="width: 200px">
                                                            <option selected="selected" value="0">-- Select Operator --</option>
                                                            <option value="<" >&lt;</option>
                                                            <option value=">" >&gt;</option>
                                                            <option value="<=" >&lt;=</option>
                                                            <option value=">=" >&gt;=</option>
                                                            <option value="=" >=</option>
                                                        </select></td>
                                                        <td id="td0_5"><input type="text" name="amount" id="txtAmount0_5" onblur="return checkAmount();" style="width: 200px">
                                                            <span id="amountMsg"  style="color: red; font-weight: bold">&nbsp;</span>
                                                        </td>
                                                </tr>--%>

                                            </table>
                                                <div  class="t-align-right t_space" >
                                                    <a href="#" class="action-button-add"  name="btnAdd" id="btnAdd" onclick="addComponent();">Add</a>
                                                    <a href="#" class="action-button-delete"  name="btnRemove" id="btnRemove" onclick="removeComponent()">Remove</a>
                                               
                                                 </div>
                                                <%--<table  border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="t-align-center">
                                                    </td>
                                                </tr>
                                                </table>--%>
                                                <div class="t-align-center t_space"  >
                                                
                                                </div>

                                            <div>&nbsp;&nbsp;</div>

                                            <table width="100%" id="table0" width="80%" cellspacing="0" class="tableList_1 t_space">
                                                <tr>
                                                    <th class="t-align-left" width="5"></th>
                                                    <th class="t-align-left">Procurement Category</th>
                                                    <th class="t-align-left">Procurement Method  </th>
                                                    <th class="t-align-left">Budget Type  </th>
                                                    <th class="t-align-left">Operator  </th>
                                                    <th class="t-align-left">Amount (In Million. Nu.) </th>
                                                </tr>
                                                <tbody id="tbody0"  >
                                                    <% int count = 0;
                                                    short tmpDeptId=0;
                                                    int tmpEmpRoleId=0;
                                                    //java.util.Formatter fmt = new java.util.Formatter();
                                                    //java.text.DecimalFormat fmt = new java.text.DecimalFormat("#.##");
                                                    if(vwFinancePower != null)
                                                    for (VwEmpfinancePower financePower : vwFinancePower) {
                                                            count++;
                                                        int cnt = -1;                                                        
                                                        if(count == 1){
                                                            tmpEmpRoleId = financePower.getId().getEmployeeRoleId();
                                                        }
                                                        if(tmpEmpRoleId == financePower.getId().getEmployeeRoleId()){

                                                    %>
                                                    <tr  id="tr0_<%=count%>" >
                                                        <td id="td<%=count%>_<%=(++cnt)%>" >
                                                            <input type="checkbox"  name="checkbox"
                                                                   id="checkbox<%=count%>_<%=cnt%>" />
                                                        </td>
                                                        <td id="td<%=count%>_<%=(++cnt)%>" >
                                                            <label ><%=financePower.getId().getProcurementNature()%></label>
                                                            <input type="hidden"  name="cmbnature<%=count%>_<%=cnt%>"
                                                                   id="cmbnature<%=count%>_<%=cnt%>" value="<%=financePower.getId().getProcurementNatureId()%>"/>
                                                        </td>
                                                        <td id="td<%=count%>_<%=(++cnt)%>" >
                                                            <label ><%=financePower.getId().getProcurementMethod()%></label>
                                                            <input type="hidden"  name="cmbMethod<%=count%>_<%=cnt%>"
                                                                   id="cmbMethod<%=count%>_<%=cnt%>" value="<%=financePower.getId().getProcurementMethodId()%>"/>
                                                        </td>
                                                        <td class="t-align-center" id="td<%=count%>_<%=(++cnt)%>" >
                                                            <label ><%=financePower.getId().getBudgetType()%></label>
                                                            <input type="hidden"  name="cmbBudgetType<%=count%>_<%=cnt%>"
                                                                   id="cmbBudgetType<%=count%>_<%=cnt%>" value="<%=financePower.getId().getBudgetTypeId()%>"/>
                                                        </td>
                                                        <td class="t-align-center" id="td<%=count%>_<%=(++cnt)%>" >
                                                            <label><%=financePower.getId().getOperator()%></label>
                                                            <input type="hidden"  name="cmbOperator<%=count%>_<%=cnt%>"
                                                                   id="cmbOperator<%=count%>_<%=cnt%>" value="<%=financePower.getId().getOperator()%>"/>
                                                        </td>
                                                        <td class="t-align-center" id="td<%=count%>_<%=(++cnt)%>" >
                                                            <label><%=String.format("%.2f",financePower.getId().getAmount())%></label>
                                                            <input type="hidden"  name="txtAmount<%=count%>_<%=cnt%>"
                                                                   id="txtAmount<%=count%>_<%=cnt%>" value="<%=financePower.getId().getAmount()%>"/>
                                                        </td>
                                                    </tr>
                                                    <%
                                                            }
                                                        }
                                                    %>
                                                </tbody>
                                            </table>
                                            <!-- Buttons -->
                                            <div class="t-align-center t_space"  >
                                                 <label class="formBtn_1">
                                                    <input type="submit" name="btnSubmit" id="btnSubmit" value="Submit"  onclick="return submitValidation();" />
                                                 </label>
                                                        &nbsp;
                                                        <%if(vwFinancePower.size() > 0) {%>
                                                        <%--<label class="formBtn_1">
                                                            <input type="button" name="btnSubmission" id="btnSubmission" value="Final Submission" onclick="finalSubmission();" />
                                                        </label>--%>
                                                        <%}%>
                                                        <input type="hidden" name="txtCount" id="txtCount" value="<%=count%>" />
                                                        <input type="hidden" name="empId" id="empId" value="<%=govSrUser.getEmpId()%>" />


                                            </div>

                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <div>&nbsp;</div>

                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
                <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<form id="form1" method="post">
     <%if(object != null) {%>
    <input type="hidden" name="hidEmailId" id="hidEmailId" value="<%=(String) object[0]%>" />
   <input type="hidden" name="hidPassword" id="hidPassword" value="<%=(String) object[1]%>" />
    <input type="hidden" name="hidMobNo" id="hidMobNo" value="<%=(String) object[4]%>" />
    <%}%>
</form>
<script>

 tempCount = <%=count%>;

function finalSubmission(){
    var flag=false;
     //$.post("<%=request.getContextPath()%>/GovtUserSrBean", {userId:<%//userId%>,empId:<%=empId%>,action:'finalSubmission'}, function(j){
     //       alert(j);
     //       flag=true;
     // });
      document.getElementById("form1").action="<%=request.getContextPath()%>/GovtUserSrBean?userId=<%=userId%>&empId=<%=empId%>&action=finalSubmission";
      document.getElementById("form1").submit();
}

</script>
<%
            govSrUser = null;
            projSrUser = null;
%>
