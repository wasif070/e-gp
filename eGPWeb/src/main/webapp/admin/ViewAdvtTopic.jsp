<%-- 
    Document   : ViewAdvtTopic
    Created on : Jan 24, 2013, 3:00:03 PM
    Author     : spandana.vaddi
--%>



<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblWsMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.WsMasterService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <title>Advertisement Details</title>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>

    <script type="text/javascript">
        jQuery().ready(function (){
            jQuery("#list").jqGrid({
                url: '<%if (request.getParameter("funcName") != null && request.getParameter("funcName").equalsIgnoreCase("Approved")) {%>../MakeAdvertisement?funcName=Approved<%} else {%>../MakeAdvertisement?funcName=Pending<%}%>',
                datatype: "xml",
                height: 250,
                colNames:['Advertisement Id','User Name','Description','Status','publishing time period','Action'],
                colModel:[
                    {name:'advtUserAdId',index:'advtUserAdId', width:100,sortable:true,search: true, searchoptions: { sopt: ['eq'] }},
                    {name:'advtUserUserName',index:'advtUserUserName', width:100,sortable:true,search: true, searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'advtUserDescription',index:'advtUserDescription', width:100,sortable:true,search: true, searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'advtUserPaymentStatus',index:'advtUserPaymentStatus', width:40,sortable:true,search: true,align:'center', searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'advtUserPublishingTimePeriod',index:'advtUserPublishingTimePeriod', width:40,sortable: false,search: false,align:'center'},
                    {name:'advtUserAction',index:'advtUserAction', width:100,sortable:false, search: false,align:'center'},
                ],
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum:10,
                rowList:[10,20,30],
                pager: $("#page"),
                caption: '<%if (request.getParameter("funcName") != null && request.getParameter("funcName").equalsIgnoreCase("Approved")) {%>View published list<%} else {%>View pending list<%}%>',
                gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
            }).navGrid('#page',{edit:false,add:false,del:false,search:true});//close Jquery.reay()
        });//close Jquery.ready()

        function conform()
            {
                if (confirm("Do you want to delete this configuration rule?"))
                    return true;
                else
                    return false;
            }
    </script>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                       <span class="c-alignment-right"><a href="../contentPublicForumBoard.jsp" class="action-button-goback">Go Back</a></span>
                        <td class="contentArea">
                                <div class="pageHead_1">
                                   <%if(request.getParameter("funcName")!=null && request.getParameter("funcName").equalsIgnoreCase("Approved")){%>View published list<%}else{%>View pending list<%}%>
                                </div>
                             <div id="resultDiv">
                                 <%
                                            if (request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("succ")) {%>
                                            <div align="left" id="sucMsg" class="responseMsg successMsg">Advertisement published successfully.</div><br/>
                                 <% }%>
                            <ul class="tabPanel_1 t_space">
                                <li><a href="../admin/ViewAdvtTopic.jsp?funcName=Pending" id="linkPending" <%if (request.getParameter("funcName") != null && request.getParameter("funcName").equalsIgnoreCase("Approved")) {%>class="" <%} else {%>class="sMenu"<%}%>>Pending</a></li>
                                <li><a href="../admin/ViewAdvtTopic.jsp?funcName=Approved" id="linkApproved" <%if (request.getParameter("funcName") != null && request.getParameter("funcName").equalsIgnoreCase("Approved")) {%> class="sMenu"<%}%> >Published</a></li>
                            </ul>
                            <span class="c-alignment-right"><a href="../MakeAdvertisements.jsp" class="action-button-add" >Add new Advertisement</a></span>
                            <div id="resultDiv" class="tabPanelArea_1">
                                <div class="t-align-left ff formStyle_1 t_space">
                                    To sort click on the relevant column header
                                </div>
                                <table id="list"></table>
                                <div id="page">
                                </div>
                            </div>
                        </div>
                    </td>
                    </tr>
                </table>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
