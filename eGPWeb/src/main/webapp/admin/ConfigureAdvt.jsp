<%--
    Document   : ConfigureAdvt
    Created on : Jan 24, 2013, 12:59:18 PM
    Author     : spandana.vaddi
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.AdvertisementService"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AdvtConfigMasterService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblAdvtConfigurationMaster"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="com.lowagie.text.Document"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Advertisement Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="../ckeditor/adapters/jquery.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#button').attr("disabled", false);
                $("#advtConfig").validate({
                    rules:{
                        bannerSize:{required:true,digits: true,numberZero: true},
                        dimension1:{required:true,digits: true,max:300,numberZero: true},
                        dimension2:{required:true,digits: true,max:100,numberZero: true},
                        durationt1:{digits: true},
                        durationt2:{digits: true},
                        durationt3:{digits: true},
                        fileFormat:{required:true,digits: false},
                        instructions:{required:true}
                    },
                    messages:{
                        bannerSize:{required:"<div class='reqF_1'>Please enter banner Size</div>",
                            digits:"<div class='reqF_1'>No alphabets/decimals allowed</div>",
                            numberZero:"<div class='reqF_1'>value 0 is not allowed</div>"
                        },
                        dimension1:{

                        },
                        dimension2:{
                            required:"<div class='reqF_1'>Please enter dimension</div>",
                            digits:"<div class='reqF_1'>No alphabets/decimals allowed</div>",
                            max:"<div class='reqF_1'>Please enter a value less than or equal to 100</div>",
                            numberZero:"<div class='reqF_1'>value 0 is not allowed</div>"
                        },
                        durationt1:{
                            digits:"<div class='reqF_1'>No alphabets/decimals allowed</div>"
                        },
                        durationt2:{
                            digits:"<div class='reqF_1'>No alphabets/decimals allowed</div>"
                        },
                        durationt3:{
                            digits:"<div class='reqF_1'>No alphabets/decimals allowed</div>"
                        },
                        fileFormat:{
                            required:"<div class='reqF_1'>Please select file Format</div>",
                            digits:"<div class='reqF_1'>Please enter alphabets</div>"
                        }
                    }
                });
                return false;  });
        </script>

        <script type="text/javascript" language="javascript">
            function enableTextBox()
    {
        if (document.getElementById("duration1").checked == true)
             document.getElementById("durationt1").disabled = false;
         else{
             document.getElementById("durationt1").value="0";
             document.getElementById("durationt1").disabled = true;
               }
         if (document.getElementById("duration2").checked == true)
             document.getElementById("durationt2").disabled = false;
         else{
              document.getElementById("durationt2").value="0";
             document.getElementById("durationt2").disabled = true;
         }
         if (document.getElementById("duration3").checked == true)
             document.getElementById("durationt3").disabled = false;
         else{
              document.getElementById("durationt3").value="0";
             document.getElementById("durationt3").disabled = true;
         }

    }
            function check(){
                 var loc= document.getElementById("location").value;
                 //alert("loc"+loc);
                var valid = true;
                if(document.getElementById("durationt1").disabled && document.getElementById("durationt2").disabled && document.getElementById("durationt3").disabled)
                {
                    document.getElementById("myspan").innerHTML="please select duration";
                    valid = false;
                }
                if((document.getElementById("duration1").checked == true && document.getElementById("durationt1").value=="")||(document.getElementById("duration2").checked == true && document.getElementById("durationt2").value=="")||(document.getElementById("duration3").checked == true && document.getElementById("durationt3").value=="")){
                    document.getElementById("myspan").innerHTML="please enter duration value";
                    valid = false;
                }
                if($.trim($("#instructions").val()).length > 2000) {
                    document.getElementById("s_textarea").innerHTML="Maximum 2000 characters are allowed";
                    valid = false;
                }
                if(CKEDITOR.instances.instructions.getData() == 0){
                    document.getElementById("s_textarea").innerHTML="Please enter Comments.";
                    valid = false;
                }
                if(CKEDITOR.instances.instructions.getData().length > 2000)
                {
                    document.getElementById("s_textarea").innerHTML="Maximum 2000 characters are allowed";
                    valid = false;
                }
                var d1="0",d2="0",d3="0";
                     d1=document.getElementById("durationt1").value;
                     d2=document.getElementById("durationt2").value;
                     d3=document.getElementById("durationt3").value;
                    var d=d1+"days,"+d2+"months,"+d3+"years";
                    if(d1=="0" && d2=="0" && d3=="0"){
                        document.getElementById("myspan").innerHTML="please select duration";
                        valid = false;
                    }else{
                    $.post("<%=request.getContextPath()%>/AdvertisementConfig", {funcName:'location',location:document.getElementById("location").value,duration:d},  function(j){
                        var str=j;
                        //alert("..str...;."+j);
                        if(str=="fail"){
                            document.getElementById("myspan").innerHTML="The combination of given location and duration already exists";
                        }else{
                            document.getElementById("myspan").innerHTML="";
                        }
                    });
                    }
                    if(document.getElementById("myspan").innerHTML=="The combination of given location and duration already exists")
                        {valid=false;}
                 if(valid == false){
                   return false;
                }
            }
            function resetTextArea()
            {
                CKEDITOR.instances.instructions.setData('');
            }
            function conform()
            {
                if (confirm("Do you want to delete this configuration rule?"))
                    return true;
                else
                    return false;
            }
        </script>

    </head>
    <body onload="enableTextBox();">
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                <tr valign="top">
                    <%@include file="../LeftPanelForForum.jsp"  %>
                    <!--Dashboard Content Part End-->
                    <td class="contentArea_1" style="vertical-align: top; height: 400px;" align="center" valign="middle">
                        <div class="pageHead_1" align="left">Advertisement Configuration
                            <span class="c-alignment-right"><a href="/contentPublicForumBoard.jsp" class="action-button-goback">Go Back</a></span>
                        </div>
                        <form id="advtConfig" method="post" action ="/AdvertisementConfig" onsubmit="return check();">
                            <input type="hidden" name="adConfigId" id="adConfigId" value=""/>
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td style="font-style: italic" class="ff t-align-left" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                </tr>
                                <tr>
                                    <td class="ff t-align-left" width="23%" >Banner location : <span class="mandatory">*</span></td>
                                    <td class="ff t-align-left">
                                        <select id="location" name="location">
                                            <option value="TopLeft">TopLeft</option>
                                            <option value="Bottom">Bottom</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff t-align-left" width="23%">Banner size (in KB) : <span class="mandatory">*</span></td>
                                    <td class="ff t-align-left">
                                        <input name="bannerSize" type="text" class="formTxtBox_1" id="bannerSize" style="width:195px;" value=""/>
                                        <span id="tfsinmb" class="reqF_1" ></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff t-align-left" width="23%">Banner dimension : <span class="mandatory">*</span></td>
                                    <td class=" t-align-left">
                                        <input name="dimension1" type="text" class="formTxtBox_1" id="dimension1" style="width:40px;" value=""/>
                                        <span class="ff" >X</span>
                                        <input name="dimension2" type="text" class="formTxtBox_1" id="dimension2" style="width:40px;" value=""/>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td class="ff t-align-left" width="23%">file formats : <span class="mandatory">*</span></td>
                                    <td class="ff t-align-left">
                                        <input name="fileFormat" type="text" class="formTxtBox_1" id="fileFormat" style="width:195px;" value=".jpeg,.gif,.bmp,.psd,.png"/>
                                        <span id="tfsinmb" class="reqF_1" ></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff t-align-left" width="23%">Duration : <span class="mandatory">*</span></td>
                                    <td class="ff t-align-left" >
                                        <table><tr><td>  Days</td><td><input name="duration1" type="checkbox" class="" id="duration1" style="width:95px;" onclick="enableTextBox();"/></td>
                                                <td><input name="durationt1" type="text" class="formTxtBox_1" id="durationt1" style="width:195px;" value="0"/></td></tr>
                                            <tr><td>  Months</td><td><input name="duration2" type="checkbox" class="" id="duration2" style="width:95px;" onclick="enableTextBox();"/></td>
                                                <td><input name="durationt2" type="text" class="formTxtBox_1" id="durationt2" style="width:195px;" value="0"/></td></tr>
                                            <tr><td>  Years</td><td><input name="duration3" type="checkbox" class="" id="duration3" style="width:95px;" onclick="enableTextBox();"/></td>
                                                <td><input name="durationt3" type="text" class="formTxtBox_1" id="durationt3" style="width:195px;" value="0"/></td></tr></table>
                                        <div id="myspan" class="reqF_1" style="font-size:12"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff t-align-left" width="23%">Instructions to user : <span class="mandatory">*</span></td>
                                    <td class=" t-align-left">
                                        <textarea cols="100" rows="5" name="instructions" id="instructions"  class="formTxtBox_1"></textarea>
                                        <script type="text/javascript">
                                            CKEDITOR.replace( 'instructions',
                                            {
                                                fullPage : false
                                            });
                                        </script>
                                        <span id="s_textarea" style="color: red; ">&nbsp;</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">&nbsp;</td>
                                    <td class="t-align-left">
                                        <label class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Submit" />
                                        </label>
                                        <label class="formBtn_1">
                                            <input type="reset" name="reset" id="reset" value="Reset" onclick="resetTextArea();"/>
                                        </label>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <table width="75%" cellspacing="0" id="resultTable" class="tableList_3 t_space" cols="@0,3" align="center">
                            <tr>
                                <th class="t-align-center">Sl. No.</th>
                                <th class="t-align-center">Banner location</th>
                                <th class="t-align-center">Banner size</th>
                                <th class="t-align-center">Banner dimension</th>
                                <th class="t-align-center">File formats</th>
                                <th class="t-align-center">Instructions</th>
                                <th class="t-align-center">Duration</th>
                                <th class="t-align-center">Action</th>
                            </tr>
                            <%
                                        try{
                                        AdvtConfigMasterService advtConfigMasterService = (AdvtConfigMasterService) AppContext.getSpringBean("advtconfigMasterService");
                                        List<TblAdvtConfigurationMaster> list = advtConfigMasterService.getAllConfigMaster();
                                        int i = 0;
                                        String styleclass="";
                                        for (TblAdvtConfigurationMaster tbl : list) {
                                            i++;
                                            if(i%2==0)
                                                styleclass="background-color:#E9F9DB";
                                            else
                                                styleclass="";
                            %>
                            <tr style="<%=styleclass%>">
                                <td><%= i%></td>
                                <td><%=tbl.getLocation()%></td>
                                <td><%=tbl.getBannerSize()%></td>
                                <td><%=tbl.getDimensions()%></td>
                                <td><%=tbl.getFileFormat()%></td>
                                <td><%=tbl.getInstructions()%></td>
                                <td><%=tbl.getDuration()%></td>
                                <td><a href="../AdvertisementConfig?action=delete&id=<%=tbl.getAdConfigId()%>" onclick='return conform();'>Delete</a></td>
                            </tr>
                            <%}} catch (Exception exp) {
                                exp.printStackTrace();
                            }%>
                        </table>
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <%@include file="/resources/common/Bottom.jsp" %>
        </div>
    </body>
</html>