<%-- 
    Document   : Adment
    Created on : Nov 27, 2010, 12:38:29 PM
    Author     : Naresh.Akurathi
--%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%
            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            Object objUName = session.getAttribute("userName");
            boolean isLoggedIn = false;
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userId").toString();
            }
            if (objUName != null) {
                isLoggedIn = true;
            }
%>

<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigAmendment"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <%@page buffer="15kb"%>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Amendment Business Rule</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>


        <script type="text/javascript">
            function validate()
            {
                var count=document.getElementById("TotRule").value;
                var flag=true;
                if(document.getElementById("hiddenaction"))
                {
                    if("edit"==document.getElementById("hiddenaction").value)
                    {
                    $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/BusinessRuleConfigurationServlet",
                        data:"configAmendmentId="+$('#hiddenid').val()+"&"+"procType="+$('#procType_1').val()+"&"+"procMethod="+$('#procMethod_1').val()+"&"+"funName=AmendmentRule",
                        async: false,
                        success: function(j){

                            if(j.toString()!="0"){
                                flag=false;
                                jAlert("Combination of 'ProcurementType' and 'ProcurementMethod' already exist.","Amendment Business Rule Configuration", function(RetVal) {
                                });
                            }
                        }
                    });
                    }
                }
                for(var k=1;k<=count;k++)
                {
                    if(document.getElementById("amendPercent_"+k) != null)
                    {
                        if(document.getElementById("amendPercent_"+k).value=="")
                        {
                            document.getElementById("AmendPercent_"+k).innerHTML="<br/>Please specify % of total time allowed for publishing Corrigendum / Amendment";
                            flag=false;
                        }
                        else
                        {
                            if(digits(document.getElementById("amendPercent_"+k).value))
                            {
                            
                                if(!ChkMaxLength(document.getElementById("amendPercent_"+k).value))
                                {
                                    //document.getElementById("AmendPercent_"+k).innerHTML="<br/> % of time must not exceed 100.";
                                    document.getElementById("AmendPercent_"+k).innerHTML="<br/> Amendment Rules configuration % of time must not exceed 100.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("AmendPercent_"+k).innerHTML="";
                                }
                            }
                            else
                            {
                                document.getElementById("AmendPercent_"+k).innerHTML="<br/>Please enter numbers only.";
                                flag=false;
                            }
                        }
                    }
                    if(document.getElementById("amendMinDays_"+k) !=null)
                    {
                        if(document.getElementById("amendMinDays_"+k).value=="")
                        {
                            document.getElementById("AmendMinDays_"+k).innerHTML="<br/>Please specify minimum no. of days by which Corrigendum / Amendment should be extended";
                            flag=false;
                        }
                        else
                        {
                            if(digits(document.getElementById("amendMinDays_"+k).value))
                            {
                                if(!ChkMaxLen(document.getElementById("amendMinDays_"+k).value))
                                {
                                    document.getElementById("AmendMinDays_"+k).innerHTML="<br/>Maximum 3 numbers  without decimal are allowed.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("AmendMinDays_"+k).innerHTML="";

                                }
                            }
                            else
                            {
                                document.getElementById("AmendMinDays_"+k).innerHTML="<br/>Please enter numbers only.";
                                flag=false;
                            }
                        }
                    }
                }
                // Start OF--Checking for Unique Rows
                if(document.getElementById("TotRule")!=null){
                    var totalcount = eval(document.getElementById("TotRule").value);} //Total Count After Adding Rows
                //alert(totalcount);
                var chk=true;
                var i=0;
                var j=0;
                for(i=1; i<=totalcount; i++) //Loop For Newly Added Rows
                {
                    if(document.getElementById("amendPercent_"+i)!=null)
                    if($.trim(document.getElementById("amendPercent_"+i).value)!='' && $.trim(document.getElementById("amendMinDays_"+i).value) !='') // If Condition for When all Data are filled thiscode is Running
                    {

                        for(j=1; j<=totalcount && j!=i; j++) // Loop for Total Count but Not same as (i) value
                        {
                            chk=true;
                            //If Condition for Check Duplicate Rows are there or not.If Columns are diff then chk variable set to false
                            //IF Row is same Give alert message.
                            if($.trim(document.getElementById("procType_"+i).value) != $.trim(document.getElementById("procType_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("procMethod_"+i).value) != $.trim(document.getElementById("procMethod_"+j).value))
                            {
                                chk=false;
                            }
                            /*     if($.trim(document.getElementById("amendPercent_"+i).value) != $.trim(document.getElementById("amendPercent_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("amendMinDays_"+i).value) != $.trim(document.getElementById("amendMinDays_"+j).value))
                            {
                                chk=false;
                            }
                             */
                            if(flag){
                            if(chk==true) //If Row is same then give alert message
                            {
                                    jAlert("Duplicate record found. Please enter unique record","Amendment Business Rule Configuration",function(RetVal) {
                                    });
                                return false;
                            }
                        }
                        }

                    }
                }
                // End OF--Checking for Unique Rows

                if (flag==false){

                    return false;
                }
            };

            function ChkMaxLength(value)
            {
                var ValueChk=value;
                //alert(ValueChk);
                if(ValueChk>100)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            function ChkMaxLen(value)
            {
                var ValChk=value;
                //alert('ok');
                if(ValChk.length>3)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            function digits(value)
            {
                return /^\d+$/.test(value);
            }

            function ChkamendPercent(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk=true;
                if(obj.value!='')
                {
                    if(digits(obj.value))
                    {
                        if(!ChkMaxLength(obj.value))
                        {
                            //document.getElementById("AmendPercent_"+i).innerHTML="<br/> % of time must not exceed 100.";
                            document.getElementById("AmendPercent_"+i).innerHTML="<br/> Amendment Rules configuration % of time must not exceed 100.";
                            chk=false;
                        }
                        else
                        {
                            document.getElementById("AmendPercent_"+i).innerHTML="";
                        }
                        
                        //document.getElementById("AmendPercent_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("AmendPercent_"+i).innerHTML="<br/>Please enter numbers only.";
                        chk=false;
                    }
                }
                else
                {
                    document.getElementById("AmendPercent_"+i).innerHTML="<br/>Please specify % of total time allowed for publishing Corrigendum / Amendment";
                    chk=false;
                }
                if(chk==false)
                {
                    return false;
                }

            };

            function ChkamendMinDays(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk1=true;

                if(obj.value!='')
                {
                    if(digits(obj.value))
                    {
                        if(!ChkMaxLen(obj.value))
                        {
                            document.getElementById("AmendMinDays_"+i).innerHTML="<br/>Maximum 3 numbers  without decimal are allowed.";
                            chk1=false;
                        }
                        else
                        {
                            document.getElementById("AmendMinDays_"+i).innerHTML="";
                        }
                        //document.getElementById("AmendMinDays_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("AmendMinDays_"+i).innerHTML="<br/>Please enter numbers only.";
                        chk1=false;
                    }
                }
                else
                {
                    document.getElementById("AmendMinDays_"+i).innerHTML="<br/>Please specify minimum no. of days by which Corrigendum / Amendment should be extended";
                    chk1=false;
                }
                if(chk1==false)
                {
                    return false;
                }
            }
        </script>

        <%!    ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();

        %>


        <%
                    StringBuilder tenderType = new StringBuilder();
                    StringBuilder procNature = new StringBuilder();
                    StringBuilder procType = new StringBuilder();
                    StringBuilder procMethod = new StringBuilder();
                    configPreTenderRuleSrBean.setLogUserId(strUserTypeId);

                    TblProcurementMethod tblProcurementMethod2 = new TblProcurementMethod();
                    Iterator pmit2 = configPreTenderRuleSrBean.getProcurementMethod().iterator();
                    while (pmit2.hasNext()) {
                        tblProcurementMethod2 = (TblProcurementMethod) pmit2.next();
                        if("OTM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                            "LTM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                            "DP".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                           // "LEQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                          //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                            "RFQ".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                           // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                          //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                            "FC".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                           // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                            "DPM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())||
                             "QCBS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "LCS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "SFB".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "SBCQ".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "SSS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "IC".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()))
                         {
                             if(tblProcurementMethod2.getProcurementMethod().equals("RFQ"))
                            {
                                procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>LEM</option>");

                            }
                            else if(tblProcurementMethod2.getProcurementMethod().equals("DPM"))
                            {
                                procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>DCM</option>");

                            }
                            else
                            {
                                procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>" + tblProcurementMethod2.getProcurementMethod() + "</option>");

                            }
                            
                         }
                    }

                    TblProcurementTypes tblProcurementTypes2 = new TblProcurementTypes();
                    Iterator ptit2 = configPreTenderRuleSrBean.getProcurementTypes().iterator();
                    while (ptit2.hasNext()) {
                        tblProcurementTypes2 = (TblProcurementTypes) ptit2.next();
                        if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("NCT"))
                        {
                            procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>NCB</option>");
                        }
                        else if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("ICT"))
                        {
                            procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>ICB</option>");
                        }
                        else
                        {
                            procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + tblProcurementTypes2.getProcurementType() + "</option>");
                        }
                    }


        %>


        <script type="text/javascript">

            var counter = 1;
            var delCnt = 0;
            $(function() {
                $('#linkAddRule').click(function() {
                    counter = eval(document.getElementById("TotRule").value);
                    if(document.getElementById("delrow")!=null)
                    {
                        delCnt = eval(document.getElementById("delrow").value);
                    }
                   
                    $('span.#lotMsg').css("visibility","collapse");
                    var htmlEle = "<tr id='trrule_"+ (counter+1) +"'>"+
                        "<td class='t-align-center'><input type='checkbox' name='checkbox"+(counter+1)+"' id='checkbox_"+(counter+1)+"' value='"+(counter+1)+"' /></td>"+
                        "<td class='t-align-center'><select name='procType"+(counter+1)+"' class='formTxtBox_1' id='procType_"+(counter+1)+"'><%=procType%></select></td>"+
                        "<td class='t-align-center'><select name='procMethod"+(counter+1)+"' class='formTxtBox_1' id='procMethod_"+(counter+1)+"'><%=procMethod%></select></td>"+
                        "<td class='t-align-center'><input name='amendPercent"+(counter+1)+"' type='text' class='formTxtBox_1' id='amendPercent_"+(counter+1)+"' style='width:90%;' onBlur='return ChkamendPercent(this);'/><span id='AmendPercent_"+ (counter+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><input name='amendMinDays"+(counter+1)+"' type='text' class='formTxtBox_1' id='amendMinDays_"+(counter+1)+"' style='width:90%;'  onBlur='return ChkamendMinDays(this);'/><span id='AmendMinDays_"+(counter+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "</tr>";
                    $("#tblrule").append(htmlEle);
                    document.getElementById("TotRule").value = (counter+1);
                    
                });
            });

            $(function() {
                $('#linkDelRule').click(function() {
                    counter = eval(document.getElementById("TotRule").value);
                    delCnt = eval(document.getElementById("delrow").value);

                    var tmpCnt = 0;
                    for(var i=1;i<=counter;i++){
                        if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                            tmpCnt++;
                        }
                    }

                    if(tmpCnt==(eval(counter-delCnt))){
                        $('span.#lotMsg').css("visibility","visible");
                        $('span.#lotMsg').css("color","red");
                        $('span.#lotMsg').html('Minimum 1 record is needed!');
                    }else{
                        if(tmpCnt>0){jConfirm('Are You Sure You want to Delete.','Procurement Method Business Rule Configuration', function(ans) {if(ans){
                            for(var i=1;i<=counter;i++){
                                if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                                    $("tr[id='trrule_"+i+"']").remove();
                                    $('span.#lotMsg').css("visibility","collapse");
                                    delCnt++;
                                }
                            }
                            document.getElementById("delrow").value = delCnt;
                        }});}else{jAlert("please Select checkbox first","Procurement Method Business Rule Configuration", function(RetVal) {});}
                    }

                });
            });
        </script>

    </head>

    <body>

        <%
                    String msg = "";
                    int cnt = 0;
                    if ("Submit".equals(request.getParameter("button")) || "Update".equals(request.getParameter("button"))) {
                        int row = Integer.parseInt(request.getParameter("TotRule"));
                        if ("edit".equals(request.getParameter("action"))) {
                            msg = "Not Deleted";
                        } else {
                            msg = configPreTenderRuleSrBean.delAllConfigAmendment();
                        }

                        if (msg.equals("Deleted")) {
                           String action = "Add Amendment Rules";
                           try{
                                for (int i = 1; i <= row; i++) {
                                    if (request.getParameter("procType" + i) != null) {
                                        byte ptypes = Byte.parseByte(request.getParameter("procType" + i));
                                        byte pmethod = Byte.parseByte(request.getParameter("procMethod" + i));
                                        float amendPercent = Float.parseFloat(request.getParameter("amendPercent" + i));
                                        short amendMinDays = Short.parseShort(request.getParameter("amendMinDays" + i));

                                        TblConfigAmendment configAmend = new TblConfigAmendment();
                                        configAmend.setTblProcurementTypes(new TblProcurementTypes(ptypes));
                                        configAmend.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                        configAmend.setAmendPercent(new BigDecimal(amendPercent));
                                        configAmend.setAmendMinDays(amendMinDays);


                                        msg = configPreTenderRuleSrBean.addConfigAmendment(configAmend);

                                    }
                                }
                            }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                            }finally{
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                                action=null;
                            }
                        } else {
                           String action = "Edit Amendment Rules";
                           try{
                                for (int i = 1; i <= row; i++) {
                                    if (request.getParameter("procType" + i) != null) {
                                        byte ptypes = Byte.parseByte(request.getParameter("procType" + i));
                                        byte pmethod = Byte.parseByte(request.getParameter("procMethod" + i));
                                        float amendPercent = Float.parseFloat(request.getParameter("amendPercent" + i));
                                        short amendMinDays = Short.parseShort(request.getParameter("amendMinDays" + i));

                                        TblConfigAmendment configAmend = new TblConfigAmendment();
                                        configAmend.setConfigAmendmentId(Integer.parseInt(request.getParameter("id")));
                                        configAmend.setTblProcurementTypes(new TblProcurementTypes(ptypes));
                                        configAmend.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                        configAmend.setAmendPercent(new BigDecimal(amendPercent));
                                        configAmend.setAmendMinDays(amendMinDays);


                                        msg = configPreTenderRuleSrBean.updateConfingAmendment(configAmend);

                                    }
                                }
                           }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                            }finally{
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                                action=null;
                            }
                        }
                        if (msg.equals("Values Added")) {
                            msg = "Amendment Business Rule Configured successfully";
                            response.sendRedirect("AmendmentRuleView.jsp?msg=" + msg);
                        }
                        if (msg.equals("Updated")) {
                            msg = "Amendment Business Rule Updated successfully";
                            response.sendRedirect("AmendmentRuleView.jsp?msg=" + msg);
                        }

                    }
        %>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
                        <div class="pageHead_1">Amendment Business Rule Configuration</div>

                        <div style="font-style: italic;" class="t-align-right"><normal>Fields marked with (<span class="mandatory">*</span>) are mandatory</normal></div>

                        <%
                                    if (request.getParameter("id") == null) {
                        %>

                        <div align="right" class="t_space b_space">
                            <span id="lotMsg" style="color: red; font-weight: bold; float: left; visibility: collapse;">&nbsp;</span>
                            <a id="linkAddRule"class="action-button-add">Add Rule</a> <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a>

                        </div>
                        <%}%>
                        <div>&nbsp;</div>

                        <form action="AmendmentRuleDetails.jsp" method="post">
                            <%--<div class="pageHead_1" align="center"> <%=msg%></div>--%>
                            <table width="100%" cellspacing="0" class="tableList_1" id="tblrule" name="tblrule">
                                <tr>
                                    <th >Select</th>
                                    <th width="16%" >Procurement Type <br />(<span class="mandatory">*</span>)</th>
                                    <th width="18%" >Procurement Method <br />(<span class="mandatory">*</span>)</th>
                                    <th width="36%" >Time remaining in % of Total Tender Preparation time (Tender Closing Date - Tender Publication Date)<br />(<span class="mandatory">*</span>)</th>
                                    <th width="30%" >Specify minimum no. of days by which Submission Date should be extended
                                    <br />(<span class="mandatory">*</span>)</th>
                                </tr>



                                <%
                                            List lst = null;

                                            if (request.getParameter("id") != null && "edit".equals(request.getParameter("action"))) {
                                                String action = request.getParameter("action");
                                                int id = Integer.parseInt(request.getParameter("id"));
                                                %>
                                                <input type="hidden" id="hiddenaction" name="action" value="<%=action%>"/>
                                                <input type="hidden" id="hiddenid" name="id" value="<%=id%>"/>
                                                <%
                                                lst = configPreTenderRuleSrBean.getConfigAmendment(id);
                                            } else
                                                    lst = configPreTenderRuleSrBean.getConfigAmendment();



                                            int i = 0;
                                            TblProcurementMethod tblProcurementMethod = new TblProcurementMethod();
                                            List configten = configPreTenderRuleSrBean.getProcurementMethod();
                                            TblProcurementTypes tblProcurementTypes = new TblProcurementTypes();
                                            List configpretend = configPreTenderRuleSrBean.getProcurementTypes();
                                            if (!lst.isEmpty()) {
                                                TblConfigAmendment configamd = new TblConfigAmendment();
                                                Iterator it = lst.iterator();

                                                List getProcMethod = configPreTenderRuleSrBean.getProcurementMethod();
                                                List getProcType = configPreTenderRuleSrBean.getProcurementTypes();

                                                while (it.hasNext()) {

                                                    configamd = (TblConfigAmendment) it.next();
                                                    cnt = i++;

                                                    StringBuilder pType = new StringBuilder();
                                                    StringBuilder pMethod = new StringBuilder();

                                                    Iterator pmit = getProcMethod.iterator();
                                                    while (pmit.hasNext()) {
                                                        tblProcurementMethod = (TblProcurementMethod) pmit.next();
                                                        if("OTM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                           "LTM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                           "DP".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                          // "LEQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                         //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                           "RFQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                          // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                         //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                           "FC".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                          // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                                                           "DPM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())||
                                                            "QCBS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                            "LCS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                            "SFB".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                            "SBCQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                            "SSS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                            "IC".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()))
                                                        {
                                                            pMethod.append("<option value='");
                                                            if (tblProcurementMethod.getProcurementMethodId() == configamd.getTblProcurementMethod().getProcurementMethodId()) {
                                                                if(tblProcurementMethod.getProcurementMethod().equals("RFQ"))
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>LEM");

                                                                }
                                                                else if(tblProcurementMethod.getProcurementMethod().equals("DPM"))
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>DCM");

                                                                }
                                                                else
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>" + tblProcurementMethod.getProcurementMethod());

                                                                }
                                                                
                                                            } else {
                                                                if(tblProcurementMethod.getProcurementMethod().equals("RFQ"))
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>LEM");

                                                                }
                                                                else if(tblProcurementMethod.getProcurementMethod().equals("DPM"))
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>DCM");

                                                                }
                                                                else
                                                                {
                                                                    pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>" + tblProcurementMethod.getProcurementMethod());

                                                                }
                                                                
                                                            }
                                                            pMethod.append("</option>");
                                                         }
                                                    }

                                                    Iterator ptit = getProcType.iterator();
                                                    String procureType="";
                                                    while (ptit.hasNext()) {
                                                        tblProcurementTypes = (TblProcurementTypes) ptit.next();
                                                        if(tblProcurementTypes.getProcurementType().equalsIgnoreCase("NCT"))
                                                                procureType="NCB";
                                                        else if(tblProcurementTypes.getProcurementType().equalsIgnoreCase("ICT"))
                                                                procureType="ICB";
                                                        pType.append("<option value='");
                                                        if (tblProcurementTypes.getProcurementTypeId() == configamd.getTblProcurementTypes().getProcurementTypeId()) {
                                                            pType.append(tblProcurementTypes.getProcurementTypeId() + "' selected='true'>" + procureType);
                                                        } else {
                                                            pType.append(tblProcurementTypes.getProcurementTypeId() + "'>" + procureType);
                                                        }
                                                        pType.append("</option>");
                                                    }

                                %>
                                <input type="hidden" name="delrow" value="0" id="delrow"/>


                                <tr id="trrule_<%=i%>">
                                    <td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_<%=i%>" value="<%=i%>" /></td>
                                    <td class="t-align-center"><select name="procType<%=i%>" class="formTxtBox_1" id="procType_<%=i%>">
                                            <%=pType%></select>
                                    </td>
                                    <td class="t-align-center"><select name="procMethod<%=i%>" class="formTxtBox_1" id="procMethod_<%=i%>">
                                            <%=pMethod%></select>
                                    </td>
                                    <td class="t-align-center"><input name="amendPercent<%=i%>" type="text" class="formTxtBox_1" id="amendPercent_<%=i%>" style="width:90%;" value="<%=configamd.getAmendPercent()%>" onBlur="return ChkamendPercent(this);"/><span id="AmendPercent_<%=i%>" style="color: red;">&nbsp;</span></td>
                                    <td class="t-align-center"><input name="amendMinDays<%=i%>" type="text" class="formTxtBox_1" id="amendMinDays_<%=i%>" style="width:90%;" value="<%=configamd.getAmendMinDays()%>" onBlur="return ChkamendMinDays(this);"/><span id="AmendMinDays_<%=i%>" style="color: red;">&nbsp;</span></td>
                                </tr>

                                <%
                                                }
                                            } else {                                                
                                                msg = "No Record Found";
                                            }

                                %>

                                <tbody id="trbody">

                                </tbody>
                            </table>
                            <div>&nbsp;</div>
                            <input type="hidden" name="TotRule" id="TotRule" value="<%=i%>"/>
                            <input type="hidden" name="introw" value="" id="introw"/>
                            <table width="100%" cellspacing="0" >
                                <tr>
                                    <%if ("edit".equals(request.getParameter("action"))) {%>
                                    <td colspan="11" class="t-align-center"><span class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Update" onclick="return validate();" />
                                        </span></td>
                                        <%} else {%>
                                    <td colspan="11" class="t-align-center"><span class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Submit" onclick="return validate();" />
                                        </span></td>
                                    <%}%>
                                </tr>
                            </table>
                            <div align="center"> <%=msg%></div>
                        </form>

                    </td>
                </tr>
            </table>

            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>
             <script>
                var obj = document.getElementById('lblAmendmentRuleEdit');
                if(obj != null){
                    if(obj.innerHTML == 'Edit'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>



