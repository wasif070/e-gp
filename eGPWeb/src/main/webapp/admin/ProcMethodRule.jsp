<%-- 
    Document   : ProcMethodRule
    Created on : Nov 22, 2010, 6:43:06 PM
    Author     : Naresh.Akurathi
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigProcurement"%>
<%@page import="com.cptu.egp.eps.model.table.TblBudgetType"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTypes"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Procurement Method Business Rule</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        
        <%!    ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();
        %>

        <%
                    StringBuilder budgetType = new StringBuilder();
                    StringBuilder tenderType = new StringBuilder();
                    StringBuilder procNature = new StringBuilder();
                    StringBuilder procType = new StringBuilder();
                    StringBuilder procMethod = new StringBuilder();
                    
                    TblBudgetType budgetTypes2 = new TblBudgetType();
                    Iterator bdit2 = configPreTenderRuleSrBean.getBudgetType().iterator();
                    String budType="";
                    while (bdit2.hasNext()) {
                        budgetTypes2 = (TblBudgetType) bdit2.next();
                        if(budgetTypes2.getBudgetType().equalsIgnoreCase("Development"))
                            budType="Capital";
                        else if(budgetTypes2.getBudgetType().equalsIgnoreCase("Revenue"))
                            budType="Recurrent";
                        else
                            budType= budgetTypes2.getBudgetType();
                        budgetType.append("<option value='" + budgetTypes2.getBudgetTypeId() + "'>" + budType + "</option>");
                    }

                    TblTenderTypes tenderTypes2 = new TblTenderTypes();
                    Iterator trit2 = configPreTenderRuleSrBean.getTenderNames().iterator();
                    while (trit2.hasNext()) {
                        tenderTypes2 = (TblTenderTypes) trit2.next();
                        if(!tenderTypes2.getTenderType().equals("PQ") && !tenderTypes2.getTenderType().equals("RFA") && !tenderTypes2.getTenderType().equals("2 Stage-PQ"))
                        {
                            tenderType.append("<option value='" + tenderTypes2.getTenderTypeId() + "'>" + tenderTypes2.getTenderType() + "</option>");
                        }
                    }
                    TblProcurementNature tblProcureNature2 = new TblProcurementNature();
                    Iterator pnit2 = configPreTenderRuleSrBean.getProcurementNature().iterator();
                    while (pnit2.hasNext()) {
                        tblProcureNature2 = (TblProcurementNature) pnit2.next();
                        procNature.append("<option value='" + tblProcureNature2.getProcurementNatureId() + "'>" + tblProcureNature2.getProcurementNature() + "</option>");
                    }

                    TblProcurementMethod tblProcurementMethod2 = new TblProcurementMethod();
                    Iterator pmit2 = configPreTenderRuleSrBean.getProcurementMethod().iterator();
                    while (pmit2.hasNext()) {
                        tblProcurementMethod2 = (TblProcurementMethod) pmit2.next();
                        if("OTM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                            "LTM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                            "DP".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                           // "LEQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                          //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                            "RFQ".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                           // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                          //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                            "FC".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                           // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                            "DPM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())||
                             "QCBS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "LCS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "SFB".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "SBCQ".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "SSS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                             "IC".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()))
                         {
                            if(tblProcurementMethod2.getProcurementMethod().equals("RFQ"))
                            {
                                procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>LEM</option>");

                            }
                            else if(tblProcurementMethod2.getProcurementMethod().equals("DPM"))
                            {
                                procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>DCM</option>");

                            }
                            else
                            {
                                procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>" + tblProcurementMethod2.getProcurementMethod() + "</option>");

                            }
                         }
                        
                    }

                    TblProcurementTypes tblProcurementTypes2 = new TblProcurementTypes();
                    Iterator ptit2 = configPreTenderRuleSrBean.getProcurementTypes().iterator();
                    String procureType="";
                    while (ptit2.hasNext()) {
                        tblProcurementTypes2 = (TblProcurementTypes) ptit2.next();
                        if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("NCT"))
                            procureType="NCB";
                        else if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("ICT"))
                            procureType="ICB";
                        procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + procureType + "</option>");
                    }


        %>


        <script type="text/javascript">


            var totCnt = 1;

            $(function() {
                $('#linkAddRule').click(function() {
                    var count=($("#tblrule tr:last-child").attr("id").split("_")[1]*1);

                    var htmlEle = "<tr id='trrule_"+ (count+1) +"'>"+
                        "<td class='t-align-center'><input type='checkbox' name='checkbox"+(count+1)+"' id='checkbox_"+(count+1)+"' value='"+(count+1)+"' style='width:90%;'/></td>"+
                        "<td class='t-align-center'><select name='budgetType"+(count+1)+"' class='formTxtBox_1' id='budgetType_"+(count+1)    +"'><%=budgetType%></select></td>"+
                        "<td class='t-align-center'><select name='tenderType"+(count+1)+"' class='formTxtBox_1' id='tenderType_"+(count+1)    +"'><%=tenderType%></select></td>"+
                        "<td class='t-align-center'><select name='procMethod"+(count+1)+"' class='formTxtBox_1' id='procMethod_"+(count+1)    +"'><%=procMethod%></select></td>"+
                        "<td class='t-align-center'><select name='procType"+(count+1)+"' class='formTxtBox_1' id='procType_"+(count+1)+"'><%=procType%></select></td>"+
                        "<td class='t-align-center'><select name='procNature"+(count+1)+"' class='formTxtBox_1' id='procNature_"+(count+1)+"'><%=procNature%></select></td>"+
                        "<input type='hidden' name='nationalDisaster"+(count+1)+"' class='formTxtBox_1' id='nationalDisaster_"+(count+1)+"' value='Normal'/>"+
                        "<input type='hidden' name='urgency"+(count+1)+"' class='formTxtBox_1' id='urgency_"+(count+1)+"' value='Yes'/>"+
                        "<td class='t-align-center'><input name='minValue"+(count+1)+"' type='text' class='formTxtBox_1' id='minValue_"+(count+1)+"' style='width:90%;' onblur='return chkMinVal(this);'/><span id='minvalue_"+(count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><input name='maxValue"+(count+1)+"' type='text' class='formTxtBox_1' id='maxValue_"+(count+1)+"' style='width:90%;' onblur='return chkMaxVal(this);'/><span id='maxvalue_"+(count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><input name='minNoDays"+(count+1)+"' type='text' class='formTxtBox_1' id='minNoDays_"+(count+1)+"' style='width:90%;' onblur='return chkMinDays(this);'/><span id='MinNoDays_"+(count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><input name='maxNoDays"+(count+1)+"' type='text' class='formTxtBox_1' id='maxNoDays_"+(count+1)+"' style='width:90%;' onblur='return chkMaxDays(this);'/><span id='MaxNoDays_"+(count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "</tr>";

                    totCnt++;
                    //alert(htmlEle);
                    $("#tblrule").append(htmlEle);
                    document.getElementById("TotRule").value = (count+1);
                    //alert(count+1);

                });
            });


            $(function() {
                $('#linkDelRule').click(function() {
                    var cnt = 0;
                    var tmpCnt = 0;
                    var counter = ($("#tblrule tr:last-child").attr("id").split("_")[1]*1);
                    for(var i=1;i<=counter;i++){
                        if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                            tmpCnt++;
                        }
                    }
                    if(tmpCnt==totCnt){
                        $('span.#lotMsg').css("visibility","visible");
                        $('span.#lotMsg').css("color","red");
                        $('span.#lotMsg').html('Minimum 1 record is needed!');
                    }else{
                        for(var i=1;i<=counter;i++){
                            if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                                $("tr[id='trrule_"+i+"']").remove();
                                $('span.#lotMsg').css("visibility","collapse");
                                cnt++;
                            }
                        }
                        totCnt -= cnt;
                    }

                });
            });
        </script>
    </head>
    <body>
        <%

                    String msg = "";
                    if ("Submit".equals(request.getParameter("button"))) {
                        int row = Integer.parseInt(request.getParameter("TotRule"));
                        for (int i = 1; i <= row; i++) {
                            if (request.getParameter("budgetType" + i) != null) {
                                byte btype = Byte.parseByte(request.getParameter("budgetType" + i));
                                byte ttype = Byte.parseByte(request.getParameter("tenderType" + i));
                                byte pnature = Byte.parseByte(request.getParameter("procNature" + i));
                                byte pmethod = Byte.parseByte(request.getParameter("procMethod" + i));
                                byte ptypes = Byte.parseByte(request.getParameter("procType" + i));
                                float minvalue = Float.parseFloat(request.getParameter("minValue" + i));
                                float maxvalue = Float.parseFloat(request.getParameter("maxValue" + i));
                                short minsubdays = Short.parseShort(request.getParameter("minNoDays" + i));
                                short maxsubdays = Short.parseShort(request.getParameter("maxNoDays" + i));
                                
                                TblConfigProcurement configProcurement = new TblConfigProcurement();
                                configProcurement.setTblBudgetType(new TblBudgetType(btype));
                                configProcurement.setTblTenderTypes(new TblTenderTypes(ttype));
                                configProcurement.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                configProcurement.setTblProcurementTypes(new TblProcurementTypes(ptypes));
                                configProcurement.setTblProcurementNature(new TblProcurementNature(pnature));
                                configProcurement.setIsNationalDisaster(request.getParameter("nationalDisaster" + i));
                                configProcurement.setIsUrgency(request.getParameter("urgency" + i));
                                configProcurement.setMinValue(new BigDecimal(request.getParameter("minValue" + i)));
                                configProcurement.setMaxValue(new BigDecimal(request.getParameter("maxValue" + i)));
                                configProcurement.setMinSubDays(minsubdays);
                                configProcurement.setMaxSubDays(maxsubdays);
                                
                                msg = configPreTenderRuleSrBean.addConfigProcurement(configProcurement);
                            }
                        }
                        if (msg.equals("Values Added")) {
                            msg = "Procurement Method Business Rule Configured Successfully";
                            response.sendRedirect("ProcMethodRuleView.jsp?msg=" + msg);
                        }

                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 25%; display: block;">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                            </tr>
                        </table>
                    </td>
                    <td class="contentArea">
                        <div class="pageHead_1">Procurement Method Business Rule Configuration</div>

                        <div style="font-style: italic" class="t-align-right"><normal>Fields marked with (<span class="mandatory">*</span>) are mandatory</normal></div>

<!--                        <div align="right" class="t_space b_space">
                            <span id="lotMsg" style="color: red; float: left; font-weight: bold; visibility: collapse;">&nbsp;</span>
                            <a id="linkAddRule" class="action-button-add">Add Rule</a> <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a><br/>   
                        </div>-->

                        <%--<div class="pageHead_1" align="center"> <%=msg%></div>--%>
                        <!--<div class="overFlowContainer t-align-right">-->
                            <form action="ProcMethodRule.jsp" method="post">
                                <table width="100%" cellspacing="0" class="tableList_1" id="tblrule" name="tblrule">
                                    <tr>
                                        <th >Select</th>
                                        <th >Budget  Type <br />(<span class="mandatory">*</span>)</th>
                                        <th >Tender  Type<br />(<span class="mandatory">*</span>)</th>
                                        <th >Procurement  Method<br />(<span class="mandatory">*</span>)</th>
                                        <th >Procurement  Type<br />(<span class="mandatory">*</span>)</th>
                                        <th >Procurement  Category<br />(<span class="mandatory">*</span>)</th>
                                        <th >Minimum Value<br />(In Nu.) <br />(<span class="mandatory">*</span>)</th>
                                        <th >Maximum Value<br />(In Nu.) <br />(<span class="mandatory">*</span>) </th>
                                        <th >No. of days for <br/>New-Tender/New-Proposal Submission    <br />(<span class="mandatory">*</span>) </th>
                                        <th >No. of days for <br/>Re-Tender/Re-Proposal Submission<br />(<span class="mandatory">*</span>) </th>
                                    </tr>
                                    <input type="hidden" name="delrow" value="0" id="delrow"/>
                                    <input type="hidden" name="TotRule" id="TotRule" value="1"/>
                                    <input type="hidden" name="introw" value="" id="introw"/>
                                    <tr id="trrule_1">
                                        <td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_1" value="" /></td>
                                        <td class="t-align-center"><select name="budgetType1" class="formTxtBox_1" id="budgetType_1">
                                                <%=budgetType%></select>
                                        </td>
                                        <td class="t-align-center"><select name="tenderType1" class="formTxtBox_1" id="tenderType_1">
                                                <%=tenderType%></select>
                                        </td>
                                        <td class="t-align-center"><select name="procMethod1" class="formTxtBox_1" id="procMethod_1">
                                                <%=procMethod%></select>
                                        </td>
                                        <td class="t-align-center"><select name="procType1" class="formTxtBox_1" id="procType_1">
                                                <%=procType%></select>
                                        </td>
                                        <td class="t-align-center"><select name="procNature1" class="formTxtBox_1" id="procNature_1">
                                                <%=procNature%></select>
                                        </td>
                                      
                                        <input type="hidden" name="nationalDisaster1" class="formTxtBox_1" id="nationalDisaster_1" value="Normal">
                                        <input name="urgency1"  type="hidden" class="formTxtBox_1" id="urgency_1" value="Yes"/>
                                        <td class="t-align-center"><input name="minValue1" type="text" class="formTxtBox_1" id="minValue_1" style="width:80%;" onBlur="return chkMinVal(this);" /><span id="minvalue_1" style="color:red;"></span></td>
                                        <td class="t-align-center"><input name="maxValue1" type="text" class="formTxtBox_1" id="maxValue_1" style="width:80%;" onBlur="return chkMaxVal(this);" /><span id="maxvalue_1" style="color:red;"></span></td>
                                        <td class="t-align-center"><input name="minNoDays1" type="text" class="formTxtBox_1" id="minNoDays_1" style="width:80%;" onBlur="return chkMinDays(this);" /><span id="MinNoDays_1" style="color:red;"></span></td>
                                        <td class="t-align-center"><input name="maxNoDays1" type="text" class="formTxtBox_1" id="maxNoDays_1" style="width:80%;" onBlur="return chkMaxDays(this);" /><span id="MaxNoDays_1" style="color:red;"></span></td>
                                    </tr>
                                </table>
                                <div>&nbsp;</div>
                                <table width="100%" cellspacing="0" >
                                    <tr>
                                        <td align="right" colspan="9" class="t-align-center">
                                            <span class="formBtn_1"><input type="submit" name="button" id="button" value="Submit" onclick="return validate();"/></span>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                       <!-- </div>-->
                    </td>
                </tr>
            </table>


            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>
             <script>
                var obj = document.getElementById('lblProcMethodAdd');
                if(obj != null){
                    if(obj.innerHTML == 'Add'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            <script type="text/javascript">

            function validate(){

                var count=document.getElementById("TotRule").value;
                var flag=true;
                
                $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/BusinessRuleConfigurationServlet",
                        data:"budgetType="+$('#budgetType_1').val()+"&"+"tenderType="+$('#tenderType_1').val()+"&"+"procMethod="+$('#procMethod_1').val()+"&"+"procType="+$('#procType_1').val()+"&"+"procNature="+$('#procNature_1').val()+"&"+"typeofImergency="+$('#nationalDisaster_1').val()+"&"+"minvalue="+$('#minValue_1').val()+"&"+"maxvalue="+$('#maxValue_1').val()+"&"+"funName=ProcMethodRule2",
                        async: false,
                        success: function(j){

                            if(j.toString()!="0"){
                                flag=false;
                                jAlert("Combination of 'BudgetType', 'TenderType', 'ProcurementMethod','ProcurementType', and 'ProcurementNature' already exist.","Procurement Method Business Rule Configuration", function(RetVal) {
                                });
                            }
                        }
                    });
                
                
                for(var k=1;k<=count;k++)
                {
                    if(document.getElementById("minValue_"+k)!=null)
                    {
                        if(document.getElementById("minValue_"+k).value=="")
                        {
                            document.getElementById("minvalue_"+k).innerHTML="<br/>Please Enter Minimum Value in Nu.";
                            flag=false;
                        }
                        else
                        {
                            if(decimal(document.getElementById("minValue_"+k).value))
                            {
                                if(!chkMax(document.getElementById("minValue_"+k).value))
                                {
                                    document.getElementById("minvalue_"+k).innerHTML="Maximum 12 digits are allowed.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("minvalue_"+k).innerHTML="";
                                }
                            }
                            else
                            {
                                document.getElementById("minvalue_"+k).innerHTML="</br>Please Enter Numbers and 2 digits after Decimal.";
                                flag=false;
                            }

                        }

                    }
                    if(document.getElementById("maxValue_"+k)!=null)
                    {
                        if(document.getElementById("maxValue_"+k).value=="")
                        {
                            document.getElementById("maxvalue_"+k).innerHTML="<br/>Please Enter Maximum Value in Nu.";
                            flag=false;
                        }
                        else
                        {
                            if(decimal(document.getElementById("maxValue_"+k).value))
                            {
                                if(!chkMax(document.getElementById("maxValue_"+k).value))
                                {
                                    document.getElementById("maxvalue_"+k).innerHTML="Maximum 12 digits are allowed.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("maxvalue_"+k).innerHTML="";
                                }

                            }
                            else
                            {
                                document.getElementById("maxvalue_"+k).innerHTML="</br>Please Enter Numbers and 2 digits after Decimal.";
                                flag=false;
                            }

                        }

                    }

                    if(document.getElementById("minNoDays_"+k)!=null)
                    {
                        if(document.getElementById("minNoDays_"+k).value=="")
                        {
                            document.getElementById("MinNoDays_"+k).innerHTML="<br/>Please Enter Minimum No. of Days for New-Tender/New-Proposal Submission(in days).";
                            flag=false;
                        }

                        else
                        {
                            if(numeric(document.getElementById("minNoDays_"+k).value))
                            {
                                if(!chkMaxLength(document.getElementById("minNoDays_"+k).value))
                                {
                                    document.getElementById("MinNoDays_"+k).innerHTML="<br/>Maximum 3 digits are allowed.";
                                    flag= false;
                                }
                                else
                                {
                                    document.getElementById("MinNoDays_"+k).innerHTML="";
                                }
                            }
                            else
                            {
                                document.getElementById("MinNoDays_"+k).innerHTML="<br/>Please Enter numbers only.";
                                flag= false;
                            }
                        }

                    }

                    if(document.getElementById("maxNoDays_"+k)!=null)
                    {
                        if(document.getElementById("maxNoDays_"+k).value=="")
                        {
                            document.getElementById("MaxNoDays_"+k).innerHTML="<br/>Please Enter Maximum No. of Days for Re-Tender/Re-Proposal Submission(in days).";
                            flag=false;
                        }
                        else
                        {
                            if(numeric(document.getElementById("maxNoDays_"+k).value))
                            {
                                if(!chkMaxLength(document.getElementById("maxNoDays_"+k).value))
                                {
                                    document.getElementById("MaxNoDays_"+k).innerHTML="<br/>Maximum 3 digits are allowed.";
                                    flag= false;
                                }
                                else
                                {
                                    document.getElementById("MaxNoDays_"+k).innerHTML="";
                                }
                            }
                            else
                            {
                                document.getElementById("MaxNoDays_"+k).innerHTML="<br/>Please Enter numbers only.";
                                flag= false;
                            }
                        }
                    }

                }
                // Start OF--Checking for Unique Rows
                if(document.getElementById("TotRule")!=null){
                    var totalcount = eval(document.getElementById("TotRule").value);} //Total Count After Adding Rows
                var chk=true;
                var i=0;
                var j=0;
                for(i=1; i<=totalcount; i++) //Loop For Newly Added Rows
                {
                    if(document.getElementById("minValue_"+i)!=null)
                    if($.trim(document.getElementById("minValue_"+i).value)!='' && $.trim(document.getElementById("maxValue_"+i).value) !='' && $.trim(document.getElementById("minNoDays_"+i).value)!='' && $.trim(document.getElementById("maxNoDays_"+i).value)!='') // If Condition for When all Data are filled thiscode is Running
                    {

                        for(j=1; j<=totalcount && j!=i; j++) // Loop for Total Count but Not same as (i) value
                        {
                            chk=true;
                            //If Condition for Check Duplicate Rows are there or not.If Columns are diff then chk variable set to false
                            //IF Row is same Give alert message.
                            if($.trim(document.getElementById("budgetType_"+i).value) != $.trim(document.getElementById("budgetType_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("tenderType_"+i).value) != $.trim(document.getElementById("tenderType_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("procMethod_"+i).value) != $.trim(document.getElementById("procMethod_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("procType_"+i).value) != $.trim(document.getElementById("procType_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("procNature_"+i).value) != $.trim(document.getElementById("procNature_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("nationalDisaster_"+i).value) != $.trim(document.getElementById("nationalDisaster_"+j).value))
                            {
                                chk=false;
                            }
                            var minvi=document.getElementById("minValue_"+i).value;
                            var minvj=document.getElementById("minValue_"+j).value;
                            if(minvi.indexOf(".")==-1)
                            {
                                minvi=minvi+".00";
                            }
                            if(minvj.indexOf(".")==-1)
                            {
                                minvj=minvj+".00";
                            }
                            if($.trim(minvi) != $.trim(minvj))
                            {
                                chk=false;
                            }
                            var maxvi=document.getElementById("maxValue_"+i).value;
                            var maxvj=document.getElementById("maxValue_"+j).value;
                            if(maxvi.indexOf(".")==-1)
                            {
                                maxvi=maxvi+".00";
                            }
                            if(maxvj.indexOf(".")==-1)
                            {
                                maxvj=maxvj+".00";
                            }
                            if($.trim(maxvi) != $.trim(maxvj))
                            {
                                chk=false;
                            }
//                            if($.trim(document.getElementById("minNoDays_"+i).value) != $.trim(document.getElementById("minNoDays_"+j).value))
//                            {
//                                chk=false;
//                            }
//                            if($.trim(document.getElementById("maxNoDays_"+i).value) != $.trim(document.getElementById("maxNoDays_"+j).value))
//                            {
//                                chk=false;
//                            }
                            //alert(j);
                            //alert("chk" +chk);
                            if(flag){
                            if(chk==true) //If Row is same then give alert message
                            {

                                    jAlert("Duplicate record found. Please enter unique record","Procurement Method Business Rule Configuration",function(RetVal) {
                                    });
                                return false;
                            }
                        }
                    }
                }
                }
                // End OF--Checking for Unique Rows

                if (flag==false){

                    return false;
                }
            }

            function chkMax(value)
            {
                var chkVal=value;
                var ValSplit=chkVal.split('.');
                //alert(chkVal);
                //alert(ValSplit[0].length);
                if(ValSplit[0].length>12)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            function chkMaxLength(value)
            {
                var ValueChk=value;
                //alert(ValueChk);

                if(ValueChk.length>3)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            function chkMinVal(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk=true;

                if(obj.value!='')
                {
                    if(decimal(obj.value))
                    {
                        if(!chkMax(obj.value))
                        {
                            document.getElementById("minvalue_"+i).innerHTML="Maximum 12 digits are allowed.";
                            chk=false;
                        }
                        else
                        {
                            document.getElementById("minvalue_"+i).innerHTML="";
                        }
                        //document.getElementById("minvalue_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("minvalue_"+i).innerHTML="</br>Please Enter Numbers and 2 digits after Decimal.";
                        chk=false;
                    }
                }
                else
                {
                    document.getElementById("minvalue_"+i).innerHTML="</br>Please Enter Minimum Value in Nu.";
                    chk=false;
                }
                if(chk==false)
                {
                    return false;
                }

            };

            function chkMaxVal(obj){
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk1=true;

                if(obj.value!='')
                {
                    if(decimal(obj.value))
                    {
                        if(!chkMax(obj.value))
                        {
                            document.getElementById("maxvalue_"+i).innerHTML="Maximum 12 digits are allowed.";
                            chk=false;
                        }
                        else
                        {
                            document.getElementById("maxvalue_"+i).innerHTML="";
                        }
                        // document.getElementById("maxvalue_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("maxvalue_"+i).innerHTML="</br>Please Enter Numbers and 2 digits after Decimal.";
                        chk1=false;
                    }
                }
                else
                {
                    document.getElementById("maxvalue_"+i).innerHTML="</br>Please Enter Maximum Value in Nu.";
                    chk1=false;
                }
                if(chk1==false)
                {
                    return false;
                }

            };

            function chkMinDays(obj){
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk2=true;

                if(obj.value!='')
                {
                    if(numeric(obj.value))
                    {
                        if(!chkMaxLength(obj.value))
                        {
                            document.getElementById("MinNoDays_"+i).innerHTML="Maximum 3 digits are allowed.";
                            chk2=false;
                        }
                        else
                        {
                            document.getElementById("MinNoDays_"+i).innerHTML="";
                        }
                        //document.getElementById("MinNoDays_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("MinNoDays_"+i).innerHTML="</br>Please Enter numbers only.";
                        chk2=false;
                    }
                }
                else
                {
                    document.getElementById("MinNoDays_"+i).innerHTML="</br>Please Enter Minimum No. of Days for New-Tender/New-Proposal Submission(in days).";
                    chk2=false;
                }
                if(chk2==false)
                {
                    return false;
                }
            };

            function chkMaxDays(obj){
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk3=true;

                if(obj.value!='')
                {
                    if(numeric(obj.value))
                    {
                        if(!chkMaxLength(obj.value))
                        {
                            document.getElementById("MaxNoDays_"+i).innerHTML="Maximum 3 digits are allowed.";
                            chk3=false;
                        }
                        else
                        {
                            document.getElementById("MaxNoDays_"+i).innerHTML="";
                        }
                        //document.getElementById("MaxNoDays_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("MaxNoDays_"+i).innerHTML="</br>Please Enter numbers only.";
                        chk3=false;
                    }
                }
                else
                {
                    document.getElementById("MaxNoDays_"+i).innerHTML="</br>Please Enter Maximum No. of Days for Re-Tender/Re-Proposal Submission(in days).";
                    chk3=false;
                }
                if(chk3==false)
                {
                    return false;
                }
            };

            function decimal(value){

                return /^(\d+(\.\d{2})?)$/.test(value);
            };

            function numeric(value) {
                return /^\d+$/.test(value);
            }

        </script>

    </body>
</html>

