<%-- 
    Document   : TOCFormationRuleView
    Created on : Nov 25, 2010, 12:58:52 PM
    Author     : Naresh.Akurathi
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigTec"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TOC Formation Business Rule Details</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

<script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
   <script type="text/javascript">
            $(document).ready(function() {
                sortTable();
            });
        </script>

<script type="text/javascript">
    function conform()
    {    
        if (confirm("Do you want to delete this business rule?"))
            return true;
        else
            return false;      
    }
</script>

<%!
  ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();
  
%>

</head>
<body>

    <%
            String mesg = request.getParameter("msg");
            String delMsg = "";
            if("delete".equals(request.getParameter("action"))){
                String action = "Remove TOC Formation";
                try{
                    int configId = Integer.parseInt(request.getParameter("id"));
                    TblConfigTec configTec = new TblConfigTec();
                    configTec.setConfigTec(configId);
                    configTec.setCommitteeType("TOC");
                    configTec.setMaxMemReq(Byte.parseByte("1"));
                    configTec.setMaxTenderVal(new BigDecimal(Byte.parseByte("1")));
                    configTec.setMinApproval(Byte.parseByte("1"));
                    configTec.setMinMemFromPe(Byte.parseByte("1"));
                    configTec.setMinMemFromTec(Byte.parseByte("1"));
                    configTec.setMinMemOutSidePe(Byte.parseByte("1"));
                    configTec.setMinMemReq(Byte.parseByte("1"));
                    configTec.setMinTenderVal(new BigDecimal(Byte.parseByte("1")));
                    configTec.setTblProcurementNature(new TblProcurementNature(Byte.parseByte("1")));

                    configPreTenderRuleSrBean.delConfigTec(configTec);
                    delMsg = "Opening Committee Rule Deleted successfully";
                 }catch(Exception e){
                    System.out.println(e);
                    action = "Error in : "+action +" : "+ e;
                }finally{
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                    action=null;
                }
                }
    %>

<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <div class="topHeader">
    <%@include file="../resources/common/AfterLoginTop.jsp" %>
  </div>

                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea" valign="top">
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="pageHead_1">TOC Formation Business Rule Details
  <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('2,3,6,9');">Save as PDF</a></span>
  </div>
  <div>&nbsp;</div>
  <%if("delete".equals(request.getParameter("action"))){%>
  <div class="responseMsg successMsg"><%=delMsg%></div>
  <%}%>
  <%if(mesg != null){%>
  <div class="responseMsg successMsg"><%=mesg%></div>
  <%}%>
    
 <form action="TOCFormationRuleView.jsp" method="post">
 <table width="100%" cellspacing="0" class="tableList_1 t_space" name="resultTable" id="resultTable"  cols="@9">
     <tr>
       <th>Committee Type<br /></th>
       <th>Procurement Category<br /></th>
       <th  style="display: none;">Min.  Tender  <br />value(In Mn. Tk.)<br /></th>
       <th style="display: none;">Max. Tender <br />value(In Mn. Tk.)<br /></th>
       <th>Min. Member<br />Required<br /></th>
       <th>Max. Member<br />Required<br /></th>
       <th  style="display: none;">Min. Members <br />outside PA<br /></th>
       <th>Min. Member <br />required from PA <br /></th>
       <th>Min. Member <br />required from TC<br /></th>
       <th>Action</th>
    </tr>

     <%

           List lt = configPreTenderRuleSrBean.getCommiteeTypeData("TOC,POC");
           
           String msg = "";
           if(!lt.isEmpty()){
               TblConfigTec tblconfigtec = new TblConfigTec();
               Iterator it = lt.iterator();
               int i = 0;
               while(it.hasNext()){
                  
                   tblconfigtec = (TblConfigTec)it.next();
                   i++;

     %>
     
      <%if(i%2==0){%>
      <tr style='background-color:#E4FAD0;'>
          <%}else{%>
          <tr>
          <%}%>
	<!--td class="t-align-center"><%=tblconfigtec.getCommitteeType()%></td-->
        <td class="t-align-center">TOC</td>
        <td class="t-align-center"><%=tblconfigtec.getTblProcurementNature().getProcurementNature()%></td>
        <td class="t-align-center" style="display: none;">0</td>
        <td class="t-align-center" style="display: none;">0</td>
        <td class="t-align-center"><%=tblconfigtec.getMinMemReq()%></td>
        <td class="t-align-center"><%=tblconfigtec.getMaxMemReq()%></td>
        <td class="t-align-center" style="display: none;">0</td>
        <td class="t-align-center"><%=tblconfigtec.getMinMemFromPe()%></td>
        <td class="t-align-center"><%=tblconfigtec.getMinMemFromTec()%></td>
        <td class="t-align-center"><a href="TOCFormationRuleDetails.jsp?action=edit&id=<%=tblconfigtec.getConfigTec()%>&natureId=<%=tblconfigtec.getTblProcurementNature().getProcurementNatureId()%>">Edit</a>&nbsp;<%--|&nbsp;<a href="TOCFormationRuleView.jsp?action=delete&id=<%=tblconfigtec.getConfigTec()%>" onclick="return conform();">Delete</a>--%></td> 

          </tr></tr>
        <%

                   }

               }
           else
               msg="No Record Found";
     %>

 </table>
 <div align="center"> <%=msg%></div>
 </form>
                    </td>
                </tr>
            </table>

            <form id="formstyle" action="" method="post" name="formstyle">

               <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
               <%
                 SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                 String appenddate = dateFormat1.format(new Date());
               %>
               <input type="hidden" name="fileName" id="fileName" value="TOCRule_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="TOCRule" />
            </form>
  <div>&nbsp;</div>
  
   <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
</div>
 <script>
                var obj = document.getElementById('lblTOCFormationRuleView');
                if(obj != null){
                    if(obj.innerHTML == 'View'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
<script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
</body>
</html>

