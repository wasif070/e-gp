<%-- 
    Document   : EditSbBranchAdmin
    Created on : Apr 5, 2011, 3:07:50 PM
    Author     : nishit
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="scBankPartnerAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ScBankPartnerAdminSrBean"/>
    <jsp:useBean id="partnerAdminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.PartnerAdminMasterDtBean"/>
    <jsp:setProperty name="partnerAdminMasterDtBean" property="*"/>
    <%@page buffer="15kb"%>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <%
                    byte usrTypeId = 0;
                    String strUserTypeId = request.getParameter("userTypeId");
                    if (strUserTypeId != null && !strUserTypeId.isEmpty()) {
                        usrTypeId = Byte.parseByte(request.getParameter("userTypeId"));
                    }
                    String usrId = request.getParameter("userId");
                    boolean bole_formWhere = false;
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        scBankPartnerAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                    }
                    if(usrId.equalsIgnoreCase(session.getAttribute("userId").toString())){
                            bole_formWhere = true;
                        }
                    if (usrId != null && !usrId.equals("")) {
                        partnerAdminMasterDtBean.setUserId(Integer.parseInt(usrId));
                        partnerAdminMasterDtBean.setUserTypeId(usrTypeId);
                        if (request.getMethod().equalsIgnoreCase("get")) {
                            scBankPartnerAdminSrBean.setAuditTrail(null);
                            partnerAdminMasterDtBean.populateInfo();
                        }
                    }
                    
                    String partnerType = request.getParameter("partnerType");
                    String type = "";
                    String title = "";
                    String title1 = "";
                    String userType = "";
                    if (partnerType != null && partnerType.equals("Development")) {
                        type = "Development";
                        title = "Development Partner";
                        title1 = "Development Partner";
                        userType = "dev";
                    }
                    else {
                        type = "ScheduleBank";
                        title = "Financial Institution";
                        title1 = "Financial Institution";
                        userType = "sb";
                    }
                    String hasMobile = "";
                    if(request.getParameter("hasMobile") != null){
                        hasMobile = request.getParameter("hasMobile");
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
         <%if(!bole_formWhere){%>
        <title>Edit <%=title1%> Branch Admin Details</title>
        <%}else{%>
        <title>Edit Profile</title>
        <%}%>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                var hasMobileNo = '<%=hasMobile%>';
                if(hasMobileNo == 'No')
                    alert("Please insert your mobile no");

                $("#frmSBAdmin").validate({
                    rules: {
                        fullName: {spacevalidate: true, requiredWithoutSpace: true , alphaName:true, maxlength:100 },
                         mobileNo: {required: true,number: true, minlength: 8, maxlength:8 },
                        nationalId: { required: true, number: true,maxlength: 11,minlength: 11}
                    },
                    messages: {
                        fullName: { spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                                    requiredWithoutSpace: "<div class='reqF_1'>Please enter Full Name</div>",
                                    alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only </div>",
                                    maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"},


                       nationalId: {
                            required: "<div class='reqF_1'>Please enter CID No.</div>",
                            number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                            maxlength: "<div class='reqF_1'>CID No should comprise of maximum 11 digits</div>",
                            minlength: "<div class='reqF_1'>CID No should comprise of minimum 11 digits</div>"
                        },
                        mobileNo: {
                             required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                            number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" ,
                            minlength:"<div class='reqF_1'>Minimum 8 digits are required</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 8 digits only</div>"
                    }
                }
                }
            );
            });
        </script>
        <script type="text/javascript">
//            $(function() {
//                $('#txtNtnlId').change(function() {
//                    if($('#txtNtnlId').val()!=$('#oldNtnl').val()) {
//                        var spaceTest=/^([a-zA-Z0-9]+)$/;
//                        if(spaceTest.test($('#txtNtnlId').val())) {
//                       //     $('span.#NtnlIdMsg').html("Checking for unique National Id...");
//                            $.post("<%=request.getContextPath()%>/ScBankServlet", {ntnlId:$('#txtNtnlId').val(),funName:'verifyNtnlId'},  function(j){
//                                if(j.toString().indexOf("Ok", 0)!=-1){
//                                    $('#NtnlIdMsg').css("color","white");
//                                }
//                                else if(j.toString().indexOf("Ok", 0)==-1){
//                                    $('#NtnlIdMsg').css("color","red");
//                                    $('#frmUserReg').submit(function(){
//                                        return false;
//                                    });
//                                }
//                                $('#NtnlIdMsg').html(j);
//                            });
//                        }
//                        else
//                        {
//                            $('#NtnlIdMsg').css("color","red");
//                            $('#NtnlIdMsg').html("Please enter valid National ID.");
//                        }
//                    }
//                    else
//                    {
//                        $('#NtnlIdMsg').html("");
//                    }
//                });
//            });
        </script>
        <script type="text/javascript">
//            $(function() {
//                $('#txtMob').change(function() {
//                    if($('#txtMob').val()!= $('#oldMob')) {
//                        var digitTest=/^([0-9]+)$/;
//                        if(digitTest.test($('#txtMob').val())) {
//                            $('span.#MobileNoMsg').html("Checking for unique Mobile No...");
//                            $.post("<%=request.getContextPath()%>/ScBankServlet", {mobileNo:$('#txtMob').val(),funName:'verifyMobile'},  function(j){
//                                if(j.toString().indexOf("Ok", 0)!=-1){
//                                    $('#MobileNoMsg').css("color","green");
//                                }
//                                else if(j.toString().indexOf("Ok", 0)==-1){
//                                    $('#MobileNoMsg').css("color","red");
//                                    $('#frmUserReg').submit(function(){
//                                        return false;
//                                    });
//                                }
//                                $('#MobileNoMsg').html(j);
//                            });
//                        }
//                        else
//                        {
//                            $('#MobileNoMsg').css("color","red");
//                            $('#MobileNoMsg').html("Please enter digits only.");
//                        }
//                    }
//                    else
//                    {
//                        $('#MobileNoMsg').html("");
//                    }
//                });
//            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#frmSBAdmin').submit(function() {
                    if($('#MobileNoMsg').html().length> 0)
                    {
                        if($('#MobileNoMsg').html()!="Ok")
                            return false;
                    }
                    if(($('#partnerType').val()=="ScheduleBank")&&($('#NtnlIdMsg').html().length > 0)){
                        if($('#NtnlIdMsg').html()!="Ok")
                            return false;
                    }
                    return true;
                });
            });
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <%
                                                    if ("Update".equals(request.getParameter("add"))) {
                                                        System.out.println("Hello");
                                                        scBankPartnerAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                                        //scBankPartnerAdminSrBean.setBankId(Integer.parseInt(request.getParameter("bank")));
                                                        partnerAdminMasterDtBean.setIsMakerChecker("BranchAdmin");
                                                        boolean updateFlag = scBankPartnerAdminSrBean.updateDevelopmentScBankAdmin(partnerAdminMasterDtBean);
                                                        if (updateFlag) {
                                                            String viewURL = "ViewSbBranchAdmin.jsp?userId=" + usrId + "&userTypeId=" + usrTypeId + "&partnerType=" + type + "&msg=success&from=updated";
                                                            response.sendRedirect(viewURL);
                                                        }
                                                        else {%>
                                        <div class="responseMsg errorMsg"><%=title%> <%=appMessage.scheduleBranchAdminUpdate %></div>
                                        <%}
                                                    }
                                        %>
                    <tr valign="top">
                        <%if(userTypeId==1||userTypeId==7){%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" ></jsp:include>
                        <%}%>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                                <tr valign="top">
                                    <td class="contentArea_1">
                                        <%if(userTypeId==15){%>
                                        <div class="pageHead_1">Edit Profile</div>
                                        <%}else{%>
                                        <div class="pageHead_1">Edit <%=title1%> Branch Admin Details</div>
                                        <%}%>

                                        <form id="frmSBAdmin" name="frmSBAdmin" method="post" action="">
                                            <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td class="ff" width="15%"><%=title1%> Branch: </td>
                                                    <td width="85%">
                                                        <label class="ff" id="bankName">${partnerAdminMasterDtBean.sbDevelopName}</label>
                                                        <input type="hidden" id="partnerType" name="partnerType" value="<%=type%>"/>
                                                        <input type="hidden" name="sbDevelopName" value="${partnerAdminMasterDtBean.sbDevelopName}"/>
                                                        <input type="hidden" name="partnerId" value="${partnerAdminMasterDtBean.partnerId}"/>
                                                        <input type="hidden" name="isAdmin" value="${partnerAdminMasterDtBean.isAdmin}"/>
                                                        <input type="hidden" name="sbankDevelopId" value="${partnerAdminMasterDtBean.sbankDevelopId}"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">e-mail ID : </td>
                                                    <td>
                                                        <input type="hidden" name="emailId" value="${partnerAdminMasterDtBean.emailId}"/>
                                                        <label class="ff" id="txtMail">${partnerAdminMasterDtBean.emailId}</label>
                                                    </td>
                                                </tr>

                                                <tr>

                                                    <td class="ff">Full Name : <span>*</span></td>
                                                    <td><input class="formTxtBox_1" type="text" id="txtFullName" name="fullName" value="${partnerAdminMasterDtBean.fullName}" maxlength="200"/>
                                                    </td>
                                                </tr>
                                                <%if (!"Development".equalsIgnoreCase(type)) {%>
                                                <tr>
                                                    <td class="ff">CID No. : <span>*</span></td>
                                                    <td>
                                                        <input type="text" id="txtNtnlId" class="formTxtBox_1" name="nationalId" value="${partnerAdminMasterDtBean.nationalId}"/>
                                                        <input type="hidden" name="oldVal" id="oldNtnl" value="${partnerAdminMasterDtBean.nationalId}"/>
                                                        <div id="NtnlIdMsg"></div>
                                                        <%--<label class="ff" id="txtNationalId" >${partnerAdminMasterDtBean.nationalId}</label>--%>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <tr>
                                                    <td class="ff">Mobile No. : <span>*</span></td>
                                                    <td><input class="formTxtBox_1" id="txtMob" type="text" name="mobileNo" id="txtMobileNo" value="${partnerAdminMasterDtBean.mobileNo}" maxlength="16"/><span id="mNo" style="color: grey;"> (Mobile No. format should be e.g 12345678)</span>
                                                        <input type="hidden" name="oldVal" id="oldMob" value="${partnerAdminMasterDtBean.mobileNo}"/>
                                                        <div id="MobileNoMsg"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;
                                                    </td>
                                                    <td align="left">
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="add" id="btnAdd" value="Update" />
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <%if(userTypeId==15){%>
            <script>
                var headSel_Obj = document.getElementById("headTabMyAcc");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            <%}else{%>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            <%}%>
    </body>
</html>
