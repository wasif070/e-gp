<%-- 
    Document   : EditUserAccountInformation
    Created on : Oct 23, 2010, 5:05:11 PM
    Author     : parag
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>User Account Information</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="Include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="../resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1">User Account Information</div>
                                <div class="txt_1 c_t_space"><br />
                                </div>
                            </div>
                            <form id="frmUserAccountInformation" method="post">
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                    
                                     <tr>
                                        <td class="ff">e-mail ID : <span>*</span></td>
                                        <td><input name="emailId" type="text" class="formTxtBox_1" id="txtEmailId" style="width:200px;" readonly="true" />
                                    </tr>

                                    <tr>
                                        <td class="ff">Registration Type : <span>*</span></td>
                                        <td><input name="registrationType" type="text" class="formTxtBox_1" id="txtRegistrationType" style="width:200px;" readonly="true" />
                                    </tr>
                                    <tr>
                                        <td class="ff">Is JVCA? : <span>*</span></td>
                                        <td><input name="jvca" type="text" class="formTxtBox_1" id="txtJvca" style="width:200px;" readonly="true" />
                                    </tr>
                                    <tr>
                                        <td class="ff">Country of Business : <span>*</span></td>
                                        <td><input name="businessCountry" type="text" class="formTxtBox_1" id="txtBusinessCountry" style="width:200px;" readonly="true" />
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><label class="formBtn_1">
                                                <input type="submit" name="businessCountrySubmit" id="btnBusinessCountrySubmit" value="Submit" />
                                            </label>
                                            &nbsp;
                                            <label class="formBtn_1">
                                                <input type="reset" name="businessCountryReset" id="btnBusinessCountryReset" value="Reset" />
                                            </label></td>
                                    </tr>
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>


