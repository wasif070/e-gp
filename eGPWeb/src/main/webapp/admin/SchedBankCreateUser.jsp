<%--
    Document   : SchedBankCreateuser
    Created on : Oct 23, 2010, 7:04:46 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%
            String partnerType = request.getParameter("partnerType");
            String type = "";
            String title = "";
            String userType = "";
            String title1="";
            String details = "";
            if (partnerType != null && partnerType.equals("Development")) {
                type = "Development";
                title = "Development Partner";
                userType = "dev";
                title1="Development Partner";
                details="Regional/Country Office";
            } else {
                type = "ScheduleBank";
                title = "Financial Institution";
                userType = "sb";
                title1 ="Financial Institution";
                details="Branch";
            }
%>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - <%=title1%> User Role Creation</title>
        <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
<%--        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmSchedBankUser").validate({
                    rules: {
                        office:{required: true},
                        mailId:{required: true,email:true},
                        password:{required:true,minlength:8,maxlength:25,spacevalidate: true,alphaForPassword: true},
                        confPassword:{required:true,equalTo:"#txtPass",spacevalidate: true},
                        fullName:{required:true,maxlength:200,FullNameAlpha:true},
                       // ntnlId:{required:true,maxlength:13,number: true},
                       ntnlId:{maxlength:25,number: true},
                        designation:{required:true,maxlength:20},
                        mobileNo:{number:true,minlength:10,maxlength:15}//,
                        //  branch:{required:true}
                    },
ffdsafdsafdafdsafdsa
                    messages: {
                        office: {required: "<div class='reqF_1'>Please select <%=title1%></div>" },

                        mailId:{required: "<div class='reqF_1'>Please enter e-mail ID</div>",
                            email:"<div class='reqF_1'>Please Enter Valid e-mail ID.</div>"},

                        password:{required:"<div class='reqF_1'>Please enter Password</div>",
                            alphaForPassword: "<div class='reqF_1'>Please enter only Alphanumeric value</div>",
                            spacevalidate: "<div class='reqF_1'>Space is not allowed</div>",
                            minlength:"<div class='reqF_1'>Password requires minimum 8 characters</div>",
                            maxlength:"<div class='reqF_1'>Maximum 25 characters are allowed</div>"},

                        confPassword:{required: "<div class='reqF_1'>Please retype Password</div>",
                            equalTo: "<div class='reqF_1'>Password does not match. Please try again</div>",
                            spacevalidate: "<div class='reqF_1'>Space is not allowed</div>"
                        },

                        fullName:{required: "<div class='reqF_1'>Please enter Full Name</div>",
                            FullNameAlpha: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (.,()/-_) Only</div>",
                            maxlength:"<div class='reqF_1'>Maximum 200 characters are allowed.</div>"
                        },

                        ntnlId:{
                           // required:"<div class='reqF_1'>Please Enter National Id.</div>",
                            number:"<div class='reqF_1'>Please enter numbers only</div>" ,
                            maxlength:"<div class='reqF_1'>Maximum 25 digits are allowed.</div>"
                        },
                        designation: {required: "<div class='reqF_1'>Please enter Designation</div>",
                            maxlength:"<div class='reqF_1'> Maximum 20 characters are allowed.</div>"
                        },
                        // branch:"<div class='reqF_1'> Please Select Designation.</div>",

                        mobileNo :{number:"<div class='reqF_1'>Please enter Numbers only</div>",
                            minlength:"<div class='reqF_1'>Minimum 10 digits required.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 15 digits are allowed.</div>"
                        }

//                        mobileNo :{required: "<div class='reqF_1'>Please enter Mobile No.</div>",
//                            number:"<div class='reqF_1'>Please Enter Numbers only.</div>",
//                           minlength:"<div class='reqF_1'>Minimum 10 digits required.</div>",
//                            maxlength:"<div class='reqF_1'>Maximum 15 digits are allowed.</div>"

                    },
                    errorPlacement:function(error ,element)
                    {
                        if(element.attr("name")=="password")
                        {
                            error.insertAfter("#tipPassword");
                        }
                        else
                        {
                            error.insertAfter(element);

                        }
                    }
                }
            );
            });

        </script>--%>
        <script type="text/javascript">
            $(function() {
                $('#txtMail').blur(function() {
                    $('span.#mailMsg').hide();
                    if($('#txtMail').val()!='') {
                        $('span.#mailMsg').html("Checking for unique Mail Id...");
                        $.post("<%=request.getContextPath()%>/CommonServlet", {mailId:$('#txtMail').val(),funName:'verifyMail'},  function(j){
                            if(j.toString().indexOf("OK", 0)!=-1){
                                $('span.#mailMsg').css("color","green");
                                
                            }
                            else if(j.toString().indexOf("OK", 0)==-1){
                                $('span.#mailMsg').css("color","red");

//                                $('#frmUserReg').submit(function(){
//                                    return  false;
//                                });
                            }
                            $('span.#mailMsg').html(j);
                            $('span.#mailMsg').show();
                        });
                    }
                });
            });
       
           <%-- $(function() {
                $('#txtNtnlId').blur(function() {
                    $('span.#NtnlIdMsg').hide();
                    var spaceTest=/^([a-zA-Z0-9]+)$/;
                    if(spaceTest.test($('#txtNtnlId').val())) {
                        $('span.#NtnlIdMsg').html("Checking for National Id...");
                        $.post("<%=request.getContextPath()%>/ScBankServlet", {ntnlId:$('#txtNtnlId').val(),funName:'verifyNtnlId'},  function(j){
                            if(j.toString().indexOf("Ok", 0)!=-1){
                                $('span.#NtnlIdMsg').html("");
                                $('span.#NtnlIdMsg').css("color","green");
                                return false;
                            }
                            else if(j.toString().indexOf("Ok", 0)==-1){
                                $('span.#NtnlIdMsg').css("color","red");
                                $('#frmUserReg').submit(function(){
                                    return  false;
                                });
                            }
                            //$('span.#NtnlIdMsg').html(j);
                        });
                    }
                    else
                    {
            $('span.#NtnlIdMsg').css("color","red");
             $('span.#NtnlIdMsg').html("Please Enter Valid National Id");
                         }
                     });
                 });
                 --%>
       

          //  $(function() {
                //$('#txtMob').blur(function() {
                  //  var digitTest=/^([0-9]+)$/;
                    //if(digitTest.test($('#txtMob').val())) {
                       // $('span.#MobileNoMsg').html("Checking for unique Mobile No...");
                      //  $.post("<%=request.getContextPath()%>/CommonServlet", {mobileNo:$('#txtMob').val(),funName:'verifyMobileNo'},  function(j){
                           // if(j.toString().indexOf("OK", 0)!=-1){
                          //      $('span.#MobileNoMsg').html("");
                          //      $('span.#MobileNoMsg').css("color","green");
                          //  }
                           // else if(j.toString().indexOf("OK", 0)==-1){
                          //      $('span.#MobileNoMsg').css("color","red");
                         //       $('#frmUserReg').submit(function(){
                        //            return false;
                         //       });
                           // }
                            //$('span.#MobileNoMsg').html("OK");

                    //    });
               //     }
                    //else
                  //  {
               //         $('span.#MobileNoMsg').css("color","red");
                //        $('span.#MobileNoMsg').html("Please Enter Valid Mobile Number");
                   // }
              //  });
            //    });

//            $(function() {
//                $('#txtMob').blur(function() {
//                    var digitTest=/^([0-9]+)$/;
//                    if(digitTest.test($('#txtMob').val())) {
//                        $('span.#MobileNoMsg').html("Checking for unique Mobile No...");
//                        $.post("<%=request.getContextPath()%>/CommonServlet", {mobileNo:$('#txtMob').val(),funName:'verifyMobileNo'},  function(j){
//                            if(j.toString().indexOf("OK", 0)!=-1){
//                                $('span.#MobileNoMsg').html("");
//                                //  $('span.#MobileNoMsg').css("color","green");
//                            }
//                            else if(j.toString().indexOf("OK", 0)==-1){
//                                $('span.#MobileNoMsg').css("color","red");
//                                $('span.#MobileNoMsg').html(j);
//                                $('#frmUserReg').submit(function(){
//                                    return false;
//                                });
//                            }
//
//                        });
//                    }
//                    else
//                    {
//                        $('span.#MobileNoMsg').css("color","red");
//                        $('span.#MobileNoMsg').html("Please Enter Valid Mobile Number");
//                    }
//                });
//            });


 <%--           function RadioClick(){
                var data = '';
                var action ='';
                $('span.#chekerMsg').html("");
                $('span.#cmbBranchMsg').html("");
                $('span.#cmbBranchMsg').hide();
                if($('#bankChecker').attr("checked")==true){
                    data = $('#cmbOffice').val();
                    action = $('#bankChecker').val();
                }else{
                    if($('#bankCheckerRole').attr("checked")==true){
                        $('#cmbBranchTr').show();
                        $('span.#chekerMsg').html('');
                    }
                    $('#cmbBranchTr').show();
                    if($('#cmbDescmbBankignation').val()!=""&&$('#branchChecker').attr("checked")==true){
                        data = $('#cmbDescmbBankignation').val();
                        action = $('#branchChecker').val();

                    }
                }
                if(data!=''){
                    if(action=='BankChecker'){
                        $('#cmbBranchTr').hide();
                    }
                    $('span.#chekerMsg').html("Checking for Existing Role...");
                    $.post("<%=request.getContextPath()%>/CommonServlet", {officeId:data,role:action,funName:'bankChecker'},  function(j){
                        if(j.toString().length>2){
                            $('span.#chekerMsg').css("color","red");
                            $('span.#chekerMsg').html(j);
                            $('#frmUserReg').submit(function(){
                                return  false;
                            });
                        }else{
                            $('span.#chekerMsg').html('');
                            return true;
                        }

                        
                    });
                }
            }


function checkNatId(){
var flag = true;
    if($('#bankChecker').attr("checked")==false){
        if($('#cmbDescmbBankignation').val()==""){
            $('span.#cmbBranchMsg').html("Please select <%=title1%> branch");
                var counterror=document.getElementById("hdnerrorcount").value;
                counterror=eval(eval(counterror)+1);
                document.getElementById("hdnerrorcount").value = counterror;
                $('span.#cmbBranchMsg').show();
                flag = false;
        }

    }
    if($('#partnerType').val()=="ScheduleBank"){
        if($('span.#NtnlIdMsg').html().length>2){
            $('span.#NtnlIdMsg').html("");
               //flag = true;
        }else{
            $('span.#NtnlIdMsg').html("");
            // return false;
            // flag =  true;
        }
    }else{
        if($('#partnerType').val()!='ScheduleBank'){
            if($('#cmbDescmbBankignation').val()==""){
                     $('span.#cmbBranchMsg').html("Please select <%=title1%> branch");
                     var counterror=document.getElementById("hdnerrorcount").value;
                     counterror=eval(eval(counterror)+1);
                     document.getElementById("hdnerrorcount").value = counterror;
                     $('span.#cmbBranchMsg').show();
                     flag = false;
            }
        }
    }
    if(flag==false){
        return false;
    }else{
        if(document.getElementById("hdnerrorcount").value="1"){
        }else{
            return false;
        }
    }
}

    function checkNatId()
            {
                var flag = false;
                if($('#bankChecker').attr("checked")==false){
                        if($('#cmbDescmbBankignation').val()==""){
                            $('span.#cmbBranchMsg').html("Please Select Branch");
                            $('span.#cmbBranchMsg').show();
                            flag = false;
                        }
                    }
                if($('#partnerType').val()=="ScheduleBank"){
                    if($('span.#NtnlIdMsg').html()=="Ok"){
                        $('span.#NtnlIdMsg').html("");
                        flag = true;
                    }
                    else{
                        $('span.#NtnlIdMsg').html("");
                        return false;
                       flag =  true;
                    }


                }else{
                     if($('#partnerType').val()!='ScheduleBank'){
                    if($('#cmbDescmbBankignation').val()==""){
                            $('span.#cmbBranchMsg').html("Please Select Branch");
                            $('span.#cmbBranchMsg').show();
                            flag = false;
                        }else{
                            flag = true;
                        }
            }
                }
                if(flag){
                    return true;
                }else{
                    return false;
                }
            }
--%>
function validateUser(){
        var flag = true;
        var spaceTest = "";
        
    if($('#cmbOffice')!=null){
        //alert($('#cmbOffice').val())
            if($('#cmbOffice').val()==""){
                $('#spanOffice').html('Please select <%=title1%>');
                 $('#spanOffice').show();
                flag = false;
            }else{
                $('#spanOffice').html('');
            }
            //alert("cmbOffice"+flag);
        }
        if($('#partnerType').val()!='ScheduleBank'){
            if($('#cmbDescmbBankignation')!=null){
            
                        if($('#cmbDescmbBankignation').val()==""){
                        $('#cmbBranchMsg').html('Please select <%=title1%> Branch');
                        $('#cmbBranchMsg').show();
                        flag = false;
                    }else{
                        $('#cmbBranchMsg').html('');
                        $('#cmbBranchMsg').hide();
                    }
                }
               // alert("cmbBranchMsg development"+flag);
        }else{
            //alert($('#bankChecker').attr("checked"));
            if($('#bankChecker').attr("checked")==false){
             if($('#cmbDescmbBankignation').val()==""){
                        $('#cmbBranchMsg').html('Please select <%=title1%> Branch');
                        $('#cmbBranchMsg').show();
                        flag = false;
                    }else{
                        $('#cmbBranchMsg').html('');
                        $('#cmbBranchMsg').hide();
                    }
            }
            //alert("cmbBranchMsg scheduleBank"+flag);
        }
         //alert($('#txtMail').val())
        if($('#txtMail').val()==""){
                $('span.#mailMsg').css("color","red");
                $('#mailMsg').html('Please enter e-mail ID');
                $('#mailMsg').show();
                flag = false;
            }else{
                $('#mailMsg').html('');
                $('#mailMsg').hide();
                spaceTest = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;
                if(!spaceTest.test($('#txtMail').val())){
                    $('#mailMsg').html('Please enter valid e-mail ID');
                    $('#mailMsg').show();
                    flag = false;
                }else{
                    $('#mailMsg').html('');
                    $('#mailMsg').hide();
                }
                //alert("MailMsg"+flag);
            }
             //alert($('#txtPass').val())
            if($('#txtPass').val()==""){
                $('#passMsg').html('Please enter password');
                $('#passMsg').show();
                flag = false;
            }else{
                $('#passMsg').html('');
                $('#passMsg').hide();
                spaceTest = /^\s+|\s+$/g;
                if(spaceTest.test($('#txtPass').val())){
                //alert('here');
                    $('#passMsg').html('Space is not allowed');
                    //alert('here space');
                    $('#passMsg').show();
                    flag = false;
                }else{
                    $('#passMsg').html('');
                    $('#passMsg').hide();
                }
                //alert($('#txtPass').val());
                spaceTest = /^(?![a-zA-Z\s\[\]\!\(\)\-\.\?\_\~\`\@\#\$\%\&\*]+$)(?![0-9\s\[\]\!\(\)\-\.\?\_\~\`\@\#\$\%\&\*]+$)([a-zA-Z 0-9\s\[\]\!\(\)\-\.\?\_\~\`\@\#\$\%\&\*]+$)/;
                //alert(spaceTest.test($('#txtPass').val())+"spaceTest.test($('#txtPass').val())");
                if(!spaceTest.test($('#txtPass').val())){
                    $('#passMsg').html('Please enter only Alphanumeric value');
                    //alert('here in alpha')
                    $('#passMsg').show();
                    flag = false;
                }else{
                    $('#passMsg').html('');
                    $('#passMsg').hide();
                }
               // alert($('#txtPass').val().length);
                if($('#txtPass').val().length<8){
                    $('#passMsg').html('Please enter atleast 8 character and password must contain both alphabets and number');
                    $('#passMsg').show();
                    //alert('here min char')
                    flag = false;
                }else{
                    $('#passMsg').html('');
                    $('#passMsg').hide();
                }
                if($('#txtPass').val().length>25){
                    $('#passMsg').html('Maximum 25 characters are allowed');
                    //alert('here max char');
                    $('#passMsg').show();
                    flag = false;
                }else{
                    $('#passMsg').html('');
                    $('#passMsg').hide();
                }
                //alert($('#txtPass').val().indexOf(" ")+"---"+$('#confPassword').val());
                
                //alert("passMsg"+flag);
            }
            //alert($('#txtConfPass').val())
            if($('#txtConfPass').val()==''){
                $('#confPassMsg').html('Please retype Password');
                $('#confPassMsg').show();
                        flag = false;
            }
            else{
                $('#confPassMsg').html('');
                $('#confPassMsg').hide();
                
                if(!$('#txtConfPass').val().indexOf(" ") < 0 && !$('#txtConfPass').val() != ""){
                        $('#confPassMsg').html('Space is not allowed');
                        $('#confPassMsg').show();
                        flag = false;
                }else{
                    $('#confPassMsg').html('');
                    $('#confPassMsg').hide();
                }
                if($('#txtConfPass').val() != $('#txtPass').val()){
                    $('#confPassMsg').html('Password does not match. Please try again');
                    $('#confPassMsg').show();
                    flag = false;
                }else{
                    $('#confPassMsg').html('');
                    $('#confPassMsg').hide();
                }
                //alert("confPassMsg"+flag);
            }
           // alert($('#txtFullName').val())
            if($('#txtFullName').val()==''){
                $('#spnFullName').html('Please enter Full Name');
                $('#spnFullName').show();
                        flag = false;
                
            }else{
                $('#spnFullName').html('');
                $('#spnFullName').hide();
                spaceTest = /^\s+|\s+$/g;
                if(spaceTest.test($('#txtFullName').val())){
                //alert('here');
                    $('#spnFullName').html('Only Space is not allowed');
                    $('#spnFullName').show();
                    flag = false;
                }else{
                    $('#spnFullName').html('');
                    $('#spnFullName').hide();
                }
                //spaceTest = /^([\a-zA-Z]+([\a-zA-Z\s.]+[\s-/._,\(\)]+)?)+$/;
                spaceTest =  /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/
                if(!spaceTest.test($('#txtFullName').val())){
                    $('#spnFullName').html('Allows Characters (A to Z) & Special Characters (& , \' " } { - . _) Only');
                    $('#spnFullName').show();
                    flag = false;
                }else{
                    $('#spnFullName').html('');
                    $('#spnFullName').hide();
                }if($('#txtFullName').val().length>100){
                    $('#spnFullName').html('Allows maximum 100 characters only.');
                    $('#spnFullName').show();
                    flag = false;
                }else{
                    $('#spnFullName').html('');
                    $('#spnFullName').hide();
                }
                //alert("spnFullName"+flag);
            }
             //alert($('#txtDesignation').val())
            if($('#txtDesignation').val()==''){
                $('#spnDesignation').html('Please enter Designation');
                $('#spnDesignation').show();
                        flag = false;
            }else{

                $('#spnDesignation').show();
                if(document.getElementById('txtDesignation').value.charAt(0) == " "){
                    $('#spnDesignation').html('Only Space is not allowed');
                    flag = false;
                }/*else {
                    $('#spnDesignation').html('');
                    //return true; -
                }*/
                else if(!chkreg($('#txtDesignation').val())){
                    $('#spnDesignation').html('Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' " } { - . _) Only');
                    flag = false;
                } else if($('#txtDesignation').val().length>50){
                     $('#spnDesignation').html('Allows maximum 50 characters only');
                     $('#spnDesignation').show();
                     flag = false;
                }else{
                    $('#spnDesignation').html('');
                    $('#spnDesignation').hide();
                }
                //alert("spnDesignation"+flag);
            }
            
            //alert($('#txtMob').val())
                if($('#txtMob').val()!=''){
                    spaceTest =  /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
                        if(!spaceTest.test($('#txtMob').val())){
                                $('#MobileNoMsg').html('Please enter digits (0-9) only');
                                $('#MobileNoMsg').show();
                                flag = false;
                            }
                        else{
                                $('#MobileNoMsg').html('');
                                $('#MobileNoMsg').hide();
                            if($('#txtMob').val().length < 8){
                                $('#MobileNoMsg').html('Minimum 8 digits are required');
                                $('#MobileNoMsg').show();
                                flag = false;
                            }
                            else if($('#txtMob').val().length > 8){
                                $('#MobileNoMsg').html('Allows maximum 8 digits only');
                                $('#MobileNoMsg').show();
                                flag = false;
                            }else{
                                $('#MobileNoMsg').html('');
                                $('#MobileNoMsg').hide();
                            }
                        }
                          // alert("MobileNoMsg"+flag);
                }
                else if($('#txtMob').val()=='')
                {
                    $('#MobileNoMsg').html('Please Enter Mobile No.');
                    $('#MobileNoMsg').show();
                    flag = false;
                    
                }
                else
                {
                    $('#MobileNoMsg').html('');
                    $('#MobileNoMsg').hide();
                }
                if($('#partnerType').val()=='ScheduleBank'){
                     spaceTest =  /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
                if($('#txtNtnlId').val()!=''){
                    if(!spaceTest.test($('#txtNtnlId').val())){
                        $('#NtnlIdMsg').css("color","red");
                        $('#NtnlIdMsg').html('Please enter digits (0-9) only');
                        $('#NtnlIdMsg').show();
                        flag = false;
                    }else if($('#txtNtnlId').val().length>11){
                        $('#NtnlIdMsg').css("color","red");
                        $('#NtnlIdMsg').html('CID No should comprise of maximum 11 digits');
                        $('#NtnlIdMsg').show();
                        flag = false;
                    }else if($('#txtNtnlId').val().length<11){
                        $('#NtnlIdMsg').css("color","red");
                        $('#NtnlIdMsg').html('CID No should comprise of minimum 11 digits');
                        $('#NtnlIdMsg').show();
                        flag = false;
                    }else{
                            $('#NtnlIdMsg').html('');
                            $('#NtnlIdMsg').hide();
                        }
                }else if($('#txtNtnlId').val()=='')
                {
                    $('#NtnlIdMsg').html('Please enter CID No.');
                    $('#NtnlIdMsg').show();
                    flag = false;
                }
                else
                {
                            $('#NtnlIdMsg').html('');
                            $('#NtnlIdMsg').hide();
        
                }
                        //alert("NtnlIdMsg"+flag);
                   if($('#chekerMsg').val().length>2){
                   //alert("chekerMsg"+flag);
                    flag = false;
                }
                }
                
                if($('#mailMsg').val().length>2){
                     //alert("mailMsg"+flag);
                    flag = false;
                }
                //alert(flag);
                if(flag==false){
                            return false;
                        }else{
                            return true;
                        }
}
function chkreg(value)
            {
                return /^[a-zA-Z 0-9](?![0-9]+$)[a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+$/.test(value);

            }
function officeVali(){
    if($('#cmbOffice')!=null){
        //alert($('#cmbOffice').val())
            if($('#cmbOffice').val()==""){
                $('#spanOffice').html('Please select <%=title1%>');
                 $('#spanOffice').show();
            }else{
                $('#spanOffice').html('');
            }
            //alert("cmbOffice"+flag);
        }
}
function confPassWordVali(){
    if($('#txtConfPass').val()==''){
                $('#confPassMsg').html('Please retype Password');
                $('#confPassMsg').show();
            }
            else{
                $('#confPassMsg').html('');
                $('#confPassMsg').hide();
                if(!$('#txtConfPass').val().indexOf(" ") < 0 && !$('#txtConfPass').val() != ""){
                        $('#confPassMsg').html('Space is not allowed');
                        $('#confPassMsg').show();
                }else{
                    $('#confPassMsg').html('');
                    $('#confPassMsg').hide();
                }
                if($('#txtConfPass').val() != $('#txtPass').val()){
                    $('#confPassMsg').html('Password does not match. Please try again');
                    $('#confPassMsg').show();
                }else{
                    $('#confPassMsg').html('');
                    $('#confPassMsg').hide();
                }
            }
}
function mobileNoVali(){
    var spaceTest =  /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
    if($('#txtMob').val()!=''){
                        if(!spaceTest.test($('#txtMob').val())){
                                $('#MobileNoMsg').html('Please enter digits (0-9) only');
                                $('#MobileNoMsg').show();
                            }
                        else{
                                $('#MobileNoMsg').html('');
                                $('#MobileNoMsg').hide();
                                
                            if($('#txtMob').val().length<8){
                                $('#MobileNoMsg').html('</br>Minimum 8 digits are required');
                                $('#MobileNoMsg').show();
                            }else if($('#txtMob').val().length > 8){
                                $('#MobileNoMsg').html('</br>Allows maximum 8 digits only');
                                $('#MobileNoMsg').show();
                            }else{
                                $('#MobileNoMsg').html('');
                                $('#MobileNoMsg').hide();
                            }
                        }
                }
                else if($('#txtMob').val()=='')
                {
                    $('#MobileNoMsg').html('Please Enter Mobile No.');
                    $('#MobileNoMsg').show();
                    
                }
                else
                {
                    $('#MobileNoMsg').html('');
                    $('#MobileNoMsg').hide();
                }
}
function desigVali(){
    if($('#txtDesignation').val()==''){
                $('#spnDesignation').html('Please enter Designation');
                $('#spnDesignation').show();
            }else{
                $('#spnDesignation').show();
                if(document.getElementById('txtDesignation').value.charAt(0) == " "){
                    $('#spnDesignation').html('Only Space is not allowed');
                    flag = false;
                }/*else {
                    $('#spnDesignation').html('');
                    //return true; -
                }*/
                else if(!chkreg($('#txtDesignation').val())){
                    $('#spnDesignation').html('Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' " } { - . _) Only ');
                    flag = false;
                } else if($('#txtDesignation').val().length>50){
                     $('#spnDesignation').html('Allows maximum 50 characters only');
                     $('#spnDesignation').show();
                     flag = false;
                }else{
                    $('#spnDesignation').html('');
                    $('#spnDesignation').hide();
                }
                /*if($('#txtDesignation').val().length>50){
                     $('#spnDesignation').html('<br/>Allows maximum 50 characters only');
                     $('#spnDesignation').show();
                }else{
                    $('#spnDesignation').html('');
                    $('#spnDesignation').hide();
                }*/
            }
}
function ntnlIdVali(){
    var spaceTest =  /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
    if($('#partnerType').val()=='ScheduleBank'){
        //alert($('#txtNtnlId').val());
        if($.trim($('#txtNtnlId').val())!=''){
            if(!spaceTest.test($('#txtNtnlId').val())){
                $('#NtnlIdMsg').css("color","red");
                $('#NtnlIdMsg').html('Please enter digits (0-9) only');
                $('#NtnlIdMsg').show();
            }else if($('#txtNtnlId').val().length>11){
                $('#NtnlIdMsg').css("color","red");
                $('#NtnlIdMsg').html('CID No should comprise of maximum 11 digits');
                $('#NtnlIdMsg').show();
            }else if($('#txtNtnlId').val().length<11){
                $('#NtnlIdMsg').css("color","red");
                $('#NtnlIdMsg').html('CID No should comprise of minimum 11 digits');
                $('#NtnlIdMsg').show();
            }else{
                $('#NtnlIdMsg').html('');
                $('#NtnlIdMsg').hide();
            }
        }else{
            $('#NtnlIdMsg').html('');
            $('#NtnlIdMsg').hide();
        }
    }
}
function fullNameVali(){
    var spaceTest = "";
    var flag = true;
if($('#txtFullName').val()==''){
                $('#spnFullName').html('Please enter Full Name');
                $('#spnFullName').show();
            }else if($('#txtFullName').val()!=''){
                $('#spnFullName').html('');
                $('#spnFullName').hide();
                spaceTest = /^\s+|\s+$/g;
                if(spaceTest.test($('#txtFullName').val())){
                    $('#spnFullName').html('Only Space is not allowed');
                    $('#spnFullName').show();
                    flag = false;
                }
                if(flag==true){
                        //spaceTest = /^([a-zA-Z]+[a-zA-Z0-9]+([a-zA-Z \s\-\.\"\_\&\`]){0,100}$)/
                    //alert(spaceTest.test($('#txtFullName').val()));
                        //spaceTest =/^[a-zA-Z0-9\s.\-\&\'\"\_]+$/;
                        spaceTest =  /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/
                       //spaceTest = /^([a-zA-Z]+[a-zA-Z0-9]+([a-zA-Z \s\-\.\"\_\&\`])*$)/
                    //spaceTest = /^([\a-zA-Z]+([\a-zA-Z\s.]+[\s-/._,\(\)]+)?)+$/;
                    //alert(spaceTest.test($('#txtFullName').val()));
                    if($('#txtFullName').val().length>100){
                        $('#spnFullName').html('Allows maximum 100 characters only.');
                        $('#spnFullName').show();
                    }
                    if(!spaceTest.test($('#txtFullName').val())){
                        $('#spnFullName').html('Allows Characters (A to Z) & Special Characters (& , \' " } { - . _) Only');
                        $('#spnFullName').show();
                    }
                }
            }else{
                    $('#spnFullName').html('');
                    $('#spnFullName').hide();
            }
}
function passWordVali(){
    var spaceTest = "";
    if($('#txtPass').val()==""){
                $('#passMsg').html('Please enter password');
                $('#passMsg').show();
            }

            else if($('#txtPass').val()!=""){
                $('#passMsg').html('');
                $('#passMsg').hide();
                spaceTest = /^\s+|\s+$/g;
                if(spaceTest.test($('#txtPass').val())){
                    $('#passMsg').html('Space is not allowed');
                    //alert('here space');
                    $('#passMsg').show();
                    return  false;
                }
                spaceTest = /^(?![a-zA-Z\s\[\]\!\(\)\-\.\?\_\~\`\@\#\$\%\&\*]+$)(?![0-9\s\[\]\!\(\)\-\.\?\_\~\`\@\#\$\%\&\*]+$)([a-zA-Z 0-9\s\[\]\!\(\)\-\.\?\_\~\`\@\#\$\%\&\*]+$)/;
                if(!spaceTest.test($('#txtPass').val())){
                    $('#passMsg').html('Please enter only Alphanumeric value');
                    //alert('here in alpha')
                    $('#passMsg').show();
                    return false;
                }
               // alert($('#txtPass').val().length);
                if($('#txtPass').val().length<8){
                    $('#passMsg').html('Please enter atleast 8 character and password must contain both alphabets and number');
                    $('#passMsg').show();
                    //alert('here min char')
                    return false;
                }
                if($('#txtPass').val().length>25){
                    $('#passMsg').html('Maximum 25 characters are allowed');
                    //alert('here max char');
                    $('#passMsg').show();
                    return false;
                }
                
            }else{
                    $('#passMsg').html('');
                    $('#passMsg').hide();
                }
            
}

function setBranch(){
    $.post("<%=request.getContextPath()%>/ComboServlet", {type:'<%=title1%>',objectId:$('#cmbOffice').val(),funName:'branchCombo'}, function(j){
        $("select#cmbDescmbBankignation").html(j);
    });
}
        </script>
<script type="text/javascript">
function errorPt(){
             jAlert("Copy and Paste not allowed.","<%=title1%> User", function(RetVal) {
            });
            return false;
}
</script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Dashboard Header End-->

                <!--Dashboard Content Part Start-->

                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr valign="top">

                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>

                        <td class="contentArea_1">
                            <div class="pageHead_1">Create <%=title1%> Branch User</div>
                            <!-- Success failure -->
                            <%
                                        String msg = request.getParameter("msg");
                                        if (msg != null && msg.equals("fail")) {%>
                            <div class="responseMsg errorMsg"><%=title%> User Creation Failed.</div>
                            <%} else if (msg != null && msg.equals("success")) {%>
                            <div class="responseMsg successMsg"><%=title%> User Created Successfully</div>
                            <%}%>
                            <%
                                        int strM = -1;
                                        String msgValidate = "";
                                        if (request.getParameter("strM") != null) {
                                            try {
                                                strM = Integer.parseInt(request.getParameter("strM"));
                                            } catch (Exception ex) {
                                                strM = 4;
                                            }
                                            switch (strM) {
                                                case (1):
                                                    msg = "Please Enter Valid e-mail ID";
                                                    break;
                                                case (2):
                                                    msg = "e-mail ID already exists, Please enter another e-mail ID";
                                                    break;
                                                case (3):
                                                    msg = "CID No. already exists";
                                                    break;
                                                case (4):
                                                    msg = "Please Enter Valid Mobile No";
                                                    break;
                                                case (5):
                                                    msg = "Mobile No already exists";
                                                    break;
                                            }
                            %>
                            <div class="responseMsg errorMsg" style="margin-top: 10px;"><%=msg%></div>
                            <%
                                            msgValidate = null;
                                        }
                            %>
                            <!-- Result Display End-->
                            <jsp:useBean id="scBankCreationSrBean" class="com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean" scope="request"/>
                            <%
                            
                                    String logUserId = "0";
                                    if(session.getAttribute("userId")!=null){
                                                logUserId = session.getAttribute("userId").toString();
                                        }
                                    scBankCreationSrBean.setLogUserId(logUserId);
                                    scBankCreationSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                            %>

                            <form method="POST" id="frmSchedBankUser" action="<%=request.getContextPath()%>/ScBnkDevpartUserSrBean">

                                <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">

                                    <%if (!"Development".equalsIgnoreCase(type)) {%>
                                    <tr>
                                        <td class="ff" width="31%">
                                            Select Role :
                                        </td>
                                        <td width="69%">
                                            <label><input type="radio" name="userRole" id="bankCheckerRole" value="BranchMaker" checked="checked" /> Branch Maker</label>
                                            <%--<label><input type="radio" name="userRole" id="bankChecker" value="BankChecker" onclick="return RadioClick();"/> Bank Checker &nbsp;</label>--%>
                                            <label><input type="radio" name="userRole" id="branchChecker" value="BranchChecker" /> Branch Checker &nbsp;</label>


                                             &nbsp;&nbsp;&nbsp;<span id="chekerMsg" style="font-weight: bold">&nbsp;</span>
                                        </td>
                                    </tr>
                                    <%}%>
                                    <%
                                                    if (userTypeId != 1) {
                                                        int userId = Integer.parseInt(session.getAttribute("userId").toString());
                                                        scBankCreationSrBean.getBankName(userId, (byte) userTypeId);
                                                        if(userTypeId == 7 || userTypeId == 6){
                                        %>
                                    <tr>


                                        <td class="ff" width="31%"><%=title1%> :</td>
                                        
                                        <td width="69%"><%=scBankCreationSrBean.getSbDevelopName()%>
                                            <%
                                                                                                    scBankCreationSrBean.setsBankDevelHeadId(scBankCreationSrBean.getsBankDevelopId());
                                            %>
                                            <input type="hidden" name="office" id="cmbOffice" value="${scBankCreationSrBean.sBankDevelopId}"/>
                                        </td>
                                       
                                    </tr>
                                    <tr id="cmbBranchTr">
                                        <td class="ff" width="31%">Select <%=title1%> Branch : <span>*</span></td>

                                        <td width="69%">
                                            <select name="branch"  id="cmbDescmbBankignation" class="formTxtBox_1" style="width: 200px" >
                                                <option selected="selected" value="">-- Select <%=title1%> Branch --</option>
                                                <c:forEach var="cList" items="${scBankCreationSrBean.branchList}">
                                                    <option  value="${cList.objectId}">${cList.objectValue}</option>
                                                </c:forEach>

                                            </select>
                                            <br /><span id="cmbBranchMsg" class="reqF_1" style="display: none;"></span>
                                        </td>
                                    </tr>
                                    <%}if(userTypeId==15){
                                        List<Object[]> list;
                                            list = scBankCreationSrBean.getBankBranchByUserId(session.getAttribute("userId").toString());
                                            if(list!=null && !list.isEmpty()){
                                    %>
                                    <tr>
                                        <td class="ff" width="30%"><%=title%> : </td>
                                        <td>
                                            <label><%=list.get(0)[3]%></label>
                                            <input type="hidden" name="office" value="<%=list.get(0)[2]%>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="30%"><%=title1%> Branch: </td>
                                        <td>
                                            <label><%=list.get(0)[1]%></label>
                                            <input type="hidden" name="branch" value="<%=list.get(0)[0]%>" />
                                        </td>
                                    </tr>
                                    <%}/*Check null and size of list*/}/*Condtion for UserTypeId = 15 Ends*/ }/*If userId!=1 Ends here*/ else {%>
                                    <tr>
                                        <td class="ff" width="30%">Select <%=title1%> : <span>*</span></td>
                                        <td width="70%"><select name="office"  id="cmbOffice" class="formTxtBox_1" style="width: 200px" onchange="setBranch(); officeVali();">
                                                <option selected="selected" value="">-- Select <%=title1%> --</option>
                                                <%
                                                                                                        for (SelectItem officeItem : scBankCreationSrBean.getBankPartnerList(type)) {
                                                %>
                                                <option value="<%=officeItem.getObjectId()%>"><%=officeItem.getObjectValue()%></option>
                                                <%
                                                    int id = Integer.parseInt(officeItem.getObjectId().toString());
                                                                                                                                                            
                                                %>

                                                <%scBankCreationSrBean.setsBankDevelHeadId(id);%>
                                                <%}%>
                                            </select>
                                            <span class="reqF_1" id="spanOffice" style="display: none;"></span>
                                        </td>
                                    </tr>
                                    <tr id="cmbBranchTr">
                                        <td class="ff" width="35%">Select <%=title1%> <%=details%>: <span>*</span></td>

                                        <td width="65%">
                                            <select name="branch"  id="cmbDescmbBankignation" class="formTxtBox_1" style="width: 200px" >
                                                <option selected="selected" value="">-- Select <%=title%> <%=details%> --</option>
                                            </select>
                                              <br /><span id="cmbBranchMsg" class="reqF_1" style="display: none">&nbsp;</span>
                                        </td>
                                    </tr>
                                    <%}/*If userId = 1*/%>
                                    <input type="hidden" id="partnerType" name="partnerType" value="<%=type%>"/>
                                    <input type="hidden" name="isAdmin" value="No"/>
                                    <input type="hidden" name="action" value="create"/>
                                    <input type="hidden" id="hdnerrorcount" value="0"/>
                                    <tr>
                                        <td class="ff" width="30%">e-mail ID : <span>*</span></td>
                                        <td width="70%"><input name="mailId" id="txtMail" type="text" class="formTxtBox_1"  style="width:200px;" />
                                            <br /><span id="mailMsg" class="reqF_1" style="display: none;"></span></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="30%">Password : <span>*</span></td>
                                        <td width="70%"><input name="password" id="txtPass" type="password" class="formTxtBox_1" style="width:200px;" maxlength="25" onblur="return passWordVali();" autocomplete="off" /><br/><span id="tipPassword" style="color: grey;"> (Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. 
                                                <br/>Special characters may be added) </span>
                                            <div class="reqF_1" id="passMsg" style="display: none;"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Confirm Password : <span>*</span></td>
                                        <td><input name="confPassword" id="txtConfPass" type="password" class="formTxtBox_1" style="width:200px;" onblur="return confPassWordVali();" onpaste="return errorPt();" autocomplete="off" />
                                            <span class="reqF_1" id="confPassMsg" style="display: none;"></span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff">Full Name : <span>*</span></td>
                                        <td><input name="fullName" id="txtFullName" type="text" class="formTxtBox_1" style="width:200px;" onblur="return fullNameVali();"/>
                                        <span class="reqF_1" id="spnFullName" style="display: none;"></span>
                                        </td>
                                    </tr>
                                    <%-- <tr>
                                         <td class="ff">Name in Bangla : </td>
                                         <td><input name="bngName" id="txtBngName" type="text" class="formTxtBox_1" style="width:200px;" /></td>
                                     </tr>--%>

                                    <%if (!"Development".equalsIgnoreCase(type)) {%>
                                    <tr>

                                        <td class="ff">CID No. : <span>*</span></td>
                                        <td><input name="ntnlId" id="txtNtnlId" type="text" class="formTxtBox_1" style="width:200px;" maxlength="26" onblur="return ntnlIdVali();"/>
                                            <span id="NtnlIdMsg" class="reqF_1" style="display: none;">&nbsp;</span></td>
                                    </tr>
                                    <%}%>
                                    <tr>
                                        <td class="ff">Designation : <span>*</span></td>
                                        <td><input name="designation" id="txtDesignation" type="text" class="formTxtBox_1" style="width:200px;" maxlength="51" onblur="return desigVali();"/>
                                            <span class="reqF_1" id="spnDesignation" style="display: none;"></span>
                                            <%--<select name="designation"  id="cmbDesignation" class="formTxtBox_1" style="width: 200px">
                                                <option selected="selected" value="">-- Select Designation --</option>
                                                <%
                                                            for (SelectItem designationItem : scBankCreationSrBean.getDesignationList()) {
                                                %>
                                                <option value="<%=designationItem.getObjectId()%>"><%=designationItem.getObjectValue()%></option>
                                                <%}%>
                                            </select>--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Mobile No : <span>*</span></td>
                                        <td><input name="mobileNo" id="txtMob" type="text" class="formTxtBox_1" style="width:200px;" onblur="return mobileNoVali();" maxlength="16" /><span id="mNo" style="color: grey;"> (Mobile No. format should be e.g 12345678)</span>
                                            <span id="MobileNoMsg" class="reqF_1" style="display: none;"></span></td>
                                    </tr>
                                    <%--  <tr>
                                          <td class="ff">fax No : </td>
                                          <td><input name="faxNo" type="text" class="formTxtBox_1" id="txtFax" style="width:200px;" /></td>
                                      </tr>--%>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><label class="formBtn_1">
                                                <input type="submit" name="btnSubmit" id="btnSubmit" value="Submit" onclick="return validateUser();"/>
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <!--Dashboard Content Part End-->
                </table>
                <!--Dashboard Footer Start-->
                <%@include file="../resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
                <script>
                var obj = document.getElementById('lblDevPartnerUsersCreation');
                if(obj != null){
                   // if(obj.innerHTML == 'Create'){
                        obj.setAttribute('class', 'selected');
                  //  }
                }

                var obj1 = document.getElementById('lblDevPartnerUsersCreation');
                if(obj1 != null){
                   // if(obj1.innerHTML == 'Create Development Partner User'){
                        obj1.setAttribute('class', 'selected');
                   // }
                }

        </script>
                <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
