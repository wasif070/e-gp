<%-- 
    Document   : STDRule
    Created on : Nov 15, 2010, 4:40:59 PM
    Author     : Naresh.Akurathi
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigStd"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigPreTender"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Standard Bidding Document Selection Rule</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <%!    ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();

        %>


        <%
                    StringBuilder tenderType = new StringBuilder();
                    StringBuilder procNature = new StringBuilder();
                    StringBuilder procType = new StringBuilder();
                    StringBuilder procMethod = new StringBuilder();
                    StringBuilder templateMaster = new StringBuilder();


                    TblProcurementNature tblProcureNature2 = new TblProcurementNature();
                    Iterator pnit2 = configPreTenderRuleSrBean.getProcurementNature().iterator();
                    while (pnit2.hasNext()) {
                        tblProcureNature2 = (TblProcurementNature) pnit2.next();
                        procNature.append("<option value='" + tblProcureNature2.getProcurementNatureId() + "'>" + tblProcureNature2.getProcurementNature() + "</option>");
                    }

                    TblProcurementMethod tblProcurementMethod2 = new TblProcurementMethod();
                    Iterator pmit2 = configPreTenderRuleSrBean.getProcurementMethod().iterator();
                    while (pmit2.hasNext()) {
                        tblProcurementMethod2 = (TblProcurementMethod) pmit2.next();
                        procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>" + tblProcurementMethod2.getProcurementMethod() + "</option>");
                    }

                    TblProcurementTypes tblProcurementTypes2 = new TblProcurementTypes();
                    Iterator ptit2 = configPreTenderRuleSrBean.getProcurementTypes().iterator();
                    String procureType="";
                    while (ptit2.hasNext()) {
                        tblProcurementTypes2 = (TblProcurementTypes) ptit2.next();
                        if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("NCT"))
                            procureType="NCB";
                        else if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("ICT"))
                            procureType="ICB";
                        procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + procureType + "</option>");
                    }

                    TblTemplateMaster tblTemplateMaster2 = new TblTemplateMaster();
                    Iterator tmit = configPreTenderRuleSrBean.getTemplateMaster().iterator();
                    while (tmit.hasNext()) {
                        tblTemplateMaster2 = (TblTemplateMaster) tmit.next();
                        templateMaster.append("<option value='" + tblTemplateMaster2.getTemplateId() + "'>" + tblTemplateMaster2.getTemplateName() + "</option>");
                    }

        %>


      
    </head>
    <body>

        <%
                    String msg = "";
                    if ("Submit".equals(request.getParameter("button"))) {
                        int row = Integer.parseInt(request.getParameter("TotRow"));
                        msg = configPreTenderRuleSrBean.delAllConfigStd();
                        if (msg.equals("Deleted")) {
                            for (int i = 1; i <= row; i++) {
                                if (request.getParameter("pNature" + i) != null) {

                                    byte pnature = Byte.parseByte(request.getParameter("pNature" + i));
                                    byte pmethod = Byte.parseByte(request.getParameter("pMethod" + i));
                                    byte ptypes = Byte.parseByte(request.getParameter("pType" + i));
                                    float tvalues = Float.parseFloat(request.getParameter("values" + i));
                                    short stdName = Short.parseShort(request.getParameter("stdName" + i));

                                    TblConfigStd tblConfigStd = new TblConfigStd();
                                    tblConfigStd.setTblProcurementNature(new TblProcurementNature(pnature));
                                    tblConfigStd.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                    tblConfigStd.setTblProcurementTypes(new TblProcurementTypes(ptypes));
                                    tblConfigStd.setOperator(request.getParameter("operator" + i));
                                    tblConfigStd.setTenderValue(new BigDecimal(request.getParameter("values" + i)));
                                    tblConfigStd.setTblTemplateMaster(new TblTemplateMaster(stdName));

                                    msg = configPreTenderRuleSrBean.addConfigStd(tblConfigStd);

                                }
                            }
                        }
                        if (msg.equals("Values Added")) {
                            msg = "SBD Business Rule Configured Successfully";
                            response.sendRedirect("STDRuleView.jsp?msg=" + msg);
                        }

                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
                        <!--Dashboard Header End-->
                        <!--Dashboard Content Part Start-->
                        <div class="pageHead_1">Standard Bidding Document Selection Rule</div>

                        <div style="font-style: italic" class="t-align-left t_space"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>

                        <div align="right" class="t_space b_space">
                            <span id="lotMsg" style="color: red; font-weight: bold; float: left; visibility: collapse;">&nbsp;</span>
                            <a id="linkAddRule" class="action-button-add">Add Rule</a> <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a>

                        </div>

                        <%--<div align="center"> <%=msg%></div>--%>
                        <form action="STDRule.jsp" method="post">
                            <table width="100%" cellspacing="0" class="tableList_1" id="tblstd" name="tblstd">
                                <tr>
                                    <th nowrap>Select</th>
                                    <th nowrap>Procurement<br/>Category<br />(<span class="mandatory">*</span>)</th>
                                    <th nowrap>Procurement<br/> Method<br />(<span class="mandatory">*</span>)</th>
                                    <th nowrap>Procurement<br/> Type<br />(<span class="mandatory">*</span>)</th>
                                    <th nowrap>Operator<br />(<span class="mandatory">*</span>)</th>
                                    <th nowrap>Value (in Nu.)<br />(<span class="mandatory">*</span>)</th>
                                    <th nowrap >SBD Name<br />(<span class="mandatory">*</span>)</th>
                                </tr>
                                <input type="hidden" name="delrow" value="0" id="delrow"/>
                                <input type="hidden" name="TotRow" id="TotRow" value="1"/>
                                <input type="hidden" name="introw" value="" id="introw"/>
                                <tr id="trstd_1">
                                    <td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_1" value="" /></td>
                                    <td class="t-align-center"><select name="pNature1" class="formTxtBox_1" id="pNature_1">
                                            <%=procNature%>
                                        </select></td>
                                    <td class="t-align-center"><select name="pMethod1" class="formTxtBox_1" id="pMethod_1">
                                            <%=procMethod%>
                                        </select></td>
                                    <td class="t-align-center"><select name="pType1" class="formTxtBox_1" id="pType_1">
                                            <%=procType%>
                                        </select></td>
                                    <td class="t-align-center"><select name="operator1" class="formTxtBox_1" id="operator_1">
                                            <option><</option>
                                            <option><=</option>
                                            <option>></option>
                                            <option>>=</option>
                                            <option>=</option>
                                        </select></td>
                                    <td class="t-align-center"><input name="values1" type="text" class="formTxtBox_1" id="values_1" onblur="return chkValueBlank(this);"/><span id="val_1" style="color: red;">&nbsp;</span></td>
                                    <td class="t-align-center"><select name="stdName1" class="formTxtBox_1" id="stdName_1">
                                            <%=templateMaster%></select>
                                    </td>
                                </tr>
                                <tbody id="trbody"></tbody>
                            </table>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" >
                                <tr>
                                    <td align="right" colspan="9" class="t-align-center">
                                        <span class="formBtn_1"><input type="submit" name="button" id="button" value="Submit"  onclick="return validate();"/></span>
                                    </td>
                                </tr>
                            </table>

                        </form>
                    </td>
                </tr>
            </table>

            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>
             <script>
                var obj = document.getElementById('lblSTDRuleAdd');
                if(obj != null){
                    if(obj.innerHTML == 'Add'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
              <script type="text/javascript">


            var totCnt = 1;

            $(function() {
                $('#linkAddRule').click(function() {
                    var count=($("#tblstd tr:last-child").attr("id").split("_")[1]*1);

                    var htmlEle = "<tr id='trstd_"+ (count+1) +"'>"+
                        "<td class='t-align-center'><input type='checkbox' name='checkbox"+(count+1)+"' id='checkbox_"+(count+1)+"' value='"+(count+1)+"' /></td>"+
                        "<td class='t-align-center'><select name='pNature"+(count+1)+"' class='formTxtBox_1' id='pNature_"+(count+1)+"'><%=procNature%></select></td>"+
                        "<td class='t-align-center'><select name='pMethod"+(count+1)+"' class='formTxtBox_1' id='pMethod_"+(count+1)+"'><%=procMethod%></select></td>"+
                        "<td class='t-align-center'><select name='pType"+(count+1)+"' class='formTxtBox_1' id='pType_"+(count+1)+"'><%=procType%></select></td>"+
                        "<td class='t-align-center'><select name='operator"+(count+1)+"' class='formTxtBox_1' id='operator_"+(count+1)+"'><option><</option><option><=</option><option>></option><option>>=</option><option>=</option></</select></td>"+
                        "<td  class='t-align-center'><input type='text' name='values"+(count+1)+"' class='formTxtBox_1' id='values_"+(count+1)+"'  onblur='return chkValueBlank(this);'/><span id='val_"+(count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><select name='stdName"+(count+1)+"' class='formTxtBox_1' id='stdName_"+(count+1)+"'><%=templateMaster%></select></td>"+
                        "</tr>";

                    totCnt++;
                    $("#tblstd").append(htmlEle);
                    document.getElementById("TotRow").value = (count+1);

                });
            });


            $(function() {
                $('#linkDelRule').click(function() {
                    var delcnt=0;
                    var cnt = 0;
                    var tmpCnt = 0;
                    var counter = ($("#tblstd tr:last-child").attr("id").split("_")[1]*1);
                    for(var i=1;i<=counter;i++){
                        if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                            tmpCnt++;
                        }
                    }

                    if(tmpCnt==totCnt){
                        $('span.#lotMsg').css("visibility","visible");
                        $('span.#lotMsg').css("color","red");
                        $('span.#lotMsg').html('Minimum 1 record is needed!');
                    }else{
                        for(var i=1;i<=counter;i++){
                            if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                                $("tr[id='trstd_"+i+"']").remove();
                                $('span.#lotMsg').css("visibility","collapse");

                                document.getElementById('delrow').value=i;
                                cnt++;
                            }
                        }
                        totCnt -= cnt;
                    }

                });
            });
        </script>
        <script type="text/javascript">
            function validate()
            {
                //alert('dhruti');
                var flag = true;
                var counter = eval(document.getElementById("TotRow").value);
                //alert(counter);
                for(var k=1;k<= counter;k++)
                {
                    if(document.getElementById("values_"+k)!=null)
                    {
                        if(document.getElementById("values_"+k).value==""){
                            document.getElementById("val_"+k).innerHTML="<br/>Please Enter Value.";
                            flag = false;
                        }
                        else
                        {
                            if(decimal(document.getElementById("values_"+k).value))
                            {
                                if(chkMax(document.getElementById("values_"+k).value))
                                {
                                        document.getElementById("val_"+k).innerHTML="";
                                }
                                else
                                {
                                    document.getElementById("val_"+k).innerHTML="<br/>Maximum 12 digits are allowed.";
                                    flag=false;
                                }
                            }
                            else
                            {
                                document.getElementById("val_"+k).innerHTML="<br/>Please Enter numbers and 2 Digits After Decimal";
                                return false;
                            }
                        }
                    }

                }

                // Start OF Dhruti--Checking for Unique Rows

              if(document.getElementById("TotRow")!=null){
                var totalcount = eval(document.getElementById("TotRow").value); }//Total Count After Adding Rows
                //alert(totalcount);
                var chk=true;
                var i=0;
                var j=0;
                for(i=1; i<=totalcount; i++) //Loop For Newly Added Rows
                {
                    if(document.getElementById("values_"+i)!=null)
                    if($.trim(document.getElementById("values_"+i).value)!='') // If Condition for When all Data are filled thiscode is Running
                    {

                        for(j=1; j<=totalcount && j!=i; j++) // Loop for Total Count but Not same as (i) value
                        {
                            chk=true;
                            //If Condition for Check Duplicate Rows are there or not.If Columns are diff then chk variable set to false
                            //IF Row is same Give alert message.
                            if($.trim(document.getElementById("pNature_"+i).value) != $.trim(document.getElementById("pNature_"+j).value))
                            {
                                chk=false;
                                //alert('bt');
                            }
                            if($.trim(document.getElementById("pMethod_"+i).value) != $.trim(document.getElementById("pMethod_"+j).value))
                            {
                                chk=false;
                                //alert('tt');
                            }
                            if($.trim(document.getElementById("pType_"+i).value) != $.trim(document.getElementById("pType_"+j).value))
                            {
                                chk=false;
                                //alert('pm');
                            }
                            if($.trim(document.getElementById("operator_"+i).value) != $.trim(document.getElementById("operator_"+j).value))
                            {
                                chk=false;
                                //alert('pt');
                            }
                            var vi=document.getElementById("values_"+i).value;
                            var vj=document.getElementById("values_"+j).value;
                            if(vi.indexOf(".")==-1)
                            {
                                vi=vi+".00";
                                //alert(vi);
                            }
                            if(vj.indexOf(".")==-1)
                            {
                                vj=vj+".00";
                                //alert(vj);
                            }
                            if($.trim(vi) != $.trim(vj))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("stdName_"+i).value) != $.trim(document.getElementById("stdName_"+j).value))
                            {
                                chk=false;
                            }

                            //alert(j);
                            //alert("chk" +chk);
                            if(flag){
                            if(chk==true) //If Row is same then give alert message
                            {
                                    jAlert("Duplicate record found. Please enter unique record","SBD Selection Business Rule Configuration",function(RetVal) {
                                    });
                                return false;
                            }
                        }
                        }

                    }
                }
                // End OF Dhruti--Checking for Unique Rows

                if (flag == false){
                    return false;
                }
            }
            function decimal(value) {
                return /^(\d+(\.\d{2})?)$/.test(value);
            }
            function chkMax(value)
            {
                var chkVal=value;
                var ValSplit=chkVal.split('.');

                if(ValSplit[0].length>12)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            function chkValueBlank(obj){
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                if(obj.value!='')
                {
                    if(decimal(obj.value))
                    {
                        if(chkMax(obj.value))
                        {
                            document.getElementById("val_"+i).innerHTML="";
                        }
                        else
                        {
                            document.getElementById("val_"+i).innerHTML="<br/>Maximum 12 digits are allowed.";
                            flag=false;
                        }
                    }
                    else
                    {
                        document.getElementById("val_"+i).innerHTML="<br/>Please Enter numbers and 2 Digits After Decimal";
                        return false;
                    }

                }
                else{
                    document.getElementById("val_"+i).innerHTML = "<br/>Please Enter Value.";
                    return false;
                }
            }
        </script>

    </body>
</html>

