<%--
    Document   : PEOfficeCreation
    Created on : Oct 23, 2010, 7:14:31 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblDepartmentMaster"%>
<jsp:useBean id="peOfficeCreationSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.PEOfficeCreationSrBean"/>
<jsp:useBean id="peOfficeCreationDtBean" scope="request" class="com.cptu.egp.eps.web.databean.PEOfficeCreationDtBean"/>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService" %>
<jsp:useBean id="tendererMasterDtBean" class="com.cptu.egp.eps.web.databean.TendererMasterDtBean"/>
<jsp:useBean id="tendererMasterSrBean" class="com.cptu.egp.eps.web.servicebean.TendererMasterSrBean"/>
<jsp:setProperty property="*" name="tendererMasterDtBean"/>
<jsp:setProperty name="peOfficeCreationDtBean" property="*"/>
<%@page buffer="15kb"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            int userId = 0;
            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                userId = Integer.parseInt(session.getAttribute("userId").toString());
                peOfficeCreationSrBean.setLogUserId(session.getAttribute("userId").toString());
            }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PA Office Creation</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>
        <script type="text/javascript">
            
            
            
            function setDeptID(obj) {
                document.getElementById("txtchk").value = obj.value;


            }
            function checkOrgType()
            {
                //document.getElementById("cmbOfcType").style.display = 'block';
                $.post("<%=request.getContextPath()%>/CommonServlet", {deptId: $('#txtdepartmentid').val(), funName: 'checkOrgType'},
                        function (j)
                        {

                            if (j.toString().indexOf("no", 0) != -1) {
                                document.getElementById("cmbOfcType").style.display = 'block';
                                document.getElementById("trOfcType").style.display = 'block';
                            } else {

                                document.getElementById("cmbOfcType").style.display = 'none';
                                document.getElementById("trOfcType").style.display = 'none';
                            }


                        });
            }

        </script>
        <%
            String deptType = "";
            if (!request.getParameter("deptType").equals("")) {
                deptType = request.getParameter("deptType");
            }
        %>
        <script type="text/javascript">
            function alphanumregex(value)
            {
                //return /^([\a-zA-Z\d]+([\sa-zA-Z]+[\d\s-./&]+)?)+$/.test(value);

                return /^([\a-zA-Z\d]?([\sa-zA-Z]+[\d\s-./&]+)?)+$/.test(value);
            }
            function ChkMaxLength(value)
            {
                var ValueChk = value;
                if (ValueChk.length > 150)
                {
                    return false;
                } else
                {
                    return true;
                }
            }
            var bvalidationName;
            function chkreg(value)
            {
                return /^[a-zA-Z 0-9](?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9]+$)+$/.test(value);

            }
            function myFunction(val) {
                alert("The input value has changed. The new value is: " + val);
            } 
            $(function () {
                $('#txtOfficeName').blur(function () {
                    bvalidationName = true;
                    if ($('#txtOfficeName').val() == '') {
                        $('#Msg').html("");
                        $('#readonlyMsg').html('Please enter Office Name');
                    } else if (document.getElementById('txtOfficeName').value.charAt(0) == " ") {
                        $('#Msg').html("");
                        $('#readonlyMsg').html('Only Space is not allowed');
                        bvalidationName = false;
                    } else if (!chkreg($('#txtOfficeName').val())) {
                        $('#Msg').html("");
                        $('#readonlyMsg').html('Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' " } { - . _) Only');
                        bvalidationName = false;
                    } else if (document.getElementById('txtOfficeName').value.length > 100) {
                        $('#Msg').html("");
                        $('#readonlyMsg').html('Allows maximum 100 characters only');
                        bvalidationName = false;
                    } else {
                        $('#readonlyMsg').html('');
                        if ($('#userTID').val() == 5) {
                            $('#Msg').css("color", "red");
                            $('#Msg').html("Checking for unique Office Name...");
                            $.post("<%=request.getContextPath()%>/CommonServlet", {officeName: $.trim($('#txtOfficeName').val()), deptId: $('#txtdepartmentid').val(), funName: 'verifyOfficeName'},
                                    function (j)
                                    {
                                        if (j.toString().indexOf("OK", 0) != -1) {
                                            $('#Msg').css("color", "green");
                                            $('#errMsg').html('');
                                        } else if (j.toString().indexOf("Office", 0) != -1) {
                                            $('#Msg').css("color", "red");
                                            $('#errMsg').html('');
                                        }
                                        $('#c').html(j);

                                        if (document.getElementById('oName') == null) {
                                            $('#Msg').html(j);
                                            $('#errMsg').html('');
                                        } else {
                                            document.getElementById('oName').removeAttribute('id');
                                            $('#Msg').html('');
                                            $('#errMsg').html('');
                                        }
                                    });
                        } else
                        {
                            $('#Msg').css("color", "red");
                            $('#Msg').html("Checking for unique Office Name...");
                            $.post("<%=request.getContextPath()%>/CommonServlet", {officeName: $.trim($('#txtOfficeName').val()), deptId: $('#txtchk').val(), funName: 'verifyOfficeName'},
                                    function (j)
                                    {
                                        if (j.toString().indexOf("OK", 0) != -1) {
                                            $('#Msg').css("color", "green");
                                            $('#errMsg').html('');
                                        } else if (j.toString().indexOf("Office", 0) != -1) {
                                            $('#Msg').css("color", "red");
                                            $('#errMsg').html('');

                                        }
                                        //$('span.#Msg').html(j);

                                        //alert(document.getElementById('oName'));
                                        if (document.getElementById('oName') == null) {
                                            $('#Msg').html(j);
                                            $('#errMsg').html('');
                                        } else {
                                            document.getElementById('oName').removeAttribute('id');
                                            $('#Msg').html('');
                                            $('#errMsg').html('');
                                        }
                                    });
                        }
                    }
                });
                
                
                
            });
            
           
            
            

        </script>
        <script type="text/javascript">
            $(function () {
                $('#frmPEOfficeCreation').submit(function () {
                    if (($('#Msg').html() == "OK") || ($('span.#Msg').html() == "Ok")) {
                        //return true;
                        if (bvalidation == true && bvalidationName == true && bvalidationPostCode == true) {
                            if ($('#frmPEOfficeCreation').valid()) {
                                if ($('#btnSubmit') != null) {
                                    $('#btnSubmit').attr("disabled", "true");
                                    $('#hdnbutton').val("Submit");
                                }
                            }
                        }
                    } else {
                        return false;
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $.validator.addMethod(
                        "regex",
                        function (value, element, regexp) {
                            var check = false;
                            return this.optional(element) || regexp.test(value);
                        },
                        "Please check your input."
                        );
                $("#frmPEOfficeCreation").validate({
                    rules: {
                        txtdepartment: {required: true},
                        deptName: {required: true},
                        deptBanglaString: {maxlength: 100},
                        peCode: {maxlength: 50},
                        address: {required: true, maxlength: 1000},
                        //countryId: { required: true },
                        stateId: {required: true},
                        //city: {requiredWithoutSpace: true, spacevalidate: true, alphaCity: true, maxlength: 100},
                        //upjilla: {requiredWithoutSpace: true, spacevalidate: true, alphaCity: true, maxlength: 100},
                        postCode: {number: true, minlength: 4},
                        phoneNo: {required: true, regex: /^[0-9]+-[0-9,]*[0-9]$/},
                        faxNo: {regex: /^[0-9]+-[0-9,]*[0-9]$/}
                        
                        //officeType: {required: true}
                    },
                    messages: {
                        deptName: {required: "<div name='ErrTree' id='ErrTree' class='reqF_1'>Please select <%=deptType%></div>"},
                        txtdepartment: {required: "<div class='reqF_1'>Please select Department</div>"},
                        deptBanglaString: {maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"
                        },
                        peCode: {maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed</div>"},
                        address: {required: "<div class='reqF_1'>Please enter Address</div>",
                            maxlength: "<div class='reqF_1'>Only 1000 characters are allowed</div>"},
                        //countryId: { required: "<div class='reqF_1'>Please select Country.</div>" },
                        stateId: {required: "<div class='reqF_1'>Please enter Dzongkhag / District Name</div>"},
                        //  officeType: { required: "<div class='reqF_1'>Please enter Office Type</div>" },
//                        city: {requiredWithoutSpace: "<div class='reqF_1'>Please enter City / Town Name</div>",
//                            alphaCity: "<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , ' \" } { - . _) Only</div>",
//                            maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>",
//                            spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>"},
//                        upjilla: {requiredWithoutSpace: "<div class='reqF_1'>Please enter Thana / UpaZilla</div>",
//                            alphaCity: "<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , ' \" } { - . _) Only</div>",
//                            spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
//                            maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"},
                        postCode: {number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                            maxlength: "<div class='reqF_1'>Maximum 4 digits are allowed</div>",
                            minlength: "<div class='reqF_1'>Minimum 4 digits required</div>"},
                        phoneNo: {required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            regex: "<div class='reqF_1'>Area code should contain minimum 2 digits and maximum 5 digits.</br>Phone no. should contain minimum 3 digits and maximum 10 digits.</br>In case of more than one Phone No. use comma.</br>Do not repeat Area Code.</div>"
                        },
                        faxNo: {regex: "<div class='reqF_1'>Area code should contain minimum 2 digits and maximum 5 digits.</br>Fax no. should contain minimum 3 digits and maximum 10 digits.</br>In case of more than one Fax No. use comma.</br>Do not repeat Area Code.</div>"
                        }

                    },
                    errorPlacement: function (error, element) {
                        if (element.attr("name") == "deptName")
                            error.insertAfter("#errTreeIcn");
                        else if (element.attr("name") == "txtdepartment")
                        {
                            error.insertAfter("#btnnewbutton");
                        } else if (element.attr("name") == "phoneNo")
                        {
                            error.insertAfter("#phno");
                        } else if (element.attr("name") == "faxNo")
                        {
                            error.insertAfter("#fxno");
                        }
                        //error.insertAfter(element);
                        //if (element.attr("name") == "txtdepartment")

                        else
                            error.insertAfter(element);
                    }
                });
            });
        </script>

        <script type="text/javascript">
            $(function () {
                $('#cmbCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCountry').val(), funName: 'stateCombo'}, function (j) {
                        $("select#cmbState").html(j);
                    });
                });
            });
            $(function () {
                $('#cmbState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue2'}, function (j) {
                        $('#txtCity').html(j);
                    });
                });
            });
            $(function () {
                $('#cmbState').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbState').val(), funName: 'subDistrictComboValue3'}, function (j) {
                        $('#txtupJilla').html(j);
                    });
                });
            });
            $(function () {
                $('#txtCity').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#txtCity').val(), funName: 'subDistrictComboValue4'}, function (j) {
                        $('#txtupJilla').html(j);
                    });
                });
            });

            $(function () {
                $('#txtOfficeName').focus(function () {
                    if ($('#deptName').val() == "") {
                        if ($('#valOfficeName').val() == undefined) {
                            $('#readonlyMsg').html('Please select <%=deptType%> first');
                        } else {
                            $('#valOfficeName').html('Please select <%=deptType%> first');
                        }
                    }
                });
            });
            function chkValidationUnique() {/*
             if(document.getElementById('deptName') != null){
             if(document.getElementById('deptName').value == ""){
             //alert(document.getElementById('deptName').value);
             //alert(document.getElementById('errMsg').value);
             document.getElementById('errMsg').innerHTML = '';
             document.getElementById('Msg').innerHTML = '';
             document.getElementById('readonlyMsg').innerHTML = 'Please select <-%=deptType%> first';
             
             //$('#errMsg').html('');
             }else{
             document.getElementById('txtOfficeName').removeAttribute('readonly','false');
             //document.getElementById('readonlyMsg').innerHTML = '';
             }
             }*/
            }
            function chkregPostCode(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            var bvalidationPostCode = true;
            var bvalidation;
            function clrMsg() {
                bvalidation = true;
                if ($('#txtOfficeName').val() == '') {
                    $('#readonlyMsg').html('Please enter Office Name');
                    bvalidation = false;
                }

            }

        </script>
    </head>
    <body>
        <%

            int aLvlOff = 0;
            if ("Ministry".equals(deptType)) {
                aLvlOff = 1;
            }else if ("Autonomus".equals(deptType)) {
                aLvlOff = 7;
            } else if ("Division".equals(deptType)) {
                aLvlOff = 2;
            } else if ("Organization".equals(deptType)) {
                aLvlOff = 3;
            } else if ("District".equals(deptType)) {
                aLvlOff = 6;
            } else if ("SubDistrict".equals(deptType)) {
                aLvlOff = 4;
            } else if ("Gewog".equals(deptType)) {
                aLvlOff = 5;
            }

            if ("Submit".equals(request.getParameter("hdnbutton"))) {
                String msg = "";
                CommonService cService = (CommonService) AppContext.getSpringBean("CommonService");
                //msg=commonService.verifyOfficeName(offficeName,deptId);                        
                msg = cService.verifyOfficeName(peOfficeCreationDtBean.getOfficeName().trim(), peOfficeCreationDtBean.getTxtchk());
                if (msg.trim().equalsIgnoreCase("OK")) {
                    String parseMe = request.getParameter("stateId");
                    peOfficeCreationDtBean.setStateId(parseMe);
                    peOfficeCreationDtBean.setOfficeName(peOfficeCreationDtBean.getOfficeName().trim());
                    //peOfficeCreationSrBean.peOfficeCreation(peOfficeCreationDtBean);
                    peOfficeCreationSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                    if (peOfficeCreationSrBean.peOfficeCreation(peOfficeCreationDtBean)) {
                        response.sendRedirect("PEOfficeView.jsp?officeid=" + peOfficeCreationSrBean.getOfficeid() + "&departmentType=" + deptType + "&msg=create");
                    } else {
                        out.print("<div class='responseMsg errorMsg'>Error in Inserting</div>");
                        response.sendRedirect("PEOfficeCreation.jsp?deptType=" + deptType + "&msg=creatFail");
                    }

                } else {
                    response.sendRedirect("PEOfficeCreation.jsp?deptType=" + deptType + "&msgVal=no");
                }
            } else {
        %>
        <script type="text/javascript">
            //        function chkTree(){
            //                if($('#txtdepartmentid').val()=='')
            //                {
            //                    $('#ErrTree').html('Please select <%=deptType%>.');
            //                    return false;
            //                }
            //                else
            //                {  $('#ErrTree').html('');
            //                    return true;
            //                }
            //            }
        </script>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("hdnUserType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                    userType.append(request.getParameter("hdnUserType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                            <td class="contentArea_1">
                                <div class="pageHead_1">Create Procuring Agency (PA) Office</div>
                            <%
                                String result = request.getParameter("msgVal");
                                if (request.getParameter("msg") != null && request.getParameter("msg").equals("creatFail")) {%>
                            <div class='responseMsg errorMsg'><%=appMessage.peOfficeCreation%></div>
                            <% }
                                if (request.getParameter("msgVal") != null && result.equals("no")) {%>
                            <div class="responseMsg errorMsg">Duplicate PE office found</div>
                            <% }%>
                            <!--Page Content Start-->
                            <%
                                int userTId = 0;
                                if (session.getAttribute("userTypeId") != null && !"".equalsIgnoreCase(session.getAttribute("userTypeId").toString())) {
                                    userTId = Integer.parseInt(session.getAttribute("userTypeId").toString());
                                }
                                boolean orgType = false;
                            %>
                            <form id="frmPEOfficeCreation" name="frmPEOfficeCreation" method="post" action="PEOfficeCreation.jsp?deptType=<%= deptType%>">
                                <table width="100%" class="formStyle_1" border="0" cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td style="font-style: italic; font-weight: normal;" colspan="2" class="ff"  align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                    </tr>
                                    <% if (userTId == 1) {
                                            String deptName = "";
                                            if (deptType.equalsIgnoreCase("Autonomus")) {
                                                deptName = "Autonomus Body";

                                            }else if (deptType.equalsIgnoreCase("District")) {
                                                deptName = "Dzongkhag";

                                            } else if (deptType.equalsIgnoreCase("SubDistrict")) {
                                                deptName = "Dungkhag";

                                            } else if (deptType.equalsIgnoreCase("Division")) {
                                                deptName = "Department";

                                            } else if (deptType.equalsIgnoreCase("Organization")) {
                                                deptName = "Division";

                                            } else {
                                                deptName = deptType;
                                            }%>
                                    <tr>
                                        <td class="ff" width="200">Select <%=deptName%> : <span>*</span></td>
                                        <td>
                                            <%if (deptType.equals("CCGP") || deptType.equals("Cabinet Division")) {%>
                                            <input type="text" readonly id="deptName" name="deptName" class="formTxtBox_1" style="width: 400px;" />
                                            <input type="hidden" id="txtdepartmentid" name="txtdepartmentid" />
                                            <a href="#" id="errTreeIcn" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?aLvlOff=<%= aLvlOff%>&operation=offcre', '', 'width=350px,height=400px,scrollbars=1', '');">
                                                <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                            </a>
                                            <span id="ErrTree" class="reqF_1"></span>
                                            <%--<input class="formTxtBox_1" name="txtdepartment" type="text" id="txtdepartment" style="width: 200px;" readonly="true"/>--%>
                                            <%--<input class="formTxtBox_1" name="txtdepartmentid" type="text" id="txtdepartmentid" style="width: 200px;"/>--%>
                                            <%--<label class="formBtn_1"><input id="btnnewbutton" name="btnnewbutton" type="button" value="New Window!" onClick="window.open('DepartmentSelection.jsp','window','width=400,height=400')"></label>--%>
                                            <input type="hidden" id="txtchk" name="txtchk" />

                                            <%} else {%>
                                            <select id="cmbDept" name="txtdepartmentid_sel" id="txtdeptid" class="formTxtBox_1" onchange="setDeptID(this);" style="width: 405px;display: none">
                                                <option value="">-Select-</option>
                                                <% for (TblDepartmentMaster deptTypeList : peOfficeCreationSrBean.getDepartmentType(deptType)) {
                                                %><option value="<%=deptTypeList.getDepartmentId()%>"><%=deptTypeList.getDepartmentName()%></option>
                                                <% }%>
                                            </select>
                                            <input type="text" readonly id="deptName" name="deptName" class="formTxtBox_1" style="width: 200px;" />
                                            <input type="hidden" id="txtdepartmentid" name="txtdepartmentid" />
                                            <a href="#" id="errTreeIcn" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?aLvlOff=<%= aLvlOff%>&operation=offcre', '', 'width=350px,height=400px,scrollbars=1', '');">
                                                <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                            </a>
                                            <span id="ErrTree" class="reqF_1"></span>
                                            <%--<input class="formTxtBox_1" name="txtdepartment" type="text" id="txtdepartment" style="width: 200px;" readonly="true"/>--%>
                                            <%--<input class="formTxtBox_1" name="txtdepartmentid" type="text" id="txtdepartmentid" style="width: 200px;"/>--%>
                                            <%--<label class="formBtn_1"><input id="btnnewbutton" name="btnnewbutton" type="button" value="New Window!" onClick="window.open('DepartmentSelection.jsp','window','width=400,height=400')"></label>--%>
                                            <input type="hidden" id="txtchk" name="txtchk" />
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Office Name : <span>*</span></td>
                                        <td>
                                            <%if (deptType.equals("Organization")) {%>
                                            <input class="formTxtBox_1" id="txtOfficeName" maxlength="101" name="officeName" style="width: 400px;" type="text"/>
                                            <%} else {%>
                                            <input class="formTxtBox_1" id="txtOfficeName" maxlength="101" name="officeName" style="width: 400px;" type="text"/>
                                            <%}%>
                                            <div id="readonlyMsg" class="reqF_1"></div>
                                            <div id="errMsg" style="color: red;"></div>
                                            <div id="Msg" style="color: red; font-weight: bold"></div>
                                        </td>
                                    </tr>
                                    <% } else if (userTId == 5) {
                                        for (TblDepartmentMaster tblDepartmentMaster : peOfficeCreationSrBean.getDepartmentName(userId)) {
                                            if (tblDepartmentMaster.getOrganizationType().equals("no")) {
                                                orgType = true;
                                            }
                                    %>
                                    <tr>
                                        <td class="ff" width="200">Organization :</td>
                                        <td><label id="lblDepartment"><%=tblDepartmentMaster.getDepartmentName()%></label>
                                            <input class="formTxtBox_1" name="txtdepartmentid" type="hidden" value="<%=tblDepartmentMaster.getDepartmentId()%>" id="txtdepartmentid" style="width: 200px;"/>
                                            <input type="hidden" name="userTID" value="<%=userTId%>" id="userTID"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Office Name : <span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtOfficeName" id="txtOfficeName" name="officeName" style="width: 400px;" type="text" maxlength="101"/>

                                            <div id="readonlyMsg" class="reqF_1"></div>
                                            <div id="errMsg" style="color: red;"></div>
                                            <div id="Msg" style="color: red; font-weight: bold"></div>
                                        </td>
                                    </tr>
                                    <% }
                                        }%>

<!--                                    <tr>
                                        <td class="ff" width="200">Office Name in Dzongkha :</td>
                                        <td><input class="formTxtBox_1" id="txtOfficeNameBangla" name="deptBanglaString" maxlength="101" style="width: 400px;" type="text" />
                                        </td>
                                    </tr>-->
                                    <%if (deptType.equals("Organization") && orgType == true) { %>
                                    <tr>
                                        <td class="ff" width="200">Office Type :<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbOfcType" name="officeType" style="width: 205px" >
                                                <option value="Division">Division Level</option>
                                                <option value="District">Dzongkhag / District Level</option>
                                                <option value="Upazila">Gewog Level</option>
                                                <option value="Circle">Circle Level</option>
                                                <option value="Others" selected>Others</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <%} else {%>
                                    <tr>
                                        <td class="ff" width="200" style="display: none;" id="trOfcType" >Office Type :<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbOfcType" name="officeType" style="width: 205px;display: none" >

                                                <option value="Division">Division Level</option>
                                                <option value="District">Dzongkhag / District Level</option>
                                                <option value="Upazila">Upazila Level</option>
                                                <option value="Circle">Circle Level</option>
                                                <option value="Others" selected>Others</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <%}%>
                                    <tr>
                                        <td class="ff" width="200">PA Code :</td>
                                        <td><input class="formTxtBox_1" id="txtPECode" name="peCode" style="width: 200px;" type="text" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Address : <span>*</span></td>
                                        <td><textarea rows="5" class="formTxtBox_1"  id="taAddress" name="address" style="width: 400px" onkeypress="return imposeMaxLength(this, 1001, event);" ></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Country : </td>
                                        <td>Bhutan
                                            <select name="countryId" class="formTxtBox_1" id="cmbCountry" style="width: 205px;display:none">
                                                <%
                                                    for (SelectItem country : peOfficeCreationSrBean.getCountryList()) {
                                                        if (country.getObjectValue().equals("Bhutan")) {
                                                %>
                                                <option  value="<%=country.getObjectId()%>" selected="true"><%=country.getObjectValue()%></option>
                                                <%                                                                                                    } else {
                                                %>
<!--                                                <option  value="<%=country.getObjectId()%>"><%=country.getObjectValue()%></option>-->
                                                <%                                                                }
                                                    }
                                                %>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Dzongkhag : <span>*</span></td>
                                        <td>
                                            <select name="stateId" class="formTxtBox_1" id="cmbState">
                                                <option  value="">-- Select Dzongkhag --</option>
                                                <%for (SelectItem state : peOfficeCreationSrBean.getStateList()) {%>
                                                <option value="<%=state.getObjectValue()%>,<%=state.getObjectId()%>"><%=state.getObjectValue()%></option>

                                                <%}%>
                                            </select>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Dungkhag  : </td>
                                        <td>
                                            <select class="formTxtBox_1" id="txtCity" name="city">
                                                <option  value="">-- Select Dungkhag --</option>
                                            </select>
                                        </td>

                                    </tr>

                                    <tr>
                                        <td class="ff">Gewog : </td>
                                        <td>
                                            <select class="formTxtBox_1" id="txtupJilla" name="upjilla">
                                                <option  value="">-- Select Gewog --</option> 
                                            </select>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Postcode : </td>
                                        <td><input class="formTxtBox_1" id="txtPostCode" name="postCode" style="width: 200px;" type="text" maxlength="19"/>
                                            <div id="SearchValErrorPostCode" class="reqF_1"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Phone No. : <span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtPhoneNumber" style="width: 400px;" name="phoneNo" type="text" maxlength="245"/>
                                            <span id="phno" style="color: grey;"> (Area Code - Phone No. e.g. 02-336962,336942)</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Fax No. :</td>
                                        <td><input class="formTxtBox_1" id="txtFaxNumber" style="width: 400px;"  name="faxNo" type="text" maxlength="245"/>
                                            <span id="fxno" style="color: grey;"> (Area Code - Fax No. e.g. 02-336961,336941)</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <!--                                        <td align="left"><label class="formBtn_1"><input id="btnSubmit" name="Submit" value="Submit" type="submit" onclick="return chkTree();" /></label></td>-->
                                        <td align="left"><label class="formBtn_1"><input id="btnSubmit" name="Submit" value="Submit" type="submit" onclick="clrMsg();" /></label>
                                            <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                </div>
            </div>
        <% }%>
        <script>
            var department = '<%=deptType%>';
            if (department == 'Autonomus') {
                var obj = document.getElementById('lblAutonomousBodyPEOfficeCreation');
                if (obj != null) {
                    obj.setAttribute('class', 'selected');
                }
            } else if (department == 'Ministry') {
                var obj = document.getElementById('lblMinistryPEOfficeCreation');
                if (obj != null) {
                    //if(obj.innerHTML == 'Create Offices'){
                    obj.setAttribute('class', 'selected');
                    // }
                }
            } else if (department == 'Division') {
                var obj1 = document.getElementById('lblDivisionPEOfficeCreation');
                if (obj1 != null) {
                    // if(obj1.innerHTML == 'Create Offices'){
                    obj1.setAttribute('class', 'selected');
                    //}
                }
            } else if (department == 'Organization') {
                var obj2 = document.getElementById('lblOrganizationPEOfficeCreation');
                if (obj2 != null) {
                    //if(obj2.innerHTML == 'Create Offices'){
                    obj2.setAttribute('class', 'selected');
                    //}
                }
            } else if (department == 'District') {
                var obj2 = document.getElementById('lblDistrictPEOfficeCreation');
                if (obj2 != null) {
                    //if(obj2.innerHTML == 'Create Offices'){
                    obj2.setAttribute('class', 'selected');
                    //}
                }
            } else if (department == 'SubDistrict') {
                var obj2 = document.getElementById('lblSubDistrictPEOfficeCreation');
                if (obj2 != null) {
                    //if(obj2.innerHTML == 'Create Offices'){
                    obj2.setAttribute('class', 'selected');
                    //}
                }
            } else if (department == 'Gewog') {
                var obj2 = document.getElementById('lblGewogPEOfficeCreation');
                if (obj2 != null) {
                    //if(obj2.innerHTML == 'Create Offices'){
                    obj2.setAttribute('class', 'selected');
                    //}
                }
            }
            var headSel_Obj = document.getElementById("headTabMngUser");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }

        </script>
    </body>
    <%
        peOfficeCreationSrBean = null;
        peOfficeCreationDtBean = null;
    %>
</html>
