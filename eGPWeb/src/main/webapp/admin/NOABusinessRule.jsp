<%-- 
    Document   : NOABusinessRule
    Created on : Dec 29, 2010, 12:14:06 PM
    Author     : Naresh.Akurathi
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigNoa"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<%@page import="java.util.Iterator"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Letter of Acceptance (LOA) Business Rules Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>


        
        <%!    ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();
        %>

        <%
                    StringBuilder procNature = new StringBuilder();
                    StringBuilder procType = new StringBuilder();
                    StringBuilder procMethod = new StringBuilder();


                    TblProcurementNature tblProcureNature2 = new TblProcurementNature();
                    Iterator pnit2 = configPreTenderRuleSrBean.getProcurementNature().iterator();
                    while (pnit2.hasNext()) {
                        tblProcureNature2 = (TblProcurementNature) pnit2.next();
                        procNature.append("<option value='" + tblProcureNature2.getProcurementNatureId() + "'>" + tblProcureNature2.getProcurementNature() + "</option>");
                    }

                    TblProcurementMethod tblProcurementMethod2 = new TblProcurementMethod();
                    Iterator pmit2 = configPreTenderRuleSrBean.getProcurementMethod().iterator();
                    while (pmit2.hasNext()) {
                        tblProcurementMethod2 = (TblProcurementMethod) pmit2.next();
                        procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>" + tblProcurementMethod2.getProcurementMethod() + "</option>");
                    }

                    TblProcurementTypes tblProcurementTypes2 = new TblProcurementTypes();
                    Iterator ptit2 = configPreTenderRuleSrBean.getProcurementTypes().iterator();
                    while (ptit2.hasNext()) {
                        //ICT->ICB & NCT->NCB by Emtaz on 19/April/2016
                        tblProcurementTypes2 = (TblProcurementTypes) ptit2.next();
                        if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("ICT"))
                            procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + "ICB" + "</option>");
                        else if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("NCT"))
                            procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + "NCB" + "</option>");
                         //procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + tblProcurementTypes2.getProcurementType() + "</option>");
                    }


        %>


        <script type="text/javascript">


            var totCnt = 1;

            $(function() {
                $('#linkAddRule').click(function() {
                    var count=($("#tblrule tr:last-child").attr("id").split("_")[1]*1);

                    var htmlEle = "<tr id='trrule_"+ (count+1) +"'>"+
                        "<td class='t-align-center'><input type='checkbox' name='checkbox"+(count+1)+"' id='checkbox_"+(count+1)+"' value='"+(count+1)+"' style='width:90%;'/></td>"+
                        "<td class='t-align-center'><select name='procNature"+(count+1)+"' class='formTxtBox_1' id='procNature_"+(count+1)+"'><%=procNature%></select></td>"+
                        "<td class='t-align-center'><select name='procMethod"+(count+1)+"' class='formTxtBox_1' id='procMethod_"+(count+1) +"'><%=procMethod%></select></td>"+
                        "<td class='t-align-center'><select name='procType"+(count+1)+"' class='formTxtBox_1' id='procType_"+(count+1)+"'><%=procType%></select></td>"+
                        "<td class='t-align-center'><input name='minValue"+(count+1)+"' type='text' class='formTxtBox_1' id='minValue_"+(count+1)+"' style='width:90%;' onblur='return chkMinVal(this);' /><span id='MinValue_"+(count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><input name='maxValue"+(count+1)+"' type='text' class='formTxtBox_1' id='maxValue_"+(count+1)+"' style='width:90%;' onblur='return chkMaxVal(this);'/><span id='MaxValue_"+(count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><input name='noaAcceptDays"+(count+1)+"' type='text' class='formTxtBox_1' id='noaAcceptDays_"+(count+1)+"' style='width:90%;' onblur='return chkNOAacceptance(this);' /><span id='NOAacceptDays_"+(count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><input name='perSecSubDays"+(count+1)+"' type='text' class='formTxtBox_1' id='perSecSubDays_"+(count+1)+"' style='width:90%;' onblur='return chkPerSecSubDays(this);' /><span id='PerSecSubDays_"+(count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><input name='conSignDays"+(count+1)+"' type='text' class='formTxtBox_1' id='conSignDays_"+(count+1)+"' style='width:90%;' onblur='return chkConSignDays(this);' /><span id='ConSignDays_"+(count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "</tr>";

                    totCnt++;
                    //alert(htmlEle);
                    $("#tblrule").append(htmlEle);
                    document.getElementById("TotRule").value = (count+1);
                    //alert(count+1);

                });
            });


            $(function() {
                $('#linkDelRule').click(function() {
                    var cnt = 0;
                    var tmpCnt = 0;
                    var counter = ($("#tblrule tr:last-child").attr("id").split("_")[1]*1);
                    for(var i=1;i<=counter;i++){
                        if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                            tmpCnt++;
                        }
                    }
                    if(tmpCnt==totCnt){
                        $('span.#lotMsg').css("visibility","visible");
                        $('span.#lotMsg').css("color","red");
                        $('span.#lotMsg').html('Minimum 1 record is needed!');
                    }else{
                        for(var i=1;i<=counter;i++){
                            if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                                $("tr[id='trrule_"+i+"']").remove();
                                $('span.#lotMsg').css("visibility","collapse");
                                cnt++;
                            }
                        }
                        totCnt -= cnt;
                    }

                });
            });
        </script>
    </head>
    <body>
        <%

                    String msg = "";
                    if ("Submit".equals(request.getParameter("button"))) {
                        int row = Integer.parseInt(request.getParameter("TotRule"));

                        msg = configPreTenderRuleSrBean.delAllConfigNoa();
                        if (msg.equals("Deleted")) {
                            for (int i = 1; i <= row; i++) {
                                if (request.getParameter("procNature" + i) != null) {
                                    byte pnature = Byte.parseByte(request.getParameter("procNature" + i));
                                    byte pmethod = Byte.parseByte(request.getParameter("procMethod" + i));
                                    byte ptypes = Byte.parseByte(request.getParameter("procType" + i));
                                    float minValue = Float.parseFloat(request.getParameter("minValue" + i));
                                    float maxValue = Float.parseFloat(request.getParameter("maxValue" + i));
                                    short noaAcceptDays = Short.parseShort(request.getParameter("noaAcceptDays" + i));
                                    short perSecSubDays = Short.parseShort(request.getParameter("perSecSubDays" + i));
                                    short conSignDays = Short.parseShort(request.getParameter("conSignDays" + i));

                                    TblConfigNoa tblConfigNoa = new TblConfigNoa();
                                    tblConfigNoa.setTblProcurementNature(new TblProcurementNature(pnature));
                                    tblConfigNoa.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                    tblConfigNoa.setTblProcurementTypes(new TblProcurementTypes(ptypes));
                                    tblConfigNoa.setMinValue(new BigDecimal(request.getParameter("minValue" + i)));
                                    tblConfigNoa.setMaxValue(new BigDecimal(request.getParameter("maxValue" + i)));
                                    tblConfigNoa.setNoaAcceptDays(noaAcceptDays);
                                    tblConfigNoa.setPerSecSubDays(perSecSubDays);
                                    tblConfigNoa.setConSignDays(conSignDays);

                                    msg = configPreTenderRuleSrBean.addConfigNoa(tblConfigNoa);
                                }
                            }
                        }
                        if (msg.equals("Values Added")) {
                            msg = "Letter of Acceptance (LOA) business rule configured successfully";
                            response.sendRedirect("NOABusinessRuleView.jsp?msg=" + msg);
                        }

                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
                        <div class="pageHead_1">Letter of Acceptance (LOA) Business Rules Configuration</div>

                        <div style="font-style: italic" class="t-align-left t_space"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>

                        <div align="right" class="t_space">
                            <span id="lotMsg" style="color: red; float: left; font-weight: bold; visibility: collapse;">&nbsp;</span>
                            <a id="linkAddRule" class="action-button-add">Add Rule</a> <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a><br/>
                        </div>

                        <%--<div class="pageHead_1" align="center"> <%=msg%></div>--%>

                        <form action="NOABusinessRule.jsp" method="post">
                            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tblrule" name="tblrule">
                                <tr>
                                    <th >Select</th>
                                    <th >Procurement  Category<br />(<span class="mandatory">*</span>)</th>
                                    <th >Procurement  Method<br />(<span class="mandatory">*</span>)</th>
                                    <th >Procurement  Type<br />(<span class="mandatory">*</span>)</th>
                                    <th >Minimum Value <br />(In Nu.)<br />(<span class="mandatory">*</span>)</th>
                                    <th >Maximum Value<br />(In Nu.)<br />(<span class="mandatory">*</span>) </th>
                                    <th >No. of Days for <br /> Letter of Acceptance (LOA) acceptance <br />(<span class="mandatory">*</span>) </th>
                                    <th >No. of Days for <br /> Performance Security <br /> submission <br />(<span class="mandatory">*</span>) </th>
                                    <th >No. of Days for <br />Contract Signing <br /> from date of <br />issuance of Letter of Acceptance (LOA) <br />(<span class="mandatory">*</span>) </th>
                                    <th >No. of Days between LoI and LOA</th>
                                </tr>
                                <input type="hidden" name="delrow" value="0" id="delrow"/>
                                <input type="hidden" name="TotRule" id="TotRule" value="1"/>
                                <input type="hidden" name="introw" value="" id="introw"/>
                                <tr id="trrule_1">
                                    <td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_1" value="" /></td>
                                    <td class="t-align-center"><select name="procNature1" class="formTxtBox_1" id="procNature_1">
                                            <%=procNature%></select>
                                    </td>
                                    <td class="t-align-center"><select name="procMethod1" class="formTxtBox_1" id="procMethod_1">
                                            <%=procMethod%></select>
                                    </td>
                                    <td class="t-align-center"><select name="procType1" class="formTxtBox_1" id="procType_1">
                                            <%=procType%></select>
                                    </td>
                                    <td class="t-align-center"><input name="minValue1" type="text" class="formTxtBox_1" id="minValue_1" style="width:90%;" onblur="return chkMinVal(this);" /><span id="MinValue_1" style="color:red;"></span></td>
                                    <td class="t-align-center"><input name="maxValue1" type="text" class="formTxtBox_1" id="maxValue_1" style="width:90%;" onblur="return chkMaxVal(this);" /><span id="MaxValue_1" style="color:red;"></span></td>
                                    <td class="t-align-center"><input name="noaAcceptDays1" type="text" class="formTxtBox_1" id="noaAcceptDays_1" style="width:90%;" onblur="return chkNOAacceptance(this);" /><span id="NOAacceptDays_1" style="color:red;"></span></td>
                                    <td class="t-align-center"><input name="perSecSubDays1" type="text" class="formTxtBox_1" id="perSecSubDays_1" style="width:90%;" onblur="return chkPerSecSubDays(this);" /><span id="PerSecSubDays_1" style="color:red;"></span></td>
                                    <td class="t-align-center"><input name="conSignDays1" type="text" class="formTxtBox_1" id="conSignDays_1" style="width:90%;" onblur="return chkConSignDays(this);" /><span id="ConSignDays_1" style="color:red;"></span></td>
                                    <td class="t-align-center"><input name="loiNoa1" type="text" class="formTxtBox_1" id="loiNoa_1" style="width:90%;" onblur="return chkLoiNoa(this);" /><span id="LoiNoa_1" style="color:red;"></span></td>
                                </tr>
                            </table>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" >
                                <tr>
                                    <td align="right" colspan="9" class="t-align-center">
                                        <span class="formBtn_1"><input type="submit" name="button" id="button" value="Submit" onclick="return validate();"/></span>
                                    </td>
                                </tr>
                            </table>
                        </form>

                    </td>
                </tr>
            </table>


            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>
            <script>
                var obj = document.getElementById('lblNOARuleAdd');
                if(obj != null){
                    if(obj.innerHTML == 'Add'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
        <script>
            var headSel_Obj = document.getElementById("headTabConfig");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
        <script type="text/javascript">

            function validate(){

                var count=document.getElementById("TotRule").value;
                var flag=true;

                for(var k=1;k<=count;k++){

                    if(document.getElementById("minValue_"+k)!=null)
                    {
                        if(document.getElementById("minValue_"+k).value=="")
                        {
                            document.getElementById("MinValue_"+k).innerHTML="<br/>Please enter Minimum Value (In Nu.).";
                            flag=false;
                        }
                        else
                        {
                            if(decimal(document.getElementById("minValue_"+k).value))
                            {
                                if(!chkMax(document.getElementById("minValue_"+k).value))
                                {
                                    document.getElementById("MinValue_"+k).innerHTML="Maximum 12 digits are allowed.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("MinValue_"+k).innerHTML="";
                                }

                            }
                            else
                            {
                                document.getElementById("MinValue_").innerHTML="</br>Please Enter Numbers and 2 digits after Decimal.";
                                flag=false;
                            }

                        }

                    }
                    if(document.getElementById("maxValue_"+k)!=null)
                    {
                        if(document.getElementById("maxValue_"+k).value=="")
                        {
                            document.getElementById("MaxValue_"+k).innerHTML="<br/>Please enter Maximum Value (In Nu.).";
                            flag=false;
                        }
                        else
                        {
                            if(decimal(document.getElementById("maxValue_"+k).value))
                            {
                                if(!chkMax(document.getElementById("maxValue_"+k).value))
                                {
                                    document.getElementById("MaxValue_"+k).innerHTML="Maximum 12 digits are allowed.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("MaxValue_"+k).innerHTML="";
                                }

                            }
                            else
                            {
                                document.getElementById("MaxValue_"+k).innerHTML="</br>Please Enter Numbers and 2 digits after Decimal.";
                                flag=false;
                            }

                        }

                    }

                    if(document.getElementById("noaAcceptDays_"+k)!=null)
                    {
                        if(document.getElementById("noaAcceptDays_"+k).value=="")
                        {
                            document.getElementById("NOAacceptDays_"+k).innerHTML="<br/>Please enter No. of days for Letter of Acceptance (LOA) acceptance.";
                            flag=false;
                        }

                        else
                        {
                            if(numeric(document.getElementById("noaAcceptDays_"+k).value))
                            {
                                document.getElementById("NOAacceptDays_"+k).innerHTML="";
                                //                                if(!chkMaxLength(document.getElementById("noaAcceptDays"+k).value))
                                //                                {
                                //                                    document.getElementById("NOAacceptDays_"+k).innerHTML="<br/>Maximum 3 numbers are allowed.";
                                //                                    flag= false;
                                //                                }
                                //                                else
                                //                                {
                                //                                    document.getElementById("NOAacceptDays_"+k).innerHTML="";
                                //                                }
                            }
                            else
                            {
                                document.getElementById("NOAacceptDays_"+k).innerHTML="<br/>Please Enter Numbers only.";
                                flag= false;
                            }
                        }

                    }
                    if(document.getElementById("perSecSubDays_"+k)!=null)
                    {
                        if(document.getElementById("perSecSubDays_"+k).value=="")
                        {
                            document.getElementById("PerSecSubDays_"+k).innerHTML="<br/>Please enter No. of days for Performance Security submission.";
                            flag=false;
                        }

                        else
                        {
                            if(numeric(document.getElementById("perSecSubDays_"+k).value))
                            {
                                document.getElementById("PerSecSubDays_"+k).innerHTML="";
                                //                                if(!chkMaxLength(document.getElementById("noaAcceptDays"+k).value))
                                //                                {
                                //                                    document.getElementById("NOAacceptDays_"+k).innerHTML="<br/>Maximum 3 numbers are allowed.";
                                //                                    flag= false;
                                //                                }
                                //                                else
                                //                                {
                                //                                    document.getElementById("NOAacceptDays_"+k).innerHTML="";
                                //                                }
                            }
                            else
                            {
                                document.getElementById("PerSecSubDays_"+k).innerHTML="<br/>Please Enter Numbers only.";
                                flag= false;
                            }
                        }

                    }
                    if(document.getElementById("conSignDays_"+k)!=null)
                    {
                        if(document.getElementById("conSignDays_"+k).value=="")
                        {
                            document.getElementById("ConSignDays_"+k).innerHTML="<br/>Please enter No. of days for Contract Signing.";
                            flag=false;
                        }

                        else
                        {
                            if(numeric(document.getElementById("conSignDays_"+k).value))
                            {
                                document.getElementById("ConSignDays_"+k).innerHTML="";
                                //                                if(!chkMaxLength(document.getElementById("noaAcceptDays"+k).value))
                                //                                {
                                //                                    document.getElementById("NOAacceptDays_"+k).innerHTML="<br/>Maximum 3 numbers are allowed.";
                                //                                    flag= false;
                                //                                }
                                //                                else
                                //                                {
                                //                                    document.getElementById("NOAacceptDays_"+k).innerHTML="";
                                //                                }
                            }
                            else
                            {
                                document.getElementById("ConSignDays_"+k).innerHTML="<br/>Please Enter Numbers only.";
                                flag= false;
                            }
                        }

                    }
                    if (document.getElementById("loiNoa_" + k) != null)
                    {
                        if (document.getElementById("loiNoa_" + k).value == "")
                        {
                            document.getElementById("LoiNoa_" + k).innerHTML = "<br/>Please enter No. of Days between LoI and Letter of Acceptance (LOA)";
                            flag = false;
                        } 
                        else
                        {
                            if (numeric(document.getElementById("loiNoa_" + k).value))
                            {
                                document.getElementById("LoiNoa_" + k).innerHTML = "";                            
                            } 
                            else
                            {
                                document.getElementById("LoiNoa_" + k).innerHTML = "<br/>Please Enter Numbers only";
                                flag = false;
                            }
                        }
                    }
                }
                

                
                

                // Start OF--Checking for Unique Rows
                if(document.getElementById("TotRule")!=null){
                    var totalcount = eval(document.getElementById("TotRule").value);} //Total Count After Adding Rows
                var chk=true;
                var i=0;
                var j=0;
                for(i=1; i<=totalcount; i++) //Loop For Newly Added Rows
                {
                    if(document.getElementById("minValue_"+i)!=null)
                        if($.trim(document.getElementById("minValue_"+i).value)!='' && $.trim(document.getElementById("maxValue_"+i).value) !='' && $.trim(document.getElementById("noaAcceptDays_"+i).value)!='' && $.trim(document.getElementById("perSecSubDays_"+i).value)!='' && $.trim(document.getElementById("conSignDays_"+i).value)!='') // If Condition for When all Data are filled thiscode is Running
                    {

                        for(j=1; j<=totalcount && j!=i; j++) // Loop for Total Count but Not same as (i) value
                        {
                            chk=true;
                            //If Condition for Check Duplicate Rows are there or not.If Columns are diff then chk variable set to false
                            //IF Row is same Give alert message.
                            if($.trim(document.getElementById("procNature_"+i).value) != $.trim(document.getElementById("procNature_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("procMethod_"+i).value) != $.trim(document.getElementById("procMethod_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("procType_"+i).value) != $.trim(document.getElementById("procType_"+j).value))
                            {
                                chk=false;
                            }
                            var minvi=document.getElementById("minValue_"+i).value;
                            var minvj=document.getElementById("minValue_"+j).value;
                            if(minvi.indexOf(".")==-1)
                            {
                                minvi=minvi+".00";
                            }
                            if(minvj.indexOf(".")==-1)
                            {
                                minvj=minvj+".00";
                            }
                            if($.trim(minvi) != $.trim(minvj))
                            {
                                chk=false;
                            }
                            var maxvi=document.getElementById("maxValue_"+i).value;
                            var maxvj=document.getElementById("maxValue_"+j).value;
                            if(maxvi.indexOf(".")==-1)
                            {
                                maxvi=maxvi+".00";
                            }
                            if(maxvj.indexOf(".")==-1)
                            {
                                maxvj=maxvj+".00";
                            }
                            if($.trim(maxvi) != $.trim(maxvj))
                            {
                                chk=false;
                            }
//                            if($.trim(document.getElementById("noaAcceptDays_"+i).value) != $.trim(document.getElementById("noaAcceptDays_"+j).value))
//                            {
//                                chk=false;
//                            }
//                            if($.trim(document.getElementById("perSecSubDays_"+i).value) != $.trim(document.getElementById("perSecSubDays_"+j).value))
//                            {
//                                chk=false;
//                            }
//                            if($.trim(document.getElementById("conSignDays_"+i).value) != $.trim(document.getElementById("conSignDays_"+j).value))
//                            {
//                                chk=false;
//                            }
                            //alert(j);
                            //alert("chk" +chk);
                            if(flag){
                            if(chk==true) //If Row is same then give alert message
                            {

                                    jAlert("Duplicate record found. Please enter unique record","Letter of Acceptance(LOA) Business Rule Configuration",function(RetVal) {
                                    });
                                return false;
                            }
                        }
                    }
                }
                }
                // End OF--Checking for Unique Rows


                if(flag==false){
                    return false;
                }
            }

            function chkMinVal(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk=true;

                if(obj.value!='')
                {
                    if(decimal(obj.value))
                    {
                        if(!chkMax(obj.value))
                        {
                            document.getElementById("MinValue_"+i).innerHTML="Maximum 12 digits are allowed.";
                            chk=false;
                        }
                        else
                        {
                            document.getElementById("MinValue_"+i).innerHTML="";
                        }
                        //document.getElementById("minvalue_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("MinValue_"+i).innerHTML="</br>Please Enter Numbers and 2 digits after Decimal.";
                        chk=false;
                    }
                }
                else
                {
                    document.getElementById("MinValue_"+i).innerHTML="</br>Please enter Minimum Value (In Nu.).";
                    chk=false;
                }
                if(chk==false)
                {
                    return false;
                }

            };

            function chkMaxVal(obj){
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk1=true;

                if(obj.value!='')
                {
                    if(decimal(obj.value))
                    {
                        if(!chkMax(obj.value))
                        {
                            document.getElementById("MaxValue_"+i).innerHTML="Maximum 12 digits are allowed.";
                            chk=false;
                        }
                        else
                        {
                            document.getElementById("MaxValue_"+i).innerHTML="";
                        }
                        // document.getElementById("maxvalue_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("MaxValue_"+i).innerHTML="</br>Please Enter Numbers and 2 digits after Decimal.";
                        chk1=false;
                    }
                }
                else
                {
                    document.getElementById("MaxValue_"+i).innerHTML="</br>Please enter Maximum Value (In Nu.).";
                    chk1=false;
                }
                if(chk1==false)
                {
                    return false;
                }

            };
            function chkNOAacceptance(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk2=true;

                if(obj.value!='')
                {
                    if(numeric(obj.value))
                    {
                        if(!chkMaxLength(obj.value))
                        {
                            document.getElementById("NOAacceptDays_"+i).innerHTML="Maximum 3 numbers are allowed.";
                            chk2=false;
                        }
                        else
                        {
                            document.getElementById("NOAacceptDays_"+i).innerHTML="";
                        }
                        //document.getElementById("MinNoDays_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("NOAacceptDays_"+i).innerHTML="</br>Please Enter numbers only.";
                        chk2=false;
                    }
                }
                else
                {
                    document.getElementById("NOAacceptDays_"+i).innerHTML="</br>Please enter  No. of days for Letter of Acceptance (LOA) acceptance.";
                    chk2=false;
                }
                if(chk2==false)
                {
                    return false;
                }
            };
            function chkPerSecSubDays(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk3=true;

                if(obj.value!='')
                {
                    if(numeric(obj.value))
                    {
                        if(!chkMaxLength(obj.value))
                        {
                            document.getElementById("PerSecSubDays_"+i).innerHTML="Maximum 3 numbers are allowed.";
                            chk3=false;
                        }
                        else
                        {
                            document.getElementById("PerSecSubDays_"+i).innerHTML="";
                        }
                        //document.getElementById("MinNoDays_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("PerSecSubDays_"+i).innerHTML="</br>Please Enter numbers only.";
                        chk3=false;
                    }
                }
                else
                {
                    document.getElementById("PerSecSubDays_"+i).innerHTML="</br>Please enter No. of days for Performance Security submission.";
                    chk3=false;
                }
                if(chk3==false)
                {
                    return false;
                }
            };
            function chkConSignDays(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk4=true;

                if(obj.value!='')
                {
                    if(numeric(obj.value))
                    {
                        if(!chkMaxLength(obj.value))
                        {
                            document.getElementById("ConSignDays_"+i).innerHTML="Maximum 3 numbers are allowed.";
                            chk4=false;
                        }
                        else
                        {
                            document.getElementById("ConSignDays_"+i).innerHTML="";
                        }
                        //document.getElementById("MinNoDays_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("ConSignDays_"+i).innerHTML="</br>Please Enter numbers only.";
                        chk4=false;
                    }
                }
                else
                {
                    document.getElementById("ConSignDays_"+i).innerHTML="</br>Please enter No. of days for Contract Signing.";
                    chk4=false;
                }
                if(chk4==false)
                {
                    return false;
                }
            };

            function chkLoiNoa(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                var chk4 = true;

                if (obj.value != '')
                {
                    if (numeric(obj.value))
                    {
                        if (!chkMaxLength(obj.value))
                        {
                            document.getElementById("LoiNoa_" + i).innerHTML = "Maximum 3 numbers are allowed";
                            chk4 = false;
                        }
                        /*else if(document.getElementById("LoiNoa_" + k).value == '0' || document.getElementById("LoiNoa_" + k).value == '00')
                        {
                            document.getElementById("LoiNoa_" + k).innerHTML = "<br/>Zero will not be allowed";
                            chk4 = false; 
                        }*/
                        else
                        {
                            document.getElementById("LoiNoa_" + i).innerHTML = "";
                        }
                    }
                    else
                    {
                        document.getElementById("LoiNoa_" + i).innerHTML = "<br/>Please Enter numbers only";
                        chk4 = false;
                    }
                } 
                else
                {
                    document.getElementById("LoiNoa_" + i).innerHTML = "<br/>Please enter No. of Days between LoI and Letter of Acceptance(LOA) for Performance Security submission";
                    chk4 = false;
                }
                if (chk4 == false)
                {
                    return false;
                }
            };
            


            function chkMax(value)
            {
                var chkVal=value;
                var ValSplit=chkVal.split('.');
                //alert(chkVal);
                //alert(ValSplit[0].length);
                if(ValSplit[0].length>12)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            function chkMaxLength(value)
            {
                var ValueChk=value;
                //alert(ValueChk);

                if(ValueChk.length>3)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            function decimal(value){

                return /^(\d+(\.\d{2})?)$/.test(value);
            };

            function numeric(value) {
                return /^\d+$/.test(value);
            }
        </script>
    </body>
</html>

