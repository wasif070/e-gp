<%-- 
    Document   : ProjUserFP
    Created on : Nov 1, 2010, 8:55:17 PM
    Author     : parag
--%>

<%@page import="com.cptu.egp.eps.model.table.TblProjectRoles"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="projSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectSrBean" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.List,com.cptu.egp.eps.dao.storedprocedure.CommonAppData" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPProjectRolesReturn,com.cptu.egp.eps.model.table.TblProcurementMethod" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPProjectFPReturn"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPProjectDetailReturn,java.util.StringTokenizer" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Project User’s Financial Power</title>
        <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
       <%-- <script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%
                boolean isEdit = false;
                int projectId = 1;
                int userId = 0;
                String action = "assignFP";
                byte suserTypeId=0;

                if (session.getAttribute("userTypeId") != null) {
                    suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                }

                if (request.getParameter("userId") != null) {
                    userId = Integer.parseInt(request.getParameter("userId"));
                }
                if (request.getParameter("projectId") != null) {
                    projectId = Integer.parseInt(request.getParameter("projectId"));
                }

                SPProjectDetailReturn pProjectDetailReturns = null;
                SPProjectRolesReturn userWiseList = null;

                pProjectDetailReturns = projSrUser.getpProjectDetailReturns(projectId).get(0);

                
                userWiseList = projSrUser.getUserRolesReturns(projectId, userId).get(0);

                List<SPProjectFPReturn> pProjectFPReturns = projSrUser.getpProjectFPReturns(userId, projectId);

                if ("Edit".equalsIgnoreCase(request.getParameter("Edit"))) {
                    isEdit = true;
                    action = "deleteFP";
                }
                
    %>
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <%--<%@include file="/resources/common/AfterLoginLeft.jsp" %>--%>

                        <td class="contentArea_1">
                            <form id="frmProjectFP" name="frmProjectFP" method="post" action="<%=request.getContextPath()%>/ProjectSrBean?action=<%=action%>">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                <div class="pageHead_1">
                                    <p align="left">Project User’s Financial Power</p>
                                </div>
                                
                                <div id="successMsg" class="responseMsg successMsg t_space" style="display:none"></div>
                                <div id="errMsg" class="responseMsg errorMsg t_space" style="display:none"></div>
                                    
                                <div class="stepWiz_1 t_space">
                                    <ul>
                                         <li> <%
                                                long pjid = 0;
                                               pjid = projSrUser.checkIsProjectExist(projectId);
                                               
                                               if(pjid != 0){
                                                      %>
                                                      <a style="text-decoration: underline;" href="CreateEditProject.jsp?projectId=<%=projectId%>&Edit=Edit" >Key Fields Information</a>
                                                <%  }else{ %>
                                                Key Fields Information
                                                <%  } %></li>
                                         <li>&gt;&gt;&nbsp;&nbsp; <%
                                              
                                                List<TblProjectRoles> projectroles = null;
                                               

                                                   projectroles =  projSrUser.checkIsProjectRoleExist(projectId);
                                                   

                                                  int uid = 0;
                                                   if( projectroles.size()>0){
                                                     TblProjectRoles proroles =   projectroles.get(0);
                                                     uid  = proroles.getTblLoginMaster().getUserId();
                                                   %>
                                                   <a style="text-decoration: underline;" href="ProjectRoles.jsp?hidProjectId=<%=projectId%>&Edit=Edit&userId=<%=uid%>" >Add Users </a>
                                                    <% }else{ %> Add Users <% } %> </li>
                                        <li class="sMenu">&gt;&gt;&nbsp;&nbsp;User Financial Power</li>
                                    </ul>
                                </div>
                                

                                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                        <tr>
                                            <td style="font-style: italic" class="ff t-align-left" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="22%">Project Information Code :</td>
                                            <td width="78%"><%
                                                        if (pProjectDetailReturns != null) {
                                                            out.print(pProjectDetailReturns.getProjectCode());
                  }%>
                                                <input name="hidProjCode" type="hidden" class="formTxtBox_1" id="hidProjCode"
                                                       value="<%
                                                                   if (pProjectDetailReturns != null) {
                                                                       out.print(pProjectDetailReturns.getProjectCode());
                                       }%>"  readonly="true" style="width:200px;" /></td>
                                        </tr>
                                        <tr>
                                            <td class="ff">Project Information Name : </td>
                                            <td><%
                                                        if (pProjectDetailReturns != null) {
                                                            out.print(pProjectDetailReturns.getProjectName());
                                                        }
                                                %>
                                                <input name="hidProjName" type="hidden" class="formTxtBox_1" id="hidProjName"
                                                       value="<%
                                                                   if (pProjectDetailReturns != null) {
                                                                       out.print(pProjectDetailReturns.getProgName());
                                                                   }
                                                       %>" readonly="true" style="width:200px;" /></td>
                                        </tr>
                                        <tr>
                                            <td><span class="ff">Procurement Role :    <span></span></span></td>
                                            <td><%
                                                        String rolesName = "";
                                                        if (userWiseList != null) {
                                                            rolesName = userWiseList.getRolesName();
                                                        }
                                                        out.print(rolesName);
                                                        StringTokenizer st = new StringTokenizer(rolesName, ",");
                                                        /*for(int irole=0; irole<=st.countTokens() ; irole++){
                  %>
                                                <input name="hidProjRole" type="hidden" class="formTxtBox_1" id="hidProjRole"
                                                       value="<%=st.nextElement()%>" />
                                                <%
                                                            }*/
                                                %>

                                                <%--<select name="cmbProcRole" class="formTxtBox_1" id="cmbProcRole" multiple="multiple" style="width:200px;">
                                                  <option>Value 1</option>
                                                  <option>Value 2</option>
                                                </select>--%>
                                            </td>
                                        </tr>
                                    </table>
                                            <div class="t-align-right">
                                                <a href="#" class="action-button-add"  name="btnAdd" id="btnAdd" onclick="addComponent();">Add More Fin. Power</a>&nbsp;
                                            <a href="#" class="action-button-delete"  name="btnRemove" id="btnRemove" onclick="removeComponent()">Remove Fin. Power</a>
                                            </div>
                                    <div class="tableHead_1 t_space">Financial Power</div>
                                    <div align="right" class="t_space">
                                        <%--<a href="#" class="action-button-add">Add More Fin. Power</a>--%>
                                        

                                            
                                       <%--  <label class="formBtn_1">
                                            <input type="button" name="btnAdd" id="btnAdd" value="Add More Fin. Power" onclick="addComponent();" />
                                        </label>
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input type="button" name="btnRemove" id="btnRemove" value="Remove Fin. Power" onclick="removeComponent()" />
                                        </label>--%>
                                        
                                        <%--<a href="#" class="action-button-delete no-margin">Remove Fin. Power</a>--%></div>

                                    <table width="100%" id="table0" cellspacing="0" class="tableList_1 t_space">
                                        <tr id="tr">
                                            <th width="10%" class="t-align-left">Select</th>
                                            <th width="30%" class="t-align-left">Procurement Method / Category</th>
                                            <th width="30%" class="t-align-left">Operator</th>
                                            <th width="30%" class="t-align-left">Amount In Million (In Nu. )</th>
                                        </tr>
                                        <tbody id="tbody0"  >
                                        <%
                                                    int count = 0;
                                                   // java.text.DecimalFormat fmt = new java.text.DecimalFormat("#.##");
                                                    if (pProjectFPReturns != null)
                                                        if (pProjectFPReturns.size() > 0) {
                                                            for (int iP = 0; iP < pProjectFPReturns.size(); iP++) {
                                                              
                                                                count++;
                                        %>
                                        <tr id="tr0_<%=count%>">
                                            <td id="td<%=count%>_0" class="t-align-left"><input type="checkbox" name="chk<%=count%>_0" id="chk<%=count%>_0" value="" />
                                            </td>
                                            <td id="td<%=count%>_1" class="t-align-left">
                                                <select name="cmbMethod<%=count%>_1" class="formTxtBox_1" id="cmbMethod<%=count%>_1" style="width:200px;" onchange="chkMethod(this,'<%=count%>');" >
                                                    <option selected="selected" value="0">-- Select --</option>
                                                    <option value="Rfq" <%if("Rfq".equalsIgnoreCase(pProjectFPReturns.get(iP).getProcurementmethodid()) || "RfqU".equalsIgnoreCase(pProjectFPReturns.get(iP).getProcurementmethodid()) ||"RfqL".equalsIgnoreCase(pProjectFPReturns.get(iP).getProcurementmethodid())){%>
                                                            selected<%}%>>RFQ</option>
                                                    <option value="Goods" <%if("Goods".equalsIgnoreCase(pProjectFPReturns.get(iP).getProcurementmethodid())){%>
                                                            selected<%}%>>Goods</option>
                                                    <option value="Services" <%if("Services".equalsIgnoreCase(pProjectFPReturns.get(iP).getProcurementmethodid())){%>
                                                            selected<%}%>>Services</option>
                                                    <option value="Works" <%if("Works".equalsIgnoreCase(pProjectFPReturns.get(iP).getProcurementmethodid())){%>
                                                            selected<%}%>>Works</option>
                                                </select>
                                                 <span id="msgMethod_<%=count%>" style="color: red; font-weight: bold">&nbsp;</span>
                                            </td>
                                            <td  id="td<%=count%>_2" class="t-align-left">
                                                <select name="cmbOperator<%=count%>_2" class="formTxtBox_1" id="cmbOperator<%=count%>_2" style="width:200px;" onchange="chkOperator(this,'<%=count%>');">
                                                    <option selected="selected" value="0">-- Select --</option>
                                                    <option value="lt"
                                                            <%if ("lt".equalsIgnoreCase(pProjectFPReturns.get(iP).getOperation())) {%>
                                                            selected
                                                            <%}%> >
                                                        &lt;</option>
                                                    <option value="gt" <%if ("gt".equalsIgnoreCase(pProjectFPReturns.get(iP).getOperation())) {%>
                                                            selected
                                                            <%}%> > &gt;</option>
                                                    <option value="lt=" <%if ("lt=".equalsIgnoreCase(pProjectFPReturns.get(iP).getOperation())) {%>
                                                            selected
                                                            <%}%> > &lt;=</option>
                                                    <option value="gt=" <%if ("gt=".equalsIgnoreCase(pProjectFPReturns.get(iP).getOperation())) {%>
                                                            selected
                                                            <%}%> > &gt;=</option>
                                                    <option value="=" <%if ("=".equalsIgnoreCase(pProjectFPReturns.get(iP).getOperation())) {%>
                                                            selected
                                                            <%}%> > =</option>
                                                </select>
                                                <span id="msgOperator_<%=count%>" style="color: red; font-weight: bold">&nbsp;</span>
                                            </td>
                                            <td id="td<%=count%>_3" class="t-align-left">
                                                <input name="txtAmount<%=count%>_3" type="text" class="formTxtBox_1" id="txtAmount<%=count%>_3"
                                                       value="<%=String.format("%.2f",pProjectFPReturns.get(iP).getAmount())%>" style="width:200px;" onblur="chkAmount(this,'<%=count%>');" />
                                                <span id="msgAmount_<%=count%>" style="color: red; font-weight: bold">&nbsp;</span>
                                            </td>
                                        </tr>

                                        <%     }
                                                } else {
                                                    
                                                    count++;
                                        %>
                                        <tr id="tr0_1">
                                            <td id="td1_0" class="t-align-left"><input type="checkbox" name="checkbox" id="chk1_0" value="" /></td>
                                            <td id="td1_1" class="t-align-left">
                                                <select name="cmbMethod1_1" class="formTxtBox_1" id="cmbMethod1_1" style="width:200px;" onchange="chkMethod(this,'<%=count%>');" >
                                                    <option selected="selected" value="0">-- Select --</option>
                                                    <option value="Rfq">RFQ</option>
                                                    <option value="Goods">Goods</option>
                                                    <option value="Services">Services</option>
                                                    <option value="Works">Works</option>
                                                </select>
                                                <span id="msgMethod_<%=count%>" style="color: red; font-weight: bold">&nbsp;</span>
                                            </td>
                                            <td  id="td1_2" class="t-align-left">
                                                <select name="cmbOperator1_2" class="formTxtBox_1" id="cmbOperator1_2" style="width:200px;" onchange="chkOperator(this,'<%=count%>');">
                                                    <option selected="selected" value="0">-- Select --</option>
                                                    <option value="lt" >&lt;</option>
                                                    <option value="gt" >&gt;</option>
                                                    <option value="lt=" >&lt;=</option>
                                                    <option value="gt=" >&gt;=</option>
                                                    <option value="=" >=</option>
                                                </select>
                                                <span id="msgOperator_<%=count%>" style="color: red; font-weight: bold">&nbsp;</span>
                                            </td>
                                            <td id="td1_3" class="t-align-left">
                                                <input name="txtAmount1_3" type="text" class="formTxtBox_1" id="txtAmount1_3" style="width:200px;" onblur="chkAmount(this,'1');" />
                                                <span id="msgAmount_1" style="color: red; font-weight: bold">&nbsp;</span></td>
                                        </tr>

                                        <%
                                                    }
                                        %>                                        
                                        </tbody>
                                    </table>

                                    <div>&nbsp;
                                        <table width="100%" id="table1" cellspacing="0" border="0" >
                                            <tr>
                                                <td class="t-align-center"><span class="formBtn_1">
                                                        <input type="submit" name="button2" id="button2" value="Submit" onclick="return checkFinancialPower();" />
                                                    </span>
                                                    <%if (pProjectFPReturns.size() > 0) {%>
                                                    &nbsp;&nbsp;
                                                    <%--<label class="formBtn_1">
                                                        <input type="button" name="btnSubmission" id="btnSubmission" value="Approve" onclick="forApprove();" />
                                                    </label>--%>
                                                </td>
                                                <%}%>
                                            </tr>
                                        </table>
                                        <input type="hidden" name="txtCount" id="txtCount" value="<%=count%>" />
                                        <input type="hidden" name="userId" id="userId" value="<%=userId%>" />
                                        <input type="hidden" name="hidProjectId" id="hidProjectId" value="<%=projectId%>" />
                                    </div>
                    </tr>
                </table>
                </form>
                <!--Page Content End-->
                </td>
                </tr>
                </table>
                <%--<%@include file="/resources/common/Bottom.jsp" %>--%>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<form id="form1" method="post" action="">
    <input type="hidden" name="hidProjectId" id="hidProjectId" value="<%=projectId%>" />
</form>
<script>
   <%-- function forApprove(){
        var flag=false;
        document.getElementById("form1").action="<%=request.getContextPath()%>/ProjectSrBean?projectId=<%=projectId%>&action=approve";
        document.getElementById("form1").submit();
    }--%>
</script>
<%
    projSrUser = null;
%>
<script type="text/javascript">
    <%--function numeric(value) {
        return /^\d+$/.test(value);
    }--%>
    function numericDE(value) {
       // alert('DE ::'+value);
        return  /^(\d+(\.\d{1,2})?)$/.test(value);
    }

    var count=1;
    function addComponent(){
        count=document.getElementById("txtCount").value;
        //alert('sanjay start');
        var procurementMethod1="";
        var budgetType="";
        var amount="";
        var operator="";
        var str="";
        if(!checkFinancialPower()){
            return false;
        }

                count++;

                document.getElementById("txtCount").value=count;
                var tbody0=document.getElementById("tbody0");
               // var table0 = document.getElementById("table0");

                var tr0=document.createElement("tr");
                tr0.id="tr0_"+count;

                var cnt=-1;
                //alert(count +" == "+cnt );
                var td0=document.createElement("td");
                td0.id="td"+count+"_"+(++cnt);
                td0.className="t-align-left";
                td0.innerHTML ="<input type='checkbox' id='chk"+count+"_"+cnt+"' name='checkbox'  />";
                tr0.appendChild(td0);

                //alert(cnt);

                var td1=document.createElement("td");
                td1.id="td"+count+"_"+(++cnt);
                td1.className="t-align-left";
                procurementMethod1 = " <select name='cmbMethod"+count+"_"+(cnt)+"' id='cmbMethod"+count+"_"+(cnt)+"' class='formTxtBox_1' style='width: 200px' onchange='chkMethod(this,"+count+");'>";
                procurementMethod1 += "<option value='0'>-- Select --</option>";
                procurementMethod1 += "<option value='Rfq'> RFQ</option>";
                procurementMethod1 += " <option value='Goods'>Goods</option>";
                procurementMethod1 += " <option value='Services'>Services</option>";
                procurementMethod1 += " <option value='Works'>Works</option>";
                procurementMethod1 += "</select>";
                procurementMethod1 += " <span id='msgMethod_"+count+"' style='color: red; font-weight: bold'>&nbsp;</span>";
                td1.innerHTML = procurementMethod1;
                tr0.appendChild(td1);

                var td2=document.createElement("td");
                td2.id="td"+count+"_"+(++cnt);
                td2.className="t-align-left";
                operator = "<select name='cmbOperator"+count+"_"+(cnt)+"'  id='cmbOperator"+count+"_"+(cnt)+"' class='formTxtBox_1' style='width: 200px' onchange='chkOperator(this,"+count+");'>";
                operator += "<option value='0'>-- Select --</option>";
                operator += "<option value='lt'>&lt;</option>";
                operator += "<option value='gt'>&gt;</option>";
                operator += "<option value='lt='>&lt;=</option>";
                operator += "<option value='gt='>&gt;=</option>";
                operator += "<option value='='>=</option>";
                operator += "</select>";
                operator += " <span id='msgOperator_"+count+"' style='color: red; font-weight: bold'>&nbsp;</span>";
                td2.innerHTML = operator;
                tr0.appendChild(td2);

                var td3=document.createElement("td");
                td3.id="td"+count+"_"+(++cnt);
                td3.className="t-align-left";
                amount = "<input name='txtAmount"+count+"_"+(cnt)+"'  id='txtAmount"+count+"_"+(cnt)+"' class='formTxtBox_1' style='width: 200px' onblur='chkAmount(this,"+count+");' />";
                amount += " <span id='msgAmount_"+count+"' style='color: red; font-weight: bold'>&nbsp;</span> ";
                //amount += "";
                td3.innerHTML = amount;
                tr0.appendChild(td3);
                tbody0.appendChild(tr0);
            }

            function removeComponent(){
                count=document.getElementById("txtCount").value;
                var tbody0=document.getElementById("tbody0");
                for(var i=1;i<=count;i++){
                    if(document.getElementById("chk"+i+"_"+0) != null){
                        if(document.getElementById("chk"+i+"_"+0).checked == true){
                            tbody0.removeChild(document.getElementById("tr0_"+i));
                        }
                    }
                }
            }

            function checkFinancialPower(){
                count=document.getElementById("txtCount").value;
                var isMethod=false;
                var isOperator=false;
                var isAmount=false;
                var flag=true;
                for(var i=1;i<=count;i++){
                    if(document.getElementById("chk"+i+"_"+0) != null){
                        if(document.getElementById("cmbMethod"+i+"_"+1).value == 0){
                           <%-- document.getElementById("errMsg").innerHTML="";
                            document.getElementById("errMsg").style.display='block';
                            document.getElementById("errMsg").innerHTML ='Please Select Procurement Method.';--%>
                            $('#msgMethod_'+i).html('<br/>Please Select Procurement Method or Category.');
                            isMethod = true;
                        }
                        else{
                            $('#msgMethod_'+i).html('');
                        }
                        if(document.getElementById("cmbOperator"+i+"_"+2).value == 0){
                           <%-- document.getElementById("errMsg").innerHTML="";
                            document.getElementById("errMsg").style.display='block';
                            document.getElementById("errMsg").innerHTML ='Please Select Operator.';--%>
                            $('#msgOperator_'+i).html('<br/>Please Select Operator.');
                            isOperator = true;
                        }
                        else{
                            $('#msgOperator_'+i).html('');
                        }
                        if(document.getElementById("txtAmount"+i+"_"+3).value == ""){
                            <%--document.getElementById("errMsg").innerHTML="";
                            document.getElementById("errMsg").style.display='block';
                            document.getElementById("errMsg").innerHTML ='Please Enter Amount.';--%>
                            isAmount = true;
                        }
                    }
                    if(isMethod || isOperator || isAmount){
                        flag=false;
                    }

                    for(var j=1;j<=count;j++){
                        if(document.getElementById("chk"+j+"_"+0) != null){
                           // alert(document.getElementById("txtAmount"+j+"_"+3).value);
                           // alert(numericDE(document.getElementById("txtAmount"+j+"_"+3).value));
                            if(!numericDE(document.getElementById("txtAmount"+j+"_"+3).value)){
                                $('#msgAmount_'+j).html('<br/>Please enter numbers with Two decimal only.');
                                flag = false;
                            }
                        }
                    }
                }
                
                return flag;
            }

            function chkAmount(obj,msgCount){
                var flag=true;
                if(!numericDE(obj.value)){
                    $('#msgAmount_'+msgCount).html('<br/>Please enter numbers with Two decimal only.');
                    flag = false;
                }else{
                    $('#msgAmount_'+msgCount).html('');
                }
                return flag;
            }

            function chkMethod(obj,msgCount){
                var flag=true;
                if(obj.value == 0)
                {
                    $('#msgMethod_'+msgCount).html('Please Select Procurement Method or Category.');
                    flag = false;
                }else{
                    $('#msgMethod_'+msgCount).html('');
                }
                return flag;
            }

            function chkOperator(obj,msgCount){
                var flag=true;
                if(obj.value == 0)
                {
                    $('#msgOperator_'+msgCount).html('Please Select Operator.');
                    flag = false;
                }else{
                    $('#msgOperator_'+msgCount).html('');
                }
                return flag;
            }

    $('#frmProjectFP').submit(function(){
      $('#button2').attr("disabled", true);
       document.getElementById("frmProjectFP").submit();
    });
</script>