<%--
    Document   : ManagePEAdmin
    Created on : Oct 30, 2010, 5:25:12 PM
    Author     : rishita
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblStateMaster"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.databean.ManageAdminDtBean,com.cptu.egp.eps.web.servicebean.PEAdminSrBean" %>
<%@page import="com.cptu.egp.eps.model.table.TblOfficeMaster" %>
<%@page import="java.util.List" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%
    String hasMobile = "";
                    if(request.getParameter("hasMobile") != null){
                        hasMobile = request.getParameter("hasMobile");
                    }
    %>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage PA Admin</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                var hasMobileNo = '<%=hasMobile%>';
                if(hasMobileNo == 'No')
                    alert("Please insert your mobile no");

                $("#frmManagePEAdmin").validate({
                    rules: {
                        fullName: { spacevalidate: true, requiredWithoutSpace: true , alphaName:true, maxlength:100 },
                        officeList: { required: "#officeList" },
                        mobileNo: {required:true,number: true, minlength: 8, maxlength:8 },
                        phoneNo: { required:true,numberWithHyphen:true,PhoneFax: true},
                        nationalId: { required:true, number: true,maxlength: 11,minlength: 11}
                    },
                    messages: {
                        fullName: {
                            spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace: "<div class='reqF_1'>Please enter Full Name</div>",
                            alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only </div>",
                            maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"
                        },
                        officeList: { required: "<div class='reqF_1'>Please select Office.</div>"},

                        mobileNo: {
                            required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                            number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" ,
                            minlength:"<div class='reqF_1'>Minimum 8 digits are required</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 8 digits only</div>"
                        },
                        phoneNo: {
                            required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            numberWithHyphen: "<div class='reqF_1'>Allows numbers (0-9) and hyphen (-) only</div>",
                            PhoneFax:"<div class='reqF_1'>Please enter valid Phone No. Area code should contain minimum 2 digits and maximum 5 digits. Phone no. should contain minimum 3 digits and maximum 10 digits</div>"
                        },
                        nationalId: {
                            required: "<div class='reqF_1'>Please enter CID No.</div>",
                            number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                            maxlength: "<div class='reqF_1'>CID No. should comprise of maximum 11 digits</div>",
                            minlength: "<div class='reqF_1'>CID No. should comprise of minimum 11 digits</div>"}
                    },

                    errorPlacement:function(error ,element)
                    {
                        if(element.attr("name")=="phoneNo")
                        {
                            error.insertAfter("#fxno");
                        }
                        else
                        {
                            error.insertAfter(element);

                        }
                    }
                });
            });
        </script>
        <script type="text/javascript">
//            var validCheckMno = true;
//            $(function() {
//                $('#txtMobileNumber').blur(function() {
//                    var hdMobileNo = document.getElementById('hdmobileNo').value;
//                    var mobileNo = document.getElementById('txtMobileNumber').value;
//                    if(hdMobileNo != mobileNo){
//                        $('span.#mobMsg').css("color","red");
//                        $('span.#mobMsg').html("Checking for unique Mobile No...");
//                        $.post("<-%=request.getContextPath()%>/CommonServlet", {mobileNo:$.trim($('#txtMobileNumber').val()),funName:'verifyMobileNo'},  function(j){
//                            if(j.toString().indexOf("OK", 0)==0){
//                                $('span.#mobMsg').css("color","green");
//                                validCheckMno = true;
//                            }
//                            else if(j.toString().indexOf("Mobile", 0)!=-1){
//                                $('span.#mobMsg').css("color","red");
//                                validCheckMno = false;
//                            }
//                            $('span.#mobMsg').html(j);
//                        });
//                    }else{
//                        $('span.#mobMsg').html("");
//                    }
//                });
//            });

            function getOfficesByState(){

                $.post("<%=request.getContextPath()%>/CommonServlet", {deptId:$('#txtdepartmentid').val(),stateId:$('#cmbdistrict').val(),funName:'officeListByStateId'}, function(j){
                    //alert(j);
                    $("select#officeList").html(j);
                });
            }
            function check(){
                if(document.getElementById("officeList").value==0){
                    document.getElementById("officeMsg").innerHTML = "<br/>Please select Office.";
                    return false;
                }else{
                    document.getElementById("officeMsg").innerHTML = "";
                    return true;
                }
            }
            
        </script>
    </head>
    <jsp:useBean id="manageAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.UpdateAdminSrBean"/>
    <jsp:useBean id="adminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AdminMasterDtBean"/>
    <jsp:useBean id="peAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.PEAdminSrBean"/>
    <jsp:useBean id="commonServlet" scope="request" class="com.cptu.egp.eps.web.servlet.CommonServlet"/>
    <jsp:setProperty name="adminMasterDtBean" property="*"/>
    <body>
        <%
                    byte userTypeid = 0;
                    if (request.getParameter("userTypeid") != null) {
                        userTypeid = Byte.parseByte(request.getParameter("userTypeid"));
                    }

                    int uTypeid = 0;
                    if (session.getAttribute("userTypeId") != null) {
                        uTypeid = Integer.parseInt(session.getAttribute("userTypeId").toString());
                    }

                    int userId = 0;
                    if (request.getParameter("userId") != null) {
                        userId = Integer.parseInt(request.getParameter("userId"));
                    }

                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        peAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                        manageAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                    }
                    
                    if ("Update".equals(request.getParameter("Update"))) {
                        String msg = "";
                        int strM = -1; // Default
                       // String nationalId = adminMasterDtBean.getNationalId();
                        if (strM == -1) {
                            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                            manageAdminSrBean.setAuditTrail(null);
                            List<ManageAdminDtBean> peAdmin = manageAdminSrBean.getManagePEDetail(userTypeid, userId);
                            String comNationalId = peAdmin.get(0).getNationalId();
                            /*if (!comNationalId.equals(nationalId)) {
                                msg = commonService.verifyNationalId(nationalId);
                                if (msg.length() > 2) {
                                    strM = 1; //NationalId Already Exist
                                }
                            }*/
                        }

                        if (strM == -1) {
                            if(adminMasterDtBean.getNationalId()!=null)
                                adminMasterDtBean.setNationalId(adminMasterDtBean.getNationalId().trim());
                            else
                                adminMasterDtBean.setNationalId("");

                            if(adminMasterDtBean.getMobileNo()!=null)
                                adminMasterDtBean.setMobileNo(adminMasterDtBean.getMobileNo().trim());
                            else
                                adminMasterDtBean.setMobileNo("");

                            String actionAudit = null;
                            String moduleName = null;
                            if(!"1".equals(session.getAttribute("userTypeId").toString())){
                               manageAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                               actionAudit = "Edit Profile";
                               moduleName = EgpModule.My_Account.getName();
                            }else{
                                
                                if(userTypeid == 4){
                                    manageAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                    actionAudit = "Edit PE Admin";
                                    moduleName = EgpModule.Manage_Users.getName();
                                }
                            }
                            if (manageAdminSrBean.peUpdate(adminMasterDtBean, request.getParameterValues("officeList"),actionAudit,moduleName)) {
                                manageAdminSrBean.setAuditTrail(null);
        %>
        <form id="frmUpdatePE" name="frmUpdatePE" method="POST" action="ViewPEAdmin.jsp">
            <input type="hidden" name="userId" id="userTypeid" value="<%=userId%>"/>
            <input type="hidden" name="userTypeid" id="userTypeid" value="<%=userTypeid%>"/>
            <input type="hidden" name="from" id="from" value="create"/>
        </form>
        <script>
            document.getElementById("frmUpdatePE").submit();
        </script>
        <%
                            }else{
                                response.sendRedirect("ManagePEAdmin.jsp?userId=" + userId + "&userTypeid=" + userTypeid + "&msg=updateFail");
                            }
                        } else {
                            response.sendRedirect("ManagePEAdmin.jsp?userId=" + userId + "&userTypeid=" + userTypeid + "&strM=" + strM);
                        }
                    }
        %>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("hdnUserType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                            userType.append(request.getParameter("hdnUserType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }
                        %>
                        <%if(Integer.parseInt(session.getAttribute("userTypeId").toString())==1 || Integer.parseInt(session.getAttribute("userTypeId").toString())==5){%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <%}%>
                        <td class="contentArea_1">
                            <%
                                        int strM = -1;
                                        String msg = "";
                                        if (request.getParameter("strM") != null) {
                                            try {
                                                strM = Integer.parseInt(request.getParameter("strM"));
                                            } catch (Exception ex) {
                                                strM = 2;
                                            }
                                            switch (strM) {
                                                case (1):
                                                    msg = "NationalID Already Exists";
                                                    break;
                                            }
                            %>
                            <div class="responseMsg errorMsg" style="margin-left:7px; margin-right: 2px; margin-top: 15px;">
                                <%= msg%>
                            </div>
                            <%
                                            msg = null;
                                        }
                            %>
                            <%if(Integer.parseInt(session.getAttribute("userTypeId").toString())==1 || Integer.parseInt(session.getAttribute("userTypeId").toString())==5){%>
                                <div class="pageHead_1">Edit PA Admin Details</div>
                            <%}else{%>
                            <div class="pageHead_1">Edit Profile</div>
                            <%}%>
                            <% if(request.getParameter("msg") != null && "updateFail".equals(request.getParameter("msg"))){ %>
                            <div class='responseMsg errorMsg'><%=appMessage.peAdminUpdate %></div>
                            <% } %>
                            <!--Page Content Start-->
                            <form id="frmManagePEAdmin" name="frmManagePEAdmin" method="post" action="">

                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td class="ff" width="200">Hierarchy Node :</td>
                                        <%
                                                    manageAdminSrBean.setAuditTrail(null);
                                                    List<ManageAdminDtBean> officeListDetails = manageAdminSrBean.getOfficeList(userId);
                                                    List<ManageAdminDtBean> allOfficeListDetails = manageAdminSrBean.getOfficeAllList(userId);
                                                    String deptName = "";
                                                    int deptId = 0;
                                                    for (ManageAdminDtBean officelist : officeListDetails) {
                                                        deptName = officelist.getDepartmentName();
                                                        deptId = officelist.getDepartmentId();
                                                    }
                                        %>
                                        <td>
                                            <label><%=deptName%></label>
                                            <input type="hidden" name="txtdepartmentid" id="txtdepartmentid" value="<%=deptId%>">
                                        </td>
                                    </tr>
<%
                                    for (ManageAdminDtBean peAdminDetails : manageAdminSrBean.getManagePEDetail(userTypeid, userId)) {
%>
<%-- <tr>
                                        
                                        <td class="ff" width="200">Dzongkhag / District :</td>
                                        <td>
                                            <%if(Integer.parseInt(session.getAttribute("userTypeId").toString())==1 || Integer.parseInt(session.getAttribute("userTypeId").toString())==5){%>
<%
                                        short countryId = 150;
                                        List<TblStateMaster> stateMasters =  peAdminSrBean.getBanglaStates(countryId);
                                        
                                        request.setAttribute("stateMasters", stateMasters);
%>
                                            <select style="width:200px;" class="formTxtBox_1" name="district" id="cmbdistrict" onchange="getOfficesByState();">
<%
                                        for(int i=0;i<stateMasters.size();i++){
%>
                                        <option  value="<%=stateMasters.get(i).getStateId()%>" <%if(stateMasters.get(i).getStateId()==peAdminDetails.getStateId()){out.print(" selected ");}%>>
                                            <%=stateMasters.get(i).getStateName()%>
                                        </option>
<%
                                        }
%>
                                            </select>
                                            <%}else{
                                            out.println(peAdminDetails.getStateName());
                                            %>
                                            <input type="hidden" name="district" id="cmbdistrict" value="<%=peAdminDetails.getStateId()%>" />
                                            <%}%>
                                        </td>

                                    </tr>--%>
                                    <tr>
                                        <td class="ff" width="200">PA Office : <%if(uTypeid == 1){%><span>*</span><% } %></td>
                                        <td>
                                            <%if(Integer.parseInt(session.getAttribute("userTypeId").toString())==1 || Integer.parseInt(session.getAttribute("userTypeId").toString())==5){%>
                                            <input type="hidden" name="officeListNeed" id="officeListNeed" value="0">
                                            <select name="officeList" id="officeList" style="width: 200px;" class="formTxtBox_1" onclick="return check();">
                                                <option value="0">-- Select Office --</option>
<%
                                                            for (ManageAdminDtBean allOfficeList : allOfficeListDetails) {
%>
                                                <option value="<%=allOfficeList.getOfficeId()%>"
                                                    <%if(peAdminDetails.getOfficeId() == allOfficeList.getOfficeId()) {%>selected<%}%> >
                                                    <%=allOfficeList.getOfficeName()%>
                                                <script> var oldOffice = '<%=peAdminDetails.getOfficeId()%>'; </script>
                                                </option>
<%
                                                            }
%>
                                            </select>
                                            <%}else{
                                                    out.println(peAdminDetails.getOfficeName());
                                            %>
                                            <input type="hidden" name="officeList" id="officeList" value="<%=peAdminDetails.getOfficeId()%>">
                                             <%}%>
                                            
                                            <span id="officeMsg" style="color: red; font-weight: bold"></span>
                                            <div id="readonlyMsg" class="reqF_1"></div>
                                            <div id="errMsg" style="color: red;"></div>
                                            <div id="Msg" style="color: red; font-weight: bold"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">e-mail ID:</td>
                                        <td>
                                            <input type="hidden" name="adminId" value="<%= peAdminDetails.getAdminId()%>"/>
                                            <input type="hidden" name="userId" value="<%= peAdminDetails.getUserId()%>"/>
                                            <label id="lblEmailId"><%= peAdminDetails.getEmailId()%></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff" width="200">Full Name : <span>*</span></td>
                                        <td>
                                            <input class="formTxtBox_1" name="fullName" value="<%=peAdminDetails.getFullName()%>" id="txtFullName"  maxlength="101" type="text" style="width: 200px;"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200"> CID No. : <span>*</span> </td>
                                        <td>
    <!--                                        <label id="lblNationalId"><%= peAdminDetails.getNationalId()%></label>-->
                                            <input class="formTxtBox_1" type="text" id="txtNationalId" name="nationalId" value="<%= peAdminDetails.getNationalId()%>" onblur="return chkUnique();" maxlength="26" style="width: 200px;"/>
                                            <input class="formTxtBox_1" type="hidden" id="txtHiddenNationalId" name="hiddenNationalId" value="<%= peAdminDetails.getNationalId()%>" />
                                            <span id="errMsg" style="color: red; font-weight: bold"></span>
                                        </td>
                                    </tr>
                                    <!--<tr>
                                        <td class="ff" width="200">Phone No. : <span>*</span></td>
                                        <td>
                                            <input class="formTxtBox_1" name="phoneNo" value="<%=peAdminDetails.getPhoneNo()%>" id="txtPhoneNumber"  type="text" style="width: 200px;" maxlength="17"/>
                                            <span id="fxno" name="fxno" style="color: grey;"> AreaCode-Phone No. i.e. 02-336962</span>
                                        </td>
                                    </tr>-->
                                    <tr>
                                        <td class="ff" width="200">Mobile No. : <span>*</span></td>
                                        <td>
                                            <input class="formTxtBox_1" name="mobileNo" value="<%=peAdminDetails.getMobileNo()%>" id="txtMobileNumber" maxlength="16" type="text" style="width: 200px;" /><span id="mNo" style="color: grey;"> (Mobile No. format should be e.g 12345678)</span>
                                            <input class="formTxtBox_1" name="hdmobileNo" value="<%=peAdminDetails.getMobileNo()%>" id="hdmobileNo"  type="hidden"  />
                                            <span id="mobMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <%  }%>

                                        <td>&nbsp;</td>
                                        <td align="left"><label class="formBtn_1"><input id="Update" name="Update" value="Update" type="submit" onclick="return chkUniqueNationalID();"/></label></td>
                                    </tr>
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            
            
            
       <script type="text/javascript">
        var flag = true;
        $(function () {
            $('#officeList').blur(function () {
                var newOffice = $('#officeList').val();
                //alert("The input value has changed. The new value is: "+newOffice);
                flag = true;
                $('#Msg').css("color", "red");
                $('#Msg').html("Checking for unique Office Admin...");
                $.post("<%=request.getContextPath()%>/CommonServlet", {officeId: $('#officeList').val(), funName: 'verifyOffice'},
                        function (j)
                        {
                            if (j.toString().indexOf("OK", 0) != -1 || oldOffice == newOffice) {
                                if(oldOffice == newOffice)
                                {
                                    j = "OK";
                                }
                                $('#Msg').css("color", "green");
                                $('#errMsg').html('');
                                
                            } else if (j.toString().indexOf("Admin", 0) != -1) {
                                flag = false;
                                $('#Msg').css("color", "red");
                                $('#errMsg').html('');

                            }
                            //$('span.#Msg').html(j);

                            //alert(document.getElementById('oName'));
                            if (document.getElementById('oName') == null) {
                                $('#Msg').html(j);
                                $('#errMsg').html('');
                            } else {
                                document.getElementById('oName').removeAttribute('id');
                                $('#Msg').html('');
                                $('#errMsg').html('');
                            }
                        });
                
                
            });
        });
        


    </script>
            
            
            
            
            
            
        <script type="text/javascript">
            function chkUnique(){
                var txtHiddenNationalId = document.getElementById('txtHiddenNationalId').value;
                var txtNationalId = document.getElementById('txtNationalId').value;
                if(txtHiddenNationalId != txtNationalId){
                    //$('span.#errMsg').css("color","red");
                   // $('span.#errMsg').html("Checking for unique National ID ...");
                    $.post("<%=request.getContextPath()%>/CommonServlet", {nationalId:$.trim($('#txtNationalId').val()),flag:'true',funName:'verifyNationalId'},  function(j){
                        if(j.toString().indexOf("OK", 0)!=-1){
                            $('span.#errMsg').css("color","green");
                        }
                        else if(j.toString().indexOf("National", 0)!=-1){
                            $('span.#errMsg').css("color","red");
                        }
                        $('span.#errMsg').html(j);
                        $('span.#errMsg').hide();
                    });
                }

            }
            function chkUniqueNationalID(){

                if(document.getElementById("officeListNeed").value=="0" && document.getElementById("officeList").value=="0"){
                    document.getElementById("officeMsg").innerHTML = "<br/>Please select Office.";
                    return false;
                }
                if(!flag)
                {
                    return false;
                }
                if(validCheckMno){
                }else{
                    return false;
                }
                if(document.getElementById('errMsg').innerHTML == 'Checking for unique National ID ...' || document.getElementById('errMsg').innerHTML == 'National-ID already exists' || document.getElementById('mobMsg').innerHTML  == 'Checking for unique Mobile No...'){
                    return false;
                }
                else{
                    return true;
                }
            }
                var uTypeId = '<%=uTypeid%>';
                var headSel_Obj;
                if(uTypeId == 1){
                    headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
                }else{
                    headSel_Obj = document.getElementById("headTabMyAcc");
                    if(headSel_Obj != null){
                        headSel_Obj.setAttribute("class", "selected");
                    }
                }
            </script>
    </body>
    <%
                manageAdminSrBean = null;
                adminMasterDtBean = null;
    %>
</html>
