<%--
    Document   : ViewSbDpTransferHistory
    Created on : Apr 18, 2011, 11:26:04 AM
    Author     : Ketan Prajapati
--%>

<%@page import="com.cptu.egp.eps.model.table.TblEmployeeTrasfer"%>
<%@page import="com.cptu.egp.eps.model.view.VwEmpfinancePower"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPGovtEmpRolesReturn"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<%@page import="com.cptu.egp.eps.model.table.TblPartnerAdminTransfer"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<%@page import="java.util.Collections"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <jsp:useBean id="govSrUser" class="com.cptu.egp.eps.web.servicebean.GovtUserSrBean" />
    <%@page import="java.util.List" %>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <%
                    String str = request.getContextPath();
                    int userId = 0;
                    int empId = 0;
                    int j = 1;

                    if (request.getParameter("empId") != null) {
                        empId = Integer.parseInt(request.getParameter("empId").toString());
                    }
                    if (request.getParameter("userId") != null) {
                        userId = Integer.parseInt(request.getParameter("userId"));
                        govSrUser.setUserId(userId);
                    } else {
                        if (empId != 0) {
                            userId = govSrUser.takeUserIdFromEmpId(empId);
                            govSrUser.setUserId(userId);
                        }
                    }
                    try {
                        empId = govSrUser.takeEmpIdFromUserId(userId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    govSrUser.setEmpId(empId);
                    govSrUser.setUserId(userId);
                    Object[] object = null;
                    object = govSrUser.getGovtUserData();
                    List<TblEmployeeTrasfer> list = govSrUser.getUserHistory(userId);
                    
                     // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    String idType="userId";
                    int auditId=userId;
                    String auditAction="View Transfer History of Government User";
                    String moduleName=EgpModule.Manage_Users.getName();
                    String remarks="";
                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>User Transfer History</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
    </head>

    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" ></jsp:include>
                        <td class="contentArea_1">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td>
                                        <div class="pageHead_1">
                                            User Transfer History
                                            <span class="c-alignment-right"><a href="ViewGovtUserDetail.jsp?empId=<%=empId%>&userTypeId=<%=userTypeId%>&mode=View" class="action-button-goback">Go Back</a></span>
                                        </div>
                                        <div class="t_space">
                                        </div>
                                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                            <tr>
                                                <td class="ff">e-mail ID : </td>
                                                <td><% out.print((String) object[0]);%>
                                                </td>
                                            </tr>
                                            <%--<tr>
                                                <td class="ff">Name in Dzongkha : </td>
                                                <%if (object[3] != null) {
                                                                if (!"".equalsIgnoreCase(new String(((byte[]) object[3]), "UTF-8"))) {%>
                                                <td><% out.print("" + new String(((byte[]) object[3]), "UTF-8"));%>
                                                </td>
                                                <% }
                                                            }%>
                                            </tr>--%>
                                            <tr>
                                                <td class="ff">CID No. : </td>
                                                <%     if (!"".equalsIgnoreCase((String) object[5])) {%>
                                                <td><%out.print((String) object[5]);%>
                                                </td>
                                                <% }%>
                                            </tr>
                                            <tr>
                                                <td class="ff">Mobile No : </td>
                                                <%       if (!"".equalsIgnoreCase(((String) object[4]))) {%>
                                                <td><%out.print((String) object[4]);%>
                                                </td>
                                                <% }%>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="tableList_1 t_space" cellspacing="0" width="100%">
                                            <tr>
                                                <th class="t-align-center">Sl. No.</th>
                                                <th class="t-align-center">Full Name</th>
                                                <th class="t-align-center">Last Working Date</th>
                                                <th class="t-align-center">Replacement Date and Time</th>
                                                <th class="t-align-center">Replaced By</th>
                                                <th class="t-align-center">Comments</th>
                                            </tr>
                                            <%
                                                        boolean flag = false;
                                                        for (int i = 0; i < list.size(); i++) {
                                                            String strFullName = "-";
                                                            String strReplacedBy = "-";
                                                            String strComments = "-";

                                                            if (!"".equalsIgnoreCase(list.get(i).getReplacedBy())) {
                                                                strReplacedBy = list.get(i).getReplacedBy();
                                                                flag = true;
                                                                if (!"".equalsIgnoreCase(list.get(i).getEmployeeName())) {
                                                                    strFullName = list.get(i).getEmployeeName();
                                                                }

                                                                if (!"".equalsIgnoreCase(list.get(i).getRemarks())){
                                                                    strComments = list.get(i).getRemarks();
                                                                }
                                                                
                                            %>
                                            <tr
                                                <%if (i % 2 == 0) {%>
                                                class="bgColor-Green"
                                                <%} else {%>
                                                class="bgColor-white"

                                                <%   }%>
                                                >
                                                <td class="t-align-center"> <%=j%>
                                                    
                                                </td>
                                                <td class="t-align-center"><%=strFullName%></td>
                                                <td class="t-align-center"><%=DateUtils.gridDateToStr(list.get(i).getTransferDt())%></td>
                                                <td class="t-align-center"><%=DateUtils.gridDateToStr(list.get(i).getTransferDt())%></td>
                                                <td class="t-align-center"><%=strReplacedBy%></td>
                                                <td class="t-align-center"><%=strComments%></td>
                                            </tr>
                                            <%j++;}
                                                        }
                                                        if (!flag) {
                                            %>
                                            <td class="t-align-center" colspan="6" style="color: red;font-weight: bold">No Record Found</td>
                                            <%}%>


                                        </table>
                                        <script>
                                            var obj = document.getElementById('lblGovUsersCreation');
                                            if(obj != null){
                                                if(obj.innerHTML == 'Create Government Users'){
                                                    obj.setAttribute('class', 'selected');
                                                }
                                            }

                                        </script>
                                        <script>
                                            var headSel_Obj = document.getElementById("headTabMngUser");
                                            if(headSel_Obj != null){
                                                headSel_Obj.setAttribute("class", "selected");
                                            }
                                        </script>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>

</html>
