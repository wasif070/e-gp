<%-- 
    Document   : viewQuestion
    Created on : May 29, 2017, 1:01:54 PM
    Author     : feroz
--%>

<%@page import="com.cptu.egp.eps.model.table.TblQuizAnswer"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuizAnswerService"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuizQuestion"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuizQuestionService"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuestionModule"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuestionModuleService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Question</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>        <script type="text/javascript">
            $(document).ready(function() {
                sortTable();
            });
        </script>
        
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <%
                    int moduleId = -1;
                    if(request.getParameter("moduleId")!=null)
                    {
                        moduleId = Integer.parseInt(request.getParameter("moduleId"));
                    }
                    QuizQuestionService quizQuestionService = (QuizQuestionService) AppContext.getSpringBean("QuizQuestionService");
                    List<TblQuizQuestion> tblQuizQuestion = quizQuestionService.findTblQuizQuestion(moduleId);
                    
                    QuizAnswerService quizAnswerService = (QuizAnswerService) AppContext.getSpringBean("QuizAnswerService");
                    List<TblQuizAnswer> tblQuizAnswer = quizAnswerService.getAllTblQuizAnswer();
         %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea">
                        
                        <div class="pageHead_1">Question
                        </div>
                        <br>
                        <%
                            if(tblQuizQuestion.isEmpty())
                            {
                                %><b><span style="color: red; padding-left: 42%; font-size:20px;"><%out.print("No data found!");%></span></b><%
                            }
                            else{
                        %>
                        <table class="tableList_1 t_space" cellspacing="0" width="100%" id="resultTable" cols="">
                                <tbody id="tbodyData">&nbsp;
                                    
                                <tr>
                                    <th class="t-align-center" style="width: 4%">Sl. No.</th>
                                    <th class="t-align-center">Question Module</th>
                                    <th class="t-align-center">Action</th>
                                </tr>
                                <%
                                  int cnt = 0;
                                  for(TblQuizQuestion tblQuizQuestionThis : tblQuizQuestion){
                                        cnt++;
                                        if(cnt%2==0){%>
                                              <tr>
                                      <%}else{%>
                                              <tr style='background-color:#E4FAD0;'>
                                      <%}%>
                                              <td class="t-align-center">
                                                  <%out.print(cnt);%>
                                              </td>
                                              <td class="t-align-center">
                                                  <%out.print(tblQuizQuestionThis.getQuestion());%>
                                              </td>
                                              <td class="t-align-center">
                                                  <a href="questionDetails.jsp?questionId=<%=tblQuizQuestionThis.getQuestionId()%>">Details</a>&nbsp;
                                                  <a href="editQuestion.jsp?questionId=<%=tblQuizQuestionThis.getQuestionId()%>">Edit</a>&nbsp;
                                                  <a href="deleteQuestion.jsp?questionId=<%=tblQuizQuestionThis.getQuestionId()%>">Delete</a>
                                              </td>
                                              </tr>
                                <%}%>
                                </tbody>
                            </table>
                                <%}%>
                </td>
            </tr>
        </table>
        

        <form id="formstyle" action="" method="post" name="formstyle">

           <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
           <%
             SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
             String appenddate = dateFormat1.format(new Date());
           %>
           <input type="hidden" name="fileName" id="fileName" value="DebarmentCommittee_<%=appenddate%>" />
            <input type="hidden" name="id" id="id" value="EvaluationRule" />
        </form>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabDebar");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        var leftObj = document.getElementById("lblViewDebarCom");
        if(leftObj != null){
            leftObj.setAttribute("class", "selected");
        }
    </script>
</html>