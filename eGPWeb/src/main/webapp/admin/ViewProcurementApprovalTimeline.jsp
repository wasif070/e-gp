<%-- 
    Document   : ViewProcurementApprovalTimeline
    Created on : May 26, 2015, 10:47:42 AM
    Author     : Istiak (Dohatec)
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Procurement Approval Timeline</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function() {
                LoadTimeline();
            });

            function DeleteRow(id){
                jConfirm('Are You Sure You want to Delete.','Procurement Method Business Rule Configuration', function(ans) {if(ans){
                    $.post("<%=request.getContextPath()%>/ProcurementTimelineServlet", {action: "DeleteRows", rowNo: id, flag: "view"},  function(j){
                        if(j == "false"){
                            jAlert("Rule delete failed!!","Procurement Approval Timeline Configuration", function() {});
                        }else{
                            $('#del_'+id).closest('tr').remove();
                            jAlert("Rule delete successfully.","Procurement Approval Timeline Configuration", function() {});
                        }
                    });
                }});
            }

            function LoadTimeline(){
                $.post("<%=request.getContextPath()%>/ProcurementTimelineServlet", {action: "ViewTimeline", flag: "view"},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    sortTable();
                });
            }
        </script>        
    </head>

    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea">
                        <div class="pageHead_1">
                            Procurement Approval Timeline
                            <span style="float: right;">
                                <a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('6');">Save as PDF</a>
                            </span>
                        </div>
                      
                        <table class="tableList_1 t_space" cellspacing="0" width="100%" id="resultTable" name="resultTable" cols="@6">
                            <tr>
                                <th class="t-align-center" width="15%">Approval Authority</th>
                                <th class="t-align-center" width="15%">Procurement <br/>Category</th>
                                <th class="t-align-center" width="10%">No of Days <br/>For TC</th>
                                <th class="t-align-center" width="15%">No of Days <br/>For TEC or PEC</th>
                                <th class="t-align-center" width="15%">Total No of Days <br/>For Evaluation</th>
                                <th class="t-align-center" width="15%">No of Days <br/>For Approval</th>
                                <th class="t-align-center" width="15%">Action</th>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                    String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="ProcurementApprovalRule_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="ProcurementApprovalRule" />
            </form>
               
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script>
        var obj = document.getElementById('procAppTimeView');
        if(obj != null){
            if(obj.innerHTML == 'View'){
                obj.setAttribute('class', 'selected');
            }
        }
                
        var headSel_Obj = document.getElementById("headTabConfig");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

