<%-- 
    Document   : EditConfigClarification
    Created on : Mar 01, 2017, 5:02:22 PM
    Author     : MD. Emtazul Haque
--%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigClarification"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConfigClarificationService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Edit Clarification Days Configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            function validate(){
                $(".err").remove();
                var tbol = true;
                var len=$('#tbodyData').children().length-1;
                var cnt=0;
                var cnt2=0;
                for(var i=0;i<len;i++){
                    if($.trim($("#ClarificationDays_"+i).val()) == "") {
                        $("#ClarificationDays_"+i).parent().append("<div class='err' style='color:red;'>Please enter no. of Days</div>");
                        cnt++;
                    }
                }
                if(cnt!=0){
                    tbol = false;
                }
                if(cnt2!=0){
                    tbol = false;
                }
                if(tbol){
                    $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/ConfigClarificationServlet",
                        data:"ConfigClarificationId="+$('#ConfigClarificationId').val()+"&"+"SBDName="+$('#SBDName_0').val()+"&action=ajaxUniqueConfig",
                        async: false,
                        success: function(j){
                            if(j.toString()!="0"){
                                tbol=false;
                                jAlert("Rule for this SBD already exists.","Tender Clarification Days Rule Configuration", function(RetVal) {
                                });                                
                            }
                        }
                    });                                        
                }
                return tbol;
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <%
                    ConfigClarificationService configService = (ConfigClarificationService) AppContext.getSpringBean("ConfigClarificationService");
                    configService.setLogUserId(session.getAttribute("userId").toString());
                    List<Object[]> SBDNames = configService.getSBDList();
                    List<TblConfigClarification> getConfigClarification = configService.getTblConfigClarification("configClarificationId", Operation_enum.EQ, Integer.parseInt(request.getParameter("ConfigClarificationId")));
        %>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
         <td class="contentArea">
      
            <div class="pageHead_1">Edit Clarification Days Configuration</div>
            <form action="<%=request.getContextPath()%>/ConfigClarificationServlet?action=editSingleConfigClarification" method="post" >
                <table width="100%">
                    <tr>
                        
                        <td valign="top">
                            <table class="tableList_1 t_space" cellspacing="0" width="100%" id="members">
                                <tbody id="tbodyData">
                                    <tr>
                                        <th class="t-align-center">SBD Name</th>
                                        <th class="t-align-center">Number of days<br>before Tender closing date<br>for posting query</th>
                                    </tr>
                                    <%
                                                int cnt = 0;
                                                do {
                                    %>
                                    <tr>
                                        <td class="t-align-center">
                                            <input type="hidden" id="ConfigClarificationId" name="ConfigClarificationId" value="<%=getConfigClarification.get(cnt).getConfigClarificationId()%>">
                                            <%for (Object[] proc : SBDNames) {
                                                if (getConfigClarification.get(cnt).getTemplateId() == (Short) proc[0]) {
                                                    %>
                                                        <input type="hidden" value="<%=proc[0]%>" name="SBDName" id="SBDName_<%=cnt%>" >
                                                    <%
                                                    out.print(proc[1]);
                                                }%>
                                            <%}%>
                                        </td>
                                        
                                        <td class="t-align-center">
                                            <input type="text" value="<%out.print(getConfigClarification.get(cnt).getClarificationDays());%>" name="ClarificationDays" id="ClarificationDays_<%=cnt%>" class="formTxtBox_1">
                                        </td>
                                        
                                    </tr>
                                    <%cnt++;
                                                } while (cnt < getConfigClarification.size());%>
                                </tbody>
                                <tr>
                                    <td colspan="8" class="t-align-center"><span class="formBtn_1">

                                            <input name="button2" id="button2" value="Submit" type="submit" onclick="return validate();">
                                        </span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </form>
         </td>
            </tr>
        </table>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
</html>
