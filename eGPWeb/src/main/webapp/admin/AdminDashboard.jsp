<%-- 
    Document   : AdminDashboard
    Created on : Oct 25, 2010, 10:51:28 AM
    Author     : parag
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title id="adminTitle">e-GP Configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>

    </head>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Middle Content Table Start-->
            <%
                StringBuilder userType = new StringBuilder();
                if(request.getParameter("userType")!=null){
                    if(!"".equalsIgnoreCase(request.getParameter("userType"))){
                        userType.append(request.getParameter("userType"));
                    }
                    else {
                        userType.append("org");
                    }
                }else {
                    userType.append("org");
                }

                String UType = "";
                Object objUserType = "";
                if (request.getParameter("hiddenUserType") != null) {
                    if (!"".equalsIgnoreCase(request.getParameter("hiddenUserType"))) {
                        UType = request.getParameter("hiddenUserType");
                    }
                    else {
                        objUserType = session.getAttribute("hiddenUserType");
                        if (objUserType != null) {
                            UType = objUserType.toString();
                        }
                    }
                }
                else {
                    objUserType = session.getAttribute("hiddenUserType");
                    if (objUserType != null) {
                        UType = objUserType.toString();
                    }
                }
%>
  
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                <tr valign="top">
                    <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <!--Page Content Start-->
                        <%
                        if(UType.equals("config")){
                        %>
                        <td class="contentArea" style="vertical-align: top;" valign="middle">
                        <div class="pageHead_1">Configuration Dash Board</div>
                        <%--<table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
            <tr>
              <td class="aboutUs_title" style="padding-left:2px;"> For all the users of e-GP System, the following Disclaimer and Privacy Policy shall be applied: </td>
            </tr>
            <tr valign="top">
              <td><div class="listBox_disclaimer" style="padding-right:0px;">
                  <div class="txt_1 t_space"> Configuration allows eGP Admin to configure the Key governing business rules in eGP for the following modules. </div>
                  <ul>
                    <li>Work Flow (Change to Work Flow Configuration)</li>
                    <li>Procurement Method (Change to Procurement Method Rules)</li>
                    <li>Pre Tender Meeting (Change to Pre Tender Meeting Rules)</li>
                    <li>Amendment (Change to Amendment Rules)</li>
                    <li>Opening Committee Formation (Change to TOC/POC Formation Rules)</li>
                    <li>Evaluation Committee Formation (Change to TEC/PEC Formation Rules)</li>
                    <li>TSC Formation (Change to TSC Formation Rules)</li>
                    <li>STD Selection (Change to STD Selection Rules)</li>
                    <li>Financial Year Configuration</li>
                    <li>Configure Document Size  (Change to Document Size Configuration)</li>
                  </ul>
                </div></td>
            </tr>
          </table>--%>

                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
            <%--<tr>
              <td class="aboutUs_title" style="padding-left:2px;"> On the first page show the following text instead of welcome message. </td>
            </tr>--%>
            <tr valign="top">
              <td><div class="listBox_disclaimer" style="padding-right:0px;">
                  <div class="txt_1 t_space"> As e-GP Admin, you can configure the key Tender Process business rules using the following options; </div>
                  <ul>
                    <!--<li> <strong>Work Flow Configuration</strong><br />-->
                      <!--Using this option you can configure Organization & module specific work flow for acceptance, rejection of work using 4 key functions Create/Review/Accept/Reject a file </li>-->
                    <li> <strong> Procurement Method Rules</strong><br />
                      Using this option you can configure Procurement rules using 10 key parameters like Budget, Area, Tender Type, Procurement Method, NCB/ICB, Procurement Category, Min. & Max. Tender value, Time for Tender Submission & No. of days required for Re-Tender. </li>
                    <li> <strong>Pre Bid Meeting Rules</strong><br />
                      Using this option you can configure as for which all types of procurement, Pre Bid Meeting is allowed, No. of days required & days required for uploading MoM of Pre Bid Meeting. </li>
                    <li> <strong>Amendment Rules</strong><br />
                      Using this option you can configure as when Amendments can be published, minimum days required for different types of procurement. </li>
                    <li> <strong>TOC Formation Rules</strong><br />
                      Using this option you can configure as how many Members are required for TOC, Min. No. of member from same PA Office, Min. No. of members from TC for different types of procurement. </li>
                    <li> <strong>TEC Formation Rules</strong><br />
                      Using this option you can configure as how many Members are required for TEC, Min. No. of member from outside PA Office & Min. No. of member from same PA Office for different types of procurement. </li>

                      <li> <strong>TC Formation Rules</strong><br />
                      Using this option you can configure as how many Members are required for TC,  Min. No. of member from outside PA Office & Min. No. of member from same PA Office for different types of procurement. </li>

                      <li> <strong>SBD Selection Rules</strong><br />
                      This option allows you to configure as which SBD should be used for a given Estimated Procurement Value for different types of procurement & conditions. </li>
                      
                      <li> <strong>Letter of Acceptance (LOA) Rules</strong><br />
                          
                      Using this option you can configure No. of days allowed for Letter of Acceptance, Performance security submission and for contract singing for different types of Tender. No. of Days between LoI and Letter of Acceptance (LOA) is also configured in this option.</li>
                     
                      <li> <strong>Evaluation Method</strong><br />
                      Using this option you can configure No. of Envelopes required, Evaluation method in different types of tenders.</li>
                      
                      <li> <strong>Clarification Days</strong><br />
                      Using this option you can configure No. of days required before Tender closing date for posting query in different types of SBDs.</li>

                      <li> <strong>Financial Year Configuration</strong><br />
                      This functionality allows you to define the Financial year & Default Financial year to be used throughout the system. </li>
                      
                      <li> <strong>APP Revise Date Configuration</strong><br />
                      This functionality allows you to define the Maximum APP Revise Count for a given Financial year and/or between Start Date & End Date in the system. </li>
                      
                      <li> <strong>Holiday Configuration</strong><br />
                      This option allows you to configure the Holiday in the system for a given date with description.  By default Saturday and Sunday have been marked for Weekend holidays during which no events can be scheduled.</li>
                      <li> <strong>Document Size Configuration</strong><br />
                      This options allows you to configure the Size of Document, Type of Document & as who can upload it i.e. Bidder/Consultant, Procuring Agency. Further you can specify upper cap of single file & max size allocated for Bidder/Consultant Briefcase.</li>
                      <%--<li> <strong>Tender Payment</strong><br />
                      Using this option you can configure in which type of Tender document fees, Tender security, performance security and tender validity is required or not.</li>--%>
                      <li> <strong>BSR Configuration</strong><br />
                      This options allows you to configure BSR with Category, Sub Category, Code, Description and Unit.</li>
                    
                  </ul>
                </div></td>
            </tr>
          </table>
                        </td>
                    <%
                        }else{
                    %>
                        <td class="contentArea_1" style="vertical-align: top; height: 400px;" align="center" valign="middle">
                           <div style="width:50%; padding:20px;border: 1px solid #FF9326" >                        
                        <h1> WELCOME </h1><br/>
                        
                        <p style="line-height:20px; font-size: 14px;"> Electronic Government Procurement (e-GP) System<br/>
                        Government Procurement and Property Management Division (GPPMD)<br/>
                            Department of National Properties<br/>
                            Ministry of Finance<br/>
                            Royal Government of Bhutan<br/>

                       </p>
                    
                    </div>
                        </td>
                    <%
                        }
                    %>
                </tr>
            </table>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
           <%
                if(userTypeId == 1 || userTypeId == 4){
                        if(UType.equals("content")){
                        %>

                    <script>
                        var headSel_Obj = document.getElementById("headTabContent");
                        if(headSel_Obj != null){
                            headSel_Obj.setAttribute("class", "selected");
                            //document.getElementById('adminTitle').innerHTML='e-GP Content';
                            document.title = 'e-GP Content'
                        }
                    </script>

           <%           }else if(UType.equals("config")){

               %>

                    <script>
                        var headSel_Obj = document.getElementById("headTabConfig");
                        if(headSel_Obj != null){
                            headSel_Obj.setAttribute("class", "selected");
                        }
                    </script>

           <%
                        }else if(UType.equals("manageDebar")){

               %>

                    <script>
                        var headSel_Obj = document.getElementById("headTabDebar");
                        if(headSel_Obj != null){
                            headSel_Obj.setAttribute("class", "selected");
                        }
                    </script>

           <%
                        }

                        else {
                        %>
                    <script>
                        var headSel_Obj = document.getElementById("headTabMngUser");
                        if(headSel_Obj != null){
                            headSel_Obj.setAttribute("class", "selected");
                        }
                    </script>
           <%
                        }
                    }

                    if(userTypeId == 6 || userTypeId == 5 || userTypeId == 7){
                        if(UType.equals("content")){
                        %>

                    <script>
                        var headSel_Obj = document.getElementById("headTabContent");
                        if(headSel_Obj != null){
                            headSel_Obj.setAttribute("class", "selected");
                            //document.getElementById('adminTitle').innerHTML='e-GP Content';
                            document.title = 'e-GP Content'
                        }
                    </script>

           <%           }else{
            %>
                    <script>
                        var headSel_Obj = document.getElementById("headTabMngUser");
                        if(headSel_Obj != null){
                            headSel_Obj.setAttribute("class", "selected");
                        }
                    </script>
            <%
                       }
                    }

            %>
    </body>
      
</html>
