<%-- 
    Document   : ChangeHintQusAns
    Created on : Jan 13, 2011, 5:51:45 PM
    Author     : parag
--%>

<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.web.servicebean.LoginMasterSrBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>--%>
<%
String  str= request.getContextPath();
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");

        boolean isAfterLoginTop=false;

        if(request.getParameter("fromWhere") != null){
            if("MyAccount".equalsIgnoreCase(request.getParameter("fromWhere"))){
                isAfterLoginTop=true;
            }
        }
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Change Hint Question and Answer</title>
         <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
          <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
          <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
          <script type="text/javascript">

//     $(document).ready(function() {
//        $("#frmChangePass").validate({
//            rules: {
//                HintQuestion:{required:true},
//                OtherHint:{required:true},
//                HintAns:{required:true,maxlength:50}
//            },
//            messages: {
//               HintQuestion:{required:"<div class='reqF_1'>Please select Hint Question.</div>"
//                            },
//               OtherHint:{required:"<div class='reqF_1'>Please enter your own Hint Question.</div>"
//                        },
//
//               HintAns:{required:"<div class='reqF_1'>Please enter Hint Answer.</div>",
//                        maxlength:"<div class='reqF_1'>Maximum 50 characters are allowed.</div>"}
//
//            }
//        });
//    });

function validateQue(){
        var spaceTest = /^\s+|\s+$/g
        document.getElementById("spnHintAns").innerHTML = "";
        document.getElementById("spnOtherHint").innerHTML = "";
        var valid = true;
        var spaceTest = /^\s+|\s+$/g
        var Hintque = document.getElementById("cmbHintQuestion").value;
        if(Hintque=="other"){
            if(document.getElementById("txtOtherHint").value==""){
                document.getElementById("spnOtherHint").innerHTML = "Please enter your own Hint Question.";
                valid = false;
            }else{
                if(spaceTest.test($('#txtOtherHint').val())){
                  document.getElementById("spnOtherHint").innerHTML = "Space is not allowed";
                  valid = false;
            }
            }
            if(document.getElementById("txtHintAns").value==""){
                document.getElementById("spnHintAns").innerHTML = "Please enter Hint Answer.";
                valid = false;
            }else{
                if(spaceTest.test($('#txtHintAns').val())){
                  document.getElementById("spnHintAns").innerHTML = "Space is not allowed";
                  valid = false;
            }
            }
        }else{
            if(document.getElementById("txtHintAns").value==""){
                document.getElementById("spnHintAns").innerHTML = "Please enter Hint Answer.";
                valid = false;
            }else{
                //alert(spaceTest.test($('#txtHintAns').val()));
                if(spaceTest.test($('#txtHintAns').val())){
                  document.getElementById("spnHintAns").innerHTML = "Space is not allowed";
                  valid = false;
            }
        }
        }
        return valid;
}
 </script>
  </head>
<%
    String userId="0";
    if(session.getAttribute("userId")!=null){
        userId=session.getAttribute("userId").toString();
    }

    LoginMasterSrBean loginMasterSrBean = new LoginMasterSrBean(userId);
    userId=null;
%>

    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%if(isAfterLoginTop){%>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
               <%}else{%>
               <jsp:include page="../resources/common/Top.jsp" />
               <%}%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td><div class="pageHead_1">Change Hint Question and Answer</div>
                                        <form id="frmChangePass" name="ChangePassword" method="post" action='<%=str%>/LoginSrBean?action=checkHintQusAns'>
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">

                                                <tr>
                                                    <td width="10%" class="ff">Hint Question : <span>*</span></td>
                                                    <td width="90%">
                                                        <select class="formTxtBox_1" name="HintQuestion" id="cmbHintQuestion">
                                                            <%for(SelectItem hintQus : loginMasterSrBean.getHintQueList()){%>
                                                                <option value="<%=hintQus.getObjectId()%>"><%=hintQus.getObjectValue()%></option>
                                                            <%}%>                                                            
                                                                 <!--<option value="other">Create your own Hint Question</option>-->
                                                        </select>
                                                    </td>
                                                </tr>

                                                <tr id="hintQueTr" style="display:none;">
                                                    <td class="ff">Create your own Hint Question : <span>*</span></td>
                                                    <td>
                                                        <textarea cols="50" rows="3" id="txtOtherHint" name="OtherHint" class="formTxtBox_1"></textarea>
                                                        <span id="spnOtherHint" class="reqF_1"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Hint Answer : <span>*</span></td>
                                                    <td>
                                                        <input class="formTxtBox_1" type="text" id="txtHintAns" name="HintAns" />
                                                        <span id="spnHintAns" class="reqF_1"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td align="left">
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="Add" id="btnAdd" value="Submit" onclick="return validateQue();" /></label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMyAcc");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<%loginMasterSrBean=null;%>
<!--<script type="text/javascript">
      $(function() {
          $('#cmbHintQuestion').bind("change",function(){
              if($('#cmbHintQuestion').val() == 'other' ){
                  $('#hintQueTr').show();
              }
              else{
                  $('#hintQueTr').hide();
              }
          });
      });      
 </script>-->
