<%-- 
    Document   : ConfigureDFY
    Created on : Oct 30, 2010, 7:57:50 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Register Organization Admin</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("hdnUserType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                            userType.append(request.getParameter("hdnUserType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1">
                                        <div class="pageHead_1">Configure Financial Year details</div>
                                        <div>&nbsp;</div>
                                        <div id="action-tray">
                                            <ul>
                                                <li><a href="#" class="action-button-goback">Go Back</a></li>
                                            </ul>
                                        </div>
                                        <div>&nbsp;</div>
                                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                            <tr>
                                                <td class="ff">Financial Year : </td>
                                                <td>
                                                    <select name="select" class="formTxtBox_1" id="select" style="width:100px;">
                                                        <option selected="selected">2010-2011</option>
                                                        <option>2009-2010</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Is default Financial Year? : </td>
                                                <td class="txt_2"><input type="checkbox" name="checkbox" id="checkbox" /></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td><label class="formBtn_1">
                                                        <input type="submit" name="button" id="button" value="Submit" />
                                                    </label>       </td>
                                            </tr>
                                        </table>
                                        <div>&nbsp;</div>

                                        <!--Page Content End-->
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
