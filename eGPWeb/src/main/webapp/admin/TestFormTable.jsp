<%--
    Document   : TestFormTable
    Created on : Jan 09, 2011, 12:00:21 PM
    Author     : yanki
--%>

<%@page import="com.cptu.egp.eps.model.table.TblListCellDetail"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblListBoxMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ComboSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblListBoxDetail"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonFormData"%>
<%@page import="java.util.ListIterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateCells"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateColumns"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateFormulas"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="frmView"  class="com.cptu.egp.eps.web.servicebean.TemplateTableSrBean" />
<table width="100%" cellspacing="0" class="tableList_1" id="FormMatrix">
<%
    int isTotal = 0;
    int FormulaCount = 0;
    int cols = 0;
    int rows = 0;
    int tableId = 0;
    int showOrHide = 0;

    int arrFormulaColid[] = null;
    int arrWordFormulaColid[] = null;
    int arrOriValColId[] = null;

    short columnId = 0;
    short filledBy = 0;
    short dataType = 0;

    String colHeader = "";
    String colType = "";
    String tableIndex = "1";
    String inputtype = "";
    String textAreaValue = "";

    if(request.getParameter("tableId")!=null && !"".equalsIgnoreCase(request.getParameter("tableId"))){
        tableId = Integer.parseInt(request.getParameter("tableId"));
    }
    if(request.getParameter("cols")!=null && !"".equalsIgnoreCase(request.getParameter("cols"))){
        cols = Integer.parseInt(request.getParameter("cols"));
    }

    if(request.getParameter("rows")!=null && !"".equalsIgnoreCase(request.getParameter("rows"))){
        rows = Integer.parseInt(request.getParameter("rows"));
    }

    if(request.getParameter("TableIndex")!=null && !"".equalsIgnoreCase(request.getParameter("TableIndex"))){
        tableIndex = request.getParameter("TableIndex");
    }

    int bidId = 0;
    if (request.getParameter("bidId") != null) {
        bidId = Integer.parseInt(request.getParameter("bidId"));
    }

    String logUserId="0";
    if(session.getAttribute("userId")!=null){
        logUserId=session.getAttribute("userId").toString();
    }
    frmView.setLogUserId(logUserId);

    String action = "";
    boolean isMultiTable = false;
    if(request.getParameter("isMultiTable")!=null && !"".equalsIgnoreCase(request.getParameter("isMultiTable"))){
        if("yes".equalsIgnoreCase(request.getParameter("isMultiTable"))){
            isMultiTable = true;
        }
    }
%>
        <tbody id="MainTable<%=tableId%>">
            <input type="hidden" name="tableCount<%=tableId%>" value="1"  id="tableCount<%=tableId%>">
            <input type="hidden" name="originalTableCount<%=tableId%>" value="1"  id="originalTableCount<%=tableId%>">
            <script>
                var lbl_ids=new Array();
                var lbl_countid=0;
            </script>
<%
        short fillBy[] = new short[cols];
        int arrColId[] = new int[cols];
        int cellId = 0;
        arrFormulaColid = new int[cols];
        arrWordFormulaColid = new int[cols];
        arrOriValColId = new int[cols];
        for(int i=0;i<arrFormulaColid.length;i++)
        {
                arrFormulaColid[i] = 0;
                arrWordFormulaColid[i] = 0;
                arrOriValColId[i] = 0;
        }

        java.util.ListIterator<com.cptu.egp.eps.model.table.TblTemplateColumns> tblColumnsDtl = frmView.getColumnsDtls(tableId, true).listIterator();
        java.util.ListIterator<com.cptu.egp.eps.model.table.TblTemplateCells> tblCellsDtl = frmView.getCellsDtlsTestForm(tableId).listIterator();
        java.util.ListIterator<com.cptu.egp.eps.model.table.TblTemplateFormulas> tblFormulaDtl = frmView.getTableFormulas(tableId).listIterator();
        //ListIterator<CommonFormData> tblColumnsDtl = tenderBidSrBean.getTableColumns(tableId, true).listIterator();
        //ListIterator<CommonFormData> tblCellsDtl = tenderBidSrBean.getTableCells(tableId).listIterator();
        //ListIterator<CommonFormData> tblFormulaDtl = tenderBidSrBean.getTableFormulas(tableId).listIterator();

        int cntRow = 0;
        int TotalBidCount=0;
        int editedRowCount = 0;
        StringBuilder bidTableIds = new StringBuilder();

        cntRow = 1;
%>
            <script>
		var rowcount = parseInt("<%=rows%>")*parseInt("<%=cntRow%>");
		arrCompType[<%=tableIndex%>] = new Array(rowcount);
		arrCellData[<%=tableIndex%>] = new Array(rowcount);
		arrDataTypesforCell[<%=tableIndex%>] = new Array(rowcount);
                for (i=1;i<=rowcount;i++)
		{
			arrCompType[<%=tableIndex%>][i]=new Array(<%=cols%>);
			arrCellData[<%=tableIndex%>][i]=new Array(<%=cols%>);
			arrDataTypesforCell[<%=tableIndex%>][i]=new Array(<%=cols%>);
		}

		if(rowcount == 1)
		{
			i = <%=cols%>;
		}
                
                arrColFillBy[<%=tableIndex%>] = new Array(<%=cols%>);
            </script>
        <%
        if(true){
            int cnt = 0;
            int counter = 0;
            while(tblFormulaDtl.hasNext()){
                TblTemplateFormulas formulaData = tblFormulaDtl.next();
                if(formulaData.getFormula().contains("TOTAL")){
                    isTotal = 1;
                    arrFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
%>
                    <script>
                        isColTotalforTable[<%=tableIndex%>] = 1;
                        for(var i=1;i<=<%=cols%>;i++){
                            if(i == <%=formulaData.getColumnId()%>){
                                arrColTotalIds[<%=tableIndex%>][i-1] = '<%=formulaData.getColumnId()%>';
                            }
                        }
                    </script>
<%
                }
                cnt++;
                FormulaCount = cnt;
            }

            /*for(int x=0;x<arrFormulaColid.length;x++){
                out.println(" ==================  " + arrFormulaColid[x]);
            }*/
%>
            <script>
                    var totalWordArr = new Array();
                    var q = 0;
            </script>
<%
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }

            while(tblFormulaDtl.hasNext()){
                TblTemplateFormulas formulaData = tblFormulaDtl.next();
                for(int x=0;x<arrFormulaColid.length;x++){
                    if(formulaData.getFormula().contains("WORD("+arrFormulaColid[x]+")")){
%>
                    <script>
                        var TotalWordColId = "<%=formulaData.getColumnId()%>";
                        totalWordArr[q] = "<%=formulaData.getColumnId()%>";
                        q++;
                        arrColTotalWordsIds[<%=tableIndex%>][<%=x%>] = '<%=formulaData.getColumnId()%>';
                    </script>
<%
			arrWordFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                    }
                }
            }
%>
            <script>
                arrTableFormula[<%=tableIndex%>] = new Array(<%=FormulaCount%>); // Stores Formula
                arrFormulaFor[<%=tableIndex%>] = new Array(<%=FormulaCount%>); // Stores where to Apply Formula
                arrIds[<%=tableIndex%>] = new Array(<%=FormulaCount%>);
            </script>
<%
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }
            while(tblFormulaDtl.hasNext()){
                TblTemplateFormulas formulaData = tblFormulaDtl.next();
%>
		<script>
                    arrTableFormula[<%=tableIndex%>][<%=counter%>] = '<%=formulaData.getFormula()%>';
                    arrFormulaFor[<%=tableIndex%>][<%=counter%>] = '<%=formulaData.getColumnId()%>';
		</script>
<%
                counter++;
            }
%>
            <script language="JavaScript" >
                arrColIds[<%=tableIndex%>] =new Array();
                arrStaticColIds[<%=tableIndex%>] =new Array();
                arrForLabelDisp[<%=tableIndex%>] = new Array();
            </script>
<%
        }else{
            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }
            while(tblFormulaDtl.hasNext()){
                TblTemplateFormulas formulaData = tblFormulaDtl.next();
                if(formulaData.getFormula().contains("TOTAL")){
                    isTotal = 1;
                    arrFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                }
            }

            while(tblFormulaDtl.hasPrevious()){
                tblFormulaDtl.previous();
            }
            while(tblFormulaDtl.hasNext()){
                TblTemplateFormulas formulaData = tblFormulaDtl.next();
                for(int x=0;x<arrFormulaColid.length;x++){
                    if(formulaData.getFormula().contains("WORD("+arrFormulaColid[x]+")")){
			arrWordFormulaColid[formulaData.getColumnId() - 1] = formulaData.getColumnId();
                    }
                }
            }
        }

        if(true) // Form Add Start
        {
            int colId = 0;
            for (short i = -1; i <= rows; i++) {
                if(i == 0){
%>
             <tr id="ColumnRow">
<%
                    for(short j=0;j<cols;j++){
                        if(tblColumnsDtl.hasNext()){
                            TblTemplateColumns colData = tblColumnsDtl.next();

                            colHeader = colData.getColumnHeader();
                            colType = colData.getColumnType();
                            filledBy = colData.getFilledBy();
                            showOrHide = Integer.parseInt(colData.getShoworhide());
                            dataType = colData.getDataType();
                            colId = colData.getColumnId();
                            colData = null;
                        }
                        fillBy[j] = filledBy;
                        arrColId[j] = colId;
%>
                <th id="addTD<%= j + 1 %>">
                    <%= colHeader %>
                    <%if(showOrHide==2){%>
                    <script>
                        document.getElementById("addTD<%= j + 1 %>").visibility = "collapse";
                    </script>
                    <%}%>
                    <script>
                       arrColFillBy[<%=tableIndex%>] [<%=colId%>]=<%=filledBy%>;
                    </script>
                </th>
<%
                    }
%>
             </tr>
<%
                }
                if(i > 0){
%>
             <tr id="row<%=tableId%>_<%=i%>">
<%
                    int cnt = 0;
                    for(int j=0; j<cols; j++){
                        String cellValue = "";
%>
                <td id="td<%=tableId%>_<%=i%>_<%=columnId%>" align="center"
                <%if(showOrHide==2){%>
                    <script>
                        document.getElementById("td<%=tableId%>_<%=i%>_<%=columnId%>").visibility = "collapse";
                    </script>
                <%}%>
                >
<%
                        if(tblCellsDtl.hasNext()){
                            cnt++;

                            TblTemplateCells cellData = tblCellsDtl.next();
                            dataType = cellData.getCellDatatype();
                            cellId = cellData.getCellId();
                            //filledBy = cellData.getCellDatatype();
                            //filledBy = cellData.getFillBy();
                            cellValue = cellData.getCellvalue();
                            columnId = cellData.getColumnId();


                            if(true){
                                String cValue = ""+cellValue.trim();
                                cValue = cellValue.replaceAll("\r\n", "<br />");
                                cValue = cValue.replaceAll("\n\r", "<br />");
                                cValue = cValue.replaceAll("\r", "<br />");
                                cValue = cValue.replaceAll("\n", "<br />");
%>

                            <script>
                                arrCellData[<%=tableIndex%>][<%=i%>][<%=columnId%>] = '<%=cValue%>';
                                arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = '';
                             </script>
<%
                            }
                            if(fillBy[j] == 1){ // Filled By PE User

%>
                                   <label  name="lbl@<%=tableId%>_<%=i%>_<%=columnId%>" id="lbl@<%=tableId%>_<%=i%>_<%=columnId%>"
                                                <%if(dataType==3 || dataType==4 || dataType==8 || dataType== 13 ){%>
                                                value="<%=cellValue%>"
                                                <%}else{%>
                                                value=""
                                                <%}%>
                                                >
                                                <%=cellValue%>
                                        </label>
                                <script>
                                    lbl_ids[lbl_countid]="lbl@<%=tableId%>_<%=i%>_<%=columnId%>";
                                    lbl_countid++;
				</script>
<%
                                if(true){
%>
                                <script>
                                    arrStaticColIds[<%=tableIndex%>].push('<%=columnId%>');
				</script>
<%
                               }
                            } else if(fillBy[j] == 2 || fillBy[j] == 3){ // Filled by Tenderer or Auto
                                if(dataType != 2){
                                    if(i == (rows)){ // When displaying last row
                                        if(FormulaCount>0){
                                            if(isTotal == 1){
                                                if(arrFormulaColid[columnId-1] != columnId){
                                                    if(arrWordFormulaColid[columnId-1] == columnId){
                                                        if(fillBy[j] == 3 && dataType == 3){ // Filled By Auto and DataType - Money Posititive
%>
                                                        <label
								readonly
								tabindex="-1"
								name="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>"
								id="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>"
                                                                title="label" value="<%=textAreaValue%>">
                                                        </label>
                                                        <!--<textarea style="display:none" class="formTxtBox_1"
                                                                readonly
								tabindex="-1"
								cols="30"
								name="row<%=tableId%>_<%=i%>_<%=columnId%>"
								id="row<%=tableId%>_<%=i%>_<%=columnId%>"
							><%//=textAreaValue%></textarea>-->
<%
                                                        }else {
%>
                                                        <textarea class="formTxtBox_1"
                                                            readonly
                                                            style="width: 98%;"
                                                            tabindex="-1"
                                                            cols="30"
                                                            name="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                                            id="row<%=tableId%>_<%=i%>_<%=columnId%>"
                                                        ><%=textAreaValue%></textarea>
<%
                                                        }
                                                    }
                                                    out.print("</td>");
                                                    continue;
                                                 }else{
                                                    inputtype = "text";
                                                 }
                                            }
                                        }
                                    }
                                    //out.print("FillBy : "+fillBy[j]);
                                    //out.print("  Datatype : "+dataType);
                                   if(i != (rows))
                                   {
%>
                                       <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1 newTestFormWidth"
                                        <%if(fillBy[j] == 3){ // Filled By Auto%>
                                        readOnly style="width: 98%;"
                                        <%}%>
<%
                                        if(fillBy[j] != 3){
                                            if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                            }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                            } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                            } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                            }else if(dataType == 1 || dataType == 2){
%>
                                         style="width: 98%;"
<%
                                            }else if(dataType==9 || dataType==10){
%>
                                         style="display: none;"
<%
                                            }
                                            else if(dataType==12){
%>
style="width: 80%;/*height: 20px;*/" readonly="true" onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                            }
                                            else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                            }
                                         }
%>
                                      />

<%
                                            if(fillBy[j] != 3 && dataType==12){
%>
                                         &nbsp;<a onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' title='Calender'><img src='../resources/images/Dashboard/calendarIcn.png' onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','imgrow<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name ='calendarIcn'  alt='Select a Date' border='0' style='vertical-align:middle;'/></a>
<%
                                            }
                                            if(dataType==9 || dataType==10){
                                                List<TblListCellDetail> listCellDetail = new ArrayList<TblListCellDetail>();
                                                ComboSrBean cmbSrBean = new ComboSrBean();
                                                listCellDetail = frmView.getListCellDetail(tableId,columnId,cellId);
                                                if(listCellDetail.size() > 0){
                                                TblListBoxMaster tblListBoxMaster = listCellDetail.get(0).getTblListBoxMaster();
                                                List<TblListBoxDetail> listBoxDetail = cmbSrBean.getListBoxDetail(tblListBoxMaster.getListBoxId());
                                                 %>
                                                 <select id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1" onchange="changeTextVal(this,'row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>');" style="width: 98%;" >
                                                    <!--option value="">--Select--</option-->
                                                    <% for(TblListBoxDetail tblListBoxDetail:listBoxDetail){ %>
                                                        <option value="<%=tblListBoxDetail.getItemValue()%>"><%=tblListBoxDetail.getItemText()%></option>
                                                    <% } %>
                                                </select>
<%                                             }
                                            }
                                   }
                                   else if(i == (rows))
                                   {
                                        if(FormulaCount > 0)
                                        {
                                            if(isTotal == 1)
                                            {
                                                if(arrFormulaColid[columnId-1] == columnId)
                                                {
%>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly style="width: 98%;"
                                        <%}%>
<%
                                                    if(fillBy[j] != 3){
                                                        if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                        }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                        } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                        } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                }else if(dataType == 1 || dataType == 2){
%>
                                                    style="width: 98%;"
<%
                                                }
                                                else if(dataType==12){
%>
                                         style="width: 80%;height: 20px;" readonly="true" onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                }
                                                else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                            }
                                         }
                                                    
%>
                                        />
<%
                                                if(fillBy[j] != 3 && dataType==12){
%>
                                                    &nbsp;<a onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' title='Calender'><img src='../resources/images/Dashboard/calendarIcn.png' onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','imgrow<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name ='calendarIcn'  alt='Select a Date' border='0' style='vertical-align:middle;'/></a>
<%
                                                }
                                            
                                                }
                                            }else{
%>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1 newTestFormWidth"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly style="width: 98%;"
                                        <%}%>
<%
                                                if(fillBy[j] != 3){
                                                    if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                    }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                    } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                             } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                    }else if(dataType == 1 || dataType == 2){
%>
                                                    style="width: 98%;"
<%
                                                }
                                                     if(dataType==9 || dataType==10){
 %>
                                        style="display: none;"
<%
                                                    }
                                                     if(dataType==12){
%>
                                         style="width: 80%;/*height: 20px;*/" readonly="true" onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                }
                                                else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                            }
                                          }     
%>
                                        />
<%
                                                if(fillBy[j] != 3 && dataType==12){
%>
                                                    &nbsp;<a onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' title='Calender'><img src='../resources/images/Dashboard/calendarIcn.png' onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','imgrow<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name ='calendarIcn'  alt='Select a Date' border='0' style='vertical-align:middle;'/></a>
<%
                                                }
                                        
                                            if(dataType==9 || dataType==10){
                                                List<TblListCellDetail> listCellDetail = new ArrayList<TblListCellDetail>();
                                                ComboSrBean cmbSrBean = new ComboSrBean();
                                                listCellDetail = frmView.getListCellDetail(tableId,columnId,cellId);
                                                if(listCellDetail.size() > 0){
                                                TblListBoxMaster tblListBoxMaster = listCellDetail.get(0).getTblListBoxMaster();
                                                List<TblListBoxDetail> listBoxDetail = cmbSrBean.getListBoxDetail(tblListBoxMaster.getListBoxId());
                                                 %>
                                                 <select id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1" onchange="changeTextVal(this,'row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>');" style="width: 98%;" >
                                                    <!--option value="">--Select--</option-->
                                                    <% for(TblListBoxDetail tblListBoxDetail:listBoxDetail){ %>
                                                        <option value="<%=tblListBoxDetail.getItemValue()%>"><%=tblListBoxDetail.getItemText()%></option>
                                                    <% } %>
                                                </select>
<%                                             }
                                            }
 %>
<%
                                            }
                                        }else{
%>
                                        <input type="text" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1 newTestFormWidth"
                                        <%if(fillBy[j] == 3){%>
                                        readOnly style="width: 98%;"
                                        <%}%>
<%
                                            if(fillBy[j] != 3){
                                                if(dataType==3){
%>
                                        onBlur="CheckFloat1('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                }else if(dataType==4){
%>
                                        onBlur="CheckNumeric('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                                } else if(dataType==8){
%>
                                        onBlur="moneywithminus('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                             } else if(dataType==11){
%>
                                        onBlur="chkFloatMinus5Plus5('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                               }else if(dataType == 1 || dataType == 2){
%>
                                                    style="width: 98%;"
<%
                                                }
                                                else if(dataType==9 || dataType==10){
 %>
                                        style="display: none;"
<%
                                                }
                                                else if(dataType==12){
%>
                                         style="width: 80%;/*height: 20px;*/" readonly="true" onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>');"
<%
                                                }
                                                else if(dataType==13){
%>
                                        onBlur="CheckNumeric3Decimal('<%=tableId%>',this,'<%=tableIndex%>');" style="width: 98%;"
<%
                                            }
                                         }
%>
                                        />
<%
                                                if(fillBy[j] != 3 && dataType==12){
%>
                                                    &nbsp;<a onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' title='Calender'><img src='../resources/images/Dashboard/calendarIcn.png' onclick="GetCal('row<%=tableId%>_<%=i%>_<%=columnId%>','imgrow<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>',this,'<%=tableIndex%>')" id='imgrow<%=tableId%>_<%=i%>_<%=columnId%>' name ='calendarIcn'  alt='Select a Date' border='0' style='vertical-align:middle;'/></a>
<%
                                                }
                                        
                                            if(dataType==9 || dataType==10){
                                                List<TblListCellDetail> listCellDetail = new ArrayList<TblListCellDetail>();
                                                ComboSrBean cmbSrBean = new ComboSrBean();
                                                listCellDetail = frmView.getListCellDetail(tableId,columnId,cellId);
                                                if(listCellDetail.size() > 0){
                                                TblListBoxMaster tblListBoxMaster = listCellDetail.get(0).getTblListBoxMaster();
                                                List<TblListBoxDetail> listBoxDetail = cmbSrBean.getListBoxDetail(tblListBoxMaster.getListBoxId());
                                                 %>
                                                 <select id="idcombodetail<%=tableId%>_<%=i%>_<%=columnId%>" name="namecombodetail<%=tableId%>_<%=i%>_<%=columnId%>" class="formTxtBox_1" onchange="changeTextVal(this,'row<%=tableId%>_<%=i%>_<%=columnId%>','<%=tableId%>');" style="width: 98%;" >
                                                    <!--option value="">--Select--</option-->
                                                    <% for(TblListBoxDetail tblListBoxDetail:listBoxDetail){ %>
                                                        <option value="<%=tblListBoxDetail.getItemValue()%>"><%=tblListBoxDetail.getItemText()%></option>
                                                    <% } %>
                                                </select>
<%                                             }
                                            }
 %>
<%
                                        }
                                   }
                           }

                                if(dataType == 2){
                                    if(i == (rows)){
                                        if(FormulaCount > 0){
                                            if(isTotal == 1){
                                                if(arrFormulaColid[columnId-1] != columnId){
                                                    out.print("</td>");
                                                    continue;
                                                }else{
                                                    inputtype = "text";
                                                }
                                            }
                                        }
                                    }

                                //out.print("cellId : "+cellData.getCellId());
                                    if(fillBy[j] == 3){
%>
                                        <label name="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>_<%=tableId%>"
                                        title="label" value="<%=textAreaValue%>">
                                        </label>
                                        <textarea cols="30" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);" readOnly class="formTxtBox_1" style="width: 98%;" ></textarea>
<%
                                        if(true){
%>
                                        <script>
					if(arrForLabelDisp[<%=tableIndex%>].length != i+1)
						arrForLabelDisp[<%=tableIndex%>].push('<%=tableId%>_<%=columnId%>');
                                        </script>
<%
                                        }
                                    }else{
%>
                                        <textarea cols="30" name="row<%=tableId%>_<%=i%>_<%=columnId%>" id="row<%=tableId%>_<%=i%>_<%=columnId%>"
					onBlur="CheckLongText(<%=tableId%>, this);" class="formTxtBox_1 newTestFormWidth" style="width: 98%;" ></textarea>
<%

                                    }
                                }
                                if(true){
%>
                                                    <script type="text/javascript" >
                                       <%if(dataType==2){%>
                                        arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "TEXTAREA";
                                       <%}else{%>
                                           arrCompType[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "TEXT";
                                       <%}%>
                                        arrDataTypesforCell[<%=tableIndex%>][<%=i%>][<%=columnId%>] = "<%=dataType%>";

                                        if(arrColIds[<%=tableIndex%>].length != i+1)
                                                arrColIds[<%=tableIndex%>].push('<%=columnId%>');

                                    </script>
<%
                                }
                            }

                        } // celldtl
%>
                </td>
<%
                    }
%>
            </tr>
<%
                } // if(i>0)
            } //for(row)
        }
%>
        </tbody>
    <div>
    <tr>
        <td colspan="<%=cols%>" style="display:none">
            <label class="formBtn_1" id="lblAddTable">
                <input type="button" name="btn<%=tableId%>" id="bttn<%=request.getParameter("tableIndex")%>" value="Add Table" onClick="AddTable(this.form,<%=tableId%>,this)"/>
            </label>
        </td>
    </tr>
    </div>
            <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }

            </script>
</table>
