<%--
    Document   : DepartmentCreation
    Created on : Oct 23, 2010, 6:31:48 PM
    Author     : rishita
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblDepartmentMaster"%>
<jsp:useBean id="deptMasterSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.DepartmentCreationSrBean"/>
<jsp:useBean id="deptDataBean" scope="request" class="com.cptu.egp.eps.web.databean.DepartmentCreationDtBean"/>
<jsp:setProperty name="deptDataBean" property="*"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>        

    <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");

        String deptType = "";
        if (!request.getParameter("deptType").equals("")) {
            deptType = request.getParameter("deptType");
        }
        int userid = 0;
        HttpSession hs = request.getSession();
        if (hs.getAttribute("userId") != null) {
            userid = Integer.parseInt(hs.getAttribute("userId").toString());
            deptMasterSrBean.setLogUserId(hs.getAttribute("userId").toString());
            deptMasterSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
        }

        if ("Submit".equals(request.getParameter("hdnbutton"))) {
            String msg = "";
            int strM = -1; // Default
            String deptName = deptDataBean.getDepartmentName().trim();
            if (strM == -1) {
                CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                msg = commonService.verifyDeptName(deptName, deptType);
                if (msg.length() > 2) {
                    strM = 1; // Already Exist
                }
            }
            if (strM == -1) {
                deptDataBean.setDepartmentName(deptDataBean.getDepartmentName().trim());
                if (deptMasterSrBean.deparementCreation(deptDataBean)) {
                    response.sendRedirect("ViewEmployee.jsp?deptid=" + deptMasterSrBean.getDeptid() + "&division=" + deptType + "&msg=create");
                } else {
                    response.sendRedirect("DepartmentCreation.jsp?deptType=" + deptType + "&msg=deptCreate");
                }

                //response.sendRedirect("ManageEmployee.jsp?deptType=" + deptType + "&msg=success");
            } else {
                response.sendRedirect("DepartmentCreation.jsp?deptType=" + deptType + "&strM=" + strM);
            }
        } else {

    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Create <%if (deptType.equalsIgnoreCase("Organization")) {
                out.print("Division");
            } else if (deptType.equalsIgnoreCase("Division")) {
                out.print("Department");
            } else {
                out.print(deptType);
            }%></title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                $('#cmdDepartmentType').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmdDepartmentType').val(), funName: 'ministryC'}, function (j) {
                        $("select#cmbParentType").html(j);
                    });
                });
            });
            $(document).ready(function () {
                $.validator.addMethod(
                        "regex",
                        function (value, element, regexp) {
                            var check = false;
                            return this.optional(element) || regexp.test(value);
                        },
                        "Please check your input."
                        );
                $.validator.addMethod(
                        "regex2",
                        function (value, element, regexp) {
                            var check = false;
                            return this.optional(element) || regexp.test(value);
                        },
                        "Please check your input."
                        );
                $('#cmdDepartmentType').change(function () {
                    //alert($('#cmdDepartmentType').val());
                    //alert($('#cmbParentType').val());
                    if ($('#cmdDepartmentType').val() == 'Ministry') {
                        //alert('in');
                        $('#cmbParentType').val('1');
                        $('#cmbParentType').attr('disabled', 'disabled');
                    } else {
                        $('#cmbParentType').attr('disabled', '');
                    }
                });
                $("#frmDepartmentCreation").validate({
                    rules: {
                        txtdepartment: {required: true},
                        office: {required: true},
                        emailId: {required: true},
                        district: {required: true},
                        password: {spacevalidate: true, requiredWithoutSpace: true, maxlength: 25, minlength: 8, alphaForPassword: true},
                        //, spacevalidate: true
                        confPassword: {required: true, equalTo: "#txtPassword"},
                        //, spacevalidate: true
                        //fullName: { required: true, alphaName:true,maxlength: 100 },
                        //nationalId: { alphaNationalId:true,maxlength: 25},
                        nationalId: {required: true, number: true, minlength: 11, maxlength: 11},
                        phoneNo: {required: true, regex: /^[0-9]+-[0-9,]*[0-9]$/},
                        faxNo: {regex: /^[0-9]+-[0-9,]*[0-9]$/},
                        mobileNo: {required: true, number: true, minlength: 8, maxlength: 8},
                        postCode: {number: true, minlength: 4}
                    },
                    messages: {
                        txtdepartment: {required: "<div class='reqF_1'>Please select Organization</div>"},
                        office: {required: "<div class='reqF_1'>Please select Office</div>"},
                        emailId: {required: "<div class='reqF_1'>Please enter e-mail ID</div>"
                        },
                        district: {required: "<div class='reqF_1'>Please select Dzongkhag / District</div>"
                        },
                        password: {spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace: "<div class='reqF_1'>Please enter Password</div>",
                            alphaForPassword: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>",
                            //spacevalidate: "<div class='reqF_1'>Space is not allowed.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>",
                            minlength: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>"
                        },
                        confPassword: {required: "<div class='reqF_1'>Please re-type Password</div>",
                            equalTo: "<div class='reqF_1'>Password does not match. Please try again</div>"
                                    //spacevalidate: "<div class='reqF_1'>Space is not allowed.</div>"
                        },
                        /*fullName: { required: "<div class='reqF_1'>Please enter Full Name</div>",
                         alphaName:"<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , ' \" } { - . _) Only</div>",
                         maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>"
                         },*/

                        /*nationalId: {
                         alphaNationalId: "<div class='reqF_1'>Only special characters not allowed</div>",
                         maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>"},*/
                        nationalId: {
                            required: "<div class='reqF_1'>Please enter CID No.</div>",
                            number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                            minlength: "<div class='reqF_1'>CID should comprise of minimum 11 digits</div>",
                            maxlength: "<div class='reqF_1'>CID should comprise of maximum 11 digits</div>"},
                        phoneNo: {required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            regex: "<div class='reqF_1'>Give dash (-) after Area Code.</br>Use comma to separate alternative Phone no.</br>Do not repeat Area Code.</div>",
                        },
                        faxNo: {regex: "<div class='reqF_1'>Give dash (-) after Area Code.</br>Use comma to separate alternative Fax no.</br>Do not repeat Area Code.</div>",
                        },
                        mobileNo: {
                            required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                            number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                            minlength: "<div class='reqF_1'>Minimum 8 digits are required</div>",
                            maxlength: "<div class='reqF_1'>Allows maximum 8 digits only</div>"
                        },
                        postCode: {
                            number: "<div class='reqF_1'>Only digits (0-9) are allowed.</div>",
                            minlength: "<div class='reqF_1'>Minimum 4 digits are required.</div>"
                        }
                    },
                    errorPlacement: function (error, element)
                    {
                        if (element.attr("name") == "parentDepartmentId")
                        {
                            error.insertAfter("#imgTree");
                        } else if (element.attr("name") == "deptHirarchy")
                        {
                            error.insertAfter("#imgTree");
                        } else if (element.attr("name") == "deptHirarchyMD")
                        {
                            error.insertAfter("#imgTre");
                        } else if (element.attr("name") == "phoneNo")
                        {
                            error.insertAfter("#phno");
                        } else if (element.attr("name") == "website")
                        {
                            error.insertAfter("#websiteToolTip");
                        } else if (element.attr("name") == "faxNo")
                        {
                            error.insertAfter("#fxno");
                        } else
                            error.insertAfter(element);
                    }
                }
                );

            });
            function submitFunction()
            {
                jConfirm('Please confirm your department selection. Once department selected can not be modified', 'Alert', function (RetVal) {
                    if (RetVal) {

                    }
                });
            }
        </script>
        <script type="text/javascript">
            $(function () {
                $('#cmbCountry').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbCountry').val(), funName: 'stateCombo'}, function (j) {
                        $("select#cmbState").html(j);
                    });
                });
            });
        </script>

        <script type="text/javascript">

            $(function () {
                $('#cmbMinistry').change(function () {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId: $('#cmbMinistry').val(), funName: 'divisionCombo'}, function (j) {
                        $("select#cmbDivision").html(j);
                    });
                });
            });
        </script>

        <script type="text/javascript">

            function chkreg(value)
            {
                //Old, was not allowing single occurence of characters
                //return /^[a-zA-Z 0-9](?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9]+$)+$/.test(value);

                //following allows single occurence of characters, but not for Digits.
                //--return /^([\a-zA-Z]+([\a-zA-Z\s.]+[a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9]+[\s-.&,\(\)']+)?)+$/.test(value);
                //-- Last -- return /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/.test(value);

                //allows all sequesnes, except special characters in starting
                return /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/.test(value);
                //allows digits+char but dont allow char+digits
                //return /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z])?)+$/.test(value);
                //similar to regexp for City
                //return /^[a-zA-Z 0-9](?![0-9]+$)[a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+$/.test(value);

            }
            function chkregCity(value)
            {
                //old
                return /^[a-zA-Z 0-9](?![0-9]+$)[a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+$/.test(value);
                // /^([\a-zA-Z]+([\a-zA-Z\s.]+[\s-.&,\(\)']+)?)+$/.test(value);

                //new    return /^ ([a-zA-Z 0-9]+([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+)?)+$/.test(value);
                //return /^[a-zA-Z 0-9](?![0-9]+$)[a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+$/.test(value);

            }
            function chkregPostCode(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
            }
            var bvalidation;
            var bvalidationPostCode = true;
            var bvalidationName;
            var bvalidationCity = true;
            var bvalidationThana = true;
            $(function () {
                $('#Submit').click(function () {
                    bvalidation = true;
                    if ($('#txtDeptName').val() == '') {
                        $('#SearchValError1').html('Please enter Name of <%if (deptType.equalsIgnoreCase("Organization")) {
                                out.print("Organization");
                            } else {
                                out.print(deptType);
                            }%>');
                        bvalidation = false;
                    }
//                            if($('#txtCity').val()==''){
//                                $('#SearchValErrorCity').html('Please enter City / Town Name');
//                                bvalidation = false;
//                            }
//                            if($('#txtupJilla').val()==''){
//                                $('#SearchValErrorThana').html('Please enter Thana / Upazilla Name');
//                                bvalidation = false;
//                            }
//                            if($('#txtPostCode').val()==''){
//                                //$('#SearchValErrorPostCode').html('Please enter Postcode');
//                                bvalidation = true;
//                            }
                });
//                        $('#txtPostCode').blur(function() {
//                            bvalidationPostCode = true;
//                            if($('#txtPostCode').val()==''){
//                                //$('#SearchValErrorPostCode').html('Please enter Postcode');
//                                bvalidationPostCode = true;
//                            }else if(!chkregPostCode($('#txtPostCode').val())){
//                                $('#SearchValErrorPostCode').html('Please enter digits (0-9) only');
//                                bvalidationPostCode = false;
//                            }else if($('#txtPostCode').val().length > 4 || $('#txtPostCode').val().length < 4){
//                                $('#SearchValErrorPostCode').html('Postcode should comprise of 4 digits only');
//                                bvalidationPostCode = false;
//                            }else{
//                                $('#SearchValErrorPostCode').html('');
//                            }
//                        });
//                        $('#txtCity').blur(function() {
//                            bvalidationCity = true;
//                            if($('#txtCity').val()==''){
//                                $('#SearchValErrorCity').html('Please enter City / Town Name');
//                                bvalidationCity = false;
//                            }else if(document.getElementById('txtCity').value.charAt(0) == " "){
//                                $('#SearchValErrorCity').html('Only Space is not allowed');
//                                bvalidationCity = false;
//                            }else if(!chkregCity($('#txtCity').val())){
//                                $('#SearchValErrorCity').html('Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' " } { - . _) Only');
//                                bvalidationCity = false;
//                            }else if($('#txtCity').val().length > 100){
//                                $('#SearchValErrorCity').html('Allows maximum 100 characters only');
//                                bvalidationCity = false;
//                            }else{
//                                $('#SearchValErrorCity').html('');
//                            }
//                        });
//                        $('#txtupJilla').blur(function() {
//                            bvalidationThana = true;
//                            if($('#txtupJilla').val()==''){
//                                $('#SearchValErrorThana').html('Please enter Thana / Upazilla Name');
//                                bvalidationThana = false;
//                            }else if(document.getElementById('txtupJilla').value.charAt(0) == " "){
//                                $('#SearchValErrorThana').html('Only Space is not allowed');
//                                bvalidationThana = false;
//                            }else if(!chkregCity($('#txtupJilla').val())){
//                                $('#SearchValErrorThana').html('Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' " } { - . _) Only');
//                                bvalidationThana = false;
//                            }else if($('#txtupJilla').val().length > 100){
//                                $('#SearchValErrorThana').html('Allows maximum 100 characters only');
//                                bvalidationThana = false;
//                            }else{
//                                $('#SearchValErrorThana').html('');
//                            }
//                        });
                $('#txtDeptName').blur(function () {
                    bvalidationName = true;
                    if ($('#txtDeptName').val() != '') {
                        var flag = document.getElementById('txtDeptName').value;
                        if (flag.charAt(0) == " ") {
                            $('#Msg').html("");
                            $('#SearchValError1').html('Only Space is not allowed');
                            bvalidationName = false;
                        } else if (!chkreg($('#txtDeptName').val())) {
                            $('#Msg').html("");
                            $('#SearchValError1').html('Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' " } { - . _) Only');
                            bvalidationName = false;
                        } else if (flag.length > 100) {
                            $('#Msg').html("");
                            $('#SearchValError1').html('Allows maximum 100 characters only');
                            bvalidationName = false;
                        } else {
                            $('#SearchValError1').html('');
                            /*}
                             if(flag.charAt(0) != " " && flag.length <= 100 && chkreg($('#txtDeptName').val())){*/
                            $('#Msg').css("color", "red");
                            $('#Msg').html("Checking for unique Name of <%if (deptType.equalsIgnoreCase("Organization")) {
                                    out.print("Organization");
                                } else {
                                    out.print(deptType);
                                }%>...");
                            $.post("<%=request.getContextPath()%>/CommonServlet", {depName: $.trim($('#txtDeptName').val()), depType: '<%=deptType%>', funName: 'verifyDeptName'},
                                    function (j)
                                    {
                                        if (j.toString().indexOf("OK", 0) != -1) {
                                            $('#Msg').css("color", "green");
                                        } else if (j.toString().indexOf("<%=deptType%>", 0) != -1) {
                                            $('#Msg').css("color", "red");
                                        }
                                        $('#Msg').html(j);
                                    });
                        }
                    } else {
                        $('#Msg').html("");
                    }
                    //}
                });
            });
        </script>
        <script type="text/javascript">
            //            $(function() {
            //                $('#frmDepartmentCreation').submit(function() {
            //
            //                });
            //            });
        </script>
        <script type="text/javascript">
            $(function () {
                $('#frmDepartmentCreation').submit(function () {
                    if (($('#Msg').html() == "OK") || ($('#Msg').html() == "Ok")) {
                        if (bvalidation == true && bvalidationName == true && bvalidationCity == true && bvalidationThana == true && bvalidationPostCode == true) {
                            if ($('#frmDepartmentCreation').valid()) {
                                if ($('#Submit') != null) {
                                    $('#Submit').attr("disabled", "true");
                                    $('#hdnbutton').val("Submit");
                                }
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                });
            });
        </script>
    </head>

    <body>
        <%

        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%  int aLvlOff = 0;
                            if ("Division".equals(deptType)) {
                                aLvlOff = 2;
                            } else if ("Organization".equals(deptType)) {
                                aLvlOff = 3;
                            } else if ("SubDistrict".equals(deptType)) {
                                aLvlOff = 4;
                            } else if ("Gewog".equals(deptType)) {
                                aLvlOff = 5;
                            }else if ("District".equals(deptType)) {
                                aLvlOff = 2;
                            }
                            StringBuilder hdnUserType = new StringBuilder();
                            if (request.getParameter("hdnUserType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                    hdnUserType.append(request.getParameter("hdnUserType"));
                                } else {
                                    hdnUserType.append("org");
                                }
                            } else {
                                hdnUserType.append("org");
                            }
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=hdnUserType%>" ></jsp:include>

                            <td class="contentArea_1">
                            <%
                                int strM = -1;
                                String msg = "";
                                if (request.getParameter("strM") != null) {
                                    try {
                                        strM = Integer.parseInt(request.getParameter("strM"));
                                    } catch (Exception ex) {
                                        strM = 2;
                                    }
                                    switch (strM) {
                                        case (1):
                                            String OrgtoPA = "";
                                            if (deptType.equalsIgnoreCase("Organization")) {
                                                OrgtoPA = "Procuring Agency";
                                            } else {
                                                OrgtoPA = deptType;
                                            }
                                            msg = OrgtoPA + " Name Already Exists";
                                            break;
                                    }
                            %>
                            <div class="responseMsg errorMsg" style="margin-left:7px; margin-right: 2px; margin-top: 15px;">
                                <%= msg%>
                            </div>
                            <%
                                    msg = null;
                                }
                            %>
                            <div class="pageHead_1">Create <% if (deptType.equalsIgnoreCase("Organization")) {
                                    out.print("Division");
                                } else if (deptType.equalsIgnoreCase("Division")) {
                                    out.print("Department");
                                } else {
                                    out.print(deptType);
                                }%></div>
                            <!--Page Content Start-->
                            <%
                                if (request.getParameter("msg") != null && request.getParameter("msg").equals("deptCreate")) {
                                    if (deptType.equalsIgnoreCase("Ministry")) {%>
                            <div class='responseMsg errorMsg'><%= appMessage.departmentCreationM%></div>
                            <% } else if (deptType.equalsIgnoreCase("Division")) {%>
                            <div class='responseMsg errorMsg'><%= appMessage.departmentCreationD%></div>
                            <% } else if (deptType.equalsIgnoreCase("Organization")) {%>
                            <div class='responseMsg errorMsg'><%= appMessage.departmentCreationO%></div>
                            <% }
                                } %>
                            <form id="frmDepartmentCreation" name="frmDepartmentCreation" method="post" action="">
                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10" width="100%">
                                    <tr align="left">
                                        <td style="font-style: italic; font-weight: normal;" colspan="2" class="ff t-align-left">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>

                                    </tr>
                                    <%

                                        if (deptType.equals("Ministry")) {

                                        } else if (deptType.equals("Division") || deptType.equals("District")) {
                                            short deptid = 1;
                                    %>
                                    <tr><td class="ff" width="200">Ministry/ Autonomus Body : <span>*</span></td>
                                        <td>
                                            <!--                                            <select class="formTxtBox_1" name="parentDepartmentId_sel" id="cmbdefaultDivision" name="cmbdefaultDivision" style="width: 405px;display: none" onchange="deptHir(this);" >
                                                                                            <option value="">--Select--</option>-->
                                            <%
                                                for (TblDepartmentMaster lst : deptMasterSrBean.getDefaultMinistryList(deptid)) {
                                            %>
<!--                                                <option value="<%= lst.getDepartmentId()%>"><%= lst.getDepartmentName()%></option>-->
                                            <%
                                                }
                                            %>
                                            <!--                                            </select>-->

                                            <input type="text" style="display: none" name="parentDepartmentId" id="parentDepartmentId" />

                                            <input type="text" name="deptHirarchy" id="deptHirarchy" class="formTxtBox_1" readonly style="width: 400px;"/>
                                            <a id="imgTree" href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?aLvlOff=<%= aLvlOff%>', '', 'width=350px,height=400px,scrollbars=1', '');">
                                                <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                            </a>
                                            <!--                                            <span id="ErrTree" class="reqF_1"></span>-->
                                        </td>
                                    </tr>
                                    <%
                                    } else if (deptType.equals("Organization") || deptType.equals("SubDistrict") || deptType.equals("Gewog")) {
                                        short deptid = 2;
                                    %>

                                    <tr>
                                        <%if (deptType.equals("SubDistrict")) {%>
                                        <td class="ff" width="200">Dzongkhag : <span>*</span></td>
                                        <%} else if (deptType.equals("Gewog")) {%>
                                        <td class="ff" width="200">Dzongkhag / Dungkhag : <span>*</span></td>
                                        <%} else {%>
                                        <td class="ff" width="200">Department : <span>*</span></td>
                                        <%}%>

                                        <td>
<!--                                            <select class="formTxtBox_1" name="parentDepartmentId_Sel" id="cmbMinistry" <%--id="cmbdefaultDivision"--%> <%--name="cmbdefaultDivision"--%> style="width: 405px;display: none" onchange="depHirch(this);">-->
                                            <!--                                                <option value="">--Select--</option>-->
                                            <%
                                                for (TblDepartmentMaster lst : deptMasterSrBean.getDefaultMinistryList(deptid)) {
                                            %>
    <!--                                                <option value="<%= lst.getDepartmentId()%>"><%= lst.getDepartmentName()%></option>-->
                                            <%
                                                }
                                            %>
                                            <!--                                            </select>-->
                                            <input type="text" style="display: none" name="parentDepartmentId" id="parentDepartmentId"/>
                                            <input type="text" name="deptHirarchyMD" readonly id="deptHirarchyMD" class="formTxtBox_1" style="width: 400px;" />
                                            <a  id="imgTre" href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?aLvlOff=<%= aLvlOff%>', '', 'width=350px,height=400px,scrollbars=1', '');">
                                                <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                            </a>
                                            <!--                                            <span id="ErrTree" class="reqF_1"></span>-->
                                        </td>
                                    </tr>
                                    <tr style="display: none">
                                        <td class="ff" width="200">Division :
                                        <td>
                                            <select class="formTxtBox_1" name="parentDepartmentId_Sel" id="cmbDivision" <%--id="cmbdefaultOrganization" name="cmbdefaultOrganization"--%> onchange="depHirch(this);" style="width: 405px; display: none">
                                                <option value="">--Select--</option>
                                                <%
                                                    //for (TblDepartmentMaster lst : deptMasterSrBean.getDefaultDivisionList()) {
                                                %>
                                                <%--<option value="<%= lst.getDepartmentId()%>"><%= lst.getDepartmentName()%></option>--%>
                                                <option value=""></option>
                                                <%
                                                    //}
                                                %>
                                            </select>
                                        </td>

                                    </tr>
                                    <% }
                                        String deptName = "";
                                        if (deptType.equalsIgnoreCase("District")) {
                                            deptName = "Dzongkhag";

                                        } else if (deptType.equalsIgnoreCase("SubDistrict")) {
                                            deptName = "Dungkhag";

                                        } else if (deptType.equalsIgnoreCase("Division")) {
                                            deptName = "Department";

                                        } else if (deptType.equalsIgnoreCase("Organization")) {
                                            deptName = "Division";

                                        } else if (deptType.equalsIgnoreCase("Autonomus")) {
                                            deptName = "Autonomus Body";

                                        } else {
                                            deptName = deptType;
                                        }
                                    %>
                                    <tr>
                                    <input type="hidden" name="hdnerrorcount" id="hdnerrorcount"/>

                                    <td class="ff" width="200">Name of <%=deptName%> : <span>*</span></td>
                                    <td><input class="formTxtBox_1" id="txtDeptName" name="departmentName" style="width: 400px;" type="text" maxlength="101"/>
                                        <div id="Msg" style="color: red; font-weight: bold"></div>
                                        <div id="SearchValError1" class="reqF_1"></div>
                                    </td>
                                    <td><input type="hidden" value="<%= deptType%>" name="departmentType" id="txtDepartmentType"/></td>
                                    </tr>
                                    <tr>
                                        <%--<td class="ff" width="200">Name of <%if (deptType.equalsIgnoreCase("Organization")) {
                                                out.print("Organization");
                                            } else {
                                                out.print(deptType);
                                            }%> in Dzongkha :</td>--%>
                                        <%--<td><input class="formTxtBox_1" id="txtDeptNameBangla" name="deptBanglaString" style="width: 400px;" type="text" maxlength="101"/></td>--%>
                                        <td><input type="hidden" name="deptParentId" id="deptParentId"/></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Address : <span>*</span></td>
                                        <td><textarea rows="5" class="formTxtBox_1" id="taAddress" name="address" style="width: 400px;" onkeypress="return imposeMaxLength(this, 1001, event);"></textarea>
                                        </td>
                                    </tr>
                                    <tr>

                                        <td class="ff" width="200">Country : </td>
                                        <td> Bhutan
                                            <select name="countryId" class="formTxtBox_1" id="cmbCountry" style="width: 205px;display:none">
                                                <%
                                                    for (SelectItem country : deptMasterSrBean.getCountryMaster()) {
                                                        if (country.getObjectValue().equals("Bhutan")) {
                                                %>
                                                <option value="<%=country.getObjectId()%>" selected="true"><%=country.getObjectValue()%></option>
                                                <%                                                                                                    } else {
                                                %>
<!--                                                <option  value="<%=country.getObjectId()%>"><%=country.getObjectValue()%></option>-->
                                                <%                                                                }
                                                    }
                                                %>
                                            </select>
                                        </td>
                                        <td><input type="hidden" value="<%=userid%>" name="approvingAuthId" id="approvingAuthId"/></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Dzongkhag / District : <span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbState" name="stateId" style="width: 205px">
                                                <%

                                                    for (SelectItem stateItem : deptMasterSrBean.getStateList()) {
                                                        if (stateItem.getObjectValue().equalsIgnoreCase("Thimphu")) {
                                                %>
                                                <option  value="<%=stateItem.getObjectId()%>" selected="selected"><%=stateItem.getObjectValue()%></option>
                                                <%
                                                } else {
                                                %>
                                                <option  value="<%=stateItem.getObjectId()%>"><%=stateItem.getObjectValue()%></option>
                                                <%  }
                                                    }%>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">City / Town : </td>
                                        <td><input class="formTxtBox_1" id="txtCity" name="city" style="width: 200px;" maxlength="101" type="text" />
                                            <div id="SearchValErrorCity" class="reqF_1"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <!--                                        <td class="ff" width="200">Thana / Upazilla : <span>*</span></td>-->
                                        <td><input type= "hidden" class="formTxtBox_1" id="txtupJilla" name="upJilla" maxlength="101" style="width: 200px;" type="text" value="Thimpu"/>
                                            <div id="SearchValErrorThana" class="reqF_1"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Post code : </td>
                                        <td><input class="formTxtBox_1" id="txtPostCode" name="postCode" style="width: 200px;" type="text" maxlength="19"/>
                                            <div id="SearchValErrorPostCode" class="reqF_1"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Phone No. : <span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtPhoneNumber" style="width: 400px;" name="phoneNo" type="text" maxlength="245"/>
                                            <span id="phno" style="color: grey;">(Area Code-Phone No e.g. 02-336962,336942,336932)</span>
                                        </td>
                                    </tr>
                                    <!--                                    <tr>
                                                                            <td class="ff" width="200">Mobile Number</td>
                                                                            <td><input class="formTxtBox_1" id="txtMobileNumber" style="width: 200px;" name="mobileNo" type="text" maxlength="16"/>
                                                                            </td>
                                                                        </tr>-->
                                    <tr>
                                        <td class="ff" width="200">Fax No. :</td>
                                        <td><input class="formTxtBox_1" id="txtFaxNumber" style="width: 400px;" name="faxNo" type="text" maxlength="245" />
                                            <span id="fxno" style="color: grey;"> (Area Code - Fax No. e.g. 02-336961,336941,336931)</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Website :</td>
                                        <td><input class="formTxtBox_1" id="txtWebsite" style="width: 200px;" name="website" type="text" maxlength="100"/>
                                            <div id="websiteToolTip" style="color: grey;">(Enter the Website name without 'http://' e.g. pppd.gov.bt/)</div>
                                            <span id="webMsg" style="color: red; ">&nbsp;</span>
                                        </td>
                                    </tr>
                                    <% if (deptType.equalsIgnoreCase("Organization")) {%>
                                    <tr>
                                        <td class="ff" width="200">Corporation Type :</td>
                                        <td><input type="radio" name="organizationType" value="yes" id="organizationType1"/> Yes
                                            <input checked="checked" type="radio" name="organizationType" value="no" id="organizationType2"/> No
                                        </td>
                                    </tr>
                                    <% }%>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td align="left"><label class="formBtn_1">
                                                <input  id="Submit" name="Submit" value="Submit" type="submit" /></label>
                                            <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <% //} %>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                </div>
            </div>

            <script>
                function deptHir(obj) {
                    document.getElementById("deptHirarchy").value = obj.options[obj.selectedIndex].text;
                    document.getElementById("deptParentId").value = obj.value;
                }
                function depHirch(obj) {

                    document.getElementById("deptHirarchy").value = obj.options[obj.selectedIndex].text;
                    document.getElementById("deptParentId").value = obj.value;
                }
            <%--function changeDepatymentTyepe(obj){
                //alert(document.getElementById('cmdDepartmentType').va);
                document.getElementById('txtDepartmentType').value = obj.value;
                document.getElementById('lblName').innerHTML = 'Name of '+ obj.value;
                document.getElementById('lblNameBangla').innerHTML = 'Name of '+ obj.value +' in Bangladesh ';
            }--%>
        </script>
        <script>
            var department = '<%=deptType%>';
            if (department == 'Ministry') {
                var obj = document.getElementById('lblMinistryCreation');
                if (obj != null) {
                    // if(obj.innerHTML == 'Create'){
                    obj.setAttribute('class', 'selected');
                    //}
                }
            } else if (department == 'Autonomus') {
                var obj = document.getElementById('lblAutonomousBodyCreation');
                if (obj != null) {
                    // if(obj.innerHTML == 'Create'){
                    obj.setAttribute('class', 'selected');
                    //}
                }
            } else if (department == 'Division') {
                var obj1 = document.getElementById('lblDivisionCreation');
                if (obj1 != null) {
                    //if(obj1.innerHTML == 'Create'){
                    obj1.setAttribute('class', 'selected');
                    //}
                }
            } else if (department == 'Organization') {
                var obj2 = document.getElementById('lblOrganizationCreation');
                if (obj2 != null) {
                    // if(obj2.innerHTML == 'Create'){
                    obj2.setAttribute('class', 'selected');
                    // }
                }
            } else if (department == 'District') {
                var obj2 = document.getElementById('lblDistrictCreation');
                if (obj2 != null) {
                    // if(obj2.innerHTML == 'Create'){
                    obj2.setAttribute('class', 'selected');
                    // }
                }
            } else if (department == 'SubDistrict') {
                var obj2 = document.getElementById('lblSubDistrictCreation');
                if (obj2 != null) {
                    // if(obj2.innerHTML == 'Create'){
                    obj2.setAttribute('class', 'selected');
                    // }
                }
            } else if (department == 'Gewog') {
                var obj2 = document.getElementById('lblGewogCreation');
                if (obj2 != null) {
                    // if(obj2.innerHTML == 'Create'){
                    obj2.setAttribute('class', 'selected');
                    // }
                }
            }

            var headSel_Obj = document.getElementById("headTabMngUser");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
    <%
        deptMasterSrBean = null;
        deptDataBean = null;
    %>
    <% }%>
</html>