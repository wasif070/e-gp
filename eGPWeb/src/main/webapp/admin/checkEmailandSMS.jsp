<%--
    Document   :check Email and SMS functionality
    Created on : Mar 17, 2011, 11:37:06 AM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>check Email and SMS</title>
    </head>
    <%

        // validate ip address for security
        String ipAddress = request.getHeader("X-FORWARDED-FOR");
        if(request.getHeader("X-FORWARDED-FOR")!=null && !request.getHeader("X-FORWARDED-FOR").equalsIgnoreCase("") && !request.getHeader("X-FORWARDED-FOR").equalsIgnoreCase("null")){
            ipAddress = request.getHeader("X-FORWARDED-FOR");
        }else{
            ipAddress = request.getRemoteAddr();
        }

        if("127.0.0.1".equals(ipAddress)){
            response.sendRedirect("../UnderConstruction.jsp");
        }
        
        String msg = "";

        // Verify Email action
        if("Verify".equalsIgnoreCase(request.getParameter("btnVerifyEmail"))){
            String strEmail = "";

            if(request.getParameter("txtEmail")!=null && !"".equalsIgnoreCase(request.getParameter("txtEmail"))){
                strEmail = request.getParameter("txtEmail");
            }

            String mails[]={strEmail};
            SendMessageUtil sendMessageUtil = new SendMessageUtil();
            MailContentUtility mailContentUtility = new MailContentUtility();

            // set dummy Email
            String mailText = "This is test mail";
            sendMessageUtil.setEmailTo(mails);
            sendMessageUtil.setEmailSub("This is testing mail");
            sendMessageUtil.setEmailMessage(mailText);
            
            try{
                //send mail
                sendMessageUtil.sendEmail();
                mails=null;
                sendMessageUtil=null;
                mailContentUtility=null;
                response.sendRedirect("checkEmailandSMS.jsp?msg=success");
            }catch(Exception ex){
                mails=null;
                sendMessageUtil=null;
                mailContentUtility=null;
                response.sendRedirect("checkEmailandSMS.jsp?msg=error");
            }
        }
        
        // verify SMS action
        if("Verify".equalsIgnoreCase(request.getParameter("btnVerifyMobile"))){

            String strMobile = "";

            if(request.getParameter("txtMobile")!=null && !"".equalsIgnoreCase(request.getParameter("txtMobile"))){
                strMobile = request.getParameter("txtMobile");
            }

            SendMessageUtil sendMessageUtil = new SendMessageUtil();

            // set dummy SMS Text
            sendMessageUtil.setSmsNo(strMobile);
            sendMessageUtil.setSmsBody("This is a Test Message");
            try{
                sendMessageUtil.sendSMS();
                response.sendRedirect("checkEmailandSMS.jsp?msg=success");
            }
            catch(Exception ex){
                response.sendRedirect("checkEmailandSMS.jsp?msg=error");
            }

        }

        // set Successfule msg
        if(request.getParameter("msg")!=null && !"".equalsIgnoreCase(request.getParameter("msg"))){

            if("success".equalsIgnoreCase(request.getParameter("msg"))){
                msg = "Message has been Sent Successfully";
            }
            if("error".equalsIgnoreCase(request.getParameter("msg"))){
                msg = "Error in sending Message";
            }
        }
    %>
    <body>
        <!-- form Display start here -->
        <form name="frmcheckSMSEmail" id="frmcheckSMSEmail" method="post" action="checkEmailandSMS.jsp">
            <table>
                <!-- Display msg here -->
                <% if(!"".equals(msg)){ %>
                <tr>
                    <td colspan="3"><%=msg%></td>
                </tr>
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <% } %>
                <tr>
                    <td>Email :</td>
                    <td><input type="text" name="txtEmail" id="txtEmail" value="" /></td>
                    <td><input type="submit" value="Verify" name="btnVerifyEmail" id="btnVerifyEmail" /></td>
                </tr>
                <tr>
                    <td>Mobile :</td>
                    <td><input type="text" name="txtMobile" id="txtMobile" value="" /></td>
                    <td><input type="submit" value="Verify" name="btnVerifyMobile" id="btnVerifyMobile" /></td>
                </tr>
            </table>
        </form>
    </body>
</html>
