<%-- 
    Document   : GovtUserCreation
    Created on : Oct 23, 2010, 5:28:28 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.model.table.TblWsMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.WsMasterService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Web-Service Consumer Detail </title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmWSConsumer").validate({
                    rules: {
                        mailId: { required: true, email: true },
                        fullName:{required: true},
                        password: {spacevalidate: true, requiredWithoutSpace: true , minlength: 8, maxlength: 25 ,alphaForPassword : true },
                        confPassword: { required: true, equalTo: "#txtPass" },
                        contactPerson: { required: true},
                        mobileNo: {required: true,number: true, minlength: 13, maxlength:15 },
                        bngName:{ maxlength:100},
                        cmbWebServiceList:{required: true}

                    },
                    messages: {
                        mailId: { required: "<div class='reqF_1'>Please enter e-mail ID</div>",
                            email: "<div id='e1' class='reqF_1'>Please Enter Valid e-mail ID</div>"
                        },
                        cmbWebServiceList:{
                            required: "<div class='reqF_1'>Please select WebServices to access</div>"
                        },
                        contactPerson: {
                            required: "<div class='reqF_1'>Please enter contact person</div>"
                        },
                        password: {
                            spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace: "<div class='reqF_1'>Please enter Password</div>",
                            alphaForPassword : "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>",
                            minlength: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>",
                            maxlength:"<div class='reqF_1'>Maximum 25 characters are allowed</div>"
                        },
                        confPassword:{ required: "<div class='reqF_1'>Please retype Password</div>",
                            equalTo: "<div class='reqF_1'>Password does not match. Please try again</div>"
                        },
                        fullName: {
                            required: "<div class='reqF_1'>Please enter Orgnization Name</div>",
                            spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace: "<div class='reqF_1'>Please Enter Organization Details</div>",
                            alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only </div>",
                            maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"
                        },                        
                        mobileNo: {
                            required: "<div class='reqF_1'>Please enter Mobile Number</div>",
                            number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" ,
                            minlength:"<div class='reqF_1'>Minimum 13 digits are required</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 15 digits only</div>"
                        },               
                        bngName:{
                            maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>"
                        }                      
                    },
                    errorPlacement:function(error ,element){
                        if(element.attr("name")=="password") {
                            error.insertAfter("#tipPassword");                       
                        }
                        else{
                            error.insertAfter(element);
                        }
                    }
                });
            });
        </script>

        <script type="text/javascript">

            function numeric(value) {
                return /^\d+$/.test(value);
            }

            function checkMail(){                            
                var mailflag=false;
                // alert('::'+document.getElementById("txtMail").value+'::');

                if($.trim(document.getElementById("txtMail").value) != "" ){
                    $('span.#mailMsg').css("color","red");
                    $('span.#mailMsg').html("Checking for unique Mail Id...");
                    $.post("<%=request.getContextPath()%>/WebServiceConsumerControlServlet?action=checkMailId", {mailId:$.trim($('#txtMail').val()),funName:'checkMail'}, function(j){
                        if(j == "OK"){
                            $('span.#mailMsg').css("color","green");
                            mailflag = true;
                        }else{
                            $('span.#mailMsg').css("color","red");
                        }
                        $('span.#mailMsg').html(j);
                        if(document.getElementById("e1") != null){
                            document.getElementById("e1").innerHTML = "";
                        }
                        return mailflag;
                    });
                }else{
                    if(document.getElementById("e1") != null){
                        document.getElementById("e1").innerHTML = "";
                    }
                    document.getElementById('mailMsg').innerHTML = "";
                }
            }
            function checkData(){
                //var isMail=false;
                var retval;
                var isForEdit = 'false';
                if(isForEdit == "false"){
                    if(document.getElementById("txtMail").value !="" && document.getElementById("mailMsg").innerHTML != "OK"){                        
                        return false;
                    }
                }
                if($('#frmWSConsumer').valid()){
                    //$('#btnSubmit').attr("disabled", true);
                    document.getElementById("frmWSConsumer").action="/WebServiceConsumerControlServlet?action=add";
                    document.getElementById("frmWSConsumer").submit();
                    //return true;
                }
            }
               
        </script>
        <script type="text/javascript">
            function passMsg(){
                jAlert("Copy and Paste not allowed.","Web Service Consumer Creation", function(RetVal) {
                });
                return false;
            }
        </script>
        <%
                    boolean flag = false;
                    String msg = "";

                    if (request.getParameter("flag") != null) {
                        if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
                            flag = true;
                        }
                    }
                    if (request.getParameter("msg") != null) {
                        if ("emailPattern".equalsIgnoreCase(request.getParameter("msg"))) {
                            msg = "Please Enter Valid e-mail ID";
                        } else if ("email".equalsIgnoreCase(request.getParameter("msg"))) {
                            msg = "e-mail ID already exists, Please enter another e-mail ID";
                        }
                    }
        %>

        <%
                    //getting webservices list
                    WsMasterService wsMasterService = (WsMasterService) AppContext.getSpringBean("WsMasterService");
                    List<TblWsMaster> webServiceList = new ArrayList<TblWsMaster>();
                    webServiceList = wsMasterService.getAllWsMaster();
        %>
    </head>
    <body>       
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1">
                                        <div class="pageHead_1">
                                            Add Web-Service Consumer Detail
                                            <span style="float: right; text-align: right;">
                                                <a class="action-button-goback" href="WSConsumersList.jsp?mode=view">Go back</a>
                                            </span>
                                        </div>
                                        <form method="POST" id="frmWSConsumer" action="/WebServiceConsumerControlServlet?action=add">
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" >
                                                <tr>
                                                    <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.                               
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff" width="20%" >e-Mail ID : <span>*</span></td>
                                                    <td width="80%">
                                                        <input name="mailId" id="txtMail" type="text" class="formTxtBox_1"  style="width:200px;" onblur="return checkMail();" />                                                        
                                                        <span id="mailMsg" style="color: red; font-weight: bold"></span>
                                                        <span id="phno" style="color: grey;"><br />Please enter Official and Designation specific e-mail ID i.e. ce@lged.gov.bd</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Password : <span>*</span></td>
                                                    <td><input name="password" id="txtPass" type="password" class="formTxtBox_1" style="width:200px;" maxlength="25" value="" autocomplete="off" /><br/><span id="tipPassword" style="color: grey;"> ( Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. 
                                                <br/>Special characters may be added) </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Confirm Password : <span>*</span></td>
                                                    <td><input name="confPassword" id="txtConfPass" type="password" class="formTxtBox_1"
                                                               style="width:200px;" onpaste="return passMsg();" autocomplete="off" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Organization Name : <span>*</span></td>
                                                    <td><input name="fullName" id="txtaFullName" type="text" class="formTxtBox_1" style="width:200px;"/>
                                                        <span id="orgNameMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Organization Name in Bangla : </td>
                                                    <td><input name="bngName" id="txtBngName" type="text" class="formTxtBox_1" 
                                                               style="width:200px;" maxlength="101" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Organization Address / Details : </td>
                                                    <td><textarea name="txtAreaOrgAdd" rows="3" class="formTxtBox_1" id="txtAreaOrgAdd" style="width:400px;"></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Contact Person : <span>*</span></td>
                                                    <td><input name="contactPerson" id="contactPerson" type="text" class="formTxtBox_1" style="width:200px;"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Contact Person Mobile No : <span>*</span></td>
                                                    <td><input name="mobileNo" maxlength="16" id="txtMob" type="text" class="formTxtBox_1" style="width:200px;" />
                                                        <span id="mNo" style="color: grey;"> (Mobile No. format e.g 12345678 )</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Assign Web-Services To Access : <span>*</span></td>
                                                    <td>
                                                        <select name="cmbWebServiceList" class="formTxtBox_1" id="cmbWebServiceList" multiple size="5">
                                                            <%  for (TblWsMaster tblWsMaster : webServiceList) {
                                                                            out.println("<option value=" + tblWsMaster.getWsId() + ">" + tblWsMaster.getWsName() + "</option>");
                                                                        }
                                                            %>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Status : </td>
                                                    <td class="t-align-left">
                                                        <select name="status" class="formTxtBox_1" id="cmbHintQue" style="width:auto;min-width: 200px;">
                                                            <option value="N" selected>Activate</option>
                                                            <option value="Y">Deactivate</option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="t-align-left">
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnSubmit" id="btnSubmit" value="Submit" onclick="return checkData();" />
                                                        </label> 
                                                    </td>
                                                </tr>                                                
                                            </table>
                                        </form>
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
</html>