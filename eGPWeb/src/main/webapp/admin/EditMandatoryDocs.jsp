<%-- 
    Document   : EditMandatoryDocs
    Created on : Apr 25, 2011, 6:49:59 PM
    Author     : TaherT
--%>

<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TemplateSectionFormImpl"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Required Documents for a form</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            function validate(){
                $(".err").remove();
                var bool=true;
                var cnt=0;
                $("textarea").each(function(){
                    if($.trim($(this).val())==""){
                        $(this).parent().append("<div class='err' style='color:red;'>Please enter Document Name.</div>");
                        cnt++;
                    }else{
                        if($.trim($(this).val()).length>500){
                            $(this).parent().append("<div class='err' style='color:red;'>Max 500 Character allowed.</div>");
                            cnt++;
                        }
                    }
                })
                if(cnt!=0){
                   bool=false;
                }
                return bool;
            }
        </script>
    </head>
    <body>
        <%
                    TemplateSectionFormImpl templateForm = (TemplateSectionFormImpl) AppContext.getSpringBean("AddFormService");
                    List<Object[]> mandDocList = templateForm.getSTDMandDocs(request.getParameter("mId"));
        %>
        <div class="contentArea_1">
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <div class="pageHead_1">Edit Required Documents for a form<span style="float: right;"><a class="action-button-goback" href="DefineSTDInDtl.jsp?templateId=<%=request.getParameter("tId")%>">Go Back to Dashboard</a></span></div>
            <form action="<%=request.getContextPath()%>/CreateSTDForm?action=updateSTDMandDocs" method="post">
                <input type="hidden" value="<%=request.getParameter("pg")%>" name="pageName"/>
                 <input type="hidden" value="<%=request.getParameter("tId")%>" name="tId"/>
                <input type="hidden" value="<%=request.getParameter("fId")%>" name="fId"/>
                <input type="hidden" value="<%=request.getParameter("sId")%>" name="sId"/>
            <table class="tableList_1" cellspacing="0" width="100%" id="members">
                <tbody id="tbodyData">
                    <tr>
                        <th class="t-align-center" width="4%">Sl. <br/> No.</th>
                        <th class="t-align-center" width="96%">Name of Document<span class="mandatory">*</span></th>
                    </tr>
                    <%
                                int man_cnt = 1;
                                for (Object[] data : mandDocList) {
                    %>
                    <tr>
                        <td class="t-align-center"><%=man_cnt%></td>
                        <td>
                            <textarea cols="10" rows="5"  id="doc_0" name="docName" style="width: 400px;" class="formTxtBox_1"><%=data[1]%></textarea>
                            <input type="hidden" value="<%=data[0]%>" name="mId"/>
                        </td>
                    </tr>
                    <%man_cnt++;
                     }%>
                </tbody>                
            </table>
                <div class="t-align-center t_space">
                    <label class="formBtn_1">
                        <input type="submit" name="submit" value="Submit" onclick="return validate();"/>
                    </label>
                </div>
            </form>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
