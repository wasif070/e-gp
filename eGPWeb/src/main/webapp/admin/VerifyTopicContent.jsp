<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.Date" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.PublicForumPostService" %>
<%@page import="com.cptu.egp.eps.model.table.TblPublicProcureForum" %>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster" %>
<%@page import="java.util.Date" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Verify Topic Detail</title>
        <link href="../resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script type="text/javascript">
    $(document).ready(function(){
         $("#ketan").hide();
        $("#adminaction").change(function(){
            $(".err").remove();
            $(".mendatory").remove();
            if($("#adminaction").val()== "Rejected"){
                $("#lblComments").append("<span class='mendatory'>&nbsp;*</span>");
            }
        });

        $("#comments").change(function()
        {
            if($("#adminaction").val()== "Accepted"){
               $(".err").remove();
            }

        });
    })
        </script>

        <script type="text/javascript">
    function validate()
    {
         $(".err").remove();

                var valid = true;
                //alert("Ketan : "+$.trim($("#adminaction").val()));
                if($("#adminaction").val()== "Rejected"){
                    if($.trim($("#comments").val())=="" ){
                       $("#comments").parent().append("<div class='err' style='color:red;'>Please enter Comments</div>");
                       valid = false;
                    }
                    if($.trim($("#comments").val()).length > 2000){
                       $("#comments").parent().append("<div class='err' style='color:red;'>Maximum 2000 Characters are allowed</div>");
                       valid = false;
                    }
                }

                if(!valid){
                    return false;
                }
    }
 /*   function hideDetails()
   {
        alert("Ketan");
        $(document.ready(function(){
              alert("Ketan");
            $("#topicdetail").hide();
        }));

    }*/

        </script>
    </head>
    <body>
        <%
                    String topicSubject = "";
    String topicDetail = "";
    String ppfid = "";
    String parentppfid = "";
    String auother = "";
                    String postedBy = "";
                    String desc = "";
    Date postedDate = null;
    String verificationStatus = "";

                    if (request.getParameter("ppfid") != null) {
        PublicForumPostService publicForumPostService = (PublicForumPostService) AppContext.getSpringBean("PublicForumPostService");
        ppfid = request.getParameter("ppfid");


        List<TblPublicProcureForum> colResult =  publicForumPostService.getAllForums(ppfid);
                        for (TblPublicProcureForum procureForum : colResult) {
            topicSubject = procureForum.getSubject();
            topicDetail = procureForum.getDescription();
            postedBy = String.valueOf(procureForum.getPostedBy());
            postedDate = procureForum.getPostDate();
            parentppfid = String.valueOf(procureForum.getPpfParentId());
            auother = procureForum.getFullName();
            verificationStatus = procureForum.getStatus();
            desc = procureForum.getComments();


        }
    }
        %>
        <div class="dashboard_div">
  <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
  <div class="pageHead_1"> Verify Topic Content <span class="c-alignment-right"><a href="../ViewAllTopic.jsp" class="action-button-goback">Go Back</a></span> </div>
  <form id="frmPublicProcForum" name="frmPublicProcForum" method="post" action="<%=request.getContextPath()%>/PublicProcForumServlet" >
    <table cellspacing="10" class="formStyle_1 t_space" width="100%">
      <tr>
        <td width="13%" class="ff">Topic :</td>
        <td colspan="3"><%=topicSubject%></td>
      </tr>
      <tr>
        <td class="ff">Detail : </td>
        <td colspan="3"><%=topicDetail%></td>
      </tr>
      <tr>
        <td class="ff">Author :</td>
        <td width="46%"><%=auother%></td>
        <td width="9%" class="ff">Posted on :</td>
        <td width="32%"><%=DateUtils.gridDateToStr(postedDate)%></td>
      </tr>
      <tr>
        <td class="ff">Verification Status :</td>
        <td><%=verificationStatus%></td>
        <td class="ff">Action :</td>
        <td><select name="adminaction" class="formTxtBox_1" id="adminaction" style="width:85px;">
                <option value="Accepted">Accept</option>
          <option value="Rejected">Reject</option>
        </select></td>
      </tr>
      <tr>
        <td class="ff" id="lblComments">Comments :</td>
        <td colspan="3"><span class="t-align-left">
                                    <textarea cols=""  rows="5" id="comments" name="comments"
                                                                  class="formTxtBox_1" style="width:50%;"><%if(desc!=null && !"".equals(desc)){
                                                  out.println(desc);
                                                  }%></textarea>
        </span></td>
      </tr>
      <tr>
          <td>&nbsp;</td>
        <td colspan="3" class="t-align-left"><label class="formBtn_1 t-align-center">
                <input type="submit" name="button" value="Submit" onclick="return validate()" />
                                                </label></td>
      </tr>
    </table>
    <input type="hidden" value="verifyForum" name="action" id="action" />
    <input type="hidden" value="topic" name="forumType" id="forumType" />
    <input type="hidden" value="<%=ppfid%>" name="ppfid" id="ppfid" />
    <input type="hidden" value="<%=topicSubject%>" name="topicSubject" id="topicSubject" />
    <input type="hidden" value="<%=postedDate.toString()%>" name="postedDate" id="postedDate" />
    <input type="hidden" value="<%=postedBy%>" name="postedBy" id="postedBy" />
    <input type="hidden" value="<%=parentppfid%>" name="parentppfid" id="parentppfid" />
    <input type="hidden" value="<%=auother%>" name="auother" id="auother" />
                    <div id="ketan"> <textarea cols="5"  rows="5" id="topicDetail" name="topicDetail" class="formTxtBox_1" style="width:60%;"><%=topicDetail%></textarea>
                <script type="text/javascript">
                                    CKEDITOR.replace( 'topicDetail',
                                    {
                                        fullPage : false
                                    });
                        </script> </div>

  </form>
                <!--Dashboard Content Part End-->
            </div>
        </div>
                <!--Dashboard Footer Start-->
  <jsp:include page="../resources/common/Bottom.jsp"></jsp:include>
        <!--Dashboard Footer End-->
    </body>
</html>

<%--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="java.util.Date" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.PublicForumPostService" %>
<%@page import="com.cptu.egp.eps.model.table.TblPublicProcureForum" %>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster" %>
<%@page import="java.util.Date" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Verify Topic Detail</title>
        <link href="../resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="../resources/js/form/CommonValidation.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script type="text/javascript">
            $(document).ready(function(){
                //$("#ketan").hide();
                $("#adminaction").change(function(){
                    $(".err").remove();
                    $(".mendatory").remove();
                    if($("#adminaction").val()== "Rejected"){
                        $("#lblComments").append("<span class='mendatory'>&nbsp;*</span>");
                    }
                });

                $("#comments").change(function()
                {
                    if($("#adminaction").val()== "Accepted"){
                        $(".err").remove();
                    }
           
                });
            })
        </script>

        <script type="text/javascript">
            function validate()
            {
                $(".err").remove();

                var valid = true;
                //alert("Ketan : "+$.trim($("#adminaction").val()));
                if($.trim($("#topicSubject").val())=="" ){
                        $("#topicSubject").parent().append("<div class='err' style='color:red;'>Please enter Topic</div>");
                        valid = false;
                }
                if($("#topicSubject").val().length > 100){
                    $("#topicSubject").parent().append("<div class='err' style='color:red;'>Maximum 100 characters are allowed</div>");
                    valid = false;
                }
                if(isCKEditorFieldBlank($.trim(CKEDITOR.instances.topicDetail.getData().replace(/<[^>]*>|\s/g, '')))){
                    $("#topicDetail").parent().append("<div class='err' style='color:red;'>Please enter Detail</div>");
                    CKEDITOR.instances.topicDetail.setData("");
                    valid = false;
                }
                if($.trim(CKEDITOR.instances.topicDetail.getData().replace(/<[^>]*>|\s/g, '')).length > 2000) {
                    $("#topicDetail").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters are allowed</div>");
                    valid = false;
                }
                if($("#adminaction").val()== "Rejected"){
                    if($.trim($("#comments").val())=="" ){
                        $("#comments").parent().append("<div class='err' style='color:red;'>Please enter Comments</div>");
                        valid = false;
                    }
                    if($.trim($("#comments").val()).length > 2000){
                        $("#comments").parent().append("<div class='err' style='color:red;'>Maximum 2000 Characters are allowed</div>");
                        valid = false;
                    }
                }

                if(!valid){
                    return false;
                }
            }
            /*   function hideDetails()
   {
        alert("Ketan");
        $(document.ready(function(){
              alert("Ketan");
            $("#topicdetail").hide();
        }));

    }*/

        </script>
    </head>
    <body>
        <%
                    String topicSubject = "";
                    String topicDetail = "";
                    String ppfid = "";
                    String parentppfid = "";
                    String auother = "";
                    String ppfStatus = "";
                    String postedBy = "";
                    String desc = "";
                    Date postedDate = null;
                    String verificationStatus = "";

                    if (request.getParameter("ppfid") != null) {
                        PublicForumPostService publicForumPostService = (PublicForumPostService) AppContext.getSpringBean("PublicForumPostService");
                        ppfid = request.getParameter("ppfid");


                        List<TblPublicProcureForum> colResult = publicForumPostService.getAllForums(ppfid);
                        for (TblPublicProcureForum procureForum : colResult) {
                            topicSubject = procureForum.getSubject();
                            topicDetail = procureForum.getDescription();
                            postedBy = String.valueOf(procureForum.getPostedBy());
                            postedDate = procureForum.getPostDate();
                            parentppfid = String.valueOf(procureForum.getPpfParentId());
                            auother = procureForum.getFullName();
                            verificationStatus = procureForum.getStatus();
                            desc = procureForum.getComments();
                            ppfStatus = procureForum.getStatus();


                        }
                    }
        %>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1"> Verify Topic Content <span class="c-alignment-right"><a href="../ViewAllTopic.jsp" class="action-button-goback">Go Back</a></span> </div>
                <form id="frmPublicProcForum" name="frmPublicProcForum" method="post" action="<%=request.getContextPath()%>/PublicProcForumServlet" >
                    <table cellspacing="10" class="formStyle_1 t_space" width="100%">
                        <tr>
                            <td width="13%" class="ff">Topic :</td>
                            <td colspan="3">
                                <input type="text" name="topicSubject" value="<%=topicSubject%>" class="formTxtBox_1" id="topicSubject" style="width:200px;" maxlength="100" />
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Detail : </td>
                            <td colspan="3">
                                <textarea rows="5" id="topicDetail" name="topicDetail" class="formTxtBox_1" style="width:60%;" maxlength="2000"><%=topicDetail%></textarea>
                                <script type="text/javascript">
                                    CKEDITOR.replace('topicDetail',
                                    {
                                        fullPage : false
                                    });
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Author :</td>
                            <td width="46%"><%=auother%></td>
                            <td width="9%" class="ff">Posted on :</td>
                            <td width="32%"><%=DateUtils.gridDateToStr(postedDate)%></td>
                        </tr>
                        <tr>
                            <td class="ff">Verification Status :</td>
                            <td><%=verificationStatus%></td>
                            <td class="ff">Action :</td>
                            <td><select name="adminaction" class="formTxtBox_1" id="adminaction" style="width:85px;">
                                    <option value="Accepted" <%=ppfStatus.equals("Accepted") ? " selected " : ""%>>Accept</option>
                                    <option value="Rejected" <%=ppfStatus.equals("Rejected") ? " selected " : ""%>>Reject</option>
                                </select></td>
                        </tr>
                        <tr>
                            <td class="ff" id="lblComments">Comments :</td>
                            <td colspan="3"><span class="t-align-left">
                                    <textarea cols=""  rows="5" id="comments" name="comments"
                                        class="formTxtBox_1" style="width:50%;"><%if (desc != null && !"".equals(desc)) {
                                                                                  out.println(desc);
                                                                              }%></textarea>
                                </span></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="3" class="t-align-left"><label class="formBtn_1 t-align-center">
                                    <input type="submit" name="button" value="Submit" onclick="return validate()" />
                                </label></td>
                        </tr>
                    </table>
                    <input type="hidden" value="verifyForum" name="action" id="action" />
                    <input type="hidden" value="topic" name="forumType" id="forumType" />
                    <input type="hidden" value="<%=ppfid%>" name="ppfid" id="ppfid" />
                    <!--    <input type="hidden" value="< %=topicSubject%>" name="topicSubject" id="topicSubject" />-->
                    <input type="hidden" value="<%=postedDate.toString()%>" name="postedDate" id="postedDate" />
                    <input type="hidden" value="<%=postedBy%>" name="postedBy" id="postedBy" />
                    <input type="hidden" value="<%=parentppfid%>" name="parentppfid" id="parentppfid" />
                    <input type="hidden" value="<%=auother%>" name="auother" id="auother" />
                </form>
                <!--Dashboard Content Part End-->
            </div>
        </div>
        <!--Dashboard Footer Start-->
        <jsp:include page="../resources/common/Bottom.jsp"></jsp:include>
        <!--Dashboard Footer End-->
    </body>
</html>
--%>