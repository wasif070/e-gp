<%-- 
    Document   : AITConfigutation
    Created on : Jul 12, 2011, 3:15:16 PM
    Author     : Sreenu.Durga
--%>

<%@page import="java.lang.String"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsAitConfigBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>AIT Configuration</title>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>

        <%
                    StringBuilder userType = new StringBuilder();
                    if (request.getParameter("userType") != null) {
                        if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                            userType.append(request.getParameter("userType"));
                        } else {
                            userType.append("org");
                        }
                    } else {
                        userType.append("org");
                    }
                    String uId = "0";
                    if (session.getAttribute("userId") != null) {
                        uId = session.getAttribute("userId").toString();
                    }
                    final int ZERO = 0;
                    CmsAitConfigBean cmsAitConfigBean = new CmsAitConfigBean();
                    cmsAitConfigBean.setLogUserId(uId);
                    List<CommonAppData> financialYearsList = cmsAitConfigBean.getRemainingCmsAitConfigFinancialYears();
                    String financialyearId = "";
                    int currentYearId = 0;
                    if (request.getParameter("financialyearId") != null) {
                        financialyearId = request.getParameter("financialyearId");
                        currentYearId = Integer.parseInt(request.getParameter("financialyearId"));
                    }
        %>
    </head>
    <script Language="JavaScript" >
        function formValidation(form){
            var counter = parseInt(document.getElementById("hdnTotalSlabs").value);
            if(counter == 0){
                return false;
            }else{
                for (var slabNum = 1;  slabNum <= counter;  slabNum++){
                    var startingAmount = document.getElementById("txtStartAmountSlab"+slabNum).value;
                    var endingAmount = document.getElementById("txtEndAmountSlab"+slabNum).value;
                    var aitPercentage = document.getElementById("txtAitPercentageSlab"+slabNum).value;                   
                    if(startingAmount =="" || startingAmount.length==0 || startingAmount.indexOf(" ")!=-1){
                        jAlert('Please fill the values', 'Warning!');
                        return false;
                    }
                    if(endingAmount =="" || endingAmount.length==0 || endingAmount.indexOf(" ")!=-1){
                        jAlert('Please fill the values', 'Warning!');
                        return false;
                    }
                    if(aitPercentage =="" || aitPercentage.length==0 || aitPercentage.indexOf(" ")!=-1){
                        jAlert('Please fill the values', 'Warning!');
                        return false;
                    }                    
                } //for
                var flatPercent = document.getElementById("txtFlatAitPercentage").value;
                if(flatPercent =="" || flatPercent.length==0 || flatPercent.indexOf(" ")!=-1){
                    jAlert('Please fill the Flat Percent', 'Warning!');
                    return false;
                }
            }//else
            return true;
        }
    </script>
    <script Language="JavaScript" >
        function checkAmountValues(control) {
            var counter = parseInt(document.getElementById("hdnTotalSlabs").value);
            var enteredValue=control.value;
            var checkOk = ".1234567890";
            if(enteredValue =="" || enteredValue.length==0 || enteredValue.indexOf(" ") != -1){
                return false;
            }else{
                for (var i = 0;  i < enteredValue.length;  i++){
                    var ch = enteredValue.charAt(i);
                    if (checkOk.indexOf(ch)==-1){
                        control.focus();
                        jAlert("Please enter the values between 0-9 Only","Warning!");
                        control.value="";
                        return false;
                    }//if
                }//for
            }//else           
            if(counter>0){
                var selectedControlName = parseInt(control.name.toString().substr(16,control.name.toString().length));
                if(control.name.indexOf("End")>-1){
                    var startingAmount = document.getElementById("txtStartAmountSlab"+(selectedControlName)).value;
                    var endingAmount = document.getElementById("txtEndAmountSlab"+(selectedControlName)).value;
                    if(startingAmount =="" || startingAmount.length==0){
                        jAlert('Starting Amount should not be Empty', 'Warning!');
                        control.value="";
                        control.focus();
                        return false;
                    }else if(endingAmount =="" || endingAmount.length==0){
                        jAlert('Ending Amount should not be Empty', 'Warning!');
                        control.value="";
                        control.focus();
                        return false;
                    }else{
                        var startingAmountValue = parseInt(document.getElementById("txtStartAmountSlab"+(selectedControlName)).value);
                        var endingAmountValue = parseInt(document.getElementById("txtEndAmountSlab"+(selectedControlName)).value);
                        if(startingAmountValue >= endingAmountValue){
                            jAlert('Starting Amount should be lessthan Ending Amount', 'Warning!');
                            control.value="";
                            control.focus();
                            return false;
                        } else if(counter>1){
                            var previousValue = parseInt(document.getElementById("txtEndAmountSlab"+(selectedControlName)).value);
                            document.getElementById("txtStartAmountSlab"+(selectedControlName+1)).value = previousValue+1;
                            document.getElementById("txtStartAmountSlab"+(selectedControlName+1)).readOnly = true;
                        }
                    }
                }
            }
            return true;
        }
    </script>
    <script Language="JavaScript" >
        function checkPercentageRange(control) {
            var enteredValue=control.value;
            var flag=checkAmountValues(control);
            if(flag){
                flag=false;
                if((enteredValue <0) || (enteredValue>100)){
                    jAlert('Percentage should be 0 to 100 only', 'Warning!');
                    control.value = "";
                    control.focus();
                    flag = false;
                }else{
                    flag = true;
                }
                return flag;
            }else{
                flag=false;
            }
            return flag;
        }
        function setInfiniteValue(control){
            var counter = parseInt(document.getElementById("hdnTotalSlabs").value);
            document.getElementById("txtEndAmountSlab"+counter).value = "above";
        }
    </script>

    <script type="text/javascript">        
        $(function() {
            $('#linkAddSlab').click(function() {
                var counter = parseInt(document.getElementById("hdnTotalSlabs").value)+1;
                if(counter >1){
                    document.getElementById("infiniteButton"+(counter-1)).style.display =  'none';
                    document.getElementById("txtEndAmountSlab"+(counter-1)).value =  '';
                }
                var html =" <tr  id='tableRow_"+counter+"'><td class='t-align-center'>"+counter+"</td>"+
                    "<td class='t-align-center'><input name='txtStartAmountSlab"+counter+"' type='text' size='10' class='formTxtBox_1' id='txtStartAmountSlab"+counter+"' onblur='return checkAmountValues(this);' /></td>"+
                    "<td class='t-align-center'><input name='txtEndAmountSlab"+counter+"' type='text' size='10' class='formTxtBox_1' id='txtEndAmountSlab"+counter+"'  onblur='return checkAmountValues(this);'></input>" +
                    "<input type='button' align='right' name='infiniteButton"+counter+"' id='infiniteButton"+counter+"'  value='InfiniteValue' onclick='setInfiniteValue(this);' />"+"</td>"+
                    "<td class='t-align-center'><input name='txtAitPercentageSlab"+counter+"' type='text' size='10' class='formTxtBox_1' id='txtAitPercentageSlab"+counter+"' onblur='return checkPercentageRange(this);'  /></td></tr>";
                $("#tblAitConfigSlab").append(html);
                document.getElementById("hdnTotalSlabs").value = (counter);
            });
            $('#linkDeleteSlab').click(function() {
                var counter = parseInt(document.getElementById("hdnTotalSlabs").value);
                if(counter > 1){
                    document.getElementById("hdnTotalSlabs").value = counter-1;
                    $("tr[id='tableRow_"+counter+"']").remove();
                    document.getElementById("infiniteButton"+(counter-1)).style.display = 'inline';
                }else if(counter == 1){
                    document.getElementById("hdnTotalSlabs").value = counter-1;
                    $("tr[id='tableRow_"+counter+"']").remove();
                }
            });
        });

    </script>

    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%--Dashboard Header Start--%>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%--Dashboard Header End--%>
                <%--Dashboard Body Start--%>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">                    
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType.toString()%>" ></jsp:include>
                        <td class="contentArea">
                            <div>
                                <div class="pageHead_1">AIT Configuration</div>
                            </div>
                            <form id="formAitTaxConfiguration" name="formAitTaxConfiguration" method="post" action='/AITConfigServlet?action=add' onsubmit="return formValidation(this);">
                                <div>                                    
                                    <div class="t-align-right t_space b_space">
                                        <a id="linkAddSlab"  href="javascript:void(0);" class="action-button-add">Add Slab</a>
                                        <a id="linkDeleteSlab"  href="javascript:void(0);" class="action-button-delete">Delete Slab</a>
                                    </div>
                                    <div class="t-align-left t_space b_space">
                                        Financial Year : <span style="color: red">*</span>
                                        <select name="cmbFinancialYear" class="formTxtBox_1" id="cmbFinancialYear" style="width:200px;" >
                                            <%
                                                        if ((financialYearsList != null) && (!financialYearsList.isEmpty())) {
                                                            String selected = "";
                                                            for (CommonAppData commonApp : financialYearsList) {
                                                                //if request came from AITViewList
                                                                if (financialyearId.length() > 0) {
                                                                    if (financialyearId.equalsIgnoreCase(commonApp.getFieldName1())) {
                                                                        selected = " selected";
                                                                    } else {
                                                                        selected = "";
                                                                    }
                                                                } else {
                                                                    //if request came from Left Menu
                                                                    if ("Yes".equalsIgnoreCase(commonApp.getFieldName3())) {
                                                                        selected = " selected";
                                                                    } else {
                                                                        selected = "";
                                                                    }
                                                                }
                                                                out.println(" <option value=" + commonApp.getFieldName1() + " " + selected + ">");
                                                                out.println(commonApp.getFieldName2());
                                                                out.println("</option>");
                                                            }// outer for loop
                                                        }
                                            %>
                                        </select>                                        
                                    </div>
                                </div>
                                <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_3" id="tblAitConfigSlab">
                                    <tr id='tableRow_0'>
                                        <th width="8%" valign="top" class="ff">Slab No.</th>
                                        <th width="34%">Starting Amount</th>
                                        <th width="34%">Ending Amount</th>
                                        <th width="24%">AIT Percent</th>
                                    </tr>       
                                </table>
                                <input type="hidden" name="hdnTotalSlabs" id="hdnTotalSlabs" value="<%=ZERO%>"/>                              
                                <table width="100%" cellspacing="5" cellpadding="0" border="0"  class="formStyle_1 t_space">
                                    <tr>
                                        <td width="14%" class="ff">Flat AIT Percentage :</td>
                                        <td width="86%">
                                            <input name="txtFlatAitPercentage" type="text" size="10" class="formTxtBox_1" id="txtFlatAitPercentage" onblur='return checkPercentageRange(this);' />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="t-align-center" colspan="2">
                                            <label class="formBtn_1">
                                                <input type="submit" align="middle" name="submit" id="submit" value="Submit" />
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <%--Dashboard Body End--%>
                <%--Dashboard Footer Start--%>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <%--Dashboard Footer End--%>
            </div><%-- end dashboard_div--%>
        </div><%-- end mainDiv--%>
    </body>
</html>
