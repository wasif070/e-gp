<%--
    Document   : CompanyVerification
    Created on : Nov 5, 2010, 11:40 PM
    Author     : taher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Verification</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="../text/javascript" src="resources/js/pngFix.js"></script>-->

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />

        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript">
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat:"%d/%m/%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            function showHide()
        {
            if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
                document.getElementById('tblSearchBox').style.display = 'table';
                document.getElementById('collExp').innerHTML = '- Advanced Search';
            }else{
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Advanced Search';
            }
        }
        function hide()
        {
            document.getElementById('tblSearchBox').style.display = 'none';
            document.getElementById('collExp').innerHTML = '+ Advanced Search';
        }
        </script>
        <script type="text/javascript">            
            <%--function changeParam(){
                document.getElementById("status").value = 'Approved';
                fillGrid();
            }--%>
            function fillGrid(){
                if($('#cmbstatus').val()=="pending"){
                    document.getElementById("linkPending").className = "sMenu";
                    document.getElementById("linkApproved").className = "";
//                    document.getElementById("linkRejected").className = "";
                    document.getElementById("linkReapply").className = "";
                    //fillGridOnEvent('pending');
                }else if($('#cmbstatus').val()=="approved"){
                    document.getElementById("linkPending").className = "";
                    document.getElementById("linkApproved").className = "sMenu";
//                    document.getElementById("linkRejected").className = "";
                    document.getElementById("linkReapply").className = "";
                    //fillGridOnEvent('approved')
                }else if($('#cmbstatus').val()=="reapply"){
                    document.getElementById("linkPending").className = "";
                    document.getElementById("linkApproved").className = "";
//                    document.getElementById("linkRejected").className = "";
                    document.getElementById("linkReapply").className = "sMenu";
                    //fillGridOnEvent('approved')
                }else{
                    document.getElementById("linkPending").className = "";
                    document.getElementById("linkApproved").className = "";
//                    document.getElementById("linkRejected").className = "sMenu";
                    document.getElementById("linkReapply").className = "";
                    //fillGridOnRejectionEvent('rejected');
                }
                $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                jQuery("#list").jqGrid({
                    url:'<%=request.getContextPath()%>/ContentAdminServlet?q=1&action=fetchData&regType='+document.getElementById("cmbRegType").value+'&cmpRegNo='+document.getElementById("txtCompNo").value +'&cmpName='+document.getElementById("txtCompName").value+'&email='+document.getElementById("txtMail").value+'&regDateTo='+document.getElementById("txtRegToDate").value+'&regDateFrom='+document.getElementById("txtRegFromDate").value+'&status='+document.getElementById("cmbstatus").value,
                    datatype: "xml",
                    height: 250,
                    colNames:['Sl. No.','Registration Type','Company Name','e-mail ID',"Country","Dungkhag / Sub-district","Registration Date and Time","Action"],
                    colModel:[
                            {name:'srNo',index:'srNo', width:35,sortable:false,align:'center'},
                            {name:'registrationType',index:'registrationType', width:130},
                            {name:'companyName',index:'companyName', width:180},
                            {name:'emailId',index:'emailId', width:150},
                            {name:'country',index:'tm.country', width:80,align:'center'},
                            {name:'state',index:'tm.state', width:80,align:'center'},
//                            {name:'city',index:'city', width:80,align:'center'},
                            {name:'registeredDate',index:'registeredDate', width:90,align:'center'},
                            {name:'userid',index:'userid', width:130,sortable:false,align:'center'}
                    ],
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),                    
                    caption: " ",
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{edit:false,add:false,del:false,search:false});                
            }
            jQuery().ready(function (){
                //fillGrid();
            });
            <%--function filllstatus(){
                document.getElementById("status").value = document.getElementById("cmbstatus").value;
            }--%>
        </script>

        <script>
                function test(type){
                    if(type==1){
                        document.getElementById("linkPending").className = "sMenu";
                        document.getElementById("linkApproved").className = "";
//                        document.getElementById("linkRejected").className = "";
                        document.getElementById("linkReapply").className = "";
                        fillGridOnEvent('pending');
                    }else if(type==2){
                        document.getElementById("linkPending").className = "";
                        document.getElementById("linkApproved").className = "sMenu";
//                        document.getElementById("linkRejected").className = "";
                        document.getElementById("linkReapply").className = "";
                        fillGridOnEvent('approved')
                    }else if(type==4)
                    {
                        document.getElementById("linkPending").className = "";
                        document.getElementById("linkApproved").className = "";
//                        document.getElementById("linkRejected").className = "";
                        document.getElementById("linkReapply").className = "sMenu";
                        fillGridOnEvent('reapply');
                    }
                    else{
                        document.getElementById("linkPending").className = "";
                        document.getElementById("linkApproved").className = "";
//                        document.getElementById("linkRejected").className = "sMenu";
                        document.getElementById("linkReapply").className = "";
//                        fillGridOnRejectionEvent('rejected');
                    }
                }
        </script>
        <script>
                function fillGridOnEvent(type){
                    $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                    jQuery("#list").jqGrid({
                        url:'<%=request.getContextPath()%>/ContentAdminServlet?q=1&action=fetchData&status='+type,
                        datatype: "xml",
                        height: 250,
                        
                        colNames:['Sl. <br/> No.','Registration Type','Company Name','e-mail ID',"Country","Dungkhag / Sub-district","Registration Date and Time","Action"],
                        colModel:[
                            {name:'srNo',index:'srNo', width:45,sortable:false,align:'center'},
                            {name:'registrationType',index:'registrationType', width:130},
                            {name:'companyName',index:'companyName', width:180},                             
                            {name:'emailId',index:'emailId', width:200},
                            {name:'country',index:'tm.country', width:80,align:'center'},
                            {name:'state',index:'tm.state', width:80,align:'center'},
//                            {name:'city',index:'tm.city', width:80,align:'center'},
                            {name:'registeredDate',index:'registeredDate', width:120,align:'center'},
                            {name:'userid',index:'userid', width:130,sortable:false,align:'center'}
                        ],
                        multiselect: false,
                        paging: true,
                        rowNum:10,
                        rowList:[10,20,30],
                        pager: $("#page"),
                        sortable:false,
                        caption: " ",
                        gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                    }).navGrid('#page',{edit:false,add:false,del:false,search:false});                    
                }
                
                function fillGridOnRejectionEvent(type){
                    $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                    jQuery("#list").jqGrid({
                        url:'<%=request.getContextPath()%>/ContentAdminServlet?q=1&action=fetchData&status='+type,
                        datatype: "xml",
                        height: 250,
                        
                        colNames:['Sl. No.','Registration Type','Company Name','Cause of Rejection',"Country","Dungkhag / Sub-district","Registration Date and Time"],
                        colModel:[
                            {name:'srNo',index:'srNo', width:45,sortable:false,align:'center'},
                            {name:'registrationType',index:'registrationType', width:130},
                            {name:'companyName',index:'companyName', width:180},                             
                            {name:'comments',index:'emailId', width:250},
                            {name:'country',index:'tm.country', width:100,align:'center'},
                            {name:'state',index:'tm.state', width:80,align:'center'},
//                            {name:'city',index:'tm.city', width:80,align:'center'},
                            {name:'registeredDate',index:'registeredDate', width:150,align:'center'}
//                            {name:'userid',index:'userid', width:130,sortable:false,align:'center'}
                        ],
                        multiselect: false,
                        paging: true,
                        rowNum:10,
                        rowList:[10,20,30],
                        pager: $("#page"),
                        sortable:false,
                        caption: " ",
                        gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                    }).navGrid('#page',{edit:false,add:false,del:false,search:false});                    
                }
                jQuery().ready(function (){
                    //fillGrid();
                });                
        </script>
    </head>       
    <body onload="fillGridOnEvent('pending');hide();">
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="pageHead_1">
            <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('8');">Save as PDF</a></span>
            </div><br/>
                <!--Middle Content Table Start-->
                <%if("a".equals(request.getParameter("succ"))){out.print("<br/><div class=\"responseMsg successMsg\">User profile approved successfully.</div>");}
                else if("n".equals(request.getParameter("succ"))){out.print("<br/><div class=\"responseMsg successMsg\">Request for New Procurement Category approved successfully.</div>");}
                    else if("m".equals(request.getParameter("succ"))){out.print("<br/><div class=\"responseMsg successMsg\">User profile Notified for Modification.</div>");}
                    else if("r".equals(request.getParameter("succ"))){out.print("<br/><div class=\"responseMsg successMsg\">Request for New Procurement Category rejected successfully.</div>");}%>
                <div class="contentArea_1">
                    <form method="post"><%--action="CompanyVerification.jsp" method="post"--%>
                    
                        <div  class="formBg_1 t_space">
                            <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                            <table cellspacing="10" class="formStyle_1" width="100%" id="tblSearchBox">
                            <%--<tr>
                                <td colspan="4" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                            </tr>--%>
                            <tr>
                                <td class="ff">Registration Type : <%--<span>*</span>--%></td>
                                <td><select name="registrationType" class="formTxtBox_1" id="cmbRegType" style="width:200px;">
                                        <option value="all">All</option>
                                        <option value="contractor">Bidder / Consultant</option>
                                        <option value="individualconsultant">Individual Consultant</option>
                                        <!--<option value="govtundertaking">Government owned Enterprise</option>-->
                                        <!--<option value="media">Media</option>-->
                                    </select></td>
                                    <td class="ff" style="display: none">Company Registration Number : <%--<span>*</span>--%></td>
                                <td style="display: none"><input name="companyRegNumber" type="text" class="formTxtBox_1" id="txtCompNo" style="width:200px;"/>
                                    <span id="comMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Company Name : <%--<span>*</span>--%></td>
                                <td><input name="companyName" type="text" class="formTxtBox_1" id="txtCompName" style="width:200px;"/></td>
                                <td class="ff">e-mail ID : <%--<span>*</span>--%></td>
                                <td><input name="emailId" type="text" class="formTxtBox_1" id="txtMail" style="width:200px;" />
                                    <span id="mailMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Registration Date From : <%--<span>*</span>--%></td>
                                <td><input name="regtoDateStr" class="formTxtBox_1" id="txtRegFromDate" type="text" style="width:100px;" onfocus="GetCal('txtRegFromDate','txtRegFromDate');" readonly/>
                                    <a href="javascript:void(0);"  title="Calender" onclick="GetCal('lastResponseDate','lastResponseDate');" >
                                <img src="../resources/images/Dashboard/calendarIcn.png" id="imgfield_1" style="vertical-align:middle;" border="0" onclick="GetCal('txtRegFromDate','imgfield_1');" /></a>
                                </td>
                                <td class="ff">Registration Date To : <%--<span>*</span>--%></td>
                                <td><input name="regfromDateStr" type="text" class="formTxtBox_1" type="text" id="txtRegToDate" style="width:100px;"  onfocus="GetCal('txtRegToDate','txtRegToDate');" readonly/>
                                <a href="javascript:void(0);"  title="Calender" onclick="GetCal('lastResponseDate','lastResponseDate');" >
                                <img src="../resources/images/Dashboard/calendarIcn.png" id="imgfield_2" style="vertical-align:middle;" border="0" onclick="GetCal('txtRegToDate','imgfield_2');" /></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Status : </td>
                                <td>
                                    <select name="status" class="formTxtBox_1" id="cmbstatus" style="width: 200px" onchange="filllstatus();">
                                        <option value="pending">Pending</option>
                                        <option value="approved">Approved</option>
                                        <!--<option value="rejected">Rejected</option>--> 
                                        <option value="reapply">Request for New Procurement Category</option> 
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                    <br/>
                                    <label class="formBtn_1">
                                        <input type="button" name="button" id="button" value="Search User" onclick="fillGrid();" />
                                    </label>
                                    &nbsp;
                                    <label class="formBtn_1">
                                        <input type="reset" name="Reset" id="buttonReset" value="Reset" onclick="test(1);"/>
                                    </label>
                                </td>
                            </tr>
                        </table>
                   </div>
                                &nbsp;
                </form>                                
                <ul class="tabPanel_1">
                    <li><a href="javascript:void(0);" id="linkPending" onclick="test(1);" class="sMenu">Pending</a></li>
                    <li><a href="javascript:void(0);" id="linkApproved" onclick="test(2);">Approved</a></li>
                    <!--<li><a href="javascript:void(0);" id="linkRejected" onclick="test(3);">Rejected</a></li>-->
                    <li><a href="javascript:void(0);" id="linkReapply" onclick="test(4);">Request for New Procurement Category</a></li>
                </ul>                          
                <div class="tabPanelArea_1">
                    <table cellspacing="0" cellpadding="0" class="t_space" style="margin: 0 auto;">
                    <tr>
                        <td>
                            <div id="jQGrid" class="">                        
                    </div>
                        </td>
                    </tr>
                    </table>
                </div>                
                </div>
                <!--For Generate PDF  Starts-->
                <form id="formstyle" action="" method="post" name="formstyle">
                    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                    <%
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                        String appenddate = dateFormat1.format(new Date());
                    %>
                    <input type="hidden" name="fileName" id="fileName" value="CompanyVerification_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="CompanyVerification" />
                </form>
                <!--For Generate PDF  Ends-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script type="text/javascript">
                var headSel_Obj = document.getElementById("headTabCompVerify");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
                </script>

    </body>
</html>
