<%-- 
    Document   : WSList
    Created on : Jun 6, 2011, 2:02:49 AM
    Author     : Sreenu
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblWsMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.WsMasterService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <title>Web Service Details</title>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.generatepdf.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <%
        boolean flag = false;
        String message = "";
        String mode = "";
        if (request.getParameter("mode") != null) {
            mode = request.getParameter("mode");
        } else {
            mode = "view";
        }
        if (request.getParameter("flag") != null) {
            if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
                flag = true;
            }
        }
        if (flag) {
            if (request.getParameter("message") != null) {
                message = request.getParameter("message");
                if (message.equalsIgnoreCase("add")) {
                    message = "Web Service created successfully";
                } else if (message.equalsIgnoreCase("edit")) {
                    message = "Web Service updated successfully";
                } else {
                    message = "Web Service status changed successfully";
                }
            }//request checking null
        }//flag

    %>

    <%        StringBuilder userType = new StringBuilder();
        if (request.getParameter("userType") != null) {
            if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                userType.append(request.getParameter("userType"));
            } else {
                userType.append("org");
            }
        } else {
            userType.append("org");
        }
    %>
    <script type="text/javascript">
        jQuery().ready(function () {
            jQuery("#list").jqGrid({
                url: "../WebServiceDetailsServlet",
                datatype: "xml",
                height: 250,
                colNames: ['Web-Service Name', 'Description', 'Status', 'Authentication', 'Action'],
                colModel: [
                    {name: 'webSerivceName', index: 'webSerivceName', width: 150, sortable: true, searchoptions: {sopt: ['eq', 'cn']}},
                    {name: 'wsDescription', index: 'wsDescription', width: 100, sortable: true, searchoptions: {sopt: ['eq', 'cn']}},
                    {name: 'wsStatus', index: 'wsStatus', width: 40, sortable: true, search: false, align: 'center'},
                    {name: 'wsAuthentication', index: 'wsAuthentication', width: 65, search: false, sortable: true, align: 'center'},
                    {name: 'wsAction', index: 'wsAction', width: 90, sortable: false, search: false, align: 'center'},
                ],
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum: 10,
                rowList: [10, 20, 30],
                pager: $("#page"),
                caption: "Web-Services",
                gridComplete: function () {
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
            }).navGrid('#page', {edit: false, add: false, del: false});//close Jquery.reay()
        });//close Jquery.ready()
        // <input type="button"  value="Tracking Page" onClick=window.location.href='tracking.jsp?devId=';>
    </script>
    <body>
        <% //String userType = "config";%>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType.toString()%>" ></jsp:include>
                            <td class="contentArea">
                                <div>
                                    <div class="pageHead_1">View Web Services
                                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('4');">Save as PDF</a></span>
                                    </div>
                                </div>
                            <%if (flag) {%>
                            <div id="successMsg" class="responseMsg successMsg" style="display:block; margin-top: 10px;">
                                <%=message%>
                            </div>
                            <%} else {%>
                            <div id="errMsg" class="responseMsg errorMsg" style="display:none; margin-top: 10px;">
                                <%=message%>
                            </div>
                            <%}%>
                            <div class="t-align-left ff formStyle_1 t_space">
                                To sort click on the relevant column header
                            </div>
                            <%--
                              <% if ("view".equalsIgnoreCase(mode)) {%>
                              <div align="right" class="t_space b_space">
                                  <a href="WSDetail.jsp" class="action-button-add" >Add Web Service</a>
                              </div>
                              <% }%>
                            --%>
                            <table id="list"></table>
                            <div id="page">
                            </div>
                        </td>
                    </tr>
                </table>
                <form id="formstyle" action="" method="post" name="formstyle">

                    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                    <%
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                        String appenddate = dateFormat1.format(new Date());
                    %>
                    <input type="hidden" name="fileName" id="fileName" value="WebServices_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="WebServices" />
                </form>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script>
            var obj = document.getElementById('lblWsDetailView');
            if (obj != null) {
                if (obj.innerHTML == 'View') {
                    obj.setAttribute('class', 'selected');
                }
            }

            var headSel_Obj = document.getElementById("headTabConfig");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>    
</html>
