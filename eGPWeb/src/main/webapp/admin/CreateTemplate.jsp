<%-- 
    Document   : CreateTemplate
    Created on : 24-Oct-2010, 1:03:35 AM
    Author     : yanki
--%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblScBankDevPartnerMaster"%>
<jsp:useBean id="schBankDevpartner" class="com.cptu.egp.eps.web.servicebean.ScBankPartnerAdminSrBean" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create SBD</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            function validateProcType(){
                $("#procType_tr").find(".reqF_1").remove();
                var txtProcType = $.trim($("#txtProcType").val());
                if(txtProcType == ""){
                    $("#txtProcType").parent().append("<div  class='reqF_1'>Please select Procurement Type</div>");
                    return false;
                    //elem = false;
                }
            }
        </script>
        <script>
            function validateSection(showMsg){
                var isOkOrNot = false;
                $("#stdName_tr").find(".reqF_1").remove();
                if($.trim($("#txtSection").val()) == "") {
                    $("#txtSection").val("");
                    $("#txtSection").parent().append("<div  class='reqF_1'>Please enter Name of SBD</div>");
                    return false;
                }else {
                    $("#txtSection").val($.trim($("#txtSection").val()));
                    //$(".reqF_1").remove();
                    $("#buttonSubmit").disabled = true;
                    if($("#txtSection").val() != ""){
                        if(eval(showMsg) == 1){
                        $('span.#msg').html("Checking for unique SBD Name...");
                        }
                        $.ajax({
                            url: "<%=request.getContextPath()%>/CreateTemplateSrBean",
                            data: {"txtSection": $.trim($("#txtSection").val()),
                                   "action": "varifyName" },
                            context: document.body,
                            type: 'POST',
                            success: function(msg){
                                if(msg == "valid") {
                                    $("#msg").html("<span style='color:green'>OK</span>");
                                    document.frmCreateTemplate.buttonSubmit.disabled = false;
                                    isOkOrNot = true;
                                } else if(msg == "invalid") {
                                    $("#msg").html("SBD name already exists");
                                    document.frmCreateTemplate.buttonSubmit.disabled = true;
                                    isOkOrNot = false;
                                } else {
                                    if(eval(showMsg) == 2){
                                        isOkOrNot = true;
                                }
                            }
                                return isOkOrNot;
                            }
                        });
                    }
                }
            }
        </script>
        <script>
            function validateSectionNo(){
                
                $("#stdNo_tr").find(".reqF_1").remove();
                var noOfSectionAfterTrim = $.trim($("#txtSectionNo").val());
                if(noOfSectionAfterTrim == ""){
                    $("#txtSectionNo").parent().append("<div  class='reqF_1'>Please enter No. of Sections Required</div>");
                    return false;
                    //elem = false;
                } else {
                    $("#stdNo_tr").find(".reqF_1").remove();
                    var ele = noOfSectionAfterTrim;
                    if(isNaN(ele)){
                        $("#stdNo_tr").find(".reqF_1").remove();
                        $("#txtSectionNo").parent().append("<div  class='reqF_1'>Please enter numeric value</div>");
                        return false;
                    }else {
                        if(eval(ele) > 99){
                            $("#stdNo_tr").find(".reqF_1").remove();
                            $("#txtSectionNo").parent().append("<div  class='reqF_1'>Allows maximum 2 digits</div>");
                            return false;
                        }else if(eval(ele) < 0){
                            $("#stdNo_tr").find(".reqF_1").remove();
                            $("#txtSectionNo").parent().append("<div  class='reqF_1'>Negative Values are not allowed</div>");
                            return false;
                        }
                        else{
                            if(ele.indexOf(".") == -1){
                                if(ele.indexOf("0") == 0){
                                    $("#stdNo_tr").find(".reqF_1").remove();
                                    $("#txtSectionNo").parent().append("<div  class='reqF_1'>Allows maximum 2 digits</div>");
                                    return false;
                                }else{
                                    $("#stdNo_tr").find(".reqF_1").remove();
                                    return true;
                                }
                            }else{
                                $("#stdNo_tr").find(".reqF_1").remove();
                                $("#txtSectionNo").parent().append("<div  class='reqF_1'>Allows maximum 2 digits</div>");
                                return false;
                            }
                        }
                    }
                }
            }
        </script>
        <script>
            function clearmsg(){
                document.frmCreateTemplate.buttonSubmit.disabled = false;
                document.getElementById('devpartner').style.display='none';
                 $("#procType_tr").find(".reqF_1").remove();
                 $("#stdName_tr").find(".reqF_1").remove();
                 $("#stdNo_tr").find(".reqF_1").remove();
                $("#msg").html("");
            }
        </script>
        <script>
            function validateData(){
                $("#stdName_tr").find(".reqF_1").remove();
                /*$("#stdName_tr").children().each(function(){
                    alert($(this).attr("id"));
                    if($(this).attr("class") == "reqF_1"){
                        $(this).remove();
                    }
                });*/
        
                var elem = true;
                if(validateProcType() == false){
                    elem = false;
                }

                var msgObj = document.getElementById("msg");
                if($("#msg").html() != "<span id=\"msg\" style=\"color: red;\">OK</span>"){
                
                    if(validateSection(2) == false){
                         elem = false;
                    }
                }
                if(validateSectionNo() == false){
                   
                    elem = false;
                }
                if(document.getElementById('rdoprocurmentRulesfollow2').checked){
                    if(document.getElementById('selDevPartner').value==""){
                        document.getElementById('msgDevpatner').innerHTML = "Please select Development Partner"
                       
                        elem = false;
                    }
                }
                return elem;
            }
        </script>
    </head>
    <body>
        <%
                    String logUserId = "0";
                    if (session.getAttribute("userId") != null) {
                        logUserId = session.getAttribute("userId").toString();
                    }
                    schBankDevpartner.setLogUserId(logUserId);
        %>
        <div class="dashboard_div">

            <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <div class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1">
                                    Create Standard Bidding Document
                                    <span style="float: right; text-align: right;">
                                        <a href="ListSTD.jsp" class="action-button-goback">Go Back</a>
                                    </span>
                                </div>
                            </div>
                <form  name ="frmCreateTemplate" id="frmCreateTemplate" action="<%=request.getContextPath()%>/CreateTemplateSrBean?action=insert" method="post">

                                <%
                                if (request.getParameter("er") != null) {
                                %>
                                <div class="t_space"><div class="responseMsg noticeMsg">Please Try Again</div></div>
                    <%                                            }
                                %>
                                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                    <tr>
                                        <td style="font-style: italic" width="80%" colspan="2" class="ff" align="left">Fields marked with (<span>*</span>) are mandatory.</td>
                                    </tr>
                                    <tr id ="procType_tr">
                                        <td width="20%" class="ff">Procurement Type : <span>*</span></td>
                                        <td width="80%">
                                            <select name="txtProcType" id="txtProcType" class="formTxtBox_1" style="width:205px;">
                                                <option value="">--Select--</option>
                                                <option value="goods">Goods</option>
                                                <option value="works">Works</option>
                                    <option value="srvcmp">Services - Consulting Firms</option>
                                    <option value="srvindi">Services - Individual Consultant</option>
                                    <option value="srvnoncon">Services - Non consulting services</option>
                                            </select>
                                            <span id="msgproc" style="color: red;"></span>
                                        </td>
                                    </tr>
                                    <tr id ="stdName_tr">
                                        <td width="20%" class="ff">Name of SBD : <span>*</span></td>
                            <td width="80%"><input name="txtSection" maxlength="120" type="text" class="formTxtBox_1" id="txtSection" style="width:200px;" onblur="validateSection(1)" />
                                            <span id="msg" style="color: red;"></span>
                                            <span id="msggreen" style="color: green;"></span>
                                        </td>
                                    </tr>
                                    <tr id ="stdNo_tr">
                                        <td class="ff">No. of Section Required : <span>*</span></td>
                                        <td><input name="txtSectionNo" type="text" maxlength="3" class="formTxtBox_1" id="txtSectionNo" style="width:200px;" onblur="validateSectionNo()" /></td>
                                    </tr>
                        <tr id ="stdNo_tr">
                            <td class="ff">Follows Procurement Rules of : <span>*</span></td>
                            <td>
                                <input type="radio" name="rdoprocurmentRulesfollow" id="rdoprocurmentRulesfollow1" value="Royal Government of Bhutan" checked="checked" class="formTxtBox_1" onclick="if(this.checked==true){ document.getElementById('devpartner').style.display='none';}" />&nbsp;Royal Government of Bhutan&nbsp;&nbsp;
                                <input type="radio" name="rdoprocurmentRulesfollow" id="rdoprocurmentRulesfollow2" value="Development Partner" class="formTxtBox_1" onclick="if(this.checked==true){ document.getElementById('devpartner').style.display='table-row';}" />&nbsp;Development Partner<br />
                            </td>
                        </tr>
                        <tr id="devpartner" style="display: none;">
                            <td class="ff">Development Partner : <span>*</span></td>
                            <td>
                                <select name="selDevPartner" id="selDevPartner" class="formTxtBox_1">
                                    <option value="">--Select--</option>
                                    <%
                                                List<TblScBankDevPartnerMaster> list = null;
                                                list = schBankDevpartner.getDevelopPartnerList();
                                                if (list.size() > 0) {
                                                    for (TblScBankDevPartnerMaster tblScBankDevPartnerMaster : list) {
                                    %>
                                    <option value="<%=tblScBankDevPartnerMaster.getSbankDevelopId()%>"><%=tblScBankDevPartnerMaster.getSbDevelopName()%></option>
                                    <%
                                                    }
                                                }
                                    %>
                                </select>
                                <span class="reqF_1" id="msgDevpatner"></span>
                            </td>
                        </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><label class="formBtn_1">
                                                <input type="submit" name="button" id="buttonSubmit" value="Submit" onclick="return validateData();" /></label>
                                            &nbsp;
                                            <label class="formBtn_1">
                                                <input type="reset" name="Reset" id="button" value="Reset" onclick="return clearmsg();" />
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </form>

                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
         <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>