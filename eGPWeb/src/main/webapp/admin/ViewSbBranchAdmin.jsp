<%-- 
    Document   : ViewSbBranchAdmin
    Created on : Apr 5, 2011, 3:02:37 PM
    Author     : nishit
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblUserActivationHistory"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="scBankPartnerAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ScBankPartnerAdminSrBean"/>
    <jsp:useBean id="partnerAdminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.PartnerAdminMasterDtBean"/>
    <jsp:setProperty name="partnerAdminMasterDtBean" property="*"/>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <%
                    byte usrTypeId = 0;
                    if (!request.getParameter("userTypeId").equals("")) {
                        usrTypeId = Byte.parseByte(request.getParameter("userTypeId"));
                    }
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        scBankPartnerAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                    }
                    String usrId = request.getParameter("userId");
                    if(request.getParameter("mode") != null && request.getParameter("mode").equalsIgnoreCase("view"))
                    {
                         scBankPartnerAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                    }
                    else
                    {
                         scBankPartnerAdminSrBean.setAuditTrail(null);
                    }
                    if (usrId != null && !usrId.equals("")) {
                        partnerAdminMasterDtBean.setUserId(Integer.parseInt(usrId));
                        partnerAdminMasterDtBean.setUserTypeId(usrTypeId);
                        partnerAdminMasterDtBean.populateInfo("Admin");
                    }

                    String from = request.getParameter("from");
                    if (from == null || from.equals("")) {
                        from = "Operation";
                    }
                    String partnerType = request.getParameter("partnerType");
                    String type = "";
                    String title = "";
                    String title1 = "";

                    if (partnerType != null && partnerType.equals("Development")) {
                        type = "Development";
                        title = "Development Partner";
                        title1 = "Development Partner";

                    }
                    else {
                        type = "ScheduleBank";
                        title = "Financial Institution";
                        title1 = "Financial Institution";

                    }
                     TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
                    List<TblUserActivationHistory> list = transferEmployeeServiceImpl.viewAccountStatusHistory(usrId + "");

                    //String gridURL = "ManageSbDpAdmin.jsp?partnerType=" + type;
                    String editURL = "EditSbBranchAdmin.jsp?userId=" + usrId + "&userTypeId=" + usrTypeId + "&partnerType=" + type;
                    String okURL = "ManageSbBranchAdmin.jsp";
                    String SbBranch = "ViewSbBrnchAdmin.jsp?userId=" + usrId + "&userTypeId=" + usrTypeId + "&partnerType=" + type;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <%if(Integer.parseInt(session.getAttribute("userTypeId").toString())!=15){%>
        <title><%=title%> Branch Admin : View</title>
        <%}else{%>
        <title><%=title%> Branch View Profile</title>
        <%}%>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

    </head>


    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <%if(Integer.parseInt(session.getAttribute("userTypeId").toString())==1||Integer.parseInt(session.getAttribute("userTypeId").toString())==7){%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                        <%}%>
                        <td class="contentArea_1">
                            <!--Page Content Start-->

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <!-- Success failure -->
                                    <!-- Result Display End-->

                                    <td>
                                        <%if(Integer.parseInt(session.getAttribute("userTypeId").toString())!=15){%>
                                        <div class="pageHead_1">View Financial Institution Branch Admin</div>
                                        <%}else{%>
                                        <div class="pageHead_1">View Profile</div>
                                        <%}%>
                                        <div class="t_space">
                                        <%
                                                String msg = request.getParameter("msg");
                                            if (msg != null && msg.equals("success")) {
                                        %>
                                                 <%if(Integer.parseInt(session.getAttribute("userTypeId").toString())==15){%>
                                                 <div class="responseMsg successMsg">Profile <%=from%> successfully</div>
                                            <%}else{%>
                                            <div class="responseMsg successMsg"><%=title1%> Branch Admin <%=from%> successfully</div>
                                            <%}%>
                                    <%}%>
                                        </div>
                                        <form id="frmSBAdmin" name="frmSBAdmin" method="post" action="">
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <%--<tr>
                                                    <td width="20%" class="ff">Role : </td>
                                                    <td>
                                                        <label id="role">${partnerAdminMasterDtBean.isMakerChecker}</label>
                                                    </td>
                                                </tr>--%>
                                                <tr>
                                                    <td width="25%" class="ff"><%=title1%> Branch : </td>
                                                    <td width="75%">
                                                        <label id="bankName">${partnerAdminMasterDtBean.sbDevelopName}</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">e-mail ID : </td>
                                                    <td>
                                                        <label id="txtMail">${partnerAdminMasterDtBean.emailId}</label>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td class="ff">Full Name :</td>
                                                    <td><label id="txtName">${partnerAdminMasterDtBean.fullName}</label>
                                                    </td>
                                                </tr>
                                                    <tr>
                                                    <td class="ff">CID No. : </td>
                                                    <td>
                                                        <label id="txtNationalId" >${partnerAdminMasterDtBean.nationalId}</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Mobile No. :</td>
                                                    <td>
                                                        <label id="txtMobileNo">${partnerAdminMasterDtBean.mobileNo}</label>
                                                    </td>
                                                </tr>
                                            <%if(Integer.parseInt(session.getAttribute("userTypeId").toString())==1||Integer.parseInt(session.getAttribute("userTypeId").toString())==7){%>
                                                <tr>
                                                    <td width="20%">&nbsp;</td>
                                                    <td width="80%" class="t-align-left" >
                                                        <a href="<%=okURL%>" class="anchorLink" style="text-decoration: none; color: #fff;">OK</a>
                                                        <a href="<%=editURL%>" class="anchorLink" style="text-decoration: none; color: #fff;">Edit</a>
                                                         <%if(list.size()>0){%>
                                                        <a href="<%=SbBranch%>" class="anchorLink" style="text-decoration: none; color: #fff;">View Account History</a>
                                                    <%}%>
                                                    </td>
                                                </tr>
                                            <%}%>
                                            </table>
                                            <%--<%if(Integer.parseInt(session.getAttribute("userTypeId").toString())==1||Integer.parseInt(session.getAttribute("userTypeId").toString())==7){%>
                                            <table width="100%" cellspacing="10" cellpadding="0" border="0">
                                                <tr>
                                                    <td width="20%">&nbsp;</td>
                                                    <td width="80%" class="t-align-left" >
                                                        <a href="<%=okURL%>" class="anchorLink">OK</a>
                                                        <a href="<%=editURL%>" class="anchorLink">Edit</a>
                                                         <%if(list.size()>0){%>
                                                        <a href="<%=SbBranch%>" class="anchorLink">View Account History</a>
                                                        <%}%>
                                                    </td>
                                                </tr>
                                            </table>
                                            <%}%>--%>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <%if(userTypeId==15){%>
            <script>
                var headSel_Obj = document.getElementById("headTabMyAcc");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            <%}else{%>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            <%}%>
    </body>
</html>
