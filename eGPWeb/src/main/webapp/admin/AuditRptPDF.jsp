<%-- 
    Document   : AuditRptPDF
    Created on : Nov 21, 2011, 4:45:51 PM
    Author     : nishit
--%>

<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />
<%@page import="com.cptu.egp.eps.service.serviceimpl.LoginReportImpl,com.cptu.egp.eps.dao.storedprocedure.LoginReportDtBean"%>
<html>
    <head>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Audit Trail Report</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        </head>
    <body>
         <%
                String typeOfUser = "";
                    if (request.getParameter("typeOfUser")!=null && !"".equals(request.getParameter("typeOfUser"))) {
                        typeOfUser = request.getParameter("typeOfUser");
                    }

                    String ipAddress = "";
                    if (request.getParameter("ipAddress")!=null && !"".equals(request.getParameter("ipAddress"))) {
                        ipAddress = request.getParameter("ipAddress");
                    }

                    String txtFromDate = null;
                    if (request.getParameter("txtFromDate")!=null && !"null".equals(request.getParameter("txtToDate")) && !"".equals(request.getParameter("txtFromDate"))) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        txtFromDate = dateFormat.format((Date)formatter.parse(request.getParameter("txtFromDate")));                    }

                    String tenderId = "";
                    if (request.getParameter("tenderId")!=null && !"".equals(request.getParameter("tenderId"))) {
                        tenderId = request.getParameter("tenderId");
                    }
                    String cmbTenderId = "";
                    if (request.getParameter("cmbTenderId")!=null && !"".equals(request.getParameter("cmbTenderId"))) {
                        cmbTenderId = request.getParameter("cmbTenderId");
                    }
                    String module = "";
                    if (request.getParameter("module")!=null && !"".equals(request.getParameter("module"))) {
                        module = request.getParameter("module");
                    }

                    String txtToDate = null;
                    if (request.getParameter("txtToDate")!=null && !"null".equals(request.getParameter("txtToDate")) && !"".equals(request.getParameter("txtFromDate"))) {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        txtToDate = dateFormat.format((Date)formatter.parse(request.getParameter("txtToDate")));                    }

                    String ipEmailId = "";
                    if (request.getParameter("ipEmailId")!=null && !"".equals(request.getParameter("ipEmailId"))) {
                        ipEmailId = request.getParameter("ipEmailId");
                    }

                    String activity = "";
                    if (request.getParameter("activity")!=null && !"".equals(request.getParameter("activity").trim())) {
                        activity = request.getParameter("activity");
                    }

                    String txtdepartmentid = "";
                    if (request.getParameter("txtdepartmentid")!=null && !"".equals(request.getParameter("txtdepartmentid"))) {
                        txtdepartmentid = request.getParameter("txtdepartmentid");
                    }

                    String procuringEntity = "";
                    if (request.getParameter("procuringEntity")!=null && !"".equals(request.getParameter("procuringEntity"))) {
                        procuringEntity = request.getParameter("procuringEntity");
                    }
                    String strPageNo = request.getParameter("strPageNo");
                    //System.out.println("Page No: " + strPageNo);
                    //int pageNo = Integer.parseInt(strPageNo);
                    String strOffset = request.getParameter("strOffset");
                    //int recordOffset = Integer.parseInt(strOffset);
                    String isPDF = "true";
                    
             
    %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Audit Trail Report</div>
            <div>&nbsp;</div>
            <table width="100%" cellspacing="0">
                <tr valign="top">
                    <td class="contentArea">
                        <div class="formBg_1 t_space">
                            <table cellspacing="10" class="formStyle_1" width="100%">
                                <%
                                    if(typeOfUser!=null && !"".equalsIgnoreCase(typeOfUser)){
                                %>
                                <tr>
                                    <td width="10%" class="ff">Type of user :</td>
                                    <td width="44%">
                                        <%if("0".equalsIgnoreCase(typeOfUser)){%>
                                            All
                                     <%}else if("2".equalsIgnoreCase(typeOfUser)){%>
                                            Bidder/Consultant
                                    <%}else if("6".equalsIgnoreCase(typeOfUser)){%>
                                            Development Partner User
                                    <%}else if("7".equalsIgnoreCase(typeOfUser)){%>
                                            Schedule Financial Institute User
                                    <%}else if("8".equalsIgnoreCase(typeOfUser)){%>
                                            Admin User
                                    <%}else if("3".equalsIgnoreCase(typeOfUser)){%>
                                            Government Users
                                    <% } %>
                                </tr>        
                                <%}if(module!=null && !"".equalsIgnoreCase(module)){%>
                                <tr>
                                <td width="10%" class="ff"><label id="lblModule">Module :</label></td>
                                <td width="44%"><%=module%></td>
                                </tr>
                                <%}if(ipAddress!=null && !"".equalsIgnoreCase(ipAddress)){%>
                                <tr>
                                <td width="10%" class="ff"><label id="lblModule">IP Address :</label></td>
                                <td width="44%"><%=ipAddress%></td>
                                </tr>
                                <%}if(txtFromDate!=null && !"".equalsIgnoreCase(txtFromDate)){%>
                                <tr>
                                <td width="10%" class="ff"><label id="lblModule">From Date :</label></td>
                                <td width="44%"><%=txtFromDate%></td>
                                </tr>
                                <%}if(txtToDate!=null && !"".equalsIgnoreCase(txtToDate)){%>
                                <tr>
                                <td width="10%" class="ff"><label id="lblModule">To Date :</label></td>
                                <td width="44%"><%=txtToDate%></td>
                                </tr>
                                <%}if(ipEmailId!=null && !"".equalsIgnoreCase(ipEmailId)){%>
                                <tr>
                                <td width="10%" class="ff"><label id="lblModule">e-mail ID :</label></td>
                                <td width="44%"><%=ipEmailId%></td>
                                </tr>
                                <%}if(activity!=null && !"".equalsIgnoreCase(activity)){%>
                                <tr>
                                <td width="10%" class="ff"><label id="lblModule">Activity :</label></td>
                                <td width="44%"><%=activity%></td>
                                </tr>
                                <%}if(cmbTenderId!=null && !"".equalsIgnoreCase(cmbTenderId) && tenderId!=null && !"".equalsIgnoreCase(tenderId) ){%>
                                <tr>
                                <td width="10%" class="ff"><label id="lblModule"><%=cmbTenderId%> :</label></td>
                                <td width="44%"><%=tenderId%></td>
                                </tr>
                               <%}%>
                               
                                
                            </table>
                        </div>
                    </td>
                </tr>
                <br/>
                <tr>
                    <td class="contentArea">
                         <table width="100%" cellspacing="0" class="tableList_1 t_space">
                           <tr>
                            <th width="4%" class="t-align-center">Sl. <br/>No.</th>
                            <th width="10%" class="t-align-center">e-mail ID</th>
                            <th width="11%" class="t-align-center">User Name</th>
                            <th width="10%" class="t-align-center">Ministry / Division / Organization / PA's Name, Procuring Entity</th>
                            <th width="10%" class="t-align-center">Log In Date<br/>and Time</th>
                            <th width="10%" class="t-align-center">Log Out<br/>Date<br/>and Time</th>
                            <th width="10%" class="t-align-center">IP Address</th>
                            <th width="10%" class="t-align-center">Module<br/>Name</th>
                            <th width="10%" class="t-align-center">Page URL Address</th>
                            <th width="15%" class="t-align-center">Activity</th>
                        </tr>
                           <% 
                             CommonSearchDataMoreService commonSearchDataMoreService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                    List<SPCommonSearchDataMore> auditTrail = commonSearchDataMoreService.geteGPDataMore("GeteGPAuditTrail", strPageNo, strOffset, typeOfUser, ipAddress, txtFromDate, txtToDate, ipEmailId, activity, txtdepartmentid, procuringEntity,tenderId,cmbTenderId,module);
                    int i = 0;
                    if(auditTrail!=null && !auditTrail.isEmpty()){
                            for(SPCommonSearchDataMore auditTrailList :  auditTrail){ 
                                i++;
                                StringBuffer activityShow = new StringBuffer();
                                activityShow.append(Character.toUpperCase(auditTrailList.getFieldName10().charAt(0))).append(auditTrailList.getFieldName10().replace("Id", " Id").substring(1));
                                activityShow.append(" : " + auditTrailList.getFieldName9() + "<br/>");
                            activityShow.append("Action : " + auditTrailList.getFieldName11() + "<br/>");
                            activityShow.append("Action Date : " + auditTrailList.getFieldName12() + "<br/>");
                            if (auditTrailList.getFieldName13() != null && !"".equals(auditTrailList.getFieldName13())) {
                                activityShow.append("Remarks : " + auditTrailList.getFieldName13() + "<br/>");
                            }
                    %>
                            <tr>
                                <td class="t-align-center"><%= i %></td>
                                <td class="t-align-left"><%= auditTrailList.getFieldName1() %></td>
                                <td class="t-align-center"><%= auditTrailList.getFieldName2()  %></td>
                                <td class="t-align-center"><%= auditTrailList.getFieldName3() %></td>
                                <td class="t-align-center"><%= auditTrailList.getFieldName4() %></td>
                                <td class="t-align-center"><%= auditTrailList.getFieldName5() %></td>
                                <td class="t-align-center"><%= auditTrailList.getFieldName6() %></td>
                                <td class="t-align-center"><%= auditTrailList.getFieldName7() %></td>
                                <td class="t-align-center"><%= auditTrailList.getFieldName8() %></td>
                                <td class="t-align-center"><%= activityShow %></td>
                            </tr>
                           <%
                                if(auditTrailList!=null){
                                    auditTrailList = null;
                                }
                            }
                           }else{
                            out.print("<tr>");
                            out.print("<td colspan=\"10\" id=\"noRecordFound\" value=\"noRecordFound\" class=\"t-align-center\" style=\"color: red;font-weight: bold\"> No Record Found. Please Enter Proper Search Criteria and Try  </td>");
                            out.print("</tr>");
                           }
                           %>
                        </table>
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>

            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>
<%
if(auditTrail!=null){
    auditTrail.clear();
    auditTrail = null;
}
%>
