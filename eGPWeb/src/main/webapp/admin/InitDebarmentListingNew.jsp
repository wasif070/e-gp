<%-- 
    Document   : InitDebarmentListingNew
    Created on : Jun 16, 2016, 12:42:12 PM
    Author     : Nafiul
--%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.BiddingPermissionService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.DebarredBiddingPermissionService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.web.servlet.BOQServlet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            Debarment
        </title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />


        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />


        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>


        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <script>
            function GetCal(txtname, controlname) {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat: "%Y-%m-%d",
                    onSelect: function () {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>

        <%
            String companyId = request.getParameter("companyId");
            String debarredUser = request.getParameter("debarredUser");
            CommonSearchService commonService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            DebarredBiddingPermissionService debar = (DebarredBiddingPermissionService) AppContext.getSpringBean("DebarredBiddingPermissionService");
            BiddingPermissionService nonDebar = (BiddingPermissionService) AppContext.getSpringBean("BiddingPermissionService");
            String msg = "";
            //save data

            if ("Save".equalsIgnoreCase(request.getParameter("btnSave"))) {
//                msg = nonDebar.updateNonDebarredBiddingList(companyId, debarredUser, request.getParameterValues("nonDebarBidPermissionId"), request.getParameterValues("nonDebarCategory"), request.getParameterValues("nonDebarWorkType"), request.getParameterValues("nonDebarWorkCategory"), request.getParameterValues("nonDebarStatus"), request.getParameterValues("nonDebarReapplied"));
            }
            if (msg.equalsIgnoreCase("success")) {
//                msg = debar.updateDebarredBiddingList(companyId, debarredUser, request.getParameterValues("debarBidPermissionId"), request.getParameterValues("debarCategory"), request.getParameterValues("debarWorkType"), request.getParameterValues("debarWorkCategory"), request.getParameterValues("debarStatus"), request.getParameterValues("debarReapplied"), request.getParameterValues("debarUpto"), request.getParameterValues("debarFrom"), request.getParameterValues("comments"));
            }

            List<SPCommonSearchData> list = new ArrayList<SPCommonSearchData>();
            list = commonService.searchData("GetUserDebarredData", debarredUser, companyId, null, null, null, null, null, null, null);
            List<SPCommonSearchData> userInfo = new ArrayList<SPCommonSearchData>();
            userInfo = commonService.searchData("GetDebarUserInfo", debarredUser, companyId, null, null, null, null, null, null, null);


        %>


    </head>
    <body>
        <form name="frmsubmit" id="frmsubmit" method="post"></form>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="contentArea_1">
                <div class="pageHead_1">
                    Debarment
                </div>

                <div style="width: 99%;" class="tabPanelArea_2 t_space">
                    <div class="formBg_1 t_space">
                        <table cellspacing="10" class="formStyle_1" width="100%">
                            <tr>
                                <td width="20%" class="ff">Email Id :</td>
                                <td width="30%">
                                    <%=userInfo.get(0).getFieldName2()%>
                                </td>
                                <td width="20%" class="ff">Company Name : </td>
                                <td width="30%">
                                    <%=userInfo.get(0).getFieldName1()%>
                                </td>
                            </tr>

                        </table>
                        <br/><br/>
                    </div>
                    <form method="POST" id="frmInitDebar" action="InitDebarmentListingNew.jsp?companyId=<%=companyId%>&debarredUser=<%=debarredUser%>">
                        <div class="tabPanelArea_1">
                            <div align="center">
                                <table width="100%" cellspacing="0" id="resultTableNonDebarred" class="tableList_3 t_space">
                                    <thead>
                                        <tr>
                                            <th width="7%" class="t-align-center">Select All <input type="checkbox" onclick="checkUncheck('map', this);" id="mapallchk"/></th>
                                            <th width="20%" class="t-align-left">Category</th>
                                            <th width="25%" class="t-align-left">Work Type</th>
                                            <th width="20%" class="t-align-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <%
                                            if (list != null && !list.isEmpty()) {
                                                for (int i = 0; i < list.size(); i++) {
                                                    if (list.get(i).getFieldName7().equalsIgnoreCase("Non Debarred")) {
                                                        String workType = "-";
                                                        String work = "";
                                                        String wCategory = "";
                                                        if (list.get(i).getFieldName3() != null && !list.get(i).getFieldName3().equalsIgnoreCase("")) {
                                                            workType = list.get(i).getFieldName3();
                                                            work = list.get(i).getFieldName3();
                                                            if (list.get(i).getFieldName4() != null && !list.get(i).getFieldName4().equalsIgnoreCase("")) {
                                                                workType += " (" + list.get(i).getFieldName4() + ")";
                                                                wCategory = list.get(i).getFieldName4();
                                                            }
                                                        }
                                        %>                                
                                        <tr>
                                            <td class="t-align-center"><input name="nonDebarBidPermissionId" type="checkbox" value="<%=list.get(i).getFieldName1()%>" id="chkUnmap<%=(i + 1)%>" chk="map" /></td>
                                            <td class="t-align-left"><input type="hidden" name="nonDebarCategory" value="<%=list.get(i).getFieldName2()%>" /><%=list.get(i).getFieldName2()%></td>
                                            <td class="t-align-left"><input type="hidden" name="nonDebarWorkType" value="<%=work%>" /><input type="hidden" name="nonDebarWorkCategory" value="<%=wCategory%>" /><%=workType%></td>
                                            <td class="t-align-center"><input type="hidden" name="nonDebarStatus" value="<%=list.get(i).getFieldName7()%>" />
                                                <input type="hidden" name="nonDebarReapplied" value="<%=list.get(i).getFieldName10()%>" /><span> <%=list.get(i).getFieldName7()%> </span></td>
                                        </tr>
                                        <%
                                                }
                                            }
                                        } else {
                                        %>
                                        <tr>
                                            <td colspan="4" id="noRecordFound" value="noRecordFound" class="t-align-center" style="color: red;font-weight: bold"> No Records Found</td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                    </tbody>
                                </table>
                            </div>
                            <span id="msgDebarList" class="reqF_2"></span>
                        </div>

                        <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">

                            <tr>
                                <td width="10%" class="ff">Debar From :</td>
                                <td width="90%"><input type="text" onblur="return DateBlank(this);" value="" readonly="readonly" style="width:100px;" id="textfield_2" class="formTxtBox_1" name="textfield_2" />
                                    <a onclick="GetCal('textfield_2', 'textfield_2');" title="Calender" href="javascript:void(0);">
                                        <img border="0" onclick="GetCal('textfield_2', 'imgfield_2');" style="vertical-align:middle;" id="imgfield_2" alt="add Date" src="../resources/images/Dashboard/calendarIcn.png">
                                    </a><span style="color: red;" id="spDate_1"></span>
                                    <span id="msgDebarFrom" class="reqF_2"></span>
                                </td>

                            </tr>
                            <tr>
                                <td width="10%" class="ff">Debar Upto :</td>
                                <td width="90%"><input type="text" onblur="return DateBlank(this);" value="" readonly="readonly" style="width:100px;" id="textfield_1" class="formTxtBox_1" name="textfield_1" />
                                    <a onclick="GetCal('textfield_1', 'textfield_1');" title="Calender" href="javascript:void(0);">
                                        <img border="0" onclick="GetCal('textfield_1', 'imgfield_1');" style="vertical-align:middle;" id="imgfield_1" alt="add Date" src="../resources/images/Dashboard/calendarIcn.png">
                                    </a><span style="color: red;" id="spDate_2"></span>
                                    <span id="msgDebarTo" class="reqF_2"></span>
                                </td>
                            </tr>
                            <tr>
                                <td width="10%" class="ff">Grounds :</td>
                                <td width="90%">
                                    <textarea name="textfield_3" rows="4" class="formTxtBox_1" id="textfield_3" maxlength="250" style="width:400px;"></textarea>
                                    <span id="msgDebarComments" class="reqF_2"></span>
                                </td>   
                            </tr>
                            <tr>
                                <td width="10%">&nbsp;</td>
                                <td width="20%"><label class="formBtn_1"><input type="button" name="add" id="btnAdd" value="Add Debarment" onClick="AddToDebarredList();"/></label></td>   
                            </tr>
                        </table>
                        <div class="tabPanelArea_1">
                            <div align="center">
                                <table width="100%" cellspacing="0" id="resultTableDebarred" class="tableList_3 t_space">
                                    <thead>
                                        <tr>
                                            <th width="10%" class="t-align-center">Select All <input type="checkbox" onclick="checkUncheckUnmap('map', this);" id="unmapallchk"/></th>
                                            <th width="10%" class="t-align-left">Category</th>
                                            <th width="15%" class="t-align-left">Work Type</th>
                                            <th width="10%" class="t-align-center">Status</th>
                                            <th width="10%" class="t-align-center">Debarred From</th>
                                            <th width="10%" class="t-align-center">Debarred To</th>                                            
                                            <th width="40%" class="t-align-center">Comments</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            int flag = 0;
                                            if (list != null && !list.isEmpty()) {
                                                for (int i = 0; i < list.size(); i++) {
                                                    if (list.get(i).getFieldName7().equalsIgnoreCase("Debarred")) {
                                                        flag = 1;
                                                        String workType = "-";
                                                        String work = "";
                                                        String wCategory = "";
                                                        if (list.get(i).getFieldName3() != null && !list.get(i).getFieldName3().equalsIgnoreCase("")) {
                                                            workType = list.get(i).getFieldName3();
                                                            work = list.get(i).getFieldName3();
                                                            if (list.get(i).getFieldName4() != null && !list.get(i).getFieldName4().equalsIgnoreCase("")) {
                                                                workType += " (" + list.get(i).getFieldName4() + ")";
                                                                wCategory = list.get(i).getFieldName4();
                                                            }
                                                        }
                                        %>                                
                                        <tr>
                                            <td class="t-align-center"><input name="debarBidPermissionId" type="checkbox" value="<%=list.get(i).getFieldName1()%>" id="chkUnmap<%=i + 1%>" chk="map" /></td>
                                            <td class="t-align-left"><input type="hidden" name="debarCategory" value="<%=list.get(i).getFieldName2()%>" /><%=list.get(i).getFieldName2()%></td>
                                            <td class="t-align-left"><input type="hidden" name="debarWorkType" value="<%=work%>" /><input type="hidden" name="debarWorkCategory" value="<%=wCategory%>" /><%=workType%></td>
                                            <td class="t-align-center"><input type="hidden" name="debarStatus" value="<%=list.get(i).getFieldName7()%>" />
                                                <input type="hidden" name="debarReapplied" value="<%=list.get(i).getFieldName10()%>" /> <span> <%=list.get(i).getFieldName7()%> </span></td>
                                            <td class="t-align-center"><input type="hidden" name="debarFrom" value="<%=list.get(i).getFieldName11()%>" /><%=list.get(i).getFieldName11()%></td>
                                            <td class="t-align-center"><input type="hidden" name="debarUpto" value="<%=list.get(i).getFieldName8()%>" /><%=list.get(i).getFieldName8()%></td>
                                            <td class="t-align-center"><input type="hidden" name="comments" value="<%=list.get(i).getFieldName12()%>" /><%=list.get(i).getFieldName12()%></td>
                                        </tr>
                                        <%
                                                    }
                                                }
                                            } 
                                        if(flag == 0){
                                        %>
                                        <tr>
                                            <td colspan="7" id="noRecordFound" value="noRecordFound" class="t-align-center" style="color: red;font-weight: bold"> No Records Found</td>
                                        </tr>
                                        <%
                                            }
                                        %>
                                    </tbody>
                                </table>
                            </div>
                            <br/>
                            <div class="ff">
                                <label class="formBtn_1"><input type="button" name="ClearTbl" id="btnClear" value="Remove Debarment" onClick="RemoveFromDebarredList();"/></label>
                                <span id="msgDebarredList" class="reqF_2"></span>
                            </div>
                        </div>
                        <br/>

                        <br/>
                        <div style="display: table; margin: 0 auto;">
                            <label class="formBtn_1"><input type="Submit" name="btnSave" id="btnSave" value="Save" /></label>
                            <span>&nbsp;&nbsp;&nbsp;</span>
                            <label class="formBtn_1"><input type="button" name="Reset" id="button" value="Reset" onclick="resetMe()"/></label>
                        </div>
                    </form>
                </div>
            </div>

            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <script>
            function checkUncheck(val, chkall) {
                $("#resultTableNonDebarred tbody tr td input:checkbox[chk='" + val + "']").each(function () {
                    if ($(chkall).attr('checked')) {
                        $(this).attr('checked', true);
                    } else {
                        $(this).removeAttr('checked');
                    }
                });
            }
            function checkUncheckUnmap(val, chkall) {
                $("#resultTableDebarred tbody tr td input:checkbox[chk='" + val + "']").each(function () {
                    if ($(chkall).attr('checked')) {
                        $(this).attr('checked', true);
                    } else {
                        $(this).removeAttr('checked');
                    }
                });
            }
            function AddToDebarredList() {
                $('#msgDebarTo').html(' ');
                $('#msgDebarFrom').html(' ');
                $('#msgDebarComments').html(' ');
                $('#msgDebarList').html(' ');

                if ($("#textfield_2").val() == "") {
                    $('#msgDebarFrom').html('<br/>Please enter Date');
                    return;
                }
                if ($("#textfield_1").val() == "") {
                    $('#msgDebarTo').html('<br/>Please enter Date');
                    return;
                }
                if ($("#textfield_3").val() == "") {
                    $('#msgDebarComments').html('<br/>Please enter Grounds');
                    return;
                }

                var isSelect = 0;
                $("#resultTableNonDebarred tbody tr td input[id^='chkUnmap']").each(function () {
                    if ($(this).attr('checked')) {
                        var flag = 1;
                        var officerId = $(this).val();
                        $("#resultTableDebarred tbody tr td input[id^='chkUnmap']").each(function () {
                            if ($(this).val() == officerId)
                                flag = 0;
                        });
                        if (flag == 1) {
                            
                            $("tr").find("td[id = 'noRecordFound']").closest("tr").remove();
                            var row = $(this).closest('tr').clone();
                            row.append("<td class='t-align-center'><input type='hidden' name='debarUpto' value=" + $("#textfield_1").val() + " />" + $("#textfield_1").val() + "</td>");
                            row.append("<td class='t-align-center'><input type='hidden' name='debarFrom' value=" + $("#textfield_2").val() + " />" + $("#textfield_2").val() + "</td>");
                            row.append("<td class='t-align-center'><input type='hidden' name='comments' value=" + $("#textfield_3").val() + " />" + $("#textfield_3").val() + "</td>");
                            row.find("td input[name = 'nonDebarBidPermissionId' ]").attr('name', 'debarBidPermissionId');
                            row.find("td input[name = 'nonDebarCategory' ]").attr('name', 'debarCategory');
                            row.find("td input[name = 'nonDebarWorkType' ]").attr('name', 'debarWorkType');
                            row.find("td input[name = 'nonDebarWorkCategory' ]").attr('name', 'debarWorkCategory');
                            row.find("td input[name = 'nonDebarStatus' ]").closest("td").find("span").html('Debarred');
                            row.find("td input[name = 'nonDebarStatus' ]").attr('name', 'debarStatus');
                            row.find("td input[name = 'nonDebarReapplied' ]").attr('name', 'debarReapplied');

                            $('#resultTableDebarred tbody').append(row);
                            isSelect = 1;
                            $(this).closest('tr').remove();
                        } else if (flag == 0) {
                            $(this).closest('tr').remove()
                        }
                        ;
                    }
                });
                if (isSelect == 0) {
                    $('#msgDebarList').html('<br/>Please Select Bidding Permission');
                }
            }

            function RemoveFromDebarredList() {
                
                var flag = false;
                $("#resultTableDebarred tbody tr td input[id^='chkUnmap']").each(function () {
                    if ($(this).attr('checked')) {
                        $("tr").find("td[id = 'noRecordFound']").closest("tr").remove();
                        var row = $(this).closest('tr').clone();
                        row.find("td input[name='debarUpto']").closest("td").remove();
                        row.find("td input[name='debarFrom']").closest("td").remove();
                        row.find("td input[name='comments']").closest("td").remove();
                        row.find("td input[name = 'debarBidPermissionId' ]").attr('name', 'nonDebarBidPermissionId');
                        row.find("td input[name = 'debarCategory' ]").attr('name', 'nonDebarCategory');
                        row.find("td input[name = 'debarWorkType' ]").attr('name', 'nonDebarWorkType');
                        row.find("td input[name = 'debarWorkCategory' ]").attr('name', 'nonDebarWorkCategory');
                        row.find("td input[name = 'debarStatus' ]").closest("td").find("span").html('Non Debarred');
                        row.find("td input[name = 'debarStatus' ]").attr('name', 'nonDebarStatus');
                        row.find("td input[name = 'debarReapplied' ]").attr('name', 'nonDebarReapplied');
                        $('#resultTableNonDebarred tbody').append(row);
                        flag = true;
                        $(this).closest('tr').remove();
                    }
                });
                if (flag) {
                    $('#msgDebarredList').html('');
                    $("#resultTableDebarred tbody tr td input[id^='chkUnmap']").each(function () {
                        $(this).removeAttr('checked');
                    });
                    $("#unmapallchk").removeAttr('checked');
                } else {
                    $('#msgDebarredList').html('<br/>Please select Bidding Permission');
                }
            }

            $('#frmInitDebar').submit(function () {
                $("#resultTableNonDebarred tbody tr td input[id^='chkUnmap']").each(function () {
                    $(this).attr('checked', true);
                });
                $("#resultTableDebarred tbody tr td input[id^='chkUnmap']").each(function () {
                    $(this).attr('checked', true);
                });
                return true;
            });

            function resetMe() {
                document.getElementById('frmsubmit').action = window.location;
                //document.getElementById('frmsubmit').method = 'get';
                document.getElementById('frmsubmit').submit();
            }

        </script>
        <script>
            function DateBlank(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                var chk = true;
                if (obj.value != null)
                {
                    if (obj.value != '')
                    {
                        document.getElementById("spDate_" + i).innerHTML = "";
                        return true;
                    }
                }
            }
        </script>
        <script>
            var headSel_Obj = document.getElementById("headTabDebar");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
