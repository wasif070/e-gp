<%-- 
    Document   : ReinstateDebarment
    Created on : Aug 9, 2016, 1:56:48 PM
    Author     : NAHID
--%>

<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblDebarmentTypes"%>
<%@page import="com.cptu.egp.eps.web.servicebean.InitDebarmentSrBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.dao.storedprocedure.UserApprovalBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData,java.util.List,java.util.Calendar,com.cptu.egp.eps.service.serviceimpl.ContentAdminService" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Initiate Debarment Process</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript">

            $(function () {
                $('#btnSearch').click(function () {

                    $('#dis').css("display", "none");
                    if ($('#cmbSearchBy').val() == 'emailId') {

                        txt = $('#txtSearchVal').val();
                        if ($('#txtSearchVal').val() == "") {
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('Please enter e-mail ID.')
                            return false;
                        }

                        var SearchByval = $('#cmbSearchBy').val();
                        var val = EmailCheck(txt)

                        if (val == true) {
                            $('#SearchValError').html('')
                            $('#dis').css("display", "none");
                            //return true;
                        } else {
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('<div class="reqF_1"> Please enter valid e-mail ID.</div>')
                            return false;
                        }
                    } else if ($('#cmbSearchBy').val() == 'companyName') {

                        if ($('#txtSearchVal').val() == "") {
                            $('#dis').css("display", "table-row");
                            $('#SearchValError').html('Please enter Company Name.')
                            return false;
                        }
                    }

                    /*else if($('#cmbSearchBy').val()=='companyRegNumber'){
                     if($('#txtSearchVal').val()==""){
                     $('#dis').css("display", "table-row");
                     $('#SearchValError').html('Please enter Registration No.')
                     return false;
                     }
                     }*/
                });
            });

            function EmailCheck(value) {
                //alert('in');
                //alert(value);
                var c = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(value);
                //alert(c);
                return c;

            }
            function GetCal(txtname, controlname) {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: false,
                    dateFormat: "%d/%m/%Y",
                    onSelect: function () {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            function validate() {

                $(".err").remove();

                var valid = true;
                if ($("#companyName").val() == "") {
                    $("#companyName").parent().append("<div class='err' style='color:red;'>Please select Company Name</div>");
                    valid = false;
                }
                if ($("#grounds").val() == "") {
                    $("#grounds").parent().append("<div class='err' style='color:red;'>Please enter Reinstate Reasons</div>");
                    valid = false;
                } else {
                    if ($("#grounds").val().length > 2000) {
                        $("#grounds").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters allowed</div>");
                        valid = false;
                    }
                }
                
                if (!valid) {
                    return false;
                }
            }


            function LoadWorkCategory()
            {
                $.post("<%=request.getContextPath()%>/InitDebarment", {objectId: $('#companyName').val(), action: 'loadWorkCategoryReinstate'}, function (j) {
                    $('#lvlworkCategory').html(j.ProcCat);
                    $('#debarmentType').html(j.debarReason);
                    $('#clarification').html(j.grounds);
                    $('#textfield_2').html(j.debarFrom);
                    $('#textfield_1').html(j.debarTo);
                    $('#alreadyDebarred').html(j.alreadyDebar);
                }, "json");
            }

        </script>
    </head>
    <body>
        <input type="hidden" value="<%=DateUtils.formatStdDate(new java.util.Date())%>" name="currDate" id="hdnCurrDate"/>
        <div class="mainDiv">
            <div class="fixDiv">
                <div class="contentArea_1">

                    <!--Dashboard Header Start-->
                    <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                    <!--Dashboard Header End-->
                    <!--Dashboard Content Part Start-->

                    <%                        
                        InitDebarmentSrBean debarmentSrBean = new InitDebarmentSrBean();
                        java.util.List<SPTenderCommonData> list1 = new java.util.ArrayList<SPTenderCommonData>();
                        if ("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {
                            list1 = debarmentSrBean.searchCompanyForDebar(request.getParameter("searchBy"), request.getParameter("searchVal"));
                        }
                        //List<TblDebarmentTypes> debarTypes = debarmentSrBean.getWorkCategory();
                    %>
                    <!--            <div class="pageHead_1">Initiate Debarment Process<span style="float:right;"> <a class="action-button-goback" href="DebarmentListing.jsp">Go back</a> </span></div><br/>-->
                    <% 
                        if (request.getParameter("flag") != null) {
                            String flag = request.getParameter("flag");
                            if (flag.equalsIgnoreCase("fail")) {
                                out.println("<div class='responseMsg errorMsg'>Error Initiating Reinstate Process</div><br/>");
                            }
                            if (flag.equalsIgnoreCase("success")) {
                                out.println("<div class='responseMsg successMsg'>Reinstate Process Successful</div><br/>");
                            }
                        }
                    %>
                    <div class="formBg_1">
                        <form  method="post" id="search" name="search" action="ReinstateDebarment.jsp">
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td class="ff" width="10%">Search by   : <span>*</span></td>
                                    <td width="90%"><select name="searchBy" class="formTxtBox_1" id="cmbSearchBy" style="width: 200px;">
                                            <!--<option value="companyRegNumber">Registration No</option>-->
                                            <option value="companyName">Company Name</option>
                                            <option value="emailId" selected>e-mail ID</option>
                                        </select>&nbsp;
                                        <input name="searchVal" type="text" class="formTxtBox_1" id="txtSearchVal" style="width:200px;"/>&nbsp;
                                        <label class="formBtn_1">
                                            <input type="submit" name="btnSearch" id="btnSearch" value="Search" />
                                        </label>
                                        <%if ("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {
                                                if (list1.isEmpty()) {
                                                    out.print("<span style='color : red;font-weight:bold;'>&nbsp;No matching records found.</span>");
                                                }
                                            }%>
                                    </td>
                                </tr>
                                <tr id="dis" style="display: none;"><td width="10%">&nbsp;</td><td class="t-align-left"><span id="SearchValError" class="reqF_1"></span></td></tr>
                            </table>
                        </form>
                    </div>
                    <form action="<%=request.getContextPath()%>/InitDebarment" method="post">
                        <input type="hidden" value="ReinstateDebarment" name="action" id="action" />
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <%if ("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {
                                    if (!list1.isEmpty()) {%>
                            <tr>
                                <td width="16%" class="t-align-left ff">Individual/Company Name :<span class="mandatory">*</span></td>
                                <td width="84%" class="t-align-left">
                                    <select name="companyName" class="formTxtBox_1" id="companyName" onchange="LoadWorkCategory()" style="width:200px;">
                                        <option selected="selected" value="">- Select Company -</option>
                                        <%for (SPTenderCommonData data : list1) {%>
                                        <option value="<%=data.getFieldName2()%>"><%=data.getFieldName1()%></option>
                                        <%}%>
                                    </select>
                                    <%if ("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {
                                            if (list1.isEmpty()) {
                                                out.print("<span style='color : red;font-weight:bold;'>No matching records found.</span>");
                                            } else {
                                                out.print("<span style='color : #FF9326;font-weight:bold;'>Details found please select the Company Name.</span>");
                                            }
                                        }%>
                                </td>
                            </tr>                    
                            <tr>
                                <td class="t-align-left ff">Procurement Category :</td>
                                <td class="t-align-left">
                                    <%//int i = 0;
                                        //for (TblDebarmentTypes types : debarTypes) {%>
                                    <span id="lvlworkCategory"></span>&nbsp;&nbsp;&nbsp;
                                    <%//i++;
                                        //}%>                            
                                    <input type="hidden" value="" id="debarType"/>
                                    <!--                                    <input type="hidden" value="" name="debarIds" id="debarIds"/>
                                                                        <span class="c-alignment-right"><a id="addDet" class="action-button-add">Add Details</a></span>-->
                                </td>
                            </tr>
                                                                
                            <tr>
                                <td class="t-align-left ff">Already Debarred :</td>
                                <td class="t-align-left">
                                    <span id="alreadyDebarred">N/A</span>
                                </td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Debarment Reason :</td>
                                <td class="t-align-left">
                                    <span id="debarmentType"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="t-align-left ff">Grounds :</td>
                                <td width="84%" class="t-align-left">
                                    <span id="clarification"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Debarred From :</td>
                                <td class="t-align-left">
                                    <span id="textfield_2"></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="t-align-left ff">Debarred To :</td>
                                <td class="t-align-left">
                                    <span id="textfield_1"></span>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="t-align-left ff">Reinstatement Reason :<span class="mandatory">*</span></td>
                                <td width="84%" class="t-align-left">
                                    <textarea rows="5" id="grounds" name="grounds" class="formTxtBox_1" style="width:99%;"></textarea>
                                </td>
                            </tr>
                            <%}}%>
                            
                        </table>
                        <%if("Search".equalsIgnoreCase(request.getParameter("btnSearch"))) {if(!list1.isEmpty()){%>
                        <div class="t-align-center t_space">
                            <label class="formBtn_1"><input type="submit" name="button3" id="button3" value="Reinstate" onclick="return validate();"  /></label>
                        </div>
                        <%}}%>
                    </form>
                </div>
                <%@include file="../resources/common/Bottom.jsp" %>
            </div>
            <!--Dashboard Footer End-->
        </div>

    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabDebar");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

