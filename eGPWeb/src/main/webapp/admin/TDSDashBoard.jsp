<%--
    Document   : TDSDashBoard
    Created on : Nov 10, 2010, 7:57:34 PM
    Author     : yanki
--%>

<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBean" />
<jsp:useBean id="prepareTDSSrBean" class="com.cptu.egp.eps.web.servicebean.PrepareTDSSrBean" />
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />
<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster" %>
<%@page import="java.util.List" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        
            if(session.getAttribute("userId") != null){
                createSubSectionSrBean.setLogUserId(session.getAttribute("userId").toString());
                prepareTDSSrBean.setLogUserId(session.getAttribute("userId").toString());
                defineSTDInDtlSrBean.setLogUserId(session.getAttribute("userId").toString());
            }

            int templateId = 0;
            if(request.getParameter("templateId")!=null){
                templateId = Integer.parseInt(request.getParameter("templateId"));
            }
            String procType = "";
            String procTypefull = "";
            String STDName = "";
            List<TblTemplateMaster> templateMasterLst = defineSTDInDtlSrBean.getTemplateMaster((short) templateId);
            if(templateMasterLst != null){
                if(templateMasterLst.size() > 0){
                    procType = templateMasterLst.get(0).getProcType();
                    if("goods".equalsIgnoreCase(procType)){
                        procTypefull = "Goods";
                    }else if("works".equalsIgnoreCase(procType)){
                        procTypefull = "Works";
                    }else if("srvcmp".equalsIgnoreCase(procType)){
                        procTypefull = "Services - Consulting Firms";
                    }else if("srvindi".equalsIgnoreCase(procType)){
                        procTypefull = "Services - Individual Consultant";
                }
                    else if("srvnoncon".equalsIgnoreCase(procType)){
                        procTypefull = "Services - Non consulting services";
                    }
                    STDName = templateMasterLst.get(0).getTemplateName();
                }
                templateMasterLst = null;
            }
            int sectionId = Integer.parseInt(request.getParameter("sectionId"));
            List<com.cptu.egp.eps.model.table.TblIttHeader> subSectionDetail = createSubSectionSrBean.getSubSection_TDSAppl(sectionId);
            String contentType = createSubSectionSrBean.getContectType(sectionId);
            String sectionName = "";
            if(contentType.equalsIgnoreCase("ITT"))
                {
                sectionName = createSubSectionSrBean.getSecName((short)templateId, "TDS");
                }else  if(contentType.equalsIgnoreCase("GCC")){
                     sectionName = createSubSectionSrBean.getSecName((short)templateId, "PCC");
                    }
            if("srvcmp".equals(procType)){
                if("ITT".equals(contentType)){
                    contentType = contentType.replace("ITT", "ITC");
                }else if("TDS".equals(contentType)){
                    contentType = contentType.replace("TDS", "PDS");
                }
            }else if("srvindi".equals(procType)){
                if("ITT".equals(contentType)){
                    contentType = contentType.replace("ITT", "ITA");
                }
            }
            if(contentType.equalsIgnoreCase("ITT")){
                contentType = "TDS";
            }else if(contentType.equalsIgnoreCase("ITC")){
                contentType = "PDS";
            }else if(contentType.equalsIgnoreCase("GCC")){
                contentType = "PCC";
            }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%if(contentType.equals("TDS")){out.print("BDS");}else if(contentType.equals("PCC")){out.print("SCC");}else{out.print(contentType);}%> Dashboard</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <!--Middle Content Table Start-->
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" cellspacing="10"  class="tableView_1" >
                                <tr>
                                    <td  align="left" colspan="2">
                                    <a class="action-button-goback" href="DefineSTDInDtl.jsp?templateId=<%=request.getParameter("templateId")%>">
                                            Go back to SBD Dashboard</a></td>
                                </tr>
                            </table>
                            <div class="t_space">
                                <div class="pageHead_1"><%if(contentType.equals("TDS")){out.print("BDS");}else if(contentType.equals("PCC")){out.print("SCC");}else{out.print(contentType);}%> Dashboard</div>
                            </div>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td width="30%" align="center"><b>Name of SBD :</b></td>
                                    <td width="70%"><%=STDName%></td>
                                </tr>
                                <tr>
                                    <td width="30%" align="center"><b>Procurement Type :</b></td>
                                    <td width="70%"><%=procTypefull%></td>
                                </tr>
                                <tr>
                                    <td width="30%" align="center"><b>Section :</b></td>
                                    <td width="70%"><%=sectionName%></td>
                                </tr>
                            </table>
                            <form action="DefineSTDInDtl.jsp" method="post">
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <%
                                for(int i=0;i<subSectionDetail.size();i++){
                                %>
                                    <tr>
                                        <td align="center" width="30%"><b>Sub Section No.</b></td>
                                        <td width="70%"><%=(i+1)%></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><b>Name of Sub Section</b></td>
                                        <td><%=subSectionDetail.get(i).getIttHeaderName() %></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><b>Action</b></td>
                                        <td>
                                            <%
                                            if(!prepareTDSSrBean.isTdsSubClauseGenerated(subSectionDetail.get(i).getIttHeaderId())){
                                            %>
                                                <a href="<%=request.getContextPath()%>/admin/PrepareTDS.jsp?templateId=<%= templateId %>&ittHeaderId=<%=subSectionDetail.get(i).getIttHeaderId()%>&sectionId=<%=sectionId%>&templateId=<%=request.getParameter("templateId")%>">Create</a>
                                            <%
                                            } else {
                                            %>
                                                <a href="<%=request.getContextPath()%>/admin/PrepareTDS.jsp?templateId=<%= templateId %>&ittHeaderId=<%=subSectionDetail.get(i).getIttHeaderId()%>&sectionId=<%=sectionId%>&templateId=<%=request.getParameter("templateId")%>&edit=true">Edit</a> |
                                                <a href="<%=request.getContextPath()%>/admin/ViewTDS.jsp?templateId=<%= templateId %>&ittHeaderId=<%=subSectionDetail.get(i).getIttHeaderId()%>&sectionId=<%=sectionId%>&templateId=<%=request.getParameter("templateId")%>" target="_blank">View</a>
                                            <%
                                            }
                                            %>


                                        </td>
                                    </tr>
                                <%
                                }
                                subSectionDetail=null;
                                createSubSectionSrBean=null;
                                %>
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<%
    if(defineSTDInDtlSrBean != null){
        defineSTDInDtlSrBean = null;
    }
%>