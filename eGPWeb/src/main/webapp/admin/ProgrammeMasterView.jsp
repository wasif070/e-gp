<%-- 
    Document   : ProgrammeMasterView
    Created on : Dec 31, 2010, 3:28:45 PM
    Author     : Naresh.Akurathi
--%>



<%@page import="java.util.ListIterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="progmasterDtBean" class="com.cptu.egp.eps.web.databean.ProgrammeMasterDtBean"/>
<jsp:useBean id="progmasterSrBean" class="com.cptu.egp.eps.web.servicebean.ProgrammeMasterSrBean"/>
<jsp:setProperty property="*" name="progmasterDtBean"/>
<%@page import="com.cptu.egp.eps.model.table.TblProgrammeMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Programme Information </title>
      <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
       <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
       <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
       <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
    </head>
    <body>
        <%
            String userID="0";
            if(session.getAttribute("userId")!=null){
            userID=session.getAttribute("userId").toString();
            }
            progmasterSrBean.setLogUserId(userID);
        %>
        <%!
            TblProgrammeMaster tblPrgMaster = new TblProgrammeMaster();
            List<TblProgrammeMaster> lstPrgMaster = null;
            int id=0;

        %>

        <%
            String mesg =  request.getParameter("msg");
            String caption = "Programme Information";
            String colHeader = "Sl. <br/> No.,Programme Code,Programme Name,Implementing Agency,Action";
            String colName = "S.No,progCode,progName,progOwner,action";
            String align = "center,left,left,left,center";
            String sortColumn = "false, true, true,true,false";
            String widths = "4,19,31,30,8";
            String search = "false,true,true,true,false";
            String soptOptions = "'eq', 'cn'@'eq', 'cn'@'eq', 'cn'@'eq', 'cn'@'eq', 'cn'";
        %>
        <div class="dashboard_div">
          <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <%--<%@include file="/resources/common/AfterLoginLeft.jsp" %>--%>

                        <td class="contentArea">

            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Programme Information
            <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('4');">Save as PDF</a></span>
            </div>
            <div>&nbsp;</div>
                        <%if(mesg != null){%>
                        <div class="responseMsg successMsg"><%=mesg%></div>
                        <%}%>
                        
            <div align="left" class="t_space">
                            <jsp:include page="../resources/common/GridCommon.jsp" >
                                <jsp:param name="caption" value="<%=caption%>" />
                                <jsp:param name="url" value='<%=request.getContextPath() + "/ProgrammeGridServlet" %>' />
                                <jsp:param name="colHeader" value="<%=colHeader%>" />
                                <jsp:param name="colName" value="<%=colName%>" />
                                <jsp:param name="aling" value="<%=align%>" />
                                <jsp:param name="sortColumn" value="<%=sortColumn%>" />
                                <jsp:param name="width" value="<%=widths%>" />
                                <jsp:param name="search" value="<%=search%>" />
                                <jsp:param name="soptOptions" value="<%=soptOptions%>" />
                            </jsp:include>
                        </div>
            </td>
           </tr>
        </table>
            <!--For Generate PDF  Starts-->
            <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                    String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="Programmes_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="Programmes" />
            </form>
            <!--For Generate PDF  Ends-->
            <!--Dashboard Content Part End-->


            <!--Dashboard Footer Start-->
            <%@include file="/resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
            <script>
                var obj = document.getElementById('lblProgInfoView');
                if(obj != null){
                    if(obj.innerHTML == 'View Programmes'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabContent");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
