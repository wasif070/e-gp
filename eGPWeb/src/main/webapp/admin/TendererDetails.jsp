<%--
    Document   : CompanyVerification
    Created on : Nov 5, 2010, 11:40 PM
    Author     : taher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblTendererMaster"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum" %>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Verification</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="../text/javascript" src="resources/js/pngFix.js"></script>-->

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />

    </head>       
    <body>
        <%
                    ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
                    contentAdminService.setAuditTrail(null);
                    TblTendererMaster tblTendererMaster = contentAdminService.findTblTendererMasters("tendererId", Operation_enum.EQ, Integer.parseInt(request.getParameter("tId"))).get(0);
                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                    
                     // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    String idType="userId";
                    int auditId=Integer.parseInt(session.getAttribute("userId").toString());
                    String auditAction="View Bidder Personal Details ";
                    String moduleName=EgpModule.New_User_Registration.getName();
                    String remarks="";
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks); 
                    String status = "";
                    if(request.getParameter("status") != null)
                    {
//                        status = "&status=reapply";
                        status = request.getParameter("status").toString();
                    }
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="tableHead_1 t_space"><% if(tblTendererMaster.getTblLoginMaster().getRegistrationType().equals("individualconsultant")) {%> Personal Details <% } else { %>Company Contact Person Details<% } %></div>
                <jsp:include page="EditAdminNavigation.jsp" ></jsp:include>
                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                    <tr>
                        <td width="18%" class="ff">Title : </td>
                        <td width="82%"><%=tblTendererMaster.getTitle()%></td>
                    </tr>
                    <tr>
                        <td class="ff">First Name : </td>
                        <td><%=tblTendererMaster.getFirstName()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Middle Name : </td>
                        <td><%=tblTendererMaster.getMiddleName()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Last Name : </td>
                        <td><%=tblTendererMaster.getLastName()%></td>
                    </tr>
                    <tr style="display: none">
                        <td class="ff">Name in Bangla :</td>
                        <td><%if(tblTendererMaster.getFullNameInBangla()!=null){out.print(BanglaNameUtils.getUTFString(tblTendererMaster.getFullNameInBangla()));}%></td>
                    </tr>
<!--                    <tr>
                        <td class="ff">CID No. : </td>
                        <td><%--<%//=tblTendererMaster.getNationalIdNo()%>--%>
                        </td>
                    </tr>-->
                    <%if (!"no".equals(request.getParameter("jv"))) {%>
                    <tr>
                        <td class="ff">Designation : </td>
                        <td><%=tblTendererMaster.getDesignation()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Department : </td>
                        <td><%=tblTendererMaster.getDepartment()%></td>
                    </tr>
                    <%}%>
                    <tr>
                        <td class="ff">Address : </td>
                        <td><%=tblTendererMaster.getAddress1()%></td>
                    </tr>
                    <tr style="display: none">
                        <td class="ff">Address Line L2 : </td>
                        <td><%=tblTendererMaster.getAddress2()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Country : </td>
                        <td><%=tblTendererMaster.getCountry()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Dzongkhag / District : </td>
                        <td><%=tblTendererMaster.getState()%></td>
                    </tr>
                    
                    <tr id="trsubdistrict">
                        <td class="ff">Dungkhag / Sub-district : </td>
                        <td><% if(tblTendererMaster.getSubDistrict()!=null){out.print(tblTendererMaster.getSubDistrict());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">City / Town : </td>
                        <td><%=tblTendererMaster.getCity()%></td>
                    </tr>
                    <tr id="trthana">
                        <td class="ff">Gewog : </td>
                        <td><%=tblTendererMaster.getUpJilla()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Post Code : </td>
                        <td><%=tblTendererMaster.getPostcode()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Phone No. : </td>
                        <td><%if(!tblTendererMaster.getPhoneNo().equals("")){out.print(commonService.countryCode(tblTendererMaster.getCountry(), false)+"-"+tblTendererMaster.getPhoneNo());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Fax No. : </td>
                        <td><%if(!tblTendererMaster.getFaxNo().equals("")){out.print(commonService.countryCode(tblTendererMaster.getCountry(), false)+"-"+tblTendererMaster.getFaxNo());}%></td>
                    </tr>
                    <tr>
                        <td class="ff">Mobile No. : </td>
                        <td><%=commonService.countryCode(tblTendererMaster.getCountry(), false)+"-"+tblTendererMaster.getMobileNo()%>
                        </td>
                    </tr>                    
                    <%if ("no".equals(request.getParameter("jv"))) {%>
                    <tr>
                        <td class="ff">Tax Payment Number : </td>
                        <td>
                             <%if(tblTendererMaster.getTinNo().equalsIgnoreCase("other")){out.print(tblTendererMaster.getTinDocName().substring(0, tblTendererMaster.getTinDocName().indexOf("$"))+"<div class=\"formNoteTxt\">(Other No)</div>"+tblTendererMaster.getTinDocName().substring(tblTendererMaster.getTinDocName().indexOf("$")+1,tblTendererMaster.getTinDocName().length())+"<div class=\"formNoteTxt\">(Description)</div>");}else{out.print(tblTendererMaster.getTinDocName());}%>
                        </td>
                    </tr>
<!--                 <tr>
                        <td class="ff">Specialization : </td>
                        <td>
                             <%//=tblTendererMaster.getSpecialization()%>
                        </td>
                    </tr>-->
                     <tr>
                        <td class="ff">Website : </td>
                        <td><%=tblTendererMaster.getWebsite()%></td>
                    </tr>
                    <%}%>                                       
                </table>
                <table border="0" cellspacing="10" cellpadding="0" width="100%">
                    <tr>
                        <td width="18%">&nbsp;</td>
                        <td width="82%" align="left">
                            <a href="<%if("no".equals(request.getParameter("jv"))||"n".equals(request.getParameter("jv"))){%>
                               TendererDocs.jsp?s=<%=request.getParameter("s")%>&tId=<%=request.getParameter("tId")%>&uId=<%=request.getParameter("uId")%>&payId=<%=request.getParameter("payId")%>&cId=<%=request.getParameter("cId")%>&status=<%=status%><%}
                                else{%>TendererJVDetails.jsp?s=<%=request.getParameter("s")%>&payId=<%=request.getParameter("payId")%>&tId=<%=request.getParameter("tId")%>&uId=<%=request.getParameter("uId")%>&cId=<%=request.getParameter("cId")%>&jv=<%=request.getParameter("jv")%>&status=<%=status%><%}%>"  class="anchorLink">Next</a>
                        </td>
                    </tr>
                </table>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript">
                var headSel_Obj = document.getElementById("headTabCompVerify");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
    </script>
</html>
