<%@page import="java.util.Map"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.model.table.TblEventMaster"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblModuleMaster"%>
<%@page import="java.util.List"%>
<jsp:directive.page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Administration - Default Workflow Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
    </head>
    <body>
        <%
                    WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                    List<TblModuleMaster> modueleMaster = workFlowSrBean.getWorkflowData();
                    request.setAttribute("modulemaster", modueleMaster);
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->


            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td valign="top" class="contentArea">
                        <div class="pageHead_1"> Default Workflow Configuration</div>
                        
                        <table width="100%" cellspacing="0" class="tableList_1 t_space">
                            <tr>
                                <th width="30%" >Module</th>
                                <th>Process</th>
                                <th>Action</th>
                            </tr>
                            <%
                                        List<TblModuleMaster> masterList = (List) request.getAttribute("modulemaster");
                                        Iterator it = masterList.iterator();
                                        String fieldName1 = "WorkFlowRuleEngine";
                                        String fieldName2 = null;
                                        String fieldName3 = "";
                                        int flag=0;//This flag is being used to prevent the display of "Contract Management System (CMS)" (Feroz Ahmed, 17th April, 2016)
                                        
                                        while (it.hasNext() && flag<2) {//The flag is being used to prevent the display of "Contract Management System (CMS)" (Feroz Ahmed, 17th April, 2016)
                                            flag++;
                                            boolean first = false;
                                            TblModuleMaster tblModuleMaster = (TblModuleMaster) it.next();
                                            Set<TblEventMaster> eventMasterSet = tblModuleMaster.getTblEventMasters();
                                            List<TblEventMaster> eventList = new ArrayList<TblEventMaster>(eventMasterSet);
                                            Iterator sortnames = eventList.iterator();
                                            StringBuffer sb = new StringBuffer();
                                            while(sortnames.hasNext()){
                                                 TblEventMaster tem = (TblEventMaster)sortnames.next();
                                                 sb.append(tem.getEventName()+",");
                                                }
                                            if(sb.length()>0)
                                                sb.deleteCharAt(sb.length()-1);
                                            String[] strevn = sb.toString().split(",");
                                             List l = Arrays.asList(strevn);
                                              Collections.sort(l);
                                             
                                            out.write("<tr>");
                                            out.write("<td class='t-align-left' rowspan='" + l.size() + "'>" + tblModuleMaster.getModuleName() + "</td>");
                                            Iterator eventMaster = eventList.iterator();
                                            int i=0;
                                            Map m = new Hashtable();
                                            while(eventMaster.hasNext()){

                                            TblEventMaster tblEventMaster = (TblEventMaster) eventMaster.next();
                                            m.put(tblEventMaster.getEventName(), tblEventMaster.getEventId());
                                                }
                                                

                                            while (i<l.size()) {
                                                if (first != false) {
                                                    out.write("<tr>");
                                                }
                                                String eventname = (String) l.get(i);
                                                Short obj = (Short)m.get(eventname);
                                                fieldName2 = Short.toString(obj);
                                                List<CommonAppData> recordexist = workFlowSrBean.editWorkFlowData(fieldName1, fieldName2, fieldName3);
                                                if(!eventname.equalsIgnoreCase("Technical Sub Committee Formation Workflow"))
                                                {
                                                    out.write("<td class='t-align-left' >" + eventname + "</td>");
                                                    if (recordexist.size() > 0) {
                                                        out.write("<td class='t-align-center' >"
//                                                                + "<a href='DefWorkflowConf.jsp?common=Edit&eventid="
//                                                                + obj.shortValue() + "'>Edit</a>&nbsp|&nbsp"
                                                                + "<a href='DefWorkflowConf.jsp?eventid="
                                                                + obj.shortValue() + "&common=View" + "'>View</a></td>");
                                                    } else {
                                                        out.write("<td class='t-align-center' ><a href='DefWorkflowConf.jsp?moduleName="
                                                                + tblModuleMaster.getModuleName() + "&EventName=" + eventname
                                                                + "&eventid=" + obj.shortValue() + "&common=Insert"
                                                                + "'>Create</a></td>");
                                                    }
                                                }
                                                out.write("</tr>");
                                                i++;
                                            }

                                        }

                            %>

                        </table>
                    </td>
                    <div>&nbsp;</div>
                    <!--Dashboard Content Part End-->
                </tr>
            </table>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
             <script>
                var obj = document.getElementById('lbWorkflowConfig');
                if(obj != null){
                    if(obj.innerHTML == 'Configure'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
