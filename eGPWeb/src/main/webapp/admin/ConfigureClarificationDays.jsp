<%-- 
    Document   : ConfigureClarificationDays
    Created on : Feb 28, 2017, 2:15:40 PM
    Author     : MD. Emtazul Haque
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.model.table.TblConfigClarification"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConfigClarificationService"%>
<!DOCTYPE html>

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender Clarification Days Configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function() {
                $('#remRule').click(function() {
                    var counter=$('#tbodyData').children().length-1;
                    var tmpCnt = 0;
                    for(var i=0;i<=counter;i++){
                        if(document.getElementById("chk_"+i) != null && document.getElementById("chk_"+i).checked){
                            tmpCnt++;
                        }
                    }
                    if(tmpCnt>0){jConfirm('Are You Sure You want to Delete.','Tender Clarification Days Rule Configuration', function(ans) {if(ans){
                     $(":checkbox[checked='true']").each(function(){                         
                         var len=$('#tbodyData').children().length-1;
                         var curRow = $(this).closest('tr');
                         if(len==1){
                            jAlert("Atleast one configuration is required.","Tender Clarification Days Rule Configuration", function(RetVal) {
                            });
                         }else{
                           var id=$(this).attr("id").substring(4,$(this).attr("id").length);
                           for(var i=id;i<len;i++){
                                $('#SBDName_'+eval(eval(i)+eval(1))).attr("id","SBDName_"+(i));
                                $('#ClarificationDays_'+eval(eval(i)+eval(1))).attr("id","ClarificationDays_"+(i));
                                $('#remRule_'+eval(eval(i)+eval(1))).attr("id","remRule_"+(i));
                                $('#chk_'+eval(eval(i)+eval(1))).attr("id","chk_"+(i));
                            }
                            curRow.remove();
                          }
                     });
                  }});}else{jAlert("please Select checkbox first","Tender Clarification Days Rule Configuration", function(RetVal) {});}
                });
            });
            $(function() {
                $('#addRule').click(function() {
                    var len=$('#tbodyData').children().length-1;
                    $('#tbodyData').append("<tr>"+
                        "<td class='t-align-center'>"+
                        "<input type='checkbox' id='chk_"+len+"' />"+
                        "</td>"+
                        "<td class='t-align-center'>"+
                        "<select name='SBDName' class='formTxtBox_1' id='SBDName_"+len+"'>"+$('#cmdSBDName').val()+"</select>"+
                        "</td>"+
                        "<td class='t-align-center'>"+
                        "<input type='text' value='' name='ClarificationDays' id='ClarificationDays_"+len+"' class='formTxtBox_1'  style='width: 50px;'>"+
                        "</td>"+                      
                        "</tr>");
                });
            });
            function delRow(row){
                var curRow = $(row).closest('tr');
                var len=$('#tbodyData').children().length-1;
                if(len==1){
                    jAlert("Atleast one configuration is required.","Tender Clarification Days Rule Configuration", function(RetVal) {
                    });
                }else{
                    var id=$(row).attr("id").substring(8,$(row).attr("id").length);
                    for(var i=id;i<len;i++){
                        $('#SBDName_'+eval(eval(i)+eval(1))).attr("id","SBDName_"+(i));
                        $('#ClarificationDays_'+eval(eval(i)+eval(1))).attr("id","ClarificationDays_"+(i));
                        $('#remRule_'+eval(eval(i)+eval(1))).attr("id","remRule_"+(i));
                    }
                    curRow.remove();
                }
            }

            function validate(){
                $(".err").remove();
                var tbol = true;
                var len=$('#tbodyData').children().length-1;
                var cnt=0;
                var cnt2=0;
                for(var i=0;i<len;i++){
                    if($.trim($("#ClarificationDays_"+i).val()) == "") {
                        $("#ClarificationDays_"+i).parent().append("<div class='err' style='color:red;'>Please enter no. of Days</div>");
                        cnt++;
                    }                    
                }
                if(cnt!=0){
                    tbol = false;
                }
                if(cnt2!=0){
                    tbol = false;
                }
                var chkCount=0;
                for(var i=0;i<len;i++){
                    var SBDname = $("#SBDName_"+i).val();
                    for(var j=i+1;j<len;j++){
                        var SBDname2 = $("#SBDName_"+j).val();
                        if(SBDname==SBDname2){
                            chkCount++;
                        }
                    }
                }
                if(chkCount!=0){
                    jAlert("Duplicate record found. Please enter unique record.","Tender Clarification Days Rule Configuration", function(RetVal) {
                    });
                    tbol=false;
                }
                return tbol;
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <%
                    ConfigClarificationService configService = (ConfigClarificationService) AppContext.getSpringBean("ConfigClarificationService");
                    configService.setLogUserId(session.getAttribute("userId").toString());
                    List<Object[]> SBDList = configService.getSBDList();
                    StringBuilder cmb1 = new StringBuilder();
                    for (Object[] proc : SBDList) {
                        cmb1.append("<option value='" + proc[0] + "'>" + proc[1] + "</option>");
                    }
                    
                    List<TblConfigClarification> getConfigClarification = new ArrayList<TblConfigClarification>();
                    boolean isEdit = false;
                    if ("y".equals(request.getParameter("isedit"))) {
                        isEdit = true;
                        getConfigClarification = configService.getAllTblConfigClarification();
                    }
        %>
        <input type="hidden" value="<%=cmb1%>" id="cmdSBDName"/>
        
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea">
                        <div class="pageHead_1">Tender Clarification Days Configuration</div>
                        <div style="font-style: italic" class="t-align-left t_space"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>
                        
                        <div align="right" class="t_space b_space">
                                <a href="javascript:void(0);" class="action-button-add" id="addRule">Add Rule</a>
                                <!--<a href="javascript:void(0);" class="action-button-delete" id="remRule">Remove Rule</a>-->
                            </div>
                        <div>&nbsp;</div>
                        
                    <form action="<%=request.getContextPath()%>/ConfigClarificationServlet?action=<%if (isEdit) {
                                 out.print("ConfigClarificationUpdate");
                             } else {
                                 out.print("ConfigClarificationInsert");
                             }%>" method="post" >


                            <%if ("fail".equals(request.getParameter("msg"))) {
                                            out.print("<div class='responseMsg errorMsg'>Error in inserting Database</div>");
                                        }%>


                            

                            <table class="tableList_1" cellspacing="0" width="100%" id="members">
                                <tbody id="tbodyData">
                                    <tr>
                                        <!--                        <th class="t-align-center">Select</th>-->
                                        <th class="t-align-center">Select</th>
                                        <th class="t-align-center">SBD Name<br>(<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center">Number of days<br>before Tender closing date<br>for posting query(<span class="mandatory" style="color: red;">*</span>)</th>
                                    </tr>
                                    <%
                                                int cnt = 0;
                                                do {
                                    %>
                                    <tr>
                                        <td class="t-align-center">
                                            <input type="checkbox" id="chk_<%=cnt%>" />
                                        </td>
                                        <td class="t-align-center">
                                            <select name="SBDName" class="formTxtBox_1" id="SBDName_<%=cnt%>">
                                                <%for (Object[] proc : SBDList) {%>
                                                <option value="<%=proc[0]%>" <%if (isEdit) {
                                            if (getConfigClarification.get(cnt).getTemplateId()== (Short) proc[0]) {
                                                out.print("selected");
                                            }
                                        }%>><%=proc[1]%></option>
                                                <%}%>
                                            </select>
                                        </td>
                                        
                                        <td class="t-align-center">
                                            <input type="text" value="<%if (isEdit) {
                                                                    out.print(getConfigClarification.get(cnt).getClarificationDays());
                                                                }%>" name="ClarificationDays" id="ClarificationDays_<%=cnt%>" class="formTxtBox_1" style="width: 50px;">
                                        </td>
                                                                     
                                    </tr>
                                    <%cnt++;
                                    } while (cnt < getConfigClarification.size());%>
                                </tbody>

                                

                            </table>
                                <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" >
                                <tr>
                                    <td colspan="8" class="t-align-center">
                                        <span class="formBtn_1">
                                            <input name="button2" id="button2" value="Submit" type="submit" onclick="return validate();">
                                        </span>
                                    </td>
                                </tr>
                            </table>
                    </form>
                </td>
            </tr>
        </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    <%if (isEdit) {%>
    <script type="text/javascript" >
        var obj = document.getElementById('lblConfigClarificationEdit');
        if(obj != null){
            if(obj.innerHTML == 'Edit'){
                obj.setAttribute('class', 'selected');
            }
        }

    </script>

    <%} else {%>
    <script>
        var obj = document.getElementById('lblConfigClarificationAdd');
        if(obj != null){
            if(obj.innerHTML == 'Add'){
                obj.setAttribute('class', 'selected');
            }
        }

    </script>

    <% }%>

    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabConfig");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
</html>
