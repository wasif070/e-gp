<%-- 
    Document   : UploadSTDDoc
    Created on : 24-Oct-2010, 4:36:54 PM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Middle Content Table Start-->
            <form action="CreateForm.jsp" method="post"  enctype="multipart/form-data">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="pageHead_1">Upload Documents</div>
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                <tr>
                                    <td class="ff">Document   : <span>*</span></td>
                                    <td><input name="file" type="file" class="formTxtBox_1" style="width:250px; background:none;" />
                                        <div class="t_space"><img src="../resources/images/Dashboard/uploadIcn.png" alt="Upload" class="linkIcon_1" /><a href="#" title="Upload">Upload</a></div></td>
                                </tr>

                            </table>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" class="tableList_2">
                                <tr>
                                    <th style="text-align:center; width:4%;">Sl. No.</th>
                                    <th>Document Name</th>

                                    <th>File Size</th>
                                    <th style="text-align:center; width:10%;">Action</th>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">1</td>
                                    <td>Document 1</td>

                                    <td>2 MB</td>
                                    <td style="text-align:center;"><a href="#" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a> &nbsp; <a href="#" title="Remove"><img src="../resources/images/Dashboard/delIcn.png" alt="Remove" width="16" height="16" /></a></td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">2</td>
                                    <td>Document 2</td>

                                    <td>200 KB</td>
                                    <td style="text-align:center;"><a href="#" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a> &nbsp; <a href="#" title="Remove"><img src="../resources/images/Dashboard/delIcn.png" alt="Remove" width="16" height="16" /></a></td>
                                </tr>
                            </table>
                            <div class="t_space" align="center">
                                <label class="formBtn_1">
                                    <input type="submit" name="button" id="button" value="Next" /></label>
                            </div>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
            </form>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
