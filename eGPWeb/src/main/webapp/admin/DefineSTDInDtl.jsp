<%--
    Document   : DefineSTDInDtl
    Created on : 24-Oct-2010, 1:03:35 AM
    Author     : yanki
--%>


<%@page import="com.cptu.egp.eps.service.serviceinterface.CreateComboService"%>
<%@page import="java.io.File"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateGuildeLines"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ScBankPartnerAdminSrBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />
<%@page import="com.cptu.egp.eps.model.table.TblTemplateSectionDocs" %>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster" %>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="pdfcmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd"  />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>SBD Dashboard</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <form method="POST" id="deleteConfirm">

        </form>
        <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="contentArea_1">
                <!--Middle Content Table Start-->
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1">
                                    Standard Bidding Document (SBD) Dashboard
                                    <span style="float: right; text-align: right;">
                                        <a href="ListSTD.jsp" title="SBD Listing" class="action-button-goback">Go back to SBD Listing</a>
                                    </span>
                                </div>
                            </div>
                            <form action="DefineSTDInDtl.jsp" method="post">

                                <%
                                
                                String logUserId = "0";
                                if (session.getAttribute("userId") != null) {
                                    logUserId = session.getAttribute("userId").toString();
                                }
                                CreateComboService createcomboservice = (CreateComboService) AppContext.getSpringBean("CreateComboService");
                                createcomboservice.setLogUserId(logUserId);
                                defineSTDInDtlSrBean.setLogUserId(logUserId);
                                short templateId = Short.parseShort(request.getParameter("templateId"));
                                java.util.List<com.cptu.egp.eps.model.table.TblTemplateSections> tblTemplateSections = defineSTDInDtlSrBean.getTemplateSection(templateId);
                                String procType = "";
                                String stdFor = "";
                                String devPartner = "";
                                String templateName = "";
                                String devPartnetName = "";
                                int setPccid = 0;
                                boolean isAccepted = false;
                                List<TblTemplateMaster> templateMasterLst = defineSTDInDtlSrBean.getTemplateMaster(templateId);
                                if (templateMasterLst != null) {
                                    if (templateMasterLst.size() > 0) {

                                        procType = templateMasterLst.get(0).getProcType();
                                        stdFor = templateMasterLst.get(0).getStdFor();
                                        devPartner = templateMasterLst.get(0).getDevPartnerId();
                                        if (devPartner != null && !"".equalsIgnoreCase(devPartner)) {
                                            ScBankPartnerAdminSrBean scBankPartnerAdminSrBean = new ScBankPartnerAdminSrBean();
                                            devPartnetName = scBankPartnerAdminSrBean.getSbDevInfo(Integer.parseInt(devPartner));
                                        }
                                        templateName = templateMasterLst.get(0).getTemplateName();

                                        if ("A".equalsIgnoreCase(templateMasterLst.get(0).getStatus())) {
                                            isAccepted = true;
                                        }
                                    }
                                    templateMasterLst = null;
                                }
                                int ittId = 0;
                                int gccId = 0;
                                boolean isIttCreated = false;
                                boolean isGccCreated = false;
                    %>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr style="background-color: #ffc926;">
                            <td width="25%"><b>Name of SBD :</b></td>
                            <td><%=templateName%></td>
                        </tr>
                        <tr>
                            <td><b>Follows Procurement Rules of :</b></td>
                            <td><%if ("Government of Bangladesh".equals(stdFor)) {out.print("Royal Government of Bhutan");}else{out.print(stdFor);}%></td>
                        </tr>
                        <% if ("Development Partner".equals(stdFor)) {%>
                        <tr>
                            <td><b>Development Partner :</b></td>
                            <td><%=devPartnetName%></td>
                        </tr>
                        <% }%>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>

                            <td width="25%"><b>Guidance Notes :</b></td>
                            <td><a href="GuidenceDocUpload.jsp?templateId=<%=templateId%>">Upload Document</a>


                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <th width="4%">Sl. No.</th>
                                        <th>File Name</th>
                                        <th>File Description</th>
                                        <th>File Size In KB</th>
                                        <th>Action</th>
                                    </tr>
                                    <%
                                                java.util.List<com.cptu.egp.eps.model.table.TblTemplateGuildeLines> docss = defineSTDInDtlSrBean.getGuideLinesDocs(templateId);
                                                if (docss != null) {
                                                    int size = 0;
                                                    if (docss.size() > 0) {
                                                        short j = 0;
                                                        for (TblTemplateGuildeLines ttsd : docss) {
                                                            j++;
                                                            size = Integer.parseInt(ttsd.getDocSize()) / 1024;
                                %>
                                    <tr>
                                        <td style="text-align:center;"><%= j%></td>
                                        <td style="text-align:left;"><%= ttsd.getDocName()%></td>
                                        <td style="text-align:left;"><%= ttsd.getDescription()%></td>
                                        <td style="text-align:center;"><%=size%></td>
                                        <td style="text-align:center;">
                                            <a href="../GuideLinesDocUploadServlet?docName=<%= ttsd.getDocName()%>&templateId=<%= templateId%>&docSize=<%= ttsd.getDocSize()%>&action=downloadDoc" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                        </td>
                                    </tr>

                                    <%
                                                                                                ttsd = null;
                                                                                            }
                                                                                        } else {
                                    %>
                                    <tr>
                                        <td colspan="5" class="t-align-center">
                                            No records found
                                        </td>
                                    </tr>
                                    <%                                                                                                        }
                                                                                        docss = null;
                                                                                    } else {
                                    %>
                                    <tr>
                                        <td colspan="5" class="t-align-center">
                                            No records found
                                        </td>
                                    </tr>
                                    <%                                                            }
                                    %>
                                </table>



                            </td>
                        </tr>


                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <%
                                    for (int i = 0; i < tblTemplateSections.size(); i++) {
                        %>
                        <tr style="background-color: #ffc926;">
                                        <td align="center"><b>Section No.</b></td>
                            <td colspan="3">Section <%=(i + 1)%></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><b>Name of Section</b></td>
                            <td><%=tblTemplateSections.get(i).getSectionName()%>

                                <%
                                                                        if (("ITT".equals(tblTemplateSections.get(i).getContentType())
                            || "TDS".equals(tblTemplateSections.get(i).getContentType())
                            || "GCC".equals(tblTemplateSections.get(i).getContentType())
                            || "PCC".equals(tblTemplateSections.get(i).getContentType()))
                                                                                && isAccepted) {
                                String folderName = pdfConstant.STDPDF;
                                String encodedSecName = URLEncoder.encode("Section"+(i + 1)+"_"+tblTemplateSections.get(i).getSectionName(),"UTF-8");
                                                                            String genId = templateId + "_" + tblTemplateSections.get(i).getSectionId();
                boolean isDevelopment = false;
                boolean isStaging = false;
                boolean isProduction = false;
                boolean isTraining = false;
           if(request.getRequestURL().toString().contains("development.eprocure.gov.bd") || request.getRequestURL().toString().contains("10.1.3.21")){
            isDevelopment = true;
            }
            if(request.getRequestURL().toString().contains("staging.eprocure.gov.bd") || request.getRequestURL().toString().contains("10.1.4.21")){
                isStaging = true;
            }
            if(request.getRequestURL().toString().contains("training.eprocure.gov.bd") || request.getRequestURL().toString().contains("10.1.40.25")){
                isTraining = true;
            }
            if(request.getRequestURL().toString().contains("www.eprocure.gov.bd") || request.getRequestURL().toString().contains("10.1.2.26") || request.getRequestURL().toString().contains("10.1.2.27")){
                isProduction = true;
            }
                String strDriveLetter =  XMLReader.getMessage("driveLatter");
           /* if(isDevelopment || isTraining)
            {
                strDriveLetter = "D:";
            }
            else if(isStaging)
            {
                strDriveLetter = "E:";
            }
            else if(isProduction)
            {
                strDriveLetter = "\\egpprdblog\\AppSharedDrive";
            }
*/
            String path = strDriveLetter+"/eGP/PDF/";
                                                                            

            File file = new File(path + "/" + folderName + "/" + templateId + "_" + tblTemplateSections.get(i).getSectionId());
            if (file.exists()) {
           %>
                                &nbsp;<a class="action-button-savepdf"
                        href="<%=request.getContextPath()%>/GeneratePdf?folderName=<%=folderName%>&id=<%=genId%>&pdfName=<%=encodedSecName%>" >Save As PDF </a>
            <%}%>

                                &nbsp;<a class="action-button-edit" 
                                         href="GenerateFreshSTDPDF.jsp?sectionId=<%=tblTemplateSections.get(i).getSectionId()%>&contentType=<%=tblTemplateSections.get(i).getContentType()%>&templateId=<%=templateId%>&pdfName=<%=encodedSecName%>" >Generate fresh PDF</a>

           <%
                    }
           %>
                                        </td>
                                        <td><b>Content Type</b></td>
                                        <td>
                                         <%--   <%
                                                                        if ("srvcmp".equals(procType)) {
                                                                            if ("ITT".equals(tblTemplateSections.get(i).getContentType())) {
                                                %>
                                <%= tblTemplateSections.get(i).getContentType().replace("ITT", "ITC")%>
                                                <%
                                                                                                                        } else if ("TDS".equals(tblTemplateSections.get(i).getContentType())) {
                                                %>
                                <%= tblTemplateSections.get(i).getContentType().replace("TDS", "PDS")%>
                                                <%
                                                                                                                        } else {
                                                %>
                                <%= tblTemplateSections.get(i).getContentType()%>
                                                <%
                                                    }
                                                                                                                    } else if ("srvindi".equals(procType)) {
                                                                                                                        if ("ITT".equals(tblTemplateSections.get(i).getContentType())) {
                                                %>
                                <%= tblTemplateSections.get(i).getContentType().replace("ITT", "ITA")%>
                                                <%
                                                                                                                                            } else {
                                                %>
                                <%= tblTemplateSections.get(i).getContentType()%>
                                                <%
                                                    }
                                                                                                                    } else {
                                            %>
                                <%= tblTemplateSections.get(i).getContentType()%>
                                            <%
                                                }
                                            %>--%>
                                          <%if ("ITT".equals(tblTemplateSections.get(i).getContentType())) { %>
                                          ITB
                                          <%} else if("TDS".equals(tblTemplateSections.get(i).getContentType())){%>
                                          BDS
                                          <%} else if("PCC".equals(tblTemplateSections.get(i).getContentType())){%>
                                          SCC
                                          <%} else if("Document By PE".equals(tblTemplateSections.get(i).getContentType())){%>
                                          Document By PA
                                          <%} else{%>
                                          <%= tblTemplateSections.get(i).getContentType()%>
                                          <%} %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center"><b>Action</b></td>
                                        <td colspan="3">
                                            <%
                                                                        if (!tblTemplateSections.get(i).getContentType().contains("Document")) {
                                                                            if (tblTemplateSections.get(i).getContentType().equalsIgnoreCase("ITT")) {
                                                    ittId = tblTemplateSections.get(i).getSectionId();
                                                    isIttCreated = defineSTDInDtlSrBean.checkSubSectionCreate(ittId);
                                                                                if (!isIttCreated) {
                                                    %>
                                <a href="<%=request.getContextPath()%>/admin/CreateSubSection.jsp?templateId=<%=templateId%>&sectionId=<%=tblTemplateSections.get(i).getSectionId()%>">Create</a> |
                                                    <%
                                                                                                                            } else {
                                                    %>
                                                        <a href="<%=request.getContextPath()%>/admin/CreateSubSection.jsp?templateId=<%=templateId%>&sectionId=<%=tblTemplateSections.get(i).getSectionId()%>&edit=true">Edit Section Details</a> |
                                <a href="<%=request.getContextPath()%>/admin/subSectionDashBoard.jsp?sectionId=<%=tblTemplateSections.get(i).getSectionId()%>&templateId=<%=templateId%>">Dashboard</a> |
                                <a href="<%=request.getContextPath()%>/admin/ITTView.jsp?sectionId=<%=tblTemplateSections.get(i).getSectionId()%>&templateId=<%=templateId%>" target="_new">Abstract View</a> |
                                                    <%
                                                    }
                                                %>

                                                <%
                                                                                                                        } else if (tblTemplateSections.get(i).getContentType().equalsIgnoreCase("TDS")) {
                                                                                                                            int TDSsecid = tblTemplateSections.get(i).getSectionId();
                                                                                                                            if (!isIttCreated) {
                                                                                                                                if ("srvcmp".equals(procType)) {
                                                    %>
                                                        ITC sub section yet not created.
                                <%                                                                                                            } else {
                                                    %>
                                                        ITB sub section yet not created.
                                <%                                                                                                            }
                                                    } else {
                                                    %>
                                <a href="<%=request.getContextPath()%>/admin/TDSDashBoard.jsp?templateId=<%=templateId%>&sectionId=<%=ittId%>">
                                                        Dashboard</a> |
                                                        <a href="<%=request.getContextPath()%>/admin/TDSView.jsp?sectionId=<%=ittId%>&templateId=<%=templateId%>" target="_new">Abstract View</a> |
                                                    <%
                                                    }
                                                                                                                        } else if (tblTemplateSections.get(i).getContentType().equalsIgnoreCase("GCC")) {
                                                    gccId = tblTemplateSections.get(i).getSectionId();
                                                    isGccCreated =  defineSTDInDtlSrBean.checkSubSectionCreate(gccId);

                                                                                                                            if (!isGccCreated) {
                                                    %>
                                <a href="<%=request.getContextPath()%>/admin/CreateSubSection.jsp?templateId=<%=templateId%>&sectionId=<%=tblTemplateSections.get(i).getSectionId()%>">Create</a> |
                                                    <%
                                                                                                                                                } else {
                                                                                                                              setPccid = tblTemplateSections.get(i).getSectionId();
                                                    %>
                                                        <a href="<%=request.getContextPath()%>/admin/CreateSubSection.jsp?templateId=<%=templateId%>&sectionId=<%=tblTemplateSections.get(i).getSectionId()%>&edit=true">Edit Section Details</a> |
                                <a href="<%=request.getContextPath()%>/admin/subSectionDashBoard.jsp?templateId=<%=templateId%>&sectionId=<%=tblTemplateSections.get(i).getSectionId()%>">Dashboard</a> |
                                <a href="<%=request.getContextPath()%>/admin/ITTView.jsp?sectionId=<%=tblTemplateSections.get(i).getSectionId()%>&templateId=<%=templateId%>" target="_new">Abstract View</a> |
                                                    <%
                                                    }
                                                                                                                        } else if (tblTemplateSections.get(i).getContentType().equalsIgnoreCase("PCC")) {
                                                                                                                           // int pccid = gccId = tblTemplateSections.get(i).getSectionId();
                                                                                                                            if (!isGccCreated) {
                                                    %>
                                                        GCC sub section yet not created.
                                <%                                                                                                        } else {
                                                    %>
                                <a href="<%=request.getContextPath()%>/admin/TDSDashBoard.jsp?templateId=<%=templateId%>&sectionId=<%=gccId%>">
                                                        Dashboard</a> |
                                <a href="<%=request.getContextPath()%>/admin/TDSView.jsp?sectionId=<%=gccId%>&templateId=<%=templateId%>" target="_new">AbstractView</a> |

                                                    <%
                                                    }
                                                                                                                        } else if (tblTemplateSections.get(i).getContentType().equalsIgnoreCase("Form") || tblTemplateSections.get(i).getContentType().equalsIgnoreCase("TOR")) {
                                                %>
                                <a href="CreateForm.jsp?templateId=<%= templateId%>&sectionId=<%= tblTemplateSections.get(i).getSectionId()%>">Create</a> /
                                                <%
                                                                                                                        } else {
                                                %>
                                <a href="<%=request.getContextPath()%>/admin/CreateSubSection.jsp?templateId=<%=templateId%>&sectionId=<%=tblTemplateSections.get(i).getSectionId()%>">Create</a> |
                                                <%
                                                }
                                                %>

                                            <%
                                            }
                                            %>
                                <a href="TemplateDocument.jsp?templateId=<%= templateId%>&sectionId=<%= tblTemplateSections.get(i).getSectionId()%>" title="Upload">Upload Document</a> |
                                            <a onclick="return confirm('Are you sure you want to delete this section?');" href="<%=request.getContextPath()%>/CreateSubSectionSrBean?action=deleteSubSection&sectionId=<%=tblTemplateSections.get(i).getSectionId()%>&templateId=<%=templateId%>" title="Delete">Delete Section</a>

                                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                <tr>
                                                    <th width="4%">Sl. No.</th>
                                                    <th>File Name</th>
                                                    <th>File Size In KB</th>
                                                    <th>Action</th>
                                                </tr>
                                                <%
                                                    java.util.List<com.cptu.egp.eps.model.table.TblTemplateSectionDocs> docs = defineSTDInDtlSrBean.getTemplateSectionDocs(templateId, (short) tblTemplateSections.get(i).getSectionId());
                                                                            if (docs != null) {
                                                                                if (docs.size() > 0) {
                                                            short j = 0;
                                                                                    for (TblTemplateSectionDocs ttsd : docs) {
                                                                j++;
                                                %>
                                                    <tr>
                                        <td style="text-align:center;"><%= j%></td>
                                        <td style="text-align:left;"><%= ttsd.getDocName()%></td>
                                        <td style="text-align:center;"><%= Integer.parseInt(ttsd.getDocSize()) / 1024%></td>
                                                        <td style="text-align:center;">
                                            <a href="../TemplateFileUploadSrvt?docName=<%= ttsd.getDocName()%>&docSize=<%= ttsd.getDocSize()%>&sectionId=<%= tblTemplateSections.get(i).getSectionId()%>&action=downloadDoc" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                                                        </td>
                                                    </tr>

                                                <%
                                                               ttsd = null;
                                                            }
                                                                                                                                } else {
                                                %>
                                                    <tr>
                                                        <td colspan="4" class="t-align-center">
                                                            No records found
                                                        </td>
                                                    </tr>
                                    <%                                                                                                        }
                                                        docs = null;
                                                                                                                            } else {
                                                %>
                                                <tr>
                                                    <td colspan="4" class="t-align-center">
                                                        No records found
                                                    </td>
                                                </tr>
                                    <%                                                                                        }
                                                %>
                                            </table>
                                            <%
                                                                        if (tblTemplateSections.get(i).getContentType().equalsIgnoreCase("Form") || tblTemplateSections.get(i).getContentType().equalsIgnoreCase("TOR")) {
                                                java.util.List<com.cptu.egp.eps.model.table.TblTemplateSectionForm> forms = defineSTDInDtlSrBean.getTemplateSectionForm(templateId, tblTemplateSections.get(i).getSectionId());
                                            %>
                                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                <tr>
                                                    <th width="4%">Sl. No.</th>
                                                    <th>Form Name</th>
                                        <th>Action</th>
                                                </tr>
                                            <%
                                                                                                                            if (forms != null) {
                                                                                                                                if (forms.size() != 0) {
                                                                                                                                    for (int j = 0; j < forms.size(); j++) {
                                            %>
                                                        <tr>
                                        <td width="4%" style="text-align:center; width:4%;"><%=(j + 1)%></td>
                                        <td><%= forms.get(j).getFormName()%></td>
                                        <td class="t-align-justify" style="line-height: 1.75">
                                            <a href="CreateForm.jsp?templateId=<%= templateId%>&sectionId=<%= tblTemplateSections.get(i).getSectionId()%>&formId=<%= forms.get(j).getFormId()%>">Edit</a> &nbsp; | &nbsp;
                                            <a href="<%=request.getContextPath()%>/CreateSTDForm?action=delForm&templateId=<%= templateId%>&formId=<%= forms.get(j).getFormId()%>" onclick="return confirm('Do you really want to delete this form?');" >Delete</a> &nbsp; | &nbsp;
                                            <a href="TableDashboard.jsp?templateId=<%= templateId%>&sectionId=<%= tblTemplateSections.get(i).getSectionId()%>&formId=<%= forms.get(j).getFormId()%>">Form Dashboard</a> &nbsp; | &nbsp;
                                            <a href="ViewCompleteForm.jsp?templateId=<%= templateId%>&sectionId=<%= tblTemplateSections.get(i).getSectionId()%>&formId=<%= forms.get(j).getFormId()%>">View Form</a>
                                            <%if ("no".equals(forms.get(j).getIsPriceBid())) {%>
                                                                    &nbsp; | &nbsp;
                                            <a href="TestForm.jsp?templateId=<%= templateId%>&sectionId=<%= tblTemplateSections.get(i).getSectionId()%>&formId=<%= forms.get(j).getFormId()%>">Test Form</a>
                                            <%}%> <br/>
                                            <a href="DefineMandatoryDocs.jsp?tId=<%=templateId%>&sId=<%=tblTemplateSections.get(i).getSectionId()%>&fId=<%=forms.get(j).getFormId()%>">Prepare Required Document List</a>&nbsp; | &nbsp;
                                            <a href="ViewMandatoryDocs.jsp?tId=<%=templateId%>&sId=<%=tblTemplateSections.get(i).getSectionId()%>&fId=<%=forms.get(j).getFormId()%>">View Required Document List</a>&nbsp; | &nbsp;
                                            <a href="CreateCombo.jsp?formId=<%= forms.get(j).getFormId()%>&templateId=<%= templateId%>">Create Combo</a>
                                            <%
                                                long lng = createcomboservice.getComboCount(forms.get(j).getFormId());
                                                if(lng!=0)
                                                {
                                            %>&nbsp; | &nbsp;
                                            <a href="ViewCombo.jsp?templetformid=<%= forms.get(j).getFormId()%>&templateId=<%= templateId%>&nw=yes" TARGET = "_blank">View Combo</a>
                                            <%}%>
                                                            </td>
                                                        </tr>
                                            <%
                                                    }
                                                }
                                                forms = null;
                                            }
                                            %>
                                                </table>
                                            <%
                                            }
                                            %>
                                        </td>
                                    </tr>
                                <%
                                }
                                %>

                                </table>
                            </form>
                            <!--Page Content End-->
                </div>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
             <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>