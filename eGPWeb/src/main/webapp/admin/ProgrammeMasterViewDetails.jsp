<%-- 
    Document   : ProgrammeMasterViewDetails
    Created on : Jan 23, 2011, 5:15:28 PM
    Author     : Naresh.Akurathi
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.model.table.TblProgrammeMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ProgrammeMasterSrBean"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - Programme Master</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
    </head>
    <body>
        <%!
        TblProgrammeMaster tpm = new TblProgrammeMaster();
        ProgrammeMasterSrBean prgMasterSrBean = new ProgrammeMasterSrBean();
        %>
        <%
            prgMasterSrBean.setLogUserId(session.getAttribute("userId").toString());
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
       <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <%--<%@include file="/resources/common/AfterLoginLeft.jsp" %>--%>

                        <td class="contentArea">

            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">View Programme Information</div>
            <div class="t-align-left t_space b_space">
                <span style="float: right; text-align: right;">
                        <a class="action-button-goback" href="ProgrammeMasterView.jsp">Go back</a>
                </span>
            </div>

                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <%
                            int id = Integer.parseInt(request.getParameter("id"));
                            String msg = "";
                            prgMasterSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                            List<TblProgrammeMaster> lst = prgMasterSrBean.getData(id);
                            if(!lst.isEmpty()){
                    %>
                    <tr>
                        <td class="ff">Programme Code </td><td class="ff">:</td>
                        <td><%=lst.get(0).getProgCode()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Programme Name </td><td  class="ff">:</td>
                        <td><%=lst.get(0).getProgName()%></td>
                    </tr>
                    <tr>
                        <td class="ff">Implementing Agency </td><td class="ff">:</td>
                        <td><%=lst.get(0).getProgOwner()%></td>
                    </tr>
                    <%}else
                            msg = "No Record Found";
                        %>
                        <tr>
                            <td>&nbsp;</td>
                            <td><%=msg%></td>
                        </tr>
                </table>
                         </td>
                    </tr>
                </table>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@include file="/resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
            <script>
                var obj = document.getElementById('lblProgInfoCreate');
                if(obj != null){
                    if(obj.innerHTML == 'Create'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>


