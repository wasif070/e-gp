<%--
    Document   : CreateForm
    Created on : 24-Oct-2010, 4:49:09 PM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <form action="CreateFormula.jsp" method="post">
                    <div class="pageHead_1">Form Creation</div>
                    <table width="100%" cellspacing="10" class="tableView_1">
                        <tr>
                            <td width="100" class="ff">Form Name :</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="10" class="tableView_1">
                        <tr>
                            <td align="left"><img src="../resources/images/Dashboard/addIcn.png" alt="Add Row" class="linkIcon_1" /><a href="#" title="Add Row">Add Row</a> &nbsp;|&nbsp; <img src="../resources/images/Dashboard/removeIcn.png" alt="Delete Row" class="linkIcon_1" /><a href="#" title="Delete Row">Delete Row</a></td>
                            <td align="right"><img src="../resources/images/Dashboard/addIcn.png" alt="Add Column" class="linkIcon_1" /><a href="#" title="Add Column">Add Column</a> &nbsp;|&nbsp; <img src="../resources/images/Dashboard/removeIcn.png" alt="Delete Column" class="linkIcon_1" /><a href="#" title="Delete Column">Delete Column</a></td>
                        </tr>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th width="2%">&nbsp;</th>
                            <th width="2%">&nbsp;</th>
                            <th style="text-align:center;"><input type="checkbox" name="checkbox2" id="checkbox2" /></th>
                            <th style="text-align:center;"><input type="checkbox" name="checkbox2" id="checkbox2" /></th>
                            <th style="text-align:center;"><input type="checkbox" name="checkbox2" id="checkbox2" /></th>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><table border="0" cellspacing="5" cellpadding="0" class="formStyle_1">
                                    <tr>
                                        <td class="ff">Column Header :</td>
                                        <td><textarea name="textarea" rows="3" class="formTxtBox_1" id="textarea" style="width:170px;"></textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Filled By :</td>
                                        <td><select name="select" class="formTxtBox_1" id="select">
                                                <option>Select</option>
                                                <option>Govt. User</option>
                                                <option>Bidder/Consultant</option>
                                                <option>Auto</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Data Type :</td>
                                        <td><select name="select5" class="formTxtBox_1" id="select5">
                                                <option>Select</option>
                                                <option>Small Text</option>
                                                <option>Long Text</option>
                                                <option>Money Positive</option>
                                                <option>Money All</option>
                                                <option>Numeric</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Show / Hide :</td>
                                        <td><select name="select" class="formTxtBox_1" id="select3">
                                                <option>Select</option>
                                                <option>Yes</option>
                                                <option>No</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Add Column Type :</td>
                                        <td><select name="select5" class="formTxtBox_1" id="select5">
                                                <option>Select</option>
                                                <option>Normal</option>
                                                <option>Qty</option>
                                                <option>Qty by Bidder/Consultant</option>
                                                <option>EE Unit Rate</option>
                                                <option>EE Total Rate</option>
                                                <option>Unit Rate</option>
                                                <option>Total Rate</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Sort Order :</td>
                                        <td><input name="textfield" type="text" class="formTxtBox_1" id="textfield" style="width:50px;" /></td>
                                    </tr>



                                </table></td>
                            <td><table border="0" cellspacing="5" cellpadding="0" class="formStyle_1">
                                    <tr>
                                        <td class="ff">Column Header :</td>
                                        <td><textarea name="textarea2" rows="3" class="formTxtBox_1" id="textarea2" style="width:170px;"></textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Filled By :</td>
                                        <td><select name="select" class="formTxtBox_1" id="select">
                                                <option>Select</option>
                                                <option>Govt. User</option>
                                                <option>Bidder/Consultant</option>
                                                <option>Auto</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Data Type :</td>
                                        <td><select name="select5" class="formTxtBox_1" id="select5">
                                                <option>Select</option>
                                                <option>Small Text</option>
                                                <option>Long Text</option>
                                                <option>Money Positive</option>
                                                <option>Money All</option>
                                                <option>Numeric</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Show / Hide :</td>
                                        <td><select name="select" class="formTxtBox_1" id="select3">
                                                <option>Select</option>
                                                <option>Yes</option>
                                                <option>No</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Add Column Type :</td>
                                        <td><select name="select5" class="formTxtBox_1" id="select5">
                                                <option>Select</option>
                                                <option>Normal</option>
                                                <option>Qty</option>
                                                <option>Qty by Bidder/Consultant</option>
                                                <option>EE Unit Rate</option>
                                                <option>EE Total Rate</option>
                                                <option>Unit Rate</option>
                                                <option>Total Rate</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Sort Order :</td>
                                        <td><input name="textfield" type="text" class="formTxtBox_1" id="textfield" style="width:50px;" /></td>
                                    </tr>

                                </table></td>
                            <td><table border="0" cellspacing="5" cellpadding="0" class="formStyle_1">
                                    <tr>
                                        <td class="ff">Column Header :</td>
                                        <td><textarea name="textarea3" rows="3" class="formTxtBox_1" id="textarea3" style="width:170px;"></textarea></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Filled By :</td>
                                        <td><select name="select" class="formTxtBox_1" id="select">
                                                <option>Select</option>
                                                <option>Govt. User</option>
                                                <option>Bidder/Consultant</option>
                                                <option>Auto</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Data Type :</td>
                                        <td><select name="select5" class="formTxtBox_1" id="select5">
                                                <option>Select</option>
                                                <option>Small Text</option>
                                                <option>Long Text</option>
                                                <option>Money Positive</option>
                                                <option>Money All</option>
                                                <option>Numeric</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Show / Hide :</td>
                                        <td><select name="select" class="formTxtBox_1" id="select3">
                                                <option>Select</option>
                                                <option>Yes</option>
                                                <option>No</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Add Column Type :</td>
                                        <td><select name="select5" class="formTxtBox_1" id="select5">
                                                <option>Select</option>
                                                <option>Normal</option>
                                                <option>Qty</option>
                                                <option>Qty by Bidder/Consultant</option>
                                                <option>EE Unit Rate</option>
                                                <option>EE Total Rate</option>
                                                <option>Unit Rate</option>
                                                <option>Total Rate</option>
                                            </select></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Sort Order :</td>
                                        <td><input name="textfield" type="text" class="formTxtBox_1" id="textfield" style="width:50px;" /></td>
                                    </tr>

                                </table></td>
                        </tr>
                        <tr>
                            <td align="center"><input type="checkbox" name="checkbox" id="checkbox" /></td>
                            <td align="center"><select name="select2" class="formTxtBox_1" id="select2">
                                    <option>1</option>
                                </select></td>
                            <td><input name="textfield5" type="text" class="formTxtBox_1" id="textfield4" style="width:225px; height: 30px" /></td>
                            <td><input name="textfield5" type="text" class="formTxtBox_1" id="textfield4" style="width:225px; height: 30px" /></td>
                            <td><input name="textfield5" type="text" class="formTxtBox_1" id="textfield4" style="width:225px; height: 30px" /></td>
                        </tr>
                    </table>
                    <div align="center"><label class="formBtn_1"><input type="submit" name="button2" id="button2" value="Next Step" /></label></div>
                </form>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
            <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
