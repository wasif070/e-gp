<%--
    Document   : AmendmentRule
    Created on : Nov 21, 2010, 12:28:20 PM
    Author     : Naresh.Akurathi
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigAmendment"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Amendment Business Rule</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <script type="text/javascript">
            function check()
            {
                var count=document.getElementById("TotRow").value;
                var flag=true;

                for(var k=1;k<=count;k++)
                {
                    if(document.getElementById("amendPercent_"+k)!= null)
                    {
                        if(document.getElementById("amendPercent_"+k).value=="")
                        {
                            document.getElementById("AmendPercent_"+k).innerHTML="<br />Please specify % of total time allowed for publishing Corrigendum / Amendment";
                            flag=false;
                        }
                        else
                        {
                            if(digits(document.getElementById("amendPercent_"+k).value))
                            {
                                if(!ChkMaxLength(document.getElementById("amendPercent_"+k).value))
                                {
                                    document.getElementById("AmendPercent_"+k).innerHTML="<br /> % of time must not exceed 100.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("AmendPercent_"+k).innerHTML="";

                                }
                            }
                            else
                            {
                                document.getElementById("AmendPercent_"+k).innerHTML="<br />Please enter numbers only.";
                                flag=false;
                            }
                        }
                    }
                    if(document.getElementById("amendMinDays_"+k) != null)
                    {
                        if(document.getElementById("amendMinDays_"+k).value=="")
                        {
                            document.getElementById("AmendMinDays_"+k).innerHTML="<br />Please specify minimum no. of days by which Corrigendum / Amendment should be extended.";
                            flag=false;
                        }
                        else
                        {
                            if(digits(document.getElementById("amendMinDays_"+k).value))
                            {
                                if(!ChkMaxLen(document.getElementById("amendMinDays_"+k).value))
                                {
                                    document.getElementById("AmendMinDays_"+k).innerHTML="<br/>Maximum 3 numbers  without decimal are allowed.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("AmendMinDays_"+k).innerHTML="";

                                }
                            }
                            else
                            {
                                document.getElementById("AmendMinDays_"+k).innerHTML="<br/>Please enter numbers only.";
                                flag=false;
                            }
                        }
                    }
                }

                // Start OF--Checking for Unique Rows
                if(document.getElementById("TotRow")!=null){
                    var totalcount = eval(document.getElementById("TotRow").value);} //Total Count After Adding Rows
                //alert(totalcount);
                var chk=true;
                var i=0;
                var j=0;
                for(i=1; i<=totalcount; i++) //Loop For Newly Added Rows
                {
                    if(document.getElementById("amendPercent_"+i)!=null)
                    if($.trim(document.getElementById("amendPercent_"+i).value)!='' && $.trim(document.getElementById("amendMinDays_"+i).value) !='') // If Condition for When all Data are filled thiscode is Running
                    {

                        for(j=1; j<=totalcount && j!=i; j++) // Loop for Total Count but Not same as (i) value
                        {
                            chk=true;
                            //If Condition for Check Duplicate Rows are there or not.If Columns are diff then chk variable set to false
                            //IF Row is same Give alert message.
                            if($.trim(document.getElementById("procType_"+i).value) != $.trim(document.getElementById("procType_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("procMethod_"+i).value) != $.trim(document.getElementById("procMethod_"+j).value))
                            {
                                chk=false;
                            }
                            /*     if($.trim(document.getElementById("amendPercent_"+i).value) != $.trim(document.getElementById("amendPercent_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("amendMinDays_"+i).value) != $.trim(document.getElementById("amendMinDays_"+j).value))
                            {
                                chk=false;
                            }
                             */if(flag){
                            if(chk==true) //If Row is same then give alert message
                            {
                                    jAlert("Duplicate record found. Please enter unique record","Amendment Business Rule Configuration",function(RetVal) {
                                    });
                                return false;
                            }
                        }
                        }

                    }
                }
                // End OF--Checking for Unique Rows

                if (flag==false){

                    return false;
                }
            }

            function ChkMaxLength(value)
            {
                var ValueChk=value;
                //alert(ValueChk);
                if(ValueChk>100)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            function ChkMaxLen(value)
            {
                var ValChk=value;
                if(ValChk.length>3)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            function ChkamendPercent(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk=true;

                if(obj.value!='')
                {
                    if(digits(obj.value))
                    {
                        if(!ChkMaxLength(obj.value))
                        {
                            document.getElementById("AmendPercent_"+i).innerHTML="<br/> % of time must not exceed 100.";
                            chk=false;
                        }
                        else
                        {
                            document.getElementById("AmendPercent_"+i).innerHTML="";
                        }

                        //document.getElementById("AmendPercent_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("AmendPercent_"+i).innerHTML="<br/>Please enter numbers only.";
                        chk=false;
                    }
                }
                else
                {
                    document.getElementById("AmendPercent_"+i).innerHTML="<br/>Please specify % of total time allowed for publishing Corrigendum / Amendment";
                    chk=false;
                }
                if(chk==false)
                {
                    return false;
                }
            };

            function ChkamendMinDays(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk1=true;

                if(obj.value!='')
                {
                    if(digits(obj.value))
                    {
                        if(!ChkMaxLen(obj.value))
                        {
                            document.getElementById("AmendMinDays_"+i).innerHTML="<br/>Maximum 3 numbers  without decimal are allowed.";
                            chk1=false;
                        }
                        else
                        {
                            document.getElementById("AmendMinDays_"+i).innerHTML="";
                        }
                        //document.getElementById("AmendMinDays_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("AmendMinDays_"+i).innerHTML="<br/>Please enter numbers only.";
                        chk1=false;
                    }
                }
                else
                {
                    document.getElementById("AmendMinDays_"+i).innerHTML="<br/>Please specify minimum no. of days by which Corrigendum / Amendment should be extended.";
                    chk1=false;
                }
                if(chk1==false)
                {
                    return false;
                }

            };


            function digits(value)
            {
                return /^\d+$/.test(value);
            };

        </script>

        <%!    ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();

        %>


        <%
                    StringBuilder tenderType = new StringBuilder();
                    StringBuilder procNature = new StringBuilder();
                    StringBuilder procType = new StringBuilder();
                    StringBuilder procMethod = new StringBuilder();

                    TblProcurementMethod tblProcurementMethod2 = new TblProcurementMethod();
                    Iterator pmit2 = configPreTenderRuleSrBean.getProcurementMethod().iterator();
                    while (pmit2.hasNext()) {
                        tblProcurementMethod2 = (TblProcurementMethod) pmit2.next();
                        procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>" + tblProcurementMethod2.getProcurementMethod() + "</option>");
                    }

                    TblProcurementTypes tblProcurementTypes2 = new TblProcurementTypes();
                    Iterator ptit2 = configPreTenderRuleSrBean.getProcurementTypes().iterator();
                    String procureType="";
                    while (ptit2.hasNext()) {
                        tblProcurementTypes2 = (TblProcurementTypes) ptit2.next();
                        if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("NCT"))
                            procureType="NCB";
                        else if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("ICT"))
                            procureType="ICB";
                        procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + procureType + "</option>");
                    }


        %>


        <script type="text/javascript">


            var totCnt = 1;

            $(function() {
                $('#linkAddRule').click(function() {
                    var count=($("#tblrule tr:last-child").attr("id").split("_")[1]*1);

                    var htmlEle = "<tr id='trrule_"+ (count+1) +"'>"+
                        "<td class='t-align-center'><input type='checkbox' name='checkbox"+(count+1)+"' id='checkbox_"+(count+1)+"' value='"+(count+1)+"' /></td>"+
                        "<td class='t-align-center'><select name='procType"+(count+1)+"' class='formTxtBox_1' id='procType_"+(count+1)+"'><%=procType%></select></td>"+
                        "<td class='t-align-center'><select name='procMethod"+(count+1)+"' class='formTxtBox_1' id='procMethod_"+(count+1)+"'><%=procMethod%></select></td>"+
                        "<td class='t-align-center'><input name='amendPercent"+(count+1)+"' type='text' class='formTxtBox_1' id='amendPercent_"+(count+1)+"' style='width:90%;' onBlur='return ChkamendPercent(this);'/><span id='AmendPercent_"+ (count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><input name='amendMinDays"+(count+1)+"' type='text' class='formTxtBox_1' id='amendMinDays_"+(count+1)+"' style='width:90%;'  onBlur='return ChkamendMinDays(this);'/><span id='AmendMinDays_"+(count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "</tr>";

                    totCnt++;
                    $("#tblrule").append(htmlEle);
                    document.getElementById("TotRow").value = (count+1);

                });
            });


            $(function() {
                $('#linkDelRule').click(function() {
                    var delcnt=0;
                    var cnt = 0;
                    var tmpCnt = 0;
                    var delr=0;
                    var counter = ($("#tblrule tr:last-child").attr("id").split("_")[1]*1);

                    for(var i=1;i<=counter;i++){
                        if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                            tmpCnt++;

                        }
                    }

                    if(tmpCnt==totCnt){
                        $('span.#lotMsg').css("visibility","visible");
                        $('span.#lotMsg').css("color","red");
                        $('span.#lotMsg').html('Minimum 1 record is needed!');
                    }else{
                        for(var i=1;i<=counter;i++){
                            if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){

                                $("tr[id='trrule_"+i+"']").remove();
                                $('span.#lotMsg').css("visibility","collapse");

                                delr+=","+i;
                                document.getElementById('delrow').value=delr;
                                cnt++;
                            }
                        }
                        totCnt -= cnt;
                    }

                });
            });

        </script>

    </head>

    <body>

        <%
                    String msg = "";
                    if ("Submit".equals(request.getParameter("button"))) {

                        int row = Integer.parseInt(request.getParameter("TotRow"));

                        for (int i = 1; i <= row; i++) {
                            if (request.getParameter("procType" + i) != null) {

                                byte ptypes = Byte.parseByte(request.getParameter("procType" + i));
                                byte pmethod = Byte.parseByte(request.getParameter("procMethod" + i));
                                float amendPercent = Float.parseFloat(request.getParameter("amendPercent" + i));
                                short amendMinDays = Short.parseShort(request.getParameter("amendMinDays" + i));

                                TblConfigAmendment configAmend = new TblConfigAmendment();
                                configAmend.setTblProcurementTypes(new TblProcurementTypes(ptypes));
                                configAmend.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                configAmend.setAmendPercent(new BigDecimal(amendPercent));
                                configAmend.setAmendMinDays(amendMinDays);


                                msg = configPreTenderRuleSrBean.addConfigAmendment(configAmend);

                            }
                        }
                        if (msg.equals("Values Added")) {
                            msg = "Amendment Business Rule Configured Successfully";
                            response.sendRedirect("AmendmentRuleView.jsp?msg=" + msg);

                        }

                    }
        %>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">

                        <div class="pageHead_1">Amendment Business Rule Configuration</div>

                        <div style="font-style: italic" class="t-align-left t_space"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>

                        <div align="right" class="t_space b_space">
                            <span id="lotMsg" style="color: red; font-weight: bold; float: left; visibility: collapse">&nbsp;</span>
                            <a id="linkAddRule"class="action-button-add">Add Rule</a> <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a>
                        </div>




                        <form action="AmendmentRule.jsp" method="post">
                            <%--<div align="center"> <%=msg%></div>--%>
                            <table width="100%" cellspacing="0" class="tableList_1" id="tblrule" name="tblrule">
                                <tr>
                                    <th >Select</th>
                                    <th width="16%" >Procurement Type <br />(<span class="mandatory">*</span>)</th>
                                    <th width="18%" >Procurement Method <br />(<span class="mandatory">*</span>)</th>
                                    <th width="36%" >Time allowed for publishing Corrigendum / Amendment as % of Total time allowed (Date of Tender Submission - Date of Tender Publication)<br />(<span class="mandatory">*</span>)</th>
                                    <th width="30%" >Specify minimum no. of days by which Submission Date should be extended<br />(<span class="mandatory">*</span>)</th>
                                </tr>


                                <input type="hidden" name="delrow" value="0" id="delrow"/>
                                <input type="hidden" name="TotRow" id="TotRow" value="1"/>
                                <input type="hidden" name="introw" value="" id="introw"/>
                                <tr id="trrule_1">
                                    <td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_1" value="1" /></td>
                                    <td class="t-align-center"><select name="procType1" class="formTxtBox_1" id="procType_1">
                                            <%=procType%></select>
                                    </td>
                                    <td class="t-align-center"><select name="procMethod1" class="formTxtBox_1" id="procMethod_1">
                                            <%=procMethod%></select>
                                    </td>
                                    <td class="t-align-center"><input name="amendPercent1" type="text" class="formTxtBox_1" id="amendPercent_1" style="width:90%;" onBlur="return ChkamendPercent(this);" /><span id="AmendPercent_1" style="color:red;"></span></td>
                                    <td class="t-align-center"><input name="amendMinDays1" type="text" class="formTxtBox_1" id="amendMinDays_1" style="width:90%;" onBlur="return ChkamendMinDays(this);" /><span id="AmendMinDays_1" style="color:red;"></span></td>
                                </tr>
                            </table>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" >
                                <tr>
                                    <td colspan="11" class="t-align-center"><span class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Submit" onclick="return check();"/>
                                        </span></td>
                                </tr>
                            </table>
                        </form>

                    </td>
                </tr>
            </table>

            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
         <script>
                var obj = document.getElementById('lblAmendmentRuleAdd');
                if(obj != null){
                    if(obj.innerHTML == 'Add'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
        <script>
            var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>

