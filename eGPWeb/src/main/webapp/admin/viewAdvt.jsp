<%--
    Document   : viewAdvt
    Created on : Jan 24, 2013, 4:32:11 PM
    Author     : spandana.vaddi
--%>

<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AdvertisementService"%>
<%@page import="com.cptu.egp.eps.model.table.TblAdvertisement"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.service.serviceimpl.PublicForumPostService" %>
<%@page import="com.cptu.egp.eps.model.table.TblPublicProcureForum" %>
<%@page import="java.util.Date" %>
<%@page import=" com.cptu.egp.eps.web.utility.HandleSpecialChar" %>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Cache-Control", "no-store");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Verify Reply Detail</title>
        <link href="../resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>

    </head>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <div class="pageHead_1"> Advertisement Details <span class="c-alignment-right"><a href="/admin/ViewAdvtTopic.jsp" class="action-button-goback">Go Back</a></span>  </div>
                <form id="frmPublicProcForum" name="frmPublicProcForum" method="post" action="/MakeAdvertisement?funcName=process&adid=<%=request.getParameter("wsOrgId")%>" >
                    <% ArrayList<TblAdvertisement> cont = (ArrayList<TblAdvertisement>) request.getAttribute("items");
                                for (TblAdvertisement tbl : cont) {
                    %>
                    <table cellspacing="10" class="formStyle_1 t_space" width="100%">
                        <input type="hidden" id="adid" value="<%= tbl.getAdId() %>"/>
                        <tr>
                            <td width="13%" class="ff">Design :</td>
                            <td>
                                <%try{
                               AdvertisementService advertisementService = (AdvertisementService) AppContext.getSpringBean("AdvertisementService");
                               List<TblAdvertisement> ads=advertisementService.findTblAdvertisement("adId",Operation_enum.EQ,tbl.getAdId());
                               if(ads.size()>0){%>
                               <%for(TblAdvertisement tblimg :ads){%>
                               <img class="linkIcon_1" alt="<%=tbl.getBannerName()%>" src="<%=request.getContextPath()%>/MakeAdvertisement?funcName=image&adId=<%=tblimg.getAdId()%>"/>
                                <%}}}catch(Exception e){
                                  e.printStackTrace();
                                }
                                %>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">File size :</td>
                            <td><%= tbl.getBannerSize() %></td>
                        </tr>
                        <tr>
                            <td class="ff">Dimensions :</td>
                            <td><%= tbl.getDimension() %></td>
                        </tr>
                        <tr>
                           <td class="ff">Description :</td>
                           <td><%= tbl.getDescription() %></td>
                       </tr>
                       <tr>
                           <td class="ff">Price :</td>
                           <td><%= tbl.getPrice() %></td>
                       </tr>
                        <tr>
                            <td class="ff">Verification Status :</td>
                            <td><%= tbl.getStatus() %></td>
                        </tr>
                        <tr>
                            <td class="ff">User Name :</td>
                            <td><%= tbl.getUserName()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Organization :</td>
                            <td><%= tbl.getOrganization()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Address :</td>
                            <td><%= tbl.getAddress()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Phone Number :</td>
                            <td><%= tbl.getPhoneNumber()%></td>
                        </tr>
                        <tr>
                            <td class="ff">e-Mail Id :</td>
                            <td><%= tbl.geteMailId()%></td>
                        </tr>
                        <tr>
                            <%if (!(request.getParameter("pubaction") == null) && (request.getParameter("pubaction").equalsIgnoreCase("published"))) {%>
                            <%} else {%>
                            <td class="ff">Action :</td>
                            <td><select name="adminaction" class="formTxtBox_1" id="adminaction" style="width:85px;">
                                    <option value="Accepted">Accept</option>
                                    <option value="Rejected">Reject</option>
                                </select></td>
                            <%}%>
                        </tr>
                        <tr>
                            <td class="ff">Comments :</td>
                            <%if (!(request.getParameter("pubaction")==null) && (request.getParameter("pubaction").equalsIgnoreCase("published"))){%>
                            <td><%= tbl.getComments() %></td>
                            <%}else {%>
                            <td><div id="ketan"> <textarea cols="5"  rows="5" id="comments" name="comments" class="formTxtBox_1" style="width:60%;"></textarea>
                                </div></td>
                            <%}%>
                        </tr>
                        <tr>
                            <%if (!(request.getParameter("pubaction")==null) && (request.getParameter("pubaction").equalsIgnoreCase("published"))){%>
                            <td></td>
                            <%}else{%>
                            <td colspan="4" class="t-align-center"><label class="formBtn_1 t-align-center">
                                    <input type="submit" name="button" id="button" value="Submit"/>
                                </label></td>
                            <%}%>
                        </tr>
                    </table>
                    <%}%>
                </form>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
            </div>
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
    </body>
</html>

