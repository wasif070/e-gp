<%--
    Document   : TSCFormationRule
    Created on : Nov 17, 2010, 7:32:49 PM
    Author     : Naresh.Akurathi
--%>


<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigTec"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>TC Formation Business Rule</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />       
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <script type="text/javascript">

            function check()
            {
                //var count=document.getElementById("TotRule").value;
                var count=document.getElementById("TotRow").value;
                var flag=true;

                for(var k=1;k<=count;k++)
                {
                    if(document.getElementById("minMemb_"+k)!=null)
                    {
                        if(document.getElementById("minMemb_"+k).value=="")
                        {
                            document.getElementById("MinMemb_"+k).innerHTML="<br/>Please Enter Min. Member Required.";
                            flag=false;
                        }
                        else
                        {
                            if(numeric(document.getElementById("minMemb_"+k).value))
                            {
                                if(!numberZero(document.getElementById("minMemb_"+k).value))
                                {
                                    document.getElementById("MinMemb_"+k).innerHTML="<br/>Please enter numbers between (1-9) only.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("MinMemb_"+k).innerHTML="";
                                }
                            }
                            else
                            {
                                document.getElementById("MinMemb_"+k).innerHTML="</br>Please Enter numbers only.";
                                flag=false;
                            }
                        }
                    }

                    if(document.getElementById("maxMemb_"+k)!=null)
                    {
                        if(document.getElementById("maxMemb_"+k).value=="")
                        {
                            document.getElementById("MaxMemb_"+k).innerHTML="<br/>Please Enter Max. Member Required.";
                            flag=false;
                        }
                        else
                        {
                            if(numeric(document.getElementById("maxMemb_"+k).value))
                            {
                                document.getElementById("MaxMemb_"+k).innerHTML="";
                            }
                            else
                            {
                                document.getElementById("MaxMemb_"+k).innerHTML="</br>Please Enter numbers only.";
                                flag=false;
                            }
                        }
                    }
                    
                }
                // Start OF--Checking for Unique Rows

                if(document.getElementById("TotRow")!=null){
                    var totalcount = eval(document.getElementById("TotRow").value);} //Total Count After Adding Rows
                //alert(totalcount);
                var chk=true;
                var i=0;
                var j=0;
                for(i=1; i<=totalcount; i++) //Loop For Newly Added Rows
                {
                    if(document.getElementById("minMemb_"+i)!=null)
                    if($.trim(document.getElementById("minMemb_"+i).value)!='' && $.trim(document.getElementById("maxMemb_"+i).value) !='') // If Condition for When all Data are filled thiscode is Running
                    {

                        for(j=1; j<=totalcount && j!=i; j++) // Loop for Total Count but Not same as (i) value
                        {
                            chk=true;
                            //If Condition for Check Duplicate Rows are there or not.If Columns are diff then chk variable set to false
                            //IF Row is same Give alert message.
                            if($.trim(document.getElementById("commitType_"+i).value) != $.trim(document.getElementById("commitType_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("procNature_"+i).value) != $.trim(document.getElementById("procNature_"+j).value))
                            {
                                chk=false;
                            }
                       /*     if($.trim(document.getElementById("minMemb_"+i).value) != $.trim(document.getElementById("minMemb_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("maxMemb_"+i).value) != $.trim(document.getElementById("maxMemb_"+j).value))
                            {
                                chk=false;
                            }*/
                            //alert(j);
                            //alert("chk" +chk);
                            if(chk==true) //If Row is same then give alert message
                            {
                                jAlert("Duplicate record found. Please enter unique record","TC Formation Business Rule Configuration",function(RetVal) {
                                });
                                return false;
                            }
                        }

                    }
                }
                // End OF--Checking for Unique Rows


                if (flag==false)
                {
                    return false;
                }
            }
            function ChkminMemb(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk=true;

                if(obj.value!='')
                {
                    if(numeric(obj.value))
                    {
                        if(!numberZero(obj.value))
                        {
                            document.getElementById("MinMemb_"+i).innerHTML="<br/>Please enter numbers between (1-9) only.";
                            flag=false;
                        }
                        else
                        {
                            document.getElementById("MinMemb_"+i).innerHTML="";
                        }
                       
                    }
                    else
                    {
                        document.getElementById("MinMemb_"+i).innerHTML="</br>Please Enter numbers only.";
                        chk=false;
                    }
                }
                else
                {
                    document.getElementById("MinMemb_"+i).innerHTML="</br>Please Enter Min. Member Required.";
                    chk=false;
                }
                if(chk==false)
                {
                    return false;
                }
            }

            function ChkmaxMemb(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk1=true;

                if(obj.value!='')
                {
                    if(numeric(obj.value))
                    {
                        document.getElementById("MaxMemb_"+i).innerHTML="";
                    }
                    else
                    {
                        document.getElementById("MaxMemb_"+i).innerHTML="</br>Please Enter numbers only.";
                        chk1=false;
                    }
                }
                else
                {
                    document.getElementById("MaxMemb_"+i).innerHTML="</br>Please Enter Max. Member Required.";
                    chk1=false;
                }
                if(chk1==false)
                {
                    return false;
                }
            }

            function numberZero(value)
            {
                var flag ;
                //alert(value.indexOf("."));
                if(isNaN(value)==false)
                {
                    if(value.indexOf(".")==-1)
                    {
                        if(value>0)
                        {
                            flag=true;
                        }
                        else
                        {
                            flag=false;
                        }
                    }
                    else
                    {
                        flag=false;
                    }
                }
                else
                {
                    flag=false;
                }
                if(flag==false)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            function numeric(value) {
                return /^\d+$/.test(value);
            }


        </script>


        <%!    ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();

        %>


        <%
                    StringBuilder tenderType = new StringBuilder();
                    StringBuilder procNature = new StringBuilder();


                    TblProcurementNature tblProcureNature2 = new TblProcurementNature();
                    Iterator pnit2 = configPreTenderRuleSrBean.getProcurementNature().iterator();
                    while (pnit2.hasNext()) {
                        tblProcureNature2 = (TblProcurementNature) pnit2.next();
                        procNature.append("<option value='" + tblProcureNature2.getProcurementNatureId() + "'>" + tblProcureNature2.getProcurementNature() + "</option>");
                    }


        %>


        <script type="text/javascript">


            var totCnt = 1;

            $(function() {
                $('#linkAddRule').click(function() {
                    var count=($("#tbltec tr:last-child").attr("id").split("_")[1]*1);

                    var htmlEle = "<tr id='trtec_"+ (count+1) +"'>"+
                        "<td class='t-align-center'><input type='checkbox' name='checkbox"+(count+1)+"' id='checkbox_"+(count+1)+"' value='"+(count+1)+"' style='width:90%;'/></td>"+
                        "<td class='t-align-center'><input type='text' name='commitType"+(count+1)+"' id='commitType_"+(count+1)+"' value='TC' readonly='readonly' class='formTxtBox_1' style='width:90%;'/></td>"+
                        "<td class='t-align-center'><select name='procNature"+(count+1)+"' class='formTxtBox_1' id='procNature_"+(count+1)+"'><%=procNature%></select></td>"+
                        "<td  class='t-align-center' style='display: none;'><input type='text' name='minTender"+(count+1)+"' class='formTxtBox_1' id='minTender_"+(count+1)+"' value='0' readonly='readonly' style='width:90%;'/></td>"+
                        "<td  class='t-align-center' style='display: none;'><input type='text' name='maxTender"+(count+1)+"' class='formTxtBox_1' id='maxTender_"+(count+1)+"' value='0' readonly='readonly' style='width:90%;'/></td>"+
                        "<td  class='t-align-center'><input type='text' name='minMemb"+(count+1)+"' class='formTxtBox_1' id='minMemb_"+(count+1)+"' style='width:90%;' onBlur='return ChkminMemb(this);' maxlength='2'/><span id='MinMemb_"+ (count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td  class='t-align-center'><input type='text' name='maxMemb"+(count+1)+"' class='formTxtBox_1' id='maxMemb_"+(count+1)+"' style='width:90%;' onBlur='return ChkmaxMemb(this);' maxlength='2'/><span id='MaxMemb_"+ (count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td  class='t-align-center' style='display: none;'><input type='text' name='minMemOutsidePe"+(count+1)+"' class='formTxtBox_1' id='minMemOutsidePe_"+(count+1)+"' value='0' readonly='readonly' style='width:90%;'/></td>"+
                        "<td  class='t-align-center' style='display: none;'><input type='text' name='minMemFromPe"+(count+1)+"' class='formTxtBox_1' id='minMemFromPe_"+(count+1)+"' value='0' readonly='readonly' style='width:90%;'/></td>"+
                        "<td  class='t-align-center' style='display: none;'><input type='text' name='minMemFromTEC"+(count+1)+"' class='formTxtBox_1' id='minMemFromTEC_"+(count+1)+"' value='0' readonly='readonly' style='width:90%;'/></td>"+
                        "</tr>";

                    totCnt++;
                    $("#tbltec").append(htmlEle);
                    document.getElementById("TotRow").value = (count+1);

                });
            });


            $(function() {
                $('#linkDelRule').click(function() {

                    var cnt = 0;
                    var tmpCnt = 0;
                    var counter = ($("#tbltec tr:last-child").attr("id").split("_")[1]*1);

                    for(var i=1;i<=counter;i++){
                        if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                            tmpCnt++;
                        }
                    }

                    if(tmpCnt==totCnt){
                        $('span.#lotMsg').css("visibility","visible");
                        $('span.#lotMsg').css("color","red");
                        $('span.#lotMsg').html('Atlest one lot has to be remain!!!');
                    }else{
                        for(var i=1;i<=counter;i++){
                            if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                                $("tr[id='trtec_"+i+"']").remove();
                                $('span.#lotMsg').css("visibility","collapse");

                                document.getElementById('delrow').value=i;
                                cnt++;
                            }
                        }
                        totCnt -= cnt;
                    }

                });
            });
        </script>
    </head>
    <body>

        <%
                    String msg = "";
                    if ("Submit".equals(request.getParameter("button"))) {
                        int row = Integer.parseInt(request.getParameter("TotRow"));
                        
                        for (int i = 1; i <= row; i++) {
                            if (request.getParameter("procNature" + i) != null) {

                                byte pnature = Byte.parseByte(request.getParameter("procNature" + i));
                                double minTender = Double.parseDouble(request.getParameter("minTender" + i));
                                double maxTender = Double.parseDouble(request.getParameter("maxTender" + i));
                                byte minmem = Byte.parseByte(request.getParameter("minMemb" + i));
                                byte maxmem = Byte.parseByte(request.getParameter("maxMemb" + i));
                                byte minoutsidepr = Byte.parseByte(request.getParameter("minMemOutsidePe" + i));
                                byte minmemFromPe = Byte.parseByte(request.getParameter("minMemFromPe" + i));
                                byte minmemformtec = Byte.parseByte(request.getParameter("minMemFromTEC" + 1));
                                TblConfigTec configTec = new TblConfigTec();
                                configTec.setCommitteeType(request.getParameter("commitType" + i));
                                configTec.setTblProcurementNature(new TblProcurementNature(pnature));
                                configTec.setMinTenderVal(new BigDecimal(minTender));
                                configTec.setMaxTenderVal(new BigDecimal(maxTender));
                                configTec.setMinMemReq(minmem);
                                configTec.setMaxMemReq(maxmem);
                                configTec.setMinMemOutSidePe(minoutsidepr);
                                configTec.setMinMemFromPe(minmemFromPe);
                                configTec.setMinMemFromTec(minmemformtec);


                                msg = configPreTenderRuleSrBean.addConfigTec(configTec);

                            }
                        }
                        if (msg.equals("Values Added")) {
                            msg = "TC formation Business Rule Configured Successfully";
                            response.sendRedirect("TSCFormationRuleView.jsp?msg=" + msg);
                        }
                    }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">

                        <!--Dashboard Header End-->
                        <!--Dashboard Content Part Start-->
                        <div class="pageHead_1">TC Formation Business Rule</div>

                        <div style="font-style: italic" class="t-align-left t_space"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>

                        <div align="right" class="t_space"><a id="linkAddRule" class="action-button-add">Add Rule</a> <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a></div>




                        <%--<div class="pageHead_1" align="center"> <%=msg%></div>--%>
                        <form action="TSCFormationRule.jsp" method="post">
                            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tbltec" name="tbltec">
                                <tr>
                                    <th width="5%" >Select</th>
                                    <th width="8%" >Committee Type<br />(<span class="mandatory">*</span>)</th>
                                    <th width="8%" >Procurement Category<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%"  style="display: none;">Min.  Tender  <br />value(In Mn. Tk.)<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%"  style="display: none;">Max. Tender <br />value(In Mn. Tk.)<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%" >Min. Member<br />Required<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%" >Max. Member<br /> Required<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%"  style="display: none;">Min. Members <br />Outside PA<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%"  style="display: none;">Min. Member <br />from PA <br />(<span class="mandatory">*</span>) </th>
                                    <th width="10%"  style="display: none;">Min. Member <br />from TEC <br />(<span class="mandatory">*</span>)       </th>
                                </tr>
                                <input type="hidden" name="delrow" value="0" id="delrow"/>
                                <input type="hidden" name="TotRow" id="TotRow" value="1"/>
                                <input type="hidden" name="introw" value="" id="introw"/>
                                <tr id="trtec_1">
                                    <td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_1" value="" /></td>
                                    <td class="t-align-center"><input name="commitType1" type="text" class="formTxtBox_1" id="commitType_1" style="width:90%;" value="TC" readonly="readonly"/></td>
                                    <td class="t-align-center"><select name="procNature1" class="formTxtBox_1" id="procNature_1">
                                            <%=procNature%></select>
                                    </td>
                                    <td class="t-align-center" style="display: none;"><input name="minTender1" type="text" class="formTxtBox_1" id="minTender_1" style="width:90%;" value="0" readonly="readonly"/></td>
                                    <td class="t-align-center" style="display: none;"><input name="maxTender1" type="text" class="formTxtBox_1" id="maxTender_1" style="width:90%;" value="0" readonly="readonly"/></td>
                                    <td class="t-align-center"><input name="minMemb1" type="text" class="formTxtBox_1" id="minMemb_1" style="width:90%;" onBlur="return ChkminMemb(this);" maxlength="2"/><span id="MinMemb_1" style="color:red;"></span></td>
                                    <td class="t-align-center"><input name="maxMemb1" type="text" class="formTxtBox_1" id="maxMemb_1" style="width:90%;" onBlur="return ChkmaxMemb(this);" maxlength="2"/><span id="MaxMemb_1" style="color:red;"></span></td>
                                    <td class="t-align-center" style="display: none;"><input name="minMemOutsidePe1" type="text" class="formTxtBox_1" id="minMemOutsidePe_1" style="width:90%;" value="0" readonly="readonly"/></td>
                                    <td class="t-align-center" style="display: none;"><input name="minMemFromPe1" type="text" class="formTxtBox_1" id="minMemFromPe_1" style="width:90%;" value="0" readonly="readonly"/></td>
                                    <td class="t-align-center" style="display: none;"><input name="minMemFromTEC1" type="text" class="formTxtBox_1" id="minMemFromTEC_1" style="width:90%;" value="0" readonly="readonly"/></td>

                                </tr>
                            </table>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" >
                                <tr>
                                    <td colspan="11" class="t-align-center"><span class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Submit" onclick="return check();"/>
                                        </span></td>
                                </tr>
                            </table>
                        </form>

                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
         <script>
                var obj = document.getElementById('lblTSCFormationRuleAdd');
                if(obj != null){
                    if(obj.innerHTML == 'Add'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
        <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
