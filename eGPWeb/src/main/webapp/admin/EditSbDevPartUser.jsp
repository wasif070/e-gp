<%-- 
    Document   : EditSbDevPartUser
    Created on : Nov 17, 2010, 7:44:51 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="scBankPartnerAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ScBankPartnerAdminSrBean"/>
<jsp:useBean id="partnerAdminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.PartnerAdminMasterDtBean"/>
<jsp:useBean id="scBankCreationSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean"/>
<jsp:setProperty name="partnerAdminMasterDtBean" property="*"/>

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <%
                    String logUserId = "0";
                    if(session.getAttribute("userId")!=null){
                        logUserId = session.getAttribute("userId").toString();
                    }
                    scBankCreationSrBean.setLogUserId(logUserId);
                    scBankPartnerAdminSrBean.setLogUserId(logUserId);
                    String partnerType = request.getParameter("partnerType");
                    String type = "";
                    String title = "";
                    String title1 = "";
                    String title2 = "";
                    String userType = "";
                    if (partnerType != null && partnerType.equals("Development")) {
                        type = "Development";
                        title = "Development Partner";
                        title1 = "Development Partner";
                        title2 = "Development Partner";
                        userType = "dev";
                    } else {
                        type = "ScheduleBank";
                        title = "Financial Institution";
                        title1 = "Financial Institution";
                        title2 = "Financial Institution Branch";
                        userType = "sb";
                    }
                    String hasMobile = "";
                    if(request.getParameter("hasMobile") != null){
                        hasMobile = request.getParameter("hasMobile");
                    }
        %>
        <title>Edit <%=title1%> User Details</title>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script type="text/javascript">
function setBranch(){
    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbOffice').val(),branchId:$('#branch').val(),funName:'branchComboEdit'}, function(j){
        $("select#cmbDescmbBankignation").html(j);
    });
}

</script>
    </head>
    <body onload="hideTr();">
        <%
        String strUserTypeId = request.getParameter("userTypeId");
        String usrId = request.getParameter("userId");
        byte usrTypeId = 0;
        boolean bole_fromWhere=  false;
                       if(usrId.equalsIgnoreCase(session.getAttribute("userId").toString())){
                                bole_fromWhere = true;
                        }
        
                        if (strUserTypeId != null && !strUserTypeId.isEmpty()) {
                            usrTypeId = Byte.parseByte(request.getParameter("userTypeId"));
                        }
                                                    if ("Update".equals(request.getParameter("add"))) {
                                                        scBankPartnerAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                                        //scBankPartnerAdminSrBean.setBankId(Integer.parseInt(request.getParameter("bank")));
                                                        String msg = "";
                                                        int strM = -1; // Default
                                                        String oldMobile = request.getParameter("oldMob");
                                                        //String oldNtnl = "";
                                                        //String nationalId = "";
                                                        if(title.equals("Schedule Bank")){
                                                        //oldNtnl = request.getParameter("oldNtnl");
                                                        //nationalId = partnerAdminMasterDtBean.getNationalId();
                                                        }
                                                        partnerAdminMasterDtBean.setOfficeName(request.getParameter("branch"));
                                                        partnerAdminMasterDtBean.setIsMakerChecker(request.getParameter("userRole"));
                                                        String mobileNo = partnerAdminMasterDtBean.getMobileNo();
                                                        /*if (!oldMobile.equalsIgnoreCase(mobileNo)) {
                                                            if (strM == -1) {
                                                                CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                                msg = commonService.verifyMobileNo(mobileNo);
                                                                if (msg.length() > 2) {
                                                                    strM = 1;

                                                                }
                                                            }
                                                        }*/
                                                       /* if (strM == -1) {
                                                        if (!oldNtnl.equals(nationalId)) {
                                                            if (title.equals("Schedule Bank")) {
                                                                    CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                                                    msg = commonService.verifyNationalId(nationalId);
                                                                    if (msg.length() > 2) {
                                                                        strM = 3; //NationalId Already Exist
                                                                    }
                                                                }
                                                            }

                                                        }*/
                                                        String viewLink = "";
                                                        boolean updateFlag = false;
                                                        if (strM == -1) {
                                                            updateFlag = scBankPartnerAdminSrBean.updateDevelopmentScBankAdmin(partnerAdminMasterDtBean);
                                                            TransferEmployeeServiceImpl parteradmintransfer = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
                                                            String status = "yes";
                                                            boolean flag1 = parteradmintransfer.updatePartnerAdmintransfer(partnerAdminMasterDtBean.getFullName(), partnerAdminMasterDtBean.getIsMakerChecker(), partnerAdminMasterDtBean.getDesignation(), partnerAdminMasterDtBean.getNationalId(),partnerAdminMasterDtBean.getPartnerId(),status);
                                                        } else {
                                                            viewLink = viewLink = "EditSbDevPartUser.jsp?partnerType=" + type + "&strM=" + strM + "&userId=" + usrId + "&userTypeId=" + usrTypeId;
                                                        }
                                                         if (updateFlag) {
                                                                viewLink = "ViewSbDevPartUser.jsp?userId=" + usrId + "&userTypeId=" + usrTypeId + "&partnerType=" + type + "&msg=success&from=Updated";
                                                            } else {
                                                                strM = 3;
                                                                viewLink = "EditSbDevPartUser.jsp?partnerType=" + type + "&strM=" + strM + "&userId=" + usrId + "&userTypeId=" + usrTypeId;
                                                            }
                                                         response.sendRedirect(viewLink);
                                                    }else{                                                
                        if (usrId != null && !usrId.equals("")) {
                            partnerAdminMasterDtBean.setUserId(Integer.parseInt(usrId));
                            partnerAdminMasterDtBean.setUserTypeId(usrTypeId);
                            if (request.getMethod().equalsIgnoreCase("get")) {
                                scBankPartnerAdminSrBean.setAuditTrail(null);
                                partnerAdminMasterDtBean.populateInfo();
                                
                            }
                        }
                        
                                        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr valign="top">
                        <%if(!bole_fromWhere){%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <%}%>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                
                                <tr valign="top">
                                    <td class="contentArea_1">
                                        <%if(bole_fromWhere){%>
                                        <div class="pageHead_1">Edit Profile</div>
                                        <%}else{%>
                                        <div class="pageHead_1">Edit <%=title2%> User Details</div>
                                        <%}%>
                                        <form id="frmSBAdmin" name="frmSBAdmin" method="post" action="">
                                            <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <%
                                                            int strM = -1;
                                                            String msg = "";

                                                            if (request.getParameter("strM") != null) {
                                                                try {
                                                                    strM = Integer.parseInt(request.getParameter("strM"));
                                                                } catch (Exception ex) {
                                                                    strM = 6;
                                                                }
                                                                switch (strM) {
                                                                    case (1):
                                                                        msg = "Mobile No Already Exists";
                                                                        break;
                                                                    case (2):
                                                                        msg = "EmailId Already Exists";
                                                                        break;
                                                                    case (3):
                                                                        msg = title2 + "User Updation Failed";
                                                                        break;
                                                                }%>

                                                <div class="t_space">
                                                    <div class="responseMsg errorMsg" style="margin-left:7px; margin-right: 2px; margin-top: 15px;">
                                                        <%= msg%>
                                                    </div>
                                                    <%
                                                                    msg = null;
                                                                }
                                                    %></div>
                                                    <tr>
                                                        <td style="font-style: italic" colspan="2" class="t-align-left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                                    </tr>
                                                     <%if (!"Development".equalsIgnoreCase(type)) {%>
                                                    <tr>
                                                       <td class="ff" width="25%">
                                                        Select Role :
                                                        </td>
                                                        <td>
                                                            <%if(!bole_fromWhere){%>
                                                            <%/*if(partnerAdminMasterDtBean.getIsMakerChecker().equalsIgnoreCase("BankChecker")){*/%>
                                                            <%--<input type="hidden" id="hidOldValue" value="<%=partnerAdminMasterDtBean.getIsMakerChecker()%>" />
                                                            <label><input type="radio" name="userRole" id="bankCheckerRole" value="BranchMaker" onclick="return RadioClick();"/> Branch Maker&nbsp;</label>
                                                            <label><input type="radio" name="userRole" id="bankChecker" value="BankChecker" checked="checked" onclick="return RadioClick();"/> Bank Checker &nbsp;</label>
                                                            <label><input type="radio" name="userRole" id="branchChecker" value="BranchChecker" onclick="return RadioClick();"/> Branch Checker &nbsp;</label>--%>
                                                            <%/*}else*/ if(partnerAdminMasterDtBean.getIsMakerChecker().equalsIgnoreCase("BranchChecker")){%>
                                                            <input type="hidden" id="hidOldValue" value="<%=partnerAdminMasterDtBean.getIsMakerChecker()%>" />
                                                            <label><input type="radio" name="userRole" id="bankCheckerRole" value="BranchMaker" /> Branch Maker&nbsp;</label>
                                                            <%--<label><input type="radio" name="userRole" id="bankChecker" value="BankChecker" checked="checked" onclick="return RadioClick();"/> Bank Checker &nbsp;</label>--%>
                                                            <label><input type="radio" name="userRole" id="branchChecker" value="BranchChecker" checked="checked"  /> Branch Checker &nbsp;</label>
                                                            <%}else{%>
                                                            <input type="hidden" id="hidOldValue" value="<%=partnerAdminMasterDtBean.getIsMakerChecker()%>" />
                                                            <label><input type="radio" name="userRole" id="bankCheckerRole" value="BranchMaker" checked="checked" /> Branch Maker&nbsp;</label>
                                                            <%--<label><input type="radio" name="userRole" id="bankChecker" value="BankChecker" checked="checked" onclick="return RadioClick();"/> Bank Checker &nbsp;</label>--%>
                                                            <label><input type="radio" name="userRole" id="branchChecker" value="BranchChecker" /> Branch Checker &nbsp;</label>
                                                            <%}%>
                                                            &nbsp;&nbsp;&nbsp;<span id="chekerMsg" style="font-weight: bold"></span>
                                                            <%}else{%>
                                                            <%if(partnerAdminMasterDtBean.getIsMakerChecker().equalsIgnoreCase("BankChecker")){%>
<!--                                                            <input type="radio" name="userRole" id="bankCheckerRole" value="BankChecker" checked="cheked"/> Bank Checker &nbsp;-->
                                                            <label>Bank Checker</label>
                                                            <%}else if(partnerAdminMasterDtBean.getIsMakerChecker().equalsIgnoreCase("BranchChecker")){%>
<!--                                                            <input type="radio" name="userRole" id="branchCheckerRole" value="BranchChecker" checked="cheked"/> Branch Checker &nbsp;-->
                                                                <label> Branch Checker</label>
                                                            <%}else{%>
<!--                                                            <input type="radio" name="userRole" id="branchMakerRole" value="maker" checked="cheked"/> Branch Maker-->
                                                            <label> Branch Maker</label>

                                                            <%}%>
                                                            <input type="hidden" name="userRole" value="<%=partnerAdminMasterDtBean.getIsMakerChecker()%>" />
                                                            <%}%>
                                                        </td>
                                                    </tr>
                                                    <%}%>
                                                    <%if(userTypeId==1){
                                                        scBankCreationSrBean.getBankForAdmin((partnerAdminMasterDtBean.getSbankDevelopId()));
                                                        if(scBankCreationSrBean.getsBankDevelopId()!=0){
                                                    %>
                                                    <input type="hidden" id="cmbOffice" value="<%=scBankCreationSrBean.getsBankDevelopId()%>"/>
                                                    <input type="hidden" id="branch" value="<%=partnerAdminMasterDtBean.getSbankDevelopId()%>"/>
                                                    <%}else{%>
                                                    <input type="hidden" id="cmbOffice" value="<%=partnerAdminMasterDtBean.getSbankDevelopId()%>"/>
                                                    <%}%>
                                                    <script type="text/javascript">
                                                        setBranch();
                                                    </script>
                                                <tr>
                                                    <td class="ff" width="20%"><%=title1%> : </td>
                                                    <td width="80%">
<!--                                                        <label class="ff" id="bankName">${partnerAdminMasterDtBean.sbDevelopName}</label>-->
                                                        <%if(!scBankCreationSrBean.getSbDevelopName().equals("")){%>
                                                        <label class="ff" id="bankName"><%=scBankCreationSrBean.getSbDevelopName()%></label>
                                                        <%}else{%>
                                                        <label class="ff" id="bankName">${partnerAdminMasterDtBean.sbDevelopName}</label>
                                                        <%}%>
                                                        <input type="hidden" name="partnerType" value="<%=type%>"/>
<!--                                                        <input type="hidden" name="sbDevelopName" value="${partnerAdminMasterDtBean.sbDevelopName}"/>
                                                        <input type="hidden" name="partnerId" value="${partnerAdminMasterDtBean.partnerId}"/>
                                                        <input type="hidden" name="isAdmin" value="${partnerAdminMasterDtBean.isAdmin}"/>
                                                        <input type="hidden" name="sbankDevelopId" value="${partnerAdminMasterDtBean.sbankDevelopId}"/>-->
                                                    </td>
                                                </tr>
                                                <tr id="cmbBranchTr">
                                                    <td class="ff"><%=title1%> Branch :<span>*</span></td>
                                                    <td>
                                                        <select name="branch"  id="cmbDescmbBankignation" class="formTxtBox_1" style="width: 200px" onchange="return blurBranch();">

                                                        </select>
                                                        <br /><span class="reqF_1" id="cmbBranchMsg" ></span>

                                                <%}else{%>
                                                <%
                                                    scBankCreationSrBean.setsBankDevelHeadId(partnerAdminMasterDtBean.getSbankDevelopId());
                                                    scBankCreationSrBean.getBankForAdmin(partnerAdminMasterDtBean.getSbankDevelopId());
                                                %>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td class="ff" width="20%"><%=title1%> :
                                                     <%if(!bole_fromWhere){%>
                                                        <span>*</span>
                                                        <%}%>
                                                    </td>
                                                    <td id="bankName" width="80%">
                                                        <%boolean bole_Hide = false;
                                                        if(!scBankCreationSrBean.getSbDevelopName().equalsIgnoreCase("")){%>
                                                        <label><%=scBankCreationSrBean.getSbDevelopName()%></label>
                                                        <input type="hidden" name="sbankDevelopId" id="cmbOffice" value="<%=scBankCreationSrBean.getsBankDevelopId()%>"/>
                                                        <%}else{
                                                                bole_Hide = true;
                                                         %>
                                                        <label>${partnerAdminMasterDtBean.sbDevelopName}</label>
                                                        <input type="hidden" name="branch"  id="cmbDescmbBankignation" value="${partnerAdminMasterDtBean.sbankDevelopId}" />
                                                        <%}%>

                                                    </td>
                                                </tr>
                                                <tr id="cmbBranchTr">
                                                    <%if(bole_fromWhere){%>
                                                    <%if(!bole_Hide){%>
                                                    <td class="ff"><%=title1%> Branch : 
                                                    </td>
                                                    <td>
                                                        <label>${partnerAdminMasterDtBean.sbDevelopName}</label>
                                                        <input type="hidden" name="branch"  id="cmbDescmbBankignation" value="${partnerAdminMasterDtBean.sbankDevelopId}" />
                                                    </td>
                                                    <%}}else{%>
                                                    <td class="ff"><%=title1%> Branch : 
                                                    <span>*</span></td>
                                                    <td>
                                                        <select name="branch"  id="cmbDescmbBankignation" class="formTxtBox_1" style="width: 200px" >
                                                            <option  value="">-- Select Bank Branch --</option>
                                                            <%
                                                                            scBankCreationSrBean.setsBankDevelHeadId(scBankCreationSrBean.getsBankDevelopId());
                                                                         for (SelectItem branchItem : scBankCreationSrBean.getBranchList()){
                                                                             
                                                                             if(partnerAdminMasterDtBean.getSbankDevelopId()==Integer.parseInt(branchItem.getObjectId().toString())){
                                                            %>
                                                            <option selected="selected" value="<%=branchItem.getObjectId()%>"><%=branchItem.getObjectValue()%></option>
                                                            <%}else{%>
                                                            <option  value="<%=branchItem.getObjectId()%>"><%=branchItem.getObjectValue()%></option>
                                                            <%}}%>
                                                        </select>
                                                    </td>
                                                    <%}%>
                                                </tr>
                                                <%}%>
                                                 
                                                <tr>
                                                    <td class="ff">Email ID : </td>
                                                    <td>
                                                        <input type="hidden" name="emailId" value="${partnerAdminMasterDtBean.emailId}"/>
                                                        <label class="ff" id="txtMail">${partnerAdminMasterDtBean.emailId}</label>
                                                        <input type="hidden" id="partnerType" name="partnerType" value="<%=type%>"/>
                                                        <input type="hidden" name="sbDevelopName" value="${partnerAdminMasterDtBean.sbDevelopName}"/>
                                                        <input type="hidden" name="partnerId" value="${partnerAdminMasterDtBean.partnerId}"/>
                                                        <input type="hidden" name="isAdmin" value="${partnerAdminMasterDtBean.isAdmin}"/>
                                                        <input type="hidden" name="sbankDevelopId" value="${partnerAdminMasterDtBean.sbankDevelopId}"/>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td class="ff">Full Name : <span>*</span></td>
                                                    <td><input class="formTxtBox_1" type="text" id="txtFullName" name="fullName" value="${partnerAdminMasterDtBean.fullName}" maxlength="101" onchange="return blurFullName();"/>
                                                    </td>
                                                </tr>
                                               
                                                <%if (!"Development".equalsIgnoreCase(type)) {%>
                                                <tr>
                                                    <td class="ff">CID No. : <span>*</span></td>
                                                    <td>
                                                        <input type="text" id="txtNtnlId" class="formTxtBox_1" name="nationalId" value="${partnerAdminMasterDtBean.nationalId}" maxlength="26" onchange="return blurNationalId();"/>
                                                        <input type="hidden" name="oldNtnl" id="oldNtnl" value="${partnerAdminMasterDtBean.nationalId}"/>
                                                        <span id="NtnlIdMsg"></span>
                                                        <%--    <label class="ff" id="txtNationalId" >${partnerAdminMasterDtBean.nationalId}</label>--%>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <tr>
                                                    <td class="ff">Designation : <span>*</span></td>
                                                    <td>
                                                        <input type="text" id="txtDesignation" class="formTxtBox_1" name="designation" value="${partnerAdminMasterDtBean.designation}" maxlength="51" onchange="return blurDesignation();"/>

                                                        <%--    <label class="ff" id="txtNationalId" >${partnerAdminMasterDtBean.nationalId}</label>--%>
                                                    </td>
                                                </tr>
                                                <%--<tr>
                                                    <td class="ff">Designataion : <span>*</span></td>
                                                    <td><input class="formTxtBox_1" id="txtMob" type="text" name="designataion" id="txtDesignataion" value="${partnerAdminMasterDtBean.designation}" maxlength="20"/>


                                                    </td>
                                                </tr>--%>
                                                <tr>
                                                    <td class="ff">Mobile No : <span>*</span></td>
                                                    <td><input class="formTxtBox_1" id="txtMob" type="text" name="mobileNo" id="txtMobileNo" value="${partnerAdminMasterDtBean.mobileNo}" maxlength="16" onchange="return blurMobileNo();"/><span id="mNo" style="color: grey;"> (Mobile No. format should be e.g 12345678)</span>
                                                        <input type="hidden" name="oldMob" id="oldMob" value="${partnerAdminMasterDtBean.mobileNo}"/>
                                                        <span id="MobileNoMsg"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%">&nbsp;</td>
                                                    <td colspan="2"  width="80">
                                                        <label class="formBtn_1">
<!--                                                            <input type="submit" name="add" id="btnAdd" value="Update" onclick="return validate();" />-->
                                                                <input type="submit" name="add" id="btnAdd" value="Update" onclick="return validate();"/>
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <%}%>
            
            <%if(bole_fromWhere){%>
            <script>
                var headSel_Obj = document.getElementById("headTabMyAcc");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            <%}else{%>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            <%}%>
            <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

<!--        <script type="text/javascript">
            function validate(){
                //if($('NtnlIdMsg').innerhtml("Ok")){
                if(document.getElementById("NtnlIdMsg").innerHTML=="National Id Already Exists") {
                    return false;
                }else{
                    return true;
                }
            }

        </script>-->
<%--<script type="text/javascript">
            $(function() {
                $('#txtNtnlId').change(function() {
                    if($('#txtNtnlId').val()!=$('#oldNtnl').val()) {
                        var spaceTest=/^([a-zA-Z0-9]+)$/;
                        if(spaceTest.test($('#txtNtnlId').val())) {
                            $.post("<%=request.getContextPath()%>/ScBankServlet", {ntnlId:$('#txtNtnlId').val(),funName:'verifyNtnlId'},  function(j){
                                if(j.toString().indexOf("Ok", 0)!=-1){
                                    $('span.#NtnlIdMsg').css("color","white");
                                }
                                else {
                                    $('span.#NtnlIdMsg').css("color","red");
                                    $('#frmUserReg').submit(function(){
                                        return false;
                                    });
                                }
                                $('span.#NtnlIdMsg').html(j);
                            });
                        }
                        else
                        {
                            $('span.#NtnlIdMsg').css("color","red");
                            $('span.#NtnlIdMsg').html("Please Enter Valid National Id");
                        }
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#txtMob').change(function() {
                    if($('#txtMob').val()!= $('#oldMob')) {
                        var digitTest=/^([0-9]+)$/;
                        if(digitTest.test($('#txtMob').val())) {
                            $('span.#MobileNoMsg').css("color","red");
                            $('span.#MobileNoMsg').html("Checking Valid Mobile No...");
                            $.post("<%=request.getContextPath()%>/CommonServlet", {mobileNo:$('#txtMob').val().length>10,mobileNo:$('#txtMob').val().length<15,funName:'verifyMobileNo'},  function(j){
                                if(j.toString().indexOf("OK", 0)!=-1){
                                    $('span.#MobileNoMsg').css("color","green");
                                }
                                else{
                                    $('span.#MobileNoMsg').css("color","red");
                                    $('#frmUserReg').submit(function(){
                                        return false;
                                    });
                                }
                                $('span.#MobileNoMsg').html(j);
                            });
                        }
                        else
                        {
                            $('span.#MobileNoMsg').css("color","red");
                            $('span.#MobileNoMsg').html("Please Enter Valid Mobile Number");
                        }
                    }
                });
            });
        </script>
<script type="text/javascript">

            function RadioClick(){
                 $('span.#chekerMsg').html('');
                  $('span.#cmbBranchMsg').html('');
                  $('span.#cmbBranchMsg').hide();
                var data = '';
                var action ='';
                var oldData = $('#hidOldValue').val();
                if($('#bankChecker').attr("checked")==true){
                    data = $('#cmbOffice').val();
                    action = $('#bankChecker').val();
                     $('#cmbBranchTr').hide();
                }else{

                    if($('#bankCheckerRole').attr("checked")==true){
                        $('#cmbBranchTr').show();
                        $('span.#chekerMsg').html('');
                    }
                    $('#cmbBranchTr').show();
                    if($('#cmbDescmbBankignation').val()!=""&&$('#branchChecker').attr("checked")==true){
                        data = $('#cmbDescmbBankignation').val();
                        action = $('#branchChecker').val();

                    }
                }
                 if(oldData=='BranchMaker'){
                    return false;
                }
                if(action!=oldData && data!='' ){
                    if(action=='BankChecker'){
                        $('#cmbBranchTr').hide();
                    }
                    $('span.#chekerMsg').html("Checking for Existing Role...");
                    $.post("<%=request.getContextPath()%>/CommonServlet", {officeId:data,role:action,funName:'bankChecker'},  function(j){
                        if(j.toString().length>2){
                            $('span.#chekerMsg').css("color","red");
                            $('span.#chekerMsg').html(j);
                            $('#frmUserReg').submit(function(){
                                return false;
                            });
                        }else{
                            $('span.#chekerMsg').html('');
                        }
                    });//Here ends $.post condition
                    }
            }



</script>--%>
<script type="text/javascript">
            $(function() {
                var hasMobileNo = '<%=hasMobile%>';
                if(hasMobileNo == 'No')
                    alert("Please insert your mobile no");
                
                $('#frmSBAdmin').submit(function() {
                    if(($('#partnerType').val()=="ScheduleBank")&&($('span.#chekerMsg').html().length > 0)){
                        return false;
                    }
                    if(($('#partnerType').val()=="ScheduleBank")){
                            if($('#bankChecker').attr("checked")==false){
                                    if($('#cmbDescmbBankignation').val()==""){
                                        $('span.#cmbBranchMsg').show();
                                        $('span.#cmbBranchMsg').html("Please Select Branch");
                                        return false;
                                    }else{
                                        $('span.#cmbBranchMsg').html("");
                                    }
                             }
                    }
<%--                    if($('#MobileNoMsg').html().length> 0)
                    {
                        if($('span.#MobileNoMsg').html()!="OK")
                            return false;
                    }
                    if(($('#partnerType').val()=="ScheduleBank")&&($('span.#NtnlIdMsg').html().length > 0)){
                        if($('span.#NtnlIdMsg').html()!="Ok")
                            alert($('span.#chekerMsg').html().length+"2");
                            return false;
                    }--%>
                    return true;
                });
            });
</script>

<script type="text/javascript">
            /*$(document).ready(function() {
                $("#frmSBAdmin").validate({
                    rules: {
                        fullName: { spacevalidate: true, requiredWithoutSpace: true , alphaName:true, maxlength:100 },
                        //mobileNo: {maxlength:15,minlength:10, number: true },
                        mobileNo: {number: true, minlength: 10, maxlength:15 },
                        //nationalId:{required: true,maxlength:13,number: true},
                        nationalId:{number: true,minlength: 13,maxlength: 25},
                        designation: {spacevalidate: true, requiredWithoutSpace: true,maxlength:50,alphaName:true}
                    },
                    messages: {
                        fullName: { spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace: "<div class='reqF_1'>Please enter Full Name</div>",
                            alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , ' \" } { - . _) Only </div>",
                            maxlength: "<div class='reqF_1'></div>"
                        },
                        nationalId:{
                            number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                            minlength: "<div class='reqF_1'>National ID should comprise of minimum 13 digits</div>",
                            maxlength: "<div class='reqF_1'>National ID should comprise of maximum 25 digits</div>"
                        },

                        mobileNo: {number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" ,
                            minlength:"<div class='reqF_1'>Minimum 10 digits are required</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 15 digits only</div>"
                        },
                        designation:{requiredWithoutSpace:"<div class='reqF_1' id='desigdiv'>Please enter Designation.</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 50 characters only</div>",
                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>",
                            alphaName:"<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' \" } { - . _) Only </div>"
                        }
                    },
                    errorPlacement:function(error ,element)
                    {
                        if(element.attr("name")=="mobileNo")
                        {
                            error.insertAfter("#MobileNoMsg");
                        }
                        else
                        {
                            error.insertAfter(element);

                        }
                    }
                }
            );
            });*/

function validate(){
             var valid = true;
             $(".err").remove();
              $(".fullNameerr").remove();
             $(".designationerr").remove();
             $(".mobileNoerr").remove();
            $(".nationIderr").remove();
            $(".cmbDescmbBankignationerr").remove();
            var spaceTest;
            if($("#cmbDescmbBankignation").val() == "") {
                    $("#cmbDescmbBankignation").parent().append("<div class='err' style='color:red;'>Please Selet <%=title1%> Branch</div>");
                    valid =  false;
            }
            if(($('#partnerType').val()=="ScheduleBank")){
                spaceTest =  /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
                    if($("#txtNtnlId").val() != "") {
                        if(!spaceTest.test($("#txtNtnlId").val())){
                            $("#txtNtnlId").parent().append("<div class='err' style='color:red;'>Please enter digits (0-9) only</div>");
                            valid =  false;
                        }else{
                            if($("#txtNtnlId").val().length<11){
                                $("#txtNtnlId").parent().append("<div class='err' style='color:red;'>CID No should comprise of minimum 11 digits</div>");
                                valid = false;
                            }
                            if($("#txtNtnlId").val().length>11){
                                $("#txtNtnlId").parent().append("<div class='err' style='color:red;'>CID No should comprise of maximum 11 digits</div>");
                                valid = false;
                            }
                        }
                    }
                    else{
                        if($("#txtNtnlId").val() == "") {
                            $("#txtNtnlId").parent().append("<div class='err' style='color:red;'>Please enter CID No.</div>");
                            valid = false;
                        }
                    }
            }
            if($("#txtFullName").val() != "") {
                spaceTest = /^\s+|\s+$/g;
                if(spaceTest.test($('#txtFullName').val())){
                    $("#txtFullName").parent().append("<div class='err' style='color:red;'>Only Space is not allowed</div>");
                    valid =  false;
                }else{
                    spaceTest =  /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/
                    if(!spaceTest.test($("#txtFullName").val())){
                        $("#txtFullName").parent().append("<div class='err' style='color:red;'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only</div>");
                        valid =  false;
                    }else{
                        if($("#txtFullName").val().length>100){
                            $("#txtFullName").parent().append("<div class='err' style='color:red;'>Allows maximum 100 characters only</div>");
                            valid = false;
                        }
                    }
                }
            }else{
                if($("#txtFullName").val() == "") {
                    $("#txtFullName").parent().append("<div class='err' style='color:red;'>Please enter Full Name</div>");
                    valid = false;
                }
            }

             if($("#txtDesignation").val() != "") {
                spaceTest = /^\s+|\s+$/g;
                if(spaceTest.test($("#txtDesignation").val())){
                    $("#txtDesignation").parent().append("<div class='err' style='color:red;'>Only Space is not allowed</div>");
                    valid = false;
                }else{
                    spaceTest =  /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/
                    if(!spaceTest.test($("#txtDesignation").val())){
                        $("#txtDesignation").parent().append("<div class='err' style='color:red;'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only</div>");
                        valid =  false;
                    }else{
                        if($("#txtDesignation").val().length>50){
                            $("#txtDesignation").parent().append("<div class='err' style='color:red;'>Allows maximum 50 characters only</div>");
                            valid = false;
                        }
                    }

                }
            }else{
                if($("#txtDesignation").val() == "") {
                    $("#txtDesignation").parent().append("<div class='err' style='color:red;'>Please enter Designation</div>");
                    valid = false;
                }
            }

            spaceTest =  /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
            if($("#txtMob").val() != "") {
                if(!spaceTest.test($("#txtMob").val())){
                    $("#txtMob").parent().append("<div class='err' style='color:red;'>Please enter digits (0-9) only</div>");
                    valid =  false;
                }else{
                    if($("#txtMob").val().length<8){
                        $("#txtMob").parent().append("<div class='err' style='color:red;'>Minimum 8 digits are required</div>");
                        valid = false;
                    }else{
                         if($("#txtMob").val().length>8){
                            $("#txtMob").parent().append("<div class='err' style='color:red;'>Allows maximum 8 digits only</div>");
                            valid = false;
                        }
                    }
                }
            }else{
                if($("#txtMob").val() == "") {
                    $("#txtMob").parent().append("<div class='err' style='color:red;'>Please enter Mobile No.</div>");
                    valid = false;
                }
            }
            if(!valid){
                return false;
            }
    }
</script>
<script type="text/javascript">

    function blurFullName(){
        $(".err").remove();
         $(".fullNameerr").remove();

         if($("#txtFullName").val() != "") {
                spaceTest = /^\s+|\s+$/g;
                if(spaceTest.test($('#txtFullName').val())){
                    $("#txtFullName").parent().append("<div class='fullNameerr' style='color:red;'>Only Space is not allowed</div>");
                    return false;
                }
                spaceTest =  /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/
                if(!spaceTest.test($("#txtFullName").val())){
                    $("#txtFullName").parent().append("<div class='fullNameerr' style='color:red;'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only</div>");
                    return false;
                }
                if($("#txtFullName").val().length>100){
                    $("#txtFullName").parent().append("<div class='fullNameerr' style='color:red;'>Allows maximum 100 characters only</div>");
                }
            }else{
                if($("#txtFullName").val() == "") {
                    $("#txtFullName").parent().append("<div class='fullNameerr' style='color:red;'>Please enter Full Name</div>");
                }
            }
    }
    function blurDesignation(){
        $(".err").remove();
         $(".designationerr").remove();

         if($("#txtDesignation").val() != "") {
                spaceTest = /^\s+|\s+$/g;
                if(spaceTest.test($("#txtDesignation").val())){
                    $("#txtDesignation").parent().append("<div class='designationerr' style='color:red;'>Only Space is not allowed</div>");
                }
                spaceTest =  /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/
                if(!spaceTest.test($("#txtDesignation").val())){
                    $("#txtDesignation").parent().append("<div class='designationerr' style='color:red;'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only</div>");
                    return false;
                }
                if($("#txtDesignation").val().length>50){
                    $("#txtDesignation").parent().append("<div class='designationerr' style='color:red;'>Allows maximum 50 characters only</div>");
                }

            }else{
                if($("#txtDesignation").val() == "") {
                    $("#txtDesignation").parent().append("<div class='designationerr' style='color:red;'>Please enter Designation</div>");
                }
            }
    }
    function blurMobileNo(){
        $(".err").remove();
         $(".mobileNoerr").remove();

         if($("#txtMob").val() != "") {
             spaceTest =  /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
                if(!spaceTest.test($("#txtMob").val())){
                    $("#txtMob").parent().append("<div class='mobileNoerr' style='color:red;'>Please enter digits (0-9) only</div>");
                    return false;
                }
                if($("#txtMob").val().length<8){
                    $("#txtMob").parent().append("<div class='mobileNoerr' style='color:red;'>Minimum 8 digits are required</div>");
                }
                if($("#txtMob").val().length>8){
                    $("#txtMob").parent().append("<div class='mobileNoerr' style='color:red;'>Allows maximum 8 digits only</div>");
                }
            }
    }
    function blurNationalId(){
        $(".err").remove();
         $(".nationIderr").remove();
         spaceTest =  /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
                    if($("#txtNtnlId").val() != "") {
                        if(!spaceTest.test($("#txtNtnlId").val())){
                            $("#txtNtnlId").parent().append("<div class='nationIderr' style='color:red;'>Please enter digits (0-9) only</div>");
                            return false;
                        }
                        if($("#txtNtnlId").val().length<11){
                            $("#txtNtnlId").parent().append("<div class='nationIderr' style='color:red;'>National ID should comprise of minimum 11 digits</div>");
                        }
                        if($("#txtNtnlId").val().length>11){
                            $("#txtNtnlId").parent().append("<div class='nationIderr' style='color:red;'>National ID should comprise of maximum 11 digits</div>");
                        }
                    }
    }
function blurBranch(){
$(".err").remove();
$(".cmbDescmbBankignationerr").remove();
     if($("#cmbDescmbBankignation").val() == "") {
          $("#cmbDescmbBankignation").parent().append("<div class='cmbDescmbBankignationerr' style='color:red;'>Please Selet <%=title1%> Branch</div>");
        }
}
</script>
<script type="text/javascript">
    function hideTr(){
        if($('#bankChecker').attr("checked")==true){
            $('#cmbBranchTr').hide();
        }
    }
</script>

    </body>
</html>
