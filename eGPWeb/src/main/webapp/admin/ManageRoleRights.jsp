<%-- 
    Document   : ManageRoleRights
    Created on : Mar 9, 2012, 2:53:17 PM
    Author     : dipal.shah
--%>


<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.ManageUserRights"%>
<%@page import="com.cptu.egp.eps.model.table.TblMenuMaster"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ManageUserRightsImpl"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage Role Wise Rights</title>
        <%String contextPath = request.getContextPath();%>
        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/deployJava.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/CommonValidation.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <body>
          <script type="text/javascript">
            function chkValidate()
            {
                var obj=document.frmSubmit.chk;
                var flg=false;
                for (i = 0; i < obj.length; i++)
                    {
                        if(obj[i].checked == true)
                            {
                                flg=true;
                                return;
                            }
                    }

                if(!flg)
                {
                    alert("Select at least one checkbox to assign rights.");
                    return false;
                }
                else
                    return true;
            }

            function checkParent(lstControl,varMainMenuId)
            {
               // alert(lstControl.length);
                var flg=false;
                for (i = 0; i < lstControl.length; i++)
                {
                    if(lstControl[i].checked == true)
                        {
                            flg=true;
                        }
                }
                document.getElementById("chk_"+varMainMenuId).checked=flg;
            }


          </script>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="fixDiv">

                 <!--Middle Content Table Start-->
<%
                ManageUserRightsImpl objManageUserRights1 = (ManageUserRightsImpl)AppContext.getSpringBean("ManageUserRigths");

                int userId = 0;
                if(session.getAttribute("userId") != null) {
                    if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                    }
                }

                int roleId = 0;
                if (request.getParameter("roleId") != null) {
                    roleId = Integer.parseInt(request.getParameter("roleId"));
                }

                int logUserTypeid = 0;
                if (session.getAttribute("userTypeId") != null) {
                    logUserTypeid = Integer.parseInt(session.getAttribute("userTypeId").toString());
                }

                String action = "";
                if(request.getParameter("action") != null && !"".equalsIgnoreCase(request.getParameter("action")))
                {
                     action = request.getParameter("action");
                }

                List<TblMenuMaster> lstMenuMaster=objManageUserRights1.getMenuList(0);

                List<Object[]> objUserDetails=objManageUserRights1.getRoleDetails(roleId);
                String roleName="";
                if(objUserDetails != null)
                {
                    for(Object[] objArray : objUserDetails)
                    {
                        if(objArray!=null && objArray[0]!=null)
                        {
                            roleName=objArray[0].toString();
                        }
                    }
                }

                ArrayList objRoleRights = objManageUserRights1.getRoleMenuRightsList(roleId);
                Map<Integer,String> objUserRightsMap1= null;
                objUserRightsMap1=objManageUserRights1.getUserMenuRightsMap(loguserId);
                //out.println(objUserRights.size());

                %>

                    <div class="contentArea_1">
                        <div class="t_space">
                                <div class="pageHead_1">
                                    Manage Role Rights
                                    <span style="float: right; text-align: right;">
                                            <a href="RoleMasterGrid.jsp" class="action-button-goback">Go back</a>
                                    </span>
                                </div>
                        </div>

                  <form id="frmSubmit" name="frmSubmit" method="post" action="<%=request.getContextPath()%>/ManageUserRights">
                    <input type="hidden" name="roleId" id="roleId" value="<%=roleId%>">
                    <input type="hidden" name="action" id="action" value="assignRoleRights">
                    
                        <div class="tableHead_1 t_space">Role Details</div>
                        <table width="100%"  cellspacing="10" class="tableView_1 infoBarBorder">
                        <tr>
                          <td width="10%" class="ff">Role Name :</td>
                          <td width="25%" align="left"><%= roleName%></td>
                        </tr>
                       </table>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" align="center">
                        <th>
                            Module / Menu
                        </th>
                        <th>
                            Functionality / Sub Menu
                        </th>
                        <%

                        for(TblMenuMaster objMenu : lstMenuMaster)
                        {
                            if(logUserTypeid == 19 && objUserRightsMap1!=null && !objUserRightsMap1.containsKey(objMenu.getMenuId()))
                            {
                                %>
                                <tr valign="top" style="display: none;">
                                <%
                            }
                            else
                            {
                                %>
                                <tr valign="top">
                                <%
                            }
                               %>
                                    <td>
                                        <%= objMenu.getMenuName()%>
                                        <input style="display: none;" type="checkbox" id="chk_<%= objMenu.getMenuId()%>" name="chk" value="<%= objMenu.getMenuId() %>"
                                               <%
                                                if(objRoleRights!=null && objRoleRights.contains(objMenu.getMenuId()))
                                                { out.println("checked");}
                                               %>
                                        >
                                    </td>
                                    <td>
                                        <%
                                        for(TblMenuMaster objSubMenu : objManageUserRights1.getMenuList(objMenu.getMenuId()))
                                        {

                                                if(logUserTypeid == 19 && objUserRightsMap1!=null && !objUserRightsMap1.containsKey(objSubMenu.getMenuId()))
                                                {
                                                   %>
                                                   <input  style="display: none;"  type="checkbox" id="chk_sub_<%= objMenu.getMenuId() %>" name="chk" onclick="checkParent(document.frmSubmit.chk_sub_<%= objMenu.getMenuId() %>,'<%=objMenu.getMenuId()%>');" value="<%= objSubMenu.getMenuId() %>"
                                                   <%
                                                   if(objRoleRights!=null && objRoleRights.contains(objSubMenu.getMenuId()))
                                                      { out.println("checked");}
                                                    %>
                                                   >
                                                   <%
                                                }
                                                else
                                                {
                                                   %>
                                                     <input type="checkbox" id="chk_sub_<%= objMenu.getMenuId() %>" name="chk" onclick="checkParent(document.frmSubmit.chk_sub_<%= objMenu.getMenuId() %>,'<%=objMenu.getMenuId()%>');" value="<%= objSubMenu.getMenuId() %>"
                                                   <%
                                                   if(objRoleRights!=null && objRoleRights.contains(objSubMenu.getMenuId()))
                                                      { out.println("checked");}
                                                    %>
                                                   >
                                                    <%=objSubMenu.getMenuName()%><BR>
                                                    <%
                                                }
                                        }
                                        %>

                                    </td>
                                </tr>
                               <%

                        }
                        %>
                        <tr>
                            <td colspan="2">
                                <center>
                                    <label class="formBtn_1" id="lblSave">
                                        <input type="submit" name="save" id="save" value="Save" onclick="return chkValidate(this);">
                                    </label>
                                </center>
                            </td>
                        </tr>
                    </table>


                </form>
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>

