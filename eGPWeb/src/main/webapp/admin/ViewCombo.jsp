<%-- 
    Document   : ViewCombo
    Created on : Feb 5, 2011, 6:32:04 PM
    Author     : dixit
--%>
 <%
    StringBuffer colHeader = new StringBuffer();
    colHeader.append("'Sl. <br/> No.','Combo box Name','View','Action'");
 %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
         <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <%
                
                String templetid="";
                if(!"".equalsIgnoreCase(request.getParameter("templateId"))){
                templetid = request.getParameter("templateId");
                }
                String templetformid = "";
                if(!"".equalsIgnoreCase(request.getParameter("templetformid"))){
                templetformid = request.getParameter("templetformid");}

                String responsemsg = "";
                if(!"".equalsIgnoreCase(request.getParameter("msg"))){
                responsemsg = request.getParameter("msg");}
                
        %>
        
       
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Combo</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript"  language="javascript">
            /* jquery function for displaying jqgrid  */
            function loadGrid(){
                $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                jQuery("#list").jqGrid({
                    datatype: 'xml',
                    url : '<%=request.getContextPath()%>/CreateComboSrBean?templetformID=<%=templetformid %>&templetID=<%=templetid %>&action=fetchData&viewmessage=std',
                    height: 300,
                    colNames:[<%=colHeader %>],
                    colModel:[
                        {name:'Sr. No',index:'Sr. No', width:10,sortable: false, search: false, align:'center'},
                        {name:'listBoxName',index:'listBoxName', width:100, search: true,sortable: true,searchoptions: { sopt: ['eq', 'cn'] }},
                        {name:'process',index:'process', width:30,sortable: false, search: false},
                        {name:'action',index:'action', width:30,sortable: false, search: false,align:'center'}                        
                    ],
                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    caption: " ",
                    searchGrid: {sopt:['cn','bw','eq','ne','lt','gt','ew']},
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{edit:false,add:false,del:false});
            }
            $(document).ready(function(){
                loadGrid();
            });

            function goback()
            {
                window.close();
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                <tr valign="top">
                        <%--<jsp:param name="userType" value="<%=userType.toString()%>"/>
                    </jsp:include>--%>
                        <%if("succ".equalsIgnoreCase(responsemsg)){%>
                        <div class="responseMsg successMsg t_space"><span>Combo box created successfully</span></div>
                        <%}%>
                        <td class="contentArea" style="vertical-align: top;" align="center" valign="middle">
                        <div class="pageHead_1 t-align-left">View Combo box

                        <div class="t-align-right b_space t_space">
<!--                            <a href="CreateCombo.jsp?formId=<%=templetformid%>&templateId=<%=templetid%>" class="action-button-add"onclick="">Add Combo box</a>-->
                        <!-- for solution of #4884  and #5127-->
                        
                        <%if(true || "true".equalsIgnoreCase(request.getParameter("nw"))){
                            
                                if("succ".equalsIgnoreCase(responsemsg)){%>
                                    <a class="action-button-goback" href="DefineSTDInDtl.jsp?templateId=<%=templetid%>" title="STD Dashboard">SBD Dashboard</a>
                            <%}else{%>
                                     <a class="action-button-goback" href="javascript:goback()" title="STD Dashboard">SBD Dashboard</a>
                            <%}}%>
                        </div>
<!--                        <span style="float: right; text-align: right;">
                        <a class="action-button-goback" href="DefineSTDInDtl.jsp?templateId=<a%=templetid%>" title="STD Dashboard">SBD Dashboard</a>
                        </span>-->
                        
                        </div>
                        <div class="t-align-right t_space">
                             
                        </div>
                        <table width="100%" cellspacing="0" class="t_space">
                            <tr valign="top">
                                <td>
                                    <div class="tabPanelArea_1">
                                        <div id="jQGrid" align="center" style="width: 98.3%;">
                                            <div><table id="list"></table></div>
                                            <div id="page"></div>
                                        </div>
                                    </div>
                                    <!--Bottom controls-->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pagging_1">

                                    </table>
                                    <div>&nbsp;</div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
    </body>
    <script type="text/javascript" language="Javascript">
            var headSel_Obj = document.getElementById("headTabSTD");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }

            function deleteCombo(listboxid)
            {
                $.alerts._show("Delete Combo", 'Do you really want to delete this combo?', null, 'confirm', function(r) {
                if (r == true){
                $.ajax({
                    url: "<%=request.getContextPath()%>/CreateComboSrBean?listBoxId="+listboxid+"&action=delete&templateid=<%=templetid%>",
                    method: 'POST',
                    async: false,
                    success: function(j) {

                    }
                });
                jAlert("combo deleted successfully.","Combo Deleted", function(RetVal) {
                        });
                loadGrid();
                  }
        });

    }

            function deleteUsedCombo()
            {
                jAlert("You can not delete this combo.","Combo Deleted", function(RetVal) {
                        });
            }

    </script>
</html>
