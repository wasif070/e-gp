<%-- 
    Document   : AITViewList
    Created on : Jul 12, 2011, 3:14:47 PM
    Author     : Sreenu.Durga
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblCmsAitConfig"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.web.servicebean.CmsAitConfigBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <title>AIT View Page</title>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <%
                StringBuilder userType = new StringBuilder();
                if (request.getParameter("userType") != null) {
                    if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                        userType.append(request.getParameter("userType"));
                    } else {
                        userType.append("org");
                    }
                } else {
                    userType.append("org");
                }
                String message = "";
                String successMessage = "";
                String errorMessage = "";
                if (request.getParameter("message") != null) {
                    message = request.getParameter("message");
                    if (message.equalsIgnoreCase("delete")) {
                        successMessage = "AIT Configuration deleted Successfully";
                    }
                    if (message.equalsIgnoreCase("error")) {
                        errorMessage = "Please select proper value to delete. Because deletion is possible only for Maximum Value";
                    }
                }//request checking null

                CmsAitConfigBean cmsAitConfigBean = new CmsAitConfigBean();
                if (session.getAttribute("userId") != null) {
                    cmsAitConfigBean.setLogUserId(session.getAttribute("userId").toString());
                }
                List<CommonAppData> fianancialYearsList = cmsAitConfigBean.getAllFinancialYears();
                String financialyearId = "";
                int currentYearId = 0;
                // if request came from AITConfiguration page
                if (request.getParameter("financialyearId") != null) {
                    financialyearId = request.getParameter("financialyearId");
                    currentYearId = Integer.parseInt(request.getParameter("financialyearId"));
                } else if (request.getParameter("selectedYearId") != null) {
                    // if request came from current page i.e change the finacial year
                    currentYearId = Integer.parseInt(request.getParameter("selectedYearId"));
                    financialyearId = currentYearId + "";
                } else {
                    // if request came from LeftMenu
                    if (fianancialYearsList != null) {
                        for (CommonAppData commonAppData : fianancialYearsList) {
                            if ("Yes".equalsIgnoreCase(commonAppData.getFieldName3())) {
                                currentYearId = Integer.parseInt(commonAppData.getFieldName1());
                                financialyearId = currentYearId + "";
                                break;
                            }
                        }// end for loop
                    }// if null checking
                }
                final BigDecimal INFINITE_VALUE = new BigDecimal(-1.000);
                final int ZERO = 0;
                final String SPACE = "&nbsp;";
                double flatPercentage = cmsAitConfigBean.getFlatPercentage(currentYearId);
                List<TblCmsAitConfig> tblCmsAitConfigList = cmsAitConfigBean.getCmsAitConfigForFianancialYear(currentYearId);

    %>
    <script Language="JavaScript" >
        function setTableRowsWithFinancialYear(control) {
            var selectedValue=control.value;
            document.getElementById("hiddenselectedlYearId").value = selectedValue;
            document.getElementById("formViewAitConfiguration").action="/admin/AITViewList.jsp?selectedYearId="+selectedValue;
            document.getElementById("formViewAitConfiguration").submit();
            return (true);
        }
    </script>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%--Dashboard Header Start--%>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%--Dashboard Header End--%>
                <%--Dashboard Body Start--%>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType.toString()%>" ></jsp:include>
                        <td class="contentArea">
                            <div>
                                <div class="pageHead_1">AIT Configuration View</div>
                            </div>
                            <%
                                        if (successMessage.length() > 0) {
                                            out.print("<div id='successMsg' class='responseMsg successMsg' style='display:block; margin-top: 10px;'>");
                                            out.print(successMessage);
                                            out.print("</div>");
                                        } else if (errorMessage.length() > 0) {
                                            out.print("<div id='errorMsg' class='responseMsg errorMsg' style='display:block; margin-top: 10px;'>");
                                            out.print(errorMessage);
                                            out.print("</div>");
                                        }
                            %>
                            <form id="formViewAitConfiguration" name="formViewAitConfiguration" method="post" action='/admin/AITViewList.jsp?selectedYearId=<%=currentYearId%>'>
                                <div class="t-align-left t_space b_space">
                                    <b>Financial Year : </b>
                                    <select name="cmbFinancialYear" class="formTxtBox_1" id="cmbFinancialYear" style="width:200px;" onchange="return setTableRowsWithFinancialYear(this);">
                                        <%
                                                    if ((fianancialYearsList != null) && (!fianancialYearsList.isEmpty())) {
                                                        String selected = "";
                                                        for (CommonAppData commonApp : fianancialYearsList) {
                                                            if (financialyearId.equalsIgnoreCase(commonApp.getFieldName1())) {
                                                                selected = " selected";
                                                            } else {
                                                                selected = "";
                                                            }
                                                            out.println(" <option value=" + commonApp.getFieldName1() + " " + selected + ">");
                                                            out.println(commonApp.getFieldName2());
                                                            out.println("</option>");
                                                        }
                                                    }
                                        %>
                                    </select>
                                    <%
                                                if (flatPercentage != 0) {
                                                    for (int i = 0; i < 14; i++) {
                                                        out.print(SPACE);
                                                    }
                                                    out.print("<b>Flat Percentage : </b>");
                                                    out.print(flatPercentage);
                                                }
                                    %>
                                </div>
                                <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="tableList_3" id="tblAitConfigSlab">
                                    <input type="hidden" name="hiddenselectedlYearId" id="hiddenselectedlYearId" />
                                    <tr>
                                        <th width="10%" valign="top" class="ff">Slab No.</th>
                                        <th width="20%">Starting Amount</th>
                                        <th width="20%">Ending Amount</th>
                                        <th width="20%">AIT Percent</th>
                                        <th width="10%">Action</th>
                                    </tr>
                                    <%
                                                if (tblCmsAitConfigList != null && tblCmsAitConfigList.isEmpty()) {
                                                    out.print("<tr>");
                                                    out.print("<td class='t-align-center' colspan='5'><b> No Data Found. If you want configure "
                                                            + "<a href ='/admin/AITConfigutation.jsp?financialyearId=" + currentYearId + "'>click here</a></b></td>");
                                                    out.print("</tr>");
                                                } else if (tblCmsAitConfigList != null) {
                                                    int slabCount = 0;
                                                    for (TblCmsAitConfig tblCmsAitConfig : tblCmsAitConfigList) {
                                                        slabCount++;
                                                        out.print("<tr>");
                                                        out.print("<td class='t-align-center'>" + slabCount + "</td>");
                                                        out.print("<td class='t-align-center'>" + tblCmsAitConfig.getSlabStartAmt() + "</td>");
                                                        // comapre the value is infinte value. The comparision of BigDecimal is different. If they are equals it returns ZERO
                                                        if (INFINITE_VALUE.compareTo(tblCmsAitConfig.getSlabEndAmt()) == ZERO) {
                                                            out.print("<td class='t-align-center'>" + "above " + "</td>");
                                                        } else {
                                                            out.print("<td class='t-align-center'>" + tblCmsAitConfig.getSlabEndAmt() + "</td>");
                                                        }
                                                        out.print("<td class='t-align-center'>" + tblCmsAitConfig.getSlabPercent() + "</td>");
                                                        out.print("<td class='t-align-center'> <a href='/AITConfigServlet?action=delete&aitConfigId=" + tblCmsAitConfig.getAitConfigId() + "'>delete" + " </a></td>");
                                                        out.print("</tr>");
                                                    }// end for loop
                                                }
                                    %>
                                </table>
                            </form>
                    </tr>
                </table>
                <%--Dashboard Body End--%>
                <%--Dashboard Footer Start--%>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <%--Dashboard Footer End--%>
            </div><%-- end dashboard_div--%>
        </div><%-- end mainDiv--%>
    </body>
</html>
