


<%-- 
    Document   : GovtUserCreateRoleOff
    Created on : Oct 23, 2010, 6:01:34 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="govSrUser" class="com.cptu.egp.eps.web.servicebean.GovtUserSrBean" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.List,com.cptu.egp.eps.model.table.TblEmployeeRoles,com.cptu.egp.eps.model.table.TblDepartmentMaster" %>
<%@page import="com.cptu.egp.eps.model.table.TblEmployeeOffices,com.cptu.egp.eps.dao.storedprocedure.SPGovtEmpRolesReturn" %>
<%
            String str = request.getContextPath();

            int empId = 0;
            int userId = 0;
            short departmentId = 0;
            int designationId = 0;
            int govUserId = 0;
            int suserTypeId = 0;
            int govDeptId = 0;
            boolean flag=false;
            String action="";
            String msg="";
            String str_edit = "";
            String logUserId = "0";
            
            if (session.getAttribute("userId") != null) {
                govUserId = Integer.parseInt(session.getAttribute("userId").toString());
                logUserId = session.getAttribute("userId").toString();
            }
            govSrUser.setLogUserId(logUserId);
            if (session.getAttribute("userTypeId") != null) {
                suserTypeId = Integer.parseInt(session.getAttribute("userTypeId").toString());
            }

            if (suserTypeId == 4 || suserTypeId == 5) {
                govDeptId = govSrUser.getUserTypeWiseDepartmentId(govUserId, suserTypeId);
            }
            

            if (request.getParameter("empId") != null) {
                empId = Integer.parseInt(request.getParameter("empId"));
                govSrUser.setEmpId(empId);
            }
            if (request.getParameter("edit") != null) {
                str_edit = request.getParameter("edit");
                }

            if (request.getParameter("userId") != null) {
                userId = Integer.parseInt(request.getParameter("userId"));
                govSrUser.setUserId(userId);
            } else {
                userId = govSrUser.takeUserIdFromEmpId(empId);
                govSrUser.setUserId(userId);
            }
            govSrUser.setEmpId(empId);
            govSrUser.setUserId(userId);
          
            List<SPGovtEmpRolesReturn> empRolesReturn = govSrUser.getEmpRoleses(empId);
            String status = govSrUser.employeeStatus(empId);            
            govSrUser.setAuditTrail(null);
            Object[] object = govSrUser.getGovtUserData();
            designationId = (Integer) object[6];
            String employeeName = (String) object[2];
            govSrUser.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
           
%>
<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - Govt User Role Creation</title>
        <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmRole").validate({
                    rules: {
                        txtdepartment : { required: true },
                        office: { required: true }
                    },
                    messages: {
                        txtdepartment: { required: "<div class='reqF_1'>Please Select Department</div>"},
                        office: { required: "<div class='reqF_1'>Please Select Office</div>"}
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "office")
                            error.insertAfter("#office");
                        else
                            error.insertAfter(element);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            <%-- $(function() {
                 if(document.getElementById("txtdepartmentid") != ""){
                 $('#txtdepartmentid').blur(function() {
                     $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId:$('#txtdepartmentid').val(),funName:'Office'}, function(j){
                         $("select#cmbOffice").html(j);
                     });
                 });
                 }
             });--%>
                 function onSelectionOfTreeOff(){
                     $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId:$('#txtdepartmentid').val(),funName:'Office'}, function(j){
                         //  alert(" value is :: "+j);
                         $("select#cmbOffice").html(j);
                     });
                 }
                 
                
                 /*$(function() {
                     if(document.getElementById("txtdepartmentid") != ""){
                         $('#txtdepartment').blur(function() {
                             $.post("<%//request.getContextPath()%>/GovtUserSrBean", {objectId:$('#txtdepartmentid').val(),funName:'Office'}, function(j){
                                //  alert(" value is :: "+j);
                                 $("select#cmbOffice").html(j);
                             });
                         });
                     }
                 });

                 $(function() {
                     if(document.getElementById("txtdepartmentid") != ""){
                         $('#txtdepartment').blur(function() {
                             $.post("<%//request.getContextPath()%>/GovtUserSrBean", {objectId:$('#txtdepartmentid').val(),funNameforSecond:'Designation'}, function(j){
                                // alert(" value is des:: "+j);
                                 $("select#cmbDesig").html(j);
                             });
                         });
                     }
                 });
                  */
                 function ShowHideforPMC(){

                     /*As "Accounts Officer" role is no more, the java scripts for "Accounts Officer" should be disabled*/
                     //if(document.getElementById("rdProcRoleGrSixA").checked == true || 
                     if(document.getElementById("rdProcRoleGrThreeB").checked == true ||
                         
                        document.getElementById("rdProcRoleGrFiveA").checked == true || document.getElementById("rdProcRoleGrThreeA").checked == true)
                         {
                         $("#trProcRoleGrSeven").hide();
                         }

                 }
                 function showHide(){
                    document.getElementById("rdCCGP").checked = false;
                    document.getElementById("rdProcRoleGrOneA").checked = false;
                    document.getElementById("rdProcRoleGrOneB").checked = false;
                    document.getElementById("rdProcRoleGrOneC").checked =false;
                    document.getElementById("rdProcRoleGrTwoA").checked = false;
                    //document.getElementById("rdProcRoleGrTwoB").checked = false;
                    document.getElementById("rdProcRoleGrThreeA").checked = false;
                    document.getElementById("rdProcRoleGrThreeB").checked = false;
                    document.getElementById("rdProcRoleGrFourA").checked = false;
                    document.getElementById("rdProcRoleGrFiveA").checked = false;
                    //document.getElementById("rdProcRoleGrSixA").checked = false;
                    
                     $("#trCcgp").hide();
                     $("#trProcRoleGrOne").hide();
                     $("#trProcRoleGrTwo").hide();
                     $("#trProcRoleGrThree").hide();
                     $("#trProcRoleGrFour").hide();
                     $("#trProcRoleGrFive").hide();
                     $("#trProcRoleGrSix").hide();
                     $("#trProcRoleGrSeven").hide();
                 }

                 function checkCondition(){
                     if(document.getElementById("txtDepartmentType").value == "CCGP"){
                         document.getElementById("trCcgp").style.display="block";
                         //for none
                         document.getElementById("trProcRoleGrOne").style.display="none";
                         document.getElementById("trProcRoleGrTwo").style.display="none";
                         document.getElementById("trProcRoleGrThree").style.display="none";
                         document.getElementById("trProcRoleGrFour").style.display="none";
                         document.getElementById("trProcRoleGrFive").style.display="none";
                         document.getElementById("trProcRoleGrSix").style.display="none";
                     }
                     else if(document.getElementById("txtDepartmentType").value == "Cabinet Div"){
                        // document.getElementById("trProcRoleGrOne").style.display="block"; change by dohatec
                         document.getElementById("tdGrOneA").style.display="none";
                         document.getElementById("tdGrOneC").style.display="none";
                         //for none
                         document.getElementById("trCcgp").style.display="block";  //change by dohatec
                         document.getElementById("trProcRoleGrTwo").style.display="none";
                         document.getElementById("trProcRoleGrThree").style.display="none";
                         document.getElementById("trProcRoleGrFour").style.display="none";
                         document.getElementById("trProcRoleGrFive").style.display="none";
                         document.getElementById("trProcRoleGrSix").style.display="none";
                     }
                     else if(document.getElementById("txtDepartmentType").value == "Ministry"){
                         document.getElementById("trCcgp").style.display="none";
                         $("#trProcRoleGrOne").show();
                         $("#tdGrOneA").show();
                         $("#tdGrOneB").show();
                         $("#tdGrOneC").hide();
                         $("#trProcRoleGrTwo").show();
                         $("#trProcRoleGrThree").show();
                         $("#trProcRoleGrFour").show();
                         $("#trProcRoleGrFive").show();
                         $("#trProcRoleGrSix").show();
                         //document.getElementById("trProcRoleGrOne").style.display="block";
                         //document.getElementById("trProcRoleGrTwo").style.display="block";
                         // document.getElementById("trProcRoleGrThree").style.display="block";
                         //document.getElementById("tdGrOneC").style.display="none";
                     }
                     else if(document.getElementById("txtDepartmentType").value == "Division"){
                         document.getElementById("trProcRoleGrOne").style.display="block";
                         document.getElementById("tdGrOneA").style.display="none";
                         document.getElementById("tdGrOneC").style.display="none";
                         document.getElementById("tdGrOneB").style.display="block";
                         document.getElementById("trProcRoleGrTwo").style.display="block";
                         document.getElementById("trProcRoleGrThree").style.display="block";
                         document.getElementById("trProcRoleGrFour").style.display="block";
                         document.getElementById("trProcRoleGrFive").style.display="block";
                         document.getElementById("trProcRoleGrSix").style.display="block";
                         //for none
                         document.getElementById("trCcgp").style.display="none";
                     }
                     else if(document.getElementById("txtDepartmentType").value == "Organization"){

                         document.getElementById("trProcRoleGrOne").style.display="block";
                         document.getElementById("tdGrOneC").style.display="block";
                         document.getElementById("tdGrOneA").style.display="none";
                         document.getElementById("tdGrOneB").style.display="none";
                         //for none
                         document.getElementById("trCcgp").style.display="none";
                         document.getElementById("trProcRoleGrTwo").style.display="block";
                         document.getElementById("trProcRoleGrThree").style.display="block";
                         document.getElementById("trProcRoleGrFour").style.display="block";
                         document.getElementById("trProcRoleGrFive").style.display="block";
                         document.getElementById("trProcRoleGrSix").style.display="block";
                     }
                     // document.getElementById("trProcRoleGrOne").style.visibility="visible";
                 }

                 function forMinistry(){
                     if(document.getElementById("rdProcRoleGrOneA").checked == true ){
                         document.getElementById("trProcRoleGrTwo").style.display="none";
                         document.getElementById("trProcRoleGrThree").style.display="none";
                     }
                     else if(document.getElementById("rdProcRoleGrOneB").checked == true ){
                         document.getElementById("trProcRoleGrTwo").style.display="block";
                         document.getElementById("trProcRoleGrThree").style.display="block";
                     }
            
                 }

            <%--function checkRole(){
                var count=0;
                if(document.getElementById("rdCCGP").checked == true){
                    count++;
                }
                if(document.getElementById("rdProcRoleGrOneA").checked == true){
                    count++;
                }
                if(document.getElementById("rdProcRoleGrOneB").checked == true){
                    count++;
                }
                if(document.getElementById("rdProcRoleGrOneC").checked == true){
                    count++;
                }
                if(document.getElementById("rdProcRoleGrTwoA").checked == true){
                    count++;
                }
                if(document.getElementById("rdProcRoleGrTwoB").checked == true){
                    count++;
                }
                if(document.getElementById("rdProcRoleGrThreeA").checked == true){
                    count++;
                }
                if(document.getElementById("rdProcRoleGrThreeB").checked == true){
                    count++;
                }
                if(count <= 1){
                    document.getElementById("rdAssFinPowerU").checked = true;
                    document.getElementById("lbAssFinPower").innerHTML = "(Please Select Multipal Procurement Role To Assign Role Specific Financial Power.)";
                    document.getElementById("lbAssFinPower").style.color='red';
                }
            }--%>

                function validateData(){
                   
                   // alert($('#cmbOffice').val());
                   // alert(document.getElementById('cmbOffice').value);

                   <%--if($('#txtdepartmentid').val() == "" || $('#txtdepartmentid').val() == null){
                        document.getElementById("errMsg").innerHTML="";
                        document.getElementById("errMsg").style.display='block';
                        document.getElementById("errMsg").innerHTML ='Please Select Department.';
                        return false;
                    }else{
                        document.getElementById("errMsg").innerHTML="";
                        document.getElementById("errMsg").style.display='none';
                    }--%>
                    if($('#cmbOffice').val() == 0 || $('#cmbOffice').val() == null){
                        document.getElementById("errMsg").innerHTML="";
                        document.getElementById("errMsg").style.display='block';
                        document.getElementById("errMsg").innerHTML ='Please Select Office.';
                        return false;
                    }else{
                        document.getElementById("errMsg").innerHTML="";
                        document.getElementById("errMsg").style.display='none';
                    }
                    //as accounts officer role is no more, the java scripts for accounts officer should be disables

//                   if(document.getElementById("rdProcRoleGrSixA").checked 
//                            && (document.getElementById("rdProcRoleGrOneA").checked 
//                            || document.getElementById("rdProcRoleGrOneB").checked
//                            || document.getElementById("rdProcRoleGrOneC").checked || document.getElementById("rdProcRoleGrTwoA").checked
//                            || document.getElementById("rdProcRoleGrThreeA").checked
//                            || document.getElementById("rdProcRoleGrThreeB").checked || document.getElementById("rdProcRoleGrFourA").checked
//                            || document.getElementById("rdProcRoleGrFiveA").checked)){
//                            document.getElementById("errMsg").style.display='block';
//                            document.getElementById("errMsg").innerHTML ='Account Officer can not have any other Procurement Role/s';
//                            return false;
//                        }
                        
                        document.getElementById("errMsg").innerHTML="";
                        document.getElementById("errMsg").style.display='none';
                        if($('#frmRole').valid()){
                             $('#btnAdd').attr("disabled", true);
                             $('#btnReset').attr("disabled", true);
                             document.getElementById("frmRole").action="<%=request.getContextPath()%>/GovtUserSrBean?action=createGovtUserRole";
                             document.getElementById("frmRole").submit();
                             return true;
                        }
                    
                
                }

                function clearProcureRoles(){
                        
                        document.getElementById("rdCCGP").checked = false;
                        document.getElementById("rdProcRoleGrOneA").checked = false;
                        document.getElementById("rdProcRoleGrOneB").checked = false;
                        document.getElementById("rdProcRoleGrOneC").checked = false;
                        document.getElementById("rdProcRoleGrTwoA").checked = false;
                        //document.getElementById("rdProcRoleGrTwoB").checked = false;
                        document.getElementById("rdProcRoleGrThreeA").checked = false;
                        document.getElementById("rdProcRoleGrThreeB").checked = false;
                        document.getElementById("rdProcRoleGrFourA").checked = false;
                        document.getElementById("rdProcRoleGrFiveA").checked = false;
                        //document.getElementById("rdProcRoleGrSixA").checked = false;
                        document.getElementById("rdProcRoleGrEightA").checked = false;
                        

                }

        </script>
<script type="text/javascript">
function finalSubmission(){
    var flag=false;
     //$.post("<%=request.getContextPath()%>/GovtUserSrBean", {userId:<%//userId%>,empId:<%=empId%>,action:'finalSubmission'}, function(j){
     //       alert(j);
     //       flag=true;
     // });
      document.getElementById("form1").action="<%=request.getContextPath()%>/GovtUserSrBean?userId=<%=userId%>&empId=<%=empId%>&action=finalSubmission&hidEmailId=<%=(String)object[0]%>&hidPassword=<%=(String)object[1]%>&hidMobNo=<%=(String)object[4]%>&edit=<%=str_edit%>";
      document.getElementById("form1").submit();
}

</script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                    <tr valign="top">
                        <%if(suserTypeId!=3){%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>
                        <%}%>

                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1"><div class="pageHead_1">
                                            <%if (empRolesReturn.size() <= 0) {
                                                           out.print("Assign Procurement Roles");
                                                       } else {
                                                           out.print("Assign Procurement Roles");
                                                       }
                                        boolean isEdit=false;
                                        if(request.getParameter("flag") != null) {
                                            if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
                                                flag = true;
                                            }
                                        }
                                        if(request.getParameter("Edit") != null) {
                                            if ("Edit".equalsIgnoreCase(request.getParameter("Edit"))) {
                                                isEdit = true;
                                            }
                                        }
                                            %></div>
                                         <%
                                            if(!flag && isEdit){
                                         %>
                                           <div id="errMsg1" class="responseMsg errorMsg" style="display:block">
                                              PA role is already created for selected office.
                                           </div>
                                         <%
                                            }
                                         %>
                                            <%-- <div class="pageHead_1">Create User</div>--%>
                                        <div id="successMsg" class="responseMsg successMsg" style="display:none"></div>
                                        <div id="errMsg" class="responseMsg errorMsg" style="display:none"></div>
                                        
                                        <div class="stepWiz_1 t_space">
                                            <ul>
                                                <li >
                                                    <% long empys = 0;

                                                    empys = govSrUser.countForQuery("TblEmployeeMaster", "employeeId = "+empId);                                                    
                                                    if(empys != 0){
                                                                if(suserTypeId==3){%>
                                                    <a style="text-decoration: underline;" href="GovtUserCreation.jsp?empId=<%=empId%>&Edit=Edit&fromWhere=EditProfile" >Employee Information</a>
                                                    <%}else{%>
                                                    <a style="text-decoration: underline;" href="GovtUserCreation.jsp?empId=<%=empId%>&Edit=Edit" >Employee Information</a>
                                                    <%}  }else{ %>
                                                    Employee Information
                                                    <% }  %>
                                                </li>
                                                <li class="sMenu">&gt;&gt;&nbsp;&nbsp;Employee Office Information </li>
<!--                                                <li>&gt;&gt;&nbsp;&nbsp;-->
                                                    <%

                                                   int dpId = 0;
                                                   String offid;
                                                   boolean check = false;
                                                   if(empRolesReturn != null){
                                                            if(empRolesReturn.size() > 0){
                                                        for (SPGovtEmpRolesReturn govtEmpRolesReturn : empRolesReturn) {
                                                            String roleId = govtEmpRolesReturn.getProcurementRoleId();
                                                            String roleName = govtEmpRolesReturn.getProcurementRole();
                                                            String finPowerBy = govtEmpRolesReturn.getFinPowerBy();
                                                            dpId = govtEmpRolesReturn.getDepartmentId();
                                                            String empRoleId = govtEmpRolesReturn.getEmployeeRoleId();
                                                            offid = govtEmpRolesReturn.getOfficeId();

                                                    if (!"Authorized User".equalsIgnoreCase(govtEmpRolesReturn.getProcurementRole().trim()) && !"AU".equalsIgnoreCase(govtEmpRolesReturn.getProcurementRole().trim())) {
                                                        check = true;
                                                    %>
<!--                                                    <a style="text-decoration: underline;" href="GovtUserFinanceRole.jsp?Edit=Edit&userId=<%=userId%>&empId=<%=empId%>&empRoleId=<%=empRoleId%>&roleId=<%=roleId%>&roleName=<%=roleName%>&finPowerBy=<%=finPowerBy%>&deptId=<%=dpId%>"> Assign Financial Power</a>-->
                                                    <%  }
                                                        }
                                                        }
                                                      }
                                                    //if(empRolesReturn.isEmpty() || check == false){ %>
<!--                                                    Assign Financial Power-->
<!--                                                    <% // } %> </li>-->
                                            </ul>
                                        </div>

                                        <%if (empRolesReturn.size() <= 0) {%>
                                        <form name="frmRole" id="frmRole" action="" method="POST"  >
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" >
                                                <tr>
                                                    <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                                </tr>
                                                <tr>
                                                    <td class="ff" width="15%">Employee Name : </td>
                                                    <td width="85%"><%=employeeName%></td>
                                                </tr>
                                                <tr style="display:none">
                                                    <td width="125" class="ff">Select Office Level: <span>*</span></td>
                                                    <td>
                                                        <input  class="formTxtBox_1" name="txtDepartmentType" type="hidden" id="txtDepartmentType" readonly style="width: 200px;"/>
                                                    </td>
                                                </tr>
                                                <% if (suserTypeId != 4 && suserTypeId != 5) {%>
                                                <tr>
                                                    <td class="ff" width="15%">Select Hierarchy Node <span>*</span></td>
                                                    <td width="85%"><input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 400px;"
                                                                           id="txtdepartment" onblur="showHide();checkCondition();"  readonly style="width: 200px;"/>
                                                        <input class="formTxtBox_1" name="txtdepartmentid" type="hidden" id="txtdepartmentid"  style="width: 200px;"/>
                                                        <label class="formBtn_1" style="display: none" > <input style="display: none" type="button" value="Select Department" onClick="window.open('DepartmentSelection.jsp','window','width=400,height=400')">
                                                        </label>
                                                        <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=govuser', '', 'width=350px,height=400px,scrollbars=1','');">
                                                            <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                                        </a>
                                                    </td>
                                                </tr>
                                                <%} else {%>
                                                <input class="formTxtBox_1" name="txtdepartmentid" type="hidden" value="<%=govDeptId%>" />
                                                <%}%>

                                                <tr id="trOffice">
                                                    <% if (suserTypeId != 4){%>
                                                    <td class="ff">Select Office : <span>*</span></td>                                                    
                                                    <td >
                                                        <select name="office"  id="cmbOffice" <%--multiple--%> class="formTxtBox_1" onchange="forOfficeselect();" style="width:280px;" >
                                                            <option value="0" >------ Select Office ------</option>
                                                        </select>
                                                    </td>
                                                    <%}else{%>
                                                    <td class="ff" >Office : </td>
                                                    <td id="tdOffice">
                                                        
                                                    </td>
                                                    <%}%>
                                                </tr>
                                                <tr>
                                                    <td class="ff" width="15%"> Procurement Role :<span>*</span> </td>
                                                    <td width="85%" class="t-align-left" valign="top" >
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="radioList_1">
                                                            <tr id="trCcgp" style="display:none" >
                                                                <td id="tdccgp" colspan="3" ><input type="radio" name="ccgp" id="rdCCGP" value="7"  />
                                                                    CCGP</td>
                                                            </tr>
                                                            <tr id="trProcRoleGrOne" style="display:none">
                                                                <td id="tdGrOneA" width="15%" class="t-align-left"><input type="radio" name="procRoleGrOne" id="rdProcRoleGrOneA" value="3" onclick="forMinistry();" />
                                                                    Minister</td>
                                                                <td id="tdGrOneB" width="40%" class="t-align-left"><input type="radio" name="procRoleGrOne" id="rdProcRoleGrOneB" value="2" onclick="forMinistry();"/>
                                                                    Secretary</td>
                                                                <td id="tdGrOneC" width="45%" class="t-align-left"><input type="radio" name="procRoleGrOne" id="rdProcRoleGrOneC" value="4" />
                                                                    BoD</td>
                                                            </tr>
                                                            <tr id="trProcRoleGrTwo" style="display:none">
                                                                <td id="tdGrTwoA"><input type="radio" name="procRoleGrTwo" id="rdProcRoleGrTwoA" value="6" />
                                                                    HOPA</td>
                                                                <td id="tdGrTwoB" colspan="2">&nbsp;</td>
                                                            </tr>
                                                            <tr id="trProcRoleGrThree" style="display:none">
                                                                <td id="tdGrThreeA"><input type="radio" name="procRoleGrThree" id="rdProcRoleGrThreeA" value="1" onchange="ShowHideforPMC()" />
                                                                    PA</td>
                                                                <td id="tdGrThreeA" colspan="2"><input type="radio" name="procRoleGrThree" id="rdProcRoleGrThreeB" value="5" onchange="ShowHideforPMC()" />
                                                                    Authorized User </td>
                                                            </tr>
                                                            <tr id="trProcRoleGrEight">
                                                                <td id="tdGrEightA"><input type="radio" name="procRoleGrEight" id="rdProcRoleGrEightA" value="32" />
                                                                    TC</td>                                                             
                                                            </tr>
                                                            <tr id="trProcRoleGrFour" style="display:none">
                                                                <td colspan="2" id="tdGrFourA"><input type="radio" name="procRoleGrFour" id="rdProcRoleGrFourA" value="22" onchange="ShowHideforPMC()" />
                                                                    TOC
                                                                </td>
                                                            </tr>
                                                            <tr id="trProcRoleGrFive" style="display:none">
                                                                <td id="tdGrFiveA" colspan="2"><input type="radio" name="procRoleGrFive" id="rdProcRoleGrFiveA" value="23" onchange="ShowHideforPMC()" />
                                                                    TEC
                                                                </td>
                                                            </tr>
                                                            <!--tr id="trProcRoleGrSix" style="display:none">
                                                                <td id="tdGrSixA" colspan="2"><input type="radio" name="procRoleGrSix" id="rdProcRoleGrSixA" value="25" onchange="ShowHideforPMC()" />
                                                                    Accounts Officer
                                                                </td>
                                                            </tr-->
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr class="t-align-left">
                                                    <td>&nbsp;</td>
                                                    <td class="t-align-left">
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="btnAdd" id="btnAdd" value="Add Procurement Role" onclick="return validateData();" />
                                                        </label>&nbsp;
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnReset" id="btnReset" value="Reset" onclick="clearProcureRoles();" />
                                                        </label>
                                                        <input type="hidden" name="empId" id="empId" value="<%=govSrUser.getEmpId()%>" />
                                                        &nbsp;
                                                        <%-- <label class="formBtn_1">
                                                             <input type="button" name="btnRemove" id="btnRemove" value="Remove Procurement Role" />
                                                         </label>--%>
                                                        &nbsp;
                                                        <%--<label class="formBtn_1">
                                                            <input type="button" name="btnNext" id="btnNext" value="Next" />
                                                        </label>--%>
                                                    </td>
                                                </tr>
                                            </table>

                                            <%--<table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">

                                            </table>--%>
                                            <%--<table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <td class="ff" width="125"> Assign Financial power :<span>*</span> </td>
                                                <td>
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="radioList_1">
                                                        <tr id="trAssFinPower">
                                                            <td id="tdAssFinPowerU"><input type="radio" name="assFinPower" id="rdAssFinPowerU" value="user" checked />
                                                                User specific</td>
                                                            <td id="tdAssFinPowerR"><input type="radio" name="assFinPower" id="rdAssFinPowerR" value="role" onclick="return checkRole();" />
                                                                Role specific </td>
                                                            <td id="tdGrThreeA"><label name="lbAssFinPower" id="lbAssFinPower" style="color:red" > (&nbsp;Please Select Multipal Procurement Role To Assign Role Specific Financial Power.) </label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </table>--%>
                                            <!-- Buttons -->
                                            <table width="100%" align="" border="0" cellspacing="10" cellpadding="0" class="">

                                            </table>

                                        </form>
                                        <%}%>
                                        <form id="form1" style="display:none" method="post"  >
                                            <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td width="25%" class="ff">Select Office: <span>*</span></td>
                                                    <td>
                                                    <td width ="75%" id="tdOffice" width="125" class="ff" style="display:block;">
                                                        <select name="soffice"   id="cmbsOffice" <%--multiple--%> class="formTxtBox_1" >
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="btnAddOffice" id="btnAddOffice" value="Add Office" onclick="addOffice();" />
                                                        </label>
                                                         <input type="hidden" name="pRoleId" id="pRoleId" value=""  />
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                        <table width="100%" cellspacing="0" class="tableList_1 t_space" >
                                            <tr>
                                                <%-- <th class="t-align-left" width="5%" align="center">Sr No. </th>--%>
                                                <th>Employee Name </th>
                                                <th>e-mail ID</th>
                                                <th>Hierarchy Node</th>
                                                <th>Office</th>
                                                <th>Designation</th>
                                                <th>Procurement Role </th>
                                                <% if(!(suserTypeId==3 || suserTypeId==4)){ %>
                                                <th>Office</th>
<!--                                                <th>Assign Financial<br> Power </th>-->
                                                <%if(!"approved".equalsIgnoreCase(status)){%>
                                                <th>Remove Procurement Roles</th>
                                                <%}/*When status is not approved*/}%>
                                            </tr>
                                            <%
                                                        /*List<TblEmployeeRoles> empRoles = govSrUser.getEmployeeRoles();                                                        
                                                        String rEmp = "";
                                                        for (TblEmployeeRoles roles : empRoles) {
                                                        rEmp += roles.getTblProcurementRole().getProcurementRole() + ",";
                                                        }

                                                        departmentId = empRoles.get(0).getTblDepartmentMaster().getDepartmentId();
                                                        govSrUser.setDepartmentId(departmentId);

                                                        TblDepartmentMaster departmentMaster = govSrUser.getDepartmentMasterData();
                                                        govSrUser.setUserId(userId);
                                                        */
                                                        
                                                        // String designationName = govSrUser.takeDesignationName(designationId);
                                                        int deptId = 0;
                                                        String officeId = "";
                                                        if(empRolesReturn != null){
                                                            if(empRolesReturn.size() > 0){
                                                        for (SPGovtEmpRolesReturn govtEmpRolesReturn : empRolesReturn) {
                                                            String roleId = govtEmpRolesReturn.getProcurementRoleId();
                                                            String roleName = govtEmpRolesReturn.getProcurementRole();
                                                            String finPowerBy = govtEmpRolesReturn.getFinPowerBy();
                                                            deptId = govtEmpRolesReturn.getDepartmentId();
                                                            String empRoleId = govtEmpRolesReturn.getEmployeeRoleId();
                                                            officeId = govtEmpRolesReturn.getOfficeId();
                                                            //govtEmpRolesReturn.
                                            %>
                                            <tr>
                                                <%--<td class="t-align-left" width="5%" >1</td>--%>
                                                <td class="t-align-left"><%=govtEmpRolesReturn.getEmployeeName()%></td>
                                                <td class="t-align-left"><%=(String)object[0]%></td>
                                                <td class="t-align-left"><%=govtEmpRolesReturn.getDepartmentname()%></td>
                                                <td class="t-align-left"><%=govtEmpRolesReturn.getOfficeName()%></td>
                                                <td class="t-align-left"><%=govtEmpRolesReturn.getDesignationName()%></td>
                                                <td class="t-align-center">
                                                <%
                                                    String ReplaceHOPE = govtEmpRolesReturn.getProcurementRole();
//                                                    while(ReplaceHOPE.charAt(0)==' ')
//                                                    {
//                                                        ReplaceHOPE = ReplaceHOPE.substring(1,ReplaceHOPE.length());
//                                                    }
                                                    if(ReplaceHOPE.contains("HOPE"))
                                                    {
                                                        ReplaceHOPE = ReplaceHOPE.replaceAll("HOPE", "HOPA");
                                                        //out.print("<td width=\"10%\" class=\"t-align-center\">" + ReplaceHOPE + "</td>");
                                                    }
                                                    if(ReplaceHOPE.contains("PE"))
                                                    {
                                                        int LocationOfPE = ReplaceHOPE.indexOf("PE");
                                                        char[] ReplaceHOPEChars = ReplaceHOPE.toCharArray();
                                                        if(LocationOfPE==0)
                                                        {

                                                            ReplaceHOPEChars[LocationOfPE+1] = 'A';
                                                            ReplaceHOPE = String.valueOf(ReplaceHOPEChars);

                                                        }
                                                        else
                                                        {
                                                            if(ReplaceHOPE.charAt(LocationOfPE-1)==',' || ReplaceHOPE.charAt(LocationOfPE-1)==' ')
                                                            {
                                                                ReplaceHOPEChars[LocationOfPE+1] = 'A';
                                                                ReplaceHOPE = String.valueOf(ReplaceHOPEChars);
                                                            }

                                                        }
                                                    }
                                                    //out.print(ReplaceHOPE);
                                                %>
                                                <%=ReplaceHOPE%>
                                                </td>
                                                <% if(!(suserTypeId==3 || suserTypeId==4)){ %>
                                                <td class="t-align-center">
                                                    <a href="#" id="hrfOffice" onclick="showOffice('<%=govtEmpRolesReturn.getProcurementRole().trim()%>');" title="Edit">Edit</a>
                                                </td>
<!--                                                <td class="t-align-center" >
                                                    <% 
                                                    if (!"Authorized User".equalsIgnoreCase(govtEmpRolesReturn.getProcurementRole().trim()) && !"AU".equalsIgnoreCase(govtEmpRolesReturn.getProcurementRole().trim())) {
                                                    %>
                                                    <a href="GovtUserFinanceRole.jsp?Edit=Edit&userId=<%=userId%>&empId=<%=empId%>&empRoleId=<%=empRoleId%>&roleId=<%=roleId%>&roleName=<%=roleName%>&finPowerBy=<%=finPowerBy%>&deptId=<%=deptId%>"> Assign</a> </td>
                                                    <%} else {
                                                                                                                    out.print(" - ");
                                                                                                                }%>
                                                -->
                                                <%if(!"approved".equalsIgnoreCase(status)){%>
                                                <td class="t-align-center">
                                                    <a href="../GovtUserSrBean?action=Remove&userId=<%=userId%>&empId=<%=empId%>&empRoleId=<%=empRoleId%>&officeId=<%=officeId%>" title="Remove"><img  src="../resources/images/Dashboard/delIcn.png" alt="Remove" width="16" height="16" /></a>
                                                </td>
                                                <%}/*Condtion for deleteing role before stuatus is approved*/%>
                                                <%}%>
                                            </tr>
                                            
                                            <%
                                                        }
                                                        }
                                                        else{
                                                            out.print("<tr><td colspan='8'>Record Not Found.</td></tr>");
                                                        }
                                                      }
                                                      else{
                                                        out.print("<tr><td colspan='8'>Record Not Found.</td></tr>");
                                                      }
                                            %>

                                        </table>
                                            <%if(empRolesReturn != null){
                                                            if(empRolesReturn.size() > 0){
                                                                if(!status.equalsIgnoreCase("approved")){
                                                                  if(suserTypeId==1||suserTypeId==5||suserTypeId==4){%>
                                                <table width="100%" class="t_space" >
                                                        <tr>
                                                            <td  class="t-align-center">
                                                                 <label class="formBtn_1">
                                                                    <input type="button" name="btnSubmission" id="btnSubmission" value="Complete Registration" onclick="finalSubmission();" />
                                                                 </label>
                                                            </td>
                                                        </tr>
                                                </table>
                                                            <%}}}}%>
                                    </td>

                                </tr>

                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <div>&nbsp;</div>

                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <%--<%@include file="../resources/common/Bottom.jsp" %>--%>
                <jsp:include page="../resources/common/Bottom.jsp" />
                <!--Dashboard Footer End-->
            </div>
        </div>
<script type="text/javascript">
var headSel_Obj = document.getElementById("headTabMngUser");
if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
}
</script>
    </body>
</html>
<script type="text/javascript">
    //../GovtUserSrBean?action=officeEdit&userId=<%=userId%>&empId=<%=empId%>&designation=<%=designationId%>&officeId=
    <%if (suserTypeId == 4){%>
    $(document).ready(function() {
        $.ajax({
                    url: "<%=request.getContextPath()%>/GovtUserSrBean",
                    data: "objectId=<%=deptId%>&userTypeId=<%=userTypeId%>&employeeId=<%=empId%>&funName=SOffice",
                    method: 'POST',
                    async: false,
                    success: function(j) {
            $("#tdOffice").html(j);
                    }
        });
            if($('#cmbOffice').html()!=null){
                $.ajax({
                    url: "<%=request.getContextPath()%>/GovtUserSrBean",
                    data: "office="+$('#cmbOffice').val()+"&funNameforPE=checkPE",
                    method: 'POST',
                    cache: false,
                    async: false,
                    success: function(j) {
                        if(j=='true'){
                        $('#tdGrThreeA').hide();
                    }else{
                        $('#tdGrThreeA').show();
                    }
                    }
    });
            }
    });
    <%}if (suserTypeId == 5||suserTypeId == 1){%>
    $(document).ready(function() {
       /* $.post("<%=request.getContextPath()%>/GovtUserSrBean",{objectId:<%=deptId%>,employeeId:<%=empId%>,funName:'SOffice'}, function(j){
            alert(j+"soffice");
            $("select#cmbsOffice").html(j);

        });*/
            $.ajax({
                    url: "<%=request.getContextPath()%>/GovtUserSrBean",
                    data: "objectId=<%=deptId%>"+"&employeeId=<%=empId%>"+"&funName=SOffice",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                         $("select#cmbsOffice").html(j);
                    }
        });
        /*$(document).ready(function() {
            $.post("<%=request.getContextPath()%>/GovtUserSrBean",{objectId:<%=govDeptId%>,funName:'Office'}, function(j){
                alert(j+"office");
                $("select#cmbOffice").html(j);
            });
        });*/
            $.ajax({
                    url: "<%=request.getContextPath()%>/GovtUserSrBean",
                    data: "objectId=<%=govDeptId%>"+"&funName=Office",
                    method: 'POST',
                    async: false,
                    cache: false,
                    success: function(j) {
                        $("select#cmbOffice").html(j);
                    }
        });
         
//        alert($('#cmbOffice').val()+"officeval");
       /* if($('#cmbOffice').val()!=null){
            
                $.ajax({
                    url: "<%=request.getContextPath()%>/GovtUserSrBean",
                    data: "office="+$('#cmbOffice').val()+"&funNameforPE=checkPE",
                    method: 'POST',
                    async: false,
                    success: function(j) {
                        alert(j+"check Pe");
                        if(j=='true'){
                        $('#tdGrThreeA').hide();
                    }else{
                        $('#tdGrThreeA').show();
                    }
                    }
    });
            }*/
         
    });
    
    <%}%>


    <%--<%if (suserTypeId == 4 || suserTypeId == 5) {%>
        $(document).ready(function() {
            $.post("<%=request.getContextPath()%>/GovtUserSrBean",{objectId:<%=govDeptId%>,funName:'Office'}, function(j){
                $("select#cmbOffice").html(j);
            });
        });
    <%}%>--%>

    <%--<%if (suserTypeId == 4 || suserTypeId == 5) {%>
        $(document).ready(function() {
            $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId:<%=govDeptId%>,funNameforSecond:'Designation'}, function(j){
                $("select#cmbDesig").html(j);
            });
        });
    <%}%>--%>

<%if (suserTypeId == 4 || suserTypeId == 5) {%>
        $(document).ready(function() {
            $('#trProcRoleGrThree').show();
            $('#trProcRoleGrFour').show();
            $('#trProcRoleGrFive').show();
            $('#trProcRoleGrSix').show();
            
            <%if(suserTypeId == 4){%>
                  $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId:<%=govDeptId%>,funNameforHope:'checkHope'}, function(j){

               $('#trProcRoleGrSeven').show();
                if(j == "true"){
                    $('#trProcRoleGrTwo').show(); // for fixing issue #4159
                    $('#tdGrTwoA').hide();
                }
                else
                 {
                        $('#trProcRoleGrTwo').show();
                        //$('#tdGrTwoA').show();
                        //$('#tdGrTwoB').hide();
                    }
            });                                
            <%}%>
        });
    <%}%>

        <%--$(function() {
            $('#hrfOffice').bind("click",function(){
                $('#form1').show();
            });
        });--%>

        function showOffice(procurementRoleId){
            $('#form1').show();
            //alert(procurementRoleId);
            $('#pRoleId').val(procurementRoleId);
           // alert($('#pRoleId').val());
        }

    function checkHOPE(){
        if(document.getElementById("txtdepartmentid") != ""){
            $.post("<%=request.getContextPath()%>/GovtUserSrBean", {objectId:$('#txtdepartmentid').val(),funNameforHope:'checkHope'}, function(j){
                if(j == "true"){
                    document.getElementById("rdProcRoleGrTwoA").checked = false;
                    $('#tdGrTwoA').hide();
                }
                //rdProcRoleGrTwoA
            });
        }
    }

        function checkPEForOffice(){
             $.post("<%=request.getContextPath()%>/GovtUserSrBean", {office:$('#cmbOffice').val(),funNameforPE:'checkPE'}, function(j){
                    //alert(j);
                    if(j == "true"){

                    }
             });
        }
        function forOfficeselect(){
           <%-- $.post("<%=request.getContextPath()%>/GovtUserSrBean", {office:$('#cmbOffice').val(),funNameforPE:'checkPE'}, function(j){
                                alert(j);
                                if(j == "true"){
                                    document.getElementById("rdProcRoleGrThreeA").checked = false;
                                    $('#tdGrThreeA').hide();
                                }
                            });--%>
            $.ajax({
                url: "<%=request.getContextPath()%>/GovtUserSrBean?office="+$('#cmbOffice').val()+"&funNameforPE=checkPE&",
                method: 'POST',
                async: true,
                cache: false,
                success: function(j) {
                    if(j == "true"){
                        document.getElementById("rdProcRoleGrThreeA").checked = false;
                        $('#tdGrThreeA').hide();
                    }else{
                       $('#tdGrThreeA').show();
                    }
                }
            });
        }

        function addOffice(){
            $('#btnAddOffice').attr("disabled", true);
            document.getElementById("form1").action = "<%=request.getContextPath()%>/GovtUserSrBean?action=officeEdit&userId=<%=userId%>&empId=<%=empId%>&designation=<%=designationId%>&officeId=<%=officeId%>";
            document.getElementById("form1").submit();
        }

</script>
<%
            // govSrUser=null;
%>
