<%-- 
    Document   : Statistics
    Created on : Jan 27, 2011, 5:59:04 PM
    Author     : rishita
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearch"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.UserStatisticsDtBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Statistics</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("hdnUserType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                            userType.append(request.getParameter("hdnUserType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }
                                    //CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                                    //UserStatisticsDtBean userStatisticsDtBean = commonService.getUserStatisticsDtBean();
                                    CommonSearchService commonSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
                                    List<SPCommonSearchData> detailsOne = commonSearchService.searchData("GetManageUsersStatisticsOne", "", "", "", "", "", "", "", "", "");
                                    List<SPCommonSearchData> detailsTwo = commonSearchService.searchData("GetManageUsersStatisticsTwo", "", "", "", "", "", "", "", "", "");
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <td class="contentArea">
                            <div class="pageHead_1">Statistics</div>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space">

                                <tr>
                                    <th colspan="2">Ministry:</th>
                                </tr>
                                <tr>
                                    <td>No of Ministries:</td>
                                    <td><%=detailsOne.get(0).getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td>No. of PE Offices:</td>
                                    <td><%=detailsOne.get(0).getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td>No. of PE Admin Users:</td>
                                    <td><%=detailsOne.get(0).getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td>No. of Government Users:</td>
                                    <td><%=detailsOne.get(0).getFieldName4()%></td>
                                </tr>

                                <tr>
                                    <th colspan="2">Division:</th>
                                </tr>
                                <tr>
                                    <td>No. of Divisions:</td>
                                    <td><%=detailsOne.get(0).getFieldName5()%></td>
                                </tr>
                                <tr>
                                    <td>No. of PE Offices:</td>
                                    <td><%=detailsOne.get(0).getFieldName6()%></td>
                                </tr>
                                <tr>
                                    <td>No. of PE Admin Users:</td>
                                    <td><%=detailsOne.get(0).getFieldName7()%></td>
                                </tr>
                                <tr>
                                    <td>No. of Government Users:</td>
                                    <td><%=detailsOne.get(0).getFieldName8()%></td>
                                </tr>

                                <tr>
                                    <th colspan="2">Organization:</th>
                                </tr>
                                <tr>
                                    <td>No. of Organizations:</td>
                                    <td><%=detailsOne.get(0).getFieldName9()%></td>
                                </tr>
                                <tr>
                                    <td>No. of PE Offices:</td>
                                    <td><%=detailsOne.get(0).getFieldName10()%></td>
                                </tr>
                                <tr>
                                    <td>No. of PE Admin Users:</td>
                                    <td><%=detailsOne.get(0).getFieldName11()%></td>
                                </tr>
                                <tr>
                                    <td>No. of Government Users:</td>
                                    <td><%=detailsOne.get(0).getFieldName12()%></td>
                                </tr>

                                <tr>
                                    <th colspan="2">Development Partner:</th>
                                </tr>
                                <tr>
                                    <td>No. of Development Partners:</td>
                                    <td><%=detailsTwo.get(0).getFieldName1()%></td>
                                </tr>
                                <tr>
                                    <td>No. of Development Partners’ Regional/Country Offices:</td>
                                    <td><%=detailsTwo.get(0).getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td>No. of Development Partner Admin Users:</td>
                                    <%//userStatisticsDtBean.getCountDevelopmentPartnerAdminUser() %>
                                    <td><%=detailsTwo.get(0).getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td>No. of Development Partner Users:</td>
                                    <%//userStatisticsDtBean.getCountDevelopmentPartnerUser() %>
                                    <td><%=detailsTwo.get(0).getFieldName4()%></td>
                                </tr>

                                <tr>
                                    <th colspan="2">Scheduled Banks:</th>
                                </tr>
                                <tr>
                                    <td>No. of Scheduled Banks:</td>
                                    <td><%=detailsTwo.get(0).getFieldName5()%></td>
                                </tr>
                                <tr>
                                    <td>No. of Scheduled Bank Branches:</td>
                                    <td><%=detailsTwo.get(0).getFieldName6()%></td>
                                </tr>
                                <tr>
                                    <td>No. of Scheduled Bank Admin Users:</td>
                                    <td><%=detailsTwo.get(0).getFieldName7()%></td>
                                </tr>
                                <tr>
                                    <td>No. of Scheduled Bank Users:</td>
                                    <td><%=detailsTwo.get(0).getFieldName8()%></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
