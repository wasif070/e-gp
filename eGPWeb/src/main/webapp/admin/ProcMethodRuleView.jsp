<%-- 
    Document   : ProcMethodRuleView
    Created on : Nov 25, 2010, 12:08:06 PM
    Author     : Naresh.Akurathi
--%>


<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="com.cptu.egp.eps.model.table.TblBudgetType"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigProcurement"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<%@page import="java.util.Iterator"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
    %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Procurement Method Business Rule</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
<script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>        <script type="text/javascript">
            $(document).ready(function() {
                sortTable();
            });
        </script>

<script type="text/javascript">
    function conform()
    {
        if (confirm("Do you want to delete this business rule?"))
            return true;
        else
            return false;
    }
    
    
    $(function() {
                var count;
                $('#btnSearch').click(function() {
                    
                    var headString = "<tr>"+
                        "<th >Budget  Type <br /></th>"+
                        "<th >Tender  Type<br /></th>"+
                        "<th >Procurement  Method<br /></th>"+
                        "<th >Procurement  Type<br /></th>"+
                        "<th >Procurement  Category<br /></th>"+
                        "<th >Minimum Value<br />(In Nu.)<br /></th>"+
                        "<th >Minimum Value<br />(In Nu. Words)<br /></th>"+
                        "<th >Maximum  Value<br />(In Nu.)<br /></th>"+
                        "<th >Maximum  Value<br />(In Nu. Words)<br /></th>"+
                        "<th >No. of days for <br/>New-Tender/New-Proposal Submission   <br /></th>"+
                        "<th >No. of days for <br/>Re-Tender/Re-Proposal Submission<br /></th>"+
                        "<th >Action</th>"+
                     "</tr>";
             
                    var tableBody="";
                    var rFlag= "true";
                    
                    if(document.getElementById('budgetType').value=='null' && document.getElementById('tenderType').value=='null' &&document.getElementById('procMethod').value=='null' && document.getElementById('procType').value=='null' && document.getElementById('procNature').value=='null')
                    {
                        valAll.innerHTML = "Please enter at least one search criteria";
                    }
                    else
                    {
                        valAll.innerHTML = "";
                        <%List l2 = configPreTenderRuleSrBean.getConfigProcurement();
                          int i2 = 0;
                          if(!l2.isEmpty() || l2 != null){
                            Iterator it2 = l2.iterator();

                            TblConfigProcurement configProcurement2;
                            while(it2.hasNext())
                            {
                                i2++;
                                configProcurement2 = (TblConfigProcurement)it2.next();
                                %>
                                    givenBudgetType = "<%=configProcurement2.getTblBudgetType().getBudgetTypeId()%>";
                                    givenTenderType = "<%=configProcurement2.getTblTenderTypes().getTenderTypeId()%>";
                                    givenProcMethod = "<%=configProcurement2.getTblProcurementMethod().getProcurementMethodId()%>";
                                    givenProcType = "<%=configProcurement2.getTblProcurementTypes().getProcurementTypeId()%>";
                                    givenProcNature = "<%=configProcurement2.getTblProcurementNature().getProcurementNatureId()%>";
                                    
                                    min = CurrencyConverter(<%=configProcurement2.getMinValue()%>);
                                    max = CurrencyConverter(<%=configProcurement2.getMaxValue()%>);
                                    if(document.getElementById('budgetType').value!="All" && document.getElementById('budgetType').value!="null" && document.getElementById('budgetType').value!=givenBudgetType)
                                    {
                                        rFlag = "false";
                                    }
                                    if(document.getElementById('tenderType').value!='All' && document.getElementById('tenderType').value!='null' && givenTenderType != document.getElementById('tenderType').value)
                                    {
                                        rFlag = "false";
                                    }
                                    if(document.getElementById('procMethod').value!='All' && document.getElementById('procMethod').value!='null' && givenProcMethod != document.getElementById('procMethod').value)
                                    {
                                        rFlag = "false";
                                    }
                                    if(document.getElementById('procType').value!='All' && document.getElementById('procType').value!='null' && givenProcType != document.getElementById('procType').value)
                                    {
                                        rFlag = "false";
                                    }
                                    if(document.getElementById('procNature').value!='All' && document.getElementById('procNature').value!='null' && givenProcNature != document.getElementById('procNature').value)
                                    {
                                        rFlag = "false";
                                    }
                                    if(rFlag == "true")
                                    {
                                        tableBody+="<tr><td class='t-align-center'><%if(configProcurement2.getTblBudgetType().getBudgetType().equalsIgnoreCase("Development")){out.print("Capital");}else if(configProcurement2.getTblBudgetType().getBudgetType().equalsIgnoreCase("Revenue")){out.print("Recurrent");} else {out.print(configProcurement2.getTblBudgetType().getBudgetType());}%></td><td class='t-align-center'><%=configProcurement2.getTblTenderTypes().getTenderType()%></td><td class='t-align-center'><%if(configProcurement2.getTblProcurementMethod().getProcurementMethod().equalsIgnoreCase("RFQ")){out.print("LEM");}else if(configProcurement2.getTblProcurementMethod().getProcurementMethod().equalsIgnoreCase("DPM")){out.print("DCM");}else{out.print(configProcurement2.getTblProcurementMethod().getProcurementMethod());}%></td><td class='t-align-center'><%if(configProcurement2.getTblProcurementTypes().getProcurementType().equalsIgnoreCase("NCT")){out.print("NCB");} else if(configProcurement2.getTblProcurementTypes().getProcurementType().equalsIgnoreCase("ICT")){out.print("ICB");}%></td><td class='t-align-center'><%=configProcurement2.getTblProcurementNature().getProcurementNature()%></td><td class='t-align-center'><%=configProcurement2.getMinValue()%></td><td class='t-align-center'><span  id='minwords_<%=i2%>'>"+min+"</span></td><td class='t-align-center'><%=configProcurement2.getMaxValue()%></td><td class='t-align-center'>"+max+"</td><td class='t-align-center'><%=configProcurement2.getMinSubDays()%></td><td class='t-align-center'><%=configProcurement2.getMaxSubDays()%></td><td class='t-align-center'><a href='ProcMethodRuleDetails.jsp?action=edit&id=<%=configProcurement2.getConfigProcurementId()%>'>Edit</a>&nbsp;|&nbsp;<a href='ProcMethodRuleView.jsp?action=delete&id=<%=configProcurement2.getConfigProcurementId()%>' onclick='return conform();'>Delete</a></td></tr>";
                                    
                                    }
                                    rFlag = "true"
                                    
                                    <%
                                    
                            }
                            
                        }
                        
                        %>
                        if(tableBody=="")
                        {
                            resultTable.innerHTML = "<p style=\"color:red;width:100%;padding-left:43%; font-size:20px;\"><b>No Records Found</p>";
                        }
                        else{
                                resultTable.innerHTML = headString+tableBody;
                        }
                        
                        
                    }
                    
                });
            });
            
            
            
            $(function() {
                var count;
                $('#btnReset').click(function() {
                    
                    var headString = "<tr>"+
                        "<th >Budget  Type <br /></th>"+
                        "<th >Tender  Type<br /></th>"+
                        "<th >Procurement  Method<br /></th>"+
                        "<th >Procurement  Type<br /></th>"+
                        "<th >Procurement  Category<br /></th>"+
                        "<th >Minimum Value<br />(In Nu.)<br /></th>"+
                        "<th >Minimum Value<br />(In Nu. Words)<br /></th>"+
                        "<th >Maximum  Value<br />(In Nu.)<br /></th>"+
                        "<th >Maximum  Value<br />(In Nu. Words)<br /></th>"+
                        "<th >No. of days for <br/>New-Tender/New-Proposal Submission   <br /></th>"+
                        "<th >No. of days for <br/>Re-Tender/Re-Proposal Submission<br /></th>"+
                        "<th >Action</th>"+
                     "</tr>";
             
                    var tableBody="";
                   
                         <%List l23 = configPreTenderRuleSrBean.getConfigProcurement();
                          int i23 = 0;
                          if(!l23.isEmpty() || l23 != null){
                            Iterator it23 = l23.iterator();

                            TblConfigProcurement configProcurement23;
                            while(it23.hasNext())
                            {
                                i23++;
                                configProcurement23 = (TblConfigProcurement)it23.next();
                                %>
                                    givenBudgetType = "<%=configProcurement23.getTblBudgetType().getBudgetTypeId()%>";
                                    givenTenderType = "<%=configProcurement23.getTblTenderTypes().getTenderTypeId()%>";
                                    givenProcMethod = "<%=configProcurement23.getTblProcurementMethod().getProcurementMethodId()%>";
                                    givenProcType = "<%=configProcurement23.getTblProcurementTypes().getProcurementTypeId()%>";
                                    givenProcNature = "<%=configProcurement23.getTblProcurementNature().getProcurementNatureId()%>";
                                    
                                    min = CurrencyConverter(<%=configProcurement23.getMinValue()%>);
                                    max = CurrencyConverter(<%=configProcurement23.getMaxValue()%>);
                                    tableBody+="<tr><td class='t-align-center'><%if(configProcurement23.getTblBudgetType().getBudgetType().equalsIgnoreCase("Development")){out.print("Capital");}else if(configProcurement23.getTblBudgetType().getBudgetType().equalsIgnoreCase("Revenue")){out.print("Recurrent");} else {out.print(configProcurement23.getTblBudgetType().getBudgetType());}%></td><td class='t-align-center'><%=configProcurement23.getTblTenderTypes().getTenderType()%></td><td class='t-align-center'><%if(configProcurement23.getTblProcurementMethod().getProcurementMethod().equalsIgnoreCase("RFQ")){out.print("LEM");}else if(configProcurement23.getTblProcurementMethod().getProcurementMethod().equalsIgnoreCase("DPM")){out.print("DCM");}else{out.print(configProcurement23.getTblProcurementMethod().getProcurementMethod());}%></td><td class='t-align-center'><%if(configProcurement23.getTblProcurementTypes().getProcurementType().equalsIgnoreCase("NCT")){out.print("NCB");} else if(configProcurement23.getTblProcurementTypes().getProcurementType().equalsIgnoreCase("ICT")){out.print("ICB");}%></td><td class='t-align-center'><%=configProcurement23.getTblProcurementNature().getProcurementNature()%></td><td class='t-align-center'><%=configProcurement23.getMinValue()%></td><td class='t-align-center'><span  id='minwords_<%=i2%>'>"+min+"</span></td><td class='t-align-center'><%=configProcurement23.getMaxValue()%></td><td class='t-align-center'>"+max+"</td><td class='t-align-center'><%=configProcurement23.getMinSubDays()%></td><td class='t-align-center'><%=configProcurement23.getMaxSubDays()%></td><td class='t-align-center'><a href='ProcMethodRuleDetails.jsp?action=edit&id=<%=configProcurement23.getConfigProcurementId()%>'>Edit</a>&nbsp;|&nbsp;<a href='ProcMethodRuleView.jsp?action=delete&id=<%=configProcurement23.getConfigProcurementId()%>' onclick='return conform();'>Delete</a></td></tr>";
                                    
                                    
                                    <%
                                    
                            }
                            
                        }
                        
                        %>
                        resultTable.innerHTML = headString+tableBody;
                        document.getElementById("sForm").reset();
                    
                    
                });
            });
            
            
            
    
</script>

<%!
  ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();
 
    %>

</head>
    <body onload="hide();">

    <%
            String mesg =  request.getParameter("msg");
            String delMsg = "";
            if("delete".equals(request.getParameter("action"))){
                String action = "Remove Procurement Method Rules";
                try{
                    int configId = Integer.parseInt(request.getParameter("id"));

                    TblConfigProcurement configProcurement = new TblConfigProcurement();

                    configProcurement.setConfigProcurementId(configId);
                    configProcurement.setIsNationalDisaster("ss");
                    configProcurement.setIsUrgency("ur");
                    configProcurement.setMaxSubDays(Short.parseShort("1"));
                    configProcurement.setMaxValue(new BigDecimal(Byte.parseByte("1")));
                    configProcurement.setMinSubDays(Short.parseShort("1"));
                    configProcurement.setMinValue(new BigDecimal(Byte.parseByte("1")));
                    configProcurement.setTblBudgetType(new TblBudgetType(Byte.parseByte("1")));
                    configProcurement.setTblProcurementMethod(new TblProcurementMethod(Byte.parseByte("1")));
                    configProcurement.setTblProcurementNature(new TblProcurementNature(Byte.parseByte("1")));
                    configProcurement.setTblProcurementTypes(new TblProcurementTypes(Byte.parseByte("1")));
                    configProcurement.setTblTenderTypes(new TblTenderTypes(Byte.parseByte("1")));

                    configPreTenderRuleSrBean.delConfigProcurement(configProcurement);
                    delMsg = "Procurement Method Business Rule Deleted successfully";

                }catch(Exception e){
                    System.out.println(e);
                    action = "Error in : "+action +" : "+ e;
                 }finally{
                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                    action=null;
                  }
                }
    %>
<div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
                        <div class="pageHead_1">Procurement Method Business Rule Configuration
                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('12');">Save as PDF</a></span>
                        </div>
                
                <%
                            StringBuilder budgetType = new StringBuilder();
                            StringBuilder tenderType = new StringBuilder();
                            StringBuilder procNature = new StringBuilder();
                            StringBuilder procType = new StringBuilder();
                            StringBuilder procMethod = new StringBuilder();
                            
                            TblBudgetType budgetTypes2 = new TblBudgetType();
                            Iterator bdit2 = configPreTenderRuleSrBean.getBudgetType().iterator();
                            String budType="";
                            budgetType.append("<option value='null'>---Select---</option>");
                            budgetType.append("<option value='All'>All</option>");
                            while (bdit2.hasNext()) {
                                budgetTypes2 = (TblBudgetType) bdit2.next();
                                if(budgetTypes2.getBudgetType().equalsIgnoreCase("Development"))
                                    budType="Capital";
                                else if(budgetTypes2.getBudgetType().equalsIgnoreCase("Revenue"))
                                    budType="Recurrent";
                                else
                                    budType= budgetTypes2.getBudgetType();
                                budgetType.append("<option value='" + budgetTypes2.getBudgetTypeId() + "'>" + budType + "</option>");
                            }

                            TblTenderTypes tenderTypes2 = new TblTenderTypes();
                            Iterator trit2 = configPreTenderRuleSrBean.getTenderNames().iterator();
                            tenderType.append("<option value='null'>---Select---</option>");
                            tenderType.append("<option value='All'>All</option>");
                            while (trit2.hasNext()) {
                                tenderTypes2 = (TblTenderTypes) trit2.next();
                                if(!tenderTypes2.getTenderType().equals("PQ") && !tenderTypes2.getTenderType().equals("RFA") && !tenderTypes2.getTenderType().equals("2 Stage-PQ"))
                                {
                                    tenderType.append("<option value='" + tenderTypes2.getTenderTypeId() + "'>" + tenderTypes2.getTenderType() + "</option>");
                                }
                            }
                            TblProcurementNature tblProcureNature2 = new TblProcurementNature();
                            Iterator pnit2 = configPreTenderRuleSrBean.getProcurementNature().iterator();
                            procNature.append("<option value='null'>---Select---</option>");
                            procNature.append("<option value='All'>All</option>");
                            while (pnit2.hasNext()) {
                                tblProcureNature2 = (TblProcurementNature) pnit2.next();
                                procNature.append("<option value='" + tblProcureNature2.getProcurementNatureId() + "'>" + tblProcureNature2.getProcurementNature() + "</option>");
                            }

                            TblProcurementMethod tblProcurementMethod2 = new TblProcurementMethod();
                            Iterator pmit2 = configPreTenderRuleSrBean.getProcurementMethod().iterator();
                            procMethod.append("<option value='null'>---Select---</option>");
                            procMethod.append("<option value='All'>All</option>");
                            while (pmit2.hasNext()) {
                                tblProcurementMethod2 = (TblProcurementMethod) pmit2.next();
                                if("OTM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                                    "LTM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                                    "DP".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                                   // "LEQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                  //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                    "RFQ".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                                   // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                  //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                    "FC".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                                   // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                                    "DPM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())||
                                     "QCBS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                                     "LCS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                                     "SFB".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                                     "SBCQ".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                                     "SSS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()) ||
                                     "IC".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod()))
                                 {
                                    if(tblProcurementMethod2.getProcurementMethod().equals("RFQ"))
                                    {
                                        procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>LEM</option>");

                                    }
                                    else if(tblProcurementMethod2.getProcurementMethod().equals("DPM"))
                                    {
                                        procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>DCM</option>");

                                    }
                                    else
                                    {
                                        procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>" + tblProcurementMethod2.getProcurementMethod() + "</option>");

                                    }
                                 }

                            }

                            TblProcurementTypes tblProcurementTypes2 = new TblProcurementTypes();
                            Iterator ptit2 = configPreTenderRuleSrBean.getProcurementTypes().iterator();
                            String procureType="";
                            procType.append("<option value='null'>---Select---</option>");
                            procType.append("<option value='All'>All</option>");
                            while (ptit2.hasNext()) {
                                tblProcurementTypes2 = (TblProcurementTypes) ptit2.next();
                                if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("NCT"))
                                    procureType="NCB";
                                else if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("ICT"))
                                    procureType="ICB";
                                procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + procureType + "</option>");
                            }


                %>
                
                
                
                
                <div>&nbsp;</div>
                <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                <div class="formBg_1">
                    <jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>
                    
                    <table id="tblSearchBox" cellspacing="10" class="formStyle_1" width="100%">

                        <form id="sForm">
                        <tr>
                            <td width="17%" class="ff">Budget Type :</td>
                            <td width="33%">
                                <select name='budgetType' class='formTxtBox_1' id='budgetType' style="width:200px;"><%=budgetType%></select>
                            </td>
                        </tr>
                            
                        <tr>
                            <td width="17%" class="ff">Tender Type :</td>
                            <td width="33%">
                                <select name='tenderType' class='formTxtBox_1' id='tenderType' style="width:200px;"><%=tenderType%></select>
                            </td>
                            <td width="17%" class="ff">Procurement Method :</td>
                            <td width="33%">
                                <select name='procMethod' class='formTxtBox_1' id='procMethod' style="width:200px;"><%=procMethod%></select>
                            </td>
                        </tr>
                            
                            
                         <tr>
                            <td width="17%" class="ff">Procurement Type :</td>
                            <td width="33%">
                                <select name='procType' class='formTxtBox_1' id='procType' style="width:200px;"><%=procType%></select>
                            </td>
                            <td width="17%" class="ff">Procurement Category :</td>
                            <td width="33%">
                                <select name='procNature' class='formTxtBox_1' id='procNature' style="width:200px;"><%=procNature%></select>
                            </td>
                        </tr>
                        </form>
                        
                        <tr>
                            <td class="ff">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="ff">&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center"><label class="formBtn_1">
                                    <input type="submit" name="button" id="btnSearch" value="Search" />
                                </label>
                                &nbsp;
                                <label class="formBtn_1">
                                    <input type="button" name="btnReset" id="btnReset" value="Reset"/>
                                </label></td>
                        </tr>
                        <tr>
                            <td class="t-align-center" colspan="4">
                                <div id="valAll" class="reqF_1"></div>
                            </td>
                        </tr>
                    </table>

                </div>
                        
                        
                        <div>&nbsp;</div>
                        <%if("delete".equals(request.getParameter("action"))){%>
                          <div class="responseMsg successMsg"><%=delMsg%></div>
                          <%}%>
                        <%if(mesg != null){%>
                        <div class="responseMsg successMsg"><%=mesg%></div>
                        <%}%>

   <form action="ProcMethodRuleView.jsp" method="post">
 <table width="100%" cellspacing="0" class="tableList_1 t_space" name="resultTable" id="resultTable"  cols="@12">
     <tr>
       <th >Budget  Type <br /></th>
       <th >Tender  Type<br /></th>
       <th >Procurement  Method<br /></th>
       <th >Procurement  Type<br /></th>
       <th >Procurement  Category<br /></th>
      <%-- <th >Type Of Emergency<br /></th>--%>
       <th >Minimum Value<br />(In Nu.)<br /></th>
       <th >Minimum Value<br />(In Nu. Words)<br /></th>
       <th >Maximum  Value<br />(In Nu.)<br /></th>
       <th >Maximum  Value<br />(In Nu. Words)<br /></th>
       <th >No. of days for <br/>New-Tender/New-Proposal Submission   <br /></th>
       <th >No. of days for <br/>Re-Tender/Re-Proposal Submission<br /></th>
       <th >Action</th>
    </tr>

     <%
            String msg = "";
            List l = configPreTenderRuleSrBean.getConfigProcurement();
            int i = 0;
            if(!l.isEmpty() || l != null){
                    Iterator it = l.iterator();

                    TblConfigProcurement configProcurement;
                    while(it.hasNext())
                    {
                        configProcurement = (TblConfigProcurement)it.next();
                        i++;
                        if(configProcurement.getIsNationalDisaster().equals("Normal")){
                        if("OTM".equalsIgnoreCase(configProcurement.getTblProcurementMethod().getProcurementMethod()) ||
                            "LTM".equalsIgnoreCase(configProcurement.getTblProcurementMethod().getProcurementMethod()) ||
                            "DP".equalsIgnoreCase(configProcurement.getTblProcurementMethod().getProcurementMethod()) ||
                           // "LEQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                          //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                            "RFQ".equalsIgnoreCase(configProcurement.getTblProcurementMethod().getProcurementMethod()) ||
                           // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                          //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                            "FC".equalsIgnoreCase(configProcurement.getTblProcurementMethod().getProcurementMethod()) ||
                           // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                            "DPM".equalsIgnoreCase(configProcurement.getTblProcurementMethod().getProcurementMethod())||
                             "QCBS".equalsIgnoreCase(configProcurement.getTblProcurementMethod().getProcurementMethod()) ||
                             "LCS".equalsIgnoreCase(configProcurement.getTblProcurementMethod().getProcurementMethod()) ||
                             "SFB".equalsIgnoreCase(configProcurement.getTblProcurementMethod().getProcurementMethod()) ||
                             "SBCQ".equalsIgnoreCase(configProcurement.getTblProcurementMethod().getProcurementMethod()) ||
                             "SSS".equalsIgnoreCase(configProcurement.getTblProcurementMethod().getProcurementMethod()) ||
                             "IC".equalsIgnoreCase(configProcurement.getTblProcurementMethod().getProcurementMethod()))
                         {

    %>
    <tr
      <%if(i%2==0){%>
                        class="bgColor-Green"
                    <%} else {%>
                    class="bgColor-white"
                    <%   }%>
                    >
        <td class="t-align-center"><%if(configProcurement.getTblBudgetType().getBudgetType().equalsIgnoreCase("Development")){out.print("Capital");}else if(configProcurement.getTblBudgetType().getBudgetType().equalsIgnoreCase("Revenue")){out.print("Recurrent");} else {out.print(configProcurement.getTblBudgetType().getBudgetType());}%></td> <!--Edit by Aprojit -->
        <td class="t-align-center"><%=configProcurement.getTblTenderTypes().getTenderType()%></td>
        <td class="t-align-center"><%if(configProcurement.getTblProcurementMethod().getProcurementMethod().equalsIgnoreCase("RFQ")){out.print("LEM");}else if(configProcurement.getTblProcurementMethod().getProcurementMethod().equalsIgnoreCase("DPM")){out.print("DCM");}else{out.print(configProcurement.getTblProcurementMethod().getProcurementMethod());}%></td>
        <td class="t-align-center"><%if(configProcurement.getTblProcurementTypes().getProcurementType().equalsIgnoreCase("NCT")){out.print("NCB");} else if(configProcurement.getTblProcurementTypes().getProcurementType().equalsIgnoreCase("ICT")){out.print("ICB");}%></td> <!--Edit by Aprojit -->
        <td class="t-align-center"><%=configProcurement.getTblProcurementNature().getProcurementNature()%></td>
       <%-- <td class="t-align-center"><%=configProcurement.getIsNationalDisaster()%></td>--%>

        <td class="t-align-center"><%=configProcurement.getMinValue()%></td>
        <td class="t-align-center"><span  id="minwords_<%=i%>"></span></td>
        <td class="t-align-center"><%=configProcurement.getMaxValue()%></td>
        <td class="t-align-center"><span id="maxwords_<%=i%>"></span></td>
        <td class="t-align-center"><%=configProcurement.getMinSubDays()%></td>
        <td class="t-align-center"><%=configProcurement.getMaxSubDays()%></td>
        <td class="t-align-center"><a href="ProcMethodRuleDetails.jsp?action=edit&id=<%=configProcurement.getConfigProcurementId()%>">Edit</a>&nbsp;|&nbsp;<a href="ProcMethodRuleView.jsp?action=delete&id=<%=configProcurement.getConfigProcurementId()%>" onclick="return conform();">Delete</a></td>
          </tr>

    <script type="text/javascript">
            $("#minwords_"+<%=i%>).html(CurrencyConverter(<%=configProcurement.getMinValue()%>));
            $("#maxwords_"+<%=i%>).html(CurrencyConverter(<%=configProcurement.getMaxValue()%>));
    </script>
        <%
        }}}
    }
    else
      {
          msg = "No Record Found";
      }

%>
 </table>
<div align="center"> <%=msg%></div>
</form>
                    </td>
                </tr>
            </table>
        <form id="formstyle" action="" method="post" name="formstyle">

           <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
           <%
             SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
             String appenddate = dateFormat1.format(new Date());
           %>
           <input type="hidden" name="fileName" id="fileName" value="ProcRule_<%=appenddate%>" />
            <input type="hidden" name="id" id="id" value="ProcurementRule" />
        </form>
  <div>&nbsp;</div>
   <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

</div>
    <script>
                var obj = document.getElementById('lblProcMethodView');
                if(obj != null){
                    if(obj.innerHTML == 'View'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
   <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
                
    function showHide()
    {
        if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
            document.getElementById('tblSearchBox').style.display = 'table';
            document.getElementById('collExp').innerHTML = '- Advanced Search';
        }else{
            document.getElementById('tblSearchBox').style.display = 'none';
            document.getElementById('collExp').innerHTML = '+ Advanced Search';
        }
    }
    function pageReload()
    {
        location.reload(true);
    }
    function hide()
    {
        document.getElementById('tblSearchBox').style.display = 'none';
        document.getElementById('collExp').innerHTML = '+ Advanced Search';
    }
    
    
    </script>
</body>
</html>



