<%-- 
    Document   : ContentVerification
    Created on : Dec 4, 2010, 2:51:43 PM
    Author     : Ramesh.Janagondakuruba
--%>

<%@page import="com.cptu.egp.eps.web.databean.ContentVeriConfigDtBean"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ContentVeriConfigSrBean" %>
<jsp:useBean id="contentVeriConfigDtBean" class="com.cptu.egp.eps.web.databean.ContentVeriConfigDtBean" />
<jsp:useBean id="contentVeriConfigSrBean" class="com.cptu.egp.eps.web.servicebean.ContentVeriConfigSrBean"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
     <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Verification </title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
</head>
<body>
    <%!
        ContentVeriConfigDtBean con=new ContentVeriConfigDtBean();
   %>
   <%
            if("Submit".equals(request.getParameter("button"))){
                String forum=request.getParameter("forum");
                String expert=request.getParameter("expert");             
                 con.setIsProcureExpert(expert);
                con.setIsPublicForum(forum);
                contentVeriConfigSrBean.addContent(con);
            }

   %>
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <div class="topHeader">
    <%@include file="../resources/common/AfterLoginTop.jsp" %>
  </div>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <form  id="conVeriConfig" method="post" action="ContentVeriConfig.jsp">
   <div class="pageHead_1">Content Verification Configuration</div>
   <div>&nbsp;</div>
  <table width="100%" cellspacing="0" class="tableList_1 t_space" id="conVeri" name="conVeri">
    <tr>
      <td class="ff" width="17%">Public Forum Verification Requires : </td>
      <td width="83%"><select name="forum" class="formTxtBox_1" id="forum" style="width:100px;">
        <option>- Select -</option>
        <option> Yes </option>
        <option> No </option>
      </select></td>
    </tr>
    <tr>
	  <td class="ff">Ask Procurement Expert :</td>
	  <td><select name="expert" class="formTxtBox_1" id="expert" style="width:100px;">
	    <option>- Select -</option>
	    <option> Yes </option>
	    <option> No </option>
	    </select></td>
    </tr>
    <tr>
      <td></td>
      <td class="t-align-left">
      	<label class="formBtn_1">
        <input type="submit" name="button" id="button" value="Submit" />
        </label>
      </td>
    </tr>
  </table>
  <div>&nbsp;</div>
  </form>
  <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
   <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
  <!--Dashboard Footer End-->
</div>
  <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
  </script>
</body>
</html>

