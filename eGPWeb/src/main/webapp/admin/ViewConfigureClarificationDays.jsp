<%-- 
    Document   : ViewConfigClarificationDays
    Created on : Mar 01, 2017, 4:00:15 PM
    Author     : MD. Emtazul Haque
--%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigClarification"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ConfigClarificationService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Tender Clarification Days Configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>        <script type="text/javascript">
            $(document).ready(function() {
                sortTable();
            });
        </script>
        <script type="text/javascript">
            function confirmDelete(){
                var len=$('#tbodyData').children().length-1;                 
                if(len==2){
                    jAlert("Atleast one configuration is required.","Tender Clarification Days Rule Configuration", function(RetVal) {
                    });
                    return false;
                }else{
                if(window.confirm("Do you really want to delete the configuration Rule?")){
                    return true;
                }
                else{
                    return false;
            }
            }
         }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <%
                    ConfigClarificationService configService = (ConfigClarificationService) AppContext.getSpringBean("ConfigClarificationService");
                    configService.setLogUserId(session.getAttribute("userId").toString());
                    List<Object[]> SBDNames = configService.getSBDList();
                    List<TblConfigClarification> getConfigClarification = configService.getAllTblConfigClarification();
            %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea">
                        <div class="pageHead_1">Tender Clarification Days Configuration
                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('2');">Save as PDF</a></span>
                        </div>
                        <table class="tableList_1 t_space" cellspacing="0" width="100%" id="resultTable" cols="@2">
                            <tbody id="tbodyData">&nbsp;
                                <%
                                            String message = "";
                                            if (!"".equalsIgnoreCase(request.getParameter("msg"))) {
                                                message = request.getParameter("msg");
                                                if ("updsucc".equalsIgnoreCase(message)) {
                                %>
                            <div class="responseMsg successMsg t_space"><span>Tender Clarification Days Configured Successfully</span></div><br/>
                            <%                                                } else if ("editsucc".equalsIgnoreCase(message)) {
                            %>
                            <div class="responseMsg successMsg t_space"><span>Tender Clarification Days updated successfully</span></div><br/>
                            <%                                    }
                            else if ("delsucc".equalsIgnoreCase(message)) {
                                                        %>
                                                        <div class="responseMsg successMsg t_space"><span>Tender Clarification Days deleted successfully</span></div><br/>
                                                        <%                                    }
                                        }
                            %>
                            <tr>
                                <th class="t-align-center">SBD Name</th>
                                <th class="t-align-center">Number of days<br>before Tender closing date<br>for posting query</th>
                                <th class="t-align-center">Action</th>
                            </tr>
                            <%
                                        int cnt = 0;
                                        for(cnt=0;cnt<getConfigClarification.size();cnt++){
                            %>
                             <%if(cnt%2==0){%>
                            <tr>
          <%}else{%>
          <tr style='background-color:#E4FAD0;'>
          <%}%>
                                <td class="t-align-center">

                                    <%for (Object[] proc : SBDNames) {
                                        if (getConfigClarification.get(cnt).getTemplateId() == (Short) proc[0]) {
                                            out.print(proc[1]);
                                        }%>
                                    <%}%>
                                </td>
                               
                                <td class="t-align-center">
                                    <%out.print(getConfigClarification.get(cnt).getClarificationDays());%>
                                </td>
                                
                                <td class="t-align-center">
                                    <a href="EditConfigClarification.jsp?ConfigClarificationId=<%=getConfigClarification.get(cnt).getConfigClarificationId()%>">Edit</a> <!-- &nbsp;|&nbsp; -->
                                    <!--<a href="<%//=request.getContextPath()%>/ConfigClarificationServlet?action=delSingleConfigClarification&ConfigClarificationId=<%//=getConfigClarification.get(cnt).getConfigClarificationId()%>" onclick="return confirmDelete()" >Delete</a>-->
                                </td>
                            </tr>
                            <%
                                } %>
                            </tbody>
                        </table>
                </td>
            </tr>
        </table>

        <form id="formstyle" action="" method="post" name="formstyle">

           <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
           <%
             SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
             String appenddate = dateFormat1.format(new Date());
           %>
           <input type="hidden" name="fileName" id="fileName" value="EvaluationRule_<%=appenddate%>" />
            <input type="hidden" name="id" id="id" value="EvaluationRule" />
        </form>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script>
        var obj = document.getElementById('lblConfigClarificationView');
        if(obj != null){
            if(obj.innerHTML == 'View'){
                obj.setAttribute('class', 'selected');
            }
        }

    </script>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabConfig");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
