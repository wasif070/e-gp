<%--
    Document   : CreateTemplate
    Created on : 24-Oct-2010, 1:03:35 AM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />
<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Define SBD Content Type</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->

        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../resources/js/form/CommonValidation.js"></script>

        <script type="text/javascript" language="javascript">
            
            function isValid(val)
            {
                var vResult = true;
                if(val.indexOf("\\") != -1 || val.indexOf("/") != -1 || val.indexOf(":") != -1 ||
                    val.indexOf("*") != -1 || val.indexOf("?") != -1 || val.indexOf("\"") != -1 ||
                    val.indexOf("<") != -1 || val.indexOf(">") != -1 || val.indexOf("|") != -1 ||
                    val.indexOf("&") != -1){
                    vResult  = false;
                }
                if(val.charAt(0) == "." || val.charAt(val.length - 1) == ".")
                {
                    vResult = false;
                }
                return vResult;
            }

            function Validate()
            {
                HideValidate();
                var noofsection='<%=request.getParameter("noOfSection")%>';
                var chk=true;
                for(var i=0; i<noofsection; i++){
                    var tempname="txtSecName"+(i+1);
                    var tempname_val  = document.getElementById(tempname).value;
                    if(trim(document.getElementById(tempname).value)=='')
                    {
                        var temperror="error"+(i+1)                        
                        document.getElementById(temperror).innerHTML="Please enter Section Name";
                        chk=false;
                    }
                    else if(!isValid(tempname_val))
                    {
                        var temperror="error"+(i+1)
                        document.getElementById(temperror).innerHTML="Characters ( \\ / : *? \" < > | & ) are not allowed in section name";
                        chk=false;
                        flag = false;
                    }
                    else
                    {
                        var temperror="error"+(i+1)
                        if(!ChkLength(document.getElementById(tempname).value))
                        {
                            document.getElementById(temperror).innerHTML="Maximum 200 characters are allowed";
                            chk=false;
                                  
                        }
                        else
                        {
                            document.getElementById(temperror).innerHTML="";

                        }
                    }

                    var tempname1="selContentType"+(i+1);
                    if(document.getElementById(tempname1).value=='')
                    {
                        var temperror="dropdownlist"+(i+1)
                        document.getElementById(temperror).innerHTML="Please select Content Type";
                        chk=false;
                    }
                }

                if(chk==false)
                {
                    return false;
                }
            }

            function ChkLength(value)
            {
                var ValChk=value;
                if(ValChk.length>200)
                {
                    return false;
                }
                else
                {
                    return true;

                }

            }

            function HideValidate()
            {
                var noofsection='<%=request.getParameter("noOfSection")%>';
                for(var i=0; i<noofsection; i++){
                    var tempname="txtSecName"+(i+1);
                    if(trim(document.getElementById(tempname).value) == '')
                    {
                        document.getElementById(tempname).value = "";
                        var temperror="error"+(i+1)
                        //document.getElementById(temperror).innerHTML="Please Enter Section Name";
                    }
                    else
                    {
                        document.getElementById(tempname).value = trim(document.getElementById(tempname).value);
                        var temperror="error"+(i+1)
                        document.getElementById(temperror).innerHTML="";
                    }
                }
            }

            // Yagnesh.
            function ChangeSelected(currentid, pType){
                var currentNo = 0;
                var noOfSections = parseInt('<%=request.getParameter("noOfSection")%>');
                var currentSelected = document.getElementById(currentid).value;
                currentNo = currentid.substring(14);
                if(currentSelected == "ITT" || currentSelected == "TDS" || currentSelected == "GCC" || currentSelected == "PCC"){
                    var counter = 0;
                    for(var i=0; i<noOfSections; i++){
                        var itrObjVal = $("#selContentType"+(i+1)).val();
                        if(itrObjVal == currentSelected){
                            counter++;
                        }
                        if(counter > 1){
                            document.getElementById(currentid).value = "";
                            if(pType == 'goods' || pType == 'works' || pType=='srvnoncon'){
                                document.getElementById("dropdownlist"+currentNo).innerHTML = "Content Type " + currentSelected +" already selected";
                            }else if(pType == 'srvcmp'){
                                if(currentSelected == 'ITT'){
                                    currentSelected = 'ITC';
                                }
                                if(currentSelected == 'TDS'){
                                    currentSelected = 'PDS';
                                }
                                document.getElementById("dropdownlist"+currentNo).innerHTML = "Content Type " + currentSelected +" already selected";
                            }else if(pType == 'srvindi'){
                                if(currentSelected == 'ITT'){
                                    currentSelected = 'ITA';
                                }
                                document.getElementById("dropdownlist"+currentNo).innerHTML = "Content Type " + currentSelected +" already selected";
                            }

                            break;
                        }
                    }
                    if(counter == 1){
                        document.getElementById("dropdownlist"+currentNo).innerHTML = "";
                    }
                    //alert("After Break This is Called");
                }else if(currentSelected == "Form" || currentSelected == "Document" || currentSelected == "Document By PE" || currentSelected == "TOR"){
                    document.getElementById("dropdownlist"+currentNo).innerHTML = "";
                }

            }

            // Old
            function ChangeSelected123(currentid)
            {
                var currentelementid=document.getElementById(currentid).value;
                var j=0;
                var noofsection='<%=request.getParameter("noOfSection")%>';
                for(var i=0; i<noofsection; i++){
                    var tempname="selContentType"+(i+1);
                    if(document.getElementById(tempname).value=='')
                    {
                        var temperror="dropdownlist"+(i+1)
                        //document.getElementById(temperror).innerHTML="Please Enter Content Type";
                    }
                    else
                    {
                        var temperror="dropdownlist"+(i+1)
                        var tempdrop="#"+tempname;

                        var e = $(""+tempdrop+"").val();
                        
                        if (currentelementid=="ITT" || currentelementid=="TDS" || currentelementid=="GCC" ||currentelementid=="PCC")
                        {
                            if (currentelementid==e && j==0)
                            {
                                j=1;
                            }
                            else if(currentelementid!=e && j==0)
                            {
                            }
                            else
                            {
                                j=0;
                                document.getElementById(currentid).value="";
                            }
                        }
                        document.getElementById(temperror).innerHTML="";
                    }
                }
            }
        </script>
    </head>
    <body>
        <%
                    String logUserId = "0";
                    if (session.getAttribute("userId") != null) {
                        logUserId = session.getAttribute("userId").toString();
                    }
                    defineSTDInDtlSrBean.setLogUserId(logUserId);
        %>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Middle Content Table Start-->

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr valign="top">
                    <td class="contentArea_1">
                        <!--Page Content Start-->
                        <div class="t_space">
                            <div class="pageHead_1">
                                Define SBD Content Type
                                <span style="float: right; text-align: right;">
                                    <a href="ListSTD.jsp" class="action-button-goback">Go Back</a>
                                </span>
                            </div>
                        </div>
                        <form action="<%=request.getContextPath()%>/DefineSTDSectionsSrBean" method="post" id="DefineSTDSectionsSrBean" name="DefineSTDSectionsSrBean">
                            <%
                                        byte noOfSection = Byte.parseByte(request.getParameter("noOfSection"));
                                        int templateId = Integer.parseInt(request.getParameter("templateId"));
                                        String procType = "";
                                        List<TblTemplateMaster> templateMasterLst = defineSTDInDtlSrBean.getTemplateMaster((short) templateId);
                                        if (templateMasterLst != null) {
                                            if (templateMasterLst.size() > 0) {
                                                procType = templateMasterLst.get(0).getProcType();
                                            }
                                            templateMasterLst = null;
                                        }
                                        String procurementType = "";
                                        if ("goods".equals(procType)) {
                                            procurementType = "Goods";
                                        } else if ("works".equals(procType)) {
                                            procurementType = "Works";
                                        } else if ("srvcmp".equals(procType)) {
                                            procurementType = "Services - Consulting Firms";
                                        } else if ("srvindi".equals(procType)) {
                                            procurementType = "Services - Individual Consultant";
                                        } else if ("srvnoncon".equals(procType)) {
                                            procurementType = "Services - Non consulting services";
                                        }
                                        //out.println("procType -> " + procType);
%>
                            <input type="hidden" name="hdNoOfSection" value="<%=noOfSection%>" />
                            <input type="hidden" name="hdTemplateId" value="<%=templateId%>" />
                            <table width="100%" border="0" cellpadding="" cellspacing="0" class="t_space">
                                <tr>
                                    <td style="font-style: italic" align="left" class="ff" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                </tr>
                                <tr>
                                    <td colspan="2" height="7"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="15%"><span class="">SBD Name: </span></td>
                                    <td align="left" width="85%"><span class=""><b><%=request.getParameter("templateName")%></b></span></td>
                                </tr>
                                <tr>
                                    <td colspan="2" height="7"></td>
                                </tr>
                                <tr>
                                    <td align="left" width="15%"><span class="">Procurement Type: </span></td>
                                    <td align="left" width="85%"><span class=""><b><%= procurementType%></b></span></td>
                                </tr>
                            </table>

                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <th width="1%" >Section No.</th>
                                    <th>Section Name
                                        <span style="color:red;">*</span>
                                    </th>
                                    <th>Content Type
                                        <span style="color:red;">*</span>
                                    </th>
                                </tr>
                                <%
                                            for (int i = 0; i < noOfSection; i++) {
                                %>
                                <tr>
                                    <td class="t-align-center" ><%=i + 1%></td>
                                    <td>
                                        <textarea rows="3" class="formTxtBox_1" name="txtSecName<%=i + 1%>"  id="txtSecName<%=i + 1%>" style="width:400px;"></textarea>
                                        <br/><span id="error<%=i + 1%>" class='reqF_1'></span>
                                    </td>
                                    <td class="t-align-center">
                                        <select name="selContentType<%=i + 1%>" class="formTxtBox_1" id="selContentType<%=i + 1%>" onchange="ChangeSelected($(this).attr('id'), '<%= procType%>');">
                                            <option value="">Select Content Type</option>
                                            <%if ("goods".equals(procType) || "works".equals(procType)) {%>
                                            <option value="ITT">ITB</option>
                                            <option value="TDS">BDS</option>
                                            <option value="GCC">GCC</option>
                                            <option value="PCC">SCC</option>
                                            <option value="Form">Form</option>
                                            <option value="Document">Document</option>
                                            <option value="Document By PE">Document By PA</option>
                                            <%} else if ("srvcmp".equals(procType)) {%>
                                            <option value="ITT">ITC</option>
                                            <option value="TDS">BDS</option>
                                            <option value="GCC">GCC</option>
                                            <option value="PCC">SCC</option>
                                            <option value="TOR">TOR</option>
                                            <option value="Form">Form</option>
                                            <option value="Document">Document</option>
                                            <option value="Document By PE">Document By PA</option>
                                            <%} else if ("srvindi".equals(procType)) {%>
                                            <option value="ITT">ITA</option>
                                            <option value="TOR">TOR</option>
                                            <option value="Form">Form</option>
                                            <option value="Document">Document</option>
                                            <option value="Document By PE">Document By PA</option>
                                            <%} else if ("srvnoncon".equals(procType)) {%>
                                            <option value="ITT">ITB</option>
                                            <option value="TDS">BDS</option>
                                            <option value="GCC">GCC</option>
                                            <option value="PCC">SCC</option>
                                            <option value="Form">Form</option>
                                            <option value="Document">Document</option>
                                            <option value="Document By PE">Document By PA</option>
                                            <%}%>
                                        </select>
                                        <br/><span id="dropdownlist<%=i + 1%>" class="reqF_1"></span>
                                    </td>
                                </tr>
                                <%
                                            }
                                %>
                            </table>
                            <div class="t_space" align="center">
                                <label class="formBtn_1">
                                    <input type="submit" name="button" id="button_submit" value="Submit" onclick="return Validate();"/></label>
                                &nbsp;
                                <label class="formBtn_1">
                                    <input type="reset" name="Reset" id="button" value="Reset" />
                                </label>
                            </div>
                        </form>
                        <!--Page Content End-->
                    </td>
                </tr>
            </table>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <script>
            var headSel_Obj = document.getElementById("headTabSTD");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
<%
            if (defineSTDInDtlSrBean != null) {
                defineSTDInDtlSrBean = null;
            }
%>
