<%--
    Document   : Add NewClause
    Created on : 24-Oct-2010, 3:13:11 PM
    Author     : yanki
--%>

<jsp:useBean id="sectionClauseSrBean" class="com.cptu.egp.eps.web.servicebean.SectionClauseSrBean" />
<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBean" />
<jsp:useBean id="prepareTDSSrBean" class="com.cptu.egp.eps.web.servicebean.PrepareTDSSrBean" />
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />

<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster" %>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.model.table.TblIttClause" %>
<%@page import="com.cptu.egp.eps.model.table.TblIttSubClause" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <%
        int templateId = 0;
        if(request.getParameter("templateId")!=null){
            templateId = Integer.parseInt(request.getParameter("templateId"));
        }
        String procType = "";
        List<TblTemplateMaster> templateMasterLst = defineSTDInDtlSrBean.getTemplateMaster((short) templateId);
        if(templateMasterLst != null){
            if(templateMasterLst.size() > 0){
                procType = templateMasterLst.get(0).getProcType();
            }
            templateMasterLst = null;
        }
        
        int ittHeaderId = Integer.parseInt(request.getParameter("ittHeaderId"));
        int sectionId = Integer.parseInt(request.getParameter("sectionId"));
        String contentType = createSubSectionSrBean.getContectType(sectionId);
        if("srvcmp".equals(procType)){
            if("ITT".equals(contentType)){
                contentType = contentType.replace("ITT", "ITC");
            }else if("TDS".equals(contentType)){
                contentType = contentType.replace("TDS", "PDS");
            }
        }else if("srvindi".equals(procType)){
            if("ITT".equals(contentType)){
                contentType = contentType.replace("ITT", "ITA");
            }
        }
        String contentType1 = "";
        String instruction = "";
        String clauseInstr = "";
        if(contentType.equalsIgnoreCase("ITT") || contentType.equalsIgnoreCase("TDS")){
            contentType1 = "TDS";
            instruction = "Instructions for completing Bid Data Sheet are provided in italics in parenthesis for the relevant ITB clauses";
            clauseInstr = "Amendments of, and Supplements to, Clauses in the Instructions to Tenderers/Consultants";
        }else if(contentType.equalsIgnoreCase("ITC")){
            contentType1 = "PDS";
            instruction = "Instructions for completing Proposal Data Sheet are provided in italics in parenthesis for the relevant ITC clauses";
            clauseInstr = "Amendments of, and Supplements to, Clauses in the Instructions to Tenderers/Consultants";
        }else if(contentType.equalsIgnoreCase("GCC")){
            contentType1 = "PCC";
            instruction = "Instructions for completing the Particular Conditions of Contract are provided in italics in parenthesis for the relevant GCC Clauses.";
            clauseInstr = "Amendments of, and Supplements to, Clauses in the General Conditions of Contract";
        }
        List<SPTenderCommonData> tblTdsSubClause = prepareTDSSrBean.getTDSSubClause(ittHeaderId);
    %>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="../ckeditor/adapters/jquery.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
<!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
-->
        <style type="text/css">
            ul li {margin-left: 20px;}
        </style>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1">View <%if(contentType1.equals("TDS")){out.print("BDS");}else if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%></div>
                            </div>
                            <table width="100%" cellspacing="10"  class="tableView_1" >
                                <tr>
                                    <td  align="left">
                                    <a class="action-button-goback" href="DefineSTDInDtl.jsp?templateId=<%=request.getParameter("templateId")%>">
                                            Go Back to SBD Dashboard</a></td>
                                    <td align="right">
                                    <a class="action-button-goback" href="TDSDashBoard.jsp?templateId=<%=request.getParameter("templateId")%>&sectionId=<%=request.getParameter("sectionId") %>">
                                            Go Back to <%if(contentType1.equals("TDS")){out.print("BDS");}else if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Dashboard</a></td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellspacing="10" class="tableView_1">
                                <tr>
                                    <td align="left">
                                        <%--<img src="../resources/images/Dashboard/addIcn.png" width="16" height="16" class="linkIcon_1" />
                                        <a id="addClause" href="javascript:void(0);">Add New Clause</a> &nbsp;&nbsp;--%>
                                    </td>
                                </tr>
                            </table>
                            <form action="<%=request.getContextPath()%>/PrepareTDSSrBean?action=save" method="post" onsubmit="return validate();">
                                <input type="hidden" name="sectionId" id="sectionId" value="<%=request.getParameter("sectionId")%>" />
                                <input type="hidden" name="templateId" id="templateId" value="<%=request.getParameter("templateId")%>" />



                                        <table width="100%" cellspacing="0" id="tdsInfo" class="tableList_1 t_space">
                                            <tr>
                                                <td colspan="2"><%=instruction%></td>
                                            </tr>
                                            <tr>
                                                <td><b><%if(contentType.equals("ITT")){out.print("ITB");}else{out.print(contentType);}%> Clause</b></td>
                                                <td><%=clauseInstr%></td>
                                            </tr>
                                    <%
                                        //tblTdsSubClause
                                        String ittClauseId = "-1";
                                        /*
                                            ittHeaderId, ittClauseId, ittClauseName,
                                            ittsubclauseid, ittSubClauseName, tdsSubClauseName,
                                            tdsSubClauseId, ittHeaderName, tdsSubClauseId,
                                            orderNo
                                        */
                                        int k=1,i;
                                        if(tblTdsSubClause.size() > 0){
                                        %>
                                                <tr>
                                                    <td class="t-align-center" colspan="2"><b><%=tblTdsSubClause.get(0).getFieldName8()%></b></td>
                                                </tr>
                                        <%
                                        }
                                        for(i=0;i<tblTdsSubClause.size(); i++){
                                            %>
                                                <tr id="tr_<%=k++%>">
                                            <%
                                            if(!ittClauseId.equals(tblTdsSubClause.get(i).getFieldName2())){
                                                ittClauseId = tblTdsSubClause.get(i).getFieldName2();
                                                %>
                                                    <td><%if(contentType.equals("ITT")){out.print("ITB");}else{out.print(contentType);}%> Clause</td>
                                                    <td><%=tblTdsSubClause.get(i).getFieldName3()%></td>
                                                   
                                                <%
                                            }
                                            %>
                                                    
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td><%if(tblTdsSubClause.get(i).getFieldName5() != null)
                                                    {
                                                        out.print(tblTdsSubClause.get(i).getFieldName5());
                                                    }%>
                                                    </td></tr><tr>
                                                   <td>&nbsp;</td>
                                                   <td>
                                          <% if(tblTdsSubClause.get(i).getFieldName6() != null)
                                                    {
                                                out.print(tblTdsSubClause.get(i).getFieldName6());
                                            }
                                            if(tblTdsSubClause.get(i).getFieldName6() == null && tblTdsSubClause.get(i).getFieldName5() == null)
                                            {out.print("<i>Information Not Available</i>");}%></td>
                                                </tr>
                                            <%
                                        }
                                        %>
                                       
                                        </table>
                                        
                                        <input type="hidden" id="total" name ="total" value="<%=i%>" />
                                        
                            </form>

                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<%
    if(defineSTDInDtlSrBean != null){
        defineSTDInDtlSrBean = null;
    }
%>