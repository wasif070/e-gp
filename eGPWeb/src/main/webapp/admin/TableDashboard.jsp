<%--
    Document   : CreateForm
    Created on : 24-Oct-2010, 4:49:09 PM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="tableDashboard"  class="com.cptu.egp.eps.web.servicebean.TemplateTableSrBean" />
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Form Dashboard</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <%--<script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />--%>
        <script>
            function confirmDelete(){
                //jConfirm('Are you sure you want to delete this table?', 'Confirm', function(RetVal) {
                 //   if (RetVal) {
                  //      return true;
                   // }else{
                    //    return false;
                    //}
                //});
                if(confirm('Do you really want to delete this table?')){
                    return true;
                }else{
                    return false;
                }
            }
        </script>
    </head>
    <%
                short templateId = 0;
                int sectionId = 0;
                int formId = 0;

                if (request.getParameter("templateId") != null) {
                    templateId = Short.parseShort(request.getParameter("templateId"));
                }
                if (request.getParameter("sectionId") != null) {
                    sectionId = Integer.parseInt(request.getParameter("sectionId"));
                }
                if (request.getParameter("formId") != null) {
                    formId = Integer.parseInt(request.getParameter("formId"));
                }

                String logUserId="0";
                if(session.getAttribute("userId")!=null){
                    logUserId=session.getAttribute("userId").toString();
                }
                tableDashboard.setLogUserId(logUserId);
    %>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Middle Content Table Start-->
            <%
                        java.util.List<com.cptu.egp.eps.model.table.TblTemplateTables> tblTemplateTables = tableDashboard.getTemplateTables(templateId, sectionId, formId);
            %>
            <div class="contentArea_1">
            <div class="pageHead_1">
                Form Dashboard
                <span style="float: right; text-align: right;">
                    <a href="DefineSTDInDtl.jsp?templateId=<%= templateId %>" title="SBD Dashboard" class="action-button-goback">SBD Dashboard</a>
                </span>
            </div>
            <table width="100%" cellspacing="0" class="tableView_1 t_space">
                <tr>
                    <td align="left">
                        <a class="action-button-add" href="CreateFormTable.jsp?templateId=<%= templateId %>&sectionId=<%= sectionId %>&formId=<%= formId %>" onclick="addRow(document.getElementById('frmTableCreation'));" title="Add Row">Add Table</a>
                    </td>
                </tr>
            </table>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                <tr>
                    <th style="text-align:left;">Table Name</th>
                    <th style="text-align:left;">Action</th>
                </tr>
                <%
                            boolean isEntryInColumns = false;
                            for (int i = 0; i < tblTemplateTables.size(); i++) {
                                isEntryInColumns = tableDashboard.isEntryPresentForCols(tblTemplateTables.get(i).getTableId());
                %>
                <tr>
                    <td><%= tblTemplateTables.get(i).getTableName()%></td>
                    <td>
                        <a href="CreateFormTable.jsp?templateId=<%= templateId %>&sectionId=<%= sectionId %>&formId=<%= formId %>&tableId=<%= tblTemplateTables.get(i).getTableId() %>">Edit Table Details</a>
                        | <img src="../resources/images/Dashboard/removeIcn.png" alt="Delete Row" class="linkIcon_1" />
                        <a onclick="return confirmDelete();" href="<%=request.getContextPath()%>/CreateSTDForm?action=formTableDel&templateId=<%=templateId%>&sectionId=<%=sectionId%>&formId=<%=formId%>&tableId=<%=tblTemplateTables.get(i).getTableId()%>" title="Delete Table">Delete Table</a>
                        |
                        <% if(isEntryInColumns){ %>
                            <a href="TableMatrix.jsp?templateId=<%=templateId%>&sectionId=<%=sectionId%>&formId=<%=formId%>&tableId=<%=tblTemplateTables.get(i).getTableId()%>">Edit Table Matrix</a> |
                            <a href="ViewTableMatrix.jsp?templateId=<%=templateId%>&sectionId=<%=sectionId%>&formId=<%=formId%>&tableId=<%=tblTemplateTables.get(i).getTableId()%>">View Table Matrix</a>
                            <%
                                if(tableDashboard.isAutoColumnPresent(tblTemplateTables.get(i).getTableId())){
                                    if(tableDashboard.atLeastOneFormulaCreated(tblTemplateTables.get(i).getTableId())){
                            %>
                                | <a href="CreateFormula.jsp?templateId=<%=templateId%>&sectionId=<%=sectionId%>&formId=<%=formId%>&tableId=<%=tblTemplateTables.get(i).getTableId()%>">Edit Formula</a>
                            <%
                                    }else{
                            %>
                                | <a href="CreateFormula.jsp?templateId=<%=templateId%>&sectionId=<%=sectionId%>&formId=<%=formId%>&tableId=<%=tblTemplateTables.get(i).getTableId()%>">Create Formula</a>
                            <%
                                    }
                                }
                            %>
                        <%}else{%>
                            <img src="../resources/images/Dashboard/addIcn.png" alt="Create Table Matrix" class="linkIcon_1" />
                            <a href="TableMatrix.jsp?templateId=<%=templateId%>&sectionId=<%=sectionId%>&formId=<%=formId%>&tableId=<%=tblTemplateTables.get(i).getTableId()%>" title="Create Table Matrix">Create Table Matrix</a>
                        <%}%>
                        
                    </td>
                </tr>
                <%
                            }
                %>
            </table>
            </div>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
    <%
                if (tblTemplateTables != null) {
                    tblTemplateTables = null;
                }
                if (tableDashboard != null) {
                    tableDashboard = null;
                }
    %>
</html>