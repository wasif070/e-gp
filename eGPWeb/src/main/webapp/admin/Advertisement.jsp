<%--
    Document   : Advertisement
    Created on : jan 24, 2013, 7:55:54 PM
    Author     : sambasiva.sugguna
--%>

<%@page import="com.cptu.egp.eps.model.table.TblAdvtConfigurationMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblAdvertisement"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigureCalendarSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblHolidayMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Advertisement Configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" ></jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="pageHead_1">Advertisement Configuration</div>
                            <div>&nbsp;</div>
                            <div><span id="remmsg" ></span></div>
                            <table width="100%" cellspacing="1" class="tableList_1 t_space" id ="tableListDetail">
                                <%
                                            String message = "";
                                            message = request.getParameter("msg");
                                            if (!"".equalsIgnoreCase(request.getParameter(message))) {
                                                if ("editsucc".equalsIgnoreCase(message)) {
                                %>
                                <div class="responseMsg successMsg t_space"><span>Advertisement Method updated successfully</span></div><br/>
                                <%                                            }
                                            }
                                %>
                                <tr>
                                    <th>S.No</th>
                                    <th>Location</th>
                                    <th>Period</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                                <% ArrayList<TblAdvtConfigurationMaster> cont = (ArrayList) request.getAttribute("items");
                                int i=0;
                                String styleclass="";
                                for (TblAdvtConfigurationMaster tbl : cont) {
                                    i++;
                                                if(i%2==0)
                                                styleclass="background-color:#E9F9DB";
                                            else
                                                styleclass="";
                                %>
                                <tr style="<%=styleclass%>">
                                    <td><%=tbl.getAdConfigId()%></td>
                                    <td><%=tbl.getLocation()%></td>
                                    <td><%=tbl.getDuration()%></td>
                                    <td><%=tbl.getFee()%></td>
                                    <td><a href="/AdvertisementConfig?funct=edit&adConfigId=<%=tbl.getAdConfigId()%>">Configure Price</a></td>
                                </tr>
                                <%}%>
                            </table>
                        </td>
                    </tr>
                </table>
                <div>&nbsp;</div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
