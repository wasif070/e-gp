<%-- 
    Document   : DocConfigurationView
    Created on : Jan 11, 2011, 12:19:26 PM
    Author     : Naresh.Akurathi
--%>



<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigMasterSrBean"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
    %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Document Management Configuration View</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

    <%!
        ConfigMasterSrBean configMasterSrBean = new ConfigMasterSrBean();
    %>
    <%
        configMasterSrBean.setLogUserId(session.getAttribute("userId").toString()); 
    %>


</head>
<body>

    <% String mesg = request.getParameter("msg");%>

<div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
                        <!--Dashboard Content Part Start-->
  <div class="pageHead_1">Document Management Configuration View</div>
  <div>&nbsp;</div>
  <%if(mesg != null){%>
  <div class="responseMsg successMsg"><%=mesg%></div>
  <%}%>

  <div>&nbsp;</div>
  <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tblstd" name="tblstd">
     <tr>
        <th>Permissible File Types</th>
        <th>Type of User</th>
        <th>Single File Size (In MB)</th>
        <th>Total File Size (In MB)</th>
        <th>Action</th>
     </tr>

     <%
            String msg ="";
            List<TblConfigurationMaster> lst = configMasterSrBean.getAllConfigMaster();
            if(!lst.isEmpty()){
                Iterator it = lst.iterator();
                int i=0;
                String userName = null;
                while(it.hasNext())
                    {
                        TblConfigurationMaster tblConfigMaster = (TblConfigurationMaster)it.next();
                        i++;

     %>

      <%if(i%2==0){%>
      <tr style='background-color:#F0E68C;'>
          <%}else{%>
          <tr>
          <%}%>
	<td class="t-align-center"><%= tblConfigMaster.getAllowedExtension()%></td>
        <%
            if(tblConfigMaster.getUserType().toString().equalsIgnoreCase("tenderer")){
                userName = "Bidder/Consultant";
            }
             else if(tblConfigMaster.getUserType().toString().equalsIgnoreCase("Officer")){
                userName = "Officer";
            }
                         else if(tblConfigMaster.getUserType().toString().equalsIgnoreCase("egpadmin")){
                userName = "e-GP Admin";
            }else{
                userName = "Common";
            }

        %>
        <%--<td class="t-align-center"><%= tblConfigMaster.getUserType()%></td>--%>
        <td class="t-align-center"><%=userName%></td>
        <td class="t-align-center"><%= tblConfigMaster.getFileSize()%></td>
        <td class="t-align-center"><%= tblConfigMaster.getTotalSize()%></td>
        <td class="t-align-center"><a href="DocConfiguration.jsp?edit=true&cId=<%=tblConfigMaster.getConfigId()%>">Edit</a></td>
        
          </tr></tr>

     <%
                    }
             }else
                 msg = "No Record Found";
     %>

</table>
   <div align="center"> <%=msg%></div>

                    </td>
                </tr>
            </table>

  <div>&nbsp;</div>

   <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
</div>
<script>
                var obj = document.getElementById('lblConfigDocView');
                if(obj != null){
                    if(obj.innerHTML == 'View'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
</body>
</html>


