<%-- 
    Document   : EditJvcaDetails
    Created on : Oct 23, 2010, 7:51:47 PM
    Author     : parag
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>JVCA Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="Include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="../resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <!--Page Content Start-->
                        <td class="contentArea_1"><div class="pageHead_1">Edit User Registration - JVCA Details</div>
                            <form action=""  id="frmJvcaDetails" method="post">
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">                                    
                                    <tr>
                                        <td class="ff">Company Name : <span>*</span></td>
                                        <td><input name="textfield5" type="text" class="formTxtBox_1" id="txtComName" style="width:200px;" readonly="true" /></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Company Registration No. : <span>*</span></td>
                                        <td><input name="textfield51" type="text" class="formTxtBox_1" id="txtComRegNo" style="width:200px;" readonly="true" /></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">JVCA Role : <span>*</span></td>
                                        <td><input name="textfield52" type="text" class="formTxtBox_1" id="txtJVCARole" style="width:200px;" readonly="true" /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><img src="Images/Dashboard/addIcn.png" width="16" height="16" class="linkIcon_1" /><a href="#">Add More Company</a></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><label class="formBtn_1">
                                                <input type="submit" name="btnSubmit" id="btnSubmit" value="Previous" />
                                            </label>
                                            &nbsp;
                                            <label class="formBtn_1">
                                                <input type="submit" name="btnSave" id="btnSave" value="Save" />
                                            </label>
                                            &nbsp;
                                            <label class="formBtn_1">
                                                <input type="submit" name="btnNext" id="btnNext" value="Next" />
                                            </label></td>
                                    </tr>
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
