<%-- 
    Document   : TSCFormationRuleDetails
    Created on : Nov 19, 2010, 1:21:13 PM
    Author     : 
--%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%
    String strUserTypeId = "";
    Object objUserId = session.getAttribute("userId");
    Object objUName = session.getAttribute("userName");
    boolean isLoggedIn = false;
    if (objUserId != null) {
        strUserTypeId = session.getAttribute("userId").toString();
    }
    if (objUName != null) {
        isLoggedIn = true;
    }
%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.lowagie.tools.split_pdf"%>
<%@page import="java.util.List"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigTec"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>TC Formation Business Rules Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <%!    ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();

        %>


        <%
            StringBuilder tenderType = new StringBuilder();
            StringBuilder procNature = new StringBuilder();
            configPreTenderRuleSrBean.setLogUserId(strUserTypeId);
            TblProcurementNature tblProcureNature2 = new TblProcurementNature();
            Iterator pnit2 = configPreTenderRuleSrBean.getProcurementNature().iterator();
            while (pnit2.hasNext()) {
                tblProcureNature2 = (TblProcurementNature) pnit2.next();
                procNature.append("<option value='" + tblProcureNature2.getProcurementNatureId() + "'>" + tblProcureNature2.getProcurementNature() + "</option>");
            }


        %>



    </head>
    <body > <!--onload="convert()"-->

        <%  String msg = "";
            int natureID = 0;
            if ("Submit".equals(request.getParameter("button")) || "Update".equals(request.getParameter("button"))) {
                int row = Integer.parseInt(request.getParameter("TotRule"));

                if ("edit".equals(request.getParameter("action"))) {
                    msg = "Not Deleted";
                } else {
                    msg = configPreTenderRuleSrBean.delAllConfigTec("TC,PC");
                }

                if (msg.equals("Deleted")) {
                    String action = "Add TC Formation Rules";
                    try {
                        for (int i = 1; i <= row; i++) {
                            if (request.getParameter("procNature" + i) != null) 
                            {
                                //Nitish Start:: 
                                byte pnature = 0;
                                String test = "";
                                test = (String) (request.getParameter("procNature" + i));
                                if (test.equals("Works")) 
                                {
                                    pnature = 2;
                                } 
                                else if (test.equals("Services")) 
                                {
                                    pnature = 3;
                                }
                                else if (test.equals("Goods")) 
                                {
                                    pnature = 1;
                                }
                                //Nitish END:: 

                                //byte pnature = Byte.parseByte(request.getParameter("procNature" + i));
                                float minTender = Float.parseFloat(request.getParameter("minTender" + i));
                                float maxTender = Float.parseFloat(request.getParameter("maxTender" + i));
                                byte minmem = Byte.parseByte(request.getParameter("minMemb" + i));
                                byte maxmem = Byte.parseByte(request.getParameter("maxMemb" + i));
                                byte minoutsidepr = Byte.parseByte(request.getParameter("minMemOutsidePe" + i));
                                byte minmemFromPe = Byte.parseByte(request.getParameter("minMemFromPe" + i));
                                byte minmemformtec = Byte.parseByte(request.getParameter("minMemFromTEC" + i));
                                TblConfigTec configTec = new TblConfigTec();
                                //configTec.setCommitteeType(request.getParameter("commitType" + i));
                                if (pnature == 3) {
                                    configTec.setCommitteeType("PC");
                                } else {
                                    configTec.setCommitteeType("TC");
                                }
                                configTec.setTblProcurementNature(new TblProcurementNature(pnature));
                                configTec.setMinTenderVal(new BigDecimal(request.getParameter("minTender" + i)));
                                configTec.setMaxTenderVal(new BigDecimal(request.getParameter("maxTender" + i)));
                                configTec.setMinMemReq(minmem);
                                configTec.setMaxMemReq(maxmem);
                                configTec.setMinMemOutSidePe(minoutsidepr);
                                configTec.setMinMemFromPe(minmemFromPe);
                                configTec.setMinMemFromTec(minmemformtec);

                                msg = configPreTenderRuleSrBean.addConfigTec(configTec);
                            }
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                        action = "Error in : " + action + " : " + e;
                    } finally {
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                        action = null;
                    }
                } else {
                    String action = "Edit TC Formation Rules";
                    try {
                        for (int i = 1; i <= row; i++) {
                            if (request.getParameter("procNature" + i) != null) 
                            {
                                //Nitish Start:: 
                                byte pnature = 0;
                                String test = (String) (request.getParameter("procNature" + i));
                                if (test.equals("Works")) 
                                {
                                    pnature = 2;
                                }
                                else if (test.equals("Services")) 
                                {
                                    pnature = 3;
                                }
                                else if (test.equals("Goods")) 
                                {
                                    pnature = 1;
                                }
                                //Nitish END:: 

                                //byte pnature = Byte.parseByte(request.getParameter("procNature" + i));
                                float minTender = Float.parseFloat(request.getParameter("minTender" + i));
                                float maxTender = Float.parseFloat(request.getParameter("maxTender" + i));
                                byte minmem = Byte.parseByte(request.getParameter("minMemb" + i));
                                byte maxmem = Byte.parseByte(request.getParameter("maxMemb" + i));
                                byte minoutsidepr = Byte.parseByte(request.getParameter("minMemOutsidePe" + i));
                                byte minmemFromPe = Byte.parseByte(request.getParameter("minMemFromPe" + i));
                                byte minmemformtec = Byte.parseByte(request.getParameter("minMemFromTEC" + i));
                                TblConfigTec configTec = new TblConfigTec();
                                configTec.setConfigTec(Integer.parseInt(request.getParameter("id")));
                                configTec.setCommitteeType(request.getParameter("commitType" + i));
                                configTec.setTblProcurementNature(new TblProcurementNature(pnature));
                                configTec.setMinTenderVal(new BigDecimal(request.getParameter("minTender" + i)));
                                configTec.setMaxTenderVal(new BigDecimal(request.getParameter("maxTender" + i)));
                                configTec.setMinMemReq(minmem);
                                configTec.setMaxMemReq(maxmem);
                                configTec.setMinMemOutSidePe(minoutsidepr);
                                configTec.setMinMemFromPe(minmemFromPe);
                                configTec.setMinMemFromTec(minmemformtec);

                                msg = configPreTenderRuleSrBean.updateConfigTec(configTec);
                            }
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                        action = "Error in : " + action + " : " + e;
                    } finally {
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                        action = null;
                    }
                }

                if (msg.equals("Values Added")) {
                    msg = "TC Business Rule configured successfully";
                    response.sendRedirect("TSCFormationRuleView.jsp?msg=" + msg);
                }

                if (msg.equals("Updated")) {
                    msg = "TC Business Rule Updated successfully";
                    response.sendRedirect("TSCFormationRuleView.jsp?msg=" + msg);
                }
            }


        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                        <td class="contentArea">
                            <!--Dashboard Header End-->
                            <!--Dashboard Content Part Start-->
                            <div class="pageHead_1">TC Formation Business Rules Configuration</div>


                            <div style="font-style: italic" class="t-align-right t_space"><normal>Fields marked with (<span class="mandatory">*</span>) are mandatory</normal></div>
                                <%                            if (request.getParameter("id") == null) {

                                %>
                            <%--Commit By Nitish--%>
                                <%--<div align="right" class="t_space b_space">
                                    <span id="lotMsg" style="color: red; font-weight: bold; float: left; visibility: collapse">&nbsp;</span>
                                    <a id="linkAddRule" class="action-button-add">Add Rule</a> <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a><br/>

                        </div>--%>
                            <%--Commit By Nitish--%>
                                <%}%>
                        <div>&nbsp;</div>
                        <%--<div align="center"> <%=msg%></div> --%>
                        <form action="TSCFormationRuleDetails.jsp" method="post">
                            <table width="100%" cellspacing="0" class="tableList_1" id="tblrule" name="tblrule">
                                <tr>
                                    <%--<th width="5%" >Select</th>--%>
                                    <th width="8%" >Committee Type</th>
                                    <th width="8%" >Procurement Category</th>

                                    <!--In TC formation rules, maximum and minimum values are irrelevant currently (24-01-2017) -->
                                    <!--<th width="10%" >Min.  Tender  <br />value (In Nu.)<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%" >Min.  Tender  <br />value (in Nu.)<br />In Words<br /><span class="mandatory"></span></th>
                                    <th width="10%" >Max. Tender <br />value (In Nu.)<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%" >Max. Tender <br />value (in Nu.)<br />In Words<br /><span class="mandatory"></span></th>-->
                                    <th width="10%" >Min. Member<br />Required<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%" >Max. Member<br /> Required<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%" >Min. Members <br />Outside PA(<span class="mandatory">*</span>)</th>
                                    <th width="10%" >Min. Member <br />required from PA(<span class="mandatory">*</span>) </th>
                                    <th width="10%"  style="display: none;">Min. Member <br />required from TEC  <br />(<span class="mandatory">*</span>)</th>
                                </tr>
                                <input type="hidden" name="delrow" value="0" id="delrow"/>
                                <input type="hidden" name="introw" value="" id="introw"/>
                                <%
                                    String procureNature = null;
                                    List lt = null;

                                    if (request.getParameter("id") != null && "edit".equals(request.getParameter("action"))) {
                                        String action = request.getParameter("action");
                                        int id = Integer.parseInt(request.getParameter("id"));
                                        if(request.getParameter("natureId") != null)
                                        {
                                            natureID = Integer.parseInt(request.getParameter("natureId"));
                                        }
                                %>
                                <input type="hidden" id="hiddenaction" name="action" value="<%=action%>"/>
                                <input type="hidden" id="hiddenid"name="id" value="<%=id%>"/>
                                <%

                                        lt = configPreTenderRuleSrBean.getConfigTec(id);
                                    } else
                                        lt = configPreTenderRuleSrBean.getCommiteeTypeData("TC,PC");

                                    int i = 0;
                                    int length = 0;
                                    if (!lt.isEmpty()) {
                                        List getProcNature = configPreTenderRuleSrBean.getProcurementNature();
                                        TblConfigTec tblconfigtec = new TblConfigTec();
                                        Iterator it = lt.iterator();
                                        String minTenderVal = "";
                                        String maxTenderVal = "";
                                        while (it.hasNext()) {
                                            i++;
                                            length++;
                                            StringBuilder pNature = new StringBuilder();
                                            StringBuilder commitetype = new StringBuilder();
                                            tblconfigtec = (TblConfigTec) it.next();

                                            TblProcurementNature tblProcureNature = new TblProcurementNature();
                                            Iterator pnit = getProcNature.iterator();
                                            while (pnit.hasNext()) {
                                                tblProcureNature = (TblProcurementNature) pnit.next();

                                                pNature.append("<option value='");
                                                if (tblProcureNature.getProcurementNatureId() == tblconfigtec.getTblProcurementNature().getProcurementNatureId()) {

                                                    pNature.append(tblProcureNature.getProcurementNatureId() + "' selected='true'>" + tblProcureNature.getProcurementNature());

                                                } else {
                                                    pNature.append(tblProcureNature.getProcurementNatureId() + "'>" + tblProcureNature.getProcurementNature());
                                                }
                                                pNature.append("</option>");

                                            }

                                            if ("TC".equals(tblconfigtec.getCommitteeType())) {
                                                commitetype.append("<option value='TC' selected='true'>TC</option>");
                                            } else {
                                                commitetype.append("<option value='PC' selected='true'>TC</option>");
                                            }

                                            minTenderVal = new DecimalFormat("#.##").format(tblconfigtec.getMinTenderVal());
                                            maxTenderVal = new DecimalFormat("#.##").format(tblconfigtec.getMaxTenderVal());


                                %>

                                <tr id="trrule_<%=i%>">

                                    <%
                                        //Nitish Start
                                        if (i == 1) 
                                        {
                                            procureNature = "Works";
                                            if (natureID != 0) 
                                            {
                                                if (natureID == 1) 
                                                {
                                                    procureNature = "Goods";
                                                }
                                                if (natureID == 2) 
                                                {
                                                    procureNature = "Works";
                                                }
                                                if (natureID == 3) 
                                                {
                                                    procureNature = "Services";
                                                }
                                            }
                                        }
                                        else if (i == 2) 
                                        {
                                            procureNature = "Services";
                                        } 
                                        else if (i == 3) 
                                        {
                                            procureNature = "Goods";
                                        }
                                        //Nitish End

                                    %>   


                                    <%--<td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_<%=i%>" value="" /></td> --%>
                                    <td class="t-align-center"><input name="commitType<%=i%>" type="text" class="formTxtBox_1" id="commitType_<%=i%>" value="TC" readonly="readonly" />
                                        <%--Changed by Nitish:: input feild is readonly from select --%>
                                    </td>
                                    <td class="t-align-center"><input name="procNature<%=i%>" class="formTxtBox_1" id="procNature_<%=i%>" value="<%=procureNature%>" readonly="readonly"/>

                                    </td>

                                    <!--In TC formation rules, maximum and minimum values are irrelevant currently (24-01-2017) -->
                                    <input type="hidden" name="minTender<%=i%>" type="text" class="formTxtBox_1" id="minTender_<%=i%>" style="width:90%;" value="<%=minTenderVal%>" onblur="return chkMinTenderBlank(this);"/><span id="mint_<%=i%>" style="color: red;">&nbsp;</span>
                                    <span type="hidden" id="mintwords_<%=i%>" >&nbsp;</span>
                                    <input type="hidden" name="maxTender<%=i%>" type="text" class="formTxtBox_1" id="maxTender_<%=i%>" style="width:90%;" value="<%=maxTenderVal%>" onblur="return chkMaxTenderBlank(this);"/><span id="maxt_<%=i%>" style="color: red;">&nbsp;</span>
                                    <span type="hidden" id="maxtwords_<%=i%>" >&nbsp;</span>
                                    <td class="t-align-center"><input name="minMemb<%=i%>" type="text" class="formTxtBox_1" id="minMemb_<%=i%>" style="width:90%;" value="<%=tblconfigtec.getMinMemReq()%>" onblur="return chkMinMemberBlank(this);" maxlength="2"/><span id="minm_<%=i%>" style="color: red;">&nbsp;</span></td>
                                    <td class="t-align-center"><input name="maxMemb<%=i%>" type="text" class="formTxtBox_1" id="maxMemb_<%=i%>" style="width:90%;" value="<%=tblconfigtec.getMaxMemReq()%>" onblur="return chkMaxMemberBlank(this);" maxlength="2"/><span id="maxm_<%=i%>" style="color: red;">&nbsp;</span></td>
                                    <td class="t-align-center"><input name="minMemOutsidePe<%=i%>" type="text" class="formTxtBox_1" id="minMemOutsidePe_<%=i%>" style="width:90%;" value="<%=tblconfigtec.getMinMemOutSidePe()%>" onblur="return chkMinMemOutPEBlank(this);" maxlength="2"/><span id="minmop_<%=i%>" style="color: red;">&nbsp;</span></td>
                                    <td class="t-align-center"><input name="minMemFromPe<%=i%>" type="text" class="formTxtBox_1" id="minMemFromPe_<%=i%>" style="width:90%;" value="<%=tblconfigtec.getMinMemFromPe()%>" onblur="return chkMinMemFromPEBlank(this);" maxlength="2"/><span id="minmfp_<%=i%>" style="color: red;">&nbsp;</span></td>
                                    <td class="t-align-center" style="display: none;"><input name="minMemFromTEC<%=i%>" type="text" class="formTxtBox_1" id="minMemFromTEC_<%=i%>" style="width:90%; display: none;" value="0" readonly="readonly"/></td>

                                </tr>
                                <%

                                        }

                                    } else
                                        msg = "No Record Found";
                                %>

                            </table>
                            <div>&nbsp;</div>
                            <input type="hidden" name="TotRule" id="TotRule" value="<%=i%>"/>

                            <table width="100%" cellspacing="0" >
                                <tr>
                                    <%if ("edit".equals(request.getParameter("action"))) {%>
                                    <td colspan="11" class="t-align-center"><span class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Update" onclick="return validate();"/>
                                        </span></td>
                                        <%} else {%>
                                    <td colspan="11" class="t-align-center"><span class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Submit" onclick="return validate();"/>
                                        </span></td>
                                        <%}%>
                                </tr>
                            </table>

                            <%--<div align="center"> <%=msg%></div>--%>
                        </form>
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
            <script>
                var obj = document.getElementById('lblTSCFormationRuleEdit');
                if (obj != null) {
                    if (obj.innerHTML == 'Edit') {
                        obj.setAttribute('class', 'selected');
                    }
                }

            </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if (headSel_Obj != null) {
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            <script type="text/javascript">
                function convert()
                {
                    for (var k = 1; k <=<%= length%>; k++)
                    {
                        //document.getElementById("mintwords_"+k).innerHTML = DoIt(document.getElementById("minTender_"+k).value);
                        //document.getElementById("maxtwords_"+k).innerHTML = DoIt(document.getElementById("maxTender_"+k).value);
                        document.getElementById("mintwords_" + k).innerHTML = CurrencyConverter(document.getElementById("minTender_" + k).value);
                        document.getElementById("maxtwords_" + k).innerHTML = CurrencyConverter(document.getElementById("maxTender_" + k).value);
                    }
                }
        </script>
    </body>
    <script type="text/javascript">

        var counter = 1;
        var delCnt = 0;
        $(function () {
            $('#linkAddRule').click(function () {
                counter = eval(document.getElementById("TotRule").value);
                delCnt = eval(document.getElementById("delrow").value);

                $('span.#lotMsg').css("visibility", "collapse");

                //In TC formation rules, maximum and minimum values are irrelevant currently (24-01-2017)
                var htmlEle = "<tr id='trrule_" + (counter + 1) + "'>" +
                        "<td class='t-align-center'><input type='checkbox' name='checkbox" + (counter + 1) + "' id='checkbox_" + (counter + 1) + "' value='" + (counter + 1) + "' style='width:90%;'/></td>" +
                        "<td class='t-align-center'><select name='commitType" + (counter + 1) + "' class='formTxtBox_1' id='commitType_" + (counter + 1) + "' ><option value='TC'>TC</option></select></td>" +
                        "<td class='t-align-center'><select name='procNature" + (counter + 1) + "' class='formTxtBox_1' id='procNature_" + (counter + 1) + "'><%=procNature%></select></td>" +
                        "<td  style='display:none;' class='t-align-center'><input type='hidden' name='minTender" + (counter + 1) + "' class='formTxtBox_1' id='minTender_" + (counter + 1) + "' value='0' style='width:90%;' onblur='return chkMinTenderBlank(this);'/><span id='mint_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                        "<span type='hidden' id='mintwords_" + (counter + 1) + "' >&nbsp;</span>" +
                        "<td  style='display:none;' class='t-align-center'><input type='hidden' name='maxTender" + (counter + 1) + "' class='formTxtBox_1' id='maxTender_" + (counter + 1) + "' value='0' style='width:90%;' onblur='return chkMaxTenderBlank(this);'/><span id='maxt_" + (counter + 1) + "'' style='color: red;'>&nbsp;</span></td>" +
                        "<span type='hidden' id='maxtwords_" + (counter + 1) + "'' >&nbsp;</span>" +
                        "<td  class='t-align-center'><input type='text' name='minMemb" + (counter + 1) + "' class='formTxtBox_1' id='minMemb_" + (counter + 1) + "' style='width:90%;' onblur='return chkMinMemberBlank(this);' maxlength='2'/><span id='minm_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                        "<td  class='t-align-center'><input type='text' name='maxMemb" + (counter + 1) + "' class='formTxtBox_1' id='maxMemb_" + (counter + 1) + "' style='width:90%;' onblur='return chkMaxMemberBlank(this);' maxlength='2'/><span id='maxm_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                        "<td  class='t-align-center'><input type='text' name='minMemOutsidePe" + (counter + 1) + "' class='formTxtBox_1' id='minMemOutsidePe_" + (counter + 1) + "' style='width:90%;' onblur='return chkMinMemOutPEBlank(this);' maxlength='2'/><span id='minmop_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                        "<td  class='t-align-center'><input type='text' name='minMemFromPe" + (counter + 1) + "' class='formTxtBox_1' id='minMemFromPe_" + (counter + 1) + "' style='width:90%;' onblur='return chkMinMemFromPEBlank(this);' maxlength='2'/><span id='minmfp_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                        "<td  class='t-align-center' style='display: none;'><input type='text' name='minMemFromTEC" + (counter + 1) + "' class='formTxtBox_1' id='minMemFromTEC_" + (counter + 1) + "' value='0' style='width:90%;' onblur='return chkMinMemTECBlank(this);'/><span id='minmt_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                        "</tr>";
                $("#tblrule").append(htmlEle);
                document.getElementById("TotRule").value = (counter + 1);

            });
        });

        $(function () {
            $('#linkDelRule').click(function () {
                counter = eval(document.getElementById("TotRule").value);
                delCnt = eval(document.getElementById("delrow").value);

                var tmpCnt = 0;
                for (var i = 1; i <= counter; i++) {
                    if (document.getElementById("checkbox_" + i) != null && document.getElementById("checkbox_" + i).checked) {
                        tmpCnt++;
                    }
                }

                if (tmpCnt == (eval(counter - delCnt))) {
                    $('span.#lotMsg').css("visibility", "visible");
                    $('span.#lotMsg').css("color", "red");
                    $('span.#lotMsg').html('Minimum 1 record is needed!');
                } else {
                    if (tmpCnt > 0) {
                        jConfirm('Are You Sure You want to Delete.', 'Procurement Method Business Rule Configuration', function (ans) {
                            if (ans) {
                                for (var i = 1; i <= counter; i++) {
                                    if (document.getElementById("checkbox_" + i) != null && document.getElementById("checkbox_" + i).checked) {
                                        $("tr[id='trrule_" + i + "']").remove();
                                        $('span.#lotMsg').css("visibility", "collapse");
                                        delCnt++;
                                    }
                                }
                                document.getElementById("delrow").value = delCnt;
                            }
                        });
                    } else {
                        jAlert("please Select checkbox first", "Procurement Method Business Rule Configuration", function (RetVal) {});
                    }
                }

            });
        });
    </script>
    <script type="text/javascript">
        function validate()
        {
            var flag = true;
            var counter = eval(document.getElementById("TotRule").value);
            if (document.getElementById("hiddenaction"))
            {
                if ("edit" == document.getElementById("hiddenaction").value)
                {
                    //Nitish Start
                    var nature = $('#procNature_1').val();
                    var natureId;

                    if (nature == "Goods")
                    {
                        natureId = 1;
                    }
                    if (nature == "Works")
                    {
                        natureId = 2;
                    }
                    if (nature == "Services")
                    {
                        natureId = 3;
                    }
                    //nitish END
                    $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/BusinessRuleConfigurationServlet",
                        data: "configtecId=" + $('#hiddenid').val() + "&" + "commitType=" + $('#commitType_1').val() + "&" + "procNature=" + $('#procNature_1').val() + "&" + "minvalue=" + $('#minTender_1').val() + "&" + "maxvalue=" + $('#maxTender_1').val() + "&" + "funName=TCajaxUniqueConfig",
                        async: false,
                        success: function (j) {

                            if (j.toString() != "0") {
                                flag = false;
                                jAlert("Combination of 'Committee Type' and 'Procurement Category' already exist.", "TC Formation Business Rule Configuration", function (RetVal) {
                                });
                            }
                        }
                    });
                }
            }
            for (var k = 1; k <= counter; k++)
            {
                if (document.getElementById("minTender_" + k) != null)
                {
                    if (document.getElementById("minTender_" + k).value == "") {
                        document.getElementById("mint_" + k).innerHTML = "<br/>Please enter Min Tender Value (In Nu.)";
                        flag = false;
                    } else
                    {
                        if (decimal(document.getElementById("minTender_" + k).value))
                        {
                            if (chkMax(document.getElementById("minTender_" + k).value))
                            {
                                document.getElementById("mint_" + k).innerHTML = "";
                            } else
                            {
                                document.getElementById("mint_" + k).innerHTML = "<br/>Maximum 12 numbers are allowed";
                                flag = false;
                            }
                        } else
                        {
                            document.getElementById("mint_" + k).innerHTML = "<br/>Please Enter numbers and 2 Digits After Decimal";
                            flag = false;
                        }
                    }
                }
                if (document.getElementById("maxTender_" + k) != null)
                {
                    if (document.getElementById("maxTender_" + k).value == "")
                    {
                        document.getElementById("maxt_" + k).innerHTML = "<br/>Please enter Max Tender Value (In Nu.)";
                        flag = false;
                    } else
                    {
                        if (decimal(document.getElementById("maxTender_" + k).value))
                        {
                            if (chkMax(document.getElementById("maxTender_" + k).value))
                            {
                                document.getElementById("maxt_" + k).innerHTML = "";
                            } else
                            {
                                document.getElementById("maxt_" + k).innerHTML = "<br/>Maximum 12 numbers are allowed";
                                flag = false;
                            }
                        } else
                        {
                            document.getElementById("maxt_" + k).innerHTML = "<br/>Please Enter numbers and 2 Digits After Decimal";
                            flag = false;
                        }
                    }
                }
                if (document.getElementById("minMemb_" + k) != null)
                {
                    if (document.getElementById("minMemb_" + k).value == "")
                    {
                        document.getElementById("minm_" + k).innerHTML = "<br/>Please enter Min. Members Required";
                        flag = false;
                    } else
                    {
                        if (numeric(document.getElementById("minMemb_" + k).value))
                        {
                            if (!numberZero(document.getElementById("minMemb_" + k).value))
                            {
                                document.getElementById("minm_" + k).innerHTML = "<br/>Please enter numbers between (1-9) only";
                                flag = false;
                            } else
                            {
                                document.getElementById("minm_" + k).innerHTML = "";
                            }
                        } else
                        {
                            document.getElementById("minm_" + k).innerHTML = "<br/>Please Enter numbers only";
                            flag = false;
                        }
                    }
                }
                if (document.getElementById("maxMemb_" + k) != null)
                {
                    if (document.getElementById("maxMemb_" + k).value == "")
                    {
                        document.getElementById("maxm_" + k).innerHTML = "<br/>Please enter Max. Members Required";
                        flag = false;
                    } else
                    {
                        if (numeric(document.getElementById("maxMemb_" + k).value))
                        {
                            document.getElementById("maxm_" + k).innerHTML = "";
                        } else
                        {
                            document.getElementById("maxm_" + k).innerHTML = "<br/>Please Enter numbers only";
                            flag = false;
                        }

                    }
                }
                if (document.getElementById("minMemOutsidePe_" + k) != null)
                {
                    if (document.getElementById("minMemOutsidePe_" + k).value == "") {
                        document.getElementById("minmop_" + k).innerHTML = "<br/>Please enter Min. Members required from Outside PA";
                        flag = false;
                    } else
                    {
                        if (numeric(document.getElementById("minMemOutsidePe_" + k).value))
                        {
                            document.getElementById("minmop_" + k).innerHTML = "";
                        } else
                        {
                            document.getElementById("minmop_" + k).innerHTML = "<br/>Please Enter numbers only";
                            flag = false;
                        }
                    }
                }
                if (document.getElementById("minMemFromPe_" + k) != null)
                {
                    if (document.getElementById("minMemFromPe_" + k).value == "")
                    {
                        document.getElementById("minmfp_" + k).innerHTML = "<br/>Please enter Min. Members required from PA";
                        flag = false;
                    } else
                    {
                        if (numeric(document.getElementById("minMemFromPe_" + k).value))
                        {
                            document.getElementById("minmfp_" + k).innerHTML = "";
                        } else
                        {
                            document.getElementById("minmfp_" + k).innerHTML = "<br/>Please Enter numbers only";
                            flag = false;
                        }
                    }
                }
            }

            // Start OF Dhruti--Checking for Unique Rows

            if (document.getElementById("TotRule") != null) {
                var totalcount = eval(document.getElementById("TotRule").value);
            } //Total Count After Adding Rows
            //alert(totalcount);
            var chk = true;
            var i = 0;
            var j = 0;
            for (i = 1; i <= totalcount; i++) //Loop For Newly Added Rows
            {
                if (document.getElementById("minTender_" + i) != null)
                    if ($.trim(document.getElementById("minTender_" + i).value) != '' && $.trim(document.getElementById("maxTender_" + i).value) != '' && $.trim(document.getElementById("minMemb_" + i).value) != '' && $.trim(document.getElementById("maxMemb_" + i).value) != '' && $.trim(document.getElementById("minMemOutsidePe_" + i).value) != '' && $.trim(document.getElementById("minMemFromPe_" + i).value) != '') // If Condition for When all Data are filled thiscode is Running
                    {

                        for (j = 1; j <= totalcount && j != i; j++) // Loop for Total Count but Not same as (i) value
                        {
                            chk = true;
                            //If Condition for Check Duplicate Rows are there or not.If Columns are diff then chk variable set to false
                            //IF Row is same Give alert message.
//                            if($.trim(document.getElementById("commitType_"+i).value) != $.trim(document.getElementById("commitType_"+j).value))
//                            {
//                                chk=false;
//                            }
                            if ($.trim(document.getElementById("procNature_" + i).value) != $.trim(document.getElementById("procNature_" + j).value))
                            {
                                chk = false;
                            }
                            if ($.trim(document.getElementById("minTender_" + i).value) != $.trim(document.getElementById("minTender_" + j).value))
                            {
                                chk = false;
                            }
                            if ($.trim(document.getElementById("maxTender_" + i).value) != $.trim(document.getElementById("maxTender_" + j).value))
                            {
                                chk = false;
                            }
                            /*   var minti=document.getElementById("minTender_"+i).value;
                             var mintj=document.getElementById("minTender_"+j).value;
                             if(minti.indexOf(".")==-1)
                             {
                             minti=minti+".00";
                             }
                             if(mintj.indexOf(".")==-1)
                             {
                             mintj=mintj+".00";
                             }
                             if($.trim(minti) != $.trim(mintj))
                             {
                             chk=false;
                             }
                             var maxti=document.getElementById("maxTender_"+i).value;
                             var maxtj=document.getElementById("maxTender_"+j).value;
                             if(maxti.indexOf(".")==-1)
                             {
                             maxti=maxti+".00";
                             }
                             if(maxtj.indexOf(".")==-1)
                             {
                             maxtj=maxtj+".00";
                             }
                             if($.trim(maxti) != $.trim(maxtj))
                             {
                             chk=false;
                             }
                             if($.trim(document.getElementById("minMemb_"+i).value) != $.trim(document.getElementById("minMemb_"+j).value))
                             {
                             chk=false;
                             }
                             if($.trim(document.getElementById("maxMemb_"+i).value) != $.trim(document.getElementById("maxMemb_"+j).value))
                             {
                             chk=false;
                             }
                             if($.trim(document.getElementById("minMemOutsidePe_"+i).value) != $.trim(document.getElementById("minMemOutsidePe_"+j).value))
                             {
                             chk=false;
                             }
                             if($.trim(document.getElementById("minMemFromPe_"+i).value) != $.trim(document.getElementById("minMemFromPe_"+j).value))
                             {
                             chk=false;
                             }*/

                            //alert(j);
                            //alert("chk" +chk);
                            if (flag) {
                                if (chk == true) //If Row is same then give alert message
                                {
                                    jAlert("Duplicate record found. Please enter unique record", "TC Formation Business Rules Configuration", function (RetVal) {
                                    });
                                    return false;
                                }
                            }

                        }
                    }
                // End OF Dhruti--Checking for Unique Rows

                if (flag == false) {
                    return false;
                }
            }
        }

        function chkMaxLength(value)
        {
            var ValueChk = value;
            //alert(ValueChk);

            if (ValueChk.length > 3)
            {
                return false;
            } else
            {
                return true;
            }
        }

        function chkMax(value)
        {
            var chkVal = value;
            var ValSplit = chkVal.split('.');

            if (ValSplit[0].length > 12)
            {
                return false;
            } else
            {
                return true;
            }
        }

        function numberZero(value)
        {
            var flag;
            //alert(value.indexOf("."));
            if (isNaN(value) == false)
            {
                if (value.indexOf(".") == -1)
                {
                    if (value > 0)
                    {
                        flag = true;
                    } else
                    {
                        flag = false;
                    }
                } else
                {
                    flag = false;
                }
            } else
            {
                flag = false;
            }
            if (flag == false)
            {
                return false;
            } else
            {
                return true;
            }
        }

        function numeric(value) {
            return /^\d+$/.test(value);
        }

        function decimal(value) {
            return /^(\d+(\.\d{2})?)$/.test(value);
        }
        function chkMinTenderBlank(obj) {
            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
            //document.getElementById("mintwords_"+i).innerHTML = DoIt(document.getElementById("minTender_"+i).value);
            document.getElementById("mintwords_" + i).innerHTML = CurrencyConverter(document.getElementById("minTender_" + i).value);
            var chk = true;
            if (obj.value != '')
            {
                if (decimal(obj.value))
                {
                    if (chkMax(obj.value))
                    {
                        document.getElementById("mint_" + i).innerHTML = "";
                    } else
                    {
                        document.getElementById("mint_" + i).innerHTML = "<br/>Maximum 12 numbers are allowed";
                        flag = false;
                    }
                } else
                {
                    document.getElementById("mint_" + i).innerHTML = "<br/>Please Enter numbers and 2 Digits After Decimal";
                    flag = false;
                }
            } else
            {
                document.getElementById("mint_" + i).innerHTML = "<br/>Please enter Min Tender Value (In Nu.)";
                chk = false;
            }
            if (chk == false)
            {
                return false;
            }
        }
        function chkMaxTenderBlank(obj) {
            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
            //document.getElementById("maxtwords_"+i).innerHTML = DoIt(document.getElementById("maxTender_"+i).value);
            document.getElementById("maxtwords_" + i).innerHTML = CurrencyConverter(document.getElementById("maxTender_" + i).value);
            var chk1 = true;
            if (obj.value != '')
            {
                if (decimal(obj.value))
                {
                    if (chkMax(obj.value))
                    {
                        document.getElementById("maxt_" + i).innerHTML = "";
                    } else
                    {
                        document.getElementById("maxt_" + i).innerHTML = "<br/>Maximum 12 numbers are allowed";
                        flag = false;
                    }
                } else
                {
                    document.getElementById("maxt_" + i).innerHTML = "<br/>Please Enter numbers and 2 Digits After Decimal";
                    flag = false;
                }
            } else
            {
                document.getElementById("maxt_" + i).innerHTML = "<br/>Please enter Max Tender Value (In Nu.)";
                chk1 = false;
            }
            if (chk1 == false)
            {
                return false;
            }
        }
        function chkMinMemberBlank(obj) {
            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
            var chk2 = true;
            if (obj.value != '')
            {
                if (numeric(obj.value))
                {
                    if (!numberZero(obj.value))
                    {
                        document.getElementById("minm_" + i).innerHTML = "<br/>Please enter numbers between (1-9) only";
                        flag = false;
                    } else
                    {
                        document.getElementById("minm_" + i).innerHTML = "";
                    }

                } else
                {
                    document.getElementById("minm_" + i).innerHTML = "<br/>Please Enter numbers only";
                    chk2 = false;
                }
            } else
            {
                document.getElementById("minm_" + i).innerHTML = "<br/>Please enter Min. Members Required";
                chk2 = false;
            }
            if (chk2 == false)
            {
                return false;
            }
        }
        function chkMaxMemberBlank(obj) {
            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
            var chk3 = true;
            if (obj.value != '')
            {
                if (numeric(obj.value))
                {
                    document.getElementById("maxm_" + i).innerHTML = "";
                } else
                {
                    document.getElementById("maxm_" + i).innerHTML = "<br/>Please Enter numbers only";
                    chk3 = false;
                }
            } else
            {
                document.getElementById("maxm_" + i).innerHTML = "<br/>Please enter Max. Members Required";
                chk3 = false;
            }
            if (chk3 == false)
            {
                return false;
            }
        }
        function chkMinMemOutPEBlank(obj) {
            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
            var chk4 = true;

            if (obj.value != '')
            {
                if (numeric(obj.value))
                {
                    document.getElementById("minmop_" + i).innerHTML = "";
                } else
                {
                    document.getElementById("minmop_" + i).innerHTML = "<br/>Please Enter numbers only";
                    chk4 = false;
                }
            } else
            {
                document.getElementById("minmop_" + i).innerHTML = "<br/>Please enter Min. Members required from Outside PA";
                chk4 = false;
            }
            if (chk4 == false)
            {
                return false;
            }
        }
        function chkMinMemFromPEBlank(obj) {
            var i = (obj.id.substr(obj.id.indexOf("_") + 1));
            var chk5 = true;

            if (obj.value != '')
            {
                if (numeric(obj.value))
                {
                    document.getElementById("minmfp_" + i).innerHTML = "";
                } else
                {
                    document.getElementById("minmfp_" + i).innerHTML = "<br/>Please Enter numbers only";
                    chk5 = false;
                }
            } else {
                document.getElementById("minmfp_" + i).innerHTML = "<br/>Please enter Min. Members required from PA";
                chk5 = false;
            }
            if (chk5 == false)
            {
                return false;
            }
        }

    </script>

</html>
