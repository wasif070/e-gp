<%--
    Document   : ManageSbDpUser
    Created on : Nov 20, 2010, 11:21:09 AM
    Author     : Administrator
--%>

<!--Search formation changed by Emtaz on 16/May/2016-->

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%
            String partnerType = request.getParameter("partnerType");
            String type = "";
            String title = "";
            String title1 = "";
            String userType = "";
            if (partnerType != null && partnerType.equals("Development")) {
                type = "Development";
                title = "Development Partner";
                title1 = "Development Partner";
                userType = "dev";
            } else {
                type = "ScheduleBank";
                title = "Financial Institution";
                title1 = "Financial Institution Branch";
                userType = "sb";
            }

            String link = "ManageSbDpUserServlet?q=1&action=fetchData&partnerType=" + type;
%>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage <%=title1%> Admin</title>
       <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        
        <script type="text/javascript">
            /*jQuery().ready(function (){
                //alert("url:" + < %=link%>);
                jQuery("#list").jqGrid({
                    url: "../ManageSbDpUserServlet?q=1&action=fetchData&userType=<%=type%>",
                    datatype: "xml",
                    height: 250,
                    colNames:['S. No.','Full Name',<% if (type != null && !("Development").equals(type)) { %> 'Role', <% } %>'e-mail ID','<%=title1%>',"Action"],
                    colModel:[
                        {name:'Sr No',index:'srno', width:25,sortable: false, search: false, align: 'center'},
                        {name:'fullName',index:'fullName', width:80,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                        <% if (type != null && !("Development").equals(type)) { %>
                            {name:'isMakerChecker',index:'isMakerChecker', width:80,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }, align: 'center'},
                        <% } %>
                        {name:'emailId',index:'emailId', width:100,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                        {name:'sbDevelopName',index:'sbDevelopName', width:100,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                        {name:'action',index:'action', width:100,sortable: false, search: false, align: 'center'}
                    ],
                    autowidth: true,
                    multiselect: false,
                    sortable: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    caption: "<%=title%> User Data",
                    gridComplete: function(){
                        $("#list tr:nth-child(even)").css("background-color", "#fff");
                    }
                }).navGrid('#page',{edit:false,add:false,del:false});
            });*/
        </script>
        <script type="text/javascript">
        
        $(document).ready(function(){
        $('#DpHeadName').change(function(){
                    var payType= '';
                    var DpName= $('#DpHeadName').val();
                    $.post("<%=request.getContextPath()%>/APPServlet", { param1:payType, param2:DpName, funName:'getBankBranchListDD'},  function(j){
                        $("select#DpName").html(j);
                    });
                })
                })
        
        function chkdisble(pageNo){
            $('#dispPage').val(Number(pageNo));
            if(parseInt($('#pageNo').val(), 10) != 1){
                $('#btnFirst').removeAttr("disabled");
                $('#btnFirst').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnFirst').attr("disabled", "true");
                $('#btnFirst').css('color', 'gray');
            }


            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnPrevious').attr("disabled", "true")
                $('#btnPrevious').css('color', 'gray');
            }

            if(parseInt($('#pageNo').val(), 10) > 1){
                $('#btnPrevious').removeAttr("disabled");
                $('#btnPrevious').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnLast').attr("disabled", "true");
                $('#btnLast').css('color', 'gray');
            }

            else{
                $('#btnLast').removeAttr("disabled");
                $('#btnLast').css('color', '#333');
            }
            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnNext').attr("disabled", true)
                $('#btnNext').css('color', 'gray');
            }
            else{
                $('#btnNext').removeAttr("disabled");
                $('#btnNext').css('color', '#333');
            }
        }
        function showHide()
        {
            if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
                document.getElementById('tblSearchBox').style.display = 'table';
                document.getElementById('collExp').innerHTML = '- Advanced Search';
            }else{
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Advanced Search';
            }
        }
        function hide()
        {
            document.getElementById('tblSearchBox').style.display = 'none';
            document.getElementById('collExp').innerHTML = '+ Advanced Search';
        }
    </script>

    <script type="text/javascript">
        $(function() {
            $('#btnFirst').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);

                if(totalPages>0 && $('#pageNo').val()!="1")
                {
                    $('#pageNo').val("1");
                    loadTable();
                    $('#dispPage').val("1");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnLast').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);
                if(totalPages>0 && $('#pageNo').val()!=totalPages)
                {
                    $('#pageNo').val(totalPages);
                    loadTable();
                    $('#dispPage').val(totalPages);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnNext').click(function() {
                var pageNo=parseInt($('#pageNo').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo < totalPages) {
                    $('#pageNo').val(Number(pageNo)+1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)+1);
                    $('#btnPrevious').removeAttr("disabled");
                }
            });
        }); 

    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnPrevious').click(function() {
                var pageNo=$('#pageNo').val();

                if(parseInt(pageNo, 10) > 1)
                {
                    $('#pageNo').val(Number(pageNo) - 1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo) - 1);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnGoto').click(function() {
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(function() {
            $('#btnSearch').click(function() {
                count=0;
                if(document.getElementById('FullName') !=null && document.getElementById('FullName').value !=''){
                    count = 1;
                }else if(document.getElementById('EmailID') !=null && document.getElementById('EmailID').value !=''){
                    count = 1;
                }else if(document.getElementById('DpName') !=null && document.getElementById('DpName').value !=''){
                    count = 1;
                }
                else if(document.getElementById('DpHeadName') !=null && document.getElementById('DpHeadName').value !=''){
                    count = 1;
                }

                if(count == 0){
                    $('#valMsg').html('Please enter at least One search criteria');
                }else{
                    $('#valMsg').html('');
                    $('#pageNo').val('1');
                    loadTable();
                }
            });
            $('#btnReset').click(function() {
               $("#pageNo").val("1");
                $("#FullName").val('');
                $("#EmailID").val('');
                $("#DpName").val('');
                $("#DpHeadName").val('');
                $('#valMsg').html('');
                loadTable();
            });
        });
        function loadTable()
        {
            $.post("<%=request.getContextPath()%>/ManageSbDpUserServlet", {
                    action: "fetchData",
                    userType: $("#userType").val(),
                    FullName: $("#FullName").val(),
                    EmailID: $("#EmailID").val(),
                    DpName: $("#DpName").val(),
                    DpHeadName: $("#DpHeadName").val(),
                    pageNo: $("#pageNo").val(),
                    size: $("#size").val()
                },  function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);
                sortTable();
                //sortTable();
                if($('#noRecordFound').attr('value') == "noRecordFound"){
                    $('#pagination').hide();
                }else{
                    $('#pagination').show();
                }
                if($("#totalPages").val() == 1){
                    $('#btnNext').attr("disabled", "true");
                    $('#btnLast').attr("disabled", "true");
                }else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnLast').removeAttr("disabled");
                }
                $("#pageNoTot").html($("#pageNo").val());
                chkdisble($("#pageNo").val());
                $("#pageTot").html($("#totalPages").val());
            });
        }
    </script>  
    </head>
    <body onload="loadTable();hide();">
        <div class="mainDiv">
            <div class="dashboard_div">
                <%--<jsp:include page="../resources/common/AfterLoginTop.jsp" ></jsp:include>--%>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <td class="contentArea">
                            <div class="pageHead_1">View <%=title1%> Users
                            <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('5');">Save as PDF</a></span>
                            </div>
                            <%if ("y".equals(request.getParameter("succFlag"))) {
                                            if ("approved".equalsIgnoreCase(request.getParameter("userStatus"))) {%>
                            <div id="succMsg" class="responseMsg successMsg">User's account Activated Successfully.</div>
                            <%} else {%>
                            <div id="succMsg" class="responseMsg successMsg">User's account Deactivated Successfully.</div>
                            <%}
                                        }%>

                            <%if ("y".equals(request.getParameter("replaceFlag"))) {
                                            %>
                            <div id="succMsg" class="responseMsg successMsg">User Replaced Successfully</div>
                            <%}%>

                            
                            <div class="t-align-left ff formStyle_1">To sort click on the relevant column header</div>
                            
                            <div class="formBg_1">
                                <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                                <table id="tblSearchBox" cellspacing="10" class="formStyle_1" width="100%">

                                   
                                    <tr>
                                        <td class="ff">Full Name : </td>
                                        <td><input type="text" class="formTxtBox_1" id="FullName" style="width:202px;" /></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Email ID : </td>
                                        <td><input type="text" class="formTxtBox_1" id="EmailID" style="width:202px;" /></td>
                                    </tr>
                                    <% if(type != null && !("Development").equals(type)) {
                                        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                        if(session.getAttribute("userId").toString().equalsIgnoreCase("1")) {
                                        List<SPTenderCommonData> BankList = tenderCommonService.returndata("getBankBranchList", "0", "");
                                    %>
                                    <tr>
                                        <td class="ff"><%=title%> Name : </td>
                                        <td>
                                            <select name="DpHeadName" class="formTxtBox_1" id="DpHeadName" style="width:202px;">
                                                <option value="" >-- Select Financial Institution --</option>
                                                <%
                                                    for(SPTenderCommonData BankInfo : BankList)
                                                    {
                                                %>
                                                    <option value="<%=BankInfo.getFieldName1()%>"><%=BankInfo.getFieldName1() %></option>
                                                <%
                                                    }
                                                %>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff"><%=title1%> Name : </td>
                                        <td>
                                            <select class="formTxtBox_1" name="DpName" id="DpName" style="width:202px;" >
                                                <option value="">-- Select Branch --</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <% }else{
                                        List<SPTenderCommonData> BankList = tenderCommonService.returndata("getBankBranchesFromBankAdminUserId", session.getAttribute("userId").toString(), "");
                                    %>
                                    <input type="hidden" class="" id="DpHeadName" value="" />
                                    <tr>
                                        <td class="ff"><%=title1%> Name : </td>
                                        <td>
                                            <select class="formTxtBox_1" name="DpName" id="DpName" style="width:202px;" >
                                                <option value="">-- Select Branch --</option>
                                                <%
                                                    for(SPTenderCommonData BankInfo : BankList)
                                                    {
                                                %>
                                                    <option value="<%=BankInfo.getFieldName1()%>"><%=BankInfo.getFieldName1() %></option>
                                                <%
                                                    }
                                                %>
                                            </select>
                                        </td>
                                    </tr>
                                    <% } %>
                                    
                                     <%   }
                                        else{ %>
                                     <tr>
                                        <td class="ff"><%=title1%> Name : </td>
                                        <td><input type="text" class="formTxtBox_1" id="DpName" style="width:202px;" /></td>
                                        <input type="hidden" class="" id="DpHeadName" value="" />
                                    </tr>
                                    <% } %>
                                   
                                    <tr>
                                        <td class="ff">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td class="ff">&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="t-align-center">
                                            <label class="formBtn_1"><input type="button" tabindex="6" name="search" id="btnSearch" value="Search"/>
                                            </label>&nbsp;&nbsp;
                                            <label class="formBtn_1"><input type="button" tabindex="7" name="reset" id="btnReset" value="Reset"/>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="t-align-center">
                                            <div class="reqF_1" id="valMsg"></div>
                                        </td>
                                    </tr>
                                </table>

                            </div>  
                            <div class="tableHead_1 t_space">
                                Search Result
                            </div>
                    
                            <div class="tabPanelArea_1">
                                <% if (type != null && !("Development").equals(type)) { %>
                                <table width="100%" cellspacing="0" class="tableList_1" id="resultTable" cols="@0,6">
                                    <tr>
                                        <th width="5%" class="t-align-center">
                                            Sl. <br/>
                                            No.
                                        </th>
                                        <th width="15%" class="t-align-center">
                                            Full Name                                
                                        </th>
                                        <th width="8%" class="t-align-center">
                                            Role                              
                                        </th>
                                        <th width="17%" class="t-align-center">
                                            Email ID
                                        </th>
                                        <%if(session.getAttribute("userId").toString().equalsIgnoreCase("1")) {%>
                                         <th width="15%" class="t-align-center">
                                            <%=title%>
                                        </th>
                                        <%}%>
                                        <th width="20%" class="t-align-center">
                                            <%=title1%>
                                        </th>
                                        <th width="20%" class="t-align-center">
                                            Action
                                        </th>
                                    </tr>
                                </table>
                              <% } else {%>
                                <table width="100%" cellspacing="0" class="tableList_1" id="resultTable" cols="@0,4">
                                    <tr>
                                        <th width="5%" class="t-align-center">
                                            Sl. <br/>
                                            No.
                                        </th>
                                        <th width="15%" class="t-align-center">
                                            Full Name                                
                                        </th>
                                        <th width="30%" class="t-align-center">
                                            Email ID
                                        </th>
                                        <th width="30%" class="t-align-center">
                                            <%=title1%>
                                        </th>
                                        <th width="20%" class="t-align-center">
                                            Action
                                        </th>
                                    </tr>
                                </table>
                              <% } %>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                                    <tr>
                                        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                        <td align="center"><input name="textfield3" onkeypress="checkKeyGoTo(event);" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input   type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                        </label></td>
                                        <td  class="prevNext-container">
                                            <ul>
                                                <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                                <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                                <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <div align="center">

                                    <input type="hidden" id="pageNo" value="1"/>
                                    <input type="hidden" name="size" id="size" value="10"/>
                                    <input type="hidden" name="userType" id="userType" value="<%=type%>" />
                                    <!--<input type="hidden" name="UserTypeId" id="UserTypeId" value="" />-->
                                    
                                </div>
                            </div>
                            
<!--                            <div class="t_space">
                                <table id="list"></table>
                                <div id="page">

                                </div>
                            </div>-->
                        </td>
                    </tr>
                </table>
                <!--For Generate PDF  Starts-->
                <form id="formstyle" action="" method="post" name="formstyle">
                    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                    <%
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                        String appenddate = dateFormat1.format(new Date());
                    %>
                    <input type="hidden" name="fileName" id="fileName" value='<%=title.replaceAll(" ","")%>Users_<%=appenddate%>' />
                    <input type="hidden" name="id" id="id" value='<%=title.replaceAll(" ","")%>Users' />
                </form>
                <!--For Generate PDF  Ends-->
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script>
            var obj = document.getElementById('lblDevPartnerUsersView');
            if(obj != null){
                //if(obj.innerHTML == 'View'){
                obj.setAttribute('class', 'selected');
                // }
            }

            var obj1 = document.getElementById('lblDevPartnerUsersView');
            if(obj1 != null){
                //if(obj1.innerHTML == 'View Development Partner Users'){
                obj1.setAttribute('class', 'selected');
                // }
            }
        </script>
        <script>
            
            var Interval;
            clearInterval(Interval);
            Interval = setInterval(function(){
                var PageNo = document.getElementById('pageNo').value;
                var Size = document.getElementById('size').value;
                CorrectSerialNumber(PageNo-1,Size); 
            }, 100);
            
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
