<%-- 
    Document   : NOABusinessRuleDetails
    Created on : Dec 29, 2010, 3:24:34 PM
    Author     : Naresh.Akurathi
--%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%
    String strUserTypeId = "";
    Object objUserId = session.getAttribute("userId");
    Object objUName = session.getAttribute("userName");
    boolean isLoggedIn = false;
    if (objUserId != null) {
        strUserTypeId = session.getAttribute("userId").toString();
    }
    if (objUName != null) {
        isLoggedIn = true;
    }
%>

<%@page import="java.util.List"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigNoa"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<%@page import="java.util.Iterator"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Letter of Acceptance (LOA) Business Rules Configuration </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <%!    ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();
        %>

        <%
            StringBuilder procNature = new StringBuilder();
            StringBuilder procType = new StringBuilder();
            StringBuilder procMethod = new StringBuilder();
            configPreTenderRuleSrBean.setLogUserId(strUserTypeId);

            TblProcurementNature tblProcureNature2 = new TblProcurementNature();
            Iterator pnit2 = configPreTenderRuleSrBean.getProcurementNature().iterator();
            while (pnit2.hasNext()) {
                tblProcureNature2 = (TblProcurementNature) pnit2.next();
                procNature.append("<option value='" + tblProcureNature2.getProcurementNatureId() + "'>" + tblProcureNature2.getProcurementNature() + "</option>");
            }

            TblProcurementMethod tblProcurementMethod2 = new TblProcurementMethod();
            Iterator pmit2 = configPreTenderRuleSrBean.getProcurementMethod().iterator();
            while (pmit2.hasNext()) {
                tblProcurementMethod2 = (TblProcurementMethod) pmit2.next();
                if ("OTM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())
                        || "LTM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())
                        || "DP".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())
                        || // "LEQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                        //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                        "RFQ".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())
                        || // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                        //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                        "FC".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())
                        || // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                        "DPM".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())
                        || "QCBS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())
                        || "LCS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())
                        || "SFB".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())
                        || "SBCQ".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())
                        || "SSS".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())
                        || "IC".equalsIgnoreCase(tblProcurementMethod2.getProcurementMethod())) {
                    if (tblProcurementMethod2.getProcurementMethod().equals("RFQ")) {
                        procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>LEM</option>");

                    } else if (tblProcurementMethod2.getProcurementMethod().equals("DPM")) {
                        procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>DCM</option>");

                    } else {
                        procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>" + tblProcurementMethod2.getProcurementMethod() + "</option>");

                    }

                }
            }

            TblProcurementTypes tblProcurementTypes2 = new TblProcurementTypes();
            Iterator ptit2 = configPreTenderRuleSrBean.getProcurementTypes().iterator();
            while (ptit2.hasNext()) 
            {
                tblProcurementTypes2 = (TblProcurementTypes) ptit2.next();
                
                // changes to ICT/NCT to ICB/NCB :::nitish 
                if(tblProcurementTypes2.getProcurementType().equals("NCT"))
                {
                    procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + "NCB" + "</option>");
                    //procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + tblProcurementTypes2.getProcurementType() + "</option>");
                }
                if(tblProcurementTypes2.getProcurementType().equals("ICT"))
                {
                    procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + "ICB" + "</option>");
                    //procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + tblProcurementTypes2.getProcurementType() + "</option>");
                }
            }


        %>


    </head>
    <body onload="convert()">
        <%            String msg = "";
            if ("Submit".equals(request.getParameter("button")) || "Update".equals(request.getParameter("button"))) 
            {
                int row = Integer.parseInt(request.getParameter("TotRule"));

                if ("edit".equals(request.getParameter("action"))) {
                    msg = "Data will not be Upadated";
                } else {
                    msg = configPreTenderRuleSrBean.delAllConfigNoa();
                }

                if (msg.equals("Deleted")) {
                    String action = "Add NOA Rules";
                    try {
                        for (int i = 1; i <= row; i++) {
                            if (request.getParameter("procNature" + i) != null) {
                                byte pnature = Byte.parseByte(request.getParameter("procNature" + i));
                                byte pmethod = Byte.parseByte(request.getParameter("procMethod" + i));
                                byte ptypes = Byte.parseByte(request.getParameter("procType" + i));
                                float minValue = Float.parseFloat(request.getParameter("minValue" + i));
                                float maxValue = Float.parseFloat(request.getParameter("maxValue" + i));
                                short noaAcceptDays = Short.parseShort(request.getParameter("noaAcceptDays" + i));
                                short perSecSubDays = Short.parseShort(request.getParameter("perSecSubDays" + i));
                                short conSignDays = Short.parseShort(request.getParameter("conSignDays" + i));
                                short loiNoa = Short.parseShort(request.getParameter("loiNoa" + i)); //loiNoa variable 

                                TblConfigNoa tblConfigNoa = new TblConfigNoa();
                                tblConfigNoa.setTblProcurementNature(new TblProcurementNature(pnature));
                                tblConfigNoa.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                tblConfigNoa.setTblProcurementTypes(new TblProcurementTypes(ptypes));
                                tblConfigNoa.setMinValue(new BigDecimal(request.getParameter("minValue" + i)));
                                tblConfigNoa.setMaxValue(new BigDecimal(request.getParameter("maxValue" + i)));
                                tblConfigNoa.setNoaAcceptDays(noaAcceptDays);
                                tblConfigNoa.setPerSecSubDays(perSecSubDays);
                                tblConfigNoa.setConSignDays(conSignDays);
                                tblConfigNoa.setLoiNoa(loiNoa); //set the loiNoa variable into the database 
                                msg = configPreTenderRuleSrBean.addConfigNoa(tblConfigNoa);
                            }
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                        action = "Error in : " + action + " : " + e;
                    } finally {
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                        action = null;
                    }
                } else {
                    String action = "Edit NOA Rules";
                    try {
                        for (int i = 1; i <= row; i++) {
                            if (request.getParameter("procNature" + i) != null) {
                                byte pnature = Byte.parseByte(request.getParameter("procNature" + i));
                                byte pmethod = Byte.parseByte(request.getParameter("procMethod" + i));
                                byte ptypes = Byte.parseByte(request.getParameter("procType" + i));
                                float minValue = Float.parseFloat(request.getParameter("minValue" + i));
                                float maxValue = Float.parseFloat(request.getParameter("maxValue" + i));
                                short noaAcceptDays = Short.parseShort(request.getParameter("noaAcceptDays" + i));
                                short perSecSubDays = Short.parseShort(request.getParameter("perSecSubDays" + i));
                                short conSignDays = Short.parseShort(request.getParameter("conSignDays" + i));
                                short loiNoa = Short.parseShort(request.getParameter("loiNoa" + i)); //get loiNoa valu from input field

                                TblConfigNoa tblConfigNoa = new TblConfigNoa();
                                tblConfigNoa.setConfigNoaId(Integer.parseInt(request.getParameter("id")));
                                tblConfigNoa.setTblProcurementNature(new TblProcurementNature(pnature));
                                tblConfigNoa.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                tblConfigNoa.setTblProcurementTypes(new TblProcurementTypes(ptypes));
                                tblConfigNoa.setMinValue(new BigDecimal(request.getParameter("minValue" + i)));
                                tblConfigNoa.setMaxValue(new BigDecimal(request.getParameter("maxValue" + i)));
                                tblConfigNoa.setNoaAcceptDays(noaAcceptDays);
                                tblConfigNoa.setPerSecSubDays(perSecSubDays);
                                tblConfigNoa.setConSignDays(conSignDays);
                                tblConfigNoa.setLoiNoa(loiNoa); //set the loiNoa variable into the database 

                                msg = configPreTenderRuleSrBean.updateConfigNoa(tblConfigNoa);
                            }
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                        action = "Error in : " + action + " : " + e;
                    } finally {
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                        action = null;
                    }
                }

                if (msg.equals("Values Added")) {
                    msg = "Letter of Acceptance (LOA) business rule configured successfully";
                    response.sendRedirect("NOABusinessRuleView.jsp?msg=" + msg);
                }
                if (msg.equals("Updated")) {
                    msg = "Letter of Acceptance (LOA) business rule Updated successfully";
                    response.sendRedirect("NOABusinessRuleView.jsp?msg=" + msg);
                }

            }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                        <td class="contentArea">
                            <div class="pageHead_1">Letter of Acceptance (LOA) Business Rules Configuration</div>

                            <div style="font-style: italic;" class="t-align-right"><normal>Fields marked with (<span class="mandatory">*</span>) are mandatory</normal></div>

                            <div align="right" class="t_space">
                                <span id="lotMsg" style="color: red; float: left; font-weight: bold; visibility: collapse;">&nbsp;</span>
                                <a id="linkAddRule" class="action-button-add">Add Rule</a> <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a><br/>
                            </div>

                        <%--<div class="pageHead_1" align="center"> <%=msg%></div>--%>

                        <form action="NOABusinessRuleDetails.jsp" method="post">
                            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tblrule" name="tblrule">
                                <tr>
                                    <th >Select</th>
                                    <th >Procurement  Category<br />(<span class="mandatory">*</span>)</th>
                                    <th >Procurement  Method<br />(<span class="mandatory">*</span>)</th>
                                    <th >Procurement  Type<br />(<span class="mandatory">*</span>)</th>
                                    <th >Minimum Value<br /> (In Nu.)<br />(<span class="mandatory">*</span>)</th>
                                    <th >Minimum Value<br /> (In Nu.)<br />words</th>
                                    <th >Maximum Value<br /> (In Nu.)<br />(<span class="mandatory">*</span>) </th>

                                    <th >Maximum Value<br /> (In Nu.)<br />words</th>
                                    <th >No. of Days for <br /> Letter of Acceptance (LOA) acceptance<br/>(<span class="mandatory">*</span>) </th>
                                    <th >No. of Days for <br /> Performance Security <br /> submission <br />(<span class="mandatory">*</span>) </th>
                                    <th >No. of Days for <br />Contract Signing  <br /> from date of <br />issuance of Letter of Acceptance (LOA) <br />(<span class="mandatory">*</span>) </th>
                                    <th >No. of Days between LoI and LOA</th>
                                </tr>
                                <input type="hidden" name="delrow" value="0" id="delrow"/>
                                <input type="hidden" name="introw" value="" id="introw"/>

                                <%                                    List l = null;

                                    if (request.getParameter("id") != null && "edit".equals(request.getParameter("action"))) {
                                        String action = request.getParameter("action");
                                        int id = Integer.parseInt(request.getParameter("id"));
                                %>
                                <input type="hidden" id="hiddenaction" name="action" value="<%=action%>"/>
                                <input type="hidden" id="hiddenid" name="id" value="<%=id%>"/>
                                <%
                                        l = configPreTenderRuleSrBean.getConfigNoa(id);
                                    } else
                                        l = configPreTenderRuleSrBean.getAllConfigNoa();

                                    int i = 0;
                                    int length = 0;
                                    if (!l.isEmpty()) {
                                        Iterator it = l.iterator();
                                        TblConfigNoa configNoa = new TblConfigNoa();
                                        List getProcNature = configPreTenderRuleSrBean.getProcurementNature();
                                        List getProcMethod = configPreTenderRuleSrBean.getProcurementMethod();
                                        List getProcTypes = configPreTenderRuleSrBean.getProcurementTypes();
                                        while (it.hasNext()) {
                                            configNoa = (TblConfigNoa) it.next();
                                            i++;
                                            length++;
                                            StringBuilder tType = new StringBuilder();
                                            StringBuilder bType = new StringBuilder();
                                            StringBuilder pNature = new StringBuilder();
                                            StringBuilder pType = new StringBuilder();
                                            StringBuilder pMethod = new StringBuilder();
                                            StringBuilder natDisaster = new StringBuilder();
                                            StringBuilder urgency = new StringBuilder();
                                            String minValue = new String();
                                            String maxValue = new String();
                                            String minDays = new String();
                                            String maxDays = new String();

                                            TblProcurementNature tblProcureNature = new TblProcurementNature();
                                            Iterator pnit = getProcNature.iterator();
                                            while (pnit.hasNext()) {
                                                tblProcureNature = (TblProcurementNature) pnit.next();
                                                pNature.append("<option value='");
                                                if (tblProcureNature.getProcurementNatureId() == configNoa.getTblProcurementNature().getProcurementNatureId()) {
                                                    pNature.append(tblProcureNature.getProcurementNatureId() + "' selected='true'>" + tblProcureNature.getProcurementNature());
                                                } else {
                                                    pNature.append(tblProcureNature.getProcurementNatureId() + "'>" + tblProcureNature.getProcurementNature());
                                                }
                                                pNature.append("</option>");
                                            }

                                            TblProcurementMethod tblProcurementMethod = new TblProcurementMethod();
                                            Iterator pmit = getProcMethod.iterator();
                                            while (pmit.hasNext()) {
                                                tblProcurementMethod = (TblProcurementMethod) pmit.next();
                                                if ("OTM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())
                                                        || "LTM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())
                                                        || "DP".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())
                                                        || // "LEQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                        //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                        "RFQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())
                                                        || // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                        //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                        "FC".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())
                                                        || // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                                                        "DPM".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())
                                                        || "QCBS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())
                                                        || "LCS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())
                                                        || "SFB".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())
                                                        || "SBCQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())
                                                        || "SSS".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())
                                                        || "IC".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod())) {
                                                    pMethod.append("<option value='");
                                                    if (tblProcurementMethod.getProcurementMethodId() == configNoa.getTblProcurementMethod().getProcurementMethodId()) {
                                                        if (tblProcurementMethod.getProcurementMethod().equals("RFQ")) {
                                                            pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>LEM");

                                                        } else if (tblProcurementMethod.getProcurementMethod().equals("DPM")) {
                                                            pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>DCM");

                                                        } else {
                                                            pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>" + tblProcurementMethod.getProcurementMethod());

                                                        }

                                                    } else if (tblProcurementMethod.getProcurementMethod().equals("RFQ")) {
                                                        pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>LEM");

                                                    } else if (tblProcurementMethod.getProcurementMethod().equals("DPM")) {
                                                        pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>DCM");

                                                    } else {
                                                        pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>" + tblProcurementMethod.getProcurementMethod());

                                                    }
                                                    pMethod.append("</option>");
                                                }
                                            }

                                            TblProcurementTypes tblProcurementTypes = new TblProcurementTypes();
                                            Iterator ptit = getProcTypes.iterator();
                                            while (ptit.hasNext()) {
                                                tblProcurementTypes = (TblProcurementTypes) ptit.next();
                                                pType.append("<option value='");
                                                //ICT->ICB & NCT->NCB by Emtaz on 19/April/2016
                                                String ProcurementType = "";
                                                if (tblProcurementTypes.getProcurementType().equalsIgnoreCase("ICT")) {
                                                    ProcurementType = "ICB";
                                                } else if (tblProcurementTypes.getProcurementType().equalsIgnoreCase("NCT")) {
                                                    ProcurementType = "NCB";
                                                }
                                                if (tblProcurementTypes.getProcurementTypeId() == configNoa.getTblProcurementTypes().getProcurementTypeId()) {
                                                    pType.append(tblProcurementTypes.getProcurementTypeId() + "' selected='true'>" + ProcurementType);
                                                } else {
                                                    pType.append(tblProcurementTypes.getProcurementTypeId() + "'>" + ProcurementType);
                                                }
                                                pType.append("</option>");

                                            }

                                %>
                                <tr id="trrule_<%=i%>">
                                    <td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_<%=i%>" value="" /></td>
                                    <td class="t-align-center"><select name="procNature<%=i%>" class="formTxtBox_1" id="procNature_<%=i%>">
                                            <%=pNature%></select>
                                    </td>
                                    <td class="t-align-center"><select name="procMethod<%=i%>" class="formTxtBox_1" id="procMethod_<%=i%>">
                                            <%=pMethod%></select>
                                    </td>
                                    <td class="t-align-center"><select name="procType<%=i%>" class="formTxtBox_1" id="procType_<%=i%>">
                                            <%=pType%></select>
                                    </td>
                                    <td class="t-align-center"><input name="minValue<%=i%>" type="text"  class="formTxtBox_1" maxlength="18" id="minValue_<%=i%>" style="width:90%;"   value="<%=configNoa.getMinValue()%>" onblur="return chkMinVal(this);" /><span id="MinValue_<%=i%>" style="color:red;"></span></td>
                                    <td class="t-align-center"><span id="MinValuewords_<%=i%>"></span></td>
                                    <td class="t-align-center"><input name="maxValue<%=i%>" type="text"  class="formTxtBox_1" maxlength="18" id="maxValue_<%=i%>" style="width:90%;" value="<%=configNoa.getMaxValue()%>" onblur="return chkMaxVal(this);"/><span id="MaxValue_<%=i%>" style="color:red;"></span></td>
                                    <td class="t-align-center"><span id="MaxValuewords_<%=i%>" ></span></td>
                                    <td class="t-align-center"><input name="noaAcceptDays<%=i%>" type="text" class="formTxtBox_1" id="noaAcceptDays_<%=i%>" style="width:90%;" value="<%=configNoa.getNoaAcceptDays()%>" onblur="return chkNOAacceptance(this);"/><span id="NOAacceptDays_<%=i%>" style="color:red;"></span></td>
                                    <td class="t-align-center"><input name="perSecSubDays<%=i%>" type="text" class="formTxtBox_1" id="perSecSubDays_<%=i%>" style="width:90%;" value="<%=configNoa.getPerSecSubDays()%>" onblur="return chkPerSecSubDays(this);"/><span id="PerSecSubDays_<%=i%>" style="color:red;"></span></td>
                                    <td class="t-align-center"><input name="conSignDays<%=i%>" type="text" class="formTxtBox_1" id="conSignDays_<%=i%>" style="width:90%;" value="<%=configNoa.getConSignDays()%>" onblur="return chkConSignDays(this);"/><span id="ConSignDays_<%=i%>" style="color:red;"></span></td>
                                    <td class="t-align-center"><input name="loiNoa<%=i%>" type="text" class="formTxtBox_1" id="loiNoa_<%=i%>" style="width:90%;" value="<%=configNoa.getLoiNoa()%>" onblur="return chkLoiNoa(this);"/><span id="LoiNoa_<%=i%>" style="color:red;"></span></td>
                                </tr>
                                <%
                                        }
                                    } else {
                                        msg = "No Record Found";
                                    }

                                %>
                            </table>
                            <div>&nbsp;</div>
                            <input type="hidden" name="TotRule" id="TotRule" value="<%=i%>"/>
                            <table width="100%" cellspacing="0" >
                                <tr>
                                    <%if ("edit".equals(request.getParameter("action"))) {%>
                                    <td colspan="11" class="t-align-center"><span class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Update" onclick="return validate();" />
                                        </span></td>
                                        <%} else {%>
                                    <td colspan="11" class="t-align-center"><span class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Submit" onclick="return validate();" />
                                        </span></td>
                                        <%}%>
                                </tr>
                            </table>
                            <div>&nbsp;</div>
                            <div align="center"> <%=msg%></div>
                        </form>

                    </td>
                </tr>
            </table>


            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

            </div>
            <script>
                var obj = document.getElementById('lblNOARuleEdit');
                if (obj != null) {
                    if (obj.innerHTML == 'Edit') {
                        obj.setAttribute('class', 'selected');
                    }
                }

            </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if (headSel_Obj != null) {
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            <script type="text/javascript">
                function convert()
                {
                    for (var k = 1; k <=<%= length%>; k++)
                    {
                        //document.getElementById("MinValuewords_"+k).innerHTML = DoIt(document.getElementById("minValue_"+k).value);
                        //document.getElementById("MaxValuewords_"+k).innerHTML = DoIt(document.getElementById("maxValue_"+k).value);
                        document.getElementById("MinValuewords_" + k).innerHTML = CurrencyConverter(document.getElementById("minValue_" + k).value);
                        document.getElementById("MaxValuewords_" + k).innerHTML = CurrencyConverter(document.getElementById("maxValue_" + k).value);
                    }
                }
        </script>
        <script type="text/javascript">


            function validate()
            {
                var count = document.getElementById("TotRule").value;
                var flag = true;
                if (document.getElementById("hiddenaction"))
                {
                    if ("edit" == document.getElementById("hiddenaction").value)
                    {
                        $.ajax({
                            type: "POST",
                            url: "<%=request.getContextPath()%>/BusinessRuleConfigurationServlet",
                            data: "configNoaId=" + $('#hiddenid').val() + "&" + "procNature=" + $('#procNature_1').val() + "&" + "procMethod=" + $('#procMethod_1').val() + "&" + "procType=" + $('#procType_1').val() + "&" + "minvalue=" + $('#minValue_1').val() + "&" + "maxvalue=" + $('#maxValue_1').val() + "&" + "funName=NOABusinessRule",
                            async: false,
                            success: function (j) {

                                if (j.toString() != "0") {
                                    flag = false;
                                    jAlert("Combination of 'ProcurementNature' , 'ProcurementMethod' and  'ProcurementType' ,  already exist.", "NOA Business Rules Configuration", function (RetVal) {
                                    });
                                }
                            }
                        });
                        //return flag; // always update data feild without checking value 
                    }
                }
                for (var k = 0; k <= count; k++)
                {
                    if (document.getElementById("minValue_" + k) != null)
                    {
                        if (document.getElementById("minValue_" + k).value == "")
                        {
                            document.getElementById("MinValue_" + k).innerHTML = "<br/>Please enter Minimum Value (In Nu.)";
                            flag = false;
                        } else
                        {
                            if (decimal(document.getElementById("minValue_" + k).value))
                            {
                                if (!chkMax(document.getElementById("minValue_" + k).value))
                                {
                                    document.getElementById("MinValue_" + k).innerHTML = "Maximum 12 digits are allowed";
                                    flag = false;
                                } else
                                {
                                    document.getElementById("MinValue_" + k).innerHTML = "";
                                }

                            } else
                            {
                                document.getElementById("MinValue_").innerHTML = "<br/>Please Enter Numbers and 2 digits after Decimal";
                                flag = false;
                            }
                        }
                    }

                    if (document.getElementById("maxValue_" + k) != null)
                    {
                        if (document.getElementById("maxValue_" + k).value == "")
                        {
                            document.getElementById("MaxValue_" + k).innerHTML = "<br/>Please enter Maximum Value (In Nu.)";
                            flag = false;
                        } else
                        {
                            if (decimal(document.getElementById("maxValue_" + k).value))
                            {
                                if (!chkMax(document.getElementById("maxValue_" + k).value))
                                {
                                    document.getElementById("MaxValue_" + k).innerHTML = "Maximum 12 digits are allowed";
                                    flag = false;
                                } else
                                {
                                    document.getElementById("MaxValue_" + k).innerHTML = "";
                                }

                            } else
                            {
                                document.getElementById("MaxValue_" + k).innerHTML = "<br/>Please Enter Numbers and 2 digits after Decimal";
                                flag = false;
                            }

                        }

                    }

                    if (document.getElementById("noaAcceptDays_" + k) != null)
                    {
                        if (document.getElementById("noaAcceptDays_" + k).value == "")
                        {
                            document.getElementById("NOAacceptDays_" + k).innerHTML = "<br/>Please enter No. of days for Letter of Acceptance (LOA) acceptance";
                            flag = false;
                        } else
                        {
                            if (numeric(document.getElementById("noaAcceptDays_" + k).value))
                            {
                                document.getElementById("NOAacceptDays_" + k).innerHTML = "";
                                //                                if(!chkMaxLength(document.getElementById("noaAcceptDays"+k).value))
                                //                                {
                                //                                    document.getElementById("NOAacceptDays_"+k).innerHTML="<br/>Maximum 3 numbers are allowed";
                                //                                    flag= false;
                                //                                }
                                //                                else
                                //                                {
                                //                                    document.getElementById("NOAacceptDays_"+k).innerHTML="";
                                //                                }
                            } else
                            {
                                document.getElementById("NOAacceptDays_" + k).innerHTML = "<br/>Please Enter Numbers only";
                                flag = false;
                            }
                        }

                    }
                    
                    
                    
                    
                    
                    

                    if (document.getElementById("perSecSubDays_" + k) != null)
                    {
                        if (document.getElementById("perSecSubDays_" + k).value == "")
                        {
                            document.getElementById("PerSecSubDays_" + k).innerHTML = "<br/>Please enter No. of days for Performance Security submission";
                            flag = false;
                        } else
                        {
                            if (numeric(document.getElementById("perSecSubDays_" + k).value))
                            {
                                document.getElementById("PerSecSubDays_" + k).innerHTML = "";
                                //                                if(!chkMaxLength(document.getElementById("noaAcceptDays"+k).value))
                                //                                {
                                //                                    document.getElementById("NOAacceptDays_"+k).innerHTML="<br/>Maximum 3 numbers are allowed";
                                //                                    flag= false;
                                //                                }
                                //                                else
                                //                                {
                                //                                    document.getElementById("NOAacceptDays_"+k).innerHTML="";
                                //                                }
                            } else
                            {
                                document.getElementById("PerSecSubDays_" + k).innerHTML = "<br/>Please Enter Numbers only";
                                flag = false;
                            }
                        }

                    }

                    if (document.getElementById("conSignDays_" + k) != null)
                    {
                        if (document.getElementById("conSignDays_" + k).value == "")
                        {
                            document.getElementById("ConSignDays_" + k).innerHTML = "<br/>Please enter No. of days for Contract Signing";
                            flag = false;
                        } else
                        {
                            if (numeric(document.getElementById("conSignDays_" + k).value))
                            {
                                document.getElementById("ConSignDays_" + k).innerHTML = "";
                                //                                if(!chkMaxLength(document.getElementById("noaAcceptDays"+k).value))
                                //                                {
                                //                                    document.getElementById("NOAacceptDays_"+k).innerHTML="<br/>Maximum 3 numbers are allowed";
                                //                                    flag= false;
                                //                                }
                                //                                else
                                //                                {
                                //                                    document.getElementById("NOAacceptDays_"+k).innerHTML="";
                                //                                }
                            } else
                            {
                                document.getElementById("ConSignDays_" + k).innerHTML = "<br/>Please Enter Numbers only";
                                flag = false;
                            }
                        }

                    }
                    
                    if (document.getElementById("loiNoa_" + k) != null)
                    {
                        if (document.getElementById("loiNoa_" + k).value == "")
                        {
                            document.getElementById("LoiNoa_" + k).innerHTML = "<br/>Please enter No. of Days between LoI and Letter of Acceptance(LOA)";
                            flag = false;
                        } 
                        else
                        {
                            /*if(document.getElementById("loiNoa_" + k).value == '0' || document.getElementById("loiNoa_" + k).value == '00')
                            {
                               document.getElementById("LoiNoa_" + k).innerHTML = "<br/>Zero will not be allowed";
                               flag = false; 
                            }
                            else*/ 
                            if (numeric(document.getElementById("loiNoa_" + k).value))
                            {
                                document.getElementById("LoiNoa_" + k).innerHTML = "";                            
                            } 
                            else
                            {
                                document.getElementById("LoiNoa_" + k).innerHTML = "<br/>Please Enter Numbers only";
                                flag = false;
                            }
                        }
                    }
                    
                    
                    
                    

                    // Start OF--Checking for Unique Rows
                    var totalcount = 0;
                    if (document.getElementById("TotRule") != null) {
                        totalcount = eval(document.getElementById("TotRule").value);
                    } //Total Count After Adding Rows
                    var chk = true;
                    var i = 0;
                    var j = 0;
                    for (i = 1; i <= totalcount; i++) //Loop For Newly Added Rows
                    {
                        if (document.getElementById("minValue_" + i) != null)
                            if ($.trim(document.getElementById("minValue_" + i).value) != '' && $.trim(document.getElementById("maxValue_" + i).value) != '' && $.trim(document.getElementById("noaAcceptDays_" + i).value) != '' && $.trim(document.getElementById("perSecSubDays_" + i).value) != '' && $.trim(document.getElementById("conSignDays_" + i).value) != '') // If Condition for When all Data are filled thiscode is Running
                            {
                                // bug id = 1931
                                var chkCount = 0;
                                for (var i = 1; i <= totalcount; i++) {
                                    var pN = $("#procNature_" + i).val();
                                    var tT = $("#procType_" + i).val();
                                    var pM = $("#procMethod_" + i).val();
                                    var minV = $("#minValue_" + i).val();
                                    var maxV = $("#maxValue_" + i).val();
                                    for (var j = i + 1; j <= totalcount; j++) {
                                        var pN2 = $("#procNature_" + j).val();
                                        var tT2 = $("#procType_" + j).val();
                                        var pM2 = $("#procMethod_" + j).val();
                                        var minV2 = $("#minValue_" + j).val();
                                        var maxV2 = $("#maxValue_" + j).val();
                                        if (pN == pN2 && tT == tT2 && pM == pM2 && minV == minV2 && maxV == maxV2) {
                                            chkCount++;
                                        }
                                    }
                                }
                                if (chkCount != 0) {
                                    jAlert("Duplicate record found. Please enter unique record", "Letter of Acceptance (LOA) Business Rule Configuration", function (RetVal) {
                                    });
                                    return false;
                                }

                                for (j = 1; j <= totalcount && j != i; j++) // Loop for Total Count but Not same as (i) value
                                {
                                    chk = true;
                                    //If Condition for Check Duplicate Rows are there or not.If Columns are diff then chk variable set to false
                                    //IF Row is same Give alert message.
                                    if (document.getElementById("procNature_" + i) != null && ($.trim(document.getElementById("procNature_" + i).value) != $.trim(document.getElementById("procNature_" + j).value)))
                                    {
                                        chk = false;
                                    }
                                    if (document.getElementById("procMethod_" + i) != null && ($.trim(document.getElementById("procMethod_" + i).value) != $.trim(document.getElementById("procMethod_" + j).value)))
                                    {
                                        chk = false;
                                    }
                                    if (document.getElementById("procType_" + i) != null && ($.trim(document.getElementById("procType_" + i).value) != $.trim(document.getElementById("procType_" + j).value)))
                                    {
                                        chk = false;
                                    }
                                    if (document.getElementById("minValue_" + i) != null && document.getElementById("minValue_" + j) != null) {
                                        var minvi = document.getElementById("minValue_" + i).value;
                                        var minvj = document.getElementById("minValue_" + j).value;
                                        if (minvi.indexOf(".") == -1)
                                        {
                                            minvi = minvi + ".00";
                                        }
                                        if (minvj.indexOf(".") == -1)
                                        {
                                            minvj = minvj + ".00";
                                        }
                                        if ($.trim(minvi) != $.trim(minvj))
                                        {
                                            chk = false;
                                        }
                                        var maxvi = document.getElementById("maxValue_" + i).value;
                                        var maxvj = document.getElementById("maxValue_" + j).value;
                                        if (maxvi.indexOf(".") == -1)
                                        {
                                            maxvi = maxvi + ".00";
                                        }
                                        if (maxvj.indexOf(".") == -1)
                                        {
                                            maxvj = maxvj + ".00";
                                        }
                                        if ($.trim(maxvi) != $.trim(maxvj))
                                        {
                                            chk = false;
                                        }
                                    } else {
                                        chk = false;
                                    }
//                                if($.trim(document.getElementById("noaAcceptDays_"+i).value) != $.trim(document.getElementById("noaAcceptDays_"+j).value))
//                                {
//                                    chk=false;
//                                }
//                                if($.trim(document.getElementById("perSecSubDays_"+i).value) != $.trim(document.getElementById("perSecSubDays_"+j).value))
//                                {
//                                    chk=false;
//                                }
//                                if($.trim(document.getElementById("conSignDays_"+i).value) != $.trim(document.getElementById("conSignDays_"+j).value))
//                                {
//                                    chk=false;
//                                }
                                    //alert(j);
                                    //alert("chk" +chk);
                                    if (flag) {
                                        if (chk == true) //If Row is same then give alert message
                                        {

                                            jAlert("Duplicate record found. Please enter unique record", "Letter of Acceptance (LOA) Business Rule Configuration", function (RetVal) {
                                            });
                                            return false;
                                        }
                                    }
                                }
                            }
                    }
                    // End OF--Checking for Unique Rows
                }

                if (flag == false) {
                    return false;
                }

            }

            function chkMinVal(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                //document.getElementById("MinValuewords_"+i).innerHTML = DoIt(document.getElementById("minValue_"+i).value);
                document.getElementById("MinValuewords_" + i).innerHTML = CurrencyConverter(document.getElementById("minValue_" + i).value);
                var chk = true;

                if (obj.value != '')
                {
                    if (decimal(obj.value))
                    {
                        if (!chkMax(obj.value))
                        {
                            document.getElementById("MinValue_" + i).innerHTML = "Maximum 12 digits are allowed";
                            chk = false;
                        } else
                        {
                            document.getElementById("MinValue_" + i).innerHTML = "";
                        }
                        //document.getElementById("minvalue_"+i).innerHTML="";
                    } else
                    {
                        document.getElementById("MinValue_" + i).innerHTML = "<br/>Please Enter Numbers and 2 digits after Decimal";
                        chk = false;
                    }
                } else
                {
                    document.getElementById("MinValue_" + i).innerHTML = "<br/>Please enter Minimum Value (In Nu.)";
                    chk = false;
                }
                if (chk == false)
                {
                    return false;
                }

            }
            ;

            function chkMaxVal(obj) {
                var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                //document.getElementById("MaxValuewords_"+i).innerHTML = DoIt(document.getElementById("maxValue_"+i).value);
                document.getElementById("MaxValuewords_" + i).innerHTML = CurrencyConverter(document.getElementById("maxValue_" + i).value);
                var chk1 = true;

                if (obj.value != '')
                {
                    if (decimal(obj.value))
                    {
                        if (!chkMax(obj.value))
                        {
                            document.getElementById("MaxValue_" + i).innerHTML = "Maximum 12 digits are allowed";
                            chk = false;
                        } else
                        {
                            document.getElementById("MaxValue_" + i).innerHTML = "";
                        }
                        // document.getElementById("maxvalue_"+i).innerHTML="";
                    } else
                    {
                        document.getElementById("MaxValue_" + i).innerHTML = "<br/>Please Enter Numbers and 2 digits after Decimal";
                        chk1 = false;
                    }
                } else
                {
                    document.getElementById("MaxValue_" + i).innerHTML = "<br/>Please enter Maximum Value (In Nu.)";
                    chk1 = false;
                }
                if (chk1 == false)
                {
                    return false;
                }

            }
            ;
            function chkNOAacceptance(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                var chk2 = true;

                if (obj.value != '')
                {
                    if (numeric(obj.value))
                    {
                        if (!chkMaxLength(obj.value))
                        {
                            document.getElementById("NOAacceptDays_" + i).innerHTML = "Maximum 3 digits are allowed";
                            chk2 = false;
                        } else
                        {
                            document.getElementById("NOAacceptDays_" + i).innerHTML = "";
                        }
                        //document.getElementById("MinNoDays_"+i).innerHTML="";
                    } else
                    {
                        document.getElementById("NOAacceptDays_" + i).innerHTML = "<br/>Please Enter numbers only";
                        chk2 = false;
                    }
                } else
                {
                    document.getElementById("NOAacceptDays_" + i).innerHTML = "<br/>Please enter  No. of days for Letter of Acceptance (LOA) acceptance";
                    chk2 = false;
                }
                if (chk2 == false)
                {
                    return false;
                }
            }
            ;
            function chkPerSecSubDays(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                var chk3 = true;

                if (obj.value != '')
                {
                    if (numeric(obj.value))
                    {
                        if (!chkMaxLength(obj.value))
                        {
                            document.getElementById("PerSecSubDays_" + i).innerHTML = "Maximum 3 digits are allowed";
                            chk3 = false;
                        } else
                        {
                            document.getElementById("PerSecSubDays_" + i).innerHTML = "";
                        }
                        //document.getElementById("MinNoDays_"+i).innerHTML="";
                    } else
                    {
                        document.getElementById("PerSecSubDays_" + i).innerHTML = "<br/>Please Enter numbers only";
                        chk3 = false;
                    }
                } else
                {
                    document.getElementById("PerSecSubDays_" + i).innerHTML = "<br/>Please enter No. of days for Performance Security submission";
                    chk3 = false;
                }
                if (chk3 == false)
                {
                    return false;
                }
            }
            ;
            
            function chkConSignDays(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                var chk4 = true;

                if (obj.value != '')
                {
                    if (numeric(obj.value))
                    {
                        if (!chkMaxLength(obj.value))
                        {
                            document.getElementById("ConSignDays_" + i).innerHTML = "Maximum 3 digits are allowed";
                            chk4 = false;
                        } 
                        else
                        {
                            document.getElementById("ConSignDays_" + i).innerHTML = "";
                        }
                        //document.getElementById("MinNoDays_"+i).innerHTML="";
                    } 
                    else
                    {
                        document.getElementById("ConSignDays_" + i).innerHTML = "<br/>Please Enter numbers only";
                        chk4 = false;
                    }
                } 
                else
                {
                    document.getElementById("ConSignDays_" + i).innerHTML = "<br/>Please enter No. of days for Performance Security submission";
                    chk4 = false;
                }
                if (chk4 == false)
                {
                    return false;
                }
            }
            ;


            function chkLoiNoa(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                var chk4 = true;

                if (obj.value != '')
                {
                    if (numeric(obj.value))
                    {
                        if (!chkMaxLength(obj.value))
                        {
                            document.getElementById("LoiNoa_" + i).innerHTML = "Maximum 3 digits are allowed";
                            chk4 = false;
                        }
                        /*else if(document.getElementById("LoiNoa_" + k).value == '0' || document.getElementById("LoiNoa_" + k).value == '00')
                        {
                            document.getElementById("LoiNoa_" + k).innerHTML = "<br/>Zero will not be allowed";
                            chk4 = false; 
                        }*/
                        else
                        {
                            document.getElementById("LoiNoa_" + i).innerHTML = "";
                        }
                    }
                    else
                    {
                        document.getElementById("LoiNoa_" + i).innerHTML = "<br/>Please Enter numbers only";
                        chk4 = false;
                    }
                } 
                else
                {
                    document.getElementById("LoiNoa_" + i).innerHTML = "<br/>Please enter No. of Days between LoI and Letter of Acceptance(LOA) for Performance Security submission";
                    chk4 = false;
                }
                if (chk4 == false)
                {
                    return false;
                }
            };




            function chkMax(value)
            {
                var chkVal = value;
                var ValSplit = chkVal.split('.');
                //alert(chkVal);
                //alert(ValSplit[0].length);
                if (ValSplit[0].length > 12)
                {
                    return false;
                } else
                {
                    return true;
                }
            }

            function chkMaxLength(value)
            {
                var ValueChk = value;
                //alert(ValueChk);

                if (ValueChk.length > 3)
                {
                    return false;
                } else
                {
                    return true;
                }
            }
            function decimal(value) {

                return /^(\d+(\.\d{2})?)$/.test(value);
            }
            ;

            function numeric(value) {
                return /^\d+$/.test(value);
            }
        </script>
        <script type="text/javascript">


            var counter = 1;
            var delCnt = 0;
            $(function () {
                $('#linkAddRule').click(function () {
                    counter = eval(document.getElementById("TotRule").value);
                    delCnt = eval(document.getElementById("delrow").value);

                    $('span.#lotMsg').css("visibility", "collapse");
                    var htmlEle = "<tr id='trrule_" + (counter + 1) + "'>" +
                            "<td class='t-align-center'><input type='checkbox' name='checkbox" + (counter + 1) + "' id='checkbox_" + (counter + 1) + "' value='" + (counter + 1) + "' style='width:90%;'/></td>" +
                            "<td class='t-align-center'><select name='procNature" + (counter + 1) + "' class='formTxtBox_1' id='procNature_" + (counter + 1) + "'><%=procNature%></select></td>" +
                            "<td class='t-align-center'><select name='procMethod" + (counter + 1) + "' class='formTxtBox_1' id='procMethod_" + (counter + 1) + "'><%=procMethod%></select></td>" +
                            "<td class='t-align-center'><select name='procType" + (counter + 1) + "' class='formTxtBox_1' id='procType_" + (counter + 1) + "'><%=procType%></select></td>" +
                            "<td class='t-align-center'><input name='minValue" + (counter + 1) + "' type='text' class='formTxtBox_1' maxlength='18' id='minValue_" + (counter + 1) + "' style='width:90%;' onblur='return chkMinVal(this);' /><span id='MinValue_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                            "<td class='t-align-center'><span id='MinValuewords_" + (counter + 1) + "' >&nbsp;</span></td>" +
                            "<td class='t-align-center'><input name='maxValue" + (counter + 1) + "' type='text' class='formTxtBox_1' maxlength='18' id='maxValue_" + (counter + 1) + "' style='width:90%;' onblur='return chkMaxVal(this);'/><span id='MaxValue_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                            "<td class='t-align-center'><span id='MaxValuewords_" + (counter + 1) + "' >&nbsp;</span></td>" +
                            "<td class='t-align-center'><input name='noaAcceptDays" + (counter + 1) + "' type='text' class='formTxtBox_1' id='noaAcceptDays_" + (counter + 1) + "' style='width:90%;' onblur='return chkNOAacceptance(this);' /><span id='NOAacceptDays_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                            "<td class='t-align-center'><input name='perSecSubDays" + (counter + 1) + "' type='text' class='formTxtBox_1' id='perSecSubDays_" + (counter + 1) + "' style='width:90%;' onblur='return chkPerSecSubDays(this);' /><span id='PerSecSubDays_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                            "<td class='t-align-center'><input name='conSignDays" + (counter + 1) + "' type='text' class='formTxtBox_1' id='conSignDays_" + (counter + 1) + "' style='width:90%;' onblur='return chkConSignDays(this);' /><span id='ConSignDays_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                            "<td class='t-align-center'><input name='loiNoa" + (counter + 1) + "' type='text' class='formTxtBox_1' id='loiNoa_" + (counter + 1) + "' style='width:90%;' onblur='return chkLoiNoa(this);' /><span id='LoiNoa_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                            "</tr>";
                    $("#tblrule").append(htmlEle);
                    document.getElementById("TotRule").value = (counter + 1);

                });
            });


            $(function () {
                $('#linkDelRule').click(function () {
                    counter = eval(document.getElementById("TotRule").value);
                    delCnt = eval(document.getElementById("delrow").value);

                    var tmpCnt = 0;
                    for (var i = 1; i <= counter; i++) {
                        if (document.getElementById("checkbox_" + i) != null && document.getElementById("checkbox_" + i).checked) {
                            tmpCnt++;
                        }
                    }

                    if (tmpCnt == (eval(counter - delCnt))) {
                        $('span.#lotMsg').css("visibility", "visible");
                        $('span.#lotMsg').css("color", "red");
                        $('span.#lotMsg').html('Minimum 1 record is needed!');
                    } else {
                        if (tmpCnt > 0) {
                            jConfirm('Are You Sure You want to Delete.', 'Procurement Method Business Rule Configuration', function (ans) {
                                if (ans) {
                                    for (var i = 1; i <= counter; i++) {
                                        if (document.getElementById("checkbox_" + i) != null && document.getElementById("checkbox_" + i).checked) {
                                            $("tr[id='trrule_" + i + "']").remove();
                                            $('span.#lotMsg').css("visibility", "collapse");
                                            delCnt++;
                                        }
                                    }
                                    document.getElementById("delrow").value = delCnt;
                                }
                            });
                        } else {
                            jAlert("please Select checkbox first", "Procurement Method Business Rule Configuration", function (RetVal) {});
                        }
                    }

                });
            });
        </script>

    </body>
</html>


