<%-- 
    Document   : NOABusinessRuleView
    Created on : Dec 29, 2010, 2:32:26 PM
    Author     : Naresh.Akurathi
--%>


<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigNoa"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<%@page import="java.util.Iterator"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Letter of Acceptance (LOA) Business Rules Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
           <script type="text/javascript">
                    $(document).ready(function() {
                        sortTable();
                    });
                </script>
        <script type="text/javascript">
            function conform()
            {
                if (confirm("Do you want to delete this business rule?"))
                    return true;
                else
                    return false;
            }
        </script>

        
    </head>
    <body>
        <%
            ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();
            String mesg =  request.getParameter("msg");
            String delMsg = "";
            if("delete".equals(request.getParameter("action"))){
                String action = "Remove NOA Rules";
                  try{
                    int noaId = Integer.parseInt(request.getParameter("id"));
                    TblConfigNoa tblNoa = new TblConfigNoa();
                    tblNoa.setConfigNoaId(noaId);
                    tblNoa.setConSignDays(Short.parseShort("1"));
                    tblNoa.setMaxValue(new BigDecimal(Byte.parseByte("1")));
                    tblNoa.setMinValue(new BigDecimal(Byte.parseByte("1")));
                    tblNoa.setNoaAcceptDays(Short.parseShort("1"));
                    tblNoa.setPerSecSubDays(Short.parseShort("1"));
                    tblNoa.setTblProcurementMethod(new TblProcurementMethod(Byte.parseByte("1")));
                    tblNoa.setTblProcurementNature(new TblProcurementNature(Byte.parseByte("1")));
                    tblNoa.setTblProcurementTypes(new TblProcurementTypes(Byte.parseByte("1")));

                    configPreTenderRuleSrBean.delConfigNoa(tblNoa);
                    delMsg="Letter of Acceptance (LOA) Business Rule Deleted Successfully";
                  }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                   }finally{
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                        action=null;
                   }
                }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
                        <div class="pageHead_1">Letter of Acceptance (LOA) Business Rules Configuration
                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('11');">Save as PDF</a></span>
                        </div>

                        <div>&nbsp;</div>

                          <%if("delete".equals(request.getParameter("action"))){%>
                          <div class="responseMsg successMsg"><%=delMsg%></div>
                          <%}%>

                          <%if(mesg != null){%>
                          <div class="responseMsg successMsg"><%=mesg%></div>
                          <%}%>
                          <div>&nbsp;</div>

                        <%--<div class="pageHead_1" align="center"> <%=msg%></div>--%>

                          
                                <table width="100%" cellspacing="0" class="tableList_1 t_space" name="resultTable" id="resultTable"  cols="@10">
                                    <tr>
                                        <th >Procurement  Category<br /></th>
                                        <th >Procurement  Method<br /></th>
                                        <th >Procurement  Type<br /></th>
                                        <th >Minimum Value<br />(In Nu.)<br /></th>
                                        <th >Minimum Value<br />(In Nu.)<br />words</th>
                                        <th >Maximum Value<br />(In Nu.)<br /></th>
                                        <th >Maximum Value<br />(In Nu.)<br />words</th>
                                        <th >No. of Days for <br /> Letter of Acceptance (LOA) acceptance <br /></th>
                                        <th >No. of Days for <br /> Performance Security<br /> submission <br /></th>
                                        <th >No. of Days for <br />Contract Signing <br /> from date of <br />issuance of Letter of Acceptance (LOA) <br /></th>
                                        <th >No. of Days between LoI and LOA</th>
                                        <th >Action<br /></th>
                                    </tr>
                                   <%
                                        String msg = "";
                                        List l = configPreTenderRuleSrBean.getAllConfigNoa();
                                        if(!l.isEmpty() || l != null){
                                            Iterator it = l.iterator();
                                            int i = 0;
                                            TblConfigNoa tblConfigNoa = new TblConfigNoa();
                                            while(it.hasNext()){
                                                tblConfigNoa = (TblConfigNoa)it.next();
                                                i++;
                                   
                                   %>
                                    <%if(i%2==0){%>
                                      <tr style='background-color:#E4FAD0;'>
                                          <%}else{%>
                                          <tr>
                                          <%}%>
                                        <td class="t-align-center"><%=tblConfigNoa.getTblProcurementNature().getProcurementNature()%></td>
                                        <td class="t-align-center"><%if(tblConfigNoa.getTblProcurementMethod().getProcurementMethod().equalsIgnoreCase("RFQ")){out.print("LEM");}else if(tblConfigNoa.getTblProcurementMethod().getProcurementMethod().equalsIgnoreCase("DPM")){out.print("DCM");}else{out.print(tblConfigNoa.getTblProcurementMethod().getProcurementMethod());}%></td>
                                        <td class="t-align-center"><%
                                            //ICT->ICB & NCT->NCB by Emtaz on 19/April/2016
                                            if(tblConfigNoa.getTblProcurementTypes().getProcurementType().equalsIgnoreCase("ICT"))
                                                    {out.print("ICB");} 
                                            else if(tblConfigNoa.getTblProcurementTypes().getProcurementType().equalsIgnoreCase("NCT"))
                                                    {out.print("NCB");} 
                                        %></td>
                                        <td class="t-align-center"><%=tblConfigNoa.getMinValue()%></td>
                                        <td class="t-align-center"><span  id="mininlakhs_<%=i%>"></span></td>
                                        <td class="t-align-center"><%=tblConfigNoa.getMaxValue()%></td>
                                        <td class="t-align-center"><span  id="maxinlakhs_<%=i%>"></span></td>
                                        <td class="t-align-center"><%=tblConfigNoa.getNoaAcceptDays()%></td>
                                        <td class="t-align-center"><%=tblConfigNoa.getPerSecSubDays()%></td>
                                        <td class="t-align-center"><%=tblConfigNoa.getConSignDays()%></td>
                                        <td class="t-align-center"><%=tblConfigNoa.getLoiNoa()%></td> 
                                        <td class="t-align-center"><a href="NOABusinessRuleDetails.jsp?action=edit&id=<%=tblConfigNoa.getConfigNoaId()%>">Edit</a>&nbsp;|&nbsp;<a href="NOABusinessRuleView.jsp?action=delete&id=<%=tblConfigNoa.getConfigNoaId()%>" onclick="return conform();">Delete</a></td>
                                    </tr>
                                </tr>
                              <script type="text/javascript">
                                $("#mininlakhs_"+<%=i%>).html(CurrencyConverter(<%=tblConfigNoa.getMinValue()%>));
                                $("#maxinlakhs_"+<%=i%>).html(CurrencyConverter(<%=tblConfigNoa.getMaxValue()%>));
                             </script>

                                    <%
                                            }
                                        } else{
                                              msg = "No Record Found";
                                          }

                                     %>
                                </table>
                               <form id="formstyle" action="" method="post" name="formstyle">

                                   <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                                   <%
                                     SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                                     String appenddate = dateFormat1.format(new Date());
                                   %>
                                   <input type="hidden" name="fileName" id="fileName" value="NOARule_<%=appenddate%>" />
                                    <input type="hidden" name="id" id="id" value="NOARule" />
                                </form>
                                <div>&nbsp;</div>
                                <div align="center"> <%=msg%></div>
                                
                                                         
                    </td>
                </tr>
            </table>


            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>
         <script>
                var obj = document.getElementById('lblNOARuleView');
                if(obj != null){
                    if(obj.innerHTML == 'View'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>

