<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.MediaContentService"%>
<%@page import="com.cptu.egp.eps.model.table.TblMediaContent"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <%
                String strUserTypeId = "";
                Object objUserId = session.getAttribute("userId");
                Object objUName = session.getAttribute("userName");
                boolean isLoggedIn = false;
                if (objUserId != null) {
                    strUserTypeId = session.getAttribute("userTypeId").toString();
                }
                if (objUName != null) {
                    isLoggedIn = true;
                }

                String strMediaId = request.getParameter("MediaId");
                String strMediaSubject = "";
                String strMediaDetail = "";
                String strMediaType = "";
                String strScreenTitle = "";
                MediaContentService mediaContentService = (MediaContentService) AppContext.getSpringBean("MediaContentService");
                mediaContentService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()));

                List<TblMediaContent> colMediaDetail = mediaContentService.getMediaDetail(strMediaId);
                for (TblMediaContent tblMediaContent : colMediaDetail) {
                    strMediaSubject = tblMediaContent.getContentSub();
                    strMediaDetail = tblMediaContent.getContentText();
                    strMediaType = tblMediaContent.getContentType();
                }
                if (strMediaType.equals("Press")) {
                    strScreenTitle = "View Press Release Detail";
                } else if (strMediaType.equals("Complaint")) {
                    strScreenTitle = "View Complaint Detail";
                } else {
                    strScreenTitle = "View Other Information Detail";
                }
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=strScreenTitle%></title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript"></script>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/simpletreemenu.js"></script>


        <script type="text/javascript">


            function goBack()
            {
                var vlink;
                var vUserTypeId = "<%=strUserTypeId%>";
                if(vUserTypeId == "1")
                {
                    vLink = "<%=request.getContextPath()%>/admin/MediaContList.jsp?contentType="+"<%=strMediaType%>";
                    window.location=vLink;
                    return true;
                }
                else
                {
                    vLink = "<%=request.getContextPath()%>/tenderer/MediaContList.jsp?contentType="+"<%=strMediaType%>";
                    window.location=vLink;
                    return true;
                }
            }
        </script>

    </head>
    <body>
        <form id="alltenderFrm" method="post">
            <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <div class="topHeader">
                    <%
                                if (objUName != null) {
                    %>
                    <div class="dashboard_div">
                        <%@include file="../resources/common/AfterLoginTop.jsp" %> <%} else {%>
                        <div class="fixDiv">

                            <jsp:include page="../resources/common/Top.jsp" ></jsp:include> <%}%>
                        </div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr valign="top">

                                <%if (strUserTypeId.equals("1")) {%>
                                <td>
                                    <jsp:include page="../resources/common/AfterLoginLeft.jsp" ></jsp:include>
                                </td>
                                <%}%>
                                <td class="contentArea-Blogin">
                                    <div class="DashboardContainer">
                                        <div class="pageHead_1"><%=strScreenTitle%> <span class="c-alignment-right"><a href="#"  onclick="return goBack()" title="Go Back" class="action-button-goback">Go Back</a></span></div>
                                        <table width="100%" cellspacing="8" class="formStyle_1 t_space">
                                            <tr>
                                                <td width="8%" class="ff">Subject :</td>
                                                <td width="92%"><%=strMediaSubject%></td>
                                            </tr>
                                            <tr>
                                                <td width="8%" class="ff">Detail :</td>
                                                <td><%=strMediaDetail%></td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr></table></form>
                        <!--Dashboard Content Part End-->

                        <!--Dashboard Footer Start-->
                        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                        <!--Dashboard Footer End-->
                    </div>
                </div>
                </body>
                </html>
