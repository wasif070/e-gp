<%--
    Document   : PromiseIndicator.jsp
    Created on : Jan 20, 2012, 11:20:54 AM
    Author     : shreyansh.shah
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.AppMISService"%>
<%@page import="com.cptu.egp.eps.model.table.TblStateMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblFinancialYear"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.FinancialYearService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    FinancialYearService financialYearService = (FinancialYearService) AppContext.getSpringBean("financialYearService");
                    int userid = 0;

                    HttpSession hs = request.getSession();

                    if (hs != null) {
                        if (hs.getAttribute("userId") != null) {
                            userid = Integer.parseInt(hs.getAttribute("userId").toString());
                        }
                    } else {
                        userid = Integer.parseInt(request.getParameter("userId").toString());
                    }

                    boolean isPDF = false;
                    if (request.getParameter("isPDF") != null) {
                        isPDF = true;
                    }
                    int uTypeId = 0;
                    String suserId = null;
                    AppMISService mISService = (AppMISService) AppContext.getSpringBean("AppMISService");
                    CommonService cservice = (CommonService) AppContext.getSpringBean("CommonService");
                    //  System.out.println("isPDF value :"+isPDF);
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Promise Indicator</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <link type="text/css" rel="stylesheet" href="../resources/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/lang/en.js"></script>
        <script type="text/javascript">
            /* Call Print function */

            $(document).ready(function() {
                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });
            function printElem(options){
                //$('#titleDiv').show();
                $('#print_div').printElement(options);
                //$('#titleDiv').hide();
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%if (!isPDF) {
             suserId = session.getAttribute("userId").toString();%>
            
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <%uTypeId = Integer.parseInt(objUserTypeId.toString());
                } else {
                    uTypeId = Integer.parseInt(request.getParameter("uTypeId"));
                    suserId = request.getParameter("suserId");
            }%>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <%if (!isPDF) {%>
                <div class="pageHead_1" id="Mainheading">1. Advertisement of Tender/Proposal Opportunities in Newspaper</div>
                <div>&nbsp;</div>
                <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">- Collapse</a></div>
                <% }%>
                <div class="formBg_1">
                    <jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>
                    <table id="tblSearchBox" cellspacing="10" class="formStyle_1" width="100%" <%if (isPDF) {%> style="display: none" <% }%>>
                        <tr>
                            <td class="ff">Performance Indicator : </td>
                            <td>
                                <select name="PEId" class="formTxtBox_1" id="PEId" style="width:auto;">
                                    <option value="1" selected>1. Advertisement of Tender/Proposal Opportunities in Newspaper</option>
                                    <!--Change CPTU to GPPMD by Proshanto-->
                                    <option value="2">2. Advertisement of Tender/Proposal Opportunities in GPPMD's Website</option>
                                    <!--Change GoB to RGoB by Proshanto-->
                                    <option value="3">3. Tenders/Proposals Following RGoB Procurement Rules</option>
                                    <option value="4">4. Tenders/Proposals Following Development Partner Rules</option>
                                    <option value="5">5. Multiple Locations Submission Tenders/Proposals</option>
                                    <option value="6">6. Tender/Proposal Preparation Time in Open Tendering Method</option>
                                    <option value="7">7. Tender/Proposal Time Compliance</option>
                                    <option value="8">8. Sale of Tender/Proposal Documents</option>
                                    <option value="9">9. Bidder/Consultant Participation</option>
                                    <option value="10">10. Bidder/Consultant Participation Index</option>
                                    <option value="11">11. Tender/Proposal Opening Committee Formation</option>
                                    <option value="12">12. Tender/Proposal Evaluation Committee Formation</option>
                                    <option value="13">13. External Member in TEC</option>
                                    <option value="14">14. Tender/Proposal Evaluation Time</option>
                                    <option value="15">15. Compliance of Tender/Proposal Evaluation Time</option>
                                    <option value="16">16. Tender/Proposal Acceptance</option>
                                    <option value="17">17. Re-Tendering</option>
                                    <option value="18">18. Tender/Proposal Cancellation</option>
                                    <option value="19">19. Tender/Proposal Evaluation Approval Time</option>
                                    <option value="20">20. Compliance of Financial Delegation</option>
                                    <option value="21">21. Submission of Evaluation Report to Appropriate Authority</option>
                                    <option value="22">22. TER Approval Compliance</option>
                                    <option value="23">23. Additional Review of TER</option>
                                    <option value="24">24. Higher Tier Approval</option>
                                    <option value="25">25. Time for Issuance of LOA to Bidder/Consultant</option>
                                    <option value="26">26. Tender/Proposal Processing Lead Time</option>
                                    <option value="27">27. Total Tender/Proposal Processing Time</option>
                                    <option value="28">28. Publication of Award Information</option>
                                    <option value="29">29. Efficiency in Contract Award</option>
                                    <option value="30">30. Delivery Time</option>
                                    <option value="31">31. Liquidated Damage</option>
                                    <option value="32">32. Completion Rate</option>
                                    <option value="33">33. Payment Release Compliance</option>
                                    <option value="34">34. Late Payment</option>
                                    <option value="35">35. Interests Paid for Delayed Payment</option>
                                    <option value="36">36. Tender/Proposal Procedure Complaints</option>
                                    <option value="37">37. Resolution of Complaints With Award Modification</option>
                                    <option value="38">38. Resolution of Complaints</option>
                                    <option value="39">39. Independent Review Panel</option>
                                    <option value="40">40. Contract Amendments/Variations</option>
                                    <option value="41">41. Unresolved Disputes</option>
                                    <option value="42">42. Fraud and Corruption</option>
                                    <option value="43">43. Procurement Training</option>
                                    <option value="44">44. Percentage of Procurement Entities with Trained Persons</option>
                                    <option value="45">45. Number of Procurement Persons by Organization</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Financial Year :</td>
                            <td>
                                <select name="procNature" id="procNature" class="formTxtBox_1" id="select2" style="width:208px;" onchange="changeQuater();">
                                    <option value="-" selected="selected">All</option>
                                    <%
                                                List<TblFinancialYear> list = new ArrayList<TblFinancialYear>();
                                                try {
                                                    list = financialYearService.getFinancialYearDetails(); //calling the Service mehod the get the details.
                                                } catch (Exception e) {
                                                }
                                                for (TblFinancialYear tblfyear : list) {
                                    %>
                                    <option value="<%=tblfyear.getFinancialYear()%>"> <% out.println(tblfyear.getFinancialYear());%> </option>
                                    <% }%>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Procurement Type : </td>
                            <td>
                                <select name="procType" class="formTxtBox_1" id="cmbType" style="width:208px;">
                                    <!--Change NCT to NCB and ICT to ICB by Proshanto-->
                                    <option value="-">-- Select Type --</option>
                                    <option value="NCT">NCB</option>
                                    <option value="ICT">ICB</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Select Ministry/Division/Organization/PA :</td>
                            <td colspan="3">
                             <%if (uTypeId == 5) {                                             
                                                Object[] objData = mISService.getOrgAdminOrgName(Integer.parseInt(suserId));
                                                out.print(objData[1].toString());
                                                out.print("<input type=\"hidden\" id=\"txtdepartment\" name=\"depName\" value=\"" + objData[1].toString() + "\"/>");
                                                out.print("<input type=\"hidden\" id=\"txtdepartmentid\" name=\"depId\" value=\"" + objData[0] + "\"/>");
                                                out.print("<input type=\"hidden\" id=\"uTypeId\" name=\"uTypeId\" value=\"" + uTypeId + "\"/>");
                            }
                            else if (uTypeId == 3){
                                                Object[] objData = mISService.getOrgAdminOrgNameByUserId(Integer.parseInt(suserId));
                                                out.print(objData[1].toString());
                                                out.print("<input type=\"hidden\" id=\"txtdepartment\" name=\"depName\" value=\"" + objData[1].toString() + "\"/>");
                                                out.print("<input type=\"hidden\" id=\"txtdepartmentid\" name=\"depId\" value=\"" + objData[0] + "\"/>");
                                                out.print("<input type=\"hidden\" id=\"uTypeId\" name=\"uTypeId\" value=\"" + uTypeId + "\"/>");

                            }else{%>
                                <input type="hidden" name="viewType" id="viewType" value="Live"/>
                                <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 300px;"
                                       id="txtdepartment"  readonly style="width: 200px;"/>
                                <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />
                                <input type="hidden" name="uTypeId" id="uTypeId" value="<%=uTypeId%>" />

                                <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=homeallTenders', '', 'width=350px,height=400px,scrollbars=1','');">
                                    <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                </a>
                                <%}%>
                            </td>

                        </tr>
                        <%
                         List<Object[]> listProject = cservice.checkPD(String.valueOf(userid));
                        if(listProject.size()>0)
                            {%>
                            <tr>
                                 <td width="20%" class="ff" align="left">Project :</td>
                                 <td width="30%">
                                    <select name="cmbProject" class="formTxtBox_1" id="cmbProject" style="width:95%;" onchange="loadOfficeForProject();">
                                    <option value="">-- Select --</option>
                                    <%
                                    for(Object[] project:listProject)
                                        {
                                          out.println("<option value='" + project[1].toString() + "'>" + project[0].toString() + "</option>");
                                        }
                                    %>
                                    </select>
                                 </td>
                            </tr>
                           <% }else{ %>
                           <select name="cmbProject" class="formTxtBox_1" id="cmbProject" style="width:95%;display: none;">
                                    <option value="">-- Select --</option>
                           </select> <%}%>
                        <tr>
                           <!--Change Entity to Agency by Proshanto-->
                            <td class="ff">Procuring Entity :</td>
                            <td colspan="3">
                                <select name="office" class="formTxtBox_1" id="cmbOffice" style="width:305px;">
                                    <option value="0">-- Select Office --</option>
                                     <%
                                      List<Object[]> listProjectOffice = cservice.getProjectOffice(String.valueOf(userid));
                                                
                                                  if ((uTypeId == 5 || uTypeId == 3) && listProject.size()==0) {
                                                    if(listProjectOffice.size()>0){
                                                     for(Object[] projectOff:listProjectOffice){
                                                        out.print("<option value='" + projectOff[0].toString() + "'>" +  projectOff[1].toString() + "</option>");
                                                        }
                                                    }else{
                                                    List<Object[]> cmbLists = null;
                                                    cmbLists = mISService.getOfficeList(suserId, "" + uTypeId);
                                                    for (Object[] cmbList : cmbLists) {
                                                        String selected = "";
                                                        if (request.getParameter("offId") != null) {
                                                            if (cmbList[0].toString().equals(request.getParameter("offId"))) {
                                                                selected = "selected";
                                                            }
                                                        }
                                                        out.print("<option value='" + cmbList[0] + "' " + selected + ">" + cmbList[1] + "</option>");
                                                        selected = null;
                                                    }
                                                    if (cmbLists.isEmpty()) {
                                                        out.print("<option value='0'> No Office Found.</option>");
                                                    }
                                                }
                                              }
                                            %>
                                   
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" class="ff">Dzongkhag / District : </td>
                            <%
                                //Code by Proshanto
                                //short countryId = 136;
                                short countryId = 150;
                                List<TblStateMaster> liststate = cservice.getState(countryId);
                            %>
                            <td width="30%">
                                <select name="cmbDistrict" class="formTxtBox_1" id="cmbDistrict" style="width:95%;">
                                    <option value="">-- Select --</option>
                                    <%
                                        for (TblStateMaster state : liststate) {
                                            out.println("<option value='" + state.getStateId() + "'>" + state.getStateName() + "</option>");
                                        }
                                    %>
                                </select>
                                <span id="msgdistrict" class="reqF_1"></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="15%" class="ff" align="left">By Quarter :</td>
                                    <td width="35%" align="left">
                                        <select name="quater" id="quater" disabled style="width:200px;" class="formTxtBox_1" onchange="setQuaterName()">
                                            <option value="2012-07-1_2013-06-30">All Four Quarter</option>
                                            <option value="2012-07-1_2012-09-30">Q1</option>
                                            <option value="2012-10-1_2012-12-31">Q2</option>
                                            <option value="2013-01-1_2013-03-31">Q3</option>
                                            <option value="2013-04-1_2013-06-30">Q4</option>
                                            <option value="2012-07-1_2012-12-31">Q1 and Q2</option>
                                            <option value="2012-10-1_2013-03-31">Q2 and Q3</option>
                                            <option value="2013-01-1_2013-06-30">Q3 and Q4</option>
                                            <option value="2012-07-1_2013-03-31">Q1 and Q2 and Q3</option>
                                            <option value="2012-10-1_2013-06-30">Q2 and Q3 and Q4</option>
                                        </select>
                                        <input type="hidden" name="quatername" id="quatername"/>
                                    </td>
                        </tr>
                        
                        <tr>
                            <td colspan="4" align="center"><label class="formBtn_1">
                                    <input type="submit" name="button" id="btnSearch" value="Search" />
                                </label>
                                &nbsp;
                                <label class="formBtn_1">
                                    <input type="reset" name="Reset" id="btnReset" value="Reset" />
                                </label></td>
                                <%if (!isPDF) {%>
                            <td class="t-align-right">
                                <%
                                    String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
                                    String folderName = pdfConstant.PROMISREPORT;
                                    String genId = cdate + "_" + userid;
                                %>
                                <a class="action-button-savepdf"
                                   href="<%=request.getContextPath()%>/GeneratePdf?reqURL=&reqQuery=&folderName=<%=folderName%>&id=<%=genId%>" >Save As PDF </a>
                                &nbsp;
                                <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
                            </td>
                            <% }%>
                        </tr>
                        <tr>
                            <td class="t-align-center" colspan="4">
                                <div id="valAll" class="reqF_1"></div>
                            </td>
                        </tr>
                    </table>

                </div>

                <div id="print_div">
                <div class="tableHead_1 t_space" id="getSubHeading">
                    <!--1. Percentage of Invitation for Tender/Proposal (IFT) Published in Newspaper-->
                </div>

                <div class="tabPanelArea_1">
                    <table width="100%" cellspacing="0" class="tableList_1" id="resultTable">
                        <div id="progressBar" class="tableHead_1 t_space" align="center" style="visibility:hidden"  >
                             <img id="theImg" alt="ddd" src="<%=request.getContextPath()%>/resources/images/loading.gif"/>
                         </div>
                        <tr id="subtableheading">

                        </tr>

                    </table>
                    <!--                    <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                                            <tr>
                                                <td align="left">Page <span id="pageNoTot">1</span> - <span id="pageTot">10</span></td>
                                                <td align="center"><center><input name="textfield3" onkeypress="checkKeyGoTo(event);" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                        &nbsp;
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                                        </label></center></td>
                                                <td class="prevNext-container">
                                                    <ul>
                                                        <li>&laquo; <a href="javascript:void(0)" id="btnFirst">First</a></li>
                                                        <li>&#8249; <a href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                                        <li><a href="javascript:void(0)" id="btnNext">Next</a> &#8250;</li>
                                                        <li><a href="javascript:void(0)" id="btnLast">Last</a> &raquo;</li>
                                                    </ul>
                                                </td>
                                            </tr>
                                        </table>-->
                    <div align="center">
                        <input type="hidden" id="pageNo" value="1"/>
                        <input type="hidden" name="size" id="size" value="1000"/>
                    </div>
                </div>
                <div>&nbsp;</div>
                        </div>
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <%if (!isPDF) {%>
            <%@include file="../resources/common/Bottom.jsp" %>
            <%}%>
            <!--Dashboard Footer End-->

        </div>
    </body>
    <script type="text/javascript">
        function loadProjectByOffice()
        {
            
        }

        function changeQuater(){
                var cmb = document.getElementById('procNature');
                if($('#procNature').val()=='-'){
                    document.getElementById("quater").disabled = true;}
                else{

                document.getElementById("quater").disabled = false;
                var cval1 = cmb.value.split("-")[0];
                var cval2 = cmb.value.split("-")[1];
//                var qstring = "<option value='0'>Please Select Quarter</option><option value='"+cval1+"-07-1_"+cval1+"-09-30'>Q1</option><option value='"+cval1+"-10-1_"+cval1+"-12-31'>Q2</option><option value='"+cval2+"-01-1_"+cval2+"-03-31'>Q3</option><option value='"+cval2+"-04-1_"+cval2+"-06-30'>Q4</option><option value='"+cval1+"-07-1_"+cval1+"-12-31'>Q1 and Q2</option><option value='"+cval1+"-10-1_"+cval2+"-03-31'>Q2 and Q3</option><option value='"+cval2+"-01-1_"+cval2+"-06-30'>Q3 and Q4</option><option value='"+cval1+"-07-1_"+cval2+"-03-31'>Q1 and Q2 and Q3</option><option value'"+cval1+"-10-1_"+cval2+"-06-30'>Q2 and Q3 and Q4</option><option value='"+cval1+"-07-1_"+cval2+"-06-30'>All</option>";
                var qstring = "<option value='"+cval1+"-07-1_"+cval2+"-06-30'>All Four Quarter</option><option value='"+cval1+"-07-1_"+cval1+"-09-30'>Q1</option><option value='"+cval1+"-10-1_"+cval1+"-12-31'>Q2</option><option value='"+cval2+"-01-1_"+cval2+"-03-31'>Q3</option><option value='"+cval2+"-04-1_"+cval2+"-06-30'>Q4</option><option value='"+cval1+"-07-1_"+cval1+"-12-31'>Q1 and Q2</option><option value='"+cval1+"-10-1_"+cval2+"-03-31'>Q2 and Q3</option><option value='"+cval2+"-01-1_"+cval2+"-06-30'>Q3 and Q4</option><option value='"+cval1+"-07-1_"+cval2+"-03-31'>Q1 and Q2 and Q3</option><option value='"+cval1+"-10-1_"+cval2+"-06-30'>Q2 and Q3 and Q4</option>";
                //var qcombo1 = document.getElementById('quater').options[1].value.substring(0, 4);
                //var qcombo2 = document.getElementById('quater').options[3].value.substring(0, 4);
                $("#quater").html(qstring);
                }
            }
       function setQuaterName(){
                var cmb = document.getElementById('quater');
                $("#quatername").val(cmb.options[cmb.selectedIndex].text);
            }
       function validate(){
                $(".err").remove();
                var cnt=0;
                if($('#procNature').val()=='-'){
                    $('#procNature').parent().append("<div class='err' style='color:red;'>Please select Financial Year</div>");
                    cnt++;
                }
                else
                    loadTenderTable();
                   
                    
                return (cnt==0);
            }
    </script>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <script type="text/javascript">
        function GetCal(txtname,controlname)
        {
            new Calendar({
                inputField: txtname,
                trigger: controlname,
                showTime: false,
                dateFormat:"%d/%m/%Y",
                onSelect: function() {
                    var date = Calendar.intToDate(this.selection.get());
                    LEFT_CAL.args.min = date;
                    LEFT_CAL.redraw();
                    this.hide();
                }
            });

            var LEFT_CAL = Calendar.setup({
                weekNumbers: false
            })
        }
    </script>
    <script type="text/javascript">
        function chkdisble(pageNo){
            $('#dispPage').val(Number(pageNo));
            if(parseInt($('#pageNo').val(), 10) != 1){
                $('#btnFirst').removeAttr("disabled");
                $('#btnFirst').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnFirst').attr("disabled", "true");
                $('#btnFirst').css('color', 'gray');
            }


            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnPrevious').attr("disabled", "true")
                $('#btnPrevious').css('color', 'gray');
            }

            if(parseInt($('#pageNo').val(), 10) > 1){
                $('#btnPrevious').removeAttr("disabled");
                $('#btnPrevious').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnLast').attr("disabled", "true");
                $('#btnLast').css('color', 'gray');
            }

            else{
                $('#btnLast').removeAttr("disabled");
                $('#btnLast').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnNext').attr("disabled", "true")
                $('#btnNext').css('color', 'gray');
            }
            else{
                $('#btnNext').removeAttr("disabled");
                $('#btnNext').css('color', '#333');
            }
        }
    </script>
    <script type="text/javascript">
        function changeTab(tabNo){
          //  loadTenderTableone();
        }
         
    </script>
    <script type="text/javascript">
        function loadOffice() {
            var deptId=$('#txtdepartmentid').val();
            $.post("<%=request.getContextPath()%>/ComboServlet", {departmentId: deptId,funName:'officeComboforpromis'},  function(j){
                $('#cmbOffice').children().remove().end()
                $("select#cmbOffice").html(j);
            });
        }
        function loadOfficeForProject() {
            var projectId=$("#cmbProject").val();
            $.post("<%=request.getContextPath()%>/ComboServlet", {projectId: projectId,funName:'officeComboforproject'},  function(j){
                $('#cmbOffice').children().remove().end()
                $("select#cmbOffice").html(j);
            });
        }


    </script>
    <!-- AJAX Grid Functions Start -->
    <script type="text/javascript">
        $(function() {
            var count;
            $('#btnSearch').click(function() {
                loadTenderTable();
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnReset').click(function() {
                $("#pageNo").val("1");
                $("#refNo").val('');
                $("#tenderId").val('');
              //  $("#txtdepartmentid").val('0');
                if(($('#uTypeId').val() != '5') && ($('#uTypeId').val() != '3' )){
                    $('#cmbOffice').empty().append('<option value="0">-- Select Office --</option>');
                    $("#txtdepartmentid").val('0');
                    $("#txtdepartment").val('');
                    }
                
                //$("#PEId").val('');
                
                $("select#PEId option[selected]").removeAttr("selected");
                $("select#PEId option[value='1']").attr("selected", "selected");
                $("select#procNature option[selected]").removeAttr("selected");
                $("select#procNature option[value='-']").attr("selected", "selected");
                $("select#cmbType option[selected]").removeAttr("selected");
                $("select#cmbType option[value='-']").attr("selected", "selected");
                $(".err").remove();
                //$("#cmbProcMethod").val('');
                $("#pubDtFrm").val('');
                $("#pubDtTo").val('');
                $("#cmbDistrict").val('');
                $("select#quater")[0].selectedIndex = 0;
                $("#getSubHeading").html("");
                $("#subtableheading").html("");
                $("#resultTable").hide();


              // loadTenderTable();
            });
        });
    </script>

    <script type="text/javascript">
        function loadTenderTableone()
        {
            //  alert('abc');
            $.post("<%=request.getContextPath()%>/PromiseReportServlet", {
                fyear :$("#statusTab").val(),
                ptype: "Ministry",
                deptid:$("#statusTab").val(),
                officeid: $("#status").val(),
                PEId : $("#PEId").val(),
                pageNo: $("#pageNo").val(),
                size: $("#size").val(),
                isPDF:'<%=isPDF%>'
            },
            function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);


                if($('#noRecordFound').attr('value') == "noRecordFound"){
                    $('#pagination').hide();
                }else{
                    $('#pagination').show();
                }
                chkdisble($("#pageNo").val());
                if($("#totalPages").val() == 1){
                    $('#btnNext').attr("disabled", "true");
                    $('#btnLast').attr("disabled", "true");
                }else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnLast').removeAttr("disabled");
                }
                $("#pageTot").html($("#totalPages").val());
                $("#pageNoTot").html($("#pageNo").val());
                $('#resultDiv').show();

                var counter = $('#cntTenBrief').val();
                for(var i=0;i<counter;i++){
                    if($('#tenderBrief_'+i).html() != null){
                        //alert($('#tenderBrief_'+i).html());
                        //alert($('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, ''));
                        var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                        var temp1 = $('#tenderBrief_'+i).html();
                        if(temp.length > 250){
                            temp = temp1.substr(0, 250);
                            $('#tenderBrief_'+i).html(temp+'...');
                        }
                    }
                }
            });
        }

        /**
         *  In PROMIS Indicator columns are changing based on indicator. Here we have created function which will display
         *  column heading based on indicator selected by user from listbox.
         *  @param peid
         *  @return list of columns for selected indicator id
         *  Note : For details of columns heading for each indicator you can refer report format given by Dohatec
         */
        function getTableHeading()
        {
            var tableheadings = '';
            var PromiseIndicatorId = 0;
            PromiseIndicatorId = $("#PEId").val();
            if (PromiseIndicatorId == 1) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Published Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">No. of Tenders/Proposals Published in Newspaper</th>'+
                    '<th width="10%" class="t-align-center">Percentage of Tenders/Proposals Published in Newspaper</th>'
            } else if (PromiseIndicatorId == 2) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Published Tender/Proposal</th>'+
                    //Change CPTU to GPPMD by Proshanto
                    '<th width="15%" class="t-align-center">No. of Tenders/Proposals Published in GPPMD Website</th>'+
                    //Change CPTU to GPPMD by Proshanto
                    '<th width="10%" class="t-align-center">Percentage of Tenders/Proposals Published in GPPMD Website</th>'
            } else if (PromiseIndicatorId == 3) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal Following Rules</th>'+
                    //Change GoB to RGoB by Proshanto
                    '<th width="15%" class="t-align-center">Tenders/Proposals Following RGoB Procurement Rules</th>'+
                    '<th width="20%" class="t-align-center">Tenders/Proposals Following RGoB Procurement Rules %</th>'
            } else if (PromiseIndicatorId == 4) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal Following Rules</th>'+
                    '<th width="15%" class="t-align-center">Tenders/Proposals Following Development Partner Rules</th>'+
                    '<th width="20%" class="t-align-center">Tenders/Proposals Following  Development Partner Rules %</th>'
            } else if (PromiseIndicatorId == 5) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal </th>'+
                    '<th width="15%" class="t-align-center">Tenders/Proposals Submission in Multiple Location</th>'+
                    '<th width="20%" class="t-align-center">Tenders/Proposals Submission in Multiple Location %</th>'
            } else if (PromiseIndicatorId == 6) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="10%" class="t-align-center">Total Published Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Total Days (Submission-Advertisement)</th>'+
                    '<th width="15%" class="t-align-center">Average No. of Days (Submission-Advertisement)</th>'
            } else if (PromiseIndicatorId == 7) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Having Sufficient Tender/Proposal Submission Time</th>'+
                    '<th width="20%" class="t-align-center">Having Sufficient Tender/Proposal Submission Time(%)</th>'
            } else if (PromiseIndicatorId == 8) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">No. of Tenderers/Consultants Purchased Tender/Proposal Documents</th>'+
                    '<th width="20%" class="t-align-center">Avg No. of Tenderers/Consultants Purchased Tender/Proposal Documents</th>'
            } else if (PromiseIndicatorId == 9) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">No of Tenderers/Consultants Submitted Tenders/Proposals</th>'+
                    '<th width="20%" class="t-align-center">Avg No of Tenderers/Consultants Submitted Tenders/Proposals</th>'
            } else if (PromiseIndicatorId == 10) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Tender/Proposal Documents Sold</th>'+
                    '<th width="15%" class="t-align-center">Tenderers/Consultants Submitted Tenders/Proposals</th>'+
                    '<th width="20%" class="t-align-center">Ratio of No. of Tender/Proposal Document Sold and No. of Participants</th>'
            } else if (PromiseIndicatorId == 11) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">TOC Included at Least One Member from PEC/TEC</th>'+
                    '<th width="20%" class="t-align-center">TOC Included at Least One Member from PEC/TEC(%)</th>'
            } else if (PromiseIndicatorId == 12) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">TEC Formed by Approving Authority</th>'+
                    '<th width="20%" class="t-align-center">TEC Formed by Approving Authority (%)</th>'
            } else if (PromiseIndicatorId == 13) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">TEC Included Two External Members</th>'+
                    '<th width="20%" class="t-align-center">TEC Included 2 External Members (%)</th>'
            } else if (PromiseIndicatorId == 14) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="20%" class="t-align-center">Total Evaluation Completed Tender/Proposal </th>'+
                    '<th width="15%" class="t-align-center">Total Days (Evaluation-Opening)</th>'+
                    '<th width="15%" class="t-align-center">Average No. of Days (Evaluation-Opening)</th>'
            } else if (PromiseIndicatorId == 15) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Approved Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Approval Completed within Timeline</th>'+
                    '<th width="20%" class="t-align-center">Approval Completed within Timeline (%)</th>'
            } else if (PromiseIndicatorId == 16) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="9%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="9%" class="t-align-center">Total Evaluation Completed Tender/Proposal </th>'+
                    '<th width="9%" class="t-align-center">Total Participants</th>'+
                    '<th width="9%" class="t-align-center">Total Responsive</th>'+
                    '<th width="9%" class="t-align-center">Average Participants</th>'+
                    '<th width="9%" class="t-align-center">Average Responsive</th>'
            } else if (PromiseIndicatorId == 17) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">TEC Recommended Re-Tendering</th>'+
                    '<th width="20%" class="t-align-center">TEC Recommended Re-Tendering (%)</th>'
            } else if (PromiseIndicatorId == 18) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Total Cancelled Tender/Proposal</th>'+
                    '<th width="20%" class="t-align-center">Cancelled Tender/Proposal(%)</th>'
            } else if (PromiseIndicatorId == 19) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="20%" class="t-align-center">Total Submitted Tender/Proposal for Approval</th>'+
                    '<th width="15%" class="t-align-center">Total Days (Evaluation to Approval)</th>'+
                    '<th width="15%" class="t-align-center">Average No. of Days (Approval-Evaluation)</th>'
            } else if (PromiseIndicatorId == 20) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Approved by Financial Delegated Authority</th>'+
                    '<th width="20%" class="t-align-center">Approved by Financial Delegated Authority (%)</th>'
            } else if (PromiseIndicatorId == 21) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Report Submitted to the Contract Approving Authority</th>'+
                    '<th width="15%" class="t-align-center">TEC Submitted Report Directly to the Contract Approving Authority</th>'+
                    '<th width="20%" class="t-align-center">TEC Submitted Report Directly to the Contract Approving Authority(%)</th>'
            } else if (PromiseIndicatorId == 22) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Contract Award Decision Made within Timeline</th>'+
                    '<th width="20%" class="t-align-center">Contract Award Decision Made within Timeline(%)</th>'
            } else if (PromiseIndicatorId == 23) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Report Submitted to the Contract Approving Authority</th>'+
                    '<th width="15%" class="t-align-center">TER Reviewed by Person/Commettee Besides Contract Approving Authority</th>'+
                    '<th width="20%" class="t-align-center">TER Reviewed by Person/Commettee Besides Contract Approving Authority(%)</th>'
            } else if (PromiseIndicatorId == 24) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Total Higher Tier Approval</th>'+
                    '<th width="20%" class="t-align-center">Total Higher Tier Approval (%)</th>'
            } else if (PromiseIndicatorId == 25) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="20%" class="t-align-center">Total LOA issued Tender/Proposal </th>'+
                    '<th width="15%" class="t-align-center">Total Days(Approval to LOA)</th>'+
                    '<th width="15%" class="t-align-center">Average No. of Days(Approval to LOA)</th>'
            } else if (PromiseIndicatorId == 26) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="20%" class="t-align-center">Total LOA issued Tender/Proposal </th>'+
                    '<th width="15%" class="t-align-center">Total Days(Opening to LOA)</th>'+
                    '<th width="15%" class="t-align-center">Average No. of Days(Opening to LOA)</th>'
            } else if (PromiseIndicatorId == 27) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="20%" class="t-align-center">Total LOA issued Tender/Proposal </th>'+
                    '<th width="15%" class="t-align-center">Total Days(Invitation-LOA)</th>'+
                    '<th width="15%" class="t-align-center">Average No. of Days(Invitation-LOA)</th>'
            } else if (PromiseIndicatorId == 28) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="20%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Awarded Tender/Proposal </th>'+
                    '<th width="10%" class="t-align-center">Publicly Award Disclosed</th>'+
                    '<th width="10%" class="t-align-center">Publicly Award Disclosed(%)</th>'
            } else if (PromiseIndicatorId == 29) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="35%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Awarded Tender/Proposal</th>'+
                    '<th width="20%" class="t-align-center">Completed Tender/Proposal within Initial Validity Period</th>'+
                    '<th width="20%" class="t-align-center">Complete Tender/Proposal within Initial Validity Period (%)</th>'
            } else if (PromiseIndicatorId == 30) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Delivered Tender/Proposal within Original Schedule</th>'+
                    '<th width="20%" class="t-align-center">Delivered Tender/Proposal within Original Schedule(%)</th>'
            } else if (PromiseIndicatorId == 31) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Contracts Having Liquidated Damage</th>'+
                    '<th width="20%" class="t-align-center">Contracts Having Liquidated Damage(%)</th>'
            } else if (PromiseIndicatorId == 32) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Contract Signed</th>'+
                    '<th width="15%" class="t-align-center">Contract Completed</th>'+
                    '<th width="20%" class="t-align-center">Contract Completed(%)</th>'
            } else if (PromiseIndicatorId == 33) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="25%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="25%" class="t-align-center">Average no of Days</th>'
            } else if (PromiseIndicatorId == 34) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="10%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="10%" class="t-align-center">Total Cases</th>'+
                    '<th width="10%" class="t-align-center">Tenders/Proposals of Late Payment</th>'+
                    '<th width="20%" class="t-align-center">Tenders/Proposals of Late Payment(%)</th>'
            } else if (PromiseIndicatorId == 35) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Interests Paid for Delayed Payment</th>'+
                    '<th width="20%" class="t-align-center">Interests Paid for Delayed Payment(%)</th>'
            } else if (PromiseIndicatorId == 36) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Approved Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Tender/Proposal Procedure Complaints</th>'+
                    '<th width="20%" class="t-align-center">Tender/Proposal Procedure Complaints(%)</th>'
            } else if (PromiseIndicatorId == 37) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="10%" class="t-align-center">Total Complaints</th>'+
                    '<th width="10%" class="t-align-center">Total Resolution</th>'+
                    '<th width="15%" class="t-align-center">Resolution of Compliants with Modification of Award</th>'+
                    '<th width="15%" class="t-align-center">Percentage of Resolution of Compliants with Modification of Award(%)</th>'
            } else if (PromiseIndicatorId == 38) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Complaints</th>'+
                    '<th width="15%" class="t-align-center">Total Resolved Tender/Proposal</th>'+
                    '<th width="20%" class="t-align-center">Resolved Tender/Proposal(%)</th>'
            } else if (PromiseIndicatorId == 39) {
                tableheadings = '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="10%" class="t-align-center">Total Complaints</th>'+
                    '<th width="10%" class="t-align-center">Total Resolution</th>'+
                    '<th width="15%" class="t-align-center">Contracts having Review Panel\'s Decisions Upheld</th>'+
                    '<th width="15%" class="t-align-center">Contracts having Review Panel\'s Decisions Upheld(%)</th>'
            } else if (PromiseIndicatorId == 40) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="10%" class="t-align-center">Advertised Tender/Proposal(s)</th>'+
                    '<th width="10%" class="t-align-center">Contract Signed</th>'+
                    '<th width="15%" class="t-align-center">Contract Variation</th>'+
                    '<th width="15%" class="t-align-center">Contract Variation (%)</th>'
            } else if (PromiseIndicatorId == 41) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Contract</th>'+
                    '<th width="15%" class="t-align-center">Unresolved Dispute</th>'+
                    '<th width="20%" class="t-align-center">Dispute Unresolved Dispute(%)</th>'
            } else if (PromiseIndicatorId == 42) {
                tableheadings =  '<th width="10%" class="t-align-center">Department Id</th>'+
                    '<th width="40%" class="t-align-center">Ministry/Division/Organization/PA Name</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal</th>'+
                    '<th width="15%" class="t-align-center">Total Tender/Proposal with Fraud and Corruption Cases</th>'+
                    '<th width="20%" class="t-align-center">Percentage of Tender/Proposal with Fraud and Corruption Cases(%)</th>'
            } else if (PromiseIndicatorId == 43) {
                tableheadings = '<th width="10%" class="t-align-center">Total No. of Procuring Entities</th>'+
                    '<th width="40%" class="t-align-center">Total Trained Procurement Staff</th>'+
                    '<th width="50%" class="t-align-center">Average No. of Trained Procurement Staff</th>'
            } else if (PromiseIndicatorId == 44) {
                tableheadings = '<th width="10%" class="t-align-center">Total No. of Procuring Entities</th>'+
                    '<th width="40%" class="t-align-center">Total No. of Procurement Entities which has Atleast One Trained/Certified Staff</th>'+
                    '<th width="50%" class="t-align-center">Percentage Of Procurement Entities which has Atleast One Trained/Certified Staff</th>'
            } else if (PromiseIndicatorId == 45) {
                tableheadings = '<th width="10%" class="t-align-center">Agency ID</th>'+
                    '<th width="40%" class="t-align-center">Agency Name</th>'+
                    '<th width="50%" class="t-align-center">Trained Staff</th>'
            }
            return tableheadings;
        }
        /**
         *  In PROMIS Indicator heading change according to selected indicator id.
         *  Here we have cteated function which will return heading of selected promis id
         *  @param peid
         *  @return Heading of promis report
         *  To check heading of promis report you can refer report format given by Dohatec
         */
        function getMainHeading()
        {
            var peid;
            var Heading = "";
            peid = $("#PEId").val();
            //  alert(peid);
            switch (parseInt(peid)) {
                case 1:
                    Heading = "1. Advertisement of Tender/Proposal Opportunities in Newspaper";
                    break;
                case 2:
                    //Change CPTU to GPPMD by Proshanto
                    Heading = "2. Advertisement of Tender/Proposal Opportunities in GPPMD's Website";
                    break;
                case 3:
                    //Change GoB to RGoB by Proshanto
                    Heading = "3. Tenders/Proposals Following RGoB Procurement Rules";
                    break;
                case 4:
                    Heading = "4. Tenders/Proposals Following Development Partner Rules";
                    break;
                case 5:
                    Heading = "5. Multiple Locations Submission Tenders/Proposals";
                    break;
                case 6:
                    Heading = "6. Tender/Proposal Preparation Time in Open Tendering Method";
                    break;
                case 7:
                    Heading = "7. Tender/Proposal Time Compliance";
                    break;
                case 8:
                    Heading = "8. Sale of Tender/Proposal Documents";
                    break;
                case 9:
                    Heading = "9. Bidder/Consultant Participation";
                    break;
                case 10:
                    Heading = "10. Bidder/Consultant Participation Index";
                    break;
                case 11:
                    Heading = "11. Tender/Proposal Opening Committee Formation";
                    break;
                case 12:
                    Heading = "12. Tender/Proposal Evaluation Committee Formation";
                    break;
                case 13:
                    Heading = "13. External Member in TEC";
                    break;
                case 14:
                    Heading = "14. Tender/Proposal Evaluation Time";
                    break;
                case 15:
                    Heading = "15. Compliance of Tender/Proposal Evaluation Time";
                    break;
                case 16:
                    Heading = "16. Tender/Proposal Acceptance";
                    break;
                case 17:
                    Heading = "17. Re-Tendering";
                    break;
                case 18:
                    Heading = "18. Tender/Proposal Cancellation";
                    break;
                case 19:
                    Heading = "19. Tender/Proposal Evaluation Approval Time";
                    break;
                case 20:
                    Heading = "20. Compliance of Financial Delegation";
                    break;
                case 21:
                    Heading = "21. Submission of Evaluation Report to Appropriate Authority";
                    break;
                case 22:
                    Heading = "22. TER Approval Compliance";
                    break;
                case 23:
                    Heading = "23. Additional Review of TER";
                    break;
                case 24:
                    Heading = "24. Higher Tier Approval";
                    break;
                case 25:
                    Heading = "25. Time for Issuance of LOA to Bidder/Consultant";
                    break;
                case 26:
                    Heading = "26. Tender/Proposal Processing Lead Time";
                    break;
                case 27:
                    Heading = "27. Total Tender/Proposal Processing Time";
                    break;
                case 28:
                    Heading = "28. Publication of Award Information";
                    break;
                case 29:
                    Heading = "29. Efficiency in Contract Award";
                    break;
                case 30:
                    Heading = "30. Delivery Time";
                    break;
                case 31:
                    Heading = "31. Liquidated Damage";
                    break;
                case 32:
                    Heading = "32. Completion Rate";
                    break;
                case 33:
                    Heading = "33. Payment Release Compliance";
                    break;
                case 34:
                    Heading = "34. Late Payment";
                    break;
                case 35:
                    Heading = "35. Interests Paid for Delayed Payment";
                    break;
                case 36:
                    Heading = "36. Tender/Proposal Procedure Complaints";
                    break;
                case 37:
                    Heading = "37. Resolution of Complaints With Award Modification";
                    break;
                case 38:
                    Heading = "38. Resolution of Complaints";
                    break;
                case 39:
                    Heading = "39. Independent Review Panel";
                    break;
                case 40:
                    Heading = "40. Contract Amendments/Variations";
                    break;
                case 41:
                    Heading = "41. Unresolved Disputes";
                    break;
                case 42:
                    Heading = "42. Fraud and Corruption";
                    break;
                case 43:
                    Heading = "43. Procurement Training";
                    break;
                case 44:
                    Heading = "44. Percentage of Procurement Entities with Trained Persons";
                    break;
                case 45:
                    Heading = "45. Number of Procurement Persons by Organization";
                    break;
                default:

                    Heading = "1. Advertisement of Tender/Proposal Opportunities in Newspaper";

            }
            return Heading;
        }
        /**
         * In PROMIS Indicator subheadings are changing based on indicator. Here we have created function which will display
         *  subheading heading based on indicator selected by user from listbox.
         *  @param peid
         *  @return SubHeading for selected indicator id
         *  Note : For details of  subheading for each indicator you can refer report format given by Dohatec
         */
        function getSubTitle() {
            var SubHeading = "";
            var peid;
            peid = $("#PEId").val();
            switch (parseInt(peid)) {
                case 1:
                    SubHeading = "1. Percentage of Invitation for Tender/Proposal (IFT) Published in Newspaper";
                    break;
                case 2:
                    //Change CPTU to GPPMD by Proshanto
                    SubHeading = "2. Percentage of Invitation for Tender/Proposal (above threshold) Advertised in GPPMD’s Website";
                    break;
                case 3:
                    //Change GoB to RGoB by Proshanto
                    SubHeading = "3. Percentage of Tenders/Proposals Following RGoB Procurement Rules";
                    break;
                case 4:
                    SubHeading = "4. Percentage of Tenders/Proposals Following Development Partner Rules";
                    break;
                case 5:
                    SubHeading = "5. Percentage of Tenders/Proposals Allowed to Submit in Multiple Locations";
                    break;
                case 6:
                    SubHeading = "6. Average Number of Days Between Publishing of Advertisement and Tender/Proposal Submission Deadline";
                    break;
                case 7:
                    SubHeading = "7. Percentage of Tenders/Proposals having Sufficient Tender/Proposal Submission Time";
                    break;
                case 8:
                    SubHeading = "8. Average Number of Tenderers/Consultants Purchased Tender/Proposal Documents";
                    break;
                case 9:
                    SubHeading = "9. Average Number of Tenderers/Consultants Submitted Tenders/Proposals";
                    break;
                case 10:
                    SubHeading = "10. Ratio of Number of Tender/Proposal Document Sold and Number of Tender/Proposal Submission";
                    break;
                case 11:
                    SubHeading = "11. Percentage of Cases TOC Included At Least One Member From PEC/TEC";
                    break;
                case 12:
                    SubHeading = "12. Percentage of Cases TEC Formed by Approving Authority";
                    break;
                case 13:
                    SubHeading = "13. Percentage of Cases TEC Included Two External Members Outside The Procuring Entity";
                    break;
                case 14:
                    SubHeading = "14. Average Number of Days Between Tender/Proposal Opening and Completion of Evaluation";
                    break;
                case 15:
                    SubHeading = "15. Percentage of Cases Tender/Proposal Evaluation has been Completed within Timeline";
                    break;
                case 16:
                    SubHeading = "16. Average Number of Responsive Tenders/Proposals";
                    break;
                case 17:
                    SubHeading = "17. Percentage of Cases TEC Recommended Re-Tendering";
                    break;
                case 18:
                    SubHeading = "18. Percentage of Cases where Tender/Proposal Process Cancelled";
                    break;
                case 19:
                    SubHeading = "19. Average Number of Days Taken Between Submission of Tender/Proposal Evaluation and Approval Contract";
                    break;
                case 20:
                    SubHeading = "20. Average Number of Tenders/Proposals Approved by Proper Financial Delegated Authority";
                    break;
                case 21:
                    SubHeading = "21. Percentage of Cases TEC Submitted Report Directly to The Contract Approving Authority";
                    break;
                case 22:
                    SubHeading = "22. Percentage of Cases Contract Award Decision Made within Timeline by Contract Approving Authority";
                    break;
                case 23:
                    SubHeading = "23. Percentage of Cases TER Reviewed by Person/Committee Other Than The Contract Approving Authority";
                    break;
                case 24:
                    SubHeading = "24. Percentage of Tenders/Proposals Approved by Higher Tier than the Contract Approving Authority";
                    break;
                case 25:
                    SubHeading = "25. Average Number of Days Between Final Approval and Letter of Acceptance (LOA)";
                    break;
                case 26:
                    SubHeading = "26. Average Number of Days Between Tender/Proposal Opening and Letter of Acceptance (LOA)";
                    break;
                case 27:
                    SubHeading = "27. Average Number of Days Between Invitation for Tender/Proposal (IFT) and Letter of Acceptance (LOA)";
                    break;
                case 28:
                    //Change CPTU to GPPMD by Proshanto
                    SubHeading = "28. Percentage of Contract Award Published in GPPMD’s Website";
                    break;
                case 29:
                    SubHeading = "29. Percentage of Contract Awarded within Initial Tender/Proposal Validity Period";
                    break;
                case 30:
                    SubHeading = "30. Percent of Contracts Completed/Delivered within the Original Schedule as Mentioned in Contract";
                    break;
                case 31:
                    SubHeading = "31. Percentage of Contracts Having Liquidated Damage Imposed for Delayed Delivery/Completion";
                    break;
                case 32:
                    SubHeading = "32. Percentage of Contracts Fully Completed and Accepted";
                    break;
                case 33:
                    SubHeading = "33. Average Number of Days Taken to Release Payments";
                    break;
                case 34:
                    SubHeading = "34. Percentage of Cases (considering each installment as a case) with Delayed Payment";
                    break;
                case 35:
                    SubHeading = "35. Percentage of Contracts where Interest for Delayed Payments was Made";
                    break;
                case 36:
                    SubHeading = "36. Percentage of Tender/Proposal Procedure Complaints";
                    break;
                case 37:
                    SubHeading = "37. Percentage of Complaints Resulting in Modification of Awards";
                    break;
                case 38:
                    SubHeading = "38. Percentage of Cases Complaints have been Resolved";
                    break;
                case 39:
                    SubHeading = "39. Percentage of Cases Review Panel’s Decisions Upheld";
                    break;
                case 40:
                    SubHeading = "40. Percentage of Contract Amendments/Variations";
                    break;
                case 41:
                    SubHeading = "41. Percentage of Contracts With Unresolved Disputes";
                    break;
                case 42:
                    SubHeading = "42. Percentage of Tenders/Proposals of Fraud and Corruption";
                    break;
                case 43:
                    SubHeading = "43. Average no of Trained Procurement Staff in Each Procuring Entity";
                    break;
                case 44:
                    SubHeading = "44. Percentage of Procuring Entity Which Has At Least One Trained/Certified Procurement Staff";
                    break;
                case 45:
                    SubHeading = "45. Total Number of Procurement Persons in The Organization With Procurement Training";
                    break;

            }
            return SubHeading;
        }
        /**
         * This function loads all data based on filter selected by user
         */
        function loadTenderTable()
        {
            $("#progressBar").show();
            $("#progressBar").css("visibility","visible");
            
            var Heading = getMainHeading();
            var deptid = 0;
            $("#Mainheading").html(Heading);
            var SubHeading = getSubTitle();
            $("#getSubHeading").html(SubHeading);
            var subtableheading = getTableHeading();
            $("#subtableheading").html(subtableheading);
            if($("#txtdepartmentid").val() != "")
            {
                deptid = $("#txtdepartmentid").val();
            }
            $('#resultTable').show();
            //alert($("#cmbOffice").val());
            $.post("<%=request.getContextPath()%>/PromiseReportServlet", {
                funName : "PromiseReport",
                fyear :$("#procNature").val(),
                ptype: $("#cmbType").val(),
                deptid: deptid,
                officeid:$("#cmbOffice").val(),
                PEId : $("#PEId").val(),
                pageNo:$("#pageNo").val(),
                size:$("#size").val(),
                isPDF:'<%=isPDF%>',
                districtId:$("#cmbDistrict").val(),
                quater:$("#quater").val(),
                 projectId:$("#cmbProject").val()
            },
            function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);
                $("#progressBar").hide();
                //                if($('#noRecordFound').attr('value') == "noRecordFound"){
                //                    $('#pagination').hide();
                //                }else{
                //                    $('#pagination').show();
                //                }
                //                chkdisble($("#pageNo").val());
                //                if($("#totalPages").val() == 1){
                //                    $('#btnNext').attr("disabled", "true");
                //                    $('#btnLast').attr("disabled", "true");
                //                }else{
                //                    $('#btnNext').removeAttr("disabled");
                //                    $('#btnLast').removeAttr("disabled");
                //                }
                //                $("#pageTot").html($("#totalPages").val());
                //                $("#pageNoTot").html($("#pageNo").val());
                //                $('#resultDiv').show();

                //                var counter = $('#cntTenBrief').val();
                //                for(var i=0;i<counter;i++){
                //                    var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                //                    var temp1 = $('#tenderBrief_'+i).html();
                //                    if(temp.length > 250){
                //                        temp = temp1.substr(0, 250);
                //                        $('#tenderBrief_'+i).html(temp+'...');
                //                    }
                //                    //$('#tenderBrief_'+i).attr('style', 'width: 20px;');
                //                }
            });
        }
       
    </script>

    <script type="text/javascript">
        $(function() {
            $('#btnFirst').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);

                if(totalPages>0 && $('#pageNo').val()!="1")
                {
                    $('#pageNo').val("1");
                    loadTenderTable();
                    $('#dispPage').val("1");
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnLast').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);
                if(totalPages>0)
                {
                    $('#pageNo').val(totalPages);
                    loadTenderTable();
                    $('#dispPage').val(totalPages);
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnNext').click(function() {
                var pageNo=parseInt($('#pageNo').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo <= totalPages) {
                    $('#pageNo').val(Number(pageNo)+1);
                    loadTenderTable();
                    $('#dispPage').val(Number(pageNo)+1);
                    $('#btnPrevious').removeAttr("disabled");
                }
            });
        });

    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnPrevious').click(function() {
                var pageNo=$('#pageNo').val();

                if(parseInt(pageNo, 10) > 1)
                {
                    $('#pageNo').val(Number(pageNo) - 1);
                    loadTenderTable();
                    $('#dispPage').val(Number(pageNo) - 1);
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnGoto').click(function() {
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTenderTable();
                        $('#dispPage').val(Number(pageNo));
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                        if(parseInt($('#pageNo').val(), 10) > 1)
                            $('#btnPrevious').removeAttr("disabled");
                    }
                }
                loadTenderTable();
            });
        });
    </script>
    <!-- AJAX Grid Finish-->
    <script type="text/javascript">
        //  alert('herer');
      //  loadTenderTable();
    </script>
    <script type="text/javascript">
        function showHide()
        {
            if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='- Collapse'){
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Expand';
            }else{
                document.getElementById('tblSearchBox').style.display = 'table';
                document.getElementById('collExp').innerHTML = '- Collapse';
            }
        }

        function checkKeyGoTo(e)
        {
            var keyValue = (window.event)? e.keyCode : e.which;
            if(keyValue == 13){
                //Validate();
                $(function() {
                    //$('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            //loadTable();
                            $('#btnGoto').click();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
                //});
            }
        }
    </script>
</html>
