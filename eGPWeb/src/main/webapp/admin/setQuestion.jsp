<%-- 
    Document   : setQuestion.jsp
    Created on : May 28, 2017, 3:26:42 PM
    Author     : feroz
--%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuestionModuleService"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuestionModule"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuizAnswer"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuizQuestion"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuizAnswerService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuizQuestionService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.BhutanDebarmentCommitteeService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.util.Calendar"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Set Question</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
            
                $("#setQ").validate({
                    rules: {
                        moduleId:{required: true},
                        quizQuestion:{required:true},
                        optA:{required:true},
                        optB:{required:true},
                        optC:{required:true},
                        optD:{required:true},
                        quizAnswer:{required:true}

                    },
                    messages: {
                        moduleId:{ required: "<div class='reqF_1'> Please Select Question Module.</div>"},
                        
                        quizQuestion:{ required: "<div class='reqF_1'> Please Enter Question.</div>"},
                        
                        optA:{ required: "<div class='reqF_1'> Please Enter Option A.</div>"},
                        
                        optB:{ required: "<div class='reqF_1'> Please Enter Option B.</div>"},
                        
                        optC:{ required: "<div class='reqF_1'> Please Enter Option C.</div>"},
                        
                        optD:{ required: "<div class='reqF_1'> Please Enter Option D.</div>"},
                        
                        quizAnswer:{ required: "<div class='reqF_1'> Please Select Correct Answer.</div>"}
                    }
                }
            );
            });

        </script>
        <%
                    int createdBy = Integer.parseInt(session.getAttribute("userId").toString());
                    if ("Create".equals(request.getParameter("button"))) 
                    {
                        boolean doneFlag = false;
                        String action = "";
                        String quizQuestion = request.getParameter("quizQuestion");
                        String optA = request.getParameter("optA");
                        String optB = request.getParameter("optB");
                        String optC = request.getParameter("optC");
                        String optD = request.getParameter("optD");
                        String quizAnswer = request.getParameter("quizAnswer");
                        String moduleId = request.getParameter("moduleId");
                        QuizQuestionService quizQuestionService = (QuizQuestionService) AppContext.getSpringBean("QuizQuestionService");
                        QuizAnswerService quizAnswerService = (QuizAnswerService) AppContext.getSpringBean("QuizAnswerService");
                        TblQuizQuestion tblQuizQuestionThis = new TblQuizQuestion();
                        TblQuizAnswer tblQuizAnswerThis1 = new TblQuizAnswer();
                        TblQuizAnswer tblQuizAnswerThis2 = new TblQuizAnswer();
                        TblQuizAnswer tblQuizAnswerThis3 = new TblQuizAnswer();
                        TblQuizAnswer tblQuizAnswerThis4 = new TblQuizAnswer();
                        TblQuestionModule tblQuestionModuleThis = new TblQuestionModule();
                        try{
                                tblQuestionModuleThis.setModuleId(Integer.parseInt(moduleId));
                                tblQuizQuestionThis.setQuestion(quizQuestion);
                                tblQuizQuestionThis.setTblQuestionModule(tblQuestionModuleThis);
                                quizQuestionService.addTblQuizQuestion(tblQuizQuestionThis);
                                
                                tblQuizAnswerThis1.setAnswer(optA);
                                if(quizAnswer.equals("A"))
                                {
                                    tblQuizAnswerThis1.setIsCorrect(true);
                                }
                                else
                                {
                                    tblQuizAnswerThis1.setIsCorrect(false);
                                }
                                tblQuizAnswerThis1.setTblQuizQuestion(tblQuizQuestionThis);
                                quizAnswerService.addTblQuizAnswer(tblQuizAnswerThis1);
                                
                                tblQuizAnswerThis2.setAnswer(optB);
                                if(quizAnswer.equals("B"))
                                {
                                    tblQuizAnswerThis2.setIsCorrect(true);
                                }
                                else
                                {
                                    tblQuizAnswerThis2.setIsCorrect(false);
                                }
                                tblQuizAnswerThis2.setTblQuizQuestion(tblQuizQuestionThis);
                                quizAnswerService.addTblQuizAnswer(tblQuizAnswerThis2);
                                
                                tblQuizAnswerThis3.setAnswer(optC);
                                if(quizAnswer.equals("C"))
                                {
                                    tblQuizAnswerThis3.setIsCorrect(true);
                                }
                                else
                                {
                                    tblQuizAnswerThis3.setIsCorrect(false);
                                }
                                tblQuizAnswerThis3.setTblQuizQuestion(tblQuizQuestionThis);
                                quizAnswerService.addTblQuizAnswer(tblQuizAnswerThis3);
                                
                                tblQuizAnswerThis4.setAnswer(optD);
                                if(quizAnswer.equals("D"))
                                {
                                    tblQuizAnswerThis4.setIsCorrect(true);
                                }
                                else
                                {
                                    tblQuizAnswerThis4.setIsCorrect(false);
                                }
                                tblQuizAnswerThis4.setTblQuizQuestion(tblQuizQuestionThis);
                                quizAnswerService.addTblQuizAnswer(tblQuizAnswerThis4);
                                
                            }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                            }finally{
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                                action=null;
                            }
                        response.sendRedirect("moduleForViewQuestion.jsp?msg=crS");
                    }
         %>
        
    </head>
    <body>
        <%
                    QuestionModuleService questionModuleService = (QuestionModuleService) AppContext.getSpringBean("QuestionModuleService");
                    List<TblQuestionModule> tblQuestionModule = questionModuleService.getAllTblQuestionModule();
         %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
       
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea"><div class="pageHead_1">Set Question</div>
                        <form id="setQ" name="setQ" action="setQuestion.jsp" method="post">
                        <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                            <tr>
                                <td style="font-style: italic" class="t-align-right" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <tr>
                                <td class="ff">Select Module : <span>*</span></td>
                                <td>
                                    <select style="width:150px;" class="formTxtBox_1" id="moduleId" name="moduleId">
                                    <option value="">--Select Module--</option>
                                    <%
                                    for(TblQuestionModule tblQuestionModuleThis : tblQuestionModule){%>
                                        <option value="<%=tblQuestionModuleThis.getModuleId()%>"><%out.print(tblQuestionModuleThis.getModuleName());%></option>
                                    <%}%>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Question : <span>*</span></td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="text" id="quizQuestion" name="quizQuestion" maxlength="450"></td>
                            </tr>
                            <tr>
                                <td class="ff">Option A : <span>*</span></td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="text" id="optA" name="optA" maxlength="150"></td>
                            </tr>
                            <tr>
                                <td class="ff">Option B : <span>*</span></td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="text" id="optB" name="optB" maxlength="150"></td>
                            </tr>
                            <tr>
                                <td class="ff">Option C : <span>*</span></td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="text" id="optC" name="optC" maxlength="150"></td>
                            </tr>
                            <tr>
                                <td class="ff">Option D : <span>*</span></td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="text" id="optD" name="optD" maxlength="150"></td>
                            </tr>
                            <tr>
                                <td class="ff">Correct Answer : <span>*</span></td>
                                <td>
                                    <select style="width:150px;" class="formTxtBox_1" id="quizAnswer" name="quizAnswer">
                                        <option value="">--Select Answer--</option>
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" cellpadding="0" >
                                <tr>
                                    <td class="t-align-center" style="padding-right:110px;"><span class="formBtn_1"><input type="submit" name="button" id="button" value="Create" onclick="return validate();"/></span></td>
                                </tr>
                            </table>
                        </form>
                </td>
            </tr>
        </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabContent");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        var leftObj = document.getElementById("lblSetQuestion");
        if(leftObj != null){
            leftObj.setAttribute("class", "selected");
        }
        function GetCal(txtname, controlname)
        {
            new Calendar({
                inputField: txtname,
                trigger: controlname,
                showTime: 24,
                onSelect: function () {
                    var date = Calendar.intToDate(this.selection.get());
                    LEFT_CAL.args.min = date;
                    LEFT_CAL.redraw();
                    this.hide();
                    document.getElementById(txtname).focus();
                    
                }
            });

            var LEFT_CAL = Calendar.setup({
                weekNumbers: false
            })

        }
        function validate()
        {
            var role = document.getElementById("uRole").value;
            var isActive = document.getElementById("isActive").value;
            var result = "";
            if(role == "Chairman" && isActive == 1)
            {
                <%
                    BhutanDebarmentCommitteeService bhutanDebarmentCommitteeService = (BhutanDebarmentCommitteeService) AppContext.getSpringBean("BhutanDebarmentCommitteeService");
                    List<TblBhutanDebarmentCommittee> tblBhutanDebarmentCommitteeCheck = bhutanDebarmentCommitteeService.getAllTblBhutanDebarmentCommittee();
                    for(TblBhutanDebarmentCommittee tblBhutanDebarmentCommitteeMatch : tblBhutanDebarmentCommitteeCheck)
                    {
                        if(tblBhutanDebarmentCommitteeMatch.getRole().equals("Chairman") && tblBhutanDebarmentCommitteeMatch.isIsActive())
                        {
                            %>result = "false";<%
                        }
                    }
                %>
            }
            if(result == "false")
            {
                jAlert("There is already an active Chairman!"," Alert ", "Alert");
                return false;
            }
        }
   </script>
</html>