<%-- 
    Document   : SecurityConfigView
    Created on : Dec 3, 2010, 2:39:48 PM
    Author     : Naresh.Akurathi
--%>

<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigMasterSrBean"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Security Configuration</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

    <%!
        ConfigMasterSrBean configMasterSrBean = new ConfigMasterSrBean();
        String msg="";
    %>

</head>
<body>

<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <div class="topHeader">
     <%@include  file="../resources/common/AfterLoginTop.jsp"%>
  </div>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="pageHead_1">Security Configuration Details</div>
  <div>&nbsp;</div>
  <div>&nbsp;</div>
  <table width="100%" cellspacing="0" class="tableList_1 t_space">
     <tr>
        <th >Failed Login attempts Count </th>
        <th >PKI Requires </th>        
    </tr>

<%

          TblConfigurationMaster tblConfigurationMaster = new TblConfigurationMaster();
          List l=configMasterSrBean.getConfigMaster();

          if(!l.isEmpty() && l!=null){
          Iterator it = l.iterator();
          if(it.hasNext())
              {
                    tblConfigurationMaster=(TblConfigurationMaster)it.next();
                   

        %>
      <tr>	
        <td class="t-align-center"><%=tblConfigurationMaster.getFailedLoginAttempt()%></td>
        <td class="t-align-center"><%=tblConfigurationMaster.getIsPkiRequired()%></td>
    </tr>
    <%
            }

                }
          else
              msg="No Data in ConfigMaster";
        %>

</table>
   <div class="pageHead_1" align="center"> <%=msg%></div>

  <div>&nbsp;</div>
   <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
</div>
<script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
</body>
</html>


