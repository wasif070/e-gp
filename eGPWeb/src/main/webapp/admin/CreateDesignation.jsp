<%-- 
    Document   : CreateDesignation
    Created on : Oct 23, 2010, 4:51:51 PM
    Author     : parag
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    int uTypeID = 0;
                    if (session.getAttribute("userTypeId") != null) {
                        uTypeID = Integer.parseInt(session.getAttribute("userTypeId").toString());
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create Designation</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <%--<script type="text/javascript" src="Include/pngFix.js"></script>--%>

        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#frmCreateDesignation").validate({
                    rules:{
                        txtdepartmentid:{required:true},
                        designation:{spacevalidate: true, requiredWithoutSpace: true,maxlength:250,alphaName:true},
                        grade:{required:true}
                    },

                    messages:{
                        txtdepartmentid:{required:"<div class='reqF_1' id='deptdiv'>Please select Hierarchy Node.</div>"
                        },
                        designation:{requiredWithoutSpace:"<div class='reqF_1' id='desigdiv'>Please enter Designation.</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 250 characters only</div>",
                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>",
                            alphaName:"<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' \" } { - . _) Only </div>"
                        },
                        grade:{required:"<div class='reqF_1'>Please select Class-Grade.</div>"
                        }
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "txtdepartmentid")
                            error.insertAfter("#deptTreeIcn");
                        else
                            error.insertAfter(element);
                    }

                });

            });
        </script>

        <script type="text/javascript">
            function verifyDesignation()  {
                var dept=$('#txtdepartmentid').val();
                var deptId=dept;
                var spacetest =  /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/;
                if(spacetest.test($.trim($('#txtDesignation').val()))) {
                    $('#desigMsg').html("Checking for unique designation...");
                    $.post("<%=request.getContextPath()%>/DesignationServlet", {department: deptId,designation:$('#txtDesignation').val(),funName:'verifyDesignation'},  function(j){
                        if(j.toString().indexOf("OK", 0)!=-1){
                            $('#deptMsg').html("");
                            $('#desigMsg').css("color","green");
                        }
                        else{
                            $('#desigMsg').css("color","red");
                        }
                        $('#desigMsg').html(j);
                    });
                }
                else
                {
                    if($("#crntVal").val().length > 0 ){
                        $('#desigMsg').css("color","red");
                        $('#desigMsg').html("Please enter valid Designation");
                    }else{
                        if(document.getElementById("desigdiv").innerHTML.length>0){
                            $('#desigMsg').html("");
                        }else{
                            $('#desigMsg').css("color","red");
                            $('#desigMsg').html("Please enter Designation");
                        }
                    }
                }
            }
        </script>
        <script type="text/javascript">
            /*$(function() {
                        $('#txtDesignation').blur(function() {
                            $('#txtDesignation').val($.trim($('#txtDesignation').val()));
                              var spacetest=/^[\sa-zA-Z!,&.\'\-]+$/;
                            if(spacetest.test($('#txtDesignation').val())) {
                        
                                $('#crntVal').val($('#txtdepartmentid').val());

                                if($('#txtDesignation').val().length>0){
                                    verifyDesignation();
                                }else
                                {
                            
                                    if($("#crntVal").val().length > 0){
                                        $('#desigMsg').css("color","red");
                                        $('span.#desigMsg').html("Please Select Department");
                                    }
                                }
                            }
                            else
                            {
                                $('#desigMsg').css("color","red");
                                $('span.#desigMsg').html("Please Select Valid Designation");
                            }
                        });
                    });*/
        </script>
        <script type="text/javascript">
            $(function() {
           
                $('#txtDesignation').blur(function() {
                    var spacetest =  /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/;
                    if(spacetest.test($('#txtDesignation').val())!=null) {
                        $('#crntVal').val($('#txtDesignation').val());
                        if($('#txtdepartmentid').val().length>0){
                            if($('#txtDesignation').val().length<200){
                                verifyDesignation();
                            }
                        }
                        else
                        {
                            if(document.getElementById("deptdiv")!=null){
                                if(document.getElementById("deptdiv").innerHTML.length>0){
                                    $('#deptMsg').css("color","red");
                                    $('span.#deptMsg').html("");
                                }else{
                                    $('#deptMsg').css("color","red");
                                    $('span.#deptMsg').html("Please select Department first.");
                                }
                            }
                        }
                    }
                    else
                    {
                        if($("#crntVal").val().length > 0 ){
                            $('#desigMsg').css("color","red");
                            $('#desigMsg').html("Please enter valid Designation");
                        }else{
                            if(document.getElementById("desigdiv").innerHTML.length>0){
                                $('#desigMsg').html("");
                            }else{
                                $('#desigMsg').css("color","red");
                                $('#desigMsg').html("Please enter Designation");
                            }
                        }
                    }
                });
            });
        </script>

        <script type="text/javascript">
            $(function() {
                $('#frmCreateDesignation').submit(function() {
                    if($('#desigMsg').html().indexOf("OK", 0) == -1){
                        $('#deptMsg').html("");
                        //   $('span.#desigMsg').html("");
                        return false;
                    }else{
                
                        return true;
                    }
                
                });
            });
        </script>
        <script type="text/javascript">
            function clearMsg(){
                document.getElementById('desigMsg').innerHTML='';
                $(".reqF_1").remove();
            }
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <jsp:useBean id="designationSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.DesignationSrBean"/>
                <%
                            String logUserId = "0";
                            if (session.getAttribute("userId") != null) {
                                logUserId = session.getAttribute("userId").toString();
                            }
                            designationSrBean.setLogUserId(logUserId);
                %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <td class="contentArea">
                            <!--Page Content Start-->
                            <div>
                                <div class="pageHead_1">Create Designation</div>
                            </div>
                            <!-- Success failure -->
                            <%
                                        String msg = request.getParameter("msg");
                                        if (msg != null && msg.equals("fail")) {%>
                            <div class="responseMsg errorMsg">Designation Creation Failed.</div>
                            <%} else if (msg != null && msg.equals("success")) {%>
                            <div class="responseMsg successMsg">Designation Created Successfully</div>
                            <%}%>
                            <!-- Result Display End-->                           
                            <form id="frmCreateDesignation" action="<%=request.getContextPath()%>/DesignationServlet" method="POST">
                                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">


                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="t-align-right">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="225px"><%if(uTypeID == 1){ %>Select<% } %> Hierarchy Node/PA :<%if(uTypeID == 1){ %>&nbsp;<span class="mandatory">*</span><% } %></td>
                                        <%
                                                    int userId = Integer.parseInt(session.getAttribute("userId").toString());
                                                    designationSrBean.setUserId(userId);
                                                    if (userId == 1) {

                                        %>
                                        <td>
                                            <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 200px;"
                                                   id="txtdepartment"  readonly style="width: 200px;"/>
                                            <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />
                                            <img onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=createdesignation', '', 'width=350px,height=400px,scrollbars=1','');" style="vertical-align: bottom; cursor: pointer;" height="25" id="deptTreeIcn" name="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                            <span id="deptMsg" style="font-weight: bold;">&nbsp;</span>
                                        </td>
                                        <%} else {%>
                                        <%for (SelectItem officeItem : designationSrBean.getMinistryforAdmin()) {%>
                                        <td><label><%=officeItem.getObjectValue()%></label>
                                            <%--<input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 200px;" value="<%=(officeItem.getObjectValue())%>" readonly/>--%>
                                            <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" value="<%=officeItem.getObjectId()%>" />                                                 <%}%>
                                        </td>
                                        <%}%>
                                    </tr>
                                    <tr>
                                        <td class="ff">Designation : <span>*</span></td>
                                        <td><input name="designation" type="text" class="formTxtBox_1" id="txtDesignation" style="width:200px;" maxlength="51"/>
                                            <div id="desigMsg" style="font-weight: bold;"></div>
                                            <input type="hidden" id="crntVal" name="crntVal"/>
                                        </td>
                                    </tr>
                                    <tr Style = "display: none;">
                                        <td class="ff">Class - Grade : <span>*</span></td>
                                        <td><select name="grade" class="formTxtBox_1" id="cmbGrade" style="width:208px;">
                                                <option value="1">-- Select Grade --</option>
                                                <c:forEach var="grade" items="${designationSrBean.gradeList}">
                                                    <option value="${grade.objectId}">${grade.objectValue}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;</td>
                                        <td><label class="formBtn_1">
                                                <input type="submit" name="designationSubmit" id="btnDesignationSubmit" value="Submit" />
                                            </label>
                                            &nbsp;
                                            <label class="formBtn_1">
                                                <input type="reset" name="designationReset" id="btnDesignationReset" value="Reset" onclick="clearMsg();" />
                                            </label></td>
                                    </tr>

                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script>
            var obj = document.getElementById('lblDesignationCreation');
            if(obj != null){
                if(obj.innerHTML == 'Create Designation'){
                    obj.setAttribute('class', 'selected');
                }
            }

        </script>
        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
