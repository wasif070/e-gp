<%-- 
    Document   : selectModule
    Created on : May 28, 2017, 3:13:29 PM
    Author     : feroz
--%>

<%-- 
    Document   : QuestionModule
    Created on : May 26, 2017, 10:46:28 AM
    Author     : feroz
--%>

<%@page import="com.cptu.egp.eps.model.table.TblQuestionModule"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuestionModuleService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Question Module</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>        <script type="text/javascript">
            $(document).ready(function() {
                sortTable();
            });
        </script>
        
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <%
                    QuestionModuleService questionModuleService = (QuestionModuleService) AppContext.getSpringBean("QuestionModuleService");
                    List<TblQuestionModule> tblQuestionModule = questionModuleService.getAllTblQuestionModule();
         %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea">
                        <div class="pageHead_1">Select Question Module
                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('2');">Save as PDF</a></span>
                        </div>
                        <br>
                        <table class="tableList_1 t_space" cellspacing="0" width="100%" id="resultTable" cols="">
                                <tbody id="tbodyData">&nbsp;
                                    
                                <%
                                  int cnt = 0;
                                  for(TblQuestionModule tblQuestionModuleThis : tblQuestionModule){
                                        cnt++;
                                        if(cnt%2==0){%>
                                              <tr>
                                      <%}else{%>
                                              <tr style='background-color:#E4FAD0;'>
                                      <%}%>
                                              <td class="t-align-center">
                                                  <a href="setQuestion.jsp?moduleId=<%=tblQuestionModuleThis.getModuleId()%>"><%out.print(tblQuestionModuleThis.getModuleName());%></a>
                                              </td>
                                              </tr>
                                <%}%>
                                </tbody>
                            </table>
                </td>
            </tr>
        </table>

        <form id="formstyle" action="" method="post" name="formstyle">

           <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
           <%
             SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
             String appenddate = dateFormat1.format(new Date());
           %>
           <input type="hidden" name="fileName" id="fileName" value="DebarmentCommittee_<%=appenddate%>" />
            <input type="hidden" name="id" id="id" value="EvaluationRule" />
        </form>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabDebar");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        var leftObj = document.getElementById("lblViewDebarCom");
        if(leftObj != null){
            leftObj.setAttribute("class", "selected");
        }
    </script>
</html>