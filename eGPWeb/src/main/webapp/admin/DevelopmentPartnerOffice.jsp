<%-- 
    Document   : DevelopmentPartnerOffice
    Created on : Oct 23, 2010, 7:48:47 PM
    Author     : rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Development Partner Office</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
     $(document).ready(function() {
        $("#frmDevelopmentPartnerOffice").validate({
            rules: {
                developmentPartner:{required: true},
                name:{required:true},
                country:{required:true},
                district:{required:true},
                city:{required:true,maxlength:50},
                postCode:{number:true,minlength:4},
                phoneNumber:{digits:true,maxlength:20},
                faxNumber:{digits:true,maxlength:20},
                website: {required:true, url: true}

            },
            messages: {
                developmentPartner:{ required: "<div class='reqF_1'> Please select Development Partner.</div>"},

                name:{ required: "<div class='reqF_1'> Please Enter Regional Office Name.</div>"},

               country:{required: "<div class='reqF_1'>Please select country.</div>"
                        },

               district:{required: "<div class='reqF_1'>Please select Dzongkhag / District Name.</div>"
                        },

               city:{required:"<div class='reqF_1'>Please Enter City/Town.</div>",
                        maxlength:"<div class='reqF_1'>Maximum 50 Characters are to be allowed.</div>"
                    },

               postCode:{number: "<div class='reqF_1'>Only numbers (0-9) are allowed.</div>",
                        minlength:"<div class='reqF_1'>Minimum 4 characters are required.</div>",
                        },

               phoneNumber:{digits:"<div class='reqF_1'>Please Enter Numbers only.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 20 characters are allowed.</div>"
                            },

               faxNumber:{digits:"<div class='reqF_1'>Please Enter Numbers only.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 20 characters are allowed.</div>"
                            },
               website: {required:"<div class='reqF_1'>Please enter Website Name.</div>",
                        url: "<div class='reqF_1'>Please Enter Valid Website Name</div>"}
            }
        }
    );
    });

 </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="../resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <div class="pageHead_1">Development Partner Office</div>
                            <!--Page Content Start-->
                            <form name="DevelopmentPartnerOffice" id="frmDevelopmentPartnerOffice" method="post" action="">
                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td class="ff" width="200">Select Development Partner<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmdDeveParter" name="developmentPartner" style="width: 200px">
                                                <option value=""></option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Regional Office Name<span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtName" name="name" style="width: 200px;" type="text"/>
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="ff" width="200">Country<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbCountry" name="country" style="width: 200px">
                                                <option></option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Dzongkhag / District<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbDistrict" name="district" style="width: 200px">
                                                <option></option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">City<span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtCity" name="city" style="width: 200px;" type="text" maxlength="50"/>
                                        </td>
                                    </tr

                                    <tr>
                                        <td class="ff" width="200">Post Code</td>
                                        <td><input class="formTxtBox_1" id="txtPostCode" name="postCode" style="width: 200px;" type="text" maxlength="10"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Phone Number</td>
                                        <td><input class="formTxtBox_1" id="txtPhoneNumber" style="width: 200px;" name="phoneNumber" type="text" maxlength="20"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Fax Number</td>
                                        <td><input class="formTxtBox_1" id="txtFaxNumber" style="width: 200px;" name="faxNumber" type="text" maxlength="20"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Website<span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtWebsite" style="width: 200px;" name="website" type="text" maxlength="100"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formBtn_1" align="center"><input id="btnSubmit" name="submit" value="Submit" type="submit"/></td>
                                        <td class="formBtn_1" align="center"><input id="btnUpdate" name="update" value="Update" type="submit"/></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
