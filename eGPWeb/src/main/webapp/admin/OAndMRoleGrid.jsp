<%-- 
    Document   : OAndMRoleGrid.jsp
    Created on : Mar 9, 2012, 2:55:10 PM
    Author     : shreyansh.shah
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Role Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
    </head>
    <%
                byte userTypeIdC = 0;
                if (!request.getParameter("userTypeid").equals("")) {
                    userTypeIdC = Byte.parseByte(request.getParameter("userTypeid"));
                }
    %>
    <script type="text/javascript">
        jQuery().ready(function (){
            jQuery("#list").jqGrid({
                url:'<%=request.getContextPath()%>/ManageUserRights?q=1&action=get RoleDetails&userTypeId=<%=userTypeIdC%>',
                datatype: "xml",
                height: 250,
                colNames:['Sl. No.','Role Name',"Action"],
                colModel:[
                    {name:'srno',index:'srno', width:5,sortable:false,align:'center'},
                    {name:'tlm.RoleNO',index:'rollName', width:35,sortable:true},
                    {name:'Operation',index:'Operation', width:25,sortable:false,align:'center'}
                ],
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum:10,
                rowList:[10,20,30],
                pager: $("#page"),
                caption: "Role Creation",
                gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
            }).navGrid('#page',{edit:false,add:false,del:false,search:false});
        });
    </script>
    <jsp:useBean id="manageAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.UpdateAdminSrBean"/>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("userType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                            userType.append(request.getParameter("userType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                        <td class="contentArea">
                            <div class="pageHead_1">View Roles</div>
                            <%
                            if (request.getParameter("roleRightsmsg") != null && request.getParameter("roleRightsmsg").equalsIgnoreCase("success")) {
                            %>
                            <div id="succMsg" class="responseMsg successMsg">Rights assign Successfully.</div>
                            <%                                                        } else if (request.getParameter("roleRightsmsg") != null && request.getParameter("roleRightsmsg").equalsIgnoreCase("fail")) {
                            %>
                            <div id="succMsg" class="responseMsg successMsg">Some Problem occured during rights assign.</div>
                            <%                                                        }
                            %>
                            <div class="t-align-left ff formStyle_1">To sort click on the relevant column header</div>
                            <div align="right" class="t_space b_space">
                                <a href="OAndMRoleCreation.jsp?action=Create Role" class="action-button-add" >Add User Rights Role</a>
                            </div>
                            <div class="t_space">
                                <%if (request.getParameter("from") != null) {%>
                                <div align="left" id="sucMsg" class="responseMsg successMsg">Role created successfully</div>
                                <%}%>
                                <table id="list"></table>
                                <div id="page">

                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                <!--Middle O & M Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script>
            var obj = document.getElementById('lblOAndMAdminView');
            if(obj != null){
                if(obj.innerHTML == 'View Roles'){
                    obj.setAttribute('class', 'selected');
                }
            }

        </script>
        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
    <%
                manageAdminSrBean = null;
    %>
</html>

