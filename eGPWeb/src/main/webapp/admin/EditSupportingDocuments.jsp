<%-- 
    Document   : EditSupportingDocuments
    Created on : Oct 23, 2010, 8:13:49 PM
    Author     : parag
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Supporting Documents</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="Include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="../resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="pageHead_1">Edit User Registration - Supporting Documents</div>
                            <form id="frmSupportingDocuments" method="post">
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                <tr>
                                    <td class="ff">File Name   : <span>*</span></td>
                                    <td><input name="textfield1" type="text" class="formTxtBox_1" id="txtFileName" style="width:200px;" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Document Name : <span>*</span></td>
                                    <td><input name="textfield2" type="text" class="formTxtBox_1" id="txtDocumentName" style="width:200px;" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Description : <span>*</span></td>
                                    <td><input name="textfield5" type="text" class="formTxtBox_1" id="txtDescription" style="width:200px;" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><label class="formBtn_1">
                                            <input type="submit" name="btnSubmit" id="btnSubmit" value="Previous" />
                                        </label>
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input type="submit" name="btnSave" id="btnSave" value="Save" />
                                        </label>
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input type="submit" name="btnNext" id="btnNext" value="Next" />
                                        </label></td>
                                </tr>
                            </table>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" class="tableList_2">
                                <tr>
                                    <th style="text-align:center; width:4%;">Sl. No.</th>
                                    <th>Document Name</th>
                                    <th>Document Description</th>
                                    <th>File Size</th>
                                    <th style="text-align:center; width:10%;">Action</th>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">1</td>
                                    <td>Document 1</td>
                                    <td>This is test Document....</td>
                                    <td>2 MB</td>
                                    <td style="text-align:center;"><a href="#" title="Download"><img src="Images/Dashboard/downloadIcn.png" alt="Download" /></a> &nbsp; <a href="#" title="Remove"><img src="Images/Dashboard/delIcn.png" alt="Remove" width="16" height="16" /></a></td>
                                </tr>
                                <tr>
                                    <td style="text-align:center;">2</td>
                                    <td>Document 2</td>
                                    <td>This is test Document....</td>
                                    <td>200 KB</td>
                                    <td style="text-align:center;"><a href="#" title="Download"><img src="Images/Dashboard/downloadIcn.png" alt="Download" /></a> &nbsp; <a href="#" title="Remove"><img src="Images/Dashboard/delIcn.png" alt="Remove" width="16" height="16" /></a></td>
                                </tr>
                            </table>
                           </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>
