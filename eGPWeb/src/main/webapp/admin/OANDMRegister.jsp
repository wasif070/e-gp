<%-- 
    Document   : OANDMRegister
    Created on : Mar 5, 2012, 2:37:03 PM
    Author     : shreyansh.shah
    Modified By: Dohatec
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.model.table.TblRoleMaster"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    String OMTitle = "";
                    if (request.getParameter("isUser") != null && request.getParameter("isUser") != "null" && request.getParameter("isUser") != "") {
                        if(request.getParameter("isUser").equals("DLI"))
                             OMTitle = "Performance Monitoring User";
                        else
                             OMTitle = "O & M User";
                    } else {
                        OMTitle = "O & M Admin";
                    }
                    ManageUserRightsImpl objManageuser = (ManageUserRightsImpl) AppContext.getSpringBean("ManageUserRigths");
        %>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=OMTitle%></title>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>

        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js" type="text/javascript"></script>

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
             var validCheckMail = true;
             var validCheckNid = true;
             var validCheckMno = true;
            $(document).ready(function() {
               
                $("#frmOAndMAdmin").validate({
                    rules: {
                        emailId:{required: true,email:true},
                        password:{spacevalidate: true, requiredWithoutSpace: true,minlength:8,maxlength:25, alphaForPassword : true},
                        confPassword:{required:true,equalTo:"#txtPassword"},
                        //,spacevalidate: true
                        fullName: {spacevalidate: true, requiredWithoutSpace: true , alphaName:true, maxlength:100 },
                        nationalId: { number: true,maxlength: 25,minlength: 13},
                        phoneNo: { required:true,numberWithHyphen:true,PhoneFax: true},
                        mobileNo: {number: true, minlength: 8, maxlength:8 },
                        RoleType: {required: true },
                        rollId: { required:
                                function()
                                {
                                        var val = $('input:radio[name=RoleType]:checked').val();
                                        if( val == 'R')
                                        {
                                            return true;
                                        }
                                        else{
                                            return false;
                                        }
                                }
                                }

                    },
                    messages: {
                        emailId:{ required: "<div class='reqF_1'> Please enter e-mail ID.</div>",
                            email:"<div class='reqF_1'> e-mail ID must be in xyz@abc.com format.</div>"},

                        password:{spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace:"<div class='reqF_1'>Please enter Password.</div>",
                            //spacevalidate: "<div class='reqF_1'>Space is not allowed.</div>",
                            alphaForPassword: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>",
                            minlength:"<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>",
                            maxlength:"<div class='reqF_1'>Maximum 25 characters are allowed.</div>"},

                        confPassword:{required: "<div class='reqF_1'>Please re-type Password.</div>",
                            equalTo: "<div class='reqF_1'>Password does not match. Please try again.</div>"
                            //spacevalidate: "<div class='reqF_1'>Space is not allowed.</div>"
                        },
                        fullName: { spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace: "<div class='reqF_1'>Please enter Full Name</div>",
                            alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only </div>",
                            maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"},
                        nationalId: {
                            number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                            maxlength: "<div class='reqF_1'>CID No should comprise of maximum 25 digits</div>",
                            minlength: "<div class='reqF_1'>CID No should comprise of minimum 13 digits</div>"
                        },
                        phoneNo: { required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            numberWithHyphen: "<div class='reqF_1'>Allows numbers (0-9) and hyphen (-) only</div>",
                            PhoneFax:"<div class='reqF_1'>Please enter valid Phone No. Area code should contain minimum 2 digits and maximum 5 digits. Phone no. should contain minimum 3 digits and maximum 10 digits</div>"},
                        mobileNo: {
                            number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" ,
                            minlength:"<div class='reqF_1'>Minimum 8 digits are required</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 8 digits only</div>"
                        },
                         RoleType: {
                            required: "<div class='reqF_1'>Please select RoleType.</div>"
                        }, rollId: {
                            required: "<div class='reqF_1'>Please select Role.</div>"
                        }
                        
                    },
                    errorPlacement:function(error ,element)
                    {
                        if(element.attr("name")=="phoneNo")
                        {
                            error.insertAfter("#fxno")
                        }
                        else if(element.attr("name")=="password")
                        {
                            error.insertAfter("#tipPassword");
                        }
                        else if(element.attr("name")=="RoleType")
                        {
                            error.insertAfter("#rtyperadio");
                        }
                        else
                            error.insertAfter(element);
                    }
                }
            );
            });

            function checkctrlkey(){
                jAlert("Copy Paste not allowed.","<%=OMTitle%>", function(RetVal) {});
                return false;
            }

            function checkrightclick(e){
                $(e).bind("contextmenu",function(e){
                    return false;
                });
            }

            function checkRoleType(obj)
            {
                if(obj.value == "I")
                {
                    // document.getElementById("tdIndividual").style.display='block';
                    document.getElementById("tdRole").style.display='none';
                    document.getElementById("rollId").selectedIndex=0;
                }
                else if(obj.value == "R")
                {
                    // document.getElementById("tdIndividual").style.display='none';
                    document.getElementById("tdRole").style.display='block';
                }



            }
        </script>

    </head>

    <jsp:useBean id="conAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ConAdminSrBean"/>
    <jsp:useBean id="adminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AdminMasterDtBean"/>
    <jsp:useBean id="loginMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.LoginMasterDtBean"/>
    <jsp:useBean id="manageAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.UpdateAdminSrBean"/>
    <jsp:setProperty name="adminMasterDtBean" property="*"/>
    <jsp:setProperty name="loginMasterDtBean" property="*"/>

    <%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
    <%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
    <body>
        <%

                    String action="add";
                    if(request.getParameter("action")!= null && request.getParameter("action").equalsIgnoreCase("Edit"))
                    {
                        action="Edit";
                    }

                    int uId=0;
                    if(request.getParameter("uId")!= null)
                    {
                        uId=Integer.parseInt(request.getParameter("uId"));
                    }

                    List<Object[]> objUserDetail=null;
                    Object [] objArrUserDetail=null;
                    if (action != null && action.equalsIgnoreCase("Edit"))
                    {
                        objUserDetail=objManageuser.getOandMUserDetails(uId);
                        if(objUserDetail!= null && objUserDetail.size()> 0)
                            objArrUserDetail=objUserDetail.get(0);
                    }
                    
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        conAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                        manageAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                    }
        
                    
                    if ("Submit".equals(request.getParameter("Add")))
                    {
                        String msg = "";
                        int strM = -1; // Default
                        String mailId = loginMasterDtBean.getEmailId();
                        //String nationalId = adminMasterDtBean.getNationalId();
                        //String mobileNo = adminMasterDtBean.getMobileNo();
                        if (!mailId.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-\\-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")) {
                            strM = 1; //Not Valid EmailId
                        }
                        if (strM == -1) {
                            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                            msg = commonService.verifyMail(mailId.trim());
                            if (msg.length() > 2) {
                                strM = 2; //EmailId Already Exist
                            }
                            /*if(strM == -1){
                            msg = commonService.verifyNationalId(nationalId.trim());
                            if(msg.length() > 2){
                            strM = 3; //NationalId Already Exist
                            }
                            }*/
                            /* if (strM == -1) {
                            msg = commonService.verifyMobileNo(mobileNo.trim());
                            if (msg.length() > 2) {
                            strM = 4; //Mobile No Already Exist
                            }
                            }*/
                        }
                        if (strM == -1) {

                            if (adminMasterDtBean.getMobileNo() == null) {
                                adminMasterDtBean.setMobileNo("");
                            }
                            if (adminMasterDtBean.getNationalId() == null) {
                                adminMasterDtBean.setNationalId("");
                            }
                            conAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                            int cId;

                            String isUser = "";
                             if (request.getParameter("isUser") != null && request.getParameter("isUser").toString().equals("Yes")) {
                                 isUser = "O&M";
                              }
                            else if(request.getParameter("isUser") != null && request.getParameter("isUser").toString().equals("DLI")) {
                                 isUser = "DLI";
                              }

                            if (isUser.equals("O&M")) {
                                cId = conAdminSrBean.conAdminReg(adminMasterDtBean, loginMasterDtBean, request.getParameter("password"), (byte) 20, "Create " + OMTitle);
                            } else if (isUser.equals("DLI")) {
                                cId = conAdminSrBean.conAdminReg(adminMasterDtBean, loginMasterDtBean, request.getParameter("password"), (byte) 21, "Create " + OMTitle);
                            }
                            else {
                                cId = conAdminSrBean.conAdminReg(adminMasterDtBean, loginMasterDtBean, request.getParameter("password"), (byte) 19, "Create " + OMTitle);
                            }

                            if (cId > 0) {
                                if (isUser.equals("O&M")) {
                                    // System.out.println("Role type selected by user is :"+request.getParameter("RoleType"));
                                    // response.sendRedirect("ManageUserRights.jsp?uId=" + cId + "&userTypeid=20&msg=");
                                    if (request.getParameter("RoleType").equals("R")) {
                                        response.sendRedirect("OAndMAdminGrid.jsp?userTypeid=20&isUser=Yes&RoleType=" + request.getParameter("RoleType"));
                                    } else {
                                        response.sendRedirect("ManageUserRights.jsp?uId=" + cId + "&userTypeid=20&msg=");
                                    }

                                }
                                else if(isUser.equals("DLI"))
                                    {
                                      response.sendRedirect("OAndMAdminGrid.jsp?userTypeid=21&isUser=DLISuccess");
                                    }
                                else {
                                    response.sendRedirect("ManageUserRights.jsp?uId=" + cId + "&userTypeid=19&msg=");
                                }
                                //response.sendRedirect("ViewManageAdmin.jsp?userId=" + cId + "&userTypeid=19&msg=create");
                                // To Display user rights screen after crating admin
                            } else {
                                String strResponseURL ="OANDMRegister.jsp?msg=creationFail";
                                 if (isUser.equals("O&M")) {
                                     strResponseURL = strResponseURL+"&isUser=Yes";
                                 }
                                 else if (isUser.equals("DLI")) {
                                     strResponseURL = strResponseURL+"&isUser=DLI";
                                 }
                                 response.sendRedirect(strResponseURL);
                            }
                        } else {
                            response.sendRedirect("OANDMRegister.jsp?strM=" + strM);
                        }
                    }
                    else if ("Update".equals(request.getParameter("Edit")))
                    {
                        String msg = "";
                        int strM = -1; // Default
                            if (adminMasterDtBean.getMobileNo() == null) {
                                adminMasterDtBean.setMobileNo("");
                            }
                            if (adminMasterDtBean.getNationalId() == null) {
                                adminMasterDtBean.setNationalId("");
                            }
                                    
                            manageAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                            boolean returnval=true;
                            if (request.getParameter("isUser") != null && request.getParameter("isUser").toString().equals("Yes")) {
                                returnval=manageAdminSrBean.organizationUpdate(adminMasterDtBean, "Edit O & M User", EgpModule.Manage_Users.getName());
                                if(adminMasterDtBean.getRollId()!= 0) // Role wise rights then delete from individual rights.
                                    objManageuser.deleteUserMenuRights(adminMasterDtBean.getUserId());
                            } 

                            if (returnval) {
                                if (request.getParameter("isUser") != null && request.getParameter("isUser").toString().equals("Yes")) {
                                    // System.out.println("Role type selected by user is :"+request.getParameter("RoleType"));
                                    // response.sendRedirect("ManageUserRights.jsp?uId=" + cId + "&userTypeid=20&msg=");
                                    if (request.getParameter("RoleType").equals("R")) {
                                        response.sendRedirect("OAndMAdminGrid.jsp?userTypeid=20&isUser=Yes&RoleType=" + request.getParameter("RoleType"));
                                    } else {
                                        response.sendRedirect("ManageUserRights.jsp?uId=" + uId + "&userTypeid=20&msg=");
                                    }

                                } else {
                                    response.sendRedirect("ManageUserRights.jsp?uId=" + uId + "&userTypeid=19&msg=");
                                }
                                //response.sendRedirect("ViewManageAdmin.jsp?userId=" + cId + "&userTypeid=19&msg=create");
                                // To Display user rights screen after crating admin
                            } else {
                                response.sendRedirect("OANDMRegister.jsp?msg=creationFail");
                            }

                        
                    }
                    else
                    {
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle O & M Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                                StringBuilder userType = new StringBuilder();
                                                if (request.getParameter("hdnUserType") != null) {
                                                    if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                                        userType.append(request.getParameter("hdnUserType"));
                                                    } else {
                                                        userType.append("org");
                                                    }
                                                } else {
                                                    userType.append("org");
                                                }
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1">
                            <%
                                                    int strM = -1;
                                                    String msg = "";
                                                    if (request.getParameter("strM") != null) {
                                                        try {
                                                            strM = Integer.parseInt(request.getParameter("strM"));
                                                        } catch (Exception ex) {
                                                            strM = 5;
                                                        }
                                                        switch (strM) {
                                                            case (1):
                                                                msg = "Please enter valid e-mail ID";
                                                                break;
                                                            case (2):
                                                                msg = "e-mail ID already exists, Please enter another e-mail ID.";
                                                                break;
                                                            case (3):
                                                                msg = "National-ID already exists.";
                                                                break;
                                                            case (4):
                                                                msg = "Mobile No already exists.";
                                                                break;
                                                        }
                            %>
                            <div class="responseMsg errorMsg" style="margin-left:7px; margin-right: 2px; margin-top: 15px;">
                                <%= msg%>
                            </div>
                            <div>&nbsp;</div>
                            <%
                                                        msg = null;
                                                    }
                            %>
                            <!--Page O & M Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1">
                                        <div class="pageHead_1">
                                        <%
                                            if(action!=null && action.equalsIgnoreCase("Edit"))
                                            {
                                        %>
                                            Edit 
                                        <%
                                            }
                                            else
                                            {
                                        %>
                                            Create
                                        <%
                                            }
                                        %>
                                            <%=OMTitle%></div>
                                        <% if (request.getParameter("msg") != null && "creationFail".equals(request.getParameter("msg"))) {%>
                                        <div class='responseMsg errorMsg'><%=(request.getParameter("isUser") != null && request.getParameter("isUser").toString().equals("Yes")?appMessage.OAndMUserReg:appMessage.OAndMAdminReg)%></div>
                                        <% }%>
                                        <form id="frmOAndMAdmin" name="frmOAndMAdmin" method="post">
                                            <input type="hidden" name="action" id="action" value="<%=action%>"/>
                                            <% if(action!=null && action.equalsIgnoreCase("Edit"))
                                            {
                                            %>

                                            <input type="hidden" name="userId" id="userId" value="<%=uId%>"/>
                                            <input type="hidden" name="adminId" id="adminId" value="<%=objArrUserDetail[7]%>"/>
                                            <%
                                            }
                                            %>
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td style="font-style: italic" class="ff t-align-left" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">e-mail ID : <span>*</span></td>
                                                    <td>
                                                        <%
                                                        if(action!=null && action.equalsIgnoreCase("Edit"))
                                                        {
                                                          %>
                                                          <%=objArrUserDetail[6]%>
                                                          <input  type="hidden" id="txtMail" name="emailId"  maxlength="100" value="<%=objArrUserDetail[6]%>"/>
                                                          <%
                                                        }
                                                        else
                                                        {
                                                        %>
                                                        <input style="width:195px;" class="formTxtBox_1" type="text" id="txtMail" name="emailId"  maxlength="100"/>
                                                        <%
                                                        }
                                                        %>
                                                        <span id="mailMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <%
                                                if(action!=null && !action.equalsIgnoreCase("Edit"))
                                                {
                                                %>
                                                <tr>
                                                    <td class="ff">Password : <span>*</span></td>
                                                    <td><input style="width:195px;" class="formTxtBox_1" type="password" id="txtPassword" name="password" autocomplete="off" /><br/><span id="tipPassword" style="color: grey;"> ( Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. 
                                                <br/>Special characters may be added) </span>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="ff">Confirm Password : <span>*</span></td>
                                                    <td><input class="formTxtBox_1" style="width:195px;" type="password" id="txtConfPassword" name="confPassword" value="" onpaste="return checkctrlkey();" autocomplete="off" />
                                                    </td>
                                                </tr>
                                                <%
                                                }
                                                   if (OMTitle == "O & M User")
                                                   {
                                                       
                                                %>
                                                <tr>
                                                    <td class="ff" >Role Type :</td>
                                                    <td>
                                                        <div id="rtyperadio">
                                                    <%
                                                    if(action!=null && action.equalsIgnoreCase("Edit"))
                                                    {
                                                      %>
                                                        
                                                         <%
                                                         if(objArrUserDetail != null && objArrUserDetail[9]!= null && !objArrUserDetail[9].toString().equalsIgnoreCase("0"))
                                                         {
                                                             %>
                                                                <input type="radio" name="RoleType"  value="I" onclick="checkRoleType(this);"/> Individual
                                                                <input type="radio" name="RoleType"  Value="R" onclick="checkRoleType(this);" checked/> Role Wise
                                                             <%
                                                         }
                                                         else 
                                                         {
                                                             %>
                                                                <input type="radio" name="RoleType"  value="I" onclick="checkRoleType(this);" checked/> Individual
                                                                <input type="radio" name="RoleType"  Value="R" onclick="checkRoleType(this);" /> Role Wise
                                                             <%
                                                         }
                                                    }
                                                    else
                                                    {
                                                    %>
                                                        <input type="radio" name="RoleType"  value="I" onclick="checkRoleType(this);"/> Individual
                                                        <input type="radio" name="RoleType"  Value="R" onclick="checkRoleType(this);"/> Role Wise
                                                    <%
                                                    }
                                                    %>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> </td>
                                                    <%
                                                      if(objArrUserDetail != null && objArrUserDetail[9]!= null && !objArrUserDetail[9].toString().equalsIgnoreCase("0"))
                                                      {
                                                    %>
                                                    <td id="tdRole"  style="display: block;">
                                                    <%
                                                       }
                                                    else
                                                        {
                                                        %>
                                                    <td id="tdRole"  style="display: none;">
                                                    <%
                                                        }
                                                    %>
                                                        <%
                                                             List<TblRoleMaster> roleMaster = objManageuser.getAllActiveRoles();
                                                             //  System.out.println("rolemaster size :"+roleMaster.size());
                                                        %>
                                                        <select style="width:200px;" class="formTxtBox_1" name="rollId" id="rollId">
                                                            <option value="">-- Select Roles --</option>
                                                            <% for (int i = 0; i < roleMaster.size(); i++) 
                                                            {
                                                            %>
                                                                <option  value=<%=roleMaster.get(i).getRollId()%>
                                                                <%
                                                                  if(objArrUserDetail != null && objArrUserDetail[9]!= null && objArrUserDetail[9].toString().equalsIgnoreCase(roleMaster.get(i).getRollId()+""))
                                                                  {
                                                                      out.println("selected");
                                                                  }
                                                                %>
                                                                 ><%=roleMaster.get(i).getRollName()%></option>
                                                            <%
                                                            }
                                                            %>
                                                        </select>
                                                        <% }%>
                                                    </td>
                                                </tr>

                                                <tr>

                                                    <td class="ff">Full Name : <span>*</span></td>

                                                    <td>
                                                        <%
                                                        if(action!=null && action.equalsIgnoreCase("Edit"))
                                                        {
                                                          %>
                                                          <input style="width:195px;" class="formTxtBox_1" value="<%=objArrUserDetail[4]%>" type="text" id="txtFullName" name="fullName" maxlength="150"/>
                                                          <%
                                                        }
                                                        else
                                                        {
                                                        %>
                                                        <input style="width:195px;" class="formTxtBox_1"type="text" id="txtFullName" name="fullName" maxlength="150"/>
                                                        <%
                                                        }
                                                        %>
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">CID No. : </td>

                                                    <td>
                                                        <%
                                                        if(action!=null && action.equalsIgnoreCase("Edit"))
                                                        {
                                                          %>
                                                          <input style="width:195px;" class="formTxtBox_1" value="<%=objArrUserDetail[3]%>" type="text" id="txtNationalId" name="nationalId" maxlength="26"/>
                                                        
                                                          <%
                                                        }
                                                        else
                                                        {
                                                        %>
                                                        <input style="width:195px;" class="formTxtBox_1" type="text" id="txtNationalId" name="nationalId" maxlength="26"/>
                                                        <%
                                                        }
                                                        %>
                                                        <span id="errMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Phone No. : <span>*</span></td>
                                                    <td>
                                                        <%
                                                        if(action!=null && action.equalsIgnoreCase("Edit"))
                                                        {
                                                          %>
                                                          <input style="width:195px;" class="formTxtBox_1" value="<%=objArrUserDetail[8]%>" type="text" name="phoneNo" id="txtPhoneNo" maxlength="20"/>
                                                          <%
                                                        }
                                                        else
                                                        {
                                                        %>
                                                        <input style="width:195px;" class="formTxtBox_1" type="text" name="phoneNo" id="txtPhoneNo" maxlength="20"/>
                                                        <%
                                                        }
                                                        %>

                                                        <span id="fxno" name="fxno" style="color: grey;"> AreaCode-Phone No. i.e. 02-336962</span>
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td class="ff">Mobile No. : </td>

                                                    <td>
                                                        <%
                                                        if(action!=null && action.equalsIgnoreCase("Edit"))
                                                        {
                                                          %>
                                                          <input style="width:195px;" class="formTxtBox_1" value="<%=objArrUserDetail[2]%>" type="text" name="mobileNo" id="txtMobileNo" maxlength="16"/>
                                                          <%
                                                        }
                                                        else
                                                        {
                                                        %>
                                                        <input style="width:195px;" class="formTxtBox_1" type="text" name="mobileNo" id="txtMobileNo" maxlength="16"/>
                                                        <%
                                                        }
                                                        %>

                                                        <span id="mNo" style="color: grey;">(Mobile No. format should be e.g 12345678)</span>
                                                        <span id="mobMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <tr><td></td>
                                                    <td align="left">
                                                        <label class="formBtn_1">
                                                            <%
                                                            if(action!=null && action.equalsIgnoreCase("Edit"))
                                                            {
                                                              %>
                                                              <input type="submit" name="Edit" id="btnEdit" value="Update" onClick="return validate();"/>
                                                              <%
                                                            }
                                                            else
                                                            {
                                                            %>
                                                            <input type="submit" name="Add" id="btnAdd" value="Submit" onClick="return validate();"/>
                                                            <%
                                                            }
                                                            %>
                                                            
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page O & M End-->
                        </td>
                    </tr>
                </table>
                <!--Middle O & M Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <%
                    }
        %>
        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
    <script type="text/javascript">
        $(function() {
            $('#txtMail').blur(function() {
                var varEmailId  = trim(document.getElementById("txtMail").value);
                if(varEmailId!=""){
                    $('span.#mailMsg').html("Checking for unique Mail Id...");
                    $.post("<%=request.getContextPath()%>/CommonServlet", {mailId:varEmailId,funName:'verifyMail'},  function(j){
                        if(j.toString().indexOf("OK", 0)!=-1){
                            $('span.#mailMsg').css("color","green");
                            validCheckMail = true;
                        }
                        else if(j.toString().indexOf("Mail", 0)!=-1){
                            $('span.#mailMsg').css("color","red");
                            validCheckMail = false;
                        }
                        $('span.#mailMsg').html(j);
                    });
                }else{
                    $('span.#mailMsg').html("");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#txtNationalId').blur(function() {
                var varNatioinalId = trim(document.getElementById("txtNationalId").value);
                if(varNatioinalId!=""){
                    //  $('span.#errMsg').html("Checking for unique National Id...");
                    $.post("<%=request.getContextPath()%>/CommonServlet", {nationalId:varNatioinalId,funName:'verifyNationalId'},  function(j){
                        if(j.toString().indexOf("OK", 0)!=-1){
                            $('span.#errMsg').css("color","white");
                            validCheckNid = true;
                        }
                        else if(j.toString().indexOf("National", 0)!=-1){
                            $('span.#errMsg').css("color","red");
                            validCheckNid = false;
                        }
                        $('span.#errMsg').html(j);
                    });
                }else{
                    $('span.#errMsg').html("");
                }
            });
        });

        $(function() {
            $('#txtMobileNo').blur(function() {
                var varMobileNo = trim(document.getElementById("txtMobileNo").value);
                if(varMobileNo!=""){
                    if((document.getElementById("txtMobileNo").value.length>=10) && (document.getElementById("txtMobileNo").value.length<=15) && (/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(document.getElementById("txtMobileNo").value))) // max min number
                    {
                        /* $('span.#mobMsg').html("Checking for unique Mobile No...");
                        $.post("<%=request.getContextPath()%>/CommonServlet", {mobileNo:varMobileNo,funName:'verifyMobileNo'},  function(j){

                            if(j.toString().indexOf("OK", 0)==0){
                                $('span.#mobMsg').css("color","green");
                                validCheckMno = true;
                            }
                            else if(j.toString().indexOf("Mobile", 0)!=-1){
                                $('span.#mobMsg').css("color","red");
                                validCheckMno = false;
                            }
                            $('span.#mobMsg').html(j);
                        }); */
                    }
                }
                else{
                    $('span.#mobMsg').html("");
                }
            });
        });

        function validate(){
            if(document.getElementById("mailMsg")!=null && document.getElementById("mailMsg").innerHTML=="e-mail ID already exists, Please enter another e-mail ID."){
                return false;
            }
            if(document.getElementById("errMsg")!=null && document.getElementById("errMsg").innerHTML=="National-ID already exists."){
                return false;
            }
            if(document.getElementById("mobMsg")!=null && document.getElementById("mobMsg").innerHTML=="Mobile No. already exists."){
                return false;
            }
            if(validCheckMail && validCheckNid && validCheckMno){
                return true;
            }else{
                return false;
            }
        }
    </script>
</html>

