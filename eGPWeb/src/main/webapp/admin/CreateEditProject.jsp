<%--
    Document   : CreateEditProject
    Created on : Nov 1, 2010, 8:52:20 PM
    Author     : parag
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblProjectRoles"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPProjectRolesReturn"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="projSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectSrBean" />
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.List,java.util.Date,com.cptu.egp.eps.dao.storedprocedure.CommonAppData,com.cptu.egp.eps.web.utility.SelectItem" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPProjectDetailReturn,java.text.SimpleDateFormat"  %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <%
                int projectId = 0;
                int suserTypeId = 0;

                if (session.getAttribute("userTypeId") != null) {
                    suserTypeId = Byte.parseByte(session.getAttribute("userTypeId").toString());
                }
    %>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Project Details : Key Information</title>
        <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                
                 $.validator.addMethod('PrecisionControl', function(value, element) {
                    return /^[0-9,]+(\.\d{0,6})?$/.test(value); 
                }, "");
                
                $("#frmCreateEditProject").validate({
                    rules: {
                        txtProjCode:{required: true,maxlength: 150},
                        txtProjName:{required: true,maxlength: 500},
                        txtProjCost:{required:true,number:true,PrecisionControl:true},
                        dtStartDate:{required:true},//, CompareToForToday:"#dtStartDate"
                        //dtStartDate:{required:true, CompareToForToday:"#dtStartDate"},
                        // dtStartDate:{required:true,CompareToForGreater: "#dtCurrentDate"},
                        dtEndDate:{required:true,CompareToForGreater:"#dtStartDate"},
                        cmbSource:{required:true}
                        //,cmbDPartner:{required:true}


                    },
                    messages: {
                        txtProjCode:{ required: "<div class='reqF_1'>Please enter Project Code</div>",
                            maxlength:"<div class='reqF_1'>Maximum 150 characters are allowed</div>"},

                        txtProjName:{ required: "<div class='reqF_1'>Please enter Project Name</div>",
                            maxlength:"<div class='reqF_1'>Maximum 500 characters are allowed</div>"},

                        txtProjCost:{required:"<div class='reqF_1'>Please enter Project Cost</div>",
                            number:"<div class='reqF_1'>Please enter numeric value</div>",
                            PrecisionControl:"<div class='reqF_1'>6 digits are allowed after decimal point.</div>"
                        },


                        dtStartDate:{required: "<div class='reqF_1'>Please select Project Start Date</div>"
                            //CompareToForToday:"<div class='reqF_1'>Project Information Start Date must be greater than the Current date</div>"
                        },

                    dtEndDate:{required:"<div class='reqF_1'>Please select Project End Date </div>",
                        CompareToForGreater:"<div class='reqF_1'>Project End Date  date must be greater than the Project Start Date</div>"
                    },

                    cmbSource:{required:"<div class='reqF_1'>Please select Source of Fund</div>"
                    }

                    //,cmbDPartner:{required:"<div class='reqF_1'>Please Select Development Partner.</div>"
                    //}
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "dtStartDate")
                        error.insertAfter("#dtStartDate1");
                    else if (element.attr("name") == "dtEndDate")
                        error.insertAfter("#dtEndDate1");
                    else
                        error.insertAfter(element);
                }
            }

                
        );
        
        });

        </script>
        <script type="text/javascript">
        function GetCal(txtname,controlname)
        {
            new Calendar({
                inputField: txtname,
                trigger: controlname,
                showTime: false,
                dateFormat:"%d/%m/%Y",
                onSelect: function() {
                    var date = Calendar.intToDate(this.selection.get());
                    LEFT_CAL.args.min = date;
                    LEFT_CAL.redraw();
                    this.hide();
                    document.getElementById(txtname).focus();
                }
            });

            var LEFT_CAL = Calendar.setup({
                weekNumbers: false
            })
        }

        function checkProjectCode(){
            // $('#txtProjCode').blur(function() {
            if($.trim(document.getElementById("txtProjCode").value) != "" ){
                $.post("<%=request.getContextPath()%>/ProjectSrBean", {projectCode:$.trim($('#txtProjCode').val()),projectId:<%=session.getAttribute("userId")%>,funName:'verifyProjCode'},
                function(j){
                    if(j == "<br />OK"){
                        $('span.#projCodeMsg').css("color","green");
                    }
                    else{
                        $('span.#projCodeMsg').css("color","red");
                    }
                    $('span.#projCodeMsg').html(j);
                });
            }else{
                document.getElementById("projCodeMsg").innerHTML="";
            }
            // });
        }

        function checkProjectName(){
            // $('#txtProjName').blur(function() {
            if($.trim(document.getElementById("txtProjName").value) != "" ){
                $.post("<%=request.getContextPath()%>/ProjectSrBean", {projectName:$.trim($('#txtProjName').val()),projectId:<%=session.getAttribute("userId")%>,funName:'verifyProjName'},
                function(j){
                    if(j == "<br />OK"){
                        $('span.#projNameMsg').css("color","green");
                    }
                    else{
                        $('span.#projNameMsg').css("color","red");
                    }
                    $('span.#projNameMsg').html(j);
                });
            }else{
                document.getElementById("projNameMsg").innerHTML="";
            }
            //  });
        }
           
           
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                            // List<CommonAppData> programmList = projSrUser.getProgramList();
                            boolean isEdit = false;
                            String action = "create";


                            List<SelectItem> programList = projSrUser.getProgramList();
                            List<SelectItem> developmentPartner = projSrUser.getDevelopmentPartner();
                            SPProjectDetailReturn pProjectDetailReturns = null;
                            List<SPProjectDetailReturn> developmentPar = null;

                            if ("Edit".equalsIgnoreCase(request.getParameter("Edit"))) {
                                
                                if (request.getParameter("projectId") != null) {
                                    projectId = Integer.parseInt(request.getParameter("projectId"));
                                }
                                
                                pProjectDetailReturns = projSrUser.getpProjectDetailReturns(projectId,Integer.parseInt(session.getAttribute("userId").toString())).get(0);
                                
                                developmentPar = projSrUser.getDevelopmentPar(projectId,Integer.parseInt(session.getAttribute("userId").toString()));
                                // projectId = pProjectDetailReturns.getProjectId();
                                
                                action = "update";
                                isEdit = true;
                            }

                            java.util.Calendar currentDate = java.util.Calendar.getInstance();
                            
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <%--<%@include file="/resources/common/AfterLoginLeft.jsp" %>--%>

                        <td class="contentArea">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td>
                                        <!--Dashboard Header End-->
                                        <!--Dashboard Content Part Start-->
                                        <div class="pageHead_1">Project Information Details</div>


                                        <!--Dashboard Header End-->
                                        <!--Dashboard Content Part Start-->
                                        <%--<div class="pageHead_1">Project Details : Key Information</div>
                                        <div>&nbsp;</div>--%>
                                        <%--<div id="action-tray">
                                            <ul>
                                                <li><a href="ProjectDetail.jsp" class="action-button-goback">Go Back</a></li>
                                            </ul>
                                        </div>--%>

                                        <div class="stepWiz_1 t_space">
                                            <ul>
                                                <li class="sMenu">Key Fields Information</li>
                                                <li>&gt;&gt;&nbsp;&nbsp; <%
                                                            List<SPProjectRolesReturn> projectRolesReturns = null;
                                                            List<TblProjectRoles> projectroles = null;
                                                            int peuserid = 0;
                                                            int pduserid = 0;
                                                            

                                                            projectroles = projSrUser.checkIsProjectRoleExist(projectId);
                                                            

                                                            int uid = 0;
                                                            if (projectroles.size() > 0) {
                                                                TblProjectRoles proroles = projectroles.get(0);
                                                                uid = proroles.getTblLoginMaster().getUserId();
                                                    %>
                                                    <a style="text-decoration: underline;" href="ProjectRoles.jsp?hidProjectId=<%=projectId%>&Edit=Edit&userId=<%=uid%>" >Add Users </a>
                                                    <% } else {%> Add Users <% }%> </li>
                                                <%--<!--                                                <li>&gt;&gt;&nbsp;&nbsp;-->
                                                    <%

                                                                projectRolesReturns = projSrUser.getRolesReturns(projectId);
                                                                

                                                                if (projectRolesReturns.size() > 0) {
                                                                    for (int i = 0; i < projectRolesReturns.size(); i++) {
                                                                        
                                                                        if (projectRolesReturns.get(i).getRolesName().trim().contains("PD")) {
                                                                            pduserid = projectRolesReturns.get(i).getUserId();
                                                                        } else if (projectRolesReturns.get(i).getRolesName().trim().contains("PE")) {
                                                                            peuserid = projectRolesReturns.get(i).getUserId();
                                                                        }
                                                                    }
                                                                    if (pduserid == peuserid && pduserid != 0 && peuserid != 0) {

                                                    %>
<!--                                                    <a href="ProjectUserFP.jsp?Edit=Edit&userId=<%=pduserid%>&projectId=<%=projectId%>" style="text-decoration: underline;"> User Financial Power </a>-->
                                                    <%} else if (pduserid != peuserid && pduserid != 0 && peuserid != 0) {

                                                    %>
<!--                                                    <a href="ProjectUserFP.jsp?Edit=Edit&userId=<%=pduserid%>&projectId=<%=projectId%>" style="text-decoration: underline;"> User Financial Power(PD) </a>-->
                                                    &nbsp;|&nbsp
<!--                                                    <a href="ProjectUserFP.jsp?Edit=Edit&userId=<%=peuserid%>&projectId=<%=projectId%>" style="text-decoration: underline;"> User Financial Power(PE) </a>-->
                                                    <%
                                                                                                            } else if (pduserid != 0) {
                                                    %>
<!--                                                    <a href="ProjectUserFP.jsp?Edit=Edit&userId=<%=pduserid%>&projectId=<%=projectId%>" style="text-decoration: underline;"> User Financial Power </a>-->
                                                    <%
                                                                                                            } else if (peuserid != 0) {
                                                    %>
<!--                                                    <a href="ProjectUserFP.jsp?Edit=Edit&userId=<%=peuserid%>&projectId=<%=projectId%>" style="text-decoration: underline;"> User Financial Power </a>-->
                                                    <%
                                                                                                                } else {%>
<!--                                                    User Financial Power-->
                                                    <%}
                                                                                                                    if (projectRolesReturns.isEmpty()) {
                                                                                                                        //out.print("User Financial Power");
                                                                                                                    }

                                                                                                                } else {%>
<!--                                                    User Financial Power-->
                                                    <% }%>
<!--                                                </li>-->--%>
                                            </ul>
                                        </div>

                                        <form method="post" id="frmCreateEditProject" action="<%=request.getContextPath()%>/ProjectSrBean?action=<%=action%>" >
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                                <tr>
                                                    <td style="font-style: italic" class="t-align-right" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                                </tr>

                                                <tr>
                                                    <td class="ff" width="30%">Select Programme : </td>
                                                    <td width="70%">
                                                        <%if (isEdit) {
                                                                        if ((pProjectDetailReturns.getProgName()).equals("")) {
                                                                            out.print("-");
                                                                        } else {
                                                                            out.print(pProjectDetailReturns.getProgName());
                                                                        }
                                                        %>
                                                        <input name="cmbProg" type="hidden" class="formTxtBox_1" id="cmbProg"
                                                               value="<%=pProjectDetailReturns.getProgId()%>" style="width:200px;" />
                                                        <%
                                                        } else {
                                                        %>
                                                        <select name="cmbProg" class="formTxtBox_1" id="cmbProg" style="width:230px;">

                                                            <%for (int i = 0; i < (programList.size()); i++) {%>
                                                            <option value="<%=programList.get(i).getObjectId()%>"  >
                                                                <%=programList.get(i).getObjectValue()%></option>
                                                                <%
                                                                    }
                                                                %>
                                                        </select>
                                                        <%
                                                                    }
                                                        %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Project Code :  <%if (!isEdit) {%> <span class="mandatory">*</span><%}%></td>
                                                    <td>
                                                        <%
                                                                    if (isEdit) {
                                                                        out.print(pProjectDetailReturns.getProjectCode());
                                                        %>
                                                        <input name="txtProjCode" type="hidden" class="formTxtBox_1" id="txtProjCode"
                                                               value="<%=pProjectDetailReturns.getProjectCode()%>" style="width:200px;" onblur="checkProjectCode();" />
                                                        <%
                                                                                                                            } else {
                                                        %>
                                                        <input name="txtProjCode" type="text" class="formTxtBox_1" id="txtProjCode" onblur="checkProjectCode();"
                                                               value="" style="width:195px;" />
                                                        <span id="projCodeMsg" style="color: red;">&nbsp;</span>
                                                        <%                      }
                                                        %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Project Name : <%if (!isEdit) {%> <span>*</span><%}%></td>
                                                    <td>
                                                        <%
                                                                    if (isEdit) {
                                                                        out.print(pProjectDetailReturns.getProjectName());
                                                        %>
                                                        <input name="txtProjName" type="hidden" class="formTxtBox_1" id="txtProjName"
                                                               value="<%=pProjectDetailReturns.getProjectName()%>" style="width:195px;" onblur="checkProjectName();" />
                                                        <%
                                                                                                                            } else {
                                                        %>
                                                        <input name="txtProjName" type="text" class="formTxtBox_1" id="txtProjName" onblur="checkProjectName();" 
                                                               value="" style="width:195px;" />
                                                        <span id="projNameMsg" style="color: red;">&nbsp;</span>
                                                        <%                      }
                                                        %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Project Cost In million (Nu.) :  <span>*</span></td>
                                                    <td><input name="txtProjCost" type="text" class="formTxtBox_1" id="txtProjCost"
                                                               value="<%if (isEdit) {
                                                                               out.print(String.format("%.6f", pProjectDetailReturns.getProjectCost().divide(new BigDecimal("1000000"),8,BigDecimal.ROUND_HALF_UP)));
                                                                           }%>" style="width:195px;" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Project Start Date :   <span>*</span></td>
                                                    <% SimpleDateFormat sd = new SimpleDateFormat("dd-MMM-yyyy");
                                                                SimpleDateFormat sd1 = new SimpleDateFormat("dd/MM/yyyy");
                                                    %>
                                                    <td><input name="dtStartDate" type="text" class="formTxtBox_1" id="dtStartDate" onfocus="GetCal('dtStartDate','dtStartDate');"
                                                               value="<%if (isEdit) {
                                                                               out.print(sd1.format(sd.parse(pProjectDetailReturns.getProjectStartDate())));
                                                                               // out.print(pProjectDetailReturns.getProjectStartDate());
                                                                           }%>" style="width:100px;" readonly  />
                                                        <img id="dtStartDate1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('dtStartDate','dtStartDate1');"/>
                                                        <%--<input type="hidden" name="dtCurrentDate" id="dtCurrentDate" value="<%=new SimpleDateFormat("dd/MM/yyyy").format(new Date())%>"  />--%>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="ff">Project End Date :   <span>*</span></td>
                                                    <td><input name="dtEndDate" type="text" class="formTxtBox_1" id="dtEndDate" onfocus="GetCal('dtEndDate','dtEndDate');"
                                                               value="<%if (isEdit) {
                                                                               out.print(sd1.format(sd.parse(pProjectDetailReturns.getProjectEndDate())));
                                                                               //   out.print(pProjectDetailReturns.getProjectEndDate());
                                                                           }%>"
                                                               style="width:100px;"  readonly />
                                                        <img id="dtEndDate1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick="GetCal('dtEndDate','dtEndDate1');"/>

                                                    </td>

                                                </tr>

                                                <tr>
                                                    <td class="ff">Source of Fund :  <span>*</span></td>
                                                    <td>
                                                        <select name="cmbSource" class="formTxtBox_1" multiple id="cmbSource" style="width:200px;"  >
                                                            <%if (isEdit) {%>
                                                            <option value="Government" <%if (pProjectDetailReturns.getSourceOfFund().contains("Government")) {%>selected<%}%> >Government</option>
                                                            <option value="Aid or Grant" <%if (pProjectDetailReturns.getSourceOfFund().contains("Aid or Grant")) {%>selected<%}%>>Aid or Grant</option>
                                                            <option value="Own fund" <%if (pProjectDetailReturns.getSourceOfFund().contains("Own fund")) {%>selected<%}%> >Own fund</option>
                                                            <%} else {%>
                                                            <option value="Government" selected >Government</option>
                                                            <option value="Aid or Grant" >Aid or Grant</option>
                                                            <option value="Own fund">Own fund</option>
                                                            <%}%>
                                                        </select> </td>
                                                </tr>

                                                <tr id="trDevePar" >
                                                    <td class="ff">Development Partner :  <span>*</span></td>
                                                    <td><select name="cmbDPartner" class="formTxtBox_1" id="cmbDPartner" multiple="multiple" size="5" style="width:200px;">
                                                            <%boolean isSelected = false;
                                                                        if (isEdit) {%>
                                                            <%for (int i = 0; i < (developmentPartner.size()); i++) {
                                                                                                                                 for (int j = 0; j < (developmentPar.size()); j++) {
                                                                                                                                     if (developmentPar.get(j).getsBankDevelopId() == Integer.parseInt(developmentPartner.get(i).getObjectId().toString())) {
                                                                                                                                         isSelected = true;
                                                                                                                                         
                                                                                                                                     }
                                                                                                                                 }
                                                            %>
                                                            <option value="<%=developmentPartner.get(i).getObjectId()%>" <%if (isSelected) {%>selected<%}%> >
                                                                <%=developmentPartner.get(i).getObjectValue()%>
                                                            </option>
                                                            <%    isSelected = false;
                                                                                                                             }
                                                                                                                         } else {%>
                                                            <%for (int i = 0; i < (developmentPartner.size()); i++) {%>
                                                            <option value="<%=developmentPartner.get(i).getObjectId()%>">
                                                                <%=developmentPartner.get(i).getObjectValue()%>
                                                            </option>
                                                            <%}
                                                                        }%>
                                                        </select>
                                                        <span id="developerror"></span>
                                                    </td>
                                                </tr>
                                                <%--<%if(isEdit){%>
                                                <script type="text/javascript">
                                                    alert($('#cmbSource').val());
                                                    if($('#cmbSource').val().indexOf("Aid or Grant", 0)!=-1){
                                                    $('#trDevePar').show();
                                                    }else{
                                                        $('#trDevePar').hide();
                                                    }
                                                </script>
                                                <%}%>--%>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <%if (!isEdit) {%>
                                                    <td>
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="btnSubmit" id="btnSubmit" value="Submit" onclick="return checkData();" />
                                                        </label>
                                                        &nbsp;&nbsp;&nbsp;
                                                    </td>
                                                    <%} else {%>
                                                    <td>
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="btnSubmit" id="btnSubmit" value="Update" onclick="return checkData();" />
                                                        </label>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnNext" id="btnNext" value="Next" onclick="frmNext();" />
                                                        </label>
                                                    </td>
                                                    <td><label >
                                                            <input type="hidden" name="hidProjectId" id="hidProjectId" value="<%=projectId%>" />
                                                        </label>
                                                    </td>
                                                    <%}%>

                                                </tr>
                                            </table>
                                        </form>
                                    </td>

                                </tr>
                            </table>

                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>

                <%--<jsp:include page="/resources/common/Bottom.jsp" />--%>
                <%--<%@include file="/resources/common/Bottom.jsp" %>--%>
                <div>&nbsp;</div>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <%@include file="../resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
        <script>
        var obj = document.getElementById('lblProjInfoCreate');
        if(obj != null){
            if(obj.innerHTML == 'Create Project'){
                obj.setAttribute('class', 'selected');
            }
        }

        </script>
        <script>
        var headSel_Obj = document.getElementById("headTabContent");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        </script>
    </body>
    <form id="frmNext" >
        <input type="hidden" name="hidProjectId" id="hidProjectId" value="<%=projectId%>" />
    </form>
</html>
<script type="text/javascript">

function frmNext(){
    document.getElementById("frmNext").action="ProjectRoles.jsp?hidProjectId=<%=projectId%>";
    document.getElementById("frmNext").submit();
}

$(document).ready(function(){
    $("#cmbSource").change(onSelectChange);
    $("#cmbSource").keyup(onSelectChange);
});

function onSelectChange(){
    var selected = new Array();
    selected = $("#cmbSource option:selected");
    var flag = false;
    for(var i=0;i<selected.length;i++){
        var cmb=selected[i];
        if(cmb.value != 0){
            if(cmb.value == 'Aid or Grant' ){
                //$('#trDevePar').show();
                flag = true;
            }
            else{
                //$('#trDevePar').hide();
            }
        }
    }
    if(flag){
        $('#trDevePar').show();
    }else{
        $('#trDevePar').hide();
    }
        
}
<%--<%if(!isEdit){%>--%>
$(document).ready(function() {
    var selected = $("#cmbSource option:selected");
    if(selected.val() != 0){
        if(selected.text().indexOf("Aid or Grant", 0)!=-1){
            $('#trDevePar').show();
        }
        else{
            $('#trDevePar').hide();
        }
    }
});
<%--<%}%>--%>


function checkData(){
    //var sDt = document.getElementById("dtStartDate").value;
    //var eDt = document.getElementById("dtEndDate").value;
    //alert(CompareToForGreater(sDt,eDt)+"---CompareToForGreater");  
    //alert(CompareToForToday(sDt)+"----CompareToForToday");
   // CompareToForToday(sDt);
    //CompareToForGreater(sDt,eDt);
    //alert($('#cmbSource').val());
    if($('#cmbSource').val() == "Aid or Grant"){
        if($('#cmbDPartner').val() == null || $('#cmbDPartner').val() == ""){
            document.getElementById("developerror").innerHTML ="<div class='reqF_1'>Please select Development Partner.</div>";
            return false;
        }
        else{
            document.getElementById("developerror").innerHTML ='';
        }
    }
    
    
    if(document.getElementById("txtProjCode").value != "" ){
        if(document.getElementById("projCodeMsg").innerHTML.toString().indexOf("OK", 0) == -1){
            return false;
        }
    }
    if(document.getElementById("txtProjName").value != "" ){
        if(document.getElementById("projNameMsg").innerHTML.toString().indexOf("OK", 0) == -1){
            return false;
        }
    }
    //alert($('#frmCreateEditProject').valid()+"frmCreateEditProject.valid");
    if($('#frmCreateEditProject').valid()){
        $('#btnSubmit').attr("disabled", true);
        document.getElementById("frmCreateEditProject").submit();
        return true;
    }
}
    
      
</script>
<%--<script type ="text/javascript">
  function CompareToForToday(first)
               {
                   var mdy = first.split('/')  //Date and month split
                   var mdyhr= mdy[2].split(' ');  //Year and time split
                   //var mdyhrtime=mdyhr[1].split(':');
                   if(mdyhr[1] == undefined){
                       var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0]);
                   }else
                   {
                       var valuedate= new Date(mdyhr[0], mdy[1]-1, mdy[0], mdyhrtime[0], mdyhrtime[1]);
                   }

                   var d = new Date();
                   if(mdyhr[1] == undefined){
                       var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                   }
                   else
                   {
                       var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate(),d.getHours(),d.getMinutes());
                   }
                   //alert(Date.parse(valuedate) > Date.parse(todaydate)+"------Date.parse(valuedate) > Date.parse(todaydate)")
                   return Date.parse(valuedate) > Date.parse(todaydate);
               }

               //Function for CompareToForGreater
               function CompareToForGreater(value,params)
               {
                   if(value!='' && params!=''){

                       var mdy = value.split('/')  //Date and month split
                       var mdyhr=mdy[2].split(' ');  //Year and time split
                       var mdyp = params.split('/')
                       var mdyphr=mdyp[2].split(' ');


                       if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                       {
                           //alert('Both Date');
                           var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                           var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                       }
                       else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                       {
                           //alert('Both DateTime');
                           var mdyhrsec=mdyhr[1].split(':');
                           var date =  new Date( mdyhr[0], parseInt(mdy[1])-1, mdy[0], mdyhrsec[0], mdyhrsec[1]);
                           var mdyphrsec=mdyphr[1].split(':');

                           var datep =  new Date(mdyphr[0], parseInt(mdyp[1])-1, mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                   }
                       else
                       {
                           //alert('one Date and One DateTime');
                           var a = mdyhr[1];  //time
                           var b = mdyphr[1]; // time

                           if(a == undefined && b != undefined)
                           {
                               //alert('First Date');
                               var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                               var mdyphrsec=mdyphr[1].split(':');
                               var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                           }
                           else
                           {
                               //alert('Second Date');
                               var mdyhrsec=mdyhr[1].split(':');
                               var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                               var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                           }
                       }
                       //alert(Date.parse(date) > Date.parse(datep)+"-----");
                       return Date.parse(date) > Date.parse(datep);
                   }
                   else
                   {
                       return false;
                   }
               }
</script>--%>