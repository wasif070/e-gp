<%-- 
    Document   : ViewAdminAccountHistory
    Created on : Apr 21, 2011, 11:48:42 AM
    Author     : KETAN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.databean.ManageAdminDtBean" %>
<%@page import="com.cptu.egp.eps.model.table.TblUserActivationHistory"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<%@page import="java.util.Collections"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

                    byte userTypeid = 0;
                    if (request.getParameter("userTypeid") != null) {
                        userTypeid = Byte.parseByte(request.getParameter("userTypeid"));
                    }

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <% if (userTypeid == 12) {%>
        <title>View Procurement Expert Account History</title>
        <% } else if (userTypeid == 5) {%>
        <title>View Organization Admin Account History</title>
        <% } else if (userTypeid == 19) {%>
        <title>View O & M Admin History</title>
        <% } else if (userTypeid == 20) {%>
        <title>View O & M User History</title>
        <% }
            else if (userTypeid == 21) {%>
        <title>View Performance Monitoring User History</title>
        <% }
            else {%>
        <title>View Content Admin Account History</title>
        <% }%>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
    </head>
    <jsp:useBean id="manageAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.UpdateAdminSrBean"/>
    <jsp:useBean id="adminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AdminMasterDtBean"/>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%
                            if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                                manageAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                            }
                            int userId = 0;
                            if (request.getParameter("userId") != null) {
                                userId = Integer.parseInt(request.getParameter("userId"));
                            }
                %>
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("hdnUserType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                            userType.append(request.getParameter("hdnUserType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }
                                    int uTypeId = 0;
                                    sn = request.getSession();
                                    if (sn.getAttribute("userTypeId") != null) {
                                        uTypeId = Integer.parseInt(sn.getAttribute("userTypeId").toString());
                                    }

                        %>
                        <%
                                    TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
                                    List<TblUserActivationHistory> list = transferEmployeeServiceImpl.viewAccountStatusHistory(userId + "");
                                    boolean flag = false;
                                    int j = 1;
                        %>
                        <% if (Integer.parseInt(session.getAttribute("userTypeId").toString()) == 1) {%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <% }%>
                        <td class="contentArea_1">
                            <div class="pageHead_1">
                                <% if (userTypeid == 12) {%>
                                View Procurement Expert Account History
                                <%} else if (userTypeid == 8) {%>
                                View Content Admin Account History
                                <%} else if (userTypeid == 5) {%>
                                View Organization Account History
                                <%} else if (userTypeid == 19) {%>
                                View O & M Admin History
                                <%} else if (userTypeid == 20) {%>
                                View O & M User History
                                <%}
                                else if (userTypeid == 21) {%>
                                View Performance Monitoring User History
                                <%}
                                else {%>
                                View Organization Admin
                                <% }%>
                                <span class="c-alignment-right"><a href="ViewManageAdmin.jsp?userId=<%=userId%>&userTypeid=<%=userTypeid%>" class="action-button-goback">Go Back</a></span>
                            </div>
                            <!--Page Content Start-->
                            <form id="frmManageAdmin" name="frmManageAdmin" method="post" action="">
                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10">
                                    <%   for (ManageAdminDtBean manageAdminDtBean : manageAdminSrBean.getManageDetail(userTypeid, userId)) {
                                                    if (userTypeid == 5) {
                                    %>
                                    <tr>
                                        <td class="ff" width="200">PA :</td>
                                        <td>
                                            <label id="lblOrgaization"><%= manageAdminDtBean.getDepartmentName()%></label>
                                        </td>
                                    </tr>
                                    <% }%>
                                    <tr>
                                        <td class="ff" width="200">e-mail ID :</td>
                                        <td>
                                            <label id="lblEmailId"><%= manageAdminDtBean.getEmailId()%></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff" width="200">National ID :</td>
                                        <td>
                                            <label id="lblNationalId"><%=manageAdminDtBean.getNationalId()%></label>
                                            <input class="formTxtBox_1" name="nationalId" value="<%=manageAdminDtBean.getNationalId()%>" id="txtNationalID"  type="hidden" style="width: 200px;"/>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff" width="200">Phone No. :</td>
                                        <td>
                                            <label id="lblPhoneNo"><%=manageAdminDtBean.getPhoneNo()%></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff" width="200">Mobile No. :</td>
                                        <td>
                                            <label id="lblMobileNo"><%=manageAdminDtBean.getMobileNo()%></label>
                                        </td>
                                    </tr>

                                    <table class="tableList_1 t_space" cellspacing="0" width="100%">
                                        <tr>
                                            <th class="t-align-center">Sl. No.</th>
                                            <th class="t-align-center">Full Name</th>
                                            <th class="t-align-center">Status</th>
                                            <th class="t-align-center">Action By</th>
                                            <th class="t-align-center">Action Date</th>
                                            <th class="t-align-center">Reason</th>
                                        </tr>
                                        <%

                                               for (int i = 0; i < list.size(); i++) {
                                                   String strFullName = "-";
                                                   String straction = "-";
                                                   String strComments = "-";

                                                   if (!"".equalsIgnoreCase(list.get(i).getAction())) {
                                                       straction = list.get(i).getAction();
                                                       flag = true;


                                                       strFullName = manageAdminDtBean.getFullName();


                                                       if (!"".equalsIgnoreCase(list.get(i).getComments())) {
                                                           strComments = list.get(i).getComments();
                                                       }
                                                       if (!"".equalsIgnoreCase(list.get(i).getAction())) {
                                                           straction = list.get(i).getAction();
                                                       }

                                        %>
                                        <tr
                                            <%if (i % 2 == 0) {%>
                                            class="bgColor-white"
                                            <%} else {%>
                                            class="bgColor-Green"
                                            <%   }%>
                                            >
                                            <td class="t-align-center"> <%=j%>
                                                <%j++;%>
                                            </td>
                                            <td class="t-align-center"><%=strFullName%></td>
                                            <td class="t-align-center"><%=straction%></td>
                                            <td class="t-align-center"><%=list.get(i).getActionByName()%></td>
                                            <td class="t-align-center"><%=DateUtils.gridDateToStr(list.get(i).getActionDt())%></td>
                                            <td class="t-align-center"><%=strComments%></td>

                                        </tr>
                                        <%}
                                               }
                                               if (!flag) {
                                        %>
                                        <td class="t-align-center" colspan="6" style="color: red;font-weight: bold">No Record Found</td>
                                        <%}%>
                                        <% }%>

                                    </table>
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
    <%
                manageAdminSrBean = null;
                adminMasterDtBean = null;
    %>
</html>
