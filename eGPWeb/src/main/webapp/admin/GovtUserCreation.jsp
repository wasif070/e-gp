<%-- 
    Document   : GovtUserCreation
    Created on : Oct 23, 2010, 5:28:28 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.model.table.TblDesignationMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.GovtUserCreationService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPGovtEmpRolesReturn"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="govSrUser" class="com.cptu.egp.eps.web.servicebean.GovtUserSrBean" />
<jsp:setProperty property="*" name="govSrUser"/>
<%@page import="java.util.List" %>
<%
    Logger LOGGER = Logger.getLogger("GovtUserCreation.jsp");
    String str = request.getContextPath();
    boolean isEdit = false;

    //createGovtUser
    //editGovtUser
    int govDeptId = 0;
    String action = "createGovtUser";
    int userId = 0;
    int empId = 0;
    int govUserId = 0;
    int userTypeId1 = 0;
    String logUserId = "0";
    String fromWhere = "";
    String hasMobile = "";

    if (session.getAttribute("userId") != null) {
        govUserId = Integer.parseInt(session.getAttribute("userId").toString());
        logUserId = session.getAttribute("userId").toString();
    }
    govSrUser.setLogUserId(logUserId);
    if (session.getAttribute("userTypeId") != null) {
        userTypeId1 = Integer.parseInt(session.getAttribute("userTypeId").toString());
    }
    if (userTypeId1 == 4 || userTypeId1 == 5) {
        govDeptId = govSrUser.getUserTypeWiseDepartmentId(govUserId, userTypeId1);
    }
    if (request.getParameter("userId") != null) {
        userId = Integer.parseInt(request.getParameter("userId").toString());
    }
    if (request.getParameter("empId") != null) {
        empId = Integer.parseInt(request.getParameter("empId").toString());
    }

    if (request.getParameter("fromWhere") != null) {
        fromWhere = request.getParameter("fromWhere");
    }

    if (request.getParameter("hasMobile") != null) {
        hasMobile = request.getParameter("hasMobile");
    }

    if ("EditProfile".equalsIgnoreCase(fromWhere)) {
        userId = Integer.parseInt(session.getAttribute("userId").toString());
        try {
            empId = govSrUser.takeEmpIdFromUserId(userId);
        } catch (Exception e) {
            LOGGER.error("error:" + logUserId + " : " + e);
        }
    }

    if ("Edit".equalsIgnoreCase(request.getParameter("Edit"))) {
        isEdit = true;
        action = "editGovtUser";
    }

    govSrUser.setUserId(userId);
    govSrUser.setEmpId(empId);
    Object[] object = null;
    String status = null;
    if (isEdit) {
        status = govSrUser.employeeStatus(empId);

        govSrUser.setAuditTrail(null);
        object = govSrUser.getGovtUserData();

    }
    govSrUser.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                                       
%>

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%if (isEdit) { %>
        <title>Edit Profile</title>
        <%} else { %>
        <title>Create Profile</title>
        <%}%>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js" type="text/javascript"></script>
        <%
            boolean flag = false;
            String msg = "";

            if (request.getParameter("flag") != null) {
                if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
                    flag = true;
                }
            }
            if (request.getParameter("msg") != null) {
                if ("emailPattern".equalsIgnoreCase(request.getParameter("msg"))) {
                    msg = "Please Enter Valid e-mail ID";
                } else if ("email".equalsIgnoreCase(request.getParameter("msg"))) {
                    msg = "e-mail ID already exists, Please enter another e-mail ID";
                }
                if ("nation".equalsIgnoreCase(request.getParameter("msg"))) {
                    msg = "Citizen-ID already exists";
                }
                if ("mobileId".equalsIgnoreCase(request.getParameter("msg"))) {
                    msg = "Mobile No. already exists";
                }
                //if("useremployeeid".equalsIgnoreCase(request.getParameter("msg"))){
                //    msg="Employee ID already exists";
                //}
            }
        %>
    </head>

    <body>

        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <%                    StringBuilder userType = new StringBuilder();
                    if (request.getParameter("userType") != null) {
                        if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                            userType.append(request.getParameter("userType"));
                        } else {
                            userType.append("org");
                        }
                    } else {
                        userType.append("org");
                    }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%if (!"EditProfile".equalsIgnoreCase(fromWhere)) {%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>
                        <%}%>
                        <td class="contentArea_1">
                            <div >
                                <%if (!flag && !"".equalsIgnoreCase(msg)) {%>
                                <div id="errMsg" class="responseMsg errorMsg" style="display:block"><%=msg%></div>
                                <%}%>
                            </div>
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1"><div class="pageHead_1">
                                            <%if (isEdit) {
                                                    out.print("Edit Profile");
                                                } else {
                                                    out.print("Create Government User");
                                                }%></div>
                                            <%-- <div class="pageHead_1">Create User</div>--%>
                                        <div class="stepWiz_1 t_space">
                                            <ul>
                                                <%if (!"EditProfile".equalsIgnoreCase(fromWhere)) {%>
                                                <li class="sMenu">Employee Information</li>
                                                    <%} else {%>
                                                <li class="sMenu">Personal Information</li>
                                                    <%}%>

                                                <%if (!fromWhere.equalsIgnoreCase("EditProfile")) {%>
                                                <li>&gt;&gt;&nbsp;&nbsp;
                                                    <% long contOffices = 0;

                                                        contOffices = govSrUser.countForQuery("TblEmployeeOffices", "employeeId = " + empId);
                                                        //out.print("count office-"+contOffices);

                                                        if (contOffices != 0) {
                                                            if (isEdit) {
                                                            }

                                                    %>
                                                    <a style="text-decoration: underline;" href="GovtUserCreateRoleOff.jsp?empId=<%=empId%>&edit=<%=isEdit%>" > Employee Office Information </a>
                                                    <%
                                                    } else { %>
                                                </li>Employee Office Information  <%  } %>

                                                <!--                                                <li>&gt;&gt;&nbsp;&nbsp;-->
                                                <%
                                                    List<SPGovtEmpRolesReturn> empRolesReturn = null;
                                                    empRolesReturn = govSrUser.getEmpRoleses(empId);
                                                    int deptId = 0;
                                                    String offid;
                                                    boolean check = false;
                                                    if (empRolesReturn != null) {
                                                        if (empRolesReturn.size() > 0) {
                                                            for (SPGovtEmpRolesReturn govtEmpRolesReturn : empRolesReturn) {
                                                                String roleId = govtEmpRolesReturn.getProcurementRoleId();
                                                                String roleName = govtEmpRolesReturn.getProcurementRole();
                                                                String finPowerBy = govtEmpRolesReturn.getFinPowerBy();
                                                                deptId = govtEmpRolesReturn.getDepartmentId();
                                                                String empRoleId = govtEmpRolesReturn.getEmployeeRoleId();
                                                                offid = govtEmpRolesReturn.getOfficeId();
                                                                String d=govtEmpRolesReturn.getDesignationName();

                                                                /*if (!"Authorized User".equalsIgnoreCase(govtEmpRolesReturn.getProcurementRole().trim()) && !"AU".equalsIgnoreCase(govtEmpRolesReturn.getProcurementRole().trim())) {
                                                    check = true;*/
                                                %>
                                                <!--                                                    <a style="text-decoration: underline;" href="GovtUserFinanceRole.jsp?Edit=Edit&userId=&empId=&empRoleId=&roleId=&roleName=&finPowerBy=&deptId="> Assign Financial Power</a>-->
                                                <% /*}*/
                                                            }
                                                        }
                                                    }
                                                    //if(empRolesReturn.isEmpty() || check == false){
                                                %>
                                                <!--                                                    Assign Financial Power-->
                                                <%
                                                    }
                                                %>
<!--                                                    <% // } %> </li>-->
                                            </ul>
                                        </div>
                                        <form method="POST" id="frmGovUser" action="">

                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" >
                                                <tr>
                                                    <td style="font-style: italic" colspan="2"  align="right">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                                </tr>

                                                <tr>
                                                    <td class="ff" width="20%" >e-mail ID :<%if(!isEdit){%><span>*</span><%}%> </td>
                                                    <td width="80%"><%if(isEdit && object != null){
                                                        if (status.equalsIgnoreCase("approved")) {
                                                            out.print((String) object[0]);
                                                        } else {%>
                                                        <input name="mailId" id="txtMail" type="text" class="formTxtBox_1"  style="width:200px;" onblur="return checkMail();" value="<%=(String) object[0]%>"/>
                                                        <input type="hidden" name="hdnMailId" id="hdnMailId" value="<%=(String) object[0]%>" />
                                                        <span id="mailMsg" style="color: red; font-weight: bold"></span>
                                                        <%}
                                                            }

                                                            
                                                            
                                                            else{%>
                                                        <input name="mailId" id="txtMail" type="text" class="formTxtBox_1"  style="width:200px;" onblur="return checkMail();" />
                                                        <span id="mailMsg" style="color: red; font-weight: bold"></span>
                                                        <%}if(!isEdit

                                                            
                                                            
                                                            ){%>
                                                        <span id="phno" style="color: grey;"><br />Please enter Official and Designation specific e-mail ID i.e. ce@abcd.gov.bt</span>
                                                            <%}%>
                                                    </td>
                                                </tr>

                                                <%if(!isEdit

                                                    
                                                    
                                                    ){%>
                                                <tr>
                                                    <td class="ff">Password : <span>*</span></td>
                                                    <td><input name="password" id="txtPass" type="password" class="formTxtBox_1" style="width:200px;" maxlength="25" value="" autocomplete="off" /><br/><span id="tipPassword" style="color: grey;"> ( Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. 
                                                <br/>Special characters may be added) </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Confirm Password : <span>*</span></td>
                                                    <td><input name="confPassword" id="txtConfPass" type="password" class="formTxtBox_1"
                                                               style="width:200px;" onpaste="return passMsg();" autocomplete="off" /></td>
                                                </tr>
                                                <%}%>
                                                <tr>
                                                    <td class="ff">Full Name : <span>*</span></td>
                                                    <td><input name="fullName" id="txtaFullName" type="text" class="formTxtBox_1" 
                                                               style="width:200px;"
                                                               value='<%if(isEdit && object

                                                            
                                                                != null){ out.print((String) object[2]);
                                                            }

                                                            
                                                            
                                                            else{%><%}%>'
                                                               />

                                                    </td>
                                                </tr>
                                                <%--tr id="trBangla">
                                                    <td class="ff">Name in Dzongkha : </td>
                                                    <td><input name="banglaName" id="txtBngName" type="text" class="formTxtBox_1"
                                                               style="width:200px;" maxlength="300"
                                                               value="<%if(isEdit && object

                                                           
                                                                       != null){
                                                                   if (object[3] != null) {
                                                                           //out.print(new String());
                                                                           //out.print(""+ new String(((byte[])object[3]),"UTF-8"));
                                                                           out.print(BanglaNameUtils.getUTFString(((byte[]) object[3])));
                                                               }
                                                           } %>" />
                                                    </td>
                                                </tr--%>
                                                <input type="hidden" name="banglaName" id="txtBngName" type="text" class="formTxtBox_1"
                                                               style="width:200px;" maxlength="300"
                                                               value="<%if(isEdit && object

                                                           
                                                                       != null){
                                                                   if (object[3] != null) {
                                                                           //out.print(new String());
                                                                           //out.print(""+ new String(((byte[])object[3]),"UTF-8"));
                                                                           out.print(BanglaNameUtils.getUTFString(((byte[]) object[3])));
                                                               }
                                                           } %>" />
                                                <tr>
                                                    <td class="ff">Employee ID : <span>*</span></td>
                                                    <!-- Emtaz on 08/May/2016. PE user can't edit CID and Employee ID -->
                                                    <% if(userTypeId1

                                                        
                                                            == 4) {%>
                                                    <td><input name="employeeId" id="txtemployeeId" type="text" class="formTxtBox_1"
                                                               style="width:200px;" maxlength="26"
                                                               value='<%if (isEdit && object[7] != null) {
                                                                  out.print((String) object[7]);
                                                              } else {%><%}%>'  onblur="return checkEmployeeId();" />
                                                        <span id="empMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                    </td>
                                                    <%}

                                                        
                                                            else{%>
                                                    <%if (isEdit) {%>
                                                    <td>
                                                        <% if (object[7] != null) {%>
                                                        <% out.print((String) object[7]);
                                                             } else {
                                                             }%> 
                                                    </td>   
                                                <input name="employeeId" id="txtemployeeId" type="hidden" class="formTxtBox_1"
                                                       style="width:200px;" maxlength="26"
                                                       value='<%if (isEdit && object[7] != null) {
                                                                  out.print((String) object[7]);
                                                              } else {%><%}%>'  onblur="return checkEmployeeId();" />
                                                <span id="empMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                <%} else{%>
                                                <td><input name="employeeId" id="txtemployeeId" type="text" class="formTxtBox_1"
                                                           style="width:200px;" maxlength="26"
                                                           value='<%if (isEdit && object[7] != null) {
                                                                  out.print((String) object[7]);
                                                              } else {%><%}%>'  onblur="return checkEmployeeId();" />
                                                    <span id="empMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                </td>
                                                <%}%>
                                                <!--
                                                <td><input name="employeeId" id="txtemployeeId" type="text" class="formTxtBox_1"
                                                      style="width:200px;" maxlength="26"
                                                      value='<%if (isEdit && object[7] != null) {
                                                                  out.print((String) object[7]);
                                                              } else {%><%}%>'  onblur="return checkEmployeeId();" />
                                                    <span id="empMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                </td>
                                                -->
                                                <%}%>
                                                </tr>
                                                <tr>
                                                    <td class="ff">CID No. : </td> <!--ntnlId-->
                                                    <!-- Emtaz on 08/May/2016. PE user can't edit CID and Employee ID -->
                                                    <% if(userTypeId1

                                                        
                                                            == 4) {%>
                                                    <td><input name="citizenId" id="txtcitizenId" type="text" class="formTxtBox_1"
                                                               style="width:200px;" maxlength="26"
                                                               value='<%if (isEdit && object[5] != null) {
                                                                  out.print((String) object[5]);
                                                              } else {%><%}%>'  onblur="return checkNationalId();" />
                                                        <span id="citizenMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                    </td>
                                                    <%}

                                                        
                                                            else{%>
                                                    <%if (isEdit) {%>
                                                    <td>
                                                        <% if (object[5] != null) {%>
                                                        <% out.print((String) object[5]);
                                                             } else {
                                                             }%> 
                                                    </td> 
                                                <input name="citizenId" id="txtcitizenId" type="hidden" class="formTxtBox_1"
                                                       style="width:200px;" maxlength="26"
                                                       value='<%if (isEdit && object[5] != null) {
                                                                  out.print((String) object[5]);
                                                              } else {%><%}%>'  onblur="return checkNationalId();" />
                                                <span id="citizenMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                <% } else { %>
                                                <!--
                                                <td><input name="citizenId" id="txtcitizenId" type="text" class="formTxtBox_1"
                                                      style="width:200px;" maxlength="26"
                                                      value='<%if (isEdit && object != null) {
                                                                  out.print((String) object[5]);
                                                              } else {%><%}%>'  onblur="return checkNationalId();" />
                                                    <span id="citizenMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                </td>
                                                -->

                                                <td><input name="citizenId" id="txtcitizenId" type="text" class="formTxtBox_1"
                                                           style="width:200px;" maxlength="26"
                                                           value='<%if (isEdit && object[5] != null) {
                                                                  out.print((String) object[5]);
                                                              } else {%><%}%>'  onblur="return checkNationalId();" />
                                                    <span id="citizenMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                </td>
                                                <% } %>
                                                <%}%>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Mobile No : <span>*</span></td>
                                                    <td><input name="mobileNo" maxlength="16" id="txtMob" type="text" class="formTxtBox_1"
                                                               style="width:200px;"
                                                               value='<%if(isEdit && object

                                                             
                                                                 != null){ out.print((String) object[4]);
                                                             }

                                                             
                                                             
                                                             else{%><%}%>'
                                                               /><span id="mNo" style="color: grey;"> (Mobile No. format should be e.g 12345678)</span>
                                                        <span id="mobMsg" style="color: red; font-weight: bold">&nbsp;</span></td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Contact Address : </td>
                                                    <td><input name="contactAddress" id="txtcontactAddress" type="text" class="formTxtBox_1"
                                                               style="width:200px; height:15px"
                                                               value='<%if(isEdit && object

                                                           
                                                                       != null){
                                                                   if (object[8] != null) {
                                                                           //out.print(new String());
                                                                           try {
                                                                               // out.print(""+ new String(((byte[])object[8]),"UTF-8"));
                                                                               out.print((String) object[8]);
                                                                           } catch (Exception e) {
                                                                               System.out.println(" Error " + e.getMessage());
                                                                           }
                                                               }
                                                           }

                                                           
                                                           
                                                           else{%><%}%>' />
                                                        </td>
                                                </tr>
                                                
                                               <% if(userTypeId1== 4) {%>
                                                <tr>
                                                    <td class="ff">Designation : <span>*</span></td>
                                                                         
                                                    <td> 
                                                        <select name="designation"  id="cmbDesig" class="formTxtBox_1" style="width:215px;"  >
                                                            <option value="0">-----Select Designation-----</option>
                                                        <% 
                                                            int dptId = govSrUser.getDptId(govUserId);
                                                            int oOffice = govSrUser.getOnlyOffice(govUserId);
                                                            GovtUserCreationService govtUserCreationService = (GovtUserCreationService) AppContext.getSpringBean("GovtUserCreationService");
                                                            short x= (short) dptId;
                                                            String mm="";
                                                            String str1="";
                                                            if(isEdit && object[6] != null)
                                                            {
                                                                 mm = object[6].toString();
                                                                 str1 = govSrUser.takeDesignationName(Integer.parseInt(mm));
                                                            }
                                                             List<TblDesignationMaster> list = govtUserCreationService.designationMasterList(x);
                                                            for (TblDesignationMaster Value : list) {%>
                                                                <option value="<% out.print(Value.getDesignationId()); %>" <%if(isEdit && str1.contains(Value.getDesignationName())){out.print("selected");}%>><% out.print(Value.getDesignationName()); %></option>
                                                                <%}%>
                                                    </select>
                                                    <input type="hidden" value="<%=oOffice%>" name="office">
                                                    </td>
                                                </tr>
                                                <%}%>
                                                
                                                
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="t-align-left">
                                                        <%if(!isEdit

                                                            
                                                            

                                                            
                                                                ){%>
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnSubmit" id="btnSubmit" value="Submit" onclick="return checkData();" />
                                                        </label>                                                       
                                                        <%}else{%>
                                                        &nbsp;
                                                        <label class="formBtn_1">
                                                            <%--<form action="GovUserCreateRoleOff.html" method="POST">--%>
                                                            <input type="submit" name="btnUpdate" id="btnUpdate" value="Update" onclick="return checkData();"/>
                                                            <input type="hidden" name="userId" id="userId" value="<%=govSrUser.getUserId()%>"/>
                                                            <input type="hidden" name="empId" id="empId" value="<%=govSrUser.getEmpId()%>"/>
                                                            <%if (object != null) {%>
                                                            <input type="hidden" name="nId" id="nId" value="<%out.print((String) object[5]);%>"/>
                                                            <input type="hidden" name="mobId" id="mobId" value="<%out.print((String) object[4]);%>"/>
                                                            <%}%>
                                                            <input type="hidden" name="fromWhere" id="fromWhere" value="<%=fromWhere%>"/>
                                                            <%--</form>--%>
                                                        </label>
                                                        &nbsp;
                                                        <%if (!"EditProfile".equalsIgnoreCase(fromWhere)) {%>
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnNext" id="btnNext" value="Next" onclick="frmNext();"  />
                                                        </label>
                                                        <%}%>

                                                        <%}%>
                                                    </td>
                                                </tr>                                                
                                            </table>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>

        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
    <form id="frmNext" method="post">
        <input type="hidden" name="empId" id="empId" value="<%=govSrUser.getEmpId()%>"/>
    </form>
</html>
<script>
    var obj = document.getElementById('lblGovUsersCreation');
    if (obj != null) {
        if (obj.innerHTML == 'Create Government Users') {
            obj.setAttribute('class', 'selected');
        }
    }

</script>
<script type="text/javascript">

    function frmNext() {
        document.getElementById("frmNext").action = "GovtUserCreateRoleOff.jsp?empId=<%=empId%>&edit=<%=isEdit%>";
        document.getElementById("frmNext").submit();
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        
        
        
       

        var hasMobileNo = '<%=hasMobile%>';
        if (hasMobileNo == 'No')
            alert("Please insert your mobile no");
        
        $.validator.addMethod("aFunction",
    function(value, element) {
        if (value == "0")
            return false;
        else
            return true;
    },
    "Please select a value");
        
        $("#frmGovUser").validate({
            rules: {
                designation: {aFunction: true},
                mailId: {required: true, email: true},
                password: {spacevalidate: true, requiredWithoutSpace: true, minlength: 8, maxlength: 25, alphaForPassword: true},
                confPassword: {required: true, equalTo: "#txtPass"},
                fullName: {requiredWithoutSpace: true, alphaName: true, maxlength: 100},
                citizenId: {number: true, maxlength: 11, minlength: 11},
                employeeId: {number: true, required: true},
                phoneNo: {required: true, minlength: 6, maxlength: 20, number: true},
                mobileNo: {required: true, number: true, minlength: 8, maxlength: 8},
                faxNo: {numberWithHyphen: true, PhoneFax: true},
                otherName: {maxlength: 100},
                contactAddress: {maxlength: 1000}

            },
            messages: {
                designation: {aFunction: "<div class='reqF_1'>Please select designation</div>",
                },
                mailId: {required: "<div class='reqF_1'>Please enter e-mail ID</div>",
                    email: "<div id='e1' class='reqF_1'>Please Enter Valid e-mail ID</div>"
                },
                userEmployeeId: {
                    required: "<div class='reqF_1'>Please enter Employee ID</div>"
                },
                password: {spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                    requiredWithoutSpace: "<div class='reqF_1'>Please enter Password</div>",
                    alphaForPassword: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>",
                    minlength: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>",
                    maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed</div>"
                },
                confPassword: {required: "<div class='reqF_1'>Please retype Password</div>",
                    equalTo: "<div class='reqF_1'>Password does not match. Please try again</div>"
                },
                fullName: {//spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                    requiredWithoutSpace: "<div class='reqF_1'>Please enter Full Name</div>",
                    alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only </div>",
                    maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"},
//                        ntnlId: { required: "<div class='reqF_1'>Please enter National ID</div>",
//                            number:"<div class='reqF_1'>Please enter Numbers Only</div>" ,
//                            maxlength: "<div class='reqF_1'>Maximum 25 digits are allowed</div>",
//                            minlength: "<div class='reqF_1'>Please enter minimum 13 digits</div>"
//
//                        },
                citizenId: {
                    number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                    maxlength: "<div class='reqF_1'>Citizen ID should comprise of exactly 11 digits</div>",
                    minlength: "<div class='reqF_1'>Citizen ID should comprise of exactly 11 digits</div>"
                },
                employeeId: {
                    required: "<div class='reqF_1'>Please enter Employee ID</div>",
                    number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                    maxlength: "<div class='reqF_1'>Employee ID should comprise of maximum 25 digits</div>",
                    minlength: "<div class='reqF_1'>Employee ID should comprise of minimum 13 digits</div>"
                },
                phoneNo: {required: "<div class='reqF_1'>Please enter Phone No.</div>",
                    number: "<div class='reqF_1'>Please enter Numbers Only</div>",
                    maxlength: "<div class='reqF_1'>Maximum 20 digits are allowed</div>",
                    minlength: "<div class='reqF_1'>Please enter Minimum 6 Digits</div>"
                },
//                        mobileNo: { required: "<div class='reqF_1'>Please enter Mobile No.</div>",
//                            number: "<div class='reqF_1'>Please enter Numbers Only</div>",
//                             minlength:"<div class='reqF_1'>Minimum 10 digits required.</div>",
//                            maxlength:"<div class='reqF_1'>Maximum 15 digits are allowed.</div>"
//                        },
                mobileNo: {
                    required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                    number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                    minlength: "<div class='reqF_1'>Minimum 8 digits are required</div>",
                    maxlength: "<div class='reqF_1'>Allows maximum 8 digits only</div>"
                },
                faxNo: {numberWithHyphen: "<div class='reqF_1'>Allows numbers (0-9) and hyphen (-) only</div>",
                    PhoneFax: "<div class='reqF_1'>Please enter valid Phone No. Area code should contain min 2 digits and max 5 digits. Phone no. should contain min 3 digits and max 10 digits</div>"},
                otherName: {
                    maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"
                }

            },
            errorPlacement: function (error, element)
            {
                if (element.attr("name") == "password")
                {
                    error.insertAfter("#tipPassword");
                } else if (element.attr("name") == "mobileNo")
                {
                    error.insertAfter("#mobMsg");
                } else
                {
                    error.insertAfter(element);

                }
            }
        }
        );
    });
</script>

<script type="text/javascript">

    function numeric(value) {
        return /^\d+$/.test(value);
    }

    function checkMail() {
        var oldMail = "";
        if (document.getElementById("hdnMailId") != null) {
            oldMail = document.getElementById("hdnMailId").value;
            if (oldMail == document.getElementById("txtMail").value) {
                return true;
            }
        }
        var mailflag = false;
        // alert('::'+document.getElementById("txtMail").value+'::');

        if ($.trim(document.getElementById("txtMail").value) != "") {
            $('span.#mailMsg').css("color", "red");
            $('span.#mailMsg').html("Checking for unique Mail Id...");
            $.post("<%=request.getContextPath()%>/CommonServlet", {mailId: $.trim($('#txtMail').val()), funName: 'verifyMail'}, function (j) {
                if (j == "OK") {
                    $('span.#mailMsg').css("color", "green");
                    mailflag = true;
                } else {
                    $('span.#mailMsg').css("color", "red");
                }
                $('span.#mailMsg').html(j);
                if (document.getElementById("e1") != null) {
                    document.getElementById("e1").innerHTML = "";
                }
                return mailflag;
            });
        } else {
            if (document.getElementById("e1") != null) {
                document.getElementById("e1").innerHTML = "";
            }
            document.getElementById('mailMsg').innerHTML = "";
        }
    }


    function checkNationalId() {
        var flagNat = false;
        var isEdit = '<%=isEdit%>';
        if ($.trim(document.getElementById("txtcitizenId").value) != "") {
            if (isEdit) {
                if ($.trim($('#txtcitizenId').val()) != $('#nId').val()) {
                    $('span.#natMsg').css("color", "red");
                    //   $('span.#natMsg').html("Checking for unique National Id...");
                    $.post("<%=request.getContextPath()%>/CommonServlet", {nationalId: $.trim($('#txtcitizenId').val()), funName: 'verifyNationalId'}, function (j) {
                        if (j == "OK") {
                            $('span.#natMsg').css("color", "white");
                            flagNat = true;
                        } else {
                            $('span.#natMsg').css("color", "red");
                            flagNat = false;
                        }
                        $('span.#natMsg').html(j);
                        return flagNat;
                    });
                } else {
                    document.getElementById("natMsg").innerHTML = "";
                }
            } else {
                $('span.#natMsg').css("color", "red");
                // $('span.#natMsg').html("Checking for unique National Id...");
                $.post("<%=request.getContextPath()%>/CommonServlet", {nationalId: $.trim($('#txtcitizenId').val()), funName: 'verifyNationalId'}, function (j) {
                    if (j == "OK") {
                        $('span.#natMsg').css("color", "white");
                        flagNat = true;
                    } else {
                        $('span.#natMsg').css("color", "red");
                        flagNat = false;
                    }
                    $('span.#natMsg').html(j);
                    return flagNat;
                });
            }
        } else {
            document.getElementById("txtcitizenId").value = "";
            document.getElementById("natMsg").innerHTML = "";
        }
    }




    function checkEmployeeId() {
        var flagNat = false;
        var isEdit = '<%=isEdit%>';
        if ($.trim(document.getElementById("txtemployeeId").value) != "") {
            if (isEdit) {
                if ($.trim($('#txtemployeeId').val()) != $('#nId').val()) {
                    $('span.#empMsg').css("color", "red");
                    //   $('span.#natMsg').html("Checking for unique National Id...");
                    $.post("<%=request.getContextPath()%>/CommonServlet", {userEmployeeId: $.trim($('#txtemployeeId').val()), funName: 'verifyUserEmployeeId'}, function (j) {
                        if (j == "OK") {
                            $('span.#empMsg').css("color", "white");
                            flagNat = true;
                        } else {
                            $('span.#empMsg').css("color", "red");
                            flagNat = false;
                        }
                        $('span.#empMsg').html(j);
                        return flagNat;
                    });
                } else {
                    document.getElementById("empMsg").innerHTML = "";
                }
            } else {
                $('span.#empMsg').css("color", "red");
                // $('span.#natMsg').html("Checking for unique National Id...");
                $.post("<%=request.getContextPath()%>/CommonServlet", {userEmployeeId: $.trim($('#txtemployeeId').val()), funName: 'verifyUserEmployeeId'}, function (j) {
                    if (j == "OK") {
                        $('span.#empMsg').css("color", "white");
                        flagNat = true;
                    } else {
                        $('span.#empMsg').css("color", "red");
                        flagNat = false;
                    }
                    $('span.#empMsg').html(j);
                    return flagNat;
                });
            }
        } else {
            document.getElementById("txtemployeeId").value = "";
            document.getElementById("empMsg").innerHTML = "";
        }
    }

    function checkMobileNo() {
        var flagMob = false;
        var isEdit = '<%=isEdit%>';
        if ($.trim(document.getElementById("txtMob").value) != "" && $.trim(document.getElementById("txtMob").value.length) > 9 && $.trim(document.getElementById("txtMob").value.length) < 16 && numeric($.trim($('#txtMob').val()))) {
            //$('#txtMob').val($.trim($('#txtMob').val()));
            if (isEdit) {
                if ($.trim($('#txtMob').val()) != $('#mobId').val()) {
                    $('span.#mobMsg').css("color", "red");
                    $('span.#mobMsg').html("Checking for unique Mobile No...");
                    $.post("<%=request.getContextPath()%>/CommonServlet", {mobileNo: $.trim($('#txtMob').val()), funName: 'verifyMobileNo'}, function (j) {
                        if (j == "OK") {
                            $('span.#mobMsg').css("color", "green");
                            flagMob = true;
                        } else {
                            $('span.#mobMsg').css("color", "red");
                            flagMob = false;
                        }
                        $('span.#mobMsg').html(j);
                        if (document.getElementById("m1") != null) {
                            document.getElementById("m1").innerHTML = "";
                        }
                        return flagMob;
                    });
                } else {
                    if (document.getElementById("m1") != null) {
                        document.getElementById("m1").innerHTML = "";
                    }
                    document.getElementById("mobMsg").innerHTML = "";
                }
            } else {
                $('span.#mobMsg').css("color", "red");
                $('span.#mobMsg').html("Checking for unique Mobile No...");
                $.post("<%=request.getContextPath()%>/CommonServlet", {mobileNo: $.trim($('#txtMob').val()), funName: 'verifyMobileNo'}, function (j) {
                    if (j == "OK") {
                        $('span.#mobMsg').css("color", "green");
                        flagMob = true;
                    } else {
                        $('span.#mobMsg').css("color", "red");
                        flagMob = false;
                    }
                    $('span.#mobMsg').html(j);
                    if (document.getElementById("m1") != null) {
                        document.getElementById("m1").innerHTML = "";
                    }
                    return flagMob;
                });
            }
        } else {
            //$('#txtMob').val($.trim($('#txtMob').val()));
            document.getElementById("mobMsg").innerHTML = "";
        }
    }

    function checkData() {
        //var isMail=false;
        var retval;
        var isForEdit = '<%=isEdit%>';
        if (isForEdit == "false") {
            if (document.getElementById("txtMail").value != "" && document.getElementById("mailMsg").innerHTML != "OK") {
                return false;
            }
        }
        /*if(isForEdit == "true"){
         if($('#txtNtnlId').val() !=  $('#nId').val()){
         if(document.getElementById("txtNtnlId").value !="" && document.getElementById("natMsg").innerHTML != "OK"){
         //return false;
         }
         }
         } */
        /* else if(document.getElementById("txtNtnlId").value !="" && document.getElementById("natMsg").innerHTML != "OK"){
         alert('here');
         return false;
         }*/
        /*if(isForEdit == "true"){
         if($('#txtMob').val() !=  $('#mobId').val()){
         if(document.getElementById("txtMob").value !="" && document.getElementById("mobMsg").innerHTML != "OK"){
         return false;
         }
         }
         }
         else if(document.getElementById("txtMob").value !="" && document.getElementById("mobMsg").innerHTML != "OK"){
         return false;
         }*/
        if ($('#frmGovUser').valid()) {
            //$('#btnSubmit').attr("disabled", true);
            $(".err").remove();
            if ($('#txtaFullName').val().length != 0 && $.trim($('#txtaFullName').val()).length == 0) {
                $("#txtaFullName").parent().append("<div class='err' style='color:red;'>Only space is not allowed</div>");
            } else {
                document.getElementById("txtaFullName").value = trim(document.getElementById("txtaFullName").value);
                //$('#txtaFullName').val($('#txtaFullName').val().trim());
                document.getElementById("frmGovUser").action = "<%=str%>/GovtUserSrBean?action=<%=action%>";
    <%if(!isEdit

                            
                            
                    ){%>
                                document.getElementById("frmGovUser").submit();
    <%}%>
                            }
                            //return true;
                        }

                    }


</script>
<script type="text/javascript">
    function passMsg() {
        jAlert("Copy and Paste not allowed.", "Govt User Creation", function (RetVal) {
        });
        return false;
    }

    $('#txtaFullName').blur(function () {
        $(".err").remove();
        if ($('#txtaFullName').val().length != 0 && $.trim($('#txtaFullName').val()).length == 0) {
            $("#txtaFullName").parent().append("<div class='err' style='color:red;'>Only space is not allowed</div>");
        }
    });

</script>
<%
    govSrUser  = null;
%>
