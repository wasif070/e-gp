<%--
Document   : ChangePassword
Created on : Oct 23, 2010, 6:13:00 PM
Author     : Sanjay
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.web.servicebean.LoginMasterSrBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>--%>
<%
String  str= request.getContextPath();

%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        boolean isAfterLoginTop=false;

    if(request.getParameter("fromWhere") != null){
        if("MyAccount".equalsIgnoreCase(request.getParameter("fromWhere"))){
            isAfterLoginTop=true;
            //setAuditTrail(new AuditTrail(request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                    }
                    }
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Change Password</title>
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
<script type="text/javascript">
    var $j = jQuery.noConflict();
      $j(function() {
        if(document.getElementById("hintMsg")!=null){
            document.getElementById("hintMsg").innerHTML ="";
        }
//          $j('#cmbHintQuestion').bind("change",function(){
//              if($j('#cmbHintQuestion').val() == 'other' ){
//                  $j('#hintQueTr').show();
//              }
//              else{
//                  $j('#hintQueTr').hide();
//              }
//          });
      });

       $j(function() {
           if(document.getElementById("hintMsg")!=null){
         document.getElementById("hintMsg").innerHTML ="";
         }
        $j('#txtCurPass').bind("blur",function(){
       if(document.getElementById("txtCurPass").value != ""){
            $j.post("<%=request.getContextPath()%>/LoginSrBean", {txtCurPass:$j('#txtCurPass').val(),action:'checkPassword'}, function(j){
                if(j.toString()== "Valid password"){
                    $j('span.#passMsg').css("color","green");
                }
                else{
                    $j('span.#passMsg').css("color","red");
                }
                $j('span.#passMsg').html(j);
            });
        }else{
            $j('span.#passMsg').html(' ');
        }
       });
    });

    function checkCurrAndNewPass(){
               if(document.getElementById("hintMsg")!=null){
         document.getElementById("hintMsg").innerHTML ="";
         }
        if(document.getElementById("txtNewPass").value != ""){
        if(document.getElementById("txtCurPass").value == document.getElementById("txtNewPass").value){
            document.getElementById("snewPass").innerHTML = "<div class='reqF_1'>New Password and Current Password should not be the same</div>";
            return false;
        }else{
            document.getElementById("snewPass").innerHTML = '';
            return true;
        }
        }else{
            return true;
        }
    }
    String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
    }
    function checkPass(){
        var flag=true;
        if(!checkCurrAndNewPass()){
            flag = false;
        }if(document.getElementById("hintMsg")!=null){
            if(document.getElementById("cmbHintQuestion").value=="other"){
                if(document.getElementById("txtOtherHint").value.trim()==""){
                    document.getElementById("hintMsg").innerHTML = "Please enter Hint Question";
                        flag =  false;
                }
            }
        }else{
                if(document.getElementById("hintMsg")!=null){
                 document.getElementById("hintMsg").innerHTML ="";
            }
        }
        if(document.getElementById("txtCurPass").value != ""){
            if(document.getElementById("passMsg").innerHTML != "Valid password"){
                    flag=false;
            }
        }
        /*if(flag && $j('#frmChangePass').valid()){
            flag = checkTenderSubLeft();
        }*/
        return flag;
    }

    function checkTenderSubLeft(){
        var tbol= true;
         $j.ajax({
                type: "POST",
                url: "<%=request.getContextPath()%>/LoginSrBean",
                data: "action=ChkChngPasswordBidder4Tender",
                async: false,
                success: function(j){
                    if(j.toString()=="0"){
                        tbol=false;
                          alert("One of your Tender Submission is pending so either submit the Tender or delete the Bids of it.");
                    }
                }
        });
        return tbol;
    }
  </script>
<script type="text/javascript">
     $j(document).ready(function() {
         if(document.getElementById("hintMsg")!=null){
         document.getElementById("hintMsg").innerHTML ="";
         }
         $j('#btnAdd').attr("disabled", false);
        $j("#frmChangePass").validate({
            rules: {
                CurPass: { required: true},
                NewPass:{required:true,minlength:8,maxlength:25,alphaForPassword:true},
                ConfPassword:{required:true,equalTo:"#txtNewPass"},
                HintQuestion:{required:true},
                //OtherHint:{maxlength:100},
                HintAns:{required:true,maxlength:50}
            },
            messages: {
                CurPass: { required: "<div class='reqF_1'>Please enter Current Password</div>"},
                NewPass:{required:"<div class='reqF_1'>Please enter New Password.</div>",
                         minlength:"<div class='reqF_1'>Password requires minimum 8 characters and must contain both alphabets and numbers.</div>",
                         maxlength:"<div class='reqF_1'>Maximum 25 characters are allowed.</div>",
                         alphaForPassword: "<div class='reqF_1'>Please enter alphanumeric only.</div>"},

               ConfPassword:{required: "<div class='reqF_1'>Please retype the Password</div>",
                            equalTo: "<div id='msgPwdNotMatch' class='reqF_1'>Password does not match. Please try again.</div>"
                            },
               HintQuestion:{required:"<div class='reqF_1'>Please select Hint Question.</div>"
                            },
              // OtherHint:{maxlength:"<div>Maximum 100 Characters are to be allowed.</div>"
               //         },

               HintAns:{required:"<div class='reqF_1'>Please enter Hint Answer.</div>",
                        maxlength:"<div class='reqF_1'>Maximum 50 characters are allowed.</div>"}

            },
            errorPlacement:function(error ,element)
                {
                    if(element.attr("name")=="NewPass")
                    {
                        error.insertAfter("#tipPassword");
                    }
                    else
                    {
                        error.insertAfter(element);

                    }
                }
        }
    );
    });
//    function chkPasswordMatches(){
//        document.getElementById("hintMsg").innerHTML ="";
//                var objPwd = document.getElementById("txtNewPass");
//                var objConfirmPwd = document.getElementById("txtConfPassword");
//                if(objPwd != null && objConfirmPwd != null){
//                    if($j.trim(objPwd.value) == "" || $j.trim(objConfirmPwd.value) == ""){}else{
//                        var msgPwdMatchObjDiv = document.getElementById("pwdMatchMsg");
//                        if(objPwd.value == objConfirmPwd.value){
//                            var msgPwdNotMatch = document.getElementById("msgPwdNotMatch");
//                            if(msgPwdNotMatch!=null){
//                                if(msgPwdNotMatch.innerHTML == "Password does not match. Please try again."){
//                                    msgPwdNotMatch.style.diplay = "none";
//                                    msgPwdNotMatch.innerHTML = "";
//                                }
//                            }
//                            msgPwdMatchObjDiv.innerHTML = "<b>Password Matches</b>";
//                        }else{
//                            msgPwdMatchObjDiv.innerHTML = "";
//                        }
//                    }
//                }
//            }
   
   
</script>
<script type="text/javascript">


    function passAlert(){
        if(document.getElementById("hintMsg")!=null){
         document.getElementById("hintMsg").innerHTML ="";
         }
        jAlert("Copy and Paste not allowed.","Change Password", function(RetVal) {
        });
        return false;
    }

</script>
  </head>
<%
    String userId="0";
    if(session.getAttribute("userId")!=null){
        userId=session.getAttribute("userId").toString();
        }
    LoginMasterSrBean loginMasterSrBean = new LoginMasterSrBean(userId);
    userId=null;
    

%>

    <body>
        <div class="mainDiv">
            <div class="fixDiv">
               <%if(isAfterLoginTop){%>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
               <%}else{%>
               <jsp:include page="../resources/common/Top.jsp" />
               <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
               <%}%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    
                    <tr valign="top">

                        <td class="contentArea-Blogin">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">         
                                    <td><div class="pageHead_1">Change Password</div>
                                        <form id="frmChangePass" name="ChangePassword" method="post" action='<%=str%>/LoginSrBean?action=changePassword'>
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>&nbsp;<td style="font-style: italic; font-weight: normal;" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td></tr>
                                                <tr>
                                                    <td width="15%" class="ff">Current Password : <span>*</span></td>
                                                    <td width="85%">
                                                        <input class="formTxtBox_1" type="password" id="txtCurPass" name="CurPass" autocomplete="off" />
                                                        <span id="passMsg" style="color: red; font-weight: bold"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">New Password : <span>*</span></td>
                                                    <td>
                                                        <input class="formTxtBox_1" type="password" id="txtNewPass" name="NewPass" onblur="checkCurrAndNewPass();" autocomplete="off" /><br/><span id="tipPassword" style="color: grey;"> Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. 
                                                <br/>Special characters may be added. </span>
                                                         <span id="snewPass"></span>
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td class="ff">Confirm Password : <span>*</span></td>
                                                    <td>
                                                        <input class="formTxtBox_1" type="password" id="txtConfPassword" name="ConfPassword" onpaste="return passAlert();" autocomplete="off" />
                                                        <div style="color: green" id="pwdMatchMsg"></div>
                                                    </td>
                                                </tr>
                                                <%if(!isAfterLoginTop){%>
                                                <tr>
                                                    <td class="ff">Hint Question : <span>*</span></td>
                                                    <td>
                                                        <select class="formTxtBox_1" name="HintQuestion" id="cmbHintQuestion">
                                                            <%for(SelectItem hintQus : loginMasterSrBean.getHintQueList()){%>
                                                                <option value="<%=hintQus.getObjectId()%>"><%=hintQus.getObjectValue()%></option>
                                                            <%}%>
<!--                                                                 <option value="other">Create Your own Question</option>-->
                                                        </select>
                                                    </td>
                                                </tr>
                                                
                                                <tr id="hintQueTr" style="display:none;">
                                                    <td class="ff">Create your own Hint Question : <span>*</span></td>
                                                    <td>
                                                        <textarea cols="20" rows="3" id="txtOtherHint" name="OtherHint"></textarea>
                                                        <span id="hintMsg" class="reqF_1"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Hint Answer : <span>*</span></td>
                                                    <td>
                                                        <input class="formTxtBox_1" type="text" id="txtHintAns" name="HintAns" />
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td align="left">
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="Add" id="btnAdd" value="Submit" onclick="return checkPass();" disabled/>
                                                        </label>
                                                        <input type="hidden" name="myAChangePass"  id="myAChangePass" value="<%=isAfterLoginTop%>" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMyAcc");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>

