<%--
    Document   : HelpListing
    Created on : Dec 18, 2010, 5:25:01 PM
    Author     : Rikin
--%>

<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.AddUpdateOpeningEvaluation"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblHelpManual"%>
<%@page import="com.cptu.egp.eps.web.servicebean.HelpManualSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add BSR</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        
        <script type="text/javascript">

            function validatateElement(elementId, msgElementId, errorMsg)
            {
                var a = document.getElementById(elementId).value;
                if(a === '')
                {
                    document.getElementById(msgElementId).innerHTML = errorMsg;
                    return false;
                }
                else
                {
                    document.getElementById(msgElementId).innerHTML = '';
                    return true;
                }
            }
            /* validate Add , Edit Help Content */
            function checkData(){
                var isValid = true;
                isValid &= validatateElement('cmbCategory','cmbCategoryMsg','<br/>Please select a category');
                isValid &= validatateElement('txtDescription','txtDescriptionMsg','<br/>Please enter description');
                isValid &= validatateElement('txtUnit','txtUnitMsg','<br/>Please enter unit');
                                
                return isValid?true:false;
            }
        </script>
    </head>
    <body>
        <%
               String userID="0";
               if(session.getAttribute("userId")!=null){
               userID=session.getAttribute("userId").toString();
               }
               
               AddUpdateOpeningEvaluation addUpdate = (AddUpdateOpeningEvaluation) AppContext.getSpringBean("AddUpdateOpeningEvaluation");                              
        %>
        <%
//p_add_upd_openingEvaluation
//            if(msg.equals("Values Added")){
//                response.sendRedirect("HelpListing.jsp");
//            }
            
            if ( "POST".equalsIgnoreCase(request.getMethod())) 
            {
                String category = request.getParameter("cmbCategory");
                String subCategory = request.getParameter("txtSubCategory");
                String code = request.getParameter("txtCode");
                String description = request.getParameter("txtDescription");
                String unit = request.getParameter("txtUnit");
                
                CommonMsgChk commonMsgChk = addUpdate.addUpdOpeningEvaluation("AddBOQ",category,subCategory,code,description, unit,HandleSpecialChar.handleSpecialChar(request.getParameter("txtComments"))).get(0); 
                
                if (commonMsgChk.getFlag()) 
                {
                    response.sendRedirect("BOQList.jsp?msgId=added");
                }
            }

            
            
            /* Edit and View Help Content */
//            if("Edit".equals(request.getParameter("action")) || "View".equals(request.getParameter("action"))){
//                int id = Integer.parseInt(request.getParameter("id").toString());
//                Iterator t = helpManualSrBean.getData(id).iterator();
//
//                while(t.hasNext()){
//                   tblHelpManual = (TblHelpManual)t.next();
//                   helpManualId = tblHelpManual.getHelpManualId();
//                   helpUrl = tblHelpManual.getHelpUrl();
//                   helpContent = tblHelpManual.getHelpContent();
//                }
//            }
        %>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>               
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                <tr valign="top">
                    <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <%--<jsp:param name="userType" value="<%=userType.toString()%>"/>
                    </jsp:include>--%>
                        <td class="contentArea" style="vertical-align: top;" align="center" valign="middle">
                            <div class="pageHead_1 t-align-left">Add BSR
                                <span class="c-alignment-right">
<!--                                    <a href="HelpListing.jsp" class="action-button-goback">Go Back to Dashboard</a>-->
                                </span>
                            </div>
                        <form action="AddBOQ.jsp" method="post">
                        
                <table width="100%" cellspacing="10" class="formStyle_1 t_space">
                    <% if(!"View".equals(request.getParameter("action"))){ %>
                    <tr>
                        <td style="font-style: italic;" colspan="2" class="t-align-right"><normal>Fields marked with (*) are Mandatory</normal></td>
                    </tr>
                    <% } %>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <!-- Page Name field -->
                    <tr>
                    <td width="15%" class="t-align-left ff">Category: <% if(!"View".equals(request.getParameter("action"))){ %><span class="mandatory">*</span><%}%> </td>
                    <td width="85%" class="t-align-left">
                        <select style="width:200px;" id="cmbCategory" class="formTxtBox_1" name="cmbCategory">
                                <option value="">- Select Category -</option>
                                <option value="General">General</option>
                                <option value="Civil">Civil</option>
                                <option value="Electrical">Electrical</option>
                            </select>                        
                        <span id="cmbCategoryMsg" class="reqF_2" >&nbsp;</span>
                        <span id="cmbCategoryMsg1"></span>
                    </td>
                </tr>
                    <tr>
                    <td width="15%" class="t-align-left ff">Sub Category:</td>
                    <td width="85%" class="t-align-left">
                        <input name="txtSubCategory" type="text" class="formTxtBox_1" id="txtSubCategory" style="width:200px;" />                  
                        <span id="subCategoryMsg" class="reqF_2" >&nbsp;</span>
                        <span id="idhelpuniqMsg11"></span>
                    </td>
                </tr>
                    <tr>
                    <td width="15%" class="t-align-left ff">Code:</td>
                    <td width="85%" class="t-align-left">
                        <input name="txtCode" type="text" class="formTxtBox_1" id="txtCode" style="width:200px;" />                  
                        <span id="idhelpMsg" class="reqF_2" >&nbsp;</span>
                        <span id="idhelpuniqMsg"></span>
                    </td>
                </tr>
                    <tr>
                    <td width="15%" class="t-align-left ff">Description: <% if(!"View".equals(request.getParameter("action"))){ %><span class="mandatory">*</span><%}%> </td>
                    <td width="85%" class="t-align-left">
                        <input name="txtDescription" type="text" class="formTxtBox_1" id="txtDescription" style="width:600px;"/>                      
                        <span id="txtDescriptionMsg" class="reqF_2" >&nbsp;</span>
                        <span id="idhelpuniqMsg"></span>
                    </td>
                </tr>
                    <tr>
                    <td width="15%" class="t-align-left ff">Unit: <span class="mandatory">*</span></td>
                    <td width="85%" class="t-align-left">
                        <input name="txtUnit" type="text" class="formTxtBox_1" id="txtUnit" style="width:200px;"/>                   
                        <span id="txtUnitMsg" class="reqF_2" >&nbsp;</span>
                        <span id="idhelpuniqMsg"></span>
                    </td>
                </tr>
                <!-- Page Content field -->
                
                <tr>
                    <td class="t-align-left ff">&nbsp;</td>
                    <td width="85" class="t-align-left">
                        <label class="formBtn_1">
                            <input name="btnsubmit" type="submit" value="Submit" onclick="return checkData();" />
                        </label>
                    </td>
                </tr>
                </table>
            </form>
                    </td>
                </tr>
            </table>            
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <script>
                var obj = document.getElementById('lblBOQAdd');
                if(obj != null){
                    if(obj.innerHTML == 'Add'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
                <script>
                var headSel_Obj = document.getElementById("headTabContent");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
     </body>
</html>
