<%--
    Document   : WSDetail
    Created on : Jun 6, 2011, 5:14:26 AM
    Author     : visitor
--%>

<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblWsMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.WsMasterService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Web Service </title>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#formAddWebSerice").validate({
                rules: {
                    txtWebServiceName: {spacevalidate: true, requiredWithoutSpace: true , alphaName:true, maxlength:100 }
                },
                messages: {
                    txtWebServiceName: { spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                        requiredWithoutSpace: "<div class='reqF_1'>Please Enter Web Service Name</div>",
                        alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only </div>",
                        maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"}
                },
                errorPlacement:function(error ,element){
                    error.insertAfter(element);
                }
            });
        });
        function numeric(value) {
            return /^\d+$/.test(value);
        }
        function checkData(){
            //var isMail=false;
            var retval;
            var isForEdit = 'false';
            if(isForEdit == "false"){
                if(document.getElementById("txtWebServiceName").value !="" && document.getElementById("txtWebServiceName").innerHTML != "OK"){
                    return false;
                }
            }
            if($('#formAddWebSerice').valid()){
                //$('#btnSubmit').attr("disabled", true);
                document.getElementById("formAddWebSerice").action="/WebServiceControlServlet?action=add";
                document.getElementById("formAddWebSerice").submit();
                //return true;
            }
        }
    </script>
    <script type="text/javascript">
        function passMsg(){
            jAlert("Empty Values not allowed.","Web Service Creation", function(RetVal) {
            });
            return false;
        }
    </script>

    <body>
        <%
                    StringBuilder userType = new StringBuilder();
                    if (request.getParameter("userType") != null) {
                        if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                            userType.append(request.getParameter("userType"));
                        } else {
                            userType.append("org");
                        }
                    } else {
                        userType.append("org");
                    }
                    int webServiceId = Integer.parseInt(request.getParameter("webServiceId"));
                    WsMasterService wsMasterService = (WsMasterService) AppContext.getSpringBean("WsMasterService");
                    TblWsMaster wsMaster = wsMasterService.getTblWsMaster(webServiceId);                    
                    String parentPage = request.getParameter("parentPage");
        %>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType.toString()%>" ></jsp:include>
                        <td class="contentArea">
                            <div>
                                <div class="pageHead_1">
                                    Editing a Web-Service
                                    <span style="float: right; text-align: right;">
                                        <%
                                                    if (parentPage == null) {
                                                        out.print("<a class='action-button-goback' href='WSList.jsp?mode=view'>Go back</a>");
                                                    } else {
                                                        out.print("<a class='action-button-goback' href='WSDetailView.jsp?webServiceId="+wsMaster.getWsId()+"&action=view'>Go back</a>");
                                                    }
                                        %>
                                    </span>
                                </div>
                            </div>
                            <form id="formAddWebSerice" method="post" action="/WebServiceControlServlet?action=edit">
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="ff" align="left">
                                            Fields marked with (<span class="mandatory">*</span>) are mandatory.
                                        </td>
                                    </tr>
                                    <tr></tr>
                                    <tr>
                                        <td class="ff" width="20%">
                                            Web-Service Name : <span>*</span>
                                        </td>
                                        <td width="80%">
                                            <input name="txtWebServiceName" type="text" class="formTxtBox_1" id="txtWebServiceName" style="width:400px;" value=<%=wsMaster.getWsName()%> />
                                            <span id="WSname_warning" style="color:red"></span>
                                            <br/>
                                            <input type="hidden" name="webServiceId" value=<%=wsMaster.getWsId()%> />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">
                                            Web-Service Description :
                                        </td>
                                        <td width="80%">
                                            <textarea name="txtWebServiceDesc" rows="3" class="formTxtBox_1" id="txtWebServiceDesc" style="width:400px;"><%=wsMaster.getWsDesc()%></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">
                                            Status :
                                        </td>
                                        <td>
                                            <select name="selectWsStatus" class="formTxtBox_1" id="selectWsStatus" style="width:auto;" >
                                                <%
                                                            if ("Y".equalsIgnoreCase(wsMaster.getIsActive())) {
                                                                out.print("<option value='Y' selected>Active</option>");
                                                                out.print("<option value='N'>DeActivate</option>");
                                                            } else {
                                                                out.print("<option value='N' selected>DeActivate</option>");
                                                                out.print("<option value='Y' >Active</option>");
                                                            }
                                                %>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="20%">
                                            Authentication :
                                        </td>
                                        <td>
                                            <select name="selectWsAuthentication" class="formTxtBox_1" id="selectWsAuthentication" style="width:auto;" >
                                                <%
                                                            if ("Y".equalsIgnoreCase(wsMaster.getIsAuthenticate())) {
                                                                out.print("<option value='Y' selected>Yes</option>");
                                                                out.print("<option value='N'>No</option>");
                                                            } else {
                                                                out.print("<option value='N' selected>No</option>");
                                                                out.print("<option value='Y' >Yes</option>");
                                                            }
                                                %>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="t-align-left">
                                            <label class="formBtn_1">
                                                <input type="submit" align="middle" name="submitWebSerivce" id="submitWebSerivce" value="Submit"/>
                                            </label>
                                        </td>
                                    </tr>
                                </table> 
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </body>
    <jsp:include page="../resources/common/Bottom.jsp"></jsp:include>
</html>
