<%-- 
    Document   : DefWorkflowConf
    Created on : Oct 28, 2010, 6:58:39 PM
    Author     : dhruti
--%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%
            String strUserTypeId = "";
            Object objUserId = session.getAttribute("userId");
            Object objUName = session.getAttribute("userName");
            boolean isLoggedIn = false;
            if (objUserId != null) {
                strUserTypeId = session.getAttribute("userId").toString();
            }
            if (objUName != null) {
                isLoggedIn = true;
            }
%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementRole"%>
<%@page import="com.cptu.egp.eps.web.servicebean.WorkFlowSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>e-GP Administration - Default Workflow Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            function checkPrRoles(){
               
                return checkstartsBy();
            
            }
          
        </script>

        <script type="text/javascript">
            function checkstartsBy(){
                var stch = false
                var ends = false;var flag =false;
                var stcmb = document.getElementById('cmbStartsBy');
                for (i=0; i<stcmb.options.length; i++) {
                    if (stcmb.options[i].selected) {
                        stch = true;
                    }
                }
                if(stch == true){
                    flag = true;
                }else{
                    jAlert("please Select at least One Procurement Role in Starts By","Default Workflow Configuration ", function(RetVal) {});
                    return false;
                }
                var endcmb = document.getElementById('cmbEndsBy');
                    for (i=0; i<endcmb.options.length; i++) {
                        if (endcmb.options[i].selected) {
                            ends = true;
                        }
                    }
                    if(ends == true){
                        flag = true;
                    }else{
                        jAlert("please Select at least One Procurement Role in Ends By","Default Workflow Configuration ", function(RetVal) {});                        
                        return false;
                    }
                    return flag;
                }

             
        </script>

    </head>
    <body>
        <%
                    Integer initiatorprocurementroleid = 1;
                    Integer approverprocurementroleid = 2;
                    String action = null;
                    Integer actionby = 1;
                    Date actiondate = new Date();
                    Integer wfinitruleid = 0;
                    String ispublishdaterequired = "NO";
                    String fieldName = "WorkflowRuleEngine";
                    String moduleName = request.getParameter("moduleName");
                    String eventName = request.getParameter("EventName");
                    String eventid = request.getParameter("eventid");
                    action = request.getParameter("common");

                    WorkFlowSrBean workFlowSrBean = new WorkFlowSrBean();
                    workFlowSrBean.setLogUserId(strUserTypeId);
                    if ("Submit".equals(request.getParameter("submit")) || "Update".equals(request.getParameter("submit"))) {
                        String[] stBy = request.getParameterValues("startsby");
                        StringBuffer sb = new StringBuffer();
                        for (int i = 0; i < stBy.length; i++) {
                            sb.append(stBy[i] + ",");
                        }
                        if (sb.length() > 0) {
                            sb.deleteCharAt(sb.length() - 1);
                        }
                        String startsBy = sb.toString();
                        String[] enBy = request.getParameterValues("endsby");
                        StringBuffer sb1 = new StringBuffer();
                        for (int i = 0; i < enBy.length; i++) {
                            sb1.append(enBy[i] + ",");
                        }
                        if (sb1.length() > 0) {
                            sb1.deleteCharAt(sb1.length() - 1);
                        }
                        String endsBy = sb1.toString();
                        String ispublish = request.getParameter("publishdate");
                        if (ispublish != null && !ispublish.equalsIgnoreCase("No")) {
                            if (ispublish.equals("on")) {
                                ispublishdaterequired = "YES";
                            }

                        } else {
                            ispublishdaterequired = "No";
                        }
                        // else if(ispublish.equals("yes")){
                        //  ispublishdaterequired = "YES";
                        //}
                        eventid = request.getParameter("eventid");
                        Integer eId = Integer.valueOf(eventid);
                        action = request.getParameter("action");
                        String actionWF = action.equals("Insert")?"Configure Default Workflow":"Edit Default Workflow";
                        
                        List<CommonMsgChk> insertWorkFlow = workFlowSrBean.createWorkFlow(initiatorprocurementroleid, approverprocurementroleid,
                            startsBy, endsBy, ispublishdaterequired,
                            eId, action, actionby, actiondate, wfinitruleid);
                        actionWF = insertWorkFlow.get(0).getFlag() ? actionWF : "Error in "+actionWF+" : "+insertWorkFlow.get(0).getMsg();
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), actionWF, "");                        
                        RequestDispatcher rd = request.getRequestDispatcher("WorkflowAdmin.jsp");
                        rd.forward(request, response);
                    }

                    List<TblProcurementRole> procurementRoles = null;
                    if (action.equals("Insert")) {
                        procurementRoles = workFlowSrBean.getRoles();
                        request.setAttribute("procurmentRole", procurementRoles);
                    } else if (action.equals("Edit") || action.equals("View")) {
                        procurementRoles = workFlowSrBean.getRoles();
                        List<CommonAppData> editdata = workFlowSrBean.editWorkFlowData(fieldName, eventid, "");
                        request.setAttribute("editdata", editdata);
                    }


        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td valign="top" class="contentArea">
                        <div class="pageHead_1"> Default Workflow Configuration
                            <div align="right" style="width: 20%;float: right;" >
                                <a class='action-button-goback' href='WorkflowAdmin.jsp'>Go Back</a>
                            </div>
                        </div>
<!--                        <div class="pageHead_1">Default Workflow Configuration<span style="float:right;"><a href="WorkflowAdmin.jsp" class="action-button-goback">Go Back</a></span></div>-->
                        <div>&nbsp;</div>
                        <form method="POST" id="frmDefWorkflowConf" action="">
                            <%
                                        if (action.equals("Insert")) {
                            %>
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                               
                                <tr>
                                    <td width="20%" class="ff">Module :</td>
                                    <td><%=moduleName%>
                                        <input type="hidden" name="moduleName" id="moduleName" value="<%=moduleName%>" /></td>
                                         <td></td>
                                        <td></td>
                                </tr>
                                <tr>
                                    <td class="ff">Process :</td>
                                    <td><%=eventName%>
                                        <input type="hidden" name="eventName" id="proceName" value="<%=eventName%>" />
                                        <input type="hidden" name="eventid" id="eventid" value="<%=eventid%>" />
                                        <input type="hidden" name="action" id="action" value="<%=action%>" /></td>
                                        <td></td>
                                        <td></td>
                                </tr>

                                <tr>
                                    <td class="ff">Starts By : <span>*</span></td>
                                    <td>
                                        <select name="startsby" class="formTxtBox_1" id="cmbStartsBy" multiple="multiple" style="width:200px;height:300px;">
                                            <c:forEach items="${procurmentRole}" var="proc">
                                                <option value="<c:out value="${proc.procurementRoleId}" />" ><c:out value="${proc.procurementRole}" /></option>
                                            </c:forEach>
                                        </select>
                                    </td>
                                     <td class="ff">Ends By : <span>*</span></td>
                                    <td>
                                        <select name="endsby"  class="formTxtBox_1" id="cmbendsBy" multiple="multiple" style="width:200px;height:300px;">
                                            <c:forEach items="${procurmentRole}" var="proc">
                                                <option  value="<c:out value="${proc.procurementRoleId}" />" ><c:out value="${proc.procurementRole}" /></option>
                                            </c:forEach>
                                        </select>

                                    </td>
                                </tr>

                               
                                <!--
                                <tr>
                                  <td class="ff">Publish Date Requires : </td>
                                  <td class="txt_2"><input type="checkbox"  name="publishdate" id="chkpublishdate" /></td>
                                </tr> -->
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><label class="formBtn_1">
                                            <input type="submit" name="submit" id="btnsubmit" value="Submit"  onclick="return checkPrRoles();" />
                                            <input type='hidden'  name='publishdate' id='chkpublishdate' value='No' />
                                        </label>
                                    </td>
                                </tr>
                            </table>
                            <%
                                     } else if (action.equals("Edit")) {
                                         List<CommonAppData> editdata = (List) request.getAttribute("editdata");
                                         StringBuffer nstby = new StringBuffer();
                                         StringBuffer nendby = new StringBuffer();
                                         StringBuffer stby = new StringBuffer();
                                         StringBuffer endby = new StringBuffer();
                                         Iterator it = editdata.iterator();
                                         while (it.hasNext()) {
                                             CommonAppData commonAppData = (CommonAppData) it.next();
                                             moduleName = commonAppData.getFieldName1();
                                             eventName = commonAppData.getFieldName2();
                                             stby.append(commonAppData.getFieldName3() + ",");
                                             endby.append(commonAppData.getFieldName4() + ",");
                                             ispublishdaterequired = commonAppData.getFieldName5();

                                         }
                                         if (stby.length() > 0) {

                                             String str = stby.toString();
                                             String[] str1 = str.split(",");
                                             for (int j = 0; j < str1.length; j++) {
                                                 boolean check = false;
                                                 if (nstby.length() > 0) {
                                                     String str2 = nstby.toString();
                                                     for (int k = 0; k < str2.split(",").length; k++) {
                                                         if (str1[j].equals(str2.split(",")[k])) {
                                                             check = true;
                                                         }
                                                     }
                                                     if (!check) {
                                                         nstby.append(str1[j] + ",");
                                                     }
                                                     continue;
                                                 } else {
                                                     nstby.append(str1[j] + ",");
                                                 }
                                             }
                                             nstby.deleteCharAt(nstby.length() - 1);
                                         }
                                         if (endby.length() > 0) {
                                             String str = endby.toString();
                                             String[] str1 = str.split(",");
                                             for (int j = 0; j < str1.length; j++) {
                                                 boolean check = false;
                                                 if (endby.length() > 0) {
                                                     String str2 = nendby.toString();
                                                     for (int k = 0; k < str2.split(",").length; k++) {
                                                         if (str1[j].equals(str2.split(",")[k])) {
                                                             check = true;
                                                         }
                                                     }
                                                     if (!check) {
                                                         nendby.append(str1[j] + ",");
                                                     }
                                                     continue;
                                                 } else {
                                                     nendby.append(str1[j] + ",");
                                                 }
                                             }
                                             nendby.deleteCharAt(nendby.length() - 1);
                                         }


                                         String str3 = nstby.toString();
                                         String[] startsBy = str3.split(",");
                                         String str4 = nendby.toString();
                                         String[] endsBy = str4.split(",");

                                         out.write("<table border='0' cellspacing='10' cellpadding='0' class='formStyle_1'>");
                                         out.write("<tr>");
                                         out.write("<td width='20%' class='ff'>Module :</td>");
                                         out.write("<td>");
                                         out.write("<label name='moduleName' >" + moduleName + "</label></td>");
                                         out.write("<td></td><td></td></tr>");
                                         out.write("<tr> <td class='ff'>Process :</td><td>");
                                         out.write("<label name='eventName'>" + eventName + "</label>");
                                         out.write("<input type='hidden' name='eventid' id='eventid' value='" + eventid + "'/>");
                                         out.write("<input type='hidden' name='action' id='action' value='" + action + "' /></td>");
                                         out.write("<td></td><td></td></tr>");
                                         out.write(" <tr><td class='ff'><p>Starts By :<span>*</span><br>(Procurement Role)</br></p></td><td>");
                                         out.write("<select name='startsby' class='formTxtBox_1' id='cmbStartsBy' multiple='multiple' style='width:200px;height:300px;'>");
                                         Iterator edits = procurementRoles.iterator();
                                         while (edits.hasNext()) {
                                             boolean check = false;
                                             TblProcurementRole tblProcurementRole = (TblProcurementRole) edits.next();
                                             //HOPE -> HOPA changed by Emtazul on 19/April/2016
                                            String ProcurementRole;
                                            if(tblProcurementRole.getProcurementRole().equalsIgnoreCase("HOPE"))
                                            {
                                                ProcurementRole = "HOPA";
                                            }
                                            else if(tblProcurementRole.getProcurementRole().equalsIgnoreCase("PE"))
                                            {
                                                ProcurementRole = "PA";
                                            }
                                            else
                                            {
                                                ProcurementRole = tblProcurementRole.getProcurementRole();
                                            } 
                                            for (int i = 0; i < startsBy.length; i++) {
                                                 Byte b = Byte.valueOf(startsBy[i]);
                                                 if (tblProcurementRole.getProcurementRoleId() == b) { 
                                                    
                                                    out.write("<option selected='true' value='" + tblProcurementRole.getProcurementRoleId()
                                                             + "' />" + ProcurementRole + "</option>");
                                                     check = true;
                                                 }
                                             }
                                             if (check == false) {
                                                if(ProcurementRole.equalsIgnoreCase("AU") || ProcurementRole.equalsIgnoreCase("TOC/POC") || ProcurementRole.equalsIgnoreCase("TEC/PEC") || ProcurementRole.equalsIgnoreCase("TC"))
                                                {
                                                 out.write("<option value='" + tblProcurementRole.getProcurementRoleId()
                                                         + "' />" + ProcurementRole + "</option>");     
                                                }
                                             }
                                         }

                                         out.write("</select></td>");
                                         out.write("<td class='ff'><p>Ends By :<span>*</span><br>(Procurement Role)</br></p></td><td>");
                                         out.write("<select name='endsby' class='formTxtBox_1' id='cmbEndsBy' multiple='multiple' style='width:200px;height:300px;'>");
                                         edits = procurementRoles.iterator();
                                         while (edits.hasNext()) {
                                             boolean check = false;
                                             TblProcurementRole tblProcurementRole = (TblProcurementRole) edits.next();
                                            //HOPE -> HOPA changed by Emtazul on 19/April/2016
                                            String ProcurementRole;
                                            if(tblProcurementRole.getProcurementRole().equalsIgnoreCase("HOPE"))
                                            {
                                                ProcurementRole = "HOPA";
                                            }
                                            else if(tblProcurementRole.getProcurementRole().equalsIgnoreCase("PE"))
                                            {
                                                ProcurementRole = "PA";
                                            }
                                            else
                                            {
                                                ProcurementRole = tblProcurementRole.getProcurementRole();
                                            } 
                                             for (int i = 0; i < endsBy.length; i++) {
                                                 if (tblProcurementRole.getProcurementRoleId() == Byte.valueOf(endsBy[i])) {
                                                     out.write("<option selected='true' value='" + tblProcurementRole.getProcurementRoleId()
                                                             + "' />" + ProcurementRole + "</option>");
                                                     check = true;
                                                 }
                                             }
                                             if (check == false) {
                                                    if(ProcurementRole.equalsIgnoreCase("AU") || ProcurementRole.equalsIgnoreCase("TOC/POC") || ProcurementRole.equalsIgnoreCase("TEC/PEC") || ProcurementRole.equalsIgnoreCase("TC"))
                                                    {
                                                        out.write("<option value='" + tblProcurementRole.getProcurementRoleId()
                                                         + "' />" + ProcurementRole + "</option>");
                                                    }
                                              }
                                         }

                                         out.write("</select></td></tr>");
                                         //out.write(" <tr> <td class='ff'>Publish Date Requires : </td>");

                                         //else out.write(" <td class='txt_2'><input type='checkbox' name='publishdate' id='chkpublishdate' /></td></tr>");
                                         out.write("<tr><td>&nbsp;</td><td><label class='formBtn_1'>");
                                         out.write("<input type='submit' name='submit' id='btnsubmit' value='Update' onclick='return checkPrRoles();'");
                                         out.write("</label>");
                                         if (ispublishdaterequired.equalsIgnoreCase("yes")) {
                                             out.write("<input type='hidden'  name='publishdate' id='chkpublishdate' value='Yes' />");
                                         } else {
                                             out.write("<input type='hidden'  name='publishdate' id='chkpublishdate' value='No' />");
                                         }
                                         out.write("</td>");
                                         out.write("</tr></table>");
                                     } else if (action.equals("View")) {
                                         List<CommonAppData> editdata = (List) request.getAttribute("editdata");
                                         StringBuffer nstby = new StringBuffer();
                                         StringBuffer nendby = new StringBuffer();
                                         StringBuffer stby = new StringBuffer();
                                         StringBuffer endby = new StringBuffer();
                                         Iterator it = editdata.iterator();
                                         while (it.hasNext()) {
                                             CommonAppData commonAppData = (CommonAppData) it.next();
                                             moduleName = commonAppData.getFieldName1();
                                             eventName = commonAppData.getFieldName2();
                                             stby.append(commonAppData.getFieldName3() + ",");
                                             endby.append(commonAppData.getFieldName4() + ",");
                                             ispublishdaterequired = commonAppData.getFieldName5();

                                         }
                                         if (stby.length() > 0) {

                                             String str = stby.toString();
                                             String[] str1 = str.split(",");
                                             for (int j = 0; j < str1.length; j++) {
                                                 boolean check = false;
                                                 if (nstby.length() > 0) {
                                                     String str2 = nstby.toString();
                                                     for (int k = 0; k < str2.split(",").length; k++) {
                                                         if (str1[j].equals(str2.split(",")[k])) {
                                                             check = true;
                                                         }
                                                     }
                                                     if (!check) {
                                                         nstby.append(str1[j] + ",");
                                                     }
                                                     continue;
                                                 } else {
                                                     nstby.append(str1[j] + ",");
                                                 }
                                             }
                                             nstby.deleteCharAt(nstby.length() - 1);
                                         }
                                         if (endby.length() > 0) {
                                             String str = endby.toString();
                                             String[] str1 = str.split(",");
                                             for (int j = 0; j < str1.length; j++) {
                                                 boolean check = false;
                                                 if (endby.length() > 0) {
                                                     String str2 = nendby.toString();
                                                     for (int k = 0; k < str2.split(",").length; k++) {
                                                         if (str1[j].equals(str2.split(",")[k])) {
                                                             check = true;
                                                         }
                                                     }
                                                     if (!check) {
                                                         nendby.append(str1[j] + ",");
                                                     }
                                                     continue;
                                                 } else {
                                                     nendby.append(str1[j] + ",");
                                                 }
                                             }
                                             nendby.deleteCharAt(nendby.length() - 1);
                                         }


                                         String str3 = nstby.toString();
                                         String[] startsBy = str3.split(",");
                                         String str4 = nendby.toString();
                                         String[] endsBy = str4.split(",");

                                         out.write("<table border='0' cellspacing='10' cellpadding='0' class='formStyle_1'>");
                                         out.write("<tr>");
                                         out.write("<td width='20%' class='ff'>Module :</td>");
                                         out.write("<td>");
                                         out.write("<label name='moduleName' >" + moduleName + "</label></td>");
                                         out.write("<td></td><td></td></tr>");
                                         out.write("<tr> <td class='ff'>Process :</td><td>");
                                         out.write("<label name='eventName'>" + eventName + "</label>");
                                         out.write("<input type='hidden' name='eventid' id='eventid' value='" + eventid + "'/>");
                                         out.write("<input type='hidden' name='action' id='action' value='" + action + "' /></td>");
                                         out.write("<td></td><td></td></tr>");
                                         out.write(" <tr><td class='ff'><p>Starts By :<span></span><br>(Procurement Role)</br></p></td><td>");
                                        
                                         Iterator edits = procurementRoles.iterator();
                                         StringBuffer startby = new StringBuffer();

                                         while (edits.hasNext()) {
                                             // boolean check = false;
                                             TblProcurementRole tblProcurementRole = (TblProcurementRole) edits.next();
                                             for (int i = 0; i < startsBy.length; i++) {
                                                 Byte b = Byte.valueOf(startsBy[i]);
                                                 if (tblProcurementRole.getProcurementRoleId() == b) {
                                                    //HOPE -> HOPA changed by Emtazul on 19/April/2016
                                                    if(tblProcurementRole.getProcurementRole().equalsIgnoreCase("HOPE"))
                                                        startby.append("HOPA");
                                                    else if(tblProcurementRole.getProcurementRole().equalsIgnoreCase("PE"))
                                                        startby.append("PA");
                                                    else
                                                        startby.append(tblProcurementRole.getProcurementRole());
                                                    startby.append(",");
                                                    
                                                 }
                                             }
                                             
                                         }
                                         startby.delete(startby.length()-1, startby.length());
                                         out.write(startby.toString());
                                         out.write("</td>");
                                         out.write("<td class='ff'><p>Ends By :<span></span><br>(Procurement Role)</br></p></td><td>");
                                         edits = procurementRoles.iterator();
                                          StringBuffer endsby = new StringBuffer();
                                         while (edits.hasNext()) {
                                             // boolean check = false;
                                             TblProcurementRole tblProcurementRole = (TblProcurementRole) edits.next();
                                             for (int i = 0; i < endsBy.length; i++) {
                                                 if (tblProcurementRole.getProcurementRoleId() == Byte.valueOf(endsBy[i])) {
                                                    //HOPE -> HOPA changed by Emtazul on 19/April/2016
                                                     if(tblProcurementRole.getProcurementRole().equalsIgnoreCase("HOPE"))
                                                        endsby.append("HOPA");
                                                     else if(tblProcurementRole.getProcurementRole().equalsIgnoreCase("PE"))
                                                        endsby.append("PA");
                                                    else
                                                        endsby.append(tblProcurementRole.getProcurementRole());
                                                      endsby.append(",");
                                                    
                                                 }
                                             }
                                             
                                         }
                                         endsby.delete(endsby.length()-1, endsby.length());
                                         out.write(endsby.toString());
                                         out.write("</td></tr>");
                                         out.write("</table>");
                                     }
                            %>

                            <div>&nbsp;</div>
                        </form>
                    </td>
                </tr>
            </table>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <!--Dashboard Footer End-->
        </div>
             <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
