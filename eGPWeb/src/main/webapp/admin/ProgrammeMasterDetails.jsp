<%-- 
    Document   : ProgrammeMasterDetails
    Created on : Nov 1, 2010, 8:24:50 PM
    Author     : Naresh.Akurathi
--%>


<%@page import="java.util.ListIterator"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="progmasterDtBean" class="com.cptu.egp.eps.web.databean.ProgrammeMasterDtBean"/>
<jsp:useBean id="progmasterSrBean" class="com.cptu.egp.eps.web.servicebean.ProgrammeMasterSrBean"/>
<jsp:setProperty property="*" name="progmasterDtBean"/>
<%@page import="com.cptu.egp.eps.model.table.TblProgrammeMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>e-GP - Programme Master Details</title>
      <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
       <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

    </head>
    <body>
        <%!
            TblProgrammeMaster tblPrgMaster = new TblProgrammeMaster();
            int id=0;

        %>


        <div class="dashboard_div">
          <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <%--<%@include file="/resources/common/AfterLoginLeft.jsp" %>--%>

                        <td class="contentArea">

            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Programme Information Master Details</div>
            <table width="100%" cellspacing="0" class="tableList_1 t_space">
          <tr>
            <th width="4%"  >Sl. No.</th>
            <th width="11%">Programme Information Code</th>
            <th width="12%">Programme Information Name</th>
            <th width="12%">Action</th>
          </tr>
           <%

               ListIterator it = progmasterSrBean.getAllProgrammeMaster().listIterator();
                 if(progmasterSrBean.getAllProgrammeMaster()!=null)
                 {
                    while(it.hasNext()){it.next();}
                    int i = 0;
                    int v = 0;
                    while(it.hasPrevious())
                    {
                        tblPrgMaster = (TblProgrammeMaster)it.previous();
                        id=tblPrgMaster.getProgId();
                        v++;

           %>
          <%if(v%2==0){%>
      <tr style='background-color:#E4FAD0;'>
          <%}else{%>
          <tr>
          <%}%>
            <td style="text-align:left;"><%= ++i%></td>
            <td style="text-align:left;"><%= tblPrgMaster.getProgCode() %></td>
            <td style="text-align:left;"><%= tblPrgMaster.getProgName()%></td>
            <td style="text-align:left;"><a href="ProgrammeMaster.jsp?action=edit&id=<%= id%>"><img src="../resources/images/Dashboard/viewIcn.png" alt="Go Back" class="linkIcon_1"/><b>Edit</b></a></td>

          </tr>



           <%
                }
                }
                else
                    {
                    %>
                 <tr>
            <td style="text-align:left;" colspan="4"> No Programme Found</td>

          </tr>
                <%}
           %>
           </table>

            </td>
           </tr>
        </table>
            <!--Dashboard Content Part End-->

           
            <!--Dashboard Footer Start-->
            <%@include file="/resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
            <script>
                var obj = document.getElementById('lblProgInfoEdit');
                if(obj != null){
                    if(obj.innerHTML == 'Edit'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabContent");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
