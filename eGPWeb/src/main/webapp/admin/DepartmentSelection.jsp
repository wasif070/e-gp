<%-- 
    Document   : DepartmentSelection
    Created on : Oct 25, 2010, 10:14:10 AM
    Author     : rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Select Department</title>
    </head>
    <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    <link href="../resources//css/home.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function() {
            $('#cmbcabinetDivision').change(function() {
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:2,funName:'ministryCombo'},  function(j){
                    $("select#cmbMinistry").html(j);
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#cmbMinistry').change(function() {
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbMinistry').val(),funName:'divisionCombo'},  function(j){
                    $("select#cmbDivision").html(j);
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#cmbDivision').change(function() {
                $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbDivision').val(),funName:'organizationCombo'},  function(j){
                    $("select#cmbOraganisation").html(j);
                });
            });
        });
    </script>

    <jsp:useBean id="peOfficeCreationSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.PEOfficeCreationSrBean"/>
      <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top">
                <td class="contentArea_1"><div class="pageHead_1">
                        Select Department
                    </div>
                </td>
            </tr>
           
            <tr>
        <form method="post">
            <table width="80%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td><div>&nbsp;</div>
                        &nbsp;&nbsp;<select id="cmbCCGP" name="ccgp" class="formTxtBox_1" onchange="changeCCGP(this);">
                            <option value="select">select</option>
                            <option value="1">CCGP</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;<select id="cmbcabinetDivision" name="cabinetDivision" class="formTxtBox_1" style="display: none" onchange="changeMinistry(this);">
                            <option value="select">select</option>
                            <option value="2">Cabinet Division</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td> &nbsp;&nbsp;<select id="cmbMinistry" name="ministry" style="display: none" class="formTxtBox_1" onchange="changeDivision(this);">
                            <option></option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;<select id="cmbDivision" style="display: none" name="division" class="formTxtBox_1" onchange="changeOrganization(this);">
                            <option></option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;&nbsp;<select id="cmbOraganisation" style="display: none" name="oraganisation" class="formTxtBox_1" onchange="changeOrganizationId(this);">
                            <option></option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="hidden" id="txtid"/>
                    </td>
                    <td>
                        <input type="hidden" id="txtname"/>
                    </td>
                    <td>
                        <input type="hidden" id="txtDepartmentType"/>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;<label class="formBtn_1">
                        <input type="button" name="submit" value="Submit" onclick="pageForward();"/>
                        </label>
                    </td>
                </tr>
            </table>
        </form>
      </tr>
    </table>
        <script>
            function changeCCGP(obj){
                //alert(document.getElementById('cmbCCGP').selectedIndex);
                if(document.getElementById('cmbCCGP').selectedIndex == 1){
                    var ccgp=document.getElementById("cmbCCGP");
                    var selectedccgp="";
                    for(var i=0;i<ccgp.length;i++)
                    {
                        if(ccgp[i].selected){
                            selectedccgp=ccgp[i].text;
                            document.getElementById('txtname').value =selectedccgp;
                        }
                    }
                    document.getElementById('cmbcabinetDivision').style.display = 'block';
                    document.getElementById('txtid').value = obj.value;
                    document.getElementById('txtDepartmentType').value = "CCGP";
                }
                else{
                    document.getElementById('cmbcabinetDivision').style.display = 'none';
                    document.getElementById('cmbMinistry').style.display = 'none';
                    document.getElementById('cmbDivision').style.display = 'none';
                    document.getElementById('cmbOraganisation').style.display = 'none';
                }
            }
            function changeMinistry(obj){
                if(document.getElementById('cmbcabinetDivision').selectedIndex == 1){
                    var CD=document.getElementById("cmbcabinetDivision");
                    var selectedCD="";
                    for(var i=0;i<CD.length;i++)
                    {
                        if(CD[i].selected){
                            selectedCD=CD[i].text;
                            document.getElementById('txtname').value =selectedCD;
                        }
                    }
                    document.getElementById('cmbMinistry').style.display = 'block';
                    document.getElementById('txtid').value = obj.value;
                    document.getElementById('txtDepartmentType').value = "CabinetDivision";
                }
                else{
                    document.getElementById('cmbMinistry').style.display = 'none';
                    document.getElementById('cmbDivision').style.display = 'none';
                    document.getElementById('cmbOraganisation').style.display = 'none';
                }
            }

            function changeDivision(obj){
                if(document.getElementById('cmbMinistry').selectedIndex == 0){
                    document.getElementById('cmbDivision').style.display = 'none';
                    document.getElementById('cmbOraganisation').style.display = 'none';
                }
                else{
                    var ministry=document.getElementById("cmbMinistry");
                    var selectedMinistry="";
                    for(var i=0;i<ministry.length;i++)
                    {
                        if(ministry[i].selected){
                            selectedMinistry=ministry[i].text;
                            document.getElementById('txtname').value =selectedMinistry;
                        }
                    }
                    document.getElementById('cmbDivision').style.display = 'block';
                    document.getElementById('txtid').value = obj.value;
                    document.getElementById('txtDepartmentType').value = "Ministry";
                    // document.getElementById('txtname').value = document.getElementById('cmbMinistry').name;

                }
            }
            function changeOrganization(obj){
                if(document.getElementById('cmbDivision').selectedIndex == 0){
                    document.getElementById('cmbOraganisation').style.display = 'none';
                }
                else{
                    var division=document.getElementById("cmbDivision");
                    var selectedDivision="";
                    for(var i=0;i<division.length;i++)
                    {
                        if(division[i].selected){
                            selectedDivision=division[i].text;
                            document.getElementById('txtname').value =selectedDivision;
                        }
                    }
                    document.getElementById('cmbOraganisation').style.display = 'block';
                    document.getElementById('txtid').value = obj.value;
                    document.getElementById('txtDepartmentType').value = "Division";
                }
            }
            function changeOrganizationId(obj){
                if(document.getElementById('cmbOraganisation').selectedIndex == 0){
                }
                else{
                    var organization=document.getElementById("cmbOraganisation");
                    var selectedOrganization="";
                    for(var i=0;i<organization.length;i++)
                    {
                        if(organization[i].selected){
                            selectedOrganization=organization[i].text;
                            document.getElementById('txtname').value =selectedOrganization;
                        }
                    }
                    document.getElementById('txtid').value = obj.value;
                    document.getElementById('txtDepartmentType').value = "Organization";
                }
            }
            function pageForward(){
                window.opener.document.getElementById('txtdepartment').value= document.getElementById('txtname').value;
                window.opener.document.getElementById('txtdepartmentid').value= document.getElementById('txtid').value;
                if(window.opener.document.getElementById('txtDepartmentType') != null){
                    window.opener.document.getElementById('txtDepartmentType').value=document.getElementById('txtDepartmentType').value;
                }
                window.opener.document.getElementById('txtdepartment').focus();
                window.close();
            }
        </script>
    </body>
</html>
