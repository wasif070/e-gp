<%-- 
    Document   : PrepareGCCSubSection
    Created on : 24-Oct-2010, 3:57:04 PM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Untitled Document</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1">Prepare GCC</div>
                            </div>
                            <table width="100%" border="0" cellspacing="10" class="tableView_1">
                                <tr>
                                    <td align="left">
                                        <img src="../resources/images/Dashboard/addIcn.png" width="16" height="16" class="linkIcon_1" /><a href="#">Add Sub Section</a> &nbsp;|&nbsp;
                                        <img src="../resources/images/Dashboard/addIcn.png" width="16" height="16" class="linkIcon_1" /><a href="#">Add Clause</a> &nbsp;|&nbsp;
                                        <img src="../resources/images/Dashboard/addIcn.png" width="16" height="16" class="linkIcon_1" /><a href="#">Add Sub Clause</a>
                                    </td>
                                    <td align="right">
                                        <img src="../resources/images/Dashboard/removeIcn.png" width="16" height="16" class="linkIcon_1" /><a href="#">Remove Sub Section</a> &nbsp;|&nbsp;
                                        <img src="../resources/images/Dashboard/removeIcn.png" width="16" height="16" class="linkIcon_1" /><a href="#">Remove Clause</a> &nbsp;|&nbsp;
                                        <img src="../resources/images/Dashboard/removeIcn.png" width="16" height="16" class="linkIcon_1" /><a href="#">Remove Sub Clause</a>
                                    </td>
                                </tr>
                            </table>
                            <form action="PreparePCC.jsp" method="post">
                                <table width="100%" cellspacing="0" class="tableList_1">
                                    <tr>
                                        <th>Select</th>
                                        <th>Sub Section Name</th>
                                        <th>A. General</th>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            <input type="checkbox" name="checkbox" id="checkbox" />
                                        </td>
                                        <td>
                                            Clause No. and Clause Description
                                        </td>
                                        <td>
                                            1   Definitions
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="checkbox" id="checkbox" />
                                        </td>
                                        <td>
                                            Sub Clause No. and Sub Clause Description
                                        </td>
                                        <td>
                                            1.1	In the Conditions of Contract, which include Particular Conditions and these General Conditions, the following words and expressions shall have the meaning hereby assigned to them. Boldface type is used to identify the defined terms:
                                            <br></br>
                                            (a)	Act means The Public Procurement Act, 2006 (Act 24 of 2006).<br></br>
                                            (b)	Approving Authority means the authority which, in accordance with the Delegation of Financial Powers, approves the award of contract<br></br>
                                            (c)	Bill of Quantities (BOQ) means the priced and completed Bill of   Quantities forming part of the Contract defined in GCC Clause 21<br></br>
                                            (d)	Contractor means the Person under contract with the Procuring Entity for the execution of Works under the Rules and the Act as stated in the PCC<br></br>
                                            (e)	Completion Date is the actual date of completion of the Works and Physical services certified by the Project Manager, in accordance with GCC Clause 31 & 32<br></br>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            &nbsp;
                                        </td>
                                        <td>
                                            PCC Applicable
                                        </td>
                                        <td>
                                            <input type="checkbox" name="checkbox" id="checkbox" />
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: center;">
                                            <input type="checkbox" name="checkbox" id="checkbox" />
                                        </td>
                                        <td>
                                            Clause No. and Clause Description
                                        </td>
                                        <td>
                                            2  Corrupt, Fraudulent, Collusive or Coercive Practices
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">
                                            <input type="checkbox" name="checkbox" id="checkbox" />
                                        </td>
                                        <td>
                                            Sub Clause No. and Sub Clause Description
                                        </td>
                                        <td>
                                            2.1	The Government requires that Procuring Entities, as well as Bidders and Suppliers shall, during the Procurement proceedings and the execution of Contracts under public funds, ensure-
                                            <br></br>(a)	strict compliance with the provisions of Section 64 of the Public Procurement Act 2006 (Act 24 of 2006);
                                            <br></br>(b)	abiding by the code of ethics as mentioned  in the Rule127 of the Public Procurement Rules and Regulations 2009;
                                            <br></br>(c)	that neither its any officer nor any staff or any other agents or intermediaries working on its behalf engages in any practice as detailed in the Rule 127
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            &nbsp;
                                        </td>
                                        <td>
                                            PCC Applicable
                                        </td>
                                        <td>
                                            <input type="checkbox" name="checkbox" id="checkbox" />
                                        </td>
                                    </tr>
                                </table>
                                <div class="t_space" align="center">
                                    <label class="formBtn_1">
                                        <input type="submit" name="button" id="button" value="Next" /></label>
                                </div>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
