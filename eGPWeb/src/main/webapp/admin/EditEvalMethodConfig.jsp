<%-- 
    Document   : EditEvalMethodConfig
    Created on : Mar 16, 2011, 4:12:22 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigEvalMethod"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvalMethodConfigService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Edit Evaluation Method Configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            function validate(){
                $(".err").remove();
                var tbol = true;
                var len=$('#tbodyData').children().length-1;
                var cnt=0;
                var cnt2=0;
                for(var i=0;i<len;i++){
                    if($.trim($("#noEnv_"+i).val()) == "") {
                        $("#noEnv_"+i).parent().append("<div class='err' style='color:red;'>Please enter no. of Envelopes</div>");
                        cnt++;
                    }else{
                        if(!($("#noEnv_"+i).val() == "1" || $("#noEnv_"+i).val() == "2")) {
                            $("#noEnv_"+i).parent().append("<div class='err' style='color:red;'>Please enter either 1 or 2</div>");
                            cnt2++;
                        }
                    }
                }
                if(cnt!=0){
                    tbol = false;
                }
                if(cnt2!=0){
                    tbol = false;
                }
                if(tbol){
                    $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/EvalMethodConfigServlet",
                        data:"evalId="+$('#evalId').val()+"&"+"procNature="+$('#procNature_0').val()+"&"+"tendType="+$('#tendType_0').val()+"&"+"procMethod="+$('#procMethod_0').val()+"&"+"action=ajaxUniqueConfig",
                        async: false,
                        success: function(j){
                            if(j.toString()!="0"){
                                tbol=false;
                                jAlert("Combination of 'Procurement Category','Tender Type' and 'Procurement Method' already exist.","Service Configuration", function(RetVal) {
                                });                                
                            }
                        }
                    });                                        
                }
                return tbol;
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <%
                    EvalMethodConfigService configService = (EvalMethodConfigService) AppContext.getSpringBean("EvalMethodConfigService");
                    configService.setLogUserId(session.getAttribute("userId").toString());
                    List<Object[]> procurementNature = configService.getProcurementNature();
                    List<Object[]> tenderTypes = configService.getTenderTypes();
                    List<Object[]> procurementMethod = configService.getProcurementMethod();
                    List<TblConfigEvalMethod> getEvalMethods = configService.getTblConfigEvalMethod("evalMethodConfId", Operation_enum.EQ, Integer.parseInt(request.getParameter("evalId")));
        %>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
         <td class="contentArea">
      
            <div class="pageHead_1">Edit Evaluation Method Configuration</div>
            <form action="<%=request.getContextPath()%>/EvalMethodConfigServlet?action=editSingleEval" method="post" >
                <table width="100%">
                    <tr>
                        
                        <td valign="top">
                            <table class="tableList_1 t_space" cellspacing="0" width="100%" id="members">
                                <tbody id="tbodyData">
                                    <tr>
                                        <th class="t-align-center">Procurement Category<br>(<span class="mandatory">*</span>)</th>
                                        <th class="t-align-center">Tender Type<br>(<span class="mandatory">*</span>)</th>
                                        <th class="t-align-center">Procurement Method<br>(<span class="mandatory">*</span>)</th>
                                        <th class="t-align-center">No Of Envelopes<br>(<span class="mandatory">*</span>)</th>
                                        <th class="t-align-center">Evaluation Method<br>(<span class="mandatory">*</span>)</th>
                                    </tr>
                                    <%
                                                int cnt = 0;
                                                do {
                                    %>
                                    <tr>
                                        <td class="t-align-center">
                                            <input type="hidden" id="evalId" name="evalId" value="<%=getEvalMethods.get(cnt).getEvalMethodConfId()%>">
                                            <select name="procNature" class="formTxtBox_1" id="procNature_<%=cnt%>">
                                                    <%for (Object[] proc : procurementNature) {%>
                                                    <option value="<%=proc[0]%>" <%if (getEvalMethods.get(cnt).getProcurementNatureId() == (Byte) proc[0]) {
                                                            out.print("selected");
                                                        }%>><%=proc[1]%></option>
                                                <%}%>
                                            </select>
                                        </td>
                                        <td class="t-align-center">
                                            <select name="tendType" class="formTxtBox_1" id="tendType_<%=cnt%>">
                                                    <% for (Object[] proc : tenderTypes) {
                                                        if(!proc[1].equals("PQ") && !proc[1].equals("RFA") && !proc[1].equals("2 Stage-PQ"))
                                                        {
                                                    %>
                                                    <option value="<%=proc[0]%>" <%if (getEvalMethods.get(cnt).getTenderTypeId() == (Byte) proc[0]) {
                                                             out.print("selected");
                                                         }%>><%=proc[1]%></option>
                                                <%}}%>
                                            </select>
                                        </td>
                                       
                                        <td class="t-align-center">
                                            <select name="procMethod" class="formTxtBox_1" id="procMethod_<%=cnt%>">
                                                <%for (Object[] proc : procurementMethod) {
                                                if("OTM".equalsIgnoreCase((String)proc[1]) ||
                                                           "LTM".equalsIgnoreCase((String)proc[1]) ||
                                                           "DP".equalsIgnoreCase((String)proc[1]) ||
                                                          // "LEQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                         //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                           "RFQ".equalsIgnoreCase((String)proc[1]) ||
                                                          // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                         //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                           "FC".equalsIgnoreCase((String)proc[1]) ||
                                                          // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                                                           "DPM".equalsIgnoreCase((String)proc[1])||
                                                            "QCBS".equalsIgnoreCase((String)proc[1]) ||
                                                            "LCS".equalsIgnoreCase((String)proc[1]) ||
                                                            "SFB".equalsIgnoreCase((String)proc[1]) ||
                                                            "SBCQ".equalsIgnoreCase((String)proc[1]) ||
                                                            "SSS".equalsIgnoreCase((String)proc[1]) ||
                                                            "IC".equalsIgnoreCase((String)proc[1]))
                                                            {
                                                                if("RFQ".equalsIgnoreCase((String)proc[1]))
                                                                {%>
                                                                    <option value="<%=proc[0]%>" <%if (getEvalMethods.get(cnt).getProcurementMethodId() == (Byte) proc[0]) {
                                                                    out.print("selected");
                                                                    
                                                                   }%>>LEM</option>
                                                                  <%}
                                                                  else if("DPM".equalsIgnoreCase((String)proc[1]))
                                                                  {%>
                                                                  <option value="<%=proc[0]%>" <%if (getEvalMethods.get(cnt).getProcurementMethodId() == (Byte) proc[0]) {
                                                                    out.print("selected");
                                                                    
                                                                   }%>>DCM</option>
                                                                  <%}
                                                                  else
                                                                  {%>
                                                                  <option value="<%=proc[0]%>" <%if (getEvalMethods.get(cnt).getProcurementMethodId() == (Byte) proc[0]) {
                                                                    out.print("selected");
                                                                    
                                                                   }%>><%=proc[1]%></option>
                                                            <%}}}%>
                                            </select>
                                        </td>
                                        <td class="t-align-center">
                                            <input type="text" value="<%out.print(getEvalMethods.get(cnt).getNoOfEnvelops());%>" name="noEnv" id="noEnv_<%=cnt%>" class="formTxtBox_1">
                                        </td>
                                        <td class="t-align-center">
                                            <select name="evalMethod" class="formTxtBox_1" id="evalMethod_<%=cnt%>">
                                                    <option value='1' <%if (getEvalMethods.get(cnt).getEvalMethod() == 1) {
                                                                                                            out.print("selected");
                                                                                                        }%>>T1</option>
                                                        <option value='2' <%if (getEvalMethods.get(cnt).getEvalMethod() == 2) {
                                                                                                                out.print("selected");
                                                                                                            }%>>L1</option>
                                                        <option value='3' <%if (getEvalMethods.get(cnt).getEvalMethod() == 3) {
                                                                                                                out.print("selected");
                                                                                                            }%>>T1L1</option>
<!--                                                        <option value='4' < %if (getEvalMethods.get(cnt).getEvalMethod() == 4) {
                                                                                                                out.print("selected");
                                                                                                            }%>>T1L1A</option>-->
                                            </select>
                                        </td>
                                    </tr>
                                    <%cnt++;
                                                } while (cnt < getEvalMethods.size());%>
                                </tbody>
                                <tr>
                                    <td colspan="8" class="t-align-center"><span class="formBtn_1">

                                            <input name="button2" id="button2" value="Submit" type="submit" onclick="return validate();">
                                        </span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </form>
         </td>
            </tr>
        </table>
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
</html>
