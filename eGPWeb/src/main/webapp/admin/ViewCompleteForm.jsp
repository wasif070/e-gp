<%--
    Document   : CreateForm
    Created on : 24-Oct-2010, 4:49:09 PM
    Author     : yanki
--%>

<%@page import="com.cptu.egp.eps.model.table.TblListBoxDetail"%>
<%@page import="com.cptu.egp.eps.model.table.TblListBoxMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ComboSrBean"%>
<%@page import="com.cptu.egp.eps.model.table.TblListCellDetail"%>
<%@page import="java.util.ArrayList"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="frmView"  class="com.cptu.egp.eps.web.servicebean.TemplateTableSrBean" />
<jsp:useBean id="templateFrmSrBean" class="com.cptu.egp.eps.web.servicebean.TemplateFormSrBean" />
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateTables" %>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateColumns" %>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateCells" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");

            if(session.getAttribute("userId") != null){
                frmView.setLogUserId(session.getAttribute("userId").toString());
                templateFrmSrBean.setLogUserId(session.getAttribute("userId").toString());
            }

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View Form</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <%
                short templateId = 0;
                int sectionId = 0;
                int formId = 0;

                 String isPDF= "abc";
                            if(request.getParameter("isPDF") != null)
                                    isPDF= request.getParameter("isPDF");
                 
                if (request.getParameter("templateId") != null) {
                    templateId = Short.parseShort(request.getParameter("templateId"));
                }
                if (request.getParameter("sectionId") != null) {
                    sectionId = Integer.parseInt(request.getParameter("sectionId"));
                }
                if (request.getParameter("formId") != null) {
                    formId = Integer.parseInt(request.getParameter("formId"));
                }

                boolean isBOQForm = false;
                boolean isFromMultipleFilling = false;
                boolean isMandatory = false;
                isBOQForm = frmView.isPriceBidForm(formId);
                isFromMultipleFilling = frmView.isFromMultipleFilling(formId);
                isMandatory = frmView.isFormMandatory(formId);

                String frmName = "";
                StringBuffer frmHeader = new StringBuffer();
                StringBuffer frmFooter = new StringBuffer();
                
                List<com.cptu.egp.eps.model.table.TblTemplateSectionForm> frm = templateFrmSrBean.getFormDetail(formId);
                if (frm != null) {
                    if (frm.size() > 0) {
                        frmName = frm.get(0).getFormName();
                        frmHeader.append(frm.get(0).getFormHeader());
                        frmFooter.append(frm.get(0).getFormFooter());
                    }
                    frm = null;
                }
    %>
    <body>
        <div class="dashboard_div">
            <% if(!(isPDF.equalsIgnoreCase("true")) ){  %>
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <% } %>
            <div class="contentArea_1">
            <!--Middle Content Table Start-->
                <div class="pageHead_1">
                    View Form
                    <% if(!(isPDF.equalsIgnoreCase("true")) ){  %>
                    <span style="float: right; text-align: right;">
                        <a class="action-button-goback" href="DefineSTDInDtl.jsp?templateId=<%= templateId %>" title="SBD Dashboard">SBD Dashboard</a>
                    </span>
                    <% } %>
                </div>
                <table width="100%" cellspacing="0" class="t_space">
                    <tr>
                        <td width="205" class="ff">Form to be filled multiple time : </td>
                        <td>
                            <%
                                if(isFromMultipleFilling){
                                    out.print("Yes");
                                }else{
                                    out.print("No");
                                }
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td width="205" class="ff">Is Price Schedule / BOQ Form? : </td>
                        <td>
                            <%
                                if(isBOQForm){
                                    out.print("Yes");
                                }else{
                                    out.print("No");
                                }
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td width="205" class="ff">Is Form Mandatory? : </td>
                        <td>
                            <%
                                if(isMandatory){
                                    out.print("Yes");
                                }else{
                                    out.print("No");
                                }
                            %>
                        </td>
                    </tr>
                </table>
                <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                    <tr>
                        <td width="100" class="ff">Form Name : </td>
                        <td style="font-weight: normal;"><%=frmName %></td>
                    </tr>
                </table>
                <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                    <tr>
                        <td width="100" class="ff">Form Header : </td>
                        <td style="font-weight: normal; line-height: 1.75"><%=frmHeader %></td>
                    </tr>
                </table>
                    <%
                        List<TblTemplateTables> tblTemplateTables = frmView.getTemplateTables(templateId, sectionId, formId);
                        for(TblTemplateTables tbl : tblTemplateTables){
                            java.util.List<com.cptu.egp.eps.model.table.TblTemplateTables> tblInfo = frmView.getTemplateTablesDetail(tbl.getTableId());
                            short cols = 0;
                            short rows = 0;
                            String tableName = "";
                            String tableHeader = "";
                            String tableFooter = "";
                            if (tblInfo != null) {
                                if (tblInfo.size() >= 0) {
                                    tableName = tblInfo.get(0).getTableName();
                                    tableHeader = tblInfo.get(0).getTableHeader();
                                    tableFooter = tblInfo.get(0).getTableFooter();
                                    
                                    cols = tblInfo.get(0).getNoOfCols();
                                    rows = tblInfo.get(0).getNoOfRows();
                                }
                                tblInfo = null;
                            }
                            cols = frmView.getNoOfColsInTable(tbl.getTableId());
                            rows = frmView.getNoOfRowsInTable(tbl.getTableId(), (short) 1);

                            java.util.ListIterator<com.cptu.egp.eps.model.table.TblTemplateColumns> tblColumnsDtl = frmView.getColumnsDtls(tbl.getTableId(), true).listIterator();
                            java.util.ListIterator<com.cptu.egp.eps.model.table.TblTemplateCells> tblCellsDtl = frmView.getCellsDtls(tbl.getTableId()).listIterator();

                            String colHeader = "";
                            byte filledBy = 0;
                            byte dataType = 0;
                            byte showOrHide = 1;
                            String colType = "";
                            int colId = 0;
                            
                            short fillBy[] = new short[cols];
                            short arrDataType[] = new short[cols];
                            int arrColId[] = new int[cols];
    %>
                            <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                                <tr>
                                    <td width="100" class="ff">Table Name : </td>
                                    <td style="font-weight: normal;"><%=tableName%></td>
                                </tr>
                            </table>
                                <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                                <tr>
                                    <td width="100" class="ff">Table Header : </td>
                                    <td style="font-weight: normal; line-height: 1.75"><%=tableHeader%></td>
                                </tr>
                            </table>
                                
                            <table width="100%" cellspacing="0" class="tableList_1 t_space" id="FormMatrix">
                                <tbody>
                                    <%
                                        boolean isTotFormulaCre = false;
                                        isTotFormulaCre = frmView.isTotalFormulaCreated(tbl.getTableId());
                                        String strDtType = "";
                                        for (short i = -1; i <= rows; i++) {
                                            if(i == 0){
                                    %>
                                     <tr id="ColumnRow">
                                    <%
                                                for(short j=0;j<cols;j++){
                                                    if(tblColumnsDtl.hasNext()){
                                                        TblTemplateColumns ttc = tblColumnsDtl.next();
                                                        colHeader = ttc.getColumnHeader();
                                                        colType = ttc.getColumnType();
                                                        filledBy = ttc.getFilledBy();
                                                        dataType = ttc.getDataType();
                                                        colId = ttc.getColumnId();
                                                        showOrHide = Byte.parseByte(ttc.getShoworhide());
                                                        ttc = null;
                                                    }
                                                    fillBy[j] = filledBy;
                                                    arrColId[j] = colId;
                                                    arrDataType[j] = dataType;
                                    %>
                                                <th id="addTD<%= j + 1 %>" <%if(false && showOrHide == 0){out.print(" style='display:none' ");}%> >
                                                    <%= colHeader %>
                                                </th>
                                    <%
                                                }
                                    %>
                                     </tr>
                                    <%
                                            }
                                            if(i > 0){
                                                if(i == rows && isTotFormulaCre){
                                                    java.util.HashMap<Integer, Integer> hmGTCols = frmView.getGTColumns(tbl.getTableId());
                                    %>
                                                    <tr id="TR<%=i%>">
                                    <%
                                                            for(int j=0; j<cols; j++){
                                                        %>
                                                        <td id="TD<%= i %>_<%= j + 1 %>" align="center">
                                                        <%
                                                        if(hmGTCols != null){
                                                            if(hmGTCols.containsValue(arrColId[j])){
                                                                out.print("<b>Total Formula</b>");
                                                            }else{
                                                                out.print("");
                                                            }
                                                        }
                                                        %>
                                                        </td>
                                                        <%
                                                            }
                                                        %>
                                                    </tr>
                                    <%
                                                }else{
                                    %>
                                        <tr id="TR<%=i%>">

                                            <%
                                                int cnt = 0;
                                                short columnId;
                                                int cellId = 0;
                                                List<TblListCellDetail> listCellDetail = new ArrayList<TblListCellDetail>();
                                                 
                                                for(int j=0; j<cols; j++){
                                                        String cellValue = "";
                                                        strDtType = "";
                                                        if(tblCellsDtl.hasNext()){
                                                            cnt++;
                                                            TblTemplateCells cells = tblCellsDtl.next();
                                            %>
                                                <td id="TD<%= i %>_<%= j + 1 %>" align="center" <%if(false && cells.getShowOrHide() == 0){out.print(" style='display:none' ");}%> >
                                            <%
                                                            dataType = cells.getCellDatatype();
                                                            filledBy = cells.getCellDatatype();
                                                            cellValue = cells.getCellvalue();
                                                            columnId =  cells.getColumnId();
                                                            cellId  = cells.getCellId();
                                                            listCellDetail = frmView.getListCellDetail(tbl.getTableId(),columnId,cellId);
                                                
                                                            if(dataType == 1){strDtType = "Small Text";}
                                                            if(dataType == 2){strDtType = "Long Text";}
                                                            if(dataType == 3){strDtType = "Money Positive";}
                                                            if(dataType == 8){strDtType = "Money All";}
                                                            if(dataType == 6){strDtType = "";}
                                                            if(dataType == 4){strDtType = "Numeric";}
                                                            if(dataType == 9){strDtType = "Combo Box with Calculation";}
                                                            if(dataType == 10){strDtType = "Combo Box w/o Calculation";}
                                                            if(dataType == 11){strDtType = "Money All (-5 to +5)";}
                                                            if(dataType == 12){strDtType = "Date";}
                                                            if(dataType == 13){strDtType = "Money Positive(3 digits after decimal)";}

                                                            if(fillBy[j] == 2){
                                                                out.print("Fill By Bidder/Consultant - " + strDtType);
                                                            }else if(fillBy[j] == 1){
                                                                out.print(cellValue);
                                                            }else{
                                                                out.print("Auto - "+ strDtType);
                                                            }
                                                             if(dataType==9 || dataType==10){
                                                                ComboSrBean cmbSrBean = new ComboSrBean();
                                                                if(listCellDetail.size() > 0){
                                                                TblListBoxMaster tblListBoxMaster = listCellDetail.get(0).getTblListBoxMaster();
                                                                List<TblListBoxDetail> listBoxDetail = cmbSrBean.getListBoxDetail(tblListBoxMaster.getListBoxId());
                                                             %>
                                                             <select id="idcombodetail" name="namecombodetail<%=tblListBoxMaster.getListBoxId()%>" class="formTxtBox_1">
                                                                <option value="">--Select--</option>
                                                                <% for(TblListBoxDetail tblListBoxDetail:listBoxDetail){ %>
                                                                    <option value="<%=tblListBoxDetail.getItemValue()%>"><%=tblListBoxDetail.getItemText()%></option>
                                                                <% } %>
                                                            </select>
                                                             <%
                                                                }
                                                             }
                                             %>
                                                </td>
                                             <%
                                                            cells = null;
                                                        }
                                                }
                                            %>
                                        </tr>
                                    <%
                                                }
                                            }
                                        }

                                        if(isBOQForm){
                                    %>
                                            <tr id="TRrowId">
                                    <%
                                            
                                            for(int j=0; j<cols; j++){
                                                String strDType = "";
                                    %>
                                                <td id="TDrowId_<%= j + 1 %>" align="center">
                                    <%
                                                if(arrDataType[j] == 1){strDType = "Small Text";}
                                                if(arrDataType[j] == 2){strDType = "Long Text";}
                                                if(arrDataType[j] == 3){strDType = "Money Positive";}
                                                if(arrDataType[j] == 8){strDType = "Money All";}
                                                if(arrDataType[j] == 4){strDType = "Numeric";}
                                                if(arrDataType[j] == 11){strDType = "Money All (-5 to +5";}
                                                if(arrDataType[j] == 12){strDType = "Date";}
                                                if(arrDataType[j] == 13){strDType = "Money Positive(3 digits after decimal)";}
                                                if(fillBy[j] == 2){
                                                    out.print("Fill By Bidder/Consultant - " + strDType);
                                                }else if(fillBy[j] == 1){
                                                    out.print("Fill By PA - " + strDType);
                                                }else{
                                                    out.print("Auto - "+ strDType);
                                                }
                                    %>
                                                </td>
                                    <%
                                        }
                                    %>
                                            </tr>
                                    <%
                                        }
                                    %>
                                </tbody>
                            </table>
                           <%-- <jsp:include page="ViewTableMatrix.jsp">
                                <jsp:param name="templateId" value="<%= templateId %>" />
                                <jsp:param name="sectionId" value="<%= sectionId %>" />
                                <jsp:param name="formId" value="<%= formId %>" />
                                <jsp:param name="tableId" value="<%= tbl.getTableId() %>" />
                            </jsp:include>--%>
                    <%
                            tbl = null;
                            if (tblColumnsDtl != null) {
                                tblColumnsDtl = null;
                            }
                            if (tblCellsDtl != null) {
                                tblCellsDtl = null;
                            }
                    %>
                            <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                                <tr>
                                    <td width="100" class="ff">Table Footer : </td>
                                    <td style="font-weight: normal; line-height: 1.75"><%=tableFooter%></td>
                                </tr>
                            </table>
                    <%
                        }
                    %>
                <table width="100%" cellspacing="0" class="tableHead_1 t_space">
                    <tr>
                        <td width="100" class="ff">Form Footer : </td>
                        <td style="font-weight: normal; line-height: 1.75"><%= frmFooter %></td>
                    </tr>
                </table>
                </div>
            <!--Middle Content Table End-->
            <% if(!(isPDF.equalsIgnoreCase("true")) ){  %>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <% } %>
        </div>
        <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
    <%
    if (tblTemplateTables != null) {
        tblTemplateTables = null;
    }
    if (frmView != null) {
        frmView = null;
    }
    if (templateFrmSrBean != null) {
        templateFrmSrBean = null;
    }
    %>
</html>
