<%-- 
    Document   : PEAdminGrid
    Created on : Nov 1, 2010, 8:37:28 PM
    Author     : rishita
--%>

<%-- Search Formation changed by Emtaz on 15/May/2016 --%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PA Admin</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="<%=request.getContextPath()%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        
    </head>
    <%
                //String msg = "";
                byte userTId = 0;
                if (!request.getParameter("userTypeid").equals("")) {
                    userTId = Byte.parseByte(request.getParameter("userTypeid"));
                }

//                int userid = 0;
  //              HttpSession hs = request.getSession();
    //            if (hs.getAttribute("userId") != null) {
      //            userid = Integer.parseInt(hs.getAttribute("userId").toString());
       //         }
                
    %>
    <script type="text/javascript">
        /*jQuery().ready(function (){
            jQuery("#list").jqGrid({
                url:'<%=request.getContextPath()%>/OrganizationAdminGrid?q=1&action=fetchData&userTypeId=<%=userTId%>',
                datatype: "xml",
                height: 250,
                colNames:['S. No.','e-mail ID',"Full Name","Office Name","Action"],
                colModel:[
                    {name:'srno',index:'srno', width:7,sortable:false,search: false, align:'center'},
                    {name:'tlm.emailId',index:'tlm.emailId', width:40,sortable:true, searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'tam.fullName',index:'tam.fullName', width:38,sortable:true, searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'om.officeName',index:'om.officeName', width:40,sortable:true, searchoptions: { sopt: ['eq', 'cn'] }},
                    {name:'Action',index:'Action', width:40,sortable:false, search: false,align:'center'}
                ],
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum:10,
                rowList:[10,20,30],
                pager: $("#page"),
                caption: "PE Admin Users",
                gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
            }).navGrid('#page',{edit:false,add:false,del:false});
        });*/
    </script>
    <script type="text/javascript">
        function chkdisble(pageNo){
            $('#dispPage').val(Number(pageNo));
            if(parseInt($('#pageNo').val(), 10) != 1){
                $('#btnFirst').removeAttr("disabled");
                $('#btnFirst').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnFirst').attr("disabled", "true");
                $('#btnFirst').css('color', 'gray');
            }


            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnPrevious').attr("disabled", "true")
                $('#btnPrevious').css('color', 'gray');
            }

            if(parseInt($('#pageNo').val(), 10) > 1){
                $('#btnPrevious').removeAttr("disabled");
                $('#btnPrevious').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnLast').attr("disabled", "true");
                $('#btnLast').css('color', 'gray');
            }

            else{
                $('#btnLast').removeAttr("disabled");
                $('#btnLast').css('color', '#333');
            }
            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnNext').attr("disabled", true)
                $('#btnNext').css('color', 'gray');
            }
            else{
                $('#btnNext').removeAttr("disabled");
                $('#btnNext').css('color', '#333');
            }
        }
    </script>

    <script type="text/javascript">
        $(function() {
            $('#btnFirst').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);

                if(totalPages>0 && $('#pageNo').val()!="1")
                {
                    $('#pageNo').val("1");
                    loadTable();
                    $('#dispPage').val("1");
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnLast').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);
                if(totalPages>0 && $('#pageNo').val()!=totalPages)
                {
                    $('#pageNo').val(totalPages);
                    loadTable();
                    $('#dispPage').val(totalPages);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnNext').click(function() {
                var pageNo=parseInt($('#pageNo').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo < totalPages) {
                    $('#pageNo').val(Number(pageNo)+1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)+1);
                    $('#btnPrevious').removeAttr("disabled");
                }
            });
        }); 

    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnPrevious').click(function() {
                var pageNo=$('#pageNo').val();

                if(parseInt(pageNo, 10) > 1)
                {
                    $('#pageNo').val(Number(pageNo) - 1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo) - 1);
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnGoto').click(function() {
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                    }
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(function() {
            $('#btnSearch').click(function() {
                count=0;
                if(document.getElementById('OfficeName') !=null && document.getElementById('OfficeName').value !=''){
                    count = 1;
                }else if(document.getElementById('EmailID') !=null && document.getElementById('EmailID').value !=''){
                    count = 1;
                }else if(document.getElementById('FullName') !=null && document.getElementById('FullName').value !=''){
                    count = 1;
                }

                if(count == 0){
                    $('#valMsg').html('Please enter at least One search criteria');
                }else{
                    $('#valMsg').html('');
                    $('#pageNo').val('1');
                    loadTable();
                }
            });
            $('#btnReset').click(function() {
               $("#pageNo").val("1");
                $("#EmailID").val('');
                $("#FullName").val('');
                $("#OfficeName").val('');
                $('#valMsg').html('');
                loadTable();
            });
        });
        function loadTable()
        {
            $.post("<%=request.getContextPath()%>/OrganizationAdminGrid", {
                    action: "fetchData",
                    userTypeId: $("#UserTypeId").val(),
                    Organization: "",
                    EmailID: $("#EmailID").val(),
                    FullName: $("#FullName").val(), 
                    OfficeName: $("#OfficeName").val(), 
                    pageNo: $("#pageNo").val(),
                    size: $("#size").val()
                },  function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);
                sortTable();
                //sortTable();
                if($('#noRecordFound').attr('value') == "noRecordFound"){
                    $('#pagination').hide();
                }else{
                    $('#pagination').show();
                }
                if($("#totalPages").val() == 1){
                    $('#btnNext').attr("disabled", "true");
                    $('#btnLast').attr("disabled", "true");
                }else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnLast').removeAttr("disabled");
                }
                $("#pageNoTot").html($("#pageNo").val());
                chkdisble($("#pageNo").val());
                $("#pageTot").html($("#totalPages").val());
            });
        }
    </script>      

    <body onload="loadTable();hide();">
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("hdnUserType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                            userType.append(request.getParameter("hdnUserType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <td class="contentArea">

                            <div class="pageHead_1">View Procuring Agency (PA) Admins
                            <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('4');">Save as PDF</a></span>
                            </div>
                            <div class="t_space">
                                <%if ("y".equals(request.getParameter("succFlag"))) {
                                            if ("approved".equalsIgnoreCase(request.getParameter("userStatus"))) {%>
                            <div id="succMsg" class="responseMsg successMsg">Admin Activated Successfully.</div>
                            <%} else {%>
                            <div id="succMsg" class="responseMsg successMsg">Admin Deactivated Successfully.</div>
                            <%}
                                        }%>
                                <!--Code added by Palash, Dohatec-->
                                        <%if ("pass".equals(request.getParameter("msg"))) {
                                            %>
                            <div id="succMsg" class="responseMsg successMsg">PA Admin Replaced Successfully</div>
                            <%}%>
                             <!--Code added by Palash, Dohatec-->


                            <div class="t-align-left ff formStyle_1">To sort click on the relevant column header</div>
                            <div class="formBg_1">
                                <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
                                <table id="tblSearchBox" cellspacing="10" class="formStyle_1" width="100%">

                                    <tr>
                                        <td class="ff">Email ID : </td>
                                        <td><input type="text" class="formTxtBox_1" id="EmailID" style="width:202px;" /></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Full Name : </td>
                                        <td><input type="text" class="formTxtBox_1" id="FullName" style="width:202px;" /></td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Office Name : </td>
                                        <td><input type="text" class="formTxtBox_1" id="OfficeName" style="width:202px;" /></td>
                                    </tr>
                                   
                                    <tr>
                                        <td class="ff">&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td class="ff">&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="t-align-center">
                                            <label class="formBtn_1"><input type="button" tabindex="6" name="search" id="btnSearch" value="Search"/>
                                            </label>&nbsp;&nbsp;
                                            <label class="formBtn_1"><input type="button" tabindex="7" name="reset" id="btnReset" value="Reset"/>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="t-align-center">
                                            <div class="reqF_1" id="valMsg"></div>
                                        </td>
                                    </tr>
                                </table>

                            </div>  
                            <div class="tableHead_1 t_space">
                                Search Result
                            </div>
                    
                            <div class="tabPanelArea_1">
                                <table width="100%" cellspacing="0" class="tableList_1" id="resultTable" cols="@0,4">
                                    <tr>
                                        <th width="4%" class="t-align-center">
                                            Sl. <br/>
                                            No.
                                        </th>
                                        <th width="31%" class="t-align-center">
                                            Email ID                              
                                        </th>
                                        <th width="25%" class="t-align-center">
                                            Full Name
                                        </th>
                                        <th width="20%" class="t-align-center">
                                            Office Name
                                        </th>
                                        <th width="20%" class="t-align-center">
                                            Action
                                        </th>
                                    </tr>
                                </table>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
                                    <tr>
                                        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span></td>
                                        <td align="center"><input name="textfield3" onkeypress="checkKeyGoTo(event);" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input   type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                        </label></td>
                                        <td  class="prevNext-container">
                                            <ul>
                                                <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                                                <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                                                <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                                                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                <div align="center">

                                    <input type="hidden" id="pageNo" value="1"/>
                                    <input type="hidden" name="size" id="size" value="10"/>
                                    <input type="hidden" name="UserTypeId" id="UserTypeId" value=<%=userTId%> />
                                    
                                </div>
                            </div>
                                <!-- <table id="list"></table>
                                <div id="page">

                                </div> -->
                            </div>
                        </td>
                    </tr>
                </table>
                <!--For Generate PDF  Starts-->
                <form id="formstyle" action="" method="post" name="formstyle">
                    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                    <%
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                        String appenddate = dateFormat1.format(new Date());
                    %>
                    <input type="hidden" name="fileName" id="fileName" value="PEAdmins_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="PEAdmins" />
                </form>
                <!--For Generate PDF  Ends-->
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var obj = document.getElementById('lblPEAdminView');
                if(obj != null){
                    if(obj.innerHTML == 'View PA Admins'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
    <script type="text/javascript"> 
    
    var Interval;
    clearInterval(Interval);
    Interval = setInterval(function(){
        var PageNo = document.getElementById('pageNo').value;
        var Size = document.getElementById('size').value;
        CorrectSerialNumber(PageNo-1,Size); 
    }, 100);
    
    function checkKey(e)
    {
        var keyValue = (window.event)? e.keyCode : e.which;
        if(keyValue == 13){
            //Validate();
            $('#pageNo').val('1')
            $('#btnSearch').click();
            //loadTable();
        }
    }
    function showHide()
        {
            if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
                document.getElementById('tblSearchBox').style.display = 'table';
                document.getElementById('collExp').innerHTML = '- Advanced Search';
            }else{
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Advanced Search';
            }
        }
        function hide()
        {
            document.getElementById('tblSearchBox').style.display = 'none';
            document.getElementById('collExp').innerHTML = '+ Advanced Search';
        }
        
        
    function checkKeyGoTo(e)
    {
        var keyValue = (window.event)? e.keyCode : e.which;
        if(keyValue == 13){
            //Validate();
            $(function() {
            //$('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            //loadTable();
                            $('#btnGoto').click();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            //});
        }
    }
    </script>
</html>
