<%--
    Document   : ViewSbBranchAdmin
    Created on : Apr 5, 2011, 3:02:37 PM
    Author     : nishit
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="scBankPartnerAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ScBankPartnerAdminSrBean"/>
    <jsp:useBean id="partnerAdminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.PartnerAdminMasterDtBean"/>
    <jsp:setProperty name="partnerAdminMasterDtBean" property="*"/>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <%
                    byte usrTypeId = 0;
                    String userSetTypeId = request.getParameter("userTypeId");
                    if (!request.getParameter("userTypeId").equals("")) {
                        usrTypeId = Byte.parseByte(request.getParameter("userTypeId"));
                    }
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        scBankPartnerAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                    }
                    String usrId = request.getParameter("userId");
                    if (usrId != null && !usrId.equals("")) {
                        partnerAdminMasterDtBean.setUserId(Integer.parseInt(usrId));
                        partnerAdminMasterDtBean.setUserTypeId(usrTypeId);
                        partnerAdminMasterDtBean.populateInfo();
                    }

                    String from = request.getParameter("from");
                    if (from == null || from.equals("")) {
                        from = "Operation";
                    }
                    String partnerType = request.getParameter("partnerType");
                    String type = "";
                    String title = "";
                    String title1 = "";

                    if (partnerType != null && partnerType.equals("Development")) {
                        type = "Development";
                        title = "Development Partner";
                        title1 = "Development Partner";

                    } else {
                        type = "ScheduleBank";
                        title = "Financial Institution";
                        title1 = "Financial Institution";

                    }
                    String ValueButton = "";
                    String dispstatus = "";
                    String var = "approved";
                    String status = request.getParameter("status");
                    if ("active".equalsIgnoreCase(status)) {
                        ValueButton = "Approved";
                        dispstatus = "Deactivated";
                        var = "deactive";
                        status = "Approved";
                    } else {
                        ValueButton = "Deactivate";
                        dispstatus = "Approved";
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <%if (!"15".equalsIgnoreCase(userSetTypeId)) {%>
        <title><%=title%>  Account Status</title>
        <%} else {%>
        <title><%=title%> Branch Account Status</title>
        <%}%>
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            function actionSubmit(){
                var comments = document.getElementById("comments").value;;
                if(comments.length == 0){
                    $('#passMsg').html('Please enter Comments');
                    $('#passMsg').show();
                    return false;
                }
                else if($('#comments').val().length>2000){
                    $('#passMsg').html('Allows maximum 2000 characters only');
                    $('#passMsg').show();
                    return false;
                }
                else{
                    jConfirm('Are you sure you want to <%=ValueButton%> this user?', '<%=type%> user', function (ans) {
                        if (ans)
                            document.frmSchedBankUser.submit();

                    });
                }
            }
        </script>
    </head>


    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">

                        <jsp:include page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                        <td class="contentArea_1">
                            <!--Page Content Start-->

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <!-- Success failure -->
                                    <!-- Result Display End-->

                                    <td>
                                        <%if ("15".equalsIgnoreCase(userSetTypeId)) {%>
                                        <div class="pageHead_1">Financial Institution Branch Admin</div>
                                        <%} else {%>
                                        <div class="pageHead_1"><%=title%> Account Status</div>
                                        <%}%>
                                        <div class="t_space">
                                        </div>
                                        <form method="POST" name ="frmSchedBankUser" id="frmSchedBankUser" action="<%=request.getContextPath()%>/AdminActDeactServlet">
                                            <input type="hidden" name="action" value="changestatus" />
                                            <input type="hidden" name="status" value="<%=status%>" />
                                            <input type="hidden" name="userid" value="<%=request.getParameter("userId")%>" />
                                            <input type="hidden" name="userTypeId" value="<%=request.getParameter("userTypeId")%>" />
                                            <input type="hidden" name="partnerType" value="<%=request.getParameter("partnerType")%>" />
                                            <input type="hidden" name="emailId" value="<%=partnerAdminMasterDtBean.getEmailId()%>" />
                                            <input type="hidden" name="fullName" value="<%=partnerAdminMasterDtBean.getFullName()%>" />
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td width="25%" class="ff"><%=title1%> Branch : </td>
                                                    <td width="75%">
                                                        <label id="bankName">${partnerAdminMasterDtBean.sbDevelopName}</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">e-mail ID : </td>
                                                    <td>
                                                        <label id="txtMail">${partnerAdminMasterDtBean.emailId}</label>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td class="ff">Full Name :</td>
                                                    <td><label id="txtName">${partnerAdminMasterDtBean.fullName}</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">CID No.: </td>
                                                    <td>
                                                        <label id="txtNationalId" >${partnerAdminMasterDtBean.nationalId}</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Mobile No. :</td>
                                                    <td>
                                                        <label id="txtMobileNo">${partnerAdminMasterDtBean.mobileNo}</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Status :</td>
                                                    <td>
                                                        <label id="txtMobileNo"><%=dispstatus%></label>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="ff">Comments : <span>*</span></td>
                                                    <td>
                                                        <textarea name="comments" cols=""  rows="5" style="width:50%;" class="formTxtBox_1" id="comments" ></textarea>
                                                        <span class="reqF_1" id="passMsg" style="display: none;"></span>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="ff">&nbsp;</td>
                                                    <td>
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnSubmit" id="btnSubmit" value="<%=ValueButton%>" onclick="return actionSubmit();"/>
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>

                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <%if ("15".equalsIgnoreCase(userSetTypeId)) {%>
        <script>
            var headSel_Obj = document.getElementById("headTabMyAcc");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
        <%} else {%>
        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
        <%}%>
    </body>
</html>
