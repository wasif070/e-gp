<%-- 
    Document   : editQuestion
    Created on : Jun 6, 2017, 10:29:20 AM
    Author     : feroz
--%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuestionModuleService"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuestionModule"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuizAnswer"%>
<%@page import="com.cptu.egp.eps.model.table.TblQuizQuestion"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuizAnswerService"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuizQuestionService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.BhutanDebarmentCommitteeService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.util.Calendar"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Edit Question</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
            
                $("#setQ").validate({
                    rules: {
                        quizQuestion:{required:true},
                        optA:{required:true},
                        optB:{required:true},
                        optC:{required:true},
                        optD:{required:true},
                        quizAnswer:{required:true}

                    },
                    messages: {
                        quizQuestion:{ required: "<div class='reqF_1'> Please Enter Question.</div>"},
                        
                        optA:{ required: "<div class='reqF_1'> Please Enter Option A.</div>"},
                        
                        optB:{ required: "<div class='reqF_1'> Please Enter Option B.</div>"},
                        
                        optC:{ required: "<div class='reqF_1'> Please Enter Option C.</div>"},
                        
                        optD:{ required: "<div class='reqF_1'> Please Enter Option D.</div>"},
                        
                        quizAnswer:{ required: "<div class='reqF_1'> Please Select Correct Answer.</div>"}
                    }
                }
            );
            });

        </script>
        <%
                    QuizQuestionService quizQuestionService = (QuizQuestionService) AppContext.getSpringBean("QuizQuestionService");
                    QuizAnswerService quizAnswerService = (QuizAnswerService) AppContext.getSpringBean("QuizAnswerService");
                    int createdBy = Integer.parseInt(session.getAttribute("userId").toString());
                    if ("Update".equals(request.getParameter("button"))) 
                    {
                        boolean doneFlag = false;
                        String action = "";
                        String quizQuestion = request.getParameter("quizQuestion");
                        int quizQuestionId = Integer.parseInt(request.getParameter("quizQuestionId"));
                        int optAId = Integer.parseInt(request.getParameter("optAId"));
                        int optBId = Integer.parseInt(request.getParameter("optBId"));
                        int optCId = Integer.parseInt(request.getParameter("optCId"));
                        int optDId = Integer.parseInt(request.getParameter("optDId"));
                        String optA = request.getParameter("optA");
                        String optB = request.getParameter("optB");
                        String optC = request.getParameter("optC");
                        String optD = request.getParameter("optD");
                        String quizAnswer = request.getParameter("quizAnswer");
                        String moduleId = request.getParameter("moduleId");
                        TblQuizQuestion tblQuizQuestionThis = new TblQuizQuestion();
                        TblQuizAnswer tblQuizAnswerThis1 = new TblQuizAnswer();
                        TblQuizAnswer tblQuizAnswerThis2 = new TblQuizAnswer();
                        TblQuizAnswer tblQuizAnswerThis3 = new TblQuizAnswer();
                        TblQuizAnswer tblQuizAnswerThis4 = new TblQuizAnswer();
                        TblQuestionModule tblQuestionModuleThis = new TblQuestionModule();
                        try{
                                tblQuizQuestionThis.setQuestionId(quizQuestionId);
                                tblQuestionModuleThis.setModuleId(Integer.parseInt(moduleId));
                                tblQuizQuestionThis.setQuestion(quizQuestion);
                                tblQuizQuestionThis.setTblQuestionModule(tblQuestionModuleThis);
                                
                                
                                tblQuizAnswerThis1.setAnswerId(optAId);
                                tblQuizAnswerThis1.setAnswer(optA);
                                if(quizAnswer.equals("A"))
                                {
                                    tblQuizAnswerThis1.setIsCorrect(true);
                                }
                                else
                                {
                                    tblQuizAnswerThis1.setIsCorrect(false);
                                }
                                tblQuizAnswerThis1.setTblQuizQuestion(tblQuizQuestionThis);
                                quizAnswerService.updateTblQuizAnswer(tblQuizAnswerThis1);
                                
                                tblQuizAnswerThis2.setAnswerId(optBId);
                                tblQuizAnswerThis2.setAnswer(optB);
                                if(quizAnswer.equals("B"))
                                {
                                    tblQuizAnswerThis2.setIsCorrect(true);
                                }
                                else
                                {
                                    tblQuizAnswerThis2.setIsCorrect(false);
                                }
                                tblQuizAnswerThis2.setTblQuizQuestion(tblQuizQuestionThis);
                                quizAnswerService.updateTblQuizAnswer(tblQuizAnswerThis2);
                                
                                tblQuizAnswerThis3.setAnswerId(optCId);
                                tblQuizAnswerThis3.setAnswer(optC);
                                if(quizAnswer.equals("C"))
                                {
                                    tblQuizAnswerThis3.setIsCorrect(true);
                                }
                                else
                                {
                                    tblQuizAnswerThis3.setIsCorrect(false);
                                }
                                tblQuizAnswerThis3.setTblQuizQuestion(tblQuizQuestionThis);
                                quizAnswerService.updateTblQuizAnswer(tblQuizAnswerThis3);
                                
                                tblQuizAnswerThis4.setAnswerId(optDId);
                                tblQuizAnswerThis4.setAnswer(optD);
                                if(quizAnswer.equals("D"))
                                {
                                    tblQuizAnswerThis4.setIsCorrect(true);
                                }
                                else
                                {
                                    tblQuizAnswerThis4.setIsCorrect(false);
                                }
                                tblQuizAnswerThis4.setTblQuizQuestion(tblQuizQuestionThis);
                                quizAnswerService.updateTblQuizAnswer(tblQuizAnswerThis4);
                                
                                quizQuestionService.updateTblQuizQuestion(tblQuizQuestionThis);
                                
                            }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                            }finally{
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                                action=null;
                            }
                        response.sendRedirect("moduleForViewQuestion.jsp?msg=edtS");
                    }
         %>
        
    </head>
    <body>
        <%
            if(request.getParameter("questionId")!=null)
            {
                    List<TblQuizQuestion> tblQuizQuestionEdit = quizQuestionService.findTblQuizQuestionByQid(Integer.parseInt(request.getParameter("questionId")));
                    List<TblQuizAnswer> tblQuizAnswerEdit = quizAnswerService.findTblQuizAnswer(Integer.parseInt(request.getParameter("questionId")));
                    
                    QuestionModuleService questionModuleService = (QuestionModuleService) AppContext.getSpringBean("QuestionModuleService");
                    List<TblQuestionModule> tblQuestionModule = questionModuleService.getAllTblQuestionModule();
                    
                    
                    

         %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
       
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea"><div class="pageHead_1">Edit Question</div>
                        <form id="setQ" name="setQ" action="editQuestion.jsp" method="post">
                        <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                            <tr>
                                <td style="font-style: italic" class="t-align-right" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <tr>
                                <td class="ff">Question : <span>*</span></td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="text" id="quizQuestion" name="quizQuestion" maxlength="450" value="<%=tblQuizQuestionEdit.get(0).getQuestion()%>">
                                    <input style="width:300px;" class="formTxtBox_1" type="hidden" id="quizQuestionId" name="quizQuestionId" maxlength="450" value="<%=tblQuizQuestionEdit.get(0).getQuestionId()%>">
                                    <%
                                        TblQuestionModule tblQuestionModuleThis = tblQuizQuestionEdit.get(0).getTblQuestionModule();
                                    %>
                                    <input style="width:300px;" class="formTxtBox_1" type="hidden" id="moduleId" name="moduleId" maxlength="450" value="<%=tblQuestionModuleThis.getModuleId()%>">
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Option A : <span>*</span></td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="text" id="optA" name="optA" maxlength="150" value="<%=tblQuizAnswerEdit.get(0).getAnswer()%>">
                                <input style="width:300px;" class="formTxtBox_1" type="hidden" id="optAId" name="optAId" maxlength="150" value="<%=tblQuizAnswerEdit.get(0).getAnswerId()%>">
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Option B : <span>*</span></td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="text" id="optB" name="optB" maxlength="150" value="<%=tblQuizAnswerEdit.get(1).getAnswer()%>">
                                <input style="width:300px;" class="formTxtBox_1" type="hidden" id="optBId" name="optBId" maxlength="150" value="<%=tblQuizAnswerEdit.get(1).getAnswerId()%>">
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Option C : <span>*</span></td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="text" id="optC" name="optC" maxlength="150" value="<%=tblQuizAnswerEdit.get(2).getAnswer()%>">
                                <input style="width:300px;" class="formTxtBox_1" type="hidden" id="optCId" name="optCId" maxlength="150" value="<%=tblQuizAnswerEdit.get(2).getAnswerId()%>">
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Option D : <span>*</span></td>
                                <td><input style="width:300px;" class="formTxtBox_1" type="text" id="optD" name="optD" maxlength="150" value="<%=tblQuizAnswerEdit.get(3).getAnswer()%>">
                                <input style="width:300px;" class="formTxtBox_1" type="hidden" id="optDId" name="optDId" maxlength="150" value="<%=tblQuizAnswerEdit.get(3).getAnswerId()%>">
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">Correct Answer : <span>*</span></td>
                                <td>
                                    <select style="width:150px;" class="formTxtBox_1" id="quizAnswer" name="quizAnswer">
                                        <option value="">--Select Answer--</option>
                                        <option value="A" <%if(tblQuizAnswerEdit.get(0).isIsCorrect()){out.print("selected");}%>>A</option>
                                        <option value="B" <%if(tblQuizAnswerEdit.get(1).isIsCorrect()){out.print("selected");}%>>B</option>
                                        <option value="C" <%if(tblQuizAnswerEdit.get(2).isIsCorrect()){out.print("selected");}%>>C</option>
                                        <option value="D" <%if(tblQuizAnswerEdit.get(3).isIsCorrect()){out.print("selected");}%>>D</option>
                                    </select>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" cellpadding="0" >
                                <tr>
                                    <td class="t-align-center" style="padding-right:110px;"><span class="formBtn_1"><input type="submit" name="button" id="button" value="Update" onclick="return validate();"/></span></td>
                                </tr>
                            </table>
                        </form>
                </td>
            </tr>
        </table>
                                    <%}%>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
</html>