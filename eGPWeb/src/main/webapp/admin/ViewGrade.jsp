<%-- 
    Document   : ViewGrade
    Created on : Nov 23, 2010, 11:18:12 AM
    Author     : Malhar
--%>

<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
            
             if(request.getParameter("action") != null && request.getParameter("action").equalsIgnoreCase("view") && request.getParameter("ok") == null)
            {
                 // Coad added by Dipal for Audit Trail Log.
                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                String idType="gradeId";
                int auditId=Integer.parseInt(request.getParameter("gradeId"));
                String auditAction="View Class Details";
                String moduleName=EgpModule.Manage_Users.getName();
                String remarks="";
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
            }

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Class Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" ></jsp:include>
                        <td class="contentArea_1">
                            <!-- Success failure -->
                            <%
                            String msg = request.getParameter("msg");
                            if (msg != null && msg.equalsIgnoreCase("success")) {
                                String from = request.getParameter("from");
                                if (from == null || from.equals("")) {
                                    from = "Operation";
                                }
                            %>
                            <div class="responseMsg successMsg">Class <%=from%> Successfully</div>
                    <%}%>
                    <!-- Result Display End-->
                    <jsp:useBean id="gradeDtBean" scope="page" class="com.cptu.egp.eps.web.databean.GradeDtBean"/>
                    <!--Page Content Start-->
                    <div class="t_space">
                        <div class="pageHead_1">View Class Details</div>
                        <div class="txt_1 c_t_space"><br />
                        </div>
                    </div>
                    
                    <%
                                String gradeId = request.getParameter("gradeId");
                                if (gradeId != null && !gradeId.isEmpty()) {
                                    gradeDtBean.populateInfo(Short.parseShort(gradeId));


                                    if (gradeDtBean.isDataExists()) {
                                        String editLink = "EditGrade.jsp?gradeId=" + gradeId;%>
                    <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                        <tr>
                            <td class="ff" width="25%">Class : </td>
                            <td width="75%"><label>${gradeDtBean.gradeName}</label></td>
                        </tr>
                        <tr>
                            <td class="ff">Grade :</td>
                            <td><label>Grade ${gradeDtBean.gradeLevel}</label></td>
                        </tr>
                        <%--<tr>
                            <td>&nbsp;</td>
                            <td>
                                <label class="formBtn_1">
                                    <input type="button" name="gradeEdit" id="btnEdit" value="Edit"/>
                                </label>
                                &nbsp;
                            </td>
                        </tr>--%>
                    </table>
                    <table width="100%" cellspacing="10" cellpadding="0" border="0">
                        <tr>
                            <td width="15%"></td>
                            <td width="85%" class="t-align-left" >
                               <a href="ManageGrade.jsp" class="anchorLink">Ok</a> &nbsp; <a href="<%=editLink%>" class="anchorLink">Edit</a>
                            </td>
                        </tr>
                    </table>
                    <%}
                                                        else {%>
                    <div class="responseMsg errorMsg">Data Not Found for Class: <%=gradeId%></div>
                    <%}
                                                    }
                                                    else {%>
                    <div class="responseMsg errorMsg">Plz provide Valid Class-Id</div>
                    <%}%>
                    <!--Page Content End-->
                    </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
