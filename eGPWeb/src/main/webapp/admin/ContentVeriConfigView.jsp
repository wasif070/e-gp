<%-- 
    Document   : ContentVeriConfigView
    Created on : Dec 5, 2010, 12:41:53 PM
    Author     : Ramesh.Janagondakuruba
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblContentVerification"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ContentVeriConfigSrBean" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
     <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Content Verification View</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<!--<script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
<%!
    ContentVeriConfigSrBean contentVeriConfigSrBean=new ContentVeriConfigSrBean();
%>
</head>
<body>
    <div class="dashboard_div">
 <!--Dashboard Header Start-->
  <div class="topHeader">
      <%@include file="../resources/common/AfterLoginTop.jsp" %>
  </div>
  <!--Dashboard Header End-->
   <!--Dashboard Content Part Start-->
  <div class="pageHead_1">Content Verification Configuration View</div>
  <div>&nbsp;</div>     
  <table width="100%" cellspacing="0" class="tableList_1 t_space" id="content" name="content">
      <tr>
          <th width="16%" class="t-align-center">Public Forum Verification Requires </th>
          <th width="18%" class="t-align-center">Ask Procurement Expert </th>
      </tr>
      <%
        List<TblContentVerification> lst=contentVeriConfigSrBean.getContentVerification();        
        String msg="";       
        if(!lst.isEmpty()){
           TblContentVerification conVeri=new TblContentVerification();
           Iterator it=lst.iterator();
           while(it.hasNext()){
               conVeri=(TblContentVerification)it.next();              
      %>
      <tr>
          <td  class="t-align-center"><%=conVeri.getIsPublicForum()%></td>
          <td  class="t-align-center"><%=conVeri.getIsProcureExpert()%></td>
      </tr>
      <%
      }
           }
        else{
            msg="NO Records Found in Data Base";
        }
      %>
  </table>
  <div class="pageHead_1" align="center"> <%=msg%></div>
  <!--Dashboard Footer Start-->
  <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
  <!--Dashboard Footer End-->
    </div>
  <script>
                var headSel_Obj = document.getElementById("headTabContent");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
</body>
</html>
