<%--
    Document   : CompanyVerification
    Created on : Nov 5, 2010, 11:40 PM
    Author     : taher
--%>

<%@page import="java.util.Arrays"%>
<%@page import="com.cptu.egp.eps.model.table.TblTendererAuditTrail"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="java.lang.String"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.cptu.egp.eps.model.table.TblMandatoryDoc"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.model.table.TblCompanyDocuments"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum" %>
<%@page import="com.cptu.egp.eps.model.table.TblTendererMaster"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility" %>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil" %>
<%@page  import="com.cptu.egp.eps.web.utility.XMLReader"%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Verification</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/colorbox.css" rel="stylesheet" type="text/css"/>
        <!--        <script type="../text/javascript" src="resources/js/pngFix.js"></script>-->
        
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="<%=request.getContextPath()%>/resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jquery.colorbox.js" type="text/javascript"></script>
        <jsp:useBean id="conAdminSrBean" class="com.cptu.egp.eps.web.servicebean.ConAdminSrBean"/>
        
        <script type="text/javascript">
            $(document).ready(function () {
                $("#frmComp").validate({
                    rules: {
                        comments: {required: true, maxlength: 1000}
                    },
                    messages: {
                        comments: {required: "<div class='reqF_1'>Please enter Comments.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 1000 Characters are allowed.</div>"}
                    }
                });
                
                $(".imageLink").colorbox({rel:'imageLink', opacity: 0.9, width:"90%" });
            });
        </script>
    </head>
    <body>
        <%
            HttpSession hs = request.getSession();
            Integer userTypeId1 = 0;
            if (hs.getAttribute("userTypeId1") != null) {
                userTypeId1 = Integer.parseInt(hs.getAttribute("userTypeId").toString());
            }
            String status="";
                 status   = request.getParameter("status");
            ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
            contentAdminService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
            UserRegisterService registerService = (UserRegisterService) AppContext.getSpringBean("UserRegisterService");
            TblTendererMaster tblTendererMaster = contentAdminService.findTblTendererMasters("tendererId", Operation_enum.EQ, Integer.parseInt(request.getParameter("tId"))).get(0);
            List<TblTendererAuditTrail>  tblTendererAuditTrailList = contentAdminService.findTblTendererAuditTrail("userId", Operation_enum.EQ, Integer.parseInt(request.getParameter("uId")));
            if ("Approve".equals(request.getParameter("btnApprove"))) {
                boolean exeIsAdmin = false;
                if (userTypeId1 == 8) {
                    exeIsAdmin = true;
                }
                String emailMobile = contentAdminService.changeUserStatus(Integer.parseInt(session.getAttribute("userId").toString()), Integer.parseInt(request.getParameter("uId")), "approved", request.getParameter("comments"), request.getParameter("cId"), exeIsAdmin);
                String mails[] = {emailMobile.substring(0, emailMobile.lastIndexOf("$"))};
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                MailContentUtility mailContentUtility = new MailContentUtility();
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                registerService.contentAdmMsgBox(mails[0], XMLReader.getMessage("msgboxfrom"), "Welcome to Electronic Government Procurement (e-GP) System.", msgBoxContentUtility.contentAdminApprove());
                String url = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/"));
                String mailText = mailContentUtility.contAdmAppRej("APPROVED", request.getParameter("comments"), tblTendererMaster.getFirstName(), url.substring(0, url.lastIndexOf("/")));
                sendMessageUtil.setEmailTo(mails);
                sendMessageUtil.setEmailFrom(XMLReader.getMessage("regEmailId"));
                sendMessageUtil.setEmailSub("Electronic Government Procurement (e-GP) System:  Bidder Registration - APPROVED");
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                sendMessageUtil.setSmsNo(emailMobile.substring(emailMobile.lastIndexOf("$") + 1, emailMobile.length()));
                sendMessageUtil.setSmsBody("Dear Bidder,%0AYour e-GP profile has been Approved by GPPMD.");
                sendMessageUtil.sendSMS();
                mails = null;
                sendMessageUtil = null;
                mailContentUtility = null;
                msgBoxContentUtility = null;
                response.sendRedirect("CompanyVerification.jsp?succ=a");
            } else if ("Notify for Modification".equals(request.getParameter("btnReject"))) {
                boolean exeIsAdmin = false;
                if (userTypeId1 == 8) {
                    exeIsAdmin = true;
                }
                String emailMobile = contentAdminService.rejectUser(Integer.parseInt(session.getAttribute("userId").toString()), Integer.parseInt(request.getParameter("uId")), "rejected", request.getParameter("comments"), request.getParameter("cId"), exeIsAdmin);
                String mails[] = {emailMobile.substring(0, emailMobile.lastIndexOf("$"))};
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                MailContentUtility mailContentUtility = new MailContentUtility();
                String url = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/"));
                String mailText = mailContentUtility.contAdmAppRej("MODIFICATION", request.getParameter("comments"), tblTendererMaster.getFirstName(), url.substring(0, url.lastIndexOf("/")));
                sendMessageUtil.setEmailTo(mails);
                sendMessageUtil.setEmailFrom(XMLReader.getMessage("regEmailId"));
                sendMessageUtil.setEmailSub("e-GP System:  Bidder Registration - NOTIFY FOR MODIFICATION");
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                sendMessageUtil.setSmsNo(emailMobile.substring(emailMobile.lastIndexOf("$") + 1, emailMobile.length()));
                sendMessageUtil.setSmsBody("Dear User,%0AYour e-GP profile has been notifies for modification by GPPMD.");
                sendMessageUtil.sendSMS();
                mails = null;
                sendMessageUtil = null;
                mailContentUtility = null;
                status="NotifyforModification";
                response.sendRedirect("CompanyVerification.jsp?succ=m");
            } else if ("Approve".equals(request.getParameter("btnApproveCategory"))) {
                boolean exeIsAdmin = false;
                if (userTypeId1 == 8) {
                    exeIsAdmin = true;
                }
                String emailMobile = contentAdminService.changeProcurementCategoryStatus(Integer.parseInt(session.getAttribute("userId").toString()), Integer.parseInt(request.getParameter("uId")), "approved", request.getParameter("comments"), request.getParameter("cId"), exeIsAdmin);
                String mails[] = {emailMobile.substring(0, emailMobile.lastIndexOf("$"))};
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                MailContentUtility mailContentUtility = new MailContentUtility();
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                registerService.contentAdmMsgBox(mails[0], XMLReader.getMessage("msgboxfrom"), "Welcome to Electronic Government Procurement (e-GP) System.", msgBoxContentUtility.contentAdminApprove());
                String url = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/"));
                String mailText = mailContentUtility.contAdmAppRej("New Procurement Category Approved", request.getParameter("comments"), tblTendererMaster.getFirstName(), url.substring(0, url.lastIndexOf("/")));
                sendMessageUtil.setEmailTo(mails);
                sendMessageUtil.setEmailFrom(XMLReader.getMessage("regEmailId"));
                sendMessageUtil.setEmailSub("Electronic Government Procurement (e-GP) System:  Bidder Request for New Procurement Category  - APPROVED");
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                sendMessageUtil.setSmsNo(emailMobile.substring(emailMobile.lastIndexOf("$") + 1, emailMobile.length()));
                sendMessageUtil.setSmsBody("Dear Bidder,%0AYour New Request for Procurement Category has been Approved by GPPMD.");
                sendMessageUtil.sendSMS();
                mails = null;
                sendMessageUtil = null;
                mailContentUtility = null;
                msgBoxContentUtility = null;
                response.sendRedirect("CompanyVerification.jsp?succ=n");
            } else if ("Reject".equals(request.getParameter("btnRejectCategory"))) {
                boolean exeIsAdmin = false;
                if (userTypeId1 == 8) {
                    exeIsAdmin = true;
                }
                String emailMobile = contentAdminService.changeProcurementCategoryStatus(Integer.parseInt(session.getAttribute("userId").toString()), Integer.parseInt(request.getParameter("uId")), "rejected", request.getParameter("comments"), request.getParameter("cId"), exeIsAdmin);
                String mails[] = {emailMobile.substring(0, emailMobile.lastIndexOf("$"))};
                SendMessageUtil sendMessageUtil = new SendMessageUtil();
                MailContentUtility mailContentUtility = new MailContentUtility();
                MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                registerService.contentAdmMsgBox(mails[0], XMLReader.getMessage("msgboxfrom"), "Welcome to Electronic Government Procurement (e-GP) System.", msgBoxContentUtility.contentAdminApprove());
                String url = request.getRequestURL().substring(0, request.getRequestURL().lastIndexOf("/"));
                String mailText = mailContentUtility.contAdmAppRej("REJECTED", request.getParameter("comments"), tblTendererMaster.getFirstName(),url.substring(0, url.lastIndexOf("/")));
                sendMessageUtil.setEmailTo(mails);
                sendMessageUtil.setEmailFrom(XMLReader.getMessage("regEmailId"));
                sendMessageUtil.setEmailSub("Electronic Government Procurement (e-GP) System:  Bidder Request for New Procurement Category  - REJECTED");
                sendMessageUtil.setEmailMessage(mailText);
                sendMessageUtil.sendEmail();
                sendMessageUtil.setSmsNo(emailMobile.substring(emailMobile.lastIndexOf("$") + 1, emailMobile.length()));
                sendMessageUtil.setSmsBody("Dear Bidder,%0AYour New Request for Procurement Category has been Rejected by GPPMD.");
                sendMessageUtil.sendSMS();
                mails = null;
                sendMessageUtil = null;
                mailContentUtility = null;
                msgBoxContentUtility = null;
                response.sendRedirect("CompanyVerification.jsp?succ=r");
            }
            java.util.List<TblCompanyDocuments> tblCompanyDocuments = new ArrayList<TblCompanyDocuments>();
//            if (request.getParameter("status") != null) {
//                if ("reapply".equalsIgnoreCase(request.getParameter("status").toString())) {
//                    tblCompanyDocuments = contentAdminService.findCompanyDocumentses("tblTendererMaster", Operation_enum.EQ, new TblTendererMaster(Integer.parseInt(request.getParameter("tId"))), "isReapplied", Operation_enum.EQ, 1);
//                }
//            } else {
                tblCompanyDocuments = contentAdminService.findCompanyDocumentses("tblTendererMaster", Operation_enum.EQ, new TblTendererMaster(Integer.parseInt(request.getParameter("tId"))), "documentTypeId", Operation_enum.NE, "briefcase");
//            }

            // Coad added by Dipal for Audit Trail Log.
            AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
            MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
            String idType = "userId";
            int auditId = Integer.parseInt(session.getAttribute("userId").toString());
            String auditAction = "View Tenderer Documents";
            String moduleName = EgpModule.New_User_Registration.getName();
            String remarks = "";
            makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="tableHead_1 t_space"><% if (tblTendererMaster.getTblLoginMaster().getRegistrationType().equals("individualconsultant")) {%> Individual Consultant Documents <% } else { %>Bidder/Consultant Documents<% } %></div>
                <jsp:include page="EditAdminNavigation.jsp" ></jsp:include><br/>
                    <table width="100%" cellspacing="0" class="tableList_1">
                        <tr>
                            <th style="width:4%;" class="t-align-left">Sl.  No.</th>
                            <th class="t-align-left">Document Name</th>
                            <th class="t-align-left">Document Description</th>
                            <th class="t-align-left">Document Reference No</th>
                            <th class="t-align-left">File Size (In KB)</th>
                            <th style="width:10%;"  class="t-align-left">Action</th>
                        </tr>
                    <%                        int count = 1;
                    List<TblMandatoryDoc> list=new ArrayList<TblMandatoryDoc>();
                        if (status != null) {
                            if ("reapply".equalsIgnoreCase(status)) {
                                 list = registerService.getMandatoryDocs(request.getParameter("uId"), false, true);
                            }
                            else if ("pending".equalsIgnoreCase(status) || status.isEmpty()) {
                                 list = registerService.getMandatoryDocs(request.getParameter("uId"), false, false);
                            }
                            else if ("NotifyforModification".equalsIgnoreCase(status)) {
                                 list = registerService.getMandatoryDocs(request.getParameter("uId"), true, false);
                            }
                        } else {
                             list = registerService.getMandatoryDocs(request.getParameter("uId"), false, false);
                        }
                        
                        if(list.size()!=0){
                         
                        String uid =  request.getParameter("uId"); 
                        
                        String docName = "", docSize = "", docTitle = "", filePath = "";                                                
                        
                        String []viewAbleFormats = {"jpg","pdf","png","jpeg","gif"} ;    
                                                                                                               
                        for (TblCompanyDocuments ttcd : tblCompanyDocuments) {
                            
                            docName = ttcd.getDocumentName();
                            docSize = ttcd.getDocumentSize();                            
                            
                            int j = docName.lastIndexOf('.');
                            String extension = docName.substring(j + 1);
                            
                            boolean fmCheck = false;
                            boolean isPdfFile = extension.equalsIgnoreCase("pdf");
                            
                            for(String s : viewAbleFormats){
                                if(s.equalsIgnoreCase(extension)){
                                    fmCheck = true;
                                    break;
                                }                            
                            }  
                            
                            if(fmCheck){
                                filePath =  conAdminSrBean.fileName(request, uid, docName, docSize);
                            } 
                    %>
                    <tr class="<%if (count % 2 == 0) {
                            out.print("bgColor-Green");
                        } else {
                            out.print("bgColor-white");
                        }%>">
                        <td class="t-align-center"><%=count%></td>
                        <td class="t-align-center"><%=ttcd.getDocumentName()%></td>
                        <td class="t-align-center"><%for (TblMandatoryDoc doc : list) {
                                if (doc.getDocTypeValue().equals(ttcd.getDocumentTypeId())) {
                                    out.print(doc.getDocType());
                                    docTitle = doc.getDocType();
                                }
                            }%></td>
                        <td class="t-align-center"><%if (ttcd.getDocRefNo() != null) {
                                out.print(ttcd.getDocRefNo());
                            }%></td>
                        <td class="t-align-center"><%DecimalFormat twoDForm = new DecimalFormat("#.##");
                            out.print(twoDForm.format(Double.parseDouble((ttcd.getDocumentSize())) / 1024));%></td>
                        <td  class="t-align-center">
                            
                             <% if( fmCheck && (filePath.length() > 0) ){
                                
                                    if(isPdfFile) {%>
                                    <a  href="<%=filePath%>" style="text-decoration: none;" title="<%=docTitle%>" target="_blank" >
                                        <img src="../resources/images/Dashboard/ViewFiles.png" height="20" width="20" alt="ViewFiles" />
                                    </a>
                                        
                                <% } else { %>
                                    <a class="imageLink" href="<%=filePath%>" style="text-decoration: none;" title="<%=docTitle%>" >
                                        <img src="../resources/images/Dashboard/ViewFiles.png" height="20" width="20" alt="ViewFiles" />
                                    </a>                                       
                                 <% } %>
                                  &nbsp; &nbsp;
                            <%  } %> 
                            <a href="<%=request.getContextPath()%>/SupportDocServlet?docName=<%=ttcd.getDocumentName()%>&docSize=<%=ttcd.getDocumentSize()%>&funName=download&iscontent=y&uId=<%=request.getParameter("uId")%>" title="Download"><img src="../resources/images/Dashboard/Download.png" alt="Download" /></a>
                        </td>
                    </tr>
                    <%
                            count++;
                        }}
                    %>
                </table>
                <br/>
                <div align="center" style="display: <%if ("a".equals(request.getParameter("s"))) {
                        out.print("none;");
                    } else {
                        out.print("block;");
                    }%>">
                    <form action="TendererDocs.jsp?uId=<%=request.getParameter("uId")%>&tId=<%=request.getParameter("tId")%>&cId=<%=request.getParameter("cId")%>" method="post" name="frmComp" id="frmComp">
                        <table width="100%" cellspacing="0" class="tableList_2">
                            
                            <%if (tblTendererAuditTrailList!=null && tblTendererAuditTrailList.size()!=0){%>
                            <tr>
                                <td width="18%" class="ff">Previous Rejected </br>Comments : </td>
                                <td width="82%">
                                    <% int slNo=0;
                                    for(int i = 0; i < tblTendererAuditTrailList.size(); i++) 
                                    {
                                        if("rejected".equalsIgnoreCase(tblTendererAuditTrailList.get(i).getActivity().substring(0,tblTendererAuditTrailList.get(i).getActivity().indexOf("@"))))
                                        {out.print("<b>"+(++slNo));
                                        out.print(". Reason : </b>");
                                        out.print(tblTendererAuditTrailList.get(i).getActivity().substring(tblTendererAuditTrailList.get(i).getActivity().indexOf("@")+1));
                                        out.print(" <b>Activity Date : </b>");
                                        out.print(DateUtils.gridDateToStr(tblTendererAuditTrailList.get(i).getActivityDate()));
                                        out.print("<br/>");
                                        out.print("<br/>");}
                                    }
                                    if(slNo==0)
                                    out.print("N/A");%>
                                </td>
                            </tr>
                            
                            <%}%>
                            <tr>
                                <td width="18%" class="ff">Comments : <span style="color: red;">*</span></td>
                                <td width="82%">
                                    <textarea name="comments" class="formTxtBox_1" style="width:98%" rows="3" cols="5"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <!-- approve or reject button for reapply category  -->
                                <% if (request.getParameter("status") != null) {
                                        if ("reapply".equalsIgnoreCase(request.getParameter("status").toString())) {%>
                                            <td align="left">
                                                <%--<label class="formBtn_1" style="visibility: <%if ("r".equals(request.getParameter("s")) || "p".equals(request.getParameter("s"))) {%>visible<%} else {%>collapse<%}%>">--%>
                                                <label class="formBtn_1" style="">
                                                    <input type="submit" name="btnApproveCategory" id="btnApprove" value="Approve" />
                                                </label>
                                                &nbsp;

                                                <label class="formBtn_1" style="">
                                                    <%--<label class="formBtn_1" style="visibility: <%if ("p".equals(request.getParameter("s"))) {%>visible<%} else {%>collapse<%}%>">--%>
                                                    <input type="submit" name="btnRejectCategory" id="btnReject" value="Reject" />
                                                </label>
                                            </td>
                                <%}
                                        else {%>
                                                <td align="left">
                                                    <label class="formBtn_1" style="visibility: <%if ("r".equals(request.getParameter("s")) || "p".equals(request.getParameter("s"))) {%>visible<%} else {%>collapse<%}%>">
                                                        <input type="submit" name="btnApprove" id="btnApprove" value="Approve" />
                                                    </label>
                                            &nbsp;
                                            <!--                                    To give Reject Link in approved tab keep this "a".equals(request.getParameter("s")) || -->
                                                    <label class="formBtn_1" style="visibility: <%if ("p".equals(request.getParameter("s"))) {%>visible<%} else {%>collapse<%}%>">
                                                        <input type="submit" name="btnReject" id="btnReject" value="Notify for Modification" />
                                                    </label>
                                                </td>

                                <%} }else { %>
                                <td align="left">
                                    <label class="formBtn_1" style="visibility: <%if ("r".equals(request.getParameter("s")) || "p".equals(request.getParameter("s"))) {%>visible<%} else {%>collapse<%}%>">
                                        <input type="submit" name="btnApprove" id="btnApprove" value="Approve" />
                                    </label>
                                    &nbsp;
                                    <!--                                    To give Reject Link in approved tab keep this "a".equals(request.getParameter("s")) || -->
                                    <label class="formBtn_1" style="visibility: <%if ("p".equals(request.getParameter("s"))) {%>visible<%} else {%>collapse<%}%>">
                                        <input type="submit" name="btnReject" id="btnReject" value="Notify for Modification" />
                                    </label>
                                </td>
                                <% }%>
                            </tr>
                        </table>
                    </form>
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script type="text/javascript">
            var headSel_Obj = document.getElementById("headTabCompVerify");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
