<%-- 
    Document   : GovReplaceUser
    Created on : Apr 15, 2011, 2:23:58 PM
    Author     : rishita,TaherT
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
 <!--Code added by Palash, Dohatec-->
<jsp:useBean id="adminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AdminMasterDtBean"/>
<!--End-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Transfer User</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
    </head>
    <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
                StringBuilder hdnUserType = new StringBuilder();
                if (request.getParameter("hdnUserType") != null) {
                    if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                        hdnUserType.append(request.getParameter("hdnUserType"));
                    } else {
                        hdnUserType.append("org");
                    }
                } else {
                    hdnUserType.append("org");
                }

                int empId = 0;
                if (request.getParameter("empId") != null && !"".equals(request.getParameter("empId"))) {
                    empId = Integer.parseInt(request.getParameter("empId"));
                }
                // <!--Code added by Palash, Dohatec-->
                int userTypeid = 0;
                if (request.getParameter("userTypeid") != null && !"".equals(request.getParameter("userTypeid"))) {
                    userTypeid = Integer.parseInt(request.getParameter("userTypeid"));
                }
                int adminUserId = 0;
                short adminId = 0;
                List<Object[]> listing = null;
                if (request.getParameter("adminUserId") != null) {
                    adminUserId = Integer.parseInt(request.getParameter("adminUserId").toString());

                }
                if (request.getParameter("adminId") != null) {
                    adminId = Short.parseShort(request.getParameter("adminId").toString());

                }//End
    %>
    <script type="text/javascript">
        $(function() {
            $('#Retrive').click(function() {
                $(".err").remove();
                if($.trim($('#txtEmailRetrive').val()) == $('#hdnEmailId').val()){
                    jAlert("New e-mail ID should be different from Existing User e-mail ID.","User Replace", function(RetVal) {});
                }else{
                    // Added by Palash, Dohatec
                    $.post("<%=request.getContextPath()%>/GovReplaceServlet", {emailId:$('#txtEmailRetrive').val(),funName:'retriveInfo',userTypeId:<%=userTypeid%>},  function(j){
                  //End
                  if(j.toString() != ''){
                            $('#newUserInfo').html(j.toString());
                        }else{//e-mail ID not found
                            $("#txtEmailRetrive").parent().append("<div class='err' style='color:red;'>e-mail ID not found</div>");
                            $('#txtFullName').val('');
//                            $('#txtNameBangla').val('');
                            $('#txtMobileNo').val('');
                            $('#txtNationalID').val('');
                        }
                    });
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#frmTransferUser").validate({
                rules: {
                    emailRetrive: {email: true },
                    fullName: {spacevalidate: true, requiredWithoutSpace: true , alphaName:true, maxlength:100 },
                    employeeID: {number: true, required: true},
                    nationalID: {/*required: true,*/ number: true,maxlength: 11,minlength: 11},
                    mobileNo: {required: true, number: true, minlength: 8, maxlength:8 },
                    comment: {spacevalidate: true, requiredWithoutSpace: true,maxlength:2000 },
//                    nameBangla:{ maxlength:100}

                },
                messages: {
                    emailRetrive: {
                        email: "<div id='e1' class='reqF_1'>Please enter valid e-mail ID</div>"
                    },
                    fullName: { spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                        requiredWithoutSpace: "<div class='reqF_1'>Please enter Full Name</div>",
                        alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only </div>",
                        maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"},
                    
                    employeeID: {
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" ,
                        required: "<div class='reqF_1'>Please enter Employee ID</div>"
                    },
                    nationalID: {
//                        required: "<div class='reqF_1'>Please enter CID No.</div>",
                        number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                        maxlength: "<div class='reqF_1'>CID No should comprise of maximum 11 digits</div>",
                        minlength: "<div class='reqF_1'>CID No should comprise of minimum 11 digits</div>"
                    },

                    mobileNo: {
                        required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                        number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" ,
                        minlength:"<div class='reqF_1'>Minimum 8 digits are required</div>",
                        maxlength:"<div class='reqF_1'>Allows maximum 8 digits only</div>"
                    },

                    comment:{
                        spacevalidate: "<div class='reqF_1'>Only space is not allowed</div>",
                        requiredWithoutSpace: "<div class='reqF_1'>Please enter Comments</div>",
                        maxlength:"<div class='reqF_1'>Allows maximum 2000 characters only</div>"
                    },
//                    nameBangla:{
//                        maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>"
//                    }
                },
                errorPlacement:function(error ,element)
                {
                    if(element.attr("name")=="emailRetrive")
                    {
                        error.insertAfter("#lblEmail");
                    }
                    else
                    {
                        error.insertAfter(element);

                    }
                }
            });
        });
    </script>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=hdnUserType%>" ></jsp:include>
                        <td class="contentArea">
                            <div class="pageHead_1">Transfer User :</div>
                            <%
                                        if ("fail".equals(request.getParameter("msg"))) {
                                            out.print("<div class='responseMsg errorMsg'>Problem in Transfering Employee</div>");
                                        }
                            %>
                            <div class="t_space"></div>
                            <!--Code added by Palash, Dohatec-->
                            <form action="<%=request.getContextPath()%>/GovReplaceServlet?funName=insertGov&empId=<%=empId%>&adminUserId=<%=adminUserId%>&adminId=<%=adminId%>" method="post" name="frmTransferUser" id="frmTransferUser">
                               <!--End-->
                                <div class="tableHead_1">Existing User :</div>
                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10" width="100%">
                                    <%
                                                TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
                                                String userId = "";
                                                
                                                if (session.getAttribute("userId") != null) {
                                                    userId = session.getAttribute("userId").toString();
                                                    transferEmployeeServiceImpl.setLogUserId(userId);
                                                }
                                                //<!--Code added by Palash, Dohatec-->
                                                if(empId !=0)
                                                    listing = transferEmployeeServiceImpl.getDetailsExistingUser(empId,0);
                                                else
                                                   listing = transferEmployeeServiceImpl.getDetailsExistingUser(0,adminUserId);
                                                  //End
                                                for (Object[] obj : listing) {
                                    %>
                                    <tr>
                                        <td class="ff" width="200">e-mail ID :</td>
                                        <td colspan="3"><%=obj[0]%>
                                            <input type="hidden" value="<%=obj[0]%>" id="hdnEmailId" name="hdnEmailId"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Full Name :</td>
                                        <td><%=obj[1]%>
                                            <input type="hidden" name="replacedBy" value="<%=obj[1]%>"/>
                                            <input type="hidden" name="hdnUserId" value="<%=obj[7]%>"/>
                                        </td>
                                        <!--Code added by Palash, Dohatec-->
                                        <%--<% if(empId !=0){%>
                                        <!--Code End by Palash, Dohatec-->
                                        <td class="ff" width="200">Name in Dzongkha :</td>
                                        <td><% if(obj[2] != null){                                             
                                                byte[] nameInBangla = (byte[]) obj[2];
                                                out.print(BanglaNameUtils.getUTFString(nameInBangla));}
                                            %></td> <%}%>--%>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">CID No. :</td>
                                        <td><%=obj[3]%></td>
                                        <td class="ff" width="200">Mobile No. :</td>
                                        <td><%=obj[4]%></td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Employee ID :</td>
                                        <td colspan="3"><%if(obj[8]!=null){out.print(obj[8]);}%>
                                            <input type="hidden" value="<%=obj[8]%>" id="hdnEmployeeId" name="hdnEmployeeId"/>
                                        </td>
                                    </tr>
                                     <!--Code added by Palash, Dohatec-->
                                    <% if(empId !=0){%> <tr>
                                        <td class="ff" width="200">Ministry / Division / Organization :</td>
                                         <!--Code added by Palash, Dohatec-->
                                    <%} else {%>
                                        <td class="ff" width="200">Office Name:</td>
                                        <%}%>
                                         <!--Code End by Palash, Dohatec-->
                                        <td colspan="3"><%=obj[5]%>
                                            <input type="hidden" value="<%=obj[5]%>" name="hdndeptName"/>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff" width="200">Contact Address :</td>
                                        <td colspan="3"><%if(obj[9]!=null){out.print(obj[9]);}%>
                                            <input type="hidden" value="<%=obj[9]%>" id="hdnEmployeeId" name="hdnEmployeeId"/>
                                        </td>
                                    </tr>

                                    <%//}%>
                                    <% }%>
                                     <% if(empId !=0){%>
                                    <tr>
                                        <td class="ff" width="200">PA Office Name :</td>
                                        <td colspan="3">
                                            <% for (Object office : transferEmployeeServiceImpl.getDetailsExistingUserOffice(empId)) {%>
                                            <!--                                            <input type="hidden" name="hdnofficeName" value="<-%=office%>"/>-->
                                            <%=office%><br/>
                                            
                                        </td>
                                    </tr>
                                    
                                     <tr>
                                        <td class="ff" width="200">Designation :</td>
                                        <% for (Object[] object : listing) {%>
                                        <td><%=object[6]%></td>
                                        <% }%>
                                        <td class="ff" width="200">Procurement Role :</td>
                                        <% for (Object pr : transferEmployeeServiceImpl.getDetailsExistingUserPR(empId)) {%>
                                        <td><%
                                            if(pr.equals("HOPE"))
                                                out.print("HOPA");
                                            else if(pr.equals("PE"))
                                            {
                                                out.print("PA");
                                            }
                                            else
                                                out.print(pr);                                      
                                            %></td>
                                        <% }%>
                                    </tr>



                                    <%}}%>
                                </table>
                                <div class="tableHead_1">New User :</div>
                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10" width="100%">
                                    <tr>
                                        <td class="ff" width="205">Search Existing Profile by e-mail ID : </td>
                                        <td>
                                            <input class="formTxtBox_1" id="txtEmailRetrive" name="emailRetrive" style="width: 200px;" maxlength="101" type="text"/>
                                            <label id="lblEmail" class="formBtn_1"><input id="Retrive" name="Retrive" value="Retrieve User Profile" type="button" /></label></td>
                                    </tr>
                                    <tbody id="newUserInfo">
                                        <tr>
                                            <td class="ff" width="200">Full Name : <span>*</span></td>
                                            <td><input class="formTxtBox_1" id="txtFullName" name="fullName" style="width: 200px;" maxlength="101" type="text"/></td>
                                        </tr>
                                        <%--<% if(empId !=0){%>
                                        <tr style="display: none">
                                            <td class="ff" width="200">Name in Dzongkha :</td>
                                            <td><input class="formTxtBox_1" id="txtNameBangla" name="nameBangla" style="width: 200px;" maxlength="101" type="text"/></td>
                                        </tr><%}%>--%>
                                        <tr>
                                            <td class="ff" width="200">CID No. : <span>*</span></td>
                                            <td><input class="formTxtBox_1" id="txtNationalID" name="nationalID" style="width: 200px;" maxlength="26" type="text"/></td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="200">Employee ID : <span>*</span></td>
                                            <td><input class="formTxtBox_1" id="txtEmployeeID" name="employeeID" style="width: 200px;" maxlength="26" type="text"/></td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="200">Mobile No. : <span>*</span></td>
                                            <td><input class="formTxtBox_1" id="txtMobileNo" name="mobileNo" style="width: 200px;" maxlength="16" type="text"/></td>
                                        </tr>
                                        <tr>
                                            <td class="ff" width="200">Contact Address :</td>
                                            <td><input class="formTxtBox_1" id="txtContactAddress" name="contactAddress" style="width: 200px;" maxlength="26" type="text"/></td>
                                        </tr>
                                    </tbody>
                                    <tr>
                                        <td class="ff" width="200">Comments : <span>*</span></td>
                                        <td>
                                            <textarea cols="5"  rows="5" class="formTxtBox_1" id="taComment" name="comment" style="width: 400px;" onkeypress="return imposeMaxLength(this, 2001);" onpaste="return imposeMaxLength(this, 2001);" ></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td align="left"><label class="formBtn_1">
                                                <input id="Replace" name="Replace" value="Transfer" type="submit" onclick="return showConfirmMsg();"/></label>
                                            <input type="hidden" name="hdnbutton" id="hdnbutton" value=""/>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script type="text/javascript">
            function showConfirmMsg(){
                var bVal = true;
                if($.trim($('#txtEmailRetrive').val()) == $('#hdnEmailId').val()){
                    jAlert("New e-mail ID should be different from Existing User e-mail ID.","User Replace", function(RetVal) {});
                    return false;
                    bVal = false;
                }else{
                    if($('#frmTransferUser').valid()){
                        vConfirmMsg = 'Are you sure that you want to transfer this user?';
                        vConfirmTitle = 'User Transfer';
                        if(confirm(vConfirmMsg,vConfirmTitle)){
                            return true;
                }else{
                    return false;
                }
                    }else {
                        return false;
                    }
                    }
            }
        </script>
    </body>
</html>
