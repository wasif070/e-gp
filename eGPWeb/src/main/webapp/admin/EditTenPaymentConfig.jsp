<%-- 
    Document   : EditTenPaymentConfig
    Created on : Apr 19, 2011, 6:53:49 PM
    Author     : TaherT
--%>

<%@page import="com.cptu.egp.eps.model.table.TblConfigDocFees"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenPaymentConfigService"%>
<%@page import="com.cptu.egp.eps.dao.generic.Operation_enum"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigEvalMethod"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.EvalMethodConfigService"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Document Fees, Tender Security and Performance Security, Tender Validity business rule configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/DefaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            function validate(){
                $(".err").remove();
                var tbol = true;
                    $.ajax({
                        type: "POST",
                        url: "<%=request.getContextPath()%>/TenPaymentConfigServlet",
                        data:"docFeesId="+$('#docFeesId').val()+"&"+"procType="+$('#procType_0').val()+"&"+"tendType="+$('#tendType_0').val()+"&"+"procMethod="+$('#procMethod_0').val()+"&"+"action=ajaxUniqueConfig",
                        async: false,
                        success: function(j){
                            if(j.toString()!="0"){
                                tbol=false;
                                jAlert("Duplicate record found. Please remove duplicate record and submit again.","Tender Payment Configuration", function(RetVal) {
                                });
                            }
                        }
                    });
                    return tbol;               
            }
            function changeReq(data1,data2){
                var cnt = document.getElementById(data2).options.length;
                if(data1.value=='No'){
                    for(var i=0;i<cnt;i++){
                        if(document.getElementById(data2).options[i].value=="Yes"){
                            document.getElementById(data2).options[i].disabled=true;
                        }else{
                            document.getElementById(data2).options[i].selected=true;
                        }
                    }
                }
                if(data1.value=='Yes'){
                    for(var i=0;i<cnt;i++){
                        if(document.getElementById(data2).options[i].value=="Yes"){
                            document.getElementById(data2).options[i].disabled=false;
                        }
                    }
                }
            }
        </script>
    </head>
    <body>
        <%
                    TenPaymentConfigService configService = (TenPaymentConfigService) AppContext.getSpringBean("TenPaymentConfigService");
                    configService.setLogUserId(session.getAttribute("userId").toString());
                    List<Object[]> procurementType = configService.getProcuremenTypes();
                    List<Object[]> tenderTypes = configService.getTenderTypes();
                    List<Object[]> procurementMethod = configService.getProcurementMethod();
                    List<TblConfigDocFees> getEvalMethods = configService.getTblConfigDocFees("docFeesId", Operation_enum.EQ, Integer.parseInt(request.getParameter("docFeesId")));
                    boolean isEdit = true;
                    if(getEvalMethods.isEmpty()){
                        isEdit=false;
                    }
        %>
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        <div class="tabPanelArea_1">            
                <table width="100%">
                    <tr>
                        <td valign="top">
                            <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        </td>
                        <td valign="top">
                        <form action="<%=request.getContextPath()%>/TenPaymentConfigServlet?action=editSingleEval" method="post" >
                        <div class="pageHead_1">Document Fees, Tender Security and Performance Security, Tender Validity business rule configuration</div>
                            <table class="tableList_1 t_space" cellspacing="0" width="100%" id="members">
                                <tbody id="tbodyData">
                                    <tr>
                                        <th class="t-align-center" width="15%">Tender Type<!--<br>(<span class="mandatory" style="color: red;">*</span>)--></th>
                                <th class="t-align-center" width="15%">Procurement Method<!--<br>(<span class="mandatory" style="color: red;">*</span>)--></th>
                                <th class="t-align-center" width="15%">Procurement Type<!--<br>(<span class="mandatory" style="color: red;">*</span>)--></th>
                                <th class="t-align-center" width="10%">Requires Document Fees<!--<br>(<span class="mandatory" style="color: red;">*</span>)--></th>
                                <th class="t-align-center" width="10%">Requires Tender Validity<!--<br>(<span class="mandatory" style="color: red;">*</span>)--></th>
                                <th class="t-align-center" width="10%">Requires Tender Security <!--<br>(<span class="mandatory" style="color: red;">*</span>)--></th>
                                <th class="t-align-center">Requires Performance Security<!--<br>(<span class="mandatory" style="color: red;">*</span>)--></th>
                                    </tr>
                                    <%
                                                int cnt = 0;
                                                do {
                                    %>
                                    <tr>                                        
                                        <td class="t-align-center">
                                            <select name="tendType" class="formTxtBox_1" id="tendType_<%=cnt%>">
                                                    <% for (Object[] proc : tenderTypes) {%>
                                                    <option value="<%=proc[0]%>" <%if (getEvalMethods.get(cnt).getTenderTypeId() == (Byte) proc[0]) {
                                                             out.print("selected");
                                                         }%>><%=proc[1]%></option>
                                                <%}%>
                                            </select>
                                        </td>
                                        <td class="t-align-center">
                                            <select name="procMethod" class="formTxtBox_1" id="procMethod_<%=cnt%>">
                                                <%for (Object[] proc : procurementMethod) {
                                                if("OTM".equalsIgnoreCase((String)proc[1]) ||
                                                           "LTM".equalsIgnoreCase((String)proc[1]) ||
                                                           "DP".equalsIgnoreCase((String)proc[1]) ||
                                                          // "LEQ".equalsIgnoreCase(tblProcurementMethod.getProcurementMethod()) ||
                                                         //  "TSTM".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                           "RFQ".equalsIgnoreCase((String)proc[1]) ||
                                                          // "RFQU".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                         //  "RFQL".equalsIgnoreCase(commonAppData.getFieldName2()) ||
                                                           "FC".equalsIgnoreCase((String)proc[1]) ||
                                                          // ("OSTETM".equalsIgnoreCase(commonAppData.getFieldName2()) && !"NCT".equalsIgnoreCase(procType)) ||
                                                           "DPM".equalsIgnoreCase((String)proc[1])||
                                                            "QCBS".equalsIgnoreCase((String)proc[1]) ||
                                                            "LCS".equalsIgnoreCase((String)proc[1]) ||
                                                            "SFB".equalsIgnoreCase((String)proc[1]) ||
                                                            "SBCQ".equalsIgnoreCase((String)proc[1]) ||
                                                            "SSS".equalsIgnoreCase((String)proc[1]) ||
                                                            "IC".equalsIgnoreCase((String)proc[1]))
                                                            {
                                                                if("RFQ".equalsIgnoreCase((String)proc[1]))
                                                                {%>
                                                                    <option value="<%=proc[0]%>" <%if (getEvalMethods.get(cnt).getProcurementMethodId() == (Byte) proc[0]) {
                                                                    out.print("selected");
                                                                    
                                                                   }%>>LEM</option>
                                                                  <%}
                                                                  else if("DPM".equalsIgnoreCase((String)proc[1]))
                                                                  {%>
                                                                  <option value="<%=proc[0]%>" <%if (getEvalMethods.get(cnt).getProcurementMethodId() == (Byte) proc[0]) {
                                                                    out.print("selected");
                                                                    
                                                                   }%>>DCM</option>
                                                                  <%}
                                                                  else
                                                                  {%>
                                                                  <option value="<%=proc[0]%>" <%if (getEvalMethods.get(cnt).getProcurementMethodId() == (Byte) proc[0]) {
                                                                    out.print("selected");
                                                                    
                                                                   }%>><%=proc[1]%></option>
                                                            <%}}}%>
                                            </select>
                                        </td>
                                        <td class="t-align-center">
                                            <input type="hidden" id="docFeesId" name="docFeesId" value="<%=getEvalMethods.get(cnt).getDocFeesId()%>">
                                            <select name="procType" class="formTxtBox_1" id="procType_<%=cnt%>">
                                                    <%for (Object[] proc : procurementType) {%>
                                                    <option value="<%=proc[0]%>" <%if (getEvalMethods.get(cnt).getProcurementTypeId() == (Byte) proc[0]) {
                                                            out.print("selected");
                                                        }%>><%
                                                           //ICT->ICB & NCT->NCB by Emtaz on 19/April/2016
                                                           if(proc[1].toString().equalsIgnoreCase("ICT"))
                                                           {out.print("ICB");}
                                                           else if(proc[1].toString().equalsIgnoreCase("NCT"))
                                                           {out.print("NCB");}
                                                        %></option>
                                                <%}%>
                                            </select>
                                        </td>
                                        <td class="t-align-center">
                                            <select name="isDocFeesReq" class="formTxtBox_1" id="isDocFeesReq_<%=cnt%>">
                                                <option value="Yes" <%if (isEdit) {if(getEvalMethods.get(cnt).getIsDocFeesReq().equals("Yes")) {out.print("selected");}}%>>Yes</option>
                                                <option value="No" <%if (isEdit) {if(getEvalMethods.get(cnt).getIsDocFeesReq().equals("No")) {out.print("selected");}}%>>No</option>
                                            </select>
                                        </td>
                                        <td class="t-align-center">
                                            <select name="isTenderValReq" class="formTxtBox_1" id="isTenderValReq_<%=cnt%>" onchange="changeReq(this,'isTenderSecReq_<%=cnt%>')">
                                                <option value="Yes"<%if (isEdit) {if(getEvalMethods.get(cnt).getIsTenderValReq().equals("Yes")) {out.print("selected");}}%>>Yes</option>
                                                <option value="No"<%if (isEdit) {if(getEvalMethods.get(cnt).getIsTenderValReq().equals("No")) {out.print("selected");}}%>>No</option>
                                            </select>
                                        </td>
                                        <td class="t-align-center">
                                            <select name="isTenderSecReq" class="formTxtBox_1" id="isTenderSecReq_<%=cnt%>">
                                                <option value="Yes" <%if (isEdit) {if(getEvalMethods.get(cnt).getIsTenderSecReq().equals("Yes")) {out.print("selected");}}%> <%if (isEdit) {if(getEvalMethods.get(cnt).getIsTenderValReq().equals("No")) {out.print("disabled");}}%>>Yes</option>
                                                <option value="No" <%if (isEdit) {if(getEvalMethods.get(cnt).getIsTenderSecReq().equals("No")) {out.print("selected");}}%>>No</option>
                                            </select>
                                        </td>                                                                                
                                        <td class="t-align-center">
                                            <select name="isPerSecFeesReq" class="formTxtBox_1" id="isPerSecFeesReq_<%=cnt%>">
                                                <option value="Yes" <%if (isEdit) {if(getEvalMethods.get(cnt).getIsPerSecFeesReq().equals("Yes")) {out.print("selected");}}%>>Yes</option>
                                                <option value="No" <%if (isEdit) {if(getEvalMethods.get(cnt).getIsPerSecFeesReq().equals("No")) {out.print("selected");}}%>>No</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <%cnt++;
                                                } while (cnt < getEvalMethods.size());%>
                                </tbody>
                                <tr>
                                    <td colspan="7" class="t-align-center"><span class="formBtn_1">

                                            <input name="button2" id="button2" value="Submit" type="submit" onclick="return validate();">
                                        </span></td>
                                </tr>
                            </table>
                            </form>
                        </td>
                    </tr>
                </table>            
        </div>
        <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
    </body>
</html>
