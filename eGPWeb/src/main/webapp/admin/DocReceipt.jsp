<%-- 
    Document   : DocReceipt
    Created on : Jan 27, 2011, 5:57:49 PM
    Author     : TaherT
--%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="com.cptu.egp.eps.model.table.TblLoginMaster"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ContentAdminService"%>
<%@page import="com.cptu.egp.eps.model.table.TblRegDocReceipt"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Receipt of Physical Documents Information</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <script type="text/javascript">
            function validate(){
                $(".err").remove();
                var bol=true;                                               
                if($.trim($("#trackNo").val()) == ""){
                    $("#trackNo").parent().append("<div class='err' style='color:red;'>Please enter Courier Tracking No.</div>");
                    bol= false;
                }else{
                    if($.trim($("#trackNo").val()).length>50){
                        $("#trackNo").parent().append("<div class='err' style='color:red;'>Maximum 50 characters are allowed.</div>");
                        bol= false;
                    }
                }
                if($('#receiptDt').val()==""){
                    $("#receiptDt").parent().append("<div class='err' style='color:red;'>Please select Date and time of Receipt of Documents</div>");
                     bol=false;
                }else{
                    if(CompareToForGreater($('#receiptDt').val(),$('#currDate').val())){
                        $("#receiptDt").parent().append("<div class='err' style='color:red;'>Date and time of Receipt of Documents must be smaller than the Current Date and Time</div>");
                        bol=false;
                    }
                }
                if($.trim($("#comments").val()) == ""){
                    $("#comments").parent().append("<div class='err' style='color:red;'>Please enter Comments</div>");
                    bol= false;
                }else{
                    if($.trim($("#comments").val()).length>2000){
                        $("#comments").parent().append("<div class='err' style='color:red;'>Maximum 2000 characters are allowed.</div>");
                        bol= false;
                    }
                }
                if(!bol){
                 return false;
                }else{
                    if(confirm("Please confirm the details.")){
                        return true;
                    }else{
                        return false;
                    }
                }
            }
            function CompareToForGreater(value,params)
                {

                var mdy = value.split('/')  //Date and month split
                var mdyhr=mdy[2].split(' ');  //Year and time split
                var mdyp = params.split('/')
                var mdyphr=mdyp[2].split(' ');


                if(mdyhr[1] == undefined && mdyphr[1] == undefined)
                {
                    //alert('Both Date');
                    var date =  new Date(mdyhr[0], mdy[1], mdy[0]);
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0]);
                }
                else if(mdyhr[1] != undefined && mdyphr[1] != undefined)
                {
                    //alert('Both DateTime');
                    var mdyhrsec=mdyhr[1].split(':');
                    var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                    var mdyphrsec=mdyphr[1].split(':');
                    var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                }
                else
                {
                    //alert('one Date and One DateTime');
                    var a = mdyhr[1];  //time
                    var b = mdyphr[1]; // time

                    if(a == undefined && b != undefined)
                    {
                        //alert('First Date');
                        var date =  new Date(mdyhr[0], mdy[1], mdy[0],'00','00');
                        var mdyphrsec=mdyphr[1].split(':');
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0], mdyphrsec[0], mdyphrsec[1]);
                    }
                    else
                    {
                        //alert('Second Date');
                        var mdyhrsec=mdyhr[1].split(':');
                        var date =  new Date( mdyhr[0], mdy[1], mdy[0], mdyhrsec[0], mdyhrsec[1]);
                        var datep =  new Date(mdyphr[0], mdyp[1], mdyp[0],'00','00');
                    }
                }

                return Date.parse(date) > Date.parse(datep);
            }
            function GetCal(txtname,controlname)
                        {
                            new Calendar({
                                inputField: txtname,
                                trigger: controlname,
                                showTime: 24,
                                onSelect: function() {
                                    var date = Calendar.intToDate(this.selection.get());
                                    LEFT_CAL.args.min = date;
                                    LEFT_CAL.redraw();
                                    this.hide();
                                    document.getElementById(txtname).focus();
                                }
                            });

                            var LEFT_CAL = Calendar.setup({
                                weekNumbers: false
                            })
                        }
        </script>
    </head>
    <body>
        <%
            String uId=request.getParameter("uId");
            String tId=request.getParameter("tId");
            String cId=request.getParameter("cId");
            ContentAdminService contentAdminService = (ContentAdminService) AppContext.getSpringBean("ContentAdminService");
            SPTenderCommonData data = contentAdminService.emailAndCompanyName(uId);
            List<TblRegDocReceipt> receiptList = contentAdminService.getDocReceipt(Integer.parseInt(uId));
            TblRegDocReceipt receipt= null;
            boolean isDocAvail=false;
            if(!receiptList.isEmpty()){
                isDocAvail=true;
                receipt = receiptList.get(0);
            }
            receiptList=null;
            if("Submit".equalsIgnoreCase(request.getParameter("submit"))){
                contentAdminService.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                boolean bol = contentAdminService.insertReceiptDoc(new TblRegDocReceipt(0, new TblLoginMaster(Integer.parseInt(uId)), DateUtils.convertDateToStr(request.getParameter("docReceiptDt")), request.getParameter("courierTrackingNo"), request.getParameter("comments"), Integer.parseInt(session.getAttribute("userId").toString()), new Date()));
                if(bol){
                    UserRegisterService service = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
                    String mobEmail = service.getTendererMobEmail(Integer.parseInt(uId));
                    String mail=null;                    
                    if(mobEmail!=null){
                        if(!("".equals(mobEmail))){                            
                            mail = mobEmail.substring(0,mobEmail.lastIndexOf("$"));
                        }
                    }
                    String [] mails={mail};
                    SendMessageUtil sendMessageUtil = new SendMessageUtil();
                    MailContentUtility mailContentUtility = new MailContentUtility();
                    String mailText = mailContentUtility.docReceiptMailContent(uId);
                    sendMessageUtil.setEmailTo(mails);
                    sendMessageUtil.setEmailFrom(XMLReader.getMessage("regEmailId"));
                    sendMessageUtil.setEmailSub("e-GP System: User Registration - Receipt of Attested Hard Copies of Supporting Documents");
                    sendMessageUtil.setEmailMessage(mailText);
                    sendMessageUtil.sendEmail();
                    sendMessageUtil=null;
                    mailContentUtility=null;
                    mails=null;
                   response.sendRedirect("CompanyVerification.jsp");
                }else{
                   out.println("Error in Inserting Details");
                }
            }else{
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="contentArea_1">
                    <div class="pageHead_1">Receipt of Physical Documents Information<span style="float:right;"> <a class="action-button-goback" href="CompanyVerification.jsp">Go back</a> </span></div>
                    <%if(!isDocAvail){%><table class="formStyle_1 t_space b_space" width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tbody><tr>
                                <td style="font-style: italic" class="ff t-align-left" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>

                        </tbody></table><%}%>
                    <form action="DocReceipt.jsp?uId=<%=uId%>&tId=<%=tId%>&cId=<%=cId%>" method="post">
                        <input type="hidden" value="<%=DateUtils.convertStrToDate(new java.util.Date())%>" id="currDate"/>
                        <table class="tableList_1 t_space" width="100%" border="0" cellpadding="0" cellspacing="0">

                            <tbody>
                                <tr>
                                    <td class="ff" width="25%">e-mail ID: </td>
                                    <td width="75%"><%=data.getFieldName1()%></td>
                                </tr>

                                <tr>
                                    <td class="ff">Company  / Individual Name: </td>
                                    <td><%=data.getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Date and Time of Receipt of Documents: <%if(!isDocAvail){%><span class="mandatory">*</span><%}%></td>
                                    <td>
                                        <%if(isDocAvail){out.print(DateUtils.gridDateToStr(receipt.getDocReceiptDt()));}else{%>
                                        <input name="docReceiptDt" class="formTxtBox_1" id="receiptDt" style="width: 100px;" readonly="readonly" type="text" onfocus="GetCal('receiptDt','receiptDt');">
                                        &nbsp;<img id="dtimg1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('receiptDt','dtimg1');"/>
                                        <%}%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Courier Tracking No.: <%if(!isDocAvail){%><span class="mandatory">*</span><%}%></td>
                                    <td>
                                        <%if(isDocAvail){out.print(receipt.getCourierTrackingNo());}else{%>
                                        <input name="courierTrackingNo" class="formTxtBox_1" id="trackNo" style="width: 195px;" type="text">
                                        <%}%>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="ff">Comments: <%if(!isDocAvail){%><span class="mandatory">*</span><%}%></td>
                                    <td>
                                        <%if(isDocAvail){out.print(receipt.getComments());}else{%>
                                        <textarea rows="5" id="comments" name="comments" class="formTxtBox_1" style="width: 60%;"></textarea>
                                        <%}%>
                                    </td>
                                </tr>
                                <%if(isDocAvail){%>
                                <tr>
                                    <td class="ff">Received  by :</td>
                                    <td><%=contentAdminService.contentAdminReciept(String.valueOf(receipt.getReceivedBy()))%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Date and Time of Entry : </td>
                                    <td><%if(receipt.getEntryDt()!=null){String date=DateUtils.gridDateToStr(receipt.getEntryDt()); out.print(date.subSequence(0, date.lastIndexOf(":")));}%></td>
                                </tr>
                                <%}%>
                                <%if(!isDocAvail){%>
                                <tr>
                                    <td></td>
                                    <td class="t-align-left">
                                        <label class="formBtn_1">
                                            <input name="submit" id="button" value="Submit" type="submit" onclick="return validate();">
                                        </label>
                                    </td>
                                </tr>
                                <%}%>
                            </tbody></table>
                    </form>
                </div>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <%}%>
    </body>
</html>
