<%-- 
    Document   : ViewTenderPaymentDetails
    Created on : Jul 15, 2011, 11:25:44 AM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payment Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
         <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>

        <script type="text/javascript">
            /* Call Print function */
            $(document).ready(function() {
                if (document.getElementById("anchPDF")!=null){
                     $('#anchPDF').show();
                }
                if (document.getElementById("print")!=null){
                    $('#print').show();
                    $("#print").click(function() {
                    printElem({leaveOpen: true, printMode: 'popup'});
                });
                }

            });

                function printElem(options){
                if (document.getElementById("print_area")!=null){
                    $('#print_area').printElement(options);
                }
            }
        </script>
    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <%
                boolean isPDF= false, iseGPAdmin = false;
                if(request.getParameter("isPDF") != null && request.getParameter("isPDF").equalsIgnoreCase("true")){
                    isPDF= true;
                    iseGPAdmin=true;
                }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%if(!isPDF){%>
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <% } %>
            <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                <%
                TenderCommonService objTSC = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");

                if (session.getAttribute("userId") != null) {
                        objTSC.setLogUserId(session.getAttribute("userId").toString());
                    }

                   
                    String userBranchId="";
                    String userId="", payUserId="", regPaymentId="", redirectPath="";
                    String tenderId="0", pkgLotId="0", payTyp="", paymentTxt="";

                if(!isPDF){
                 HttpSession hs = request.getSession();
                 if (hs.getAttribute("userId") != null) {
                         userId = hs.getAttribute("userId").toString();
                  } else {
                    // response.sendRedirectFilter(request.getContextPath() + "/SessionTimedOut.jsp");
                     //response.sendRedirect("SessionTimedOut.jsp");
                  }
                 }

                if(!isPDF){
                    HttpSession hs = request.getSession();
                    if ( hs.getAttribute("userTypeId")!= null) {
                        if("1".equalsIgnoreCase(hs.getAttribute("userTypeId").toString()) || "8".equalsIgnoreCase(hs.getAttribute("userTypeId").toString())){
                            iseGPAdmin=true; // userType is eGPAdmin or ContentAdmin";
                         }
                    }
                }

                String referer = "";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");
                }

                
                if (request.getParameter("uId") != null) {
                    payUserId = request.getParameter("uId");
                }

                if (request.getParameter("payId") != null) {
                    regPaymentId = request.getParameter("payId");
                }

                if (request.getParameter("tenderId") !=null){
                        tenderId=request.getParameter("tenderId");
                    }

                if (request.getParameter("lotId") != null){
                        if (request.getParameter("lotId")!="" && !request.getParameter("lotId").equalsIgnoreCase("null")){
                           pkgLotId=request.getParameter("lotId");
                        }
                    }

                if (request.getParameter("payTyp") != null){
                        if (request.getParameter("payTyp")!="" && !request.getParameter("payTyp").equalsIgnoreCase("null")){
                           payTyp=request.getParameter("payTyp");

                            if ("df".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Document Fees";
                             } else if ("ts".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Tender/Proposal Security";
                             } else if ("ps".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "Performance Security";
                             } else if ("bg".equalsIgnoreCase(request.getParameter("payTyp"))) {
                                 paymentTxt = "New Performance Security";
                             }
                           
                        }
                    }
                %>
                <div class="contentArea_1">
                    <div id="print_area">
                    <div class="pageHead_1">Payment Details
<!--                        <span style="float:right;">
                            <a href="<%=request.getContextPath()%>/report/TenderPaymentReport.jsp" class="action-button-goback">Go Back</a>
                        </span>-->
                    </div>

                        <div id="resultDiv">
                            
                                <%
                                    pageContext.setAttribute("tenderId", tenderId);
                                   pageContext.setAttribute("isPDF", request.getParameter("isPDF"));
                                   pageContext.setAttribute("userId", 0);
                                %>
                                <%@include file="../resources/common/TenderInfoBar.jsp" %>
                                <div>&nbsp;</div>   
<%if(iseGPAdmin) {%>
                    <div class="tabPanelArea_1 t_space">

                         <%                       
                         boolean isPaymentVerified=false;
                            
                         CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                         List<SPCommonSearchDataMore> lstPaymentDetail = null;
                         lstPaymentDetail = dataMore.geteGPData("getTenderPaymentDetailForAdmin", regPaymentId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                          if (!lstPaymentDetail.isEmpty()) {
                              if ("yes".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName8())) {
                                   isPaymentVerified = true;
                              }
                        %>

                        <!-- START: COMMON PACKAGE/LOT DESCRIPTION FILE -->
                        <div>                           
                            <%@include file="../partner/CommonPackageLotDescription.jsp" %>
                        </div>
                        <!-- END COMMON PACKAGE/LOT DESCRIPTION FILE -->

                        <%if(!isPDF){%>
                        <!-- START: COMMON PAYMENT RELATED DOCS -->
                        <div class="noprint">
                         <%
                                            String folderName = pdfConstant.TENDERDETAIL;
                                            String genId = regPaymentId;
                                            String pdfUrl = request.getContextPath() + "/GeneratePdf?reqURL=&reqQuery=&folderName=" + folderName + "&payId=" + regPaymentId + "&uId=" + payUserId + "&tenderId=" + tenderId + "&lotId=" + pkgLotId + "&payTyp=df&id=" + genId;
                                        %>
                            <%//@include file="../partner/CommonPaymentDocsList.jsp" %>

                            <jsp:include page="../partner/CommonPaymentDocsList.jsp" >
                                <jsp:param name="payId" value="<%=regPaymentId%>" />
                                <jsp:param name="tenderId" value="<%=tenderId%>" />
                                <jsp:param name="payTyp" value="<%=payTyp%>" />
                                <jsp:param name="pdfUrl" value="<%=pdfUrl%>" />
                            </jsp:include>
                        </div>
                            
                        <!-- END COMMON PAYMENT RELATED DOCS -->
                        <%}%>

                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td width="<%
                                        if(isPDF){
                                            out.print("25%");
                                        }else{
                                            out.print("20%");
                                        }
                                %>" class="ff">Payment Status :</td>
                            <td>
                                  <% if (!"Pending".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName10()) && !isPaymentVerified) {%>
                                                Verification is Pending
                                                <%} else {%>
                                                   <%=lstPaymentDetail.get(0).getFieldName10()%>
                                                <%}%>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Email ID :</td>
                            <td>
                                <%//=bidderEmail%>
                                <%=lstPaymentDetail.get(0).getFieldName13()%>
                            </td>
                        </tr>
                         <tr>
                            <td class="ff">Financial Institute Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName12()%></td>
                        </tr>
                        <%if(!"Online".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())){%>
                        <tr>
                            <td class="ff">Branch Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName1()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Branch Maker :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName2()%></td>
                        </tr>
                         <tr>
                            <td class="ff">Branch Checker :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName11()%></td>
                        </tr>
                        <%}else{%>
                        <tr>
                            <td class="ff">Transaction Number :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName14()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Card Number :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName15()%></td>
                        </tr>
                        <%}%>
                        <tr>
                            <td class="ff">Payment For : </td>
                            <td><%=paymentTxt%></td>
                        </tr>
                        <tr>
                            <td class="ff">Currency :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName3()%></td>
                        </tr>
                        <tr>
                            <td class="ff">Amount :</td>
                            <td>
                                <%if("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%} else if("USD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>$</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%} else if("Nu.".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%}%>
                            </td>
                        </tr>
                        <%if("Online".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())){%>
                            <tr>
                                <td class="ff" >Service Charge :</td>
                                <td><label>Taka</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName16() %></td>
                            </tr>
                            <tr>
                                <td class="ff" >Total Amount :</td>
                                <td><label>Taka</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName17() %></td>
                            </tr>
                        <%}%>
                         <tr>
                            <td class="ff">Mode of Payment :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName5()%>
                            </td>
                        </tr>
                        <%if("Pay Order".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5()) || "DD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5()) || "Bank Guarantee".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5()) || "Online".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = objTSC.returndata("getTenderPaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Instrument No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <%if(!"Online".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())){%>
                                <tr>
                                    <td class="ff">Issuing Financial Institute :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Financial Institute Branch :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuance Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName4()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Validity Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName5()%></td>
                                </tr>

                               <% }}
                            }%>

                            <%if("Account to Account Transfer".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = objTSC.returndata("getTenderPaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Account No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <%if(!"".equalsIgnoreCase(lstPaymentDetailMore.get(0).getFieldName3())){%>
                                <tr>
                                    <td class="ff">Branch Name :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <%}%>
                               <% }
                            }%>

                            <tr>
                                <td class="ff">Date and Time of Payment :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName6()%></td>
                            </tr>

                            <tr>
                                <td class="ff">Remarks :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName7()%></td>
                            </tr>
                    </table>
 </div>
                              </div>

                    <% } else {%>
                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td>Payment information not found!</td>
                        </tr>
                    </table>
                    <%}%>
<%}%>
</div>
                 <!--Dashboard Content Part End-->
 <%if(!isPDF){%>
                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->
                <%}%>
        
        
        </div>
        </div>
        </body>
</html>

