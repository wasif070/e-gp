<%-- 
    Document   : EditIndividualConsultant
    Created on : Oct 23, 2010, 6:08:16 PM
    Author     : parag
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Individual Consultant Details</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="Include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="../resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="pageHead_1">Edit User Registration - Individual Consultant Details</div>
                            <form id="frmUserRegistration" method="post">
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">                                
                                <tr>
                                    <td class="ff">Title : <span>*</span></td>
                                    <td><input name="title" type="text" class="formTxtBox_1" id="txtTitle" style="width:200px;" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">First Name : <span>*</span></td>
                                    <td><input name="firstName" type="text" class="formTxtBox_1" id="txtFirstName" style="width:200px;"  readonly="true"/></td>
                                </tr>
                                <tr>
                                    <td class="ff">Middle Name : <span>*</span></td>
                                    <td><input name="middleName" type="text" class="formTxtBox_1" id="txtMiddleName" style="width:200px;" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Last Name : <span>*</span></td>
                                    <td><input name="lastName" type="text" class="formTxtBox_1" id="txtLastName" style="width:200px;" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Designation  :</td>
                                    <td><input name="designation " maxlength="30" type="text" class="formTxtBox_1" id="txtDesignation" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Department :</td>
                                    <td><input name="department" type="text" maxlength="30"  class="formTxtBox_1" id="txtDepartment" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Name in Bangla :</td>
                                    <td><input name="nameInBangla" type="text" class="formTxtBox_1" id="txtNameInBangla" style="width:200px;" />
                                        <div class="formNoteTxt">(if Bangladeshi)</div></td>
                                </tr>
                                <tr>
                                    <td class="ff">National Id / Passport No. / <br />
                                        Driving License No : <span>*</span></td>
                                    <td><input name="npdName" type="text" class="formTxtBox_1" id="txtNpdId" style="width:200px;" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Address 1 : <span>*</span></td>
                                    <td><textarea name="address1" rows="3" class="formTxtBox_1" maxlength="250"  id="txtAAddress1" style="width:400px;"></textarea></td>
                                </tr>
                                <tr>
                                    <td class="ff">Address 2 : <span>*</span></td>
                                    <td><textarea name="address2" rows="3" class="formTxtBox_1" maxlength="250" id="txtAAddress2" style="width:400px;"></textarea></td>
                                </tr>
                                <tr>
                                    <td class="ff">Country : <span>*</span></td>
                                    <td><select name="country" class="formTxtBox_1" id="cmbCountry">
                                            <option>Select Country</option>
                                            <option>India</option>
                                            <option>Bhutan</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td class="ff">Dzongkhag / District : <span>*</span></td>
                                    <td><select name="stateDistrict" class="formTxtBox_1" id="cmbStateDistrict">
                                            <option>Select Dzongkhag / District</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td class="ff">City / Town : <span>*</span></td>
                                    <td><input name="cityTown" type="text" class="formTxtBox_1"  maxlength="50" id="txtCityTown" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Gewog : <span>*</span></td>
                                    <td><input name="thanaUpjilla" type="text" class="formTxtBox_1" maxlength="50" id="txtThanaUpjilla" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Post Code / Zip Code : <span>*</span></td>
                                    <td><input name="zipCode" type="text" class="formTxtBox_1"  maxlength="15" id="txtZipCode" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Phone No : <span>*</span></td>
                                    <td><input name="phoneNo" type="text" class="formTxtBox_1" maxlength="20" id="txtPhoneNo" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Fax No : <span>*</span></td>
                                    <td><input name="faxNo" type="text" class="formTxtBox_1"  maxlength="20" id="txtFaxNo" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Mobile No : <span>*</span></td>
                                    <td><input name="mobileNo" type="text" class="formTxtBox_1" maxlength="16" id="txtMobileNo" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Tax Payment Number : <span>*</span></td>
                                    <td><input name="taxIDNumber" type="text" class="formTxtBox_1" maxlength="20" id="cmbTaxIDNumber" style="width:200px;" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Name of Other Document similar to TIN : <span>*</span></td>
                                    <td><input name="otherDoc1TIN" type="text" class="formTxtBox_1" maxlength="20" id="txtOtherDoc1TIN" style="width:200px;" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td class="ff"> <span></span></td>
                                    <td><input name="otherDoc2TIN" type="text" class="formTxtBox_1" maxlength="20" id="txtOtherDoc2TIN" style="width:200px;" readonly="true" /></td>
                                </tr>
                                <tr>
                                    <td class="ff">Specialization : <span>*</span></td>
                                    <td><textarea name="specialization" rows="3" class="formTxtBox_1" id="txtASpecialization" style="width:400px;"></textarea></td>
                                </tr>
                                <tr>
                                    <td class="ff">Website : <span>*</span></td>
                                    <td><input name="website" type="text" class="formTxtBox_1" id="txtWebsite" style="width:200px;" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><label class="formBtn_1">
                                            <input type="submit" name="btnSubmit" id="btnSubmit" value="Save" />
                                        </label>
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Next" />
                                        </label></td>
                                </tr>
                            </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
</html>

