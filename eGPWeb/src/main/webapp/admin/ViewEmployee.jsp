<%-- 
    Document   : ViewManage
    Created on : Nov 11, 2010, 10:59:52 AM
    Author     : rishita
--%>


<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils" %>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.egp.eps.model.table.TblDepartmentMaster" %>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<jsp:useBean id="manageEmployeeSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ManageEmployeeGridSrBean"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%
                response.setHeader("Cache-Control", "no-cache");
                response.setHeader("Cache-Control", "no-store");
                response.setHeader("Pragma", "no-cache");
    %>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

                    int userid = 0;
                    
                    HttpSession hs = request.getSession();
                    if (hs.getAttribute("userId") != null) {
                        userid = Integer.parseInt(hs.getAttribute("userId").toString());
                        manageEmployeeSrBean.setLogUserId(hs.getAttribute("userId").toString());
                    }
                    short deptid = 0;
                    if (!request.getParameter("deptid").equals("")) {
                        deptid = Short.parseShort(request.getParameter("deptid"));
                    }
                    String departmentType = "";
                    if (!request.getParameter("division").equals("")) {
                        departmentType = request.getParameter("division");
                    }
                    String display = "";
                    if(departmentType.equalsIgnoreCase("Organization")){
                        display = "Organizations";
                    }else{
                        display = departmentType;
                    }
                    
                    if(request.getParameter("action") != null && request.getParameter("action").equalsIgnoreCase("view"))
                    {
                        String auditAction="View Ministry";
                        String idType="ministryId";
                        if(display != null && display.equalsIgnoreCase("Organizations"))
                        {
                              idType="organisationId";
                              auditAction="View Organization";
                        }
                       else if(display != null && display.equalsIgnoreCase("division"))
                       {
                           idType="divisionId";
                           auditAction="View Division";
                       }
                       else
                       {
                                 idType="ministryId";
                                 auditAction="View Ministry";
                       }
                       
                         // Coad added by Dipal for Audit Trail Log.
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        
                        int auditId=Integer.parseInt(request.getParameter("deptid"));
                        
                        String moduleName=EgpModule.Manage_Users.getName();
                        String remarks="User Id: "+session.getAttribute("userId")+" has viewed Ministry for Ministry Id: "+auditId;
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    }

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>View <% if(departmentType.equalsIgnoreCase("Organization")) { out.print("Division"); }else if(departmentType.equalsIgnoreCase("Division")) { out.print("Department"); } else {out.print(departmentType);}%></title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

    </head>
    <body>
<!--        <div class="mainDiv">

            <div class="dashboard_div">-->
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("hdnUserType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                    userType.append(request.getParameter("hdnUserType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }

                            if ("Edit".equals(request.getParameter("edit"))) {
                                response.sendRedirect("EditEmployee.jsp?deptid=" + deptid + "&division=" + departmentType);
                            }
                            if ("Ok".equals(request.getParameter("ok"))) {
                                response.sendRedirect("ManageEmployee.jsp?deptType=" + departmentType);
                            }
                %>
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->

                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <td class="contentArea">                         
                            <div class="pageHead_1 t_space">View <% if(departmentType.equalsIgnoreCase("Organization")) { out.print("Division"); }else if(departmentType.equalsIgnoreCase("Division")) { out.print("Department"); } else {out.print(departmentType);} %> Details</div>
                               <%
                                        String result = request.getParameter("msg");
                                        if (result.equals("success")) {%>
                            <br/><div align="left" id="sucMsg" class="responseMsg successMsg"><%if(departmentType.equalsIgnoreCase("Organization")) { out.print("Division"); }else if(departmentType.equalsIgnoreCase("Division")) { out.print("Department"); } else {out.print(departmentType);}%> updated successfully</div>
                            <% } else if (result.equals("create")) {%>
                            <br/><div align="left" id="sucMsg" class = "responseMsg successMsg"><%if(departmentType.equalsIgnoreCase("Organization")) { out.print("Division"); }else if(departmentType.equalsIgnoreCase("Division")) { out.print("Department"); } else {out.print(departmentType);}%> created successfully</div>
                            <% }
                            %>
                            <form id="frmEditEmployee" name="frmEditEmployee" method="post" action="">
                                <input type="hidden" id="departmentiddfd"/>
                                <table class="formStyle_1" border="0" cellpadding="0" width="100%" cellspacing="10">
                                    <%String pDeptName = "";
                                                for (TblDepartmentMaster tdm : manageEmployeeSrBean.getFindUser(deptid)) {
                                                    if (!departmentType.equals("Ministry")) {
                                                        if (departmentType.equals("Division")) {
                                                            pDeptName = "Ministry";
                                                        }
                                                        else if (departmentType.equals("Organization")) {
                                                            pDeptName = "Department";
                                                        }else {
                                                            pDeptName = "Division";
                                                        }
                                    %>
                                    <tr>
                                        <td class="ff" >Parent <%=pDeptName%> :</td>
                                        <td ><label><%=manageEmployeeSrBean.getParentDeptName()%></label></td>
                                    </tr>
                                    <tr>
                                        <% }%>
                                        <td class="ff" ><label id="lblName">Name of <% if(tdm.getDepartmentType().equalsIgnoreCase("Organization")){out.print("Division");} else if(tdm.getDepartmentType().equalsIgnoreCase("Division")){out.print("Department");} else{out.print(tdm.getDepartmentType());}%> :</label></td>
                                        <td>
                                            <label><%=tdm.getDepartmentName()%></label><br/>
                                        </td>
                                    </tr>
                                    <%
                                                                                            //str = new String(tdm.getDeptNameInBangla(), "UTF-8");
                                                                                            //str =BanglaNameUtils.getUTFString(tdm.getDeptNameInBangla());
                                                                                            //request.setCharacterEncoding("UTF-8");
%>
<!--                                    <tr>
                                        <td class="ff" ><label id="lblNameBangla">Name of <%if(tdm.getDepartmentType().equalsIgnoreCase("Organization")){out.print("Organization");} else{out.print(tdm.getDepartmentType());}%> in Dzongkha :</label></td>
                                        <% String str = "";
                                           if (tdm.getDeptNameInBangla() != null) { %>
                                        <td>
                                            <label><%=BanglaNameUtils.getUTFString(tdm.getDeptNameInBangla())%></label>
                                        </td>
                                        <% }%>
                                    </tr>-->
                                    <tr>
                                        <td  width="20%" class="ff" >Address :</td>
                                        <td  width="80%">
                                            <label><%=tdm.getAddress()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" >Country :</td>

                                        <td>
                                            <label><%= tdm.getTblCountryMaster().getCountryName()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" >Dzongkhag / District :</td>
                                        <td>
                                            <label><%= tdm.getTblStateMaster().getStateName()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" >City / Town :</td>
                                        <td><label><%=tdm.getCity()%></label>
                                        </td>
                                    </tr>

<!--                                    <tr>
                                        <td class="ff" >Thana / Upazilla :</td>
                                        <td>
                                            <label><%//=tdm.getUpJilla()%></label>
                                        </td>
                                    </tr>-->
                                    <tr>
                                        <td class="ff">Post Code :</td>
                                        <td>
                                            <label><%=tdm.getPostCode()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff"  >Phone No. :</td>
                                        <% if (!tdm.getPhoneNo().trim().equals("")) { %>
                                        <td><label><%=tdm.getPhoneNo()%></label></td>
                                        <% } %>
                                    </tr>
                                    <tr>
                                        <td class="ff" >Fax No. :</td>
                                        <% if (!tdm.getFaxNo().equals("")) { %>
                                        <td><label><%=tdm.getFaxNo()%></label></td>
                                        <% } %>
                                    </tr>
<!--                                 <tr>
                                        <td class="ff" >Mobile Number :</td>
                                        <% //if (!tdm.getMobileNo().equals("")) { %>
                                        <td><label><%//tdm.getMobileNo()%></label></td>
                                        <% //} %>
                                   </tr>-->
                                    <tr>
                                        <td class="ff" >Website :</td>
                                        <% if (tdm.getWebsite()!= null && !tdm.getWebsite().equals("") ) {%>
                                        <td><label><%=tdm.getWebsite()%></label></td>
                                        <% } %>
                                    </tr>
                                    <% if(departmentType.equalsIgnoreCase("Organization")){%>
                                    <tr>
                                        <td class="ff" >Corporation Type :</td>
                                        <td>
                                            <label><%=tdm.getOrganizationType()%></label>
                                        </td>
                                    </tr>
                                    <% } }%>
                                    <tr align="center">
                                        <td>&nbsp;</td>
                                        <td  align="left"><label class="formBtn_1"><input id="ok" name="ok" value="Ok" type="submit"/></label>&nbsp;<label class="formBtn_1"><input id="edit" name="edit" value="Edit" type="submit"/></label></td>
                                                <%--<td class="formBtn_1"  align="center"><input id="nextPage" name="nextPage" value="View all" type="submit"/></td>--%>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
<!--            </div>
        </div>-->
<script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
    <%
                manageEmployeeSrBean = null;
    %>
</html>
