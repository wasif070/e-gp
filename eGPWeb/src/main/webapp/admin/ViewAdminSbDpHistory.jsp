<%--
    Document   : ViewSbDevPartAdmin
    Created on : Nov 11, 2010, 11:10:54 AM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.model.table.TblUserActivationHistory"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <jsp:useBean id="scBankPartnerAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ScBankPartnerAdminSrBean"/>
    <jsp:useBean id="partnerAdminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.PartnerAdminMasterDtBean"/>
    <jsp:setProperty name="partnerAdminMasterDtBean" property="*"/>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <%
                    int uTypeID = 0;
                    if(session.getAttribute("userTypeId") != null){
                        uTypeID = Integer.parseInt(session.getAttribute("userTypeId").toString());
                    }

                    byte usrTypeId = 0;
                    if (!request.getParameter("userTypeId").equals("")) {
                        usrTypeId = Byte.parseByte(request.getParameter("userTypeId"));
                    }
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        scBankPartnerAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                    }
                    String usrId = request.getParameter("userId");
                    if (usrId != null && !usrId.equals("")) {
                        partnerAdminMasterDtBean.setUserId(Integer.parseInt(usrId));
                        partnerAdminMasterDtBean.setUserTypeId(usrTypeId);
                        partnerAdminMasterDtBean.populateInfo();
                    }

                    String from = request.getParameter("from");
                    if (from == null || from.equals("")) {
                        from = "Operation";
                    }
                    String partnerType = request.getParameter("partnerType");
                    String type = "";
                    String title = "";
                    String title1 = "";

                    if (partnerType != null && partnerType.equals("Development")) {
                        type = "Development";
                        title = "Development Partner";
                        title1 = "Development Partner";

                    }
                    else {
                        type = "ScheduleBank";
                        title = "Schedule Bank";
                        title1 = "Scheduled Bank";

                    }
                    //String gridURL = "ManageSbDpAdmin.jsp?partnerType=" + type;
                    
                     // Coad added by Dipal for Audit Trail Log.
                    AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                    String idType="userId";
                    int auditId=Integer.parseInt(request.getParameter("userId"));
                    String auditAction=null;
                    
                        if (usrTypeId == 6)
                        {
                            auditAction = "View Account History of Development Partner Admin" ;
                        }
                        if (usrTypeId == 7)
                        {
                            auditAction = "View Account History of Scheduled Bank Admin" ;
                        }
                        if (usrTypeId == 15)
                        {
                            auditAction = "View Account History of Scheduled Bank Branch Admin";
                        }
                    String moduleName=EgpModule.Manage_Users.getName();
                    String remarks="";
                    MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                    makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <%if(uTypeID!=7 && uTypeID!=6){%>
        <title><%=title%> Admin : Status History</title>
        <%}else{%>
        <title><%=title%> Status History<</title>
        <%}%>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

    </head>


    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                       
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                       
        <%
                    TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
                    List<TblUserActivationHistory> list = transferEmployeeServiceImpl.viewAccountStatusHistory(usrId);
                    boolean flag = false;
                    int j = 1;
        %>
                       
                        <td class="contentArea_1">
                            <!--Page Content Start-->

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <!-- Success failure -->
                                    <!-- Result Display End-->

                                    <td>
                                       
                                        <div class="pageHead_1"><%=title%> Admin</div>
                                       
                                        <div class="t_space">
                                        </div>
                                        <form id="frmSBAdmin" name="frmSBAdmin" method="post" action="">
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">

                                                <tr>
                                                    <td width="20%" class="ff"><%=title1%> : </td>
                                                    <td width="80%">
                                                        <label id="bankName">${partnerAdminMasterDtBean.sbDevelopName}</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">e-mail ID : </td>
                                                    <td>
                                                        <label id="txtMail">${partnerAdminMasterDtBean.emailId}</label>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td class="ff">Full Name :</td>
                                                    <td><label id="txtName">${partnerAdminMasterDtBean.fullName}</label>
                                                    </td>
                                                </tr>
                                                   
                                                    <tr>
                                                    <td class="ff">National ID : </td>
                                                    <td>
                                                        <label id="txtNationalId" >${partnerAdminMasterDtBean.nationalId}</label>
                                                    </td>
                                                </tr>
                                                  
                                                <tr>
                                                    <td class="ff">Mobile No. :</td>
                                                    <td>
                                                        <label id="txtMobileNo">${partnerAdminMasterDtBean.mobileNo}</label>
                                                    </td>
                                                </tr>
                                            </table>
                                          <table class="tableList_1 t_space" cellspacing="0" width="100%">
                                            <tr>
                                                <th class="t-align-center">Sl. No.</th>
                                                <th class="t-align-center">Full Name</th>
                                                <th class="t-align-center">Status</th>
                                                <th class="t-align-center">Action By</th>
                                                <th class="t-align-center">Action Date</th>
                                                <th class="t-align-center">Reason</th>
                                            </tr>
                                            <%

                                                        for (int i = 0; i < list.size(); i++) {
                                                            String strFullName = "-";
                                                            String straction = "-";
                                                            String strComments = "-";
                                                            String strdt = "-";

                                                            if (!"".equalsIgnoreCase(list.get(i).getAction())) {
                                                                straction= list.get(i).getAction();
                                                                flag = true;


                                                                    strFullName = partnerAdminMasterDtBean.getFullName();


                                                                if (!"".equalsIgnoreCase(list.get(i).getComments())) {
                                                                    strComments = list.get(i).getComments();
                                                                }
                                                                if (!"".equalsIgnoreCase(list.get(i).getAction())) {
                                                                    straction = list.get(i).getAction();
                                                                }

                                            %>
                                            <tr
                                                <%if (i % 2 == 0) {%>
                                                class="bgColor-white"
                                                <%} else {%>
                                                class="bgColor-Green"
                                                <%   }%>
                                                >
                                                <td class="t-align-center"> <%=j%>
                                                    <%j++;%>
                                                </td>
                                                <td class="t-align-center"><%=strFullName%></td>
                                                <td class="t-align-center"><%=straction%></td>
                                                <td class="t-align-center"><%=list.get(i).getActionByName()%></td>
                                                <td class="t-align-center"><%=DateUtils.gridDateToStr(list.get(i).getActionDt())%></td>
                                                 <td class="t-align-center"><%=strComments%></td>

                                            </tr>
                                            <%}
                                                        }
                                                        if (!flag) {
                                            %>
                                            <td class="t-align-center" colspan="6" style="color: red;font-weight: bold">No Record Found</td>
                                            <%}%>


                                        </table>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <%if(uTypeID==7||uTypeID==6){%>
            <script>
                var headSel_Obj = document.getElementById("headTabMyAcc");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            <%}else{%>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            <%}%>
    </body>
</html>
