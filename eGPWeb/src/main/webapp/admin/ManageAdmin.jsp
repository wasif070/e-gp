<%-- 
    Document   : ManageAdmin
    Created on : Oct 30, 2010, 9:49:08 AM
    Author     : rishita
--%>

<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.util.List"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.web.databean.ManageAdminDtBean" %>

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");

                    byte userTypeid = 0;
                    if (request.getParameter("userTypeid") != null) {
                        userTypeid = Byte.parseByte(request.getParameter("userTypeid"));
                    }
                    int uTypeID = 0;
                    if (session.getAttribute("userTypeId") != null) {
                        uTypeID = Integer.parseInt(session.getAttribute("userTypeId").toString());
                    }
                    String hasMobile = "";
                    if(request.getParameter("hasMobile") != null){
                        hasMobile = request.getParameter("hasMobile");
                    }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <% if (uTypeID == 12) {%>
        <title>Edit Procurement Expert Detail</title>
        <% } else if (uTypeID == 5) {%>
        <title>Edit Profile</title>
        <% } else if (uTypeID == 19) {%>
        <title>Edit O & M Admin</title>
        <% } else if (uTypeID == 20) {%>
        <title>Edit O & M User</title>
        <% }
            else if (uTypeID == 21) {%>
        <title>Edit Performance Monitoring User</title>
        <% }
            else {%>
        <title>Manage Admin</title>
        <% }%>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--        <script type="text/javascript" src="../resources/js/pngFix.js"></script>-->
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                var hasMobileNo = '<%=hasMobile%>';
                if(hasMobileNo == 'No')
                    alert("Please insert your mobile no");
                
                $("#frmManageAdmin").validate({
                    rules: {
                        fullName: { spacevalidate: true, requiredWithoutSpace: true , alphaName:true, maxlength:100 },
                        mobileNo: { required:true, number: true, minlength: 8, maxlength:8 },
                        phoneNo: { required:true,numberWithHyphen:true,PhoneFax: true},
                        nationalId: { required:true, number: true,maxlength: 11,minlength: 11}

                    },
                    messages: {
                        fullName: {
                            spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace: "<div class='reqF_1'>Please enter Full Name</div>",
                            alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only </div>",
                            maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"
                        },
                        mobileNo: {
                            required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                            number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" ,
                            minlength:"<div class='reqF_1'>Minimum 8 digits are required</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 8 digits only</div>"
                        },
                        phoneNo: {
                            required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            numberWithHyphen: "<div class='reqF_1'>Allows numbers (0-9) and hyphen (-) only</div>",
                            PhoneFax:"<div class='reqF_1'>Please enter valid Phone No. Area code should contain minimum 2 digits and maximum 5 digits. Phone no. should contain minimum 3 digits and maximum 10 digits</div>"
                        },
                        nationalId :{
                            required: "<div class='reqF_1'>Please enter CID No.</div>",
                            number: "<div class='reqF_1'>Please enter digits (0-9) only</div>",
                            maxlength: "<div class='reqF_1'>CID No should comprise of maximum 11 digits</div>",
                            minlength: "<div class='reqF_1'>CID No should comprise of minimum 11 digits</div>"
                        }
                    },

                    errorPlacement:function(error ,element)
                    {
                        if(element.attr("name")=="phoneNo")
                        {
                            error.insertAfter("#fxno");
                        }
                        else
                        {
                            error.insertAfter(element);

                        }
                    }
                }
            );
            });
            //        var validCheckMno = true;
            //        $(function() {
            //            $('#txtMobileNumber').blur(function() {
            //
            //                //   var numericChk= '/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/';
            //                var numbericChk = /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/;
            //
            //                var hdnMobNo = document.getElementById("hdnMobileNo").value;
            //                if(document.getElementById("txtMobileNumber").value!=""
            //                    && hdnMobNo != document.getElementById("txtMobileNumber").value){
            //                    if(document.getElementById("txtMobileNumber").value != ''){
            //                        numbericChk.test($.trim($('#txtMobileNumber').val()));
            //                        $('span.#mobMsg').html("");
            //                        //chk for numeric(javascript)
            //                        if(numbericChk.test($.trim($('#txtMobileNumber').val())))
            //                        {
            //                            $('span.#mobMsg').css("color","red");
            //                            $('span.#mobMsg').html("Checking for unique Mobile No...");
            //                            $.post("<-%=request.getContextPath()%>/CommonServlet", {mobileNo:$.trim($('#txtMobileNumber').val()),funName:'verifyMobileNo'},  function(j){
            //                                if(j.toString().indexOf("OK", 0)==0){
            //                                    $('span.#mobMsg').css("color","green");
            //                                    validCheckMno = true;
            //                                }
            //                                else if(j.toString().indexOf("Mobile", 0)!=-1){
            //                                    $('span.#mobMsg').css("color","red");
            //                                    validCheckMno = false;
            //                                }
            //                                $('span.#mobMsg').html(j);
            //                            });
            //                        }
            //                    }}
            //                else{
            //                    $('span.#mobMsg').html("");
            //                }
            //            });
            //        });
        </script>
    </head>
    <jsp:useBean id="manageAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.UpdateAdminSrBean"/>
    <jsp:useBean id="adminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.AdminMasterDtBean"/>
    <jsp:setProperty name="adminMasterDtBean" property="*"/>

    <body>
        <%
                    if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        manageAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
                    }
                    int userId = 0;
                    if (request.getParameter("userId") != null) {
                        userId = Integer.parseInt(request.getParameter("userId"));
                    }

                    if ("Update".equals(request.getParameter("Update"))) {
                        String msg = "";
                        int strM = -1; // Default
                        //  String nationalId = adminMasterDtBean.getNationalId();
                        if (strM == -1) {
                            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                            manageAdminSrBean.setAuditTrail(null);
                            List<ManageAdminDtBean> manageAdminDtBe = manageAdminSrBean.getManageDetail(userTypeid, userId);
                            //String comNationalId = manageAdminDtBe.get(0).getNationalId();
                           /* if (!comNationalId.equals(nationalId)) {
                            msg = commonService.verifyNationalId(nationalId);
                            if (msg.length() > 2) {
                            strM = 1; //NationalId Already Exist
                            }
                            }*/

                        }
                        if (strM == -1) {

                            if (adminMasterDtBean.getNationalId() == null) {
                                adminMasterDtBean.setNationalId("");
                            }

                            if (adminMasterDtBean.getMobileNo() == null) {
                                adminMasterDtBean.setMobileNo("");
                            }

                            adminMasterDtBean.setNationalId(adminMasterDtBean.getNationalId().trim());
                            adminMasterDtBean.setMobileNo(adminMasterDtBean.getMobileNo().trim());

                            String auditAction = "";
                            String moduleName = "";
                            if (!"1".equals(session.getAttribute("userTypeId").toString())) {
                                auditAction = "Edit Profile";
                                moduleName = EgpModule.My_Account.getName();
                                manageAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                            } else {
                                manageAdminSrBean.setAuditTrail(null);
                                if (userTypeid == 5) {
                                    auditAction = "Edit Organization Admin";
                                    moduleName = EgpModule.Manage_Users.getName();
                                    manageAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                } else if (userTypeid == 8) {
                                    auditAction = "Edit Content Admin";
                                    moduleName = EgpModule.Manage_Users.getName();
                                    manageAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                } else if (userTypeid == 12) {
                                    auditAction = "Edit Procurement Expert User";
                                    moduleName = EgpModule.Manage_Users.getName();
                                    manageAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                } else if (userTypeid == 19) {
                                    auditAction = "Edit O & M Admin";
                                    moduleName = EgpModule.Manage_Users.getName();
                                    manageAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                }  else if (userTypeid == 20) {
                                    auditAction = "Edit O & M User";
                                    moduleName = EgpModule.Manage_Users.getName();
                                    manageAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                }
                            }
                            if (manageAdminSrBean.organizationUpdate(adminMasterDtBean, auditAction, moduleName)) {
                                response.sendRedirect("ViewManageAdmin.jsp?userId=" + userId + "&userTypeid=" + userTypeid + "&msg=success");
                            } else {
                                response.sendRedirect("ManageAdmin.jsp?userId=" + userId + "&userTypeid=" + userTypeid + "&msg=updateSuccess");
                            }
                            //response.sendRedirect("AdminDashboard.jsp?userType=admin");
                        } else {
                            response.sendRedirect("ManageAdmin.jsp?userId=" + userId + "&userTypeid=" + userTypeid + "&strM=" + strM);
                        }
                    } else {
        %>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                                StringBuilder userType = new StringBuilder();
                                                if (request.getParameter("hdnUserType") != null) {
                                                    if (!"".equalsIgnoreCase(request.getParameter("userhdnUserTypeType"))) {
                                                        userType.append(request.getParameter("hdnUserType"));
                                                    } else {
                                                        userType.append("org");
                                                    }
                                                } else {
                                                    userType.append("org");
                                                }
                        %>
                        <%if (Integer.parseInt(session.getAttribute("userTypeId").toString()) == 1) {%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <%}%>
                        <td class="contentArea_1">
                            <%
                                                    int strM = -1;
                                                    String msg = "";
                                                    if (request.getParameter("strM") != null) {
                                                        try {
                                                            strM = Integer.parseInt(request.getParameter("strM"));
                                                        } catch (Exception ex) {
                                                            strM = 2;
                                                        }
                                                        switch (strM) {
                                                            case (1):
                                                                msg = "CID No Already Exists";
                                                                break;
                                                        }
                            %>
                            <div class="responseMsg errorMsg" style="margin-left:7px; margin-right: 2px; margin-top: 15px;">
                                <%= msg%>
                            </div>
                            <%
                                                        msg = null;
                                                    }
                                                    if (userTypeid == 12) {%>
                            <div class="pageHead_1">Edit Procurement Expert Detail</div>
                            <% } else if (userTypeid == 5 || userTypeid == 8) {%>
                            <div class="pageHead_1">Edit Profile</div>
                            <% } else if (userTypeid == 19) {%>
                            <div class="pageHead_1">Edit O & M Admin Details</div>
                             <% } else if (userTypeid == 20) {%>
                            <div class="pageHead_1">Edit O & M User Details</div>
                            <% }
                                 else if (userTypeid == 21) {%>
                            <div class="pageHead_1">Edit Performance Monitoring Details</div>
                            <% }
                                   else {%>
                            <div class="pageHead_1">Edit Admin User Details</div>
                            <% }
                                                    if (request.getParameter("msg") != null && "updateSuccess".equals(request.getParameter("msg"))) {
                                                        if (userTypeid == 5 && uTypeID == 1) {%>
                            <div class='responseMsg errorMsg'><%=appMessage.orgAdminUpdate%></div>
                            <%} else if (userTypeid == 12 && uTypeID == 1) {%>
                            <div class='responseMsg errorMsg'><%=appMessage.procurementExpertUpdate%></div>
                            <%} else if (userTypeid == 8 && uTypeID == 1) {%>
                            <div class='responseMsg errorMsg'><%=appMessage.contentAdminUpdate%></div>
                            <%}
                                                        }%>

                            <!--Page Content Start-->
                            <form id="frmManageAdmin" name="frmManageAdmin" method="post" action="">
                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10">
                                    <%                      manageAdminSrBean.setAuditTrail(null);
                                                            for (ManageAdminDtBean manageAdminDtBean : manageAdminSrBean.getManageDetail(userTypeid, userId)) {
                                                                if (userTypeid == 5) {
                                    %>
                                    <tr>
                                        <td class="ff" width="200">Organization :</td>
                                        <td>
                                            <label id="lblOrgaization"><%= manageAdminDtBean.getDepartmentName()%></label>
                                            <input class="formTxtBox_1" value="<%= manageAdminDtBean.getDepartmentName()%>" type="hidden" id="txtOrganization" style="width: 200px;"/>
                                        </td>
                                    </tr>
                                    <% }%>
                                    <tr>
                                        <td class="ff" width="200">e-mail ID:</td>
                                        <td>
                                            <label id="lblEmailId"><%= manageAdminDtBean.getEmailId()%></label>
                                            <input class="formTxtBox_1" id="txtEmailId" value="<%= manageAdminDtBean.getEmailId()%>" type="hidden" style="width: 200px;"/>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff" width="200">Full Name : <span>*</span></td>
                                        <td>
                                            <input class="formTxtBox_1" name="adminId" value="<%= manageAdminDtBean.getAdminId()%>" type="hidden" id="txtadminid" style="width: 200px;"/>
                                            <input class="formTxtBox_1" name="userId" value="<%= manageAdminDtBean.getUserId()%>" type="hidden" id="txtuserId" style="width: 200px;"/>
                                            <input class="formTxtBox_1" name="fullName" value="<%= manageAdminDtBean.getFullName()%>" id="txtFullName"  type="text" style="width: 200px;" maxlength="101"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">CID No. : <span>*</span></td>
                                        <td>
<!--                                            <label id="lblNationalId"><%=manageAdminDtBean.getNationalId()%></label>-->
                                            <input class="formTxtBox_1" name="nationalId" value="<%=manageAdminDtBean.getNationalId()%>" id="txtNationalId"  type="text" style="width: 200px;" maxlength="26" />
                                            <input class="formTxtBox_1" name="hiddenNationalId" value="<%=manageAdminDtBean.getNationalId()%>" id="txtHiddenNationalId"  type="hidden" style="width: 200px;"/>
                                            <span id="errMsg" style="color: red; font-weight: bold"></span>
                                        </td>
                                    </tr>

                                    <!--<tr>
                                        <td class="ff" width="200">Phone Number : <span>*</span></td>
                                        <td>
                                            <input class="formTxtBox_1" name="phoneNo" value="<%=manageAdminDtBean.getPhoneNo()%>" id="txtphoneNo"  type="text" style="width: 200px;" maxlength="17"/>
                                            <span id="fxno" style="color: grey;"> AreaCode-Phone No. i.e. 02-336962</span>
                                        </td>
                                    </tr>-->

                                    <tr>
                                        <td class="ff" width="200">Mobile Number : <span>*</span></td>
                                        <td>
                                            <input class="formTxtBox_1" name="mobileNo" value="<%=manageAdminDtBean.getMobileNo()%>" id="txtMobileNumber"  type="text" style="width: 200px;" maxlength="16"/><span id="mNo" style="color: grey;"> (Mobile No. format should be e.g 12345678)</span>
                                            <input type="hidden" id="hdnMobileNo" value="<%= manageAdminDtBean.getMobileNo()%>"/>
                                            <span id="mobMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                        </td>
                                    </tr>
                                    <% }%>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td align="left"><label class="formBtn_1"><input id="Update" name="Update" value="Update" type="submit" onclick="return chkUniqueNationalID();" /></label></td>
                                    </tr>
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script type="text/javascript">
            function chkUnique(){
                alert('rishita');
                var txtHiddenNationalId = document.getElementById('txtHiddenNationalId').value;
                var txtNationalId = document.getElementById('txtNationalId').value;
                if(txtHiddenNationalId != txtNationalId){
                    if(txtNationalId != '' || document.getElementById('txtFullName').value != ''){
                        $('span.#errMsg').css("color","red");
                        $('span.#errMsg').html("Checking for unique CID No ...");
                        $.post("<%=request.getContextPath()%>/CommonServlet", {nationalId:$.trim($('#txtNationalId').val()),flag:'true',funName:'verifyNationalId'},  function(j){
                            if(j.toString().indexOf("OK", 0)!=-1){
                                $('span.#errMsg').css("color","green");  //not equal
                            }
                            else if(j.toString().indexOf("National", 0)!= -1){
                                $('span.#errMsg').css("color","red");
                            }
                            $('span.#errMsg').html(j);
                        });
                    }
                }
                
            }
            function chkUniqueNationalID(){
                if(validCheckMno){
                }else{
                    return false;
                }
                if(document.getElementById('errMsg').innerHTML == 'Checking for unique CID No ...' || document.getElementById('errMsg').innerHTML == 'National-ID already exists' || document.getElementById('mobMsg').innerHTML  == 'Checking for unique Mobile No...'){
                    return false;
                }
                else{
                    //return true;
                }
            }
        </script>
        <%
                    }
        %>
        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
    <%
                manageAdminSrBean = null;
                adminMasterDtBean = null;
    %>
</html>
