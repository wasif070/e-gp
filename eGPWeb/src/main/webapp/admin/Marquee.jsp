<%-- 
    Document   : Marquee
    Created on : Nov 18, 2011, 10:50:56 AM
    Author     : Rajesh Singh
--%>

<%@page import="java.net.URLDecoder"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk" %>
<%@page import="com.cptu.egp.eps.model.table.TblTempCompanyMaster" %>
<%@page import="com.cptu.egp.eps.web.utility.BanglaNameUtils"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Marquee Management</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>

        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js">

            /***********************************************
             * All Levels Navigational Menu- (c) Dynamic Drive DHTML code library (http://www.dynamicdrive.com)
             * This notice MUST stay intact for legal use
             * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
             ***********************************************/

        </script>

        <script type="text/javascript">
            //<div class='reqF_1'>Please enter Marquee Text.</div>
            //<div class='reqF_1'>Please enter Marquee Start Date and Time.</div>
            //<div class='reqF_1'>Please enter Marquee End Date and Time.</div>
            //<div class='reqF_1'>Marquee End date should be greater than Start Date</div>
            $(document).ready(function(){

                $('#frmMarquee').validate({

                    rules:{
                        cmbDispLocation:{required:true},
                        txtMqeTxt:{required:true,maxlength:500},
                        txtMqeStDt:{required:true},
                        txtMqeEndDt:{required:true,CompareToForGreater:"#txtMqeStDt"}

                    },

                    messages:{

                        cmbDispLocation:{required:"<div class='reqF_1'>Please select Display Location</div>"},
                        txtMqeTxt:{required:"",
                            maxlength:"<div class='reqF_1'>Only 500 characters are allowed</div>"},
                        txtMqeStDt:{required:""
                        },
                        txtMqeEndDt:{required:"",
                            CompareToForGreater:""}
                    },
                    errorPlacement: function(error, element) {
                        if (element.attr("name") == "txtMqeStDt")
                            error.insertAfter("#imgCal");
                        else if (element.attr("name") == "txtMqeEndDt")
                            error.insertAfter("#imgCal1");
                        else
                            error.insertAfter(element);
                    }

                });

                if($("#select3 option:selected").text()=="Home Page"){
                    document.getElementById("hdndashboard").value="Home Page";
                        $("#tdheading").css("display","none");

                        $("#tdtext").css("display","none");
                }else
                    {
                        document.getElementById("hdndashboard").value="";
                        $("#tdheading").css("display","inline");

                        $("#tdtext").css("display","inline");
                    }


            })
            
        </script>
    </head>
    <body>
         <%
                    boolean flag = false;
                    String ms = "";
                    CommonXMLSPService commonXMLSPService = (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                    String userID="0";
                    if(session.getAttribute("userId")!=null){
                    userID=session.getAttribute("userId").toString();
                    }
                    commonXMLSPService.setLogUserId(userID);

                    String mid = "", poBy = "";
                    if (!request.getParameter("mid").equals("")) {
                        mid = request.getParameter("mid");
                    }

                    if (session.getAttribute("userId") != null) {
                        poBy = session.getAttribute("userId").toString();
                    }
                    
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
                    //This condition is used to submit data in database, this is button click event.
                    if (request.getParameter("buttonMarquee") != null) {
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");

                        java.text.SimpleDateFormat format1 = new java.text.SimpleDateFormat("yyyy-mm-dd");
                        java.text.SimpleDateFormat format2 = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm");
                        Date d1 = format2.parse(request.getParameter("txtMqeStDt"));
                        Date d2 = format2.parse(request.getParameter("txtMqeEndDt"));

                        if (!request.getParameter("op").equals("edit")) {
                            String dtXml = "";
                            String strTemp = request.getParameter("txtMqeTxt");
                            strTemp = strTemp.replace("<p>", "");
                            strTemp = strTemp.replace("</p>", "");
                            if(request.getParameter("hdndashboard")!=null && !request.getParameter("hdndashboard").equals("Home Page")){

                            dtXml = "<root><tbl_MarqueeMaster marqueeText=\"" + URLEncoder.encode(strTemp,"UTF-8").replaceAll("'", "''") + "\" marqueeStartDt=\"" + format.format(d1) + "\" "
                                    + "marqueeEndDt=\"" + format.format(d2) + "\" locationId=\"" + request.getParameter("cmbDispLocation") +
                                    "\" userTypeId=\"" + request.getParameter("cmbDashbrd") + "\" createdBy=\"" + poBy + "\" createdTime=\"" + format.format(new Date()) + "\"/></root>";
                            }else{
                            dtXml = "<root><tbl_MarqueeMaster marqueeText=\"" + URLEncoder.encode(strTemp,"UTF-8").replaceAll("'", "''") + "\" marqueeStartDt=\"" + format.format(d1) + "\" "
                                    + "marqueeEndDt=\"" + format.format(d2) + "\" locationId=\"" + request.getParameter("cmbDispLocation") +
                                    "\" userTypeId=\"" + "1" + "\" createdBy=\"" + poBy + "\" createdTime=\"" + format.format(new Date()) + "\"/></root>";
                            }
                            String action = "Create Marquee";
                            try{
                                CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("insert", "tbl_MarqueeMaster", dtXml, "").get(0);
                                flag = commonMsgChk.getFlag();
                                ms = "truei";
                                }catch(Exception e){
                                    action = "Error in "+action+" "+e.getMessage();
                                }finally{
                                    makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(session.getAttribute("userId").toString()), "userId", EgpModule.Content.getName(), action, "");
                                    action = null;
                                }
                            /*if(commonMsgChk.getFlag() ==true){
                            response.sendRedirect("MarqueeListing.jsp?msg=truei");
                            }else
                                {
                                response.sendRedirect("Marquee.jsp?mid="+request.getParameter("mid")+"&op="+request.getParameter("op")+"&msg=false");
                            }*/
                        } else if (request.getParameter("mid") != null && !request.getParameter("mid").equals("") && request.getParameter("op").equals("edit")) {
                            String dtXml = "";
                            String strTemp = request.getParameter("txtMqeTxt");
                            strTemp = strTemp.replace("<p>", "");
                            strTemp = strTemp.replace("</p>", "");
                            if(request.getParameter("hdndashboard")!=null && !request.getParameter("hdndashboard").equals("Home Page")){
                            dtXml = "<root><tbl_MarqueeMaster marqueeId=\"" + request.getParameter("mid") + "\" marqueeText=\"" + URLEncoder.encode(strTemp,"UTF-8").replaceAll("'", "''") + "\" marqueeStartDt=\"" + format.format(d1) + "\" "
                                    + "marqueeEndDt=\"" + format.format(d2) + "\" locationId=\"" + request.getParameter("cmbDispLocation") +
                                    "\" userTypeId=\"" + request.getParameter("cmbDashbrd") + "\" createdBy=\"" + poBy + "\" createdTime=\"" + format.format(new Date()) + "\"/></root>";
                                    }else{
                            dtXml = "<root><tbl_MarqueeMaster marqueeId=\"" + request.getParameter("mid") + "\" marqueeText=\"" + URLEncoder.encode(strTemp,"UTF-8").replaceAll("'", "''") + "\" marqueeStartDt=\"" + format.format(d1) + "\" "
                                    + "marqueeEndDt=\"" + format.format(d2) + "\" locationId=\"" + request.getParameter("cmbDispLocation") +
                                    "\" userTypeId=\"" + "1" + "\" createdBy=\"" + poBy + "\" createdTime=\"" + format.format(new Date()) + "\"/></root>";
                                    }
                             String action = "Edit Marquee";
                            try{
                                CommonMsgChk commonMsgChk = commonXMLSPService.insertDataBySP("update", "tbl_MarqueeMaster", dtXml, "").get(0);
                                flag = commonMsgChk.getFlag();
                                ms = "trueu";
                            }catch(Exception e){
                                action = "Error in "+action+" "+e.getMessage();
                            }finally{
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), Integer.parseInt(session.getAttribute("userId").toString()), "userId", EgpModule.Content.getName(), action, "");
                                action = null;
                            }
                            
                            
                            
                            
                        }
                        
                        if(flag){
                            response.sendRedirect("MarqueeListing.jsp?msg="+ms+"");
                            }else
                                {
                            response.sendRedirect("Marquee.jsp?mid="+request.getParameter("mid")+"&op="+request.getParameter("op")+"&msg=false");
                            }
                        }
        %>
        
            <div class="dashboard_div">
                <!--Dashboard Header Start-->
                <div class="topHeader">
                    <%@include file="../resources/common/AfterLoginTop.jsp" %>
                </div>
                <!--Dashboard Header End-->
                <!--Dashboard Content Part Start-->
                
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                        
                        <td class="contentArea" valign="top">
                            <div class="pageHead_1"><%if (request.getParameter("op").equals("edit")) {out.print("Edit");}else{out.print("Create");}%> Marquee</div>
                            <%
                        // Variable tenderId is defined by u on ur current page.
                                            String msg ="";
                                             msg = request.getParameter("msg");
                            %>
                            
                            <% if (msg != null && msg.contains("false")) {%>
                            <div>&nbsp;</div>
                            <div class="responseMsg errorMsg t_space">Insert Operation failed, Please try again later.</div>
                            <div>&nbsp;</div>
                            <%  }%>
                            
                            <%
                                        TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                                        tenderCommonService.setLogUserId(userID);
                            %>
                            <form id="frmMarquee" action="Marquee.jsp?mid=<%=request.getParameter("mid")%>&op=<%=request.getParameter("op")%>" method="POST">
                            
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td style="font-style: italic" class="ff t-align-left" colspan="4">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                </tr>
                                <tr>
                                    <td colspan="4"></td>
                                </tr>
                                <tr>
                                    <td class="ff" width="21%">Display Location : <span class="mandatory">*</span></td>
                                    <td width="37%">
                                        <select name="cmbDispLocation" class="formTxtBox_1" id="select3" style="width:200px;" onchange="select();">
                                            <%  List<SPTenderCommonData> cmblocList = tenderCommonService.returndata("getLocationCombo", null, null);

                                                        if (request.getParameter("op").equals("edit")) {
                                                            List<SPTenderCommonData> list = tenderCommonService.returndata("getmarqueebyID", request.getParameter("mid"), null);
                                                            if (!list.isEmpty()) {
                                                                for (SPTenderCommonData sptcd : cmblocList) {
                                                                    if (sptcd.getFieldName1().equals(list.get(0).getFieldName5().toString().trim())) {
                                            %>
                                            <option value="<%=sptcd.getFieldName1()%>" selected="true"><%=sptcd.getFieldName2()%></option>
                                            <%} else {
                                            %>
                                            <option value="<%=sptcd.getFieldName1()%>"><%=sptcd.getFieldName2()%></option>
                                            <%
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    } else {
                                                                                                        for (SPTenderCommonData sptcd : cmblocList) {

                                            if(sptcd.getFieldName2().equals("Home Page")){%>
                                            <option value="<%=sptcd.getFieldName1()%>"><%=sptcd.getFieldName2()%></option>
                                            <%}}
                                                        }%>
                                        </select>      </td>
                                    <td width="21%" class="ff">Marquee Display <br/>Start Date and Time : <span class="mandatory">*</span></td>
                                    <td width="26%">
                                        <%
                                                    if (request.getParameter("op").equals("edit")) {
                                                        List<SPTenderCommonData> list = tenderCommonService.returndata("getmarqueebyID", request.getParameter("mid"), null);
                                                        if (!list.isEmpty()) {%>
                                        <input name="txtMqeStDt" type="text" class="formTxtBox_1" id="txtMqeStDt" style="width:97px;" readonly="true" onclick ="GetCal('txtMqeStDt','txtMqeStDt');" value="<%=list.get(0).getFieldName3()%>" onblur="blockMessage()"/>
                                        <%}
                                                                                            } else {%>
                                        <input name="txtMqeStDt" type="text" class="formTxtBox_1" id="txtMqeStDt" style="width:97px;" readonly="true" onclick ="GetCal('txtMqeStDt','txtMqeStDt');"onblur="blockMessage()"/>
                                        <%}%>
                                        <img id="imgCal" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick ="GetCal('txtMqeStDt','imgCal');"/>
                                    <span id="spanstdt" class="reqF_1" ></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff" > <div id="tdheading" style="display: inline;"> Dashboard : </div></td>
                                    <td class="ff">
                                        <div id="tdtext" style="display: inline;">
                                        <select name="cmbDashbrd" class="formTxtBox_1" id="select4" style="width:200px;">
                                            <%  List<SPTenderCommonData> cmbPcList = tenderCommonService.returndata("getprojectcodeCombo", null, null);
                                                        List<SPTenderCommonData> list = tenderCommonService.returndata("getmarqueebyID", request.getParameter("mid"), null);
                                                        if (request.getParameter("op").equals("edit")) {

                                                            if (!list.isEmpty()) {
                                                                for (SPTenderCommonData sptcdpc : cmbPcList) {
                                                                    if (sptcdpc.getFieldName1().equals(list.get(0).getFieldName6().toString().trim())) {
                                            %>
                                            <option value="<%=sptcdpc.getFieldName1()%>" selected="true"><%=sptcdpc.getFieldName2()%></option>
                                            <%} else {
                                            %>
                                            <option value="<%=sptcdpc.getFieldName1()%>"><%=sptcdpc.getFieldName2()%></option>
                                            <%
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    } else {
                                                                                                        for (SPTenderCommonData sptcdpc : cmbPcList) {%>
                                            <option value="<%=sptcdpc.getFieldName1()%>"><%=sptcdpc.getFieldName2()%></option>
                                            <%}
                                                        }%>
                                        </select>
                                        </div>
                                    </td>
                               
                                    <td class="ff">Marquee Display <br/>End Date and Time : <span class="mandatory">*</span></td>
                                    <td width="26%">
                                        <%
                                                    if (request.getParameter("op").equals("edit")) {
                                                        // List<SPTenderCommonData> list = tenderCommonService.returndata("getmarqueebyID", request.getParameter("mid"), null);
                                                        if (!list.isEmpty()) {%>
                                                        <input name="txtMqeEndDt" type="text" class="formTxtBox_1" id="txtMqeEndDt" style="width:97px;" readonly="true"  onclick ="GetCal('txtMqeEndDt','txtMqeEndDt');" value="<%=list.get(0).getFieldName4()%>" onblur="blockMessage()"/>
                                        <%}
                                                                                            } else {%>
                                        <input name="txtMqeEndDt" type="text" class="formTxtBox_1" id="txtMqeEndDt" style="width:97px;" readonly="true" onclick ="GetCal('txtMqeEndDt','txtMqeEndDt');" onblur="blockMessage()"/>
                                        <%}%>
                                        <img id="imgCal1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;" onclick ="GetCal('txtMqeEndDt','imgCal1');"/>
                                    <span id="spaneddt" class="reqF_1" ></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff">Marquee Text : <span>*</span></td>
                                    <td colspan="3">
                                        <%
                                                    if (request.getParameter("op").equals("edit")) {
                                                        //List<SPTenderCommonData> list = tenderCommonService.returndata("getmarqueebyID", request.getParameter("mid"), null);
                                                        if (!list.isEmpty()) {%>
                                        <textarea rows="5" id="textarea4" name="txtMqeTxt" onblur="ReplaceMyChar(this);" class="formTxtBox_1" style="width:80%;" ><%=URLDecoder.decode(list.get(0).getFieldName2(),"UTF-8")%></textarea>
                                        <%}
                                                                                            } else {%>
                                        <textarea rows="5" id="textarea4" name="txtMqeTxt" onblur="ReplaceMyChar(this);" class="formTxtBox_1" style="width:80%;"></textarea>
                                        <%}%>
                                        <script type="text/javascript">
                                          CKEDITOR.replace( 'textarea4',{
                                               toolbar : "Basic"
                                          });
                                        </script>
                                        <span id="ckeditor" class="reqF_1" ></span>
                                        <input type="hidden" id="hdndashboard" name="hdndashboard" />
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4" class="t-align-center">
                                        <label class="formBtn_1">
                                            <input type="submit" name="buttonMarquee" id="buttonMarquee" value="Submit" onclick="return validate()"/>
                                        </label>
                                    </td>
                                </tr>
                            </table>
                                     </form>
                        </td>
                       
                    </tr>
                </table>



                <div>&nbsp;</div>

                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <!--Dashboard Footer End-->
            </div>
         <script>
                var obj = document.getElementById('lblMarqueeCreate');
                if(obj != null){
                    if(obj.innerHTML == 'Create'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
                <script>
                var headSel_Obj = document.getElementById("headTabContent");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<script type="text/javascript">
    function select(){
                if($("#select3 option:selected").text()=="Home Page"){
                    document.getElementById("hdndashboard").value="Home Page";
                        $("#tdheading").css("display","none");

                        $("#tdtext").css("display","none");
                }else
                    {
                        document.getElementById("hdndashboard").value="";
                        $("#tdheading").css("display","inline");

                        $("#tdtext").css("display","inline");
                    }
            }

            //This function is used to call calender on this page.
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: true,
                    dateFormat:"%d/%m/%Y %H:%M",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

            function ReplaceMyChar(mqText)
            {
                var txt=mqText.value;
                txt = txt.replace("’", "'");
                mqText.value = txt;
            }
            function validate()
            {
                flag = true;
                if($.trim(CKEDITOR.instances.textarea4.getData()) == ""){
                    document.getElementById("ckeditor").innerHTML = "Please enter the Marquee Text";
                    flag = false;
                }                
                var ckeditorvalue = CKEDITOR.instances.textarea4.getData();                
                if(ckeditorvalue.length > 507)
                {
                    document.getElementById("ckeditor").innerHTML = "Only 500 characters are allowed";
                    flag = false;
                }
                if(document.getElementById("txtMqeStDt").value=="")
                {
                    document.getElementById("spanstdt").innerHTML = "Please enter Marquee Start Date and Time"
                    flag = false;
                }
                if(document.getElementById("txtMqeEndDt").value=="")
                {
                    document.getElementById("spaneddt").innerHTML = "Please enter Marquee End Date and Time"
                    flag = false;
                }
                if(document.getElementById("txtMqeStDt").value!="" && document.getElementById("txtMqeEndDt").value!="")
                {
                var startdate = document.getElementById("txtMqeStDt").value;
                var enddate =  document.getElementById("txtMqeEndDt").value;
                var FirstDate = startdate.split(" ");
                var SecondDate = enddate.split(" ");
                var firstDate = FirstDate[0];
                var secondDate = SecondDate[0];

                var fdate = firstDate.split('/')  //Date and month split
                var fHr= fdate[2].split(' ');  //Year and time split

                var sDate = secondDate.split('/');
                var sHr = sDate[2].split(' ');

                if(fHr[1] == undefined){
                    var valueFirstDt= new Date(fHr[0], fdate[1]-1, fdate[0]);
                }
                if(sHr[1] == undefined){
                    var valueSecondDt= new Date(sHr[0], sDate[1]-1, sDate[0]);
                }
                var d = new Date();
                if(fHr[1] == undefined){
                    var todaydate = new Date(d.getFullYear(), d.getMonth(), d.getDate());
                }
                if(Date.parse(todaydate) > Date.parse(valueFirstDt))
                {
                    document.getElementById('spanstdt').innerHTML='<br/>Marquee Start Date should be current or future date';
                    flag = false;
                }
                if(Date.parse(todaydate) > Date.parse(valueSecondDt))
                {
                    document.getElementById('spaneddt').innerHTML='<br/>Marquee End Date should be current or future date';
                    flag = false;
                }

                if(Date.parse(valueFirstDt) > Date.parse(valueSecondDt)){
                    document.getElementById('spaneddt').innerHTML='<br/>Marquee End date should be greater than Start Date';
                    flag = false;
                }
                }
             return flag;
            }
            function blockMessage()
            {
                if(document.getElementById("txtMqeStDt").value!="")
                {
                    document.getElementById("spanstdt").innerHTML = "";
                }
                if(document.getElementById("txtMqeEndDt").value!="")
                {
                    document.getElementById("spaneddt").innerHTML = "";
                }
            }
            $(document).ready(function() {
                $(document).ready(function() {
                CKEDITOR.instances.textarea4.on('blur', function()
                {
                    if(CKEDITOR.instances.textarea4.getData() != 0)
                    {
                            document.getElementById("ckeditor").innerHTML="";
                    }
                });
            });
            });
</script>