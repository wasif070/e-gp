<%-- 
    Document   : ScheduleBankBranchCreation
    Created on : Oct 23, 2010, 7:37:24 PM
    Author     : rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Schedule Bank Creation</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmBankScheduleBranch").validate({
                    rules: {
                        Bank: { required: true},
                        BranchName:{required:true,maxlength:50},
                        SwiftCode:{maxlength:20},
                        Address:{required:true,maxlength:300},
                        Country:{required:true},
                        District:{required:true},
                        City:{required:true,maxlength:50},
                        upJilla:{required:true,maxlength:50},
                        PostCode:{number:true,minlength:10},
                        PhoneNumber:{digits:true,maxlength:20},
                        FaxNumber:{digits:true,maxlength:20}

                    },
                    messages: {
                        Bank: { required: "<div class='reqF_1'>Please select Bank.</div>"},

                        BranchName:{required:"<div class='reqF_1'>Please Enter branch name</div>",
                            maxlength:"<div class='reqF_1'>Maximum 50 characters are allowed</div>"
                        },

                        SwiftCode:{maxlength:"<div class='reqF_1'>Maximum 20 characters are allowed.</div>"
                        },

                        Address:{required:"<div class='reqF_1'>Please enter address</div>",
                            maxlength:"<div class='reqF_1'>Maximum 300 characters are allowed.</div>"
                        },

                        Country:{required:"<div class='reqF_1'>Please select country.</div>"
                        },

                        District:{required:"<div class='reqF_1'>Please select Dzongkhag / District.</div>"
                        },
                        
                        City:{required:"<div class='reqF_1'>Please Enter City name.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 50 characters are allowed.</div>"
                        },
                    
                        upJilla:{required:"<div class='reqF_1'>Please Enter Gewog.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 50 characters are allowed.</div>"
                        },
                        
                        PostCode:{number: "<div class='reqF_1'>Only numbers (0-9)are allowed.</div>",
                            minlength:"<div class='reqF_1'>Minimum 4 characters are required.</div>"
                        },
                        
                        PhoneNumber:{digits:"<div class='reqF_1'>Please Enter Numbers only.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 20 characters are allowed.</div>"
                        },

                        FaxNumber:{digits:"<div class='reqF_1'>Please Enter Numbers only.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 20 characters are allowed.</div>"
                        }
                    }
                }
            );
            });

        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="/resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <div class="pageHead_1">Schedule Bank Creation</div>
                            <!--Page Content Start-->
                            <form id="frmBankScheduleBranch" action="" name="BankScheduleBranch" method="POST">
                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10">

                                    <tr>
                                        <td class="ff" width="200">Select Bank<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmdBank" name="Bank" style="width: 200px">
                                                <option value=""> </option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Name of Branch<span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtBranchName" name="BranchName" style="width: 200px;" type="text" maxlength="50"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Swift Code</td>
                                        <td><input class="formTxtBox_1" id="txtSwiftCode" name="SwiftCode" style="width: 200px;" type="text" maxlength="20"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Address<span>*</span></td>
                                        <td><textarea class="formTxtBox_1" id="taAddress" name="Address" style="width: 200px" ></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Country<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbCountry" name="Country" style="width: 200px">
                                                <option value=""> </option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Dzongkhag / District<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbDistrict" name="District" style="width: 200px">
                                                <option value=""> </option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">City<span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtCity" name="City" style="width: 200px;" type="text" maxlength="50"/>
                                        </td>
                                    </tr
                                    <tr>
                                        <td class="ff" width="200">upJilla<span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtupJilla" name="upJilla" style="width: 200px;" type="text" maxlength="50"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Post Code<span>*</span></td>
                                        <td><input id="txtPostCode" class="formTxtBox_1" name="PostCode" style="width: 200px;" type="text" maxlength="10"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Phone Number</td>
                                        <td><input class="formTxtBox_1" id="txtPhoneNumber" style="width: 200px;" name="PhoneNumber" type="text" maxlength="20"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Fax Number</td>
                                        <td><input class="formTxtBox_1" id="txtFaxNumber" style="width: 200px;" name="FaxNumber" type="text" maxlength="20"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="formBtn_1" align="center"><input id="btnSubmit" name="Submit" value="Submit" type="submit"/></td>

                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
