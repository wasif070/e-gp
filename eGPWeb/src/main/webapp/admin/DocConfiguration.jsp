<%-- 
    Document   : DocConfiguration
    Created on : Dec 3, 2010, 3:02:51 PM
    Author     : Naresh.Akurathi
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigMasterSrBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Document Management Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        
<script type="text/javascript">
    function Invalid(){
        var textvalue = document.getElementById("singleFile").value;
         if(textvalue!=null && textvalue!=""){
             if(!textvalue.match(/^[0-9]+$/)){
                document.getElementById("tfsinmb").innerHTML="please enter numeric values";
                return false;
             }else{document.getElementById("tfsinmb").innerHTML="";}
         }}
    function blockmessage()
    {
        var textvalue = document.getElementById("singleFile").value;
        if(textvalue!=null)
        {
            if(textvalue.match(/^[0-9]+$/))
            {
                document.getElementById("tfsinmb").innerHTML="";
            }
        }
    }
   
</script>
<script type="text/javascript">
    $(function() {
                $('#userType').change(function() {
                        $.post("<%=request.getContextPath()%>/CommonServlet", {userType:$('#userType').val(),funName:'configDetail'}, function(j){
                            var split =new Array();
                                split = j.toString().split('@$',4);
                                $('#fileType').val(split[0]);
                            $('#singleFile').val(split[1]);
                            $('#totalFile').val(split[2]);
                            $('#configId').val(split[3]);
                        });
                });
            });


</script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('#button').attr("disabled", false);
                
                //Nitish Start :: check space is allowed or not
                
                jQuery.validator.addMethod("noSpace", function(value, element) 
                { 
                    return value.indexOf(" ") < 0 && value != ""; 
                    
                }, "<div class='reqF_1'> No space is allowed </div>");
                
                //Nitish End
                
                
                $("#frmDocConfig").validate({
                    rules:{
                        fileType:{required:true,noSpace: true},
                        singleFile:{required:true,max:4,digits: true},
                        totalFile:{required:true,max:50,digits: true},
                        userType:{required:true}
                    },
                    messages:{
                        fileType:{required:"<div class='reqF_1'>Please enter File Type</div>"
                        },
                        singleFile:{required:"<div class='reqF_1'>Please enter Single File Size</div>",
                            max:"<div class='reqF_1'>Maximum 4 MB is allowed</div>",
                            digits:"<div class='reqF_1'>Number with decimal not allowed</div>"
                        },
                        totalFile:{
                            required:"<div class='reqF_1'>Please enter Total File Size</div>",
                            max:"<div class='reqF_1'>Maximum 50 MB is allowed</div>",
                            digits:"<div class='reqF_1'>Number with decimal not allowed</div>"
                        },
                        userType:{
                            required:"<div class='reqF_1'>Please select User Type</div>"
                        }
                    }

                });

            });
        </script>
    </head>
    <body>

        <%        ConfigMasterSrBean configMasterSrBean = new ConfigMasterSrBean();
                    configMasterSrBean.setLogUserId(session.getAttribute("userId").toString()); 
                                    String msg = "";
                                   // List govlt = configMasterSrBean.getConfigMasterDetails("government");
                                   // if (!govlt.isEmpty()) {
                                     //   Iterator git = govlt.iterator();
                                       // TblConfigurationMaster configMaster1 = new TblConfigurationMaster();
                                       // if (git.hasNext()) {
                                       //     configMaster1 = (TblConfigurationMaster) git.next();
                       
                                      //  }
                                   // }


                                   // List commonlt = configMasterSrBean.getConfigMasterDetails("common");
                                   // if (!commonlt.isEmpty()) {
                                    //    Iterator cit = commonlt.iterator();
                                    //    TblConfigurationMaster configMaster2 = new TblConfigurationMaster();
                                    //    if (cit.hasNext()) {
                                     //       configMaster2 = (TblConfigurationMaster) cit.next();

                                       // }
                                   // }
                            TblConfigurationMaster configMasterEdit = new TblConfigurationMaster();
                                    int configid =0;
                                    boolean isEdit = false;
                                    List forEdit = null;
                                    if(request.getParameter("cId")!=null){
                                            configid =Integer.parseInt(request.getParameter("cId"));
                                            isEdit = true;
                                             forEdit = configMasterSrBean.getConfigMasterDetails(configid);
                                             if(forEdit!=null){
                                                if(forEdit.size()>0){
                                                  configMasterEdit =   new TblConfigurationMaster();
                                             configMasterEdit = (TblConfigurationMaster)forEdit.get(0);
                                                }
                                             }

                                        }


                                    if ("Submit".equals(request.getParameter("button"))||"Update".equals(request.getParameter("button"))) {
                                        
                                        short configId = Short.parseShort(request.getParameter("configId"));
                                        
                                        String userType = request.getParameter("userType");
                                        String fileType = request.getParameter("fileType");
                                        byte singleFile = Byte.parseByte(request.getParameter("singleFile"));
                                        byte totalFile = Byte.parseByte(request.getParameter("totalFile"));

                                        /*TblConfigurationMaster tblconfigMaster = new TblConfigurationMaster();
                                        tblconfigMaster.setConfigId(configId);
                                        tblconfigMaster.setAllowedExtension(fileType);
                                        tblconfigMaster.setFileSize(singleFile);
                                        tblconfigMaster.setTotalSize(totalFile);*/

                                        configMasterSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()));
                                        msg = configMasterSrBean.updateConfigMaster(fileType, userType, singleFile, totalFile);
                                        
                                        if (msg.equals("Updated")) {
                                            msg = "Document Management Configuration Successfully Updated";
                                            response.sendRedirect("DocConfigurationView.jsp?msg=" + msg);
                                        }

                                    }/*if("Create".equalsIgnoreCase(request.getParameter("button"))){
                                        String userType = request.getParameter("userType");
                                        String fileType = request.getParameter("fileType");
                                        byte singleFile = Byte.parseByte(request.getParameter("singleFile"));
                                        byte totalFile = Byte.parseByte(request.getParameter("totalFile"));
                                         msg = configMasterSrBean.insertConfigMaster(fileType, userType, singleFile, totalFile);
                                        }
                                    if (msg.equalsIgnoreCase("ok")) {
                                            msg = "Document Management Configuration Successfully Created";
                                            response.sendRedirect("DocConfigurationView.jsp?msg=" + msg);
                                        }*/
                        %>

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
                        <!--Dashboard Header End-->
                        <!--Dashboard Content Part Start-->
                        <div class="pageHead_1">Document Management Configuration</div>
                        

                        <form id="frmDocConfig" action="DocConfiguration.jsp" method="post">
                            <input type="hidden" name="configId" id="configId" value="<%=configMasterEdit.getConfigId()%>"/>
                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                <tr>
                                    <td style="font-style: italic;" colspan="2" class="t-align-right"><normal>Fields marked with (<span class="mandatory">*</span>) are mandatory</normal></td>
                                </tr>
                                <%

                                         //   List lt = configMasterSrBean.getConfigMasterDetails("tenderer");
                                            //List userType = configMasterSrBean.getAllUserType();
                                          
                                           // if (!lt.isEmpty()) {
                                          //      Iterator it = lt.iterator();
                                          //      TblConfigurationMaster configMaster = new TblConfigurationMaster();
                                           //     if (it.hasNext()) {
                                            //        configMaster = (TblConfigurationMaster) it.next();
                                                        
                                %>

<!--                                <input type="hidden" name="tendId" id="tendId" value="<%//=configMaster.getConfigId()%>"/>
                                <input type="hidden" name="tend1" id="tend1" value="<%//=configMaster.getAllowedExtension()%>"/>
                                <input type="hidden" name="tend2" id="tend2" value="<%//=configMaster.getFileSize()%>"/>
                                <input type="hidden" name="tend3" id="tend3" value="<%//=configMaster.getTotalSize()%>"/>-->

                                
                                <tr>
                                    <td class="ff" width="23%">Permissible File Types : <span class="mandatory">*</span></td>
                                    <td>
                                    <%if(isEdit){%>
                                    <input name="fileType" type="text" class="formTxtBox_1" id="fileType" maxlength="60" style="width:250px;" value="<%=configMasterEdit.getAllowedExtension()%>"/>
                                    <%}else{%>
                                    <input name="fileType" type="text" class="formTxtBox_1" id="fileType" style="width:250px;" value="<%//=configMaster.getAllowedExtension()%>"/>
                                    <%}%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff" width="23%">Type of User : <%if(!isEdit){%><span class="mandatory">*</span><%}%></td>
                                    <%String userName = null;%>
                                    
                                    <td>
                                        <%if(isEdit){
                                            if(configMasterEdit.getUserType().toString().equalsIgnoreCase("tenderer")){
                                                                        userName = "Tenderer/Consultant";
                                                                    }
                                            else if(configMasterEdit.getUserType().toString().equalsIgnoreCase("Officer")){
                                                                        userName = "Officer";
                                                                    }
                                            else if(configMasterEdit.getUserType().toString().equalsIgnoreCase("egpadmin")){
                                                                        userName = "e-GP Admin";
                                                                    }
                                            else if(configMasterEdit.getUserType().toString().equalsIgnoreCase("common")){
                                                                        userName = "Common";
                                                                    }

                                        %>
                                        <label><%=userName%></label>
                                        <%--<label><%=configMasterEdit.getUserType()%></label>--%>
                                        <input type="hidden" name="userType" value="<%=configMasterEdit.getUserType()%>" />
                                        <%}else{%>
                                        <select name="userType" class="formTxtBox_1" id="userType" style="width:200px;" >
                                            <option value="">---Select User Type---</option>
                                            <%
                                            
                                              List userType = configMasterSrBean.getConfigUserType();
                                                    if(userType!=null){
                                                        if(userType.size()>0){
                                                            for(int i=0;i<userType.size();i++){
                                                                if(userType.get(i).toString().equals("tenderer")){
                                                                        userName = " Bidder/Consultant";
                                                                    }
                                                                else if(userType.get(i).toString().equalsIgnoreCase("officer")){
                                                                        userName = "Officer";
                                                                    }
                                                                else if(userType.get(i).toString().equalsIgnoreCase("egpadmin")){
                                                                        userName = "e-GP Admin";
                                                                    }
                                                                else if(userType.get(i).toString().equalsIgnoreCase("common")){
                                                                        userName = "Common";
                                                                    }
                                                                        
                                             %>
                                             <%--<%=userType.get(i)%>--%>

                                            <option value="<%=userType.get(i).toString().toLowerCase()%>"><%=userName%></option>
                                             <%
                                             
                                                                }
                                                            }
                                                        }
                                            %>
<!--                                            <option value="government"> Government user </option>
                                            <option selected="true" value="tenderer"> Tenderer </option>-->
<!--                                            <option value="common"> Common </option>-->
                                        </select> 
                                        <%
                                        if(userType!=null){
                                                    userType.clear();
                                                    userType = null;
                                                  }
                                          }%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="ff" width="23%">Single File Size (In MB) : <span class="mandatory">*</span></td>
                                    <td>
                                        <%if(isEdit){%>
                                        <input name="singleFile" type="text" class="formTxtBox_1" id="singleFile" style="width:195px;" value="<%=configMasterEdit.getFileSize()%>"onblur="blockmessage()"/>
                                        <%}else{%>
                                        <input name="singleFile" type="text" class="formTxtBox_1" id="singleFile" style="width:195px;" value="<%//=configMaster.getFileSize()%>"onblur="blockmessage()"/>
                                        <%}%><span id="tfsinmb" class="reqF_1" ></span>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="ff" width="23%">Total File Size (In MB) : <span class="mandatory">*</span></td>
                                    <td>
                                        <%if(isEdit){%>
                                        <input name="totalFile" type="text" class="formTxtBox_1" id="totalFile" style="width:195px;" value="<%=configMasterEdit.getTotalSize()%>"/>
                                        <%}else{%>
                                        <input name="totalFile" type="text" class="formTxtBox_1" id="totalFile" style="width:195px;" value="<%//=configMaster.getTotalSize()%>"/>
                                        <%}%>
                                    </td>
                                </tr>

                                <%

                                            //    }
                                           // }
                                %>

                                <tr>
                                    <td width="20%">&nbsp;</td>
                                    <%//if(userType.size()>0){
                                       // if(isEdit){
                                    %>
<!--                                    <td class="t-align-left">
                                        <label class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Create"/>
                                        </label>
                                    </td>-->
                                    <%//}else{
                                         //   }else{
                                    %>
                                    <%if(isEdit){%>
                                    <td class="t-align-left">
                                        <label class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Update" disabled/>
                                        </label>
                                    </td>
                                    <%}else{%>
                                    <td class="t-align-left">
                                        <label class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Submit" disabled />
                                        </label>
                                    </td>
                                    <%}%>
                                    <%//}
                                            //}
                                    %>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <%@include file="/resources/common/Bottom.jsp" %>
        </div>

         <script>
                var obj = document.getElementById('lblConfigDoc');
                if(obj != null){
                    if(obj.innerHTML == 'Configure'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
        <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>

