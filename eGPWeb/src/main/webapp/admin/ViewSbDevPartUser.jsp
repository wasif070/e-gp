<%-- 
Document   : ViewSbDevPartUser
Created on : Nov 17, 2010, 7:54:13 PM
Author     : Administrator
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.model.table.TblPartnerAdminTransfer"%>
<%@page import="com.cptu.egp.eps.model.table.TblUserActivationHistory"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <jsp:useBean id="scBankCreationSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean"/>
    <jsp:useBean id="scBankPartnerAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ScBankPartnerAdminSrBean"/>
    <jsp:useBean id="partnerAdminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.PartnerAdminMasterDtBean"/>
    <jsp:setProperty name="partnerAdminMasterDtBean" property="*"/>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <%
                    String logUserId = "0";
                    if (session.getAttribute("userId") != null) {
                        logUserId = session.getAttribute("userId").toString();
                    }
                    scBankCreationSrBean.setLogUserId(logUserId);
                    scBankPartnerAdminSrBean.setLogUserId(logUserId);
                    
                   
                   
                    byte usrTypeId = 0;
                    if (!request.getParameter("userTypeId").equals("")) {
                        usrTypeId = Byte.parseByte(request.getParameter("userTypeId"));
                    }
                    String usrId = request.getParameter("userId");
                    boolean bole_fromWhere = false;
                    if (usrId.equalsIgnoreCase(session.getAttribute("userId").toString())) {
                            bole_fromWhere = true;
                        }
                    
                    scBankPartnerAdminSrBean.setAuditTrail(null);
                    
                    if (usrId != null && !usrId.equals("")) {
                        partnerAdminMasterDtBean.setUserId(Integer.parseInt(usrId));
                        partnerAdminMasterDtBean.setUserTypeId(usrTypeId);
                        partnerAdminMasterDtBean.populateInfo("User");
                    }

                    String partnerType = request.getParameter("partnerType");
                    String type = "";
                    String title = "";
                    String title1 = "";
                    String userType = "";
                    String title2 = "";
                    String from = request.getParameter("from");
                    if (from == null || from.equals("")) {
                        from = "Operation";
                    }
                    if (partnerType != null && partnerType.equals("Development")) {
                        type = "Development";
                        title = "Development Partner";
                        title1 = "Development Partner";
                        title2 = "Development Partner";
                        userType = "dev";
                    } else {
                        type = "ScheduleBank";
                        title = "Financial Institution";
                        title1 = "Financial Institution";
                        title2 = "Financial Institution Branch";
                        userType = "sb";
                    }

                    if(request.getParameter("mode") != null && request.getParameter("mode").equalsIgnoreCase("view"))
                    {
                        
                         // Coad added by Dipal for Audit Trail Log.
                        AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                        String idType="userId";
                        int auditId=Integer.parseInt(request.getParameter("userId"));
                        String auditAction="View Profile of Development Partner User";
                        if (usrTypeId == 6)
                        {
                            auditAction = "View Profile of Development Partner User";
                        }
                        if (usrTypeId == 7)
                        {
                            auditAction = "View Profile of Financial Institution User";
                        }
                        if (usrTypeId == 15)
                        {
                            auditAction = "View Profile of Financial Institution Branch Admin";
                        }
                        String moduleName=EgpModule.Manage_Users.getName();
                        String remarks="";
                        MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                        //scBankPartnerAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                         
                    }

                    String gridURL = "ManageSbDpUser.jsp?partnerType=" + type;
                    String editURL = "EditSbDevPartUser.jsp?userId=" + usrId + "&userTypeId=" + usrTypeId + "&partnerType=" + type;
                    String strTransferHistoryURL = "ViewSbDpTransferHistory.jsp?userId=" + usrId + "&userTypeId=" + usrTypeId + "&partnerType=" + type + "&from=" + from;
                    String viewaccounthistory = "ViewSbDpAccountHistory.jsp?userId=" + usrId + "&userTypeId=" + usrTypeId + "&partnerType=" + type;
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <%if (bole_fromWhere) {%>
        <title>View Profile</title>
        <%} else {%>
        <title><%=title2%> User : View</title>
        <%}%>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
    </head>


    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <%if (!bole_fromWhere) {%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                         <%}%>
                        <td class="contentArea_1">
                            <!--Page Content Start-->

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <!-- Success failure -->
                                    <!-- Result Display End-->
                                    <td>
                                        <div class="pageHead_1">
                                            <%if (bole_fromWhere) {%>
                                            View Profile
                                            <%} else {%>
                                            <%=title2%> - User Details
                                            <%}%>
                                        </div>
                                        <div class="t_space">
                                        <%
                                                String msg = request.getParameter("msg");
                                                if (msg != null && msg.equals("success")) {%>


                                            <div class="responseMsg successMsg">
                                                <%if (bole_fromWhere) {%>
                                                Profile Updated Successfully
                                                <%} else {%>
                                                <%=title1%> User <%=from%> Successfully
                                                <%}%>
                                            </div>

                                    <%}%>
                                            <%
                                                        TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
                                                        List<TblPartnerAdminTransfer> list = transferEmployeeServiceImpl.viewTransferHistory(usrId);
                                                        boolean flag = false;


                                                        List<TblUserActivationHistory> listAccountHistory = transferEmployeeServiceImpl.viewAccountStatusHistory(usrId);

                                            %>
                                            <%
                                                        String strReplacedBy = "";
                                                        for (int i = 0; i < list.size(); i++) {
                                                            if (!"".equalsIgnoreCase(list.get(i).getReplacedBy()) && list.get(i).getReplacedBy() != null) {
                                                                strReplacedBy = list.get(i).getReplacedBy();
                                                                flag = true;
                                                            }
                                                        }
                                            %>
                                    </div>
                                        <form id="frmSBAdmin" name="frmSBAdmin" method="post" action="">
                                            <table width="100%" border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <%if (!"Development".equalsIgnoreCase(type)) {%>

                                                     <tr>
                                                         <td class="ff" width="15%">
                                                        Role :
                                                        </td>
                                                        <td>
                                                            <%/*if(partnerAdminMasterDtBean.getIsMakerChecker().equalsIgnoreCase("BankChecker")){*/%>
                                                        <!--                                                            <input type="radio" name="userRole" id="bankCheckerRole" value="BankChecker" checked="cheked"/> Bank Checker &nbsp;-->
                                                            <%--<label>Bank Checker</label>--%>
                                                        <%/*}else*/ if (partnerAdminMasterDtBean.getIsMakerChecker().equalsIgnoreCase("BranchChecker")) {%>
                                                        <!--                                                            <input type="radio" name="userRole" id="branchCheckerRole" value="BranchChecker" checked="cheked"/> Branch Checker &nbsp;-->
                                                                <label> Branch Checker</label>
                                                        <%} else {%>
                                                        <!--                                                            <input type="radio" name="userRole" id="branchMakerRole" value="maker" checked="cheked"/> Branch Maker-->
                                                            <label> Branch Maker</label>

                                                            <%}%>
                                                        </td>
                                                     </tr>
                                                            <%}%>
                                                            <%
                                                                    scBankCreationSrBean.getBankForAdmin((partnerAdminMasterDtBean.getSbankDevelopId()));
                                                            if (scBankCreationSrBean.getsBankDevelopId() != 0) {
                                                            %>
                                                <tr>
                                                    <td width="15%" class="ff"><%=title1%> : </td>
                                                    <td width="85%">
                                                        <label id="bankName"><%=scBankCreationSrBean.getSbDevelopName()%></label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="20%" class="ff"><%=title1%> Branch: </td>
                                                    <td width="85%">
                                                        <label id="bankName">${partnerAdminMasterDtBean.sbDevelopName}</label>
                                                    </td>
                                                </tr>
                                                <%} else {%>
                                                <tr>
                                                    <td width="15%" class="ff"><%=title1%> : </td>
                                                    <td width="85%">
                                                        <label id="bankName">${partnerAdminMasterDtBean.sbDevelopName}</label>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <tr>
                                                    <td class="ff">e-mail ID : </td>
                                                    <td>
                                                        <label id="txtMail">${partnerAdminMasterDtBean.emailId}</label>
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td class="ff">Full Name :</td>
                                                    <td><label id="txtName">${partnerAdminMasterDtBean.fullName}</label>
                                                    </td>
                                                </tr>
                                                <%if (!"Development".equalsIgnoreCase(type)) {
                                                 %>
                                                <tr>
                                                    <td class="ff">CID No. : </td>
                                                    <td>
                                                        <label id="txtNationalId" >${partnerAdminMasterDtBean.nationalId}</label>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                 <tr>
                                                    <td class="ff">Designation :</td>
                                                    <td>
                                                        <label id="txtDesignation">${partnerAdminMasterDtBean.designation}</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Mobile No :</td>
                                                    <%if (!"".equalsIgnoreCase(partnerAdminMasterDtBean.getMobileNo())) {%>
                                                    <td>
                                                        <label id="txtMobileNo">${partnerAdminMasterDtBean.mobileNo}</label>
                                                    </td>
                                                    <%}%>
                                                </tr>
                                                <tr>
                                                    <td width="15%">&nbsp;</td>
                                                    <td width="85%" class="t-align-left" >
                                                        <%if (!bole_fromWhere) {%>
                                                        <a href="<%=gridURL%>" class="anchorLink" style="text-decoration: none;color: #fff;">Ok</a>&nbsp;&nbsp;<a href="<%=editURL%>" class="anchorLink" style="text-decoration: none;color: #fff;">Edit</a>&nbsp;&nbsp;<%if (flag) {%><a href="<%=strTransferHistoryURL%>" class="anchorLink" style="text-decoration: none;color: #fff;">View Transfer History</a>&nbsp;&nbsp;<%}%><%if (listAccountHistory.size() > 0) {%><a href="<%=viewaccounthistory%>" class="anchorLink" style="text-decoration: none;color: #fff;">View Account History</a><%}%>
                                                        <%}%>
                                                    </td>
                                                </tr>
                                            </table>
                                            <%--<table width="100%" cellspacing="10" cellpadding="0" border="0">
                                                <tr>
                                                    <td width="15%">&nbsp;</td>
                                                    <td width="85%" class="t-align-left" >
                                                        <%if (!bole_fromWhere) {%>
                                                        <a href="<%=gridURL%>" class="anchorLink">Ok</a>&nbsp;&nbsp;<a href="<%=editURL%>" class="anchorLink">Edit</a>&nbsp;&nbsp;<%if (flag) {%><a href="<%=strTransferHistoryURL%>" class="anchorLink">View Transfer History</a>&nbsp;&nbsp;<%}%><%if (listAccountHistory.size() > 0) {%><a href="<%=viewaccounthistory%>" class="anchorLink">View Account History</a><%}%>
                                                        <%}%>
                                                    </td>
                                                </tr>

                                            </table>--%>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <%if (bole_fromWhere) {%>
            <script>
            var headSel_Obj = document.getElementById("headTabMyAcc");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
        <%} else {%>
        <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
        <%}%>
</html>
