<%--
  Document   : PreTenderRule
  Created on : Nov 2, 2010, 2:50:38 PM
  Author     : Naresh.Akurathi
--%>


<%@page import="com.cptu.egp.eps.model.table.TblConfigPreTender"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Pre Tender Meeting Rule</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        
        <%
                    StringBuilder tenderType = new StringBuilder();
                    StringBuilder procNature = new StringBuilder();
                    StringBuilder procType = new StringBuilder();
                    StringBuilder procMethod = new StringBuilder();

                    TblTenderTypes tenderTypes2 = new TblTenderTypes();
                    Iterator trit2 = configPreTenderRuleSrBean.getTenderNames().iterator();
                    while (trit2.hasNext()) {
                        tenderTypes2 = (TblTenderTypes) trit2.next();
                        tenderType.append("<option value='" + tenderTypes2.getTenderTypeId() + "'>" + tenderTypes2.getTenderType() + "</option>");
                    }
                    TblProcurementNature tblProcureNature2 = new TblProcurementNature();
                    Iterator pnit2 = configPreTenderRuleSrBean.getProcurementNature().iterator();
                    while (pnit2.hasNext()) {
                        tblProcureNature2 = (TblProcurementNature) pnit2.next();
                        procNature.append("<option value='" + tblProcureNature2.getProcurementNatureId() + "'>" + tblProcureNature2.getProcurementNature() + "</option>");
                    }

                    TblProcurementMethod tblProcurementMethod2 = new TblProcurementMethod();
                    Iterator pmit2 = configPreTenderRuleSrBean.getProcurementMethod().iterator();
                    while (pmit2.hasNext()) {
                        tblProcurementMethod2 = (TblProcurementMethod) pmit2.next();
                        procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>" + tblProcurementMethod2.getProcurementMethod() + "</option>");
                    }

                    TblProcurementTypes tblProcurementTypes2 = new TblProcurementTypes();
                    Iterator ptit2 = configPreTenderRuleSrBean.getProcurementTypes().iterator();
                    String procureType="";
                    while (ptit2.hasNext()) {
                        tblProcurementTypes2 = (TblProcurementTypes) ptit2.next();
                        if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("NCT"))
                            procureType="NCB";
                        else if(tblProcurementTypes2.getProcurementType().equalsIgnoreCase("ICT"))
                            procureType="ICB";
                        procType.append("<option value='" + tblProcurementTypes2.getProcurementTypeId() + "'>" + procureType + "</option>");
                    }


        %>


        <script type="text/javascript">
    

            var totCnt = 1;
    
            $(function() {
                $('#linkAddRule').click(function() {
                    var count=($("#tblrule tr:last-child").attr("id").split("_")[1]*1);

                    var htmlEle = "<tr id='trrule_"+ (count+1) +"'>"+
                        "<td class='t-align-center'><input type='checkbox' name='checkbox"+(count+1)+"' id='checkbox_"+(count+1)+"' value='"+(count+1)+"' /></td>"+
                        "<td class='t-align-center'><select name='tenderType"+(count+1)+"' class='formTxtBox_1' id='tenderType_"+(count+1)+"'><%=tenderType%></select></td>"+
                        //"<td class='t-align-center'><select name='procurementNature"+(count+1)+"' class='formTxtBox_1' id='procurementNature_"+(count+1)+"'><%=procNature%></select></td>"+
                        "<td class='t-align-center'><select name='procurementMethod"+(count+1)+"' class='formTxtBox_1' id='procurementMethod_"+(count+1)+"'><%=procMethod%></select></td>"+
                        "<td class='t-align-center'><select name='procurementType"+(count+1)+"' class='formTxtBox_1' id='procurementType_"+(count+1)+"'><%=procType%></select></td>"+
                        //"<td  class='t-align-center'><select name='preTenderMettingRequired"+(count+1)+"' class='formTxtBox_1' id='preTenderMettingRequired_"+(count+1)+"'><option>Yes</option><option>No</option></td>"+
                        "<td class='t-align-center'><input name='daysforPublication"+(count+1)+"' type='text' class='formTxtBox_1' id='daysforPublication_"+(count+1)+"' onBlur='return chkDaysFromPub(this);'/><span id='DaysforPublication_"+ (count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        //"<td class='t-align-center'><input name='postingofQuestions"+(count+1)+"' type='text' class='formTxtBox_1' id='postingofQuestions_"+(count+1)+"' onBlur='return chkpostingofQue(this);'/><span id='PostingofQuestions_"+ (count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "<td class='t-align-center'><input name='daysforAllowed"+(count+1)+"' type='text' class='formTxtBox_1' id='daysforAllowed_"+(count+1)+"' onBlur='return chkdaysforAllowed(this);'/><span id='DaysforAllowed_"+ (count+1)+"' style='color: red;'>&nbsp;</span></td>"+
                        "</tr>";
                                 
                    totCnt++;
                    $("#tblrule").append(htmlEle);
                    document.getElementById("TotRule").value = (count+1);

                });
            });


            $(function() {
                $('#linkDelRule').click(function() {
                    var cnt = 0;
                    var tmpCnt = 0;
                    var counter = ($("#tblrule tr:last-child").attr("id").split("_")[1]*1);
                  
                    for(var i=1;i<=counter;i++){
                        if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                            tmpCnt++;
                            
                        }
                    }

                    if(tmpCnt==totCnt){
                        $('span.#lotMsg').css("visibility","visible");
                        $('span.#lotMsg').css("color","red");
                        $('span.#lotMsg').html('Minimum 1 record is needed!');
                    }else{
                        for(var i=1;i<=counter;i++){
                            if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                                $("tr[id='trrule_"+i+"']").remove();
                                $('span.#lotMsg').css("visibility","collapse");
                                cnt++;
                            }
                        }
                        totCnt -= cnt;
                    }

                });
            });
        </script>

    </head>
    <body>

        <%!        ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();

        %>

        <%
                    String msg = "";
                    if ("Submit".equals(request.getParameter("button"))) {

                        int row = Integer.parseInt(request.getParameter("TotRule"));

                        msg = configPreTenderRuleSrBean.deleteAllConfigPreTenderRule();
                        if (msg.equals("Deleted")) {
                            for (int i = 1; i <= row; i++) {

                                if (request.getParameter("tenderType" + i) != null) {
                                    byte ttype = Byte.parseByte(request.getParameter("tenderType" + i));
                                    byte pnature = Byte.parseByte("1");
                                    byte pmethod = Byte.parseByte(request.getParameter("procurementMethod" + i));
                                    byte ptypes = Byte.parseByte(request.getParameter("procurementType" + i));
                                    TblConfigPreTender master = new TblConfigPreTender();

                                    master.setTblTenderTypes(new TblTenderTypes(ttype));
                                    master.setTblProcurementNature(new TblProcurementNature(pnature));
                                    master.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                    master.setTblProcurementTypes(new TblProcurementTypes(ptypes));

                                    //master.setIsPreTenderMeetingfReq(request.getParameter("preTenderMettingRequired" + i));
                                    master.setIsPreTenderMeetingfReq("Yes");
                                    master.setPubBidDays(Short.parseShort(request.getParameter("daysforPublication" + i)));
                                    //master.setPostQueDays(Short.parseShort(request.getParameter("postingofQuestions" + i)));
                                    master.setPostQueDays(Short.valueOf("0"));
                                    master.setUploadedTenderDays(Short.parseShort(request.getParameter("daysforAllowed" + i)));

                                    msg = configPreTenderRuleSrBean.addConfigPreTenderRule(master);

                                }
                            }
                        }
                        if (msg.equals("Values Added")) {
                            msg = "PreTender Meeting Business Rule Configured Successfully";
                            response.sendRedirect("ConfigPreTenderMettingRuleView.jsp?msg=" + msg);
                        }

                    }

        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 25%; display: block;">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                            </tr>
                        </table>
                    </td>
                    <td class="contentArea">


                        <!--Dashboard Content Part Start-->
                        <div class="pageHead_1">Pre Tender Meeting Rule Configuration</div>

                        <div style="font-style: italic" class="t-align-left t_space"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>
                        <div align="right" class="t_space b_space">
                            <span id="lotMsg" style="color: red; font-weight: bold; visibility: collapse; float: left;">&nbsp;</span>
                            <a id="linkAddRule" class="action-button-add">Add Rule</a> <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a>

                        </div>
                        <div class="overFlowContainer t-align-right">
                            <form action="ConfigPreTenderMettingRule.jsp" method="post">

                                <table width="100%" cellspacing="0" class="tableList_1" id="tblrule" name="tblrule">
                                    <tr  >
                                        <th style="width:12%;">Select</th>
                                        <th style="width:16%;">Tender Type<br />(<span class="mandatory">*</span>)</th>
                                        <!--<th>Procurement Nature<br />(<span class="mandatory">*</span>)</th>-->
                                        <th style="width:16%;">Procurement Method<br />(<span class="mandatory">*</span>)</th>
                                        <th style="width:16%;">Procurement Type<br />(<span class="mandatory">*</span>)</th>
                                        <!--<th>Pre Tender Meeting Required<br />(<span class="mandatory">*</span>)</th>-->
                                        <th style="width:25%;">Specify as how many days, from date of Tender Publication, Pre Tender Meeting should start<br />(<span class="mandatory">*</span>)</th>
                                        <!--<th>Posting of questions allowed before no. of days from  tender closing date<br />(<span class="mandatory">*</span>)</th>-->
                                        <th style="width:25%;">Days allowed for uploading Pre Tender MOM, after Pre Tender meeting is over<br />(<span class="mandatory">*</span>)</th>
                                    </tr>
                                    <input type="hidden" name="delrow" value="0" id="delrow"/>
                                    <input type="hidden" name="TotRule" id="TotRule" value="1"/>
                                    <input type="hidden" name="introw" value="" id="introw"/>
                                    <tr id="trrule_1">
                                        <td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_1" value="1" /></td>

                                        <td class="t-align-center"><select name="tenderType1" class="formTxtBox_1" id="tenderType_1">
                                                <%=tenderType%></select>
                                        </td>
                                        <!--<td class="t-align-center"><select name="procurementNature1" class="formTxtBox_1" id="procurementNature_1">
                                                <%=procNature%></select>
                                </td>-->
                                        <td class="t-align-center"><select name="procurementMethod1" class="formTxtBox_1" id="procurementMethod_1">
                                                <%=procMethod%></select>
                                        </td>
                                        <td class="t-align-center"><select name="procurementType1" class="formTxtBox_1" id="procurementType_1">
                                                <%=procType%></select>
                                        </td>
                                        <!--<td class="t-align-center"><select name="preTenderMettingRequired1" class="formTxtBox_1" id="preTenderMettingRequired_1">
                                                <option>Yes</option>
                                                <option>No</option></select>
                                        </td>-->
                                        <td class="t-align-center"><input name="daysforPublication1" type="text" class="formTxtBox_1" id="daysforPublication_1" onBlur="return chkDaysFromPub(this);"/><span id="DaysforPublication_1" style="color: red;">&nbsp;</span></td>
                                        <!--<td class="t-align-center"><input name="postingofQuestions1" type="text" class="formTxtBox_1" id="postingofQuestions_1" onBlur="return chkpostingofQue(this);"/><span id="PostingofQuestions_1" style="color: red;">&nbsp;</span></td>-->
                                        <td class="t-align-center"><input name="daysforAllowed1" type="text" class="formTxtBox_1" id="daysforAllowed_1" onBlur="return chkdaysforAllowed(this);"/><span id="DaysforAllowed_1" style="color: red;">&nbsp;</span></td>
                                    </tr>
                                </table>
                                <div>&nbsp;</div>
                                <table width="100%" cellspacing="0" >
                                    <tr>
                                        <td align="right" colspan="9" class="t-align-center">
                                            <span class="formBtn_1"><input type="submit" name="button" id="button" value="Submit" onclick="return validate();"/></span>
                                        </td>
                                    </tr>
                                </table>
                            </form>

                        </div>

                        <!--Dashboard Content Part Start-->

                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>
             <script>
                var obj = document.getElementById('lblConfigPreTenderMettingRuleAdd');
                if(obj != null){
                    if(obj.innerHTML == 'Add'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
            var headSel_Obj = document.getElementById("headTabConfig");
            if(headSel_Obj != null){
    headSel_Obj.setAttribute("class", "selected");
            }
        </script>
        <script type="text/javascript">

            function validate()
            {
                var count=document.getElementById("TotRule").value;
                var flag=true;

                for(var k=1;k<=count;k++)
                {
                    if(document.getElementById("daysforPublication_"+k)!=null)
                    {
                        if(document.getElementById("daysforPublication_"+k).value=="")
                        {
                            document.getElementById("DaysforPublication_"+k).innerHTML="<br/>Please specify as how many days, from date of Tender Publication, Pre Tender Meeting should start.";
                            flag=false;
                        }
                        else
                        {
                            if(digits(document.getElementById("daysforPublication_"+k).value))
                            {
                                if(!chkMaxLength(document.getElementById("daysforPublication_"+k).value))
                                {
                                    document.getElementById("DaysforPublication_"+k).innerHTML="</br>Maximum 3 numbers  without decimal are allowed.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("DaysforPublication_"+k).innerHTML="";
                                }

                            }
                            else
                            {
                                document.getElementById("DaysforPublication_"+k).innerHTML="</br>Please enter digits only.";
                                flag=false;
                            }

                        }
                    }

                    /*  if(document.getElementById("postingofQuestions_"+k)!=null)
                    {
                        if(document.getElementById("postingofQuestions_"+k).value=="")
                        {
                            document.getElementById("PostingofQuestions_"+k).innerHTML="<br/>Please enter posting of questions allowed before no. of days from tender closing date.";
                            flag=false;
                        }
                        else
                        {
                            if(digits(document.getElementById("postingofQuestions_"+k).value))
                            {
                                if(!chkMaxLength(document.getElementById("postingofQuestions_"+k).value))
                                {
                                    document.getElementById("PostingofQuestions_"+k).innerHTML="</br>Maximum 3 numbers  without decimal are allowed.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("PostingofQuestions_"+k).innerHTML="";
                                }
                            }
                            else
                            {
                                document.getElementById("PostingofQuestions_"+k).innerHTML="</br>Please enter digits only.";
                                flag=false;
                            }
                        }
                    }*/

                    if(document.getElementById("daysforAllowed_"+k)!=null)
                    {
                        if(document.getElementById("daysforAllowed_"+k).value=="")
                        {
                            document.getElementById("DaysforAllowed_"+k).innerHTML="<br/>Please Specify no. of days allowed for uploading MoM of Pre Tender Meeting, after Pre Tender Meeting is over.";
                            flag=false;
                        }
                        else
                        {
                            if(digits(document.getElementById("daysforAllowed_"+k).value))
                            {
                                if(!chkMaxLength(document.getElementById("daysforAllowed_"+k).value))
                                {
                                    document.getElementById("DaysforAllowed_"+k).innerHTML="</br>Maximum 3 numbers  without decimal are allowed.";
                                    flag=false;
                                }
                                else
                                {
                                    document.getElementById("DaysforAllowed_"+k).innerHTML="";
                                }
                            }
                            else
                            {
                                document.getElementById("DaysforAllowed_"+k).innerHTML="</br>Please enter digits only.";
                                flag=false;
                            }
                        }
                    }

                }
                // Start OF--Checking for Unique Rows
                if(document.getElementById("TotRule")!=null){
                    var totalcount = eval(document.getElementById("TotRule").value);} //Total Count After Adding Rows
                var chk=true;
                var i=0;
                var j=0;
                for(i=1; i<=totalcount; i++) //Loop For Newly Added Rows
                {
                    if(document.getElementById("daysforPublication_"+i)!=null)
                        if($.trim(document.getElementById("daysforPublication_"+i).value)!='' &&  $.trim(document.getElementById("daysforAllowed_"+i).value)!='') // If Condition for When all Data are filled thiscode is Running
                    {

                        for(j=1; j<=totalcount && j!=i; j++) // Loop for Total Count but Not same as (i) value
                        {
                            chk=true;
                            //If Condition for Check Duplicate Rows are there or not.If Columns are diff then chk variable set to false
                            //IF Row is same Give alert message.

                            if($.trim(document.getElementById("tenderType_"+i).value) != $.trim(document.getElementById("tenderType_"+j).value))
                            {
                                chk=false;
                            }
                            /*     if($.trim(document.getElementById("procurementNature_"+i).value) != $.trim(document.getElementById("procurementNature_"+j).value))
                            {
                                chk=false;
                            }
                             */
                            if($.trim(document.getElementById("procurementMethod_"+i).value) != $.trim(document.getElementById("procurementMethod_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("procurementType_"+i).value) != $.trim(document.getElementById("procurementType_"+j).value))
                            {
                                chk=false;
                            }
                            /*   if($.trim(document.getElementById("preTenderMettingRequired_"+i).value) != $.trim(document.getElementById("preTenderMettingRequired_"+j).value))
                            {
                                chk=false;
                            }

                            if($.trim(document.getElementById("daysforPublication_"+i).value) != $.trim(document.getElementById("daysforPublication_"+j).value))
                            {
                                chk=false;
                            }
                            if($.trim(document.getElementById("postingofQuestions_"+i).value) != $.trim(document.getElementById("postingofQuestions_"+j).value))
                            {
                                chk=false;
                            }

                            if($.trim(document.getElementById("daysforAllowed_"+i).value) != $.trim(document.getElementById("daysforAllowed_"+j).value))
                            {
                                chk=false;
                            }
                             */
                            //alert(j);
                            //alert("chk" +chk);
                            if(flag){
                            if(chk==true) //If Row is same then give alert message
                            {
                                    jAlert("Duplicate record found. Please enter unique record","Pre Tender Meeting Business Rules Configuration",function(RetVal) {
                                    });
                                return false;
                            }
                        }
                        }

                    }
                }
                // End OF--Checking for Unique Rows

                if (flag==false)
                {
                    return false;
                }
            }

            function chkMaxLength(value)
            {
                var ValueChk=value;
                if(ValueChk.length>3)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            function chkDaysFromPub(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk=true;

                if(obj.value!='')
                {
                    if(digits(obj.value))
                    {
                        if(!chkMaxLength(obj.value))
                        {
                            document.getElementById("DaysforPublication_"+i).innerHTML="</br>Maximum 3 numbers  without decimal are allowed.";
                            chk=false;
                        }
                        else
                        {
                            document.getElementById("DaysforPublication_"+i).innerHTML="";
                        }
                        //document.getElementById("DaysforPublication_"+i).innerHTML="";
                        //chk=true;
                    }
                    else
                    {
                        document.getElementById("DaysforPublication_"+i).innerHTML="</br>Please enter digits only.";
                        chk=false;
                    }
                }
                else
                {
                    document.getElementById("DaysforPublication_"+i).innerHTML="</br>Please specify as how many days, from date of Tender Publication, Pre Tender Meeting should start.";
                    chk=false;
                }
                if(chk==false)
                {
                    return false;
                }
            };

            /*    function chkpostingofQue(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk1=true;

                if(obj.value!='')
                {
                    if(digits(obj.value))
                    {
                        if(!chkMaxLength(obj.value))
                        {
                            document.getElementById("PostingofQuestions_"+i).innerHTML="</br>Maximum 3 numbers  without decimal are allowed.";
                            chk1=false;
                        }
                        else
                        {
                            document.getElementById("PostingofQuestions_"+i).innerHTML="";
                        }
                        //document.getElementById("PostingofQuestions_"+i).innerHTML="";
                        //chk1=true;
                    }
                    else
                    {
                        document.getElementById("PostingofQuestions_"+i).innerHTML="</br>Please enter digits only.";
                        chk1=false;
                    }
                }
                else
                {
                    document.getElementById("PostingofQuestions_"+i).innerHTML="</br>Please enter posting of questions allowed before no. of days from tender closing date.";
                    chk1=false;
                }
                if(chk1==false)
                {
                    return false;
                }
            }
             */

            function chkdaysforAllowed(obj)
            {
                var i = (obj.id.substr(obj.id.indexOf("_")+1));
                var chk2=true;

                if(obj.value!='')
                {
                    if(digits(obj.value))
                    {
                        if(!chkMaxLength(obj.value))
                        {
                            document.getElementById("DaysforAllowed_"+i).innerHTML="</br>Maximum 3 numbers  without decimal are allowed.";
                            chk2=false;
                        }
                        else
                        {
                            document.getElementById("DaysforAllowed_"+i).innerHTML="";
                        }
                        //document.getElementById("DaysforAllowed_"+i).innerHTML="";
                        //chk2=true;
                    }
                    else
                    {
                        document.getElementById("DaysforAllowed_"+i).innerHTML="</br>Please enter digits only.";
                        chk2=false;
                    }
                }
                else
                {
                    document.getElementById("DaysforAllowed_"+i).innerHTML="</br>Please Specify no. of days allowed for uploading MoM of Pre Tender Meeting, after Pre Tender Meeting is over.";
                    chk2=false;
                }
                if(chk2==false)
                {
                    return false;
                }

            };

            function digits(value){

                return /^\d+$/.test(value);

            };
        </script>
    </body>
</html>
