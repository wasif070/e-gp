<%-- 
    Document   : ViewExtEvalCommMember
    Created on : Feb 19, 2011, 5:39:01 PM
    Author     : rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View External Evaluation Committee Members</title>
    </head>
    <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
    %>
    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />


    <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
    <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
    <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
    <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
    <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
    <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery().ready(function (){
            jQuery("#list").jqGrid({
                url:'<%=request.getContextPath()%>/ExtEvalCommMemberServlet?q=1&action=fetchData',
                datatype: "xml",
                height: 250,
                colNames:['Sl. No.','e-mail ID','Full Name','Registration Date','Action'],
                colModel:[
                    {name:'srno',index:'srno', width:'5%',sortable:false, search: false,align:'center'},
                    {name:'emailId',index:'emailId', width:'35%',sortable:true, searchoptions: { sopt: ['eq','cn']}},
                    {name:'fullName',index:'fullName', width:'35%',sortable:true, searchoptions: { sopt: ['eq','cn']}},
                    {name:'regDate',index:'creationDt', width:'15%',sortable:true, search: false},
                    {name:'Action',index:'Action', width:'10%',sortable:false, search: false,align:'center'}
                ],
                autowidth: true,
                multiselect: false,
                paging: true,
                rowNum:10,
                rowList:[10,20,30],
                pager: $("#page"),
                caption: "External Evaluation Committee Members Details",
                gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
            }).navGrid('#page',{edit:false,add:false,del:false});
        });
    </script>
    <body>
        <!--        <div class="mainDiv">
                    <div class="dashboard_div">-->
        <%@include  file="../resources/common/AfterLoginTop.jsp"%>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr valign="top">
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("hdnUserType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                    userType.append(request.getParameter("hdnUserType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                %>
                <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                <td class="contentArea">

                    <div class="pageHead_1">View External Evaluation Committee Members
                    <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('4');">Save as PDF</a></span>
                    </div>
                    <div align="center" class="t_space">
                        <div class="t-align-left ff formStyle_1">To sort click on the relevant column header</div>
                        <table id="list"></table>
                        <div id="page">
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <!--For Generate PDF  Starts-->
            <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                    String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="ExtEvaluationCommittee_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="ExtEvaluationCommittee" />
            </form>
            <!--For Generate PDF  Ends-->
        <!--Middle Content Table End-->
        <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
    </body>
            <script>
                var obj = document.getElementById('lblExtEvalCommView');
                if(obj != null){
                    if(obj.innerHTML == 'View Users'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
        <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
</html>
