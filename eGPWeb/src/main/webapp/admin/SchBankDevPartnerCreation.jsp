<%--
    Document   : SchBankDevPartnerCreation
    Created on : Oct 23, 2010, 7:31:07 PM
    Author     : rishita
--%>

<%@page import="com.lowagie.text.Document"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%
                String partnerType = request.getParameter("partnerType");
                String type = "";
                String title = "";
                String userType = "";
                String headOffice = "Head Office";
                if (partnerType != null && partnerType.equals("Development")) {
                    type = "Development";
                    title = "Development Partner";
                    userType = "dev";
                } else {
                    type = "ScheduleBank";
                    title = "Financial Institution";
                    userType = "sb";
                    headOffice = "Financial Institution Name";
                }
    %>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=title%> Creation</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js"type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $.validator.addMethod(
                "regex",
                 function(value, element, regexp) {
                 var check = false;
                 return this.optional(element) || regexp.test(value);
                 },
                 "Please check your input."
                 );
                $('#btnSubmit').attr("disabled", false);
                $("#frmBankSchedule").validate({
                    rules: {
                        officeAddress:{required:true,maxlength:300},
                        city:{//requiredWithoutSpace: true,
                            spacevalidate:true,alphaCity: true,maxlength:100},
                        thanaUpzilla:{requiredWithoutSpace: true,spacevalidate:true,alphaCity: true,maxlength:100 },
                        postCode:{number:true,minlength:4},
                        //phoneNo:{required: true,PhoneFax: true,minlength:6,maxlength:20},
                        phoneNo: {required: true, regex: /^[0-9]+-[0-9,]*[0-9]$/},
                        faxNo: {regex: /^[0-9]+-[0-9,]*[0-9]$/},
                        webSite: {url:true,maxlength:50}
                    },
                    messages: {
                        officeAddress:{ required: "<div class='reqF_1'> Please enter Address</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 300 characters only</div>"},

                        city:{ //requiredWithoutSpace: "<div class='reqF_1'>Please enter City / Town Name</div>",
                            alphaCity:"<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , ' \" } { - . _) Only</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>",
                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>"
                        },
                        thanaUpzilla:{requiredWithoutSpace: "<div class='reqF_1'>Please enter Gewog</div>",
                            alphaCity: "<div class='reqF_1'>Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , ' \" } { - . _) Only</div>",
                            spacevalidate:"<div class='reqF_1'>Only Space is not allowed</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>"
                        },
                        postCode:{//required:"<div class='reqF_1'>Please enter Post code.</div>",
                            number: "<div class='reqF_1'>Please enter numbers only</div>",
                            maxlength:"<div class='reqF_1'>Maximum 4 digits are allowed.</div>",
                            minlength:"<div class='reqF_1'>Minimum 4 digits are required.</div>"
                        },

                        phoneNo: {required: "<div class='reqF_1'>Please enter Phone No.</div>",
                            regex: "<div class='reqF_1'>Give dash (-) after Area Code.</br>Use comma to separate alternative Phone no.</br>Do not repeat Area Code.</div>",
                            },
                        faxNo: {regex: "<div class='reqF_1'>Give dash (-) after Area Code.</br>Use comma to separate alternative Fax no.</br>Do not repeat Area Code.</div>",
                            },
                        webSite: {url: "<div class='reqF_1'>Please enter website in www.xyz.gov.bt format</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 50 characters only</div>"}
                    },

                    errorPlacement:function(error ,element)
                    {
                        if(element.attr("name")=="phoneNo")
                        {
                            error.insertAfter("#phno");
                        }
                        else if(element.attr("name")=="faxNo")
                        {
                            error.insertAfter("#fxno");
                        }
                        else
                            error.insertAfter(element);
                    }
                });
            });
            function chkregPostCode(value)
            {
                return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);

            }
            function chkreg(value)
            {
                //return /^[a-zA-Z 0-9](?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9]+$)+$/.test(value);
                return /^([a-zA-Z 0-9]([a-zA-Z 0-9\s\[\]\(\)\-\.\&\"\'\{\}\_\)])+$)/.test(value);

            }
            var bvalidationPostCode = true;
            /*$(function() {
                $('#txtPostCode').blur(function() {
                    bvalidationPostCode = true;
                    if($('#txtPostCode').val()==''){
                        $('#postCodeMsg').html('Please enter Postcode');
                        bvalidationPostCode = false;
                    }else if(!chkregPostCode($('#txtPostCode').val())){
                        $('#postCodeMsg').html('Please enter digits (0-9) only');
                        bvalidationPostCode = false;
                    }else if($('#txtPostCode').val().length > 4 || $('#txtPostCode').val().length < 4){
                        $('#postCodeMsg').html('Postcode should comprise of 4 digits only');
                        bvalidationPostCode = false;
                    }else{
                        $('#postCodeMsg').html('');
                    }
                });
            });*/
        </script>
        <script type="text/javascript">
            $(function() {
                $('#cmbCountry').change(function() {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbCountry').val(),funName:'stateCombo'},  function(j){
                        $('#cmbState').children().remove().end().append('<option selected value="0">-- Select --</option>') ;
                        $("select#cmbState").html(j);
                        toggleUpzillaView();
                    });
                });
            });
        </script>
        <script type="text/javascript">
            function toggleUpzillaView()
            {
                if($('#cmbCountry option:selected').text() == "Bangladesh" && $('#partnerType').val() == "ScheduleBank")
                    $('#upjilla').show()
                else
                    $('#upjilla').hide();
            }
        </script>
        <script type="text/javascript">
            function alphanumeric(value)
            {
                return /^[\sa-zA-Z0-9\'\-]+$/.test(value);
                }
                function numeric(value)
                {
                    return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(value);
                }
                function ChkMaxLength(value)
                {
                    var ValueChk=value;
                    var charLen = 4;
                     var partnerType = '<%= type%>';
                    if(partnerType == 'Development'){
                        charLen = 10;
                    }
                    if(ValueChk.length>charLen)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                function ValPost()
                {
                    var flag = true;
//                    var partnerType = '<%= type%>';
//                    if($('#txtPostCode').val() != '')
//                    {
//                        if(partnerType == 'Development')
//                        {
//                            //alert("development condition");
//                            //alert($('#txtPostCode').val().length);
//                            if(alphanumeric($('#txtPostCode').val()))
//                            {
//                                if(!ChkMaxLength($('#txtPostCode').val()))
//                                {
//                                    $('span.#postCodeMsg').html("<br/>Postcode should comprise of 10 digits only");
//                                    flag=false;
//                                }
//                                else
//                                {
//                                    $('span.#postCodeMsg').html("");
//                                }
//                            }
//                            else
//                            {
//                                $('span.#postCodeMsg').html("<br/>Please enter only alphanumeric value.");
//                                flag = false;
//                            }
//                        }
//                        else
//                        {
//                            //alert("sc bank. condition");
//                            if(numeric($('#txtPostCode').val()))
//                            {
//                                if(!ChkMaxLength($('#txtPostCode').val()))
//                                {
//                                    $('span.#postCodeMsg').html("<br/>Postcode should comprise of 4 digits only");
//                                    flag=false;
//                                }
//                                else
//                                {
//                                    $('span.#postCodeMsg').html("");
//                                   
//                                }
//                            }
//                            else
//                            {
//                                $('span.#postCodeMsg').html("<br/>Please enter numbers only.");
//                                flag = false;
//                            }
//                        }
//                    }
//                    else
//                    {
//                        $('span.#postCodeMsg').html("<br/>Please enter Postcode.");
//                        
//                        flag = false;
//                    }
//                    if(flag==false){
//                        var counterror=document.getElementById("hdnerrorcount").value;
//                        counterror=eval(eval(counterror)+1);
//                        document.getElementById("hdnerrorcount").value = counterror;
//                        
//                    }else
//                    {
//                        var counterror=document.getElementById("hdnerrorcount").value;
//                        counterror=eval(eval(counterror)-1);
//                        document.getElementById("hdnerrorcount").value = counterror;
//                    }
//
//                    if(document.getElementById("hdnerrorcount").value>1){
//                        return false;
//                    }
//                    
//                    
                     return flag;
                }
                var bvalidationName;
                $(function() {
                    $('#txtBankName').blur(function() {
                        bvalidationName = true;
                    var flag=document.getElementById('txtBankName').value;
                    if($('#txtBankName').val() == ''){
                        $('#officeMsg').html("");
                        $('#branchValidation').html('Please enter <%=title%> Name');
                        bvalidationName = false;
                    }else if(flag.charAt(0) == " "){
                        $('#officeMsg').html("");
                        $('#branchValidation').html('Only Space is not allowed');
                        bvalidationName = false;
                    }else if(!chkreg($('#txtBankName').val())){
                        $('#officeMsg').html("");
                        $('#branchValidation').html('Allows Characters (A to Z), Numbers (0 to 9) & Special Characters (& , \' " } { - . _) Only');
                        bvalidationName = false;
                    }else if(flag.length > 100){
                        $('#officeMsg').html("");
                        $('#branchValidation').html('Allows maximum 100 characters only');
                        bvalidationName = false;
                    }else{
                        $('#branchValidation').html('');
                        $('#officeMsg').html("Checking for unique <%=title%>...");
                        $.post("<%=request.getContextPath()%>/ScBankServlet", {title: "<%=title%>",type: "<%=type%>",officeName:$('#txtBankName').val(),isBranch: 'No',funName:'verifyOffice'},
                        function(j){
                            if(j.toString().indexOf("OK", 0)!=-1){
                                $('#officeMsg').css("color","green");
                            }
                            else{
                                $('#officeMsg').css("color","red");
                            }
                            $('#officeMsg').html(j);
                        });
                        }
                    });
                });
        </script>
        <script type="text/javascript">
                //Function for State and Coruntry Combo
                $(function() {
                    $('#cmbCountry').change(function() {
                        $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbCountry').val(),funName:'stateCombo'},  function(j){
                            $('#cmbState').children().remove().end().append('<option selected value="0">-- Select --</option>') ;
                            $("select#cmbState").html(j);
                        });
                        if($('#cmbCountry option:selected').text() == "Bangladesh" && $('#partnerType').val() == "ScheduleBank")
                            $('#upjilla').show();
                        else
                            $('#upjilla').hide();
                    });
                });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnSubmit').click(function() {
                    bValidation = true;
                    if($('#txtBankName').val()==''){
                        $('#branchValidation').html('Please enter <%=title%> Name');
                        bValidation = false;
                    }
//                    if($('#txtPostCode').val()==''){
//                        $('#postCodeMsg').html('Please enter Postcode');
//                        bValidation = false;
//                    }
                });
            });
                $(function() {
                    $('#frmBankSchedule').submit(function() {
                        //alert($('span.#officeMsg').html().length);
                        if($('#officeMsg').html().length==2){
                            /*Checking length of message of messge is ok*/
                            return true;
                        }else{
                            ($('#officeMsg').clear);
                            return false;
                        }
                    });
                });
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <%--<jsp:useBean id="scBankCreationDtBean" class="com.cptu.egp.eps.web.databean.ScBankCreationDtBean" scope="request"/>--%>
                <jsp:useBean id="scBankCreationSrBean" class="com.cptu.egp.eps.web.servicebean.ScBankDevpartnerSrBean" scope="request"/>
                <%
                                String logUserId = "0";
                                if(session.getAttribute("userId")!=null){
                                        logUserId  = session.getAttribute("userId").toString();
                                    }
                                scBankCreationSrBean.setLogUserId(logUserId);
                %>
                <%--<jsp:setProperty name="scBankCreationDtBean" property="*" />--%>

                <!--Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">

                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1">
                            <div class="pageHead_1"><%=title%> Creation</div>
                            <!--Page Content Start-->
                            <!-- Success failure -->
                            <%
                                        String msg = request.getParameter("msg");
                                        if (msg != null && msg.equals("fail")) {%>
                            <div class="responseMsg errorMsg"><%=title%> Creation Failed.</div>
                            <%} else if (msg != null && msg.equals("success")) {%>
                            <div class="responseMsg successMsg"><%=title%> Created Successfully</div>
                            <%}%>
                            <!-- Result Display End-->
                            <form name="frmBankSchedule" action="<%=request.getContextPath()%>/ScBankServlet" method="POST" id ="frmBankSchedule">
                                <table width="100%" class="formStyle_1" border="0" cellpadding="0" cellspacing="10">
                                    <tr>

                                        <td style="font-style: italic; font-weight: normal;" colspan="2" class="t-align-left ff">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr><td class="ff" width="200">Name of <%=title%> : <span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtBankName" name="bankName" style="width: 400px;" type="text" maxlength="101" />
                                            <div id="officeMsg" style="font-weight: bold"></div>
                                            <div id="branchValidation" class="reqF_1"></div>
                                            <input type="hidden" name="partnerType" value="<%=type%>"/>
                                            <input type="hidden" name="isBranch" value="No"/>
                                            <input type="hidden" name="swiftCode" value=""/>
                                            <input type="hidden" name="action" value="create"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Address : <span>*</span></td>
                                        <td><textarea cols="20" rows="6" class="formTxtBox_1"  id="txtaAddress" name="officeAddress" style="width: 400px" onkeypress="return imposeMaxLength(this, 301, event);"></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <%if (type.equals("ScheduleBank")) {%>
                                        <td class="ff" width="200">Country :</td>
                                        <%} else {%>
                                        <td class="ff" width="200">Country : <span>*</span></td>
                                        <%}%>
                                        <td>

                                            <%if (!type.equals("ScheduleBank")) {%>
                                            <select class="formTxtBox_1" id="cmbCountry" name="country" style="width: 208px">
                                                <%

                                                    for (SelectItem countryItem : scBankCreationSrBean.getCountryList()) {
                                                        if (countryItem.getObjectValue().equalsIgnoreCase("Bhutan")) {
                                                %>
                                                <option  value="<%=countryItem.getObjectId()%>" selected="selected"><%=countryItem.getObjectValue()%></option>
                                                <%
                                                                                                                } else {
                                                %>
                                                <option  value="<%=countryItem.getObjectId()%>"><%=countryItem.getObjectValue()%></option>
                                                <%  }
                                                    }%>
                                            </select>
                                            <%} else {
                                                for (SelectItem countryItem : scBankCreationSrBean.getCountryList()) {
                                                    if (countryItem.getObjectValue().equalsIgnoreCase("Bhutan")) {
                                            %>
                                            <label id="cmbCountry" name="country" ><%=countryItem.getObjectValue()%></label>
                                            <input type="hidden" name="country" value="<%=countryItem.getObjectId()%>" />
                                            <%}
                                                            }
                                                        }%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Dzongkhag / District : <span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbState" name="state" style="width: 208px">
                                                <%
                                                            for (SelectItem stateItem : scBankCreationSrBean.getStateList((short) 150)) {
                                                                if (stateItem.getObjectValue().equalsIgnoreCase("Thimphu")) {
                                                %>
                                                <option  value="<%=stateItem.getObjectId()%>" selected="selected"><%=stateItem.getObjectValue()%></option>
                                                <%
                                                                                                                } else {
                                                %>
                                                <option  value="<%=stateItem.getObjectId()%>"><%=stateItem.getObjectValue()%></option>
                                                <%  }
                                                            }%>
                                            </select>
                                        </td>
                                    </tr>
                                    <%--<tr>
                                        <td class="ff" width="200">District<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbDistrict" name="district" style="width: 200px">
                                                <option></option>
                                            </select>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td class="ff" width="200">City / Town : </td>
                                        <td><input class="formTxtBox_1" id="txtCity" name="city" style="width: 200px;" type="text" maxlength="101"/>
                                        </td>
                                    </tr>
                                    <%
                                                if (type.equals("ScheduleBank")) {
                                    %>
<!--                                    <tr id="upjilla">
                                        <td class="ff" width="200">Thana / UpaZilla : <span>*</span></td>
                                        <td><input id="txtUpJilla" class="formTxtBox_1" name="thanaUpzilla" style="width: 200px;" type="text" maxlength="101"/>
                                        </td>
                                    </tr>-->
                                    <%}%>
                                    <%String maxLen = "maxlength='11'";
                                        if(type.equals("ScheduleBank")){
                                            maxLen = "maxlength='5'";
                                            }%>
                                    <tr>
                                        <td class="ff" width="200">Postcode : </td>
                                        <td>
                                            <input id="txtPostCode" name="postCode" class="formTxtBox_1" style="width: 200px;" type="text" maxlength="19"/>
                                            <span id="postCodeMsg" class="reqF_1"></span>
                                            <input type="hidden" id="hdnerrorcount" value="0"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Phone No. : <span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtPhoneNo" maxlength="245" style="width: 200px;" name="phoneNo" type="text"/>&nbsp;
                                            <%if (type.equals("ScheduleBank")) {%>
                                            <span id="phno" style="color: grey;"> (Area Code-Phone No. e.g. 02-336962)</span>
                                            <%} else {%>
                                            <span id="phno" style="color: grey;"> (Area Code-Phone No. e.g. 02-336962)</span>
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Fax No. :</td>
                                        <td><input class="formTxtBox_1" id="txtFaxNo" style="width: 200px;" name="faxNo" type="text" maxlength="245"/>&nbsp;
                                            <%if (type.equals("ScheduleBank")) {%>
                                            <span id="fxno" style="color: grey;"> (Area Code-Fax No. e.g. 02-336961)</span>
                                            <%} else {%>
                                            <span id="fxno" style="color: grey;"> (Area Code-Fax No. e.g. 02-336961)</span>
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Website :</td>
                                        <td><input class="formTxtBox_1" id="txtWebsite" style="width: 200px;" name="webSite" type="text" maxlength="51" />
                                            <div id="websiteToolTip" style="color: grey;">(Enter the Website name without 'http://' e.g. pppd.gov.bt)</div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            <label class="formBtn_1"><input id="btnSubmit" name="submit" value="Submit" type="submit" onclick="return ValPost();" disabled/>
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                    <script>
                            var partnerType ='<%=partnerType%>';
                            if(partnerType == 'Development'){
                                var obj = document.getElementById('lbldevPartnerCreation');
                                if(obj != null){
                                   // if(obj.innerHTML == 'Create Development Partner Organization'){
                                    obj.setAttribute('class', 'selected');
                                  //  }
                                }
                            }else{
                                var obj1 = document.getElementById('lblSchBankCreation');
                                if(obj1 != null){
                                    // if(obj1.innerHTML == 'Create'){
                                    obj1.setAttribute('class', 'selected');
                                    //}
                                }
                            }
                    </script>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script type="text/javascript">
                var obj = document.getElementById('lblDevlopPartnerOrgCreation');
                if(obj != null){
                    //if(obj.innerHTML == 'Create Development Partner Organization'){
                        obj.setAttribute('class', 'selected');
                   // }
                }
        
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
        </script>
    </body>
</html>

