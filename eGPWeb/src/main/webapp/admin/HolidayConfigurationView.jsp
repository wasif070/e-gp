<%--
    Document   : ConfigureCalendar
    Created on : Oct 30, 2010, 7:55:54 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblHolidayMaster"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigureCalendarSrBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<!--jsp:useBean id="configureCalendarSrBean" class="com.cptu.egp.eps.web.servicebean.ConfigureCalendarSrBean" /-->
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Holidays</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
       <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>

        <script type="text/javascript">

            function setval(selval){
                document.frmConfigureCalendar.action = 'HolidayConfigurationView.jsp?action=Remove&id='+selval
                document.frmConfigureCalendar.submit();
                //location.href='HolidayConfigurationView.jsp?action=Remove&id='+selval;
            }
        </script>
        <script type="text/javascript">
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 20) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 20) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 20) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 20) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 20) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 20) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <script type="text/javascript">
            function loadTable()
            {
                $.post("<%=request.getContextPath()%>/HolidayConfigurationServlet", {funName : "viewHolidayConfiguration",pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                   // sortTable();
                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),20);
                    var pageNo =$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),20);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),20);
                    var totalPages=parseInt($('#totalPages').val(),20);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo)+1);

                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 20) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),20);
                    var totalPages=parseInt($('#totalPages').val(),20);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
        </script>

    </head>
    <body onload="loadTable();">
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <form name="frmConfigureCalendar" id="frmConfigureCalendar" action="<%=request.getContextPath()%>/ConfigureCalendarSrBean" method="post" >
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                     
                                    StringBuilder userType = new StringBuilder();
                                    if (request.getParameter("hdnUserType") != null) {
                                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                            userType.append(request.getParameter("hdnUserType"));
                                        } else {
                                            userType.append("org");
                                        }
                                    } else {
                                        userType.append("org");
                                    }

                                     String msg ="";
                                    msg = request.getParameter("msg");
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1">
                                         <%
                                                ConfigureCalendarSrBean concal = new ConfigureCalendarSrBean();
                                                concal.setLogUserId(session.getAttribute("userId").toString());
                                                List<TblHolidayMaster> tblHolidayMaster = new ArrayList<TblHolidayMaster>();
                                                TblHolidayMaster holidayMaster  ;
                                                int i=1;
                                                long l = 0;

                                                if("Remove".equals(request.getParameter("action"))){
                                                    int j=1;
                                                    l = concal.countHolidayMasterData();                                                     
                                                     if(l>1){                                                    
                                                    String tr = request.getParameter("totrows");
                                                    int totrows = 0;
                                                     String holid = null;
                                                     if(tr != null)
                                                    totrows = Integer.parseInt(tr);
                                                    holid = request.getParameter("id");
                                                     holidayMaster = new TblHolidayMaster();
                                                     holidayMaster.setHolidayId(Integer.parseInt(holid));
                                                     holidayMaster.setHolidayDate(new Date());
                                                     holidayMaster.setIsWeekend("");
                                                     holidayMaster.setYear("");
                                                     concal.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                                                     concal.deletHolidayMaster(holidayMaster);
                                                     msg = "Holiday(s) removed successfully";

                                               } }
                                               
                                                 %>
                                        <div class="pageHead_1">Holidays
                                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('3');">Save as PDF</a></span>
                                        </div>
                                        <div>&nbsp;</div>
                                         <% if(msg != null && !msg.contains("not") &&  !msg.contains("removed")){ %>
                                             <div class="responseMsg successMsg">Holidays added successfully</div>
                                             <%  }else if(msg != null && msg.contains("not")){ %>
                                             <div class="responseMsg errorMsg">Holidays has not added successfully</div>
                                             <%  }else if(msg != null && msg.contains("removed")){ %>
                                             <div class="responseMsg successMsg">Holiday(s) removed successfully</div>
                                             <%  } %>

                                       <table width="100%" cellspacing="0" id="resultTable" class="tableList_3 t_space" >
                                                 <tr>
                                            <th width="4%" class="t-align-center">Sl. No.</th>
                                            <th width="26%" class="t-align-center">Date</th>
                                            <th width="50%" class="t-align-center">Description</th>
                                            <th width="15%" class="t-align-center">Action</th>
                                                 </tr>
                                        </table>
                                    <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                                    <form name="frmConfigureCalendarView" id="frmConfigureCalendarView" action="HolidayConfigurationView.jsp" method="post" >
                                                 <tr>
                                    <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">20</span></td>
                                    <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                        &nbsp;
                                        <label class="formBtn_1">
                                            <input type="button" name="button" id="btnGoto" id="button" value="Go To Page" />
                                        </label></td>
                                    <td align="right" class="prevNext-container"><ul>
                                            <li><font size="3">&laquo;</font> <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                                            <li><font size="3">&#8249;</font> <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                            <li><a href="javascript:void(0)" id="btnNext">Next</a><font size="3"> &#8250;</font></li>
                                            <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
                                        </ul></td>
                                    </tr></form>
                                          </table>
                                    <input type="hidden" id="pageNo" value="1"/>
                                    <input type="hidden" id="size" value="20"/>

                                               <%  if(l == 1){
                                                     %>
                                                     <script language="JavaScript" type="text/javascript">
                                                      jAlert("Atleast one row is expected "," Alert ", "Alert");
                                                      </script>
                                               <%  } %>
                                        <div>&nbsp;</div>
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
            </form>
            <form id="formstyle" action="" method="post" name="formstyle">

               <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
               <%
                 SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                 String appenddate = dateFormat1.format(new Date());
               %>
               <input type="hidden" name="fileName" id="fileName" value="Holiday_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="Holiday" />
            </form>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var obj = document.getElementById('lblHolidyConfigView');
                if(obj != null){
                    if(obj.innerHTML == 'View'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
        <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
