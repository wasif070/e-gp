<%--
    Document   : ProcurementApprovalTimelineConfig
    Created on : May 26, 2015, 10:49:26 AM
    Author     : Istiak (Dohatec)
--%>

<%@page import="com.cptu.egp.eps.service.serviceimpl.TenPaymentConfigService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.model.table.TblConfigDocFees"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "No-store, No-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "No-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Procurement Approval Timeline Configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>


        <%
            boolean isEdit = false;
            String cType = "multiple";
            
            if(request.getParameter("isEdit") != null && request.getParameter("isEdit").equalsIgnoreCase("y"))
            {
                isEdit = true;
                if(request.getParameter("cType") != null && request.getParameter("cType").equalsIgnoreCase("single"))
                    cType = "single";
            }
        %>

        <%if(isEdit){%> 
            <script type="text/javascript">
                $(document).ready(function() {
                    LoadTimeline();
                });                

                function LoadTimeline(){
                    $.post("<%=request.getContextPath()%>/ProcurementTimelineServlet", {action: "ViewTimeline", flag: "configuration", configureType: "<%=cType%>", id: "<%=request.getParameter("id")%>"},  function(j){
                        $('#resultTable').find("tr:gt(0)").remove();
                        $('#resultTable tr:last').after(j);
                    });
                }

                function DeleteRule(){

                    var checkedCount = $('input[type="checkbox"][name="RulesNo"]:checked').length;

                    if(checkedCount > 0){
                        var id = "";
                        $('input[type="checkbox"][name="RulesNo"]:checked').each(function(){
                            if($(this).val() != 'n')
                                id = $(this).val() + "," + id;
                        });
                        id = id.substr(0, (id.length-1));

                        if(confirm("Are You Sure You want to Delete?")){
                            if(id.length > 0){
                                $.post("<%=request.getContextPath()%>/ProcurementTimelineServlet", {action: "DeleteRows", rowNo: id, flag: "configuration"},  function(j){
                                    if(j == "false"){
                                        alert("Rule delete failed!!");
                                    }else{
                                        $('input[type="checkbox"][name="RulesNo"]:checked').each(function(){
                                            $(this).closest('tr').remove();
                                        });
                                        alert("Rule delete successfully.");
                                    }
                                });
                            }else if(id.length == 0 && checkedCount > 0){
                                $('input[type="checkbox"][name="RulesNo"]:checked').each(function(){
                                    $(this).closest('tr').remove();
                                });

                                alert("Rule delete successfully.");
                            }
                        }
                    }else{
                        alert("Please Select checkbox first!!!");
                    }
                }

                function NewOrEdit(id){
                    var edit = false;
                    
                    if($('#ApprovalAuthority_'+id).val() != $('#AAOld_'+id).val())
                        edit = true;
                    else if($('#ProcurementNature_'+id).val() != $('#PNOld_'+id).val())
                        edit = true;
                    else if($('#TotalNoOfDaysForEvaluation_'+id).val() != $('#TDEvalOld_'+id).val())
                        edit = true;
                    else if($('#NoOfDaysForApproval_'+id).val() != $('#DAOld_'+id).val())
                        edit = true;

                    if(edit){
                        if($('#NewOldEdit_'+id).val() == "Old")
                            $('#NewOldEdit_'+id).val("Edit");
                    }
                }
            </script>
        <%}else{%>
            <script type="text/javascript">
                $(document).ready(function(){
                    AddRule();
                });

                function DeleteRule(){

                    var checkedCount = $('input[type="checkbox"][name="RulesNo"]:checked').length;

                    if(checkedCount > 0){
                        if(confirm("Are You Sure You want to Delete?")){
                            $('input[type="checkbox"][name="RulesNo"]:checked').each(function(){
                                $(this).closest('tr').remove();
                            });
                        }
                    }else{
                        alert("Please Select checkbox first!!!");
                    }
                }
            </script>
        <%}%>

        <script type="text/javascript">

            var regex = new RegExp("^[0-9]+$");
            
            function AddRule(){               

                var lastId = Math.abs($('input[type="checkbox"][name="RulesNo"]:last').val());
                var newId = 0;
                if(isNaN(lastId))
                    newId = 1;
                else
                    newId = lastId + 1;

                $('#tbodyData').append("<tr>"+
                    "<td class='t-align-center' width='15%'><input id='chk_" + newId + "' name='RulesNo' type='checkbox' value='n' />"+
                    "<input type='hidden' name='ruleId' id='ruleId_"+ newId + " value='" + newId + "'>"+
                    "<input type='hidden' name='NewOldEdit' id='NewOldEdit_"+ newId + "' value='New'></td>"+
                    "<td class='t-align-center' width='15%'>" + ApprovalAuthorityList(newId) + "</td>"+
                    "<td class='t-align-center' width='15%'>" + ProcNatureList(newId) + "</td>"+
                    "<td class='t-align-center' width='15%'><input type='text' class='formTxtBox_1' onblur='CalculateDaysForEvaluation(" + newId + ")' name='NoOfDaysForTSC' id='NoOfDaysForTSC_" + newId + "' value=''></td>"+
                    "<td class='t-align-center' width='15%'><input type='text' class='formTxtBox_1' onblur='CalculateDaysForEvaluation(" + newId + ")' name='NoOfDaysForTECorPEC' id='NoOfDaysForTECorPEC_" + newId + "' value=''></td>"+
                    "<td class='t-align-center' width='15%'><input type='text' class='formTxtBox_1' name='TotalNoOfDaysForEvaluation' id='TotalNoOfDaysForEvaluation_" + newId + "' value='' readonly='readonly'></td>"+
                    "<td class='t-align-center' width='15%'><input type='text' class='formTxtBox_1' onblur='CheckApprovalDays(" + newId + ")' name='NoOfDaysForApproval' id='NoOfDaysForApproval_" + newId + "' value=''</td>"+
                    "</tr>");
            }

            function ProcNatureList(id){
                var list = "<select name='ProcurementNature' class='formTxtBox_1 procurementNature' id='ProcurementNature_" + id + "' style='width:100px;'>" +
                        "<option value='' selected='selected'>-- Select --</option>" +
                        "<option value='Goods'>Goods</option>" +
                        "<option value='Works'>Works</option>" +
                        "<option value='Services'>Services</option>" +
                        "</select>";
                return list;
            }

            function ApprovalAuthorityList(id){
                var list = "<select name='ApprovalAuthority' class='formTxtBox_1 approvalAuthority' id='ApprovalAuthority_" + id + "' style='width:100px;' onChange='ChangeApprovalAuthority(" + id + ")'>" +
                        "<option value='' selected='selected'>-- Select --</option>" +
                        "<option value='AO'>AO</option>" +
                        "<option value='BOD'>BOD</option>" +
                        "<option value='CCGP'>CCGP</option>" +
                        "<option value='HOPE'>HOPA</option>" +
                        "<option value='Minister'>Minister</option>" +
                        "<option value='PD'>PD</option>" +
                        "</select>";
                return list;
            }

            function CalculateDaysForEvaluation(id){
                var daysForTSC = Math.abs($('#NoOfDaysForTSC_'+id).val());
                var daysForTecPec = Math.abs($('#NoOfDaysForTECorPEC_'+id).val());
                var flag = false;
                
                if(!regex.test(daysForTSC) || $('#NoOfDaysForTSC_'+id).val().contains("-")){
                    flag = true;
                    daysForTSC = Math.round(0);
                }else if(!regex.test(daysForTecPec) || $('#NoOfDaysForTECorPEC_'+id).val().contains("-")){
                    flag = true;
                    daysForTecPec = Math.round(0);
                }
                
                var days = daysForTecPec + daysForTSC;
                $('#TotalNoOfDaysForEvaluation_'+id).val(days);

                if(flag)
                    alert('Please enter numeric value only.');

                <%if(isEdit){%>
                        if(!flag){NewOrEdit(id);}
                <%}%>
            }

            function CheckApprovalDays(id){
                var appDays = Math.abs($('#NoOfDaysForApproval_'+id).val());
                if(!regex.test(appDays) || $('#NoOfDaysForApproval_'+id).val().contains("-")){
                    alert('Please enter numeric value only.');
                }else{
                    <%if(isEdit){%>NewOrEdit(id);<%}%>
                }
            }

            function ChangeApprovalAuthority(id){

                if($('#ApprovalAuthority_'+id).val() == "CCGP"){
                    $('#NoOfDaysForApproval_'+id).val("Upto Tender Validity");
                    $('#NoOfDaysForApproval_'+id).attr('readonly', true);
                }else{
                    if($('#NoOfDaysForApproval_'+id).attr('readonly')){
                        $('#NoOfDaysForApproval_'+id).removeAttr('readonly');
                        $('#NoOfDaysForApproval_'+id).val("");
                    }

                }

                <%if(isEdit){%>NewOrEdit(id);<%}%>
            }

            function Validate(){
                var submit = true;
                
                if(submit){
                    $('.approvalAuthority').each(function(){
                        if($(this).val() == ''){
                            alert('Please Select Approval Authority');
                            submit = false;
                            return false;
                        }
                    });
                }
                
                if(submit){
                    $('.procurementNature').each(function(){
                        if($(this).val() == ''){
                            alert('Please Select Procurement Category');
                            submit = false;
                            return false;
                        }
                    });
                }

                if(submit){
                    $('.noOfDaysForTSC').each(function(){
                        if($(this).val() == ''){
                            alert('Please fillup all mandatory fields');
                            submit = false;
                            return false;
                        }else if(!regex.test($(this).val()) || $(this).val().contains("-")){
                            alert('Please enter numeric value only.');
                            submit = false;
                            return false;
                        }
                    });
                }

                if(submit){
                    $('.noOfDaysForTECorPEC').each(function(){
                        if($(this).val() == ''){
                            alert('Please fillup all mandatory fields');
                            submit = false;
                            return false;
                        }else if(!regex.test($(this).val()) || $(this).val().contains("-")){
                            alert('Please enter numeric value only.');
                            submit = false;
                            return false;
                        }
                    });
                }

                if(submit){
                    $('.noOfDaysForApproval').each(function(){
                        if($(this).val() == ''){
                            alert('Please fillup all mandatory fields');
                            submit = false;
                            return false;
                        }else if((!regex.test($(this).val()) || $(this).val().contains("-")) && $(this).val() != 'Upto Tender Validity'){
                            alert('Please enter numeric value only.');
                            submit = false;
                            return false;
                        }
                    });
                }

                if(submit){
                    $('input:hidden[name=NewOldEdit]').each(function(){
                        if($(this).val() == 'Old'){
                            submit = false;
                        }else if($(this).val() == 'New' || $(this).val() == 'Edit'){
                            submit = true;
                            return false;
                        }
                    });

                    if(!submit)
                        alert('You did not change or add any rule.');
                }

                return submit;
            }
        </script>

    </head>

    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea">
                        <div class="pageHead_1">Procurement Approval Timeline Configuration</div>
                        <div style="font-style: italic" class="t-align-left t_space"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>

                        <div align="right" class="t_space b_space">
                            <%if("multiple".equalsIgnoreCase(cType)){%>
                                <a href="javascript:void(0);" class="action-button-add" id="addRule" onclick="AddRule()">Add Rule</a>
                                <a href="javascript:void(0);" class="action-button-delete" id="removeRule" onclick="DeleteRule()">Remove Rule</a>
                            <%}%>
                        </div>

                        <br/>
                        <div >&nbsp;
                            <%if(request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("y")){%>
                                <div class='responseMsg successMsg'>Data save successfully</div>
                            <%}if(request.getParameter("msg") != null && request.getParameter("msg").equalsIgnoreCase("n")){%>
                                <div class='responseMsg errorMsg'>Data save failed</div>
                            <%}%>
                        </div>
                        <br/>
                        
                        <form action="<%=request.getContextPath()%>/ProcurementTimelineServlet?action=AddUpdate" method="post">

                            <table class="tableList_1" cellspacing="0" width="100%" id="resultTable">
                                <tbody id="tbodyData">
                                    <tr>
                                        <th class="t-align-center" width="5%">Select</th>
                                        <th class="t-align-center" width="16%">Approval Authority (<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="16%">Procurement <br/>Category (<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="15%">No of Days <br/>For TC (<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="16%">No of Days <br/>For TEC or PEC (<span class="mandatory" style="color: red;">*</span>)</th>
                                        <th class="t-align-center" width="16%">Total No of Days <br/>For Evaluation</th>
                                        <th class="t-align-center" width="16%">No of Days <br/>For Approval (<span class="mandatory" style="color: red;">*</span>)</th>
                                    </tr>
                                </tbody>
                            </table><div>&nbsp;</div>

                            <div align="center">
                                <span class="formBtn_1" >
                                    <input name="btnSubmit" align="center" id="btnSubmit" value="Submit" type="submit" onclick="return Validate();">
                                </span>
                            </div>
                            <div>&nbsp;</div>

                        </form>
                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <script>
                var obj = document.getElementById('procAppTimeEdit');
                if(obj != null){
                    if(obj.innerHTML == 'Edit'){
                        obj.setAttribute('class', 'selected');
                    }
                }
                
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
        </script>
</html>

