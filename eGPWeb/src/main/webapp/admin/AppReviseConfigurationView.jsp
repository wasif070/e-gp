<%-- 
    Document   : AppReviseConfigurationView
    Created on : May 11, 2016, 3:54:40 PM
    Author     : aprojit
--%>

<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblAppreviseConfiguration"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.servicebean.AppReviseSrBean"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonAppData"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
         <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
         %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>APP Revise Date Configuration View</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        
         <%
          AppReviseSrBean appReviseSrBean = new AppReviseSrBean();
          
          appReviseSrBean.setLogUserId(session.getAttribute("userId").toString());
         %>
    </head>
    <jsp:useBean id="appServlet" scope="request" class="com.cptu.egp.eps.web.servlet.APPServlet"/>
    <body>
        <% 
        java.util.List<CommonAppData> appListDtBean = new java.util.ArrayList<CommonAppData>();
        appListDtBean = appServlet.getAPPDetails("FinancialYear", "", "");  
        String mesg = request.getParameter("msg");
        String Editable = "";
        for (CommonAppData commonApp : appListDtBean) {
            if ("Yes".equalsIgnoreCase(commonApp.getFieldName3())) {
                Editable = commonApp.getFieldName2();
            }
        }
        %>

        <div class="dashboard_div">
                    <!--Dashboard Header Start-->
                    <div class="topHeader">
                        <%@include file="../resources/common/AfterLoginTop.jsp" %>
                    </div>
                    <!--Dashboard Header End-->
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                            <td class="contentArea">
                                <!--Dashboard Content Part Start-->
                                <div class="pageHead_1">APP Revise Date Configuration View</div>
                                
                                <div>&nbsp;</div>
                                <%if(mesg != null){%>
                                <div class="responseMsg successMsg"><%=mesg%></div>
                                <%}%>
                                <div>&nbsp;</div>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space" id="tblappRevise" name="tblappRevise">
                                    <tr>
                                        <th>Financial Year</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Maximum Revise</th>
                                        <th>Action</th>
                                    </tr>
                                      <%
                                        String msg ="";
                                        List<TblAppreviseConfiguration> lst = appReviseSrBean.getAllConfiguration();
                                        if(!lst.isEmpty()){
                                            Iterator it = lst.iterator();
                                            int i=0;
                                            while(it.hasNext())
                                                {
                                                    TblAppreviseConfiguration tblAppreviseConfiguration = (TblAppreviseConfiguration)it.next();
                                                    i++;
                                     %>
                                     <%if(i%2==0){%>
                                        <tr style='background-color:#E4FAD0;'>
                                            <%}else{%>
                                            <tr>
                                            <%}%>
                                                <td class="t-align-center"><%=tblAppreviseConfiguration.getFinancialYear()%></td>
                                                <td class="t-align-center"><%=DateUtils.convertStrToDatenew(tblAppreviseConfiguration.getStartDate())%></td>                                                 
                                                <td class="t-align-center"><%=DateUtils.convertStrToDatenew(tblAppreviseConfiguration.getEndDate())%></td>
                                                <td class="t-align-center"><%=tblAppreviseConfiguration.getMaxReviseCount()%></td>
                                                <td class="t-align-center">
                                                    <%if(Editable.equals(tblAppreviseConfiguration.getFinancialYear().trim())){%>
                                                    <a href="AppReviseConfiguration.jsp?edit=true&appConfgId=<%=tblAppreviseConfiguration.getFinancialYear()%>">Edit</a>
                                                    <%}%>
                                                </td>
                                           </tr>
                                    <%
                                                   }
                                            }else
                                           msg = "No Record Found";
                                    %>
                                    
                                </table>
                                     <div align="center"><h2><%=msg%></h2></div>
                            </td>
                        </tr>
                    </table>

          <div>&nbsp;</div>

           <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
</html>
