<%--
    Document   : CreateTemplate
    Created on : 24-Oct-2010, 1:03:35 AM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBean" />
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />
<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster" %>
<%@page import="java.util.List" %>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%

          //  if(session.getAttribute("userId") != null){
              //  createSubSectionSrBean.setLogUserId(session.getAttribute("userId").toString());
             //   defineSTDInDtlSrBean.setLogUserId(session.getAttribute("userId").toString());
           // }
            int sectionId = Integer.parseInt(request.getParameter("sectionId"));
            int templateId = Integer.parseInt(request.getParameter("templateId"));
            String procType = "";
            String procTypefull = "";
            String STDName = "";
            
            List<TblTemplateMaster> templateMasterLst = defineSTDInDtlSrBean.getTemplateMaster((short) templateId);
            if(templateMasterLst != null){
                if(templateMasterLst.size() > 0){
                    procType = templateMasterLst.get(0).getProcType();
                    if("goods".equalsIgnoreCase(procType)){
                        procTypefull = "Goods";
                   }else if("works".equalsIgnoreCase(procType)){
                        procTypefull = "Works";
                    }else if("srvcmp".equalsIgnoreCase(procType)){
                        procTypefull = "Services - Consulting Firms";
                    }else if("srvindi".equalsIgnoreCase(procType)){
                        procTypefull = "Services - Individual Consultant";
                }
                    else if("srvnoncon".equalsIgnoreCase(procType)){
                        procTypefull = "Services - Non consulting services";
                    }
                    STDName = templateMasterLst.get(0).getTemplateName();
                }
                templateMasterLst = null;
            }
            List<com.cptu.egp.eps.model.table.TblIttHeader> subSectionDetail = createSubSectionSrBean.getSubSectionDetail(sectionId);
            String contentType = createSubSectionSrBean.getContectType(sectionId);
            String sectionName = "";
            sectionName = createSubSectionSrBean.getSectionName(sectionId);
            
            if ("ITT".equals(contentType)) {  contentType="ITB";} 
            else if("TDS".equals(contentType)) {contentType="BDS";} 
            else if("PCC".equals(contentType)) {contentType="SCC";} 
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%=contentType%> Dashboard</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <!--<script type="text/javascript" src="include/pngFix.js"></script>-->
    </head>
    <body>
        <div class="mainDiv">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <div class="fixDiv">
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1"><%=contentType%> Dashboard</div>
                            </div>

                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td><a href="DefineSTDInDtl.jsp?templateId=<%=request.getParameter("templateId")%>" class="action-button-goback">Go Back to  SBD Dashboard</a></td>
                                </tr>
                            </table>
                            <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <tr>
                                    <td width="30%" align="center"><b>Name of SBD :</b></td>
                                    <td width="70%"><%=STDName%></td>
                                </tr>
                                <tr>
                                    <td width="30%" align="center"><b>Procurement Type :</b></td>
                                    <td width="70%"><%=procTypefull%></td>
                                </tr>
                                <tr>
                                    <td width="30%" align="center"><b>Section :</b></td>
                                    <td width="70%"><%=sectionName%></td>
                                </tr>
                            </table>
                            <form action="DefineSTDInDtl.jsp" method="post">
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                <%
                                for(int i=0;i<subSectionDetail.size();i++){
                                %>
                                    <tr>
                                        <td width="30%" align="center"><b>Sub Section No.</b></td>
                                        <td width="70%"><%=(i+1)%></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><b>Name of Sub Section</b></td>
                                        <td><%=subSectionDetail.get(i).getIttHeaderName() %></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><b>Action</b></td>
                                        <td>
                                            <%
                                            if(!createSubSectionSrBean.clauseCreated(subSectionDetail.get(i).getIttHeaderId())){
                                            %>
                                                <a href="<%=request.getContextPath()%>/admin/CreateSectionClause.jsp?templateId=<%= templateId %>&ittHeaderId=<%=subSectionDetail.get(i).getIttHeaderId()%>&sectionId=<%=request.getParameter("sectionId")%>&templateId=<%=templateId%>">Create</a>
                                            <%
                                            }else{
                                            %>
                                                <a href="<%=request.getContextPath()%>/admin/CreateSectionClause.jsp?templateId=<%= templateId %>&ittHeaderId=<%=subSectionDetail.get(i).getIttHeaderId()%>&sectionId=<%=request.getParameter("sectionId")%>&templateId=<%=templateId%>&edit=true">Edit</a> |
                                                <a href="<%=request.getContextPath()%>/admin/ViewSectionClause.jsp?templateId=<%= templateId %>&ittHeaderId=<%=subSectionDetail.get(i).getIttHeaderId()%>&sectionId=<%=request.getParameter("sectionId")%>&templateId=<%=templateId%>">View</a>
                                            <%
                                            }
                                            %>
                                            
                                            
                                        </td>
                                    </tr>
                                <%
                                }
                                subSectionDetail=null;
                                createSubSectionSrBean=null;
                                %>
                                </table>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
            <%
    if(defineSTDInDtlSrBean != null){
        defineSTDInDtlSrBean = null;
    }
%>