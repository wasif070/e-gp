<%-- 
    Document   : EditConfigPaThreshold
    Created on : Apr 23, 2017, 11:39:36 AM
    Author     : feroz
--%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigPaThreshold"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.ConfigPaThresholdService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Edit Threshold Configuration</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        
        <%
                    ConfigPaThresholdService configPaThresholdService = (ConfigPaThresholdService) AppContext.getSpringBean("ConfigPaThresholdService");
                    String msg = "";
                    String action = "";
                    if ("Update".equals(request.getParameter("button"))) 
                    {
                        try{
                                BigDecimal minvalue = new BigDecimal(request.getParameter("minValue"));
                                BigDecimal maxvalue = new BigDecimal(request.getParameter("maxValue"));
                                TblConfigPaThreshold configPaThreshold = new TblConfigPaThreshold();
                                configPaThreshold.setConfigPathresholdId(Integer.parseInt(request.getParameter("thresholdId")));
                                configPaThreshold.setArea(request.getParameter("getArea"));
                                configPaThreshold.setProcurementNatureId((byte)Integer.parseInt(request.getParameter("natureId")));
                                configPaThreshold.setMaxValue(maxvalue);
                                configPaThreshold.setMinValue(minvalue);
                                msg = configPaThresholdService.updateTblConfigPaThreshold(configPaThreshold);
                            }catch(Exception e){
                                System.out.println(e);
                                action = "Error in : "+action +" : "+ e;
                            }finally{
                                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                                makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                                action=null;
                            }
                         
                        response.sendRedirect("ViewConfigPaThreshold.jsp?msg=" + msg);
                        

                    }
         %>
         
         <script type="text/javascript">
            
            function decimal(value) {
                return /^(\d+(\.\d{2})?)$/.test(value);
            }
            function chkMax(value)
            {
                var chkVal=value;
                var ValSplit=chkVal.split('.');
                //alert(chkVal);
                //alert(ValSplit[0].length);
                if(ValSplit[0].length>12)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
             function validate(){
                var chk=true;
                var obj = document.getElementById("minValue").value;
                if(obj!="")
                {
                    document.getElementById("minvY").innerHTML = "";
                    if(decimal(obj))
                    {
                        document.getElementById("minvN").innerHTML = "";
                        if((parseFloat(obj)) <= (parseFloat(document.getElementById("maxValue").value)))
                        {
                            document.getElementById("maxvG").innerHTML="";
                            document.getElementById("minvG").innerHTML="";
                            if(!chkMax(obj))
                            {
                                document.getElementById("minvY").innerHTML = "";
                                document.getElementById("minvG").innerHTML = "";
                                document.getElementById("minvN").innerHTML = "";
                                document.getElementById("minv").innerHTML="Maximum 12 digits are allowed.";
                                chk=false;
                            }
                            else
                            {
                                document.getElementById("minv").innerHTML="";
                                
                            }
                        }
                        else if(document.getElementById("maxValue").value!="")
                        {
                            document.getElementById("minv").innerHTML = "";
                            document.getElementById("minvY").innerHTML = "";
                            document.getElementById("minvN").innerHTML = "";
                            document.getElementById("minvG").innerHTML="Minimum value must be less than or equal to the Maximum value in Nu.";
                            chk=false;
                        }
                        else
                        {
                            document.getElementById("minvG").innerHTML = "";
                        }
                        
                    }
                    else
                    {
                        document.getElementById("minv").innerHTML = "";
                        document.getElementById("minvG").innerHTML = "";
                        document.getElementById("minvY").innerHTML = "";
                        document.getElementById("minvN").innerHTML="<br/>Please Enter Numbers ( Maximum 2 digits are allowed after decimal).";
                        chk=false;
                    }
                }
                else
                {
                    document.getElementById("minv").innerHTML = "";
                    document.getElementById("minvG").innerHTML = "";
                    document.getElementById("minvN").innerHTML = "";
                    document.getElementById("minvY").innerHTML = "<br/>Please Enter Minimum Value in Nu.";
                    chk=false;
                }
                var chk1=true;
                var obj2 = document.getElementById("maxValue").value;
                if(obj2!="")
                {
                    document.getElementById("maxvY").innerHTML="";
                    if(decimal(obj2))
                    {
                        document.getElementById("maxvN").innerHTML = "";
                        //checking for non-zero

//                        if((obj)==0)
//                        {
//                            document.getElementById("maxv").innerHTML="<br>Zero value is not allowed.";
//                            chk1=false;
//                        }
//                        else
                        { //for max value comparision
                            if((parseFloat(obj2)) >= (parseFloat(document.getElementById("minValue").value)))
                            {
                                document.getElementById("minvG").innerHTML="";
                                document.getElementById("maxvG").innerHTML="";
                                if(!chkMax(obj2))
                                {
                                    document.getElementById("maxN").innerHTML = "";
                                    document.getElementById("maxvY").innerHTML = "";
                                    document.getElementById("maxvG").innerHTML = "";
                                    document.getElementById("maxv").innerHTML="Maximum 12 digits are allowed.";
                                    chk1=false;
                                }
                                else
                                {
                                    document.getElementById("maxv").innerHTML="";
                                    
                                }
                            }
                            else if(document.getElementById("minValue").value!="")
                            {
                                document.getElementById("maxv").innerHTML = "";
                                document.getElementById("maxvY").innerHTML = "";
                                document.getElementById("maxvN").innerHTML = "";
                                document.getElementById("maxvG").innerHTML="Maximum value must be greater than  or equal to the Minimum value in Nu.";
                                chk1=false;
                            }
                            else
                            {
                                document.getElementById("maxvG").innerHTML = "";
                            }
                        }
                    }
                    else
                    {
                        document.getElementById("maxv").innerHTML = "";
                        document.getElementById("maxvY").innerHTML = "";
                        document.getElementById("maxvG").innerHTML = "";
                        document.getElementById("maxvN").innerHTML="<br/>Please Enter Numbers ( Maximum 2 digits are allowed after decimal).";
                        chk1=false;
                    }
                }
                else
                {
                    document.getElementById("maxv").innerHTML = "";
                    document.getElementById("maxvN").innerHTML = "";
                    document.getElementById("maxvG").innerHTML = "";
                    document.getElementById("maxvY").innerHTML = "<br/>Please Enter Maximum Value in Nu.";
                    chk1=false;
                }
                if(chk == false || chk1 == false)
                {
                    return false;
                }
            }
            
            
         </script>
        
    </head>
    <body>
        <%
            if(request.getParameter("ConfigPaThresholdId")!=null)
            {
                List<TblConfigPaThreshold> paThresholdThis = configPaThresholdService.findTblConfigPaThreshold(Integer.parseInt(request.getParameter("ConfigPaThresholdId")));
            
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
         
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea">
                        <div class="pageHead_1">Edit Threshold Configuration</div>
                        <form action="EditConfigPaThreshold.jsp" method="post">
                            <table class="tableList_1 t_space" cellspacing="0" width="100%" id="resultTable" cols="@2">
                                <tbody id="tbodyData">&nbsp;
                                    <tr>
                                        <th class="t-align-center">Area</th>
                                        <th class="t-align-center">Procurement Category</th>
                                        <th class="t-align-center">Max Value (In Nu.)</th>
                                        <th class="t-align-center">Min Value (In Nu.)</th>
                                    </tr>
                                    <tr>
                                        <td class="t-align-center">
                                            <%
                                                 if(paThresholdThis.get(0).getArea().contains("SubDistrict"))
                                                 {
                                                     out.print("Dungkhag");
                                                 }
                                                 else if(paThresholdThis.get(0).getArea().contains("District"))
                                                 {
                                                     out.print("Dzongkhag");
                                                 }
                                                 else if(paThresholdThis.get(0).getArea().contains("Division"))
                                                 {
                                                     out.print("Department");
                                                 }
                                                 else if(paThresholdThis.get(0).getArea().contains("Organization"))
                                                 {
                                                     out.print("Division");
                                                 }
                                                 else if(paThresholdThis.get(0).getArea().contains("Autonomus"))
                                                 {
                                                     out.print("Autonomous");
                                                 }
                                                 else
                                                 {
                                                     out.print(paThresholdThis.get(0).getArea());
                                                 }
                                                %>
                                        </td>
                                        <td class="t-align-center">
                                                <%
                                                 if(paThresholdThis.get(0).getProcurementNatureId()==1)
                                                 {
                                                     out.print("Goods");
                                                 }
                                                 else if(paThresholdThis.get(0).getProcurementNatureId()==2)
                                                 {
                                                     out.print("Works");
                                                 }
                                                 else
                                                 {
                                                     out.print("Services");
                                                 }
                                                %>
                                            </td>
                                            
                                            <input type="hidden" name="thresholdId" id="thresholdId" value="<%=paThresholdThis.get(0).getConfigPathresholdId()%>">
                                            <input type="hidden" name="getArea" id="getArea" value="<%=paThresholdThis.get(0).getArea()%>">
                                            <input type="hidden" name="natureId" id="natureId" value="<%=paThresholdThis.get(0).getProcurementNatureId()%>">
                                            
                                            <td class="t-align-center"><input name="maxValue" type="text" class="formTxtBox_1" maxlength="18" style="width:90%;" id="maxValue" value="<%=paThresholdThis.get(0).getMaxValue()%>" onblur="return validate();"/><span id="maxv" style="color: red;">&nbsp;</span><span id="maxvG" style="color: red;">&nbsp;</span></span><span id="maxvY" style="color: red;">&nbsp;</span><span id="maxvN" style="color: red;">&nbsp;</span></td>
                                            <td class="t-align-center"><input name="minValue" type="text" class="formTxtBox_1" maxlength="18" style="width:90%;" id="minValue" value="<%=paThresholdThis.get(0).getMinValue()%>" onblur="return validate();"/><span id="minv" style="color: red;">&nbsp;</span><span id="minvG" style="color: red;">&nbsp;</span><span id="minvY" style="color: red;">&nbsp;</span><span id="minvN" style="color: red;">&nbsp;</span></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" cellpadding="0" >
                                <tr>
                                    <td class="t-align-center"><span class="formBtn_1"><input type="submit" name="button" id="button" value="Update" onclick="return validate();"/></span></td>
                                </tr>
                            </table>
                        </form>
                        
                    </td>
            </tr>
        </table>
         

            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <%}%>
        
    </body>
    <script>
        var obj = document.getElementById('lblConfigPaThreshold');
        if(obj != null){
            if(obj.innerHTML == 'View'){
                obj.setAttribute('class', 'selected');
            }
        }

    </script>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabConfig");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>
