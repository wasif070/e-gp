<%-- 
    Document   : ProgrammeMaster
    Created on : Oct 28, 2010, 2:59:59 PM
    Author     : Naresh.Akurathi
--%>


<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<jsp:useBean id="progmasterDtBean" class="com.cptu.egp.eps.web.databean.ProgrammeMasterDtBean"/>
<jsp:useBean id="progmasterSrBean" class="com.cptu.egp.eps.web.servicebean.ProgrammeMasterSrBean"/>
<jsp:setProperty property="*" name="progmasterDtBean"/>

<%@page import="com.cptu.egp.eps.model.table.TblProgrammeMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ProgrammeMasterSrBean"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Programme Information</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>


        <script type="text/javascript">

        $(function() {

            /*$('#txtCode').blur(function() {
                if($('#txtCode').val()!=""){
                var alNumRegex = /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/;
                if(alNumRegex.test($('#txtCode').val())) {
                $.post("<-%=request.getContextPath()%>/CommonServlet", {pCode:$('#txtCode').val(),funName:'checkpCode',editId:$('#editId').val(),action:$('#action').val()},  function(j){
                  if(j.toString().indexOf("OK", 0)!=-1){
                        $('span.#mymsg1').css("color","green");                        
                    }
                    else if(j.toString().indexOf("Programme", 0)!=-1){
                        $('span.#mymsg1').css("color","red");
                    }
                    $('span.#mymsg1').html(j);
                });
                }else{
                     $('span.#mymsg1').css("color","red");
                     $('span.#mymsg1').html("Only Alphanumerics are not allowed");
                }
                }else{
                     $('span.#mymsg1').css("color","red");
                     $('span.#mymsg1').html("Please enter Programme Code");
                }
            });*/
            $('#txtCode').blur(function() {
                if($.trim($('#txtCode').val()) == ""){
                    $('span.#mymsg1').css("color","red");
                    $('span.#mymsg1').html("Please enter Programme Code");
                }else{
                    $('span.#mymsg1').html("");
                }
            });

        });
        </script>

        <script type="text/javascript">
        $(function() {

            $('#txtName').blur(function() {
                if($('#txtName').val()!=""){
                 var alNumRegex = /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/;
                 if(alNumRegex.test($('#txtName').val())) {
                $.post("<%=request.getContextPath()%>/CommonServlet", {pName:$('#txtName').val(),funName:'checkpName',editId:$('#editId').val(),action:$('#action').val()},  function(j){

                    if(j.toString().indexOf("OK", 0)!=-1){
                        $('span.#mymsg2').css("color","green"); 
                    }
                    else if(j.toString().indexOf("Programme", 0)!=-1){
                        $('span.#mymsg2').css("color","red"); 
                    }
                    $('span.#mymsg2').html(j);
                });
                }
            else{
                $('span.#mymsg2').css("color","red");
                     $('span.#mymsg2').html("Only special characters are not allowed");

            }
            }else{
                    $('span.#mymsg2').css("color","red");
                     $('span.#mymsg2').html("Please enter Programme Name ");

            }
            });
            

        });
        </script>
        <script type="text/javascript">
            function Blockvalidation()
            {
                if(document.getElementById("txtOwner").value!=null)
                {
                    document.getElementById("mymsg3").innerHTML = "";
                }
            }
        </script>
        <script type="text/javascript">
        $(function() {
            $('#txtOwner').blur(function() {
                if($('#txtOwner').val()!=""){
                var alNumRegex = /^(?![0-9]+$)(([a-zA-Z 0-9])(?![0-9]+$)([a-zA-Z 0-9\&\,\'\"\(\)\{\}\_\.\-]+[\&\,\'\"\(\)\{\}\_\.\-\a-zA-Z 0-9])?)+$/;

                 if(alNumRegex.test($('#txtOwner').val())) {
               /* $.post("<a%=request.getContextPath()%>/CommonServlet", {pOwner:$('#txtOwner').val(),funName:'checkpOwner',editId:$('#editId').val(),action:$('#action').val()},  function(j){

                    if(j.toString().indexOf("OK", 0)!=-1){
                        $('span.#mymsg3').css("color","green");
                    $('span.#mymsg3').html("Only special characters are not allowed");
                    }
                    else if(j.toString().indexOf("Programme", 0)!=-1){
                        $('span.#mymsg3').css("color","red");
                    }
                    $('span.#mymsg3').html(j);
                });
                    $('span.#mymsg3').css("color","green");
                    $('span.#mymsg3').html("OK");*/
                }
                else{
                     $('span.#mymsg3').css("color","red");
                     $('span.#mymsg3').html("Only special characters are not allowed");
                }
                }else{
                    $('span.#mymsg3').css("color","red");
                     $('span.#mymsg3').html("Please enter Programme Owner");
                }
            });

        });
        </script>

        <script type="text/javascript">

                    function onsubcheck(){
                        if($.trim($('#txtCode').val())== '' && document.getElementById("txtName").value=='' && document.getElementById("txtOwner").value==''){
//                            var codeVal = document.getElementById("txtCode").value;
//                           if(!chkAlpha(codeVal)){
//                               $('span.#mymsg1').html("Only Special characters are not allowed.");
//                           }
//
//                           if(!chkAlpha(document.getElementById("txtName").value)){
//                               $('span.#mymsg2').html("Only Special characters are not allowed.");
//                           }
//                           if(!chkAlpha(document.getElementById("txtOwner").value)){
//                               $('span.#mymsg2').html("Only Special characters are not allowed.");
//                           }

                           
                            $('span.#mymsg1').html("Please enter Programme Code");
                            $('span.#mymsg2').html("Please enter Programme Name");
                            $('span.#mymsg3').html("Please enter Programme Owner");
                            //$('span.#mymsg').html("Please Enter Fields");
                        }

                     if($('#mymsg1').html()=="OK" && $('#mymsg2').html()=="OK" && $('#mymsg3').html()=="OK"){
                          //$("#button").hide();
                          //alert('cam13');
                          return true;
                     }else if(($('#mymsg1').html()=="OK" && $('#mymsg2').html()=="OK" && document.getElementById("txtCode").value!='' && document.getElementById("txtName").value!='' && document.getElementById("txtOwner").value != '') && $('#mymsg3').html()==''){
                         //alert('cam12');
                         return true;
                     }else if( ($('#mymsg1').html()=="OK"  && $('#mymsg3').html()=="OK" && document.getElementById("txtCode").value!='' && document.getElementById("txtName").value!='' && document.getElementById("txtOwner").value != '') &&  $('#mymsg2').html()=='' ){
                        //alert('cam0');
                        return true;
                     }else if(($('#mymsg2').html()=="OK"  && $('#mymsg3').html()=="OK" && document.getElementById("txtCode").value!='' && document.getElementById("txtName").value!='' && document.getElementById("txtOwner").value != '' ) &&  $('#mymsg1').html()=='' ){
                        // alert('cam1');
                         return true;
                     }else if(( $('#mymsg3').html()=="OK" && document.getElementById("txtCode").value!='' && document.getElementById("txtName").value!=''  && document.getElementById("txtOwner").value != '' ) && ( $('#mymsg1').html()=='' && $('#mymsg2').html()=='') ){
                        //alert('cam2');
                        return true;
                     }else if(( $('#mymsg2').html()=="OK" && document.getElementById("txtCode").value!='' && document.getElementById("txtName").value!=''  && document.getElementById("txtOwner").value != '') && ( $('#mymsg3').html()=='' && $('#mymsg1').html()=='') ){
                       //alert('cam3');
                       return true;
                     }else if(( $('#mymsg1').html()=="OK" && document.getElementById("txtCode").value!=''&& document.getElementById("txtName").value!=''  && document.getElementById("txtOwner").value != '') && ( $('#mymsg2').html()=='' && $('#mymsg3').html()=='') ){
                        // alert('cam4');
                          return true;
                     }else if((document.getElementById("txtCode").value!='' && document.getElementById("txtName").value!='' && document.getElementById("txtOwner").value !='' ) && ( $('#mymsg2').html()=='' && $('#mymsg3').html()==''  && $('#mymsg1').html()=='') ){
                        // alert('cam');
                        return true;
                     } else{
                          //alert("Enter Fields Properly");
                          //$('span.#mymsg').html("Enter Fields Properly");
                          return false;
                      }
                    }

                   
  </script>




    </head>
    <body>
        <%!
        
        TblProgrammeMaster tpm = new TblProgrammeMaster();
        ProgrammeMasterSrBean prgMasterSrBean = new ProgrammeMasterSrBean();

        %>

        <%
        int forId = 0;
        String valueId = "";
        String forCode = "";
        String forName = "";
        String forOwner = "";
        String msg = "";
        String action = request.getParameter("action");
        progmasterSrBean.setAuditTrail(null);
            if("Submit".equals(request.getParameter("button")) || "Update".equals(request.getParameter("button"))||"Cancel".equals(request.getParameter("button"))){//Runs when user click on Submit button
                    progmasterSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), request.getSession().getAttribute("sessionId"), request.getSession().getAttribute("userTypeId"), request.getRequestURL()));
                         valueId = request.getParameter("id");
                     int id = Integer.parseInt(valueId);                    

                     if(id != 0){ //runs for update operation       
                         
                         msg= progmasterSrBean.updateProgrammeMaster(progmasterDtBean,id);
                         if(msg.equals("Values Updated")){id=0;
                                msg = "Programme Master Modified";
                                response.sendRedirect("ProgrammeMasterView.jsp?msg="+msg);
                                }                            
                        }else{ //runs for adding operation
                            msg = progmasterSrBean.addProgrammeMaster(progmasterDtBean);
                             if(msg.equals("Values Added")){
                                 msg = "Programme Information added successfully";
                                response.sendRedirect("ProgrammeMasterView.jsp?msg="+msg);
                                }
                        }
                     


                }else if("Cancel".equals(request.getParameter("btnCancel"))){

                        response.sendRedirect("ProgrammeMasterView.jsp");

                    }else{
                valueId = request.getParameter("id");
                if(valueId == null)
                {}
                else{ //runs for diaplay & editing the data
                        int id=Integer.parseInt(valueId);
                        Iterator it = prgMasterSrBean.getData(id).iterator();

                        while(it.hasNext())
                            {
                                tpm = (TblProgrammeMaster)it.next();
                                forCode = tpm.getProgCode();
                                forName = tpm.getProgName();
                                forOwner = tpm.getProgOwner();
                                forId = tpm.getProgId();
                            }
                      }
                }

        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
       <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <%--<%@include file="/resources/common/AfterLoginLeft.jsp" %>--%>

                        <td class="contentArea">

            <!--Dashboard Content Part Start-->
            <div class="pageHead_1">Programme Information</div>
            <form id="progForm" method="post" action="ProgrammeMaster.jsp">
                <input type="hidden" name="pid" vlaue="<%%>"/>
                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                    <tr>
                        <td>&nbsp;</td>
                        <td><font color="maroon"><b><%= msg%></b></font></td>
                    </tr>
                    <tr>
                        <td class="ff">Programme Code : <span>*</span></td>
                        <td><input name="progCode" id="txtCode" type="text" class="formTxtBox_1" value="<%=forCode%>" style="width:200px;" /><br>
                        <span id="mymsg1" style="color: red;"></span>
                        <input type="hidden" name="editId" id="editId" value="<%=forId%>"/>
                        <input type="hidden" name="action" id="action" value="<%=action%>"/>
                        
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Programme Name : <span>*</span></td>
                        <td><input name="progName" id="txtName" type="text" class="formTxtBox_1" value="<%=forName%>" style="width:200px;" /><br>
                        <span id="mymsg2" style="color: red;"></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="ff">Implementing Agency : <span>*</span></td>
                        <td><input name="progOwner" id="txtOwner" type="text" class="formTxtBox_1" value="<%=forOwner%>" style="width:200px;" onblur="Blockvalidation()"/><br>
                        <span id="mymsg3" style="color: red;"></span>
                        </td>
                    </tr>
                    
                    <!--<input type="hidden" name="createdBy" id="createdBy" value="<%=session.getAttribute("userId")%>" />-->

                   

                    <tr>
                        <td>&nbsp;</td>
                        <%if(request.getParameter("id") == null){%>
                        <td><label class="formBtn_1">
                                <input type="submit" name="button" id="button" value="Submit"  onclick=" return onsubcheck();" />
                            </label>
                        </td>
                         <%}else{%>
                         <td><label class="formBtn_1">
                                 <input type="submit" name="button" id="button" value="Update" onclick="return onsubcheck();" />
                            </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                             <label class="formBtn_1">
                                <input type="submit" name="btnCancel" id="button" value="Cancel" />
                            </label>
                        </td>
                        <%}%>
                        <td colspan="3"></td>
                    </tr>

                     <tr>                        
                        <td><input name="id"  type="hidden" value="<%= forId%>" /></td>
                        <td class="ff"><span id="mymsg" style="color: red; font-weight: bold"></span>&nbsp;</td>
                    </tr>
                </table>
            </form>

                         </td>
                    </tr>
                </table>
            <!--Dashboard Content Part End-->

            <!--Dashboard Footer Start-->
            <%@include file="/resources/common/Bottom.jsp" %>
            <!--Dashboard Footer End-->
        </div>
            <script>
                var obj = document.getElementById('lblProgInfoCreate');
                if(obj != null){
                    if(obj.innerHTML == 'Create Programme'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>


