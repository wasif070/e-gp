<%-- 
    Document   : DevelopmentPartnerOrganization
    Created on : Oct 23, 2010, 7:44:02 PM
    Author     : rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
         <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Development Partner Organization</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />

          <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
         <script type="text/javascript">
     $(document).ready(function() {
        $("#frmDevelopmentPartnerOrganization").validate({
            rules: {
                Name:{required: true},
                Address:{required:true},
                Country:{required:true},
                District:{required:true},
                City:{required:true,maxlength:50},
                postCode:{number:true,minlength:4},
                phoneNumber:{digits:true,maxlength:20},
                faxNumber:{digits:true,maxlength:20},
                website:{required:true,url:true}


           },
            messages: {
                Name:{ required: "<div> Please Enter Name of Development Partner.</div>"},

                Address:{ required: "<div> Please Enter Head Office Address.</div>"},

               Country:{required: "<div class='reqF_1'>Please select country.</div>"
                        },

               District:{required: "<div class='reqF_1'>Please select Dzongkhag / District Name.</div>"
                        },

               City:{required:"<div>Please Enter City/Town.</div>",
                        maxlength:"<div class='reqF_1'>Maximum 50 Characters are to be allowed.</div>"
                    },

               postCode:{number: "<div class='reqF_1'>Only numbers (0-9) are allowed.</div>",
                        minlength:"<div class='reqF_1'>Minimum 4 characters are required.</div>"
                        },

               phoneNumber:{digits:"<div>Please Enter Numbers only.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 20 characters are allowed.</div>"
                            },

               faxNumber:{digits:"<div>Please Enter Numbers only.</div>",
                            maxlength:"<div class='reqF_1'>Maximum 20 characters are allowed.</div>"
                      },
               website:{required:"<div>Please Enter Website.</div>",
                        url:"<div> Please enter URL in www.xyz.gov.bd  format.</div>"}
            }
        }
    );
        });

 </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="/resources/common/Top.jsp" ></jsp:include>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <div class="pageHead_1">Development Partner Organization</div>
                            <!--Page Content Start-->
                            <form id="frmDevelopmentPartnerOrganization" name="frmDevelopmentPartnerOrganization" method="POST">
                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10">
                                    <tr>
                                        <td class="ff" width="200">Name of Development Partner<span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtName" name="Name" style="width: 200px;" type="text"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Head Office Address<span>*</span></td>
                                        <td><textarea class="formTxtBox_1" id="taAddress" name="Address" style="width: 200px" ></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Country<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbCountry" name="Country" style="width: 200px">
                                                <option></option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">State<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbDistrict" name="District" style="width: 200px">
                                                <option></option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">City<span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtCity" name="City" style="width: 200px;" type="text" maxlength="50"/>
                                        </td>
                                    </tr

                                    <tr>
                                        <td class="ff" width="200">Post Code</td>
                                        <td><input class="formTxtBox_1" id="txtPostCode" name="postCode" style="width: 200px;" type="text" maxlength="10"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Phone Number</td>
                                        <td><input class="formTxtBox_1" id="txtPhoneNumber" style="width: 200px;" name="phoneNumber" type="text" maxlength="20"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Fax Number</td>
                                        <td><input class="formTxtBox_1" id="txtFaxNumber" style="width: 200px;" name="faxNumber" type="text" maxlength="20"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Website<span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtWebsite" style="width: 200px;" name="website" type="text" maxlength="20"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formBtn_1" align="center"><input id="btnSubmit" name="Submit" value="Submit" type="submit"/></td>
                                        <td class="formBtn_1" align="center"><input id="btnUpdate" name="Update" value="Update" type="submit"/></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
