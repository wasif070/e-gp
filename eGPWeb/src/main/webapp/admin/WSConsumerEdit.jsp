<%--
    Document   : GovtUserCreation
    Created on : Oct 23, 2010, 5:28:28 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.egp.eps.web.utility.WebServiceUtil"%>
<%@page import="com.cptu.egp.eps.model.table.TblWsOrgMaster"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.WsOrgMasterService"%>
<%@page import="com.cptu.egp.eps.model.table.TblWsMaster"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.WsMasterService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Web-Service Consumer Detail </title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#frmWSConsumer").validate({
                    rules: {
                        fullName:{required: true},
                        contactPerson: { required: true},
                        mobileNo: {required: true,number: true, minlength: 13, maxlength:15 },
                        bngName:{ maxlength:100},
                        cmbWebServiceList:{required: true}
                    },
                    messages: {                        
                        contactPerson: { required: "<div class='reqF_1'>Please Enter contact person</div>"
                        },                        
                        fullName: { required: "<div class='reqF_1'>Please enter Orgnization Name</div>",
                            spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                            requiredWithoutSpace: "<div class='reqF_1'>Please Enter Organization Details</div>",
                            alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only </div>",
                            maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"
                        },
                        cmbWebServiceList:{
                            required: "<div class='reqF_1'>Please select WebServices to access</div>"
                        },
                        mobileNo: {
                            required: "<div class='reqF_1'>Please enter Mobile Number</div>",
                            number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" ,
                            minlength:"<div class='reqF_1'>Minimum 13 digits are required</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 15 digits only</div>"
                        },          
                        bngName:{
                            maxlength:"<div class='reqF_1'>Allows maximum 100 characters only</div>"
                        }
                    },
                    errorPlacement:function(error ,element){
                        if(element.attr("name")=="mobileNo"){
                            error.insertAfter("#mobMsg");
                        }else{
                            error.insertAfter(element);
                        }
                    }
                }
            );
            });
        </script>

        <script type="text/javascript">

            function numeric(value) {
                return /^\d+$/.test(value);
            }           

            function checkData(){
                var retval;                
                if($('#frmWSConsumer').valid()){
                    document.getElementById("frmWSConsumer").action="/WebServiceConsumerControlServlet?action=edit";
                    document.getElementById("frmWSConsumer").submit();
                }
            }
        </script>
        <script type="text/javascript">
            function passMsg(){
                jAlert("Copy and Paste not allowed.","Web Service Consumer Editing", function(RetVal) {
                });
                return false;
            }
        </script>
        <%
                    int webServiceOrgId = Integer.parseInt(request.getParameter("wsOrgId"));
                    WsOrgMasterService wsOrgMasterService = (WsOrgMasterService) AppContext.getSpringBean("WsOrgMasterService");
                    TblWsOrgMaster wsOrgMaster = wsOrgMasterService.getTblWsOrgMaster(webServiceOrgId);
                    String wsRightsString = wsOrgMaster.getWsRights();
                    String[] wsRightsArray = WebServiceUtil.getStringArray(wsRightsString);
                    String statusString = "";
                    if ("N".equalsIgnoreCase(wsOrgMaster.getIsDeleted())) {
                        statusString = "Active";
                    } else {
                        statusString = "Deactivate";
                    }
                    boolean flag = false;
                    String msg = "";

                    if (request.getParameter("flag") != null) {
                        if ("true".equalsIgnoreCase(request.getParameter("flag"))) {
                            flag = true;
                        }
                    }
                    //getting webservices list
                    WsMasterService wsMasterService = (WsMasterService) AppContext.getSpringBean("WsMasterService");
                    List<TblWsMaster> webServiceList = new ArrayList<TblWsMaster>();
                    webServiceList = wsMasterService.getAllWsMaster();
                    String parentPage = request.getParameter("parentPage");
        %>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();
                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                            <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea_1">
                                        <div class="pageHead_1">
                                            Edit Web-Service Consumer Detail
                                            <span style="float: right; text-align: right;">
                                                <%
                                                            if (parentPage == null) {
                                                                out.print("<a class='action-button-goback' href='WSConsumersList.jsp?mode=view'>Go back</a>");
                                                            } else {
                                                                out.print("<a class='action-button-goback' href='WSConsumerView.jsp?wsOrgId=" + wsOrgMaster.getOrgId() + "&action=view'>Go back</a>");
                                                            }
                                                %>
                                            </span>
                                        </div>
                                        <form method="POST" id="frmWSConsumer" action="/WebServiceConsumerControlServlet?action=add" >
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" >
                                                <tr>
                                                    <td style="font-style: italic" colspan="2" class="ff" align="left">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                                </tr>
                                                <tr>
                                                    <td class="ff" width="20%" >e-Mail ID : </td>
                                                    <td width="80%">
                                                        <%=wsOrgMaster.getEmailId()%>
                                                        <input name="mailId" id="txtMail" type="hidden"  value=<%=wsOrgMaster.getEmailId()%> />
                                                        <%--
                                                        <input name="mailId" id="txtMail" type="text" class="formTxtBox_1"  style="width:200px;" onblur="return checkMail();" readonly value=<%=wsOrgMaster.getEmailId()%> />
                                                        --%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Organization Name : <span>*</span></td>
                                                    <td><input name="fullName" id="txtaFullName" type="text" class="formTxtBox_1" style="width:200px;" value="<%=wsOrgMaster.getOrgNameEn()%>" />
                                                        <span id="orgNameMsg" style="color: red; font-weight: bold">&nbsp;</span></td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Organization Name in Dongkha : </td>
                                                    <td><input name="bngName" id="txtBngName" type="text" class="formTxtBox_1"
                                                               style="width:200px;" maxlength="101" value=<%=wsOrgMaster.getOrgNameBg()%>  />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Organization Address/Detail : </td>
                                                    <td><textarea name="txtAreaOrgAdd" rows="3" class="formTxtBox_1" id="txtAreaOrgAdd" style="width:400px;"><%=wsOrgMaster.getOrgDetail()%></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Contact Person : <span>*</span></td>
                                                    <td><input name="contactPerson" id="contactPerson" type="text" class="formTxtBox_1" style="width:200px;" value=<%=wsOrgMaster.getContactPerson()%> />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Contact Person Mobile No : <span>*</span></td>
                                                    <td><input name="mobileNo" maxlength="16" id="txtMob" type="text" class="formTxtBox_1"
                                                               style="width:200px;"  value=<%=wsOrgMaster.getMobileNo()%> />
                                                        <span id="mNo" style="color: grey;"> (Mobile No. format e.g 12345678)</span>
                                                        <span id="mobMsg" style="color: red; font-weight: bold">&nbsp;</span></td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Assign web-service access : <span>*</span></td>
                                                    <td>
                                                        <select name="cmbWebServiceList" class="formTxtBox_1" id="cmbWebServiceList" multiple size="5">
                                                            <%  for (TblWsMaster tblWsMaster : webServiceList) {
                                                                            String wsIdString = "" + tblWsMaster.getWsId();
                                                                            String selected = "";
                                                                            for (int i = 0; i < wsRightsArray.length; i++) {
                                                                                if (wsIdString.equalsIgnoreCase(wsRightsArray[i])) {
                                                                                    selected = "selected";
                                                                                    break;
                                                                                } else {
                                                                                    selected = "";
                                                                                }
                                                                            }
                                                                            out.println("<option value=" + tblWsMaster.getWsId() + " " + selected + ">" + tblWsMaster.getWsName() + "</option>");
                                                                        }
                                                            %>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Status : </td>
                                                    <td class="t-align-left">
                                                        <select name="status" class="formTxtBox_1" id="cmbHintQue" style="width:auto;min-width: 200px;">
                                                            <%
                                                                        if ("N".equalsIgnoreCase(wsOrgMaster.getIsDeleted())) {
                                                                            out.print("<option value='N' selected >Active</option>");
                                                                            out.print("<option value='Y'>DeActivate</option>");
                                                                        } else {
                                                                            out.print("<option value='Y' selected >DeActivate</option>");
                                                                            out.print("<option value='N' >Active</option>");
                                                                        }
                                                            %>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="t-align-left">
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnSubmit" id="btnSubmit" value="Submit" onclick="return checkData();" />
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Dashboard Content Part End-->
                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
    </body>
</html>