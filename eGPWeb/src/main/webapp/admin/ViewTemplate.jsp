<%-- 
    Document   : CreateTemplate
    Created on : 24-Oct-2010, 1:03:35 AM
    Author     : yanki
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />
<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster, java.util.List, com.cptu.egp.eps.model.table.TblTemplateSections" %>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%
    short templateId = 0;
    String templateName = "";
    String action = "viewTemplate";
    String strToDisplay = "View";
    int noOfSection = 0;
    if(request.getParameter("templateId") != null){
        templateId = Short.parseShort(request.getParameter("templateId"));
    }
    if(request.getParameter("action") != null){
        action = request.getParameter("action");
    }
    if(action.equals("acc")){
        action = "acceptTemplate";
        strToDisplay = "Accept";
    }else if(action.equals("can")){
        action = "cancelTemplate";
        strToDisplay = "Cancel";
    }else if(action.equals("viw")){
        action = "viewTemplate";
        strToDisplay = "View";
    }
    String logUserId = "0";
    if(session.getAttribute("userId")!=null){
        logUserId =session.getAttribute("userId").toString();
    }
    defineSTDInDtlSrBean.setLogUserId(logUserId);
    List<TblTemplateMaster> tblTMaster = defineSTDInDtlSrBean.getTemplateMaster(templateId);
    if(tblTMaster != null){
        if(tblTMaster.size() > 0){
            templateName = tblTMaster.get(0).getTemplateName();
            noOfSection = tblTMaster.get(0).getNoOfSections();
        }
        tblTMaster = null;
    }
    List<TblTemplateSections> tblTemplateSections = defineSTDInDtlSrBean.getTemplateSection(templateId);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><%= strToDisplay %> SBD</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script>
            function showFormMsg(id, fName, tName, sName){
                var msgToShow = "";
                switch(parseInt(id)){
                    case -1:
                        //msgToShow = "STD is Incomplete Please complete it first";
                        msgToShow = "Tender Forms section is missing in SBD";
                        break;
                    case 0:
                        //msgToShow = "STD is Incomplete Please complete it first";
                        msgToShow = "Please Create Forms in <b>Section</b> : " + sName;
                        break;
                    case 1:
                        //msgToShow = "STD is Incomplete Please complete it first";
                        msgToShow = "Please Create Tables in <b>Form</b> : " + fName + " of <b>Section</b> : " + sName;
                        break;
                    case 2:
                        //msgToShow = "STD is Incomplete Please complete it first";
                        msgToShow = "Please Create Columns in <b>Table</b> : " + tName + " of <b>Form</b> : " + fName  + " of <b>Section</b> : " + sName;
                        break;
                    case 3:
                        //msgToShow = "STD is Incomplete Please complete it first";
                        msgToShow = "Please Create Rows in <b>Table</b> : " + tName + " of <b>Form</b> : " + fName + " of <b>Section</b> : " + sName;
                        break;
                    case 4:
                        //msgToShow = "STD is Incomplete Please complete it first";
                        msgToShow = "Please Complete Formulas in <b>Table</b> : " + tName + " of <b>Form</b> : " + fName + " of <b>Section</b> : " + sName;
                        break;
                }
                jAlert(msgToShow, "Complete Form", function(r){
                    if(r){
                        return true;
                    }else{
                        return false;
                    }
                });
            }

            function showConfirmMsg(){
                if(confirm("Do you want to cancel this SBD? SBD Once cancelled cannot be used again")){
                    return true;
                }else{
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1">
                                    <%= strToDisplay %> Standard Bidding Document
                                    <span style="float: right; text-align: right;">
                                        <a class="action-button-goback" href="ListSTD.jsp">Go Back</a>
                                    </span>
                                </div>
                            </div>
                                <div class="t_space">
                                <%
                                    if("accd".equals(action)){
                                %>
                                    <div class="responseMsg successMsg">SBD Accepted successfully</div>
                                <%
                                    }
                                    if("cand".equals(action)){
                                %>
                                    <div class="responseMsg successMsg">SBD Canceled successfully</div>
                                <%
                                    }
                                %>
                                </div>
                                <%
                                String reqURL = request.getRequestURL().toString();
                                String folderName = pdfConstant.STDPDF;
                                %>
                            <form id="frmCreateTemplate" action="<%=request.getContextPath()%>/StdGrid?action=<%= action %>&reqURL=<%=reqURL%>&folderName=<%=folderName%>" method="post">
                                <table width="100%" border="0" cellspacing="10" cellpadding="0" class="tableHead_1 t_space">
                                    <tr>
                                        <td class="ff">Name of SBD : <%= templateName %></td>
                                    </tr>
                                    <%--<tr>
                                        <td class="ff">No. of Section Required</td>
                                        <td><%= noOfSection %></td>
                                    </tr>--%>
                                </table>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                    <tr>
                                        <th>Section No.</th>
                                        <th>Name Of Section</th>
                                        <th>Content Type</th>
                                    </tr>
                            <%
                            if(tblTemplateSections != null){
                                if(tblTemplateSections.size() > 0){
                                    for(int i=0;i<tblTemplateSections.size();i++){
                                %>
                                    <tr>
                                        <td class="t-align-center"><%= (i+1) %></td>
                                        <td><%=tblTemplateSections.get(i).getSectionName() %></td>
                                        <td>
                                            <%
                                                if("ITT".equals(tblTemplateSections.get(i).getContentType()))
                                                {
                                                    out.print("ITB");
                                                }
                                                else if("TDS".equals(tblTemplateSections.get(i).getContentType()))
                                                {
                                                    out.print("BDS");
                                                }
                                                else if("Document By PE".equals(tblTemplateSections.get(i).getContentType()))
                                                {
                                                    out.print("Document By PA");
                                                }
                                                else
                                                {
                                                    out.print(tblTemplateSections.get(i).getContentType());
                                                }
                                            %>
                                        </td>
                                    </tr>
                                <%
                                    }
                                }
                                tblTemplateSections = null;
                            }
                            %>
                                </table>
                                <%
                                    if(action.equals("viewTemplate")){

                                    }else{
                                        if("accd".equals(action) || "cand".equals(action)){

                                        }else{
                                %>
                                <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                                    <tr>
                                        <td align="center">
                                            <label class="formBtn_1">
                                                <input type="hidden" name="templateId" value="<%= templateId %>" />
                                                <%
                                                    if(action.equals("acceptTemplate")){
                                                        SPTenderCommonData isTemplateFormOk = defineSTDInDtlSrBean.isTemplateFormOk(templateId);
                                                            if(Integer.parseInt(isTemplateFormOk.getFieldName1().trim()) == 5){
                                                %>
                                                    <input type="submit" name="btnSubmit" id="btnSubmit" value="Accept" />
                                                <%
                                                            }else{
                                                %>
                                                    <input type="button" name="btnSubmit" id="btnSubmit" value="Accept" onclick="showFormMsg('<%= isTemplateFormOk.getFieldName1().trim() %>', '<%= isTemplateFormOk.getFieldName2() %>', '<%= isTemplateFormOk.getFieldName3() %>', '<%= isTemplateFormOk.getFieldName4() %>');" />
                                                <%
                                                            }
                                                    }else{
                                                %>
                                                <input type="submit" name="btnSubmit" id="btnSubmit" value="Cancel" onclick="return showConfirmMsg();"/>
                                                <%
                                                    }
                                                %>
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                                <%
                                        }
                                    }
                                %>
                            </form>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<%
    if(defineSTDInDtlSrBean!=null){
        defineSTDInDtlSrBean = null;
    }
%>