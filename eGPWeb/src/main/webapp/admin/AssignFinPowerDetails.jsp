<%-- 
    Document   : AssignFinPowerDetails
    Created on : Jan 13, 2011, 5:17:40 PM
    Author     : Naresh.Akurathi
--%>


<%@page import="java.util.Map"%>
<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.List"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblFinancialPowerByRole"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementRole"%>
<%@page import="com.cptu.egp.eps.model.table.TblBudgetType"%>
<%@page import="com.cptu.egp.eps.model.table.TblFinancialYear"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Assign Financial Power Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>


        <%
                    StringBuilder finYear = new StringBuilder();
                    StringBuilder procNature = new StringBuilder();
                    StringBuilder procRole = new StringBuilder();
                    StringBuilder budgetType = new StringBuilder();
                    StringBuilder procMethod = new StringBuilder();

                    TblFinancialYear finYear2 = new TblFinancialYear();
                    Iterator fi2 = configPreTenderRuleSrBean.getFinancialYear().iterator();
                    Map m = new Hashtable();
                    StringBuffer sb2 = new StringBuffer();
                    while(fi2.hasNext()){
                            TblFinancialYear ty =(TblFinancialYear)fi2.next();
                            String fst = ty.getFinancialYear();
                            String st[]=fst.split("-");
                            if(!m.containsKey(st[2])){
                            m.put(st[2], fst);
                            sb2.append(st[2]+",");
                            }
                        }
                    if(sb2.length()>0)
                    sb2.deleteCharAt(sb2.length()-1);

                    String[] strevn = sb2.toString().split(",");
                    List l = Arrays.asList(strevn);
                    Collections.sort(l);


                    long size = configPreTenderRuleSrBean.getFinancialYearCount();
                    int s = 0;
                    while (s<l.size()) {
                        Iterator fit2 = configPreTenderRuleSrBean.getFinancialYear().iterator();
                        while (fit2.hasNext()) {
                            finYear2 = (TblFinancialYear) fit2.next();
                            String yearfirst = (String) l.get(s);
                            String[] yrstr = finYear2.getFinancialYear().split("-");
                            if(yearfirst.equals(yrstr[2]))
                            finYear.append("<option value='" + finYear2.getFinancialYearId() + "'>" + finYear2.getFinancialYear() + "</option>");
                        }
                        s++;
                    }

                    TblBudgetType budgetType2 = new TblBudgetType();
                    Iterator bit2 = configPreTenderRuleSrBean.getBudgetType().iterator();
                    while (bit2.hasNext()) {
                        budgetType2 = (TblBudgetType) bit2.next();
                        budgetType.append("<option value='" + budgetType2.getBudgetTypeId() + "'>" + budgetType2.getBudgetType() + "</option>");
                    }

                    TblProcurementRole tblProcurementRole2 = new TblProcurementRole();
                    Iterator prit2 = configPreTenderRuleSrBean.getProcureRole().iterator();
                    while (prit2.hasNext()) {
                        tblProcurementRole2 = (TblProcurementRole) prit2.next();
                        procRole.append("<option value='" + tblProcurementRole2.getProcurementRoleId() + "'>" + tblProcurementRole2.getProcurementRole() + "</option>");
                    }

                    TblProcurementNature tblProcureNature2 = new TblProcurementNature();
                    Iterator pnit2 = configPreTenderRuleSrBean.getProcurementNature().iterator();
                    while (pnit2.hasNext()) {
                        tblProcureNature2 = (TblProcurementNature) pnit2.next();
                        procNature.append("<option value='" + tblProcureNature2.getProcurementNatureId() + "'>" + tblProcureNature2.getProcurementNature() + "</option>");
                    }

                    TblProcurementMethod tblProcurementMethod2 = new TblProcurementMethod();
                    Iterator pmit2 = configPreTenderRuleSrBean.getProcurementMethod().iterator();
                    while (pmit2.hasNext()) {
                        tblProcurementMethod2 = (TblProcurementMethod) pmit2.next();
                        procMethod.append("<option value='" + tblProcurementMethod2.getProcurementMethodId() + "'>" + tblProcurementMethod2.getProcurementMethod() + "</option>");
                    }

        %>


        <script type="text/javascript">

            var totCnt = 1;

            $(function() {
                $('#linkAddRule').click(function() {
                    var count=($("#tblrule tr:last-child").attr("id").split("_")[1]*1);

                    var htmlEle = "<tr id='trrule_"+ (count+1) +"'>"+
                        "<td class='t-align-center'><input type='checkbox' name='checkbox"+(count+1)+"' id='checkbox_"+(count+1)+"' value='"+(count+1)+"' /></td>"+
                        "<td class='t-align-center'><select name='financialYear"+(count+1)+"' class='formTxtBox_1' id='financialYear_"+(count+1)+"'><%=finYear%></select></td>"+
                        "<td class='t-align-center'><select name='budgetType"+(count+1)+"' class='formTxtBox_1' id='budgetType_"+(count+1)+"'><%=budgetType%></select></td>"+
                        "<td class='t-align-center'><select name='procRole"+(count+1)+"' class='formTxtBox_1' id='procRole_"+(count+1)+"'><%=procRole%></select></td>"+
                        "<td class='t-align-center'><select name='procNature"+(count+1)+"' class='formTxtBox_1' id='procNature_"+(count+1)+"'><%=procNature%></select></td>"+
                        "<td class='t-align-center'><select name='procMethod"+(count+1)+"' class='formTxtBox_1' id='procMethod_"+(count+1)+"'><%=procMethod%></select></td>"+
                        "<td  class='t-align-center'><select name='operator"+(count+1)+"' class='formTxtBox_1' id='operator_"+(count+1)+"'><option><</option><option>></option><option><=</option><option>>=</option><option>=</option></td>"+
                        "<td class='t-align-center'><input name='amount"+(count+1)+"' type='text' class='formTxtBox_1' id='amount_"+(count+1)+"' /></td>"+
                        "</tr>";

                    totCnt++;
                    $("#tblrule").append(htmlEle);
                    document.getElementById("TotRule").value = (count+1);

                });
            });


            $(function() {
                $('#linkDelRule').click(function() {
                    counter = eval(document.getElementById("TotRule").value);
                    delCnt = eval(document.getElementById("delrow").value);

                    var tmpCnt = 0;
                    for(var i=1;i<=counter;i++){
                        if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                            tmpCnt++;
                        }
                    }

                    if(tmpCnt==(eval(counter-delCnt))){
                        $('span.#lotMsg').css("visibility","visible");
                        $('span.#lotMsg').css("color","red");
                        $('span.#lotMsg').html('Minimum 1 record is needed!');
                    }else{
                        for(var i=1;i<=counter;i++){
                            if(document.getElementById("checkbox_"+i) != null && document.getElementById("checkbox_"+i).checked){
                                $("tr[id='trrule_"+i+"']").remove();
                                $('span.#lotMsg').css("visibility","collapse");
                                delCnt++;
                            }
                        }
                        document.getElementById("delrow").value = delCnt;
                    }

                });
            });
        </script>

    </head>
    <body>

        <%!        ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();

        %>

        <%
                    String msg = "";
                    if ("Submit".equals(request.getParameter("button"))) {

                        int row = Integer.parseInt(request.getParameter("TotRule"));

                        if("edit".equals(request.getParameter("action")))
                            msg = "Not Deleted";
                        else
                            msg = configPreTenderRuleSrBean.delAllFinancialPowerByRole();

                        if (msg.equals("Deleted")) {
                            for (int i = 1; i <= row; i++) {

                                if (request.getParameter("financialYear" + i) != null) {
                                    int finalYear = Integer.parseInt(request.getParameter("financialYear" + i));
                                    byte btype = Byte.parseByte(request.getParameter("budgetType" + i));
                                    byte prole = Byte.parseByte(request.getParameter("procRole" + i));
                                    byte pnature = Byte.parseByte(request.getParameter("procNature" + i));
                                    byte pmethod = Byte.parseByte(request.getParameter("procMethod" + i));
                                    String operator = request.getParameter("operator"+i);
                                    float amount = Float.parseFloat(request.getParameter("amount"+i));

                                    TblFinancialPowerByRole tblFinancialPowerByRole = new TblFinancialPowerByRole();
                                    tblFinancialPowerByRole.setTblFinancialYear(new TblFinancialYear(finalYear));
                                    tblFinancialPowerByRole.setTblBudgetType(new TblBudgetType(btype));
                                    tblFinancialPowerByRole.setTblProcurementRole(new TblProcurementRole(prole));
                                    tblFinancialPowerByRole.setTblProcurementNature(new TblProcurementNature(pnature));
                                    tblFinancialPowerByRole.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                    tblFinancialPowerByRole.setOperator(operator);
                                    tblFinancialPowerByRole.setFpAmount(new BigDecimal(amount));

                                    msg = configPreTenderRuleSrBean.addTblFinancialPowerByRole(tblFinancialPowerByRole);                                    

                                }
                            }
                       }else{

                            for (int i = 1; i <= row; i++) {
                                if (request.getParameter("financialYear" + i) != null) {
                                    int finalYear = Integer.parseInt(request.getParameter("financialYear" + i));
                                    byte btype = Byte.parseByte(request.getParameter("budgetType" + i));
                                    byte prole = Byte.parseByte(request.getParameter("procRole" + i));
                                    byte pnature = Byte.parseByte(request.getParameter("procNature" + i));
                                    byte pmethod = Byte.parseByte(request.getParameter("procMethod" + i));
                                    String operator = request.getParameter("operator"+i);
                                    float amount = Float.parseFloat(request.getParameter("amount"+i));

                                    TblFinancialPowerByRole tblFinancialPowerByRole = new TblFinancialPowerByRole();
                                    tblFinancialPowerByRole.setFinancialPowerByRoleId(Integer.parseInt(request.getParameter("id")));
                                    tblFinancialPowerByRole.setTblFinancialYear(new TblFinancialYear(finalYear));
                                    tblFinancialPowerByRole.setTblBudgetType(new TblBudgetType(btype));
                                    tblFinancialPowerByRole.setTblProcurementRole(new TblProcurementRole(prole));
                                    tblFinancialPowerByRole.setTblProcurementNature(new TblProcurementNature(pnature));
                                    tblFinancialPowerByRole.setTblProcurementMethod(new TblProcurementMethod(pmethod));
                                    tblFinancialPowerByRole.setOperator(operator);
                                    tblFinancialPowerByRole.setFpAmount(new BigDecimal(amount));

                                    msg = configPreTenderRuleSrBean.updateFinancialPowerByRole(tblFinancialPowerByRole);                                    

                                }
                             }

                            }

                        

                        if (msg.equals("Values Added")) {
                            msg = "Assign Financial Power Configured Successfully";
                            response.sendRedirect("AssignFinPowerView.jsp?msg=" + msg);
                        }
                        if (msg.equals("Updated")) {
                            msg = "Assign Financial Power Updated Successfully";
                            response.sendRedirect("AssignFinPowerView.jsp?msg=" + msg);
                        }

                    }

        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%--<jsp:include page="../resources/common/AfterLoginTop.jsp" ></jsp:include>--%>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 250px; display: block;">
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                            </tr>
                        </table>
                    </td>
                    <td class="contentArea">
                        <!--Dashboard Content Part Start-->
                        <div class="pageHead_1">Assign Financial Power Details</div>

                        <div style="font-style: italic" class="t-align-left t_space"><strong>Fields marked with (<span class="mandatory">*</span>) are mandatory</strong></div>
                        <div align="right" class="t_space b_space">
                            <span id="lotMsg" style="color: red; font-weight: bold; visibility: collapse; float: left;">&nbsp;</span>
                            <a id="linkAddRule" class="action-button-add">Add Rule</a> <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a>

                        </div>
                        <div>&nbsp;</div>
                        <div class="overFlowContainer t-align-right">
                            <form action="AssignFinPowerDetails.jsp" method="post">

                                <table width="100%" cellspacing="0" class="tableList_1" id="tblrule" name="tblrule">
                                    <tr  >
                                        <th >Select</th>
                                        <th >Financial Year<br />(<span class="mandatory">*</span>)</th>
                                        <th >Budget Type<br />(<span class="mandatory">*</span>)</th>
                                        <th >Procurement Role<br />(<span class="mandatory">*</span>)</th>
                                        <th >Procurement Category<br />(<span class="mandatory">*</span>)</th>
                                        <th >Procurement Method<br />(<span class="mandatory">*</span>)</th>
                                        <th >Operator<br />(<span class="mandatory">*</span>)</th>
                                        <th >Amount<br />(<span class="mandatory">*</span>)</th>
                                     </tr>

                                    <%

                                                List lt = null;

                                            if(request.getParameter("id")!=null && "edit".equals(request.getParameter("action"))){
                                                String action = request.getParameter("action");
                                                int id = Integer.parseInt(request.getParameter("id"));
                                                %>
                                                <input type="hidden" name="action" value="<%=action%>"/>
                                                <input type="hidden" name="id" value="<%=id%>"/>
                                                <%
                                                lt = configPreTenderRuleSrBean.getFinancialPowerByRole(id);
                                                }else
                                                    lt = configPreTenderRuleSrBean.getAllFinancialPowerByRole();

                                                
                                                int i = 0;
                                                if (!lt.isEmpty() || lt!= null) {
                                                    Iterator it = lt.iterator();
                                                    TblFinancialPowerByRole tblFinPower = new TblFinancialPowerByRole();
                                                    while (it.hasNext()) {
                                                        tblFinPower = (TblFinancialPowerByRole) it.next();
                                                        i++;
                                                        StringBuilder fYear = new StringBuilder();
                                                        StringBuilder bType = new StringBuilder();
                                                        StringBuilder pRole = new StringBuilder();
                                                        StringBuilder pNature = new StringBuilder();
                                                        StringBuilder pMethod = new StringBuilder();
                                                        StringBuilder operator = new StringBuilder();
                                                        StringBuilder amount = new StringBuilder();

                                                        List getFinalYear = configPreTenderRuleSrBean.getFinancialYear();
                                                        List getBudgetType = configPreTenderRuleSrBean.getBudgetType();
                                                        List getProcRole = configPreTenderRuleSrBean.getProcureRole();
                                                        List getProcNature = configPreTenderRuleSrBean.getProcurementNature();
                                                        List getProcMethod = configPreTenderRuleSrBean.getProcurementMethod();

                                                     fi2 = configPreTenderRuleSrBean.getFinancialYear().iterator();

                                                     m = new Hashtable();
                                                     StringBuffer sb3 = new StringBuffer();
                                                     while(fi2.hasNext()){
                                                            TblFinancialYear ty =(TblFinancialYear)fi2.next();
                                                            String fst = ty.getFinancialYear();
                                                            String st[]=fst.split("-");
                                                            if(!m.containsKey(st[2])){
                                                            m.put(st[2], fst);
                                                            sb3.append(st[2]+",");
                                                            }
                                                        }
                                                    if(sb3.length()>0)
                                                    sb3.deleteCharAt(sb3.length()-1);

                                                    String[] stryer = sb3.toString().split(",");
                                                    List l2 = Arrays.asList(stryer);
                                                    Collections.sort(l2);                                                    
                                                    TblFinancialYear tblFinancialYear ;
                                                       
                                                     int s1 = 0;
                                                      while (s1<l2.size()) {
                                                            Iterator fit = getFinalYear.iterator();
                                                            while (fit.hasNext()) {
                                                                tblFinancialYear = (TblFinancialYear) fit.next();
                                                                String yearfirst = (String) l2.get(s1);
                                                                String[] yrstr = tblFinancialYear.getFinancialYear().split("-");
                                                                if(yearfirst.equals(yrstr[2])){
                                                                    fYear.append("<option value='");
                                                                    if (tblFinancialYear.getFinancialYearId() == tblFinPower.getTblFinancialYear().getFinancialYearId()) {
                                                                        fYear.append(tblFinancialYear.getFinancialYearId() + "' selected='true'>" + tblFinancialYear.getFinancialYear());
                                                                    } else {
                                                                        fYear.append(tblFinancialYear.getFinancialYearId() + "'>" + tblFinancialYear.getFinancialYear());
                                                                    }
                                                                    fYear.append("</option>");
                                                                }

                                                        }
                                                        s1++;
                                                        }

                                                        TblBudgetType tblBudgetType = new TblBudgetType();
                                                        Iterator bit = getBudgetType.iterator();
                                                        while (bit.hasNext()) {
                                                            tblBudgetType = (TblBudgetType) bit.next();
                                                            bType.append("<option value='");
                                                            if (tblBudgetType.getBudgetTypeId() == tblFinPower.getTblBudgetType().getBudgetTypeId()) {
                                                                bType.append(tblBudgetType.getBudgetTypeId() + "' selected='true'>" + tblBudgetType.getBudgetType());
                                                            } else {
                                                                bType.append(tblBudgetType.getBudgetTypeId() + "'>" + tblBudgetType.getBudgetType());
                                                            }
                                                            bType.append("</option>");
                                                        }

                                                        TblProcurementRole tblProcureRole = new TblProcurementRole();
                                                        Iterator prit = getProcRole.iterator();
                                                        while (prit.hasNext()) {
                                                            tblProcureRole = (TblProcurementRole) prit.next();
                                                            pRole.append("<option value='");
                                                            if (tblProcureRole.getProcurementRoleId() == tblFinPower.getTblProcurementRole().getProcurementRoleId()) {
                                                                pRole.append(tblProcureRole.getProcurementRoleId() + "' selected='true'>" + tblProcureRole.getProcurementRole());
                                                            } else {
                                                                pRole.append(tblProcureRole.getProcurementRoleId() + "'>" + tblProcureRole.getProcurementRole());
                                                            }
                                                            pRole.append("</option>");
                                                        }
                                                       
                                                        TblProcurementNature tblProcureNature = new TblProcurementNature();
                                                        Iterator pnit = getProcNature.iterator();
                                                        while (pnit.hasNext()) {
                                                            tblProcureNature = (TblProcurementNature) pnit.next();
                                                            pNature.append("<option value='");
                                                            if (tblProcureNature.getProcurementNatureId() == tblFinPower.getTblProcurementNature().getProcurementNatureId()) {
                                                                pNature.append(tblProcureNature.getProcurementNatureId() + "' selected='true'>" + tblProcureNature.getProcurementNature());
                                                            } else {
                                                                pNature.append(tblProcureNature.getProcurementNatureId() + "'>" + tblProcureNature.getProcurementNature());
                                                            }
                                                            pNature.append("</option>");
                                                        }

                                                        TblProcurementMethod tblProcurementMethod = new TblProcurementMethod();
                                                        Iterator pmit = getProcMethod.iterator();
                                                        while (pmit.hasNext()) {
                                                            tblProcurementMethod = (TblProcurementMethod) pmit.next();
                                                            pMethod.append("<option value='");
                                                            if (tblProcurementMethod.getProcurementMethodId() == tblFinPower.getTblProcurementMethod().getProcurementMethodId()) {
                                                                pMethod.append(tblProcurementMethod.getProcurementMethodId() + "' selected='true'>" + tblProcurementMethod.getProcurementMethod());
                                                            } else {
                                                                pMethod.append(tblProcurementMethod.getProcurementMethodId() + "'>" + tblProcurementMethod.getProcurementMethod());
                                                            }
                                                            pMethod.append("</option>");
                                                        }


                                                    String[] opt = {"<", ">", "<=", ">=", "="};
                                                    int a = opt.length;
                                                    for (int j = 0; j < a; j++) {
                                                        operator.append("<option ");
                                                        if (opt[j].equals(tblFinPower.getOperator())) {
                                                            operator.append(" selected='true'>" + opt[j]);
                                                        } else {
                                                            operator.append(">" + opt[j]);
                                                        }
                                                        operator.append("</option>");

                                                    }

                                                       


                                    %>
                                    <input type="hidden" name="delrow" value="0" id="delrow"/>
                                    
                                    <input type="hidden" name="introw" value="" id="introw"/>
                                    <tr id="trrule_<%=i%>">
                                        <td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_<%=i%>" value="1" /></td>

                                        <td class="t-align-center"><select name="financialYear<%=i%>" class="formTxtBox_1" id="financialYear_<%=i%>">
                                                <%=fYear%></select>
                                        </td>
                                        <td class="t-align-center"><select name="budgetType<%=i%>" class="formTxtBox_1" id="budgetType_<%=i%>">
                                                <%=bType%></select>
                                        </td>
                                        <td class="t-align-center"><select name="procRole<%=i%>" class="formTxtBox_1" id="procRole_<%=i%>">
                                                <%=pRole%></select>
                                        </td>
                                        <td class="t-align-center"><select name="procNature<%=i%>" class="formTxtBox_1" id="procNature_<%=i%>">
                                                <%=pNature%></select>
                                        </td>
                                        <td class="t-align-center"><select name="procMethod<%=i%>" class="formTxtBox_1" id="procMethod_<%=i%>">
                                                <%=pMethod%></select>
                                        </td>
                                        <td class="t-align-center"><select name="operator<%=i%>" class="formTxtBox_1" id="operator_<%=i%>">
                                                <%=operator%></select>
                                        </td>
                                        <td class="t-align-center"><input type="text" name="amount<%=i%>" class="formTxtBox_1" id="amount_<%=i%>" value="<%=tblFinPower.getFpAmount()%>"/></td>

                                    </tr>
                                        <%

                                                    }
                                                } else {
                                                    msg = "No Record Found";
                                                }

                                    %>
                                </table>
                                <input type="hidden" name="TotRule" id="TotRule" value="<%=i%>"/>
                                <div>&nbsp;</div>
                                <table width="100%" cellspacing="0" >
                                    <tr>
                                        <td align="right" colspan="9" class="t-align-center">
                                            <span class="formBtn_1"><input type="submit" name="button" id="button" value="Submit" onclick="return validate();"/></span>
                                        </td>
                                    </tr>
                                </table>
                            </form>

                        </div>

                        <!--Dashboard Content Part Start-->

                    </td>
                </tr>
            </table>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>

        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>


