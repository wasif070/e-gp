<%-- 
    Document   : PrepareTDS
    Created on : 24-Oct-2010, 3:13:11 PM
    Author     : yanki
--%>

<jsp:useBean id="sectionClauseSrBean" class="com.cptu.egp.eps.web.servicebean.SectionClauseSrBean" />
<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBean" />
<jsp:useBean id="prepareTDSSrBean" class="com.cptu.egp.eps.web.servicebean.PrepareTDSSrBean" />
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />

<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster" %>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.model.table.TblIttClause" %>
<%@page import="com.cptu.egp.eps.model.table.TblIttSubClause" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <%
                if (session.getAttribute("userId") != null) {
                    sectionClauseSrBean.setLogUserId(session.getAttribute("userId").toString());
                    createSubSectionSrBean.setLogUserId(session.getAttribute("userId").toString());
                    prepareTDSSrBean.setLogUserId(session.getAttribute("userId").toString());
                    defineSTDInDtlSrBean.setLogUserId(session.getAttribute("userId").toString());
                }
        int templateId = 0;
                if (request.getParameter("templateId") != null) {
            templateId = Integer.parseInt(request.getParameter("templateId"));
        }
        String procType = "";
        List<TblTemplateMaster> templateMasterLst = defineSTDInDtlSrBean.getTemplateMaster((short) templateId);
                if (templateMasterLst != null) {
                    if (templateMasterLst.size() > 0) {
                procType = templateMasterLst.get(0).getProcType();
            }
            templateMasterLst = null;
        }

        int ittHeaderId = Integer.parseInt(request.getParameter("ittHeaderId"));
        int sectionId = Integer.parseInt(request.getParameter("sectionId"));
        String contentType = createSubSectionSrBean.getContectType(sectionId);
        String contentType1 = "";
        String instruction = "";
        String clauseInstr = "";

                if ("srvcmp".equals(procType)) {
                    if ("ITT".equals(contentType)) {
                contentType = contentType.replace("ITT", "ITC");
                    } else if ("TDS".equals(contentType)) {
                contentType = contentType.replace("TDS", "TDS/PDS");
            }
                } else if ("srvindi".equals(procType)) {
                    if ("ITT".equals(contentType)) {
                contentType = contentType.replace("ITT", "ITA");
            }
        }

                if (contentType.equalsIgnoreCase("ITT")) {
            contentType1 = "TDS";
            instruction = "Instructions for completing Bid Data Sheet are provided in italics in parenthesis for the relevant ITB clauses";
            clauseInstr = "Amendments of, and Supplements to, Clauses in the Instructions to Bidder /Consultants";
                } else if (contentType.equalsIgnoreCase("ITC")) {
            contentType1 = "PDS";
            instruction = "Instructions for completing Proposal Data Sheet are provided in italics in parenthesis for the relevant ITC clauses";
            clauseInstr = "Amendments of, and Supplements to, Clauses in the Instructions to Bidders/Consultants";
                } else if (contentType.equalsIgnoreCase("GCC")) {
            contentType1 = "PCC";
            instruction = "Instructions for completing the Particular Conditions of Contract are provided in italics in parenthesis for the relevant GCC Clauses.";
            clauseInstr = "Amendments of, and Supplements to, Clauses in the General Conditions of Contract";
        }

        List<SPTenderCommonData> tblTdsSubClause = null;
        boolean edit = false;
                if (request.getParameter("edit") != null) {
                    if (request.getParameter("edit").equalsIgnoreCase("true")) {
                edit = true;
                tblTdsSubClause = prepareTDSSrBean.getTDSSubClause(ittHeaderId);
            }
        }
    %>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create <%if(contentType1.equals("TDS")){out.print("BDS");}else if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%></title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.blockUI.js"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="../ckeditor/adapters/jquery.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>

         <style type="text/css">
            ul li {margin-left: 20px;}
        </style>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <style type="text/css">

            div.scroll {
                height: 50px;
                width: 950px;
                overflow: auto;
                border: 1px solid #666;
                background-color: white;
                padding: 8px;
                border-color: #A8DA7F;
            }

        </style>
        <script>
            
            $(document).ready(function(){
                $("#addClause").bind("click", function(){
                    var lastTrId = "";
                    $('tr[id^="tr_"]').each(function(){
                        lastTrId = $(this).attr("id");
                    });

                    var total = ($("#total").val()*1) + 1;
                    var name = "tds_"+total;
                    var add = "";
                    <%
                                if (edit) {
                        %>
                                    add = "<a class=\"action-button-add\" id=\"addClause_" + total + "\" href=\"javascript:;\" onclick=\"saveTDS('" + total + "');\" >Save <%if(contentType1.equals("TDS")){out.print("BDS");}else{out.print(contentType1);}%> Clause</a>";
                        <%
                    }
                    %>
                    
                    var content = "<tr id=\"tr_" + lastTrId.split("_")[1] + "\"><td colspan=\"2\"></td></tr>" +
                            "<tr  id=\"tr_" + ((lastTrId.split("_")[1]*1)+1) + "\"><td colspan='2'><textarea cols='80' id='" + name + "' name='" + name +
                            "' rows='10'></textarea><input type = 'hidden' name='hd_" + name + "' id = 'hd_" + name +
                            "' value='<%=ittHeaderId%>' />" + add + "</td></tr>";
                    $("#tdsInfo").append(content);
                    $("#total").val(($("#total").val()*1)+1);
                    //CKEDITOR.replace(name);
                    //$("#"+name).ckeditor(config);
                    CKEDITOR.replace( $("#"+name).attr("id"),
                    {
                        toolbar : 'egpToolbar'
                    });
                });
                $('textarea[name^="tds_"]').each(function(){
                    //$("#"+$(this).attr("id")).ckeditor(config);
                    CKEDITOR.replace($(this).attr("id"),
                    {
                        toolbar : 'egpToolbar'
                    });
                });
            });

            function validate(){
                var ele = true;
                $(".error").remove();
                $('textarea[name^="tds_"]').each(function(){
                    var counter = $(this).attr("id");
                    
                    if(CKEDITOR.instances[counter].getData().length == 0){
                        $(this).parent().append("<span class='error' style='color:red'>Please enter <%if(contentType1.equals("TDS")){out.print("BDS");}else{out.print(contentType1);}%> Details</span> ");
                        ele = false;
                    }
                });
                return ele;
            }
            function updateTDS(counter){
                 $(".error").remove();
                var ele = $("#hd_tds_"+counter).val().split("_");
                if(CKEDITOR.instances["tds_"+counter].getData() == ""){
                    $("#tds_"+counter).parent().append("<span class='error' style='color:red'>Please enter <%if(contentType1.equals("TDS")){out.print("BDS");}else{out.print(contentType1);}%> Details</span> ");
                    return false;
                }
                
                if(ele[3] == "null"){
                    saveTDS_New(counter);
                    return false;
                }

                $.ajax({
                    url: "<%=request.getContextPath()%>/PrepareTDSSrBean",
                    data: {"action": "update",
                            "headerId": ele[0],
                            "tdsSubClauseId": ele[3],
                            "tdsInfo": CKEDITOR.instances["tds_"+counter].getData()
                        },
                    method: "POST",
                    context: document.body,
                    success: function(response){
                        if(response == "OK"){
                            jAlert("<%if(contentType1.equals("TDS")){out.print("BDS");}else{out.print(contentType1);}%> Information updated successfully", "success");
                        } else {
                            jAlert("ERROR occured while updating <%if(contentType1.equals("TDS")){out.print("BDS");}else{out.print(contentType1);}%> record:"+response, "ERROR");
                        }
                    }, error: function (msg1,msg2, msg3){
                        alert("msg1" + msg1.responseText);
                    }
                });
            }
            function deleteTDS(counter){
                jConfirm("Do you really want to remove this <%if(contentType1.equals("TDS")){out.print("BDS");}else{out.print(contentType1);}%> Clause", "Confirm", function(result){
                    if(result){
                        var ele = $("#hd_tds_"+counter).val().split("_");
                        $.ajax({
                            url: "<%=request.getContextPath()%>/PrepareTDSSrBean",
                            data: {"action": "delete",
                                    "headerId": ele[0],
                                    "tdsSubClauseId": ele[3]
                                },
                            method: "POST",
                            context: document.body,
                            success: function(response){
                                if(response == "OK"){
                                    jAlert("<%if(contentType1.equals("TDS")){out.print("BDS");}else{out.print(contentType1);}%> Information removed successfully", "success", function(){
                                        $.blockUI({ message: '<h1><img src="<%=request.getContextPath()%>/resources/images/loading.gif"/> Just a moment...</h1>' });
                                        window.location.reload();
                                    });
                                    /*$("#hd_tds_"+counter).parent().parent().remove();
                                    $("#hd_tds_"+counter).parent().parent().prev().remove();
                                    $("#total").val(($("#total").val()*1)-1);*/
                                } else {
                                    jAlert("ERROR occured while removing record", "ERROR");
                                }
                            },
                            error: function (msg1,msg2, msg3){
                                alert("msg1" + msg1.responseText);
                            }
                        });
                    }
                });
            }

            function saveTDS_New(counter){
                //headerId_clauseid_ittsubclauseid_tdsSubClauseId
                //hd_tds_
                var ele = $("#hd_tds_"+counter).val();
                $.ajax({
                    url: "<%=request.getContextPath()%>/PrepareTDSSrBean",
                    data: {"action": "addFirst",
                            "headerId": '<%=request.getParameter("ittHeaderId")%>',
                            "tdsInfo": CKEDITOR.instances["tds_"+counter].getData(),
                            "orderNo": counter,
                            "clauseId": ele.split("_")[1],
                            "ittsubclauseid": ele.split("_")[2]
                        },
                    method: "POST",
                    context: document.body,
                    success: function(response){
                        if(response.split(",")[0] == "OK"){
                            jAlert("<%if(contentType1.equals("TDS")){out.print("BDS");}else{out.print(contentType1);}%> Information saved successfully", "success",function(){
                                $.blockUI({ message: '<h1><img src="<%=request.getContextPath()%>/resources/images/loading.gif"/> Just a moment...</h1>' });
                                window.location.reload();
                            });
                        } else {
                            jAlert("Error occured while saving <%if(contentType1.equals("TDS")){out.print("BDS");}else{out.print(contentType1);}%> Information:" + response, "Error");
                        }
                    },
                    error: function (msg1,msg2, msg3){
                        alert("msg1" + msg1.responseText);
                    }
                });
            }

            function saveTDS(counter){
                $.ajax({
                    url: "<%=request.getContextPath()%>/PrepareTDSSrBean",
                    data: {"action": "add",
                            "headerId": '<%=request.getParameter("ittHeaderId")%>',
                            "tdsInfo": CKEDITOR.instances["tds_"+counter].getData(),
                            "orderNo": $("#hd_orderNo_"+(($("#total").val()*1)-1)).val()
                        },
                    method: "POST",
                    context: document.body,
                    success: function(response){
                        if(response.split(",")[0] == "OK"){
                            jAlert("<%if(contentType1.equals("TDS")){out.print("BDS");}else{out.print(contentType1);}%> Information saved successfully", "success",function(){
                                $.blockUI({ message: '<h1><img src="<%=request.getContextPath()%>/resources/images/loading.gif"/> Just a moment...</h1>' });
                                window.location.reload();
                            });
                            //$("#total").val(($("#total").val()*1)+1);
                            /*var ele = "<input type=\"hidden\" name=\"hd_tds_" + $("#total").val() +
                                "\" id=\"hd_tds_" + $("#total").val() + "\" value=\"<%=request.getParameter("ittHeaderId")%>___"+response.split(",")[0]+"\" />" +
                                "<input type=\"hidden\" name=\"hd_orderNo_" + $("#total").val() + 
                                "\" id=\"hd_orderNo_" + $("#total").val() + "\" value=\"" + $("#total").val() + "\" />" +
                                "<a class=\"action-button-edit\" href=\"javascript:void(0);\" onclick=\"updateTDS('" + $("#total").val() + "');\" >Update TDS Details</a>" +
                                "<a class=\"action-button-delete\" href=\"javascript:void(0);\" onclick=\"deleteTDS('" + $("#total").val() + "');\">Delete TDS Details</a>";
                            $("#addClause_"+counter).remove();
                            $("#hd_tds_"+counter).parent().append(ele);*/
                        } else {
                            jAlert("Error occured while saving <%if(contentType1.equals("TDS")){out.print("BDS");}else{out.print(contentType1);}%> Information:" + response, "Error");
                        }
                    },
                    error: function (msg1,msg2, msg3){
                        alert("msg1" + msg1.responseText);
                    }
                });
            }
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <div class="t_space">
                                <div class="pageHead_1">Prepare <%if(contentType1.equals("TDS")){out.print("BDS");}else if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%></div>
                            </div>
                             <table width="100%" cellspacing="10"  class="tableView_1" >
                                <tr>
                                    <td  align="left">
                                    <a class="action-button-goback" href="DefineSTDInDtl.jsp?templateId=<%=request.getParameter("templateId")%>">
                                            Go back to SBD Dashboard</a></td>
                                    <td align="right">
                                        <a class="action-button-goback" href="TDSDashBoard.jsp?templateId=<%=request.getParameter("templateId")%>&sectionId=<%=request.getParameter("sectionId")%>">
                                            Go back to <%if(contentType1.equals("TDS")){out.print("BDS");}else if(contentType1.equals("PCC")){out.print("SCC");}else{out.print(contentType1);}%> Dashboard</a></td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellspacing="10" class="tableView_1">
                                <tr>
                                    <td align="left">
                                        <img src="../resources/images/Dashboard/addIcn.png" width="16" height="16" class="linkIcon_1" />
                                        <a id="addClause" href="javascript:void(0);">Add New Clause</a> &nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <form action="<%=request.getContextPath()%>/PrepareTDSSrBean?action=save" method="post" onsubmit="return validate();">
                                <input type="hidden" name="sectionId" id="sectionId" value="<%=request.getParameter("sectionId")%>" />
                                <input type="hidden" name="templateId" id="templateId" value="<%=request.getParameter("templateId")%>" />

                                    <%
                                            if (!edit) {
                                        %>
                                    <table width="100%" cellspacing="0" id="tdsInfo" class="tableList_1 t_space">
                                        <tr>
                                            <td colspan="2"><%=instruction%></td>
                                        </tr>
                                        <tr>
                                            <td><b><%if(contentType.equals("ITT")){out.print("ITB");}else{out.print(contentType);}%> Clause</b></td>
                                            <td><%=clauseInstr%></td>
                                        </tr>
                                        <%
                                        List<TblIttClause> ittClauseDetails = sectionClauseSrBean.getClauseDetail_TDS(ittHeaderId);

                                        List<TblIttSubClause> ittSubClauseDetails;
                                        int k = 1;
                                                                            for (int i = 0; i < ittClauseDetails.size(); i++) {
                                            ittSubClauseDetails = sectionClauseSrBean.getSubClauseDetail_TDS(ittClauseDetails.get(i).getIttClauseId());

                                                                                if (ittSubClauseDetails.size() > 0) {
                                                %>
                                                <tr>
                                                    <td colspan="2"><%=ittClauseDetails.get(i).getIttClauseName()%></td>
                                                </tr>
                                                <%
                                            }
                                                                                for (int j = 0; j < ittSubClauseDetails.size(); j++) {
                                                %>
                                                    <tr>
                                        <td colspan="2"><div class="scroll"><%=ittSubClauseDetails.get(j).getIttSubClauseName()%></div></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2">
                                                            <textarea cols="80" id="tds_<%=k%>" name="tds_<%=k%>" rows="10"></textarea>
                                            <input type="hidden" name="hd_tds_<%=k%>" id="hd_tds_<%=k%>" value="<%=ittHeaderId + "_" + ittClauseDetails.get(i).getIttClauseId() + "_" + ittSubClauseDetails.get(j).getIttSubClauseId()%>" />
                                                        </td>
                                                    </tr>
                                                <%
                                                k++;
                                            }
                                        }
                                        %>
                                        </table>
                                <input type="hidden" id="total" name ="total" value="<%=(k - 1)%>" />
                                        <div class="t_space" align="center">
                                            <label class="formBtn_1">
                                                <input type="submit" name="button" id="button" value="Submit"  /></label>
                                        </div>
                                    <%
                                    } else {
                                    %>
                                        <table width="100%" cellspacing="0" id="tdsInfo" class="tableList_1 t_space">
                                            <tr>
                                                <td colspan="2"><%=instruction%></td>
                                            </tr>
                                            <tr>
                                                <td><b><%if(contentType.equals("ITT")){out.print("ITB");}else{out.print(contentType);}%> Clause</b></td>
                                                <td><%=clauseInstr%></td>
                                            </tr>
                                    <%
                                        //tblTdsSubClause
                                        String ittClauseId = "-1";

                                        /*
                                            ittHeaderId, ittClauseId, ittClauseName,
 *                                          ittsubclauseid, ittSubClauseName, tdsSubClauseName,
 *                                          tdsSubClauseId, ittHeaderName, tdsSubClauseId,
 *                                          orderNo
                                        */

                                                                            int k = 1, i;
                                                                            for (i = 0; i < tblTdsSubClause.size(); i++) {
                                                                                if (!ittClauseId.equals(tblTdsSubClause.get(i).getFieldName2())) {
                                                ittClauseId = tblTdsSubClause.get(i).getFieldName2();
                                                %>
                                                <tr id="tr_<%=k++%>">
                                                    <td colspan="2"><%=tblTdsSubClause.get(i).getFieldName3()%></td>
                                                </tr>
                                                <%
                                            }
                                            %>
                                                <tr id="tr_<%=k++%>">
                                        <td colspan="2"><div class="scroll"><%=tblTdsSubClause.get(i).getFieldName5()%></div></td>
                                                </tr>
                                                <tr id="tr_<%=k++%>">
                                                    <td colspan="2">
                                            <textarea cols="80" id="tds_<%=i + 1%>" name="tds_<%=i + 1%>" rows="10"><%if (tblTdsSubClause.get(i).getFieldName6() != null) {
                                                                                                        out.print(tblTdsSubClause.get(i).getFieldName6());
                                                                                                    }%></textarea>
                                                                                                                                                <%--headerId_clauseid_ittsubclauseid_tdsSubClauseId--%>
                                            <input type="hidden" name="hd_tds_<%=i + 1%>" id="hd_tds_<%=i + 1%>" value="<%=ittHeaderId + "_" + tblTdsSubClause.get(i).getFieldName2() + "_" + tblTdsSubClause.get(i).getFieldName4() + "_" + tblTdsSubClause.get(i).getFieldName9()%>" />
                                            <input type="hidden" name="hd_orderNo_<%=i + 1%>" id="hd_orderNo_<%=i + 1%>" value="<%=tblTdsSubClause.get(i).getFieldName10()%>" />
                                            <a class="action-button-edit" href="javascript:void(0);" onclick="updateTDS('<%=i + 1%>');" >Update <%if(contentType1.equals("TDS")){out.print("BDS");}else{out.print(contentType1);}%> Details</a>
                                                        <%
                                                                                        if (ittClauseId.equals("")) {
                                                        %>
                                            <a class="action-button-delete" href="javascript:void(0);" onclick="deleteTDS('<%=(i + 1)%>');">Delete <%if(contentType1.equals("TDS")){out.print("BDS");}else{out.print(contentType1);}%> Details</a>
                                                        <%
                                                        }
                                                        %>


                                                    </td>
                                                </tr>
                                            <%
                                        }
                                        %>
                                        </table>
                                        <input type="hidden" id="total" name ="total" value="<%=i%>" />
                                        <%
                                    }
                                    %>
                            </form>

                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<%
            if (defineSTDInDtlSrBean != null) {
        defineSTDInDtlSrBean = null;
    }
%>