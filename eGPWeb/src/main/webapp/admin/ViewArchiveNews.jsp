<%-- 
    Document   : ViewArchiveNews
    Created on : Jan 31, 2011, 1:03:35 AM
    Author     : Karan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <%
            String Type = "";
            
            if(request.getParameter("newsType").equalsIgnoreCase("N"))
            {
                Type = "News/Advertisement";
            }
            else if(request.getParameter("newsType").equalsIgnoreCase("C"))
            {
                Type = "Circular";
            }
            else if(request.getParameter("newsType").equalsIgnoreCase("A"))
            {
                Type = "Amendment";
            }
            else if(request.getParameter("newsType").equalsIgnoreCase("I"))
            {
                Type = "Notification";
            }
        %>
        <title>
            View Archived <%=Type%>
        </title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript">
            function chkdisble(pageNo){
                //alert(pageNo);
                $('#dispPage').val(Number(pageNo));
                if(parseInt($('#pageNo').val(), 20) != 1){
                    $('#btnFirst').removeAttr("disabled");
                    $('#btnFirst').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 20) == 1){
                    $('#btnFirst').attr("disabled", "true");
                    $('#btnFirst').css('color', 'gray');
                }


                if(parseInt($('#pageNo').val(), 20) == 1){
                    $('#btnPrevious').attr("disabled", "true")
                    $('#btnPrevious').css('color', 'gray');
                }

                if(parseInt($('#pageNo').val(), 20) > 1){
                    $('#btnPrevious').removeAttr("disabled");
                    $('#btnPrevious').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 20) == parseInt($('#totalPages').val())){
                    $('#btnLast').attr("disabled", "true");
                    $('#btnLast').css('color', 'gray');
                }

                else{
                    $('#btnLast').removeAttr("disabled");
                    $('#btnLast').css('color', '#333');
                }

                if(parseInt($('#pageNo').val(), 20) == parseInt($('#totalPages').val())){
                    $('#btnNext').attr("disabled", "true")
                    $('#btnNext').css('color', 'gray');
                }
                else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnNext').css('color', '#333');
                }
            }
        </script>
        <%
                            String newsType = "";
                            int counterId = 0;
                            if (!request.getParameter("newsType").equals("")) {
                                newsType = request.getParameter("newsType");
                            }
        %>
        <script type="text/javascript">
            function loadTable()
            {
                $.post("<%=request.getContextPath()%>/NewsEventServlet", {funName : "viewArchiveNewsEventList", newstype :"<%= newsType %>",pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){                
                    $('#resultTable').find("tr:gt(0)").remove();
                    $('#resultTable tr:last').after(j);
                    if($('#noRecordFound').attr('value') == "noRecordFound"){
                        $('#pagination').hide();
                    }else{
                        $('#pagination').show();
                    }
                    chkdisble($("#pageNo").val());
                    if($("#totalPages").val() == 1){
                        $('#btnNext').attr("disabled", "true");
                        $('#btnLast').attr("disabled", "true");
                    }else{
                        $('#btnNext').removeAttr("disabled");
                        $('#btnLast').removeAttr("disabled");
                    }
                    $("#pageNoTot").html($("#pageNo").val());
                    $("#pageTot").html($("#totalPages").val());
                    $('#resultDiv').show();
                });
            }
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnFirst').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),20);
                    var pageNo =$('#pageNo').val();
                    if(totalPages>0 && pageNo!="1")
                    {
                        $('#pageNo').val("1");
                        loadTable();
                        $('#dispPage').val("1");
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnLast').click(function() {
                    var totalPages=parseInt($('#totalPages').val(),20);
                    if(totalPages>0)
                    {
                        $('#pageNo').val(totalPages);
                        loadTable();
                        $('#dispPage').val(totalPages);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnNext').click(function() {
                    var pageNo=parseInt($('#pageNo').val(),20);
                    var totalPages=parseInt($('#totalPages').val(),20);

                    if(pageNo < totalPages) {
                        $('#pageNo').val(Number(pageNo)+1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo)+1);

                        $('#dispPage').val(Number(pageNo)+1);
                    }
                });
            });

        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnPrevious').click(function() {
                    var pageNo=$('#pageNo').val();
                    if(parseInt(pageNo, 20) > 1)
                    {
                        $('#pageNo').val(Number(pageNo) - 1);
                        loadTable();
                        $('#dispPage').val(Number(pageNo) - 1);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),20);
                    var totalPages=parseInt($('#totalPages').val(),20);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            loadTable();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
            });
        </script>

    </head>
    <body onload="loadTable();">

        <div class="dashboard_div">
            <!--Dashboard Header Start-->
             <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                <tr>
                    <%--<%@include file="../resources/common/AfterLoginLeft.jsp" %>--%>
                    <jsp:include page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
                        <!--Dashboard Content Part Start-->
            <div class="pageHead_1">View Archived <%=Type%></div>
                    <div class="t-align-right t_space b_space">
                        <a href="NewsManagement.jsp?newsType=<%=request.getParameter("newsType")%>" class="action-button-viewTender" >View <%=Type%></a>
                    </div>
            <table width="100%" cellspacing="0" id="resultTable" class="tableList_3 t_space">
                <tr>
                <th width="4%" class="t-align-center">Sl. <br/> No.</th>
                <th width="71%" class="t-align-center">Brief</th>
                <th width="25%" class="t-align-center">Display Location</th>
                </tr>
            </table>
        <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
        <tr>
        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">20</span></td>
        <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
            &nbsp;
            <label class="formBtn_1">
                <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
            </label></td>
        <td align="right" class="prevNext-container"><ul>
                <li><font size="3">&laquo;</font> <a disabled href="javascript:void(0)" id="btnFirst">First</a></li>
                <li><font size="3">&#8249;</font> <a disabled href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                <li><a href="javascript:void(0)" id="btnNext">Next</a><font size="3"> &#8250;</font></li>
                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
            </ul></td>
                </tr>
            </table>
        <input type="hidden" id="pageNo" value="1"/>
        <input type="hidden" id="size" value="10"/>

            <div>&nbsp;</div>

            <!--Dashboard Content Part End-->
                    </td>
                </tr>
            </table>


            <!--Dashboard Footer Start-->
           <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
            <!--Dashboard Footer End-->
        </div>
            <script>
                 var newsType = '<%=newsType %>';
            if(newsType == 'N'){
                var obj = document.getElementById('lblNewsView');
                if(obj != null){
                    if(obj.innerHTML == 'View'){
                        obj.setAttribute('class', 'selected');
                    }
                }
            }else if(newsType == 'A'){
                var obj = document.getElementById('lblAmendmentView');
                if(obj != null){
                    if(obj.innerHTML == 'View'){
                        obj.setAttribute('class', 'selected');
                    }
                }
            }else if(newsType == 'C'){
                var obj = document.getElementById('lblCircularView');
                if(obj != null){
                    if(obj.innerHTML == 'View'){
                        obj.setAttribute('class', 'selected');
                    }
                }
            }else if(newsType == 'I'){
                var obj = document.getElementById('lblInstructionView');
                if(obj != null){
                    if(obj.innerHTML == 'View'){
                        obj.setAttribute('class', 'selected');
                    }
                }
            }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabContent");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
