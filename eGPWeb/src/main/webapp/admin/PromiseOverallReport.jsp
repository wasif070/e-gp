<%-- 
    Document   : PromiseOverallReport
    Created on : Feb 2, 2012, 11:20:54 AM
    Author     : shreyansh.shah
    This page has beed develop to display overall report of PROMIS report
--%>


<%@page import="com.cptu.egp.eps.service.serviceimpl.AppMISService"%>
<%@page import="com.cptu.egp.eps.model.table.TblStateMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblFinancialYear"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.FinancialYearService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
                    FinancialYearService financialYearService = (FinancialYearService) AppContext.getSpringBean("financialYearService");
                    CommonService cservice = (CommonService) AppContext.getSpringBean("CommonService");
                    int userid = 0;


                    HttpSession hs = request.getSession();

                    if (hs != null) {
                        if (hs.getAttribute("userId") != null) {
                            userid = Integer.parseInt(hs.getAttribute("userId").toString());
                        }
                    } else {
                        userid = Integer.parseInt(request.getParameter("userId").toString());
                    }

                    boolean isPDF = false;
                    if (request.getParameter("isPDF") != null) {
                        isPDF = true;
                    }
                    int uTypeId = 0;
                    String suserId = null;
                    AppMISService mISService = (AppMISService) AppContext.getSpringBean("AppMISService");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>Overall Procurement Performance Report</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="../resources/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/lang/en.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script type="text/javascript">
            /* Call Print function */

            $(document).ready(function() {
                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });
            function printElem(options){
                //$('#titleDiv').show();
                $('#print_div').printElement(options);
                //$('#titleDiv').hide();
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <%if (!isPDF) {
             suserId = session.getAttribute("userId").toString();%>
            
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
              <%uTypeId = Integer.parseInt(objUserTypeId.toString());
                } else {
                    uTypeId = Integer.parseInt(request.getParameter("uTypeId"));
                    suserId = request.getParameter("suserId");
            }%>
            
            <!--Dashboard Header End-->
            <!--Dashboard Content Part Start-->
            <div class="contentArea_1">
                <%if (!isPDF) {%>
                <div class="pageHead_1" id="Mainheading">Overall Procurement Performance Report</div>
                <div>&nbsp;</div>
                <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">- Collapse</a></div>
                <% }%>
                <div class="formBg_1">
                    <jsp:useBean id="advAppSearchSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.AdvAPPSearchSrBean"/>
                    <table id="tblSearchBox" cellspacing="10" class="formStyle_1" width="100%" <%if (isPDF) {%> style="display: none" <% }%>>
                        <tr>
                            <td class="ff">Financial Year :</td>
                            <td >
                                <select name="procNature" id="procNature" class="formTxtBox_1" id="select2" style="width:208px;" onchange="changeQuater();">
                                    <option value="-" selected="selected">All</option>
                                    <%
                                                List<TblFinancialYear> list = new ArrayList<TblFinancialYear>();
                                                try {
                                                    list = financialYearService.getFinancialYearDetails(); //calling the Service mehod the get the details.
                                                } catch (Exception e) {
                                                }
                                                for (TblFinancialYear tblfyear : list) {
                                    %>
                                    <option value="<%=tblfyear.getFinancialYear()%>"> <% out.println(tblfyear.getFinancialYear());%> </option>
                                    <% }%>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Procurement Type : </td>
                            <td>
                                <!--Change NCT to NCB and ICT to ICB by Proshanto-->
                                <select name="procType" class="formTxtBox_1" id="cmbType" style="width:208px;">
                                    <option value="-">-- Select Type --</option>
                                    <option value="NCT">NCB</option>
                                    <option value="ICT">ICB</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="ff">Select Ministry/Division/Organization/PA :</td>
                            <td colspan="3">
                               
                             <%if (uTypeId == 5) {
                                                Object[] objData = mISService.getOrgAdminOrgName(Integer.parseInt(suserId));
                                                out.print(objData[1].toString());
                                                out.print("<input type=\"hidden\" id=\"txtdepartment\" name=\"depName\" value=\"" + objData[1].toString() + "\"/>");
                                                out.print("<input type=\"hidden\" id=\"txtdepartmentid\" name=\"depId\" value=\"" + objData[0] + "\"/>");
                                                out.print("<input type=\"hidden\" id=\"uTypeId\" name=\"uTypeId\" value=\"" + uTypeId + "\"/>");
                                               
                            }else if (uTypeId == 3){
                                                Object[] objData = mISService.getOrgAdminOrgNameByUserId(Integer.parseInt(suserId));
                                                out.print(objData[1].toString());
                                                out.print("<input type=\"hidden\" id=\"txtdepartment\" name=\"depName\" value=\"" + objData[1].toString() + "\"/>");
                                                out.print("<input type=\"hidden\" id=\"txtdepartmentid\" name=\"depId\" value=\"" + objData[0] + "\"/>");
                                                out.print("<input type=\"hidden\" id=\"uTypeId\" name=\"uTypeId\" value=\"" + uTypeId + "\"/>");

                            }else{%>
                                <input type="hidden" name="viewType" id="viewType" value="Live"/>
                                <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 300px;"
                                       id="txtdepartment" readonly style="width: 200px;"/>
                                <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                <a href="#" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=homeallTenders', '', 'width=350px,height=400px,scrollbars=1','');">
                                    <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                </a>
                             <%}%>
                            </td>

                        </tr>
                        <%
                         List<Object[]> listProject = cservice.checkPD(String.valueOf(userid));
                        if(listProject.size()>0)
                            {%>
                            <tr>
                                 <td class="ff" >Project :</td>
                                 <td colspan="3">
                                     <select name="cmbProject" class="formTxtBox_1" id="cmbProject" style="width:305px;" onchange="loadOfficeForProject();">
                                    <option value="">-- Select --</option>
                                    <%
                                    for(Object[] project:listProject)
                                        {
                                          out.println("<option value='" + project[1].toString() + "'>" + project[0].toString() + "</option>");
                                        }
                                    %>
                                    </select>
                                 </td>
                            </tr>
                           <% }else{   %>
                           <select name="cmbProject" class="formTxtBox_1" id="cmbProject" style="width:305px;display: none;" >
                                    <option value="">-- Select --</option>
                           </select> <%}%>
                        <tr>
                            <td class="ff">Procuring Entity :</td>
                            <td colspan="3">
                                <select name="office" class="formTxtBox_1" id="cmbOffice" style="width:305px;" >
                                     <option value="0">-- Select Office --</option>
                                    <%
                                    List<Object[]> listProjectOffice = cservice.getProjectOffice(String.valueOf(userid));

                                               
                                                if ((uTypeId == 5 || uTypeId == 3) && listProject.size()==0) {
                                                   
                                                    if(listProjectOffice.size()>0){
                                                     for(Object[] projectOff:listProjectOffice){
                                                        out.print("<option value='" + projectOff[0].toString() + "'>" +  projectOff[1].toString() + "</option>");
                                                        }
                                                    }else{
                                                    List<Object[]> cmbLists = null;
                                                    cmbLists = mISService.getOfficeList(suserId, "" + uTypeId);
                                                    for (Object[] cmbList : cmbLists) {
                                                        String selected = "";
                                                        if (request.getParameter("offId") != null) {
                                                            if (cmbList[0].toString().equals(request.getParameter("offId"))) {
                                                                selected = "selected";
                                                            }
                                                        }
                                                        out.print("<option value='" + cmbList[0] + "' " + selected + ">" + cmbList[1] + "</option>");
                                                        selected = null;
                                                    }

                                                            if(cmbLists.isEmpty()) {
                                                        out.print("<option value='0'> No Office Found.</option>");
                                                   }
                                                }}
                                                     
                                            %>
                                   

                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" class="ff">Dzongkhag / District : </td>
                            <%
                                      //Change Country ID 136 to 150 to show Bhutan District by Proshanto
                                        short countryId = 150;
                                        List<TblStateMaster> liststate = cservice.getState(countryId);
                            %>
                            <td width="30%">
                                <select name="cmbDistrict" class="formTxtBox_1" id="cmbDistrict" style="width:95%;">
                                    <option value="">-- Select --</option>
                                    <%
                                                for (TblStateMaster state : liststate) {
                                                    out.println("<option value='" + state.getStateId() + "'>" + state.getStateName() + "</option>");
                                                }
                                    %>
                                </select>
                                <span id="msgdistrict" class="reqF_1"></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" class="ff">Indicator Category : </td>
                            <td width="30%">
                                <select name="cmbIndiCategory" class="formTxtBox_1" id="cmbIndiCategory" style="width:95%;">
                                    <option value="1,42">-- Select --</option>
                                    <option value="1,4">Invitation for Tender</option>
                                    <option value="5,10">Tender Submission</option>
                                    <option value="11,13">Tender Opening Committee (TOC) and Tender Evaluation Committee (TEC)</option>
                                    <option value="14,18">Tender Evaluation</option>
                                    <option value="19,24">Tender Evaluation Report (TER) Approval</option>
                                    <option value="25,29">Contract Award</option>
                                    <option value="30,32">Delivery/Completion</option>
                                    <option value="33,35">Payment</option>
                                    <option value="36,39">Complaints</option>
                                    <option value="40,40">Contract Amendments</option>
                                    <option value="41,41">Contract Dispute Resolution</option>
                                    <option value="42,42">Fraud and Corruption</option>
                                </select>
                                <span id="msgdistrict" class="reqF_1"></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="15%" class="ff" align="left">By Quarter :</td>
                                    <td width="35%" align="left">
                                        <select name="quater" id="quater" disabled style="width:200px;" class="formTxtBox_1" onchange="setQuaterName()">
                                            <option value="2012-07-1_2013-06-30">All Four Quarter</option>
                                            <option value="2012-07-1_2012-09-30">Q1</option>
                                            <option value="2012-10-1_2012-12-31">Q2</option>
                                            <option value="2013-01-1_2013-03-31">Q3</option>
                                            <option value="2013-04-1_2013-06-30">Q4</option>
                                            <option value="2012-07-1_2012-12-31">Q1 and Q2</option>
                                            <option value="2012-10-1_2013-03-31">Q2 and Q3</option>
                                            <option value="2013-01-1_2013-06-30">Q3 and Q4</option>
                                            <option value="2012-07-1_2013-03-31">Q1 and Q2 and Q3</option>
                                            <option value="2012-10-1_2013-06-30">Q2 and Q3 and Q4</option>
                                        </select>
                                        <input type="hidden" name="quatername" id="quatername"/>
                                    </td>
                        </tr>
                        
                        <tr>
                            <td colspan="4" align="center"><label class="formBtn_1">
                                    <input type="submit" name="button" id="btnSearch" value="Search" />
                                </label>
                                &nbsp;
                                <label class="formBtn_1">
                                    <input type="reset" name="Reset" id="btnReset" value="Reset" />
                                </label></td>
                                <%if (!isPDF) {%>
                            <td class="t-align-right">
                                <%
                                    String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
                                    String folderName = pdfConstant.PROMISOVERALLREPORT;
                                    String genId = cdate + "_" + userid;
                                %>
                                <a class="action-button-savepdf"
                                   href="<%=request.getContextPath()%>/GeneratePdf?reqURL=&reqQuery=&folderName=<%=folderName%>&id=<%=genId%>" >Save As PDF </a>
                                &nbsp;
                                <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
                            </td>
                            <% }%>
                        </tr>
                        <tr>
                            <td class="t-align-center" colspan="4">
                                <div id="valAll" class="reqF_1"></div>
                            </td>
                        </tr>
                    </table>

                </div>
                        
                <div id="print_div">
                    <div class="tableHead_1 t_space" id="getSubHeading">
                        Overall Procurement Performance Report
                    </div>
                    <div class="tabPanelArea_1">
                        <table width="100%" cellspacing="0" class="tableList_1" id="resultTable">
                            <div id="progressBar" class="tableHead_1 t_space" align="center" style="visibility:hidden"  >
                                <img alt="Please Wait" src="<%=request.getContextPath()%>/resources/images/loading.gif"/>
                            </div>
                            <tr id="subtableheading">
                            
                            </tr>

                        </table>
                        <!--                    <table width="100%" border="0" id="pagination" cellspacing="0" cellpadding="0" class="pagging_1">
                                                <tr>
                                                    <td align="left">Page <span id="pageNoTot">1</span> - <span id="pageTot">10</span></td>
                                                    <td align="center"><center><input name="textfield3" onkeypress="checkKeyGoTo(event);" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                                            &nbsp;
                                                            <label class="formBtn_1">
                                                                <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                                            </label></center></td>
                                                    <td class="prevNext-container">
                                                        <ul>
                                                            <li>&laquo; <a href="javascript:void(0)" id="btnFirst">First</a></li>
                                                            <li>&#8249; <a href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                                            <li><a href="javascript:void(0)" id="btnNext">Next</a> &#8250;</li>
                                                            <li><a href="javascript:void(0)" id="btnLast">Last</a> &raquo;</li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </table>-->
                        <div align="center">
                            <input type="hidden" id="pageNo" value="1"/>
                            <input type="hidden" name="size" id="size" value="1000"/>
                        </div>
                    </div>
                </div>
                <div>&nbsp;</div>
            </div>
            <!--Dashboard Content Part End-->
            <!--Dashboard Footer Start-->
            <%if (!isPDF) {%>
            <%@include file="../resources/common/Bottom.jsp" %>
            <%}%>
            <!--Dashboard Footer End-->

        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
    <script type="text/javascript">
        function changeQuater(){
                var cmb = document.getElementById('procNature');
                if($('#procNature').val()=='-'){
                    document.getElementById("quater").disabled = true;}

                else{

                document.getElementById("quater").disabled = false;
                var cval1 = cmb.value.split("-")[0];
                var cval2 = cmb.value.split("-")[1];
//                var qstring = "<option value='0'>Please Select Quarter</option><option value='"+cval1+"-07-1_"+cval1+"-09-30'>Q1</option><option value='"+cval1+"-10-1_"+cval1+"-12-31'>Q2</option><option value='"+cval2+"-01-1_"+cval2+"-03-31'>Q3</option><option value='"+cval2+"-04-1_"+cval2+"-06-30'>Q4</option><option value='"+cval1+"-07-1_"+cval1+"-12-31'>Q1 and Q2</option><option value='"+cval1+"-10-1_"+cval2+"-03-31'>Q2 and Q3</option><option value='"+cval2+"-01-1_"+cval2+"-06-30'>Q3 and Q4</option><option value='"+cval1+"-07-1_"+cval2+"-03-31'>Q1 and Q2 and Q3</option><option value'"+cval1+"-10-1_"+cval2+"-06-30'>Q2 and Q3 and Q4</option><option value='"+cval1+"-07-1_"+cval2+"-06-30'>All</option>";
                var qstring = "<option value='"+cval1+"-07-1_"+cval2+"-06-30'>All Four Quarter</option><option value='"+cval1+"-07-1_"+cval1+"-09-30'>Q1</option><option value='"+cval1+"-10-1_"+cval1+"-12-31'>Q2</option><option value='"+cval2+"-01-1_"+cval2+"-03-31'>Q3</option><option value='"+cval2+"-04-1_"+cval2+"-06-30'>Q4</option><option value='"+cval1+"-07-1_"+cval1+"-12-31'>Q1 and Q2</option><option value='"+cval1+"-10-1_"+cval2+"-03-31'>Q2 and Q3</option><option value='"+cval2+"-01-1_"+cval2+"-06-30'>Q3 and Q4</option><option value='"+cval1+"-07-1_"+cval2+"-03-31'>Q1 and Q2 and Q3</option><option value='"+cval1+"-10-1_"+cval2+"-06-30'>Q2 and Q3 and Q4</option>";
                //var qcombo1 = document.getElementById('quater').options[1].value.substring(0, 4);
                //var qcombo2 = document.getElementById('quater').options[3].value.substring(0, 4);
                $("#quater").html(qstring);
                }
            }
       function setQuaterName(){
                var cmb = document.getElementById('quater');
                $("#quatername").val(cmb.options[cmb.selectedIndex].text);
            }
       function validate(){
                $(".err").remove();
                var cnt=0;
                if($('#procNature').val()=='-'){
                    $('#procNature').parent().append("<div class='err' style='color:red;'>Please select Financial Year</div>");
                    cnt++;
                }
                else{
                    loadTenderTable();
                    $("#resultTable").show();
                  
                }
                return (cnt==0);
            }
    </script>
    <script type="text/javascript">
        function GetCal(txtname,controlname)
        {
            new Calendar({
                inputField: txtname,
                trigger: controlname,
                showTime: false,
                dateFormat:"%d/%m/%Y",
                onSelect: function() {
                    var date = Calendar.intToDate(this.selection.get());
                    LEFT_CAL.args.min = date;
                    LEFT_CAL.redraw();
                    this.hide();
                }
            });

            var LEFT_CAL = Calendar.setup({
                weekNumbers: false
            })
        }
    </script>
    <script type="text/javascript">
        function chkdisble(pageNo){
            $('#dispPage').val(Number(pageNo));
            if(parseInt($('#pageNo').val(), 10) != 1){
                $('#btnFirst').removeAttr("disabled");
                $('#btnFirst').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnFirst').attr("disabled", "true");
                $('#btnFirst').css('color', 'gray');
            }


            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnPrevious').attr("disabled", "true")
                $('#btnPrevious').css('color', 'gray');
            }

            if(parseInt($('#pageNo').val(), 10) > 1){
                $('#btnPrevious').removeAttr("disabled");
                $('#btnPrevious').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnLast').attr("disabled", "true");
                $('#btnLast').css('color', 'gray');
            }

            else{
                $('#btnLast').removeAttr("disabled");
                $('#btnLast').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnNext').attr("disabled", "true")
                $('#btnNext').css('color', 'gray');
            }
            else{
                $('#btnNext').removeAttr("disabled");
                $('#btnNext').css('color', '#333');
            }
        }
    </script>
    <script type="text/javascript">
        function changeTab(tabNo){
            loadTenderTableone();
        }

    </script>
    <script type="text/javascript">
        function loadOffice() {
            var deptId=$('#txtdepartmentid').val();
            $.post("<%=request.getContextPath()%>/ComboServlet", {departmentId: deptId,funName:'officeComboforpromis'},  function(j){
                $('#cmbOffice').children().remove().end()
                $("select#cmbOffice").html(j);
            });
        }
        function loadOfficeForProject() {
            var projectId=$("#cmbProject").val();
            $.post("<%=request.getContextPath()%>/ComboServlet", {projectId: projectId,funName:'officeComboforproject'},  function(j){
                $('#cmbOffice').children().remove().end()
                $("select#cmbOffice").html(j);
            });
        }

    </script>
    <!-- AJAX Grid Functions Start -->
    <script type="text/javascript">
        $(function() {
            var count;
            $('#btnSearch').click(function() {
                loadTenderTable();
                
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnReset').click(function() {
                $("#pageNo").val("1");
                $("#refNo").val('');
                $("#tenderId").val('');
                if(($('#uTypeId').val() != '5') && ($('#uTypeId').val() != '3' )){
                    $('#cmbOffice').empty().append('<option value="0">-- Select Office --</option>');
                //$("#PEId").val('');
                    $("#txtdepartment").val('');
                    $("#txtdepartmentid").val('0');
                }
                $("select#procNature option[selected]").removeAttr("selected");
                $("select#procNature option[value='-']").attr("selected", "selected");
                $("select#cmbType option[selected]").removeAttr("selected");
                $("select#cmbType option[value='-']").attr("selected", "selected");
                //$("#cmbProcMethod").val('');
                $("#pubDtFrm").val('');
                $("#pubDtTo").val('');
                $("#cmbDistrict").val('');
                $("select#quater")[0].selectedIndex = 0;
                $(".err").remove();
                // $("#getSubHeading").html("");
                $("#subtableheading").html("");
                $("#resultTable").hide();
              //  loadTenderTable();
            });
        });
    </script>

    <script type="text/javascript">
        function getTableHeading()
        {
            tableheadings = '<th width=\"5%\" class=\"t-align-center\">Sr.No.</th>'+
                '<th width=\"15%\" class=\"t-align-center\">Indicator Category</th>'+
                '<th width=\"25%\" class=\"t-align-center\">Process Indicator</th>'+
                '<th width=\"30%\" class=\"t-align-center\">Description</th>'+
                '<th width=\"15%\" class=\"t-align-center\">Result</th>' +
                '<th width=\"30%\" class=\"t-align-center\">Total Tender</th>'


            return tableheadings;

        }
        function loadTenderTableone()
        {
            $.post("<%=request.getContextPath()%>/PromiseReportServlet", {
                fyear :$("#statusTab").val(),
                ptype: "Ministry",
                deptid:$("#statusTab").val(),
                officeid: $("#status").val(),
                //PEId : $("#PEId").val(),
                pageNo: $("#pageNo").val(),
                size: $("#size").val(),
                isPDF:'<%=isPDF%>'
            },
            function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);

                if($('#noRecordFound').attr('value') == "noRecordFound"){
                    $('#pagination').hide();
                }else{
                    $('#pagination').show();
                }
                chkdisble($("#pageNo").val());
                if($("#totalPages").val() == 1){
                    $('#btnNext').attr("disabled", "true");
                    $('#btnLast').attr("disabled", "true");
                }else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnLast').removeAttr("disabled");
                }
                $("#pageTot").html($("#totalPages").val());
                $("#pageNoTot").html($("#pageNo").val());
                $('#resultDiv').show();

                var counter = $('#cntTenBrief').val();
                for(var i=0;i<counter;i++){
                    if($('#tenderBrief_'+i).html() != null){
                        //alert($('#tenderBrief_'+i).html());
                        //alert($('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, ''));
                        var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                        var temp1 = $('#tenderBrief_'+i).html();
                        if(temp.length > 250){
                            temp = temp1.substr(0, 250);
                            $('#tenderBrief_'+i).html(temp+'...');
                        }
                    }
                }
            });
        }

        function loadTenderTable()
        {
           
       
            //   var Heading = getMainHeading();
            $("#progressBar").show();
            $("#progressBar").css("visibility","visible");
            var deptid = 0;
            if($("#txtdepartmentid").val() != "")
            {
                deptid = $("#txtdepartmentid").val();
            }
            var subtableheading = getTableHeading();
           
            $("#subtableheading").html(subtableheading);
            $.post("<%=request.getContextPath()%>/PromiseReportServlet", {
                //                fyear :$("#statusTab").val(),
                //                ptype: "Ministry",
                //                deptid:$("#statusTab").val(),
                //                officeid: $("#status").val(),
                //                pageNo: $("#pageNo").val(),
                //                size: $("#size").val()
                funName : "OverallPromiseReport",
                fyear :$("#procNature").val(),
                ptype: $("#cmbType").val(),
                deptid: deptid,
                officeid:$("#cmbOffice").val(),
                PEId : "1",
                pageNo:$("#pageNo").val(),
                size:$("#size").val(),
                isPDF:'<%=isPDF%>',
                districtId:$("#cmbDistrict").val(),
                ctgIndicator:$("#cmbIndiCategory").val(),
                quater:$("#quater").val(),
                projectId:$("#cmbProject").val()

            },
            function(j){
                $('#resultTable').find("tr:gt(0)").remove();
                $('#resultTable tr:last').after(j);
                 $("#progressBar").hide();

                //                if($('#noRecordFound').attr('value') == "noRecordFound"){
                //                    $('#pagination').hide();
                //                }else{
                //                    $('#pagination').show();
                //                }
                //                chkdisble($("#pageNo").val());
                //                if($("#totalPages").val() == 1){
                //                    $('#btnNext').attr("disabled", "true");
                //                    $('#btnLast').attr("disabled", "true");
                //                }else{
                //                    $('#btnNext').removeAttr("disabled");
                //                    $('#btnLast').removeAttr("disabled");
                //                }
                //                $("#pageTot").html($("#totalPages").val());
                //                $("#pageNoTot").html($("#pageNo").val());
                //                $('#resultDiv').show();

                //                var counter = $('#cntTenBrief').val();
                //                for(var i=0;i<counter;i++){
                //                    var temp = $('#tenderBrief_'+i).html().replace(/<[^>]*>|\s/g, '');
                //                    var temp1 = $('#tenderBrief_'+i).html();
                //                    if(temp.length > 250){
                //                        temp = temp1.substr(0, 250);
                //                        $('#tenderBrief_'+i).html(temp+'...');
                //                    }
                //                    //$('#tenderBrief_'+i).attr('style', 'width: 20px;');
                //                }
            });
             
        }
    </script>
    <%--<script type="text/javascript">
function loadListingTable()
{
    $.post("<%=request.getContextPath()%>/TenderDetailsServlet", {funName: "MyTenders",action: "defualt",pageNo: $("#pageNo").val(),size: $("#size").val()},  function(j){
        $('#resultTable').find("tr:gt(0)").remove();
        $('#resultTable tr:last').after(j);
    });
}

        </script>--%>
    <script type="text/javascript">
        $(function() {
            $('#btnFirst').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);

                if(totalPages>0 && $('#pageNo').val()!="1")
                {
                    $('#pageNo').val("1");
                    loadTenderTable();
                    $('#dispPage').val("1");
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnLast').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);
                if(totalPages>0)
                {
                    $('#pageNo').val(totalPages);
                    loadTenderTable();
                    $('#dispPage').val(totalPages);
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnNext').click(function() {
                var pageNo=parseInt($('#pageNo').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);

                if(pageNo <= totalPages) {
                    $('#pageNo').val(Number(pageNo)+1);
                    loadTenderTable();
                    $('#dispPage').val(Number(pageNo)+1);
                    $('#btnPrevious').removeAttr("disabled");
                }
            });
        });

    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnPrevious').click(function() {
                var pageNo=$('#pageNo').val();

                if(parseInt(pageNo, 10) > 1)
                {
                    $('#pageNo').val(Number(pageNo) - 1);
                    loadTenderTable();
                    $('#dispPage').val(Number(pageNo) - 1);
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $('#btnGoto').click(function() {
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTenderTable();
                        $('#dispPage').val(Number(pageNo));
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                        if(parseInt($('#pageNo').val(), 10) > 1)
                            $('#btnPrevious').removeAttr("disabled");
                    }
                }
                loadTenderTable();
            });
        });
    </script>
    <!-- AJAX Grid Finish-->
    <script type="text/javascript">
        //loadTenderTable();
    </script>
    <script type="text/javascript">
        function showHide()
        {
            if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='- Collapse'){
                document.getElementById('tblSearchBox').style.display = 'none';
                document.getElementById('collExp').innerHTML = '+ Expand';
            }else{
                document.getElementById('tblSearchBox').style.display = 'table';
                document.getElementById('collExp').innerHTML = '- Collapse';
            }
        }

        function checkKeyGoTo(e)
        {
            var keyValue = (window.event)? e.keyCode : e.which;
            if(keyValue == 13){
                //Validate();
                $(function() {
                    //$('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            //loadTable();
                            $('#btnGoto').click();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
                //});
            }
        }
    </script>
</html>

