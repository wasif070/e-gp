<%-- 
    Document   : NotifyUserRegRenewal
    Created on : Feb 19, 2011, 2:28:06 PM
    Author     : Administrator
--%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.CommonSearchService"%>
<%@page import="com.cptu.egp.eps.web.utility.DateUtils"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserRegisterService"%>
<%@page import="com.cptu.egp.eps.web.utility.XMLReader"%>
<%@page import="com.cptu.egp.eps.web.utility.MsgBoxContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.MailContentUtility"%>
<%@page import="com.cptu.egp.eps.web.utility.SendMessageUtil"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Notify User Reg Renewal</title>
    </head>
    <body>
        <%
            Logger LOGGER = Logger.getLogger("NotifyUserRegRenewal.jsp");
            UserRegisterService  service = (UserRegisterService)AppContext.getSpringBean("UserRegisterService");
            List <SPCommonSearchData> recordCron = null;

            CommonSearchService cmnSearchService = (CommonSearchService) AppContext.getSpringBean("CommonSearchService");
            recordCron = cmnSearchService.searchData("getCronJob", "Notify for Registration Renewal", null, null, null, null, null, null, null, null);

            if(recordCron.isEmpty())
            {
                CommonXMLSPService  xmlservice = (CommonXMLSPService)AppContext.getSpringBean("CommonXMLSPService");

                Date curDate = new Date();
                Date newDate = new Date(curDate.getYear(), curDate.getMonth()+1, curDate.getDate());

                int cronStatus = 1;
                String cronMsg = "Successful Executed";
                String cronName = "Notify for Registration Renewal";
                String  cronStartTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());

                SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
                String strnewDate = sd.format(newDate);

                List <SPCommonSearchData> spcommonSearchData = null;
                spcommonSearchData = cmnSearchService.searchData("getExpiredRegCustList", strnewDate, null, null, null, null, null, null, null, null);

                try{
                    if(spcommonSearchData.size() > 0)
                    {
                        for(SPCommonSearchData userData : spcommonSearchData)
                        {
                            String mails[]={userData.getFieldName1()};
                            SendMessageUtil sendMessageUtil = new SendMessageUtil();
                            MailContentUtility mailContentUtility = new MailContentUtility();
                            MsgBoxContentUtility msgBoxContentUtility = new MsgBoxContentUtility();
                            service.contentAdmMsgBox(mails[0], XMLReader.getMessage("emailIdHelp"), "e-GP System: Your Account Renewal", msgBoxContentUtility.regRenewalContent(strnewDate));

                            String mailText = mailContentUtility.regRenewalContent(strnewDate);
                            sendMessageUtil.setEmailTo(mails);
                            sendMessageUtil.setEmailSub("e-GP System: Your Account Renewal");
                            sendMessageUtil.setEmailMessage(mailText);
                            sendMessageUtil.sendEmail();

                            mails=null;
                            sendMessageUtil=null;
                            mailContentUtility=null;
                            msgBoxContentUtility=null;
                        }
                     }
                }catch(Exception ex){
                    cronMsg = ex.toString();
                    cronStatus = 0;
                }

                String  cronEndTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());

                String xml = "<root>"
                                + "<tbl_CronJobs "
                                    + "cronJobName=\""+cronName+"\" "
                                    + "cronJobStartTime=\""+cronStartTime+"\" "
                                    + "cronJobEndTime=\""+cronEndTime+"\" "
                                    + "cronJobStatus=\""+cronStatus+"\" "
                                    + "cronJobMsg=\""+cronMsg+"\"/>"
                            + "</root>";
                try {
                    xmlservice.insertDataBySP("insert", "tbl_CronJobs", xml, "");
                } catch (Exception ex) {
                    LOGGER.error("Exception found at sendRegRenewalMail :"+ ex);
                }
            }
        %>
    </body>
</html>
