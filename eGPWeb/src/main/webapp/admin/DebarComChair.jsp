<%-- 
    Document   : DebarComChair.jsp
    Created on : May 3, 2017, 10:52:48 AM
    Author     : feroz
--%>
<%@page import="com.cptu.egp.eps.model.table.TblBhutanDebarmentCommittee"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.BhutanDebarmentCommitteeService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Debarment Committee Chairman</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>        <script type="text/javascript">
            $(document).ready(function() {
                sortTable();
            });
        </script>
        
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <%
                    BhutanDebarmentCommitteeService bhutanDebarmentCommitteeService = (BhutanDebarmentCommitteeService) AppContext.getSpringBean("BhutanDebarmentCommitteeService");
                    List<TblBhutanDebarmentCommittee> tblBhutanDebarmentCommittee = bhutanDebarmentCommitteeService.getAllTblBhutanDebarmentCommittee();
         %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea">
                          <%
                                String message = "";
                                if (!"".equalsIgnoreCase(request.getParameter("msg"))) {
                                    message = request.getParameter("msg");
                                    if ("actS".equalsIgnoreCase(message)) {
                                    %>
                                    <div class="responseMsg successMsg t_space"><span>Debarment Committee Member is activated successfully.</span></div><br/>
                                    <%}else if ("actF".equalsIgnoreCase(message)){
                                    %>
                                        <div class="responseMsg errorMsg t_space"><span>Error occurred while activating Debarment Committee Member!</span></div><br/>
                                    <%}else if ("chF".equalsIgnoreCase(message)){
                                    %>
                                        <div class="responseMsg errorMsg t_space"><span>There is already an active Chairman!</span></div><br/>
                                    <%}else if ("dctS".equalsIgnoreCase(message)){
                                    %>
                                        <div class="responseMsg successMsg t_space"><span>Debarment Committee Member is deactivated successfully.</span></div><br/>
                                    <%}else if ("dctF".equalsIgnoreCase(message)){
                                    %>
                                        <div class="responseMsg errorMsg t_space"><span>Error occurred while deactivating Debarment Committee Member!</span></div><br/>
                                    <%}else if ("edtS".equalsIgnoreCase(message)){
                                    %>
                                        <div class="responseMsg successMsg t_space"><span>Updated successfully.</span></div><br/>
                                    <%}else if ("edtF".equalsIgnoreCase(message)){
                                    %>
                                        <div class="responseMsg errorMsg t_space"><span>Error occurred while updating!</span></div><br/>
                                    <%}else if("crS".equalsIgnoreCase(message)){%>
                                     <div class="responseMsg successMsg t_space"><span>Member is created successfully!</span></div><br/>
                                <%}
                                    }
                            %>
                        <div class="pageHead_1">Chairmanship
                        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('2');">Save as PDF</a></span>
                        </div>
                        <br>
                        <div class="tabPanelArea_1">
                            <%
                                if(tblBhutanDebarmentCommittee.isEmpty())
                                {
                                    %><b><span style="color: red; padding-left: 42%; font-size:20px;"><%out.print("No data found!");%></span></b><%
                                }
                                else{
                            %>
                            <table class="tableList_1 t_space" cellspacing="0" width="100%" id="resultTable" cols="">
                                <tbody id="tbodyData">&nbsp;
                                  
                                <tr>
                                    <th class="t-align-center" style="width: 4%">Sl. No.</th>
                                    <th class="t-align-center">Name</th>
                                    <th class="t-align-center">Designation</th>
                                    <th class="t-align-center">Status</th>
                                    <th class="t-align-center">Action</th>
                                </tr>
                                <%
                                  int cnt = 0;
                                  for(TblBhutanDebarmentCommittee tblBhutanDebarmentCommitteeThis : tblBhutanDebarmentCommittee){
                                      if(tblBhutanDebarmentCommitteeThis.getRole().equals("Chairman")){
                                        cnt++;
                                        if(cnt%2==0){%>
                                              <tr>
                                      <%}else{%>
                                              <tr style='background-color:#E4FAD0;'>
                                      <%}%>
                                              <td class="t-align-center">
                                                  <%out.print(cnt);%>
                                              </td>
                                              <td class="t-align-center">
                                                  <%out.print(tblBhutanDebarmentCommitteeThis.getMemberName());%>
                                              </td>
                                              <td class="t-align-center">
                                                  <%out.print(tblBhutanDebarmentCommitteeThis.getDesignation());%>
                                              </td>
                                              <td class="t-align-center">
                                                  <%if(tblBhutanDebarmentCommitteeThis.isIsActive())
                                                  {
                                                      %><b style="color:green"><%out.print("Active");
                                                  }
                                                  else
                                                  {
                                                      out.print("Inactive");
                                                  }
                                                  %>
                                              </td>
                                              <td class="t-align-center">
                                                  <a href="ViewDebarCom.jsp?memberId=<%=tblBhutanDebarmentCommitteeThis.getId()%>">Details</a>&nbsp;
                                                  <a href="EditDebarMember.jsp?memberId=<%=tblBhutanDebarmentCommitteeThis.getId()%>">Edit</a>&nbsp;
                                                  <%
                                                    if(tblBhutanDebarmentCommitteeThis.isIsActive())
                                                    {
                                                  %>
                                                    <a href="DeactivateMember.jsp?memberId=<%=tblBhutanDebarmentCommitteeThis.getId()%>">Deactivate</a>
                                                  <%
                                                    }else
                                                    {
                                                  %>
                                                    <a href="ActivateMember.jsp?memberId=<%=tblBhutanDebarmentCommitteeThis.getId()%>">Activate</a>
                                                  <%}%>
                                              </td>
                                              </tr>
                                <%}}%>
                                </tbody>
                            </table>
                                    <%}%>
                        </div>
                </td>
            </tr>
        </table>

        <form id="formstyle" action="" method="post" name="formstyle">

           <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
           <%
             SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
             String appenddate = dateFormat1.format(new Date());
           %>
           <input type="hidden" name="fileName" id="fileName" value="DebarmentCommittee_<%=appenddate%>" />
            <input type="hidden" name="id" id="id" value="EvaluationRule" />
        </form>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabDebar");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        var leftObj = document.getElementById("lblChairDebar");
        if(leftObj != null){
            leftObj.setAttribute("class", "selected");
        }
    </script>
</html>