<%-- 
    Document   : ManageUserRights
    Created on : Mar 5, 2012, 4:33:10 PM
    Author     : dipal.shah
--%>


<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.ManageUserRights"%>
<%@page import="com.cptu.egp.eps.model.table.TblMenuMaster"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.ManageUserRightsImpl"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manage User Rights</title>
        <%String contextPath = request.getContextPath();%>
        <link href="<%=contextPath%>/resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="<%=contextPath%>/resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/deployJava.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/form/CommonValidation.js" type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery-ui-1.8.5.custom.min.js"  type="text/javascript"></script>
        <script src="<%=contextPath%>/resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>

        <body>
          <script type="text/javascript">
            function chkValidate()
            {
                var obj=document.frmSubmit.chk;
                var flg=false;
                for (i = 0; i < obj.length; i++)
                    {
                        if(obj[i].checked == true)
                            {
                                flg=true;
                                return;
                            }
                    }       

                if(!flg)
                {
                    alert("Select at least one checkbox to assign rights.");
                    return false;
                }
                else
                    return true;
            }

            function checkParent(lstControl,varMainMenuId)
            {
               // alert(lstControl.length);
                var flg=false;
                for (i = 0; i < lstControl.length; i++)
                {
                    if(lstControl[i].checked == true)
                        {
                            flg=true;
                        }
                }
                document.getElementById("chk_"+varMainMenuId).checked=flg;
            }

            
          </script>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="fixDiv">
               
                 <!--Middle Content Table Start-->
<%

                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
                
                ManageUserRightsImpl objManageUserRights1 = (ManageUserRightsImpl)AppContext.getSpringBean("ManageUserRigths");
                
                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");

                objManageUserRights1.setAuditTrail(objAuditTrail);
                objManageUserRights1.setMakeAuditTrailService(makeAuditTrailService);
                int userId = 0;
                if(session.getAttribute("userId") != null) {
                    if(!"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                        userId = Integer.parseInt(session.getAttribute("userId").toString());
                    }
                }
               objManageUserRights1.setLogUserId(userId+"");
               
                int uId = 0;
                if (request.getParameter("uId") != null) {
                    uId = Integer.parseInt(request.getParameter("uId"));
                }

                int userTypeid = 0;
                if (request.getParameter("userTypeid") != null) {
                    userTypeid = Integer.parseInt(request.getParameter("userTypeid"));
                }

                String lblUserType="User";
                if(userTypeid == 19)
                {
                    lblUserType="Admin";
                }
                int logUserTypeid = 0;
                if (session.getAttribute("userTypeId") != null) {
                    logUserTypeid = Integer.parseInt(session.getAttribute("userTypeId").toString());
                }

                String action = "";
                if(request.getParameter("action") != null && !"".equalsIgnoreCase(request.getParameter("action")))
                {
                     action = request.getParameter("action");
                }

                List<TblMenuMaster> lstMenuMaster=objManageUserRights1.getMenuList(0);

                List<Object[]> objUserDetails=objManageUserRights1.getUserDetails(uId);
                String userName="";
                String emailId="";
                if(objUserDetails != null)
                {
                    for(Object[] objArray : objUserDetails)
                    {
                        if(objArray!=null && objArray[0]!=null)
                        {
                            userName=objArray[0].toString();
                        }
                        if(objArray!=null && objArray[1]!=null)
                        {
                            emailId=objArray[1].toString();
                        }
                    }
                }

                ArrayList objUserRights = objManageUserRights1.getUserMenuRightsList(uId);
                Map<Integer,String> objUserRightsMap1= null;
                objUserRightsMap1=objManageUserRights1.getUserMenuRightsMap(loguserId);
                //out.println(objUserRights.size());

                %>
               <table border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                    <%
                        StringBuilder userType = new StringBuilder();
                        if (request.getParameter("hdnUserType") != null) {
                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                        userType.append(request.getParameter("hdnUserType"));
                        } else {
                        userType.append("org");
                        }
                        } else {
                        userType.append("org");
                        }
                    %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                        <td class="contentArea_1" width="100%">
                        <div>&nbsp;</div>
                       
                                <div class="pageHead_1">
                                    Manage <%=lblUserType%> Rights
                                </div>
                        

                  <form id="frmSubmit" name="frmSubmit" method="post" action="<%=request.getContextPath()%>/ManageUserRights">
                    <input type="hidden" name="uId" id="uId" value="<%=uId%>">
                    <input type="hidden" name="action" id="action" value="assignRights">
                    <input type="hidden" name="userTypeid" id="userTypeid" value="<%=userTypeid%>">

                        
                        <div class="tableHead_1 t_space"><%=lblUserType%> Details</div>
                        <table width="100%"  cellspacing="10" class="tableView_1 infoBarBorder">
                        <tr>
                          <td width="10%" class="ff"><%=lblUserType%> Name :</td>
                          <td width="25%" align="left"><%=userName%></td>
                          <td width="5%" class="ff">Email Id :</td>
                          <td width="25%" align="left"><%=emailId%></td>
                        </tr>
                       </table>
                    
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" align="center">
                        <th>
                            Module / Menu
                        </th>
                        <th>
                            Functionality / Sub Menu
                        </th>
                        <%

                        for(TblMenuMaster objMenu : lstMenuMaster)
                        {
                            if(logUserTypeid == 19 && objUserRightsMap1!=null && !objUserRightsMap1.containsKey(objMenu.getMenuId()))
                            {
                                %>
                                <tr valign="top" style="display: none;">
                                <%
                            }
                            else
                            {
                                %>
                                <tr valign="top">
                                <%
                            }
                               %>
                                    <td>
                                        <%= objMenu.getMenuName()%>
                                        <input style="display: none;" type="checkbox" id="chk_<%= objMenu.getMenuId()%>" name="chk" value="<%= objMenu.getMenuId() %>"
                                               <%
                                                if(objUserRights!=null && objUserRights.contains(objMenu.getMenuId()))
                                                { out.println("checked");}
                                               %>
                                        >
                                    </td>
                                    <td>
                                        <%
                                        for(TblMenuMaster objSubMenu : objManageUserRights1.getMenuList(objMenu.getMenuId()))
                                        {

                                                if(logUserTypeid == 19 && objUserRightsMap1!=null && !objUserRightsMap1.containsKey(objSubMenu.getMenuId()))
                                                {
                                                   %>
                                                   <input  style="display: none;"  type="checkbox" id="chk_sub_<%= objMenu.getMenuId() %>" name="chk" onclick="checkParent(document.frmSubmit.chk_sub_<%= objMenu.getMenuId() %>,'<%=objMenu.getMenuId()%>');" value="<%= objSubMenu.getMenuId() %>"
                                                   <%
                                                   if(objUserRights!=null && objUserRights.contains(objSubMenu.getMenuId()))
                                                      { out.println("checked");}
                                                    %>
                                                   >
                                                   <%
                                                }
                                                else
                                                {
                                                   %>
                                                     <input type="checkbox" id="chk_sub_<%= objMenu.getMenuId() %>" name="chk" onclick="checkParent(document.frmSubmit.chk_sub_<%= objMenu.getMenuId() %>,'<%=objMenu.getMenuId()%>');" value="<%= objSubMenu.getMenuId() %>"
                                                   <%
                                                   if(objUserRights!=null && objUserRights.contains(objSubMenu.getMenuId()))
                                                      { out.println("checked");}
                                                    %>
                                                   >
                                                    <%=objSubMenu.getMenuName()%><BR>
                                                    <%
                                                }
                                        }
                                        %>

                                    </td>
                                </tr>
                               <%
                           
                        }
                        %>
                        <tr>
                            <td colspan="2">
                                <center>
                                    <label class="formBtn_1" id="lblSave">
                                        <input type="submit" name="save" id="save" value="Save" onclick="return chkValidate(this);">
                                    </label>
                                </center>
                            </td>
                        </tr>
                    </table>
                        
                        
                </form>
                </td>
                    </tr>
                </table>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabTender");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
</html>
