<%-- 
    Document   : ScheduleBankCreation
    Created on : Oct 23, 2010, 7:31:07 PM
    Author     : rishita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Schedule Bank Creation</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript">
            $(function() {
                 $('#cmbCountry').change(function() {
                    $.post("<%=request.getContextPath()%>/ComboServlet", {objectId:$('#cmbCountry').val(),funName:'stateCombo'},  function(j){
                        $('#cmbState').children().remove().end().append('<option selected value="0">-- Select --</option>') ;
                        $("select#cmbState").html(j);
                    });
                });
            });
        </script>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <jsp:include page="/resources/common/Top.jsp" ></jsp:include>
                <jsp:useBean id="scBankCreationSrBean" class="com.cptu.egp.eps.web.servicebean.ScBankCreationSrBean" scope="request"/>
                <jsp:setProperty name="scBankCreationSrBean" property="*" />
                
                <!--Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <div class="pageHead_1">Schedule Bank Creation</div>
                            <!--Page Content Start-->
                            <form name="frmBankSchedule" action="" method="POST">
                                <table class="formStyle_1" border="0" cellpadding="0" cellspacing="10">
                                    <td class="ff" width="200">Name of Bank<span>*</span></td>
                                    <td><input id="txtBankName" name="bankName" style="width: 200px;" type="text"/>
                                    <td><input class="formTxtBox_1" id="txtBankName" name="bankName" style="width: 200px;" type="text" /></td>
                                    <tr>
                                        <td class="ff" width="200"> Head Office Address<span>*</span></td>
                                        <td><textarea cols="20" rows="3" class="formTxtBox_1"  id="txtaAddress" name="officeAddress" style="width: 200px" ></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Country<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbCountry" name="country" style="width: 208px">
                                                <c:forEach var="countryList" items="${scBankCreationSrBean.countryList}">
                                                    <c:if test="${countryList.objectId == 163} ">
                                                        <option  value="${countryList.objectId}" selected="selected">${countryList.objectValue}</option>
                                                    </c:if>
                                                    <option  value="${countryList.objectId}">${countryList.objectValue}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Country<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbState" name="state" style="width: 208px">
                                                <c:forEach var="stateList" items="${scBankCreationSrBean.stateList}">
                                                    <c:if test="${stateList.objectValue == 'Dhaka'} ">
                                                        <option  value="${stateList.objectId}" selected="selected">${stateList.objectValue}</option>
                                                    </c:if>
                                                    <option  value="${stateList.objectId}">${stateList.objectValue}</option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                    <%--<tr>
                                        <td class="ff" width="200">District<span>*</span></td>
                                        <td><select class="formTxtBox_1" id="cmbDistrict" name="district" style="width: 200px">
                                                <option></option>
                                            </select>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td class="ff" width="200">City<span>*</span></td>
                                        <td><input class="formTxtBox_1" id="txtCity" name="city" style="width: 200px;" type="text" maxlength="50"/>
                                        </td>
                                    </tr
                                    <tr>
                                        <td class="ff" width="200">upJilla<span>*</span></td>
                                        <td><input id="txtUpJilla" class="formTxtBox_1" name="thanaUpzilla" style="width: 200px;" type="text" maxlength="50"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Post Code</td>
                                        <td><input id="txtPostCode" name="postCode" class="formTxtBox_1" style="width: 200px;" type="text" maxlength="10"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Phone Number</td>
                                        <td><input class="formTxtBox_1" id="txtPhoneNo" style="width: 200px;" name="phoneNo" type="text" maxlength="20"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Fax Number</td>
                                        <td><input class="formTxtBox_1" id="txtFaxNo" style="width: 200px;" name="faxNo" type="text" maxlength="20"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Website</td>
                                        <td><input class="formTxtBox_1" id="txtWebsite" style="width: 200px;" name="webSite" type="text" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="formBtn_1" align="center"><input id="btnSubmit" name="submit" value="Submit" type="submit"/></td>
                                        <td class="formBtn_1" align="center"><input id="btnUpdate" name="update" value="Update" type="submit"/></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="/resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
