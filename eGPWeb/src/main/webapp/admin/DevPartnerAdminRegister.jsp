<%--
Document   : DevPartnerAdminRegister
Created on : Oct 23, 2010, 6:13:00 PM
Author     : Sanjay,rishita
--%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.web.utility.AppMessage"%>
<%@page import="com.cptu.egp.eps.web.databean.AdminMasterDtBean"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Register Development Partner Admin</title>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script src="../resources/js/form/CommonValidation.js" type="text/javascript"></script>

        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                var validCheckMail = true;
                var validCheckMno = true;
                $("#frmDPAdmin").validate({
                    rules: {
                        organization: { required: true },
                        emailId: { required: true /*, email: true */},
                        password: {spacevalidate: true, requiredWithoutSpace: true , maxlength: 25 , minlength: 8, alphaForPassword : true },
                        confPassword: { required: true, equalTo: "#txtPassword"},
                        //, spacevalidate: true 
                        fullName: {spacevalidate: true, requiredWithoutSpace: true , alphaName:true, maxlength:100 },
                        //phoneNo: { required: true,PhoneFax: true , maxlength:20 , minlength: 6},
                        mobileNo: {required: true, number: true, minlength: 8, maxlength:8 }
                    },
                    messages: {
                        organization: { required: "<div class='reqF_1'>Please select Development Partner.</div>"},

                        emailId: { required: "<div class='reqF_1'>Please enter e-mail ID.</div>" /*,
                            email: "<div class='reqF_1'>Please enter valid e-mail ID.</div>"*/
                        },
                        password: { spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                                    requiredWithoutSpace: "<div class='reqF_1'>Please enter Password.</div>" ,
                            alphaForPassword: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>",
                            //spacevalidate: "<div class='reqF_1'>Space is not allowed.</div>",
                            maxlength: "<div class='reqF_1'>Maximum 25 characters are allowed.</div>",
                            minlength: "<div class='reqF_1'>Please enter atleast 8 character and password must contain both alphabets and number</div>"
                        },
                        confPassword: { required: "<div class='reqF_1'>Please re-type Password.</div>",
                            equalTo: "<div class='reqF_1'>Password does not match. Please try again.</div>"
                            //spacevalidate: "<div class='reqF_1'>Space is not allowed.</div>"
                        },
                         fullName: { spacevalidate: "<div class='reqF_1'>Only Space is not allowed</div>",
                                    requiredWithoutSpace: "<div class='reqF_1'>Please enter Full Name</div>",
                                    alphaName: "<div class='reqF_1'>Allows Characters (A to Z) & Special Characters (& , \' \" } { - . _) Only </div>",
                                    maxlength: "<div class='reqF_1'>Allows maximum 100 characters only</div>"},
                        
                        //phoneNo: { required: "<div class='reqF_1'>Please enter Phone No.</div>",
                          //  PhoneFax:"<div class='reqF_1'>Please enter Valid Phone Number.</div>",
                           // maxlength: "<div class='reqF_1'>Maximum 20 characters are allowed</div>",
                           // minlength: "<div class='reqF_1'>Please enter Minimum 6 Digits</div>"
                        //},

                         mobileNo: {
                            required: "<div class='reqF_1'>Please enter Mobile No.</div>",
                            number:"<div class='reqF_1'>Please enter digits (0-9) only</div>" ,
                            minlength:"<div class='reqF_1'>Minimum 8 digits are required</div>",
                            maxlength:"<div class='reqF_1'>Allows maximum 8 digits only</div>"
                            }
                         
                    },

                    errorPlacement:function(error ,element)
                    {
                        if(element.attr("name")=="phoneNo")
                        {
                            error.insertAfter("#fxno");
                        }
                        else if(element.attr("name")=="password")
                        {
                            error.insertAfter("#tipPassword");
                        }
                        else if(element.attr("name")=="mobileNo")
                        {
                            error.insertAfter("#mobMsg")
                        }
                        else
                        {
                            error.insertAfter(element);

                        }
                    }
                }
            );
            });
        </script>
        <script type="text/javascript">
            $(function() {
                $('#txtMail').blur(function() {
                    var varEmailId = trim(document.getElementById("txtMail").value);
                    if(document.getElementById("txtMail").value!=""){
                        $('#mailMsg').html("Checking for unique Mail Id...");
                        $.post("<%=request.getContextPath()%>/CommonServlet", {mailId:varEmailId,funName:'verifyMail'},  function(j){
                            if(j.toString().indexOf("OK", 0)!=-1){
                                $('#mailMsg').css("color","green");
                                validCheckMail = true;
                            }
                            else if(j.toString().indexOf("Mail", 0)!=-1){
                                $('#mailMsg').css("color","red");
                                validCheckMail = false;
                            }
                            $('#mailMsg').html(j);
                        });
                    }else{
                        $('#mailMsg').html("");
                    }
                });
                
            });

            $(function() {
                $('#txtMobileNo').blur(function() {
                    var varMobileNo = trim(document.getElementById("txtMobileNo").value);
                    if(document.getElementById("txtMobileNo").value!=""){
                        if((document.getElementById("txtMobileNo").value.length==8) && (document.getElementById("txtMobileNo").value.length==8) && (/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(document.getElementById("txtMobileNo").value))) // max min number
                        {
                           /* $('span.#mobMsg').html("Checking for unique Mobile No...");
                            $.post("<%=request.getContextPath()%>/CommonServlet", {mobileNo:varMobileNo,funName:'verifyMobileNo'},  function(j){

                                if(j.toString().indexOf("OK", 0)==0){
                                    $('span.#mobMsg').css("color","green");
                                    validCheckMno = true;
                                }
                                else if(j.toString().indexOf("Mobile", 0)!=-1){
                                    $('span.#mobMsg').css("color","red");
                                    validCheckMno = false;
                                }
                                $('span.#mobMsg').html(j);
                            }); */
                        }
                    }
                    else{
                        $('span.#mobMsg').html("");
                    }
                });
            });

            function validate(){
                if(document.getElementById("mailMsg")!=null && document.getElementById("mailMsg").innerHTML=="e-mail ID already exists, Please enter another e-mail ID."){
                    return false;
                }
                if(document.getElementById("mobMsg")!=null && document.getElementById("mobMsg").innerHTML=="Mobile No. already exists"){
                    return false;
                }
                //if(document.getElementById("errMsg")!=null && document.getElementById("errMsg").innerHTML=="National-ID already exists"){
                //    return false;
                //}
                if(validCheckMail && validCheckMno){

                }else{
                    return false;
                }
            }
            function checkctrlkey(){
                   jAlert("Copy Paste not allowed.","Development Partner Admin", function(RetVal) {
                    });
                   return false;
            }

            function checkrightclick(e){
                $(e).bind("contextmenu",function(e){
                    return false;
                });
            }
        </script>

    </head>
    <jsp:useBean id="scBankPartnerAdminSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.ScBankPartnerAdminSrBean"/>
    <jsp:useBean id="partnerAdminMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.PartnerAdminMasterDtBean"/>
    <jsp:useBean id="loginMasterDtBean" scope="request" class="com.cptu.egp.eps.web.databean.LoginMasterDtBean"/>
    <jsp:setProperty name="partnerAdminMasterDtBean" property="*"/>
    <jsp:setProperty name="loginMasterDtBean" property="*"/>

    <%@page import="com.cptu.egp.eps.service.serviceinterface.CommonService" %>
    <%@page import="com.cptu.egp.eps.web.utility.AppContext" %>

    <body>
        <%
        if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
            scBankPartnerAdminSrBean.setLogUserId(session.getAttribute("userId").toString());
        }
                        scBankPartnerAdminSrBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                    if ("Submit".equals(request.getParameter("add"))) {
                        String msg = "";
                        int strM = -1; // Default
                        String mailId = loginMasterDtBean.getEmailId();
                        String mobileNo = partnerAdminMasterDtBean.getMobileNo();
                        if (!mailId.matches("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9\\-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")){
                            strM = 1; //Not Valid EmailId
                        }
                        if (strM == -1) {
                            CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                            msg = commonService.verifyMail(mailId.trim());
                            if (msg.length() > 2) {
                                strM = 2; //EmailId Already Exist
                            }
                        }
                        if (strM == -1) {
                            //CommonService commonService = (CommonService) AppContext.getSpringBean("CommonService");
                            //msg = commonService.verifyMobileNo(mobileNo.trim());
                            //if (msg.length() > 2) {
                            //    strM = 3; //Mobile No Already Exist
                            // }
                        }
                        if (strM == -1) {
                            scBankPartnerAdminSrBean.setBankId(Integer.parseInt(request.getParameter("organization")));

                            if(partnerAdminMasterDtBean.getMobileNo() == null){
                                partnerAdminMasterDtBean.setMobileNo("");
                            }
                            if(partnerAdminMasterDtBean.getNationalId() == null){
                                partnerAdminMasterDtBean.setNationalId("");
                            }
                            if(partnerAdminMasterDtBean.getIsMakerChecker() == null){
                                partnerAdminMasterDtBean.setIsMakerChecker("");
                            }

                            int userId = scBankPartnerAdminSrBean.scBankDevPartnerAdminReg(partnerAdminMasterDtBean, loginMasterDtBean, "dev", request.getParameter("password"));
                            if (userId > 0) {
                                response.sendRedirect("ViewSbDevPartAdmin.jsp?userId=" + userId + "&partnerType=Development&userTypeId=6&msg=success&from=created");
                            }else{
                                response.sendRedirect("DevPartnerAdminRegister.jsp?msg=creationFail");
                            }
                        } else {
                            response.sendRedirect("DevPartnerAdminRegister.jsp?strM=" + strM);
                        }
                    } else {
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%
                                                StringBuilder userType = new StringBuilder();
                                                if (request.getParameter("hdnUserType") != null) {
                                                    if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                                                        userType.append(request.getParameter("hdnUserType"));
                                                    } else {
                                                        userType.append("org");
                                                    }
                                                } else {
                                                    userType.append("org");
                                                }
                        %>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?userType=<%=userType%>" ></jsp:include>
                        <td class="contentArea">
                            <%
                                                    int strM = -1;
                                                    String msg = "";
                                                    if (request.getParameter("strM") != null) {
                                                        try {
                                                            strM = Integer.parseInt(request.getParameter("strM"));
                                                        } catch (Exception ex) {
                                                            strM = 4;
                                                        }
                                                        switch (strM) {
                                                            case (1):
                                                                msg = "Please enter valid e-mail ID.";
                                                                break;
                                                            case (2):
                                                                msg = "e-mail ID already exists, Please enter another e-mail ID.";
                                                                break;
                                                            case (3):
                                                                msg = "Mobile No already exists, Please enter another e-mail ID.";
                                                                break;
                                                        }
                            %>
                            <div class="responseMsg errorMsg" style="margin-left:7px; margin-right: 2px; margin-top: 15px;">
                                <%= msg%>
                            </div>
                            <%
                                                        msg = null;
                                                    }
                            %>
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td class="contentArea"><div class="pageHead_1">Create Development Partner Admin</div>
                                        <% if(request.getParameter("msg") != null && "creationFail".equals(request.getParameter("msg"))){ %>
                                        <div class='responseMsg errorMsg'><%=appMessage.devPartnerAdminReg %></div>
                                        <% } %>
                                        <form name="frmDPAdmin" id="frmDPAdmin" method="post">
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td style="font-style: italic" class="ff t-align-left" colspan="2">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Select Development Partner : <span>*</span></td>
                                                    <td>
                                                        <select style="width:400px;" class="formTxtBox_1" name="organization" id="cmbOrganization">
                                                            <option  value="">-- Select Development Partner --</option>
                                                            <c:forEach var="cList" items="${scBankPartnerAdminSrBean.dpList}">
                                                                <option  value="${cList.objectId}">${cList.objectValue}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">e-mail ID : <span>*</span></td>
                                                    <td>
                                                        <input style="width:195px;" class="formTxtBox_1" type="text" id="txtMail" name="emailId" maxlength="100"/>
                                                        <div id="mailMsg" style="color: red; font-weight: bold"></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Password : <span>*</span></td>
                                                    <td><input style="width:195px;" class="formTxtBox_1" type="password" id="txtPassword" name="password" maxlength="25" autocomplete="off" /><br/><span id="tipPassword" style="color: grey;"> (Passwords must have minimum eight (8) characters in length and must contain alphanumeric characters. 
                                                <br/>Special characters may be added) </span>
                                                    </td>

                                                </tr>

                                                <tr>
                                                    <td class="ff">Confirm Password : <span>*</span></td>
                                                    <td><input style="width:195px;" class="formTxtBox_1" type="password" id="txtConfPassword" name="confPassword" value="" onpaste="return checkctrlkey();" autocomplete="off" />
                                                    </td>
                                                </tr>
                                                <tr>

                                                    <td class="ff">Full Name : <span>*</span></td>

                                                    <td><input style="width:195px;" class="formTxtBox_1" type="text" id="txtFullName" name="fullName" maxlength="101"/>
                                                    </td>
                                                </tr>
                                                <tr style="display: none">
                                                    <td class="ff">CID : <span>*</span></td>

                                                    <td><input style="width:195px;" class="formTxtBox_1" type="text" id="txtNationalId" name="nationalId" maxlength="25" value=" "/>
                                                        <span id="errMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                    </td>
                                                </tr>
                                                <!--tr>
                                                    <td class="ff">Phone No : <span>*</span></td>
                                                    <td><input style="width:195px;" class="formTxtBox_1" type="text" name="phoneNo" id="txtPhoneNo" maxlength="20"/>
                                                        <span id="fxno" name="fxno" style="color: grey;"> STD-Phone No. i.e. 02-9144252</span>
                                                    </td>

                                                </tr-->
                                                <tr>
                                                    <td class="ff">Mobile No. : <span>*</span></td>
                                                    <td><input style="width:195px;" class="formTxtBox_1" type="text" name="mobileNo" id="txtMobileNo" maxlength="16"/><span id="mNo" style="color: grey;"> (Mobile No. format should be e.g 12345678)</span>
                                                        <span id="mobMsg" style="color: red; font-weight: bold">&nbsp;</span>
                                                    </td>

                                                </tr>
                                                <tr><td></td>
                                                    <td  align="left">
                                                        <label class="formBtn_1">
                                                            <input type="submit" name="add" id="btnAdd" value="Submit" onClick="return validate();"/>
                                                        </label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
            <script>
                var obj = document.getElementById('lblDevPartnerAdminCreation');
                if(obj != null){
                    if(obj.innerHTML == 'Create Development Partner Admin'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
    <%
                }
    %>
</html>
