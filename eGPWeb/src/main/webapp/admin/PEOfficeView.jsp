<%-- 
    Document   : PEOfficeView
    Created on : Nov 12, 2010, 12:24:07 PM
    Author     : rishita
--%>


<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.model.table.TblOfficeMaster" %>
<%@page import="com.cptu.egp.eps.model.table.TblDepartmentMaster" %>
<%@page import="com.cptu.egp.eps.web.utility.SelectItem"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PA Office</title>
    </head>
    <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
    %>
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    <jsp:useBean id="peOfficeCreationSrBean" scope="request" class="com.cptu.egp.eps.web.servicebean.PEOfficeCreationSrBean"/>
    <%
                if (session.getAttribute("userId") != null && !"".equalsIgnoreCase(session.getAttribute("userId").toString())) {
                    peOfficeCreationSrBean.setLogUserId(session.getAttribute("userId").toString());
                }
    %>
    <body>
        <%
                    int officeid = 0;
                    if (!request.getParameter("officeid").equals("")) {
                        officeid = Integer.parseInt(request.getParameter("officeid"));
                    }
                    String departmentType = "";
                    if (!request.getParameter("departmentType").equals("")) {
                        departmentType = request.getParameter("departmentType");
                    }
                    
                    if(request.getParameter("action") != null && request.getParameter("action").equalsIgnoreCase("view") && request.getParameter("ok") == null)
                     {
                           // Coad added by Dipal for Audit Trail Log.
                            AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());

                            int auditId=Integer.parseInt(request.getParameter("officeid"));
                            String auditAction="View PE Office for "+departmentType;
                            String idType="officeId";
                            String moduleName=EgpModule.Manage_Users.getName(); 
                            String remarks="User Id: "+session.getAttribute("userId")+" has viewed PE Office";
                            MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                            makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
                    }

                    StringBuilder userType = new StringBuilder();
                    if (request.getParameter("hdnUserType") != null) {
                        if (!"".equalsIgnoreCase(request.getParameter("hdnUserType"))) {
                            userType.append(request.getParameter("hdnUserType"));
                        } else {
                            userType.append("org");
                        }
                    } else {
                        userType.append("org");
                    }

                    boolean doRender = true;
                    if ("Edit".equals(request.getParameter("edit"))) {
                        doRender = false;
                        response.sendRedirect("EditPEOffice.jsp?officeid=" + officeid + "&deptType=" + departmentType);
                    } else if ("Ok".equals(request.getParameter("ok"))) {
                        doRender = false;
                        response.sendRedirect("EditPEOfficeGrid.jsp?deptType=" + departmentType);
                    }

                    if(doRender){
        %>
        <div class="mainDiv">
            <div class="fixDiv">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <!--Middle Content Table Start-->

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp?hdnUserType=<%=userType%>" ></jsp:include>
                        <td class="contentArea">
                            <%
                                        String result = request.getParameter("msg");
                                        if (result.equals("success")) {%>
                            <div class="responseMsg successMsg">PA Office updated successfully</div>
                            <% } else if (result.equals("create")) {
                                %><div class="responseMsg successMsg">PA Office created successfully</div>
                            <%                                                                        }
                            %>	
                            <div class="pageHead_1 l_space">PA Office Details</div>
                            <form id="frmEditPEOffice" name="frmEditPEOffice" action="" method="post">

                                <table cellspacing="10" class="formStyle_1" border="0" cellpadding="0">
                                    <%
                                                //if ("View all".equals(request.getParameter("viewall"))) {
                                                  //  response.sendRedirect("EditPEOfficeGrid.jsp?deptType=" + departmentType);
                                                //}
                                          for (TblOfficeMaster tom : peOfficeCreationSrBean.getFindUser(officeid)) {
                                    %>
                                    <tr>
                                        <td class="ff" width="200"><%if(departmentType.equalsIgnoreCase("Organization")){out.print("Division");}else if(departmentType.equalsIgnoreCase("Division")){out.print("Department");}else {out.print(departmentType);}%> Name : </td>
                                        <td><label><%= peOfficeCreationSrBean.getDefaultDept(tom.getDepartmentId()).get(0).getDepartmentName()%></label>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Office Name :</td>
                                        <td>
                                            <label><%= tom.getOfficeName()%></label>
                                        </td>
                                    </tr>

<!--                                    <tr>
                                        <td class="ff" width="200">Office Name in Dzongkha :</td>
                                        <%     String str = "";
                                               if (tom.getOfficeNameInBangla() == null) {
                                               } else {
                                                   str = new String(tom.getOfficeNameInBangla(), "UTF-8");
                                        %>
                                        <td><label><%=str%></label></td>
                                        <% }%>
                                    </tr>-->
                                    <%if (departmentType.equals("Organization") && peOfficeCreationSrBean.getDefaultDept(tom.getDepartmentId()).get(0).getOrganizationType().equals("no")){ %>
                                    <tr>
                                        <td class="ff" width="200">Office Type :</td>
                                       <% if (!(tom.getOfficeType().equals(""))) {
                                                if (tom.getOfficeType() != null && !tom.getOfficeType().equals("Others")) {%>
                                                    <td><label><%=tom.getOfficeType()%> Level</label></td>
                                               <%}else{ %>
                                                    <td><label><%=tom.getOfficeType()%></label></td>
                                                <%}
                                           }
                                        %>
                                    </tr> <%}%>
                                    <tr>
                                        <td class="ff" width="200">PA Code :</td>
                                        <% if (!(tom.getPeCode().equals(""))) {
                                                if (tom.getPeCode() != null) {%>
                                        <td><label><%=tom.getPeCode()%></label></td>
                                        <%
                                                }
                                           }
                                        %>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Address :</td>
                                        <td>
                                            <label><%= tom.getAddress()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Country :</td>
                                        <td><label><%= tom.getTblCountryMaster().getCountryName()%></label>
                                        </td>
                                    <tr>
                                        <td class="ff" width="200">Dzongkhag :</td>
                                        <td><label><%= tom.getTblStateMaster().getStateName()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Dungkhag :</td>
                                        <td><label><%= tom.getCity()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Gewog :</td>
                                        <td><label><%= tom.getUpjilla()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Post Code :</td>
                                        <td><label><%= tom.getPostCode()%></label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="ff" width="200">Phone No. :</td>
                                        <td>
                                            <label><%= tom.getPhoneNo()%></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff" width="200">Fax No. :</td>
                                        <% if (!(tom.getFaxNo().equals(""))) {
                                                                                                if (tom.getFaxNo() != null) {%>
                                        <td><label><%=tom.getFaxNo()%></label></td>
                                        <% }
                                                                                            }%>
                                    </tr>
                                    <%
                                                }
            %>
                                                <tr><td>&nbsp;</td>
                                                    <td align="left"><label class="formBtn_1"><input id="ok" name="ok" value="Ok" type="submit"/></label>&nbsp;<label class="formBtn_1" ><input type="submit" value="Edit" name="edit"/> </label></td>
                                    </tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                </table>
                <center><jsp:include page="../resources/common/Bottom.jsp" ></jsp:include></center>
            </div>
        </div>
        <%
        }
                    peOfficeCreationSrBean.setAuditTrail(null);
        %>
            <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
