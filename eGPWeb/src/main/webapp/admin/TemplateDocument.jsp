<%--
    Document   : Index
    Created on : Nov 19, 2010, 3:15:00 PM
    Author     : Yagnesh
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.cptu.egp.eps.model.table.TblTemplateSectionDocs, com.cptu.egp.eps.web.utility.CheckExtension" %>
<%@page import="com.cptu.egp.eps.model.table.TblConfigurationMaster" %>
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />
<jsp:useBean id="checkExtension" class="com.cptu.egp.eps.web.utility.CheckExtension" />

<html xmlns="http://www.w3.org/1999/xhtml"> 
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Upload Document</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>

<script type="text/javascript">

            $(document).ready(function() {
                $("#frmUploadDoc").validate({
                    rules: {
                        uploadDocFile: {required: true},
                        documentBrief: {required: true,maxlength:50}
                    },
                    messages: {
                        uploadDocFile: { required: "<div class='reqF_1'>Please Select Document.</div>"},
                        documentBrief: { required: "<div class='reqF_1'>Please Enter Description.</div>",
                                        maxlength: "<div class='reqF_1'>Maximum 50 characters are allowed.</div>"}
                    }
                });
            });
            
            $(function() {
                $('#frmUploadDoc').submit(function() {
                    if($('#frmUploadDoc').valid()){
                        $('.err').remove();
                        var count = 0;
                        var browserName=""
                        var maxSize = parseInt($('#fileSize').val())*1024*1024;
                        var actSize = 0;
                        var fileName = "";
                        jQuery.each(jQuery.browser, function(i, val) {
                             browserName+=i;
                        });
                        $(":input[type='file']").each(function(){
                            if(browserName.indexOf("mozilla", 0)!=-1){
                                actSize = this.files[0].size;
                                fileName = this.files[0].name;
                            }else{
                                var file = this;
                                var myFSO = new ActiveXObject("Scripting.FileSystemObject");
                                var filepath = file.value;
                                var thefile = myFSO.getFile(filepath);
                                actSize = thefile.size;
                                fileName = thefile.name;
                            }
                            if(parseInt(actSize)==0){
                                $(this).parent().append("<div class='err' style='color:red;'>File with 0 KB size is not allowed</div>");
                                count++;
                            }
                            if(fileName.indexOf("&", "0")!=-1 || fileName.indexOf("%", "0")!=-1){
                                $(this).parent().append("<div class='err' style='color:red;'>File name should not contain special characters(%,&)</div>");
                                count++;
                            }
                            if(parseInt(parseInt(maxSize) - parseInt(actSize)) < 0){
                                $(this).parent().append("<div class='err' style='color:red;'>Maximum file size of single file should not exceed "+$('#fileSize').val()+" MB. </div>");
                                count++;
                            }
                        });
                        if(count==0){
                            $('#btnUpload').attr("disabled", "disabled");
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return false;
                    }
                });
            });
        </script>

        <%--<script type="text/javascript" src="../resources/js/upload.js"> </script>
        <script type="text/javascript" src="../dwr/interface/UploadMonitor.js"> </script>
        <script type="text/javascript" src="../dwr/engine.js"> </script>
        <script type="text/javascript" src="../dwr/util.js"> </script>
        <style type="text/css">
            body { font: 11px Lucida Grande, Verdana, Arial, Helvetica, sans serif; }
            #progressBar { padding-top: 5px; }
            #progressBarBox { width: 350px; height: 20px; border: 1px inset; background: #eee;}
            #progressBarBoxContent { width: 0; height: 20px; border-right: 1px solid #444; background: #9ACB34; }
        </style>--%>
        
    </head>
    <body>
        <%
            short templateId = 0;
            int sectionId = 0;
            if (request.getParameter("templateId") != null) {
                templateId = Short.parseShort(request.getParameter("templateId"));
            }
            if (request.getParameter("sectionId") != null) {
                sectionId = Integer.parseInt(request.getParameter("sectionId"));
            }
            
            CheckExtension ext = new CheckExtension();
            TblConfigurationMaster configurationMaster = ext.getConfigurationMaster("egpadmin");
            String strAllowExt = configurationMaster.getAllowedExtension();
            String[] allowExt = null;
            if(strAllowExt!=null){
                allowExt = strAllowExt.split(",");
            }
            if(allowExt.length > 0){
                for (int i = 0; i < allowExt.length; i++) {
        %>
                    <script>
                        extension_allowed.push('<%= allowExt[i] %>');
                    </script>
        <%
                }
            }
                String logUserId = "0";
                if(session.getAttribute("userId")!=null){
                                    logUserId =session.getAttribute("userId").toString();
                                }
                defineSTDInDtlSrBean.setLogUserId(logUserId);
        %>

         <div class="dashboard_div">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <div class="contentArea_1">
                <!--Middle Content Table Start-->                
                            <!--Page Content Start-->
                            <div class="pageHead_1">
                                Upload Document
                                <span style="float: right; text-align: right;">
                                    <a href="DefineSTDInDtl.jsp?templateId=<%= templateId %>" title="SBD Dashboard" class="action-button-goback">Go back to SBD Dashboard</a>
                                </span>
                            </div>
                            <%
                            if(request.getParameter("fq")!=null){
                            %>
                            <div class="responseMsg errorMsg" style="margin-top: 10px;"><%=request.getParameter("fq")%></div>
                            <%
                            }if(request.getParameter("fs")!=null){
                            %>
                            <div class="responseMsg errorMsg" style="margin-top: 10px;">
                                Max File Size <%=request.getParameter("fs")%>MB and FileType <%=request.getParameter("ft")%> allowed.
                            </div>
                            <%
                            }
                            %>
                            <% if(request.getParameter("msg")!=null && "success".equalsIgnoreCase(request.getParameter("msg"))){ %>
                                <div class="responseMsg successMsg" style="margin-top: 10px;">Document Deleted Successfully</div>
                            <% } %>
                            <% if(request.getParameter("msg")!=null && "0".equalsIgnoreCase(request.getParameter("msg"))){ %>
                            <div class="responseMsg errorMsg" style="margin-top: 10px;">System can not find the document</div>
                            <% } %>
                            <form  id="frmUploadDoc" method="post" action="<%=request.getContextPath()%>/TemplateFileUploadSrvt" enctype="multipart/form-data" name="frmUploadDoc">
                                <input type="hidden" name="templateId" value="<%= templateId %>" />
                                <input type="hidden" name="sectionId" value="<%= sectionId %>" />
                                <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                                    <tr>
                                        <td style="font-style: italic" colspan="2" class="" align="right">Fields marked with (<span class="mandatory">*</span>) are mandatory.</td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Document   : <span class="mandatory">*</span></td>
                                        <td>
                                            <input name="uploadDocFile" id="uploadDocFile" type="file" class="formTxtBox_1" style="width:450px; background:none;"/>
                                            <div id="dvUploadFileErMsg" class='reqF_1'></div>
                                            <div id="progressBar" style="display: none;">
                                                <div id="theMeter">
                                                    <div id="progressBarText"></div>

                                                    <div id="progressBarBox">
                                                        <div id="progressBarBoxContent"></div>
                                                    </div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td class="ff">Description : <span>*</span></td>
                                        <td>
                                            <input name="documentBrief" type="text" class="formTxtBox_1" maxlength="100" id="txtDescription" style="width:200px;" />
                                            <div id="dvDescpErMsg" class='reqF_1'></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>
                                            
                                            <label class="formBtn_1">
                                                <input type="submit" name="upload" id="btnUpload" value="Upload"/>
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                                 <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th width="100%"  class="t-align-left">Instructions</th>
                        </tr>
                        <tr>
                            <%TblConfigurationMaster tblConfigurationMaster = checkExtension.getConfigurationMaster("egpadmin");%>
                            <td class="t-align-left">Any Number of files can be uploaded.  Maximum Size of a Single File should not Exceed <%=tblConfigurationMaster.getFileSize()%>MB.
                                <input type="hidden" value="<%=tblConfigurationMaster.getFileSize()%>" id="fileSize"/></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">Acceptable File Types <span class="mandatory"><%=tblConfigurationMaster.getAllowedExtension().replace(",", ",  ") %></span></td>
                        </tr>
                        <tr>
                            <td class="t-align-left">A file path may contain any below given special characters: <span class="mandatory">(Space, -, _, \)</span></td>
                        </tr>
                    </table>
                                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                                                <tr>
                                                    <th style="width: 4%">Sl. <br/> No.</th>
                                                    <th>File Name</th>
                                                    <th>File Description</th>
                                                    <th>File Size In KB</th>
                                                    <th>Action</th>
                                                </tr>
                                                <%
                                                    java.util.List<com.cptu.egp.eps.model.table.TblTemplateSectionDocs> docs = defineSTDInDtlSrBean.getTemplateSectionDocs(templateId, (short) sectionId);
                                                    if(docs != null){
                                                        if(docs.size() > 0){
                                                            short j = 0;
                                                            for(TblTemplateSectionDocs ttsd : docs){
                                                                j++;
                                                %>
                                                    <tr>
                                                        <td style="text-align:center;"><%= j %></td>
                                                        <td style="text-align:left;"><%= ttsd.getDocName()  %></td>
                                                        <td style="text-align:left;"><%= ttsd.getDescription()  %></td>
                                                        <td style="text-align:center;"><%= Integer.parseInt(ttsd.getDocSize())/1024%></td>
                                                        <td style="text-align:center;">
                                                            <a href="../TemplateFileUploadSrvt?action=removeDoc&templateId=<%= templateId %>&sectionId=<%= sectionId %>&sDocId=<%= ttsd.getSectionDocId() %>&docName=<%= ttsd.getDocName() %>"><img src="../resources/images/Dashboard/Delete.png" alt="Remove" width="16" height="16" /></a> |
                                                            <a href="../TemplateFileUploadSrvt?docName=<%= ttsd.getDocName() %>&docSize=<%= ttsd.getDocSize() %>&sectionId=<%= sectionId %>&action=downloadDoc" title="Download"><img src="../resources/images/Dashboard/downloadIcn.png" alt="Download" /></a>
                                                        </td>
                                                    </tr>

                                                <%
                                                               ttsd = null;
                                                            }
                                                        }else{
                                                %>
                                                    <tr>
                                                        <td colspan="5">
                                                            No Records Found
                                                        </td>
                                                    </tr>
                                                <%
                                                        }
                                                        docs = null;
                                                    }else{
                                                %>
                                                <tr>
                                                    <td colspan="5">
                                                        No Records Found
                                                    </td>
                                                </tr>
                                                <%
                                                    }
                                                %>
                                            </table>
                            </form>
                            <!--Page Content End-->
                </div>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
            <script>
                var headSel_Obj = document.getElementById("headTabSTD");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<%
    if(defineSTDInDtlSrBean!=null){
        defineSTDInDtlSrBean = null;
    }
%>