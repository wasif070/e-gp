<%-- 
    Document   : MiscPaymentReport
    Created on : Mar 21, 2012, 11:51:11 AM
    Author     : shreyansh.shah
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%
    response.setHeader("Expires", "-1");
    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
    response.setHeader("Pragma", "no-cache");
    boolean isPDF = false;
     if(request.getParameter("isPDF")!=null){
            isPDF = true;
    }
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Miscellaneous Payment Report</title>
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
<link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
<script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
<script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <!--<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>-->
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
        <script src="../resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript">
            /* Call Print function */
             
            $(document).ready(function() {
                $("#print").click(function() {
                    printElem({ leaveOpen: true, printMode: 'popup' });
                });

            });
            function printElem(options){
                $('#titleDiv').show();
                $('#print_area').printElement(options);
                $('#titleDiv').hide();
            }
        </script>
<script language="javascript">
    /* Call Calendar function */
    function GetCal(txtname,controlname)
    {
        new Calendar({
            inputField: txtname,
            trigger: controlname,
            showTime: true,
            dateFormat:"%d/%m/%Y",
            onSelect: function() {
                var date = Calendar.intToDate(this.selection.get());
                LEFT_CAL.args.min = date;
                LEFT_CAL.redraw();
                this.hide();
            }
        });

        var LEFT_CAL = Calendar.setup({
            weekNumbers: false
        })
    }
</script>
<script type="text/javascript">
    
    function showHide()
	{
		if(document.getElementById('collExp') != null && document.getElementById('collExp').innerHTML =='+ Advanced Search'){
			document.getElementById('tblSearchBox').style.display = 'table';
			document.getElementById('collExp').innerHTML = '- Advanced Search';
		}else{
			document.getElementById('tblSearchBox').style.display = 'none';
			document.getElementById('collExp').innerHTML = '+ Advanced Search';
		}
	}
	function hide()
	{
		document.getElementById('tblSearchBox').style.display = 'none';
		document.getElementById('collExp').innerHTML = '+ Advanced Search';
	}
    /* check if pageNO is disable or not */
    function chkdisble(pageNo){
        //alert(pageNo);
        $('#dispPage').val(Number(pageNo));
        if(parseInt($('#pageNo').val(), 10) != 1){
            $('#btnFirst').removeAttr("disabled");
            $('#btnFirst').css('color', '#333');
        }

        if(parseInt($('#pageNo').val(), 10) == 1){
            $('#btnFirst').attr("disabled", "true");
            $('#btnFirst').css('color', 'gray');
        }


        if(parseInt($('#pageNo').val(), 10) == 1){
            $('#btnPrevious').attr("disabled", "true")
            $('#btnPrevious').css('color', 'gray');
        }

        if(parseInt($('#pageNo').val(), 10) > 1){
            $('#btnPrevious').removeAttr("disabled");
            $('#btnPrevious').css('color', '#333');
        }

        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
            $('#btnLast').attr("disabled", "true");
            $('#btnLast').css('color', 'gray');
        }

        else{
            $('#btnLast').removeAttr("disabled");
            $('#btnLast').css('color', '#333');
        }

        if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
            $('#btnNext').attr("disabled", "true")
            $('#btnNext').css('color', 'gray');
        }
        else{
            $('#btnNext').removeAttr("disabled");
            $('#btnNext').css('color', '#333');
        }
    }
</script>
<script type="text/javascript">
    /*  load Grid for the User Registration Report */
    function loadTable()
    {
        if($("#keyWord").val() == undefined)
            $("#keyWord").val('');
                      $.post("<%=request.getContextPath()%>/MiscPaymentReport", {sortemailId : $('#idemailSearchOption').val(),paymentDate : $('#txtpaymentDate').val(),payeeName : $('#idpayeeName').val(),payeeNameSearch : $('#idpayeeNameSearch').val(),amountSearch : $('#idamountSearch').val(),amount:$('#txtAmount').val(),emailId : $('#txtemail').val(),action:'Search',size: $("#size").val(),pageNo: $("#pageNo").val(),isPDF:'<%=isPDF%>'},  function(j){
            $('#resultTable').find("tr:gt(0)").remove();
            $('#resultTable tr:last').after(j);
              sortTable();
            if($('#noRecordFound').attr('value') == "noRecordFound"){
                $('#pagination').hide();
            }else{
                $('#pagination').show();
            }
            chkdisble($("#pageNo").val());
            if($("#totalPages").val() == 1){
                $('#btnNext').attr("disabled", "true");
                $('#btnLast').attr("disabled", "true");
            }else{
                $('#btnNext').removeAttr("disabled");
                $('#btnLast').removeAttr("disabled");
            }
            $("#pageNoTot").html($("#pageNo").val());
            $("#pageTot").html($("#totalPages").val());
            $('#resultDiv').show();
        });
    }
</script>
<script type="text/javascript">
    /*  Handle First click event */
    $(function() {
        $('#btnFirst').click(function() {
            var totalPages=parseInt($('#totalPages').val(),10);
            var pageNo =$('#pageNo').val();
            if(totalPages>0 && pageNo!="1")
            {
                $('#pageNo').val("1");
                loadTable();
                $('#dispPage').val("1");
            }
        });
    });
</script>
<script type="text/javascript">
    /*  Handle Last Button click event */
    $(function() {
        $('#btnLast').click(function() {
            var totalPages=parseInt($('#totalPages').val(),10);
            if(totalPages>0){
                $('#pageNo').val(totalPages);
                loadTable();
                $('#dispPage').val(totalPages);
            }
        });
    });
</script>
<script type="text/javascript">
    /*  Handle Next Button click event */
    $(function() {
        $('#btnNext').click(function() {
            var pageNo=parseInt($('#pageNo').val(),10);
            var totalPages=parseInt($('#totalPages').val(),10);

            if(pageNo < totalPages) {
                $('#pageNo').val(Number(pageNo)+1);
                loadTable();
                $('#dispPage').val(Number(pageNo)+1);

                $('#dispPage').val(Number(pageNo)+1);
            }
        });
    });

</script>
<script type="text/javascript">
    /*  Handle Previous click event */
    $(function() {
        $('#btnPrevious').click(function() {
            var pageNo=$('#pageNo').val();
            if(parseInt(pageNo, 10) > 1)
            {
                $('#pageNo').val(Number(pageNo) - 1);
                loadTable();
                $('#dispPage').val(Number(pageNo) - 1);
            }
        });
    });
</script>
<script type="text/javascript">
    /*  Handle Go To Button event */
    $(function() {
        $('#btnGoto').click(function() {
            var pageNo=parseInt($('#dispPage').val(),10);
            var totalPages=parseInt($('#totalPages').val(),10);
            if(pageNo > 0)
            {
                if(pageNo <= totalPages) {
                    $('#pageNo').val(Number(pageNo));
                    loadTable();
                    $('#dispPage').val(Number(pageNo));
                }
            }
        });
    });
    <%--function restVal(){
        $('#idregStatus').val('');
        $('#txtpaymentDateFrom').val('');
        $('#txtemail').val('');
        $('#txtpaymentDateTo').val('');
        $('#txtCompanyName').val('');
        $('#cmbDescmbBankignation').attr('disabled',true);
        loadTable();
    }--%>
</script>
<script language="javascript">
    function checkExpiry(stausValue){
        if(stausValue=="Expired"){
            document.getElementById("paymentRow").style.display = "none";
        }else{
            document.getElementById("paymentRow").style.display = "table-row";
        }
    }

</script>
<%
                int userid = 0;
               
                HttpSession hs = request.getSession();
                if (hs.getAttribute("userId") != null) {
                    userid = Integer.parseInt(hs.getAttribute("userId").toString());
                }
                
                CommonSearchDataMoreService csdms = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> list = csdms.geteGPData("getMainPaymentBanksList", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
    %>
</head>
<body onload="loadTable();hide();">
<div class="dashboard_div">
  <!--Dashboard Header Start-->
  <%@include file="../resources/common/AfterLoginTop.jsp" %>
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="contentArea_1">
  <div class="pageHead_1">Miscellaneous Payment Report</div>
  <div class="formBg_1 t_space">
      <form name="frmpayment" id="frmpayment" action="MiscPaymentReport.jsp?fromWhere=MyAccount" method="post">
          <div class="ExpColl">&nbsp;&nbsp;<a href="javascript:void(0);" id="collExp" onclick="showHide();">+ Advanced Search</a></div>
          <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%" id="tblSearchBox">
    <tr>
       <td class="ff">E-Mail ID :</td>
      <td>
          <select name="txtemailSearchOption" class="formTxtBox_1" id="idemailSearchOption" style="width:80px;" >
              <option value="=" selected>Equals</option>
              <option value="Like">Contains</option>
          </select>
          <input name="txtemail" type="text" class="formTxtBox_1" id="txtemail" style="width:200px;" />
      </td>
      <td class="ff">Date of Payment :</td>
          <td>
             <input name="txtpaymentDate" type="text" class="formTxtBox_1" id="txtpaymentDate" style="width:100px;" readonly="true" onClick="GetCal('txtpaymentDate','imgCal1');" />
              &nbsp;
              <a href="javascript:void(0);" onclick ="GetCal('txtpaymentDate','imgCal1');" title="Calender">
                  <img id="imgCal1" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"/>
              </a>
          </td>

      </tr>
      <tr style="display:table-row;" id="paymentRow">
      <td class="ff">Payee Name :</td>
      <td>
          <select name="txtpayeeNameSearch" class="formTxtBox_1" id="idpayeeNameSearch" style="width:80px;" >
              <option value="Like">Contains</option>
          </select>
          <input name="txtpayeeName" type="text" class="formTxtBox_1" id="idpayeeName" style="width:200px;" />
      </td>
         <td class="ff">Amount :</td>
          <td>
          <select name="txtamountSearch" class="formTxtBox_1" id="idamountSearch" style="width:80px;" >
              <option value=">" selected>></option>
              <option value="<" selected><</option>
              <option value="=" selected>Equals</option>
          </select>
          <input name="txtAmount" type="text" class="formTxtBox_1" id="txtAmount" style="width:200px;" />
      </td>
       </tr>
      
    <tr>
      <td colspan="3" class="t-align-center">
          <label class="formBtn_1">
              <input type="button" name="button" id="button" value="Generate" onclick="loadTable()" />
          </label>
          <label class="formBtn_1">
              <input type="submit" name="btnReset" id="btnReset" value="Reset" />
          </label>
      </td>
      <td class="t-align-right">
        <%
           String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
           String folderName = pdfConstant.MISCPMTRPT;
           String genId = cdate + "_" + userid;
        %>
        <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('8');">Save as PDF</a></span>
       <!--<a class="action-button-savepdf" href="<%=request.getContextPath()%>/GeneratePdf?reqURL=&reqQuery=&folderName=<%=folderName%>&id=<%=genId%>" >Save As PDF </a>-->
                                           &nbsp;
       <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
      </td>
    </tr>
  </table>
  </form>
  </div>
<div id="resultDiv" style="display: block;">
    <div  id="print_area">
<div id='titleDiv' class="pageHead_1" style="display: none;">User Registration and Payment Report</div>
<table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable" cols="@0,8">
      <tr>
        <th width="3%" class="t-align-center">Sl. No.</th>
        <th width="15%" class="t-align-center">Description of Payment</th>
        <th width="10%" class="t-align-center">E-Mail ID</th>
        <th width="15%" class="t-align-center">Payee Name</th>
        <th width="20%" class="t-align-center">Payee Address</th>
        <th width="10%" class="t-align-center">Date of Payment</th>
        <th width="10%" class="t-align-center">Amount (In Nu.)</th>
        <th width="12%" class="t-align-center">Remarks</th>
        <th width="5%" class="t-align-center">Action</th>
    </tr>
</table>
    </div>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="pagination" class="pagging_1">
    <tr>
        <td align="left">Page <span id="pageNoTot">1</span> of <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="hidden" name="size" id="size" value="10" style="width:70px;"/>
        </td>
        <td align="center"><input name="textfield3" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
            &nbsp;
            <label class="formBtn_1">
                <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
            </label></td>
        <td  class="prevNext-container">
            <ul>
                <li><font size="3">&laquo;</font> <a href="javascript:void(0)" disabled id="btnFirst">First</a></li>
                <li><font size="3">&#8249;</font> <a href="javascript:void(0)" disabled id="btnPrevious">Previous</a></li>
                <li><a href="javascript:void(0)" id="btnNext">Next</a> <font size="3">&#8250;</font></li>
                <li><a href="javascript:void(0)" id="btnLast">Last</a> <font size="3">&raquo;</font></li>
            </ul>
       </td>
    </tr>
</table>
<input type="hidden" id="pageNo" value="1"/>
<!--input type="hidden" id="size" value="20"/-->
</div>
  <p>&nbsp;</p>
    <div>&nbsp;</div>
</div>
<form id="formstyle" action="" method="post" name="formstyle">
   <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
   <%
     SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
     String appenddate = dateFormat1.format(new Date());
   %>
   <input type="hidden" name="fileName" id="fileName" value="MiscellaneousPaymentReport_<%=appenddate%>" />
    <input type="hidden" name="id" id="id" value="MiscellaneousPaymentReport" />
</form>
    <!--Dashboard Content Part End-->
  <!--Dashboard Footer Start-->
 <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
  <!--Dashboard Footer End-->
</div>
</body>
    <script>
        var Interval;
        clearInterval(Interval);
        Interval = setInterval(function(){
            var PageNo = document.getElementById('pageNo').value;
            var Size = document.getElementById('size').value;
            CorrectSerialNumber(PageNo-1,Size); 
        }, 100);
        var headSel_Obj = document.getElementById("headTabReport");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
    </script>
</html>

