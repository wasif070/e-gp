<%--
    Document   : CreateForm
    Created on : 24-Oct-2010, 4:49:09 PM
    Author     : yanki
--%>

<%@page import="org.apache.log4j.Logger"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="formulaCreation"  class="com.cptu.egp.eps.web.servicebean.TemplateTableSrBean" />
<%@page import="com.cptu.egp.eps.model.table.TblTemplateColumns" %>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateFormulas" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Prepare Formula</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="../resources/js/form/CommonValidation.js"></script>
        <script type="text/javascript" src="../resources/js/form/CreateFormula.js"></script>
        <script type="text/javascript" src="../resources/js/form/Add.js"></script>
        <script type="text/javascript" src="../resources/js/form/ConvertToWord.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
    </head>
    <body>
        <script>
            var arrIds = new Array();
            var ApplyTo = "";
            var ForMula = "",FormulaToSave="";
            var OpenedBrace = 0, ClosedBrace = 0;
            var Tested = "";
            var arrColIds = new Array();
            var ColDataType =  new Array();
            var arrHeader = new Array();
            var colIdOrderByColId =  new Array();
            var colDataTypeOrderByColId =  new Array();
            var totalNoOfColumns = 0;
            var vdate;
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    dateFormat:"%d-%b-%Y",
                    onSelect: function() {
                        vdate = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = vdate;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }

        </script>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <%
                        Logger LOGGER = Logger.getLogger("CreateFormula.jsp");
                        short templateId = 0;
                        int sectionId = 0;
                        int formId = 0;
                        int tableId = 0;
                        if (request.getParameter("templateId") != null) {
                            templateId = Short.parseShort(request.getParameter("templateId"));
                        }
                        if (request.getParameter("sectionId") != null) {
                            sectionId = Integer.parseInt(request.getParameter("sectionId"));
                        }
                        if (request.getParameter("formId") != null) {
                            formId = Integer.parseInt(request.getParameter("formId"));
                        }
                        if (request.getParameter("tableId") != null) {
                            tableId = Integer.parseInt(request.getParameter("tableId"));
                        }
                        String logUserId = "0";
                        if (session.getAttribute("userId") != null) {
                            logUserId = session.getAttribute("userId").toString();
                        }
                        formulaCreation.setLogUserId(logUserId);

                        short colCnt = 0;

                        java.util.List<TblTemplateColumns> tblColumnsDtl = formulaCreation.getColumnsDtls(tableId, true);
                        java.util.List<TblTemplateColumns> tblAutoColumnsDtl = formulaCreation.getAutoColumnsDtls(tableId);
                        java.util.List<TblTemplateColumns> tblColumnsDtlWithOutSortOrder = formulaCreation.getColumnsDtls(tableId, false);

                        colCnt = (short) tblColumnsDtl.size();
            %>
            <script>
                totalNoOfColumns = parseInt("<%= colCnt%>");
            </script>
            <%
                        String arrColName[] = new String[colCnt];
                        short arrSortOrder[] = new short[colCnt];
                        if (tblColumnsDtlWithOutSortOrder != null) {
                            if (tblColumnsDtlWithOutSortOrder.size() > 0) {
                                byte i = 0;
                                for (TblTemplateColumns ttc : tblColumnsDtlWithOutSortOrder) {
                                    arrColName[i] = ttc.getColumnHeader();
                                    arrSortOrder[i] = ttc.getSortOrder();
            %>
            <script>
                colIdOrderByColId.push('<%= ttc.getColumnId()%>');
                colDataTypeOrderByColId.push('<%= ttc.getDataType()%>');
            </script>
            <%
                                    i++;
                                }
                            }
                            tblColumnsDtlWithOutSortOrder = null;
                        }
            %>
            <!--Middle Content Table Start-->
            <div class="contentArea_1">
                <form method="post" id="frmFormulaCreation" action="<%=request.getContextPath()%>/CreateSTDForm?action=formulaCreation" method="post">
                    <div class="pageHead_1">
                        Create Formula
                        <span style="float: right; text-align: right;">
                            <a class="action-button-goback" href="TableDashboard.jsp?templateId=<%= templateId%>&sectionId=<%= sectionId%>&formId=<%= formId%>" title="Form Dashboard">Form Dashboard</a>
                        </span>
                    </div>
                    <input type="hidden" name="templateId" value="<%=templateId%>" />
                    <input type="hidden" name="sectionId" value="<%=sectionId%>" />
                    <input type="hidden" name="formId" value="<%=formId%>" />
                    <input type="hidden" name="tableId" value="<%=tableId%>" />

                    <input type="hidden" name="hidFormula" value="" />
                    <input type="hidden" name="AddComboTo" value="" />
                    <input type="hidden" name="SaveTo" value="" />
                    <input type="hidden" name="colCnt" value="<%= colCnt%>" />
                    <table width="100%" cellspacing="0" class="tableList_1 t_space" id="table<%= tableId%>">
                        <tbody>
                            <tr>
                                <%
                                            if (tblColumnsDtl != null) {
                                                if (tblColumnsDtl.size() > 0) {
                                                    for (TblTemplateColumns ttc : tblColumnsDtl) {
                                %>
                                <th style="text-align:center; width:8%;" id="<%= ttc.getColumnId()%>" title="<%= ttc.getColumnId()%>" textToDisplay="<%=ttc.getColumnHeader().replaceAll("\"", "&quot;")%>" value="thisIsColIdOnly">
                                    <%= ttc.getColumnHeader().replaceAll("\"", "&quot;")%> (<%= ttc.getSortOrder()%>) <br />
                                    <%
                                                                        if ((ttc.getDataType() != 2) && (ttc.getFilledBy() == 3 || ttc.getFilledBy() == 2)) {
                                    %>
                                    <input type="checkbox" name="chk<%= ttc.getColumnHeader().replaceAll("\"", "&quot;")%>"
                                           id="chk<%= ttc.getColumnHeader().replaceAll("\"", "&quot;")%>"
                                           onClick="addComboInFormula(document.getElementById('frmFormulaCreation'),this,<%= ttc.getColumnId()%>, <%= tableId%>)" />
                                    <script>
                                        arrHeader.push("<%= ttc.getColumnHeader().replaceAll("\"", "&quot;")%>");
                                    </script>
                                    <input type="hidden" name="fun<%= ttc.getColumnHeader().replaceAll("\"", "&quot;")%>"
                                           id="fun<%= ttc.getColumnHeader().replaceAll("\"", "&quot;")%>" />
                                    <%
                                                                        }
                                    %>
                                </th>
                                <%
                                                        ttc = null;
                                                    }
                                                }
                                            }
                                %>
                            </tr>
                            <tr>
                                <%
                                            if (tblColumnsDtl != null) {
                                                if (tblColumnsDtl.size() > 0) {
                                                    for (TblTemplateColumns ttc : tblColumnsDtl) {
                                %>
                                <td style="text-align:center; width:8%;">
                                    <script>
                                        arrColIds.push('<%= ttc.getColumnId()%>');
                                        ColDataType.push('<%= ttc.getDataType()%>');
                                    </script>
                                    <%
                                                                            if (ttc.getFilledBy() == 1) { // Filled By PE User
                                                                                if (ttc.getDataType() == 3 || ttc.getDataType() == 4 || ttc.getDataType() == 8 || ttc.getDataType() == 11 || ttc.getDataType() == 13) {
                                    %>
                                    <input name="Col<%= ttc.getColumnId()%>" type="text" class="formTxtBox_1" id="Col<%= ttc.getColumnId()%>"
                                           onClick="return BuildFormula(document.getElementById('frmFormulaCreation'),this,<%= ttc.getColumnId()%>);" style="width:100px;"
                                           <%if(ttc.getDataType() == 11){%>
                                            onblur="chkMinus5Plus5(this);"
                                           <%}
                                           %>
                                           />
                                    <%
                                                                                    } else if (ttc.getDataType() == 2) {
                                                                                        out.print("Fill By Govt. user <br /> Long Text");
                                                                                    } else if (ttc.getDataType() == 1) {
                                                                                        out.print("Fill By Govt. user <br /> Small Text");
                                                                                    }
                                                                                    else if (ttc.getDataType() == 12) {
                                        %>
                                        <input name="Col<%= ttc.getColumnId()%>" type="text" class="formTxtBox_1" id="Col<%= ttc.getColumnId()%>"
                                                onClick="return BuildFormula(document.getElementById('frmFormulaCreation'),this,<%= ttc.getColumnId()%>,<%= ttc.getDataType()%>);" style="width:100px;" />
                                        <%
                                                                                    }
                                                                                } else if (ttc.getFilledBy() == 2) { // Filled By Tenderer
                                                                                    if (ttc.getDataType() == 3 || ttc.getDataType() == 4 || ttc.getDataType() == 6 || ttc.getDataType() == 8 || ttc.getDataType() == 9 || ttc.getDataType() == 10 || ttc.getDataType() == 11 || ttc.getDataType() == 13) {
                                                                                        if (ttc.getDataType() == 10) {
                                                                                            out.print("Fill By Bidder/Consultant <br /> Combo - Without Calculation");
                                                                                        } else {
                                    %>
                                    <input name="Col<%= ttc.getColumnId()%>" type="text" class="formTxtBox_1" id="Col<%= ttc.getColumnId()%>"
                                           onClick="return BuildFormula(document.getElementById('frmFormulaCreation'),this,<%= ttc.getColumnId()%>);" style="width:100px;"
                                           <%if(ttc.getDataType() == 11){%>
                                            onblur="chkMinus5Plus5(this);"
                                           <%}
                                           %>
                                           />
                                    <%
                                                                                        }
                                                                                    } else if (ttc.getDataType() == 2) {
                                                                                        out.print("Fill By Bidder/Consultant <br /> Long Text");
                                                                                    } else if (ttc.getDataType() == 1) {
                                                                                        out.print("Fill By Bidder/Consultant <br /> Small Text");
                                                                                    }
                                                                                    else if (ttc.getDataType() == 12) {
                                        %>
                                        <input name="Col<%= ttc.getColumnId()%>" type="text" class="formTxtBox_1" id="Col<%= ttc.getColumnId()%>"
                                                onClick="return BuildFormula(document.getElementById('frmFormulaCreation'),this,<%= ttc.getColumnId()%>,<%= ttc.getDataType()%>);" style="width:100px;" />
                                        <%
                                                                                    }
                                                                                } else if (ttc.getFilledBy() == 3) { //// Filled By Auto
                                                                                    if (ttc.getDataType() == 3 || ttc.getDataType() == 4 || ttc.getDataType() == 8 || ttc.getDataType() == 11 || ttc.getDataType() == 13) {
                                    %>
                                    <input name="Col<%= ttc.getColumnId()%>" type="text" class="formTxtBox_1" id="Col<%= ttc.getColumnId()%>"
                                           onClick="return BuildFormula(document.getElementById('frmFormulaCreation'),this,<%= ttc.getColumnId()%>);" style="width:100px;"
                                           <%if(ttc.getDataType() == 11){%>
                                            onblur="chkMinus5Plus5(this);"
                                           <%}
                                           %>
                                           />
                                    <%
                                                                                    } else if (ttc.getDataType() == 2) {
                                    %>
                                    <textarea name="Col<%= ttc.getColumnId()%>"  id="Col<%= ttc.getColumnId()%>" class="formTxtBox_1"
                                              onClick="return BuildFormula(document.getElementById('frmFormulaCreation'),this,<%= ttc.getColumnId()%>);" rows="3" cols="30"></textarea>
                                    <%
                                                                                    }
                                                                                    else if (ttc.getDataType() == 12) {
                                        %>
                                        <input name="Col<%= ttc.getColumnId()%>" type="text" class="formTxtBox_1" id="Col<%= ttc.getColumnId()%>"
                                                 onClick="return BuildFormula(document.getElementById('frmFormulaCreation'),this,<%= ttc.getColumnId()%>,<%= ttc.getDataType()%>);" style="width:100px;" />
                                        <%
                                                                                    }
                                                                                }
                                    %>
                                </td>
                                <%
                                                        ttc = null;
                                                    }
                                                }
                                                tblColumnsDtl = null;
                                            }
                                %>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <td  style="text-align:center;">
                                <div id="genButton" name="genButton" style="display: table-cell">
                                    <label class="formBtn_1"><input type="button" name="plus" id="plus" value="+" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                    <label class="formBtn_1"><input type="button" name="minus" id="minus" value="-" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                    <label class="formBtn_1"><input type="button" name="mul" id="mul" value="*" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                    <label class="formBtn_1"><input type="button" name="divide" id="divide" value="/" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                    <label class="formBtn_1"><input type="button" name="startBrace" id="startBrace" value="(" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                    <label class="formBtn_1"><input type="button" name="endBrace" id="endBrace" value=")" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                    <label class="formBtn_1"><input type="button" name="Percentage" id="Percentage" value="%" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                    <label class="formBtn_1"><input type="button" name="CustomeNumber" id="CustomeNumber" value="Number" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);" /></label>
                                </div>
                                <div id="dateButton" name="dateButton" style="display: none">
                                    <label class="formBtn_1"><input type="button" name="dateplus" id="dateplus" value="+" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                    <label class="formBtn_1"><input type="button" name="dateminus" id="dateminus" value="-" onClick="return addExpression(document.getElementById('frmFormulaCreation'),this);"/></label>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                            <td style="text-align:center;">
                                <label class="formBtn_1">
                                    <input type="button" name="clear" id="clear" value="Clear" onclick="return clearAll(document.getElementById('frmFormulaCreation'));" />
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;">
                                <select name="ApplyTo" onchange="setFormulaTo(document.getElementById('frmFormulaCreation'), this);" id="ApplyTo" class="formTxtBox_1" style="width:180px;">
                                    <option value="0">-Select-</option>
                                    <%
                                                if (tblAutoColumnsDtl != null) {
                                                    if (tblAutoColumnsDtl.size() > 0) {
                                                        for (TblTemplateColumns ttac : tblAutoColumnsDtl) {
                                    %>
                                    <option value="<%= ttac.getColumnId()%>"><%= ttac.getColumnHeader()%></option>
                                    <%
                                                            ttac = null;
                                                        }
                                                    }
                                                    tblAutoColumnsDtl = null;
                                                }
                                    %>
                                </select>
                            </td>
                            <td style="text-align:center;">
                                <textarea name="Formula" rows="7" class="formTxtBox_1" id="Formula" style="width:500px;" readonly ></textarea>
                            </td>
                            <td style="text-align:center;">
                                <input type="checkbox" name="inToWords" /> In Words<br />
                                <label class="formBtn_1"><input type="button" name="Undo" value="Undo" onClick="UndoChange(document.getElementById('frmFormulaCreation'));" /></label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;">
                                <label class="formBtn_1">
                                    <input type="button" name="TestFormula" id="TestFormula" value="Test Formula" onClick="testformula(document.getElementById('frmFormulaCreation'))"/>
                                    <input type="hidden" id="testedornot" value="n" />
                                </label>
                            </td>
                            <td style="text-align:center;">
                                <label class="formBtn_1">
                                    <input type="submit" name="SaveFormula" id="SaveFormula" value="Save Formula" onClick="return FormulaSave(document.getElementById('frmFormulaCreation'))"/>
                                    <input type="hidden" name="hiddenSubmit"  id="hiddenSubmit" value="" />
                                </label>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </form>

                <form method="post" id="frmFormulaDeletion" action="<%=request.getContextPath()%>/CreateSTDForm?action=formulaDel" method="post">
                    <input type="hidden" name="templateId" value="<%=templateId%>" />
                    <input type="hidden" name="sectionId" value="<%=sectionId%>" />
                    <input type="hidden" name="formId" value="<%=formId%>" />
                    <input type="hidden" name="tableId" value="<%=tableId%>" />
                    <div>&nbsp;</div>
                    <div class="formSubHead_1">Existing Formula(s)</div>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space">
                        <tr>
                            <th style="text-align:center; width:4%;">Sl. No.</th>
                            <th style="text-align:center;">Apply To Column</th>
                            <th style="text-align:center;">Formula</th>
                            <th style="text-align:center; width:10%;">Delete</th>
                        </tr>
                        <%
                                    short j = 0;
                                    java.util.List<TblTemplateFormulas> tblFormulas = formulaCreation.getTableFormulas(tableId);
                                    if (tblFormulas != null) {
                                        if (tblFormulas.size() > 0) {
                                            for (TblTemplateFormulas ttf : tblFormulas) {
                                                j++;
                        %>
                        <tr>
                            <td style="text-align:center;"><%= j%></td>
                            <td style="text-align:center;"><%=arrColName[ttf.getColumnId() - 1]%></td>
                            <td style="text-align:center;">
                                <%
                                                                        String tempFormula = ttf.getFormula();
                                                                        boolean wordOrTotal = false;
                                                                        String strTemp = "";
                                                                        char[] cArray = tempFormula.toCharArray();
                                                                        if (cArray[0] == 'W' || cArray[0] == 'T') {
                                                                            wordOrTotal = true;
                                                                        }

                                                                        for (int c = 0; c < cArray.length; c++) {
                                                                            try {
                                                                                if (cArray[c] == 'N') {
                                                                                    int cnt = 0;
                                                                                    for (int cc = c; cc < cArray.length; cc++) {
                                                                                        if (cArray[cc] == '+' || cArray[cc] == '-' || cArray[cc] == '*' || cArray[cc] == '/' || cArray[cc] == '(' || cArray[cc] == ')' || cArray[cc] == 'p') {
                                                                                            out.println(cArray[cc]);
                                                                                            break;
                                                                                        } else {
                                                                                            cnt++;
                                                                                            out.println(cArray[cc]);
                                                                                        }
                                                                                    }
                                                                                    c = c + cnt;
                                                                                } else {
                                                                                    if (Character.isDigit(cArray[c])) {
                                                                                        int cnt = 0;
                                                                                        String temp = "";
                                                                                        for (int cc = c; cc < cArray.length; cc++) {
                                                                                            if (cArray[cc] == '+' || cArray[cc] == '-' || cArray[cc] == '*' || cArray[cc] == '/' || cArray[cc] == '(' || cArray[cc] == ')' || cArray[cc] == 'p') {
                                                                                                cnt--;
                                                                                                break;
                                                                                            } else {
                                                                                                cnt++;
                                                                                                temp = temp + cArray[cc];
                                                                                            }
                                                                                        }
                                                                                        int t = Integer.parseInt(temp);
                                                                                        out.println(arrColName[t - 1]);
                                                                                        c = c + cnt;
                                                                                    } else {
                                                                                        if ("WORD".equalsIgnoreCase(strTemp)) {
                                                                                            wordOrTotal = false;
                                                                                            out.println("Convert into Words");
                                                                                            strTemp = "";
                                                                                        } else if ("TOTAL".equalsIgnoreCase(strTemp)) {
                                                                                            wordOrTotal = false;
                                                                                            out.println("Grand Total of");
                                                                                            strTemp = "";
                                                                                        }


                                                                                        if (wordOrTotal) {

                                                                                            strTemp = strTemp + cArray[c];

                                                                                        } else {

                                                                                            out.println(cArray[c]);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            } catch (Exception ex) {
                                                                                LOGGER.error("ex :" + logUserId + " : " + ex);
                                                                            }
                                                                        }
                                %>
                            </td>
                            <td style="text-align:center;">
                                <%
                                                                        if (ttf.getFormula() != null) {
                                                                            if (ttf.getFormula().indexOf("TOTAL") >= 0 || ttf.getFormula().indexOf("AVG") >= 0) {
                                %>
                                <input type="checkbox" name="chkTotalDelete" id="chkDelete<%= j%>" value="<%= ttf.getColumnId()%>" />
                                <%
                                                                                        } else {
                                %>
                                <input type="checkbox" name="chkDelete" id="chkDelete<%= j%>" value="<%= ttf.getColumnId()%>" />
                                <%
                                                                            }
                                                                        }
                                %>
                            </td>
                        </tr>

                        <%
                                                                ttf = null;
                                                            }
                                                        } else {
                        %>
                        <tr>
                            <td colspan="4" class="t-align-center">
                                No Records Found
                            </td>
                        </tr>
                        <%                                                                    }
                                                        tblFormulas = null;
                                                    } else {
                        %>
                        <tr>
                            <td colspan="4" class="t-align-center">
                                No Records Found
                            </td>
                        </tr>
                        <%                                            }
                        %>
                    </table>
                    <div class="t_space" align="right">
                        <label class="formBtn_1">
                            <input type="button" name="DeleteFormula" id="DeleteFormula" value="Delete" onClick="return confirmDelete(document.getElementById('frmFormulaDeletion'), <%= j%>);" />
                        </label>
                    </div>
                    <div>&nbsp;</div>
                    <!--Dashboard Content Part End-->
                    <!--Dashboard Footer Start-->
                </form>
                <!--Middle Content Table End-->
            </div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <script>
            var headSel_Obj = document.getElementById("headTabSTD");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
<%
            if (tblFormulas != null) {
                tblFormulas = null;
            }
            if (tblColumnsDtlWithOutSortOrder != null) {
                tblColumnsDtlWithOutSortOrder = null;
            }
            if (tblAutoColumnsDtl != null) {
                tblAutoColumnsDtl = null;
            }
            if (tblColumnsDtl != null) {
                tblColumnsDtl = null;
            }
            if (formulaCreation != null) {
                formulaCreation = null;
            }
%>
