<%--
    Document   : UserPreference
    Created on : Jan 23, 2011, 3:18:54 AM
    Author     : Dixit
--%>

<%@page import="com.cptu.egp.eps.model.table.TblSmsPrefMaster"%>
<%@page import="com.cptu.egp.eps.model.table.TblEmailPrefMaster"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="javax.swing.JOptionPane"%>
<%@page import="com.cptu.egp.eps.model.table.TblUserPrefrence"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.UserPrefService"%>
<%
//String  str= request.getContextPath();
//String message2 = request.getParameter("msg2");
    String message = request.getParameter("msg");
    String logUserId = "0";
    String srtEmailAalert = "";
    String strSmsAlert = "";
    int iUserPrefId = 0;
    String userId = "0";
    String strUserTypeID = "0";
    if (session.getAttribute("userId") != null) {
        logUserId = session.getAttribute("userId").toString();
    }

    UserPrefService userPrefService = (UserPrefService) AppContext.getSpringBean("UserPrefService");
    userPrefService.setLogUserId(logUserId);
    userPrefService.setLogUserId(session.getAttribute("userId").toString());
    userId = request.getSession().getAttribute("userId").toString();
    strUserTypeID = request.getSession().getAttribute("userTypeId").toString();
    List<TblUserPrefrence> getdata = userPrefService.getemailsmsAlert(Integer.parseInt(userId));
    if (!getdata.isEmpty()) {
        srtEmailAalert = getdata.get(0).getEmailAlert();
        strSmsAlert = getdata.get(0).getSmsAlert();
        iUserPrefId = getdata.get(0).getUserPrefId();
    }
    List<Object[]> list = userPrefService.findEmail(strUserTypeID);
    List<Object[]> list12 = userPrefService.findSms(strUserTypeID);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>User Preference</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.1.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript">
        </script>
        <script language="javascript">
            /*        function loadTenderTable()
             {
             //alert($("#cmbOffice").val());
             $.post("<%=request.getContextPath()%>/TblEmailPrefMasterServlet", {
             funName : "fetchdata"
             },
             function(j){
             $('#resultTable1').find("tr:gt(0)").remove();
             $('#resultTable1 tr:last').after(j);
             });
             }
             */
            var fieldName1 = 'check_list2';

            function selectall1() {
                var i = document.selectpref.elements.length;
                var e = document.selectpref.elements;
                var name = new Array();
                var value = new Array();
                var j = 0;
                for (var k = 0; k < i; k++)
                {
                    if (document.selectpref.elements[k].name == fieldName1)
                    {
                        if (document.selectpref.elements[k].checked == true) {
                            value[j] = document.selectpref.elements[k].value;
                            j++;
                        }
                    }
                }
                checkSelect1();
            }
            function selectCheck1(obj)
            {
                var i = document.selectpref.elements.length;
                for (var k = 0; k < i; k++)
                {
                    if (document.selectpref.elements[k].name == fieldName1)
                    {
                        document.selectpref.elements[k].checked = obj;
                    }
                }
                selectall1();
            }

            function selectallMe1()
            {
                if (document.selectpref.check_list1.checked == true)
                {
                    selectCheck1(true);
                } else
                {
                    selectCheck1(false);
                }
            }
            function checkSelect1()
            {
                var i = document.selectpref.elements.length;
                var berror = true;
                for (var k = 0; k < i; k++)
                {
                    if (document.selectpref.elements[k].name == fieldName1)
                    {
                        if (document.selectpref.elements[k].checked == false)
                        {
                            berror = false;
                            break;
                        }
                    }
                }
                if (berror == false)
                {
                    document.selectpref.check_list1.checked = false;
                } else
                {
                    document.selectpref.check_list1.checked = true;
                }
            }

            function validate()
            {
                var cb1 = document.getElementsByName("check_list2");
                var cb2 = document.getElementsByName("check_list4");
                var hasChecked = false;
                for (var i = 0; i < cb1.length; i++)
                {
                    if (cb1[i].checked)
                    {
                        hasChecked = true;
                        break;
                    }
                }
                if (hasChecked == false)
                {
                    alert("Please select at least one Email Alerts.");
                    return false;
                }
                var hasChecked2 = false;
                for (var i = 0; i < cb2.length; i++)
                {
                    if (cb2[i].checked)
                    {
                        hasChecked2 = true;
                        break;
                    }
                }
                if (hasChecked2 == false)
                {
                    alert("Please select at least one SMS Alerts.");
                    return false;
                }
                return true;
            }

            //alert($("#cmbOffice").val());


        </script>
        <script language="javascript">
            var fieldName2 = 'check_list4';

            function selectall2() {
                var i = document.selectpref.elements.length;
                var e = document.selectpref.elements;
                var name = new Array();
                var value = new Array();
                var j = 0;
                for (var k = 0; k < i; k++)
                {
                    if (document.selectpref.elements[k].name == fieldName2)
                    {
                        if (document.selectpref.elements[k].checked == true) {
                            value[j] = document.selectpref.elements[k].value;
                            j++;
                        }
                    }
                }
                checkSelect2();
            }
            function selectCheck2(obj)
            {
                var i = document.selectpref.elements.length;
                for (var k = 0; k < i; k++)
                {
                    if (document.selectpref.elements[k].name == fieldName2)
                    {
                        document.selectpref.elements[k].checked = obj;
                    }
                }
                selectall2();
            }

            function selectallMe2()
            {
                if (document.selectpref.check_list3.checked == true)
                {
                    selectCheck2(true);
                } else
                {
                    selectCheck2(false);
                }
            }
            function checkSelect2()
            {
                var i = document.selectpref.elements.length;
                var berror = true;
                for (var k = 0; k < i; k++)
                {
                    if (document.selectpref.elements[k].name == fieldName2)
                    {
                        if (document.selectpref.elements[k].checked == false)
                        {
                            berror = false;
                            break;
                        }
                    }
                }
                if (berror == false)
                {
                    document.selectpref.check_list3.checked = false;
                } else
                {
                    document.selectpref.check_list3.checked = true;
                }
            }
        </script>
    </head>
    <body>
        <%
            boolean isadmin = true;
            int usrId = 0;
            if (userId != null && !userId.toString().isEmpty()) {
                usrId = Integer.parseInt(userId.toString());
                NavigationRuleUtil navRuleUtil = new NavigationRuleUtil();
                isadmin = navRuleUtil.isAdmin(usrId);
            }


            /*
             */
            //boolean b=userPrefService.getpreferenceStatus(Integer.parseInt(userId), "sms");
            //boolean c=userPrefService.getpreferenceStatus(Integer.parseInt(userId), "email");
            //List<Object[]> list = userPrefService.getEmailSmsStatus("egpadmin@eprocure.gov.bd");
            //String emailalert = "";
            //String smsalert  = "";
            //if(!list.isEmpty())
            //{
            //Object[] obj = list.get(0);
            //emailalert = obj[0].toString();
            //smsalert = obj[1].toString();
            //}else
            //{
            //emailalert = "Yes";
            //smsalert = "Yes";
            //}
%>

        <div class="mainDiv">
            <div class="fixDiv">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <td><div class="pageHead_1">User Preference</div>
                                        <form id="frmpreference" name="selectpref" method="post" action='<%=request.getContextPath()%>/UserPrefSrBean'>
                                            <input type="hidden" name="hiddenprefid" id="hiddenprefid" value="<%=iUserPrefId%>"/>&nbsp;
                                            <%
                                                if ("fail".equalsIgnoreCase(message)) {
                                            %>
                                            <div class="responseMsg errorMsg t_space"><span>Your preference submission failed</span></div>
                                            <%
                                            } else if ("submit".equalsIgnoreCase(message)) {
                                            %>
                                            <div class="responseMsg successMsg t_space"><span>Your preference submitted successfully</span></div>
                                            <%                                                  } else if ("update".equalsIgnoreCase(message)) {
                                            %>
                                            <div class="responseMsg successMsg t_space"><span>Your preference updated successfully</span></div>
                                            <%                                                  }
                                            %>

                                            <%-- <div align="left" class="t_space">Fields marked with (<span class="mandatory">*</span>) are mandatory.</div>  --%>
                                            <table width="100%" cellspacing="0" id="resultTable1" cols="">
                                                <tr id="subtableheading">
                                                    <!-- FOR EMAIL PREFERANCE -->
                                                    <table border='0' cellspacing='10' cellpadding='0' class='formStyle_1'>
                                                        <tr>
                                                            <td class='ff'> Email Alerts : </td>
                                                            <%
                                                                CommonService commonService_userpref = (CommonService) AppContext.getSpringBean("CommonService");
                                                                String regType = commonService_userpref.getRegType(session.getAttribute("userId").toString());

                                                                if (regType != null && regType.equalsIgnoreCase("media")) {
                                                            %>
                                                            <td>Not Available </td>
                                                            <%
                                                            } else
                                                                if (list != null && list.size() > 0) {
                                                            %>
                                                            <td><input type='checkbox' name='check_list1' id='check_list1' value='Check All' onClick='selectallMe1()'/> &nbsp;&nbsp;&nbsp;</td><td>All </td>
                                                            <%} else {%>
                                                            <td>Not Available </td>
                                                            <%}%>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <%
                                                            String procureRole = (String) session.getAttribute("procurementRole");

                                                            Iterator itr = list.iterator();
                                                            if (regType != null && regType.equalsIgnoreCase("media")) {
                                                                ;
                                                            } else {
                                                                while (itr.hasNext()) {
                                                                    TblEmailPrefMaster emailprefmaster = (TblEmailPrefMaster) itr.next();
                                                                    boolean isEmailPrefSet = (srtEmailAalert.trim().equals("") || srtEmailAalert.trim().equalsIgnoreCase("no") ? false : srtEmailAalert.contains((new Integer(emailprefmaster.getEpid()).toString())));

                                                                    if (userTypeId == 3 && emailprefmaster.getGovtRoles() != null && !emailprefmaster.getGovtRoles().equals("NULL") && !emailprefmaster.getGovtRoles().equals("null") && (emailprefmaster.getGovtRoles()).contains(procureRole)) {
                                                        %>

                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td><input type='checkbox' name='check_list2'  <%if (isEmailPrefSet) {%>checked="true"<%}%> onclick='selectall1()' value="<%=emailprefmaster.getEpid()%>" />&nbsp;&nbsp;&nbsp;</td>
                                                            <td><%=emailprefmaster.getPrefranceType()%></td>
                                                        </tr>
                                                        <%
                                                        } else if (userTypeId == 3 && (emailprefmaster.getGovtRoles() == null || emailprefmaster.getGovtRoles().equals("NULL") || emailprefmaster.getGovtRoles().equals("null"))) {
                                                        %>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td><input type='checkbox' name='check_list2'  <%if (isEmailPrefSet) {%>checked="true"<%}%> onclick='selectall1()' value="<%=emailprefmaster.getEpid()%>" />&nbsp;&nbsp;&nbsp;</td>
                                                            <td><%=emailprefmaster.getPrefranceType()%></td>
                                                        </tr>
                                                        <%
                                                        } else if (userTypeId == 7 && isadmin == false) {
                                                        %>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td><input type='checkbox' name='check_list2'  <%if (isEmailPrefSet) {%>checked="true"<%}%> onclick='selectall1()' value="<%=emailprefmaster.getEpid()%>" />&nbsp;&nbsp;&nbsp;</td>
                                                            <td><%=emailprefmaster.getPrefranceType()%></td>
                                                        </tr>
                                                        <%
                                                        } else if (userTypeId != 3 && userTypeId != 7) {

                                                        %>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td><input type='checkbox' name='check_list2'  <%if (isEmailPrefSet) {%>checked="true"<%}%> onclick='selectall1()' value="<%=emailprefmaster.getEpid()%>" />&nbsp;&nbsp;&nbsp;</td>
                                                            <td><%=emailprefmaster.getPrefranceType()%></td>
                                                        </tr>
                                                        <%

                                                                    }
                                                                }
                                                            }
                                                        %>
                                                    </table>
                                                    <!-- FOR SMS PREFERANCE -->
                                                    <table border='0' cellspacing='10' cellpadding='0' class='formStyle_1'>
                                                        <tr>
                                                            <td class='ff'> Sms Alerts : </td>
                                                            <%
                                                                if (regType != null && regType.equalsIgnoreCase("media")) {
                                                            %>
                                                            <td>Not Available </td>
                                                            <%
                                                            } else
                                                                if (list != null && list.size() > 0) {
                                                            %>
                                                            <td><input type='checkbox' name='check_list3'id='check_list3' value='Check All' onClick='selectallMe2()'/> &nbsp;&nbsp;&nbsp;</td><td>All </td>
                                                            <%} else {%>
                                                            <td>Not Available </td>
                                                            <%}%>
                                                            <td>&nbsp;&nbsp;</td>
                                                        </tr>
                                                        <%
                                                            itr = list12.iterator();
                                                            if (regType != null && regType.equalsIgnoreCase("media")) {
                                                                ;
                                                            } else {
                                                                while (itr.hasNext()) {
                                                                    TblSmsPrefMaster smsprefmaster = (TblSmsPrefMaster) itr.next();
                                                                    boolean isSMSPrefSet = (strSmsAlert.trim().equals("") || strSmsAlert.trim().equalsIgnoreCase("no") ? false : strSmsAlert.trim().contains(new Integer(smsprefmaster.getSpid()).toString()));
                                                                    if (userTypeId == 3 && smsprefmaster.getGovtRoles() != null && !smsprefmaster.getGovtRoles().equals("null") && !smsprefmaster.getGovtRoles().equals("NULL") && (smsprefmaster.getGovtRoles()).contains(procureRole)) {
                                                        %>


                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td><input type='checkbox' name='check_list4'  <%if (isSMSPrefSet) {%>checked="true"<%}%> onclick='selectall2()' value="<%=smsprefmaster.getSpid()%>" />&nbsp;&nbsp;&nbsp;</td>
                                                            <td><%=smsprefmaster.getPrefranceType()%></td>
                                                        </tr>

                                                        <%
                                                        } else if (userTypeId == 3 && (smsprefmaster.getGovtRoles() == null || smsprefmaster.getGovtRoles().equals("null") || smsprefmaster.getGovtRoles().equals("NULL"))) {
                                                        %>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td><input type='checkbox' name='check_list4'  <%if (isSMSPrefSet) {%>checked="true"<%}%> onclick='selectall2()' value="<%=smsprefmaster.getSpid()%>" />&nbsp;&nbsp;&nbsp;</td>
                                                            <td><%=smsprefmaster.getPrefranceType()%></td>
                                                        </tr>
                                                        <%
                                                        } else if (userTypeId == 7 && isadmin == false) {
                                                        %>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td><input type='checkbox' name='check_list4'  <%if (isSMSPrefSet) {%>checked="true"<%}%> onclick='selectall2()' value="<%=smsprefmaster.getSpid()%>" />&nbsp;&nbsp;&nbsp;</td>
                                                            <td><%=smsprefmaster.getPrefranceType()%></td>
                                                        </tr>
                                                        <%
                                                        } else if (userTypeId != 3 && userTypeId != 7) {

                                                        %>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td><input type='checkbox' name='check_list4'  <%if (isSMSPrefSet) {%>checked="true"<%}%> onclick='selectall2()' value="<%=smsprefmaster.getSpid()%>" />&nbsp;&nbsp;&nbsp;</td>
                                                            <td><%=smsprefmaster.getPrefranceType()%></td>
                                                        </tr>
                                                        <%
                                                                    }
                                                                }
                                                            }
                                                        %>
                                                    </table>
                                                </tr>
                                            </table>
                                            <table>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td align="left">
                                                        <%
                                                            if (regType != null && regType.equalsIgnoreCase("media")) {
                                                                ;
                                                            } else
                                                                if (list != null && list.size() > 0) {
                                                        %>
                                                        <label class="formBtn_1">
                                                            <%
                                                                if (!getdata.isEmpty()) {
                                                            %>
                                                            <input type="submit" name="submitbtn" id="updpref" value="Update" onclick="return validate()" />
                                                            <%
                                                            } else {
                                                            %>
                                                            <input type="submit" name="submitbtn" id="subpref"   value="Submit" onclick="return validate()" />
                                                            <%
                                                                        }
                                                                    }
                                                            %>
                                                        </label>
                                                    </td>
                                                </tr>
                                                <script type="text/javascript">
                                                    var headSel_Obj = document.getElementById("headTabPref");
                                                    if (headSel_Obj != null) {
                                                        headSel_Obj.setAttribute("class", "selected");
                                                    }
                                                </script>
                                            </table>
                                        </form>
                                    </td>
                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
    </body>
    <script type="text/javascript" language="Javascript">
        var headSel_Obj = document.getElementById("headTabMyAcc");
        if (headSel_Obj != null) {
            headSel_Obj.setAttribute("class", "selected");
        }

    </script>
    <!--     <script type="text/javascript" language="Javascript">
         function validate()
         {
             $(".reqF_1").remove();
             var j=0;var k=0;
             var flag = true;
             for( i = 0; i < document.selectpref.emailrad.length; i++ )
             {
                 if( document.selectpref.emailrad[i].checked == false )
                 {
                     j=j+1;
                 }
                 if( document.selectpref.smsrad[i].checked == false )
                 {
                     k=k+1;
                 }
             }
             if(j==2)
             {
                 flag= false;
                 $('#radbtn2').parent().append("<div class='reqF_1'>please select email alert</div>");
             }
             if(k==2)
             {
                 flag= false;
                 $('#smsRad2').parent().append("<div class='reqF_1'>please select sms alert</div>");
             }
             return flag;
    
         }
         </script>-->
</html>
