<%-- 
    Document   : UserRegReportPdf
    Created on : Mar 23, 2011, 3:21:32 PM
    Author     : Swati
--%>

<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchData,com.cptu.egp.eps.service.serviceimpl.CommonSearchService,com.cptu.egp.eps.web.utility.AppContext,java.util.List"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>User Registration MIS Report</title>
<link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/js/jQuery/jquery-ui-1.8.5.custom.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
<link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />

</head>
<body>
<%
                String param1 = "getRegFeePayment_MISReport";
                String status = "";
                String paymentFrom = "";
                String paymentTo = "";
                String emailId = "";
                String companyName = "";
                String strPageNo = "";
                String strOffset = "";
                String branchName = null;
                String bankName = null;
                String sortEmail="";
                String sortCmp="";
                String paymentMode = "All";
                if(request.getParameter("status")!=null){
                   status = request.getParameter("status");
                }
                if(request.getParameter("emailId")!=null){
                   emailId = request.getParameter("emailId");
                   if("eq".equalsIgnoreCase(request.getParameter("sortEmail"))){
                        sortEmail = "=";
                    }
                    else{
                       sortEmail = request.getParameter("sortEmail");
                    }
                   
                }
                if(request.getParameter("companyName")!=null){
                   companyName = request.getParameter("companyName");
                   if("eq".equalsIgnoreCase(request.getParameter("sortCmp"))){
                        sortCmp = "=";
                    }
                    else{
                       sortCmp = request.getParameter("sortCmp");
                    }
                }
                if(request.getParameter("paymentFrom")!=null){
                   paymentFrom = request.getParameter("paymentFrom");
                }
                if(request.getParameter("paymentTo")!=null){
                   paymentTo = request.getParameter("paymentTo");
                }
                if(request.getParameter("strPageNo")!=null){
                   strPageNo = request.getParameter("strPageNo");
                }
                if(request.getParameter("strOffset")!=null){
                   strOffset = request.getParameter("strOffset");
                }
                if(request.getParameter("branchName")!=null){
                   branchName = request.getParameter("branchName").replace("_", " ");
                }
                if(request.getParameter("bankName")!=null){
                   bankName = request.getParameter("bankName").replace("_", " ");
                }
                if (request.getParameter("paymentMode") != null) {
                        paymentMode = request.getParameter("paymentMode");
                    }
                 String isPDF = "abc";
                    if (request.getParameter("isPDF") != null) {
                        isPDF = request.getParameter("isPDF");
                    }
%>
 <div class="dashboard_div">
  <div class="pageHead_1">User Registration and Payment Report</div>
  <div class="formBg_1 t_space">
  <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
      <tr>
          <td>Payment For:</td>
          <td colspan="2">
              <%=paymentMode%>
          </td>
      </tr>
      <% if(request.getParameter("status") != "") { %>
    <tr>
      <td class="ff" width="15%">Status :</td>
      <td width="40%">
          <%if(status.equalsIgnoreCase("New")){%>
                                            New
          <%}else if(status.equalsIgnoreCase("Renew")){%>
                                            Renew
          <%}else if(status.equalsIgnoreCase("Expired")){%>
                                            Expired
          <% } %>
      </td>
      <td width="13%">&nbsp;</td>
      <td width="32%">&nbsp;</td>
      </tr>
      <% } %>
      <tr style="display:table-row;" id="paymentRow">
          <% if(request.getParameter("paymentFrom") != "") { %>
          <td class="ff">Payment Date From :</td>
          <td><%=paymentFrom%></td>
          <% } %>
          <% if(request.getParameter("paymentTo") != "") { %>
         <td class="ff">Payment Date To :</td>
          <td><%=paymentTo%></td>
          <% } %>
       </tr>
      <tr>
       <% if(request.getParameter("emailId") != "") { %>
      <td class="ff">e-mail ID :</td>
      <td><%=emailId%></td>
      <% } %>
       <% if(request.getParameter("companyName") != "") { %>
      <td class="ff">Company Name :</td>
      <td><%=companyName%></td>
      <% } %>
    </tr>
    
    <% if(request.getParameter("bankName") != null && !"".equalsIgnoreCase(request.getParameter("bankName")) && request.getParameter("branchName") != null && !"".equalsIgnoreCase(request.getParameter("branchName"))){ %>
       <tr> 
      <td class="ff">Bank Name :</td>
      <td><%=request.getParameter("bankName")%></td>
      <td class="ff">Branch Name :</td>
      <td><%=request.getParameter("branchName")%></td>
      </tr>
      <% } %>
    
 
  </table>
  </div>
<div id="resultDiv" style="display: block;">
<table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable">
      <tr>
        <th width="4%" class="t-align-center">Sl. No.</th>
        <th width="11%" class="t-align-center">	Registration Type</th>
        <th width="9%" class="t-align-center">Registration Status</th>
        <th width="15%" class="t-align-center">Company Name</th>
        <th width="14%" class="t-align-center">e-mail ID</th>
        <th width="8%" class="t-align-center">Country</th>
        <th width="7%" class="t-align-center">State</th>
        <th width="5%" class="t-align-center">Payment Status</th>
        <th width="13%" class="t-align-center">Payment Date and Time</th>
        <th width="13%" class="t-align-center">Amount</th>
       
    </tr>
     <%
       CommonSearchDataMoreService searchService = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
       List<SPCommonSearchDataMore> spSearchData = searchService.geteGPData("getRegFeePayment_MISReport", strPageNo, strOffset, status, paymentFrom, paymentTo, emailId, companyName,sortEmail,sortCmp,bankName,branchName);

      for(int i = 0; i < spSearchData.size(); i++){ %>
      <tr>
                                <td class="t-align-center"><%=i+1%></td>
                                <td class="t-align-left"><%= spSearchData.get(i).getFieldName3() %></td>
                                <td class="t-align-left"><%= spSearchData.get(i).getFieldName4() %></td>
                                <td class="t-align-left"><%= spSearchData.get(i).getFieldName5() %></td>
                                <td class="t-align-left"><%= spSearchData.get(i).getFieldName6() %></td>
                                <td class="t-align-center"><%= spSearchData.get(i).getFieldName7() %></td>
                                <td class="t-align-center"><%=spSearchData.get(i).getFieldName8()%></td>
                                <td class="t-align-center"><%=spSearchData.get(i).getFieldName14()%></td>
                                <td class="t-align-center"><%=spSearchData.get(i).getFieldName9()%></td>
                                <td class="t-align-center"><%=spSearchData.get(i).getFieldName10()%></td>
     </tr>
<% }
       
                List<SPCommonSearchDataMore>  spCriteriaTotal = searchService.geteGPData("getPaymentReportTotal",null,null,status, paymentFrom, paymentTo, emailId, companyName,sortEmail,sortCmp,bankName,branchName,paymentMode);
               List<SPCommonSearchDataMore>  spCriteriaVerifiedTotal = searchService.geteGPData("getPaymentReportTotal","verified",null,status, paymentFrom, paymentTo, emailId, companyName,sortEmail,sortCmp,bankName,branchName,paymentMode);
              List<SPCommonSearchDataMore>   spCriteriaPaidTotal = searchService.geteGPData("getPaymentReportTotal","paid",null,status, paymentFrom, paymentTo, emailId, companyName,sortEmail,sortCmp,bankName,branchName,paymentMode);
%>

</table>
 <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="pagging_1" id="searchcritres">


<%

             out.print("<tr><td width=\"70%\" align='right' ><b>Grand Total Of Verified:</b></td><td align=\"right\"width=\"30%\" ><b>");

                        if(spCriteriaVerifiedTotal!=null&&!spCriteriaVerifiedTotal.isEmpty()){
                       if(spCriteriaVerifiedTotal.get(0)!=null){
                      out.print(spCriteriaVerifiedTotal.get(0).getFieldName1()+":"+spCriteriaVerifiedTotal.get(0).getFieldName2());
                      } else{
                     out.print("Nu.:0");
                           }
                    if(spCriteriaVerifiedTotal.size()>1){ if(spCriteriaVerifiedTotal.get(1)!=null){
                   out.print("  " + spCriteriaVerifiedTotal.get(1).getFieldName1()+":"+spCriteriaVerifiedTotal.get(1).getFieldName2());

                   }
                    else{
                     out.print("USD:0");
                          }
                    }else{
                     out.print("USD:0");
                          }


                 }
                        else{

                      out.print("Nu.:0 USD:0");
                 }
                         out.print(" </b> </td></tr>");
                         out.print("<tr><td  width=\"70%\" align='right' ><b>Grand Total Of Paid:</b></td><td align=\"right\"width=\"30%\" ><b>");

                          if(spCriteriaPaidTotal!=null&&!spCriteriaPaidTotal.isEmpty()){
                       if(spCriteriaPaidTotal.get(0)!=null){
                      out.print(spCriteriaPaidTotal.get(0).getFieldName1()+":"+spCriteriaPaidTotal.get(0).getFieldName2());
                      } else{
                     out.print("Nu.:0");
                           }
                    if(spCriteriaPaidTotal.size()>1){ if(spCriteriaPaidTotal.get(1)!=null){
                   out.print("  " + spCriteriaPaidTotal.get(1).getFieldName1()+":"+spCriteriaPaidTotal.get(1).getFieldName2());

                   }else{
                     out.print("USD:0");
                          }
                    }else{
                     out.print("USD:0");
                          }


                 }else{

                      out.print("Nu.:0 USD:0");
                 }
                        out.print(" </b> </td></tr>");
                        out.print("<tr><td  width=\"70%\" align='right' ><b>Grand Total: </b></td><td align=\"right\"width=\"30%\" ><b>");

                        if(spCriteriaTotal!=null&&!spCriteriaTotal.isEmpty())
                        {

                            if(spCriteriaTotal.get(0)!=null){
                      out.print(spCriteriaTotal.get(0).getFieldName1()+":"+spCriteriaTotal.get(0).getFieldName2());
                      } else{
                     out.print("Nu.:0");
                           }
                    if(spCriteriaTotal.size()>1){ if(spCriteriaTotal.get(1)!=null){
                   out.print("  " + spCriteriaTotal.get(1).getFieldName1()+":"+spCriteriaTotal.get(1).getFieldName2());

                   }else{
                     out.print("USD:0");
                          }
                    }else{
                     out.print("USD:0");
                          }


                 }else{

                      out.print("Nu.:0 USD:0");
                 }
                        out.print(" </b> </td></tr>");

%>
 </table>
       
</div>
  <p>&nbsp;</p>
    <div>&nbsp;</div>

</div>
</body>
</html>

