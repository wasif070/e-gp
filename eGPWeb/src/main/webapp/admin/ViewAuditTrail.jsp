<%-- 
    Document   : ViewAuditTrail
    Created on : Oct 11, 2011, 12:13:49 PM
    Author     : rishita
--%>

<%@page import="java.util.TreeMap"%>
<%@page import="java.util.SortedMap"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Audit Trail Report</title>
    </head>
    <%
                response.setHeader("Expires", "-1");
                response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                response.setHeader("Pragma", "no-cache");
                                int userType=0;
                HttpSession hs = request.getSession();
                if (hs.getAttribute("userTypeId") != null) {
                    userType= Integer.parseInt(hs.getAttribute("userTypeId").toString());
                }
                DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                String toDate= formatter.format(new Date());
                Date fromTempDate =new Date();
                fromTempDate.setHours(00);
                fromTempDate.setMinutes(01);
                String fromDate= formatter.format(fromTempDate);
   %>

    <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
    <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
    <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
    <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
    <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
    <script type="text/javascript" src="../resources/js/jQuery/jquery.blockUI.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
    <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
    <script type="text/javascript">
        /* Call Print function */
        $(document).ready(function() {
            $("#print").click(function() {
                printElem({ leaveOpen: true, printMode: 'popup' });
            });

        });
        function printElem(options){
            $('#divSearchBox').hide();
            $('#pagination').hide();
            $('#print').hide();
            $('#pdfLink').hide();
            if($('#divSearchDisplay') && $('#divSearchDisplay').html() != ''){
                    /--//$('#divSearhcDisplayPrint').show();
            }
            $('#print_area').printElement(options);
            $('#print').show();
            $('#pdfLink').show();
            $('#divSearchBox').show();
            //$('#divSearhcDisplayPrint').hide();
            $('#pagination').show();
        }
    </script>
    <script type="text/javascript">
        function GetCal(txtname,controlname)
        {
            new Calendar({
                inputField: txtname,
                trigger: controlname,
                showTime: 24,
                onSelect: function() {
                    var date = Calendar.intToDate(this.selection.get());
                    LEFT_CAL.args.min = date;
                    LEFT_CAL.redraw();
                    this.hide();
                    document.getElementById(txtname).focus();
                }
            });

            var LEFT_CAL = Calendar.setup({
                weekNumbers: false
            })
        }
    </script>
    <script type="text/javascript">
        function loadTable(){
            //$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
               $.blockUI();
               var fromDateTxt=$("#txtFromDate").val();
               var toDateTxt=$("#txtToDate").val();
               if(fromDateTxt=="" && toDateTxt==""){
                   fromDateTxt="<%=fromDate%>";
                   toDateTxt="<%=toDate%>";
               }
            $.post("<%=request.getContextPath()%>/AuditTrailServlet", {funName: "AuditTrailDetails",typeOfUser:$("#typeOfUser").val(),ipAddress:$("#ipAddress").val(),txtFromDate: fromDateTxt,tenderId: $("#tenderId").val(),txtToDate: toDateTxt,ipEmailId: $("#ipEmailId").val(),activity:$("#activity").val(),txtdepartmentid: $("#txtdepartmentid").val(),procuringEntity: $("#procuringEntity").val(),pageNo: $("#pageNo").val(),size: $("#size").val(),module: $("#module").val(),cmbTenderId: $("#cmbTenderId").val()},  function(j){
                $.unblockUI();
                $('#resultTable').find("tr:gt(0)").remove();                
                $('#resultTable tr:last').after(j);
                sortTable();
                if($('#noRecordFound').attr('value') == "noRecordFound"){
                    $('#pagination').hide();
                }else{
                    $('#pagination').show();
                }
                chkdisble($("#pageNo").val());
                if($("#totalPages").val() == 1){
                    $('#btnNext').attr("disabled", "true");
                    $('#btnLast').attr("disabled", "true");
                }else{
                    $('#btnNext').removeAttr("disabled");
                    $('#btnLast').removeAttr("disabled");
                }
                $("#pageTot").html($("#totalPages").val());
                $("#pageNoTot").html($("#pageNo").val());
                $('#resultDiv').show();
            });
        }
    
        function loadOffice() {
            var deptId=$('#txtdepartmentid').val();
            $.post("<%=request.getContextPath()%>/ComboServlet", {departmentId: deptId,funName:'officeCombo'},  function(j){
                $('#procuringEntity').children().remove().end()
                $("select#procuringEntity").html(j);
            });
        }

    
        function chkdisble(pageNo){
            $('#dispPage').val(Number(pageNo));
            if(parseInt($('#pageNo').val(), 10) != 1){
                $('#btnFirst').removeAttr("disabled");
                $('#btnFirst').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnFirst').attr("disabled", "true");
                $('#btnFirst').css('color', 'gray');
            }


            if(parseInt($('#pageNo').val(), 10) == 1){
                $('#btnPrevious').attr("disabled", "true")
                $('#btnPrevious').css('color', 'gray');
            }

            if(parseInt($('#pageNo').val(), 10) > 1){
                $('#btnPrevious').removeAttr("disabled");
                $('#btnPrevious').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnLast').attr("disabled", "true");
                $('#btnLast').css('color', 'gray');
            }

            else{
                $('#btnLast').removeAttr("disabled");
                $('#btnLast').css('color', '#333');
            }

            if(parseInt($('#pageNo').val(), 10) == parseInt($('#totalPages').val())){
                $('#btnNext').attr("disabled", "true")
                $('#btnNext').css('color', 'gray');
            }
            else{
                $('#btnNext').removeAttr("disabled");
                $('#btnNext').css('color', '#333');
            }
        }
   
        $(function() {
            $('#btnFirst').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);

                if(totalPages>0 && $('#pageNo').val()!="1")
                {
                    $('#pageNo').val("1");
                    loadTable();
                    $('#dispPage').val("1");
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
   
        $(function() {
            $('#btnLast').click(function() {
                var totalPages=parseInt($('#totalPages').val(),10);
                if(totalPages>0)
                {
                    $('#pageNo').val(totalPages);
                    loadTable();
                    $('#dispPage').val(totalPages);
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
   
        $(function() {
            $('#btnNext').click(function() {
                var pageNo=parseInt($('#pageNo').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo < totalPages) {
                    $('#pageNo').val(Number(pageNo)+1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo)+1);
                    $('#btnPrevious').removeAttr("disabled");
                }
            });
        });

   
        $(function() {
            $('#btnPrevious').click(function() {
                var pageNo=$('#pageNo').val();

                if(parseInt(pageNo, 10) > 1)
                {
                    $('#pageNo').val(Number(pageNo) - 1);
                    loadTable();
                    $('#dispPage').val(Number(pageNo) - 1);
                    if(parseInt($('#pageNo').val(), 10) == 1)
                        $('#btnPrevious').attr("disabled", "true")
                }
            });
        });
   
        $(function() {
            $('#btnSearch').click(function() {
                $(".err").remove();
                $('#pageNo').val("1");
                var strSearchDisplay = "";
                var vbool = true;                
                if($('#typeOfUser').val() != null && $('#typeOfUser').val() != ''){
                    strSearchDisplay = $('#lblTypeOfUser').html() + ' ' + document.getElementById('typeOfUser').options[document.getElementById('typeOfUser').selectedIndex].text;
                }
                if($('#ipAddress').val() != null && $('#ipAddress').val() != ''){
                     //below line checks regex for valid IPAddress
                    vbool = /(?:\d{1,3}\.){3}\d{1,3}/.test($('#ipAddress').val());
                    if(!vbool){
                        $('#ipAddress').parent().append("<div class='err' style='color:red;'>Please enter Valid IP Address</div>");
                    }
                    strSearchDisplay = strSearchDisplay + ' , ' + $('#lblIpAddress').html() + ' ' + $('#ipAddress').val();
                }
                if($('#txtFromDate').val() != null && $('#txtFromDate').val() != ''){
                    strSearchDisplay = strSearchDisplay + ' , ' + $('#lblFromDate').html() + ' ' + $('#txtFromDate').val();
                }
                if($('#txtToDate').val() != null && $('#txtToDate').val() != ''){
                    strSearchDisplay = strSearchDisplay + ' , ' + $('#lblToDate').html() + ' ' + $('#txtToDate').val();
                }
                if($('#ipEmailId').val() != null && $('#ipEmailId').val() != ''){
                    //below line checks regex for valid email-Id
                    vbool = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test($('#ipEmailId').val());
                    if(!vbool){
                        $('#ipEmailId').parent().append("<div class='err' style='color:red;'>Please enter Valid e-mail ID</div>");
                    }
                    strSearchDisplay = strSearchDisplay + ' , ' + $('#lblEmailId').html() + ' ' + $('#ipEmailId').val();
                }
                if($('#activity').val() != null && $('#activity').val() != ''){
                    strSearchDisplay = strSearchDisplay + ' , ' + $('#lblActivity').html() + ' ' + document.getElementById('activity').options[document.getElementById('activity').selectedIndex].text;
                }
                if($('#procuringEntity').val() != null && $.trim($('#procuringEntity').val()) != ''){
                    strSearchDisplay = strSearchDisplay + ' , ' + $('#lblProcuringEntity').html() + ' ' + document.getElementById('procuringEntity').options[document.getElementById('procuringEntity').selectedIndex].text;
                }
                if($('#txtdepartment').val() != null && $('#txtdepartment').val() != ''){
                    strSearchDisplay = strSearchDisplay + ' , ' + $('#lblMDO').html() + ' ' + $('#txtdepartment').val();
                }
                if($('#tenderId').val() != null && $('#tenderId').val() != ''){
                    vbool = /^(\d{1,10})?$/.test($('#tenderId').val());
                    if(!vbool){
                        $('#tenderId').parent().append("<div class='err' style='color:red;'>Please enter Valid ID</div>");
                    }
                    strSearchDisplay = strSearchDisplay + ' , ' + document.getElementById('cmbTenderId').options[document.getElementById('cmbTenderId').selectedIndex].text + ' : ' + $('#tenderId').val();
                }
                if($('#module').val() != null && $('#module').val() != ''){
                    strSearchDisplay = strSearchDisplay + ' , ' + $('#lblModule').html() + ' ' + document.getElementById('module').options[document.getElementById('module').selectedIndex].text;
                }
                if(strSearchDisplay.charAt(1) == ','){
                    strSearchDisplay = strSearchDisplay.substr(2, strSearchDisplay.length)
                }
                if(vbool){
                    $('#divSearchDisplay').html(strSearchDisplay);
                    loadTable();
                }
            });

            $('#btnReset').click(function() {
                $(".err").remove();
                $("#typeOfUser").val("");
                $("#ipAddress").val("");
                $("#txtFromDate").val("<%=fromDate%>");
                $("#txtToDate").val("<%=toDate%>");
                $("#ipEmailId").val("");
                $("#activity").val("");
                $("#txtdepartment").val("");
                $("#txtdepartmentid").val("");
                $("#procuringEntity").val("");
                $("#cmbTenderId").val("appId");
                $("#tenderId").val("");
                $("#module").val("");
                $('#pageNo').val("1");
                $('#divSearchDisplay').html('');
                //$('#divSearhcDisplayPrint').hide();
                getActivity();
                loadTable();
                $("#txtFromDate").val("");
                $("#txtToDate").val("");
            });
        });
   
        $(function() {
            $('#btnGoto').click(function() {
                var pageNo=parseInt($('#dispPage').val(),10);
                var totalPages=parseInt($('#totalPages').val(),10);
                if(pageNo > 0)
                {
                    if(pageNo <= totalPages) {
                        $('#pageNo').val(Number(pageNo));
                        loadTable();
                        $('#dispPage').val(Number(pageNo));
                        if(parseInt($('#pageNo').val(), 10) == 1)
                            $('#btnPrevious').attr("disabled", "true")
                        if(parseInt($('#pageNo').val(), 10) > 1)
                            $('#btnPrevious').removeAttr("disabled");
                    }
                }
            });
        });
function getActivity(){
    $.post("<%=request.getContextPath()%>/AuditTrailServlet", {funName: "getActivity",moduleName:$('#module').val()},  function(j){
        //$("select#activity").va;
        $("select#activity").html(j);
    });
}        
    </script>
    <body onload="loadTable();">
        <div class="mainDiv">
            <%@include  file="../resources/common/AfterLoginTop.jsp"%>
<div class="contentArea_1">
            <td class="contentArea_1">
                <div  id="print_area">
                    <div class="pageHead_1">Audit Trail Report
                        
<%
                                       String cdate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date(new Date().getTime()));
                                       String folderName = pdfConstant.AUDITTRAIL;
                                       String genId = cdate + "_" + session.getAttribute("userId");
%>
                        
                                    <span style="float: right;">
                                        <%
                                        if(userType == 1 || userType == 8)
                                            {
                                        %>
                                        <a id="pdfLink" class="action-button-savepdf" href="<%=request.getContextPath()%>/GeneratePdf?reqURL=&reqQuery=&folderName=<%=folderName%>&id=<%=genId%>" >Save As PDF </a>
                                           <%} else {%>
                                        <a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('-1');">Save As PDF </a>
                                        <%}%>

                            &nbsp;
                            <a href="javascript:void(0);" id="print" class="action-button-view">Print</a></span>
                    </div>
                    <div>&nbsp;</div>
                    <div class="formBg_1" id="divSearchBox">                    
                        <table id="tblSearchBox" cellspacing="10" class="formStyle_1" width="100%">
                            <tr>
                                <td width="20%" class="ff"><label id="lblTypeOfUser">Type of User :</label></td>
                                <td width="30%">
                                    <select name="typeOfUser" class="formTxtBox_1" id="typeOfUser" style="width:202px;" >
                                        <option value="">All User Types</option>
                                        <option value="schadmin">Bank Admin</option>
                                        <option value="15">Branch Admin</option>
                                        <option value="8">Content Admin</option>
                                        <option value="devadmin">Development Partner Admin</option>
                                        <option value="6">Development Partner User</option>
                                        <option value="1">e-GP Administrator</option>
                                        <option value="3">Government User</option>
                                        <!--<option value="Media">Media</option>-->
                                        <!--<option value="5">Organization Admin</option>-->
                                        <option value="4">PA Admin</option>
                                        <!--<option value="12">Procurement Expert User</option>-->                                        
                                        <option value="7">Scheduled Financial Institution User</option>                                        
                                        <option value="2">Bidder /Consultant</option>
<!--                                        <option value="16">DG CPTU</option>
                                        <option value="17">Review Panel</option>-->
                                    </select>
                                </td>
                                <td width="20%" class="ff"><label id="lblIpAddress">IP Address :</label></td>
                                <td width="30%"><input type="text" class="formTxtBox_1" id="ipAddress" style="width:202px;" /></td>
                            </tr>
                           <tr>
                                <td width="20%" class="ff"><label id="lblFromDate">Activity From Date and Time :</label></td>
                                <td width="30%">
                                    <input name="fromDate" type="text" class="formTxtBox_1" id="txtFromDate" value="<%=fromDate%>" style="width:100px;" readonly="true"  onfocus="GetCal('txtFromDate','txtFromDate');"/>
                                    <img id="txtFromDateImg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtFromDate','txtFromDateImg');"/>
                                </td>
                                <td width="20%" class="ff"><label id="lblToDate">Activity To Date and Time :</label></td>
                                <td width="30%">
                                    <input name="toDate" type="text" class="formTxtBox_1" id="txtToDate" value="<%=toDate%>" style="width:100px;" readonly="true"  onfocus="GetCal('txtToDate','txtToDate');"/>
                                    <img id="txtToDateImg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('txtToDate','txtToDateImg');"/>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" class="ff"><label id="lblEmailId">e-mail ID :</label></td>
                                <td width="30%">
                                    <input type="text" class="formTxtBox_1" id="ipEmailId" style="width:202px;" />
                                </td>
                                <td width="20%" class="ff"><label id="lblActivity">Activity :</label></td>
                                <td width="30%">
                                    <select name="activity" class="formTxtBox_1" id="activity" style="width:202px;" >
                                        <option value="">All</option>
                                        <%
                                                    MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                                                    for (Object listing : makeAuditTrailService.getAuditActivity("")) {
                                                        String Option = listing.toString();
                                                        Option = Option.replace(" PE ", " PA ");
                                                        Option = Option.replace("PE ", "PA ");
                                                        Option = Option.replace("STD", "SBD");
                                                     
                                                        if(Option.contains(" PE") && !Option.contains(" PEC"))
                                                        {
                                                            Option = Option.replace(" PE", " PA");
                                                        }
                                                        
                                                        out.print("<option value=\"" + listing + "\">" + Option + "</option>");
                                                    }
                                        %>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" class="ff"><label id="lblMDO">Hierarchy Node :</label></td>
                                <td width="30%">
                                    <input class="formTxtBox_1" name="txtdepartment" type="text" style="width: 202px;"
                                           id="txtdepartment" onblur="showHide();checkCondition();"  readonly style="width: 200px;"/>
                                    <input type="hidden"  name="txtdepartmentid" id="txtdepartmentid" />

                                    <a href="javascript:void(0);" onclick="javascript:window.open('<%=request.getContextPath()%>/resources/common/DeptTree.jsp?operation=homeallTenders', '', 'width=350px,height=400px,scrollbars=1','');">
                                        <img style="vertical-align: bottom" height="25" id="deptTreeIcn" src="<%=request.getContextPath()%>/resources/images/deptTreeIcn.png" />
                                    </a>
                                </td>
                                <td width="20%" class="ff"><label id="lblProcuringEntity">Procuring Agency Office :</label></td>
                                <td width="30%">
                                    <select name="procuringEntity" class="formTxtBox_1" id="procuringEntity" style="width:202px;" >
                                        <option value="">--Select Procuring Agency Office--</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%" class="ff">
                                    <select name="cmbTenderId" class="formTxtBox_1" id="cmbTenderId" style="width:202px;" >
                                        <option value="appId">APP ID</option>
                                        <option value="tenderId">Tender ID</option>
                                        <option value="contractId">Contract ID</option>
                                    </select>
                                </td>
                                <td width="30%">
                                    <input type="text" class="formTxtBox_1" id="tenderId" style="width:202px;" maxlength="11"/>
                                </td>
                                <td width="20%" class="ff"><label id="lblModule">Module :</label></td>
                                <td width="30%">
                                    <select name="module" class="formTxtBox_1" id="module" style="width:202px;" onchange="getActivity();">
                                        <option value="">All Modules</option>
                                        <% 
                                           SortedMap<String, EgpModule> map = new TreeMap<String, EgpModule>();
                                           for (EgpModule l : EgpModule.values()) {
                                                map.put(l.getName(), l);
                                           }
                                           for (String egpModule : map.keySet()) {
                                               if(!egpModule.equalsIgnoreCase("Ask Procurement Expert") && !egpModule.equalsIgnoreCase("Message Box")&& !egpModule.equalsIgnoreCase("Watch List"))
                                               {
                                                   String egpModule2 = egpModule;
                                                   egpModule2 = egpModule2.replace("STD", "SBD");
                                                    out.print("<option value=\"" + egpModule + "\">" + egpModule2 + "</option>");
                                               }
                                            }
                                        %>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td align="left">
                                    <label class="formBtn_1">
                                        <input type="button" name="button" id="btnSearch" value="Generate" />
                                    </label>&nbsp;
                                    <label class="formBtn_1">
                                        <input type="button" name="Reset" id="btnReset" value="Reset" />
                                    </label>
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                    <div class="formBg_1" id="divSearhcDisplayPrint">
                        <div id="divSearchDisplay"></div>
                    </div>
                    <table width="100%" cellspacing="0" class="tableList_1 t_space" id="resultTable" cols="@0,6,9">
                        <tr>
                            <th width="4%" class="t-align-center">Sl. <br/>No.</th>
                            <th width="10%" class="t-align-center">e-mail ID</th>
                            <th width="11%" class="t-align-center">User Name</th>
                            <th width="10%" class="t-align-center">Hierarchy Node, Procuring Agency Office</th>
                            <th width="10%" class="t-align-center">Log In Date<br/>and Time</th>
                            <th width="10%" class="t-align-center">Log Out<br/>Date<br/>and Time</th>
                            <th width="10%" class="t-align-center">IP Address</th>
                            <th width="10%" class="t-align-center">Module<br/>Name</th>
                            <th width="10%" class="t-align-center">Page URL Address</th>
                            <th width="15%" class="t-align-center">Activity</th>
                        </tr>
                    </table>
                    <table width="100%" border="0" id="pagination" cellspacing="0" class="pagging_1">
                        <tr>
                            <td align="left">Page <span id="pageNoTot">1</span> - <span id="pageTot">10</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                Total Records : <input type="text" name="size" id="size" value="10" style="width:70px;"/></td>
                            <td align="center"><center>
                                    <input name="textfield3" onkeypress="checkKeyGoTo(event);" type="text" id="dispPage" value="1" class="formTxtBox_1" style="width:20px;" />
                                    &nbsp;
                                    <label class="formBtn_1">
                                        <input type="submit" name="button" id="btnGoto" id="button" value="Go To Page" />
                                    </label></center></td>
                            <td class="prevNext-container">
                                <ul>
                                    <li>&laquo; <a href="javascript:void(0)" id="btnFirst">First</a></li>
                                    <li>&#8249; <a href="javascript:void(0)" id="btnPrevious">Previous</a></li>
                                    <li><a href="javascript:void(0)" id="btnNext">Next</a> &#8250;</li>
                                    <li><a href="javascript:void(0)" id="btnLast">Last</a> &raquo;</li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                    <div align="center">
                        <input type="hidden" id="pageNo" value="1"/>
                    </div>
                </div>
            </td>
        </div>
    </div>
                                    <!--For Generate PDF  Starts-->
                <form id="formstyle" action="" method="post" name="formstyle">
                    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                    <%
                        SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                        String appenddate = dateFormat1.format(new Date());
                    %>
                    <input type="hidden" name="fileName" id="fileName" value="AuditTrail_<%=appenddate%>" />
                    <input type="hidden" name="id" id="id" value="AuditTrail" />
                </form>
                <!--For Generate PDF  Ends-->
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        
        var Interval;
        clearInterval(Interval);
        Interval = setInterval(function(){
            var PageNo = document.getElementById('pageNo').value;
            var Size = document.getElementById('size').value;
            CorrectSerialNumber(PageNo-1,Size); 
        }, 100);
        
        function checkKeyGoTo(e)
        {
            var keyValue = (window.event)? e.keyCode : e.which;
            if(keyValue == 13){
                //Validate();
                $(function() {
                    //$('#btnGoto').click(function() {
                    var pageNo=parseInt($('#dispPage').val(),10);
                    var totalPages=parseInt($('#totalPages').val(),10);
                    if(pageNo > 0)
                    {
                        if(pageNo <= totalPages) {
                            $('#pageNo').val(Number(pageNo));
                            //loadTable();
                            $('#btnGoto').click();
                            $('#dispPage').val(Number(pageNo));
                        }
                    }
                });
                //});
            }
        }
    </script>
</html>
