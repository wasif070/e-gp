<%-- 
    Document   : ViewDesignation
    Created on : Nov 26, 2010, 7:05:44 PM
    Author     : Administrator
--%>

<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");

            int uTypeId = 0;
            
            HttpSession ssn = request.getSession();
            if (ssn.getAttribute("userTypeId") != null) {
                uTypeId = Integer.parseInt(ssn.getAttribute("userTypeId").toString());
            }

            if (request.getParameter("action") != null && request.getParameter("action").equalsIgnoreCase("view") && request.getParameter("ok") == null) {
                // Coad added by Dipal for Audit Trail Log.
                AuditTrail objAuditTrail = new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                String idType = "designationId";
                int auditId = Integer.parseInt(request.getParameter("designationId"));
                String auditAction = "View Designation Details";
                String moduleName = EgpModule.Manage_Users.getName();
                String remarks = "";
                MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                makeAuditTrailService.generateAudit(objAuditTrail, auditId, idType, moduleName, auditAction, remarks);
            }
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Designation</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="mainDiv">
            <div class="dashboard_div">
                <%@include  file="../resources/common/AfterLoginTop.jsp"%>
                <jsp:useBean id="designationDtBean" scope="request" class="com.cptu.egp.eps.web.databean.DesignationDtBean"/>
                <!--Middle Content Table Start-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                            <td class="contentArea_1">
                                <!--Page Content Start-->
                            <%                                String strDesigId = request.getParameter("designationId");
                                if (strDesigId != null && !strDesigId.isEmpty()) {

                            %>
                            <!-- Success failure -->
                            <!-- Result Display End-->
                            <div>
                                <div class="pageHead_1">View Designation Details</div>
                            </div>
                            <%                                int designationId = Integer.parseInt(strDesigId);
                                designationDtBean.populateInfo(designationId);
                                //Emtaz on 24/April/2016 Organization->Organization/PA
                                String DepartmentType = "";
                                if (designationDtBean.getDeptType().equalsIgnoreCase("Organization")) {
                                    DepartmentType = "Organization";
                                } else {
                                    DepartmentType = designationDtBean.getDeptType();
                                }
                                if (designationDtBean.isDataExists()) {
                                    String editLink = "EditDesignation.jsp?designationId=" + designationId;
                            %>
                            <form id="frmCreateDesignation" action="<%=request.getContextPath()%>/DesignationServlet" method="POST">
                                <table border="0" width="100%" cellspacing="10" cellpadding="0" class="formStyle_1">
                                    <div class="t_space">
                                        <%
                                            String msg = request.getParameter("msg");
                                            if (msg != null && msg.equals("success")) {
                                                String from = request.getParameter("from");
                                                if (from == null || from.isEmpty()) {
                                                    from = "Operation";
                                                }
                                        %>
                                        <div class="responseMsg successMsg">Designation <%=from%> successfully</div>
                                        <%}%>
                                    </div>
                                    <tr>
                                        <td class="ff" width="15%"><% //Emtaz on 24/April/2016 Organization->Organization

                                            if (designationDtBean.getDeptType().equalsIgnoreCase("Organization")) {
                                                out.print("Organization");
                                            } else {
                                                out.print(designationDtBean.getDeptType());
                                            }%> : 
                                        </td>
                                        <td width="85%"><label>${designationDtBean.departmentName}</label>
                                            <input type="hidden" name="txtdepartmentid" value="${designationDtBean.departmentId}"/>
                                            <input type="hidden" name="action" value="update"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ff">Designation : </td>
                                        <td>
                                            <label>${designationDtBean.designationName}</label>
                                            <input name="designationId" type="hidden" value="${designationDtBean.designationId}"/>
                                        </td>
                                    </tr>
                                    <tr Style = "display: none;">
                                        <td class="ff">Class - Grade : </td>
                                        <td>
                                            <label>${designationDtBean.gradeName}-${designationDtBean.gradeLevel}</label>
                                            <input type="hidden" name="grade" value="${designationDtBean.gradeId}"/>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" cellspacing="10" cellpadding="0" border="0">
                                    <tr>
                                        <td width="12%">&nbsp;</td>
                                        <td width="88%" class="t-align-left" >
                                            <a href="ManageDesignation.jsp" class="anchorLink">Ok</a> &nbsp; 
                                            
                                            <%if(uTypeId != 1){%>
                                            <a href="<%=editLink%>" class="anchorLink">Edit</a>
                                            <%}%>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <%
                            } else {%>
                            <div class="responseMsg errorMsg">Data NOT Found for Designation Id: <%=designationId%></div>
                            <%           }
                            } else {%>
                            <div class="responseMsg errorMsg">Plz Provide Designation Id</div>
                            <%}%>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Middle Content Table End-->
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
        </div>
        <script>
            var headSel_Obj = document.getElementById("headTabMngUser");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
