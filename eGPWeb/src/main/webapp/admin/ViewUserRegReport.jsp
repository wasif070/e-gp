<%--
    Document   : View RegistrationFeePaymentDetails Report
    Created on : Jan 22, 2011, 1:57:10 AM
    Author     : Administrator
--%>

<%@page import="java.net.URLEncoder"%>
<%@page import="com.cptu.egp.eps.web.utility.EncryptDecryptUtils"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonSearchDataMoreService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPCommonSearchDataMore"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonTenderDetails" %>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<jsp:useBean id="pdfConstant" class="com.cptu.egp.eps.web.utility.GeneratePdfConstant" />
<jsp:useBean id="pdfCmd" class="com.cptu.egp.eps.web.servicebean.GenreatePdfCmd" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration Payment Details</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
         <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_003.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery-ui.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery_002.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/main.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/print/jquery.txt"></script>
    </head>
    <jsp:useBean id="tenderInfoServlet" class="com.cptu.egp.eps.web.servicebean.TenderSrBean"/>
    <body>
        <div class="dashboard_div">
          
                <!--Dashboard Content Part Start-->
                <%
                    TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    boolean isBankUser=false;
                    boolean isBranchMaker=false, isBranchChecker=false, isBankChecker=false, isRedirect=false;
                    String userBranchId="";
                    String searchCriteria="";
                    String userId="", payUserId="", regPaymentId="", redirectPath="";
                %>


                <%

                //HttpSession hs = request.getSession();
                //if (hs.getAttribute("userId") != null) {
                //     userId = hs.getAttribute("userId").toString();
                //} else {
                //     response.sendRedirect(request.getContextPath() + "/SessionTimedOut.jsp");
                //}

                String referer = "";
                if (request.getHeader("referer") != null) {
                    referer = request.getHeader("referer");
                }

                String isPDF = "abc";
                if (request.getParameter("isPDF") != null) {
                    isPDF = request.getParameter("isPDF");
                }

                if (request.getParameter("uId") != null) {
                    payUserId = request.getParameter("uId");
                }

                if (request.getParameter("payId") != null) {
                    regPaymentId = request.getParameter("payId");
                }
                if (request.getParameter("searchCriteria") != null) {
                    searchCriteria = request.getParameter("searchCriteria");
                }
                System.out.println("search criteria :"+searchCriteria);
                System.out.println("hidden :"+request.getParameter("fromLink"));
                String folderName = folderName = pdfConstant.REGISTRATIONRENEWAL;
                String genId = payUserId+"_"+ regPaymentId;
                //swati--to generate pdf
                if (!(isPDF.equalsIgnoreCase("true"))) {
                    try{
                     String reqURL = request.getRequestURL().toString();
                     String reqQuery = "payId="+regPaymentId+"&uId="+payUserId;
                     pdfCmd.genrateCmd(reqURL, reqQuery, folderName, genId);
                     }
                    catch(Exception e){
                         e.printStackTrace();
                     }
                 }
                //end

                /* ************************* Get User Branch Id ************************** */
                List<SPTenderCommonData> lstCurBankUserRole = tenderCommonService.returndata("getBankUserRole",userId,null);
                if(!lstCurBankUserRole.isEmpty()){
                    if("BranchMaker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBranchMaker=true;
                    } else if("BranchChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBranchChecker=true;
                    } else if("BankChecker".equalsIgnoreCase(lstCurBankUserRole.get(0).getFieldName1())){
                        isBankChecker=true;
                    }
                    userBranchId=lstCurBankUserRole.get(0).getFieldName2();
                }
                %>
            <!--Dashboard Header Start-->
            <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            <%} %>
            <!--Dashboard Header End-->
                <div>&nbsp;</div>
                <div class="contentArea_1">
                    <div class="pageHead_1">Registration Payment Details
                         <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                        <span style="float:right;">
                            <a class="action-button-savepdf" href="<%=request.getContextPath()%>/GeneratePdf?reqURL=&reqQuery=&folderName=<%=folderName%>&id=<%=genId%>" >Save As PDF </a>&nbsp;
                            <a href="javascript:void(0);" id="print" class="action-button-view">Print</a>&nbsp;
                            <a href="UserRegReport.jsp?searchCriteria=<%=URLEncoder.encode(searchCriteria,"UTF-8")%>" class="action-button-goback">Go Back</a>
                        </span>
                        <% } %>
                    </div>
                    <div class="tabPanelArea_1 t_space">
                         <%
                            String bidderUserId="0", bidderEmail="NA";

                            String emailId="";

                             /* ************************* Get Email Id ************************** */
                            List<SPTenderCommonData> lstTendererEml = tenderCommonService.returndata("getEmailIdfromUserId",payUserId,null);
                            if(lstTendererEml != null && lstTendererEml.size() > 0)
                            {
                                emailId=lstTendererEml.get(0).getFieldName1();
                                bidderEmail=emailId;
                            }


                CommonSearchDataMoreService dataMore = (CommonSearchDataMoreService) AppContext.getSpringBean("CommonSearchDataMoreService");
                List<SPCommonSearchDataMore> lstPaymentDetail =null;
                lstPaymentDetail = dataMore.geteGPData("getRegistrationFeePaymentDetail", regPaymentId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

                             /* ************************* Get Payment Details ************************** */
                       // List<SPTenderCommonData> lstPaymentDetail = tenderCommonService.returndata("getRegistrationFeePaymentDetail", regPaymentId, null);
                
                            if (!lstPaymentDetail.isEmpty()) {
                                
                    %>
                    <div  id="print_area">
                        <div id='titleDiv' class="pageHead_1" style="display: none;">User Registration and Payment Report</div>
                        <%if(bidderEmail.trim().equalsIgnoreCase("NA")){ %>
                            <b>NA = Not Available</b>
                          <%}%>
                        <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td width="<%
                                if("true".equalsIgnoreCase(isPDF)){
                                    out.print("25%");
                                }else{
                                    out.print("18%");
                                }
                            %>" class="ff">e-mail ID :</td>
                            <td><%=bidderEmail%></td>
                        </tr>
                        <tr>
                            <td class="ff">Financial Institute Name :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName12()%></td>
                        </tr>
                        <%if(!"Online".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())){%>
                            <tr>
                                <td class="ff">Branch Name :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName1()%></td>
                            </tr>
                            <tr>
                                <td class="ff">Branch Maker :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName2()%></td>
                            </tr>
                            <%--<tr>
                                <td class="ff">Branch Checker :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName11()%></td>
                            </tr>--%>
                            <tr>
                                <td class="ff">Branch Checker :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName11()==null?"-":lstPaymentDetail.get(0).getFieldName11()%></td>
                            </tr>
                            <%if("yes".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName8())){%>
                            <tr>
                                <td class="ff">Verified Date :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName13()%></td>
                            </tr>
                            <%}%>
                        <%}else{%>
                            <tr>
                                <td class="ff">Card Number :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName14()%></td>
                            </tr>
                            <tr>
                                <td class="ff">Transaction Id:</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName15()%></td>
                            </tr>
                        <%}%>
                        <tr>
                            <td class="ff">Currency :</td>
                            <td><% if(lstPaymentDetail.get(0).getFieldName3().equalsIgnoreCase("USD")){out.print("USD");} else {out.print("Nu.");} %></td>
                        </tr>
                        <tr>
                            <td class="ff">Amount :</td>
                            <td>
                                <%-- changed by Emtaz on 13/April/2016. Taka->Nu. --%>
                               <%if("BTN".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%} else if("USD".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>$</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%} else if("Nu.".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName3())){%>
                                    <label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName4()%>
                                <%}%>
                            </td>
                        </tr>
                         <%if(lstPaymentDetail.get(0).getFieldName5().equalsIgnoreCase("Online")){%>
                            <tr>
                                <td class="ff" >Service Charge :</td>
                                <td><label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName16() %></td>
                            </tr>
                            <tr>
                                <td class="ff" >Total Amount :</td>
                                <td><label>Nu.</label>&nbsp;<%=lstPaymentDetail.get(0).getFieldName17() %></td>
                            </tr>
                        <%}%>
                         <tr>
                            <td class="ff">Mode of Payment :</td>
                            <td><%=lstPaymentDetail.get(0).getFieldName5()%>
                            </td>
                        </tr>
                        <%if("Pay Order".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = tenderCommonService.returndata("getRegistrationFeePaymentDetailMore", regPaymentId, null);
                                if(!lstPaymentDetailMore.isEmpty()){%>
                                <tr>
                                    <td class="ff">Instrument No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                              
                                <tr>
                                    <td class="ff">Issuing Financial Institute :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName2()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuing Financial Institute Branch :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Issuance Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName4()%></td>
                                </tr>
                                <tr>
                                    <td class="ff">Validity Date :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName5()%></td>
                                </tr>
                               <% }
                            }%>
                            <%if("Account to Account Transfer".equalsIgnoreCase(lstPaymentDetail.get(0).getFieldName5())) {
                                List<SPTenderCommonData> lstPaymentDetailMore = tenderCommonService.returndata("getRegistrationFeePaymentDetailMore", regPaymentId, null);

                                if(!lstPaymentDetailMore.isEmpty()){%>

                                <tr>
                                    <td class="ff">Account No. :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName1()%></td>
                                </tr>
                                <%if(!"".equalsIgnoreCase(lstPaymentDetailMore.get(0).getFieldName3())){%>
                                <tr>
                                    <td class="ff">Branch Name :</td>
                                    <td><%=lstPaymentDetailMore.get(0).getFieldName3()%></td>
                                </tr>
                                <%}%>
                               <% }
                            }%>
                            <tr>
                                <td class="ff">Date and Time of Payment :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName6()%></td>
                            </tr>
                            <tr>
                                <td class="ff">Remarks :</td>
                                <td><%=lstPaymentDetail.get(0).getFieldName7()%></td>
                            </tr>
                            <%if(!lstPaymentDetail.get(0).getFieldName5().trim().equalsIgnoreCase("Online") && lstPaymentDetail.get(0).getFieldName11()==null){%>
                            <tr>
                                <td class="ff" colspan="2" style="color:#FF0000">Payment Verification is Pending</td>
                            </tr>
                            <%}%>
                    </table>
                    <% } else {%>
                    <table border="0" cellspacing="0" cellpadding="0" class="tableList_1 t_space" width="100%">
                        <tr>
                            <td>Payment information not found!</td>
                        </tr>
                    </table>
                    <%}%>
                </div>
                </div>
                <script type="text/javascript">
                     /* ************************* Print Report ************************** */
                    $(document).ready(function() {
                        $("#print").click(function() {
                            //alert('sa');
                            printElem({ leaveOpen: true, printMode: 'popup' });
                        });
                    });
                    function printElem(options){
                        $('#titleDiv').show();
                        $('#print_area').printElement(options);
                        $('#titleDiv').hide();
                    }
                </script>
                <!--Dashboard Content Part End-->
                 <% if (!(isPDF.equalsIgnoreCase("true"))) { %>
                <!--Dashboard Footer Start-->
                <div align="center"><%@include file="../resources/common/Bottom.jsp" %></div>
                <!--Dashboard Footer End-->
                <% } %>
        </div>
    </body>
</html>
