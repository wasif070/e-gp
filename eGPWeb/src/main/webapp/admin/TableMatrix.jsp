<%--
Document   : CreateForm
Created on : 24-Oct-2010, 4:49:09 PM
Author     : yanki
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.cptu.egp.eps.model.table.TblListBoxMaster"%>
<%@page import="javax.swing.ListCellRenderer"%>
<%@page import="com.cptu.egp.eps.model.table.TblListCellDetail"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<jsp:useBean id="tableMatrix"  class="com.cptu.egp.eps.web.servicebean.TemplateTableSrBean" />
<jsp:useBean id="comboService" class="com.cptu.egp.eps.web.servicebean.ComboSrBean" />
<%@page import="java.util.ListIterator" %>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateColumns" %>
<%@page import="com.cptu.egp.eps.model.table.TblTemplateCells" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Create form matrix</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <%--<script type="text/javascript" src="../resources/js/pngFix.js"></script>--%>
        <script type="text/javascript" src="../resources/js/form/Add.js"></script>
        <script type="text/javascript" src="../resources/js/form/CommonValidation.js"></script>
        <script type="text/javascript" src="../resources/js/form/TableMatrix.js" ></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.1.js"></script>
        <script type="text/javascript" src="resources/js/jQuery/jquery.validate.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2_1.js"></script>
        <script  type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>

        <script type="text/javascript" src="../resources/js/jQuery/jquery.alerts.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <%
                    short templateId = 0;
                    int sectionId = 0;
                    int formId = 0;
                    int tableId = 0;

                    if (request.getParameter("templateId") != null) {
                        templateId = Short.parseShort(request.getParameter("templateId"));
                    }
                    if (request.getParameter("sectionId") != null) {
                        sectionId = Integer.parseInt(request.getParameter("sectionId"));
                    }
                    if (request.getParameter("formId") != null) {
                        formId = Integer.parseInt(request.getParameter("formId"));
                    }
                    if (request.getParameter("tableId") != null) {
                        tableId = Integer.parseInt(request.getParameter("tableId"));
                    }

                    String logUserId = "0";
                    if (session.getAttribute("userId") != null) {
                        logUserId = session.getAttribute("userId").toString();
                    }
                    tableMatrix.setLogUserId(logUserId);
                    comboService.setLogUserId(logUserId);

        // Getting Combo details
                    List<Object[]> objComboCalWise = comboService.getListBoxCatWise(formId, "Yes");
                    List<Object[]> objComboWOCalWise = comboService.getListBoxCatWise(formId, "No");

        // Getting Table details
                    java.util.List<com.cptu.egp.eps.model.table.TblTemplateTables> tblInfo = tableMatrix.getTemplateTablesDetail(tableId);
                    short cols = 0;
                    short rows = 0;
                    String tableName = "";
                    if (tblInfo != null) {
                        if (tblInfo.size() >= 0) {
                            tableName = tblInfo.get(0).getTableName();
                            cols = tblInfo.get(0).getNoOfCols();
                            rows = tblInfo.get(0).getNoOfRows();
                        }
                    }
                    boolean isBOQForm = tableMatrix.isPriceBidForm(formId);
                    boolean isInEditMode = tableMatrix.isEntryPresentForCols(tableId);
                    if (!isInEditMode) {
                        if (!(isBOQForm) && (rows == 0 || rows == -1)) {
                            rows = 1;
                        }
                    }
                    if (isBOQForm) {
                        rows = 0;
                    }

        //if(isInEditMode){
        //    cols = tableMatrix.getNoOfColsInTable(tableId);
        //    rows = tableMatrix.getNoOfRowsInTable(tableId, (short) 1);
        //}

        // Getting table column details
                    java.util.ListIterator<com.cptu.egp.eps.model.table.TblTemplateColumns> tblColumnsDtl = tableMatrix.getColumnsDtls(tableId, true).listIterator();
        // Getting table column cell details
                    java.util.ListIterator<com.cptu.egp.eps.model.table.TblTemplateCells> tblCellsDtl = tableMatrix.getCellsDtls(tableId).listIterator();
                    String colHeader = "";
                    byte filledBy = 0;
                    byte dataType = 0;
                    String showHide = "";
                    String colType = "";
                    short sortOrder = 0;

                    short fillBy[] = new short[cols];
        %>
        <script language="javascript">
            var cmbWithCalcText = new Array();
            var cmbWithCalcValue = new Array();
            var cmbWoCalcText = new Array();
            var cmbWoCalcValue = new Array();
           var isAdmin ='Yes';
            <%
                    if (objComboCalWise.size() > 0) {
                        for (int i = 0; i < objComboCalWise.get(0).length; i++) {%>
                            cmbWithCalcText.push("<%=objComboCalWise.get(0)[i]%>");
                            cmbWithCalcValue.push("<%=objComboCalWise.get(1)[i]%>");
            <%  }
                    }%>
            <%
                    if (objComboWOCalWise.size() > 0) {
                        for (int i = 0; i < objComboWOCalWise.get(0).length; i++) {%>
                            cmbWoCalcText.push("<%=objComboWOCalWise.get(0)[i]%>");
                            cmbWoCalcValue.push("<%=objComboWOCalWise.get(1)[i]%>");
            <%  }
                    }%>
        </script>
        <script language="javascript">

            function changecombowithCal(value,colCount){
                var rowcount = document.getElementById("rows").value;
                for(var i=1; i <=rowcount;i++)
                {
                    var newId = parseInt(colCount) + 1;
                    id = "selComboWithCalc"+ i +"_"+newId;
                    document.getElementById(id).options[value].selected=true;
                }
            }

            function changecomboWOCal(value,colCount){
                var rowcount = document.getElementById("rows").value;
                for(var i=1; i <=rowcount ;i++)
                {
                    var newId = parseInt(colCount) + 1;
                    id = "selComboWithOutCalc"+ i +"_"+newId;
                    document.getElementById(id).options[value].selected=true;
                }
            }
            function checkParentVal(childId, parentId, selectVal, cellAddress){
                //alert('cellAddress '+cellAddress);
                //alert("Ketan Prajapati");
                var vIsBOQForm='<%=isBOQForm%>';
                if(vIsBOQForm == 'false'){
                    // alert('test '+vIsBOQForm+' selectVal '+selectVal+ ' cellAddress '+cellAddress);
                    // alert(document.getElementById('comboWithCalc'+cellAddress))
                    //alert(document.getElementById('comboWithOutCalc'+cellAddress));
                    if(document.getElementById('comboWithCalc'+cellAddress))
                    {
                        document.getElementById('comboWithCalc'+cellAddress).style.display = 'none';
                    }
                    if(document.getElementById('comboWithOutCalc'+cellAddress))
                    {
                        document.getElementById('comboWithOutCalc'+cellAddress).style.display = 'none';
                    }

                    if(document.getElementById('comboWithCalc'+cellAddress) && selectVal == 9) // combo with calc
                    {
                        document.getElementById('comboWithCalc'+cellAddress).style.display = '';
                    }
                    if(document.getElementById('comboWithOutCalc'+cellAddress) && selectVal == 10) // combo without calc
                    {
                        document.getElementById('comboWithOutCalc'+cellAddress).style.display = '';
                    }
                    return true;
                }
                else
                {
                    if(document.getElementById(parentId).value!=selectVal){
                        var selIndex = document.getElementById(parentId).selectedIndex;
                        document.getElementById(childId).options[selIndex].selected=true;
                        jAlert("You can't Change Data Type", "Alert");
                        return false;
                    }
                }
            }
            var vDefDataType;
            function getCurrentDataType(dataType)
            {
                vDefDataType = dataType.options[dataType.options.selectedIndex].value;
                //alert("vDefDataType : "+vDefDataType);
            }
            function changeDataType(frm,dataType)
            {
                var vOldDataType = document.getElementById(dataType).value;
                var vIsInEditMode='<%=isInEditMode%>';
                var vIsBOQForm='<%=isBOQForm%>';
                if(vIsInEditMode == 'true' && vIsBOQForm == 'true'){
                    jAlert("Data Type can not be changed.","Edit Table Matrix",function(RetVal) {
                    });

                    document.getElementById(dataType).value = vDefDataType;
                    return false;
                }
                else
                {
                    return true;
                }
            }
            function GetCal(txtname,controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    dateFormat:"%d-%b-%Y",
                    onSelect: function() {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
        </script>
    </head>
    <body>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <!--Middle Content Table Start-->
            <div class="contentArea_1">
                <form action="<%=request.getContextPath()%>/CreateSTDForm?action=tableMatrix" method="post" name="frmTableCreation" id="frmTableCreation">
                    <input type="hidden" name="templateId" value="<%=templateId%>" />
                    <input type="hidden" name="sectionId" value="<%=sectionId%>" />
                    <input type="hidden" name="formId" id="formId" value="<%=formId%>" />
                    <input type="hidden" name="tableId" value="<%=tableId%>" />
                    <input type="hidden" name="cols" id="cols" value="<%=cols%>" />
                    <input type="hidden" name="rows" id="rows" value="<%=rows%>" />
                    <input type="hidden" name="rcount" id="rcount" value="<%=cols%>" />
                    <input type="hidden" name="clcount" id="clcount" value="<%=rows%>" />
                    <input type="hidden" name="delRow" id="delRow" value="0"/>
                    <input type="hidden" name="delCol" id="delCol" value="0"/>
                    <input type="hidden" name="delColArray" id="delColArr" value="0"/>
                    <div class="pageHead_1">
                        Create Table
                        <span style="float: right; text-align: right;">
                            <a class="action-button-goback" href="TableDashboard.jsp?templateId=<%= templateId%>&sectionId=<%= sectionId%>&formId=<%= formId%>" title="Form Dashboard">Form Dashboard</a>
                        </span>
                    </div>
                    <table width="100%" class="tableView_1 t_space">
                        <%--<tr>
                        <td width="100" class="ff">Form Name :</td>
                        <td>&nbsp;</td>
                        </tr>--%>
                        <tr>
                            <td width="100" class="ff">Table Name : </td>
                            <td><%=tableName%></td>
                        </tr>
                    </table>
                    <table width="100%"  class="tableView_1 t_space">
                        <tr>
                            <%if (!isBOQForm) {%>
                            <td align="left">
                                <a href="#" class="action-button-add" onclick="addRow(document.getElementById('frmTableCreation'),cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue, isAdmin);" title="Add Row">Add Row</a>
                                <a href="#" class="action-button-delete" onclick="delTableRow(document.getElementById('frmTableCreation'));" title="Delete Row">Delete Row</a>
                            </td>
                            <%}%>
                            <td align="right">
                                <a href="#" class="action-button-add" onclick="addCol(document.getElementById('frmTableCreation'), '<%= isBOQForm%>',cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue);" title="Add Column">Add Column</a>
                                <a href="#" class="action-button-delete" onclick="delTableCol(document.getElementById('frmTableCreation'),  '<%= isBOQForm%>',cmbWithCalcText,cmbWithCalcValue,cmbWoCalcText,cmbWoCalcValue);" title="Delete Column">Delete Column</a>
                            </td>
                        </tr>
                    </table>
                    <div style="overflow: auto;width: 100%;display: inline-block">
                        <table width="100%" cellspacing="0" class="tableList_1 t_space" id="FormMatrix">
                            <tbody>
                                <%
                                            for (short i = -1; i <= rows; i++) {
                                                if (i == -1) {
                                %>
                                <tr id="ChkCol">
                                    <th width="2%" colspan="2">&nbsp;</th>
                                    <%--<th width="2%">&nbsp;</th>--%>
                                    <%
                                        for (short j = 0; j < cols; j++) {
                                    %>
                                    <th style="text-align:center;" id="TDChkDel<%=  j + 1%>">
                                        <input type="checkbox" name="ChkDel<%= j + 1%>" id="ChkDel<%= j + 1%>"  />
                                    </th>
                                    <%
                                        }
                                    %>
                                </tr>
                                <%
                                    }
                                    if (i == 0) {
                                %>
                                <tr id="ColumnRow">
                                    <td width="2%" colspan="2">&nbsp;</td>
                                    <%--<td width="2%">&nbsp;</td>--%>
                                    <%
                                        for (short j = 0; j < cols; j++) {
                                            sortOrder = (short) (j + 1);
                                            if (tblColumnsDtl.hasNext()) {
                                                TblTemplateColumns ttc = tblColumnsDtl.next();
                                                colHeader = ttc.getColumnHeader();
                                                colType = ttc.getColumnType();
                                                filledBy = ttc.getFilledBy();
                                                dataType = ttc.getDataType();
                                                showHide = ttc.getShoworhide();
                                                sortOrder = ttc.getSortOrder();
                                            }
                                            fillBy[j] = filledBy;
                                    %>
                                    <td id="addTD<%= j + 1%>" colid="<%= j + 1%>">
                                        <table width="100%" id="TB1" border="0" cellspacing="5" cellpadding="0" class="formStyle_1">
                                            <tr id="TBR1">
                                                <td width="30%" class="ff" id="TBD1">Column Header :</td>
                                                <td width="70%" id="TBD2"><textarea rows="3" class="formTxtBox_1" name="Header<%=j%>" id="Header<%=j%>" style="width:98%;"><%= colHeader.replace("<br/>", "\n")%></textarea></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Filled By :</td>
                                                <td><select class="formTxtBox_1" name="FillBy<%=j%>" id="FillBy<%=j%>" onChange="setDisable(this.form,this,<%=(j + 1)%>);" style="width: 150px;">
                                                        <option value="1" <%if (filledBy == 1) {
                                                                out.print("selected");
                                                            }%> >PA User</option>
                                                        <option value="2" <%if (filledBy == 2) {
                                                                out.print("selected");
                                                            }%>>Bidder/Consultant</option>
                                                        <option value="3" <%if (filledBy == 3) {
                                                                out.print("selected");
                                                            }%>>Auto</option>
                                                       <option value="4" <%if (filledBy == 4) {
                                                                out.print("selected");
                                                            }%>>Bidder Quoted Amount</option>
                                                            <option value="5" <%if (filledBy == 5) {
                                                                out.print("selected");
                                                            }%>>Discount Amount Percentage</option>
                                                    </select></td>
                                            </tr>
                                            <tr id="dt<%=j%>">
                                                <td class="ff">Data Type :</td>
                                                <td id="DataTypeTd<%=j%>">
                                                    <select class="formTxtBox_1" name="DataType<%= j%>" id="DataType<%= j%>" onChange="changeAllCombo(this.form,this,<%= j + 1%>);" style="width: 150px;">
                                                        <option value="1" <%if (dataType == 1) {
                                                                out.print("selected");
                                                            }%>>Small Text</option>
                                                        <option value="2" <%if (dataType == 2) {
                                                                out.print("selected");
                                                            }%>>Long Text</option>
                                                        <option value="3" <%if (dataType == 3) {
                                                                out.print("selected");
                                                            }%>>Money Positive</option>
                                                        <option value="4" <%if (dataType == 4) {
                                                                out.print("selected");
                                                            }%>>Numeric</option>
                                                        <option value="8" <%if (dataType == 8) {
                                                                out.print("selected");
                                                            }%>>Money All</option>
                                                        <option value="9" <%if (dataType == 9) {
                                                                out.print("selected");
                                                            }%>>Combo Box with Calculation</option>
                                                        <option value="10" <%if (dataType == 10) {
                                                                out.print("selected");
                                                            }%>>Combo Box w/o Calculation</option>
                                                        <option value="11" <%if (dataType == 11) {
                                                                out.print("selected");
                                                            }%>>Money All(-5 to +5)</option>
                                                        <option value="12" <%if (dataType == 12) {
                                                                out.print("selected");
                                                            }%>>Date</option>
                                                        <option value="13" <%if (dataType == 13) {
                                                                out.print("selected");
                                                            }%>>Money Positive(3 digits after decimal)</option>
                                                    </select>
                                                    <%
                                                        int cmbIndex = 0;
                                                    %>
                                                    <span id="comboWithCalc<%= j%>" <% if (dataType == 9) {%>style="display: block;"<% } else {%>style="display: none;"<% }%>>
                                                        <select name="selComboWithCalc<%=j%>" id="selComboWithCalc<%=j%>"  class="formTxtBox_1" onchange="changecombowithCal(this.selectedIndex,<%=j%>);">
                                                            <% for (cmbIndex = 0; cmbIndex < objComboCalWise.get(0).length; cmbIndex++) {%>
                                                            <option value="<%=objComboCalWise.get(1)[cmbIndex]%>"><%=objComboCalWise.get(0)[cmbIndex]%></option>
                                                            <% }%>
                                                        </select>
                                                    </span>
                                                    <span id="comboWithOutCalc<%= j%>" <% if (dataType == 10) {%>style="display: block;"<% } else {%>style="display: none;"<% }%>>
                                                        <select name="selComboWithOutCalc<%=j%>" id="selComboWithOutCalc<%=j%>"  class="formTxtBox_1" onchange="changecomboWOCal(this.selectedIndex,<%=j%>);">
                                                            <% for (cmbIndex = 0; cmbIndex < objComboWOCalWise.get(0).length; cmbIndex++) {%>
                                                            <option value="<%=objComboWOCalWise.get(1)[cmbIndex]%>"><%=objComboWOCalWise.get(0)[cmbIndex]%></option>
                                                            <% }%>
                                                        </select>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="ff" >Show / Hide :</td>
                                                <td><select class="formTxtBox_1" name="ShowOrHide<%= j%>" id="ShowOrHide<%= j%>" style="width: 150px;">
                                                        <option value="1" <%if ("1".equals(showHide)) {
                                                                out.print("selected");
                                                            }%>>Show</option>
                                                        <option value="2" <%if ("2".equals(showHide)) {
                                                                out.print("selected");
                                                            }%>>Hide</option>
                                                    </select></td>
                                            </tr>
                                            <tr <%if (isBOQForm) {
                                                } else {
                                                    out.print(" style='display:none' ");
                                                }%> >
                                                <td class="ff">Add Column Type :</td>
                                                <td><select class="formTxtBox_1" id="columnType<%= j%>" name="columnType<%= j%>" style="width: 150px;">
                                                        <option value="1" <%if ("1".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Normal</option>
                                                        <option value="2" <%if ("2".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Qty</option>
                                                        <option value="3" <%if ("3".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Qty by Bidder/Consultant</option>
                                                        <option value="4" <%if ("4".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>EE Unit Rate</option>
                                                        <option value="5" <%if ("5".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>EE Total Rate</option>
                                                        <option value="6" <%if ("6".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Unit Rate</option>
                                                        <option value="7" <%if ("7".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Total Rate</option>
                                                        <option value="8" <%if ("8".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Currency</option>
                                                         <option value="9" <%if ("9".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Local Labour</option>
                                                        <option value="10" <%if ("10".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Supplier VAT</option>
                                                             <option value="11" <%if ("11".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Inland and Others</option>
                                                             <option value="12" <%if ("12".equals(colType)) {
                                                                out.print("selected");
                                                            }%>>Market Price per unit By PA</option>
                                                    </select></td>
                                            </tr>
                                            <tr>
                                                <td class="ff">Sort Order :</td>
                                                <td>
                                                    <input type="text" class="formTxtBox_1" id="SortOrder<%=j%>" name="SortOrder<%=j%>" style="width:25px;" value="<%= sortOrder%>" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <%
                                        }
                                    %>
                                </tr>
                                <%
                                    }
                                    if (i > 0) {

                                %>
                                <tr id="TR<%=i%>">
                                    <td align="center" >
                                        <input type="checkbox" name="chk" id="chk<%=i%>" />
                                    </td>
                                    <td align="center">
                                        <select class="formTxtBox_1" name="rowsort<%=i%>" id="rowsort<%=i%>">
                                            <%
                                                for (int scount = 1; scount <= rows; scount++) {
                                            %>
                                            <option value=<%= scount%>
                                                    <%
                                                        if (i == scount) {
                                                            out.print(" selected ");
                                                        }
                                                    %>
                                                    ><%=scount%></option>
                                            <%
                                                }
                                            %>
                                        </select>
                                    </td>
                                    <%

                                        int cnt = 0;
                                        short columnId;
                                        int cellId = 0;
                                        List<TblListCellDetail> listCellDetail = new ArrayList<TblListCellDetail>();
                                        for (int j = 0; j < cols; j++) {
                                            if (isInEditMode) { // Edit Table Matrix
                                                String cellValue = "";
                                    %>
                                    <td id="TD<%= i%>_<%= j + 1%>" align="center">
                                        <%

                                            if (tblCellsDtl.hasNext()) {
                                                cnt++;
                                                TblTemplateCells cells = tblCellsDtl.next();
                                                dataType = cells.getCellDatatype();
                                                filledBy = cells.getCellDatatype();
                                                cellValue = cells.getCellvalue();
                                                columnId = cells.getColumnId();
                                                cellId = cells.getCellId();
                                                listCellDetail = tableMatrix.getListCellDetail(tableId, columnId, cellId);

                                                if (fillBy[j] == 2) { // Filled By Tenderer
                                        %>
                                        <select class="formTxtBox_1" name="DataType<%= i%>_<%= j + 1%>" id="DataType<%= i%>_<%= j + 1%>" style="width: 98%" onchange="checkParentVal('DataType<%= i%>_<%= j + 1%>','DataType<%=j%>',this.value, '<%= i%>_<%= j + 1%>');" onblur="checkParentVal('DataType<%= i%>_<%= j + 1%>','DataType<%=j%>',this.value, '<%= i%>_<%= j + 1%>');">
                                            <option value="1" <%if (dataType == 1) {
                                                    out.print("selected");
                                                }%>>Small Text</option>
                                            <option value="2" <%if (dataType == 2) {
                                                    out.print("selected");
                                                }%>>Long Text</option>
                                            <option value="3" <%if (dataType == 3) {
                                                    out.print("selected");
                                                }%>>Money Positive</option>
                                            <option value="4" <%if (dataType == 4) {
                                                    out.print("selected");
                                                }%>>Numeric</option>
                                            <option value="8" <%if (dataType == 8) {
                                                    out.print("selected");
                                                }%>>Money All</option>
                                            <option value="9" <%if (dataType == 9) {
                                                    out.print("selected");
                                                }%>>Combo Box with Calculation</option>
                                            <option value="10" <%if (dataType == 10) {
                                                    out.print("selected");
                                                }%>>Combo Box w/o Calculation</option>
                                            <option value="11" <%if (dataType == 11) {
                                                    out.print("selected");
                                                }%>>Money All(-5 to +5)</option>
                                            <option value="12" <%if (dataType == 12) {
                                                    out.print("selected");
                                                }%>>Date</option>
                                            <option value="13" <%if (dataType == 13) {
                                                    out.print("selected");
                                                }%>>Money Positive(3 digits after decimal)</option>
                                        </select>

                                        <%
                                        } else { // Filled By PE User or Auto
                                            if (dataType == 1 || dataType == 3 || dataType == 4 || dataType == 8 || dataType == 11 || dataType == 13 ) {
                                        %>
                                        <input type="text" class="formTxtBox_1" style="width:98%; height: 30px"
                                               name="Cell<%= i%>_<%= j + 1%>" id="Cell<%= i%>_<%= j + 1%>"
                                               <% if (fillBy[j] == 3) {
        out.print(" disabled ");
        out.print(" value=\"disabled\" ");
    } else {%>
                                               value="<%= cellValue.replace("<br/>", "\n")%>"
                                               <%}%>
                                               />
                                        <%
                                        } else if (dataType == 2) { // DataType -  Long Text
                                        %>
                                        <textarea class="formTxtBox_1" rows="3" name="Cell<%= i%>_<%= j + 1%>" id="Cell<%= i%>_<%= j + 1%>" style="width:98%;"
                                                  <% if (fillBy[j] == 3) {
                                                          out.print(" disabled ");
                                                  %>>disabled
                                            <%} else {%>
                                            ><%= cellValue.replace("<br/>", "\n")%>
                                            <%}%>
                                        </textarea>
                                        <%
                                        } else if (dataType == 12) { // DataType -  Date
                                        %>
                                        <input type="text" class="formTxtBox_1" style="width: 80%;height: 20px;" readonly="true" name="Cell<%= i%>_<%= j + 1%>" id="Cell<%= i%>_<%= j + 1%>"
                                               <% if (fillBy[j] == 3) { // Filled By Auto
        out.print(" disabled ");
        out.print(" value=\"disabled\" ");


    } else { // Filled By PE User%>
     value="<%= cellValue%>" onfocus="GetCal('Cell<%= i%>_<%= j + 1%>','Cell<%= i%>_<%= j + 1%>')"
                                               <%}%>
                                               />
                                        <% if (fillBy[j] != 3) { // Filled By PE User%>
                                        &nbsp;<a onclick="GetCal('Cell<%= i%>_<%= j + 1%>','Cell<%= i%>_<%= j + 1%>')" id='imgCell<%= i%>_<%= j + 1%>' name='imgCell<%= i%>_<%= j + 1%>' title='Calender'><img src='../resources/images/Dashboard/calendarIcn.png' onclick="GetCal('Cell<%= i%>_<%= j + 1%>','imgCell<%= i%>_<%= j + 1%>')" id='imgCell<%= i%>_<%= j + 1%>' name ='calendarIcn'  alt='Select a Date' border='0' style='vertical-align:middle;'/></a>
                                        <%}%>

                                        <%
                                                }
                                            }
                                        %>
                                        <%
                                            int cmbIndex = 0;
                                            String selected = "";
                                            int listcellId = 0;
                                            if (listCellDetail.size() > 0) {
                                                TblListBoxMaster tblListBoxMaster = listCellDetail.get(0).getTblListBoxMaster();
                                                listcellId = tblListBoxMaster.getListBoxId();
                                            }
                                        %>
                                        <br />
                                        <span id="comboWithCalc<%= i%>_<%= j + 1%>" <% if (dataType == 9) {%>style="display: table-cell;"<% } else {%>style="display: none;"<% }%>>
                                            <select name="selComboWithCalc<%= i%>_<%= j + 1%>" id="selComboWithCalc<%= i%>_<%= j + 1%>"  class="formTxtBox_1">
                                                <%
                                                    for (cmbIndex = 0; cmbIndex < objComboCalWise.get(0).length; cmbIndex++) {
                                                        if (Integer.parseInt(objComboCalWise.get(1)[cmbIndex].toString()) == listcellId) {
                                                            selected = "selected='selected'";
                                                        } else {
                                                            selected = "";
                                                        }
                                                %>
                                                <option value="<%=objComboCalWise.get(1)[cmbIndex]%>" <%=selected%>><%=objComboCalWise.get(0)[cmbIndex]%></option>
                                                <% }%>
                                            </select>
                                        </span>
                                        <span id="comboWithOutCalc<%= i%>_<%= j + 1%>" <% if (dataType == 10) {%>style="display: table-cell;"<% } else {%>style="display: none;"<% }%>>
                                            <select name="selComboWithOutCalc<%= i%>_<%= j + 1%>" id="selComboWithOutCalc<%= i%>_<%= j + 1%>"  class="formTxtBox_1">
                                                <% for (cmbIndex = 0; cmbIndex < objComboWOCalWise.get(0).length; cmbIndex++) {%>
                                                <%
                                                     if (Integer.parseInt(objComboWOCalWise.get(1)[cmbIndex].toString()) == listcellId) {
                                                         selected = "selected='selected'";
                                                     } else {
                                                         selected = "";
                                                     }
                                                %>
                                                <option value="<%=objComboWOCalWise.get(1)[cmbIndex]%>" <%=selected%>><%=objComboWOCalWise.get(0)[cmbIndex]%></option>
                                                <% }%>
                                            </select>
                                        </span>
                                        <%
                                            }
                                        %>

                                    </td>
                                    <%
                                    } else {

                                        int cmbIndex = 0;
                                    %>
                                    <td id="TD<%= i%>_<%= j + 1%>">
                                        <input type="text" class="formTxtBox_1" style="width:98%; height: 30px"
                                               name="Cell<%= i%>_<%= j + 1%>" id="Cell<%= i%>_<%= j + 1%>" />
                                        <span id="comboWithCalc<%= i%>_<%= j + 1%>" style="display: none;">
                                            <select name="selComboWithCalc<%= i%>_<%= j + 1%>" id="selComboWithCalc<%= i%>_<%= j + 1%>"  class="formTxtBox_1">
                                                <%
                                                    for (cmbIndex = 0; cmbIndex < objComboCalWise.get(0).length; cmbIndex++) {
                                                %>
                                                <option value="<%=objComboCalWise.get(1)[cmbIndex]%>"><%=objComboCalWise.get(0)[cmbIndex]%></option>
                                                <% }%>
                                            </select>
                                        </span>
                                        <span id="comboWithOutCalc<%= i%>_<%= j + 1%>" style="display: none;">
                                            <select name="selComboWithOutCalc<%= i%>_<%= j + 1%>" id="selComboWithOutCalc<%= i%>_<%= j + 1%>"  class="formTxtBox_1">
                                                <% for (cmbIndex = 0; cmbIndex < objComboWOCalWise.get(0).length; cmbIndex++) {%>
                                                <option value="<%=objComboWOCalWise.get(1)[cmbIndex]%>"><%=objComboWOCalWise.get(0)[cmbIndex]%></option>
                                                <% }%>
                                            </select>
                                        </span>
                                    </td>
                                    <%
                                            }
                                        }
                                    %>
                                </tr>
                                <%
                                                }
                                            }
                                %>
                            </tbody>
                        </table>
                    </div>
                    <div align="center" class="t_space">
                        <label class="formBtn_1">
                            <input type="submit" name="subBtnCreateEdit" id="subBtnCreateEdit" onclick="return checkValidData(document.getElementById('frmTableCreation'));"
                                   <%if (isInEditMode) {%>
                                   value="Edit"
                                   <%} else {%>
                                   value="Next Step"
                                   <%}%>
                                   />
                        </label>
                    </div>
                </form>
            </div>
            <!--Middle Content Table End-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
        <script>
            var headSel_Obj = document.getElementById("headTabSTD");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
    <%
                if (tableMatrix != null) {
                    tableMatrix = null;
                }
    %>
</html>