<%-- 
    Document   : ViewGovtUserDetail
    Created on : Nov 13, 2010, 2:31:37 PM
    Author     : parag
--%>

<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.egp.eps.model.table.TblEmployeeTrasfer"%>
<%@page import="com.cptu.egp.eps.model.table.TblUserActivationHistory"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TransferEmployeeServiceImpl"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="govSrUser" class="com.cptu.egp.eps.web.servicebean.GovtUserSrBean" />
<jsp:useBean id="projSrUser" class="com.cptu.egp.eps.web.servicebean.ProjectSrBean" />
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod,java.util.List,com.cptu.egp.eps.model.view.VwEmpfinancePower" %>
<%@page import="java.util.List,com.cptu.egp.eps.model.table.TblEmployeeRoles,com.cptu.egp.eps.model.table.TblDepartmentMaster" %>
<%@page import="com.cptu.egp.eps.model.table.TblEmployeeOffices,com.cptu.egp.eps.dao.storedprocedure.SPGovtEmpRolesReturn" %>
<%@page import="java.nio.charset.Charset" %>
<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Profile</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>       
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                 <%@include  file="../resources/common/AfterLoginTop.jsp" %>
                 <%
            String str = request.getContextPath();
            Logger LOGGER = Logger.getLogger("ViewGovtUserDetail.jsp");
            //createGovtUser
            //editGovtUser
            
            String action = "createGovtUser";
            int userId = 0;
            int empId = 0;
            String mode = "";
            String fromWhere = "";
            
            if (request.getParameter("empId") != null) {
                empId = Integer.parseInt(request.getParameter("empId").toString());
            }


            if (request.getParameter("mode") != null) {
                mode = request.getParameter("mode").toString();
            }
            if (request.getParameter("userId") != null) {
                userId = Integer.parseInt(request.getParameter("userId"));
                govSrUser.setUserId(userId);
                
            } else {
                if (empId != 0) {
                    userId = govSrUser.takeUserIdFromEmpId(empId);
                    govSrUser.setUserId(userId);
                }
            }
            
            
            if (request.getParameter("fromWhere") != null) {
                fromWhere = request.getParameter("fromWhere");
            }
            
            if(request.getParameter("mode") != null && request.getParameter("mode").equalsIgnoreCase("view"))
            {
                 // Coad added by Dipal for Audit Trail Log.
                AuditTrail objAuditTrail  = new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL());
                String idType="userId";
                int auditId=userId;
                String auditAction="View Profile of Government User";
                String moduleName=EgpModule.Manage_Users.getName();
                String remarks="";
                MakeAuditTrailService makeAuditTrailService=(MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                makeAuditTrailService.generateAudit(objAuditTrail,auditId, idType,moduleName, auditAction, remarks);
            }
            if ("SearchUser".equalsIgnoreCase(fromWhere) || "ViewProfile".equalsIgnoreCase(fromWhere)) {
                if ("ViewProfile".equalsIgnoreCase(fromWhere)) {
                    userId = Integer.parseInt(session.getAttribute("userId").toString());
                }
                try {
                    empId = govSrUser.takeEmpIdFromUserId(userId);
                } catch (Exception e) {
                    LOGGER.error(" error in takeEmpIdFromUserId  === " + e);
                }
            }
            govSrUser.setEmpId(empId);
            govSrUser.setUserId(userId);
            

            Object[] object = null;
            object = govSrUser.getGovtUserData(String.valueOf(fromWhere));

            //out.println("test===="+ object[7] );
            //out.println("test===="+ object[8] );
            
           
            if(object!=null){
          //  LOGGER.debug(" (String) object[1] " + (String) object[1]);

            List<SPGovtEmpRolesReturn> empRolesReturn = govSrUser.getEmpRoleses(empId);
            

            List<VwEmpfinancePower> vwFinancePower = govSrUser.getGovtFinanceRole();
            


%>
                <!--Middle Content Table Start-->
                <%
                            StringBuilder userType = new StringBuilder();

                            if (request.getParameter("userType") != null) {
                                if (!"".equalsIgnoreCase(request.getParameter("userType"))) {
                                    userType.append(request.getParameter("userType"));
                                } else {
                                    userType.append("org");
                                }
                            } else {
                                userType.append("org");
                            }

                            String strTransferHistoryURL = "ViewDevUserTrsfrHistory.jsp?userId=" + userId;
                            String viewaccounthistory = "ViewGovUserAccountHistory.jsp?userId=" + userId;

                            TransferEmployeeServiceImpl transferEmployeeServiceImpl = (TransferEmployeeServiceImpl) AppContext.getSpringBean("TransferEmployeeServiceImpl");
                            List<TblUserActivationHistory> list = transferEmployeeServiceImpl.viewAccountStatusHistory(userId + "");

                            boolean flag = false;

                            List<TblEmployeeTrasfer> listAccountHistory = transferEmployeeServiceImpl.viewAccountStatusHistoryEmployee(userId);


                            String strReplacedBy = "";
                            for (int i = 0; i < listAccountHistory.size(); i++) {
                                if (!"".equalsIgnoreCase(listAccountHistory.get(i).getReplacedBy()) && listAccountHistory.get(i).getReplacedBy() != null) {
                                    strReplacedBy = listAccountHistory.get(i).getReplacedBy();
                                    flag = true;
                                }
                            }
                %>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr valign="top">
                        <%if (!"ViewProfile".equalsIgnoreCase(fromWhere)) {%>
                        <jsp:include page="../resources/common/AfterLoginLeft.jsp" >
                              <jsp:param name="userType" value="<%=userType.toString()%>"/>
                        </jsp:include>
                        <% }%>
                        <td class="contentArea_1">
                            <!--Page Content Start-->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr valign="top">
                                    <%if (!"ViewProfile".equalsIgnoreCase(fromWhere)) {%>
                                    <td class="contentArea_1"><div class="pageHead_1">View Government User</div>
                                        <%} else {%>
                                    <td class="contentArea_1"><div class="pageHead_1">View Profile</div>
                                        <%}%>
                                        <%-- <div class="pageHead_1">Create User</div>--%>
                                        <form method="POST" id="frmGovUser" action="<%=str%>/GovtUserSrBean?action=<%=action%>">
                                            <%if (!"SearchUser".equalsIgnoreCase(fromWhere)) {
                                            if(empRolesReturn !=null && empRolesReturn.size() >= 1){
                                            
                                            %>
                                            <table width="100%" cellspacing="0" class="tableList_1 t_space" <%if(userTypeId == 17) {%>style="display: none" <% } %>>
                                                <tr>
                                                    <%--<th class="t-align-left" width="5%" align="center">Sr No. </th>--%>
                                                    <th class="t-align-left" >Employee Name </th>
                                                    <th class="t-align-left" >Hierarchy Node </th>
                                                    <th class="t-align-left" >Office</th>
                                                    <th class="t-align-left" >Designation </th>
                                                    <th class="t-align-left" >Procurement Role </th>
                                                    <!--                                                    <th class="t-align-left" >Financial Power</th>-->
                                                </tr>

                                                <%

                                                            for (SPGovtEmpRolesReturn govtEmpRolesReturn : empRolesReturn) {
                                                                String roleId = govtEmpRolesReturn.getProcurementRoleId();
                                                                String roleName = govtEmpRolesReturn.getProcurementRole();
                                                                String finPowerBy = govtEmpRolesReturn.getFinPowerBy();
                                                                int deptId = govtEmpRolesReturn.getDepartmentId();
                                                                String empRoleId = govtEmpRolesReturn.getEmployeeRoleId();
                                                                String officeId = govtEmpRolesReturn.getOfficeId();
                                                                //govtEmpRolesReturn.
%>
                                                <tr>
                                                    <td class="t-align-left"><%=govtEmpRolesReturn.getEmployeeName()%></td>
                                                    <td class="t-align-left"><%=govtEmpRolesReturn.getDepartmentname()%></td>
                                                    <td class="t-align-left"><%=govtEmpRolesReturn.getOfficeName()%></td>
                                                    <td class="t-align-left"><%=govtEmpRolesReturn.getDesignationName()%></td>
                                                    
                                                    <td class="t-align-center">
                                                        <%
                                                            String ReplaceHOPE = govtEmpRolesReturn.getProcurementRole();
        //                                                    while(ReplaceHOPE.charAt(0)==' ')
        //                                                    {
        //                                                        ReplaceHOPE = ReplaceHOPE.substring(1,ReplaceHOPE.length());
        //                                                    }
                                                            if(ReplaceHOPE.contains("HOPE"))
                                                            {
                                                                ReplaceHOPE = ReplaceHOPE.replaceAll("HOPE", "HOPA");
                                                                //out.print("<td width=\"10%\" class=\"t-align-center\">" + ReplaceHOPE + "</td>");
                                                            }
                                                            if(ReplaceHOPE.contains("PE"))
                                                            {
                                                                int LocationOfPE = ReplaceHOPE.indexOf("PE");
                                                                char[] ReplaceHOPEChars = ReplaceHOPE.toCharArray();
                                                                if(LocationOfPE==0)
                                                                {

                                                                    ReplaceHOPEChars[LocationOfPE+1] = 'A';
                                                                    ReplaceHOPE = String.valueOf(ReplaceHOPEChars);

                                                                }
                                                                else
                                                                {
                                                                    if(ReplaceHOPE.charAt(LocationOfPE-1)==',' || ReplaceHOPE.charAt(LocationOfPE-1)==' ')
                                                                    {
                                                                        ReplaceHOPEChars[LocationOfPE+1] = 'A';
                                                                        ReplaceHOPE = String.valueOf(ReplaceHOPEChars);
                                                                    }

                                                                }
                                                            }
                                                            //out.print(ReplaceHOPE);
                                                        %>
                                                        <%=ReplaceHOPE%>
                                                       
                                                    </td>
                                                    <!--                                                    <td class="t-align-center">
                                                        <%
                                                            if (!"Authorized User".equalsIgnoreCase(govtEmpRolesReturn.getProcurementRole().trim()) && !"AU".equalsIgnoreCase(govtEmpRolesReturn.getProcurementRole().trim())) {
                                                        %>
                                                        <a href="#" onclick="window.open('ViewGovtUserFinancePower.jsp?userId=<%=userId%>&empId=<%=empId%>&deptId=<%=deptId%>','window','')" title="View">View</a>
                                                    <%  } else {
                                                                                                                out.print(" - ");
                                                                                                            }%>
                                                    </td>-->
                                                </tr>
                                                <%
                                                            }
                                                %>

                                            </table>
                                            
                                            <%}%>
                                            
                                            <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1">
                                                <tr>
                                                    <td class="ff">e-mail ID : </td>
                                                    <td><% if( object[0] !=null ) out.print((String) object[0]); %>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Full Name : </td>
                                                    <td><% if( object[2] !=null ) out.print((String) object[2]);%>
                                                    </td>
                                                </tr>
                                                <%--tr>
                                                    <td class="ff">Name in Dzongkha : </td>
                                                    <%if (object[3] != null) {
                                                            if (!"".equalsIgnoreCase(new String(((byte[]) object[3]), "UTF-8"))) {%>
                                                    <td><% out.print("" + new String(((byte[]) object[3]), "UTF-8"));%>
                                                    </td>
                                                    <% }
                                                        }%>
                                                </tr--%>
                                                <tr>
                                                    <td class="ff">CID No. : </td>
                                                    <%     if (object[5] !=null && (!"".equalsIgnoreCase((String) object[5]))) {%>
                                                    <td><%out.print((String) object[5]);%>
                                                    </td><%}%>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Employee ID : </td>
                                                
                                                    <%     if (object[7] !=null && (!"".equalsIgnoreCase((String) object[7]))) {%>
                                                    <td><%out.print((String) object[7]);%>
                                                    </td><%}%>
                                                    
                                                </tr>
                                                <tr>
                                                    <td class="ff">Mobile No : </td>
                                                    <%       if (object[4] !=null &&  !"".equalsIgnoreCase(((String) object[4]))) {%>
                                                    <td><%out.print((String) object[4]);%>                                                       
                                                    </td>
                                                    <% }%>
                                                </tr>
                                                <tr>
                                                    <td class="ff">Contact Address : </td>
                                                    <%     if (object[8] !=null && (!"".equalsIgnoreCase((String) object[8]))) {%>
                                                    <td><%out.print((String) object[8]);%>
                                                    </td><%}%>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </table>



                                            <%if ("finalSubmission".equalsIgnoreCase(mode)) {%>
                                                <table border="0" cellspacing="10" cellpadding="0" width="100%" >
                                                <tr class="t-align-center">
                                                    <td class="t-align-center">&nbsp;</td>
                                                    <td class="t-align-center">
                                                        <label class="formBtn_1">
                                                            <input type="button" name="btnSubmission" id="btnSubmission" value="Complete Registration" onclick="finalSubmission();" />
                                                        </label>
                                                    </td>
                                                </tr>
                                                </table>
                                            <%} else {
                                            %>
                                            <table width="100%" cellspacing="10" cellpadding="0" border="0">
                                                <tr>
                                                    <!--<td width="15%">&nbsp;</td>-->
                                                    <td width="85%" class="t-align-left" >
                                                        <% if(flag){%>
                                                        <a href='<%=strTransferHistoryURL%>' class="anchorLink">View Transfer History</a>&nbsp;&nbsp;
                                                        <% } if (list.size() > 0) {%>
                                                        <a href='<%=viewaccounthistory%>' class="anchorLink">View Account History</a>
                                                        <% }%>
                                                    </td>
                                                </tr>

                                            </table>

                                            <%}
                                                        }%>
                                        </form>
                                    </td>

                                </tr>
                            </table>
                            <!--Page Content End-->
                        </td>
                    </tr>
                </table>
                <!--Dashboard Content Part End-->

                <!--Dashboard Footer Start-->
                <%@include file="/resources/common/Bottom.jsp" %>
                <!--Dashboard Footer End-->
            </div>
        </div>
                <script>
                var headSel_Obj = document.getElementById("headTabMngUser");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
<form id="form1" method="post">
    <input type="hidden" name="hidEmailId" id="hidEmailId" value="<%=(String) object[0]%>" />
   <input type="hidden" name="hidPassword" id="hidPassword" value="<%=(String) object[1]%>" />
    <input type="hidden" name="hidMobNo" id="hidMobNo" value="<%=(String) object[4]%>" />
</form>

<script>
    function finalSubmission(){
    var flag=false;
     //$.post("<%=request.getContextPath()%>/GovtUserSrBean", {userId:<%//userId%>,empId:<%=empId%>,action:'finalSubmission'}, function(j){
     //       alert(j);
     //       flag=true;
     // });
     document.getElementById("form1").action="<%=request.getContextPath()%>/GovtUserSrBean?userId=<%=userId%>&empId=<%=empId%>&role=<% if(empRolesReturn !=null && empRolesReturn.size() >= 1){out.print(empRolesReturn.get(0).getProcurementRoleId());} %>&roleName=<% if(empRolesReturn !=null && empRolesReturn.size() >= 1){out.print(empRolesReturn.get(0).getProcurementRole());} %>&action=finalSubmission";
      document.getElementById("form1").submit();
    }

</script>
<%}%>
<%
    govSrUser = null;
    projSrUser = null;
%>