<%--
    Document   : News
    Created on : Jan 1, 2011, 12:50:14 PM
    Author     : Administrator,rishita
--%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.egp.eps.model.table.TblNewsMaster"%>
<%@page import="com.cptu.egp.eps.web.servicebean.NewsMasterSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.HandleSpecialChar"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.CommonXMLSPService"%>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.CommonMsgChk"%>
<%@page import="java.io.*,java.util.*, javax.servlet.*" %>
<%@page import="javax.servlet.http.*" %>
<%@page import="org.apache.commons.fileupload.*"%>
<%@page import="org.apache.commons.fileupload.disk.*" %>
<%@page import="org.apache.commons.fileupload.servlet.*" %>
<%@page import="org.apache.commons.io.output.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.service.serviceimpl.TenderCommonService" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.web.servicebean.TenderDocumentSrBean"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext" %>
<%@page import="com.cptu.egp.eps.web.utility.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><% if(request.getParameter("newsType").equalsIgnoreCase("N"))
                   {
                       out.print("News/Advertisement");
                   }
                   else if(request.getParameter("newsType").equalsIgnoreCase("C"))
                   {
                       out.print("Circular");
                   }
                    else if(request.getParameter("newsType").equalsIgnoreCase("A"))
                   {
                       out.print("Amendment");
                   }
                    else if(request.getParameter("newsType").equalsIgnoreCase("I"))
                   {
                       out.print("Notification");
                   }
            %> Management</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <script type="text/javascript" src="../ckeditor/ckeditor.js"></script>
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/jscal2.css" />
        <link type="text/css" rel="stylesheet" href="../resources/js/datepicker/css/border-radius.css" />
        <script type="text/javascript" src="../resources/js/datepicker/js/jscal2.js"></script>
        <script type="text/javascript" src="../resources/js/datepicker/js/lang/en.js"></script>
        <!--
        <script src="../ckeditor/_samples/sample.js" type="text/javascript"></script>
        <link href="../ckeditor/_samples/sample.css" rel="stylesheet" type="text/css" />
        -->
        <%!
            public String handleSpecialChar(String str) {
                str = str.replaceAll("&", "&amp;");
                str = str.replaceAll(">", "&gt;");
                str = str.replaceAll("<", "&lt;");
                str = str.replaceAll("'", "&#39;");
                str = str.replaceAll("\"", "quot;");
                str = str.replaceAll("-", "&#8211;");
                return str;
            }
        %>
        <script type="text/javascript">
            
            function GetCurrentDate()
            {
                var date = new Date();
                
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                var Hour = date.getHours();
                var Minutes = date.getMinutes();

                if (month < 10) month = "0" + month;
                if (day < 10) day = "0" + day;
                if (Hour < 10) Hour = "0" + Hour;
                if (Minutes < 10) Minutes = "0" + Minutes;

                var today = day + "/" + month + "/" + year + " " + Hour + ":" + Minutes;
                //return today;
                document.getElementById('Time').value = today;
                //document.getElementById('myDate').value = today;
            }
            
            function clearBrief(obj, status)
            {
                var chkNews = '<%=request.getParameter("newsType")%>';
                var Brief = "";
                //var flag=true;

                if (chkNews.toLowerCase() == 'n')
                {
                    Brief = 'News';
                } else if(chkNews.toLowerCase() == 'c')
                {
                    Brief = 'Circular';
                }
                else if(chkNews.toLowerCase() == 'a')
                {
                    Brief = 'Amendment';
                }
                else if(chkNews.toLowerCase() == 'i')
                {
                    Brief = 'Notification';
                }

                if ($.trim($(obj).val()) != "")
                {
                    $('#SP' + obj.name).html('');
                    switch (obj.name)
                    {
                        case "txtNews":
                            if (obj.value.length > 200)
                            {
                                $('#SP' + obj.name).html('Only 200 characters are allowed');
                                document.getElementById("chkFlag").value = false;
                            } else
                                $('#SP' + obj.name).html('');
                            document.getElementById("chkFlag").value = true;
                            break;

                        case "txtNewsDetl":
                            if (obj.value.length > 8000)
                            {
                                $('#SP' + obj.name).html('Only 8000 characters are allowed');
                                document.getElementById("chkFlag").value = false;
                            } else
                                $('#SP' + obj.name).html('');
                            document.getElementById("chkFlag").value = true;
                            break;
                    }
                }
//                else
//                {
//                    $('#SP' + obj.name).html('Please enter ' + Brief + ' ' + status);
//                    document.getElementById("chkFlag").value=false;
//                }
            }
            function chkSubmit()
            {
                var vbool = true;
                var chkNews = '<%=request.getParameter("newsType")%>';
                var Brief = "";
                //var flag=true;

                if (chkNews.toLowerCase() == 'n')
                {
                    Brief = 'News';
                } else if(chkNews.toLowerCase() == 'c')
                {
                    Brief = 'Circular';
                }
                else if(chkNews.toLowerCase() == 'a')
                {
                    Brief = 'Amendment';
                }
                else if(chkNews.toLowerCase() == 'i')
                {
                    Brief = 'Notification';
                }
                if (document.getElementById("txtNews").value == "")
                {
                    document.getElementById("SPtxtNews").innerHTML = "Please enter the " + Brief + " Brief";
                    vbool = false;
                }
                if ($.trim(CKEDITOR.instances.txtNewsDetl.getData()) == "") {
                    document.getElementById("SPtxtNewsDetl").innerHTML = "Please enter the " + Brief + " Details";
                    vbool = false;
                }
                if ($.trim(CKEDITOR.instances.txtNewsDetl.getData().replace(/<[^>]*>|\s/g, '').length) > 3000) {
                    document.getElementById("SPtxtNewsDetl").innerHTML = "Only 3000 characters are allowed";
                    vbool = false;
                }
                if (document.getElementById("chkFlag").value == 'false')
                {
                    vbool = false;
                }

                return vbool;
            }
            $(document).ready(function () {
                $(document).ready(function () {
                    CKEDITOR.instances.txtNewsDetl.on('blur', function ()
                    {
                        if ($.trim(CKEDITOR.instances.txtNewsDetl.getData().replace(/<[^>]*>|\s/g, '').length) <= 3000)
                        {
                            document.getElementById("SPtxtNewsDetl").innerHTML = "";
                        }
                        else{
                           document.getElementById("SPtxtNewsDetl").innerHTML = "Only 3000 characters are allowed"; 
                        }
                    });
                });
            });
        </script>
    </head>
    <body>
        <%
            String userID = "0";
            if (session.getAttribute("userId") != null) {
                userID = session.getAttribute("userId").toString();
            }
        %>
        <!--Dashboard Header Start-->
        <div class="topHeader">
            <%@include  file="../resources/common/AfterLoginTop.jsp" %>
        </div>
        <!--Dashboard Header End-->
        <%   String newsType = request.getParameter("newsType");
            //HandleSpecialChar handleSpecialChar = new HandleSpecialChar();

            if (request.getParameter("ToInsert") != null) {
                NewsMasterSrBean srBean = new NewsMasterSrBean();
                srBean.setLogUserId(userID);
                
                srBean.setAuditTrail(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()));
                if (request.getParameter("nId").equalsIgnoreCase("0")) 
                {        // *** Insert the detail
                    if(request.getParameter("newsType").equalsIgnoreCase("N"))
                    {
                        srBean.insertNewsDetails(request, request.getParameter("newsType"));
                        //TblNewsMaster newsMaster = new TblNewsMaster(0, Short.parseShort(request.getParameter("cmbContentType")), newsType.toCharArray()[0], (request.getParameter("chkImportantNews") != null ? "Yes" : "No"), HandleSpecialChar.handleSpecialChar(request.getParameter("txtNews")), request.getParameter("txtNewsDetl"), new Date(), "No", "No", null, null);
                        
                    }
                    else
                    {
                        srBean.insertCircularDetails(request, request.getParameter("newsType"));
                        //TblNewsMaster newsMaster = new TblNewsMaster(0, Short.parseShort(request.getParameter("cmbContentType")), newsType.toCharArray()[0], (request.getParameter("chkImportantNews") != null ? "Yes" : "No"), HandleSpecialChar.handleSpecialChar(request.getParameter("txtNews")), request.getParameter("txtNewsDetl"), new Date(), "No", "No", "", request.getParameter("LetterNumber"));    
                    }

                    /*SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            newsXml = "<root><tbl_NewsMaster locationId=\"" + request.getParameter("cmbContentType")
                                    + "\" newsType=\"" + newsType + "\" isImp=\"" + (request.getParameter("chkImportantNews") != null ? "Yes" : "No") + "\" "
                                    + "newsBrief=\"" + handleSpecialChar(request.getParameter("txtNews").toString()) + "\" newsDetails=\""
                                    + handleSpecialChar(request.getParameter("txtNewsDetl")) + "\" createdTime=\""+dateFormate.format(new Date())+"\"/></root>";
                     */
                    /*CommonXMLSPService commonXMLSPService =
                                    (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            CommonMsgChk commonMsgChk =
                                    commonXMLSPService.insertDataBySP("insert",
                                    "tbl_NewsMaster",
                                    newsXml,
                                    "").get(0);
                            //out.println(commonMsgChk.getMsg());*/
                } 
                else {
                    
                    if(request.getParameter("newsType").equalsIgnoreCase("N"))
                    {
                        //TblNewsMaster newsMaster = new TblNewsMaster(Integer.parseInt(request.getParameter("nId")), Short.parseShort(request.getParameter("cmbContentType")), newsType.toCharArray()[0], (request.getParameter("chkImportantNews") != null ? "Yes" : "No"), HandleSpecialChar.handleSpecialChar(request.getParameter("txtNews")), request.getParameter("txtNewsDetl"), new Date(), "No", "No", null, null);
                        srBean.updateNewsDetails(request, request.getParameter("newsType"), request.getParameter("nId"));
                    }
                    else
                    {
                        //TblNewsMaster newsMaster = new TblNewsMaster(Integer.parseInt(request.getParameter("nId")), Short.parseShort(request.getParameter("cmbContentType")), newsType.toCharArray()[0], (request.getParameter("chkImportantNews") != null ? "Yes" : "No"), HandleSpecialChar.handleSpecialChar(request.getParameter("txtNews")), request.getParameter("txtNewsDetl"), new Date(), "No", "No", request.getParameter("UploadedFile"), request.getParameter("LetterNumber"));
                        //srBean.insertNewsDetails(newsMaster);
                        srBean.updateCircularDetails(request, request.getParameter("newsType"), request.getParameter("nId"));
                    }
                    /*SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            String fields = "locationId='" + request.getParameter("cmbContentType")
                                    + "', newsType='" + newsType + "' , isImp='" + (request.getParameter("chkImportantNews") != null ? "Yes" : "No") + "'"
                                    + ",newsBrief='" + handleSpecialChar(request.getParameter("txtNews").toString().replaceAll("–", "-")) + "', newsDetails='"
                                    + handleSpecialChar(request.getParameter("txtNewsDetl")) + "'";
                            CommonXMLSPService commonXMLSPService =
                                    (CommonXMLSPService) AppContext.getSpringBean("CommonXMLSPService");
                            CommonMsgChk commonMsgChk =
                                    commonXMLSPService.insertDataBySP("updatetable",
                                    "tbl_NewsMaster",
                                    fields,
                                    "newsId=" + request.getParameter("nId")).get(0);
                            //out.println(commonMsgChk.getMsg());*/
                }
                srBean.setAuditTrail(null);
                srBean = null;
                
                //response.sendRedirect("NewsManagement.jsp?newsType=" + request.getParameter("newsType"));
                String operation = request.getParameter("nId").equalsIgnoreCase("0") ? "added" : "edited"; 
                out.write("<script type=\"text/javascript\"> window.location = \"NewsManagement.jsp?msgId="+operation+"&newsType=" + request.getParameter("newsType")+"\"</script>");
            }

            TenderCommonService tenderCommonService = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
            tenderCommonService.setLogUserId(userID);
        %>
        <div class="dashboard_div">
            <!--Dashboard Content Part Start-->
            <%
                String drive = XMLReader.getMessage("driveLatter").toUpperCase();
                if (request.getParameter("nId") != null) {
                    String newsId = request.getParameter("nId");
                    tenderCommonService
                            = (TenderCommonService) AppContext.getSpringBean("TenderCommonService");
                    List<SPTenderCommonData> list = tenderCommonService.returndata("getnewsbyid",
                            newsId,
                            newsType);
                    String Time = null;
                    String FileName = null;
                    
                    if(list.get(0).getFieldName6()!=null && !list.get(0).getFieldName6().equals(""))
                    {
                        Time = DateUtils.SqlStringDatetoApplicationStringDate(list.get(0).getFieldName6());
                    }
                    if(list.get(0).getFieldName8()!=null && !list.get(0).getFieldName8().equals(""))
                    {
                        FileName = list.get(0).getFieldName8();
                    }
                    
                    if (!list.isEmpty()) {
                        System.out.println("rishita");
                        System.out.println();
            %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                <form id="frmNews" action="News.jsp?newsType=<%=request.getParameter("newsType")%>&nId=<%=request.getParameter("nId")%>&ToInsert=Y" method="POST" enctype="multipart/form-data">
                    <td class="contentArea">
                        <div class="pageHead_1"><% if (list.get(0).getFieldName4().trim().equals("")) {
                                out.print("Create ");
                            } else {
                                out.print("Edit ");
                            }
                        
                            if(request.getParameter("newsType").equalsIgnoreCase("N"))
                            {
                                out.print("News/Advertisement");
                            }
                            else if(request.getParameter("newsType").equalsIgnoreCase("C"))
                            {
                                out.print("Circular");
                            }
                             else if(request.getParameter("newsType").equalsIgnoreCase("A"))
                            {
                                out.print("Amendment");
                            }
                             else if(request.getParameter("newsType").equalsIgnoreCase("I"))
                            {
                                out.print("Notification");
                            }%>
                            Article</div>
                        <div>&nbsp;</div>
                        <table border="0" cellspacing="10" cellpadding="0" class="formStyle_1" width="100%">
                            <tr>
                                <td style="font-style: italic" class="t-align-right" colspan="4">Fields marked with (<span class="mandatory">*</span>) are mandatory</td>
                            </tr>
                            <input type="hidden" name="cmbContentType" value="1">
                            <!--                            <tr >
                                                            <td class="ff" width="13%">Type of Content  : <span class="mandatory">*</span></td>
                                                            <td width="39%">
                                                                <select name="cmbContentType" class="formTxtBox_1" id="select3" style="width:200px;">
                                                                    <a%  List<SPTenderCommonData> cmblocList = tenderCommonService.returndata("getLocationCombo", null, null);
                                                                                                    for (SPTenderCommonData sptcd : cmblocList) {
                                                                                                        if (sptcd.getFieldName2().equalsIgnoreCase("Home Page")) {
                                                                    %>
                                                                    <option value="<a%=sptcd.getFieldName1()%>" selected="true"><a%=sptcd.getFieldName2()%></option>
                                                                    <a%} //else {%>
                                                                    <option value="<!%=sptcd.getFieldName1()%>"><!%=sptcd.getFieldName2()%></option>
                                                                    <a%// }%>
                                                                    <a%}%>
                                                                </select>
                                                            </td>
                                                            <td width="29%" class="ff">&nbsp;</td>
                                                            <td width="19%">&nbsp;</td>
                                                        </tr>-->
                            <tr>
                                <td class="ff"><%
                                   
                                    if(request.getParameter("newsType").equalsIgnoreCase("N"))
                                    {
                                        out.print("News/Advertisement");
                                    }
                                    else if(request.getParameter("newsType").equalsIgnoreCase("C"))
                                    {
                                        out.print("Circular");
                                    }
                                     else if(request.getParameter("newsType").equalsIgnoreCase("A"))
                                    {
                                        out.print("Amendment");
                                    }
                                     else if(request.getParameter("newsType").equalsIgnoreCase("I"))
                                    {
                                        out.print("Notification");
                                    }  

                                %> Brief:<span class="mandatory">*</span></td>
                                <td colspan="2">
                                    <input name="txtNews" type="text" class="formTxtBox_1" id="txtNews" onblur="return clearBrief(this, 'Brief');" value="<%=list.get(0).getFieldName4().trim()%>" style="width:95%;" />
                                    <span id="SPtxtNews" class="reqF_1"></span>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <% if(!request.getParameter("newsType").equalsIgnoreCase("N")) { %>
                            <tr>
                                <td class="ff">
                                    Upload file:
                                </td>
                                <td colspan="2">
                                    <input name="UploadedFile" id="UploadedFile" type="file" class="formTxtBox_1" style="width:450px; background:none;" />
                                    <% if(FileName != null) {%>
                                    <a href="<%=request.getContextPath()%>/TenderSecUploadServlet?funName=downloadNewDocuments&FileName=<%=FileName%>" title="Download" target="_blank">View Current file<a>
                                    <% } %>
                                    <!--<span id="SPtxtNews" class="reqF_1"></span>-->
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">
                                    Letter Number:
                                </td>
                                <td colspan="2">
                                    <input name="LetterNumber" type="text" class="formTxtBox_1" id="LetterNumber" style="width:61%;" <% if(list.get(0).getFieldName7()!=null) {%> value="<%=list.get(0).getFieldName7()%>" <% } %> />
                                    <!--<span id="SPtxtNews" class="reqF_1"></span>-->
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="ff">
                                    Time:<span class="mandatory">*</span>
                                </td>
                                <td class="formStyle_1"><input name="Time" type="text" class="formTxtBox_1" id="Time" style="width:100px;" readonly="true"  onfocus="GetCal('Time', 'Time');" onblur="findHoliday(this, 0);" <% if(Time!=null) {%> value="<%=Time%>" <% } %> />
                                    <img id="TimeImg" src="../resources/images/Dashboard/calendarIcn.png" alt="Select a Date" border="0" style="vertical-align:middle;"  onclick ="GetCal('Time', 'TimeImg');"/>
                                    <span id="SpanTime" ></span>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <% if(Time==null) { %>
                                    <script>
                                        GetCurrentDate();
                                    </script>
                                <% } %>
                            </tr>
                            <% } %>
                            <tr>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    <%
                                        if (list.get(0).getFieldName3().equalsIgnoreCase("Yes")) {
                                    %>
                                    <input name="chkImportantNews" type="checkbox" checked="checked"
                                           value="<%=list.get(0).getFieldName3()%>" />&nbsp; Important 
                                    <%
                                           if(request.getParameter("newsType").equalsIgnoreCase("N"))
                                            {
                                                out.print("News/Advertisement");
                                            }
                                            else if(request.getParameter("newsType").equalsIgnoreCase("C"))
                                            {
                                                out.print("Circular");
                                            }
                                             else if(request.getParameter("newsType").equalsIgnoreCase("A"))
                                            {
                                                out.print("Amendment");
                                            }
                                             else if(request.getParameter("newsType").equalsIgnoreCase("I"))
                                            {
                                                out.print("Notification");
                                            }
                                    %>
                                    <%      } else {%>
                                    <input name="chkImportantNews" type="checkbox"
                                           value="<%=list.get(0).getFieldName3()%>" />&nbsp; Important 
                                    <%
                                       if(request.getParameter("newsType").equalsIgnoreCase("N"))
                                        {
                                            out.print("News/Advertisement");
                                        }
                                        else if(request.getParameter("newsType").equalsIgnoreCase("C"))
                                        {
                                            out.print("Circular");
                                        }
                                         else if(request.getParameter("newsType").equalsIgnoreCase("A"))
                                        {
                                            out.print("Amendment");
                                        }
                                         else if(request.getParameter("newsType").equalsIgnoreCase("I"))
                                        {
                                            out.print("Notification");
                                        }
                                    %>
                                    <% }%>
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="ff" width="21%">
                                    <%
                                       if(request.getParameter("newsType").equalsIgnoreCase("N"))
                                        {
                                            out.print("News/Advertisement");
                                        }
                                        else if(request.getParameter("newsType").equalsIgnoreCase("C"))
                                        {
                                            out.print("Circular");
                                        }
                                         else if(request.getParameter("newsType").equalsIgnoreCase("A"))
                                        {
                                            out.print("Amendment");
                                        }
                                         else if(request.getParameter("newsType").equalsIgnoreCase("I"))
                                        {
                                            out.print("Notification");
                                        }
                                    %> Details : <span>*</span></td>
                                <td colspan="3">
                                    <textarea rows="5" id="txtNewsDetl" name="txtNewsDetl" class="formTxtBox_1" style="width:90%;"><%=list.get(0).getFieldName5()%></textarea>
                                    <script type="text/javascript">
                                        CKEDITOR.replace('txtNewsDetl', {
                                            toolbar: "egpToolbar"
                                        });
                                    </script>
                                    <span id="SPtxtNewsDetl" class="reqF_1"></span>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4" class="t-align-center">
                                    <label class="formBtn_1">
                                        <input type="hidden" id="chkFlag" name="chkFlag" value="true"/>
                                        <input type="submit" name="buttonNews" id="buttonNews"  onclick="return chkSubmit();" value=<%=request.getParameter("nId").equalsIgnoreCase("0") ? "Submit" : "Update"%> />
                                    </label>
                                </td>
                            </tr>
                        </table>
                        <%
                                }
                            }
                        %>
                    </td>
                </form>
                </tr>
            </table>
            <script>
                document.getElementById("txtNews").focus();
            </script>
            <div>&nbsp;</div>
            <!--Dashboard Content Part End-->
        </div>
        <!--Dashboard Footer Start-->
        <%@include file="../resources/common/Bottom.jsp" %>
        <!--Dashboard Footer End-->
        <script>
            
            //document.getElementById('Time').value = GetCurrentDate();
            
            function GetCal(txtname, controlname)
            {
                new Calendar({
                    inputField: txtname,
                    trigger: controlname,
                    showTime: 24,
                    onSelect: function () {
                        var date = Calendar.intToDate(this.selection.get());
                        LEFT_CAL.args.min = date;
                        LEFT_CAL.redraw();
                        this.hide();
                        document.getElementById(txtname).focus();
                    }
                });

                var LEFT_CAL = Calendar.setup({
                    weekNumbers: false
                })
            }
            function findHoliday(comp, compi) 
            {
                $(".err" + compi).remove();
                var compVal = comp.value;
                var cnt = 0;
                if (compVal != null && compVal != "") {
                    for (var i = 0; i < holiArray.length; i++) {
                        if (CompareToForEqual(holiArray[i], compVal)) {
                            cnt++;
                        }
                    }
                }
                if (cnt != 0) {
                    $('#' + comp.id).parent().append("<div class='err" + compi + "' style='color:red;'>Holiday!</div>");
                }
            }
                    
            var newsType = '<%=newsType%>';
            if (newsType == 'N') {
                var obj = document.getElementById('lblNewsCreate');
                if (obj != null) {
                    if (obj.innerHTML == 'Create') {
                        obj.setAttribute('class', 'selected');
                    }
                }
            } else if (newsType == 'A') {
                var obj = document.getElementById('lblAmendmentCreate');
                if (obj != null) {
                    if (obj.innerHTML == 'Create') {
                        obj.setAttribute('class', 'selected');
                    }
                }
            }else if (newsType == 'C') {
                var obj = document.getElementById('lblCircularCreate');
                if (obj != null) {
                    if (obj.innerHTML == 'Create') {
                        obj.setAttribute('class', 'selected');
                    }
                }
            }else if (newsType == 'I') {
                var obj = document.getElementById('lblInstructionCreate');
                if (obj != null) {
                    if (obj.innerHTML == 'Create') {
                        obj.setAttribute('class', 'selected');
                    }
                }
            }

            var headSel_Obj = document.getElementById("headTabContent");
            if (headSel_Obj != null) {
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
