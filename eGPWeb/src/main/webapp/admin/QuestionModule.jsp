<%-- 
    Document   : QuestionModule
    Created on : May 26, 2017, 10:46:28 AM
    Author     : feroz
--%>

<%@page import="com.cptu.egp.eps.model.table.TblQuestionModule"%>
<%@page import="com.cptu.egp.eps.service.serviceinterface.QuestionModuleService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.web.utility.AppContext"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Question Module</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery-1.4.3.min.js"></script>
        <script type="text/javascript" src="../resources/js/jQuery/jquery.validate.js"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>        <script type="text/javascript">
            $(document).ready(function() {
                sortTable();
            });
        </script>
        
    </head>
    <body>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
        <%@include  file="../resources/common/AfterLoginTop.jsp" %>
            </div>
        <%
                    QuestionModuleService questionModuleService = (QuestionModuleService) AppContext.getSpringBean("QuestionModuleService");
                    List<TblQuestionModule> tblQuestionModule = questionModuleService.getAllTblQuestionModule();
         %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>

                    <td class="contentArea">
                        <%
                            String message = "";
                            if (!"".equalsIgnoreCase(request.getParameter("msg"))) 
                            {
                                message = request.getParameter("msg");
                                if ("delS".equalsIgnoreCase(message)) {
                                %>
                                <div class="responseMsg successMsg t_space"><span>Deleted successfully!</span></div><br/>
                                <%}
                                else if ("actF".equalsIgnoreCase(message)){
                                %>
                                    <div class="responseMsg errorMsg t_space"><span>Error occurred while activating Debarment Committee Member!</span></div><br/>
                                <%}else if("edtS".equalsIgnoreCase(message)){%>
                                     <div class="responseMsg successMsg t_space"><span>Updated successfully.</span></div><br/>
                                <%}else if("edtF".equalsIgnoreCase(message)){%>
                                     <div class="responseMsg errorMsg t_space"><span>Error occurred while updating!</span></div><br/>
                                <%}else if("chF".equalsIgnoreCase(message)){%>
                                     <div class="responseMsg errorMsg t_space"><span>There is already an active Chairman!</span></div><br/>
                                <%}else if("crS".equalsIgnoreCase(message)){%>
                                     <div class="responseMsg successMsg t_space"><span>Question Module created successfully!</span></div><br/>
                                <%}
                            }
                        %>
                        <div class="pageHead_1">Question Module
                        </div>
                        <br>
                        <%
                            if(tblQuestionModule.isEmpty())
                            {
                                %><b><span style="color: red; padding-left: 42%; font-size:20px;"><%out.print("No data found!");%></span></b><%
                            }
                            else{
                        %>
                        <table class="tableList_1 t_space" cellspacing="0" width="100%" id="resultTable" cols="">
                                <tbody id="tbodyData">&nbsp;
                                    
                                <tr>
                                    <th class="t-align-center" style="width: 4%">Sl. No.</th>
                                    <th class="t-align-center">Question Module</th>
                                    <th class="t-align-center">Action</th>
                                </tr>
                                <%
                                  int cnt = 0;
                                  for(TblQuestionModule tblQuestionModuleThis : tblQuestionModule){
                                        cnt++;
                                        if(cnt%2==0){%>
                                              <tr>
                                      <%}else{%>
                                              <tr style='background-color:#E4FAD0;'>
                                      <%}%>
                                              <td class="t-align-center">
                                                  <%out.print(cnt);%>
                                              </td>
                                              <td class="t-align-center">
                                                  <%out.print(tblQuestionModuleThis.getModuleName());%>
                                              </td>
                                              <td class="t-align-center">
                                                  <a href="editModule.jsp?moduleId=<%=tblQuestionModuleThis.getModuleId()%>">Edit</a>&nbsp;
                                                  <!--a href="deleteModule.jsp?moduleId=<%=tblQuestionModuleThis.getModuleId()%>">Delete</a-->
                                              </td>
                                              </tr>
                                <%}%>
                                </tbody>
                            </table>
                       <%}%>
                </td>
            </tr>
        </table>

        <form id="formstyle" action="" method="post" name="formstyle">

           <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
           <%
             SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
             String appenddate = dateFormat1.format(new Date());
           %>
           <input type="hidden" name="fileName" id="fileName" value="DebarmentCommittee_<%=appenddate%>" />
            <input type="hidden" name="id" id="id" value="EvaluationRule" />
        </form>
            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
        </div>
    </body>
    <script type="text/javascript">
        var headSel_Obj = document.getElementById("headTabContent");
        if(headSel_Obj != null){
            headSel_Obj.setAttribute("class", "selected");
        }
        var leftObj = document.getElementById("lblViewQM");
        if(leftObj != null){
            leftObj.setAttribute("class", "selected");
        }
    </script>
</html>