<%-- 
    Document   : HelpListing
    Created on : Dec 18, 2010, 5:25:01 PM
    Author     : Administrator
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblHelpManual"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.cptu.egp.eps.web.servicebean.HelpManualSrBean"%>
<jsp:useBean id="helpManualSrBean" class="com.cptu.egp.eps.web.servicebean.HelpManualSrBean"/>
 <%
    StringBuffer colHeader = new StringBuffer();
    colHeader.append("'Sl. <br/> No.','Page Name','Action'");
 %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
         <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Page Help Content List</title>
        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/grid.locale-en.js"  type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.jqGrid.min.js" type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="../resources/js/jQuery/ui.multiselect.css" rel="stylesheet" type="text/css" />
        <script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
        <script type="text/javascript"  language="javascript">
            /* *********** load JQGrid for Help Listing ************** */
            function loadGrid(){
                $("#jQGrid").html("<table id=\"list\"></table><div id=\"page\"></div>");
                jQuery("#list").jqGrid({
                    datatype: 'xml',
                    url : '<%=request.getContextPath()%>/HelpGrid?action=fetchData',
                    height: 233,
                    colNames:[<%=colHeader %>],
                    colModel:[
                        {name:'Sr. No',index:'Sr. No', width:10,sortable: false, search: false, align:'center'},
                        {name:'helpUrl',index:'helpUrl', width:100, search: true,sortable: true, searchoptions: { sopt: ['eq', 'cn'] }},
                        {name:'process',index:'process', width:30,sortable: false, search: false,align:'center'}
                    ],
                    autowidth: true,
                    multiselect: false,
                    paging: true,
                    rowNum:10,
                    rowList:[10,20,30],
                    pager: $("#page"),
                    caption: " ",
                    searchGrid: {sopt:['cn','bw','eq','ne','lt','gt','ew']},
                    gridComplete: function(){
                    $("#list tr:nth-child(even)").css("background-color", "#fff");
                }
                }).navGrid('#page',{edit:false,add:false,del:false});
            }
            /* *********** Call JQGrid for Help Listing ************** */
            $(document).ready(function(){
                loadGrid();
            });

            /* *********** Validation for Confirm Deletion ************** */
            function confirmDelete(id){
                if(window.confirm("Do you want to delete this help content?")){
                    changeStatus(id);
                }else{
                    return false;
                }
            }

            /* *********** change Status of the Help Manual ************** */
            function changeStatus(id){
                try{
                    document.getElementById("hiddeletedid").value = document.getElementById("hiddeletedid").value + id +",";
                    $.ajax({
                        url: "<%=request.getContextPath()%>/HelpGrid?action=changeStatus&pageId="+id,
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("undocontent").style.display = 'block';
                            loadGrid();
                        }
                    });
                    return false;
                }catch(e){
                }
            }

            /* *********** Make Undo changes for the Help Content ************** */
            function undochanges(){
                try{
                    var hidid = document.getElementById("hiddeletedid").value;
                    $.ajax({
                        url: "<%=request.getContextPath()%>/HelpGrid?action=undoChange&pageId="+hidid,
                        method: 'POST',
                        async: false,
                        success: function(j) {
                            document.getElementById("undocontent").style.display = 'none';
                            loadGrid();
                        }
                    });
                    return false;
                }catch(e){
                }
            }
        </script>
    </head>
    <body>
        <%
            String userID="0";
            if(session.getAttribute("userId")!=null){
            userID=session.getAttribute("userId").toString();
            }
            helpManualSrBean.setLogUserId(userID);
        %>
        <%
            String msg="";
            
            /* *********** Delete Help content ************** */
            if("Delete".equals(request.getParameter("action"))){
                int id= Integer.parseInt(request.getParameter("id"));
                msg = helpManualSrBean.deleteHelpManual(id);
                if(msg=="HelpManual Deleted"){
                     response.sendRedirect("HelpListing.jsp");
                }
            }
        %>
        <div class="dashboard_div">
            <%@include file="../resources/common/AfterLoginTop.jsp" %>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="t_space">
                <tr valign="top">
                    <jsp:include page="../resources/common/AfterLoginLeft.jsp" />
                        <%--<jsp:param name="userType" value="<%=userType.toString()%>"/>
                    </jsp:include>--%>
                        <td class="contentArea" style="vertical-align: top;" align="center" valign="middle">
                            <div class="pageHead_1 t-align-left">Page Help Content List
                            <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportToPDF('2');">Save as PDF</a></span>
                            </div>&nbsp;&nbsp;
                        <%if("succ".equalsIgnoreCase(request.getParameter("msg"))){%>
                        <div class="responseMsg successMsg t_space" align="left"><span>Your Help Content updated successfully</span></div>
                        <%}%>
                        <div id="undocontent" style="display:none; margin-top: 12px;" class="tableView_1 ff">Help Content has been deleted  <a href="javascript:void(0);" onclick="undochanges()">Undo</a></div>
                        <div class="t-align-right t_space b_space">
                             <a href="<%=contextPath%>/admin/HelpContent.jsp" class="action-button-add">Create</a>
                        </div>
                        <input type="hidden" name="hiddeletedid" id="hiddeletedid" value="" />
                        <table width="100%" cellspacing="0">
                            <tr valign="top">
                                <td>
                                     <!-- load Grid Here -->
                                    <div class="tabPanelArea_1">
                                        <div id="jQGrid" align="center" style="width: 98.3%;">
                                            <div><table id="list"></table></div>
                                            <div id="page"></div>
                                        </div>
                                    </div>
                                    <!--Bottom controls-->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="pagging_1">

                                    </table>
                                    <div>&nbsp;</div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            </div>
             <!--For Generate PDF  Starts-->
            <form id="formstyle" action="" method="post" name="formstyle">
                <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                <%
                    SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
                    String appenddate = dateFormat1.format(new Date());
                %>
                <input type="hidden" name="fileName" id="fileName" value="Help_<%=appenddate%>" />
                <input type="hidden" name="id" id="id" value="Help" />
            </form>
            <!--For Generate PDF  Ends-->
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            <script>
                var obj = document.getElementById('lblHelpView');
                if(obj != null){
                    if(obj.innerHTML == 'View'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
            <script>
                var headSel_Obj = document.getElementById("headTabContent");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
    </body>
</html>
