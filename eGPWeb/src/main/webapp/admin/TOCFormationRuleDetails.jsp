<%-- 
Document   : TOC_POC Formation RuleDetails
Created on : Nov 19, 2010, 6:00:40 PM
Author     : Naresh.Akurathi
--%>
<%@page import="java.lang.String"%>
<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%
    String strUserTypeId = "";
    Object objUserId = session.getAttribute("userId");
    Object objUName = session.getAttribute("userName");
    boolean isLoggedIn = false;
    if (objUserId != null) {
        strUserTypeId = session.getAttribute("userId").toString();
    }
    if (objUName != null) {
        isLoggedIn = true;
    }
%>

<%@page import="java.util.List"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigTec"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%
            response.setHeader("Expires", "-1");
            response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
            response.addHeader("Cache-Control", "post-check=0, pre-check=0");
            response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>TOC Formation Business Rules Configuration</title>
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
        <script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
        <script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
        <link href="../resources/js/jQuery/jquery.alerts.css" rel="stylesheet" type="text/css" />
        <script src="../resources/js/jQuery/jquery.alerts.js" type="text/javascript"></script>



    </head>
    <body>

        <%
            String msg = "";
            int natureID = 0;
            if ("Submit".equals(request.getParameter("button")) || "Update".equals(request.getParameter("button"))) {

                int row = Integer.parseInt(request.getParameter("TotRule"));

                if ("edit".equals(request.getParameter("action"))) {
                    msg = "Not Deleted";
                } else {
                    msg = configPreTenderRuleSrBean.delAllConfigTec("TOC,POC");
                }

                if (msg.equals("Deleted")) {
                    String action = "Add TOC Formation";
                    try {
                        for (int i = 1; i <= row; i++) {
                            if (request.getParameter("procNature" + i) != null) 
                            {
                                //Nitish Start:: 
                                byte pnature = 0;
                                String test = "";
                                test = (String) (request.getParameter("procNature" + i));
                                if (test.equals("Works")) 
                                {
                                    pnature = 2;
                                } 
                                else if (test.equals("Services")) 
                                {
                                    pnature = 3;
                                } 
                                else if (test.equals("Goods")) 
                                {
                                    pnature = 1;
                                }
                                //Nitish END::

                                //byte pnature = Byte.parseByte(request.getParameter("procNature" + i));
                                double minTender = Double.parseDouble(request.getParameter("minTender" + i));
                                double maxTender = Double.parseDouble(request.getParameter("maxTender" + i));
                                byte minmem = Byte.parseByte(request.getParameter("minMemb" + i));
                                byte maxmem = Byte.parseByte(request.getParameter("maxMemb" + i));
                                byte minoutsidepr = Byte.parseByte(request.getParameter("minMemOutsidePe" + i));
                                byte minmemFromPe = Byte.parseByte(request.getParameter("minMemFromPe" + i));
                                byte minmemformtec = Byte.parseByte(request.getParameter("minMemFromTEC" + i));
                                TblConfigTec configTec = new TblConfigTec();
                                if (pnature == 3) {
                                    configTec.setCommitteeType("POC");
                                } else {
                                    configTec.setCommitteeType("TOC");
                                }

                                configTec.setTblProcurementNature(new TblProcurementNature(pnature));
                                configTec.setMinTenderVal(new BigDecimal(minTender));
                                configTec.setMaxTenderVal(new BigDecimal(maxTender));
                                configTec.setMinMemReq(minmem);
                                configTec.setMaxMemReq(maxmem);
                                configTec.setMinMemOutSidePe(minoutsidepr);
                                configTec.setMinMemFromPe(minmemFromPe);
                                configTec.setMinMemFromTec(minmemformtec);

                                msg = configPreTenderRuleSrBean.addConfigTec(configTec);
                            }
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                        action = "Error in : " + action + " : " + e;
                    } finally {
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                        action = null;
                    }
                } else {
                    String action = "Edit TOC Formation";
                    try {
                        for (int i = 1; i <= row; i++) {
                            if (request.getParameter("procNature" + i) != null) {
                                
                                //Code By Nitish
                                byte pnature = 0;
                                String test = (String) (request.getParameter("procNature" + i));
                                if (test.equals("Works")) {
                                    pnature = 2;
                                } else if (test.equals("Services")) {
                                    pnature = 3;
                                } else if (test.equals("Goods")) {
                                    pnature = 1;
                                }
                                
                                //Code By Nitish

                                //byte pnature = Byte.parseByte(request.getParameter("procNature" + i));
                                double minTender = Double.parseDouble(request.getParameter("minTender" + i));
                                double maxTender = Double.parseDouble(request.getParameter("maxTender" + i));
                                byte minmem = Byte.parseByte(request.getParameter("minMemb" + i));
                                byte maxmem = Byte.parseByte(request.getParameter("maxMemb" + i));
                                byte minoutsidepr = Byte.parseByte(request.getParameter("minMemOutsidePe" + i));
                                byte minmemFromPe = Byte.parseByte(request.getParameter("minMemFromPe" + i));
                                byte minmemformtec = Byte.parseByte(request.getParameter("minMemFromTEC" + i));
                                TblConfigTec configTec = new TblConfigTec();
                                configTec.setConfigTec(Integer.parseInt(request.getParameter("id")));
                                if (pnature == 3) {
                                    configTec.setCommitteeType("POC");

                                } else {
                                    configTec.setCommitteeType("TOC");

                                }

                                configTec.setTblProcurementNature(new TblProcurementNature(pnature));
                                configTec.setMinTenderVal(new BigDecimal(minTender));
                                configTec.setMaxTenderVal(new BigDecimal(maxTender));
                                configTec.setMinMemReq(minmem);
                                configTec.setMaxMemReq(maxmem);
                                configTec.setMinMemOutSidePe(minoutsidepr);
                                configTec.setMinMemFromPe(minmemFromPe);
                                configTec.setMinMemFromTec(minmemformtec);

                                msg = configPreTenderRuleSrBean.updateConfigTec(configTec);
                            }
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                        action = "Error in : " + action + " : " + e;
                    } finally {
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService) AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR") != null ? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                        action = null;
                    }
                }
                if (msg.equals("Values Added")) {
                    msg = "Opening Committee Rule Configured successfully";
                    response.sendRedirect("TOCFormationRuleView.jsp?msg=" + msg);
                }

                if (msg.equals("Updated")) {
                    msg = "Opening Committee Rule Updated successfully";
                    response.sendRedirect("TOCFormationRuleView.jsp?msg=" + msg);
                }
            }
        %>
        <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                        <td class="contentArea">
                            <div class="pageHead_1">TOC Formation Business Rules Configuration</div>

                            <div style="font-style: italic" class="t-align-right t_space"><normal>Fields marked with (<span class="mandatory">*</span>) are mandatory</normal></div>
                                <%                            if (request.getParameter("id") == null) {

                                %>
                                <%--Commit By Nitish--%>
                                <%--
                                <div align="right" class="t_space b_space">
                                    <span id="lotMsg" style="color: red; font-weight: bold; visibility: collapse">&nbsp;</span>
                                    <a id="linkAddRule" class="action-button-add">Add Rule</a> <a id="linkDelRule" class="action-button-delete no-margin">Remove Rule</a>   
                                </div> --%>
                            <%--Commit By Nitish --%>
                                <%}%>
                        <div>&nbsp;</div>


                        <%--<div class="pageHead_1" align="center"> <%=msg%></div>--%>
                        <form action="TOCFormationRuleDetails.jsp" method="post">
                            <table width="100%" cellspacing="0" class="tableList_1" id="tblrule" name="tblrule">
                                <tr>
                                    <%--<th width="5%" >Select</th> --%> <%-- Code Commit by Nitish--%>
                                    <th width="8%" >Committee Type</th>
                                    <th width="8%" >Procurement Category</th>
                                    <th width="10%"  style="display: none;">Min.  Tender  <br />value(In Mn. Nu.)<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%"  style="display: none;">Max. Tender <br />value(In Mn. Nu.)<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%" >Min. Member<br />Required<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%" >Max. Member<br /> Required<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%"  style="display: none;">Min. Members <br /> outside PA<br />(<span class="mandatory">*</span>)</th>
                                    <th width="10%" >Min. Member <br />required from PA <br />(<span class="mandatory">*</span>) </th>
                                    <th width="10%" >Min. Member <br />required from TC <br />(<span class="mandatory">*</span>)       </th>
                                </tr>

                                <%
                                    String procureNature = null;
                                    List lt = null;

                                    if (request.getParameter("id") != null && "edit".equals(request.getParameter("action"))) {
                                        String action = request.getParameter("action");
                                        int id = Integer.parseInt(request.getParameter("id"));
                                        //Code By Nitish :: for Edit rule Configuration
                                        if(request.getParameter("natureId") != null)
                                        {
                                            natureID = Integer.parseInt(request.getParameter("natureId"));
                                        }
                                        //Code By Nitish
                                %>
                                <input type="hidden" id="hiddenaction" name="action" value="<%=action%>"/>
                                <input type="hidden" id ="hiddenid" name="id" value="<%=id%>"/>
                                <%

                                        lt = configPreTenderRuleSrBean.getConfigTec(id);
                                    } else
                                        lt = configPreTenderRuleSrBean.getCommiteeTypeData("TOC,POC");

                                    int i = 0;
                                    if (!lt.isEmpty()) {
                                        List getProcNature = configPreTenderRuleSrBean.getProcurementNature();
                                        TblConfigTec tblconfigtec = new TblConfigTec();
                                        Iterator it = lt.iterator();
                                        while (it.hasNext()) {
                                            i++;
                                            StringBuilder pNature = new StringBuilder();
                                            StringBuilder commitetype = new StringBuilder();
                                            tblconfigtec = (TblConfigTec) it.next();

                                            TblProcurementNature tblProcureNature = new TblProcurementNature();
                                            Iterator pnit = getProcNature.iterator();
                                            while (pnit.hasNext()) {
                                                tblProcureNature = (TblProcurementNature) pnit.next();
                                                pNature.append("<option value='");

                                                if (tblProcureNature.getProcurementNatureId() == tblconfigtec.getTblProcurementNature().getProcurementNatureId()) {
                                                    pNature.append(tblProcureNature.getProcurementNatureId() + "' selected='true'>" + tblProcureNature.getProcurementNature());

                                                } else {
                                                    pNature.append(tblProcureNature.getProcurementNatureId() + "'>" + tblProcureNature.getProcurementNature());
                                                }
                                                pNature.append("</option>");

                                            }

                                            if ("TOC".equals(tblconfigtec.getCommitteeType())) {
                                                commitetype.append("<option value='TOC' selected='true'>TOC</option>");
                                            } else {
                                                commitetype.append("<option selected='true' value='POC'>TOC</option>");
                                            }


                                %>
                                <input type="hidden" name="delrow" value="0" id="delrow"/>
                                <input type="hidden" name="introw" value="" id="introw"/>

                                <tr id="trrule_<%=i%>">

                                    <%
                                        //Code By Nitish :: Start
                                        if (i == 1) 
                                        {
                                            procureNature = "Works";
                                            if (natureID != 0) 
                                            {
                                                if (natureID == 1) 
                                                {
                                                    procureNature = "Goods";
                                                }
                                                if (natureID == 2) 
                                                {
                                                    procureNature = "Works";
                                                }
                                                if (natureID == 3) 
                                                {
                                                    procureNature = "Services";
                                                }
                                            }
                                        }
                                        else if (i == 2) 
                                        {
                                            procureNature = "Services";
                                        } 
                                        else if (i == 3) 
                                        {
                                            procureNature = "Goods";
                                        }
                                        //Code By Nitish :: GET ProcurementNature

                                    %>    
                                    <%--<td class="t-align-center"><input type="checkbox" name="checkbox1" id="checkbox_<%=i%>" value="" /></td> --%>
                                    <td class="t-align-center"><input name="commitType<%=i%>" type="text" class="formTxtBox_1" id="commitType_<%=i%>" value="TOC" readonly="readonly" />
                                        <%--Changed by Nitish:: input feild is readonly from select --%>
                                    </td>
                                    <td class="t-align-center"><input name="procNature<%=i%>" class="formTxtBox_1" id="procNature_<%=i%>" value="<%=procureNature%>" readonly="readonly"/>

                                    </td>
                                    <td class="t-align-center" style="display: none;"><input name="minTender<%=i%>" type="text" class="formTxtBox_1" id="minTender_<%=i%>" style="width:90%;display: none;" value="0" readonly="readonly"/></td>
                                    <td class="t-align-center" style="display: none;"><input name="maxTender<%=i%>" type="text" class="formTxtBox_1" id="maxTender_<%=i%>" style="width:90%;display: none;" value="0" readonly="readonly"/></td>
                                    <td class="t-align-center"><input name="minMemb<%=i%>" type="text" class="formTxtBox_1" id="minMemb_<%=i%>" style="width:90%;" value="<%=tblconfigtec.getMinMemReq()%>" onBlur="return ChkminMemb(this);" maxlength="2"/><span id="MinMemb_<%=i%>" style="color:red;"></span></td>
                                    <td class="t-align-center"><input name="maxMemb<%=i%>" type="text" class="formTxtBox_1" id="maxMemb_<%=i%>" style="width:90%;" value="<%=tblconfigtec.getMaxMemReq()%>" onBlur="return ChkmaxMemb(this);" maxlength="2"/><span id="MaxMemb_<%=i%>" style="color:red;"></span></td>
                                    <td class="t-align-center" style="display: none;"><input name="minMemOutsidePe<%=i%>" type="text" class="formTxtBox_1" id="minMemOutsidePe_<%=i%>" style="width:90%;display: none;" value="0" readonly="readonly"/></td>
                                    <td class="t-align-center"><input name="minMemFromPe<%=i%>" type="text" class="formTxtBox_1" id="minMemFromPe_<%=i%>" style="width:90%;" value="<%=tblconfigtec.getMinMemFromPe()%>" onBlur="return ChkminMemFromPe(this);" maxlength="2"/><span id="MinMemFromPe_<%=i%>" style="color:red;"></span></td>
                                    <td class="t-align-center"><input name="minMemFromTEC<%=i%>" type="text" class="formTxtBox_1" id="minMemFromTEC_<%=i%>" style="width:90%;" value="<%=tblconfigtec.getMinMemFromTec()%>" onBlur="return ChkminMemFromTEC(this);" maxlength="2"/><span id="MinMemFromTEC_<%=i%>" style="color:red;"></span></td>

                                </tr>
                                <% }
                                    } else
                                        msg = "No Record Found";
                                %>


                            </table>
                            <input type="hidden" name="TotRule" id="TotRule" value="<%=i%>"/>
                            <div>&nbsp;</div>
                            <table width="100%" cellspacing="0" >
                                <tr>
                                    <%if ("edit".equals(request.getParameter("action"))) {%>
                                    <td colspan="11" class="t-align-center"><span class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Update" onclick="return check();"/>
                                        </span></td>
                                        <%} else {%>
                                    <td colspan="11" class="t-align-center"><span class="formBtn_1">
                                            <input type="submit" name="button" id="button" value="Submit" onclick="return check();"/>
                                        </span></td>
                                        <%}%>
                                </tr>
                            </table>
                            <div align="center"> <%=msg%></div>
                        </form>
                    </td>
                </tr>
            </table>


            <div>&nbsp;</div>
            <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
            </div>
            <script>
                var obj = document.getElementById('lblTOCFormationRuleEdit');
                if (obj != null) {
                    if (obj.innerHTML == 'Edit') {
                        obj.setAttribute('class', 'selected');
                    }
                }

            </script>
            <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if (headSel_Obj != null) {
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
            <script type="text/javascript">

                function check()
                {
                    var count = document.getElementById("TotRule").value;
                    var flag = true;
                    if (document.getElementById("hiddenaction"))
                    {
                        if ("edit" == document.getElementById("hiddenaction").value)
                        {
                            //Code By Nitish: Start
                            var nature = $('#procNature_1').val();
                            var natureId;
                            
                            if (nature == "Goods")
                            {
                                natureId = 1;
                            }
                            if (nature == "Works")
                            {
                                natureId = 2;
                            }
                            if (nature == "Services")
                            {
                                natureId = 3;
                            }
                            //Code By Nitish :: Get ProcurementNature ID
                            $.ajax({
                                type: "POST",
                                url: "<%=request.getContextPath()%>/BusinessRuleConfigurationServlet",
                                data: "configtecId=" + $('#hiddenid').val() + "&" + "commitType=" + $('#commitType_1').val() + "&" + "procNature=" + natureId + "&" + "funName=TOCajaxUniqueConfig",
                                async: false,
                                success: function (j) {

                                    if (j.toString() != "0") {
                                        flag = false;
                                        jAlert("Combination of 'Committee Type' and 'Procurement Category' already exist.", "TOC Formation Business Rules Configuration", function (RetVal) {
                                        });
                                    }
                                }
                            });
                        }
                    }
                    for (var k = 1; k <= count; k++)
                    {
                        if (document.getElementById("minMemb_" + k) != null)
                        {
                            if (document.getElementById("minMemb_" + k).value == "")
                            {
                                document.getElementById("MinMemb_" + k).innerHTML = "<br/>Please enter Min. Member Required";
                                flag = false;
                            } else
                            {
                                if (numeric(document.getElementById("minMemb_" + k).value))
                                {
                                    if (!numberZero(document.getElementById("minMemb_" + k).value))
                                    {
                                        document.getElementById("MinMemb_" + k).innerHTML = "<br/>Please enter numbers between (1-9) only";
                                        flag = false;
                                    } else
                                    {
                                        document.getElementById("MinMemb_" + k).innerHTML = "";

                                    }
                                } else
                                {
                                    document.getElementById("MinMemb_" + k).innerHTML = "</br>Please Enter numbers only";
                                    flag = false;
                                }
                            }
                        }
                        if (document.getElementById("maxMemb_" + k) != null)
                        {
                            if (document.getElementById("maxMemb_" + k).value == "")
                            {
                                document.getElementById("MaxMemb_" + k).innerHTML = "</br>Please enter Max. Member Required";
                                flag = false;
                            } else
                            {
                                if (numeric(document.getElementById("maxMemb_" + k).value))
                                {
                                    //alert(numeric(document.getElementById("maxMemb_"+k).value));
                                    document.getElementById("MaxMemb_" + k).innerHTML = "";
                                } else
                                {
                                    document.getElementById("MaxMemb_" + k).innerHTML = "</br>Please Enter numbers only";
                                    flag = false;
                                }
                            }
                        }
                        if (document.getElementById("minMemFromPe_" + k) != null)
                        {
                            if (document.getElementById("minMemFromPe_" + k).value == "")
                            {
                                document.getElementById("MinMemFromPe_" + k).innerHTML = "<br/>Please enter Min. Member required from PA";
                                flag = false;
                            } else
                            {
                                if (numeric(document.getElementById("minMemFromPe_" + k).value))
                                {
                                    //alert(numeric(document.getElementById("minMemFromPe_"+k).value));
                                    document.getElementById("MinMemFromPe_" + k).innerHTML = "";
                                } else
                                {
                                    document.getElementById("MinMemFromPe_" + k).innerHTML = "</br>Please Enter numbers only";
                                    flag = false;
                                }
                            }
                        }
                        if (document.getElementById("minMemFromTEC_" + k) != null)
                        {
                            if (document.getElementById("minMemFromTEC_" + k).value == "")
                            {
                                document.getElementById("MinMemFromTEC_" + k).innerHTML = "<br/>Please enter Min. Member required from TEC";
                                flag = false;
                            } else
                            {
                                if (numeric(document.getElementById("minMemFromTEC_" + k).value))
                                {
                                    //alert(numeric(document.getElementById("minMemFromTEC_"+k).value));
                                    document.getElementById("MinMemFromTEC_" + k).innerHTML = "";
                                } else
                                {
                                    document.getElementById("MinMemFromTEC_" + k).innerHTML = "</br>Please Enter numbers only";
                                    flag = false;
                                }
                            }
                        }
                    }

                    // Start OF Dhruti--Checking for Unique Rows

                    if (document.getElementById("TotRule") != null) {
                        var totalcount = eval(document.getElementById("TotRule").value);
                    } //Total Count After Adding Rows
                    //alert(totalcount);
                    var chk = true;
                    var i = 0;
                    var j = 0;
                    for (i = 1; i <= totalcount; i++) //Loop For Newly Added Rows
                    {
                        if (document.getElementById("minMemb_" + i) != null)
                            if ($.trim(document.getElementById("minMemb_" + i).value) != '' && $.trim(document.getElementById("maxMemb_" + i).value) != '' && $.trim(document.getElementById("minMemFromPe_" + i).value) != '' && $.trim(document.getElementById("minMemFromTEC_" + i).value) != '') // If Condition for When all Data are filled thiscode is Running
                            {
                                chk = false;
                                for (j = 1; j <= totalcount && j != i; j++) // Loop for Total Count but Not same as (i) value
                                {
                                    if ($.trim(document.getElementById("procNature_" + i).value) == $.trim(document.getElementById("procNature_" + j).value))
                                    {
                                        chk = true;
                                        break;
                                    }
                                }
                                if (flag) {
                                    if (chk == true) //If Row is same then give alert message
                                    {
                                        jAlert("Duplicate record found. Please enter unique record", "TOC Formation Business Rules Configuration");
                                        return false;
                                    }
                                }

                            }
                    }
                    // End OF Dhruti--Checking for Unique Rows


                    if (flag == false)
                    {
                        return false;
                    }

                }
                function numberZero(value)
                {
                    var flag;
                    //alert(value.indexOf("."));
                    if (isNaN(value) == false)
                    {
                        if (value.indexOf(".") == -1)
                        {
                            if (value > 0)
                            {
                                flag = true;
                            } else
                            {
                                flag = false;
                            }
                        } else
                        {
                            flag = false;
                        }
                    } else
                    {
                        flag = false;
                    }
                    if (flag == false)
                    {
                        return false;
                    } else
                    {
                        return true;
                    }
                }
                function ChkminMemb(obj)
                {
                    var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                    var chk = true;

                    if (obj.value != '')
                    {
                        if (numeric(obj.value))
                        {
                            if (!numberZero(obj.value))
                            {
                                document.getElementById("MinMemb_" + i).innerHTML = "<br/>Please enter numbers between (1-9) only";
                                flag = false;
                            } else
                            {
                                document.getElementById("MinMemb_" + i).innerHTML = "";

                            }
                        } else
                        {
                            document.getElementById("MinMemb_" + i).innerHTML = "</br>Please Enter numbers only";
                            chk = false;
                        }
                    } else
                    {
                        document.getElementById("MinMemb_" + i).innerHTML = "</br>Please enter Min. Member Required";
                        chk = false;
                    }
                    if (chk == false)
                    {
                        return false;
                    }
                }
                ;

                function ChkmaxMemb(obj)
                {
                    var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                    var chk1 = true;

                    if (obj.value != '')
                    {
                        if (numeric(obj.value))
                        {
                            document.getElementById("MaxMemb_" + i).innerHTML = "";
                        } else
                        {
                            document.getElementById("MaxMemb_" + i).innerHTML = "</br>Please Enter numbers only";
                            chk1 = false;
                        }
                    } else
                    {
                        document.getElementById("MaxMemb_" + i).innerHTML = "</br>Please enter Max. Member Required";
                        chk1 = false;
                    }
                    if (chk1 == false)
                    {
                        return false;
                    }
                }
                ;

                function ChkminMemFromTEC(obj)
                {
                    var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                    var chk2 = true;

                    if (obj.value != '')
                    {
                        if (numeric(obj.value))
                        {
                            document.getElementById("MinMemFromTEC_" + i).innerHTML = "";
                        } else
                        {
                            document.getElementById("MinMemFromTEC_" + i).innerHTML = "</br>Please Enter numbers only";
                            chk2 = false;
                        }
                    } else
                    {
                        document.getElementById("MinMemFromTEC_" + i).innerHTML = "</br>Please enter Min. Member required from TEC";
                        chk2 = false;
                    }
                    if (chk2 == false)
                    {
                        return false;
                    }
                }
                ;

                function ChkminMemFromPe(obj)
                {
                    var i = (obj.id.substr(obj.id.indexOf("_") + 1));
                    var chk3 = true;

                    if (obj.value != '')
                    {
                        if (numeric(obj.value))
                        {
                            document.getElementById("MinMemFromPe_" + i).innerHTML = "";
                        } else
                        {
                            document.getElementById("MinMemFromPe_" + i).innerHTML = "</br>Please Enter numbers only";
                            chk3 = false;
                        }
                    } else
                    {
                        document.getElementById("MinMemFromPe_" + i).innerHTML = "</br>Please enter Min. Member required from PA";
                        chk3 = false;
                    }
                    if (chk3 == false)
                    {
                        return false;
                    }

                }
                ;

                function numeric(value)
                {
                    return /^\d+$/.test(value);
                }


        </script>


        <%!    ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();

        %>


        <%
            StringBuilder tenderType = new StringBuilder();
            StringBuilder procNature = new StringBuilder();
            configPreTenderRuleSrBean.setLogUserId(strUserTypeId);
            TblProcurementNature tblProcureNature2 = new TblProcurementNature();
            Iterator pnit2 = configPreTenderRuleSrBean.getProcurementNature().iterator();
            while (pnit2.hasNext()) {
                tblProcureNature2 = (TblProcurementNature) pnit2.next();
                procNature.append("<option value='" + tblProcureNature2.getProcurementNatureId() + "'>" + tblProcureNature2.getProcurementNature() + "</option>");
            }

        %>


        <script type="text/javascript">

            var counter = 1;
            var delCnt = 0;
            $(function () {
                $('#linkAddRule').click(function () {
                    counter = eval(document.getElementById("TotRule").value);
                    delCnt = eval(document.getElementById("delrow").value);

                    $('span.#lotMsg').css("visibility", "collapse");
                    var htmlEle = "<tr id='trrule_" + (counter + 1) + "'>" +
                            "<td class='t-align-center'><input type='checkbox' name='checkbox" + (counter + 1) + "' id='checkbox_" + (counter + 1) + "' value='" + (counter + 1) + "' style='width:90%;' style='width:90%;'/></td>" +
                            "<td class='t-align-center'><select name='commitType" + (counter + 1) + "' class='formTxtBox_1' id='commitType_" + (counter + 1) + "' ><option value='TOC'>TOC</option></select></td>" +
                            "<td class='t-align-center'><select name='procNature" + (counter + 1) + "' class='formTxtBox_1' id='procNature_" + (counter + 1) + "'><%=procNature%></select></td>" +
                            "<td  class='t-align-center' style='display: none;'><input type='text' name='minTender" + (counter + 1) + "' class='formTxtBox_1' id='minTender_" + (counter + 1) + "' style='width:90%;' value='0' readonly='readonly'/></td>" +
                            "<td  class='t-align-center'style='display: none;'><input type='text' name='maxTender" + (counter + 1) + "' class='formTxtBox_1' id='maxTender_" + (counter + 1) + "' style='width:90%;' value='0' readonly='readonly'/></td>" +
                            "<td  class='t-align-center'><input type='text' name='minMemb" + (counter + 1) + "' class='formTxtBox_1' id='minMemb_" + (counter + 1) + "' style='width:90%;' onBlur='return ChkminMemb(this);' maxlength='2'/><span id='MinMemb_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                            "<td  class='t-align-center'><input type='text' name='maxMemb" + (counter + 1) + "' class='formTxtBox_1' id='maxMemb_" + (counter + 1) + "' style='width:90%;' onBlur='return ChkmaxMemb(this);' maxlength='2'/><span id='MaxMemb_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                            "<td  class='t-align-center' style='display: none;'><input type='text' name='minMemOutsidePe" + (counter + 1) + "' class='formTxtBox_1' id='minMemOutsidePe_" + (counter + 1) + "' style='width:90%;' value='0' readonly='readonly'/></td>" +
                            "<td  class='t-align-center'><input type='text' name='minMemFromPe" + (counter + 1) + "' class='formTxtBox_1' id='minMemFromPe_" + (counter + 1) + "' style='width:90%;' onBlur='return ChkminMemFromPe(this);' maxlength='2'/><span id='MinMemFromPe_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                            "<td  class='t-align-center'><input type='text' name='minMemFromTEC" + (counter + 1) + "' class='formTxtBox_1' id='minMemFromTEC_" + (counter + 1) + "' style='width:90%;' onBlur='return ChkminMemFromTEC(this);' maxlength='2'/><span id='MinMemFromTEC_" + (counter + 1) + "' style='color: red;'>&nbsp;</span></td>" +
                            "</tr>";
                    $("#tblrule").append(htmlEle);
                    document.getElementById("TotRule").value = (counter + 1);

                });
            });

            $(function () {
                $('#linkDelRule').click(function () {
                    counter = eval(document.getElementById("TotRule").value);
                    delCnt = eval(document.getElementById("delrow").value);

                    var tmpCnt = 0;
                    for (var i = 1; i <= counter; i++) {
                        if (document.getElementById("checkbox_" + i) != null && document.getElementById("checkbox_" + i).checked) {
                            tmpCnt++;
                        }
                    }

                    if (tmpCnt == (eval(counter - delCnt))) {
                        $('span.#lotMsg').css("visibility", "visible");
                        $('span.#lotMsg').css("color", "red");
                        $('span.#lotMsg').html('Minimum 1 record is needed!');
                    } else {
                        if (tmpCnt > 0) {
                            jConfirm('Are You Sure You want to Delete.', 'Procurement Method Business Rule Configuration', function (ans) {
                                if (ans) {
                                    for (var i = 1; i <= counter; i++) {
                                        if (document.getElementById("checkbox_" + i) != null && document.getElementById("checkbox_" + i).checked) {
                                            $("tr[id='trrule_" + i + "']").remove();
                                            $('span.#lotMsg').css("visibility", "collapse");
                                            delCnt++;
                                        }
                                    }
                                    document.getElementById("delrow").value = delCnt;
                                }
                            });
                        } else {
                            jAlert("please Select checkbox first", "Procurement Method Business Rule Configuration", function (RetVal) {});
                        }
                    }
                });
            });
        </script>

    </body>
</html>
