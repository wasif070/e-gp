<%--
    Document   : Add NewClause
    Created on : 24-Oct-2010, 3:13:11 PM
    Author     : yanki
--%>

<jsp:useBean id="sectionClauseSrBean" class="com.cptu.egp.eps.web.servicebean.SectionClauseSrBean" />
<jsp:useBean id="createSubSectionSrBean" class="com.cptu.egp.eps.web.servicebean.CreateSubSectionSrBean" />
<jsp:useBean id="prepareTDSSrBean" class="com.cptu.egp.eps.web.servicebean.PrepareTDSSrBean" />
<jsp:useBean id="defineSTDInDtlSrBean" class="com.cptu.egp.eps.web.servicebean.DefineSTDInDtlSrBean"  />

<%@page import="com.cptu.egp.eps.model.table.TblTemplateMaster" %>
<%@page import="java.util.List" %>
<%@page import="com.cptu.egp.eps.dao.storedprocedure.SPTenderCommonData" %>
<%@page import="com.cptu.egp.eps.model.table.TblIttClause" %>
<%@page import="com.cptu.egp.eps.model.table.TblIttSubClause" %>
<%@page import="com.cptu.egp.eps.model.table.TblIttHeader" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <%
                if (session.getAttribute("userId") != null) {
                    sectionClauseSrBean.setLogUserId(session.getAttribute("userId").toString());
                    createSubSectionSrBean.setLogUserId(session.getAttribute("userId").toString());
                    prepareTDSSrBean.setLogUserId(session.getAttribute("userId").toString());
                    defineSTDInDtlSrBean.setLogUserId(session.getAttribute("userId").toString());
                }

                int templateId = 0;
                if (request.getParameter("templateId") != null) {
                    templateId = Integer.parseInt(request.getParameter("templateId"));
                }
                String isPDF = "abc";
                if (request.getParameter("isPDF") != null) {
                    isPDF = request.getParameter("isPDF");
                }
                String procType = "";
                String procTypefull = "";
                String STDName = "";
                List<TblTemplateMaster> templateMasterLst = defineSTDInDtlSrBean.getTemplateMaster((short) templateId);
                if (templateMasterLst != null) {
                    if (templateMasterLst.size() > 0) {
                        procType = templateMasterLst.get(0).getProcType();
                        if ("goods".equalsIgnoreCase(procType)) {
                            procTypefull = "Goods";
                        } else if ("works".equalsIgnoreCase(procType)) {
                            procTypefull = "Works";
                        } else if ("srvcmp".equalsIgnoreCase(procType)) {
                            procTypefull = "Services - Consulting Firms";
                        } else if ("srvindi".equalsIgnoreCase(procType)) {
                            procTypefull = "Services - Individual Consultant";
                        } else if ("srvnoncon".equalsIgnoreCase(procType)) {
                            procTypefull = "Services - Non consulting services";
                        }
                        STDName = templateMasterLst.get(0).getTemplateName();
                    }
                    templateMasterLst = null;
                }

                int sectionId = Integer.parseInt(request.getParameter("sectionId"));
                List<TblIttHeader> headerInfo = createSubSectionSrBean.getSubSectionDetail(sectionId);

                String contentType = createSubSectionSrBean.getContectType(sectionId);
                String sectionName = "";
                if (contentType.equalsIgnoreCase("ITT")) {
                    sectionName = createSubSectionSrBean.getSecName((short) templateId, "TDS");
                } else if (contentType.equalsIgnoreCase("GCC")) {
                    sectionName = createSubSectionSrBean.getSecName((short) templateId, "PCC");
                }
                if ("ITT".equals(contentType)) {  contentType="ITB";}
                else if("TDS".equals(contentType)){contentType="BDS";}
                else if("PCC".equals(contentType)){contentType="SCC";} 
                String contentType1 = "";
                String instruction = "";
                String clauseInstr = "";
                if (contentType.equalsIgnoreCase("ITT") || contentType.equalsIgnoreCase("TDS")) {
                    contentType1 = "BDS";
                    instruction = "Instructions for completing Tender/Proposal Data Sheet are provided in italics in parenthesis for the relevant ITB clauses";
                    clauseInstr = "Amendments of, and Supplements to, Clauses in the Instructions to Tenderers/Consultants";
                } else if (contentType.equalsIgnoreCase("ITC")) {
                    contentType1 = "PDS";
                    instruction = "Instructions for completing Proposal Data Sheet are provided in italics in parenthesis for the relevant ITC clauses";
                    clauseInstr = "Amendments of, and Supplements to, Clauses in the Instructions to Tenderers/Consultants";
                } else if (contentType.equalsIgnoreCase("PCC") || contentType.equalsIgnoreCase("GCC")) {
                    contentType1 = "SCC";
                    instruction = "Instructions for completing the Particular Conditions of Contract are provided in italics in parenthesis for the relevant GCC Clauses.";
                    clauseInstr = "Amendments of, and Supplements to, Clauses in the General Conditions of Contract";
                }

    %>
    <head>
        <%
                    response.setHeader("Expires", "-1");
                    response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
                    response.addHeader("Cache-Control", "post-check=0, pre-check=0");
                    response.setHeader("Pragma", "no-cache");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View <%=contentType1%></title>

        <link href="../resources/css/home.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
        <link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
        <script>            
            function printPage()
            {
                document.getElementById("TDSView").submit();
            }
        </script>
        <style type="text/css">
            ul li {margin-left: 20px;}
        </style>
    </head>
    <body>
        <div class="mainDiv">
            <div class="fixDiv">
                <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                <% }%>
                <!--Middle Content Table Start-->
                <div class="pageHead_1">View <%=contentType1%></div>
                <table width="100%" cellspacing="0" class="tableList_1 t_space">
                    <tr>
                        <td width="30%" align="center"><b>Name of SBD :</b></td>
                        <td width="70%"><%=STDName%></td>
                    </tr>
                    <tr>
                        <td width="30%" align="center"><b>Procurement Type :</b></td>
                        <td width="70%"><%=procTypefull%></td>
                    </tr>
                    <tr>
                        <td width="30%" align="center"><b>Section :</b></td>
                        <td width="70%"><%=sectionName%></td>
                    </tr>
                </table>
                <form action="<%=request.getContextPath()%>/PDFServlet" id="TDSView" method="post">
                    <%--<input type="text" name="reportPage"  value="<%=request.getContextPath()%>/admin/TDSView.jsp?<%=request.getQueryString()%>" />--%>
                    <input type="hidden" name="reportName"  value="<%=contentType1%> View" />
                    <table width="100%" cellspacing="0" id="tdsInfo" class="tableList_1 t_space">
                        <tr>
                            <td colspan="2"><%=instruction%></td>
                        </tr>
                        <tr>
                            <td><b><%=contentType%> Clause</b></td>
                            <td><%=clauseInstr%></td>
                        </tr>
                        <%
                                    int k = 1, i = -1;
                                    for (int k1 = 0; k1 < headerInfo.size(); k1++) {
                                        int ittHeaderId = headerInfo.get(k1).getIttHeaderId();


                                        List<SPTenderCommonData> tblTdsSubClause = prepareTDSSrBean.getTDSSubClause(ittHeaderId);

                                        //tblTdsSubClause
                                        String ittClauseId = "-1";                                            /*
                                        ittHeaderId, ittClauseId, ittClauseName,
                                        ittsubclauseid, ittSubClauseName, tdsSubClauseName,
                                        tdsSubClauseId, ittHeaderName, tdsSubClauseId,
                                        orderNo
                                         */


                                        if (tblTdsSubClause.size() > 0) {
                        %>
                        <tr>
                            <td class="t-align-center" colspan="2"><b><%=tblTdsSubClause.get(0).getFieldName8()%></b></td>
                        </tr>
                        <%
                                                                }
                                                                for (i = 0; i < tblTdsSubClause.size(); i++) {
                        %>
                        <tr id="tr_<%=k++%>">
                            <%
                                                                                                    if (!ittClauseId.equals(tblTdsSubClause.get(i).getFieldName2())) {
                                                                                                        ittClauseId = tblTdsSubClause.get(i).getFieldName2();
                            %>
                            <td style="line-height: 1.75"><%=contentType%> Clause</td>
                            <td style="line-height: 1.75"><%=tblTdsSubClause.get(i).getFieldName3()%></td>

                            <%
                                                                                                    }
                            %>
                        </tr>
                        <tr>
                            <td style="line-height: 1.75">&nbsp;</td>
                            <td style="line-height: 1.75"><%if (tblTdsSubClause.get(i).getFieldName5() != null) {
                                                                                                        out.print(tblTdsSubClause.get(i).getFieldName5());
                                                                                                    } else {
                                                                                                        out.print("<i>Information Not Available</i>");
                                                                                                    }%></td>
                        </tr>
                        <tr>
                            <td style="line-height: 1.75">&nbsp;</td>
                            <td style="line-height: 1.75"><%if (tblTdsSubClause.get(i).getFieldName6() != null) {
                                                                                                        out.print(tblTdsSubClause.get(i).getFieldName6());
                                                                                                    } else {
                                                                                                        out.print("<i>Information Not Available</i>");
                                                                                                    }%></td>
                        </tr>
                        <%
                                        }
                                    }
                        %>
                        <!--                                        <tr><td class="t-align-center" colspan="2">
                                                <a class="action-button-savepdf" onclick="printPage();">Save As PDF</a></td></tr>-->
                    </table>
                    <input type="hidden" id="total" name ="total" value="<%=i%>" />
                </form>
                <!--Page Content End-->
                <!--Middle Content Table End-->
                <% if (!(isPDF.equalsIgnoreCase("true"))) {%>
                <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
                <% }%>
            </div>
        </div>
        <script>
            var headSel_Obj = document.getElementById("headTabSTD");
            if(headSel_Obj != null){
                headSel_Obj.setAttribute("class", "selected");
            }
        </script>
    </body>
</html>
<%
            if (defineSTDInDtlSrBean != null) {
                defineSTDInDtlSrBean = null;
            }
%>