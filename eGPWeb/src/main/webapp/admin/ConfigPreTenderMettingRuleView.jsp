<%-- 
    Document   : ConfigPreTenderMettingRuleView
    Created on : Nov 26, 2010, 11:30:05 AM
    Author     : Naresh.Akurathi
--%>



<%@page import="com.cptu.eps.service.audit.EgpModule"%>
<%@page import="com.cptu.eps.service.audit.AuditTrail"%>
<%@page import="com.cptu.eps.service.audit.MakeAuditTrailService"%>
<%@page import="java.util.List"%>
<%@page import="com.cptu.egp.eps.model.table.TblConfigPreTender"%>
<%@page import="com.cptu.egp.eps.model.table.TblTenderTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementMethod"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementTypes"%>
<%@page import="com.cptu.egp.eps.model.table.TblProcurementNature"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.cptu.egp.eps.web.servicebean.ConfigPreTenderRuleSrBean"%>
<%@page import="java.util.Date"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
     <%
        response.setHeader("Expires", "-1");
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Pre-Bid meeting rule Details</title>
<link href="../resources/css/defaultDashboard.css" rel="stylesheet" type="text/css" />
<link href="../resources/css/theme_1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../resources/js/ddlevelsmenu.js"></script>
<script src="../resources/js/jQuery/jquery-1.4.3.min.js" type="text/javascript"></script>
<script src="../resources/js/jQuery/jquery.validate.js"type="text/javascript"></script>
<script src="../resources/js/form/ConvertToWord.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.generatepdf.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/resources/js/jQuery/jquery.tablesorter.js"></script>
   <script type="text/javascript">
            $(document).ready(function() {
                sortTable();
            });
        </script>
<script type="text/javascript">
    function conform()
    {
        if (confirm("Do you want to delete this business rule?"))
            return true;
        else
            return false;
    }
</script>

</head>
<body>
    <%!
  ConfigPreTenderRuleSrBean configPreTenderRuleSrBean = new ConfigPreTenderRuleSrBean();
 
    %>
    <%
            String mesg =  request.getParameter("msg");
            String delMsg = "";
            if("delete".equals(request.getParameter("action"))){
                String action = "Remove Pre Tender Meeting Rules";
                try{
                        int configId = Integer.parseInt(request.getParameter("id"));
                        TblConfigPreTender configPreTender = new TblConfigPreTender();
                        configPreTender.setPreTenderMeetId(configId);
                        configPreTender.setPostQueDays(Short.parseShort("1"));
                        configPreTender.setIsPreTenderMeetingfReq("s");
                        configPreTender.setPubBidDays(Short.parseShort("1"));
                        configPreTender.setTblProcurementMethod(new TblProcurementMethod(Byte.parseByte("1")));
                        configPreTender.setTblProcurementNature(new TblProcurementNature(Byte.parseByte("1")));
                        configPreTender.setTblProcurementTypes(new TblProcurementTypes(Byte.parseByte("1")));
                        configPreTender.setTblTenderTypes(new TblTenderTypes(Byte.parseByte("1")));
                        configPreTender.setUploadedTenderDays(Short.parseShort("1"));
                        configPreTenderRuleSrBean.dellConfigPreTenderrule(configPreTender);
                        delMsg = "Pre-Bid Meeting Business Rule Deleted successfully";
                  }catch(Exception e){
                        System.out.println(e);
                        action = "Error in : "+action +" : "+ e;
                  }finally{
                        MakeAuditTrailService makeAuditTrailService = (MakeAuditTrailService)AppContext.getSpringBean("MakeAuditTrailService");
                        makeAuditTrailService.generateAudit(new AuditTrail(request.getHeader("X-FORWARDED-FOR")!=null? request.getHeader("X-FORWARDED-FOR") : request.getRemoteAddr(), session.getAttribute("sessionId"), session.getAttribute("userTypeId"), request.getRequestURL()), (Integer) session.getAttribute("userId"), "userId", EgpModule.Configuration.getName(), action, "");
                        action=null;
                   }
                }
    %>

    <div class="dashboard_div">
            <!--Dashboard Header Start-->
            <div class="topHeader">
                <%--<jsp:include page="../resources/common/AfterLoginTop.jsp" ></jsp:include>--%>
                <%@include file="../resources/common/AfterLoginTop.jsp" %>
                
            </div>
            <!--Dashboard Header End-->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <jsp:include  page="../resources/common/AfterLoginLeft.jsp"></jsp:include>
                    <td class="contentArea">
  <!--Dashboard Header End-->
  <!--Dashboard Content Part Start-->
  <div class="pageHead_1">Pre-Bid meeting rule configuration Details
  <span style="float: right;"><a class="action-button-savepdf" href="javascript:void(0);" onclick="exportDataToPDF('5');">Save as PDF</a></span>
  </div>
  <div>&nbsp;</div>
  <%if("delete".equals(request.getParameter("action"))){%>
  <div class="responseMsg successMsg"><%=delMsg%></div>
  <%}%>
  <%if(mesg != null){%>
  <div class="responseMsg successMsg"><%=mesg%></div>
  <%}%>
  
 <table width="100%" cellspacing="0" class="tableList_1 t_space" name="resultTable" id="resultTable"  cols="@5">
     <tr  >
        <th >Tender Type<br /></th>
        <!--<th >Procurement Nature<br /></th>-->
        <th >Procurement Method<br /></th>
        <th >Procurement Type<br /></th>
       <!-- <th >Pre Tender Meeting Required<br /></th>-->
        <th >Days from Publication when Pre-Bid meeting is scheduled<br /></th>
       <!-- <th >Posting of questions allowed before no. of days from  tender closing date<br /></th>-->
        <th >Days allowed for uploading Pre-Bid MOM, after Pre-Bid meeting is over</th>
        <th >Action</th>
    </tr>


     <%
            String msg = "";
            List l = configPreTenderRuleSrBean.getConfigPreTenderRuleDetails();
            TblConfigPreTender tblcon = new TblConfigPreTender();
            int i=0;
  
            if(!l.isEmpty()|| l != null){
                    Iterator it = l.iterator();
                    while(it.hasNext())
                    {
                    tblcon = (TblConfigPreTender)it.next();i++;
                   
    %>
    
    <%if(i%2==0){%>
      <tr style='background-color:#E4FAD0;'>
          <%}else{%>
          <tr>
          <%}%>
        <td class="t-align-center"><%=tblcon.getTblTenderTypes().getTenderType()%></td>
        <!--<td class="t-align-center"><%=tblcon.getTblProcurementNature().getProcurementNature()%></td>-->
        <td class="t-align-center"><%if(tblcon.getTblProcurementMethod().getProcurementMethod().equalsIgnoreCase("RFQ")){out.print("LEM");}else if(tblcon.getTblProcurementMethod().getProcurementMethod().equalsIgnoreCase("DPM")){out.print("DCM");}else{out.print(tblcon.getTblProcurementMethod().getProcurementMethod());}%></td>
        <td class="t-align-center"><%if(tblcon.getTblProcurementTypes().getProcurementType().equalsIgnoreCase("NCT")){out.print("NCB");} else if(tblcon.getTblProcurementTypes().getProcurementType().equalsIgnoreCase("ICT")){out.print("ICB");} %></td>
        <!--<td class="t-align-center"><%=tblcon.getIsPreTenderMeetingfReq()%></td>-->
        <td class="t-align-center"><%=tblcon.getPubBidDays()%></td>
        <!--<td class="t-align-center"><%=tblcon.getPostQueDays()%></td>-->
        <td class="t-align-center"><%=tblcon.getUploadedTenderDays()%></td>
        <td class="t-align-center"><a href="ConfigPreTenderMettingRuleDetails.jsp?action=edit&id=<%=tblcon.getPreTenderMeetId()%>">Edit</a>&nbsp;|&nbsp;<a href="ConfigPreTenderMettingRuleView.jsp?action=delete&id=<%=tblcon.getPreTenderMeetId()%>" onclick="return conform();">Delete</a></td>

          </tr></tr>
<%

        }
 }
            else
                {                    
                    msg = "No Record Found";
                }

%>

      </table>
    
    <div align="center"> <%=msg%></div>
      </td>
    </tr>
  </table>
      <form id="formstyle" action="" method="post" name="formstyle">

           <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
           <%
             SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MM-yyyy(hh_mm)");
             String appenddate = dateFormat1.format(new Date());
           %>
           <input type="hidden" name="fileName" id="fileName" value="PreTenderRule_<%=appenddate%>" />
            <input type="hidden" name="id" id="id" value="PreTenderRule" />
        </form>
  <div>&nbsp;</div>
   <jsp:include page="../resources/common/Bottom.jsp" ></jsp:include>
  
</div>
    <script>
                var obj = document.getElementById('lblConfigPreTenderMettingRuleView');
                if(obj != null){
                    if(obj.innerHTML == 'View'){
                        obj.setAttribute('class', 'selected');
                    }
                }

        </script>
    <script>
                var headSel_Obj = document.getElementById("headTabConfig");
                if(headSel_Obj != null){
                    headSel_Obj.setAttribute("class", "selected");
                }
            </script>
</body>
</html>

